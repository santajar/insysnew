﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using ipXML;

namespace ConsoleBatchEms
{
    class Program
    {
        static string sXmlFile;
        static string sXmlResult;
        static string sXmlLog;

        static SqlConnection oSqlConn = new SqlConnection();

        static void Main(string[] args)
        {
            Init();
            EMS_XML oEms = new EMS_XML();
            oEms = InitEmsXml();
            string sErrMsg = null;
            string sFileResult = string.Format(@"{0}\{1}.xml", sXmlResult, oEms.Attribute.sSenderID);
            if (EMSCommonClass.IsXMLInquiry(oSqlConn, ref sErrMsg, ref oEms, null, sFileResult))
                sErrMsg = "SUCCESS";
            else
                sErrMsg = "ERROR : " + sErrMsg;
            EMSCommonClass.WriteLog(sErrMsg);
            Console.Write(sErrMsg);
        }

        static void Init()
        {
            oSqlConn = new SqlConnection(new InitData().sGetConnString());
            InitDirectory();
        }

        static void InitDirectory()
        {
            string sCurrDir = Environment.CurrentDirectory;
            sXmlLog = sCurrDir + @"\EMS\LOG";
            sXmlFile = sCurrDir + @"\EMS\File";
            sXmlResult = sCurrDir + @"\EMS\Result";

            if (!Directory.Exists(sXmlLog))
                Directory.CreateDirectory(sXmlLog);
            if (!Directory.Exists(sXmlFile))
                Directory.CreateDirectory(sXmlFile);
            if (!Directory.Exists(sXmlResult))
                Directory.CreateDirectory(sXmlResult);

            EMSCommonClass.DirectoryLog = sXmlLog;
            EMSCommonClass.DirectoryXml = sXmlFile;
            EMSCommonClass.DirectoryResult = sXmlResult;
        }

        static EMS_XML InitEmsXml()
        {
            string sSenderId = string.Format("EMSINQ{0:yyyyMMdd}", DateTime.Now);

            EMS_XML.AttributeClass oAttribute = new EMS_XML.AttributeClass();
            oAttribute.sSenderID = sSenderId;
            oAttribute.sSenders = "EMS";
            oAttribute.sVersions = "1.00";
            oAttribute.sTypes = EMSCommonClass.XMLRequestType.INQ.ToString();

            EMS_XML.DataClass.FilterClass oFilter = new EMS_XML.DataClass.FilterClass();
            oFilter.sFilterFieldName="Terminal_INIT";
            oFilter.sValues = "*";

            EMS_XML.DataClass oData = new EMS_XML.DataClass();
            oData.doAddFilter(oFilter);

            EMS_XML oEms = new EMS_XML();
            oEms.Attribute = oAttribute;
            oEms.Data = oData;
            return oEms;
        }

        static void ReadEMSFile(string sPath, ref List<DataTable> dtXML)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(sPath);
            foreach (DataTable dtTemp in ds.Tables)
            {
                dtXML.Add(dtTemp);
            }
            ds.Dispose();
        }

        static DataTable dtGetTable(EMSCommonClass.XMLProperty oProperty, List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = new DataTable();
            dtTemp = dtXMLTemp.Find(delegate(DataTable dt) { return dt.TableName == oProperty.ToString(); });
            return dtTemp;
        }

        static EMSCommonClass.XMLRequestType XMLAttributeReqType(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Attribute, dtXMLTemp);
            string sTemp = dtTemp.Rows[0]["Type"].ToString().ToUpper();

            if (sTemp == "INQ")
                return EMSCommonClass.XMLRequestType.INQ;
            else if (sTemp == "ADD/UPDATE" || sTemp == "ADD_UPDATE")
                return EMSCommonClass.XMLRequestType.ADD_UPDATE;
            else
                return EMSCommonClass.XMLRequestType.DELETE;
        }

        static string sXMLSenderId(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Attribute, dtXMLTemp);
            return dtTemp.Rows[0]["Sender_ID"].ToString();
        }

        static string sXMLFilterField(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Filter, dtXMLTemp);
            return dtTemp.Rows[0]["Field_Name"].ToString();
        }

        static string sXMLFilterValue(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Filter, dtXMLTemp);
            return dtTemp.Rows[0]["Values"].ToString();
        }

        static string sXMLTerminalInitValue(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Terminal, dtXMLTemp);
            return dtTemp.Rows[0]["Terminal_INIT"].ToString();
        }

        static void doXMLViewTest(List<DataTable> dtXMLTemp)
        {
            using (SqlCommand oSqlCmd = new SqlCommand(EMSCommonSP.XML.sViewTable, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sSenderId", SqlDbType.VarChar).Value = sXMLSenderId(dtXMLTemp);
                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }

                using (SqlDataReader oReader = oSqlCmd.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    if (oReader.Read())
                        dt.Load(oReader);
                    oReader.Close();
                }
            }
        }        
    }
}
