﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace InSysConsoleNewZip_BNI
{
    public class PacketProtocol
    {
        byte[] arrbPacket;
        List<byte[]> ltarrbRecvPacket = new List<byte[]>();
        int iTotalPacket = 0;
        static string sTpduNii = "600996";

        public PacketProtocol(byte[] _arrbPacket)
        {
            arrbPacket = new byte[_arrbPacket.Length];
            arrbPacket = _arrbPacket;
            sTpduNii = ConfigurationManager.AppSettings["TPDU NII"].ToString();

            ProcessPacket();
        }

        protected void ProcessPacket()
        {
            int iIndex = 0;

            string sTemp = CommonLib.sByteArrayToHexString(arrbPacket).Replace(" ", "");
            int iLengthOfRecvPacket = sTemp.Length;
            int iLengthOfPacket = CommonConsole.iISOLenReceive(sTemp.Substring(0, 4));
            iIndex = 4;
            while (iIndex < iLengthOfRecvPacket)
            {
                string sTempPacket = sTemp.Substring(iIndex - 4, iLengthOfPacket * 2);
                if (!string.IsNullOrEmpty(sTempPacket))
                {
                    ltarrbRecvPacket.Add(CommonLib.HexStringToByteArray(sTempPacket));

                    iIndex += iLengthOfPacket * 2;
                    iLengthOfPacket = CommonConsole.iISOLenReceive(sTemp.Substring(iIndex, 4));
                    iIndex += 4;

                    if (iIndex == iLengthOfRecvPacket) break;
                }
                else
                    break;
            }
            iTotalPacket = ltarrbRecvPacket.Count;
        }

        public void GetPacketList(ref List<byte[]> _ltarrbPacket, ref int _iTotalPacket)
        {
            _ltarrbPacket = ltarrbRecvPacket;
            _iTotalPacket = iTotalPacket;
        }
    }
}
