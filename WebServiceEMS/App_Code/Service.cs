using System;
using System.Web.Services;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using System.Web.Configuration;
using ipXML;
using System.Data;

[WebService(Namespace = "http://www.integra-pratama.co.id/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Service : System.Web.Services.WebService
{
    public Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    protected List<string> ltStringColumnXML;    
    protected List<SqlEMS_XML.ValueXML> ltObjValueXML;
    //protected SqlConnection oSqlConn;
    protected string sConnString;
    protected string stimestart;
    protected string sterminalid;

    protected string sXmlLog;
    protected string sXmlFile;
    protected string sXmlResult;
    protected string sFileResult = "";

    protected bool bDebug = false;
    protected string sPath = "";
    protected string sPathTest = "";
    
    static SqlConnection oSqlConn = new SqlConnection();

    List<DataTable> dtXmlTemp = new List<DataTable>();

    EMS_XML localparam ;
       

    [WebMethod]
    public EMS_XML sStartXMLWebService(EMS_XML _oXML)
    {
        stimestart = Trace.sFullTime();
        EMS_XML oXml = new EMS_XML();
        EMS_XML oXmlResponse = new EMS_XML();
        try
        {
            bDebug = Convert.ToBoolean(WebConfigurationManager.AppSettings["Debug"].ToString());
            sPath = WebConfigurationManager.AppSettings["Path"].ToString();
            sXmlFile = sPath;
            sXmlLog = sPath;
            sXmlResult = sPath;

            doInitiateConnectionString();
            oXml = oXmlResponse = _oXML;          

            InitDirectory();

            string sResult = sProcessXML(ref oXmlResponse);

        }                
        catch (Exception ex)
        {
            string sErrMessage = "TMS Services Process Failed";
            sErrMessage = ex.ToString();
            Trace.Write(sPath, Trace.sDateYYYYMMDD(), string.Format("[{0}] => ", Trace.sFullTime()) + sterminalid + " " + sErrMessage);
            
        }
        return oXmlResponse;

    }

    static void ReadEMSFile(string sPath, ref List<DataTable> dtXML)
    {
        DataSet ds = new DataSet();
        ds.ReadXml(sPath);
        foreach (DataTable dtTemp in ds.Tables)
        {
            dtXML.Add(dtTemp);
        }
        ds.Dispose();
    }
    static EMS_XML InitEmsXml()
    {
        string sSenderId = string.Format("EMSINQ{0:yyyyMMdd}", DateTime.Now);

        EMS_XML.AttributeClass oAttribute = new EMS_XML.AttributeClass();
        oAttribute.sSenderID = sSenderId;
        oAttribute.sSenders = "EMS";
        oAttribute.sVersions = "4.00";
        //oAttribute.sTypes = EMSCommonClass.XMLRequestType.INQ.ToString();
        oAttribute.sTypes = EMSCommonClass.XMLRequestType.ADD_UPDATE.ToString();

        EMS_XML.DataClass.FilterClass oFilter = new EMS_XML.DataClass.FilterClass();
        oFilter.sFilterFieldName = "Terminal_Init";
        oFilter.sValues = "*";

        EMS_XML.DataClass oData = new EMS_XML.DataClass();
        oData.doAddFilter(oFilter);

        EMS_XML oEms = new EMS_XML();
        oEms.Attribute = oAttribute;
        oEms.Data = oData;
        return oEms;
    }

    protected void InitDirectory()
    {
        //string sCurrDir = HostingEnvironment.ApplicationPhysicalPath;
        //sXmlLog = sCurrDir + @"\BIN\EMS\LOG";
        //sXmlFile = sCurrDir + @"\BIN\EMS\File";
        //sXmlResult = sCurrDir + @"\BIN\EMS\Result";

        string sCurrDir = WebConfigurationManager.AppSettings["Path"].ToString();
        sXmlLog = sCurrDir + @"\LOG";
        sXmlFile = sCurrDir + @"\FILE";
        sXmlResult = sCurrDir + @"\RESULT";

        if (!Directory.Exists(sXmlLog))
            Directory.CreateDirectory(sXmlLog);
        if (!Directory.Exists(sXmlFile))
            Directory.CreateDirectory(sXmlFile);
        if (!Directory.Exists(sXmlResult))
            Directory.CreateDirectory(sXmlResult);

        EMSCommonClass.Directory = sCurrDir;
        EMSCommonClass.DirectoryLog = sXmlLog;
        EMSCommonClass.DirectoryXml = sXmlFile;
        EMSCommonClass.DirectoryResult = sXmlResult;
    }

    protected void doInitiateConnectionString()
    {
        //oSqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        //if (oSqlConn.State != ConnectionState.Open)
        //{
        //    oSqlConn.Close();
        //    oSqlConn.Open();
        //}
        sConnString = WebConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
    }

    protected string sProcessXML(ref EMS_XML _oXml)
    {
        string sErrMessage = "XML Process Success";
        int iError = 0;
        try
        {
            EMSCommonClass.XMLRequestType oReqType = XMLAttributeType(_oXml);
            sFileResult = string.Format(@"{1}\{0}.xml", _oXml.Attribute.sSenderID, sPath);

            EMSCommonClass.UserAccessDelete(new SqlConnection(sConnString), null);
            switch (oReqType)
            {
                case EMSCommonClass.XMLRequestType.ADD_UPDATE:
                    //EMSCommonClass.IsXMLAddUpdate(oSqlConn, ref sErrMessage, ref _oXml, null, sFileResult);
                    //EMSCommonClass.IsXMLAddUpdate(sConnString, ref sErrMessage, ref _oXml, null, sFileResult);

                    sterminalid = "";
                    sterminalid = _oXml.Attribute.sSenderID.Substring(_oXml.Attribute.sSenderID.Length - 9, 8);
                    Trace.Write(sPath, _oXml.Attribute.sSenderID, string.Format("[{0}] WEBSERVICE START ", stimestart));                    
                    Trace.Write(sPath, _oXml.Attribute.sSenderID, string.Format("[{0}] WEBSERVICE ADD_UPDATE "+ sterminalid +" START ", Trace.sFullTime()));
                    EMSCommonClass.IsXMLAddUpdate(sConnString, ref sErrMessage, ref iError, ref _oXml, null, sFileResult);
                    Trace.Write(sPath, _oXml.Attribute.sSenderID, string.Format("[{0}] WEBSERVICE ADD_UPDATE "+ sterminalid+" FINISH " + iError + " ==> " + sErrMessage, Trace.sFullTime() ));
                    Trace.Write(sPath, _oXml.Attribute.sSenderID, string.Format("[{0}] WEBSERVICE END ", Trace.sFullTime()));
                    break;
                case EMSCommonClass.XMLRequestType.DELETE:
                    //EMSCommonClass.IsXMLDelete(oSqlConn, ref sErrMessage, ref _oXml, null, sFileResult);
                    //EMSCommonClass.IsXMLDelete(sConnString, ref sErrMessage, ref _oXml, null, sFileResult);
                    EMSCommonClass.IsXMLDelete(sConnString, ref sErrMessage, ref iError, ref _oXml, null, sFileResult);
                    break;
                case EMSCommonClass.XMLRequestType.INQ:
                case EMSCommonClass.XMLRequestType.INQINIT:
                    //EMSCommonClass.IsXMLInquiry(oSqlConn, ref sErrMessage, ref _oXml, null, sFileResult);
                    //EMSCommonClass.IsXMLInquiry(sConnString, ref sErrMessage, ref _oXml, null, sFileResult);
                    EMSCommonClass.IsXMLInquiry(sConnString, ref sErrMessage, ref iError, ref _oXml, null, sFileResult);
                    break;
            }
        }
        catch (Exception ex)
        {
            sErrMessage = ex.ToString();
            //Trace.Write(sPath, Trace.sDateYYYYMMDD(), sErrMessage);
            Trace.Write(sPath, Trace.sDateYYYYMMDD(), string.Format("[{0}] ", Trace.sFullTime()) + sErrMessage);
            EMS_XML oResultEms = new EMS_XML();
            oResultEms.Attribute = _oXml.Attribute;

            foreach (EMS_XML.DataClass.TerminalClass oTerminalClass in _oXml.Data.ltTerminal)
            {
                EMS_XML.DataClass.TerminalClass oTempTerminalClass = new EMS_XML.DataClass.TerminalClass();

                oTempTerminalClass.AddColumnValue("Terminal_Init", oTerminalClass.sGetColumnValue("Terminal_Init".ToUpper()));
                oTempTerminalClass.AddColumnValue("Process_Type".ToUpper(), _oXml.Attribute.sTypes);

                oResultEms.Data.Terminal.AddColumnValue("Response_Code", "001");
                oTempTerminalClass.Result.sResultFieldName = "WEBSERVICE";
                oTempTerminalClass.Result.sResultCode = "001";
                oTempTerminalClass.Result.sResultDesc = sErrMessage;
                oTempTerminalClass.Result.sTimeProcess = DateTime.Now.ToString("ddMMyyyyhhmmss");

                oResultEms.Data.ltTerminal.Add(oTempTerminalClass);
            }
            _oXml = oResultEms;
        }
        return sErrMessage;
    }

    protected EMSCommonClass.XMLRequestType XMLAttributeType(EMS_XML _oXml)
    {
        if (_oXml.Attribute.sTypes.ToLower().Contains("inq"))
            return EMSCommonClass.XMLRequestType.INQ;
        else if (_oXml.Attribute.sTypes.ToLower().Contains("inqinit"))
            return EMSCommonClass.XMLRequestType.INQINIT;
        else if (_oXml.Attribute.sTypes.ToLower().Contains("del"))
            return EMSCommonClass.XMLRequestType.DELETE;
        else
            return EMSCommonClass.XMLRequestType.ADD_UPDATE;
    }

    class Trace
    {
        static string sPath = Environment.CurrentDirectory;
        //Path.GetDirectoryName(            Assembly.GetExecutingAssembly().GetName().CodeBase);

        public static string sDirName = "";
        public static string sFilename = "Trace_";
        static string sFileType = ".log";

        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }
        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static void Write(string sPath, string sFileName, string sMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFileName;
                sFile = sFile.Contains(sFileType) ? sFile : sFile + sFileType;

                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                while (true)
                {
                    if (IsOpenFileAllowed(sFile))
                        break;
                }
                Write2File(sFile, sMessage, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static bool isTraceFolderExist()
        {
            return Directory.Exists(sPath + sDirName);
        }

        public static bool IsOpenFileAllowed(string sFilename)
        {
            bool bAllow = false;
            FileStream fs = null;
            try
            {
                fs = File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite);
                fs.Close();
                bAllow = true;
            }
            catch (IOException)
            {
                bAllow = false;
            }
            return bAllow;
        }

        /// <summary>
        /// Write Value in string format to text file
        /// </summary>
        /// <param name="sFileName"> string : File name including file path which will be written</param>
        /// <param name="sValue">string : Value to be written to the file</param>
        /// <param name="bNewLine">bool : Value to be using new line on the file</param>
        public static void Write2File(string sFileName, string sValue, bool bNewLine)
        {
            //FileStream oFileStream;
            //StreamWriter oStreamWriter;
            try
            {
                using (FileStream oFileStream = new FileStream(sFileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter oStreamWriter = new StreamWriter(oFileStream))
                    {
                        oStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        if (bNewLine) oStreamWriter.WriteLine(sValue);
                        else oStreamWriter.Write(sValue);
                    }
                    //oStreamWriter.Close();
                    //oStreamWriter.Dispose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //oFileStream.Close();
            //oFileStream.Dispose();
        }
    }


}