﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;
using System.Configuration;
using System.IO;

namespace AppUpdateConnection
{
    public partial class FrmMoveData : Form
    {
        /*
         * v1.00
         * Feature : update DatabaseId of specified TerminalID
         * 
         * v2.00 - 26 Oct 2016
         * Feature : 
         *      - add new tag for Terminal/Acquirer/Issuer with default value
         *      - remove unused tag for Terminal/Acquirer/Issuer
         */
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        int iDatabaseIdSource;
        string sDatabaseIDSource;
        int iDatabaseIdDest;
        string sDatabaseIDDest;

        List<string> ltTerminalId = new List<string>();
        string sFilename;
        static string sUserID;

        public FrmMoveData()
        {
            InitializeComponent();            
        }

        public FrmMoveData(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConn;
            sUserID = _sUserID;
            UserData.sUserID = sUserID;
        }

        private void FrmMoveData_Load(object sender, EventArgs e)
        {
            if (oSqlConn == null)
            {
                oSqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString);
                oSqlConnAuditTrail = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString);
            }

            if (string.IsNullOrEmpty(sUserID))
            {
                FrmLogin login = new FrmLogin();
                login.ShowDialog();
                if (login.bLogin)
                {
                    sUserID = login.sUserName;
                    InitComboBoxDatabase();
                }
                else
                    this.Close();
                login.Dispose();
            }

            InitComboBoxDatabase();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                SetDisplay(false);
                bwMoveData.RunWorkerAsync();
            }
        }

        private bool IsValid()
        {
            iDatabaseIdDest = int.Parse(cmbDbDestination.SelectedValue.ToString());
            sDatabaseIDDest = cmbDbDestination.Text;
            iDatabaseIdSource = int.Parse(cmbDbSource.SelectedValue.ToString());
            sDatabaseIDSource = cmbDbSource.Text;
            ltTerminalId = new List<string>();

            if (!string.IsNullOrEmpty(sFilename))
            {
                InitTerminalID();
                if (iDatabaseIdDest != 0)
                {
                    if (iDatabaseIdSource != 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void bwMoveData_DoWork(object sender, DoWorkEventArgs e)
        {
            DataTable dtItemList = dtGetItemList(iDatabaseIdDest);
            foreach (string sTerminalID in ltTerminalId)
            {
                string sMessage = null;
                try
                {
                    if (IsValidTerminalSource(sTerminalID))
                    {
                        if (CommonClass.IsAllowUserAccess(oSqlConn, sTerminalID, ref sUserID))
                        {
                            CommonClass.UserAccessInsert(oSqlConn, sTerminalID);
                            
                            try
                            {
                                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                                DataTable dtProfileTerminal = dtGetProfileFullTable(sTerminalID);

                                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPMoveData, oSqlConn))
                                {
                                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                                    oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                                    oSqlCmd.Parameters.Add("@iDBSourceID", SqlDbType.VarChar).Value = iDatabaseIdSource;
                                    oSqlCmd.Parameters.Add("@iDBDestID", SqlDbType.VarChar).Value = iDatabaseIdDest;
                                    oSqlCmd.ExecuteNonQuery();
                                }

                                if (dtProfileTerminal != null && dtProfileTerminal.Rows.Count > 0)
                                {
                                    RemoveUnusedTag(dtProfileTerminal, dtItemList, sTerminalID);
                                    AddNewTag(dtProfileTerminal, dtItemList, sTerminalID);
                                }
                                
                                sMessage = string.Format("Move '{0}' SUCCESS, from '{1}' to '{2}'", sTerminalID, sDatabaseIDSource, sDatabaseIDDest);
                                CommonClass.InputLog(oSqlConn, "", sUserID, "", sMessage, sMessage);
                            }
                            catch (Exception ex)
                            {
                                sMessage = string.Format("Move '{0}' FAILED, {1}", sTerminalID, ex.Message);
                            }
                            CommonClass.UserAccessDelete(oSqlConn, sTerminalID);
                        }
                        else
                            sMessage = string.Format("Move '{0}' FAILED, profile is being accessed by {1}", sTerminalID, sUserID);
                    }
                    else sMessage = string.Format("{0} FAILED", sTerminalID);
                }
                catch (SqlException sqlex)
                {
                    sMessage = string.Format("{0} FAILED : {1}", sTerminalID, sqlex);
                }
                bwMoveData.ReportProgress(100, sMessage);
            }
        }

        private void AddNewTag(DataTable dtProfileTerminal, DataTable dtItemList, string sTerminalID)
        {
            AddNewTagTerminal(dtProfileTerminal, dtItemList, sTerminalID);
            AddNewTagAcquirerIssuer(dtProfileTerminal, dtItemList, sTerminalID, true);
            AddNewTagAcquirerIssuer(dtProfileTerminal, dtItemList, sTerminalID, false);
        }

        private void AddNewTagAcquirerIssuer(DataTable dtProfileTerminal, DataTable dtItemList, string sTerminalID, bool bAcquirer)
        {
            // if Acquirer, bAcquirer = true
            // if Issuer, bAcquirer = false
            DataTable dtItemTerminal = bAcquirer ?
                dtItemList.Select("FormID = 2").CopyToDataTable() :
                dtItemList.Select("FormID = 3").CopyToDataTable();

            DataTable dtNameList = bAcquirer ?
                dtProfileTerminal.Select("Tag LIKE 'AA001'").CopyToDataTable() :
                dtProfileTerminal.Select("Tag LIKE 'AE001'").CopyToDataTable();

            foreach (DataRow rowsNameList in dtNameList.Rows)
            {
                string sName = rowsNameList["Name"].ToString();
                foreach (DataRow rowsItem in dtItemTerminal.Rows)
                {
                    string sTag = rowsItem["Tag"].ToString();
                    DataRow[] rowsTagProfile = dtProfileTerminal.Select(string.Format("Tag='{0}' AND Name='{1}'", sTag, sName));
                    if (rowsTagProfile == null || rowsTagProfile.Length == 0)
                    {
                        string sDefaultValue = rowsItem["DefaultValue"].ToString();

                        string sQuery = bAcquirer ?
                            string.Format("INSERT INTO tbProfileAcquirer(TerminalID, AcquirerName, AcquirerTag, AcquirerLengthOfTagLength, AcquirerTagLength, AcquirerTagValue) VALUES ('{0}','{1}','{2}',3,'{3}','{4}')", sTerminalID, sName, sTag, sDefaultValue.Length, sDefaultValue) :
                            string.Format("INSERT INTO tbProfileIssuer(TerminalID, IssuerName, IssuerTag, IssuerLengthOfTagLength, IssuerTagLength, IssuerTagValue) VALUES ('{0}','{1}','{2}',3,'{3}','{4}')", sTerminalID, sName, sTag, sDefaultValue.Length, sDefaultValue);

                        if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                        using (SqlCommand sqlCmd = new SqlCommand(sQuery, oSqlConn))
                            sqlCmd.ExecuteNonQuery();
                    }
                }
            }
        }

        private void AddNewTagTerminal(DataTable dtProfileTerminal, DataTable dtItemList, string sTerminalID)
        {
            DataTable dtItemTerminal = dtItemList.Select("FormID = 1").CopyToDataTable();
            foreach (DataRow rowsItem in dtItemTerminal.Rows)
            {
                string sTag = rowsItem["Tag"].ToString();
                DataRow[] rowsTagProfile = dtProfileTerminal.Select(string.Format("Tag='{0}'", sTag));
                if (rowsTagProfile == null || rowsTagProfile.Length == 0)
                {
                    string sDefaultValue = rowsItem["DefaultValue"].ToString();
                    string sQuery = string.Format("INSERT INTO tbProfileTerminal(TerminalID, TerminalTag, TerminalLengthOfTagLength, TerminalTagLength, TerminalTagValue) VALUES ('{0}','{1}',3,'{2}','{3}')", sTerminalID, sTag, sDefaultValue.Length, sDefaultValue);
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlCommand sqlCmd = new SqlCommand(sQuery, oSqlConn))
                        sqlCmd.ExecuteNonQuery();
                }
            }
        }

        private void RemoveUnusedTag(DataTable dtProfileTerminal, DataTable dtItemList, string sTerminalID)
        {
            DataRow[] arrRowProfile = dtProfileTerminal.DefaultView.ToTable(true, "Tag").Select("Tag LIKE 'DE%' OR Tag LIKE 'DC%' OR Tag LIKE 'AA%' OR Tag LIKE 'AE%'");
            DataRow[] arrRowItem = dtItemList.Select("FormID IN (1,2,3)");
            if (arrRowProfile != null && arrRowProfile.Length > 0 && arrRowItem != null && arrRowItem.Length > 0)
            {
                foreach (DataRow row in arrRowProfile)
                {
                    DataTable dtItemFiltered = arrRowItem.CopyToDataTable();
                    string sTag = row["Tag"].ToString();
                    DataRow[] rowsSearchTag = dtItemFiltered.Select(string.Format("Tag = '{0}'", sTag));
                    if (rowsSearchTag == null || rowsSearchTag.Length == 0)
                        RemoveTagFromTable(sTerminalID, sTag);
                }
            }
        }

        private void RemoveTagFromTable(string sTerminalID, string sTag)
        {
            string sQuery = null;
            switch(sTag.Substring(0,2).ToLower())
            {
                case "de":
                case "dc":
                    sQuery = string.Format("DELETE FROM tbProfileTerminal WHERE TerminalID='{0}' AND TerminalTag='{1}'", sTerminalID, sTag);
                    break;
                case "aa":
                    sQuery = string.Format("DELETE FROM tbProfileAcquirer WHERE TerminalID='{0}' AND AcquirerTag='{1}'", sTerminalID, sTag);
                    break;
                case "ae":
                    sQuery = string.Format("DELETE FROM tbProfileIssuer WHERE TerminalID='{0}' AND IssuerTag='{1}'", sTerminalID, sTag);
                    break;
            }
            if(!string.IsNullOrEmpty(sQuery))
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlCommand sqlCmd = new SqlCommand(sQuery, oSqlConn))
                    sqlCmd.ExecuteNonQuery();
            }
        }

        private DataTable dtGetProfileFullTable(string sTerminalID)
        {
            DataTable dtTemp = new DataTable();
            using (SqlCommand sqlCmd = new SqlCommand(CommonSP.sSPProfileTextFullTable, oSqlConn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlDataReader reader = sqlCmd.ExecuteReader())
                    if (reader.HasRows) dtTemp.Load(reader);
            }
            return dtTemp;
        }

        private DataTable dtGetItemList(int iDbId)
        {
            DataTable dtItemList = new DataTable();
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

            using (SqlCommand sqlCmd = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = string.Format("WHERE DatabaseID = {0}", iDbId);

                using (SqlDataReader reader = sqlCmd.ExecuteReader())
                    if (reader.HasRows) dtItemList.Load(reader);
            }
            return dtItemList;
        }

        private bool IsValidTerminalSource(string sTerminalID)
        {
            int iDbTerminal;
            using (SqlCommand cmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE TerminalID='{0}'", sTerminalID);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        iDbTerminal = int.Parse(reader["DatabaseID"].ToString());
                    }
                    else
                        return false;
                }
            }
            if (iDbTerminal == iDatabaseIdSource) return true;
            else return false;
        }

        private void bwMoveData_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 50)
                rtbProgress.Text += string.Format("[{0:dd MMM yyyy, hh:mm:ss.fff tt}] {1}\n", DateTime.Now, e.UserState.ToString());
            rtbProgress.SelectionStart = rtbProgress.Text.Length;
            rtbProgress.ScrollToCaret();
        }

        private void bwMoveData_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay(true);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfileText = new OpenFileDialog();
            openfileText.Filter = "Text File(*.txt)|*.txt";
            openfileText.InitialDirectory = Environment.CurrentDirectory;
            openfileText.ShowDialog();
            if (!string.IsNullOrEmpty(openfileText.FileName))
            {
                txtFilename.Text = openfileText.FileName;
                sFilename = openfileText.FileName;
            }
        }

        #region "Function"
        /// <summary>
        /// Initialize Data Form Move Data
        /// </summary>
        private void InitComboBoxDatabase()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbDestination);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
        }

        /// <summary>
        /// Get Mapping Column
        /// </summary>
        private void InitTerminalID()
        {
            using (StreamReader sr = new StreamReader(sFilename))
            {
                string sTerminalID;
                while ((sTerminalID = sr.ReadLine()) != null)
                    ltTerminalId.Add(sTerminalID);
            }
        }

        /// <summary>
        /// Set Display
        /// </summary>
        /// <param name="isEnable">Bool: if enable True</param>
        private void SetDisplay(bool isEnable)
        {
            gbButton.Enabled = isEnable;
            gbDestination.Enabled = isEnable;
            gbSource.Enabled = isEnable;
            pbMoveData.Style = isEnable ? ProgressBarStyle.Blocks : ProgressBarStyle.Marquee;
        }

        /// <summary>
        /// Get list TerminalID in string
        /// </summary>
        /// <returns>string : list Terminal ID</returns>
        protected string sTerminalIdList()
        {
            string sValue = null;
            foreach (string sTemp in ltTerminalId)
                sValue = string.IsNullOrEmpty(sValue) ? string.Format("'{0}'", sTemp) : string.Format("{0},'{1}'", sValue, sTemp);
            return sValue;
        }
        #endregion

        private void btnClose_Click(object sender, EventArgs e)
        {
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Logoff AppUpdateMoveProfile ", "Logoff AppUpdateMoveProfile");
            this.Dispose();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            cmbDbDestination.SelectedIndex = -1;
            cmbDbSource.SelectedIndex = -1;
            txtFilename.Clear();
            rtbProgress.Clear();
        }
    }
}
