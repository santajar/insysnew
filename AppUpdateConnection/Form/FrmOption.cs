﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AppUpdateConnection
{
    public partial class FrmOption : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        int iDatabaseIdSource;
        string sDatabaseIDSource;
        int iDatabaseIdDest;
        string sDatabaseIDDest;

        List<string> ltTerminalId = new List<string>();
        DataSet dsTables;
        string sFilename;
        static string sUserID;

        public FrmOption()
        {
            InitializeComponent();
        }

        private void FrmOption_Load(object sender, EventArgs e)
        {
            if (oSqlConn == null)
            {
                oSqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString);
                oSqlConnAuditTrail = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString);
            }

            if (string.IsNullOrEmpty(sUserID))
            {
                FrmLogin login = new FrmLogin();
                login.ShowDialog();
                if (login.bLogin)
                {
                    sUserID = login.sUserName;
                    UserData.sUserID = sUserID;
                }                    
                else
                    this.Close();
                login.Dispose();
            }
        }

        private void btnMoveData_Click(object sender, EventArgs e)
        {
            FrmMoveData fMoveData = new FrmMoveData(oSqlConn, sUserID);
            fMoveData.ShowDialog();
        }

        private void btnUpdateConn_Click(object sender, EventArgs e)
        {
            FrmUpdateConnection fUpdateConn = new FrmUpdateConnection(oSqlConn, sUserID);
            fUpdateConn.ShowDialog();
        }

        private void btnUpdateParam_Click(object sender, EventArgs e)
        {
            FrmUpdateParam fUpdateParam = new FrmUpdateParam(oSqlConn, sUserID);
            fUpdateParam.ShowDialog();
        }

        private void FrmOption_FormClosed(object sender, FormClosedEventArgs e)
        {
            CommonClass.UserAccessDelete(oSqlConn, null);
        }
    }
}
