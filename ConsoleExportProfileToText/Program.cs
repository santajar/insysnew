﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Ini;

namespace ConsoleExportProfileToText
{
    class Program
    {
        /* 
         * The Application are used to export the Profile conten from dbtms database to text file
         * based on NewInSys database
         */
        static SqlConnection oConn;

        static void Main(string[] args)
        {
            try
            {
                InitConnection(ref oConn);
                List<string> ltTerminalId = new List<string>();
                ltTerminalId = GetListTerminalId();
                foreach (string sTerminalId in ltTerminalId)
                {
                    string sContent = null;
                    string sDatabaseId = null;
                    ReturnContentDbId(oConn, sTerminalId, ref sContent, ref sDatabaseId);
                    SaveContent(sTerminalId, sContent, sDatabaseId);
                    Console.WriteLine("[{0:MMM dd, yyyy hh:mm:ss.fff tt}] Writing {1} success.", DateTime.Now, sTerminalId);
                }

            }
            catch (Exception ex)
            {
                Console.Write("Main Error : {0} on {1}", ex.Message, ex.InnerException);
            }
            Console.Read();
        }

        static void InitConnection(ref SqlConnection _oConn)
        {
            string sFileConfig = Environment.CurrentDirectory + @"\app.config";
            IniFile oIni = new IniFile(sFileConfig);
            string sServer = oIni.IniReadValue("Database", "Server");
            string sUser = oIni.IniReadValue("Database", "User");
            string sPwd = oIni.IniReadValue("Database", "Pwd");
            _oConn = new SqlConnection(string.Format("Server={0};Database=dbtms;UID={1};Pwd={2};", sServer, sUser, sPwd));
            _oConn.Open();
        }

        static List<string> GetListTerminalId()
        {
            List<string> _arrsTemp = new List<string>();
            string sFilename = Environment.CurrentDirectory + @"\Terminal.config";
            StreamReader sr = new StreamReader(sFilename);
            while (!sr.EndOfStream)
                _arrsTemp.Add(sr.ReadLine());
            sr.Close();
            return _arrsTemp;
        }

        static void ReturnContentDbId(SqlConnection _oConn, string sTerminalId, 
            ref string _sContent, ref string _sDatabaseId)
        {
            SqlCommand oCmd = new SqlCommand(
                string.Format("SELECT DatabaseId, Content FROM TerminalList WHERE TerminalName='{0}'", sTerminalId)
                , _oConn);
            if (_oConn.State != ConnectionState.Open)
                _oConn.Open();
            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.HasRows)
                {
                    if (oRead.Read())
                    {
                        _sDatabaseId = oRead["DatabaseId"].ToString();
                        _sContent = oRead["Content"].ToString();
                    }
                }
            }
            oCmd.Dispose();
        }

        static void SaveContent(string sTerminalId, string sContent, string sDatabaseId)
        {
            string sPath = string.Format(@"{0}\{1}", Environment.CurrentDirectory, sDatabaseId);
            string sFilename = string.Format(@"{0}\{1}\{2}.txt", Environment.CurrentDirectory, sDatabaseId, sTerminalId);

            if (!Directory.Exists(sPath))
                Directory.CreateDirectory(sPath);
            StreamWriter sw = new StreamWriter(sFilename, false);
            StringBuilder sb = new StringBuilder(sContent);
            sw.Write(sb.Replace("\"", "").ToString());
            sw.Close();
        }
    }
}
