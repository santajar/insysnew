﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq;
using InSysClass;
using System.Configuration;
using System.IO;

namespace ipXML
{
    public class EMS_Process
    {
        protected EMS_XML oEms = new EMS_XML();
        protected SqlConnection oSqlConn;
        
        public string Error = null;

        protected string sSenderId = null;
        //protected string sPath3 = Path.GetE
        protected string sPath = ConfigurationManager.AppSettings["Path"].ToString();
        //protected string sPath = @"D:\OFFICE\NewInsysMasSupri\NewInSys\WcfServiceInSys\bin\LOG\";

        string sDatabaseName = "";//add tatang
        public EMS_Process(EMS_XML _oEms, string sConnString) : this(_oEms, (new SqlConnection(sConnString))) { }

        public EMS_Process(EMS_XML _oEms, string sConnString, string sUserID) : this(_oEms, (new SqlConnection(sConnString)), sUserID) { }

        public EMS_Process(EMS_XML _oEms, SqlConnection _oSqlConn) : this(_oEms, _oSqlConn, "EMS") { }

        public EMS_Process(EMS_XML _oEms, SqlConnection _oSqlConn, string sUserID)
        {
            EMSCommonClass.sUserID = sUserID;
            oEms = _oEms;
            sSenderId = oEms.Attribute.sSenderID;
            oSqlConn = _oSqlConn;
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            else
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }
        }

        public EMS_XML emsAddUpdate()
        {
            string sVersion = oEms.Attribute.sVersions;
            string sTableName = oEms.Attribute.sSenderID;

            DataTable dtEmsXmlMapColumn = EMSCommonClass.dtGetEmsXmlMap(oSqlConn, sVersion);
            return emsAddUpdate(dtEmsXmlMapColumn, true);
        }

        public EMS_XML emsAddUpdate(DataTable _dtEmsXmlMapColumn, bool isRemoveUnusedAcquirer)
        {
            EMS_XML oResult = new EMS_XML();
            oResult.Attribute = oEms.Attribute;
            oResult.Attribute.sTypes = EMSCommonClass.XMLResponsType.RESULT.ToString();

            string sTerminalID = "";
            string sResultCode = "000";
            string sResultDesc = "SUCCESS";
            string sVersion = oEms.Attribute.sVersions;
            string sTableName = oEms.Attribute.sSenderID;
            string sMaster = "";
            bool bTerminalMIDMatch = true;

            string sFileTrace = null;

            DataTable dtEmsXmlMapColumn = _dtEmsXmlMapColumn;

            DataTable dtEmsIgnoreTag = new DataTable();
            DataTable dtProfileOldValue = new DataTable();

            foreach (EMS_XML.DataClass.TerminalClass terminalTemp in oEms.Data.ltTerminal)
            {
                DataTable dtTerminalProfile = new DataTable();

                EMSCommonClass.XMLProcessType oType = EMSCommonClass.XMLProcessType.ADD;
                sTerminalID = terminalTemp.sGetColumnValue("Terminal_INIT");

                sFileTrace = sTerminalID + ".log";

                Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START", Trace.sFullTime(), sTerminalID));

                if (EMSCommonClass.IsAllowUserAccess(oSqlConn, sTerminalID))
                {
                    EMSCommonClass.UserAccessInsert(oSqlConn, sTerminalID);
                    sMaster = terminalTemp.sGetColumnValue("Master");

                    if (IsMasterEmsValid(ref sResultDesc, sMaster))
                    {
                        try
                        {
                            string sSoftwareVersion = terminalTemp.sGetColumnValue("Software_Version");
                            int iDatabaseId = iGetDatabaseIdBySoftName(sSoftwareVersion);
                            DataSet dsTables = dsInitTable(iDatabaseId);
                            if (iDatabaseId != 0)
                            {
                                string sMasterProfile = sGetMasterProfile(sVersion, iDatabaseId, sMaster);

                                if (IsTerminalExist(sMasterProfile))
                                {
                                    sTerminalID = sTerminalID.ToUpper();
                                    if (IsTerminalExist(sTerminalID))
                                    {
                                        if (IsMIDMatch(sTerminalID, terminalTemp.sGetColumnValue("Merchant_ID"), ref sResultDesc))
                                        {
                                            oType = EMSCommonClass.XMLProcessType.UPDATE;

                                            // get Ignore Tag list
                                            dtEmsIgnoreTag = dtGetEmsIgnoreTag(sVersion);

                                            // get values of the ignored tag list
                                            if (dtEmsIgnoreTag != null && dtEmsIgnoreTag.Rows.Count > 0)
                                                dtProfileOldValue = dtGetProfileIgnoreTagValue(sTerminalID, dtEmsIgnoreTag);

                                            Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of DeleteTerminal", Trace.sFullTime(), sTerminalID));
                                            DeleteTerminal(sTerminalID);
                                            Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of DeleteTerminal", Trace.sFullTime(), sTerminalID));
                                        }
                                        else bTerminalMIDMatch = false;
                                    }
                                    else oType = EMSCommonClass.XMLProcessType.ADD;

                                    Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} sMasterProfile : {2}", Trace.sFullTime(), sTerminalID, sMasterProfile));
                                    if (bTerminalMIDMatch)
                                    {
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of dtGetProfileFullTable", Trace.sFullTime(), sTerminalID));
                                        dtTerminalProfile = dtGetProfileFullTable(sMasterProfile);
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of dtGetProfileFullTable", Trace.sFullTime(), sTerminalID));

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} 1_RELATION : {2}", Trace.sFullTime(), sTerminalID, dtTerminalProfile.Select("Tag LIKE 'AD%'").Count()));

                                        DataColumn colTerminalId = new DataColumn("TerminalID");
                                        colTerminalId.DefaultValue = sTerminalID;
                                        dtTerminalProfile.Columns.Remove("TerminalID");
                                        dtTerminalProfile.Columns.Add(colTerminalId);
                                        //dtTerminalProfile.Select("Tag='DE01'")[0]["TagValue"] = sTerminalID;
                                        dtTerminalProfile.Select("Tag IN ('DE01','DE001')")[0]["TagValue"] = sTerminalID;
                                        dtTerminalProfile.AcceptChanges();

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} 2_RELATION : {2}", Trace.sFullTime(), sTerminalID, dtTerminalProfile.Select("Tag LIKE 'AD%'").Count()));
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of isRemoveUnusedAcquirer", Trace.sFullTime(), sTerminalID));
                                        if (isRemoveUnusedAcquirer)
                                            DeleteAcquirerList(terminalTemp, dtEmsXmlMapColumn, ref dtTerminalProfile, sVersion);

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} 3_RELATION : {2}", Trace.sFullTime(), sTerminalID, dtTerminalProfile.Select("Tag LIKE 'AD%'").Count()));

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of isRemoveUnusedAcquirer", Trace.sFullTime(), sTerminalID));

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of DeleteAcquirerPromo", Trace.sFullTime(), sTerminalID));
                                        DeleteAcquirerPromo(terminalTemp, ref dtTerminalProfile);
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of DeleteAcquirerPromo", Trace.sFullTime(), sTerminalID));

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of DeleteAcquirerPromo", Trace.sFullTime(), sTerminalID));
                                        UpdateHostIdPromo(terminalTemp, ref dtTerminalProfile);
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of DeleteAcquirerPromo", Trace.sFullTime(), sTerminalID));

                                        //Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of UpdateDisableReward & Poin Bal. Inqr", Trace.sFullTime(), sTerminalID));
                                        //UpdateDisableReward(terminalTemp, ref dtTerminalProfile, sVersion);
                                        //Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of UpdateDisableReward & Poin Bal. Inqr", Trace.sFullTime(), sTerminalID));

                                        //Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of UpdateDisableLoyaltyReward", Trace.sFullTime(), sTerminalID));
                                        //UpdateDisableLoyaltyReward(terminalTemp, ref dtTerminalProfile, sVersion);
                                        //Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of UpdateDisableLoyaltyReward", Trace.sFullTime(), sTerminalID)); 


                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of SortingAcqIssList", Trace.sFullTime(), sTerminalID));
                                        SortingAcqIssList(terminalTemp, ref dtTerminalProfile, sVersion);
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of SortingAcqIssList", Trace.sFullTime(), sTerminalID));

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of UpdateProfileTag", Trace.sFullTime(), sTerminalID));
                                        UpdateProfileTag(terminalTemp, dtEmsXmlMapColumn, ref dtTerminalProfile, sVersion, sFileTrace);
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of UpdateProfileTag", Trace.sFullTime(), sTerminalID));

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of UpdateDisableInstallment", Trace.sFullTime(), sTerminalID));
                                        UpdateDisableInstallment(terminalTemp, ref dtTerminalProfile, sVersion);
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of UpdateDisableInstallment", Trace.sFullTime(), sTerminalID));

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of UpdateMasterDial", Trace.sFullTime(), sTerminalID));
                                        UpdateMasterDial(ref dtTerminalProfile, sVersion);
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of UpdateMasterDial", Trace.sFullTime(), sTerminalID));

                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of UpdateIgnoredTag", Trace.sFullTime(), sTerminalID));
                                        if (dtProfileOldValue != null && dtProfileOldValue.Rows.Count > 0)
                                            UpdateIgnoredTag(ref dtTerminalProfile, dtProfileOldValue);
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of UpdateIgnoredTag", Trace.sFullTime(), sTerminalID));
                                        
                                        Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of IsUploadSuccess", Trace.sFullTime(), sTerminalID));
                                        if (IsUploadSuccess(sTerminalID, iDatabaseId, dsTables, dtTerminalProfile, ref sResultDesc, sMaster))
                                        {
                                            Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of IsUploadSuccess", Trace.sFullTime(), sTerminalID));
                                            Error = null;
                                            sResultCode = "000";
                                            sResultDesc = string.Format("SUCCESS. {0} Terminal {1} From EMS XML", oType.ToString(), sTerminalID);
                                            sDatabaseName = sGetDatabaseNameByTerminalID(sTerminalID);

                                            Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of spProfileTerminalUpdateLastUpdate", Trace.sFullTime(), sTerminalID));

                                            TerminalRunProcedure(sTerminalID, "spProfileTerminalUpdateLastUpdate");

                                            Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} END of spProfileTerminalUpdateLastUpdate", Trace.sFullTime(), sTerminalID));
                                        }
                                        else
                                        {
                                            sResultCode = "001";
                                            Error = sResultDesc = string.Format("TIDAK BERHASIL IsUploadSuccess. {0} Terminal {1} From EMS XML. Upload TIDAK BERHASIL. {2}",
                                                oType.ToString(), sTerminalID, sResultDesc);
                                        }
                                    }
                                    else
                                    {
                                        sResultCode = "099";
                                        sResultDesc = EMSCommon.XML.sErrorTerminalMID + sResultDesc;
                                        Error = string.Format("TIDAK BERHASIL, {0} : {1}. Master='{2}'", sTableName, sResultDesc, sMasterProfile);
                                    }
                                }
                                else
                                {
                                    sResultCode = "001";
                                    sResultDesc = EMSCommon.XML.sErrorMaster2;
                                    Error = string.Format("TIDAK BERHASIL, {0} : {1}. Master='{2}'", sTableName, sResultDesc, sMasterProfile);
                                }
                            }
                            else
                            {
                                sResultCode = "001";
                                sResultDesc = EMSCommon.XML.sErrorSoftwareVersion + sSoftwareVersion;
                                Error = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                            }
                        }
                        catch (Exception ex)
                        {
                            EMSCommonClass.UserAccessDelete(oSqlConn, sTerminalID);
                            sResultCode = "001";
                            sResultDesc = string.Format("{3}.{0} ADD Terminal {1} From EMS XML. ErrorMsg : {2}", EMSCommon.XML.sErrorCommon
                                , sTerminalID, ex.Message, ex.StackTrace);
                            //Error = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                            Error = sResultDesc;
                        }
                    }
                    else
                    {
                        sResultCode = "001";
                        sResultDesc = EMSCommon.XML.sErrorMaster;
                        Error = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                    }
                    InsertLog(sTerminalID, sResultDesc, sResultCode, sDatabaseName);
                }
                else
                {
                    sResultCode = "001";
                    sResultDesc = EMSCommon.XML.sErrorInvalidAccess;
                    Error = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                }
                oResult.Data.doAddTerminal(tcGenResult(sTerminalID, sResultDesc, sResultCode, oType));
                EMSCommonClass.UserAccessDelete(oSqlConn, sTerminalID);

                Trace.Write(sPath, sTerminalID, string.Format("[{0}], {1} FINISH, ResultCode : {2}", Trace.sFullTime(), sTerminalID, sResultCode));
            }
            return oResult;
        }


        protected void InsertToVipotProfileTable(string _sTerminalID, string _sMaster)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
   
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPVipotProfile, oSqlConn))
            {
                oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sMaster", SqlDbType.VarChar).Value = _sMaster;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                oCmd.ExecuteNonQuery();
            }
         
        }

        //protected void InsertToVipotProfileTable()
        //{
        //    using (SqlCommand oSqlCmd = new SqlCommand(EMSCommon.XML.sSPXMLGetMapColumn, oSqlConn))
        //    {
        //        oSqlCmd.CommandType = CommandType.StoredProcedure;
        //        oSqlCmd.Parameters.Add("@sMaster", SqlDbType.VarChar).Value = sGetMasterProfile;
        //        if (_oSqlConn.State != ConnectionState.Open) oSqlConn.Open();

        //        using (SqlDataReader oReader = oSqlCmd.ExecuteReader())
        //        {
        //            if (oReader.HasRows)
        //            {
        //                dtTemp = new DataTable();
        //                dtTemp.Load(oReader);
        //            }
        //            oReader.Close();
        //        }
        //    }
        //}
        protected void UpdateIgnoredTag(ref DataTable dtTerminalProfile, DataTable dtProfileOldValue)
        {
            foreach (DataRow row in dtProfileOldValue.Rows)
            {
                string sName = row["Name"].ToString();
                string sTag = row["Tag"].ToString();
                string sTagValue = row["TagValue"].ToString();
                if (sTag.Substring(0, 2).ToLower() == "aa" || sTag.Substring(0, 2).ToLower() == "ae")
                {
                    //add untuk cek
                    //if (sTag.Substring(0, 2).ToLower() == "ae")
                    //{    if (sTag == "AE014" || sTag == "AE078" || sTag == "AE079")
                    //    Trace.Write(sPath, "", string.Format("[{0}], {1} Cek nilai old: {2} :{3}", Trace.sFullTime(), sName, sTag, sTagValue));
                    //}    //
                        UpdateAcquirerIssuerTag(sName, sTag, sTagValue, ref dtTerminalProfile);
                    
                }
                else
                    UpdateTerminalTag(sTag, sTagValue, ref dtTerminalProfile);
            }

            // update new acquirer or issuer            
            DataRow[] rowsAcquirerOld = dtProfileOldValue.Select("Tag LIKE 'AA%'");
            DataTable dtAcquirerNameOld = rowsAcquirerOld.CopyToDataTable().DefaultView.ToTable(true, "Name");

            if (dtAcquirerNameOld.Rows.Count > 0)
            {
                // get default value, acquirer sort : loyalty, bca, bca2, debit, flazzbca
                DataTable dtDefaultOldValue = new DataTable();
                //DataRow[] rowsDefaultOldValue = dtProfileOldValue.Select("Name = 'LOYALTY'");
                DataRow[] rowsDefaultOldValue = dtProfileOldValue.Select("Name = 'CRONUS'");
                if (rowsDefaultOldValue == null || rowsDefaultOldValue.Length == 0)
                {
                    //rowsDefaultOldValue = dtProfileOldValue.Select("Name = 'BCA'");
                    rowsDefaultOldValue = dtProfileOldValue.Select("Name = 'CRONUS'");
                    if (rowsDefaultOldValue == null || rowsDefaultOldValue.Length == 0)
                    {
                        //rowsDefaultOldValue = dtProfileOldValue.Select("Name = 'BCA2'");
                        rowsDefaultOldValue = dtProfileOldValue.Select("Name = 'CROFFUS'");
                        if (rowsDefaultOldValue == null || rowsDefaultOldValue.Length == 0)
                        {
                            //rowsDefaultOldValue = dtProfileOldValue.Select("Name = 'DEBIT'");
                            rowsDefaultOldValue = dtProfileOldValue.Select("Name = 'DBONUS'");
                            if (rowsDefaultOldValue == null || rowsDefaultOldValue.Length == 0)
                            {
                                rowsDefaultOldValue = dtProfileOldValue.Select("Name = 'FLAZZBCA'");
                                if (rowsDefaultOldValue != null || rowsDefaultOldValue.Length > 0)
                                    dtDefaultOldValue = rowsDefaultOldValue.CopyToDataTable();
                            }
                            else
                                dtDefaultOldValue = rowsDefaultOldValue.CopyToDataTable();
                        }
                        else
                            dtDefaultOldValue = rowsDefaultOldValue.CopyToDataTable();
                    }
                    else
                        dtDefaultOldValue = rowsDefaultOldValue.CopyToDataTable();
                }
                else
                    dtDefaultOldValue = rowsDefaultOldValue.CopyToDataTable();

                if (dtDefaultOldValue != null || dtDefaultOldValue.Rows.Count > 0)
                {
                    string sAcquirerList = null;
                    foreach (DataRow row in dtAcquirerNameOld.Rows)
                        sAcquirerList = string.Format("{0}'{1}',", sAcquirerList, row["Name"].ToString());
                    sAcquirerList = sAcquirerList.Substring(0, sAcquirerList.Length - 1);

                    DataRow[] rowsAcquirer = dtTerminalProfile.Select("Tag LIKE 'AA001' AND Name NOT IN (" + sAcquirerList + ")");
                    if (rowsAcquirer != null && rowsAcquirer.Length > 0)
                        foreach (DataRow rowAcq in rowsAcquirer)
                        {
                            string sName = rowAcq["Name"].ToString();
                            foreach (DataRow row in dtDefaultOldValue.Rows)
                            {
                                string sTag = row["Tag"].ToString();
                                string sTagValue = row["TagValue"].ToString();
                                if (sTag.Substring(0, 2).ToLower() == "aa" || sTag.Substring(0, 2).ToLower() == "ae")
                                {   
                                    //add untuk cek
                                    //if (sTag.Substring(0, 2).ToLower() == "ae")
                                    //{
                                    //    if (sTag == "AE014" || sTag == "AE078" || sTag == "AE079")
                                    //        Trace.Write(sPath, "", string.Format("[{0}], {1} Cek nilai rowsDefaultOldValue: {2} :{3}", Trace.sFullTime(), sName, sTag, sTagValue));
                                    //}    //
                                    UpdateAcquirerIssuerTag(sName, sTag, sTagValue, ref dtTerminalProfile);
                                }
                                else
                                    UpdateTerminalTag(sTag, sTagValue, ref dtTerminalProfile);
                            }
                        }
                }
            }
        }

        protected DataTable dtGetEmsIgnoreTag(string sVersion)
        {
            DataTable dtTemp = new DataTable();
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand sqlcommand = new SqlCommand("spEmsMsGetIgnoreTag", oSqlConn))
            {
                sqlcommand.CommandType = CommandType.StoredProcedure;
                sqlcommand.Parameters.Add("@sVersion", SqlDbType.VarChar).Value = sVersion;
                using (SqlDataReader reader = sqlcommand.ExecuteReader())
                {
                    if (reader.HasRows) dtTemp.Load(reader);
                }
            }
            return dtTemp;
        }

        protected DataTable dtGetProfileIgnoreTagValue(string sTerminalID, DataTable dtEmsIgnoreTag)
        {
            DataTable dtTemp = new DataTable();
            DataTable dtProfile = dtGetProfileFullTable(sTerminalID);

            string sCondition = "Tag IN (";
            foreach (DataRow rowTag in dtEmsIgnoreTag.Rows)
                sCondition = string.Format("{0}'{1}',", sCondition, rowTag["Tag"].ToString());

            sCondition = string.Format("{0})", sCondition.Substring(0, sCondition.Length - 1));

            DataRow[] rowProfileIgnoredTagValue = dtProfile.Select(sCondition);
            if (rowProfileIgnoredTagValue.Length > 0)
                dtTemp = rowProfileIgnoredTagValue.CopyToDataTable();

            return dtTemp;
        }

        protected bool IsMIDMatch(string sTerminalID, string sMerchantNumber, ref string sErrorMessage)
        {
            DataTable dtTerminal = dtGetProfileFullTable(sTerminalID);
            //DataRow[] rowResult = dtTerminal.Select("Tag='AA004' AND Name IN ('BCA','DEBIT','LOYALTY','FLAZZBCA', 'BCA1')");
            DataRow[] rowResult = dtTerminal.Select("Tag='AA004' AND Name IN ('CROFFUS','DBOFFUS','CRONUS','FLAZZBCA', 'DBONUS')");
            if (rowResult != null)
                if (rowResult.Count() > 0)
                {
                    bool bMatch = true;
                    foreach (DataRow row in rowResult)
                    {
                        if (bMatch)
                        {
                            string sMID = row["TagValue"].ToString();
                            if (sMID.Length>=9)
                                sMID = sMID.Substring(sMID.Length - 9);
                            if (sMID != sMerchantNumber)
                            {
                                int iMid;
                                bMatch = !int.TryParse(sMID, out iMid);
                                sErrorMessage = sMID;
                            }
                        }
                        else
                            break;
                    }
                    return bMatch;
                }
            return false;
        }

        protected void SortingAcqIssList(EMS_XML.DataClass.TerminalClass terminalTemp, ref DataTable _dtTerminalProfile, string _sVersion)
        {
            //string[] arrAcquirer = new string[] { "BCA", "BCA1", "BCA2", "AMEX", "DINERS", "DEBIT", "LOYALTY", "FLAZZBCA", "PROMO A", "PROMO B", "PROMO C", "PROMO D", "PROMO E", "PROMO F", "PROMO G", "PROMO H", "PROMO I", "PROMO J", "PROMO SQ A", "PROMO SQ B", "PROMO SQ C", "PROMO SQ D", "PROMO SQ E" };
            string[] arrAcquirer = new string[] { "CROFFUS",  "AMEX", "DINERS", "DBONUS", "CRONUS",  "FLAZZBCA", "DBOFFUS", "SAKUKU", "PROMO A", "PROMO B", "PROMO C", "PROMO D", "PROMO E", "PROMO F", "PROMO G", "PROMO H", "PROMO I", "PROMO J", "PROMO SQ A", "PROMO SQ B", "PROMO SQ C", "PROMO SQ D", "PROMO SQ E" };
            string[] arrsIssuerMode1 = new string[] { "BCA", "VISA BCA", "MASTER BCA", "JCB BCA", "SMARTCASH", "AMEX BCA", "VISA", "MASTERCARD", "JCB", "UNIONPAY CREDIT", "UNIONPAY DEBIT", "AMEX", "DINERS", "DEBIT BCA", "SWITCHING", "MAESTRO", "CASH BCA", "DEBIT MC BCA", "DEBITMASTERCARD", "BCA SYARIAH", "FLAZZ BCA", "FLAZZ DBT", "FLAZZ CRD" };
            string[] arrsIssuerMode2 = new string[] { "VISA SQ", "VISA", "MASTERCARD", "JCB", "UNIONPAY CREDIT", "UNIONPAY DEBIT", "AMEX", "DINERS", "DEBIT BCA", "SWITCHING", "MAESTRO", "CASH BCA", "DEBIT MC BCA", "DEBITMASTERCARD", "BCA SYARIAH", "BCA", "VISA BCA", "MASTER BCA", "JCB BCA", "SMARTCASH", "AMEX BCA", "FLAZZ BCA", "FLAZZ DBT", "FLAZZ CRD" };

            DataRow[] rowsRelation = _dtTerminalProfile.Select("Tag LIKE 'AD%'", "Id");
            if (rowsRelation.Count() > 0)
            {
                DataTable dtAcqIssCard = dtGenerateAcqIssCard(rowsRelation);
                DataTable dtOrderedAcqIssCard = dtSortingAcqIssCard(dtAcqIssCard, "Acquirer", arrAcquirer);
                //if (terminalTemp.sGetColumnValue("LOYALTY") == "1")
                if (terminalTemp.sGetColumnValue("credit") == "1")
                    dtOrderedAcqIssCard = dtSortingAcqIssCard(dtAcqIssCard, "Issuer", arrsIssuerMode2);
                else
                    dtOrderedAcqIssCard = dtSortingAcqIssCard(dtAcqIssCard, "Issuer", arrsIssuerMode1);
                UpdateRelation(terminalTemp, _dtTerminalProfile, dtOrderedAcqIssCard, _sVersion);
                _dtTerminalProfile.AcceptChanges();
            }
        }

        protected void UpdateRelation(EMS_XML.DataClass.TerminalClass terminalTemp, DataTable _dtTerminalProfile
            , DataTable dtOrderedAcqIssCard, string _sVersion)
        {
            string sTerminalID = terminalTemp.sGetColumnValue("Terminal_INIT");
            DataRow[] rowsRelation = _dtTerminalProfile.Select("Tag LIKE 'AD%'");
            int iRowId = int.Parse(rowsRelation[0]["Id"].ToString()) - 1;
            foreach (DataRow rowDelete in rowsRelation)
                rowDelete.Delete();
            _dtTerminalProfile.AcceptChanges();
            foreach (DataRow rowRelation in dtOrderedAcqIssCard.Rows)
            {
                string sTagValue = null;
                int iTagLength;
                DataRow rowRelationNew = _dtTerminalProfile.NewRow();
                rowRelationNew["Id"] = ++iRowId;
                rowRelationNew["TerminalID"] = sTerminalID;
                rowRelationNew["Tag"] = _sVersion == "1.00" ? "AD01" : "AD001";
                sTagValue = rowRelation["Card"].ToString();
                iTagLength = sTagValue.Length;
                rowRelationNew["TagLength"] = iTagLength;
                rowRelationNew["TagValue"] = sTagValue;
                _dtTerminalProfile.Rows.Add(rowRelationNew);

                rowRelationNew = _dtTerminalProfile.NewRow();
                rowRelationNew["Id"] = ++iRowId;
                rowRelationNew["TerminalID"] = sTerminalID;
                rowRelationNew["Tag"] = _sVersion == "1.00" ? "AD02" : "AD002";
                sTagValue = rowRelation["Issuer"].ToString();
                iTagLength = sTagValue.Length;
                rowRelationNew["TagLength"] = iTagLength;
                rowRelationNew["TagValue"] = sTagValue;
                _dtTerminalProfile.Rows.Add(rowRelationNew);

                rowRelationNew = _dtTerminalProfile.NewRow();
                rowRelationNew["Id"] = ++iRowId;
                rowRelationNew["TerminalID"] = sTerminalID;
                rowRelationNew["Tag"] = _sVersion == "1.00" ? "AD03" : "AD003";
                sTagValue = rowRelation["Acquirer"].ToString();
                iTagLength = sTagValue.Length;
                rowRelationNew["TagLength"] = iTagLength;
                rowRelationNew["TagValue"] = sTagValue;
                _dtTerminalProfile.Rows.Add(rowRelationNew);
            }
        }

        protected DataTable dtSortingAcqIssCard(DataTable _dtAcqIssCard, string sCompareKey, string[] _arrsComparison)
        {
            DataTable dtTemp = _dtAcqIssCard.Clone();
            foreach (string sComparison in _arrsComparison)
            {
                DataRow[] rowsResult = _dtAcqIssCard.Select(string.Format("{0}='{1}'", sCompareKey, sComparison));
                if (rowsResult.Count() > 0)
                {
                    foreach (DataRow rowResult in rowsResult)
                        dtTemp.ImportRow(rowResult);
                }
            }

            if (dtTemp.Rows.Count < _dtAcqIssCard.Rows.Count)
            {
                var varExcept = from r in _dtAcqIssCard.AsEnumerable()
                                where !dtTemp.AsEnumerable().Any(r2 => r["Acquirer"].ToString() == r2["Acquirer"].ToString())
                                select r;
                if (varExcept != null && varExcept.Count() > 0)
                {
                    DataTable dtExcept = varExcept.CopyToDataTable();
                    foreach (DataRow row in dtExcept.Rows)
                        dtTemp.ImportRow(row);
                }
            }
            return dtTemp;
        }

        protected DataTable dtGenerateAcqIssCard(DataRow[] _rowsRelation)
        {
            DataTable dtTemp = new DataTable();
            dtTemp.Columns.Add("Acquirer");
            dtTemp.Columns.Add("Issuer");
            dtTemp.Columns.Add("Card");

            string[] arrAcqIssCard = new string[3];
            foreach (DataRow row in _rowsRelation)
            {
                if (row["Tag"].ToString() == "AD01" || row["Tag"].ToString() == "AD001")
                    arrAcqIssCard[0] = row["TagValue"].ToString();
                else if (row["Tag"].ToString() == "AD02" || row["Tag"].ToString() == "AD002")
                    arrAcqIssCard[1] = row["TagValue"].ToString();
                else
                {
                    arrAcqIssCard[2] = row["TagValue"].ToString();
                    DataRow rowAcqIssCard = dtTemp.NewRow();
                    rowAcqIssCard["Card"] = arrAcqIssCard[0];
                    rowAcqIssCard["Issuer"] = arrAcqIssCard[1];
                    rowAcqIssCard["Acquirer"] = arrAcqIssCard[2];
                    dtTemp.Rows.Add(rowAcqIssCard);
                }
            }
            return dtTemp;
        }

        protected void UpdateDisableInstallment(EMS_XML.DataClass.TerminalClass terminalTemp, ref DataTable _dtTerminalProfile
            , string _sVersion)
        {
            // disable installment for all issuer
            //UpdateAcquirerIssuerTag("", _sVersion == "1.00" ? "AE19" : "AE019", "1", ref _dtTerminalProfile);
            //add tatang
            string sValueSoftware = sGetColumnValue(terminalTemp, "Software_Version");
            if (sValueSoftware == "AVOCADO_MOVE")
                UpdateAcquirerIssuerTag("", _sVersion == "1.00" ? "AE19" : "AE019", "0", ref _dtTerminalProfile);
            else UpdateAcquirerIssuerTag("", _sVersion == "1.00" ? "AE19" : "AE019", "1", ref _dtTerminalProfile);
            //

            string sValue = "0";
            //if (terminalTemp.sGetColumnValue("Cicilan_Reguler") == "0" && terminalTemp.sGetColumnValue("Cicilan_Promo") == "0")
            //sValue = "1";

            string sValueReg = terminalTemp.sGetColumnValue("Cicilan_Reguler");
            string sValuePromo = terminalTemp.sGetColumnValue("Cicilan_Promo");
            // add tatang
            if (sValueSoftware == "AVOCADO_MOVE")
            {
                if (sValueReg == "0" && sValuePromo == "0")
                    sValue = "0";
                else sValue = "1";
            }
            else
            if (sValueReg == "0" && sValuePromo == "0")
                sValue = "1";
            //

            // all credit, ON US or OFF US 
            //DataRow[] rowsIssuer = _dtTerminalProfile.Select(string.Format("Tag='{0}' AND Name IN ('BCA','VISA BCA','MASTER BCA','JCB BCA','SMARTCASH','VISA','MASTERCARD','JCB','UNIONPAY CREDIT','UNIONPAY DEBIT','AMEX BCA')", _sVersion == "1.00" ? "AE19" : "AE019"));

            // add tatang
            DataRow[] rowsIssuer = _dtTerminalProfile.Select(string.Format("Tag='{0}' AND Name IN ('BCA','VISA BCA','MASTER BCA','JCB BCA','SMARTCASH','AMEX BCA','VISA SQ')", _sVersion == "1.00" ? "AE19" : "AE019"));
            //
            if (rowsIssuer.Count() > 0)
            {
                foreach (DataRow rowIssuer in rowsIssuer)
                {
                    UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE19" : "AE019", sValue, ref _dtTerminalProfile);
                }
                _dtTerminalProfile.AcceptChanges();
            }
        }

        protected void UpdateDisableReward(EMS_XML.DataClass.TerminalClass terminalTemp, ref DataTable _dtTerminalProfile, string _sVersion)
        {
            string sValuereward = terminalTemp.sGetColumnValue("reward");
            string sValueLoyalty = terminalTemp.sGetColumnValue("Loyalty");
            string sValueLoyaltyDebit = terminalTemp.sGetColumnValue("Debit_Loyalti");
            string sValueSoftwareVers = sGetColumnValue(terminalTemp, "Software_Version");
            string _sTerminalID = sGetColumnValue(terminalTemp, "Terminal_INIT");

            string sFileTrace = _sTerminalID + ".log";
            if (sValuereward == "0") // add tatang
            {
                if (sValueLoyalty == "0")
                {
                    #region
                    // DataRow[] rowsIssuer = _dtTerminalProfile.Select("Tag IN ('AE18','AE018') AND Name IN ('BCA','VISA BCA','MASTER BCA','JCB BCA','SMARTCASH','VISA','MASTERCARD','JCB','UNIONPAY CREDIT','UNIONPAY DEBIT','AMEX BCA')");
                    // add tatang 
                    DataRow[] rowsIssuerLoyaltyr0 = _dtTerminalProfile.Select("Tag IN ('AE18','AE018') AND Name IN ('BCA','VISA BCA','MASTER BCA','JCB BCA','SMARTCASH','AMEX BCA')");
                    //
                    if (rowsIssuerLoyaltyr0.Count() > 0)
                    {
                        foreach (DataRow rowIssuerLoyaltyr0 in rowsIssuerLoyaltyr0)
                        {
                            //add tatang
                            if (sValueSoftwareVers == "AVOCADO_MOVE")
                            {
                                UpdateAcquirerIssuerTag(rowIssuerLoyaltyr0["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE018", "0", ref _dtTerminalProfile);//Enable Reward
                                Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} Reward={2}, Loyalty={3}, {4}, Enable Reward=0 for " + rowIssuerLoyaltyr0["Name"].ToString(), Trace.sFullTime(), _sTerminalID, sValuereward, sValueLoyalty, sValueSoftwareVers));
                                //UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE016", "0", ref _dtTerminalProfile);//Enable Balance Inquiry
                                UpdateAcquirerIssuerTag(rowIssuerLoyaltyr0["Name"].ToString(), _sVersion == "1.00" ? "AE49" : "AE049", "0", ref _dtTerminalProfile);//Enable Point Bal. Inqr
                                //if (terminalTemp.sGetColumnValue("Reward_Debit_Poin") == "0") UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE28" : "AE028", "0", ref _dtTerminalProfile);//Loyalty Reward // add tatang 190117
                            }
                            else
                            //
                            {
                                UpdateAcquirerIssuerTag(rowIssuerLoyaltyr0["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE018", "1", ref _dtTerminalProfile);
                                Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} Reward={2}, Loyalty={3}, NOT {4}, Disable Reward=1 for " + rowIssuerLoyaltyr0["Name"].ToString(), Trace.sFullTime(), _sTerminalID, sValuereward, sValueLoyalty, sValueSoftwareVers));
                                //if (terminalTemp.sGetColumnValue("Reward_Debit_Poin") == "0") UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE28" : "AE028", "0", ref _dtTerminalProfile);//Loyalty Reward // add tatang 190117
                            }
                        }
                        _dtTerminalProfile.AcceptChanges();
                    }
                    #endregion
                }
                // add tatang for Debit_Loyalti disable reward 
                if (sValueLoyaltyDebit == "0")
                {
                    #region
                    DataRow[] rowsIssuerDBLoyaltyr0 = _dtTerminalProfile.Select("Tag IN ('AE18','AE018') AND Name IN ('DEBIT BCA','CASH BCA','DEBITMASTERCARD','DEBIT MC BCA')");
                    //
                    if (rowsIssuerDBLoyaltyr0.Count() > 0)
                    {
                        foreach (DataRow rowIssuerDBLoyaltyr0 in rowsIssuerDBLoyaltyr0)
                        {
                            //add tatang
                            if (sValueSoftwareVers == "AVOCADO_MOVE")
                            {
                                UpdateAcquirerIssuerTag(rowIssuerDBLoyaltyr0["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE018", "0", ref _dtTerminalProfile);//Enable Reward
                                Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} Reward={2}, LoyaltyDebit={3}, {4}, Enable Reward=0 for " + rowIssuerDBLoyaltyr0["Name"].ToString(), Trace.sFullTime(), _sTerminalID, sValuereward, sValueLoyaltyDebit, sValueSoftwareVers));
                                //if (terminalTemp.sGetColumnValue("Reward_Debit_Poin") == "0") UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE28" : "AE028", "0", ref _dtTerminalProfile);//Loyalty Reward // add tatang 190117
                            }
                            else
                            //
                            {
                                UpdateAcquirerIssuerTag(rowIssuerDBLoyaltyr0["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE018", "1", ref _dtTerminalProfile);
                                Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} Reward={2}, LoyaltyDebit={3}, NOT {4}, Disable Reward=1 for " + rowIssuerDBLoyaltyr0["Name"].ToString(), Trace.sFullTime(), _sTerminalID, sValuereward, sValueLoyaltyDebit, sValueSoftwareVers));
                                //if (terminalTemp.sGetColumnValue("Reward_Debit_Poin") == "0") UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE28" : "AE028", "0", ref _dtTerminalProfile);//Loyalty Reward // add tatang 190117
                            }
                        }
                        _dtTerminalProfile.AcceptChanges();
                    }
                    #endregion
                }

            }
            if (sValuereward == "1")
            {
                if (sValueLoyalty == "0")
                {
                    #region
                    // DataRow[] rowsIssuer = _dtTerminalProfile.Select("Tag IN ('AE18','AE018') AND Name IN ('BCA','VISA BCA','MASTER BCA','JCB BCA','SMARTCASH','VISA','MASTERCARD','JCB','UNIONPAY CREDIT','UNIONPAY DEBIT','AMEX BCA')");
                    // add tatang 
                    DataRow[] rowIssuersLoyaltyr1 = _dtTerminalProfile.Select("Tag IN ('AE18','AE018') AND Name IN ('BCA','VISA BCA','MASTER BCA','JCB BCA','SMARTCASH','AMEX BCA')");
                    //
                    if (rowIssuersLoyaltyr1.Count() > 0)
                    {
                        foreach (DataRow rowIssuerLoyaltyr1 in rowIssuersLoyaltyr1)
                        {
                            //add tatang
                            if (sValueSoftwareVers == "AVOCADO_MOVE")
                            {
                                UpdateAcquirerIssuerTag(rowIssuerLoyaltyr1["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE018", "0", ref _dtTerminalProfile);//Enable Reward
                                Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} Reward={2}, Loyalty={3},{4}, Enable Reward=0 for " + rowIssuerLoyaltyr1["Name"].ToString(), Trace.sFullTime(), _sTerminalID, sValuereward, sValueLoyalty, sValueSoftwareVers));
                                //UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE016", "0", ref _dtTerminalProfile);//Enable Balance Inquiry
                                UpdateAcquirerIssuerTag(rowIssuerLoyaltyr1["Name"].ToString(), _sVersion == "1.00" ? "AE49" : "AE049", "0", ref _dtTerminalProfile);//Enable Point Bal. Inqr
                                                                                                                                                                    // if (terminalTemp.sGetColumnValue("Reward_Debit_Poin") == "0") 
                                                                                                                                                                    //UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE28" : "AE028", "0", ref _dtTerminalProfile);//Loyalty Reward  // add tatang 190117
                            }
                            else
                            {
                                UpdateAcquirerIssuerTag(rowIssuerLoyaltyr1["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE018", "1", ref _dtTerminalProfile);
                                Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} Reward={2}, Loyalty={3},{4}, Disable Reward=1 for " + rowIssuerLoyaltyr1["Name"].ToString(), Trace.sFullTime(), _sTerminalID, sValuereward, sValueLoyalty, sValueSoftwareVers));
                                //UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE28" : "AE028", "1", ref _dtTerminalProfile);//Loyalty Reward  // add tatang 190117
                            }
                        }
                        _dtTerminalProfile.AcceptChanges();
                    }
                    #endregion
                }
                // add tatang for Debit_Loyalti disable reward
                if (sValueLoyaltyDebit == "0")
                {
                    #region
                    DataRow[] rowsIssuersLoyaltyDebitr1 = _dtTerminalProfile.Select("Tag IN ('AE18','AE018') AND Name IN ('DEBIT BCA','CASH BCA','DEBITMASTERCARD','DEBIT MC BCA')");
                    //
                    if (rowsIssuersLoyaltyDebitr1.Count() > 0)
                    {
                        foreach (DataRow rowIssuerLoyaltyDebitr1 in rowsIssuersLoyaltyDebitr1)
                        {
                            //add tatang
                            if (sValueSoftwareVers == "AVOCADO_MOVE")
                            {
                                UpdateAcquirerIssuerTag(rowIssuerLoyaltyDebitr1["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE018", "0", ref _dtTerminalProfile);//Enable Reward
                                Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} Reward={2}, LoyaltyDebit={3},{4}, Enable Reward=0 for " + rowIssuerLoyaltyDebitr1["Name"].ToString(), Trace.sFullTime(), _sTerminalID, sValuereward, sValueLoyaltyDebit, sValueSoftwareVers));
                                //if (terminalTemp.sGetColumnValue("Reward_Debit_Poin") == "0") UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE28" : "AE028", "0", ref _dtTerminalProfile);//Loyalty Reward // add tatang 190117
                            }
                            else
                            {
                                UpdateAcquirerIssuerTag(rowIssuerLoyaltyDebitr1["Name"].ToString(), _sVersion == "1.00" ? "AE18" : "AE018", "1", ref _dtTerminalProfile);
                                Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} Reward={2}, LoyaltyDebit={3},{4}, Disable Reward=1 for " + rowIssuerLoyaltyDebitr1["Name"].ToString(), Trace.sFullTime(), _sTerminalID, sValuereward, sValueLoyaltyDebit, sValueSoftwareVers));
                                //if (terminalTemp.sGetColumnValue("Reward_Debit_Poin") == "0") UpdateAcquirerIssuerTag(rowIssuer["Name"].ToString(), _sVersion == "1.00" ? "AE28" : "AE028", "0", ref _dtTerminalProfile);//Loyalty Reward // add tatang 190117
                            }
                        }
                        _dtTerminalProfile.AcceptChanges();
                    }
                    #endregion
                }
            }

        }
        protected void UpdateDisableLoyaltyReward(EMS_XML.DataClass.TerminalClass terminalTemp, ref DataTable _dtTerminalProfile, string _sVersion)
        {
            //if (terminalTemp.sGetColumnValue("reward") == "0") // add tatang for Reward_Debit_Poin 1 diabaikan, kredit on us, sehingga menjadi 0 190118
            string sValueloyaltyreward = terminalTemp.sGetColumnValue("Loyalty");
            if (sValueloyaltyreward == "0")
            {
                string sValueSoftware = sGetColumnValue(terminalTemp, "Software_Version");
                DataRow[] rowsIssuersLoyaltyReward = _dtTerminalProfile.Select("Tag IN ('AE18','AE018') AND Name IN ('BCA','VISA BCA','MASTER BCA','JCB BCA','SMARTCASH','AMEX BCA')");
                //
                if (rowsIssuersLoyaltyReward.Count() > 0)
                {
                    foreach (DataRow rowIssuerLoyaltyReward in rowsIssuersLoyaltyReward)
                    {
                        UpdateAcquirerIssuerTag(rowIssuerLoyaltyReward["Name"].ToString(), _sVersion == "1.00" ? "AE28" : "AE028", "0", ref _dtTerminalProfile);

                    }
                    _dtTerminalProfile.AcceptChanges();
                }
                //  }

            }


        }

        protected void UpdateMasterDial(ref DataTable _dtTerminalProfile, string _sVersion)
        {
            UpdateAcquirerIssuerTag("", _sVersion == "1.00" ? "AA35" : "AA035", "0", ref _dtTerminalProfile);
            //DataRow[] rowsAcquirer = _dtTerminalProfile.Select("Tag IN ('AA01','AA001') AND Name IN ('BCA','BCA1','BCA2','DEBIT','LOYALTY','FLAZZBCA')");
            //DataRow[] rowsAcquirer = _dtTerminalProfile.Select("Tag IN ('AA01','AA001') AND Name IN ('CRONUS','CROFFUS','DBOFFUS','DBONUS','FLAZZBCA')");
            DataRow[] rowsAcquirer = _dtTerminalProfile.Select("Tag IN ('AA01','AA001') AND Name IN ('CROFFUS','DBOFFUS','DBONUS','CRONUS','FLAZZBCA')");
            if (rowsAcquirer.Count() > 0)
            {
                //if (rowsAcquirer[0]["Name"].ToString() == "BCA1")
                //    UpdateAcquirerIssuerTag("BCA2", _sVersion == "1.00" ? "AA35" : "AA035", "1", ref _dtTerminalProfile);
                if (rowsAcquirer[0]["Name"].ToString() == "CROFFUS")
                    UpdateAcquirerIssuerTag("CROFFUS", _sVersion == "1.00" ? "AA35" : "AA035", "1", ref _dtTerminalProfile);
                else
                    UpdateAcquirerIssuerTag(rowsAcquirer[0]["Name"].ToString(), _sVersion == "1.00" ? "AA35" : "AA035", "1", ref _dtTerminalProfile);
                _dtTerminalProfile.AcceptChanges();
            }
        }
       
        // add tatang 180803 for avocado & AVOCADO_MOVE
       //public static string sValueSoftware;
        //
            
        protected void UpdateProfileTag(EMS_XML.DataClass.TerminalClass terminalTemp, DataTable dtEmsXmlMapColumn
            , ref DataTable _dtTerminalProfile, string _sVersion, string sFileTrace)
        {
            DataRow[] rowsEmsXmlcolumn = dtEmsXmlMapColumn.Select("EMSXMLFormId <> 7 AND EMSXMLFormId <> 0");

            DataRow[] rowsRelation = _dtTerminalProfile.Select("Tag LIKE 'AD%'", "Id");
            DataTable dtAcqIssCard = new DataTable();
            DataTable dtAcqIssCardNonCredit = new DataTable();// add tatang
            if (rowsRelation.Count() > 0)
                dtAcqIssCard = dtGenerateAcqIssCard(rowsRelation);

            string sStatusPromo = "";

            if (rowsEmsXmlcolumn.Count() > 0)
            {
                foreach (DataRow rowEmsXmlColumn in rowsEmsXmlcolumn)
                {
                    string sEmsXmlColumn = rowEmsXmlColumn["EMSXMLColumn"].ToString();
                    string sEmsXmlFilter = rowEmsXmlColumn["EMSXMLFilter"].ToString();
                    string sEmsXmlFormId = rowEmsXmlColumn["EMSXMLFormId"].ToString();
                    string sEmsXmlTag = rowEmsXmlColumn["EMSXMLTag"].ToString();


                    if (sEmsXmlFilter == "BCA") sEmsXmlFilter = "CROFFUS";
                    if (sEmsXmlFilter == "LOYALTY") sEmsXmlFilter = "CRONUS";
                    if (sEmsXmlFilter == "DEBIT") sEmsXmlFilter = "DBOFFUS";
                    if (sEmsXmlFilter == "LOYALTYDBT") sEmsXmlFilter = "DBONUS";

                    string sValue = sGetColumnValue(terminalTemp, sEmsXmlColumn);
                    //string sValueLoyalty = terminalTemp.sGetColumnValue("LOYALTY");
                    //string sValueLoyaltyDebit = terminalTemp.sGetColumnValue("DEBIT_LOYALTI");

                    string sValueSoftware = sGetColumnValue(terminalTemp, "Software_Version");

                    Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} START of UpdateProfileTag {2} : {3}", Trace.sFullTime(), sEmsXmlColumn, sEmsXmlTag, sValue));

                    if (!string.IsNullOrEmpty(sValue))
                    {
                        switch (sEmsXmlFormId)
                        {
                            case "1":
                                #region "Terminal"
                                if (!(sValueSoftware == "AVOCADO_MOVE" && sEmsXmlTag == "DE108"))if (!(sValueSoftware == "AVOCADO" && sEmsXmlTag == "DE106")) //"DE108"cardless avocado move & cardless DE106 avocado not update again // add tatang
                                        if (!(sEmsXmlTag=="DE005" && sEmsXmlColumn== "Fare_Non_Fare_Diners" && sValue.ToUpper()=="1")) // add tatang for Fare_Non_Fare_Diners but Fare_Non_Fare = 0 agar ga 1 mpn //190121
                                            UpdateTerminalTag(sEmsXmlTag, sValue.ToUpper(), ref _dtTerminalProfile);
                                string sValueTunai = terminalTemp.sGetColumnValue("Tunai");
                                if (sValue.ToLower() == "slcs" || sValue.ToLower() == "retl")
                                {
                                    #region
                                    if (sValueTunai == "1")
                                    {
                                        //UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE20" : "AE020", "0", ref _dtTerminalProfile);
                                        //UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE20" : "AE020", "0", ref _dtTerminalProfile);
                                        // add tatang
                                        if (sValueSoftware=="AVOCADO_MOVE")
                                        {
                                            UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE20" : "AE020", "1", ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE20" : "AE020", "1", ref _dtTerminalProfile);
                                        } else
                                        {
                                            UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE20" : "AE020", "0", ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE20" : "AE020", "0", ref _dtTerminalProfile);
                                        }
                                        //
                                    }
                                    else
                                    {
                                        //UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE20" : "AE020", "1", ref _dtTerminalProfile);
                                        //UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE20" : "AE020", "1", ref _dtTerminalProfile);
                                        if (sValueSoftware == "AVOCADO_MOVE")
                                        {
                                            UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE20" : "AE020", "0", ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE20" : "AE020", "0", ref _dtTerminalProfile);
                                        }
                                        else
                                        {
                                            UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE20" : "AE020", "1", ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE20" : "AE020", "1", ref _dtTerminalProfile);
                                        }
                                    }
                                    //UpdateAcquirerIssuerTag("DEBIT", _sVersion == "1.00" ? "AA52" : "AA052", "SALE", ref _dtTerminalProfile);
                                    //UpdateAcquirerIssuerTag("DEBIT", _sVersion == "1.00" ? "AA53" : "AA053", "TUNAI BCA", ref _dtTerminalProfile);

                                    UpdateAcquirerIssuerTag("DBONUS", _sVersion == "1.00" ? "AA52" : "AA052", "SALE", ref _dtTerminalProfile);
                                    UpdateAcquirerIssuerTag("DBONUS", _sVersion == "1.00" ? "AA53" : "AA053", "TUNAI BCA", ref _dtTerminalProfile);

                                    //UpdateAcquirerIssuerTag("LOYALTYDBT", _sVersion == "1.00" ? "AA52" : "AA052", "SALE", ref _dtTerminalProfile);
                                    //UpdateAcquirerIssuerTag("LOYALTYDBT", _sVersion == "1.00" ? "AA53" : "AA053", "TUNAI BCA", ref _dtTerminalProfile);

                                    UpdateTerminalTag(sEmsXmlTag, _sVersion == "1.00" ? sValue : "RETL", ref _dtTerminalProfile);

                                    #endregion
                                }
                                else if (sValue.ToLower() == "airl")
                                {
                                    #region
                                    //UpdateAcquirerIssuerTag("BCA", _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                    //UpdateAcquirerIssuerTag("BCA", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                    //UpdateAcquirerIssuerTag("BCA", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2

                                    UpdateAcquirerIssuerTag("CRONUS", _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                    UpdateAcquirerIssuerTag("CRONUS", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                    UpdateAcquirerIssuerTag("CRONUS", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2

                                    //UpdateAcquirerIssuerTag("BCA1", _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                    //UpdateAcquirerIssuerTag("BCA1", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                    //UpdateAcquirerIssuerTag("BCA1", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2

                                    //UpdateAcquirerIssuerTag("BCA2", _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                    //UpdateAcquirerIssuerTag("BCA2", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                    //UpdateAcquirerIssuerTag("BCA2", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2    

                                    UpdateAcquirerIssuerTag("CROFFUS", _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                    UpdateAcquirerIssuerTag("CROFFUS", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                    UpdateAcquirerIssuerTag("CROFFUS", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2                                

                                    //UpdateAcquirerIssuerTag("DEBIT", _sVersion == "1.00" ? "AA51" : "AA051", "", ref _dtTerminalProfile); //GENERIC TEXT
                                    //UpdateAcquirerIssuerTag("DEBIT", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile);//Display Amount 1
                                    //UpdateAcquirerIssuerTag("DEBIT", _sVersion == "1.00" ? "AA53" : "AA053", "TUNAI BCA", ref _dtTerminalProfile);//Display Amount 2

                                    UpdateAcquirerIssuerTag("DBONUS", _sVersion == "1.00" ? "AA51" : "AA051", "", ref _dtTerminalProfile); //GENERIC TEXT
                                    UpdateAcquirerIssuerTag("DBONUS", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile);//Display Amount 1
                                    UpdateAcquirerIssuerTag("DBONUS", _sVersion == "1.00" ? "AA53" : "AA053", "TUNAI BCA", ref _dtTerminalProfile);//Display Amount 2

                                    UpdateAcquirerIssuerTag("FLAZZBCA", _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                    UpdateAcquirerIssuerTag("FLAZZBCA", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                    UpdateAcquirerIssuerTag("FLAZZBCA", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2

                                    UpdateAcquirerIssuerTag("SAKUKU", _sVersion == "1.00" ? "AA51" : "AA051", "", ref _dtTerminalProfile); //GENERIC TEXT
                                    UpdateAcquirerIssuerTag("SAKUKU", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                    UpdateAcquirerIssuerTag("SAKUKU", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2

                                    UpdateAcquirerIssuerTag("PROMO A", _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                    UpdateAcquirerIssuerTag("PROMO A", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                    UpdateAcquirerIssuerTag("PROMO A", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2

                                    UpdateAcquirerIssuerTag("PROMO B", _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                    UpdateAcquirerIssuerTag("PROMO B", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                    UpdateAcquirerIssuerTag("PROMO B", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2

                                    //if (sValueLoyalty == "1")
                                    //{
                                    //    UpdateAcquirerIssuerTag("LOYALTY", _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile);
                                    //    UpdateAcquirerIssuerTag("LOYALTY", _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile);
                                    //    UpdateAcquirerIssuerTag("LOYALTY", _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile);
                                    //}
                                    #endregion
                                }
                                else if (sValue.ToLower() == "rest")
                                {
                                   // UpdateAcquirerIssuerTag("BCA", _sVersion == "1.00" ? "AA52" : "AA052", "BASE", ref _dtTerminalProfile);
                                   // UpdateAcquirerIssuerTag("BCA", _sVersion == "1.00" ? "AA53" : "AA053", "TIP", ref _dtTerminalProfile);
                                    UpdateAcquirerIssuerTag("CRONUS", _sVersion == "1.00" ? "AA52" : "AA052", "BASE", ref _dtTerminalProfile);
                                    UpdateAcquirerIssuerTag("CRONUS", _sVersion == "1.00" ? "AA53" : "AA053", "TIP", ref _dtTerminalProfile);
                                    //if (sValueLoyalty == "1")
                                    //{
                                    //    UpdateAcquirerIssuerTag("LOYALTY", _sVersion == "1.00" ? "AA52" : "AA052", "BASE", ref _dtTerminalProfile);
                                    //    UpdateAcquirerIssuerTag("LOYALTY", _sVersion == "1.00" ? "AA53" : "AA053", "TIP", ref _dtTerminalProfile);
                                    //}
                                }
                                else if (sValue.ToLower() == "cash")
                                    UpdateTerminalTag(sEmsXmlTag, _sVersion == "1.00" ? sValue : "CASH", ref _dtTerminalProfile);


                                switch (sEmsXmlColumn.ToLower())
                                {
                                    case "func_99_password":
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE28" : "DE028", sValue, ref _dtTerminalProfile);
                                        break;
                                    case "flazzperdana":
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE60" : "DE060", sValue, ref _dtTerminalProfile);
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE61" : "DE061", sValue, ref _dtTerminalProfile);
                                        break;
                                    case "flazztopup":
                                        string sTemp = terminalTemp.sGetColumnValue("topup_tunai");
                                        if (sValue == "0" && sTemp == "0") sValue = "0";
                                        else sValue = "1";
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE51" : "DE051", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ BCA", _sVersion == "1.00" ? "AE33" : "AE033", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ CRD", _sVersion == "1.00" ? "AE33" : "AE033", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ DEBIT", _sVersion == "1.00" ? "AE33" : "AE033", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ DBT", _sVersion == "1.00" ? "AE33" : "AE033", sValue, ref _dtTerminalProfile); // add tatang  2018-10-31 
                                        break;
                                    case "topup_tunai":
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE51" : "DE051", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ BCA", _sVersion == "1.00" ? "AE33" : "AE033", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ CRD", _sVersion == "1.00" ? "AE33" : "AE033", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ DEBIT", _sVersion == "1.00" ? "AE33" : "AE033", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ DBT", _sVersion == "1.00" ? "AE33" : "AE033", sValue, ref _dtTerminalProfile); // add tatang  2018-10-31 
                                        break;
                                    case "flazzpayment":
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE98" : "DE098", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ BCA", _sVersion == "1.00" ? "AE34" : "AE034", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ CRD", _sVersion == "1.00" ? "AE34" : "AE034", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ DEBIT", _sVersion == "1.00" ? "AE34" : "AE034", sValue, ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag("FLAZZ DBT", _sVersion == "1.00" ? "AE34" : "AE034", sValue, ref _dtTerminalProfile); // add tatang  2018-10-31 
                                        break;
                                    case "cicilan_reguler":
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE21" : "DE021", sValue, ref _dtTerminalProfile);
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE65" : "DE065", sValue, ref _dtTerminalProfile);
                                        break;
                                    case "cicilan_promo":
                                        string sValueReguler = sGetColumnValue(terminalTemp, "cicilan_reguler");
                                        if (sValueReguler == "1")
                                            UpdateTerminalTag(_sVersion == "1.00" ? "DE65" : "DE065", "1", ref _dtTerminalProfile);
                                        else if (sValueReguler == "0" && sValue == "1")
                                            UpdateTerminalTag(_sVersion == "1.00" ? "DE21" : "DE021", "1", ref _dtTerminalProfile);
                                        break;
                                    case "dcc":
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE14" : "DE014", sValue, ref _dtTerminalProfile);
                                        UpdateTerminalTag(_sVersion == "1.00" ? "DE15" : "DE015", sValue, ref _dtTerminalProfile);
                                        break;
                                    case "reward":
                                        //if (sValueLoyalty == "1" || sValueLoyaltyDebit == "1") sValue = "1";
                                        //UpdateTerminalTag(_sVersion == "1.00" ? "DE20" : "DE020", sValue, ref _dtTerminalProfile);
                                        break;
                                    case "cup":
                                        // CUP always ENABLE
                                        UpdateTerminalTag(sEmsXmlTag, "1", ref _dtTerminalProfile);
                                        break;
                                    case "eal":
                                        UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE51" : "AE051", sValue, ref _dtTerminalProfile);
                                        // 2017-12-13
                                        UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE51" : "AE051", sValue, ref _dtTerminalProfile);
                                        break;
                                    case "mpn":
                                        //2018-10-31
                                        if (sValueSoftware == "AVOCADO_MOVE")
                                        {
                                            UpdateTerminalTag("DE116", sValue, ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE78" : "AE078", sValue, ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE78" : "AE078", sValue, ref _dtTerminalProfile);
                                        }
                                        else
                                        {
                                            UpdateTerminalTag(_sVersion == "5.00" ? "DE110": "DE110", sValue, ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE31" : "AE031", sValue, ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE31" : "AE031", sValue, ref _dtTerminalProfile);                                            
                                        }
                                        break;
                                    case "va":
                                        //2018-10-31
                                        if (sValueSoftware == "AVOCADO_MOVE" && sEmsXmlColumn.ToLower() == "va")
                                            { UpdateTerminalTag("DE114", sValue, ref _dtTerminalProfile); }
                                        else 
                                        if (sValueSoftware == "AVOCADO" && sEmsXmlColumn.ToLower() == "va")
                                            { UpdateTerminalTag("DE108", sValue, ref _dtTerminalProfile); }
                                        else { UpdateTerminalTag("DE108", sValue, ref _dtTerminalProfile); }
                                        UpdateAcquirerIssuerTag("DEBIT BCA", _sVersion == "1.00" ? "AE77" : "AE077", sValue, ref _dtTerminalProfile);
                                        //2017-12-13
                                        UpdateAcquirerIssuerTag("DEBIT MC BCA", _sVersion == "1.00" ? "AE77" : "AE077", sValue, ref _dtTerminalProfile);
                                        break;
                                }
                                #endregion
                                break;
                            case "2":
                                #region "Acquirer"                                

                                string sValueOnUs = terminalTemp.sGetColumnValue("OnUs");
                                string sValueOffUs = terminalTemp.sGetColumnValue("Off_Us");
                                //if (sValueLoyalty == "0" && sValueOnUs == "1" && sValueOffUs == "1" && sEmsXmlFilter == "BCA")
                                //    sEmsXmlFilter = "BCA1";

                                //if (sValueOffUs == "1" && sEmsXmlFilter == "BCA")
                                //    sEmsXmlFilter = "CROFFUS";
                                //if (sValueOffUs == "0" && sEmsXmlFilter == "LOYALTY")
                                //    sEmsXmlFilter = "CRONUS";
                                if (sEmsXmlColumn.ToLower() == "force_settlementday")
                                sValue = string.Format("{0:00}", int.Parse(sValue));
                                else if (sEmsXmlColumn.ToLower() == "minimumtopup")
                                    sValue = string.Format("{0:00000000}", int.Parse(sValue));
                                #region "Fare non Fire"
                                string sValueAirl = terminalTemp.sGetColumnValue("fare_non_fare");
                                string sStatus = "0";
                                if (sValueAirl == "1")
                                {
                                    //sEmsXmlFilter.Select()
                                    //if ((sEmsXmlTag == "AA049" || sEmsXmlTag == "AA050") && sValueOnUs == "1")
                                    //{
                                    //    sValue = "1";
                                    //    sEmsXmlFilter = "BCA1";
                                    //    UpdateAcquirerIssuerTag(sEmsXmlFilter, sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                    //    sStatus = "1";
                                    //}
                                    //if (sEmsXmlFilter == "BCA2")
                                    //{
                                    //    UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA049", "1", ref _dtTerminalProfile);
                                    //    UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA050", "1", ref _dtTerminalProfile);
                                    //    sStatus = "1";
                                    //}
                                    if (sEmsXmlFilter == "CRONUS")
                                    {
                                        UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA049", "1", ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA050", "1", ref _dtTerminalProfile);
                                        sStatus = "1";
                                    }
                                    if (sEmsXmlFilter == "FLAZZBCA")
                                    {
                                        UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA049", "1", ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA050", "1", ref _dtTerminalProfile);
                                        sStatus = "1";
                                    }

                                    if (sEmsXmlFilter == "PROMO A" || sEmsXmlFilter == "PROMO B" || sEmsXmlFilter == "PROMO C" || sEmsXmlFilter == "PROMO D" || sEmsXmlFilter == "PROMO E" || sEmsXmlFilter == "PROMO F" || sEmsXmlFilter == "PROMO G" || sEmsXmlFilter == "PROMO H" || sEmsXmlFilter == "PROMO I" || sEmsXmlFilter == "PROMO J"
                                        || sEmsXmlFilter == "PROMO SQ A" || sEmsXmlFilter == "PROMO SQ B" || sEmsXmlFilter == "PROMO SQ C" || sEmsXmlFilter == "PROMO SQ D" || sEmsXmlFilter == "PROMO SQ E" || sEmsXmlFilter == "PROMO SQ F" || sEmsXmlFilter == "PROMO SQ G" || sEmsXmlFilter == "PROMO SQ H" || sEmsXmlFilter == "PROMO SQ I" || sEmsXmlFilter == "PROMO SQ J")
                                    {
                                        if (sStatusPromo != sEmsXmlFilter + "1")
                                        {
                                            UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA049", "1", ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA050", "1", ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag(sEmsXmlFilter, _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                            UpdateAcquirerIssuerTag(sEmsXmlFilter, _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                            UpdateAcquirerIssuerTag(sEmsXmlFilter, _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2
                                            sStatusPromo = sEmsXmlFilter + "1";
                                            sStatus = "1";
                                        }
                                    }
                                    //if (sValueLoyalty == "1" && sEmsXmlFilter == "LOYALTY")
                                    //{
                                    //    UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA049", "1", ref _dtTerminalProfile);
                                    //    UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA050", "1", ref _dtTerminalProfile);
                                    //    sStatus = "1";
                                    //}
                                    //if (sValueOffUs == "1" && sEmsXmlFilter == "CROFFUS")
                                    if (sValueOffUs == "1" && sEmsXmlFilter == "CROFFUS")
                                    {
                                        UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA049", "1", ref _dtTerminalProfile);
                                        UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA050", "1", ref _dtTerminalProfile);
                                        sStatus = "1";
                                    }
                                }
                                if (sStatus == "1")
                                {
                                    UpdateAcquirerIssuerTag(sEmsXmlFilter, sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                }
                                #endregion
                                else UpdateAcquirerIssuerTag(sEmsXmlFilter, sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                #endregion
                                break;
                            case "3":
                                #region "Issuer"
                                string[] keys = new string[] { "credit", "debit_loyalti", "unionpay" };
                                string sKeyResult = keys.FirstOrDefault<string>(s => sEmsXmlFilter.ToLower().Contains(s));

                                if (!string.IsNullOrEmpty(sKeyResult))
                                {

                                    switch (sKeyResult)
                                    {
                                        case "credit": //acquirer update all issuer on credit acquirer
                                            if (sValue == "1")
                                            {
                                                string[] arrsFilter = sEmsXmlFilter.Split(';');
                                                string sValueCredit = sGetColumnValue(terminalTemp, "Credit");
                                                if (sValueCredit == "1")
                                                {
                                                    #region 
                                                    sValue = arrsFilter[1] == "0" ? "1" : "0";
                                                    //DataRow[] rowsCredit = dtAcqIssCard.Select("Acquirer IN ('BCA','BCA1','BCA2','BCA3','BCA4','BCA5','BCA6','BCA7')");
                                                    DataRow[] rowsCredit = dtAcqIssCard.Select("Acquirer IN ('CRONUS','CROFFUS')");
                                                    if (rowsCredit.Length > 0)
                                                        foreach (DataRow rowIssuerCredit in rowsCredit)
                                                        {
                                                            //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                                            //add 18-08-03 tatang
                                                            string sValueContent = sGetColumnValue(terminalTemp, "Card_Ver");
                                                            if (sValueSoftware == "AVOCADO")
                                                            {
                                                                #region 
                                                                if (sEmsXmlTag == "AE014")
                                                                {
                                                                    if (sValueContent == "1")
                                                                    {
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE014", "0", ref _dtTerminalProfile);
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE078", "1", ref _dtTerminalProfile);
                                                                        UpdateTerminalTag("DE005", "HOTL", ref _dtTerminalProfile);

                                                                        //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE076", "1", ref _dtTerminalProfile);//Save Reversal Cardver/Auth
                                                                    }
                                                                    else if (sValueContent == "0")
                                                                    {
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE014", "1", ref _dtTerminalProfile);
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE078", "0", ref _dtTerminalProfile);
                                                                        //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE076", "0", ref _dtTerminalProfile);//Save Reversal Cardver/Auth
                                                                    }
                                                                }
                                                                else UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                                                #endregion
                                                            }
                                                            else
                                                            if (sValueSoftware == "AVOCADO_MOVE")
                                                            {
                                                                #region               
                                                                if (sEmsXmlTag == "AE014")
                                                                {
                                                                    string sValueCardVercpl = sGetColumnValue(terminalTemp, "Card_Ver");
                                                                    if (sValueCardVercpl == "1")
                                                                    {
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE014", "1", ref _dtTerminalProfile);
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE079", "1", ref _dtTerminalProfile);
                                                                        UpdateTerminalTag("DE005", "HOTL", ref _dtTerminalProfile);

                                                                        //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE076", "1", ref _dtTerminalProfile);//Save Reversal Cardver/Auth
                                                                    }
                                                                    else if (sValueCardVercpl == "0")
                                                                    {
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE014", "0", ref _dtTerminalProfile);
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE079", "0", ref _dtTerminalProfile);
                                                                        //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE076", "0", ref _dtTerminalProfile);//Save Reversal Cardver/Auth
                                                                    }
                                                                }
                                                                //else 

                                                                #endregion

                                                                #region        
                                                                //if (sEmsXmlTag == "AE013") // refund
                                                                //{
                                                                //    //ikut yang lama, hanya dibalik dari disable =enable 
                                                                //    if (sValue == "0")
                                                                //        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE013", "1", ref _dtTerminalProfile);
                                                                //    else
                                                                //        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE013", "0", ref _dtTerminalProfile);

                                                                //} 
                                                                else UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                                                #endregion
                                                                #region        
                                                                //if (sEmsXmlTag == "AE025") // Enable Auth                                                               
                                                                //    UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE025", "1", ref _dtTerminalProfile);                                                                
                                                                #endregion
                                                            }
                                                            else
                                                            {
                                                                UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                                                if (sEmsXmlTag == "AE014" && sValueContent == "1")
                                                                UpdateTerminalTag("DE005", "HOTL", ref _dtTerminalProfile);
                                                            }

                                                            //                                                       

                                                        }
                                                    #endregion
                                                }
                                                //sValueLoyalty = sGetColumnValue(terminalTemp, "Loyalty");
                                                //if (sValueLoyalty == "1")
                                                sValueOnUs = sGetColumnValue(terminalTemp, "OnUs");
                                                if (sValueOnUs == "1")
                                                {
                                                    #region
                                                    sValue = arrsFilter[1] == "0" ? "1" : "0";
                                                    //DataRow[] rowsCredit = dtAcqIssCard.Select("Acquirer='LOYALTY'");
                                                    DataRow[] rowsCredit = dtAcqIssCard.Select("Acquirer='CRONUS'");
                                                    if (rowsCredit.Length > 0)
                                                        foreach (DataRow rowIssuerCredit in rowsCredit)
                                                        {
                                                            //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                                            //add 18-08-03 tatang
                                                            if (sValueSoftware == "AVOCADO")
                                                            {
                                                                #region
                                                                if (sEmsXmlTag == "AE014")
                                                                {
                                                                    string sValueCardVer = sGetColumnValue(terminalTemp, "Card_Ver");
                                                                    if (sValueCardVer == "1")
                                                                    {
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE014", "0", ref _dtTerminalProfile);
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE078", "1", ref _dtTerminalProfile);
                                                                        //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE076", "1", ref _dtTerminalProfile);//Save Reversal Cardver/Auth
                                                                    }
                                                                    else if (sValueCardVer == "0")
                                                                    {
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE014", "1", ref _dtTerminalProfile);
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE078", "0", ref _dtTerminalProfile);
                                                                        //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE076", "0", ref _dtTerminalProfile);//Save Reversal Cardver/Auth
                                                                    }
                                                                } else UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                                                #endregion
                                                            }
                                                            else
                                                            if (sValueSoftware == "AVOCADO_MOVE")
                                                            {
                                                                #region
                                                                if (sEmsXmlTag == "AE014")
                                                                {
                                                                    string sValueCardVercpl = sGetColumnValue(terminalTemp, "Card_Ver");
                                                                    if (sValueCardVercpl == "1")
                                                                    {
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE014", "1", ref _dtTerminalProfile);
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE079", "1", ref _dtTerminalProfile);
                                                                        //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE076", "1", ref _dtTerminalProfile);//Save Reversal Cardver/Auth
                                                                    }
                                                                    else if (sValueCardVercpl == "0")
                                                                    {
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE014", "0", ref _dtTerminalProfile);
                                                                        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE079", "0", ref _dtTerminalProfile);
                                                                        //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE076", "0", ref _dtTerminalProfile);//Save Reversal Cardver/Auth
                                                                    }
                                                                } else
                                                                #endregion
                                                                #region        
                                                                //if (sEmsXmlTag == "AE013") // refund
                                                                //{
                                                                //    //ikut yang lama, hanya dibalik dari disable =enable 
                                                                //    if (sValue == "0")
                                                                //        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE013", "1", ref _dtTerminalProfile);
                                                                //    else
                                                                //        UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE013", "0", ref _dtTerminalProfile);                                                              
                                                                //}
                                                                //else 
                                                                    UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                                                #endregion
                                                                #region        
                                                                //if (sEmsXmlTag == "AE015") //void        
                                                                //{
                                                                //    //UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), "AE015", "1", ref _dtTerminalProfile);
                                                                //    UpdateAcquirerIssuerTag("BCA", "AE015", "1", ref _dtTerminalProfile);
                                                                //    UpdateAcquirerIssuerTag("VISA BCA", "AE015", "1", ref _dtTerminalProfile);
                                                                //    UpdateAcquirerIssuerTag("MASTER BCA", "AE015", "1", ref _dtTerminalProfile);
                                                                //    UpdateAcquirerIssuerTag("JCB BCA", "AE015", "1", ref _dtTerminalProfile);
                                                                //    UpdateAcquirerIssuerTag("SMARTCASH", "AE015", "1", ref _dtTerminalProfile);
                                                                //    UpdateAcquirerIssuerTag("AMEX BCA", "AE015", "1", ref _dtTerminalProfile);
                                                                //}
                                                                #endregion
                                                            }
                                                            else
                                                            {                                                                
                                                                UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                                            }
                                                            // end tatang                                                      

                                                        }
                                                    #endregion
                                                }
                                            }

                                            break;
                                        case "debit_loyalti": //acquirer update all issuer on loyaltidbt acquirer
                                            //if (sValue == "1")
                                            //{
                                            //    string[] arrsFilter = sEmsXmlFilter.Split(';');
                                            //    string sValueLoyaltyDbt = sGetColumnValue(terminalTemp, "Debit_Loyalti");
                                            //    if (sValueLoyaltyDbt == "1")
                                            //    {
                                            //        sValue = arrsFilter[1] == "0" ? "1" : "0";
                                            //        DataRow[] rowsCredit = dtAcqIssCard.Select("Acquirer IN ('LOYALTYDBT')");
                                            //        if (rowsCredit.Length > 0)
                                            //            foreach (DataRow rowIssuerCredit in rowsCredit)
                                            //                UpdateAcquirerIssuerTag(rowIssuerCredit["Issuer"].ToString(), sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                            //    }
                                            //}
                                            break;
                                        case "unionpay":
                                            if (sValue == "1")
                                            {
                                                UpdateAcquirerIssuerTag("UNIONPAY CREDIT", sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                                UpdateAcquirerIssuerTag("UNIONPAY DEBIT", sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    #region
                                    switch (sEmsXmlFilter.ToLower())
                                    {
                                        case "false":
                                            UpdateAcquirerIssuerTag("", sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                            break;
                                        case "true":
                                            if (sValue == "1") UpdateAcquirerIssuerTag("", sEmsXmlTag, "0", ref _dtTerminalProfile);
                                            else UpdateAcquirerIssuerTag("", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                            break;
                                        default:
                                            if (sEmsXmlColumn.ToLower() == "allowfinansialoffus_amexcard" && terminalTemp.sGetColumnValue("Off_Us") == "true")
                                                UpdateAcquirerIssuerTag(sEmsXmlFilter, sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                            else if (sEmsXmlColumn == "allowfinansialonus_amexcard" && terminalTemp.sGetColumnValue("OnUs") == "true")
                                                UpdateAcquirerIssuerTag(sEmsXmlFilter, sEmsXmlTag, sValue, ref _dtTerminalProfile);

                                            // 180914 add tatang for sesuaikan dengan onus off us
                                            else if (_sVersion == "5.00")
                                            {
                                                if (terminalTemp.sGetColumnValue("OnUs") == "1")
                                                {
                                                    if (sEmsXmlColumn == "allowfinansialonus_amexcard")
                                                        UpdateAcquirerIssuerTag("AMEX BCA", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                    if (sEmsXmlColumn.ToLower() == "allowfinansialonus_bca")
                                                        UpdateAcquirerIssuerTag("BCA", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                    else if (sEmsXmlColumn.ToLower() == "allowfinansialonus_visacard")
                                                        UpdateAcquirerIssuerTag("VISA BCA", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                    else if (sEmsXmlColumn.ToLower() == "allowfinansialonus_mastercard")
                                                        UpdateAcquirerIssuerTag("MASTER BCA", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                    else if (sEmsXmlColumn.ToLower() == "allowfinansialonus_jcbcard")
                                                        UpdateAcquirerIssuerTag("JCB BCA", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                    else if (sEmsXmlColumn.ToLower() == "allowfinansialonus_cupcard")
                                                        UpdateAcquirerIssuerTag("UNIONPAY", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                }
                                                if (terminalTemp.sGetColumnValue("Off_Us") == "1")
                                                {
                                                    if (sEmsXmlColumn.ToLower() == "allowfinansialoffus_amexcard")
                                                        UpdateAcquirerIssuerTag("AMEX", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                    else if (sEmsXmlColumn.ToLower() == "allowfinansialoffus_visacard")
                                                        UpdateAcquirerIssuerTag("VISA", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                    else if (sEmsXmlColumn.ToLower() == "allowfinansialoffus_mastercard")
                                                        UpdateAcquirerIssuerTag("MASTERCARD", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                    else if (sEmsXmlColumn.ToLower() == "allowfinansialoffus_jcbcard")
                                                        UpdateAcquirerIssuerTag("JCB", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                    else if (sEmsXmlColumn.ToLower() == "allowfinansialoffus_cupcard")
                                                        UpdateAcquirerIssuerTag("UNIONPAY", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                                }
                                            }
                                            // end tatang


                                            // update for flazz positive list
                                            else if (sEmsXmlColumn.ToLower() == "flazz_hi_co_brand_code")
                                            {
                                                string sValuePaymentCoBrandCode = null;
                                                int iValue;
                                                for (iValue = int.Parse(sValue) + 1; iValue < 100; iValue++)
                                                {
                                                    sValuePaymentCoBrandCode = string.Format("{0}{1:00}", sValuePaymentCoBrandCode, iValue);
                                                    if (sValuePaymentCoBrandCode.Length == 100) break;
                                                }

                                                //while (sValuePaymentCoBrandCode.Length < 100)
                                                //    sValuePaymentCoBrandCode = string.Format("{0}00", sValuePaymentCoBrandCode);
                                                //UpdateAcquirerIssuerTag(sEmsXmlFilter, "AE038", sValuePaymentCoBrandCode, ref _dtTerminalProfile);
                                            }
                                            //else if (sEmsXmlColumn.ToLower() == "reward_debit_poin")
                                            //{
                                            //    if (sValue == "0")
                                            //        UpdateAcquirerIssuerTag("", sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                            //    else
                                            //        UpdateAcquirerIssuerTag(sEmsXmlFilter, sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                            //}
                                            else
                                                UpdateAcquirerIssuerTag(sEmsXmlFilter, sEmsXmlTag, sValue, ref _dtTerminalProfile);

                                            break;
                                    }
                                    #endregion
                                    // 180803 add tatang
                                    string sValueCardVer = sGetColumnValue(terminalTemp, "Card_Ver");
                                    if (sValueSoftware == "AVOCADO")
                                    {
                                        #region
                                        if (!string.IsNullOrEmpty(sValueCardVer))
                                        {
                                            if (sValueCardVer == "0" || sValueCardVer == "1")
                                            {
                                                UpdateAcquirerIssuerTag("DEBIT BCA", "AE014", "1", ref _dtTerminalProfile);
                                                UpdateAcquirerIssuerTag("DEBIT BCA", "AE078", "0", ref _dtTerminalProfile);
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    if (sValueSoftware == "AVOCADO_MOVE")
                                    {
                                        #region
                                        if (!string.IsNullOrEmpty(sValueCardVer))
                                        {
                                            if (sValueCardVer == "0" || sValueCardVer == "1")
                                            {
                                                UpdateAcquirerIssuerTag("DEBIT BCA", "AE014", "0", ref _dtTerminalProfile);
                                                UpdateAcquirerIssuerTag("DEBIT BCA", "AE079", "0", ref _dtTerminalProfile);
                                            }
                                        }
                                        #endregion
                                    }
                                    // end tatang
                                }
                                //if (sEmsXmlFilter.ToLower() == "false")
                                //    UpdateAcquirerIssuerTag("", sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                //else if (sEmsXmlFilter.ToLower() == "true" && sValue == "1")
                                //    UpdateAcquirerIssuerTag("", sEmsXmlTag, "0", ref _dtTerminalProfile);
                                //else if (sEmsXmlFilter.ToLower() == "true" && sValue == "0")
                                //    UpdateAcquirerIssuerTag("", sEmsXmlTag, "1", ref _dtTerminalProfile);
                                //else
                                //    UpdateAcquirerIssuerTag(sEmsXmlFilter, sEmsXmlTag, sValue, ref _dtTerminalProfile);
                                
                                break;
                                #endregion
                        }
                    }
                    else
                    {
                        switch (sEmsXmlFormId)
                        {
                            case "2":
                                #region "Acquirer"                                

                                string sValueAirl = terminalTemp.sGetColumnValue("fare_non_fare");

                                if (sValueAirl == "1")
                                {
                                    if (sEmsXmlFilter == "PROMO A" || sEmsXmlFilter == "PROMO B" || sEmsXmlFilter == "PROMO C" || sEmsXmlFilter == "PROMO D" || sEmsXmlFilter == "PROMO E" || sEmsXmlFilter == "PROMO F" || sEmsXmlFilter == "PROMO G" || sEmsXmlFilter == "PROMO H" || sEmsXmlFilter == "PROMO I" || sEmsXmlFilter == "PROMO J"
                                        || sEmsXmlFilter == "PROMO SQ A" || sEmsXmlFilter == "PROMO SQ B" || sEmsXmlFilter == "PROMO SQ C" || sEmsXmlFilter == "PROMO SQ D" || sEmsXmlFilter == "PROMO SQ E" || sEmsXmlFilter == "PROMO SQ F" || sEmsXmlFilter == "PROMO SQ G" || sEmsXmlFilter == "PROMO SQ H" || sEmsXmlFilter == "PROMO SQ I" || sEmsXmlFilter == "PROMO SQ J")
                                    {
                                        if (sStatusPromo != sEmsXmlFilter + "1")
                                        {   UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA049", "1", ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag(sEmsXmlFilter, "AA050", "1", ref _dtTerminalProfile);
                                            UpdateAcquirerIssuerTag(sEmsXmlFilter, _sVersion == "1.00" ? "AA51" : "AA051", "TIKET NO :", ref _dtTerminalProfile); //GENERIC TEXT
                                            UpdateAcquirerIssuerTag(sEmsXmlFilter, _sVersion == "1.00" ? "AA52" : "AA052", "FARE", ref _dtTerminalProfile); //Display Amount 1
                                            UpdateAcquirerIssuerTag(sEmsXmlFilter, _sVersion == "1.00" ? "AA53" : "AA053", "N/F", ref _dtTerminalProfile); //Display Amount 2
                                            sStatusPromo = sEmsXmlFilter + "1";
                                        }
                                    }
                                }
                                #endregion
                                break;
                        }
                        string sEmpty = sValue;
                    }
                    _dtTerminalProfile.AcceptChanges();
                }
            }
        }

        protected string sGetColumnValue(EMS_XML.DataClass.TerminalClass terminalTemp, string sEmsXmlColumn)
        {
            string sValue = terminalTemp.sGetColumnValue(sEmsXmlColumn);
            if (sEmsXmlColumn.ToLower() == "tunai" && sValue == "1")
                sValue = "SLCS";
            else if (sEmsXmlColumn.ToLower() == "fare_non_fare" && sValue == "1")
                sValue = "AIRL";
            else if (sEmsXmlColumn.ToLower() == "tips" && sValue == "1")
                sValue = "REST";
            //else if (sEmsXmlColumn.ToLower() == "tunai" && sValue == "0") tutup oleh tatang
            //    sValue = null;
            //else if ((sEmsXmlColumn.ToLower() == "fare_non_fare" || sEmsXmlColumn.ToLower() == "tips"
            //    || sEmsXmlColumn.ToLower() == "fare_non_fare_amex" || sEmsXmlColumn.ToLower() == "tips_amex"
            //    || sEmsXmlColumn.ToLower() == "fare_non_fare_diners" || sEmsXmlColumn.ToLower() == "tips_diners")
            //    && sValue == "0")
            //    sValue = null;
            //else if (sEmsXmlColumn.ToLower() == "cashadvance" && sValue == "1")
            //    sValue = "CASH";
            //else if (sEmsXmlColumn.ToLower() == "cashadvance" && sValue == "0")
            //    sValue = null;

            // 180914 add tatang untuk hotel
            else if (sEmsXmlColumn.ToLower() == "cashadvance" && sValue == "1")
                sValue = "CASH";
            else if (sEmsXmlColumn.ToLower() == "card_ver" && sValue == "1")
                sValue = "1";
            else
            {
                if ((sEmsXmlColumn.ToLower() == "tunai" || sEmsXmlColumn.ToLower() == "cashadvance" || sEmsXmlColumn.ToLower() == "fare_non_fare" || sEmsXmlColumn.ToLower() == "tips"
                    || sEmsXmlColumn.ToLower() == "fare_non_fare_amex" || sEmsXmlColumn.ToLower() == "tips_amex"
                    || sEmsXmlColumn.ToLower() == "fare_non_fare_diners" || sEmsXmlColumn.ToLower() == "tips_diners")
                    && sValue == "0")
                    sValue = null;
            }
            // end tatang

            //else
            //    sValue = null;
            return sValue;
        }

        protected void UpdateTerminalTag(string sEmsXmlTag, string sValue, ref DataTable _dtTerminalProfile)
        {
            //if (sEmsXmlTag.Length == 4) sEmsXmlTag = sEmsXmlTag.Insert(2, "0");

            DataRow[] rowsTerminal = _dtTerminalProfile.Select(string.Format("Tag = '{0}'", sEmsXmlTag));
            if (rowsTerminal.Count() > 0)
                if (!string.IsNullOrEmpty(sValue))
                    rowsTerminal[0]["TagValue"] = sValue;
        }

        protected void UpdateAcquirerIssuerTag(string sAcquirerIssuerName, string sEmsXmlTag, string sValue, ref DataTable _dtTerminalProfile)
        {
            DataRow[] rowsAcquirerIssuer = _dtTerminalProfile.Select(
                string.IsNullOrEmpty(sAcquirerIssuerName) ? string.Format("Tag = '{0}'", sEmsXmlTag) :
                string.Format("Tag = '{0}' AND Name = '{1}'", sEmsXmlTag, sAcquirerIssuerName));
            if (rowsAcquirerIssuer.Count() > 0)
                foreach (DataRow rowAcquirerIssuer in rowsAcquirerIssuer)
                    rowAcquirerIssuer["TagValue"] = sValue;
        }

        protected void DeleteAcquirerPromo(EMS_XML.DataClass.TerminalClass terminalTemp, ref DataTable _dtTerminalProfile)
        {
            string sReguler = terminalTemp.sGetColumnValue("Cicilan_Reguler");
            string sPromo = terminalTemp.sGetColumnValue("Cicilan_Promo");
            //string sLoyalty = terminalTemp.sGetColumnValue("Loyalty");
            string sCredit = terminalTemp.sGetColumnValue("Credit");

            //string sOff_Us = terminalTemp.sGetColumnValue("Off_Us");

            string sCondition = null;
            if (sReguler == "0" && sPromo == "0")
                sCondition = string.Format("Tag IN ('AA01','AA001') AND Name LIKE 'PROMO %'");
            else if (sReguler == "1" && sPromo == "0")
            {
                //if (sCredit == "1" && sOff_Us == "1") sCondition = string.Format("Tag IN ('AA01','AA001') AND Name NOT LIKE 'PROMO A' AND Name NOT LIKE 'PROMO SQ A'");
                //else if (sCredit == "1" && sOff_Us == "0") sCondition = string.Format("Tag IN ('AA01','AA001') AND Name NOT LIKE 'PROMO A'");
                if (sCredit == "1" ) sCondition = string.Format("Tag IN ('AA01','AA001') AND Name NOT LIKE 'PROMO A' AND Name NOT LIKE 'PROMO SQ A'");
            }
            else if (sReguler == "0" && sPromo == "1")
                //if (sLoyalty == "1") sCondition = string.Format("Tag IN ('AA01','AA001') AND (Name LIKE 'PROMO A' OR Name LIKE 'PROMO SQ A')");
                //if (sCredit == "1" && sOff_Us == "1") sCondition = string.Format("Tag IN ('AA01','AA001') AND (Name LIKE 'PROMO A' OR Name LIKE 'PROMO SQ A')");
                //else sCondition = string.Format("Tag IN ('AA01','AA001') AND (Name LIKE 'PROMO A' OR Name LIKE 'PROMO SQ%')");
                if (sCredit == "1" ) sCondition = string.Format("Tag IN ('AA01','AA001') AND (Name LIKE 'PROMO A' OR Name LIKE 'PROMO SQ A')");
            else if (sReguler == "1" && sPromo == "1")
                //if (sLoyalty == "0")
                //if ((sCredit == "1" && sOff_Us == "0"))
                if (sCredit == "1" )
                {
                    sCondition = string.Format("Tag IN ('AA01','AA001') AND Name LIKE 'PROMO SQ%'");
                    DataRow[] rowsAcquirerPromo = _dtTerminalProfile.Select("Tag IN ('AA01','AA001') AND Name LIKE 'PROMO %'");
                    if (rowsAcquirerPromo.Count() > 0)
                    {
                        DataRow[] rowsAcquirer = rowsAcquirerPromo.CopyToDataTable().Select(sCondition);
                        foreach (DataRow rowAcquirerPromo in rowsAcquirer)
                            DeleteAcquirer(rowAcquirerPromo["TagValue"].ToString(), ref _dtTerminalProfile);
                    }
                    _dtTerminalProfile.AcceptChanges();
                }
            
            if (sCredit == "0")
            {
                sCondition = string.Format("Tag IN ('AA01','AA001') AND Name LIKE 'PROMO %'");
            }
            if (!string.IsNullOrEmpty(sCondition))
            {
                DataRow[] rowsAcquirerPromo = _dtTerminalProfile.Select("Tag IN ('AA01','AA001') AND Name LIKE 'PROMO %'");
                if (rowsAcquirerPromo.Count() > 0)
                {
                    DataRow[] rowsAcquirer = rowsAcquirerPromo.CopyToDataTable().Select(sCondition);
                    foreach (DataRow rowAcquirerPromo in rowsAcquirer)
                        DeleteAcquirer(rowAcquirerPromo["TagValue"].ToString(), ref _dtTerminalProfile);
                }
                _dtTerminalProfile.AcceptChanges();
            }
        }

        protected string sGetConvertColumn(string _rowAcquirerPromo)
        {
            string TemprowAcquirerPromo = "Terminal_ID_" + _rowAcquirerPromo;
            string rowAcquirerPromo = TemprowAcquirerPromo.Replace(' ', '_');
            return rowAcquirerPromo;
        }
        protected bool IsUploadSuccess(string sTerminalID, int iDatabaseId, DataSet dsTables, DataTable dtTerminalProfile, ref string sResultDesc, string sMaster)
        {
            bool bReturn = true;
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            SqlTransaction oTrans = oSqlConn.BeginTransaction();

            if (isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "relation", dtTerminalProfile, dsTables, oTrans, ref sResultDesc))
                if (isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "issuer", dtTerminalProfile, dsTables, oTrans, ref sResultDesc))
                    if (isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "acquirer", dtTerminalProfile, dsTables, oTrans, ref sResultDesc))
                        if (isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "terminal", dtTerminalProfile, dsTables, oTrans, ref sResultDesc))
                            if (isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "count", dtTerminalProfile, dsTables, oTrans, ref sResultDesc))
                            {
                                oTrans.Commit();
                                //InsertToVipotProfileTable(sTerminalID, sMaster);
                            }
                            else { oTrans.Rollback(); bReturn = false; }
                        else { oTrans.Rollback(); bReturn = false; }
                    else { oTrans.Rollback(); bReturn = false; }
                else { oTrans.Rollback(); bReturn = false; }
            else { oTrans.Rollback(); bReturn = false; }
            return bReturn;
        }

       

        protected void DeleteAcquirerList(EMS_XML.DataClass.TerminalClass terminalTemp, DataTable _dtEmsXmlMapColumn, ref DataTable _dtTerminalProfile
            , string _sVersion)
        {
            //DataRow[] rowsAcquirerXmlEms = _dtEmsXmlMapColumn.Select("EMSXMLFormId=7");
            DataRow[] rowsAcquirerXmlEms = _dtEmsXmlMapColumn.Select("EMSXMLFormId=7 and EMSxmlFilter<>'LOYALTYDBT' and  EMSxmlFilter<>'LOYALTY' ");
            string sSoftwareVersion = terminalTemp.sGetColumnValue("Software_Version");

            foreach (DataRow rowAcquirerXmlEms in rowsAcquirerXmlEms)
            {
                string sColumn = rowAcquirerXmlEms["EMSXMLColumn"].ToString();
                string sAcquirerName = rowAcquirerXmlEms["EMSXMLFilter"].ToString();
                if (sAcquirerName == "BCA") sAcquirerName = "CRONUS";
                if (sAcquirerName == "DEBIT") sAcquirerName = "DBONUS";
                //if (sAcquirerName == "LOYALTY") sAcquirerName = "CROFFUS";
                //if (sAcquirerName == "LOYALTYDBT") sAcquirerName = "DBOFFUS";
                //if (sAcquirerName == "BCA") sAcquirerName = "CROFFUS";
                //if (sAcquirerName == "DEBIT") sAcquirerName = "DBOFFUS";
                //if (sAcquirerName == "LOYALTYDBT") sAcquirerName = "DBONUS";
                //if (sAcquirerName == "LOYALTY") sAcquirerName = "CRONUS";

                string sValue = terminalTemp.sGetColumnValue(sColumn);
                //if (sColumn.ToLower() == "loyalty") sValue = "0";
                //if (sColumn.ToLower() == "debit_loyalti") sValue = "0";
                if (sValue == "0")
                {
                    #region "Delete Acquirer"
                    
                    string sValueCredit = terminalTemp.sGetColumnValue("Credit");
                    string sValueDebit = terminalTemp.sGetColumnValue("Debit");
                    //string sValueOffUs = terminalTemp.sGetColumnValue("Off_Us");
                    //if (sColumn.ToLower() != "loyalty")
                    if (sColumn.ToLower() != "credit")
                    {   //if ((sValueOffUs=="0" && sValueDebit=="1")||(sValueOffUs == "1" && sValueDebit == "0"))
                        //DeleteAcquirer("DBOFFUS", ref _dtTerminalProfile);
                        //if (sValueOffUs == "0" && sValueDebit == "0")
                        if (sValueDebit == "0")
                        {
                            DeleteAcquirer("DBONUS", ref _dtTerminalProfile);
                            DeleteAcquirer("DBOFFUS", ref _dtTerminalProfile);
                        }
                        DeleteAcquirer(sAcquirerName, ref _dtTerminalProfile);
                        switch (sAcquirerName)
                        {
                            case "FLAZZBCA":
                                UpdateTerminalTag(_sVersion == "1.00" ? "DE43" : "DE043", "0", ref _dtTerminalProfile);
                                break;
                            case "SAKUKU":
                                UpdateTerminalTag(_sVersion == "1.00" ? "DE44" : "DE044", "0", ref _dtTerminalProfile);
                                break;
                            case "CARDLESS":
                                if (sSoftwareVersion == "AVOCADO_MOVE")
                                    //UpdateTerminalTag(_sVersion == "5.00" ? "DE108" : "DE108", "0", ref _dtTerminalProfile); // add tatang
                                    UpdateTerminalTag("DE108", "0", ref _dtTerminalProfile);
                                else
                                    UpdateTerminalTag("DE106", "0", ref _dtTerminalProfile);
                                break;
                            case "QR":
                                if (sSoftwareVersion != "AVOCADO_MOVE")
                                    UpdateTerminalTag("DE114", "0", ref _dtTerminalProfile); // add tatang
                                break;
                        }
                    }
                    else
                    {
                        //if (sValueOffUs == "0")
                        //{
                            if (sValueCredit == "0")
                            {
                                //DeleteAcquirer(sAcquirerName, ref _dtTerminalProfile);
                                DeleteAcquirer("CRONUS", ref _dtTerminalProfile);
                                DeleteAcquirer("CROFFUS", ref _dtTerminalProfile);
                            }
                            else
                            {
                                //MoveIssuerCard(sAcquirerName, "BCA", ref _dtTerminalProfile);
                                MoveIssuerCard(sAcquirerName, "CRONUS", ref _dtTerminalProfile);
                                //UpdateIssuerRelation("BCA", "VISA SQ", "VISA BCA", ref _dtTerminalProfile, _sVersion);
                                UpdateIssuerRelation("CRONUS", "VISA SQ", "VISA BCA", ref _dtTerminalProfile, _sVersion);
                            }
                        //}
                        //else
                        //{
                        //    if (sValueCredit == "0" && sValueDebit == "1" && sValueOffUs == "1")
                        //    {
                        //        DeleteAcquirer("CROFFUS", ref _dtTerminalProfile);
                        //        DeleteAcquirer("CRONUS", ref _dtTerminalProfile);
                        //    }
                        //    else if (sValueCredit == "1" && sValueDebit == "0" && sValueOffUs == "1")
                        //    {
                        //        DeleteAcquirer("DBOFFUS", ref _dtTerminalProfile);
                        //        DeleteAcquirer("DBONUS", ref _dtTerminalProfile); 
                        //        //UpdateAcquirerRelation(sAcquirerName, "BCA1", "031", ref _dtTerminalProfile, _sVersion);
                        //        //UpdateAcquirerRelation(sAcquirerName, "CROFFUS", "031", ref _dtTerminalProfile, _sVersion);
                        //        //UpdateAcquirerRelation("BCA", "BCA2", "035", ref _dtTerminalProfile, _sVersion);
                        //        //UpdateIssuerRelation("BCA1", "VISA SQ", "VISA BCA", ref _dtTerminalProfile, _sVersion); //Tedie untuk Err Visa Infinite dan Visa Signature
                        //    }
                        //}

                    }
                    if (sValueCredit == "0" && sValueDebit == "0")
                    {
                        DeleteAcquirer("CROFFUS", ref _dtTerminalProfile);
                        DeleteAcquirer("CRONUS", ref _dtTerminalProfile);
                        DeleteAcquirer("DBOFFUS", ref _dtTerminalProfile);
                        DeleteAcquirer("DBONUS", ref _dtTerminalProfile);
                    }

                    #endregion
                }
                else
                {
                    //string sValueOffUs = terminalTemp.sGetColumnValue("Off_Us");
                    switch (sColumn.ToLower())
                    {
                        //case "credit":
                        //    //case "off_us":
                        //    //string sValueLoyalty = terminalTemp.sGetColumnValue("LOYALTY"); 
                        //    string sValueCredit = terminalTemp.sGetColumnValue("Credit");
                        //    //if (sValueLoyalty == "1" && sValueOffUs == "1")  
                        //    //if (sValueOffUs == "0" && sValueCredit=="1") DeleteAcquirer("CROFFUS", ref _dtTerminalProfile);
                        //    //UpdateAcquirerRelation("BCA", "BCA2", "035", ref _dtTerminalProfile, _sVersion); 
                        //    //UpdateAcquirerRelation("BCA", "BCA2", "031", ref _dtTerminalProfile, _sVersion);
                        //    //UpdateAcquirerRelation("CROFFUS", "BCA2", "031", ref _dtTerminalProfile, _sVersion);
                        //    //else if (sValueLoyalty == "1" && sValueOffUs == "0")
                        //    //{
                        //    //    //do nothing
                        //    //}
                        //    //else
                        //    //{
                        //    //    //UpdateAcquirerRelation("BCA", "BCA2", "035", ref _dtTerminalProfile, _sVersion);
                        //    //    UpdateAcquirerRelation("BCA", "BCA2", "031", ref _dtTerminalProfile, _sVersion);
                        //    //    UpdateIssuerRelation("BCA1", "VISA SQ", "VISA BCA", ref _dtTerminalProfile, _sVersion);
                        //    //}

                        //    break;
                        case "flazz":
                            UpdateTerminalTag(_sVersion == "1.00" ? "DE43" : "DE043", "1", ref _dtTerminalProfile);
                            break;
                        //case "debit":
                        //    //string sValueLoyaltyDebit = terminalTemp.sGetColumnValue("debit_loyalti");
                        //    //if (sValueLoyaltyDebit != "1")                            
                        //    string sValueDebit = terminalTemp.sGetColumnValue("Debit");
                        //    if (sValueOffUs == "0" && sValueDebit == "1" )
                        //        DeleteAcquirer("DBOFFUS", ref _dtTerminalProfile);

                        //    //    UpdateAcquirerRelation("DBONUS", "DEBIT", "", ref _dtTerminalProfile, _sVersion);
                        //    break;
                        case "sakuku":
                            //UpdateTerminalTag("DE044", "1", ref _dtTerminalProfile);
                            UpdateTerminalTag("DE044", sValue, ref _dtTerminalProfile); // tatang revisi dari "1" menjadi svalue sesuai kiriman ems
                            break;
                        case "cardless":
                            //UpdateTerminalTag("DE106", "1", ref _dtTerminalProfile);
                            // add tatang
                            if (sSoftwareVersion == "AVOCADO_MOVE")
                             UpdateTerminalTag("DE108", "1", ref _dtTerminalProfile); 
                            else if (sSoftwareVersion == "AVOCADO")
                                UpdateTerminalTag("DE106", "1", ref _dtTerminalProfile);
                            break;
                        case "qrbca":
                                if (sSoftwareVersion != "AVOCADO_MOVE")
                                UpdateTerminalTag("DE114", "1", ref _dtTerminalProfile); // tatang
                            break;
                            //case "va":
                            //    UpdateTerminalTag("DE108", sValue, ref _dtTerminalProfile); // tatang revisi dari "1" menjadi svalue sesuai kiriman ems
                            //    break;
                            //case "mpn":
                            //    UpdateTerminalTag("DE110", sValue, ref _dtTerminalProfile); // tatang revisi dari "1" menjadi svalue sesuai kiriman ems
                            //    break;
                    }
                }
            }
            _dtTerminalProfile.AcceptChanges();
        }

        protected void UpdateIssuerRelation(string sAcquirerNameNew, string sIssuerNameOld, string sIssuerNameNew
            , ref DataTable _dtTerminalProfile, string _sVersion)
        {
            //DataRow[] rowsIssRelation = _dtTerminalProfile.Select(string.Format("TagValue='{0}' AND Tag='AD02'", sIssuerNameOld));
            DataRow[] rowsIssRelation = _dtTerminalProfile.Select(string.Format("TagValue='{0}' AND Tag='{1}'", sIssuerNameOld, _sVersion == "1.00" ? "AD02" : "AD002"));
            if (rowsIssRelation.Count() > 0)
            {
                string sIdIssuer = null;
                foreach (DataRow rowIssuer in rowsIssRelation)
                {
                    if (string.IsNullOrEmpty(sIdIssuer))
                        sIdIssuer = rowIssuer["Id"].ToString();
                    else
                        sIdIssuer = string.Format("{0},{1}", sIdIssuer, rowIssuer["Id"]);
                    rowIssuer["TagValue"] = sIssuerNameNew;
                }
                //DataRow[] rowsAcquirerRelation = _dtTerminalProfile.Select(string.Format("Tag='AD03' AND Id-1 IN ({0})", sIdIssuer));
                DataRow[] rowsAcquirerRelation = _dtTerminalProfile.Select(string.Format("Tag='{1}' AND Id-1 IN ({0})", sIdIssuer
                    , _sVersion == "1.00" ? "AD03" : "AD003"));
                if (rowsAcquirerRelation.Count() > 0)
                    foreach (DataRow rowAcqRelation in rowsAcquirerRelation)
                        rowAcqRelation["TagValue"] = sAcquirerNameNew;
            }
            //add tatang for remove profile issuer old 
            DataRow[] rowsIssProfileDelete = _dtTerminalProfile.Select(string.Format("Name='{0}' AND Tag like '{1}'", sIssuerNameOld, "AE%"));
            if (rowsIssProfileDelete.Count() > 0)
            {
                foreach (DataRow rowIssProfileDelete in rowsIssProfileDelete)
                    rowIssProfileDelete.Delete();
            }
            //
        }

        protected void UpdateAcquirerRelation(string sAcquirerNameSource, string sAcquirerNameTarget, string sHostID
            , ref DataTable _dtTerminalProfile, string _sVersion)
        {
            DataRow[] rowsAcq = _dtTerminalProfile.Select(string.Format("Name='{0}' AND Tag LIKE 'AA%'", sAcquirerNameSource));
            if (rowsAcq.Count() > 0)
            {
                DataRow[] rowsAcqRelation = _dtTerminalProfile.Select(string.Format("TagValue='{0}' AND Tag='{1}'", sAcquirerNameSource
                    , _sVersion == "1.00" ? "AD03" : "AD003"));
                foreach (DataRow rowAcqRelation in rowsAcqRelation)
                {
                    rowAcqRelation["TagValue"] = sAcquirerNameTarget;
                }
                if (!string.IsNullOrEmpty(sHostID))
                    foreach (DataRow rowAcq in rowsAcq)
                    {
                        rowAcq["Name"] = sAcquirerNameTarget;
                        if (rowAcq["Tag"].ToString() == "AA01" || rowAcq["Tag"].ToString() == "AA001")
                            rowAcq["TagValue"] = sAcquirerNameTarget;
                        if (rowAcq["Tag"].ToString() == "AA02" || rowAcq["Tag"].ToString() == "AA002")
                            rowAcq["TagValue"] = sHostID;
                    }
            }
        }

        protected void MoveIssuerCard(string sAcquirerNameSource, string sAcquirerNameTarget, ref DataTable _dtTerminalProfile)
        {
            DataRow[] rowsAcq = _dtTerminalProfile.Select(string.Format("Name='{0}' AND Tag LIKE 'AA%'", sAcquirerNameSource));
            if (rowsAcq.Count() > 0)
            {
                DataRow[] rowsAcqRelation = _dtTerminalProfile.Select(string.Format("TagValue='{0}' AND Tag IN ('AD03','AD003')", sAcquirerNameSource));
                foreach (DataRow rowAcqRelation in rowsAcqRelation)
                {
                    rowAcqRelation["TagValue"] = sAcquirerNameTarget;
                }
                foreach (DataRow rowAcq in rowsAcq)
                    rowAcq.Delete();
            }
        }

        protected void DeleteAcquirer(string sAcquirerName, ref DataTable _dtTerminalProfile)
        {
            DataRow[] rowsAcq = _dtTerminalProfile.Select(string.Format("Name='{0}' AND Tag LIKE 'AA%'", sAcquirerName));
            if (rowsAcq.Count() > 0)
            {
                List<DataRow> listAcqRelation = new List<DataRow>();
                listAcqRelation.AddRange(_dtTerminalProfile.Select(string.Format("TagValue='{0}' AND Tag IN ('AD03','AD003')", sAcquirerName)));

                string sIdAcqRelationList = null;
                foreach (DataRow rowAcqRelation in listAcqRelation)
                    if (string.IsNullOrEmpty(sIdAcqRelationList))
                        sIdAcqRelationList = rowAcqRelation["Id"].ToString();
                    else
                        sIdAcqRelationList = string.Format("{0},{1}", sIdAcqRelationList, rowAcqRelation["Id"]);

                List<DataRow> listIssRelation = new List<DataRow>();
                List<DataRow> listIssuer = new List<DataRow>();
                List<DataRow> listCardRelation = new List<DataRow>();

                if (!string.IsNullOrEmpty(sIdAcqRelationList))
                {
                    listIssRelation.AddRange(_dtTerminalProfile.Select(string.Format("Id+1 IN ({0})", sIdAcqRelationList)));

                    string sIssuerList = null;
                    foreach (DataRow rowIssRelation in listIssRelation)
                        if (!string.IsNullOrEmpty(rowIssRelation["TagValue"].ToString()))
                            if (string.IsNullOrEmpty(sIssuerList))
                                sIssuerList = string.Format("'{0}'", rowIssRelation["TagValue"]);
                            else
                                sIssuerList = string.Format("{0},'{1}'", sIssuerList, rowIssRelation["TagValue"]);
                    if (!string.IsNullOrEmpty(sIssuerList))
                        listIssuer.AddRange(_dtTerminalProfile.Select(string.Format("Name IN ({0}) AND Tag LIKE 'AE%'", sIssuerList)));
                    listCardRelation.AddRange(_dtTerminalProfile.Select(string.Format("Id+2 IN ({0})", sIdAcqRelationList)));
                }
                IEnumerable<DataRow> rowsTemp = listIssuer == null ? rowsAcq : rowsAcq.Union(listIssuer);
                rowsTemp = rowsTemp.Union(listAcqRelation);
                rowsTemp = rowsTemp.Union(listIssRelation);
                rowsTemp = rowsTemp.Union(listCardRelation);

                foreach (DataRow rowDelete in rowsTemp)
                    rowDelete.Delete();
            }
        }

        protected DataSet dsInitTable(int _iDatabaseId)
        {
            DataSet dsTables = new DataSet();
            string sQuery = string.Format("exec spItemListBrowse 'WHERE DatabaseID={0}'\n{1}\n{2}\n{3}\n{4}\n{5}"
                , _iDatabaseId
                , "SELECT * FROM tbProfileTerminalList WHERE TerminalID = null"
                , "SELECT TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue FROM tbProfileTerminal WHERE TerminalID = null"
                , "SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = null"
                , "SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue FROM tbProfileIssuer WHERE TerminalID = null"
                , "SELECT TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue FROM tbProfileRelation WHERE TerminalID = null");

            // FOR SHRINK
            //, "SELECT '12345678'AS TerminalID,'12345' AS TerminalTag,0 AS TerminalLengthOfTagLength,0 AS TerminalTagLength,'111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111'AS TerminalTagValue FROM tbProfileTerminal WHERE TerminalID = null"
            //, "SELECT '12345678'AS TerminalID,'12345678012345'AS AcquirerName,'12345' AS AcquirerTag,0 AS AcquirerLengthOfTagLength,0 AS AcquirerTagLength,'111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111'AS AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = null"
            //, "SELECT '12345678'AS TerminalID,'12345678012345'AS IssuerName,'12345' AS IssuerTag,0 AS IssuerLengthOfTagLength,0 AS IssuerTagLength,'111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111'AS IssuerTagValue FROM tbProfileIssuer WHERE TerminalID = null"
            //, "SELECT TerminalID,TagCard,ValueCard,TagIssuer,ValueIssuer,TagAcquirer,ValueAcquirer FROM dbo.tbProfileRelation WHERE TerminalID = null");
            //string sQuery = string.Format("exec spItemListBrowse 'WHERE DatabaseID={0}'"
            //, _iDatabaseId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                SqlDataAdapter oAdapt = new SqlDataAdapter(oCmd);
                oAdapt.Fill(dsTables);
            }
            return dsTables;
        }

        protected bool isBulkImportProfileSuccess(string _sTerminalID, int iDatabaseId, string sTableType,
            DataTable _dtProfileTLV, DataSet _dsTables,
            SqlTransaction _oTrans,
            ref string sError)
        {
            string sTagHeader = "DE";
            int iTableID = 2;
            string sDestTableName = "tbProfileTerminal";
            switch (sTableType.ToLower())
            {
                case "terminal":
                    sTagHeader = "DE";
                    iTableID = 2;
                    sDestTableName = "tbProfileTerminal";
                    break;
                case "acquirer":
                    sTagHeader = "AA";
                    iTableID = 3;
                    sDestTableName = "tbProfileAcquirer";
                    break;
                case "issuer":
                    sTagHeader = "AE";
                    iTableID = 4;
                    sDestTableName = "tbProfileIssuer";
                    break;
                case "relation":
                    sTagHeader = "AD";
                    iTableID = 5;
                    sDestTableName = "tbProfileRelation";
                    break;
                default:
                    sTagHeader = "DC";
                    iTableID = 2;
                    sDestTableName = "tbProfileTerminal";
                    break;
            }

            DataRow[] arrRowsProfileTLV = _dtProfileTLV.Select(string.Format("Tag LIKE '{0}%'", sTagHeader), "Id");
            if (arrRowsProfileTLV.Length > 0)
            {
                DataTable dtFilteredProfileTLV = arrRowsProfileTLV.CopyToDataTable();
                if (dtFilteredProfileTLV.Rows.Count > 0)
                {
                    DataTable dtProfile = _dsTables.Tables[iTableID];
                    dtProfile.Rows.Clear();

                    foreach (DataRow rowFilteredProfile in dtFilteredProfileTLV.Rows)
                    {
                        DataRow rowProfile = dtProfile.NewRow();
                        rowProfile[0] = _sTerminalID;
                        rowProfile[1] = sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation" ?
                            rowFilteredProfile["Name"] : rowFilteredProfile["Tag"];
                        rowProfile[2] = sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation" ?
                            rowFilteredProfile["Tag"] : rowFilteredProfile["TagLength"].ToString().Length;
                        rowProfile[3] = sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation" ?
                            rowFilteredProfile["TagLength"].ToString().Length : rowFilteredProfile["TagLength"];
                        rowProfile[4] = sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation" ?
                            rowFilteredProfile["TagLength"] : rowFilteredProfile["TagValue"];
                        if (sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation")
                            rowProfile[5] = rowFilteredProfile["TagValue"];
                        dtProfile.Rows.Add(rowProfile);
                    }

                    try
                    {
                        
                        // for shrink
                        //if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                        //if (sDestTableName == "tbProfileRelation")
                        //    dtProfile = PivotRelation(dtProfile, _oTrans);
                        // for shrink

                        SqlBulkCopy sqlBulkProfile = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.Default, _oTrans);
                        sqlBulkProfile.DestinationTableName = sDestTableName;                


                        foreach (DataColumn col in dtProfile.Columns)
                        {
                            if (dtProfile.Columns.Contains(col.ColumnName))
                                sqlBulkProfile.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                        }
                        sqlBulkProfile.WriteToServer(dtProfile);


                        if (sTableType.ToLower() == "terminal")
                        {
                            DataTable dtProfileTerminalList = _dsTables.Tables[1];
                            dtProfileTerminalList.Rows.Clear();
                            DataRow rowTerminalList = dtProfileTerminalList.NewRow();
                            rowTerminalList["TerminalID"] = _sTerminalID;
                            rowTerminalList["DatabaseID"] = iDatabaseId;
                            rowTerminalList["AllowDownload"] = 1;
                            rowTerminalList["StatusMaster"] = 0;
                            rowTerminalList["InitCompress"] = 1;
                            rowTerminalList["MaxInitRetry"] = 3;
                            if (dtProfileTerminalList.Columns.Contains("EMVInitMangement"))
                                rowTerminalList["EMVInitMangement"] = 0;
                            else if (dtProfileTerminalList.Columns.Contains("EMVInitManagement"))
                                rowTerminalList["EMVInitManagement"] = 0;
                            dtProfileTerminalList.Rows.Add(rowTerminalList);

                            SqlBulkCopy sqlBulkTerminalList = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.Default, _oTrans);
                            sqlBulkTerminalList.DestinationTableName = "tbProfileTerminalList";
                            foreach (DataColumn col in dtProfileTerminalList.Columns)
                            {
                                if (dtProfileTerminalList.Columns.Contains(col.ColumnName))
                                    sqlBulkTerminalList.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }
                            sqlBulkTerminalList.WriteToServer(dtProfileTerminalList);
                            // add log jika ingin dibuat log allowdownload //190201
                            string sFileTrace = _sTerminalID + ".log";
                            Trace.Write(sPath, sFileTrace, string.Format("[{0}], {1} AllowDownload 1 EMS", Trace.sFullTime(), _sTerminalID));
                            //
                        }
                        return true;
                    }
                    catch (SqlException exsql)
                    {
                        sError = string.Format("Tag {1} SQL_EX :{0}", exsql.Message, sTagHeader);
                        return false;
                    }
                    catch (Exception ex)
                    {
                        sError = string.Format("Tag {1} EX :{0}", ex.Message, sTagHeader);
                        return false;
                    }
                }
            }
            return true;
        }

        //protected void EmsUpdateHostIdPromo(EMS_XML.DataClass.TerminalClass _terminalTemp, string _sTerminalID)
        //{
        //    string sLoyalty = _terminalTemp.sGetColumnValue("Loyalty");            
        //    if (int.Parse(sLoyalty) == 0)
        //    {
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO A", "AA02", "130");
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO B", "AA02", "131");
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO C", "AA02", "132");
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO D", "AA02", "133");
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO E", "AA02", "134");
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO SQ A", "AA02", "135");
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO SQ B", "AA02", "136");
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO SQ C", "AA02", "137");
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO SQ D", "AA02", "138");
        //        ExecuteAcquirerUpdate(_sTerminalID, "PROMO SQ E", "AA02", "139");

        //        ExecuteTerminalUpdate(_sTerminalID, "DE20", "0");
        //    }
        //}

        protected void UpdateHostIdPromo(EMS_XML.DataClass.TerminalClass _terminalTemp, ref DataTable _dtTerminalProfile)
        {
            string sValueLoyalty = _terminalTemp.sGetColumnValue("Loyalty");
            //string sValueLoyaltyDbt = _terminalTemp.sGetColumnValue("Debit_Loyalti");
            string sValuePromoReg = _terminalTemp.sGetColumnValue("Cicilan_Reguler");
            string sValuePromo = _terminalTemp.sGetColumnValue("Cicilan_Promo");
            string sValueCredit = _terminalTemp.sGetColumnValue("Credit");

            //if (sValuePromo == "1" || sValuePromo == "1")
            if (sValueLoyalty == "0")
            {
                if (_dtTerminalProfile.Select("Name='PROMO A' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO A' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "130";
                if (_dtTerminalProfile.Select("Name='PROMO B' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO B' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "131";
                if (_dtTerminalProfile.Select("Name='PROMO C' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO C' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "132";
                if (_dtTerminalProfile.Select("Name='PROMO D' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO D' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "133";
                if (_dtTerminalProfile.Select("Name='PROMO E' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO E' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "134";
                if (_dtTerminalProfile.Select("Name='PROMO F' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO F' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "135";
                if (_dtTerminalProfile.Select("Name='PROMO G' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO G' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "136";
                if (_dtTerminalProfile.Select("Name='PROMO H' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO H' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "137";
                if (_dtTerminalProfile.Select("Name='PROMO I' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO I' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "138";
                if (_dtTerminalProfile.Select("Name='PROMO J' AND (Tag='AA02' OR Tag='AA002')").Count() > 0)
                    _dtTerminalProfile.Select("Name='PROMO J' AND (Tag='AA02' OR Tag='AA002')")[0]["TagValue"] = "139";
                //_dtTerminalProfile.Select("Tag='DE20' OR Tag='DE020'")[0]["TagValue"] = "0";
                _dtTerminalProfile.AcceptChanges();
            }
        }

        //protected void UpdateAcquirerPromo(ref DataTable _dtTerminalProfile, string sOldPromoName, string sNewPromoName)
        //{
        //    foreach (DataRow row in _dtTerminalProfile.Select(string.Format("Name='{0}'", sOldPromoName)))
        //    {
        //        row["Name"] = sNewPromoName;
        //        //if (row["Tag"].ToString() == "AA01") row["TagValue"] = sNewPromoName;
        //        if (row["Tag"].ToString() == "AA01" || row["Tag"].ToString() == "AA001") row["TagValue"] = sNewPromoName;
        //    }

        //    //foreach (DataRow row in _dtTerminalProfile.Select(string.Format("Tag='AD03' AND TagValue='{0}'", sOldPromoName)))
        //    foreach (DataRow row in _dtTerminalProfile.Select(string.Format("(Tag='AD03' OR Tag='AD003') AND TagValue='{0}'", sOldPromoName)))
        //        row["TagValue"] = sNewPromoName;
        //    _dtTerminalProfile.AcceptChanges();
        //}

        //protected void ExecuteTerminalUpdate(string _sTerminalID, string sTag, string sValue)
        //{
        //    using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPProfileTerminalUpdateTag, oSqlConn))
        //    {
        //        if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
        //        oCmd.CommandType = CommandType.StoredProcedure;
        //        oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
        //        oCmd.Parameters.Add("@sTag", SqlDbType.VarChar).Value = sTag;
        //        oCmd.Parameters.Add("@sValue", SqlDbType.VarChar).Value = sValue;

        //        oCmd.ExecuteNonQuery();
        //    }
        //}

        //protected void ExecuteAcquirerUpdate(string _sTerminalID, string sAcqName, string sTag, string sValue)
        //{
        //    using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPProfileAcquirerUpdateTag, oSqlConn))
        //    {
        //        if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
        //        oCmd.CommandType = CommandType.StoredProcedure;
        //        oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
        //        oCmd.Parameters.Add("@sAcqName", SqlDbType.VarChar).Value = sAcqName;
        //        oCmd.Parameters.Add("@sTag", SqlDbType.VarChar).Value = sTag;
        //        oCmd.Parameters.Add("@sValue", SqlDbType.VarChar).Value = sValue;

        //        oCmd.ExecuteNonQuery();
        //    }
        //}

        public EMS_XML emsDelete()
        {
            EMS_XML oResult = new EMS_XML();
            oResult.Attribute = oEms.Attribute;
            oResult.Attribute.sTypes = EMSCommonClass.XMLResponsType.RESULT.ToString();

            string sTerminalID = "";
            string sResultCode = "000";
            string sResultDesc = "SUCCESS";
            string sVersion = oEms.Attribute.sVersions;
            string sTableName = oEms.Attribute.sSenderID;

            foreach (EMS_XML.DataClass.TerminalClass terminalTemp in oEms.Data.ltTerminal)
            {
                sTerminalID = terminalTemp.sGetColumnValue("Terminal_INIT");
                string sDatabaseName = sGetDatabaseNameByTerminalID(sTerminalID);
                EMSCommonClass.XMLProcessType oType = EMSCommonClass.XMLProcessType.DELETE;
                try
                {
                    if (IsTerminalExist(sTerminalID))
                    {
                        DeleteTerminal(sTerminalID);
                    }
                    else
                    {
                        sResultCode = "001";
                        sResultDesc = EMSCommon.XML.sErrorCommon + "Terminal_INIT does not exist";
                    }
                }
                catch (Exception ex)
                {
                    sResultCode = "001";
                    sResultDesc = string.Format("{3}.{0} DELETE Terminal {1} From EMS XML. ErrorMsg : {2}", EMSCommon.XML.sErrorCommon
                            , sTerminalID, ex.Message, ex.StackTrace);
                    Error = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                }
                InsertLog(sTerminalID, sResultDesc, sResultCode, sDatabaseName);
                oResult.Data.doAddTerminal(tcGenResult(sTerminalID, sResultDesc, sResultCode, oType));
            }
            return oResult;
        }

        public EMS_XML emsInquiry()
        {
            EMS_XML oResult = new EMS_XML();
            oResult.Attribute = oEms.Attribute;

            string sVersion = oEms.Attribute.sVersions;
            string sTableName = oEms.Attribute.sSenderID;
            string sType = oEms.Attribute.sTypes;

            DataTable dtEmsXmlMapColumn = dtGetEmsXmlMap(sVersion,
                oResult.Attribute.sTypes == EMSCommonClass.XMLRequestType.INQ.ToString() ? false : true);

            return emsInquiry(dtEmsXmlMapColumn, ref Error);
        }

        public EMS_XML emsInquiry(DataTable _dtEmsXmlMapColumn, ref string sError)
        {
            EMS_XML oResult = new EMS_XML();
            oResult.Attribute = oEms.Attribute;

            string sTerminalID = "";
            string sResultCode = "000";
            string sResultDesc = "SUCCESS";
            string sVersion = oEms.Attribute.sVersions;
            string sTableName = oEms.Attribute.sSenderID;
            string sType = oEms.Attribute.sTypes;
            oResult.Attribute.sTypes = sType.ToLower() == EMSCommonClass.XMLRequestType.INQ.ToString().ToLower() ?
                EMSCommonClass.XMLResponsType.RESULTINQ.ToString() : EMSCommonClass.XMLResponsType.RESULTINQINIT.ToString();

            DataTable dtEmsXml = _dtEmsXmlMapColumn;

            foreach (EMS_XML.DataClass.FilterClass filterTemp in oEms.Data.ltFilter)
            {
                string sFilterName = filterTemp.sFilterFieldName;
                string sFilterValue = filterTemp.sValues;
                if (sFilterName.ToLower() == "Terminal_INIT".ToLower())
                {
                    sTerminalID = sFilterValue;

                    EMS_XML.DataClass.TerminalClass tcTerminalResult = new EMS_XML.DataClass.TerminalClass();
                    string sDatabaseName = "";
                    try
                    {
                        if (IsTerminalExist(sTerminalID))
                        {
                            sDatabaseName = sGetDatabaseNameByTerminalID(sTerminalID);
                            tcTerminalResult.AddColumnValue("Software_Version", sDatabaseName);

                            DataTable dtTerminal = dtGetProfileFullTable(sTerminalID);
                            sError = "1";
                            foreach (DataRow rowEms in dtEmsXml.Rows)
                            {
                                string sColumn = rowEms["EMSXMLColumn"].ToString();
                                string sFilter = rowEms["EMSXMLFilter"].ToString(); 
                                string sTag = rowEms["EMSXMLTag"].ToString();
                                //if (sTag == "AA021") { 
                                //    //string tag = sTag; 
                                //}
                                if (sColumn == "Installment_Description_PROMOSQ_I")
                                {
                                    string scol = sColumn;
                                }
                                try
                                {
                                    bool bAdvance = bool.Parse(rowEms["EMSXMLIsComplicated"].ToString());
                                    sError = "2";
                                    string sValue = "";
                                    switch (sColumn.ToLower())
                                    {
                                        case "tunai":
                                            sError = string.Format("{0}.{1}", sError, sColumn);
                                            sValue = sGetValue(sTerminalID, dtTerminal, "AE020", "DEBIT BCA", false) == "0" ? "1" : "0";
                                            break;
                                        case "off_us":
                                            sError = string.Format("{0}.{1}", sError, sColumn);
                                            sValue = sGetValue(sTerminalID, dtTerminal, "AA005", "BCA1", true) == "1" ? "1" : "0";
                                            if (sValue == "0")
                                            {
                                                sValue = sGetValue(sTerminalID, dtTerminal, "AA005", "BCA2", true);
                                            }
                                            break;
                                        case "off_line_amex":
                                        case "manual_key_in_amex":
                                        case "card_ver_amex":
                                        case "fare_non_fare_amex":
                                        case "tips_amex":
                                        case "off_line_diners":
                                        case "manual_key_in_diners":
                                        case "card_ver_diners":
                                        case "fare_non_fare_diners":
                                        case "tips_diners":
                                        case "reward_debit_poin":
                                        case "adjustment_amex":
                                        case "Installment_Description_PROMOSQ_I":
                                            sError = string.Format("{0}.{1}", sError, sColumn);
                                            sValue = "0";
                                            break;
                                        //case "force_settlementday":
                                        //    sValue = sGetValue(sTerminalID, dtTerminal, sTag, sFilter, bAdvance);
                                        //    break;
                                        case "credit":
                                            sError = string.Format("{0}.{1}", sError, sColumn);
                                            sValue = sGetValue(sTerminalID, dtTerminal, sTag, sFilter, bAdvance);
                                            if (sValue == "0")
                                            {
                                                sValue = sGetValue(sTerminalID, dtTerminal, sTag, "LOYALTY", bAdvance);
                                                if (sValue == "0") sValue = sGetValue(sTerminalID, dtTerminal, sTag, "BCA1", bAdvance);
                                            }
                                            break;
                                        case "minimumtopup":
                                            sError = string.Format("{0}.{1}", sError, sColumn);
                                            sValue = sGetValue(sTerminalID, dtTerminal, sTag, sFilter, bAdvance);
                                            sValue = string.IsNullOrEmpty(sValue) ? "0" : string.Format("{0}", int.Parse(sValue));
                                            break;
                                        case "allowfinansialoffus_cupcard":
                                        case "allowfinansialonus_cupcard":
                                            sError = string.Format("{0}.{1}", sError, sColumn);
                                            string sValueCreditCUP = sGetValue(sTerminalID, dtTerminal, sTag, "UNIONPAY CREDIT", bAdvance);
                                            string sValueDebitCUP = sGetValue(sTerminalID, dtTerminal, sTag, "UNIONPAY DEBIT", bAdvance);
                                            if (sValueCreditCUP == "1" && sValueDebitCUP == "1") sValue = "1";
                                            else sValue = "0";
                                            break;
                                        default:
                                            sError = string.Format("{0}.{1}", sError, sColumn);
                                            if (tcTerminalResult.sGetColumnValue("Off_Us") == "1" && rowEms["EMSXMLFormId"].ToString() == "2"
                                                 && rowEms["EMSXMLFilter"].ToString() == "BCA")
                                                sFilter = "BCA1";
                                            sValue = sGetValue(sTerminalID, dtTerminal, sTag, sFilter, bAdvance);
                                            break;
                                    }
                                    tcTerminalResult.AddColumnValue(sColumn, sValue);
                                }
                                catch (Exception ex)
                                {
                                    sResultCode = "001";
                                    sResultDesc = string.Format("{0} INQUIRY Terminal {1} From EMS XML. ErrorMsg : {2}. Tag '{3}'", EMSCommon.XML.sErrorCommon
                                            , sTerminalID, ex.StackTrace.Substring(0, 450), sTag);
                                    sError = string.Format("TIDAK BERHASIL, {0} : {2} {1}", sTableName, sResultDesc, sColumn);
                                    Error = string.Format("TIDAK BERHASIL, {0} : {2} {1}", sTableName, sResultDesc, sColumn);
                                }
                            }
                        }
                        else
                        {
                            sResultCode = "001";
                            sResultDesc = EMSCommon.XML.sErrorCommon + "Terminal_INIT does not exist";
                            Error = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                            sError = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                        }
                    }
                    catch (SqlException sqlex)
                    {
                        sResultCode = "002";
                        sResultDesc = string.Format("{0} INQUIRY Terminal {1} From EMS XML. ErrorMsg : {2}", EMSCommon.XML.sErrorCommon
                                , sTerminalID, sqlex.StackTrace.Substring(0, 450));
                        Error = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                        sError = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                    }
                    catch (Exception ex)
                    {
                        sResultCode = "001";
                        sResultDesc = string.Format("{0} INQUIRY Terminal {1} From EMS XML. ErrorMsg : {2}", EMSCommon.XML.sErrorCommon
                                , sTerminalID, ex.StackTrace.Substring(0, 450));
                        Error = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                        sError = string.Format("TIDAK BERHASIL, {0} : {1}", sTableName, sResultDesc);
                    }
                    InsertLog(sTerminalID, sResultDesc, sResultCode, sDatabaseName);
                    oResult.Data.doAddTerminal(tcTerminalResult);
                }
            }
            return oResult;
        }

        protected bool IsMasterEmsValid(ref string sError, string sMaster)
        {
            bool bValid = true;
            if (sMaster.Length == 8)
            {
                using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPXMLMsProfileBrowse, oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value =
                        string.Format("WHERE EmsXmlProfile='{0}'", sMaster);

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    using (SqlDataReader read = oCmd.ExecuteReader())
                        if (!read.HasRows)
                        {
                            bValid = false;
                            sError = EMSCommon.XML.sErrorMaster;
                        }
                }
            }
            else
                sError = EMSCommon.XML.sErrorMasterLength;
            return bValid;
        }

        protected bool IsTerminalExist(string _sTerminalId)
        {
            bool bReturn = true;
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPTerminalListBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value =
                    string.Format("WHERE TerminalID='{0}'", _sTerminalId);

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                using (SqlDataReader read = oCmd.ExecuteReader())
                    if (!read.HasRows)
                        bReturn = false;
            }
            return bReturn;
        }

        protected int iGetDatabaseIdBySoftName(string sSoftwareVersion)
        {
            int iValue = 0;
            using (SqlCommand oCmd = new SqlCommand(
                //string.Format("SELECT dbo.iDatabaseIdBySoftName('{0}')", sSoftwareVersion), oSqlConn))
                string.Format("SELECT DatabaseID FROM tbProfileTerminalDB Where DatabaseName ='{0}'", sSoftwareVersion), oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlDataReader read = oCmd.ExecuteReader())
                    if (read.HasRows)
                        if (read.Read())
                            iValue = int.Parse(read[0].ToString());
            }
            return iValue;
        }
        
        protected DataTable PivotRelation(DataTable dtTemp,SqlTransaction sqlTrans)
        {
            string sterminal="";
            DataTable dtRelation = new DataTable();
            dtRelation.Columns.Add("terminalid");
            dtRelation.Columns.Add("TagCard");
            dtRelation.Columns.Add("ValueCard");
            dtRelation.Columns.Add("TagIssuer");
            dtRelation.Columns.Add("ValueIssuer");
            dtRelation.Columns.Add("TagAcquirer");
            dtRelation.Columns.Add("ValueAcquirer");
            dtRelation.TableName = "tbrelation";
            
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();

                foreach (DataRow row in dtTemp.Rows)
                {
                    using (SqlCommand oCmd = new SqlCommand("spProfileRelationPivotInsert", oSqlConn, sqlTrans))
                    {
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.Parameters.Add("@terminalid", SqlDbType.VarChar).Value = row[0].ToString();
                        oCmd.Parameters.Add("@relationtag", SqlDbType.VarChar).Value = row[1].ToString();
                        oCmd.Parameters.Add("@relationlengthoftaglength", SqlDbType.Int).Value = int.Parse(row[2].ToString());
                        oCmd.Parameters.Add("@relationtaglength", SqlDbType.Int).Value = int.Parse(row[3].ToString());
                        oCmd.Parameters.Add("@relationtagvalue", SqlDbType.VarChar).Value = row[4].ToString();
                        oCmd.ExecuteNonQuery();
                    }
                    //dtRelation.Rows.Add(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString());
                    sterminal = row[0].ToString();
                   
                }

            SqlCommand oCmd2 = new SqlCommand("spProfileRelationPivot", oSqlConn, sqlTrans);
            oCmd2.CommandType = CommandType.StoredProcedure;
            oCmd2.Parameters.Add("@terminalid", SqlDbType.VarChar).Value = sterminal;
            oCmd2.ExecuteNonQuery();
            new SqlDataAdapter(oCmd2).Fill(dtRelation);

            //if (dtRelation.Rows.Count != 0) //DeleteTerminalRelation(sterminal.ToString());
            return dtRelation;
        }

        protected void DeleteTerminalRelation(string _sTerminalId)
        {
            string sQuery = string.Format("Delete tbProfileRelationTempToEms where TerminalId= '{0}'", _sTerminalId);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalId;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }
        protected string sGetMasterProfile(string sVersion, int iDatabaseId, string sMaster)
        {
            string sMasterProfile = null;
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPXMLMsProfileBrowseByXmlProfile, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sVersion", SqlDbType.VarChar).Value = sVersion;
                oCmd.Parameters.Add("@sXmlProfile", SqlDbType.VarChar).Value = sMaster;
                oCmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = iDatabaseId;
                oCmd.Parameters.Add("@sMaster", SqlDbType.VarChar, 255).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();
                sMasterProfile = oCmd.Parameters["@sMaster"].Value.ToString();
            }
            return sMasterProfile;
        }

        protected void DeleteTerminal(string _sTerminalId)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPProfileTerminalDeleteProcess, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalId;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }

        //protected void CopyProfile(int iDatabaseId, string sOldTerminalId, string sNewTerminalID)
        //{
        //    using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPProfileTerminalCopyProcess, oSqlConn))
        //    {
        //        if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
        //        oCmd.CommandType = CommandType.StoredProcedure;
        //        oCmd.Parameters.Add("@iDatabaseId", SqlDbType.Int).Value = iDatabaseId;
        //        oCmd.Parameters.Add("@sOldTID", SqlDbType.VarChar).Value = sOldTerminalId;
        //        oCmd.Parameters.Add("@sNewTID", SqlDbType.VarChar).Value = sNewTerminalID;

        //        oCmd.ExecuteNonQuery();
        //    }
        //}

        //protected void EmsRunProcedure(string sTableName, string sVersion, string sTerminalInit, string sProcedure)
        //{
        //    using (SqlCommand oCmd = new SqlCommand(sProcedure, oSqlConn))
        //    {
        //        if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
        //        oCmd.CommandType = CommandType.StoredProcedure;
        //        oCmd.Parameters.Add("@sTableName", SqlDbType.VarChar).Value = sTableName;
        //        oCmd.Parameters.Add("@sVersion", SqlDbType.VarChar).Value = sVersion;
        //        oCmd.Parameters.Add("@sTerminalInit", SqlDbType.VarChar).Value = sTerminalInit;

        //        oCmd.ExecuteNonQuery();
        //    }
        //}

        protected void TerminalRunProcedure(string sTerminalID, string sProcedure)
        {
            using (SqlCommand oCmd = new SqlCommand(sProcedure, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;

                oCmd.ExecuteNonQuery();
            }
        }

        //protected void TerminalMasterAdd(string sMasterProfile, string sTerminalID)
        //{
        //    using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPProfileTerminalMasterAdd, oSqlConn))
        //    {
        //        if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
        //        oCmd.CommandType = CommandType.StoredProcedure;
        //        oCmd.Parameters.Add("@sOldTID", SqlDbType.VarChar).Value = sMasterProfile;
        //        oCmd.Parameters.Add("@sNewTID", SqlDbType.VarChar).Value = sTerminalID;

        //        oCmd.ExecuteNonQuery();
        //    }
        //}

        protected void InsertLog(string sTerminalID, string sResultDesc, string sResultCode, string sDatabaseName)
        {
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPAuditTrailInsert, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = EMSCommonClass.sUserID;
                if (sResultCode == "000")
                    oCmd.Parameters.Add("@sDatabaseName", SqlDbType.VarChar).Value = sDatabaseName;
                else
                    oCmd.Parameters.Add("@sDatabaseName", SqlDbType.VarChar).Value = "";
                oCmd.Parameters.Add("@sActionDesc", SqlDbType.VarChar).Value = sResultDesc;
                oCmd.ExecuteNonQuery();
            }
        }

        protected string sGetDatabaseNameByTerminalID(string sTerminalID)
        {
            //string sQuery = string.Format("SELECT dbo.sDatabaseNameByTerminalId('{0}')", sTerminalID);
            string sQuery = string.Format("SELECT A.DatabaseName FROM tbProfileTerminalDB A WITH(NOLOCK) JOIN tbProfileTerminalList B WITH(NOLOCK) ON  A.DatabaseID=B.DatabaseID WHERE B.TerminalID='{0}'", sTerminalID);
            string sReturn = "";
            
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oCmd.CommandTimeout = 0;
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                using (DataTable dtTemp = new DataTable())
                {
                    new SqlDataAdapter(oCmd).Fill(dtTemp);
                    sReturn = dtTemp.Rows[0][0].ToString();
                }
                //using (SqlDataReader read = oCmd.ExecuteReader())
                //    if (read.HasRows)
                //        if (read.Read())
            }

            return sReturn;
        }

        protected EMS_XML.DataClass.TerminalClass tcGenResult(string sTerminalID, string sResultDesc, string sResultCode, EMSCommonClass.XMLProcessType oType)
        {
            EMS_XML.DataClass.TerminalClass tcTemp = new EMS_XML.DataClass.TerminalClass();
            tcTemp.AddColumnValue("Terminal_Init", sTerminalID);
            tcTemp.AddColumnValue("Process_Type", oType.ToString());

            if (sResultCode != "000")
            {
                tcTemp.AddColumnValue("Response_Code", "001");

                tcTemp.Result.sResultFieldName = "Terminal_Init";
                tcTemp.Result.sResultCode = sResultCode;
                tcTemp.Result.sResultDesc = sResultDesc;
                tcTemp.Result.sTimeProcess = DateTime.Now.ToString("ddMMyyyyhhmmss");
            }
            else
            {
                tcTemp.AddColumnValue("Response_Code", "000");
            }
            return tcTemp;
        }

        protected DataTable dtGetEmsXmlMap(string sVersion, bool bInqInit)
        {
            DataTable dtTemp = new DataTable();
            string sQuery = string.Format("SELECT * FROM tbEmsMsXmlMap WITH(NOLOCK) WHERE EMSXMLVersion = '{0}' {1} ORDER BY EMSMapId"
                , sVersion
                , bInqInit ? "AND EMSXMLIsInqInit=1" : ""
                );
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        protected DataTable dtGetProfileFullTable(string _sTerminalID)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPProfileTextFullTable, oSqlConn))
            {
                oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }

        protected string sGetValue(string _sterminalID, DataTable dtTerminal, string sTag, string sFilter, bool bAdvance)
        {
            string sReturn = "";
            if (!bAdvance)
            {
                #region "SIMPLE SEARCH"
                string sCondition = string.Format("Tag='{0}'", sTag);
                if (sTag.Substring(0, 2).ToLower() != "de" && sTag.Substring(0, 2).ToLower() != "dc"
                    && !string.IsNullOrEmpty(sFilter))
                    sCondition = string.Format("{0} AND Name='{1}'", sCondition, sFilter);
                DataRow[] rowsResult = dtTerminal.Select(sCondition);
                if (rowsResult.Length > 0)
                {
                    sReturn = rowsResult[0]["TagValue"].ToString();
                    switch (sTag.ToLower())
                    {
                        case "ae14":
                        case "ae014":
                            sReturn = sReturn == "0" ? "1" : "0";
                            break;
                        //case "ae07" :
                        //case "ae007":
                        //case "ae11" :
                        //case "ae011":
                        //case "ae12" :
                        //case "ae012":
                        //    break;
                        case "de065":
                            if (sReturn == "1")
                            {
                                string sConditionPromo = string.Format("Tag LIKE 'AA001' AND Name NOT LIKE 'PROMO A' AND Name NOT LIKE 'PROMO SQ A' AND Name LIKE 'PROMO %'");
                                DataRow[] rowsResultAcqPromo = dtTerminal.Select(sConditionPromo);
                                if (rowsResultAcqPromo.Length == 0) sReturn = "0";
                            }
                            break;
                        default: break;
                    }
                    if (sFilter.Contains("PROMO SQ"))
                    {
                        bool bPromoSq = bool.Parse(sGetTID(dtTerminal, "AA005", sFilter) == "1" ? "true" : "false");
                        if (!bPromoSq)
                            sReturn = "0";
                    }
                }
                else
                    sReturn = "";
                #endregion
            }
            else
            {
                #region "ADVANCE SEARCH"
                switch (sTag)
                {
                    case "DE05":
                    case "DE005":
                        sReturn = IsCategoryValid(dtTerminal, sTag, sFilter) ? "1" : "0";
                        break;
                    case "AA04":
                    case "AA004":
                        sReturn = sGetMID(dtTerminal, sTag, "BCA");
                        if (string.IsNullOrEmpty(sReturn))
                        {
                            sReturn = sGetMID(dtTerminal, sTag, "LOYALTY");
                            if (string.IsNullOrEmpty(sReturn))
                            {
                                sReturn = sGetMID(dtTerminal, sTag, "DEBIT");
                                if (string.IsNullOrEmpty(sReturn))
                                {
                                    sReturn = sGetMID(dtTerminal, sTag, "FLAZZBCA");
                                    if (string.IsNullOrEmpty(sReturn))
                                    {
                                        sReturn = sGetMID(dtTerminal, sTag, "BCA1");
                                        if (string.IsNullOrEmpty(sReturn))
                                        {
                                            sReturn = sGetMID(dtTerminal, sTag, "BCA2");
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case "AA05":
                    case "AA005":
                        if (!string.IsNullOrEmpty(sFilter)) sReturn = sGetTID(dtTerminal, sTag, sFilter);
                        break;
                    default:
                        if (string.IsNullOrEmpty(sTag))
                        {
                            switch (sFilter.ToLower())
                            {
                                case "true":
                                case "offus":
                                    sReturn = "1";
                                    break;
                                case "lastinit":
                                    sReturn = sGetLastInitTerminalID(_sterminalID);
                                    break;
                                case "master":
                                    sReturn = sGetTerminalMasterBrowse(_sterminalID);
                                    break;
                                case "false":
                                    sReturn = "0";
                                    break;
                                case "promo":
                                    sReturn = sCicilanPromo(dtTerminal);
                                    break;
                            }
                        }
                        else
                            sReturn = sGetAcqIssValue(dtTerminal, sTag, sFilter);
                        break;
                }
                #endregion
            }
            return sReturn.Replace('<', '[').Replace('>', ']');
        }

        protected bool IsCategoryValid(DataTable dtTerminal, string sTag, string sFilter)
        {
            bool bValid = true;
            string sCondition = string.Format("Tag = '{0}'", sTag);
            DataRow[] rows = dtTerminal.Select(sCondition);
            if (rows.Count() > 0)
                bValid = sFilter == rows[0]["TagValue"].ToString() ? true : false;
            else
                bValid = false;
            return bValid;
        }

        protected string sGetMID(DataTable dtTerminal, string sTag, string sFilter)
        {
            string sReturn = "";
            string sCondition = string.Format("Tag = '{0}' AND Name = '{1}'", sTag, sFilter);
            DataRow[] rows = dtTerminal.Select(sCondition);
            if (rows.Count() > 0)
            {
                string sTemp = rows[0]["TagValue"].ToString();
                if (sTemp.Length == 15) sReturn = sTemp.Substring(sTemp.Length - 9);
            }
            return sReturn;
        }

        protected string sGetTID(DataTable dtTerminal, string sTag, string sFilter)
        {
            string sReturn = "0";
            string sTID = "";
            string sCondition = string.Format("Tag = '{0}' AND Name = '{1}'", sTag, sFilter);
            DataRow[] rows = dtTerminal.Select(sCondition);
            if (rows.Count() > 0)
            {
                sTID = rows[0]["TagValue"].ToString().Trim();
                int iNumber;
                int iLen = 6;

                if (sFilter.ToLower() == "amex" || sFilter.ToLower() == "diners") iLen = 8;
                else if (sFilter.ToLower().Contains("promo")
                    || sFilter.ToLower().Contains("bca1")
                    || sFilter.ToLower().Contains("bca2")) iLen = 5;

                //sReturn = string.IsNullOrEmpty(sTID.Trim()) && sTID.Trim().Length != 8 ? "" : int.TryParse(sReturn.Substring(sTID.Length - iLen), out iNumber) ? "1" : "0";

                if (string.IsNullOrEmpty(sTID) && sTID.Length != 8)
                    sReturn = "";
                else if (sTID.Length == 8 && int.TryParse(sTID.Substring(sTID.Length - iLen), out iNumber))
                    sReturn = "1";
                else
                    sReturn = "0";
            }
            return sReturn;
        }

        protected string sGetLastInitTerminalID(string _sterminalID)
        {
            string sReturn = "";
            string sInitTime = "";
            string sQuery = string.Format("SELECT InitTime FROM tbAuditInit WITH(NOLOCK) WHERE TerminalID='{0}' AND StatusDesc LIKE '%complete%' ORDER BY InitTime DESC"
                , _sterminalID);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                using (SqlDataReader reader = oCmd.ExecuteReader())
                {
                    if (reader.HasRows)
                        if (reader.Read())
                            sInitTime = reader[0].ToString();
                }
                if (!string.IsNullOrEmpty(sInitTime))
                {
                    DateTime dateInitTime = DateTime.Parse(sInitTime);
                    sReturn = string.Format("{0:ddMMyyhhmmss}", dateInitTime);
                }
            }
            return sReturn;
        }

        protected string sGetTerminalMasterBrowse(string _sterminalID)
        {
            string sReturn = "";
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPProfileTerminalMasterBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sterminalID;
                oCmd.Parameters.Add("@sMstTerminalId", SqlDbType.VarChar, 255).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();
                sReturn = oCmd.Parameters["@sMstTerminalId"].Value.ToString();
            }
            return sReturn;
        }

        protected string sCicilanPromo(DataTable _dtTerminal)
        {
            string sCondition = string.Format("Name <> 'PROMO A' {0} {1} {2} {3} {4} {5}",
                "AND Name like 'PROMO%' ",
                //"AND Tag='AA04' ",
                "AND Tag IN ('AA04','AA004') ",
                "AND ISNULL(TagValue,'') <> ''",
                "AND LEN(TagValue)=15",
                "AND TagValue NOT LIKE '%yyy' ",
                "AND TagValue NOT LIKE '%xxx'");
            int iCount = _dtTerminal.Select(sCondition).Count();
            return iCount > 0 ? "1" : "0";
        }

        protected string sGetAcqIssValue(DataTable dtTerminal, string sTag, string sFilter)
        {
            string sReturn = "";
            string[] arrsFilter = sFilter.Split(';');
            string sAdvanceFilter = arrsFilter[2];
            if (!string.IsNullOrEmpty(sAdvanceFilter))
            {
                string sCondition = string.Format("Tag = '{0}' AND Name <> 'AMEX' AND Name <> 'DINERS' AND TagValue <> '{1}'"
                    , sTag
                    , bool.Parse(sAdvanceFilter) == true ? 1 : 0);
                DataRow[] rows = dtTerminal.Select(sCondition);
                if (rows.Count() > 0)
                    sReturn = "1";
                else
                    sReturn = "0";
            }
            return sReturn;
        }
    }
}