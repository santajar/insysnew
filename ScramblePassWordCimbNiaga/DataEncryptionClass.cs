﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InSysClass;
using System.Security.Cryptography;


namespace ScramblePassWordCimbNiaga
{
  
        public class DataEncryptionClass
        {
            public static string sGetMD5Result(string _sToEncrypt)
            {
                byte[] bSource = ASCIIEncoding.ASCII.GetBytes(_sToEncrypt);

                MD5CryptoServiceProvider oMD5 = new MD5CryptoServiceProvider();

                byte[] bHash = oMD5.ComputeHash(bSource);
                return CommonLib.sByteArrayToString(bHash);
            }
        }
    
}
