﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScramblePassWordCimbNiaga
{
    public partial class FrmUserVerification : Form
    {
        SqlConnection oSqlConn;
        DataTable dt;
        string sUser;
        static string sUserId;
        public FrmUserVerification(SqlConnection _oSqlConn, string _sUser)
        {
            oSqlConn = _oSqlConn;
            sUser = _sUser;
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btnUpdate.Visible == false)
            {

                ClassInputData.DeleteData(oSqlConn, "DELETE FROM tbUserLoginVerification Where UserID = '" + sUserId + "'");
                SaveData();
                btnUpdate.Text = "UPDATE";
                btnDelete.Visible = true;
                btnUpdate.Visible = true;
                MessageBox.Show("UPDATE DATA BERHASIL...!!!");
            }
            else
            {
                if (SaveData() == true)
                {
                    MessageBox.Show("TAMBAH DATA USER BERHASIL..!!!");
                }
            }
            ClearText();
        }

        private bool SaveData()
        {
            bool bSaveData = false;
            if (txtUserID.Text != "" && txtNama.Text != "")
            {
                if (txtPassword.Text != "")
                {
                    if (ClassGetData.CheckRows(oSqlConn, "select * FROM tbUserLoginVerification Where UserID = '" + txtUserID + "'") == false)
                    {

                        ClassInputData.BulkDataUser(oSqlConn, txtUserID.Text.ToString(), txtNama.Text.ToString(), txtPassword.Text.ToString(), cbxUserRole.Text.ToString());
                        dataGridView1.DataSource = ClassGetData.dtGetDataTable(oSqlConn, "SELECT UserID, Nama FROM tbUserLoginVerification");
                        //MessageBox.Show("TAMBAH USER VERIFIKASI BERHASIL...!!!");
                        bSaveData = true;
                    }
                    else
                    {
                        MessageBox.Show("USERDID SUDAH TERPAKAI...!!!");
                    }

                }
                else
                {
                    MessageBox.Show("PASSWORD WAJIB DI ISI...!!!");
                }
            }
            else
            {
                MessageBox.Show("USER ID DAN NAMA DAN WAJIB DI ISI...!!!");
            }
            return bSaveData;
        }

        private void FrmUserVerification_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = ClassGetData.dtGetDataTable(oSqlConn, "SELECT UserID, Nama FROM tbUserLoginVerification");
        }

        private string sValueCell()
        {
            string sVal = "";
            if (dataGridView1.SelectedCells.Count != null)
            {
                int selectedrowindex = dataGridView1.CurrentCell.RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
                sVal = Convert.ToString(selectedRow.Cells[0].Value);
            }
            return sVal;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //btnSave.Visible = false;
            btnDelete.Visible = false;
            btnUpdate.Visible = false;
            sUserId = sValueCell();
            if (btnUpdate.Text == "UPDATE")
            {
               // btnUpdate.Text = "SAVE";
                GetRowsToUpdate();
                
            }
            //if (btnUpdate.Text == "SAVE")
            //{
            //    ClassInputData.DeleteData(oSqlConn, "DELETE FROM tbUserLoginVerification WHERE UserID = '" + sUserId + "'");
            //    SaveData();
            //    MessageBox.Show("UPDATE DATA BERHASIL...!!!");
            //    btnUpdate.Text = "UPDATE";
            //    btnSave.Visible = true;
            //    btnDelete.Visible = true;
            //}
           
        }

        private void GetRowsToUpdate()
        {
            string sUserId = sValueCell();
            DataTable dt = ClassGetData.dtGetDataTable(oSqlConn, "SELECT * FROM tbUserLoginVerification Where UserID = '" + sUserId + "'");
            txtUserID.Text = dt.Rows[0][0].ToString();
            txtNama.Text = dt.Rows[0][1].ToString();
            txtPassword.Text = dt.Rows[0][2].ToString();
            cbxUserRole.Text = dt.Rows[0][3].ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string sUserId = sValueCell();
            ClassInputData.DeleteData(oSqlConn, "DELETE FROM tbUserLoginVerification WHERE UserID = '" + sUserId + "'");
            dataGridView1.DataSource = ClassGetData.dtGetDataTable(oSqlConn, "SELECT UserID, Nama FROM tbUserLoginVerification");
            MessageBox.Show("DELETE DATA UserID : " + sUserId + "BERHASIL");
        }
        private void ClearText()
        {
            txtUserID.Text = "";
            txtNama.Text = "";
            txtPassword.Text = "";
            cbxUserRole.SelectedIndex = -1;
        }

        private void txtUserID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsControl(e.KeyChar) && !Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
       
       


    }
}
