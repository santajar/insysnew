﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ScramblePassWordCimbNiaga
{
    class ClassGetData
    {
        static SqlConnection oSqlConn;
        public static DataTable dtGetDataTable(SqlConnection _oSqlConn, string sParameter)
        {
            oSqlConn = _oSqlConn;
            DataTable dt = new DataTable();
            SqlDataAdapter adap = new SqlDataAdapter(sParameter, oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            adap.Fill(dt);
            return dt;
        }
      
        public static bool CheckRows(SqlConnection _oSqlConn, string sParameter)
        {
            bool blReady = false;
            oSqlConn = _oSqlConn;
            SqlCommand cmd = new SqlCommand(sParameter, oSqlConn);
            SqlDataReader dr;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                blReady = true;
            }
            dr.Close();
            cmd.Connection.Close();
            return blReady;
      
        }

        public static DataTable dtGetFileterDataLog(SqlConnection _oSqlConn, string sSelectBy, string sConditions, string sParameters, string sDateFrom, string sDateTo)
        {
            oSqlConn = _oSqlConn;
            DataTable dt = new DataTable();
            SqlCommand oSqlCmd = new SqlCommand("spLogDataView", oSqlConn);
            SqlDataAdapter da = new SqlDataAdapter();
            oSqlCmd.Parameters.Add("@sConditions", SqlDbType.VarChar).Value = sConditions;
            oSqlCmd.Parameters.Add("@sParameters", SqlDbType.VarChar).Value = sParameters;
            oSqlCmd.Parameters.Add("@sDateFrom", SqlDbType.NChar).Value = sDateFrom;
            oSqlCmd.Parameters.Add("@sDateTo", SqlDbType.NChar).Value = sDateTo;
            oSqlCmd.Parameters.Add("@sFlag", SqlDbType.VarChar).Value = sSelectBy;
            if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
            oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            da.SelectCommand = oSqlCmd;
            da.Fill(dt);
            //oSqlCmd.ExecuteNonQuery();
            return dt;
        }

        public static string  sGetDataString(SqlConnection _oSqlConn, string sParameter)
        {
            oSqlConn = _oSqlConn;
            string sResult ="";
            SqlCommand oSqlCmd = new SqlCommand(sParameter,oSqlConn);
            SqlDataReader dr;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            dr = oSqlCmd.ExecuteReader();
            while (dr.Read())
            {
                sResult = dr[0].ToString();
               // blReady = true;
            }
            dr.Close();
            oSqlCmd.Connection.Close();


            return sResult;

        }

        //public static string sGetUserRole(SqlConnection _oSqlConn, string sUserID)
        //{
        //    oSqlConn = _oSqlConn;
        //    string sUserName = "";
        //    SqlCommand oSqlCmd = new SqlCommand("SELECT UserRole FROM tbUserLoginVerification WHERE UserID = '" + sUserID + "'", oSqlConn);
        //    SqlDataReader dr;
        //    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
        //    dr = oSqlCmd.ExecuteReader();
        //    while (dr.Read())
        //    {
        //        sUserName = dr[0].ToString();
        //        // blReady = true;
        //    }
        //    dr.Close();
        //    oSqlCmd.Connection.Close();


        //    return sUserName;

        //}


       


    }
}
