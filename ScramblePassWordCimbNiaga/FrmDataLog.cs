﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ScramblePassWordCimbNiaga
{
    public partial class FrmDataLog : Form
    {
        SqlConnection oSqlConn;
        DataTable dt;
        string sType;
        //string sCase;
        string sParameters;
        string sConditions;
        string sDateFrom;
        string sDateTo;
        string sSelectBy;
        
        public FrmDataLog(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        private void FrmDataLog_Load(object sender, EventArgs e)
        {
            InitCbxCriteria();
        }

        public void InitCbxCriteria()
        {
            cbxCriteria.DataSource = ClassGetData.dtGetDataTable(oSqlConn, "select c.name from sys.columns c inner join sys.tables t on t.object_id = c.object_id and t.name='tbProfileVerification' and t.type='U'");
            if (cbxCriteria.DataSource != null)
            {
                cbxCriteria.DisplayMember = "name";
                cbxCriteria.ValueMember = "name";
                cbxCriteria.SelectedIndex = -1;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            if (chCriteria.Checked == true && chDate.Checked == false)
            {
                sSelectBy = "CRITERIA";
            }
            if (chCriteria.Checked == false && chDate.Checked == true)
            {
                sSelectBy = "DATE";
            }
            if (chCriteria.Checked == true && chDate.Checked == true)
            {
                sSelectBy = "ALL";
            }


            //string stgl = Convert.ToDateTime(dtpDateFrom.Value).ToString("dd/MM/yyyy");
            //sDateFrom = dtFrom.Value.ToShortDateString();
            //sDateTo = dtTo.Value.ToShortDateString();
            sDateFrom = dtFrom.Value.ToShortDateString().Replace("/","-");
            sDateTo = dtTo.Value.ToShortDateString().Replace("/","-");
            sDateFrom = dtFrom.Value.ToString("yyyy-MM-dd");
            sDateTo = dtTo.Value.ToString("yyyy-MM-dd");
            sConditions = cbxCriteria.Text;
            sParameters = txtParameter1.Text;
            sType = "View";
            StartAndStopProgress("Start", sType);
            backgroundWorker1.RunWorkerAsync();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            sConditions = cbxCriteria.Text;
            sParameters = txtParameter1.Text;
            sType = "Export";
            StartAndStopProgress("Start", sType);
            backgroundWorker1.RunWorkerAsync();
        }

        public void StartAndStopProgress(string _sFlag, string _sType)
        {
            if (_sFlag == "Start")
            {
                btnExport.Enabled = false;
                progressBar1.Value = 0;
                progressBar1.Maximum = 100;
                progressBar1.Style = ProgressBarStyle.Marquee;
            }
            else
            {
                btnExport.Enabled = true;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 100;
                if (_sType == "View")
                {
                    dataGridView1.DataSource = dt;
                }
                if (_sType == "Export")
                {
                    MessageBox.Show("Export Data Done");
                }

            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //dt = ClassGetData.dtGetDataTable(oSqlConn, "SELECT * From tbProfileVerification Where " + sConditions + " LIKE '%" + sParameters + "%'");     
                dt = ClassGetData.dtGetFileterDataLog(oSqlConn, sSelectBy, sConditions, sParameters, sDateFrom, sDateTo);
                
                if (sType == "Export")
                {
                    CommonClass.CreateFile(dt);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 100)
            {
                progressBar1.Style = ProgressBarStyle.Blocks;
                this.Cursor = Cursors.Default;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StartAndStopProgress("Stop", sType);
        }
    }
}
