﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace ScramblePassWordCimbNiaga
{
    
    class ClassInputData
    {
       // SqlConnection oSqlConn;

        public static void BulkDataVerification(SqlConnection _oSqlConn, string sEdcType, string sSoftwareVersion, string sTicket, string sTerminalID, string sTID, string sMID, string sMerchantName, string sAddress, string sCity, string sProfileCreated, string sSN, string sTechnicianName, string sRemarks, string sSPK, string sWO, string sPassword, string sTmsOperator, string sStatus)
        {
            SqlConnection oSqlConn = _oSqlConn;
            SqlCommand oSqlCmd = new SqlCommand("spInsertProfileVerification", oSqlConn);
            oSqlCmd.Parameters.Add("@sEdcType", SqlDbType.VarChar).Value = sEdcType;
            oSqlCmd.Parameters.Add("@sDbVersion", SqlDbType.VarChar).Value = sSoftwareVersion;
            oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
            oSqlCmd.Parameters.Add("@sTicketNumber", SqlDbType.VarChar).Value = sTicket;
            oSqlCmd.Parameters.Add("@sTID", SqlDbType.VarChar).Value = sTID;
            oSqlCmd.Parameters.Add("@sMerchantName", SqlDbType.VarChar).Value = sMerchantName;
            oSqlCmd.Parameters.Add("@sAddress", SqlDbType.VarChar).Value = sAddress;
            oSqlCmd.Parameters.Add("@sCity", SqlDbType.VarChar).Value = sCity;
            oSqlCmd.Parameters.Add("@sProfileCreated", SqlDbType.VarChar).Value = sProfileCreated;
            oSqlCmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = sSN;
            oSqlCmd.Parameters.Add("@sTechnisianName", SqlDbType.VarChar).Value = sTechnicianName;
            oSqlCmd.Parameters.Add("@sRemarks", SqlDbType.VarChar).Value = sRemarks;
            oSqlCmd.Parameters.Add("@sSPK", SqlDbType.VarChar).Value = sSPK;
            oSqlCmd.Parameters.Add("@sWO", SqlDbType.VarChar).Value = sWO;
            oSqlCmd.Parameters.Add("@sOperator", SqlDbType.VarChar).Value = sTmsOperator;
            oSqlCmd.Parameters.Add("@sStatus", SqlDbType.VarChar).Value = sStatus;
            if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
            oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            oSqlCmd.ExecuteNonQuery();
        }
        public static void BulkDataTechnician(SqlConnection _oSqlConn, string sIdKtp, string sNama, string sHP, string sServicePoint, string sVendorName, string sJenisKelamin)
        {
            SqlConnection oSqlConn = _oSqlConn;
            SqlCommand oSqlCmd = new SqlCommand("[spInsertProfileDataTechnician]", oSqlConn);
            oSqlCmd.Parameters.Add("@sIdKtp", SqlDbType.VarChar).Value = sIdKtp;
            oSqlCmd.Parameters.Add("@sNama", SqlDbType.VarChar).Value = sNama;
            oSqlCmd.Parameters.Add("@sHP", SqlDbType.VarChar).Value = sHP;
            oSqlCmd.Parameters.Add("@sServiePoint", SqlDbType.VarChar).Value = sServicePoint;
            oSqlCmd.Parameters.Add("@sVendorName", SqlDbType.VarChar).Value = sVendorName;
            oSqlCmd.Parameters.Add("@sJenisKelamin", SqlDbType.VarChar).Value = sJenisKelamin;
            if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
            oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oSqlCmd.ExecuteNonQuery();
            oSqlCmd.Dispose();
        }
        public static void BulkDataUser(SqlConnection _oSqlConn, string sUserID, string sNama, string sPassword, string sUserRole)
        {
            SqlConnection oSqlConn = _oSqlConn;
            SqlCommand oSqlCmd = new SqlCommand("[spInsertProfileDataUserVerification]", oSqlConn);
            oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = sUserID;
            oSqlCmd.Parameters.Add("@sNama", SqlDbType.VarChar).Value = sNama;
            oSqlCmd.Parameters.Add("@sPassword", SqlDbType.VarChar).Value = sPassword;
            oSqlCmd.Parameters.Add("@sUserRole", SqlDbType.VarChar).Value = sUserRole;
            if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
            oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oSqlCmd.ExecuteNonQuery();
            oSqlCmd.Dispose();
        }
        public static void DeleteData(SqlConnection _oSqlConn, string sPrameter)
        {
            SqlConnection oSqlConn = _oSqlConn;
            SqlCommand cmd = new SqlCommand(sPrameter, oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            cmd.ExecuteNonQuery();
            cmd.Dispose();
           
        }
        public static void ChangeScramblePassword(SqlConnection _oSqlConn, string sTerminalID)
        {
            SqlConnection oSqlConn = _oSqlConn;
            SqlCommand oSqlCmd = new SqlCommand("spGetRandomPassword", oSqlConn);
            oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
            if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
            oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            oSqlCmd.ExecuteNonQuery();
        }

    }
}
