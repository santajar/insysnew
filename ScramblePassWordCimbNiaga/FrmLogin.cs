﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using InSysClass;
namespace ScramblePassWordCimbNiaga
{
    public partial class frmLogin : Form
    {
        public string sUserName;
        public string sPassword;
        public string sPassFromDB;
        public string sUserNameFromDB;
        public string sRole;
       
        string sConnString = ConfigurationManager.ConnectionStrings["Main"].ConnectionString;
        static string sAppDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
        protected SqlConnection oSqlConn;

        public frmLogin()
        {
            InitializeComponent();
            oSqlConn = new SqlConnection(sConnString);
            oSqlConn.Open();

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (ClassGetData.CheckRows(oSqlConn, "SELECT * FROM tbUserLoginVerification WHERE UserID = '" + txtName.Text + "'") == true)
            {
                sUserName = ClassGetData.sGetDataString(oSqlConn, "SELECT Nama FROM tbUserLoginVerification WHERE UserID = '" + txtName.Text + "'");
                sPassword = ClassGetData.sGetDataString(oSqlConn, "SELECT [Password] FROM tbUserLoginVerification WHERE UserID = '" + txtName.Text + "'");
                sRole = ClassGetData.sGetDataString(oSqlConn, "SELECT UserRole FROM tbUserLoginVerification WHERE UserID = '" + txtName.Text + "'");
                if (sPassword == txtPassword.Text)
                {
                    FrmVerifikasi FrmVer = new FrmVerifikasi(oSqlConn, sUserName, sRole);
                    this.Hide();
                    FrmVer.Show();
                }
                else
                {
                    MessageBox.Show("PASSWORD SALAH");
                }
            }
            else
            {
                MessageBox.Show("USERID BELUM TERDAFTAR...!!!");
            }
        }
         
         
        


         public bool isWindowOpened(string sFormName)
        {
            bool isOpened = false;
            foreach (Form frmChild in this.MdiChildren)
            {
                if (sFormName == frmChild.Name)
                {
                    frmChild.Activate();
                    isOpened = true;
                }
            }
            return isOpened;
        }

         private void frmLogin_FormClosing(object sender, FormClosingEventArgs e)
         {
             Application.Exit();
         }
    }
}
