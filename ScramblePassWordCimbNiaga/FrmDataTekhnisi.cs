﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ScramblePassWordCimbNiaga
{
   
    public partial class FrmDataTekhnisi : Form
    {
        SqlConnection oSqlConn;
        static string sIdKtp;
        public FrmDataTekhnisi(SqlConnection _oSqlConn, string _sUser)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btnUpdate.Visible == false)
            {

                ClassInputData.DeleteData(oSqlConn, "DELETE FROM tbProfileDataTechnician WHERE IdKtp = '" + sIdKtp + "'");
                SaveData();
                btnUpdate.Text = "UPDATE";
                btnDelete.Visible = true;
                btnUpdate.Visible = true;
                MessageBox.Show("UPDATE DATA BERHASIL...!!!");
            }
            else
            {
                if (SaveData() == true)
                {
                    MessageBox.Show("TAMBAH DATA TEKHNISI BERHASIL..!!!");
                }
            }
            ClearText();
           
        }

        private void FrmDataTekhnisi_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = ClassGetData.dtGetDataTable(oSqlConn, "SELECT * FROM tbProfileDataTechnician");
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            btnUpdate.Visible = false;
            btnDelete.Visible = false;
            sIdKtp = sValueCell();
            if (btnUpdate.Text == "UPDATE")
            {
                //btnUpdate.Text = "SAVE";
                GetRowsToUpdate();

            }
            //if (btnUpdate.Text == "SAVE") 
            //{
            //    ClassInputData.DeleteData(oSqlConn, "DELETE FROM tbProfileDataTechnician WHERE IdKtp = '" + sIdKtp + "'");
            //    SaveData();
            //    MessageBox.Show("UPDATE DATA BERHASIL...!!!");
            //    btnUpdate.Text = "UPDATE";
            //    btnSave.Visible = true;
            //    btnDelete.Visible = true;
            //}
            //ClearText();
            
        }

        private string sValueCell()
        {
            string sVal = "";
            if (dataGridView1.SelectedCells.Count != null)
            {
                int selectedrowindex = dataGridView1.CurrentCell.RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
                sVal = Convert.ToString(selectedRow.Cells[0].Value);
            }
            return sVal;
        }
        private bool SaveData()
        {
            bool bSave = false;
            if (txtIdKtp.Text != "")
            {
                if (txtNama.Text != "")
                {
                    if (TxtHp.Text != "")
                    {
                        if (ClassGetData.CheckRows(oSqlConn, "select * FROM tbProfileDataTechnician where IdKtp = '" + txtIdKtp.Text + "'") == false)
                        {
                            ClassInputData.BulkDataTechnician(oSqlConn, txtIdKtp.Text.ToString(), txtNama.Text.ToString(), TxtHp.Text.ToString(), TxtServicePoint.Text.ToString(), txtVendorName.Text.ToString(), cbxJenisKelamin.Text.ToString());
                            dataGridView1.DataSource = ClassGetData.dtGetDataTable(oSqlConn, "SELECT * FROM tbProfileDataTechnician");
                            bSave = true;
                        }
                        else
                        {
                            MessageBox.Show("ID KTP SUDAH TERPAKAI...!!!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("NOMOR HP WAJIB DI ISI...!!!");
                    }
                }
                else
                {
                    MessageBox.Show("NAMA WAJID DI ISI...!!!");
                }
            }
            else
            {
                MessageBox.Show("NO KTP WAJID DI ISI..!!!");
            }
            return bSave;
        }
        private void GetRowsToUpdate()
        {
            string sIdKtp = sValueCell();
            DataTable dt = ClassGetData.dtGetDataTable(oSqlConn, "SELECT * FROM tbProfileDataTechnician Where IdKtp = '" + sIdKtp + "'");
            txtIdKtp.Text = dt.Rows[0][0].ToString();
            txtNama.Text = dt.Rows[0][1].ToString();
            TxtHp.Text = dt.Rows[0][2].ToString();
            TxtServicePoint.Text = dt.Rows[0][3].ToString();
            txtVendorName.Text = dt.Rows[0][4].ToString();
            cbxJenisKelamin.Text = dt.Rows[0][5].ToString();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            string sIdKtp = sValueCell();
            ClassInputData.DeleteData(oSqlConn, "DELETE FROM tbProfileDataTechnician WHERE IdKtp = '" + sIdKtp + "'");
            dataGridView1.DataSource = ClassGetData.dtGetDataTable(oSqlConn, "SELECT * FROM tbProfileDataTechnician");
            MessageBox.Show("DELETE DATA IdKtp : " + sIdKtp + "BERHASIL");
        }
        private void ClearText()
        {
            txtIdKtp.Text = "";
            txtNama.Text = "";
            TxtHp.Text = "";
            TxtServicePoint.Text = "";
            txtVendorName.Text = "";
            cbxJenisKelamin.SelectedIndex = -1;
        }

        private void txtIdKtp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsControl(e.KeyChar) && !Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            //if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            //{
              //  e.Handled = true;
            //}
        }

        private void TxtHp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsControl(e.KeyChar) && !Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
       



    }
}
