﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InSysClass;
using System.Security.AccessControl;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;

namespace ScramblePassWordCimbNiaga
{

    //static class UserData
    //{
    //    private static string _sUserID;
    //    /// <summary>
    //    /// Get or Set UserID from login
    //    /// </summary>
    //    public static string sUserID
    //    {
    //        set { _sUserID = value; }
    //        get { return _sUserID; }
    //    }

    //    private static string _sUserName;
    //    /// <summary>
    //    /// Get or Set UserName from login
    //    /// </summary>
    //    public static string sUserName
    //    {
    //        set { _sUserName = value; }
    //        get { return _sUserName; }
    //    }

    //    private static string _sPassword;
    //    /// <summary>
    //    /// Get or Set Password from login
    //    /// </summary>
    //    public static string sPassword
    //    {
    //        set { _sPassword = value; }
    //        get { return _sPassword; }
    //    }

    //    private static bool _isSuperUser;
    //    /// <summary>
    //    /// Get or Set if User is a SuperUser
    //    /// </summary>
    //    public static bool isSuperUser
    //    {
    //        set { _isSuperUser = value; }
    //        get { return _isSuperUser; }
    //    }

    //    private static string _sTerminalIdActive;
    //    public static string sTerminalIdActive
    //    {
    //        get { return _sTerminalIdActive; }
    //        set { _sTerminalIdActive = value; }
    //    }

    //    private static string _sGroupID;
    //    public static string sGroupID
    //    {
    //        get { return _sGroupID; }
    //        set { _sGroupID = value; }
    //    }
    //}
    class CommonClass
    {


        public static string sGetEncrypt(string sClearString)
        {
            return DataEncryptionClass.sGetMD5Result(sClearString);
        }

        public static DataTable dtLoadVerison(SqlConnection oSqlConn)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand("spBitMapBrowse", oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    //oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "";
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                        oAdapt.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            return dt;
        }


        public static DataTable dtGetContent(SqlConnection oSqlConn, string sTerminalID)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand("spGetContentScramble", oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                        oAdapt.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            return dt;
        }

        public static void CreateFile(DataTable dt)
        {
            string sDirectoryAppFile = string.Format(@"{0}\{1}Report Log.txt", Directory.GetCurrentDirectory(), DateTime.Now.ToString("dd-MM-yyyy"));
            try
            {
                using (StreamWriter oStreamWriter = new StreamWriter(sDirectoryAppFile))
                {
                    StringBuilder output = new StringBuilder();
                    foreach (DataRow dr in dt.Rows)
                    {
                        foreach (DataColumn dc in dt.Columns)
                        {
                            output.AppendFormat("{0}{1}", dr[dc], "|");

                        }
                        output.AppendLine();
                    }

                    oStreamWriter.Write(output);
                    output.AppendLine().Clear();

                }
            }
            catch
            {
            }
        }

        public static void doMinimizeChild(Form[] farrParent, string sFormName)
        {
            foreach (Form a in farrParent)
            {
                if (a.Name != sFormName)
                    a.WindowState = FormWindowState.Minimized;
            }
        }

    }
}
