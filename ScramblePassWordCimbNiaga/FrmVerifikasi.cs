﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;

namespace ScramblePassWordCimbNiaga
{
    public partial class FrmVerifikasi : Form
    {
        SqlConnection oSqlConn;
        string sUserName;
        string sUserRole;
        public FrmVerifikasi(SqlConnection _oSqlConn,  string _sUserName, string _sUserRole)
        {
            sUserName = _sUserName;
            sUserRole = _sUserRole;
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        private void FrmVerifikasi_Load(object sender, EventArgs e)
        {
            if (sUserRole == "USER")
            {
                superUserToolStripMenuItem.Visible = false;
            }
            lblTmsOperator.Text = sUserName;
            InitcbxEdcType();
            InitCbxSoftwareName();
            InitCbxTechnicianName();

        }

        protected void InitCbxSoftwareName()
        {
            cbxSoftwareVersion.DataSource = CommonClass.dtLoadVerison(oSqlConn);
            if (cbxSoftwareVersion.DataSource != null)
            {
                cbxSoftwareVersion.DisplayMember = "ApplicationName";
                cbxSoftwareVersion.ValueMember = "ApplicationName";
                cbxSoftwareVersion.SelectedIndex = -1;
            }
        }

        protected void InitcbxEdcType()
        {
            string[] lineOfContents = File.ReadAllLines("EdcType.txt");
            foreach (var line in lineOfContents)
            {
                string[] tokens = line.Split(',');
                cbxEdcType.Items.Add(tokens[0]);
            }
        }





    protected void InitCbxTechnicianName()
        {
            cbxTechnicianName.DataSource = ClassGetData.dtGetDataTable(oSqlConn, "SELECT * From tbProfileDataTechnician Order by Nama");
            if (cbxTechnicianName.DataSource != null)
            {
                cbxTechnicianName.DisplayMember = "Nama";
                cbxTechnicianName.ValueMember = "IdKtp";
                cbxTechnicianName.SelectedIndex = -1;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtTID.Text.Length == 8)
            {
                if (SearchDataTID() == false)
                {
                    MessageBox.Show("CSI TIDAK DITEMUKAN...!!!");
                }
            }
            else
            {
                MessageBox.Show("CSI tidak boleh kosong dan CSI harus 8 digit");
            }
        }

        private bool SearchDataTID()
        {
            bool bGetTid = false;
            DataTable dtContent = CommonClass.dtGetContent(oSqlConn, txtTID.Text);
            lblTicket.Text = DateTime.Now.ToString().Replace("/","").Replace(":", "").Replace(".","").Replace("AM","").Replace("PM","").Replace(" ","");
            //if (dtContent.Rows.Count > 0)
            if (dtContent.Rows.Count > -1)
            {
                lblPassword.Text = dtContent.Rows[0]["PASSWORD"].ToString();
                lblCsi.Text = dtContent.Rows[0]["CSI"].ToString();
                lblMerchantName.Text = dtContent.Rows[0]["MERCHANTNAME"].ToString();
                rchAddress.Text = dtContent.Rows[0]["ADDRESS"].ToString();
                lblCity.Text = dtContent.Rows[0]["CITY"].ToString();
                lblProfileCreated.Text = dtContent.Rows[0]["PROFILECREATED"].ToString();
                lblTID.Text = dtContent.Rows[0]["TID"].ToString();
                lblMID.Text = dtContent.Rows[0]["MID"].ToString();
                bGetTid = true;
            }
            return bGetTid;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (cbxEdcType.Text != "")
            {
                if (cbxSoftwareVersion.Text != "")
                {

                    if (txtTID.Text != "" )
                    {
                        if (txtSN.Text != "")
                        {
                            if (cbxTechnicianName.Text != "")
                            {
                                if (rchRemarks.Text != "")
                                {
                                    if (txtSPK.Text != "")
                                    {
                                        if (txtWO.Text != "")
                                        {
                                            if (cbxStatus.Text != "")
                                            {
                                                ClassInputData.BulkDataVerification(oSqlConn, cbxEdcType.Text.ToString(), cbxSoftwareVersion.Text.ToString(), lblTicket.Text.ToString(), txtTID.Text.ToString(), lblTID.Text.ToString(), lblMID.Text.ToString(), lblMerchantName.Text.ToString(), rchAddress.Text.ToString(), lblCity.Text.ToString(), lblProfileCreated.Text.ToString(), txtSN.Text.ToString(), cbxTechnicianName.Text.ToString(), rchRemarks.Text.ToString(), txtSPK.Text.ToString(), txtWO.Text.ToString(), lblPassword.Text.ToString(), lblTmsOperator.Text.ToString(), cbxStatus.Text.ToString());
                                                ClearTextBox();
                                            }
                                            else
                                            {
                                                MessageBox.Show("STATUS WAJIB DI ISI");
                                                cbxStatus.Focus();
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("NO WO WAJIB DI ISI");
                                            txtWO.Focus();
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("SPK WAJIB DI ISI");
                                        txtSPK.Focus();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("REMARKS WAJIB DI ISI");
                                    rchRemarks.Focus();
                                }
                            }
                            else
                            {
                                MessageBox.Show("NAMA TEKHNISI BELUM DIPILIH");
                                cbxTechnicianName.Focus();        
                            }
                        }
                        else
                        {
                            MessageBox.Show("SERIAL NUMBER WAJIB DI ISI");
                            txtSN.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("CSI HARUS DI ISI DENGAN BENAR");
                        txtTID.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Software harus di isi");
                    cbxSoftwareVersion.Focus();
                }
            }
            else
            {
                MessageBox.Show("EDC Type Harus di Isi");
                cbxEdcType.Focus();
            }

        
        }

        private void ClearTextBox()
        {
            cbxEdcType.SelectedIndex = -1;
            cbxSoftwareVersion.SelectedIndex = -1;
            txtTID.Text = "";
            txtSN.Text = "";
            txtSPK.Text = "";
            txtWO.Text = "";
            lblCity.Text = "-";
            lblCsi.Text = "-";
            lblMerchantName.Text="-";
            lblMID.Text = "-";
            lblProfileCreated.Text="-";
            lblPassword.Text ="-";
            lblTicket.Text ="-";
            lblTID.Text = "-";
            rchAddress.Text = "";
            cbxTechnicianName.SelectedIndex = -1;

        }

        private void inputDataTekhnisiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDataTekhnisi FrmTekhnisi = new FrmDataTekhnisi(oSqlConn, sUserName);
            FrmTekhnisi.Show();

        }

        private void inputDataUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmUserVerification FrmUserVer = new FrmUserVerification(oSqlConn, sUserName);
            FrmUserVer.Show();
        }

        private void txtSPK_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsControl(e.KeyChar) && !Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtSN_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsControl(e.KeyChar) && !Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void dataLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDataLog FrmDataLog = new FrmDataLog(oSqlConn);
            FrmDataLog.Show();
        }

        private void FrmVerifikasi_Leave(object sender, EventArgs e)
        {

        }

        private void FrmVerifikasi_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Application.Exit();
            frmLogin frmlog = new frmLogin();
            frmlog.Show();
            this.Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearTextBox();
        }


    }
}
