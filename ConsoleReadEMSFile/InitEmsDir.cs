﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Ini;

namespace ConsoleEMSReadFile
{
    class InitEmsDir
    {
        protected string sXmlFile;
        protected string sXmlResult;
        protected string sXmlLog;

        public string XmlFile { get { return sXmlFile; } }
        public string XmlResult { get { return sXmlResult; } }
        public string XmlLog { get { return sXmlLog; } }

        public InitEmsDir()
        {
            string sFilename = Environment.CurrentDirectory + @"\EmsDir.config";
            if (File.Exists(sFilename))
                InitConfig(sFilename);
        }

        protected void InitConfig(string _sFilename)
        {
            IniFile oIniEmsDir = new IniFile(_sFilename);
            sXmlFile = oIniEmsDir.IniReadValue("Directory", "File");
            sXmlLog = oIniEmsDir.IniReadValue("Directory", "Log");
            sXmlResult = oIniEmsDir.IniReadValue("Directory", "Result");
        }
    }
}
