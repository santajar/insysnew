using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

public class ConsoleTimer
{
    // Delegates 
    public delegate void Tick();

    // Properties
    public int Interval;
    public Tick OnTimerTick;

    // Private Data
    Thread _timerThread;
    volatile bool _bStop;

    public void Run()
    {
        while (true)
        {

            // Sleep for the timer interval 
            SleepIntermittently(Interval);

            // Then fire the tick event 
            OnTimerTick();
        }
    }

    public Thread Start()
    {
        _bStop = false;
        _timerThread = new Thread(new ThreadStart(Run));
        _timerThread.IsBackground = true;
        _timerThread.Start();

        return _timerThread;
    }

    public void Stop()
    {
        // Request the loop to stop 
        _bStop = true;
    }

    public void SleepIntermittently(int totalTime)
    {
        int sleptTime = 0;
        int intermittentSleepIncrement = 10;

        while (!_bStop && sleptTime < totalTime)
        {
            Thread.Sleep(intermittentSleepIncrement);
            sleptTime += intermittentSleepIncrement;
        }
    }
}