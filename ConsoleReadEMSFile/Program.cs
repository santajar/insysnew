using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Threading;
using System.IO;
using System.Data.SqlClient;
using InSysClass;
using Ini;
using ipXML;
using ConsoleEMSReadFile;
using System.Configuration;

namespace ConsoleReadEMSFile
{
    class Program
    {
        //static Thread threadtimer;
        static ConsoleTimer timerAlive = new ConsoleTimer();
        static string sXmlFile;
        static string sXmlResult;
        static string sXmlLog;

        static string sXmlBakSource;
        static string sXmlBakSourceFailed;

        static SqlConnection oSqlConn = new SqlConnection();
        static string sConnString = null;
        static bool bDebug = false;

        static void Main(string[] args)
        {
            try
            {
                string sErrMsg = null;
                Init();
                StartReadEmsFile(ref sErrMsg);
                if (string.IsNullOrEmpty(sErrMsg)) Console.WriteLine("SUCCESS");
                else Console.WriteLine("INFO, {0}", sErrMsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine("INFO, GAGAL : {0}", ex.Message);
            }
            if (bDebug) Console.Read();
        }

        static void Init()
        {
            //sConnString = new InitData().sGetConnString();
            //oSqlConn = new SqlConnection(sConnString);
            bDebug = bool.Parse(ConfigurationManager.AppSettings["Debug"].ToString());
            sConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            oSqlConn = new SqlConnection(sConnString);
            //oSqlConn.Open();

            InitDirectory();

            //InitTimer();
            //timerAlive.Interval = iIntervalSecs;
            //timerAlive.OnTimerTick += timerAlive_Tick;
            //threadtimer = timerAlive.Start();
        }

        static void InitDirectory()
        {
            //string sCurrDir = Environment.CurrentDirectory;
            //sXmlLog = sCurrDir + @"\EMS\LOG";
            //sXmlFile = sCurrDir + @"\EMS\File";
            //sXmlResult = sCurrDir + @"\EMS\Result";

            //InitEmsDir oEmsDir = new InitEmsDir();
            sXmlFile = ConfigurationManager.AppSettings["Directory File"].ToString();
            sXmlLog = ConfigurationManager.AppSettings["Directory Log"].ToString();
            sXmlResult = ConfigurationManager.AppSettings["Directory Result"].ToString();

            if (!Directory.Exists(sXmlLog))
                Directory.CreateDirectory(sXmlLog);
            if (!Directory.Exists(sXmlFile))
                Directory.CreateDirectory(sXmlFile);
            if (!Directory.Exists(sXmlResult))
                Directory.CreateDirectory(sXmlResult);

            EMSCommonClass.DirectoryLog = sXmlLog;
            EMSCommonClass.DirectoryXml = sXmlFile;
            EMSCommonClass.DirectoryResult = sXmlResult;

            sXmlBakSource = ConfigurationManager.AppSettings["BAK_SOURCE"].ToString();
            if (!string.IsNullOrEmpty(sXmlBakSource)) sXmlBakSource = @"BAK\SOURCE";
            sXmlBakSource = string.Format(@"{0}\{1}\{2:yyyy-MM-dd}", Environment.CurrentDirectory, sXmlBakSource, DateTime.Now);
            if (!Directory.Exists(sXmlBakSource)) Directory.CreateDirectory(sXmlBakSource);
            sXmlBakSourceFailed = string.Format(@"{0}\FAILED", sXmlBakSource);
            if (!Directory.Exists(sXmlBakSourceFailed)) Directory.CreateDirectory(sXmlBakSourceFailed);
        }

        static void StartReadEmsFile(ref string sErrMsg)
        {
            bool bError = false;
            int iFailedCounter = 0;
            DirectoryInfo di = new DirectoryInfo(sXmlFile);
            FileInfo[] arrfi = di.GetFiles("*.xml");
            int iError;

            foreach (FileInfo fiTemp in arrfi)
            {
                List<DataTable> dtXML = new List<DataTable>();
                ReadEMSFile(fiTemp.FullName, ref dtXML);
                string sFileResult = fiTemp.FullName.Replace(sXmlFile, sXmlResult);
                EMSCommonClass.XMLRequestType oProperty = XMLAttributeReqType(dtXML);
                
                EMS_XML oXmlResult = new EMS_XML();
                oXmlResult = null;
                iError = 0;
                EMSCommonClass.UserAccessDelete(new SqlConnection(sConnString), null);
                switch (oProperty)
                {
                    case EMSCommonClass.XMLRequestType.INQ:
                    case EMSCommonClass.XMLRequestType.INQINIT:
                        //EMSCommonClass.IsXMLInquiry(oSqlConn, ref sErrMsg, ref oXmlResult, dtXML, sFileResult);
                        EMSCommonClass.IsXMLInquiry(sConnString, ref sErrMsg, ref iError, ref oXmlResult, dtXML, sFileResult);
                        if (!bError && !string.IsNullOrEmpty(sErrMsg))
                        {
                            sErrMsg = string.Format("INFO : {0}", sErrMsg);
                            bError = true;
                        }
                        //else sErrMsg = "Success ";
                        break;
                    case EMSCommonClass.XMLRequestType.ADD_UPDATE:
                        //EMSCommonClass.IsXMLAddUpdate(oSqlConn, ref sErrMsg, ref oXmlResult, dtXML, sFileResult);
                        EMSCommonClass.IsXMLAddUpdate(sConnString, ref sErrMsg, ref iError, ref oXmlResult, dtXML, sFileResult);
                        if (!bError && !string.IsNullOrEmpty(sErrMsg))
                        {
                            sErrMsg = string.Format("INFO : {0}", sErrMsg);
                            bError = true;
                        }
                        //else sErrMsg = "Success";
                        break;
                    case EMSCommonClass.XMLRequestType.DELETE:
                        //EMSCommonClass.IsXMLDelete(oSqlConn, ref sErrMsg, ref oXmlResult, dtXML, sFileResult);
                        EMSCommonClass.IsXMLDelete(sConnString, ref sErrMsg, ref iError, ref oXmlResult, dtXML, sFileResult);
                        if (!bError && !string.IsNullOrEmpty(sErrMsg))
                        {
                            sErrMsg = string.Format("INFO : {0}", sErrMsg);
                            bError = true;
                        }
                        //else sErrMsg = "Success";
                        break;
                    default:
                        sErrMsg = string.Format("INFO : Unknown Request Property : {0}", oProperty.ToString());
                        bError = true;
                        break;
                }
                string sLog = string.Format("{0} Processed... {1}", fiTemp.Name, sErrMsg);
                EMSCommonClass.WriteLog(sLog);

                if (string.IsNullOrEmpty(sErrMsg))
                {
                    File.Copy(fiTemp.FullName, string.Format(@"{0}\{1}", sXmlBakSource, fiTemp.Name), true);
                    File.Delete(fiTemp.FullName);
                }
                else
                {
                    File.Copy(fiTemp.FullName, string.Format(@"{0}\{1}", sXmlBakSourceFailed, fiTemp.Name), true);
                    File.Delete(fiTemp.FullName);

                    bError = true;
                    iFailedCounter++;
                    if (iError == 1)
                    {
                        sErrMsg = string.Format("GAGAL : koneksi SQL gagal");
                        break;
                    }
                    else sErrMsg = string.Format("GAGAL : {0} gagal diproses.", iFailedCounter);
                }
            }
        }

        static void ReadEMSFile(string sPath, ref List<DataTable> dtXML)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(sPath);
            foreach (DataTable dtTemp in ds.Tables)
            {
                dtXML.Add(dtTemp);
            }
            ds.Dispose();
        }

        static DataTable dtGetTable(EMSCommonClass.XMLProperty oProperty, List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = new DataTable();
            dtTemp = dtXMLTemp.Find(delegate(DataTable dt) { return dt.TableName == oProperty.ToString(); });
            return dtTemp;
        }

        static EMSCommonClass.XMLRequestType XMLAttributeReqType(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Attribute, dtXMLTemp);
            string sTemp = dtTemp.Rows[0]["Type"].ToString().ToUpper();

            if (sTemp == "INQ")
                return EMSCommonClass.XMLRequestType.INQ;
            else if (sTemp == "INQINIT")
                return EMSCommonClass.XMLRequestType.INQINIT;
            else if (sTemp == "ADD/UPDATE" || sTemp == "ADD_UPDATE") 
                return EMSCommonClass.XMLRequestType.ADD_UPDATE;
            else
                return EMSCommonClass.XMLRequestType.DELETE;
        }

        static string sXMLSenderId(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Attribute, dtXMLTemp);
            return dtTemp.Rows[0]["Sender_ID"].ToString();
        }

        static string sXMLFilterField(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Filter, dtXMLTemp);
            return dtTemp.Rows[0]["Field_Name"].ToString();
        }

        static string sXMLFilterValue(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Filter, dtXMLTemp);
            return dtTemp.Rows[0]["Values"].ToString();
        }

        static string sXMLTerminalInitValue(List<DataTable> dtXMLTemp)
        {
            DataTable dtTemp = dtGetTable(EMSCommonClass.XMLProperty.Terminal, dtXMLTemp);
            return dtTemp.Rows[0]["Terminal_INIT"].ToString();
        }
    }
}