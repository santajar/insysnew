using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;
using System.Web.Configuration;

namespace ipXML
{
    public enum EmsError
    {
        Error = 0,
        ErrorSQL = 1,
    }

    public class EMSCommon
    {
        public class XML
        {
            #region "Stored Procedure"
            public static string sSPCreateTable { get { return "spXMLCreateSenderTable"; } }
            public static string sSPInsertTable { get { return "spXMLInsertSenderTable"; } }
            public static string sSPDeleteTableSender { get { return "spXMLDeleteSenderTable"; } }
            public static string sSPDeleteTableResult { get { return "spXMLDeleteResultTable"; } }
            public static string sSPViewTable { get { return "spXMLBrowseTestTemp"; } }

            public static string sSPXMLDeleteProfile { get { return "spXMLDeleteProfile"; } }
            public static string sSPXMLAddUpdateProfile { get { return "spXMLAddUpdateProfile"; } }
            public static string sSPXMLInquiryProfile { get { return "spXMLInquiryProfile"; } }
            public static string sSPXMLInquiryAllProfile { get { return "spXMLInquiryAllProfile"; } }
            public static string sSPXMLGetMapColumn { get { return "spXMLGetMapColumn"; } }
            public static string sSPXMLMsProfileBrowseByXmlProfile { get { return "spXMLMsProfileBrowseByXmlProfile"; } }
            public static string sSPXMLMsProfileBrowse { get { return "spXMLMsProfileBrowse"; } }

            public static string sSPXMLDeleteAcquirer { get { return "spXMLDeleteAcquirer"; } }
            public static string sSPXMLDeleteAcquirerPromo { get { return "spXMLDeleteAcquirerPromo"; } }
            public static string sSPXMLUpdateProfileTag { get { return "spXMLUpdateProfileTag"; } }
            public static string sSPXMLUpdateMasterDial { get { return "spXMLUpdateMasterDial"; } }

            public static string sSPTerminalListBrowse { get { return "spProfileTerminalListBrowse"; } }
            public static string sSPProfileTerminalDeleteProcess { get { return "spProfileTerminalDeleteProcess"; } }
            public static string sSPProfileTerminalListUpdateAllowDownload { get { return "spProfileTerminalListUpdateAllowDownload"; } }
            public static string sSPProfileTerminalUpdateLastUpdate { get { return "spProfileTerminalUpdateLastUpdate"; } }
            public static string sSPProfileTerminalMasterAdd { get { return "spProfileTerminalMasterAdd"; } }
            public static string sSPProfileTerminalMasterBrowse { get { return "spProfileTerminalMasterBrowse"; } }
            public static string sSPProfileTerminalCopyProcess { get { return "spProfileTerminalCopyProcess"; } }
            public static string sSPProfileTextFullTable { get { return "spProfileTextFullTable"; } }
            public static string sSPVipotProfile { get { return "spVipotProfile"; } }

            public static string sSPProfileTerminalUpdateTag { get { return "spProfileTerminalUpdateTag"; } }
            public static string sSPProfileAcquirerUpdateTag { get { return "spProfileAcquirerUpdateTag"; } }

            public static string sSPAuditTrailInsert { get { return "spAuditTrailInsert"; } }
            #endregion

            #region "Error Message"
            public static string sErrorCommon { get { return "TIDAK BERHASIL. Error "; } }
            public static string sErrorMaster { get { return "TIDAK BERHASIL. Invalid Master name "; } }
            public static string sErrorMaster2 { get { return "TIDAK BERHASIL. Invalid Master Profile name "; } }
            public static string sErrorMasterLength { get { return "TIDAK BERHASIL. Invalid Master name length "; } }
            public static string sErrorSoftwareVersion { get { return "TIDAK BERHASIL. Invalid Software_Version "; } }
            public static string sErrorInvalidAccess { get { return "TIDAK BERHASIL. Invalid Access "; } }
            public static string sErrorTerminalMID { get { return "TIDAK BERHASIL, Terminal exist, MID tidak sama "; } }
            #endregion

            #region "User Access"
            public static string sSPUserAccessInsert { get { return "spUserAccessInsert"; } }
            public static string sSPUserAccessDelete { get { return "spUserAccessDelete"; } }
            public static string sSPUserAccessBrowse { get { return "spUserAccessBrowse"; } }
            #endregion
        }
    }

    public class EMSCommonClass
    {
        public enum XMLProperty
        {
            Attribute = 0,
            Terminal,
            Terminal_Result,
            Filter,
        }

        public enum XMLRequestType
        {
            INQ = 0,
            ADD_UPDATE,
            DELETE,
            INQINIT,
        }

        public enum XMLResponsType
        {
            RESULTINQ = 0,
            RESULT,
            RESULTINQINIT,
        }

        public enum XMLProcessType
        {
            ADD = 0,
            UPDATE,
            DELETE,
        }

        protected static string sDir;
        protected static string sDirLog;
        protected static string sDirXml;
        protected static string sDirResult;

        public static string sUserID = "EMS";

        public static string Directory
        {
            set { sDir = value; }
            get { return sDir; }
        }

        public static string DirectoryLog
        {
            set { sDirLog = value; }
            get { return sDirLog; }
        }

        public static string DirectoryXml
        {
            set { sDirXml = value; }
            get { return sDirXml; }
        }

        public static string DirectoryResult
        {
            set { sDirResult = value; }
            get { return sDirResult; }
        }

        //public static bool IsXMLAddUpdate(SqlConnection oSqlConn, ref string sErrMsg, ref EMS_XML oEms_Xml, List<DataTable> dtXMLTemp, string _sFileResult)
        public static bool IsXMLAddUpdate(string _sConnString
            , ref string sErrMsg, ref int iError
            , ref EMS_XML oEms_Xml, List<DataTable> dtXMLTemp, string _sFileResult)
        {
            iError = EmsError.Error.GetHashCode();
            try
            {
                if (oEms_Xml == null)
                    oEms_Xml = new EMS_XML(dtXMLTemp);

                string sSenderId = oEms_Xml.Attribute.sSenderID;
                if (!string.IsNullOrEmpty(_sFileResult)) SaveXml(true, 1, sSenderId, oEms_Xml);
                SqlEMS_XML oSqlEMS = new SqlEMS_XML(_sConnString, sSenderId, oEms_Xml);

                if (oEms_Xml.Attribute.sVersions == "1.00")
                    if (oEms_Xml.Data.ltTerminal[0].sGetColumnValue("Software_Version").Contains("BIN"))
                        oEms_Xml.Attribute.sVersions = "2.00";
            
                    if (oSqlEMS.IsXMLAddUpdate(sSenderId, ref sErrMsg, ref oEms_Xml, _sFileResult))
                        sErrMsg = "";


                if (!string.IsNullOrEmpty(_sFileResult)) SaveXml(false, 1, sSenderId, oEms_Xml);
            }
            catch (SqlException sqlex)
            {
                sErrMsg = string.Format("Common.IsXMLAddUpdate TIDAK BERHASIL 1: SQL connection TIDAK BERHASIL. {0}\n{1}", sqlex.StackTrace, sqlex.Message);
                WriteLog(sErrMsg);
                iError = EmsError.ErrorSQL.GetHashCode();
            }
            catch (Exception ex)
            {
                sErrMsg = string.Format("Common.IsXMLAddUpdate TIDAK BERHASIL 2: {0}\n{1}", ex.StackTrace, ex.Message);
                WriteLog(sErrMsg);
            }
            //WriteLog("Common.IsXMLAddUpdate End");
            return string.IsNullOrEmpty(sErrMsg) ? true : false;
        }

        //public static bool IsXMLDelete(SqlConnection oSqlConn, ref string sErrMsg, ref EMS_XML oEms_Xml, List<DataTable> dtXMLTemp, string _sFileResult)
        public static bool IsXMLDelete(string _sConnString
            , ref string sErrMsg, ref int iError
            , ref EMS_XML oEms_Xml, List<DataTable> dtXMLTemp, string _sFileResult)
        {
            try
            {
                //WriteLog("Common.IsXMLDelete Start"); 
                iError = EmsError.Error.GetHashCode();
                if (oEms_Xml == null)
                    oEms_Xml = new EMS_XML(dtXMLTemp);

                string sSenderId = oEms_Xml.Attribute.sSenderID;
                //SqlEMS_XML oSqlEMS = new SqlEMS_XML(oSqlConn, sSenderId, oEms_Xml);
                SqlEMS_XML oSqlEMS = new SqlEMS_XML(_sConnString, sSenderId, oEms_Xml);

                if (!string.IsNullOrEmpty(_sFileResult)) SaveXml(true, 2, sSenderId, oEms_Xml);

                //if (oSqlEMS.IsCreateTable(ref sErrMsg, ref oEms_Xml))
                //    if (oSqlEMS.IsInsertRow(ref sErrMsg, ref oEms_Xml))
                if (oSqlEMS.IsXMLDelete(sSenderId, ref sErrMsg, ref oEms_Xml, _sFileResult))
                {
                    sErrMsg = "";
                }
                else
                    oEms_Xml = XmlReturnResult(oEms_Xml, "", "", sErrMsg);
                //    else
                //        oEms_Xml = XmlReturnResult(oEms_Xml, _sFileResult, "", sErrMsg);
                //else
                //    oEms_Xml = XmlReturnResult(oEms_Xml, _sFileResult, "", sErrMsg);
                //oSqlEMS.DeleteSenderTable();

                if (!string.IsNullOrEmpty(_sFileResult)) SaveXml(false, 2, sSenderId, oEms_Xml);
            }
            catch (SqlException sqlex)
            {
                sErrMsg = string.Format("Common.IsXMLDelete TIDAK BERHASIL : SQL connection TIDAK BERHASIL. {0}\n{1}", sqlex.StackTrace, sqlex.Message);
                WriteLog(sErrMsg);
                iError = EmsError.ErrorSQL.GetHashCode();
            }
            catch (Exception ex)
            {
                sErrMsg = string.Format("Common.IsXMLDelete TIDAK BERHASIL : {0}\n{1}", ex.StackTrace, ex.Message);
                WriteLog(sErrMsg);
            }
            //WriteLog("Common.IsXMLDelete End");
            return string.IsNullOrEmpty(sErrMsg) ? true : false;
        }

        //public static bool IsXMLInquiry(SqlConnection oSqlConn, ref string sErrMsg, ref EMS_XML oEms_Xml, List<DataTable> dtXMLTemp, string _sFileResult)
        public static bool IsXMLInquiry(string _sConnString
            , ref string sErrMsg, ref int iError
            , ref EMS_XML oEms_Xml, List<DataTable> dtXMLTemp, string _sFileResult)
        {
            try
            {
                //WriteLog("Common.IsXMLInquiry Start");
                iError = EmsError.Error.GetHashCode();
                if (oEms_Xml == null)
                    oEms_Xml = new EMS_XML(dtXMLTemp);

                string sSenderId = oEms_Xml.Attribute.sSenderID;
                if (!string.IsNullOrEmpty(_sFileResult)) SaveXml(true, 3, sSenderId, oEms_Xml);

                //SqlEMS_XML oSqlEMS = new SqlEMS_XML(oSqlConn, sSenderId, oEms_Xml);
                SqlEMS_XML oSqlEMS = new SqlEMS_XML(_sConnString, sSenderId, oEms_Xml);

                //WriteLog("Common.IsXMLInquiry : SqlEMS_XML oSqlEMS = new SqlEMS_XML(oSqlConn, sSenderId, oEms_Xml);");

                //if (oSqlEMS.IsCreateTable(ref sErrMsg, ref oEms_Xml))
                //    if (oSqlEMS.IsInsertRow(ref sErrMsg, ref oEms_Xml))
                //    {
                //        //oSqlEMS.XMLInquiryAllProfile(sSenderId);
                if (oSqlEMS.IsXMLInquiry(sSenderId, ref sErrMsg, ref oEms_Xml, _sFileResult))
                {
                    //WriteLog("Common.IsXMLInquiry Success");
                    sErrMsg = "";
                }
                //    }
                //oSqlEMS.DeleteSenderTable();
                if (!string.IsNullOrEmpty(_sFileResult)) SaveXml(false, 3, sSenderId, oEms_Xml);
            }
            catch (SqlException sqlex)
            {
                sErrMsg = string.Format("Common.IsXMLInquiry FAILED : SQL connection failed. {0}\n{1}", sqlex.StackTrace, sqlex.Message);
                WriteLog(sErrMsg);
                iError = EmsError.ErrorSQL.GetHashCode();
            }
            catch (Exception ex)
            {
                sErrMsg = string.Format("Common.IsXMLInquiry FAILED : {0}\n{1}", ex.StackTrace, ex.Message);
                WriteLog(sErrMsg);
            }
            //WriteLog("Common.IsXMLInquiry End");
            return string.IsNullOrEmpty(sErrMsg) ? true : false;
        }

        protected static EMS_XML XmlReturnResult(EMS_XML _oEms_Xml, string _sFileResult, string _sColError, string _sErrMsg)
        {
            //WriteLog("Common.XmlReturnResult Start");

            EMS_XML oResultEms = new EMS_XML();
            oResultEms.Attribute = _oEms_Xml.Attribute;


            foreach (EMS_XML.DataClass.TerminalClass oTerminalClass in _oEms_Xml.Data.ltTerminal)
            {
                EMS_XML.DataClass.TerminalClass oTempTerminalClass = new EMS_XML.DataClass.TerminalClass();

                oTempTerminalClass.AddColumnValue("Terminal_Init", oTerminalClass.sGetColumnValue("Terminal_Init".ToUpper()));
                oTempTerminalClass.AddColumnValue("Process_Type".ToUpper(), _oEms_Xml.Attribute.sTypes);

                if (!string.IsNullOrEmpty(_sErrMsg))
                {
                    oResultEms.Data.Terminal.AddColumnValue("Response_Code", "001");
                    oTempTerminalClass.Result.sResultFieldName = string.IsNullOrEmpty(_sColError) ? "Terminal_Init" : _sColError;
                    oTempTerminalClass.Result.sResultCode = string.IsNullOrEmpty(_sErrMsg) ? "000" : "001";
                    oTempTerminalClass.Result.sResultDesc = string.IsNullOrEmpty(_sErrMsg) ? "Success" : _sErrMsg;
                    oTempTerminalClass.Result.sTimeProcess = DateTime.Now.ToString("ddMMyyyyhhmmss");
                }
                else
                    oResultEms.Data.Terminal.AddColumnValue("Response_Code", "000");

                oResultEms.Data.ltTerminal.Add(oTempTerminalClass);
            }
            if (!string.IsNullOrEmpty(_sFileResult)) oResultEms.CreateXmlFile(_sFileResult, true);
            //WriteLog("Common.XmlReturnResult End");
            return oResultEms;
        }

        public static void WriteLog(string sMessage)
        {
            string sFilename = string.Format(@"{0}\{1:yyyyMMdd}.log", sDirLog, DateTime.Now);
            StreamWriter sw = new StreamWriter(sFilename, true);
            sw.WriteLine(string.Format("[{0:yyyy MMM dd, hh:mm:ss tt}] : {1}", DateTime.Now, sMessage));
            sw.Close();
            sw.Dispose();
        }

        public static void UserAccessInsert(SqlConnection _oSqlConn, string _sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPUserAccessInsert, _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = sUserID;
                oCmd.ExecuteNonQuery();
            }
        }

        public static void UserAccessDelete(SqlConnection _oSqlConn, string _sTerminalId)
        {
            if (!string.IsNullOrEmpty(sUserID))
                using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPUserAccessDelete, _oSqlConn))
                {
                    if (_oSqlConn.State != ConnectionState.Open)
                        _oSqlConn.Open();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = sUserID;
                    oCmd.ExecuteNonQuery();
                }
        }

        public static bool IsAllowUserAccess(SqlConnection _oSqlConn, string _sTerminalId)
        {
            string _sError = null;
            return IsAllowUserAccess(_oSqlConn, _sTerminalId, ref _sError);
        }

        public static bool IsAllowUserAccess(SqlConnection _oSqlConn, string _sTerminalId, ref string sUserIdOnAccess)
        {
            bool isAllow = true;
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPUserAccessBrowse, _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@sOutput", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();

                sUserIdOnAccess = oCmd.Parameters["@sUserId"].Value.ToString();
                isAllow = oCmd.Parameters["@sOutput"].Value.ToString() == "1" ? false : true;
            }
            return isAllow;
        }
        
        public static DataTable dtGetEmsXmlMap(SqlConnection _oSqlConn, string sVersion)
        {
            //EMSCommonClass.WriteLog("SqlEMS_XML.dtEMSXMLMap");

            DataTable dtTemp = null;
            using (SqlCommand oSqlCmd = new SqlCommand(EMSCommon.XML.sSPXMLGetMapColumn, _oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sVersion", SqlDbType.VarChar).Value = sVersion;
                if (_oSqlConn.State != ConnectionState.Open) _oSqlConn.Open();
                
                using (SqlDataReader oReader = oSqlCmd.ExecuteReader())
                {
                    if (oReader.HasRows)
                    {
                        dtTemp = new DataTable();
                        dtTemp.Load(oReader);
                    } oReader.Close();
                }
            }
            return dtTemp;
        }        

        public static void SaveXml(bool bIn, int iProcess, string sSenderId, EMS_XML input)
        {
            string sFilename = null;
            string sDirXml = null;
            //sDirLog = @"D:\Log";
            sDirLog = WebConfigurationManager.AppSettings["Path"].ToString()+"LOG";
            // add tatang dont active,, this for trace
            //string sErrMsg = string.Format("sDirLog : {0}\n{1}", sDirLog, " ");
            //WriteLog(sErrMsg);
            //

            switch (iProcess)
            {
                case 1:
                    sFilename = string.Format("ADDUPDATE_{0}", sFilename);
                    sDirXml = string.Format(@"{0}\ADDUPDATE", sDirLog);
                    // add tatang dont active,, this for trace
                    //sErrMsg = string.Format("DIR XML : {0}\n{1}", sDirXml, "");
                    //WriteLog(sErrMsg);
                    //
                    break;
                case 2: 
                    sFilename = string.Format("DELETE_{0}", sFilename);
                    sDirXml = string.Format(@"{0}\DELETE", sDirLog);
                    break;
                case 3: 
                    sFilename = string.Format("INQUIRY_{0}", sFilename);
                    sDirXml = string.Format(@"{0}\INQUIRY", sDirLog);
                    break;
            }

            if (bIn)
            {
                sFilename = string.Format("{0}IN_", sFilename);
                sDirXml = string.Format(@"{0}\IN", sDirXml);
            }
            else
            {
                sFilename = string.Format("{0}OUT_", sFilename);
                sDirXml = string.Format(@"{0}\OUT", sDirXml);
            }

            

            sFilename = string.Format(@"{0}\{1}{2}.xml", sDirXml, sFilename, sSenderId);
            // add tatang dont active,, this for trace
            //sErrMsg = string.Format("Nama File : {0}\n{1}", sFilename, " ");
            //WriteLog(sErrMsg);
            //
            SerializeToXmlDocument(input).Save(sFilename);
        }

        public static XmlDocument SerializeToXmlDocument(object input)
        {
            XmlSerializer ser = new XmlSerializer(input.GetType(), "http://schemas.yournamespace.com");

            XmlDocument xd = null;

            using (MemoryStream memStm = new MemoryStream())
            {
                ser.Serialize(memStm, input);

                memStm.Position = 0;

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreWhitespace = true;

                using (var xtr = XmlReader.Create(memStm, settings))
                {
                    xd = new XmlDocument();
                    xd.Load(xtr);
                    // add tatang dont active,, this for trace
                    //string sErrMsg = string.Format("SUCCESS File : {0}\n{1}", xtr.ToString(), " ");
                    //WriteLog(sErrMsg);
                    //
                }
            }
          
            return xd;
        }
    }

    class Trace
    {
        static string sPath = Environment.CurrentDirectory;
        //Path.GetDirectoryName(            Assembly.GetExecutingAssembly().GetName().CodeBase);

        public static string sDirName = "";
        public static string sFilename = "Trace_";
        static string sFileType = ".log";

        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static void Write(string sMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFilename + sDateYYYYMMDD() + sFileType;
                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                while (true)
                {
                    if (IsOpenFileAllowed(sFile))
                        break;
                }
                
                Write2File(sFile, sFullTime() + " " +
                    sMessage,
                    true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static void Write(string sFileName, string sMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFileName;
                sFile = sFile.Contains(sFileType) ? sFile : sFile + sFileType;

                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                while (true)
                {
                    if (IsOpenFileAllowed(sFile))
                        break;
                }
                Write2File(sFile, sMessage, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void Write(string sPath, string sFileName, string sMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFileName;
                sFile = sFile.Contains(sFileType) ? sFile : sFile + sFileType;

                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                while (true)
                {
                    if (IsOpenFileAllowed(sFile))
                        break;
                }
                Write2File(sFile, sMessage, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static bool isTraceFolderExist()
        {
            return Directory.Exists(sPath + sDirName);
        }

        public static bool IsOpenFileAllowed(string sFilename)
        {
            bool bAllow = false;
            FileStream fs = null;
            try
            {
                fs = File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite);
                fs.Close();
                bAllow = true;
            }
            catch (IOException)
            {
                bAllow = false;
            }
            return bAllow;
        }
        
        /// <summary>
        /// Write Value in string format to text file
        /// </summary>
        /// <param name="sFileName"> string : File name including file path which will be written</param>
        /// <param name="sValue">string : Value to be written to the file</param>
        /// <param name="bNewLine">bool : Value to be using new line on the file</param>
        public static void Write2File(string sFileName, string sValue, bool bNewLine)
        {
            //FileStream oFileStream;
            //StreamWriter oStreamWriter;
            try
            {
                using (FileStream oFileStream = new FileStream(sFileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter oStreamWriter = new StreamWriter(oFileStream))
                    {
                        oStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        if (bNewLine) oStreamWriter.WriteLine(sValue);
                        else oStreamWriter.Write(sValue);
                    }
                    //oStreamWriter.Close();
                    //oStreamWriter.Dispose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //oFileStream.Close();
            //oFileStream.Dispose();
        }
    }

}