﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using InSysClass;

namespace LicenseGenerator
{
    public partial class frmLicenseGenerator : Form
    {
        public frmLicenseGenerator()
        {
            InitializeComponent();
        }
        /*
         * Plain text : <Remain counter, length 10digit><Company name, 50 digit><space filler, 40 digit>
         * Registration Key =
         *      Encrypt3DES("Remain=CounterRemain\nCompany=CompanyName"
         */

        private void frmLicenseGenerator_Load(object sender, EventArgs e)
        {
            for (int i = 1; i <= 12; i++)
                cmbCounter.Items.Add(i.ToString());
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCompanyName.Text))
            {
                if (!string.IsNullOrEmpty(cmbCounter.Text))
                {
                    string sCompanyName = txtCompanyName.Text;
                    string sValidPeriod = string.Format("{0}",
                        int.Parse(cmbCounter.Text) * (rbMonth.Checked ? 30 : 365)
                        * 24 * 60 * 60);
                    string sUnlimited = chkUnlimited.Checked ? "1" : "0";
                    string sRegKey = string.Format("{0}{1}{2}{3}{4}{5}",
                        "".PadLeft(10 - sValidPeriod.Length, '0'), sValidPeriod,
                        "".PadLeft(50 - sCompanyName.Length, ' '), sCompanyName,
                        sUnlimited,
                        "".PadRight(39, ' '));

                    rtbOutput.Clear();
                    rtbOutput.Text = CommonLib.sStringToHex(EncryptionLib.Encrypt3DES(sRegKey));
                }
                else
                    MessageBox.Show("Please choose Valid Until period.");
            }
            else
                MessageBox.Show("Please fill the Company Name.");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }        
    }
}
