﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using InSysClass;

namespace BCA_Migration_316
{
    class Program
    {
        public static string sPathLog = Directory.GetCurrentDirectory();
        public static SqlTransaction oTrans;

        static void Main(string[] args)
        {
            SqlConnection oConnection;
            try
            {
                oConnection = doInitConn();
                Logs.doWriteFile(sPathLog, oConnection.ConnectionString.ToString());
                if (oConnection != null && oConnection.State == ConnectionState.Open)
                {
                    //doMigrationItemList(oConnection);
                    doMigrateTerminalData(oConnection);
                    Console.Read();
                }
                else
                {
                    Console.WriteLine("Failed to Establish Connection. Application will be closed.");
                    Console.Read();
                }
 
            }
            catch (Exception ex)
            {
                Logs.doWriteFile(sPathLog, ex.Message);
                Console.WriteLine(ex.Message);
                Console.Read();
            }
        }

        protected static SqlConnection doInitConn()
        {
            SqlConnection oConn = new SqlConnection();
            try
            {
                InitData oInit = new InitData(Directory.GetCurrentDirectory());
                oConn = SQLConnLib.EstablishConnection(oInit.sGetConnString());
                oConn.InitializeLifetimeService();
            }
            catch (Exception ex)
            {
                oConn = null;
                Logs.doWriteFile(sPathLog, ex.Message);
                Console.WriteLine(ex.Message);
            }
            return oConn;
        }

        protected static void doMigrateTerminalData(SqlConnection oConn)
        {
            try 
            {
                string sText = string.Format("{0} : Start Terminal Migration Process.", DateTime.Now.ToString());
                Logs.doWriteFile(sPathLog, sText);
                Console.WriteLine(sText);

                DataTable dtTerminalList = dtGetTerminalList(oConn);
                if (dtTerminalList != null)
                {
                    foreach (DataRow drRow in dtTerminalList.Rows)
                    {
                        doMigrateProfileTerminal(drRow["TerminalName"].ToString(), drRow["DatabaseID"].ToString(),
                                                drRow["DatabaseName"].ToString(), oConn);
                        sText = string.Format("{0} : {1} Migration Terminal Complete", 
                                                        DateTime.Now.ToString(), drRow["TerminalName"].ToString());
                        Logs.doWriteFile(sPathLog, sText);
                        Console.WriteLine(sText);
                    }
                    sText = string.Format("{0} : {1} Terminals Processed.", DateTime.Now.ToString(),
                                                    dtTerminalList.Rows.Count.ToString());
                    Console.WriteLine(sText);
                    Logs.doWriteFile(sPathLog, sText);
                    Console.Read();
                }
                else
                {
                    Console.WriteLine("Terminal Name not Found.");
                    Console.Read();
                }
            }
            catch (Exception ex)
            {
                Logs.doWriteFile(sPathLog, ex.Message);
                Console.WriteLine(ex.Message);
                Console.Read();
            }
        }

        protected static DataTable dtGetTerminalList(SqlConnection oConn)
        {
            SqlCommand oCommand = new SqlCommand(StoredProcedures.sTerminalMigrateBrowse, oConn);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapt = new SqlDataAdapter(oCommand);
            DataTable dtTable = new DataTable();
            oAdapt.Fill(dtTable);
            oAdapt.Dispose();

            return dtTable;
        }

        protected static void doMigrateProfileTerminal(string sTerminalID, string sDatabaseID, 
                                                        string sDatabaseName, SqlConnection oConn)
        {
            try
            {
                oTrans = oConn.BeginTransaction(DateTime.Now.ToString());

                SqlCommand oCommand = new SqlCommand(StoredProcedures.sTerminalMigration, oConn);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Transaction = oTrans;
                oCommand.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalID;
                oCommand.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDatabaseID;
                oCommand.Parameters.Add("@sDatabaseName", SqlDbType.VarChar).Value = sDatabaseName;
                oCommand.Transaction = oTrans;

                oCommand.ExecuteNonQuery();
                oCommand.Dispose();
                oTrans.Commit();
            }
            catch (Exception ex)
            {
                Logs.doWriteFile(sPathLog, ex.Message);
                Console.WriteLine(ex.Message);
                oTrans.Rollback();
            }
        }

        protected static void doMigrationItemList(SqlConnection oConn)
        {
            try
            {
                string sText = string.Format("{0} : Start Migrating Item List", DateTime.Now.ToString());
                Console.WriteLine(sText);
                Logs.doWriteFile(sPathLog, sText);

                SqlCommand oCommand = new SqlCommand(StoredProcedures.sItemMigration, oConn);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.ExecuteNonQuery();
                oCommand.Dispose();
                sText = String.Format("{0} : Item List Migration Complete.", DateTime.Now.ToString());
                Console.WriteLine(sText);
                Logs.doWriteFile(sPathLog, sText);
                Console.WriteLine("Press Any Key To Continue...");
                Console.Read();
            }
            catch (Exception ex)
            {
                Logs.doWriteFile(sPathLog, ex.Message);
                Console.WriteLine(ex.Message);
            }
        }

    }
}
