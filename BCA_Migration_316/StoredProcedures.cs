﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCA_Migration_316
{
    class StoredProcedures
    {
        public static string sTerminalMigrateBrowse
        { get { return "spMigrateTerminalBrowse"; } }

        public static string sTerminalMigration
        { get { return "spMigrateTerminal"; } }

        public static string sItemMigration
        { get { return "spItemMigrate"; } }
    }
}
