using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using InSysClass;

namespace InSysConsoleNew
{
    public enum ConnType
    {
        TcpIP = 1,
        Serial = 2,
        Modem = 3
    }

    class CommonConsole
    {
        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sTimeHHMMSS()
        {
            return DateTime.Now.ToString("hhmmss", ciUSFormat);
        }

        public static string sDateMMddyy()
        {
            return DateTime.Now.ToString("MMddyy", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = CommonConsole.sFullTime();
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }

        public static string sGenerateISOSend(string sContent, ISO8583_MSGLib oIsoTemp, bool isLast)
        {
            string sIsoResponse;
            ISO8583Lib oIso = new ISO8583Lib();
            oIsoTemp.MTI = "0810";
            oIsoTemp.sBinaryBitMap = "0010000000111000000000010000000000000010100000010000000001010000";
            oIso.SetField_Str(3, ref oIsoTemp, isLast ? "930000" : "930001");
            oIso.SetField_Str(12, ref oIsoTemp, string.Format("{0}", DateTime.Now.ToString("hhmmss", ciUSFormat)));
            oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMddyy}", DateTime.Now));
            oIso.SetField_Str(39, ref oIsoTemp, "00");
            oIso.SetField_Str(58, ref oIsoTemp, "1");            
            oIso.SetField_Str(60, ref oIsoTemp, sContent);
            oIso.SetField_Str(48, ref oIsoTemp, CommonLib.sStringToHex(oIsoTemp.sBit[48]));
            sIsoResponse = oIso.PackToISOHex(oIsoTemp);
            return sIsoResponse;
        }

        public static string sGenerateISOError(string sContent, ISO8583_MSGLib oIsoTemp)
        {
            return string.Format("{0}{1}{2}{3}", oIsoTemp.sTPDU, oIsoTemp.sOriginAddr, oIsoTemp.sDestNII, sContent);
        }

        public static void GetAppNameSN(string sBit48, ref string _sAppName, ref string _sSN)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            //Console.WriteLine("HexBit48: {0}", sHexBit48);
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            //Console.WriteLine("Len 48: {0}", iLenSN.ToString());
            _sSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            //Console.WriteLine("SN: {0}", _sSN);
            int iLenAppName = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2), 4));
            //Console.WriteLine("LenAppName: {0}", iLenAppName);
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2), iLenAppName * 2));
            //Console.WriteLine("AppName: {0}", _sAppName);
        }
    }
    
    class CommonSP
    {
        public static string sSPFlagControlAllowInit = "spControlFlagAllowInitBrowse";
        public static string sSPProcessMessage = "spOtherParseProcess";
        public static string sSPPortInit = "spOtherParsePortInit";
        public static string sSPInitConnBrowse = "spControlFlagInitBrowse";
        public static string sSPInitGetTableInit = "spInitGetTableInit";
        public static string sSPInitLogConnNewInit = "spInitLogConnNewInit";
        public static string sSPAuditInitInsert = "spAuditInitInsert";
        public static string sSPInitLogConnNewInitConsole = "spInitLogConnNewInitConsole";
    }
}