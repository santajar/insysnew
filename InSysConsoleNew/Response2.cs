﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using InSysClass;

namespace InSysConsoleNew
{
    class Response2
    {
        protected SqlConnection oConn;
        protected bool bCheckAllowInit;
        protected ConnType cType;

        protected string sTerminalId;
        protected string sAppName = null;
        protected string sSerialNum = null;
        protected bool bInit = false;
        protected bool bStart = false;
        protected bool bComplete = false;
        protected int iPercentage = 0;

        public string TerminalId { get { return sTerminalId; } }
        public string AppName { get { return sAppName; } }
        public string SerialNum { get { return sSerialNum; } }
        public bool IsProcessInit { get { return bInit; } }
        public bool IsStartInit { get { return bStart; } }
        public bool IsCompleteInit { get { return bComplete; } }
        public int Percentage { get { return iPercentage; } }

        public Response2(string _sConnString, bool _IsCheckAllowInit, ConnType _cType)
        {
            try
            {
                //Console.WriteLine("Create Object Response2");

                oConn = new SqlConnection();
                oConn = SQLConnLib.EstablishConnection(_sConnString);
                bCheckAllowInit = _IsCheckAllowInit;
                cType = _cType;
            }
            catch (Exception ex)
            {
                Trace.Write("Create Object Error: " + ex.Message);
                oConn.Close();
                oConn.Dispose();
            }
        }

        public void Dispose()
        {
            oConn.Close();
            oConn.Dispose();
        }

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public byte[] arrbResponse(byte[] arrbReceive)
        {            
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);
                //Console.WriteLine("Send Message: {0}", sSendMessage);

                if (!string.IsNullOrEmpty(sSendMessage))
                {
                    arrbTemp = new byte[iIsoLen + 2];
                    //arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);

                    sSendMessage = sSendMessage.Replace(" ", ""); // Remove all white space
                    byte[] arrbbuffer = new byte[sSendMessage.Length / 2];
                    for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                    {
                        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);
                    }
                    arrbTemp = arrbbuffer;
                }
            }
            catch (Exception ex)
            {
                Trace.Write("|[RESPONSE]|Error arrbResponse : " + ex.Message);
                //Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTag = null;
            int iErrorCode = -1;
            //Console.WriteLine("Begin Get Response");

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            try
            {
                // parse received ISO
                ISO8583Lib oIsoParse = new ISO8583Lib();
                ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
                //Console.WriteLine("ISO Length: {0}", oIsoMsg.sBit.Length.ToString());
                //Console.WriteLine("ArrbTemp");
                byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
                //Console.WriteLine("Unpack Iso");
                oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
                string sProcCode = oIsoMsg.sBit[3];
                sTerminalId = oIsoMsg.sBit[41];
                //Console.WriteLine("sProcCode = {0}", sProcCode);

                // kirim ke sp,...
                try
                {
                    //if (sProcCode == "950000")
                    //{
                    //}
                    //else 
                    if (sProcCode != "930001")
                    {
                        Trace.Write("|[RESPONSE]|sTemp to SQL Procedure : " + sTemp);
                        if (sProcCode != "930000")
                        {
                            Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

                            #region "NOT 930000"
                            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                            {
                                oCmd.CommandType = CommandType.StoredProcedure;
                                oCmd.CommandTimeout = 25;
                                oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                                oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                                #region "Using DataReader"
                                using (SqlDataReader reader = oCmd.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                        sTerminalId = reader["TerminalId"].ToString();
                                        sTempReceive = reader["ISO"].ToString();
                                        sTag = reader["Tag"].ToString();
                                        string scont = reader["Content"].ToString();
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            iErrorCode = 0;
                            Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

                            #region "930000"
                            if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                            {
                                //Console.WriteLine("AppNameSN {0}", sTemp);

                                CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                            }

                            bInit = true;
                            string sContent = null;
                            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitGetTableInit, oConn))
                            {
                                oCmd.CommandType = CommandType.StoredProcedure;
                                oCmd.CommandTimeout = 20;
                                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;

                                DataTable dtTable = new DataTable();
                                (new SqlDataAdapter(oCmd)).Fill(dtTable);

                                if (dtTable != null && dtTable.Rows.Count > 0)
                                {
                                    ClassInit oClass = new ClassInit(sTerminalId);
                                    oClass.dtResponses = dtTable;
                                    oClass.SerialNumber = sSerialNum;
                                    oClass.AppName = sAppName;

                                    int iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                                    if (iIndex > -1)
                                        Program.ltClassInit.RemoveAt(iIndex);

                                    Program.ltClassInit.Add(oClass);
                                    sTag = oClass.Tag;
                                    sContent = oClass.Content;
                                    oClass.UpdateFlag();
                                    iPercentage = oClass.Percentage;
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sContent = CommonLib.sStringToHex("INVALIDTID");
                                }
                            }

                            Trace.Write(string.Format("|[sContent]|{0}|sTempReceive 930000", sContent));
                            if (iErrorCode == 0)
                            {
                                if (!string.IsNullOrEmpty(sContent))
                                    sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, false);
                                bStart = true;
                            }
                            else
                            {
                                //Console.WriteLine("INVALIDTID");
                                sTempReceive = CommonConsole.sGenerateISOError(sContent, oIsoMsg);
                            }

                            Trace.Write(string.Format("|[sContent]|{0}|sTempReceive 930000", sContent));
                            if (iErrorCode == 0)
                            {
                                if (!string.IsNullOrEmpty(sContent))
                                    sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, false);
                                bStart = true;
                            }
                            else
                            {
                                sTempReceive = CommonConsole.sGenerateISOError(sContent, oIsoMsg);
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

                        #region "930001"
                        //if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                        //{
                        //    Console.WriteLine("AppNameSN {0}", sTemp);

                        //    CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                        //}
                        bInit = true;
                        //Console.WriteLine("TID: {0}", sTerminalId);

                        int iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                        if (iIndex > -1)
                        {
                            iErrorCode = Program.ltClassInit[iIndex].IsLastPacket ? 1 : 0;
                            sTag = Program.ltClassInit[iIndex].Tag;
                            string sContent = Program.ltClassInit[iIndex].Content;
                            bComplete = Program.ltClassInit[iIndex].IsLastPacket;
                            sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete);
                            Program.ltClassInit[iIndex].UpdateFlag();
                            iPercentage = Program.ltClassInit[iIndex].Percentage;
                        }
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(sTempReceive))
                    {
                        // tambahkan total length
                        _iIsoLenResponse = sTempReceive.Length / 2;
                        sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                        Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive : {1}", sTerminalId, sTempReceive));

                        if (iErrorCode == 0 || iErrorCode == 1)
                            CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                    }
                    //Trace.Write(string.Format("|[RESPONSE] {0} {1}", sTerminalId, sProcessing(iErrorCode, sTag)));
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                    //    ex.ToString(), sTemp);
                    Trace.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                        ex.ToString(), sTemp));
                }
            }
            catch (Exception ex)
            { 
                Trace.Write("Parsing Error: " + ex.Message);
            }
            return sTempReceive;
        }

        //protected string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        //{
        //    string sTag = null;
        //    int iErrorCode = -1;
        //    //Console.WriteLine("Begin Get Response");

        //    // convert _arrbRecv ke hexstring,
        //    string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
        //    string sTempReceive = null;

        //    if (cType != ConnType.Modem)
        //    {
        //        int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

        //        // buang total length & tpdu
        //        sTemp = sTemp.Substring(6, (iLen - 1) * 2);
        //    }
        //    else
        //        sTemp = sTemp.Substring(2);

        //    try
        //    {
        //        // parse received ISO
        //        ISO8583Lib oIsoParse = new ISO8583Lib();
        //        ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
        //        //Console.WriteLine("ISO Length: {0}", oIsoMsg.sBit.Length.ToString());
        //        //Console.WriteLine("ArrbTemp");
        //        byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
        //        //Console.WriteLine("Unpack Iso");
        //        oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
        //        string sProcCode = oIsoMsg.sBit[3];
        //        sTerminalId = oIsoMsg.sBit[41];
        //        //Console.WriteLine("sProcCode = {0}", sProcCode);

        //        // kirim ke sp,...
        //        try
        //        {
        //            //if (sProcCode == "950000")
        //            //{
        //            //}
        //            //else 
        //            if (sProcCode != "930001")
        //            {
        //                Trace.Write("|[RESPONSE]|sTemp to SQL Procedure : " + sTemp);
        //                if (sProcCode != "930000")
        //                {
        //                    Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

        //                    #region "NOT 930000"
        //                    using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
        //                    {
        //                        oCmd.CommandType = CommandType.StoredProcedure;
        //                        oCmd.CommandTimeout = 25;
        //                        oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
        //                        oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

        //                        #region "Using DataReader"
        //                        using (SqlDataReader reader = oCmd.ExecuteReader())
        //                        {
        //                            while (reader.Read())
        //                            {
        //                                iErrorCode = int.Parse(reader["ErrorCode"].ToString());
        //                                sTerminalId = reader["TerminalId"].ToString();
        //                                sTempReceive = reader["ISO"].ToString();
        //                                sTag = reader["Tag"].ToString();
        //                                string scont = reader["Content"].ToString();
        //                            }
        //                        }
        //                        #endregion
        //                    }
        //                    #endregion
        //                }
        //                else
        //                {
        //                    iErrorCode = 0;
        //                    Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

        //                    #region "930000"
        //                    if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
        //                    {
        //                        //Console.WriteLine("AppNameSN {0}", sTemp);

        //                        CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
        //                    }

        //                    bInit = true;
        //                    string sContent = null;
        //                    using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitGetTableInit, oConn))
        //                    {
        //                        oCmd.CommandType = CommandType.StoredProcedure;
        //                        oCmd.CommandTimeout = 20;
        //                        oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;

        //                        DataTable dtTable = new DataTable();
        //                        (new SqlDataAdapter(oCmd)).Fill(dtTable);

        //                        ClassInit oClass = new ClassInit(sTerminalId);
        //                        oClass.dtResponses = dtTable;
        //                        oClass.SerialNumber = sSerialNum;
        //                        oClass.AppName = sAppName;

        //                        int iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
        //                        if (iIndex > -1)
        //                            Program.ltClassInit.RemoveAt(iIndex);

        //                        Program.ltClassInit.Add(oClass);
        //                        sTag = oClass.Tag;
        //                        sContent = oClass.Content;
        //                        oClass.UpdateFlag();
        //                        iPercentage = oClass.Percentage;

        //                        Trace.Write(string.Format("|[sContent]|{0}|sTempReceive 930000", sContent));
        //                    }
        //                    Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive 930000", " "));
        //                    if (!string.IsNullOrEmpty(sContent))
        //                        sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, false);
        //                    bStart = true;
        //                    #endregion
        //                }
        //            }
        //            else
        //            {
        //                Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

        //                #region "930001"
        //                //if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
        //                //{
        //                //    Console.WriteLine("AppNameSN {0}", sTemp);

        //                //    CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
        //                //}
        //                bInit = true;
        //                //Console.WriteLine("TID: {0}", sTerminalId);

        //                int iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
        //                if (iIndex > -1)
        //                {
        //                    iErrorCode = Program.ltClassInit[iIndex].IsLastPacket ? 1 : 0;
        //                    sTag = Program.ltClassInit[iIndex].Tag;
        //                    string sContent = Program.ltClassInit[iIndex].Content;
        //                    sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, Program.ltClassInit[iIndex].IsLastPacket);
        //                    Program.ltClassInit[iIndex].UpdateFlag();
        //                    iPercentage = Program.ltClassInit[iIndex].Percentage;
        //                }
        //                #endregion
        //            }
        //            if (!string.IsNullOrEmpty(sTempReceive))
        //            {
        //                // tambahkan total length
        //                _iIsoLenResponse = sTempReceive.Length / 2;
        //                sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
        //                Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive : {1}", sTerminalId, sTempReceive));

        //                if (iErrorCode == 0 || iErrorCode == 1)
        //                    CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
        //            }
        //            //Trace.Write(string.Format("|[RESPONSE] {0} {1}", sTerminalId, sProcessing(iErrorCode, sTag)));
        //        }
        //        catch (Exception ex)
        //        {
        //            //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
        //            //    ex.ToString(), sTemp);
        //            Trace.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
        //                ex.ToString(), sTemp));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Trace.Write("Parsing Error: " + ex.Message);
        //    }
        //    return sTempReceive;
        //}

        protected int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            if (cType == ConnType.TcpIP)
                iReturn = CommonLib.iHexStringToInt(sTempLen);
            else if (cType == ConnType.Serial)
                iReturn = int.Parse(sTempLen);
            return iReturn;
        }

        protected string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP || cType == ConnType.Modem)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Initialize Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Initialize Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                default:
                    sTemp = "Unknown Error";
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
            }
            return sTemp;
        }
    }
}
