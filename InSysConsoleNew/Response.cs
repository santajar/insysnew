using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using InSysClass;

namespace InSysConsoleNew
{
    class Response
    {
        protected static SqlConnection oConn;
        protected static bool bCheckAllowInit;

        protected static int iCountInit = 0;
        public static int iMaxInit = 32;

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public static byte[] arrbResponse(byte[] arrbReceive, ConnType cType, SqlConnection _oConn, bool _IsCheckAllowInit)
        {
            oConn = _oConn;
            bCheckAllowInit = _IsCheckAllowInit;
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);
                if (!string.IsNullOrEmpty(sSendMessage))
                {
                    arrbTemp = new byte[iIsoLen + 2];
                    //arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);

                    sSendMessage = sSendMessage.Replace(" ", ""); // Remove all white space
                    byte[] arrbbuffer = new byte[sSendMessage.Length / 2];
                    for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                    {
                        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);
                    }
                    arrbTemp = arrbbuffer;
                }
            }
            catch (Exception ex)
            {
                Logs.doWriteErrorFile("|[RESPONSE]|Error arrbResponse : " + ex.Message);
                //Console.WriteLine("[RESPONSE] Error : {0}", ex.Message);
                //Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected static string sBeginGetResponseOld(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTerminalId = null;
            string sTag = null;
            int iErrorCode = -1;

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            Trace.Write("|[RESPONSE]|sTemp to SQL Procedure : " + sTemp);

            // kirim ke sp,...
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 27;
                    //oCmd.CommandTimeout = 0;
                    oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                    oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                    #region "Using DataReader"
                    //SqlDataReader reader;
                    using (SqlDataReader reader = oCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                            sTerminalId = reader["TerminalId"].ToString();
                            sTempReceive = reader["ISO"].ToString();
                            sTag = reader["Tag"].ToString();
                            string scont = reader["Content"].ToString();

                            //Trace.Write("[RESPONSE] Cont : " + scont);
                        }
                    }
                    //reader.Close();
                    //reader.Dispose();
                    #endregion
                }
                //oCmd.Dispose();

                if (!string.IsNullOrEmpty(sTempReceive))
                {
                    // tambahkan total length
                    _iIsoLenResponse = sTempReceive.Length / 2;
                    sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                    Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive : {1}", sTerminalId, sTempReceive));

                    if (iErrorCode == 0 || iErrorCode == 1)
                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                }
                //Trace.Write(string.Format("[RESPONSE] {0} {1}", sTerminalId, sProcessing(iErrorCode, sTag)));
            }
            catch (Exception ex)
            {
                //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                //    ex.ToString(), sTemp);
                Logs.doWriteErrorFile(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                    ex.ToString(), sTemp));
                //Console.ReadKey();
                //sTemp = null;
            }
            return sTempReceive;
        }
        protected static string sBeginGetResponseBatch(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTerminalId = null;
            string sAppName = null;
            string sSerialNum = null;
            string sTag = null;
            int iErrorCode = -1;

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            // parse received ISO
            ISO8583Lib oIsoParse = new ISO8583Lib();
            ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
            byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
            oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
            string sProcCode = oIsoMsg.sBit[3];
            sTerminalId = oIsoMsg.sTerminalId;
            
            Console.WriteLine("sProcCode = {0}", sProcCode);

            // kirim ke sp,...
            try
            {
                if (sProcCode != "930001")
                {
                    Trace.Write("|[RESPONSE]|sTemp to SQL Procedure : " + sTemp);
                    if (sProcCode != "930000")
                    {
                        #region "NOT 930000/1"
                        using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                        {
                            oCmd.CommandType = CommandType.StoredProcedure;
                            oCmd.CommandTimeout = 27;
                            //oCmd.CommandTimeout = 0;
                            oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                            oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                            #region "Using DataReader"
                            using (SqlDataReader reader = oCmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                    sTerminalId = reader["TerminalId"].ToString();
                                    sTempReceive = reader["ISO"].ToString();
                                    sTag = reader["Tag"].ToString();
                                    string scont = reader["Content"].ToString();
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        #region "930000"
                        string sContent = null;
                        using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitGetTableInit, oConn))
                        {
                            oCmd.CommandType = CommandType.StoredProcedure;
                            oCmd.CommandTimeout = 20;
                            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;

                            DataTable dtTable = new DataTable();
                            (new SqlDataAdapter(oCmd)).Fill(dtTable);

                            CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);

                            ClassInit oClass = new ClassInit(sTerminalId);
                            oClass.dtResponses = dtTable;
                            oClass.SerialNumber = sSerialNum;
                            oClass.AppName = sAppName;

                            Program.ltClassInit.Add(oClass);
                            sContent = oClass.Content;
                            oClass.UpdateFlag();
                        }
                        Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive 930000", " "));
                        if (!string.IsNullOrEmpty(sContent))
                            sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, false);
                        #endregion
                    }
                }
                else
                {
                    #region "930001"
                    int iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);

                    sTempReceive = CommonConsole.sGenerateISOSend(Program.ltClassInit[iIndex].Content, oIsoMsg, Program.ltClassInit[iIndex].IsLastPacket);
                    Program.ltClassInit[iIndex].UpdateFlag();

                    Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive 930001"));
                    #endregion
                }
                if (!string.IsNullOrEmpty(sTempReceive))
                {
                    // tambahkan total length
                    _iIsoLenResponse = sTempReceive.Length / 2;
                    sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                    Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive : {1}", sTerminalId, sTempReceive));

                    if (iErrorCode == 0 || iErrorCode == 1)
                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                }
                //Trace.Write(string.Format("|[RESPONSE] {0} {1}", sTerminalId, sProcessing(iErrorCode, sTag)));
            }
            catch (Exception ex)
            {
                //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                //    ex.ToString(), sTemp);
                Logs.doWriteErrorFile(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                    ex.ToString(), sTemp));
                //Console.ReadKey();
                //sTemp = null;
            }
            return sTempReceive;
        }
        protected static string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTerminalId = null;
            string sTag = null;
            int iErrorCode = -1;

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            // parse received ISO
            ISO8583Lib oIsoParse = new ISO8583Lib();
            ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
            byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
            oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
            string sProcCode = oIsoMsg.sBit[3];
            sTerminalId = oIsoMsg.sTerminalId;

            Console.WriteLine("sProcCode = {0}", sProcCode);

            // kirim ke sp,...
            try
            {
                Trace.Write("|[RESPONSE]|sTemp to SQL Procedure : " + sTemp);
                if (sProcCode == "950000")
                {
                }
                else if (sProcCode != "930000" && sProcCode != "930001")
                {
                    #region "NOT 930000/1"
                    using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                    {
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.CommandTimeout = 27;
                        //oCmd.CommandTimeout = 0;
                        oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                        oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                        #region "Using DataReader"
                        using (SqlDataReader reader = oCmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                sTerminalId = reader["TerminalId"].ToString();
                                sTempReceive = reader["ISO"].ToString();
                                sTag = reader["Tag"].ToString();
                                string scont = reader["Content"].ToString();
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                else
                {
                    #region "930000/1"
                    //if (iCountInit < iMaxInit)
                    using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                    {
                        iCountInit++;
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.CommandTimeout = 27;
                        //oCmd.CommandTimeout = 0;
                        oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                        oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                        #region "Using DataReader"
                        using (SqlDataReader reader = oCmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                sTerminalId = reader["TerminalId"].ToString();
                                sTempReceive = reader["ISO"].ToString();
                                sTag = reader["Tag"].ToString();
                                string scont = reader["Content"].ToString();
                            }
                        }
                        #endregion
                        iCountInit--;
                    }
                    #endregion
                }
                if (!string.IsNullOrEmpty(sTempReceive))
                {
                    // tambahkan total length
                    _iIsoLenResponse = sTempReceive.Length / 2;
                    sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                    Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive : {1}", sTerminalId, sTempReceive));

                    if (iErrorCode == 0 || iErrorCode == 1)
                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                }
                //Trace.Write(string.Format("|[RESPONSE] {0} {1}", sTerminalId, sProcessing(iErrorCode, sTag)));
            }
            catch (Exception ex)
            {
                //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                //    ex.ToString(), sTemp);
                Logs.doWriteErrorFile(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                    ex.ToString(), sTemp));
                //Console.ReadKey();
                //sTemp = null;
            }
            return sTempReceive;
        }

        protected static int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            if (cType == ConnType.TcpIP)
                iReturn = CommonLib.iHexStringToInt(sTempLen);
            else if (cType == ConnType.Serial)
                iReturn = int.Parse(sTempLen);
            return iReturn;
        }

        protected static string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP || cType == ConnType.Modem)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected static string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Initialize Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Initialize Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                default:
                    sTemp = "Unknown Error";
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected static string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
            }
            return sTemp;
        }
    }
}
