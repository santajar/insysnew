﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using InSysClass;

namespace InSysConsoleNew
{
    class Program
    {
        /*
         * The changes on this program will affect only on profile initialization
         * as soon as the console receive message, it will be parse(on console app side, not sql side)
         * and then check the proccode, if init proccode 930000 request inittable
         * if proccode 930001 search existing
         */
        static bool bCheckAllowInit;
        static SqlConnection oConn = new SqlConnection();
        static string sConnString;
        static string sAppDirectory = Directory.GetCurrentDirectory();

        public static List<ClassInit> ltClassInit = new List<ClassInit>();

        #region "TCP/IP Connection"
        static bool IsConnTcpIpActive;
        static Thread threadIP;
        #endregion

        #region "Dial-Up Modem Connection"
        static bool IsConnModemActive;

        static string sModemPort1;
        static string sModemPort2;
        static string sModemPort3;
        static string sModemPort4;
        static string sModemPort5;

        static SerialModem oSerModem1;
        static SerialModem oSerModem2;
        static SerialModem oSerModem3;
        static SerialModem oSerModem4;
        static SerialModem oSerModem5;
        #endregion

        //static void Main(string[] args)
        //{
        //    string[] arrsCompressed = new string[]{
        //        "1F8B0800000000000400EDBD07601C499625262F6DCA7B7F4AF54AD7E074A10880601324D8904010ECC188CDE692EC1D69472329AB2A81CA6556655D661640CCED9DBCF7DE7BEFBDF7DE7BEFBDF7BA3B9D4E27F7DFFF3F5C6664016CF6CE4ADAC99E2180AAC81F3F7E7C1F3F229E9EEEECEE1C9CFCDE3B78F6E8AFBDDD9D37A7AFDFA44FCF8E9FA7F4F7BD9D7BA75F9ED02FFB3B0FBE73FC7B1DBF7A734C7FDCDFD97F75FAE639FDF6E9CEFE437AE8B7073BBBF871B073B077EFFEC1C1EEA707F4D7C39DFD7B9FDEFBF4E9E9EECECEEE2EFDD8951F7BF2E3DECE2EB5DAFD9490A0",
        //        "1F0FE4C303F9EB21F5BD43BFECEDF0DF7BF2E6DEFECEA70FF9A1DFEF9BCEF73E952F05C2DEC1CEFD870F793CF7F6F9937BF719C6BD4F77F600F2DE031A277DBBBF4B7FDFA39F7B06D0FE3D6EBF7F5F7E08D87D01BB7F203F1EF28FFB32A0FB82D6FD3DEEE0FE3DF921BDDE1728F76578F71FC80F8172FF21FFF5A90CEED35DF921503E15243E95D73F15243E95D73F95D73F15241E08120F048907F2FA837B342BFCD0EFFBDEEFF7BDDF3FF57E7FE0FD2EE01F08F803017F20D81D08F803C1EE40C67820481E08920742A983036297870F89B20732CC8732CC8702E8E19E4CC3C3",
        //        "7BFA739F9A73F70FEFDBDF3EB5BFC9C01F326627F4BB208BDFF7BCDFEFEDEEEDEEEE3DA47F1FDCDFDFBB7F8C96F78E9F3FA75F8875DC738CA63BC24278E8EFFB40837E82F1E807C67E7CC20C4B3F76E5C79EFCB8273FF6E5C77DF9F1A9FC78203F0EE48740D913287B02656F4F7A209123CCD293E3574FDF0FBD4F7F48E87D7AFCC5E9EF7D6F5F90BBC713E190BBB71F22B77B5F90BB05ED76BE39E41E28720F3AC83DF839456E77E7C9C9314F6CBA2B08BAC91504492F1E7CEA23F8E9CF15827B8A2021F480B154041FD0DFFFAF40F09E2248081DF8081E1C3CFC7F07822A2040",
        //        "2840B023BDEF81207E7C28823B0782E0EB6F0B7A3BBBFBCA84821EFDBD1743EF16CAE51BA1DF2ED07B7AFAE4EC8D9111C2E85E07C32801376108D46EC010DF0D61B82318EE76315421D9B1826C30DC796F1A7E3318F668A85242401EFA18CADF3F17180A0D594A9E7E4906F8959DE44011E2EF9F1B120A827B40F0E5F3E337672FBEFAE2D8A9C28EAE7E10A1E12D04F99B9394D7BFCFF1ABB36395E5FB60BBFB0E43F9FBE782888AE1C9975F3C394B9F3D3FFEA99F521AEA247F03CAF09BA0E1CEC1D3B317A7AF5EDFDB11F454CF58F4EE595F4BD1DBFFB941EF53454F3D718B9E",
        //        "063B3FD7E81D287A071DF40E7E4ED1A380E0ECF5C9973F79FA2AFD54E717407C0CF1F7CF21FBF918AAB7F0E97EA8073FEDBAD33F7718DE570CEF2B8206C3FB3FB718DE3B25EC7E9FA7C7BF8F1731B1CFE5E1287FFF9CE1B8F39055604A0A5BF023EBA65414FCE46F87DF7B049CDF047EBBBB821FBB0C56523ED861F80631DCB9FF9D9327C621BC777F4F26D7E899FB9FEEF92E7F877AE87F477EECCA8F1E6EF8715F7E7C2A3F1C6EF84BA0EC0994386EEA0A0297835D1F37A3047F6E707B00DC2CDF31761DCA6D908B9F6DEC76EF7F71FCFA0DE996D0D1BABF7F6F6F9FD1142CE5",
        //        "EF9F3B2CF714CBEF7EF9EAB92A98FB7BE459798494BF7FEE50DC11149D06B44ADA206894F4FBF3217E7C30823AD31C18133BAA24DFDFDFBD476DBC99C6DF3F7764EC60A9324D095BCA1CFA58E2EF9F332C771EBEFE8232ED2E84E7148837D7F2F7CF9DB5DBFFC9B3D7C7829A4D0E2A6AD69BF9B961C39D03A0E6F4E1FEFDFB9FDEF36696FFBEF77336B3BB4CB9F4E4CB572FBF7C75FCE654917CF0E0DE030E3C1549FEFBE70EC97B8CE4D98B67672FCE1C8E7B9FEE73746270C4DF3FD7388666657FFF7E4847FEFBE70E4799ECD7679FBF387EF3D52B9F90C164E3EF9F1B244FE9",
        //        "9F7DA4D3E937068B053FFE7C5F7EDC971F9FCA770FE4AF03F9EBA159363AC6EADE1EFF641479790F3F58A64F45DD9C0A8AA7A26EB0D8B7AFEF0A38D138BCDA871FBBF24390DABB4764C14FC16A8F151716FDB8D37B82F23DF9F29EE81C5DEBA39F92DBFED97FA8AB831F5E570F6574FB4A785EC9C4CF3DFD794F7FEEEB4FA5CABE528518507E1EE84FA1FE7DE6115ED4C40FA1FE7DA1EF7D99C6FB42FCFB328DF799D3C046F7A072FF5FC1453B1BB868E7475C641F70119650958B1E28171D28173D142E02357E785C74009F27EDB2127E3856C25F9FCA5F8E95F0E39B67A5DD0D",
        //        "AC442B8B3F62257DFEDFA590C00B94CF387975FA94921DFFAFD0491B1969E7478C649EFF773112788182314E9AFD7F4429EDFD8897CCF3FF2E5E022F905292F594FF0F28A54F7FC448E6F97F1723811782F58661A5A4BC841F1FCC4BF8F17579E9C1265EDA155EDA555EDA155E3A30BC74B07BB07770EF60FFE0FEC1A707487B210BBBFB70EFE1BD87FBB4F2FDE9C3070FF1D1073D3FE225F0D20965967F38BC840FBF2E2F1DBC072FED082F5166E5477AE987C44BF76835EAFFFDD60DC1ED8FAC9B3C3F675CB4235CB4235CB4E373915DD3FC7F0527ED6CE2A4FD1F7192799493",
        //        "7EF809A54D9CF4C5F1E9EB37AFBEDC60D9F0D7FF1B42B71F71927DFE5FA893CC02FCFF47D4D2BD1F319379FE5FA8960C339D1C777CEE9F3366DAA49940AA1F31933CFF2FD44C3B0F5F7F71FCEA0D564EFEDFC14B3B9B78E947AB26F6F939534CBBC24BBBC24BBB212F7DF7ECCDC9B7CF5E7C1EF0127EFCBFCF63FA9191B3CFCF995E8AF21278617767FF27CF5E1FFFBF43256D62A39D8DE9C9FF7FB3D1FF6BD86847D86847D86847D808BCB0BB730036FAFF88DBFDA3E537FBFC9C59B70DACF48059E9F54FFC7F80937EB4FE661FE124D0009C84256E7012AC3F38097913E6A4FB",
        //        "3F2C4E3AA6DFF78FBF38FDBDE9378AB47728D83EA61F863D8E77F677EFFF3EE1431FDEDF39F8BDF5A1BF3EE5860FF8DF0346FE780783A49FBB0C84780BFFEEE967F7F427A1B3473F685CF7F0F5037AD950E99858CBFFEBA1FFD79EB0ECF11E649086B027EAFC784FD4F9B1F0DAF11E08471DECF1788FF718C13DF6EE8EF71E62ACF4CB3D26DBF1BD5DF941889981D9F109931EDFC37C506FF7988AC7F7581840BE7B50E742BD7BF8C4500FBF13F5F8F7FB3B0FF1E183FB0F847A27BF377FBCC7D43BD8BB77FFE060F7D303A6E2C1DEFDFB9FA271949A5E5BA2AAD7F66787BAF736",
        //        "5317C4BF257541B93DBF1F59E30AC8BAEB93F5FED3D327676F0C61F7A3843D38E812F6E9EFCD4DFE5F4F5861DB7DFD2642DAFDF724ED00E37628BCE353F8D3A7672F4E5FBD36920F9A85920FA1D81D967A22FABDFBF71E82F20FF88F870F1EE08F3E715D4B82E95AFE6C92769067C1D2B724ECCE06C2EE6C20EC012F427B6AE1D31E69E3DC7BFA7B730BE1DE4FA1D29577DDEF7DE2DAEF88B6EEF7086977A3A4C5A720AD1A404BDAFBF21748CBF4F1B976D712F7BE10175FDCD73F6F22EEAE10F7EB72ED8397AFBEFCE2CB5469BB8BD14468DB57B92F7EEF6347DBFFD76B8641F6",
        //        "FD54288C5604FFECC5EB37E9ABD3CF7F56F48352FA89A1343E8D537A836BF0FF5D4A3FF029FDE05EFAE4ABE7C72F7E36097D6208DDD7C44AE88E0FF6C5EF6DFFFAFF32A10F02427FFAB34FE8A786D07D6757B4E0FDC75F9C3D3D32447EFAFF076E7E1810D9D2E1678FC8A786C8BEEB364CE4539FC88EAC0F0292C788ECC8BA1B90FCE780C8FCB34BE4D39F0D22EFEE08F0D73F911E1B3A833071F5ECC082D63FF17B1FFBB4FEFF284333917F3886D023B6B5859F0E123B54D13FE148FFB34F6CFEFA6785D81A462B63FF2CDA428FD6D61CC281BB1DAD4FFEFF406B0D5094D63F8B",
        //        "E6D0A3B5B58820D74DCAFA277E9816F1678FCE1A6107CAFA67C5227A74B646F1E12DE8FCC5EFFD1D9FCEFF5F358AF72374FECE374EE7A7A03385DCA4365E3D4DF1F79E64E69E2200975F76BDF5177CEFFFE51AEDDEFFE2F8F59BD3570C883E6358BB3BF261AFF5CE83EF9C3CB110BD3FBC260F5F7F71FCEACDC9F1EB6F73A3E04FAFDF7B8CCED98B67672FCEDE9C0A3C93E40FE0C96224BE37BF7950145160EF23AE7F7B50EE03572514FD1A7EF92972C2E4BA71279C1FC6D7E63722A4E48E886BD0C26492D0C6FDBEBBBB8B09E13C9E76F450FE3014D21C1F033C7EFE3C3548FB",
        //        "4BF7613B6F8EF704F5DEF4B916F7065A742698010D4FB090E2C18DA4F87413296CAB83DB124C86374830BFE9BDCD4D7764921E7C717CFAFACDAB2FBBDF3EE46C9565E1E0CF7B5E2E0B9D32BFA5275FBE7AF9E5AB63E5D10129126E7EF9FCF8CDD98BAFBED8D452A723683B381DBBBB275F7EF1E42C65B47C844F94BF038475E83FB4260323DC7D7AF6FAE4CB9FA4E17C7A83C0B886FBB76D787F734341DA4CF70DF3BBA754FFEE97AF9E07EA233226F0DFEBDFE7F8D5D9B1AAB561D9168954F57780DF621CCD82FBF4CBE7CF8F5F6D06E889B85089910AB0DB438B80F9FA6DEE9D",
        //        "12FD7E9FA7C7BF8FD33B7D8B718FF49210F8E4D5E953EEDF0321F2F0FAECF317C76FBE7AB54967B3B65595E56BDB37CF2182C7273F01F3B7B34B7FC294E2277D2F1FEC9B5FEE5B138CBFC8ECDDA31FBC1A7B1F1F1CD8DF1E0A0802786F77EF807ED9DDBBFFE9C367070F9EED9063707C72EFFEA7C79F1EDC7F766FF7FED3879FDEDF7F767FEFFED383FDDD8727BB3B4F1F10605AA87D46AB87FB4F4F895FE9EB4F3F3DB9777270EFE1292D3EEE9D3C7BBA7FB07FEF84FC72D29D3B273BC7274F4F1F3CD83D79F0E993E3BD674FEFDDDFDF7F408BC6A7CF76EF3F79F8E9C3FD67CF",
        //        "769F3D79F87497DADD7FF260F7D9E9B3070F1F3CDCBFF7E90925896998B4B27C7FF7C9D38387CF9EEDDD3FD9DFBBF7E9D3BDE3FBBB4F9FD0941F3FA06F9EEC9E9C9C3E79B077F2E9B3FDD387F7EEDDDBBB7FFA747FEF60FFFEDED3074F4EF6EF3DB877F0903CE7FDD3DD2707274F4EEE3DDB393EB87FFCE0C9CEC9C17DFAEADEE9BDD3674406B83F9FD24F02706C2879B0FFE4D3FB9F3EB97FEFD3DD4F9FCA47F4DF1EB5DAC754D04F7616E927BCA4FBF49368B08F9FE478DDC7F744EDFBF7651EDF3C3F7D7AFA93BFD7E9EFA3F3B8F7B3318FB43A7FFAE4F8F4E0F4D3E3874F1E",
        //        "DEBB7F42DE1DCDD6DEDE83A79FD260F69F9D3E38BEF78438F60911FCE193639A98FDA70F1FEC9D9E1C9F7E0A57EBF8DEB393A7FB3BF70F1E3C3C7E76EF64E7E9BD934FE9E5870F9E7D7A7AF0F4F460EF84D6ED9FEC3CB9FF84BC3212A5D39D673B4F1FDE7FF6ECD9C153FAE0297D7DF2E98327C74F766885FE80E6EC21F1C8EE83FD5DE22F22C5339A83A77B7BC74F3E85C779FF60EFF4D9FD7B7BFBE4F49E1CDF7FF8E053D2F8CF1ED23A38F1CA2E10BCB7F3ECD9FD9DE367A7274FEFED3EB977F0EC60E774EFE0E0C19327FB84F4E9DE937BC418C79FDE7B7A72EFF860FF6779",
        //        "1EFF1F4659AFBB46640000"};

        //    int iCount = 0;
        //    Console.WriteLine("sHexToStringUTF7Loop");
        //    foreach (string sTempContent in arrsCompressed)
        //    {
        //        string sAscii = CommonLib.sHexToStringUTF7Loop(sTempContent);
        //        Console.WriteLine("{0}. sTempContent.Length={1}, sAscii.Length={2}. {3}",
        //            iCount, sTempContent.Length, sAscii.Length, (sTempContent.Length / 2 == sAscii.Length).ToString());
        //        iCount++;
        //    }

        //    iCount = 0;
        //    Console.WriteLine("sHexToStringUTF7");
        //    foreach (string sTempContent in arrsCompressed)
        //    {
        //        string sAscii = CommonLib.sHexToStringUTF7(sTempContent);
        //        Console.WriteLine("{0}. sTempContent.Length={1}, sAscii.Length={2}. {3}",
        //            iCount, sTempContent.Length, sAscii.Length, (sTempContent.Length / 2 == sAscii.Length).ToString());
        //        iCount++;
        //    }
        //    Console.Read();
        //}

        static void Main(string[] args)
        {
            try
            {
                oConn = InitConnection();

                if (oConn != null && oConn.State == ConnectionState.Open)
                {
                    InitControlAllowInit();
                    AsyncSocket.oConn = oConn;
                    AsyncSocket.bCheckAllowInit = bCheckAllowInit;
                    AsyncSocket.sConnString = sConnString;

                    SerialModem.oConn = oConn;
                    SerialModem.bCheckAllowInit = bCheckAllowInit;

                    InitConnType();

                    // Start the TcpIp thread
                    if (IsConnTcpIpActive)
                    {
                        InitThreadTCPIP();

                        threadIP = new Thread(new ThreadStart(AsyncSocket.StartListening));
                        threadIP.Start();

                        CommonConsole.WriteToConsole("Ready...");
                    }

                    // Start the Modem thread
                    if (IsConnModemActive)
                    {
                        InitSerialModem();
                        //OpenSerialModem();
                        CommonConsole.WriteToConsole("Ready...");
                    }
                }
                else
                {
                    CommonConsole.WriteToConsole("Database Connection Failed!...");
                    Console.WriteLine("Press ENTER to Terminate or R to Restart the program...");
                    string sKey = Console.ReadLine();
                    if (sKey.ToUpper() == "R")
                    {
                        Process oInSys = new Process();
                        oInSys.StartInfo.FileName = sAppDirectory + "\\InSysConsole.exe";
                        oInSys.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                //Logs.doWriteErrorFile("[Main] Error : " + ex.Message);                
                //CommonConsole.WriteToConsole(ex.Message);
                //Console.Read();
                //Process oInSys = new Process();
                //oInSys.StartInfo.FileName = sAppDirectory + "\\InSysConsole.exe";
                //oInSys.Start();
            }
        }

        #region "Function"
        /// <summary>
        /// Initialize all the needed variable
        /// </summary>
        static void InitSocketClass()
        {
            int iPort = 1800;
            int iMaxConn = 1000;
            InitSocketControl(ref iMaxConn, ref iPort);
            AsyncSocket.iPort = iPort;
            AsyncSocket.iConnMax = iMaxConn;
        }

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception ex)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }

        /// <summary>
        /// Get the Port number, Max simultan connections configuration
        /// </summary>
        static void InitSocketControl(ref int _iConnMax, ref int _iPortInit)
        {
            int _iPortDownload;
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPInitConnBrowse, oConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sMaxConn", SqlDbType.VarChar).Value = "InitMaxConn";
                oSqlCmd.Parameters.Add("@sInitPort", SqlDbType.VarChar).Value = "PortInit";
                oSqlCmd.Parameters.Add("@sSoftwarePort", SqlDbType.VarChar).Value = "PortDownload";

                if (oConn.State != ConnectionState.Open)
                {
                    oConn.Close();
                    oConn.Open();
                }

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                    {
                        _iConnMax = int.Parse(oRead["InitMaxConn"].ToString());
                        _iPortInit = int.Parse(oRead["PortInit"].ToString());
                        _iPortDownload = int.Parse(oRead["PortDownload"].ToString());
                    }

                    oRead.Close();
                }
            }
        }

        /// <summary>
        /// Set the Allow Initialization configuration
        /// </summary>
        static void InitControlAllowInit()
        {
            bCheckAllowInit = bGetControlAllowInit();
        }

        /// <summary>
        /// Get the Allow Init configuration from the database
        /// </summary>
        /// <returns>bool : boolean allow to initialize or not</returns>
        static bool bGetControlAllowInit()
        {
            bool bTemp = false;
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();

                SqlCommand oCmd = new SqlCommand(CommonSP.sSPFlagControlAllowInit, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter oda = new SqlDataAdapter(oCmd);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                if (dt.Rows.Count > 0)
                    bTemp = Convert.ToInt16(dt.Rows[0][0].ToString()) == 1 ? true : false;

                oCmd.Dispose();
                oda.Dispose();
                dt.Dispose();
            }
            catch (Exception ex)
            {
                //Logs.doWriteErrorFile("[MAIN] InitControlAllowInit : " + ex.Message);
            }
            return bTemp;
        }

        static void InitThreadTCPIP()
        {
            InitSocketClass();
        }

        static void InitConnType()
        {
            InitConsoleConnType oConnType = new InitConsoleConnType(sAppDirectory);
            IsConnTcpIpActive = oConnType.IsConnTcpIpActive();
            IsConnModemActive = oConnType.IsConnModemActive();

            sModemPort1 = oConnType.Modem1;
            sModemPort2 = oConnType.Modem2;
            sModemPort3 = oConnType.Modem3;
            sModemPort4 = oConnType.Modem4;
            sModemPort5 = oConnType.Modem5;
        }

        static void InitSerialModem()
        {
            if (!string.IsNullOrEmpty(sModemPort1))
                oSerModem1 = new SerialModem(sModemPort1);
            Thread.Sleep(100);
            if (!string.IsNullOrEmpty(sModemPort2))
                oSerModem2 = new SerialModem(sModemPort2);
            Thread.Sleep(100);
            if (!string.IsNullOrEmpty(sModemPort3))
                oSerModem3 = new SerialModem(sModemPort3);
            Thread.Sleep(100);
            if (!string.IsNullOrEmpty(sModemPort4))
                oSerModem4 = new SerialModem(sModemPort4);
            Thread.Sleep(100);
            if (!string.IsNullOrEmpty(sModemPort5))
                oSerModem5 = new SerialModem(sModemPort5);
            Thread.Sleep(100);
        }

        static void OpenSerialModem()
        {
            if (!string.IsNullOrEmpty(sModemPort1))
                oSerModem1.Open();
            Thread.Sleep(100);
            if (!string.IsNullOrEmpty(sModemPort2))
                oSerModem2.Open();
            Thread.Sleep(100);
            if (!string.IsNullOrEmpty(sModemPort3))
                oSerModem3.Open();
            Thread.Sleep(100);
            if (!string.IsNullOrEmpty(sModemPort4))
                oSerModem4.Open();
            Thread.Sleep(100);
            if (!string.IsNullOrEmpty(sModemPort5))
                oSerModem5.Open();
            Thread.Sleep(100);
        }
        #endregion
    }
}
