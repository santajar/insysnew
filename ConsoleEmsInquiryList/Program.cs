﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using EmsXml = ipXML.EMS_XML;
using EmsCommon = ipXML.EMSCommonClass;
using Ini;
using InSysClass;

namespace ConsoleEmsInquiryList
{
    class Program
    {
        static void Main(string[] args)
        {
            string sPathStartup = Environment.CurrentDirectory;
            string sPathResult = Environment.CurrentDirectory + @"\RESULT";
            string sPathLog = Environment.CurrentDirectory;

            //InitConfig(ref sPathLog, ref sPathResult, ref sDateFrom, ref sDateTo);
            try
            {
                string sConnString = new InitData(sPathStartup).sGetConnString();
                //SqlConnection oSqlConn = new SqlConnection(new InitData(sPathStartup).sGetConnString());
                //oSqlConn.Open();

                if (!Directory.Exists(sPathResult)) Directory.CreateDirectory(sPathResult);

                doWriteLog(sPathLog, "START : Inquiry by List");

                /*
                    <?xml version="1.0"?>
                    <TMScript>
                        <Attribute>
                            <Version>1.00</Version>
                            <Type>INQ</Type>
                            <Sender>EMS</Sender>
                            <Sender_ID>EMSTMS_Inq</Sender_ID>			
                        </Attribute>
                        <Data>
                            <Filters>
                                <Filter>
                                    <Field_Name>Terminal_Init</Field_Name>				
                                    <Values>CX433141</Values>	
                                </Filter>
                            </Filters>		
                        </Data>
                    </TMScript>
                 */

                List<string> listTerminalID = listGetTerminalList(sPathStartup + @"\LIST.txt");
                int iSuccess = 0;
                int iFailed = 0;
                if (listTerminalID.Count > 0)
                {
                    foreach (string sTerminalID in listTerminalID)
                    {
                        try
                        {
                            EmsXml oEmsXml = new EmsXml();
                            oEmsXml.Attribute.sVersions = "1.00";
                            oEmsXml.Attribute.sSenders = "LOCAL";
                            oEmsXml.Attribute.sSenderID = string.Format("LOCALINQ_{0:ddMMyyhhmmss}", DateTime.Now);
                            oEmsXml.Attribute.sTypes = "INQ";

                            EmsXml.DataClass.FilterClass oFilter = new EmsXml.DataClass.FilterClass();
                            oFilter.sFilterFieldName = "Terminal_INIT";
                            oFilter.sValues = sTerminalID;
                            oEmsXml.Data.doAddFilter(oFilter);

                            EmsXml oEmsXmlResult = new EmsXml();
                            oEmsXmlResult = oEmsXml;

                            string sErrMsg = null;
                            string sFileResult = string.Format(@"{0}\{1}.xml", sPathResult, sTerminalID);
                            //if (EmsCommon.IsXMLInquiry(oSqlConn, ref sErrMsg, ref oEmsXmlResult, null, sFileResult))
                            if (EmsCommon.IsXMLInquiry(sConnString, ref sErrMsg, ref oEmsXmlResult, null, sFileResult))
                            {
                                doWriteLog(sPathLog, string.Format("SUCCESS, inquirying {0}", sTerminalID));
                                oEmsXmlResult.CreateXmlFile(sFileResult, true);
                                iSuccess++;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("FAILED, {0}", ex.Message);
                            doWriteLog(sPathLog, string.Format("FAILED, inquirying {0}", sTerminalID));
                            iFailed++;
                            doWriteFailedList(sTerminalID);
                        }
                    }
                    Console.WriteLine("SUCCESS, inquirying {0} from {1} TerminalID", iSuccess, listTerminalID.Count);
                    doWriteLog(sPathLog, string.Format("SUCCESS, inquirying {0} from {1} TerminalID", iSuccess, listTerminalID.Count));
                }
                else
                {
                    Console.WriteLine("SUCCESS, no TerminalID to inquiry");
                    doWriteLog(sPathLog, string.Format("Inquiry date : {0:d/MM/yyyy} SUCCESS, no TerminalID to inquiry",
                        DateTime.Now));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FAILED, {0}", ex.Message);
                doWriteLog(sPathLog, string.Format("Inquiry date : {0:d/MM/yyyy} FAILED. {1}",
                    DateTime.Now, ex.Message));                
            }
            //Console.Read();
            doWriteLog(sPathLog, string.Format("END : Local Inquiry by TerminalList"));
        }

        static List<string> listGetTerminalList(string sFilename)
        {
            List<string> listTemp = new List<string>();
            if (File.Exists(sFilename))
                using (StreamReader sr = new StreamReader(sFilename))
                {
                    string sTemp = sr.ReadLine();
                    while (!string.IsNullOrEmpty(sTemp))
                    {
                        listTemp.Add(sTemp);
                        sTemp = sr.ReadLine();
                    }
                }
            return listTemp;
        }

        static void doWriteLog(string sLogDirectory, string sMessage)
        {
            //string sLogDirectory = Environment.CurrentDirectory + @"\LOG";
            string sLogFile = string.Format(@"{0}\{1:yyyyMMdd}.log", sLogDirectory, DateTime.Now);
            if (!Directory.Exists(sLogDirectory)) Directory.CreateDirectory(sLogDirectory);
            using (StreamWriter sw = new StreamWriter(sLogFile, true))
                sw.WriteLine(string.Format("[{0:yyyy MMM dd hh:mm:ss tt}] : {1}", DateTime.Now, sMessage));
        }

        static void doWriteFailedList(string sTerminalID)
        {
            string sLogFile = string.Format(@"{0}\FAILED.txt", Environment.CurrentDirectory);
            using (StreamWriter sw = new StreamWriter(sLogFile, true))
                sw.WriteLine(sTerminalID);
        }

        static void InitConfig(ref string sXmlLog, ref string sXmlResult, ref string _sStartDate, ref string _sEndDate)
        {
            string sXmlFile;
            InitEmsDir oEmsDir = new InitEmsDir();
            sXmlFile = oEmsDir.XmlFile;
            sXmlLog = oEmsDir.XmlLog;
            sXmlResult = oEmsDir.XmlResult;

            if (!Directory.Exists(sXmlLog))
                Directory.CreateDirectory(sXmlLog);
            if (!Directory.Exists(sXmlFile))
                Directory.CreateDirectory(sXmlFile);
            if (!Directory.Exists(sXmlResult))
                Directory.CreateDirectory(sXmlResult);

            EmsCommon.DirectoryLog = sXmlLog;
            EmsCommon.DirectoryXml = sXmlFile;
            EmsCommon.DirectoryResult = sXmlResult;

            _sStartDate = oEmsDir.StartDate;
            _sEndDate = oEmsDir.EndDate;
        }
    }

    class InitEmsDir
    {
        protected string sXmlFile;
        protected string sXmlResult;
        protected string sXmlLog;
        protected string sStartDate;
        protected string sEndDate;

        public string XmlFile { get { return sXmlFile; } }
        public string XmlResult { get { return sXmlResult; } }
        public string XmlLog { get { return sXmlLog; } }
        public string StartDate { get { return sStartDate; } }
        public string EndDate { get { return sEndDate; } }

        public InitEmsDir()
        {
            string sFilename = Environment.CurrentDirectory + @"\EmsInqRange.config";
            if (File.Exists(sFilename))
                InitConfig(sFilename);
        }

        protected void InitConfig(string _sFilename)
        {
            IniFile oIniEmsDir = new IniFile(_sFilename);
            sXmlFile = oIniEmsDir.IniReadValue("Directory", "File");
            sXmlLog = oIniEmsDir.IniReadValue("Directory", "Log");
            sXmlResult = oIniEmsDir.IniReadValue("Directory", "Result");

            sStartDate = oIniEmsDir.IniReadValue("DATE", "StartDate");
            sEndDate = oIniEmsDir.IniReadValue("DATE", "EndDate");
        }
    }

    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;

        public InitData() : this(Environment.CurrentDirectory) { }

        public InitData(string sPath)
        {
            doGetInitData(sPath);
        }

        protected void doGetInitData(string sActiveDir)
        {
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error : " + ex.Message);
                };
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnString(sDataSource, sDatabase, sUserID, sPassword);
        }

        public void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword)
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            try
            {
                IniFile objIniFile = new IniFile(sFileName);
                objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
                objIniFile.IniWriteValue("Database", "Database", sDatabase);
                objIniFile.IniWriteValue("Database", "UserID", sUserID);
                objIniFile.IniWriteValue("Database", "Password", sPassword);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            };
            EncryptionLib.EncryptFile(sEncryptFileName, sFileName);
            //File.Delete(sFileName);
        }
    }
}
