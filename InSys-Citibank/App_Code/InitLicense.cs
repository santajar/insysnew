using System;
using System.Globalization;
using Ini;

namespace InSys
{
    class InitLicense
    {
        private string sLicenseType;
        private string sUser;
        private string sCompany;
        private string sLicense;
        private string sInstallDate;
        private string sExpiry;
        private string sExpiryPeriod;

        private string sFileName;
        public InitLicense(string _sFileName)
        {
            sFileName = _sFileName;
            doGetInitLicense();
        }

        protected void doGetInitLicense()
        {
            try
            {
                IniFile oObjLicenseFile = new IniFile(sFileName);
                sLicenseType = oObjLicenseFile.IniReadValue("License", "Type");
                sUser = oObjLicenseFile.IniReadValue("License", "User");
                sCompany = oObjLicenseFile.IniReadValue("License", "Company");
                sLicense = oObjLicenseFile.IniReadValue("License", "License");
                sInstallDate = oObjLicenseFile.IniReadValue("License", "License Date");
                sExpiry = oObjLicenseFile.IniReadValue("License", "Expiry");
                sExpiryPeriod = oObjLicenseFile.IniReadValue("License", "Expiry Period");
            }
            catch (Exception ex)
            {
                //Trace.CreateAppLog("Get License file : " + ex.Message);
            }
        }

        public int iGetLicenseType()
        {
            return Convert.ToInt16(sLicenseType);
        }

        public string sGetUser()
        {
            return sUser;
        }

        public string sGetCompany()
        {
            return sCompany;
        }

        public string sGetLicense()
        {
            return sLicense;
        }

        public DateTime dtInstallDate(CultureInfo ciTemp)
        {
            return Convert.ToDateTime(sInstallDate, ciTemp);
        }

        public bool bGetExpiry()
        {
            return Convert.ToBoolean(Convert.ToInt16(sExpiry));
        }

        public int iGetExpiryPeriod()
        {
            return Convert.ToInt16(sExpiryPeriod);
        }

        public void doChangeExpiry(bool bExpiry)
        {
            IniFile oObjLicenseFile = new IniFile(sFileName);
            oObjLicenseFile.IniWriteValue("License", "Expiry", Convert.ToString(Convert.ToInt16(bExpiry)));
        }

        public void doChangeUser(string _sUser)
        {
            IniFile oObjLicenseFile = new IniFile(sFileName);
            oObjLicenseFile.IniWriteValue("License", "User", _sUser);
        }

        public void doChangeCompany(string _sCompany)
        {
            IniFile oObjLicenseFile = new IniFile(sFileName);
            oObjLicenseFile.IniWriteValue("License", "Company", _sCompany);
        }

        public void doChangeLicense(string _sLicense)
        {
            IniFile oObjLicenseFile = new IniFile(sFileName);
            oObjLicenseFile.IniWriteValue("License", "License", _sLicense);
        }

        public void doChangeLicenseDate(DateTime _dtLicense)
        {
            IniFile oObjLicenseFile = new IniFile(sFileName);
            oObjLicenseFile.IniWriteValue("License", "License Date", _dtLicense.ToString("yyyy/MM/dd"));
        }

        public void doChangeType(int _iType)
        {
            IniFile oObjLicenseFile = new IniFile(sFileName);
            oObjLicenseFile.IniWriteValue("License", "Type", _iType.ToString());
        }

        public void doChangeExpiryPeriod(int _iExpPeriod)
        {
            IniFile oObjLicenseFile = new IniFile(sFileName);
            oObjLicenseFile.IniWriteValue("License", "Expiry Period", _iExpPeriod.ToString());
        }
    }
}
