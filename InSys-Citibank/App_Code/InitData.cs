using System;
using System.IO;
using Ini;
using InSysClass;

namespace InSys
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;
        protected string sDataSourceAuditTrail;
        protected string sDatabaseAuditTrail;
        protected string sUserIDAuditTrail;
        protected string sPasswordAuditTrail;

        public InitData()
        {
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                    sDataSourceAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "DataSource");
                    sDatabaseAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "Database");
                    sUserIDAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "UserID");
                    sPasswordAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "Password");
                }
                catch (Exception ex)
                {
                    CommonClass.doWriteErrorFile(ex.Message);
                };
                File.Delete(sFileName);
            }
        }

        #region Data Source Properties
        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return CommonClass.sConnString(sDataSource, sDatabase, sUserID, sPassword);
        }

        public string sGetDataSourceAuditTrail()
        {
            return sDataSourceAuditTrail;
        }

        public string sGetDatabaseAuditTrail()
        {
            return sDatabaseAuditTrail;
        }

        public string sGetUserIDAuditTrail()
        {
            return sUserIDAuditTrail;
        }

        public string sGetPasswordAuditTrail()
        {
            return sPasswordAuditTrail;
        }

        public string sGetConnStringAuditTrail()
        {
            return CommonClass.sConnString(sDataSourceAuditTrail, sDatabaseAuditTrail, sUserIDAuditTrail, sPasswordAuditTrail);
        }
        #endregion

        public void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword, string sDataSourceAuditTrail, string sDatabaseAuditTrail, string sUserIDAuditTrail, string sPasswordAuditTrail)
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            try
            {
                IniFile objIniFile = new IniFile(sFileName);
                objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
                objIniFile.IniWriteValue("Database", "Database", sDatabase);
                objIniFile.IniWriteValue("Database", "UserID", sUserID);
                objIniFile.IniWriteValue("Database", "Password", sPassword);
                objIniFile.IniWriteValue("DatabaseAuditTrail", "DataSource", sDataSourceAuditTrail);
                objIniFile.IniWriteValue("DatabaseAuditTrail", "Database", sDatabaseAuditTrail);
                objIniFile.IniWriteValue("DatabaseAuditTrail", "UserID", sUserIDAuditTrail);
                objIniFile.IniWriteValue("DatabaseAuditTrail", "Password", sPasswordAuditTrail);
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            };
            EncryptionLib.EncryptFile(sEncryptFileName, sFileName);
            File.Delete(sFileName);
        }
    }
}
