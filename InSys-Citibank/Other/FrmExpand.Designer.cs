namespace InSys
{
    partial class FrmExpand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExpand));
            this.btnFindSN = new System.Windows.Forms.Button();
            this.btnTMK = new System.Windows.Forms.Button();
            this.LabelTerminal = new System.Windows.Forms.Label();
            this.ButtonIcon = new System.Windows.Forms.ImageList(this.components);
            this.ListTerminal = new System.Windows.Forms.ListView();
            this.ColumnTerminal = new System.Windows.Forms.ColumnHeader();
            this.IconList = new System.Windows.Forms.ImageList(this.components);
            this.BtnKeyMan = new System.Windows.Forms.Button();
            this.BtnAIDMan = new System.Windows.Forms.Button();
            this.btnViewAll = new System.Windows.Forms.Button();
            this.btnNewTerminal = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.LabelDetail = new System.Windows.Forms.Label();
            this.BtnSNReg = new System.Windows.Forms.Button();
            this.btnCardManagement = new System.Windows.Forms.Button();
            this.RefreshTime = new System.Windows.Forms.Timer(this.components);
            this.btnTenLast = new System.Windows.Forms.Button();
            this.BtnRegister = new System.Windows.Forms.Button();
            this.TreeTerminal = new System.Windows.Forms.TreeView();
            this.btnFindTerminal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFindSN
            // 
            this.btnFindSN.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindSN.Image = ((System.Drawing.Image)(resources.GetObject("btnFindSN.Image")));
            this.btnFindSN.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFindSN.Location = new System.Drawing.Point(548, 336);
            this.btnFindSN.Name = "btnFindSN";
            this.btnFindSN.Size = new System.Drawing.Size(160, 23);
            this.btnFindSN.TabIndex = 49;
            this.btnFindSN.Text = "         Find SN";
            this.btnFindSN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFindSN.Visible = false;
            // 
            // btnTMK
            // 
            this.btnTMK.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTMK.Image = ((System.Drawing.Image)(resources.GetObject("btnTMK.Image")));
            this.btnTMK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTMK.Location = new System.Drawing.Point(548, 304);
            this.btnTMK.Name = "btnTMK";
            this.btnTMK.Size = new System.Drawing.Size(160, 23);
            this.btnTMK.TabIndex = 48;
            this.btnTMK.Text = "         TMK Management";
            this.btnTMK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelTerminal
            // 
            this.LabelTerminal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTerminal.Location = new System.Drawing.Point(12, 9);
            this.LabelTerminal.Name = "LabelTerminal";
            this.LabelTerminal.Size = new System.Drawing.Size(200, 25);
            this.LabelTerminal.TabIndex = 45;
            this.LabelTerminal.Text = "TERMINAL";
            this.LabelTerminal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ButtonIcon
            // 
            this.ButtonIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ButtonIcon.ImageStream")));
            this.ButtonIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ButtonIcon.Images.SetKeyName(0, "");
            this.ButtonIcon.Images.SetKeyName(1, "");
            this.ButtonIcon.Images.SetKeyName(2, "");
            // 
            // ListTerminal
            // 
            this.ListTerminal.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.ListTerminal.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnTerminal});
            this.ListTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListTerminal.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.ListTerminal.LargeImageList = this.IconList;
            this.ListTerminal.Location = new System.Drawing.Point(12, 34);
            this.ListTerminal.MultiSelect = false;
            this.ListTerminal.Name = "ListTerminal";
            this.ListTerminal.Size = new System.Drawing.Size(200, 477);
            this.ListTerminal.SmallImageList = this.IconList;
            this.ListTerminal.TabIndex = 34;
            this.ListTerminal.UseCompatibleStateImageBehavior = false;
            this.ListTerminal.View = System.Windows.Forms.View.Details;
            // 
            // ColumnTerminal
            // 
            this.ColumnTerminal.Text = "Terminal";
            this.ColumnTerminal.Width = 155;
            // 
            // IconList
            // 
            this.IconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconList.ImageStream")));
            this.IconList.TransparentColor = System.Drawing.Color.Transparent;
            this.IconList.Images.SetKeyName(0, "");
            this.IconList.Images.SetKeyName(1, "");
            this.IconList.Images.SetKeyName(2, "");
            this.IconList.Images.SetKeyName(3, "");
            // 
            // BtnKeyMan
            // 
            this.BtnKeyMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnKeyMan.Image = ((System.Drawing.Image)(resources.GetObject("BtnKeyMan.Image")));
            this.BtnKeyMan.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnKeyMan.Location = new System.Drawing.Point(548, 272);
            this.BtnKeyMan.Name = "BtnKeyMan";
            this.BtnKeyMan.Size = new System.Drawing.Size(160, 23);
            this.BtnKeyMan.TabIndex = 44;
            this.BtnKeyMan.Text = "         Key Management";
            this.BtnKeyMan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BtnAIDMan
            // 
            this.BtnAIDMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAIDMan.Image = ((System.Drawing.Image)(resources.GetObject("BtnAIDMan.Image")));
            this.BtnAIDMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnAIDMan.Location = new System.Drawing.Point(548, 240);
            this.BtnAIDMan.Name = "BtnAIDMan";
            this.BtnAIDMan.Size = new System.Drawing.Size(160, 23);
            this.BtnAIDMan.TabIndex = 43;
            this.BtnAIDMan.Text = "         AID Management";
            this.BtnAIDMan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnViewAll
            // 
            this.btnViewAll.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewAll.Image = ((System.Drawing.Image)(resources.GetObject("btnViewAll.Image")));
            this.btnViewAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnViewAll.Location = new System.Drawing.Point(548, 176);
            this.btnViewAll.Name = "btnViewAll";
            this.btnViewAll.Size = new System.Drawing.Size(160, 23);
            this.btnViewAll.TabIndex = 42;
            this.btnViewAll.Text = "         View All Terminal";
            this.btnViewAll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnNewTerminal
            // 
            this.btnNewTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewTerminal.Image = ((System.Drawing.Image)(resources.GetObject("btnNewTerminal.Image")));
            this.btnNewTerminal.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNewTerminal.Location = new System.Drawing.Point(548, 16);
            this.btnNewTerminal.Name = "btnNewTerminal";
            this.btnNewTerminal.Size = new System.Drawing.Size(160, 23);
            this.btnNewTerminal.TabIndex = 36;
            this.btnNewTerminal.Text = "Create New Terminal";
            this.btnNewTerminal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Location = new System.Drawing.Point(508, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(20, 20);
            this.btnClose.TabIndex = 47;
            // 
            // LabelDetail
            // 
            this.LabelDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDetail.Location = new System.Drawing.Point(212, 9);
            this.LabelDetail.Name = "LabelDetail";
            this.LabelDetail.Size = new System.Drawing.Size(320, 25);
            this.LabelDetail.TabIndex = 46;
            this.LabelDetail.Text = "DETAIL";
            this.LabelDetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BtnSNReg
            // 
            this.BtnSNReg.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSNReg.Image = ((System.Drawing.Image)(resources.GetObject("BtnSNReg.Image")));
            this.BtnSNReg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSNReg.Location = new System.Drawing.Point(548, 80);
            this.BtnSNReg.Name = "BtnSNReg";
            this.BtnSNReg.Size = new System.Drawing.Size(160, 23);
            this.BtnSNReg.TabIndex = 38;
            this.BtnSNReg.Text = "         Terminal-SN";
            this.BtnSNReg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCardManagement
            // 
            this.btnCardManagement.BackColor = System.Drawing.SystemColors.Control;
            this.btnCardManagement.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCardManagement.Image = ((System.Drawing.Image)(resources.GetObject("btnCardManagement.Image")));
            this.btnCardManagement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCardManagement.Location = new System.Drawing.Point(548, 208);
            this.btnCardManagement.Name = "btnCardManagement";
            this.btnCardManagement.Size = new System.Drawing.Size(160, 23);
            this.btnCardManagement.TabIndex = 39;
            this.btnCardManagement.Text = "         Card Management";
            this.btnCardManagement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCardManagement.UseVisualStyleBackColor = false;
            // 
            // RefreshTime
            // 
            this.RefreshTime.Interval = 600000;
            // 
            // btnTenLast
            // 
            this.btnTenLast.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTenLast.Image = ((System.Drawing.Image)(resources.GetObject("btnTenLast.Image")));
            this.btnTenLast.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnTenLast.Location = new System.Drawing.Point(548, 144);
            this.btnTenLast.Name = "btnTenLast";
            this.btnTenLast.Size = new System.Drawing.Size(160, 23);
            this.btnTenLast.TabIndex = 41;
            this.btnTenLast.Text = "         View Last Terminal";
            this.btnTenLast.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BtnRegister
            // 
            this.BtnRegister.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRegister.Image = ((System.Drawing.Image)(resources.GetObject("BtnRegister.Image")));
            this.BtnRegister.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnRegister.Location = new System.Drawing.Point(548, 48);
            this.BtnRegister.Name = "BtnRegister";
            this.BtnRegister.Size = new System.Drawing.Size(160, 23);
            this.BtnRegister.TabIndex = 37;
            this.BtnRegister.Text = "         Register Old File";
            this.BtnRegister.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // TreeTerminal
            // 
            this.TreeTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeTerminal.Location = new System.Drawing.Point(212, 34);
            this.TreeTerminal.Name = "TreeTerminal";
            this.TreeTerminal.Size = new System.Drawing.Size(320, 477);
            this.TreeTerminal.TabIndex = 35;
            // 
            // btnFindTerminal
            // 
            this.btnFindTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindTerminal.Image = ((System.Drawing.Image)(resources.GetObject("btnFindTerminal.Image")));
            this.btnFindTerminal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFindTerminal.Location = new System.Drawing.Point(548, 112);
            this.btnFindTerminal.Name = "btnFindTerminal";
            this.btnFindTerminal.Size = new System.Drawing.Size(160, 23);
            this.btnFindTerminal.TabIndex = 40;
            this.btnFindTerminal.Text = "         Find Terminal";
            this.btnFindTerminal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmExpand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 517);
            this.Controls.Add(this.btnFindSN);
            this.Controls.Add(this.btnTMK);
            this.Controls.Add(this.LabelTerminal);
            this.Controls.Add(this.ListTerminal);
            this.Controls.Add(this.BtnKeyMan);
            this.Controls.Add(this.BtnAIDMan);
            this.Controls.Add(this.btnViewAll);
            this.Controls.Add(this.btnNewTerminal);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.LabelDetail);
            this.Controls.Add(this.BtnSNReg);
            this.Controls.Add(this.btnCardManagement);
            this.Controls.Add(this.btnTenLast);
            this.Controls.Add(this.BtnRegister);
            this.Controls.Add(this.TreeTerminal);
            this.Controls.Add(this.btnFindTerminal);
            this.Name = "FrmExpand";
            this.Text = "Expand";
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnFindSN;
        internal System.Windows.Forms.Button btnTMK;
        internal System.Windows.Forms.Label LabelTerminal;
        internal System.Windows.Forms.ImageList ButtonIcon;
        internal System.Windows.Forms.ListView ListTerminal;
        internal System.Windows.Forms.ColumnHeader ColumnTerminal;
        internal System.Windows.Forms.ImageList IconList;
        internal System.Windows.Forms.Button BtnKeyMan;
        internal System.Windows.Forms.Button BtnAIDMan;
        internal System.Windows.Forms.Button btnViewAll;
        internal System.Windows.Forms.Button btnNewTerminal;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.Label LabelDetail;
        internal System.Windows.Forms.Button BtnSNReg;
        internal System.Windows.Forms.Button btnCardManagement;
        internal System.Windows.Forms.Timer RefreshTime;
        internal System.Windows.Forms.Button btnTenLast;
        internal System.Windows.Forms.Button BtnRegister;
        internal System.Windows.Forms.TreeView TreeTerminal;
        internal System.Windows.Forms.Button btnFindTerminal;
    }
}