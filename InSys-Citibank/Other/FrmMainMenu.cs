using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;



namespace InSys
{
    public partial class FrmMainMenu : Form
    {
        private SqlConnection oObjConn;
        protected string sExitMessage = "Are You Sure want to Exit?";
        protected string sTitleExitMessage = "Exit";
        protected string sLogoutMessage = "Are You Sure want to Logout?";
        protected string sTitleLogoutMessage = "Logout";

        public FrmMainMenu()
        {
            InitializeComponent();
        }

        private void mnuLogin_Click(object sender, EventArgs e)
        {
            bool isCanLoginToSystem = false;
            FrmLogin flogin = new FrmLogin(oObjConn);
            try
            {
                flogin.ShowDialog();
            }
            finally
            {
                isCanLoginToSystem = flogin.isCanLoginToSystem;
                flogin.Dispose();
            }

            if (isCanLoginToSystem)
                this.Visible = true;
            else
            {
                Application.Exit();
            }
        }

        private void ToolBarMenu_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            int iButton = ToolBarMenu.Buttons.IndexOf(e.Button);
            ToolBarMenu.Buttons[iButton].Pushed = false;
            switch (iButton)
            {
                case 0: mnuLogin.PerformClick(); break;
                case 1: mnuLogOff.PerformClick(); break;
                case 3: mnuExit.PerformClick(); break;
                case 5: mnuDBConn.PerformClick(); break;
                case 6: mnuUserManagement.PerformClick();break;
                case 7: mnuAudit.PerformClick(); break;
                case 9: mnuSelectDB.PerformClick(); break;
                case 13: mnuSerial.PerformClick(); break;
                break;
            }
        }

        private void mnuPassword_Click(object sender, EventArgs e)
        {
            FrmChangePassword fchangepass = new FrmChangePassword();
            fchangepass.MdiParent = this;
            try
            {
                fchangepass.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            if (CommonClass.isYesMessage(sExitMessage, sTitleExitMessage))
                Application.Exit();
        }

        private void mnuDBConn_Click(object sender, EventArgs e)
        {
            FrmDatabasePrimaryConnection fdatabasepri = new FrmDatabasePrimaryConnection();
            try
            {
                fdatabasepri.ShowDialog();
            }
            finally
            {
                if (isHaveConfiguration())
                {
                    if (oObjConn != null)
                    if (oObjConn.State == ConnectionState.Open) oObjConn.Close();
                    
                    oObjConn = fdatabasepri.oObjConn;
                    oObjConn.Open();
                }
                else Application.Exit();
                fdatabasepri.Dispose();
            }
        }

        private void mnuUserManagement_Click(object sender, EventArgs e)
        {
            FrmUserManagement fusermanagement = new FrmUserManagement();
            fusermanagement.MdiParent = this;
            try
            {
                fusermanagement.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuAdmin_Click(object sender, EventArgs e)
        {
            FrmAdmin fadmin = new FrmAdmin();
            fadmin.MdiParent = this;
            try
            {
                fadmin.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuAudit_Click(object sender, EventArgs e)
        {
            FrmFilterLog ffilterlog = new FrmFilterLog();
            ffilterlog.MdiParent = this;
            try
            {
                ffilterlog.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuTLV_Click(object sender, EventArgs e)
        {
            frmTLV ftlv= new frmTLV();
            ftlv.MdiParent = this;
            try
            {
                ftlv.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuSelectDB_Click(object sender, EventArgs e)
        {
            FrmDatabaseSelection fdatabasesel = new FrmDatabaseSelection();
            fdatabasesel.MdiParent = this;
            try
            {
                fdatabasesel.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuEDC_Click(object sender, EventArgs e)
        {
            FrmPing fping = new FrmPing();
            fping.MdiParent = this;
            try
            {
                fping.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuSerial_Click(object sender, EventArgs e)
        {
            FrmDownload fdownload = new FrmDownload();
            fdownload.MdiParent = this;
            try
            {
                fdownload.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuAbout_Click(object sender, EventArgs e)
        {
            FrmAbout fabout = new FrmAbout();
            fabout.MdiParent = this;
            try
            {
                fabout.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            StatusBarPanel2.Text = DateTime.Today.ToShortTimeString();
            StatusBarPanel3.Text = DateTime.Today.ToShortDateString();
        }

        private void mnuAppSetting_Click(object sender, EventArgs e)
        {
            FrmSetting fsetting = new FrmSetting();
            fsetting.MdiParent = this;
            try
            {
                fsetting.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuRegister_Click(object sender, EventArgs e)
        {
            FrmRegister fregister = new FrmRegister();
            fregister.MdiParent = this;
            try
            {
                fregister.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected bool isHaveConfiguration()
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";

            if (File.Exists(sFileName))
                return true;
            else return false;
        }

        private void FrmMainMenu_Load(object sender, EventArgs e)
        {
            this.Visible = true;
            InitConnection();

            doLogin();
        }
        protected void doLogin()
        {
            mnuLogin.PerformClick();
        }

        protected void InitConnection()
        {
            if (!isHaveConfiguration())
            {
                mnuDBConn.PerformClick();
            }
            else InitObjectConnection();
        }

        protected void InitObjectConnection()
        {
            InitData oInitData = new InitData();
            string sConnString = oInitData.sGetConnString();
            oObjConn = new SqlConnection(sConnString);
        }
    }
}