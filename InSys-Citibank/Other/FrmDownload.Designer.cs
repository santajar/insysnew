namespace InSys
{
    partial class FrmDownload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDownload));
            this.Label8 = new System.Windows.Forms.Label();
            this.btnBrowseAM = new System.Windows.Forms.Button();
            this.txtboxApp = new System.Windows.Forms.TextBox();
            this.txtboxAM = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.Label7 = new System.Windows.Forms.Label();
            this.cboListDB = new System.Windows.Forms.ComboBox();
            this.chkSplit = new System.Windows.Forms.CheckBox();
            this.cboListFile = new System.Windows.Forms.ComboBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.btnBrowseApp = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDownload = new System.Windows.Forms.Button();
            this.cboTerminal = new System.Windows.Forms.ComboBox();
            this.cboSpeed = new System.Windows.Forms.ComboBox();
            this.cboPort = new System.Windows.Forms.ComboBox();
            this.OpenFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.cboFile = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.OpenFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.Label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(12, 105);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(72, 24);
            this.Label8.TabIndex = 66;
            this.Label8.Text = "AM";
            // 
            // btnBrowseAM
            // 
            this.btnBrowseAM.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowseAM.Image")));
            this.btnBrowseAM.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnBrowseAM.Location = new System.Drawing.Point(244, 105);
            this.btnBrowseAM.Name = "btnBrowseAM";
            this.btnBrowseAM.Size = new System.Drawing.Size(88, 24);
            this.btnBrowseAM.TabIndex = 65;
            this.btnBrowseAM.Text = "       Browse";
            this.btnBrowseAM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtboxApp
            // 
            this.txtboxApp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtboxApp.Location = new System.Drawing.Point(100, 137);
            this.txtboxApp.Name = "txtboxApp";
            this.txtboxApp.ReadOnly = true;
            this.txtboxApp.Size = new System.Drawing.Size(128, 20);
            this.txtboxApp.TabIndex = 62;
            // 
            // txtboxAM
            // 
            this.txtboxAM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtboxAM.Location = new System.Drawing.Point(100, 105);
            this.txtboxAM.Name = "txtboxAM";
            this.txtboxAM.ReadOnly = true;
            this.txtboxAM.Size = new System.Drawing.Size(128, 20);
            this.txtboxAM.TabIndex = 67;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(252, 25);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 64;
            this.btnTest.Text = "Button1";
            this.btnTest.Visible = false;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(12, 41);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(72, 24);
            this.Label7.TabIndex = 63;
            this.Label7.Text = "Database";
            // 
            // cboListDB
            // 
            this.cboListDB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboListDB.Location = new System.Drawing.Point(100, 41);
            this.cboListDB.Name = "cboListDB";
            this.cboListDB.Size = new System.Drawing.Size(121, 21);
            this.cboListDB.TabIndex = 49;
            // 
            // chkSplit
            // 
            this.chkSplit.Checked = true;
            this.chkSplit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSplit.Location = new System.Drawing.Point(100, 265);
            this.chkSplit.Name = "chkSplit";
            this.chkSplit.Size = new System.Drawing.Size(104, 24);
            this.chkSplit.TabIndex = 61;
            this.chkSplit.Text = "Split Download";
            // 
            // cboListFile
            // 
            this.cboListFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboListFile.Enabled = false;
            this.cboListFile.Location = new System.Drawing.Point(100, 73);
            this.cboListFile.Name = "cboListFile";
            this.cboListFile.Size = new System.Drawing.Size(121, 21);
            this.cboListFile.TabIndex = 51;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(12, 137);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(72, 24);
            this.Label6.TabIndex = 60;
            this.Label6.Text = "Application";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(12, 73);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(72, 24);
            this.Label5.TabIndex = 59;
            this.Label5.Text = "Terminal ID";
            // 
            // btnBrowseApp
            // 
            this.btnBrowseApp.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowseApp.Image")));
            this.btnBrowseApp.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnBrowseApp.Location = new System.Drawing.Point(244, 137);
            this.btnBrowseApp.Name = "btnBrowseApp";
            this.btnBrowseApp.Size = new System.Drawing.Size(88, 24);
            this.btnBrowseApp.TabIndex = 53;
            this.btnBrowseApp.Text = "       Browse";
            this.btnBrowseApp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnExit
            // 
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnExit.Location = new System.Drawing.Point(244, 233);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 24);
            this.btnExit.TabIndex = 58;
            this.btnExit.Text = "       Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDownload
            // 
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnDownload.Location = new System.Drawing.Point(244, 201);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(88, 24);
            this.btnDownload.TabIndex = 57;
            this.btnDownload.Text = "       Download";
            this.btnDownload.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboTerminal
            // 
            this.cboTerminal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTerminal.Location = new System.Drawing.Point(100, 233);
            this.cboTerminal.Name = "cboTerminal";
            this.cboTerminal.Size = new System.Drawing.Size(104, 21);
            this.cboTerminal.TabIndex = 56;
            // 
            // cboSpeed
            // 
            this.cboSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSpeed.Location = new System.Drawing.Point(100, 201);
            this.cboSpeed.Name = "cboSpeed";
            this.cboSpeed.Size = new System.Drawing.Size(104, 21);
            this.cboSpeed.TabIndex = 55;
            // 
            // cboPort
            // 
            this.cboPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPort.Location = new System.Drawing.Point(100, 169);
            this.cboPort.Name = "cboPort";
            this.cboPort.Size = new System.Drawing.Size(104, 21);
            this.cboPort.TabIndex = 54;
            // 
            // cboFile
            // 
            this.cboFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFile.Location = new System.Drawing.Point(100, 9);
            this.cboFile.Name = "cboFile";
            this.cboFile.Size = new System.Drawing.Size(104, 21);
            this.cboFile.TabIndex = 46;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(12, 233);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(80, 24);
            this.Label4.TabIndex = 52;
            this.Label4.Text = "Terminal       :";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(12, 201);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(80, 24);
            this.Label3.TabIndex = 50;
            this.Label3.Text = "Speed          :";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(12, 169);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(80, 24);
            this.Label2.TabIndex = 48;
            this.Label2.Text = "Port              :";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(12, 9);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(80, 24);
            this.Label1.TabIndex = 47;
            this.Label1.Text = "File                :";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 289);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.btnBrowseAM);
            this.Controls.Add(this.txtboxApp);
            this.Controls.Add(this.txtboxAM);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.cboListDB);
            this.Controls.Add(this.chkSplit);
            this.Controls.Add(this.cboListFile);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.btnBrowseApp);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnDownload);
            this.Controls.Add(this.cboTerminal);
            this.Controls.Add(this.cboSpeed);
            this.Controls.Add(this.cboPort);
            this.Controls.Add(this.cboFile);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Name = "Form1";
            this.Text = "Download";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Button btnBrowseAM;
        internal System.Windows.Forms.TextBox txtboxApp;
        internal System.Windows.Forms.TextBox txtboxAM;
        internal System.Windows.Forms.Button btnTest;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.ComboBox cboListDB;
        internal System.Windows.Forms.CheckBox chkSplit;
        internal System.Windows.Forms.ComboBox cboListFile;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Button btnBrowseApp;
        internal System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Button btnDownload;
        internal System.Windows.Forms.ComboBox cboTerminal;
        internal System.Windows.Forms.ComboBox cboSpeed;
        internal System.Windows.Forms.ComboBox cboPort;
        internal System.Windows.Forms.OpenFileDialog OpenFileDialog1;
        internal System.Windows.Forms.ComboBox cboFile;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.OpenFileDialog OpenFileDialog2;
        internal System.Windows.Forms.Label Label1;
    }
}