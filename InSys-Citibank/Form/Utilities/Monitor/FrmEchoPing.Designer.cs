﻿namespace InSys
{
    partial class FrmEchoPing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEchoPing));
            this.grpAdvanceSeacrh = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtpDateEnd = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.dtpDateStart = new System.Windows.Forms.DateTimePicker();
            this.txtType = new System.Windows.Forms.TextBox();
            this.txtAppName = new System.Windows.Forms.TextBox();
            this.txtTerminalID = new System.Windows.Forms.TextBox();
            this.txtTerminalSN = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.lblAppName = new System.Windows.Forms.Label();
            this.lblTerminalID = new System.Windows.Forms.Label();
            this.lblTerminalSN = new System.Windows.Forms.Label();
            this.dgvEchoPing = new System.Windows.Forms.DataGridView();
            this.chkDate = new System.Windows.Forms.CheckBox();
            this.grpAdvanceSeacrh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEchoPing)).BeginInit();
            this.SuspendLayout();
            // 
            // grpAdvanceSeacrh
            // 
            this.grpAdvanceSeacrh.Controls.Add(this.chkDate);
            this.grpAdvanceSeacrh.Controls.Add(this.btnSearch);
            this.grpAdvanceSeacrh.Controls.Add(this.dtpDateEnd);
            this.grpAdvanceSeacrh.Controls.Add(this.lblTo);
            this.grpAdvanceSeacrh.Controls.Add(this.dtpDateStart);
            this.grpAdvanceSeacrh.Controls.Add(this.txtType);
            this.grpAdvanceSeacrh.Controls.Add(this.txtAppName);
            this.grpAdvanceSeacrh.Controls.Add(this.txtTerminalID);
            this.grpAdvanceSeacrh.Controls.Add(this.txtTerminalSN);
            this.grpAdvanceSeacrh.Controls.Add(this.lblType);
            this.grpAdvanceSeacrh.Controls.Add(this.lblAppName);
            this.grpAdvanceSeacrh.Controls.Add(this.lblTerminalID);
            this.grpAdvanceSeacrh.Controls.Add(this.lblTerminalSN);
            this.grpAdvanceSeacrh.Location = new System.Drawing.Point(12, 12);
            this.grpAdvanceSeacrh.Name = "grpAdvanceSeacrh";
            this.grpAdvanceSeacrh.Size = new System.Drawing.Size(644, 113);
            this.grpAdvanceSeacrh.TabIndex = 0;
            this.grpAdvanceSeacrh.TabStop = false;
            this.grpAdvanceSeacrh.Text = "Advance Search";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(553, 81);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dtpDateEnd
            // 
            this.dtpDateEnd.Enabled = false;
            this.dtpDateEnd.Location = new System.Drawing.Point(328, 78);
            this.dtpDateEnd.Name = "dtpDateEnd";
            this.dtpDateEnd.Size = new System.Drawing.Size(200, 20);
            this.dtpDateEnd.TabIndex = 12;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(295, 81);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 11;
            this.lblTo.Text = "To";
            // 
            // dtpDateStart
            // 
            this.dtpDateStart.Enabled = false;
            this.dtpDateStart.Location = new System.Drawing.Point(89, 78);
            this.dtpDateStart.Name = "dtpDateStart";
            this.dtpDateStart.Size = new System.Drawing.Size(200, 20);
            this.dtpDateStart.TabIndex = 10;
            this.dtpDateStart.Value = new System.DateTime(2013, 1, 1, 11, 43, 0, 0);
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(391, 51);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(200, 20);
            this.txtType.TabIndex = 8;
            // 
            // txtAppName
            // 
            this.txtAppName.Location = new System.Drawing.Point(391, 25);
            this.txtAppName.Name = "txtAppName";
            this.txtAppName.Size = new System.Drawing.Size(200, 20);
            this.txtAppName.TabIndex = 7;
            // 
            // txtTerminalID
            // 
            this.txtTerminalID.Location = new System.Drawing.Point(89, 49);
            this.txtTerminalID.Name = "txtTerminalID";
            this.txtTerminalID.Size = new System.Drawing.Size(200, 20);
            this.txtTerminalID.TabIndex = 6;
            // 
            // txtTerminalSN
            // 
            this.txtTerminalSN.Location = new System.Drawing.Point(89, 23);
            this.txtTerminalSN.Name = "txtTerminalSN";
            this.txtTerminalSN.Size = new System.Drawing.Size(200, 20);
            this.txtTerminalSN.TabIndex = 2;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(295, 56);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(31, 13);
            this.lblType.TabIndex = 5;
            this.lblType.Text = "Type";
            // 
            // lblAppName
            // 
            this.lblAppName.AutoSize = true;
            this.lblAppName.Location = new System.Drawing.Point(295, 26);
            this.lblAppName.Name = "lblAppName";
            this.lblAppName.Size = new System.Drawing.Size(90, 13);
            this.lblAppName.TabIndex = 4;
            this.lblAppName.Text = "Application Name";
            // 
            // lblTerminalID
            // 
            this.lblTerminalID.AutoSize = true;
            this.lblTerminalID.Location = new System.Drawing.Point(18, 56);
            this.lblTerminalID.Name = "lblTerminalID";
            this.lblTerminalID.Size = new System.Drawing.Size(58, 13);
            this.lblTerminalID.TabIndex = 3;
            this.lblTerminalID.Text = "TerminalID";
            // 
            // lblTerminalSN
            // 
            this.lblTerminalSN.AutoSize = true;
            this.lblTerminalSN.Location = new System.Drawing.Point(18, 26);
            this.lblTerminalSN.Name = "lblTerminalSN";
            this.lblTerminalSN.Size = new System.Drawing.Size(62, 13);
            this.lblTerminalSN.TabIndex = 2;
            this.lblTerminalSN.Text = "TerminalSN";
            // 
            // dgvEchoPing
            // 
            this.dgvEchoPing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEchoPing.Location = new System.Drawing.Point(12, 131);
            this.dgvEchoPing.Name = "dgvEchoPing";
            this.dgvEchoPing.ReadOnly = true;
            this.dgvEchoPing.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEchoPing.Size = new System.Drawing.Size(644, 391);
            this.dgvEchoPing.TabIndex = 1;
            // 
            // chkDate
            // 
            this.chkDate.AutoSize = true;
            this.chkDate.Location = new System.Drawing.Point(21, 81);
            this.chkDate.Name = "chkDate";
            this.chkDate.Size = new System.Drawing.Size(49, 17);
            this.chkDate.TabIndex = 13;
            this.chkDate.Text = "Date";
            this.chkDate.UseVisualStyleBackColor = true;
            this.chkDate.CheckedChanged += new System.EventHandler(this.chkDate_CheckedChanged);
            // 
            // FrmEchoPing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 534);
            this.Controls.Add(this.dgvEchoPing);
            this.Controls.Add(this.grpAdvanceSeacrh);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmEchoPing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Echo Ping";
            this.Load += new System.EventHandler(this.FrmEchoPing_Load);
            this.grpAdvanceSeacrh.ResumeLayout(false);
            this.grpAdvanceSeacrh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEchoPing)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpAdvanceSeacrh;
        private System.Windows.Forms.DataGridView dgvEchoPing;
        private System.Windows.Forms.Label lblAppName;
        private System.Windows.Forms.Label lblTerminalID;
        private System.Windows.Forms.Label lblTerminalSN;
        private System.Windows.Forms.DateTimePicker dtpDateStart;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.TextBox txtAppName;
        private System.Windows.Forms.TextBox txtTerminalID;
        private System.Windows.Forms.TextBox txtTerminalSN;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DateTimePicker dtpDateEnd;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.CheckBox chkDate;
    }
}