﻿using InSysClass;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmEchoPing : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Form Echo Ping
        /// </summary>
        public FrmEchoPing(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        /// <summary>
        /// Initiliaze Form
        /// </summary>
        private void FrmEchoPing_Load(object sender, EventArgs e)
        {
            LoadData();
            //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sFormOpened + "Echo Ping", "");
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + "Echo Ping", "");
        }

        /// <summary>
        /// Load necessary Data
        /// </summary>
        private void LoadData()
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAuditTerminalSNEchoBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            if ((!string.IsNullOrEmpty(GetTerminalSN())) ||(!string.IsNullOrEmpty(GetTerminalID())) || (!string.IsNullOrEmpty(GetAppName())) || (!string.IsNullOrEmpty(GetComType())) || (chkDate.Checked==true))
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGetAllCond();

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
                DataTable oDataTableEchoPing = new DataTable();
                oAdapt.Fill(oDataTableEchoPing);
                dgvEchoPing.DataSource = oDataTableEchoPing;
        }

        private void chkDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDate.Checked == true)
            {
                dtpDateStart.Enabled = true;
                dtpDateEnd.Enabled = true;
            }
            else
            {
                dtpDateStart.Enabled = false;
                dtpDateEnd.Enabled = false;
            }
        }

        #region "Get Data"
        /// <summary>
        /// Get Comm Type Data
        /// </summary>
        private string GetComType()
        {
            return string.Format(txtType.Text);
        }

        /// <summary>
        /// Get Aplication Name Data
        /// </summary>
        private string GetAppName()
        {
            return string.Format(txtAppName.Text);
        }

        /// <summary>
        /// Get TerminalID Data
        /// </summary>
        private string GetTerminalID()
        {
            return string.Format(txtTerminalID.Text);
        }

        /// <summary>
        /// Get Terminal SN Data
        /// </summary>
        private string GetTerminalSN()
        {
            return string.Format(txtTerminalSN.Text);
        }
        #endregion

        #region "Get Function Condition"
        /// <summary>
        /// Get Condition
        /// </summary>
        private object sGetAllCond()
        {
            string sAllCondition, sCondTemp;
            int i;

            sCondTemp = sGetCond(GetTerminalSN(), GetTerminalID(), GetAppName(), GetComType());

            sAllCondition = sCondTemp.Substring(0, sCondTemp.Length - 4);

            return sAllCondition;
        }

        private string sGetCond(string TerminalSN, string TerminalID, string AppName, string ComType)
        {
            return string.Format(" WHERE " + sGetTerminalSNCond(TerminalSN) + sGetTerminalIDCond(TerminalID) + sGetAppNameCond(AppName) + sGetComTypeCond(ComType) + sGetDateCond());
        }

        private string sGetTerminalSNCond(string GetTerminalSN)
        {
            if (!string.IsNullOrEmpty(GetTerminalSN))
                return string.Format(" TerminalSN like '%" + GetTerminalSN + "%' and ");
            else
                return string.Format("");
        }

        private string sGetTerminalIDCond(string TerminalID)
        {
            if (!string.IsNullOrEmpty(TerminalID))
                return string.Format(" TerminalID like '%" + TerminalID + "%' and ");
            else
                return string.Format("");
        }

        private string sGetAppNameCond(string AppName)
        {
            if (!string.IsNullOrEmpty(AppName))
                return string.Format(" AppName like '%" + AppName + "%' and ");
            else
                return string.Format("");
        }

        private string sGetComTypeCond(string ComType)
        {
            if (!string.IsNullOrEmpty(ComType))
                return string.Format(" CommsTypeName like '%" + ComType + "%' and ");
            else
                return string.Format("");
        }

        private string sGetDateCond()
        {
            if (chkDate.Checked == true)
                return string.Format(" (datediff ( day, '" + dtpDateStart.Value.ToString("MM/dd/yyyy") + "', DateCreated) >= 0 AND datediff(day,DateCreated, '" + dtpDateEnd.Value.ToString("MM/dd/yyyy") + "') >= 0) and ");
            else
                return string.Format("");
        }
        #endregion

        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

       
    }
}
