namespace InSys
{
    partial class FrmAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAbout));
            this.PictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblProgramVersion = new System.Windows.Forms.Label();
            this.boxLicenseAgreement = new System.Windows.Forms.GroupBox();
            this.webLicenseAgreement = new System.Windows.Forms.WebBrowser();
            this.boxContactUs = new System.Windows.Forms.GroupBox();
            this.lnkMail = new System.Windows.Forms.LinkLabel();
            this.lnkWebsite = new System.Windows.Forms.LinkLabel();
            this.lblEmailTS = new System.Windows.Forms.Label();
            this.lblWebsite = new System.Windows.Forms.Label();
            this.lblKota = new System.Windows.Forms.Label();
            this.lblAlamat = new System.Windows.Forms.Label();
            this.lblKompleks = new System.Windows.Forms.Label();
            this.lblIntegraPratama = new System.Windows.Forms.Label();
            this.boxButton = new System.Windows.Forms.GroupBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.boxDisclaimer = new System.Windows.Forms.GroupBox();
            this.lblDisclaimer = new System.Windows.Forms.Label();
            this.tmrAbout = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).BeginInit();
            this.boxLicenseAgreement.SuspendLayout();
            this.boxContactUs.SuspendLayout();
            this.boxButton.SuspendLayout();
            this.boxDisclaimer.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureBox2
            // 
            this.PictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("PictureBox2.Image")));
            this.PictureBox2.Location = new System.Drawing.Point(12, 6);
            this.PictureBox2.Name = "PictureBox2";
            this.PictureBox2.Size = new System.Drawing.Size(353, 77);
            this.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox2.TabIndex = 7;
            this.PictureBox2.TabStop = false;
            // 
            // lblProgramVersion
            // 
            this.lblProgramVersion.Location = new System.Drawing.Point(371, 9);
            this.lblProgramVersion.Name = "lblProgramVersion";
            this.lblProgramVersion.Size = new System.Drawing.Size(111, 71);
            this.lblProgramVersion.TabIndex = 2;
            this.lblProgramVersion.Text = "Program Version";
            this.lblProgramVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // boxLicenseAgreement
            // 
            this.boxLicenseAgreement.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.boxLicenseAgreement.Controls.Add(this.webLicenseAgreement);
            this.boxLicenseAgreement.Location = new System.Drawing.Point(12, 83);
            this.boxLicenseAgreement.Name = "boxLicenseAgreement";
            this.boxLicenseAgreement.Size = new System.Drawing.Size(470, 240);
            this.boxLicenseAgreement.TabIndex = 1;
            this.boxLicenseAgreement.TabStop = false;
            this.boxLicenseAgreement.Text = "License Agreement";
            // 
            // webLicenseAgreement
            // 
            this.webLicenseAgreement.AllowNavigation = false;
            this.webLicenseAgreement.AllowWebBrowserDrop = false;
            this.webLicenseAgreement.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.webLicenseAgreement.IsWebBrowserContextMenuEnabled = false;
            this.webLicenseAgreement.Location = new System.Drawing.Point(6, 13);
            this.webLicenseAgreement.MinimumSize = new System.Drawing.Size(20, 20);
            this.webLicenseAgreement.Name = "webLicenseAgreement";
            this.webLicenseAgreement.ScriptErrorsSuppressed = true;
            this.webLicenseAgreement.Size = new System.Drawing.Size(458, 220);
            this.webLicenseAgreement.TabIndex = 0;
            this.webLicenseAgreement.WebBrowserShortcutsEnabled = false;
            this.webLicenseAgreement.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webLicenseAgreement_DocumentCompleted);
            // 
            // boxContactUs
            // 
            this.boxContactUs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.boxContactUs.Controls.Add(this.lnkMail);
            this.boxContactUs.Controls.Add(this.lnkWebsite);
            this.boxContactUs.Controls.Add(this.lblEmailTS);
            this.boxContactUs.Controls.Add(this.lblWebsite);
            this.boxContactUs.Controls.Add(this.lblKota);
            this.boxContactUs.Controls.Add(this.lblAlamat);
            this.boxContactUs.Controls.Add(this.lblKompleks);
            this.boxContactUs.Controls.Add(this.lblIntegraPratama);
            this.boxContactUs.Location = new System.Drawing.Point(12, 324);
            this.boxContactUs.Name = "boxContactUs";
            this.boxContactUs.Size = new System.Drawing.Size(470, 85);
            this.boxContactUs.TabIndex = 3;
            this.boxContactUs.TabStop = false;
            this.boxContactUs.Text = "Contact Us";
            // 
            // lnkMail
            // 
            this.lnkMail.AutoSize = true;
            this.lnkMail.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkMail.Location = new System.Drawing.Point(297, 49);
            this.lnkMail.Name = "lnkMail";
            this.lnkMail.Size = new System.Drawing.Size(157, 13);
            this.lnkMail.TabIndex = 8;
            this.lnkMail.TabStop = true;
            this.lnkMail.Text = "itsupport@integra-pratama.co.id";
            this.lnkMail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkMail_LinkClicked);
            // 
            // lnkWebsite
            // 
            this.lnkWebsite.AutoSize = true;
            this.lnkWebsite.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.lnkWebsite.Location = new System.Drawing.Point(249, 34);
            this.lnkWebsite.Name = "lnkWebsite";
            this.lnkWebsite.Size = new System.Drawing.Size(133, 13);
            this.lnkWebsite.TabIndex = 7;
            this.lnkWebsite.TabStop = true;
            this.lnkWebsite.Text = "www.integra-pratama.co.id";
            this.lnkWebsite.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkWebsite_LinkClicked);
            // 
            // lblEmailTS
            // 
            this.lblEmailTS.AutoSize = true;
            this.lblEmailTS.Location = new System.Drawing.Point(200, 49);
            this.lblEmailTS.Name = "lblEmailTS";
            this.lblEmailTS.Size = new System.Drawing.Size(100, 13);
            this.lblEmailTS.TabIndex = 6;
            this.lblEmailTS.Text = "Technical Support :";
            // 
            // lblWebsite
            // 
            this.lblWebsite.AutoSize = true;
            this.lblWebsite.Location = new System.Drawing.Point(200, 34);
            this.lblWebsite.Name = "lblWebsite";
            this.lblWebsite.Size = new System.Drawing.Size(52, 13);
            this.lblWebsite.TabIndex = 5;
            this.lblWebsite.Text = "Website :";
            // 
            // lblKota
            // 
            this.lblKota.AutoSize = true;
            this.lblKota.Location = new System.Drawing.Point(6, 64);
            this.lblKota.Name = "lblKota";
            this.lblKota.Size = new System.Drawing.Size(130, 13);
            this.lblKota.TabIndex = 4;
            this.lblKota.Text = "Jakarta 11530 - Indonesia";
            // 
            // lblAlamat
            // 
            this.lblAlamat.AutoSize = true;
            this.lblAlamat.Location = new System.Drawing.Point(6, 49);
            this.lblAlamat.Name = "lblAlamat";
            this.lblAlamat.Size = new System.Drawing.Size(131, 13);
            this.lblAlamat.TabIndex = 3;
            this.lblAlamat.Text = "Jl. Raya Perjuangan No. 1";
            // 
            // lblKompleks
            // 
            this.lblKompleks.AutoSize = true;
            this.lblKompleks.Location = new System.Drawing.Point(6, 34);
            this.lblKompleks.Name = "lblKompleks";
            this.lblKompleks.Size = new System.Drawing.Size(154, 13);
            this.lblKompleks.TabIndex = 2;
            this.lblKompleks.Text = "Komplek Graha Mas Blok B/16";
            // 
            // lblIntegraPratama
            // 
            this.lblIntegraPratama.AutoSize = true;
            this.lblIntegraPratama.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntegraPratama.Location = new System.Drawing.Point(6, 16);
            this.lblIntegraPratama.Name = "lblIntegraPratama";
            this.lblIntegraPratama.Size = new System.Drawing.Size(131, 14);
            this.lblIntegraPratama.TabIndex = 1;
            this.lblIntegraPratama.Text = "PT. Integra Pratama";
            // 
            // boxButton
            // 
            this.boxButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.boxButton.Controls.Add(this.btnOK);
            this.boxButton.Location = new System.Drawing.Point(393, 410);
            this.boxButton.Name = "boxButton";
            this.boxButton.Size = new System.Drawing.Size(89, 45);
            this.boxButton.TabIndex = 0;
            this.boxButton.TabStop = false;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOK.Location = new System.Drawing.Point(6, 14);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(77, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // boxDisclaimer
            // 
            this.boxDisclaimer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.boxDisclaimer.Controls.Add(this.lblDisclaimer);
            this.boxDisclaimer.Location = new System.Drawing.Point(12, 410);
            this.boxDisclaimer.Name = "boxDisclaimer";
            this.boxDisclaimer.Size = new System.Drawing.Size(375, 45);
            this.boxDisclaimer.TabIndex = 4;
            this.boxDisclaimer.TabStop = false;
            // 
            // lblDisclaimer
            // 
            this.lblDisclaimer.AutoSize = true;
            this.lblDisclaimer.Location = new System.Drawing.Point(6, 19);
            this.lblDisclaimer.Name = "lblDisclaimer";
            this.lblDisclaimer.Size = new System.Drawing.Size(280, 13);
            this.lblDisclaimer.TabIndex = 0;
            this.lblDisclaimer.Text = "Copyright � 2010 PT. Integra Pratama. All rights reserved.";
            // 
            // tmrAbout
            // 
            this.tmrAbout.Interval = 10000;
            this.tmrAbout.Tick += new System.EventHandler(this.tmrAbout_Tick);
            // 
            // FrmAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(494, 468);
            this.Controls.Add(this.boxDisclaimer);
            this.Controls.Add(this.boxButton);
            this.Controls.Add(this.boxContactUs);
            this.Controls.Add(this.boxLicenseAgreement);
            this.Controls.Add(this.lblProgramVersion);
            this.Controls.Add(this.PictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAbout";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About";
            this.Load += new System.EventHandler(this.FrmAbout_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox2)).EndInit();
            this.boxLicenseAgreement.ResumeLayout(false);
            this.boxContactUs.ResumeLayout(false);
            this.boxContactUs.PerformLayout();
            this.boxButton.ResumeLayout(false);
            this.boxDisclaimer.ResumeLayout(false);
            this.boxDisclaimer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.PictureBox PictureBox2;
        private System.Windows.Forms.Label lblProgramVersion;
        private System.Windows.Forms.GroupBox boxLicenseAgreement;
        private System.Windows.Forms.GroupBox boxContactUs;
        private System.Windows.Forms.WebBrowser webLicenseAgreement;
        private System.Windows.Forms.Label lblEmailTS;
        private System.Windows.Forms.Label lblWebsite;
        private System.Windows.Forms.Label lblKota;
        private System.Windows.Forms.Label lblAlamat;
        private System.Windows.Forms.Label lblKompleks;
        private System.Windows.Forms.Label lblIntegraPratama;
        private System.Windows.Forms.GroupBox boxButton;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox boxDisclaimer;
        private System.Windows.Forms.Label lblDisclaimer;
        private System.Windows.Forms.LinkLabel lnkWebsite;
        private System.Windows.Forms.LinkLabel lnkMail;
        private System.Windows.Forms.Timer tmrAbout;
    }
}