namespace InSys
{
    partial class FrmSetFolder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSetFolder));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.fbDialogSource = new System.Windows.Forms.FolderBrowserDialog();
            this.txtAutoInitFolder = new System.Windows.Forms.TextBox();
            this.btnAutoInitBrowse = new System.Windows.Forms.Button();
            this.gbAutoInitReport = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbAutoInitReport.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSave.Location = new System.Drawing.Point(151, 11);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 25);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(255, 11);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(97, 25);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // fbDialogSource
            // 
            this.fbDialogSource.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // txtAutoInitFolder
            // 
            this.txtAutoInitFolder.Location = new System.Drawing.Point(6, 19);
            this.txtAutoInitFolder.Name = "txtAutoInitFolder";
            this.txtAutoInitFolder.Size = new System.Drawing.Size(250, 20);
            this.txtAutoInitFolder.TabIndex = 0;
            // 
            // btnAutoInitBrowse
            // 
            this.btnAutoInitBrowse.Location = new System.Drawing.Point(262, 17);
            this.btnAutoInitBrowse.Name = "btnAutoInitBrowse";
            this.btnAutoInitBrowse.Size = new System.Drawing.Size(90, 23);
            this.btnAutoInitBrowse.TabIndex = 1;
            this.btnAutoInitBrowse.Text = "Browse";
            this.btnAutoInitBrowse.UseVisualStyleBackColor = true;
            this.btnAutoInitBrowse.Click += new System.EventHandler(this.btnAutoInitBrowse_Click);
            // 
            // gbAutoInitReport
            // 
            this.gbAutoInitReport.Controls.Add(this.txtAutoInitFolder);
            this.gbAutoInitReport.Controls.Add(this.btnAutoInitBrowse);
            this.gbAutoInitReport.Location = new System.Drawing.Point(9, 1);
            this.gbAutoInitReport.Name = "gbAutoInitReport";
            this.gbAutoInitReport.Size = new System.Drawing.Size(361, 52);
            this.gbAutoInitReport.TabIndex = 0;
            this.gbAutoInitReport.TabStop = false;
            this.gbAutoInitReport.Text = "Auto Init Report Path";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Location = new System.Drawing.Point(9, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(361, 47);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // FrmSetFolder
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(384, 112);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbAutoInitReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(390, 150);
            this.MinimumSize = new System.Drawing.Size(390, 140);
            this.Name = "FrmSetFolder";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Set Folder Path";
            this.Load += new System.EventHandler(this.FrmSetFolder_Load);
            this.gbAutoInitReport.ResumeLayout(false);
            this.gbAutoInitReport.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.FolderBrowserDialog fbDialogSource;
        private System.Windows.Forms.TextBox txtAutoInitFolder;
        private System.Windows.Forms.Button btnAutoInitBrowse;
        private System.Windows.Forms.GroupBox gbAutoInitReport;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}