using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmSetFolder : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected string sFolderPath { get { return txtAutoInitFolder.Text; } }
        protected string sAutoInitItemName = "Report AutoInit Path";
        protected string sInitCompressItemname = "GZip Path";

        /// <summary>
        /// Form to set folder's path for Init Compress and Auto Init Report
        /// </summary>
        /// <param name="_oSqlconn">SqlConnection : Connection string to database</param>
        public FrmSetFolder(SqlConnection _oSqlconn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlconn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        private void FrmSetFolder_Load(object sender, EventArgs e)
        {
            InitData();
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        private void btnAutoInitBrowse_Click(object sender, EventArgs e)
        {
            SetBrowsePath(ref txtAutoInitFolder);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValid() && 
                CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
            {
                AddFolder();
                string sLogDetail = string.Format("{0} : {1} \n", "Auto Init Path", sGetAutoInitPath());
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", "Save Folder Paths", sLogDetail);
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Display saved data from database into form
        /// </summary>
        protected void InitData()
        {
            try
            {
                LoadData(sAutoInitItemName, ref txtAutoInitFolder);
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Clear data shown in forms
        /// </summary>
        protected void ClearData()
        {
            txtAutoInitFolder.Clear();
        }
        
        /// <summary>
        /// Load path saved in database 
        /// </summary>
        /// <param name="sItemName">string : Item's name which value will be shown</param>
        /// <param name="oTxt">TextBox : Object where value will be assigned</param>
        protected void LoadData(string sItemName, ref TextBox oTxt)
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPControlFlagBrowse, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sItemName", SqlDbType.VarChar).Value = sItemName;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        oTxt.Text = oRead["Flag"].ToString();
                    oRead.Close();
                }
            }
        }

        /// <summary>
        /// Set selected path to Textbox
        /// </summary>
        /// <param name="oTxt">TextBox : object to show folder path</param>
        protected void SetBrowsePath(ref TextBox oTxt)
        {
            try
            {
                if (!string.IsNullOrEmpty(oTxt.Text))
                    fbDialogSource.SelectedPath = oTxt.Text;
                else
                    fbDialogSource.SelectedPath = Application.StartupPath;
                fbDialogSource.ShowDialog();
                if (!string.IsNullOrEmpty(fbDialogSource.SelectedPath))
                {
                    oTxt.Text = fbDialogSource.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Get current Auto Init Path
        /// </summary>
        /// <returns>string : Auto Init Path</returns>
        protected string sGetAutoInitPath()
        {
            return txtAutoInitFolder.Text.Trim();
        }

        /// <summary>
        /// Determine whether path's values valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool isValid()
        {
            if (string.IsNullOrEmpty(sGetAutoInitPath()))
                MessageBox.Show(CommonMessage.sAutoInitPathEmpty);
            else 
                return true;
            return false;
        }

        /// <summary>
        /// Save path value to database
        /// </summary>
        protected void AddFolder()
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPControlFlagUpdate, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;

                oSqlCmd.Parameters.Add("@sAutoInitItemName", SqlDbType.VarChar).Value = sAutoInitItemName;
                oSqlCmd.Parameters.Add("@sAutoInitFlag", SqlDbType.VarChar).Value = txtAutoInitFolder.Text;
                oSqlCmd.Parameters.Add("@sInitCompressItemName", SqlDbType.VarChar).Value = sInitCompressItemname;

                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

    }
}