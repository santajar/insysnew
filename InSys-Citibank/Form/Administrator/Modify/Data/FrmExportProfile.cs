using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using InSysClass;

namespace InSys
{
    public partial class FrmExportProfile : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        protected string sDatabaseId=null;
        protected string sDatabaseName=null;
        protected string sTerminalID=null;
        protected bool isClose = false;
        protected bool isFullTable;

        /// <summary>
        /// Form to export profiles to text file
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection String to database</param>
        public FrmExportProfile(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        private void FrmExportData_Load(object sender, EventArgs e)
        {
            InitComboBoxDatabase();
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        private void FrmExportData_FormClosed(object sender, FormClosedEventArgs e)
        {
            bwExportProgress.Dispose();
        }

        private void FrmExportProfile_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isClose) e.Cancel = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            isClose = true;
            this.Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {            
            InitData();
            if (IsValid())
            {
                Toggle(false);
                //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDatabaseName, 
                //    CommonMessage.sExportProfileToText, "");
                bwExportProgress.RunWorkerAsync();
                pbExport.Style = ProgressBarStyle.Marquee;
                
            }
            else
                MessageBox.Show(CommonMessage.sErrExportProfileToText);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            chkMasterProfile.Checked = false;
            chkProfile.Checked = false;
        }

        private void bwExportProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            string sFolderPath = Application.StartupPath + "\\FILE\\";
            if (!Directory.Exists(sFolderPath)) Directory.CreateDirectory(sFolderPath);
            string sFileName = null;
            DataTable dtProfileContent = dtGetProfileContent();
            foreach (DataRow drow in dtProfileContent.Rows)
            {
                if (bwExportProgress.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                sFileName = sFolderPath + drow["TerminalId"] + ".txt";
                File.Delete(sFileName);
                CommonLib.Write2File(sFileName, drow["Content"].ToString(), false);
            }
        }

        private void bwExportProgress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pbExport.Style = ProgressBarStyle.Blocks;
            MessageBox.Show("Process Done.");
            Toggle(true);
            string sDatabaseName = ((DataRowView)cmbDatabase.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = string.Format("{0} : {1} \n ", "Profile", sBooleanToString(chkProfile.Checked));
            sLogDetail += string.Format("{0} : {1} \n ", "Master Profile", sBooleanToString(chkMasterProfile.Checked));
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDatabaseName, CommonMessage.sExportProfileToText, sLogDetail);
        }

        private void txtTerminalID_Enter(object sender, EventArgs e)
        {
            txtTerminalID.Clear();
            txtTerminalID.ForeColor = Color.Black;
            txtTerminalID.CharacterCasing = CharacterCasing.Upper;
        }

        private void txtTerminalID_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTerminalID.Text))
            {
                txtTerminalID.CharacterCasing = CharacterCasing.Normal;
                txtTerminalID.ForeColor = Color.Gray;
                txtTerminalID.Text = "TerminalID";
            }
        }

        private void chkFullTable_CheckedChanged(object sender, EventArgs e)
        {
            isFullTable = chkFullTable.Checked;
        }

        #region "Function"
        /// <summary>
        /// Set group boxes enability based on given value
        /// </summary>
        /// <param name="bEnable">boolean : to determined groupboxes status</param>
        protected void Toggle(bool bEnable)
        {
            gbSource.Enabled = bEnable;
            gbButton.Enabled = bEnable;
        }

        /// <summary>
        /// Initiate Data to form
        /// </summary>
        protected void InitData()
        {
            sDatabaseId = cmbDatabase.SelectedValue.ToString();
            sDatabaseName = cmbDatabase.Text;
            sTerminalID = txtTerminalID.Text.Length > 8 ? null : txtTerminalID.Text;
        }

        /// <summary>
        /// Initiate ComboBox with data from database
        /// </summary>
        protected void InitComboBoxDatabase()
        {
            DataTable dtDatabaseName = dtGetDatabase();
            if (dtDatabaseName.Rows.Count > 0)
            {
                cmbDatabase.DataSource = dtDatabaseName;
                cmbDatabase.DisplayMember = "DatabaseName";
                cmbDatabase.ValueMember = "DatabaseId";
            }
        }

        /// <summary>
        /// Load list of versions from database and save it in DataTable
        /// </summary>
        /// <returns>DataTable : Use to store data from database</returns>
        protected DataTable dtGetDatabase()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);

            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();
            return dtTemp;
        }

        /// <summary>
        /// Determine whether data enterd by user valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValid()
        {
            if (!string.IsNullOrEmpty(sDatabaseId))
                if (chkProfile.CheckState == CheckState.Unchecked && 
                    chkMasterProfile.CheckState == CheckState.Unchecked)
                    return false;
                else
                    return true;
            else
                return false;
        }

        /// <summary>
        /// Generate Condition string based on user inputed data
        /// </summary>
        /// <returns>string : Condition string to filter database</returns>
        protected string sCondition()
        {
            string sCond = "WHERE DatabaseId=" + sDatabaseId;
            if (chkProfile.CheckState == CheckState.Checked &&
                chkMasterProfile.CheckState == CheckState.Unchecked)
                sCond += " AND StatusMaster=0";
            else if (chkProfile.CheckState == CheckState.Unchecked &&
                chkMasterProfile.CheckState == CheckState.Checked)
                sCond += " AND StatusMaster=1";
            if (!string.IsNullOrEmpty(sTerminalID) && sTerminalID.Length <= 8)
                sCond += string.Format(" AND TerminalID LIKE '{0}%'", sTerminalID);
            return sCond;
        }

        /// <summary>
        /// Load data profile from database
        /// </summary>
        /// <returns>DataTable : Use to store data from database</returns>
        protected DataTable dtGetProfileContent()
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPExportProfileToText, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.CommandTimeout = 0;
            oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sCondition();
            oCmd.Parameters.Add("@iIsFullTable", SqlDbType.Bit).Value = isFullTable ? 1 : 0;

            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();
            
            new SqlDataAdapter(oCmd).Fill(dtTemp);

            oCmd.Dispose();
            return dtTemp;
        }

        /// <summary>
        /// Convert boolean value to string value
        /// </summary>
        /// <param name="isValue">boolean : bool value which will be converted</param>
        /// <returns>string : result value from bool conversion</returns>
        protected string sBooleanToString(bool isValue)
        {
            return (isValue) ? "TRUE" : "FALSE";
        }
        #endregion
    }
}