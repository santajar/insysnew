﻿namespace InSys
{
    partial class frmCopyData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCopyData));
            this.lblNewTID = new System.Windows.Forms.Label();
            this.txtNewTID = new System.Windows.Forms.TextBox();
            this.gbDestination = new System.Windows.Forms.GroupBox();
            this.dgvTidMaster = new System.Windows.Forms.DataGridView();
            this.colSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cmbDBDest = new System.Windows.Forms.ComboBox();
            this.lblDatabaseDest = new System.Windows.Forms.Label();
            this.gbOriginal = new System.Windows.Forms.GroupBox();
            this.dgvTidOriginal = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cmbDBOriginal = new System.Windows.Forms.ComboBox();
            this.lblDatabaseFrom = new System.Windows.Forms.Label();
            this.btnexecute = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.rbCopyData = new System.Windows.Forms.RadioButton();
            this.rbMoveData = new System.Windows.Forms.RadioButton();
            this.gbDestination.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTidMaster)).BeginInit();
            this.gbOriginal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTidOriginal)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNewTID
            // 
            this.lblNewTID.AutoSize = true;
            this.lblNewTID.Location = new System.Drawing.Point(12, 18);
            this.lblNewTID.Name = "lblNewTID";
            this.lblNewTID.Size = new System.Drawing.Size(86, 13);
            this.lblNewTID.TabIndex = 0;
            this.lblNewTID.Text = "New TerminalID ";
            // 
            // txtNewTID
            // 
            this.txtNewTID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNewTID.Location = new System.Drawing.Point(104, 15);
            this.txtNewTID.MaxLength = 8;
            this.txtNewTID.Name = "txtNewTID";
            this.txtNewTID.Size = new System.Drawing.Size(121, 20);
            this.txtNewTID.TabIndex = 1;
            // 
            // gbDestination
            // 
            this.gbDestination.Controls.Add(this.dgvTidMaster);
            this.gbDestination.Controls.Add(this.cmbDBDest);
            this.gbDestination.Controls.Add(this.lblDatabaseDest);
            this.gbDestination.Location = new System.Drawing.Point(289, 50);
            this.gbDestination.Name = "gbDestination";
            this.gbDestination.Size = new System.Drawing.Size(268, 364);
            this.gbDestination.TabIndex = 2;
            this.gbDestination.TabStop = false;
            this.gbDestination.Text = "Destination";
            // 
            // dgvTidMaster
            // 
            this.dgvTidMaster.AllowUserToAddRows = false;
            this.dgvTidMaster.AllowUserToDeleteRows = false;
            this.dgvTidMaster.AllowUserToResizeRows = false;
            this.dgvTidMaster.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTidMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTidMaster.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSelect});
            this.dgvTidMaster.Location = new System.Drawing.Point(6, 50);
            this.dgvTidMaster.Name = "dgvTidMaster";
            this.dgvTidMaster.RowHeadersVisible = false;
            this.dgvTidMaster.RowHeadersWidth = 40;
            this.dgvTidMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTidMaster.Size = new System.Drawing.Size(256, 308);
            this.dgvTidMaster.TabIndex = 2;
            // 
            // colSelect
            // 
            this.colSelect.HeaderText = "Select";
            this.colSelect.Name = "colSelect";
            this.colSelect.Width = 50;
            // 
            // cmbDBDest
            // 
            this.cmbDBDest.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDBDest.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDBDest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDBDest.FormattingEnabled = true;
            this.cmbDBDest.Location = new System.Drawing.Point(121, 23);
            this.cmbDBDest.Name = "cmbDBDest";
            this.cmbDBDest.Size = new System.Drawing.Size(121, 21);
            this.cmbDBDest.TabIndex = 1;
            this.cmbDBDest.SelectedIndexChanged += new System.EventHandler(this.cmbDBDest_SelectedIndexChanged);
            // 
            // lblDatabaseDest
            // 
            this.lblDatabaseDest.AutoSize = true;
            this.lblDatabaseDest.Location = new System.Drawing.Point(6, 26);
            this.lblDatabaseDest.Name = "lblDatabaseDest";
            this.lblDatabaseDest.Size = new System.Drawing.Size(109, 13);
            this.lblDatabaseDest.TabIndex = 0;
            this.lblDatabaseDest.Text = "Database Destination";
            // 
            // gbOriginal
            // 
            this.gbOriginal.Controls.Add(this.dgvTidOriginal);
            this.gbOriginal.Controls.Add(this.cmbDBOriginal);
            this.gbOriginal.Controls.Add(this.lblDatabaseFrom);
            this.gbOriginal.Location = new System.Drawing.Point(15, 50);
            this.gbOriginal.Name = "gbOriginal";
            this.gbOriginal.Size = new System.Drawing.Size(268, 364);
            this.gbOriginal.TabIndex = 3;
            this.gbOriginal.TabStop = false;
            this.gbOriginal.Text = "Original";
            // 
            // dgvTidOriginal
            // 
            this.dgvTidOriginal.AllowUserToAddRows = false;
            this.dgvTidOriginal.AllowUserToDeleteRows = false;
            this.dgvTidOriginal.AllowUserToResizeRows = false;
            this.dgvTidOriginal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTidOriginal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTidOriginal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1});
            this.dgvTidOriginal.Location = new System.Drawing.Point(6, 50);
            this.dgvTidOriginal.Name = "dgvTidOriginal";
            this.dgvTidOriginal.RowHeadersVisible = false;
            this.dgvTidOriginal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTidOriginal.Size = new System.Drawing.Size(256, 308);
            this.dgvTidOriginal.TabIndex = 3;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "Select";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 50;
            // 
            // cmbDBOriginal
            // 
            this.cmbDBOriginal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDBOriginal.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDBOriginal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDBOriginal.FormattingEnabled = true;
            this.cmbDBOriginal.Location = new System.Drawing.Point(103, 23);
            this.cmbDBOriginal.Name = "cmbDBOriginal";
            this.cmbDBOriginal.Size = new System.Drawing.Size(121, 21);
            this.cmbDBOriginal.TabIndex = 1;
            this.cmbDBOriginal.SelectedIndexChanged += new System.EventHandler(this.cmbDBOriginal_SelectedIndexChanged);
            // 
            // lblDatabaseFrom
            // 
            this.lblDatabaseFrom.AutoSize = true;
            this.lblDatabaseFrom.Location = new System.Drawing.Point(6, 26);
            this.lblDatabaseFrom.Name = "lblDatabaseFrom";
            this.lblDatabaseFrom.Size = new System.Drawing.Size(91, 13);
            this.lblDatabaseFrom.TabIndex = 0;
            this.lblDatabaseFrom.Text = "Database Original";
            // 
            // btnexecute
            // 
            this.btnexecute.Location = new System.Drawing.Point(401, 420);
            this.btnexecute.Name = "btnexecute";
            this.btnexecute.Size = new System.Drawing.Size(75, 23);
            this.btnexecute.TabIndex = 4;
            this.btnexecute.Text = "Execute";
            this.btnexecute.UseVisualStyleBackColor = true;
            this.btnexecute.Click += new System.EventHandler(this.btnexecute_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(482, 420);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // rbCopyData
            // 
            this.rbCopyData.AutoSize = true;
            this.rbCopyData.Checked = true;
            this.rbCopyData.Location = new System.Drawing.Point(248, 16);
            this.rbCopyData.Name = "rbCopyData";
            this.rbCopyData.Size = new System.Drawing.Size(75, 17);
            this.rbCopyData.TabIndex = 6;
            this.rbCopyData.TabStop = true;
            this.rbCopyData.Text = "Copy Data";
            this.rbCopyData.UseVisualStyleBackColor = true;
            this.rbCopyData.CheckedChanged += new System.EventHandler(this.rbCopyData_CheckedChanged);
            // 
            // rbMoveData
            // 
            this.rbMoveData.AutoSize = true;
            this.rbMoveData.Location = new System.Drawing.Point(329, 16);
            this.rbMoveData.Name = "rbMoveData";
            this.rbMoveData.Size = new System.Drawing.Size(78, 17);
            this.rbMoveData.TabIndex = 7;
            this.rbMoveData.Text = "Move Data";
            this.rbMoveData.UseVisualStyleBackColor = true;
            this.rbMoveData.CheckedChanged += new System.EventHandler(this.rbMoveData_CheckedChanged);
            // 
            // frmCopyData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(568, 457);
            this.Controls.Add(this.rbMoveData);
            this.Controls.Add(this.rbCopyData);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnexecute);
            this.Controls.Add(this.gbOriginal);
            this.Controls.Add(this.gbDestination);
            this.Controls.Add(this.txtNewTID);
            this.Controls.Add(this.lblNewTID);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmCopyData";
            this.Text = "Copy Data Form";
            this.Load += new System.EventHandler(this.frmCopyData_Load);
            this.gbDestination.ResumeLayout(false);
            this.gbDestination.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTidMaster)).EndInit();
            this.gbOriginal.ResumeLayout(false);
            this.gbOriginal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTidOriginal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNewTID;
        private System.Windows.Forms.TextBox txtNewTID;
        private System.Windows.Forms.GroupBox gbDestination;
        private System.Windows.Forms.ComboBox cmbDBDest;
        private System.Windows.Forms.Label lblDatabaseDest;
        private System.Windows.Forms.GroupBox gbOriginal;
        private System.Windows.Forms.ComboBox cmbDBOriginal;
        private System.Windows.Forms.Label lblDatabaseFrom;
        private System.Windows.Forms.Button btnexecute;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView dgvTidMaster;
        private System.Windows.Forms.DataGridView dgvTidOriginal;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colSelect;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.RadioButton rbCopyData;
        private System.Windows.Forms.RadioButton rbMoveData;
    }
}