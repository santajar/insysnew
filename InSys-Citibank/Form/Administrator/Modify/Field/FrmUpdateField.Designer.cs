namespace InSys
{
    partial class FrmUpdateField
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUpdateField));
            this.pbAddFields = new System.Windows.Forms.ProgressBar();
            this.txtKeterangan = new System.Windows.Forms.TextBox();
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.chkIncludeMs = new System.Windows.Forms.CheckBox();
            this.cmbTerminal = new System.Windows.Forms.ComboBox();
            this.cmbDbSource = new System.Windows.Forms.ComboBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.lblDbSource = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnExecuteAll = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.tabFields = new System.Windows.Forms.TabControl();
            this.tabTerminal = new System.Windows.Forms.TabPage();
            this.gbTerminalNew = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTerminalMore = new System.Windows.Forms.Button();
            this.gbTerminalOld = new System.Windows.Forms.GroupBox();
            this.txtTerminalOldValue = new System.Windows.Forms.TextBox();
            this.txtTerminalOldTag = new System.Windows.Forms.TextBox();
            this.lblTerminalOldValue = new System.Windows.Forms.Label();
            this.lblTerminalOldTag = new System.Windows.Forms.Label();
            this.tabAcquirer = new System.Windows.Forms.TabPage();
            this.gbAcquirerNew = new System.Windows.Forms.GroupBox();
            this.txtAcquirerNewValue = new System.Windows.Forms.TextBox();
            this.txtAcquirerNewTag = new System.Windows.Forms.TextBox();
            this.lblAcquirerNewValue = new System.Windows.Forms.Label();
            this.lblAcquirerNewTag = new System.Windows.Forms.Label();
            this.btnAcquirerMore = new System.Windows.Forms.Button();
            this.gbAcquirerOld = new System.Windows.Forms.GroupBox();
            this.txtAcquirerOldValue = new System.Windows.Forms.TextBox();
            this.txtAcquirerOldTag = new System.Windows.Forms.TextBox();
            this.lblAcquirerOldValue = new System.Windows.Forms.Label();
            this.lblAcquirerOldTag = new System.Windows.Forms.Label();
            this.tabIssuer = new System.Windows.Forms.TabPage();
            this.gbIssuerNew = new System.Windows.Forms.GroupBox();
            this.txtIssuerNewValue = new System.Windows.Forms.TextBox();
            this.txtIssuerNewTag = new System.Windows.Forms.TextBox();
            this.lblIssuerNewValue = new System.Windows.Forms.Label();
            this.lblIssuerNewTag = new System.Windows.Forms.Label();
            this.btnIssuerMore = new System.Windows.Forms.Button();
            this.gbIssuerOld = new System.Windows.Forms.GroupBox();
            this.txtIssuerOldValue = new System.Windows.Forms.TextBox();
            this.txtIssuerOldTag = new System.Windows.Forms.TextBox();
            this.lblIssuerOldValue = new System.Windows.Forms.Label();
            this.lblIssuerOldTag = new System.Windows.Forms.Label();
            this.tabCard = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtCardNewValue = new System.Windows.Forms.TextBox();
            this.txtCardNewTag = new System.Windows.Forms.TextBox();
            this.lblCardNewValue = new System.Windows.Forms.Label();
            this.lblCardNewTag = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtCardOldValue = new System.Windows.Forms.TextBox();
            this.txtCardOldTag = new System.Windows.Forms.TextBox();
            this.lblCardOldValue = new System.Windows.Forms.Label();
            this.lblCardOldTag = new System.Windows.Forms.Label();
            this.gbSource.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.tabFields.SuspendLayout();
            this.tabTerminal.SuspendLayout();
            this.gbTerminalNew.SuspendLayout();
            this.gbTerminalOld.SuspendLayout();
            this.tabAcquirer.SuspendLayout();
            this.gbAcquirerNew.SuspendLayout();
            this.gbAcquirerOld.SuspendLayout();
            this.tabIssuer.SuspendLayout();
            this.gbIssuerNew.SuspendLayout();
            this.gbIssuerOld.SuspendLayout();
            this.tabCard.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbAddFields
            // 
            this.pbAddFields.Location = new System.Drawing.Point(12, 137);
            this.pbAddFields.Name = "pbAddFields";
            this.pbAddFields.Size = new System.Drawing.Size(874, 23);
            this.pbAddFields.TabIndex = 5;
            // 
            // txtKeterangan
            // 
            this.txtKeterangan.Enabled = false;
            this.txtKeterangan.Location = new System.Drawing.Point(309, 14);
            this.txtKeterangan.Multiline = true;
            this.txtKeterangan.Name = "txtKeterangan";
            this.txtKeterangan.Size = new System.Drawing.Size(349, 117);
            this.txtKeterangan.TabIndex = 4;
            // 
            // gbSource
            // 
            this.gbSource.Controls.Add(this.chkIncludeMs);
            this.gbSource.Controls.Add(this.cmbTerminal);
            this.gbSource.Controls.Add(this.cmbDbSource);
            this.gbSource.Controls.Add(this.lblTerminal);
            this.gbSource.Controls.Add(this.lblDbSource);
            this.gbSource.Location = new System.Drawing.Point(12, 12);
            this.gbSource.Name = "gbSource";
            this.gbSource.Size = new System.Drawing.Size(275, 119);
            this.gbSource.TabIndex = 3;
            this.gbSource.TabStop = false;
            this.gbSource.Text = "Source";
            // 
            // chkIncludeMs
            // 
            this.chkIncludeMs.AutoSize = true;
            this.chkIncludeMs.Location = new System.Drawing.Point(19, 90);
            this.chkIncludeMs.Name = "chkIncludeMs";
            this.chkIncludeMs.Size = new System.Drawing.Size(96, 17);
            this.chkIncludeMs.TabIndex = 3;
            this.chkIncludeMs.Text = "Include Master";
            this.chkIncludeMs.UseVisualStyleBackColor = true;
            // 
            // cmbTerminal
            // 
            this.cmbTerminal.FormattingEnabled = true;
            this.cmbTerminal.Location = new System.Drawing.Point(97, 60);
            this.cmbTerminal.Name = "cmbTerminal";
            this.cmbTerminal.Size = new System.Drawing.Size(160, 21);
            this.cmbTerminal.TabIndex = 2;
            // 
            // cmbDbSource
            // 
            this.cmbDbSource.FormattingEnabled = true;
            this.cmbDbSource.Location = new System.Drawing.Point(97, 24);
            this.cmbDbSource.Name = "cmbDbSource";
            this.cmbDbSource.Size = new System.Drawing.Size(160, 21);
            this.cmbDbSource.TabIndex = 1;
            // 
            // lblTerminal
            // 
            this.lblTerminal.AutoSize = true;
            this.lblTerminal.Location = new System.Drawing.Point(16, 63);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(47, 13);
            this.lblTerminal.TabIndex = 1;
            this.lblTerminal.Text = "Terminal";
            // 
            // lblDbSource
            // 
            this.lblDbSource.AutoSize = true;
            this.lblDbSource.Location = new System.Drawing.Point(16, 27);
            this.lblDbSource.Name = "lblDbSource";
            this.lblDbSource.Size = new System.Drawing.Size(53, 13);
            this.lblDbSource.TabIndex = 0;
            this.lblDbSource.Text = "Database";
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnExecuteAll);
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Controls.Add(this.btnExecute);
            this.gbButton.Location = new System.Drawing.Point(12, 639);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(873, 41);
            this.gbButton.TabIndex = 6;
            this.gbButton.TabStop = false;
            // 
            // btnExecuteAll
            // 
            this.btnExecuteAll.Location = new System.Drawing.Point(555, 12);
            this.btnExecuteAll.Name = "btnExecuteAll";
            this.btnExecuteAll.Size = new System.Drawing.Size(100, 23);
            this.btnExecuteAll.TabIndex = 0;
            this.btnExecuteAll.Text = "Execute &All";
            this.btnExecuteAll.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(767, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(661, 12);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(100, 23);
            this.btnExecute.TabIndex = 1;
            this.btnExecute.Text = "&Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // tabFields
            // 
            this.tabFields.Controls.Add(this.tabTerminal);
            this.tabFields.Controls.Add(this.tabAcquirer);
            this.tabFields.Controls.Add(this.tabIssuer);
            this.tabFields.Controls.Add(this.tabCard);
            this.tabFields.Location = new System.Drawing.Point(12, 166);
            this.tabFields.Name = "tabFields";
            this.tabFields.SelectedIndex = 0;
            this.tabFields.Size = new System.Drawing.Size(874, 474);
            this.tabFields.TabIndex = 4;
            // 
            // tabTerminal
            // 
            this.tabTerminal.Controls.Add(this.gbTerminalNew);
            this.tabTerminal.Controls.Add(this.btnTerminalMore);
            this.tabTerminal.Controls.Add(this.gbTerminalOld);
            this.tabTerminal.Location = new System.Drawing.Point(4, 22);
            this.tabTerminal.Name = "tabTerminal";
            this.tabTerminal.Padding = new System.Windows.Forms.Padding(3);
            this.tabTerminal.Size = new System.Drawing.Size(866, 448);
            this.tabTerminal.TabIndex = 0;
            this.tabTerminal.Text = "Terminal (DE)";
            this.tabTerminal.UseVisualStyleBackColor = true;
            // 
            // gbTerminalNew
            // 
            this.gbTerminalNew.Controls.Add(this.textBox1);
            this.gbTerminalNew.Controls.Add(this.textBox2);
            this.gbTerminalNew.Controls.Add(this.label1);
            this.gbTerminalNew.Controls.Add(this.label2);
            this.gbTerminalNew.Location = new System.Drawing.Point(473, 1);
            this.gbTerminalNew.Name = "gbTerminalNew";
            this.gbTerminalNew.Size = new System.Drawing.Size(385, 438);
            this.gbTerminalNew.TabIndex = 2;
            this.gbTerminalNew.TabStop = false;
            this.gbTerminalNew.Text = "New";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(138, 44);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(229, 20);
            this.textBox1.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(20, 44);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(135, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Value";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tag";
            // 
            // btnTerminalMore
            // 
            this.btnTerminalMore.Location = new System.Drawing.Point(397, 213);
            this.btnTerminalMore.Name = "btnTerminalMore";
            this.btnTerminalMore.Size = new System.Drawing.Size(70, 23);
            this.btnTerminalMore.TabIndex = 1;
            this.btnTerminalMore.Text = "More...";
            this.btnTerminalMore.UseVisualStyleBackColor = true;
            // 
            // gbTerminalOld
            // 
            this.gbTerminalOld.Controls.Add(this.txtTerminalOldValue);
            this.gbTerminalOld.Controls.Add(this.txtTerminalOldTag);
            this.gbTerminalOld.Controls.Add(this.lblTerminalOldValue);
            this.gbTerminalOld.Controls.Add(this.lblTerminalOldTag);
            this.gbTerminalOld.Location = new System.Drawing.Point(6, 2);
            this.gbTerminalOld.Name = "gbTerminalOld";
            this.gbTerminalOld.Size = new System.Drawing.Size(385, 438);
            this.gbTerminalOld.TabIndex = 0;
            this.gbTerminalOld.TabStop = false;
            this.gbTerminalOld.Text = "Old";
            // 
            // txtTerminalOldValue
            // 
            this.txtTerminalOldValue.Location = new System.Drawing.Point(137, 43);
            this.txtTerminalOldValue.Name = "txtTerminalOldValue";
            this.txtTerminalOldValue.Size = new System.Drawing.Size(229, 20);
            this.txtTerminalOldValue.TabIndex = 3;
            // 
            // txtTerminalOldTag
            // 
            this.txtTerminalOldTag.Location = new System.Drawing.Point(19, 43);
            this.txtTerminalOldTag.Name = "txtTerminalOldTag";
            this.txtTerminalOldTag.Size = new System.Drawing.Size(100, 20);
            this.txtTerminalOldTag.TabIndex = 2;
            // 
            // lblTerminalOldValue
            // 
            this.lblTerminalOldValue.AutoSize = true;
            this.lblTerminalOldValue.Location = new System.Drawing.Point(134, 21);
            this.lblTerminalOldValue.Name = "lblTerminalOldValue";
            this.lblTerminalOldValue.Size = new System.Drawing.Size(34, 13);
            this.lblTerminalOldValue.TabIndex = 1;
            this.lblTerminalOldValue.Text = "Value";
            // 
            // lblTerminalOldTag
            // 
            this.lblTerminalOldTag.AutoSize = true;
            this.lblTerminalOldTag.Location = new System.Drawing.Point(16, 21);
            this.lblTerminalOldTag.Name = "lblTerminalOldTag";
            this.lblTerminalOldTag.Size = new System.Drawing.Size(26, 13);
            this.lblTerminalOldTag.TabIndex = 0;
            this.lblTerminalOldTag.Text = "Tag";
            // 
            // tabAcquirer
            // 
            this.tabAcquirer.Controls.Add(this.gbAcquirerNew);
            this.tabAcquirer.Controls.Add(this.btnAcquirerMore);
            this.tabAcquirer.Controls.Add(this.gbAcquirerOld);
            this.tabAcquirer.Location = new System.Drawing.Point(4, 22);
            this.tabAcquirer.Name = "tabAcquirer";
            this.tabAcquirer.Padding = new System.Windows.Forms.Padding(3);
            this.tabAcquirer.Size = new System.Drawing.Size(866, 448);
            this.tabAcquirer.TabIndex = 1;
            this.tabAcquirer.Text = "Acquirer (AA)";
            this.tabAcquirer.UseVisualStyleBackColor = true;
            // 
            // gbAcquirerNew
            // 
            this.gbAcquirerNew.Controls.Add(this.txtAcquirerNewValue);
            this.gbAcquirerNew.Controls.Add(this.txtAcquirerNewTag);
            this.gbAcquirerNew.Controls.Add(this.lblAcquirerNewValue);
            this.gbAcquirerNew.Controls.Add(this.lblAcquirerNewTag);
            this.gbAcquirerNew.Location = new System.Drawing.Point(474, 5);
            this.gbAcquirerNew.Name = "gbAcquirerNew";
            this.gbAcquirerNew.Size = new System.Drawing.Size(385, 438);
            this.gbAcquirerNew.TabIndex = 5;
            this.gbAcquirerNew.TabStop = false;
            this.gbAcquirerNew.Text = "New";
            // 
            // txtAcquirerNewValue
            // 
            this.txtAcquirerNewValue.Location = new System.Drawing.Point(138, 44);
            this.txtAcquirerNewValue.Name = "txtAcquirerNewValue";
            this.txtAcquirerNewValue.Size = new System.Drawing.Size(229, 20);
            this.txtAcquirerNewValue.TabIndex = 7;
            // 
            // txtAcquirerNewTag
            // 
            this.txtAcquirerNewTag.Location = new System.Drawing.Point(20, 44);
            this.txtAcquirerNewTag.Name = "txtAcquirerNewTag";
            this.txtAcquirerNewTag.Size = new System.Drawing.Size(100, 20);
            this.txtAcquirerNewTag.TabIndex = 6;
            // 
            // lblAcquirerNewValue
            // 
            this.lblAcquirerNewValue.AutoSize = true;
            this.lblAcquirerNewValue.Location = new System.Drawing.Point(135, 22);
            this.lblAcquirerNewValue.Name = "lblAcquirerNewValue";
            this.lblAcquirerNewValue.Size = new System.Drawing.Size(34, 13);
            this.lblAcquirerNewValue.TabIndex = 5;
            this.lblAcquirerNewValue.Text = "Value";
            // 
            // lblAcquirerNewTag
            // 
            this.lblAcquirerNewTag.AutoSize = true;
            this.lblAcquirerNewTag.Location = new System.Drawing.Point(17, 22);
            this.lblAcquirerNewTag.Name = "lblAcquirerNewTag";
            this.lblAcquirerNewTag.Size = new System.Drawing.Size(26, 13);
            this.lblAcquirerNewTag.TabIndex = 4;
            this.lblAcquirerNewTag.Text = "Tag";
            // 
            // btnAcquirerMore
            // 
            this.btnAcquirerMore.Location = new System.Drawing.Point(398, 217);
            this.btnAcquirerMore.Name = "btnAcquirerMore";
            this.btnAcquirerMore.Size = new System.Drawing.Size(70, 23);
            this.btnAcquirerMore.TabIndex = 4;
            this.btnAcquirerMore.Text = "More...";
            this.btnAcquirerMore.UseVisualStyleBackColor = true;
            // 
            // gbAcquirerOld
            // 
            this.gbAcquirerOld.Controls.Add(this.txtAcquirerOldValue);
            this.gbAcquirerOld.Controls.Add(this.txtAcquirerOldTag);
            this.gbAcquirerOld.Controls.Add(this.lblAcquirerOldValue);
            this.gbAcquirerOld.Controls.Add(this.lblAcquirerOldTag);
            this.gbAcquirerOld.Location = new System.Drawing.Point(7, 6);
            this.gbAcquirerOld.Name = "gbAcquirerOld";
            this.gbAcquirerOld.Size = new System.Drawing.Size(385, 438);
            this.gbAcquirerOld.TabIndex = 3;
            this.gbAcquirerOld.TabStop = false;
            this.gbAcquirerOld.Text = "Old";
            // 
            // txtAcquirerOldValue
            // 
            this.txtAcquirerOldValue.Location = new System.Drawing.Point(137, 43);
            this.txtAcquirerOldValue.Name = "txtAcquirerOldValue";
            this.txtAcquirerOldValue.Size = new System.Drawing.Size(229, 20);
            this.txtAcquirerOldValue.TabIndex = 3;
            // 
            // txtAcquirerOldTag
            // 
            this.txtAcquirerOldTag.Location = new System.Drawing.Point(19, 43);
            this.txtAcquirerOldTag.Name = "txtAcquirerOldTag";
            this.txtAcquirerOldTag.Size = new System.Drawing.Size(100, 20);
            this.txtAcquirerOldTag.TabIndex = 2;
            // 
            // lblAcquirerOldValue
            // 
            this.lblAcquirerOldValue.AutoSize = true;
            this.lblAcquirerOldValue.Location = new System.Drawing.Point(134, 21);
            this.lblAcquirerOldValue.Name = "lblAcquirerOldValue";
            this.lblAcquirerOldValue.Size = new System.Drawing.Size(34, 13);
            this.lblAcquirerOldValue.TabIndex = 1;
            this.lblAcquirerOldValue.Text = "Value";
            // 
            // lblAcquirerOldTag
            // 
            this.lblAcquirerOldTag.AutoSize = true;
            this.lblAcquirerOldTag.Location = new System.Drawing.Point(16, 21);
            this.lblAcquirerOldTag.Name = "lblAcquirerOldTag";
            this.lblAcquirerOldTag.Size = new System.Drawing.Size(26, 13);
            this.lblAcquirerOldTag.TabIndex = 0;
            this.lblAcquirerOldTag.Text = "Tag";
            // 
            // tabIssuer
            // 
            this.tabIssuer.Controls.Add(this.gbIssuerNew);
            this.tabIssuer.Controls.Add(this.btnIssuerMore);
            this.tabIssuer.Controls.Add(this.gbIssuerOld);
            this.tabIssuer.Location = new System.Drawing.Point(4, 22);
            this.tabIssuer.Name = "tabIssuer";
            this.tabIssuer.Size = new System.Drawing.Size(866, 448);
            this.tabIssuer.TabIndex = 2;
            this.tabIssuer.Text = "Issuer (AE)";
            this.tabIssuer.UseVisualStyleBackColor = true;
            // 
            // gbIssuerNew
            // 
            this.gbIssuerNew.Controls.Add(this.txtIssuerNewValue);
            this.gbIssuerNew.Controls.Add(this.txtIssuerNewTag);
            this.gbIssuerNew.Controls.Add(this.lblIssuerNewValue);
            this.gbIssuerNew.Controls.Add(this.lblIssuerNewTag);
            this.gbIssuerNew.Location = new System.Drawing.Point(474, 5);
            this.gbIssuerNew.Name = "gbIssuerNew";
            this.gbIssuerNew.Size = new System.Drawing.Size(385, 438);
            this.gbIssuerNew.TabIndex = 5;
            this.gbIssuerNew.TabStop = false;
            this.gbIssuerNew.Text = "New";
            // 
            // txtIssuerNewValue
            // 
            this.txtIssuerNewValue.Location = new System.Drawing.Point(138, 44);
            this.txtIssuerNewValue.Name = "txtIssuerNewValue";
            this.txtIssuerNewValue.Size = new System.Drawing.Size(229, 20);
            this.txtIssuerNewValue.TabIndex = 7;
            // 
            // txtIssuerNewTag
            // 
            this.txtIssuerNewTag.Location = new System.Drawing.Point(20, 44);
            this.txtIssuerNewTag.Name = "txtIssuerNewTag";
            this.txtIssuerNewTag.Size = new System.Drawing.Size(100, 20);
            this.txtIssuerNewTag.TabIndex = 6;
            // 
            // lblIssuerNewValue
            // 
            this.lblIssuerNewValue.AutoSize = true;
            this.lblIssuerNewValue.Location = new System.Drawing.Point(135, 22);
            this.lblIssuerNewValue.Name = "lblIssuerNewValue";
            this.lblIssuerNewValue.Size = new System.Drawing.Size(34, 13);
            this.lblIssuerNewValue.TabIndex = 5;
            this.lblIssuerNewValue.Text = "Value";
            // 
            // lblIssuerNewTag
            // 
            this.lblIssuerNewTag.AutoSize = true;
            this.lblIssuerNewTag.Location = new System.Drawing.Point(17, 22);
            this.lblIssuerNewTag.Name = "lblIssuerNewTag";
            this.lblIssuerNewTag.Size = new System.Drawing.Size(26, 13);
            this.lblIssuerNewTag.TabIndex = 4;
            this.lblIssuerNewTag.Text = "Tag";
            // 
            // btnIssuerMore
            // 
            this.btnIssuerMore.Location = new System.Drawing.Point(398, 217);
            this.btnIssuerMore.Name = "btnIssuerMore";
            this.btnIssuerMore.Size = new System.Drawing.Size(70, 23);
            this.btnIssuerMore.TabIndex = 4;
            this.btnIssuerMore.Text = "More...";
            this.btnIssuerMore.UseVisualStyleBackColor = true;
            // 
            // gbIssuerOld
            // 
            this.gbIssuerOld.Controls.Add(this.txtIssuerOldValue);
            this.gbIssuerOld.Controls.Add(this.txtIssuerOldTag);
            this.gbIssuerOld.Controls.Add(this.lblIssuerOldValue);
            this.gbIssuerOld.Controls.Add(this.lblIssuerOldTag);
            this.gbIssuerOld.Location = new System.Drawing.Point(7, 6);
            this.gbIssuerOld.Name = "gbIssuerOld";
            this.gbIssuerOld.Size = new System.Drawing.Size(385, 438);
            this.gbIssuerOld.TabIndex = 3;
            this.gbIssuerOld.TabStop = false;
            this.gbIssuerOld.Text = "Old";
            // 
            // txtIssuerOldValue
            // 
            this.txtIssuerOldValue.Location = new System.Drawing.Point(137, 43);
            this.txtIssuerOldValue.Name = "txtIssuerOldValue";
            this.txtIssuerOldValue.Size = new System.Drawing.Size(229, 20);
            this.txtIssuerOldValue.TabIndex = 3;
            // 
            // txtIssuerOldTag
            // 
            this.txtIssuerOldTag.Location = new System.Drawing.Point(19, 43);
            this.txtIssuerOldTag.Name = "txtIssuerOldTag";
            this.txtIssuerOldTag.Size = new System.Drawing.Size(100, 20);
            this.txtIssuerOldTag.TabIndex = 2;
            // 
            // lblIssuerOldValue
            // 
            this.lblIssuerOldValue.AutoSize = true;
            this.lblIssuerOldValue.Location = new System.Drawing.Point(134, 21);
            this.lblIssuerOldValue.Name = "lblIssuerOldValue";
            this.lblIssuerOldValue.Size = new System.Drawing.Size(34, 13);
            this.lblIssuerOldValue.TabIndex = 1;
            this.lblIssuerOldValue.Text = "Value";
            // 
            // lblIssuerOldTag
            // 
            this.lblIssuerOldTag.AutoSize = true;
            this.lblIssuerOldTag.Location = new System.Drawing.Point(16, 21);
            this.lblIssuerOldTag.Name = "lblIssuerOldTag";
            this.lblIssuerOldTag.Size = new System.Drawing.Size(26, 13);
            this.lblIssuerOldTag.TabIndex = 0;
            this.lblIssuerOldTag.Text = "Tag";
            // 
            // tabCard
            // 
            this.tabCard.Controls.Add(this.groupBox5);
            this.tabCard.Controls.Add(this.button3);
            this.tabCard.Controls.Add(this.groupBox6);
            this.tabCard.Location = new System.Drawing.Point(4, 22);
            this.tabCard.Name = "tabCard";
            this.tabCard.Size = new System.Drawing.Size(866, 448);
            this.tabCard.TabIndex = 3;
            this.tabCard.Text = "Card (AC)";
            this.tabCard.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtCardNewValue);
            this.groupBox5.Controls.Add(this.txtCardNewTag);
            this.groupBox5.Controls.Add(this.lblCardNewValue);
            this.groupBox5.Controls.Add(this.lblCardNewTag);
            this.groupBox5.Location = new System.Drawing.Point(474, 5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(385, 438);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "New";
            // 
            // txtCardNewValue
            // 
            this.txtCardNewValue.Location = new System.Drawing.Point(138, 44);
            this.txtCardNewValue.Name = "txtCardNewValue";
            this.txtCardNewValue.Size = new System.Drawing.Size(229, 20);
            this.txtCardNewValue.TabIndex = 7;
            // 
            // txtCardNewTag
            // 
            this.txtCardNewTag.Location = new System.Drawing.Point(20, 44);
            this.txtCardNewTag.Name = "txtCardNewTag";
            this.txtCardNewTag.Size = new System.Drawing.Size(100, 20);
            this.txtCardNewTag.TabIndex = 6;
            // 
            // lblCardNewValue
            // 
            this.lblCardNewValue.AutoSize = true;
            this.lblCardNewValue.Location = new System.Drawing.Point(135, 22);
            this.lblCardNewValue.Name = "lblCardNewValue";
            this.lblCardNewValue.Size = new System.Drawing.Size(34, 13);
            this.lblCardNewValue.TabIndex = 5;
            this.lblCardNewValue.Text = "Value";
            // 
            // lblCardNewTag
            // 
            this.lblCardNewTag.AutoSize = true;
            this.lblCardNewTag.Location = new System.Drawing.Point(17, 22);
            this.lblCardNewTag.Name = "lblCardNewTag";
            this.lblCardNewTag.Size = new System.Drawing.Size(26, 13);
            this.lblCardNewTag.TabIndex = 4;
            this.lblCardNewTag.Text = "Tag";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(398, 217);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(70, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "More...";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtCardOldValue);
            this.groupBox6.Controls.Add(this.txtCardOldTag);
            this.groupBox6.Controls.Add(this.lblCardOldValue);
            this.groupBox6.Controls.Add(this.lblCardOldTag);
            this.groupBox6.Location = new System.Drawing.Point(7, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(385, 438);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Old";
            // 
            // txtCardOldValue
            // 
            this.txtCardOldValue.Location = new System.Drawing.Point(137, 43);
            this.txtCardOldValue.Name = "txtCardOldValue";
            this.txtCardOldValue.Size = new System.Drawing.Size(229, 20);
            this.txtCardOldValue.TabIndex = 3;
            // 
            // txtCardOldTag
            // 
            this.txtCardOldTag.Location = new System.Drawing.Point(19, 43);
            this.txtCardOldTag.Name = "txtCardOldTag";
            this.txtCardOldTag.Size = new System.Drawing.Size(100, 20);
            this.txtCardOldTag.TabIndex = 2;
            // 
            // lblCardOldValue
            // 
            this.lblCardOldValue.AutoSize = true;
            this.lblCardOldValue.Location = new System.Drawing.Point(134, 21);
            this.lblCardOldValue.Name = "lblCardOldValue";
            this.lblCardOldValue.Size = new System.Drawing.Size(34, 13);
            this.lblCardOldValue.TabIndex = 1;
            this.lblCardOldValue.Text = "Value";
            // 
            // lblCardOldTag
            // 
            this.lblCardOldTag.AutoSize = true;
            this.lblCardOldTag.Location = new System.Drawing.Point(16, 21);
            this.lblCardOldTag.Name = "lblCardOldTag";
            this.lblCardOldTag.Size = new System.Drawing.Size(26, 13);
            this.lblCardOldTag.TabIndex = 0;
            this.lblCardOldTag.Text = "Tag";
            // 
            // FrmUpdateField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 687);
            this.Controls.Add(this.tabFields);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.pbAddFields);
            this.Controls.Add(this.txtKeterangan);
            this.Controls.Add(this.gbSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmUpdateField";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Fields";
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.tabFields.ResumeLayout(false);
            this.tabTerminal.ResumeLayout(false);
            this.gbTerminalNew.ResumeLayout(false);
            this.gbTerminalNew.PerformLayout();
            this.gbTerminalOld.ResumeLayout(false);
            this.gbTerminalOld.PerformLayout();
            this.tabAcquirer.ResumeLayout(false);
            this.gbAcquirerNew.ResumeLayout(false);
            this.gbAcquirerNew.PerformLayout();
            this.gbAcquirerOld.ResumeLayout(false);
            this.gbAcquirerOld.PerformLayout();
            this.tabIssuer.ResumeLayout(false);
            this.gbIssuerNew.ResumeLayout(false);
            this.gbIssuerNew.PerformLayout();
            this.gbIssuerOld.ResumeLayout(false);
            this.gbIssuerOld.PerformLayout();
            this.tabCard.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar pbAddFields;
        private System.Windows.Forms.TextBox txtKeterangan;
        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.CheckBox chkIncludeMs;
        private System.Windows.Forms.ComboBox cmbTerminal;
        private System.Windows.Forms.ComboBox cmbDbSource;
        private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.Label lblDbSource;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnExecuteAll;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.TabControl tabFields;
        private System.Windows.Forms.TabPage tabTerminal;
        private System.Windows.Forms.TabPage tabAcquirer;
        private System.Windows.Forms.TabPage tabIssuer;
        private System.Windows.Forms.TabPage tabCard;
        private System.Windows.Forms.GroupBox gbTerminalOld;
        private System.Windows.Forms.Button btnTerminalMore;
        private System.Windows.Forms.GroupBox gbTerminalNew;
        private System.Windows.Forms.Label lblTerminalOldValue;
        private System.Windows.Forms.Label lblTerminalOldTag;
        private System.Windows.Forms.TextBox txtTerminalOldValue;
        private System.Windows.Forms.TextBox txtTerminalOldTag;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbAcquirerNew;
        private System.Windows.Forms.TextBox txtAcquirerNewValue;
        private System.Windows.Forms.TextBox txtAcquirerNewTag;
        private System.Windows.Forms.Label lblAcquirerNewValue;
        private System.Windows.Forms.Label lblAcquirerNewTag;
        private System.Windows.Forms.Button btnAcquirerMore;
        private System.Windows.Forms.GroupBox gbAcquirerOld;
        private System.Windows.Forms.TextBox txtAcquirerOldValue;
        private System.Windows.Forms.TextBox txtAcquirerOldTag;
        private System.Windows.Forms.Label lblAcquirerOldValue;
        private System.Windows.Forms.Label lblAcquirerOldTag;
        private System.Windows.Forms.GroupBox gbIssuerNew;
        private System.Windows.Forms.TextBox txtIssuerNewValue;
        private System.Windows.Forms.TextBox txtIssuerNewTag;
        private System.Windows.Forms.Label lblIssuerNewValue;
        private System.Windows.Forms.Label lblIssuerNewTag;
        private System.Windows.Forms.Button btnIssuerMore;
        private System.Windows.Forms.GroupBox gbIssuerOld;
        private System.Windows.Forms.TextBox txtIssuerOldValue;
        private System.Windows.Forms.TextBox txtIssuerOldTag;
        private System.Windows.Forms.Label lblIssuerOldValue;
        private System.Windows.Forms.Label lblIssuerOldTag;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtCardNewValue;
        private System.Windows.Forms.TextBox txtCardNewTag;
        private System.Windows.Forms.Label lblCardNewValue;
        private System.Windows.Forms.Label lblCardNewTag;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtCardOldValue;
        private System.Windows.Forms.TextBox txtCardOldTag;
        private System.Windows.Forms.Label lblCardOldValue;
        private System.Windows.Forms.Label lblCardOldTag;
    }
}