using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace InSys
{
    public partial class FrmUpdateField : Form
    {
        protected SqlConnection oSqlConn;
        private bool isTerminalActive, isAcqActive, isIssuerActive, isCardActive;
        public FrmUpdateField(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
            isTerminalActive = true;
            isAcqActive = false;
            isIssuerActive = false;
            isCardActive = false;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            //if (isTerminalActive)
            //    CommonClass.inputLog(oSqlConn, "", "", "", CommonMessage.sUpdateFieldTerminal, "");
            //else if (isAcqActive)
            //    CommonClass.inputLog(oSqlConn, "", "", "", CommonMessage.sUpdateFieldAcq, "");
            //else if (isIssuerActive)
            //    CommonClass.inputLog(oSqlConn, "", "", "", CommonMessage.sUpdateFieldIssuer, "");
            //else if (isCardActive)
            //    CommonClass.inputLog(oSqlConn, "", "", "", CommonMessage.sUpdateFieldCard, "");
        }
    }
}