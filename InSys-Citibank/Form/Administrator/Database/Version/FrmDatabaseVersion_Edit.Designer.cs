﻿namespace InSys
{
    partial class FrmDatabaseVersion_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDatabaseVersion_Edit));
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbDatabase = new System.Windows.Forms.GroupBox();
            this.pnlNewDb = new System.Windows.Forms.Panel();
            this.pnlGPRSInit = new System.Windows.Forms.Panel();
            this.rdGPRSInitNo = new System.Windows.Forms.RadioButton();
            this.rdGPRSInitYes = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlLoyaltyProd = new System.Windows.Forms.Panel();
            this.rdLoyaltyProdNo = new System.Windows.Forms.RadioButton();
            this.rdLoyaltyProdYes = new System.Windows.Forms.RadioButton();
            this.lblLoyaltyProd = new System.Windows.Forms.Label();
            this.pnlTLEInit = new System.Windows.Forms.Panel();
            this.rdTLEInitNo = new System.Windows.Forms.RadioButton();
            this.rdTLEInitYes = new System.Windows.Forms.RadioButton();
            this.lblTLEInit = new System.Windows.Forms.Label();
            this.pnlEMVInit = new System.Windows.Forms.Panel();
            this.rdEMVInitNo = new System.Windows.Forms.RadioButton();
            this.rdEMVInitYes = new System.Windows.Forms.RadioButton();
            this.pnlNewInit = new System.Windows.Forms.Panel();
            this.rdNewInitNo = new System.Windows.Forms.RadioButton();
            this.rdNewInitYes = new System.Windows.Forms.RadioButton();
            this.lblEMVInit = new System.Windows.Forms.Label();
            this.lblNewInit = new System.Windows.Forms.Label();
            this.txtDbName = new System.Windows.Forms.TextBox();
            this.lblDbName = new System.Windows.Forms.Label();
            this.txtDbID = new System.Windows.Forms.TextBox();
            this.lblDbID = new System.Windows.Forms.Label();
            this.gbChooseDatabase = new System.Windows.Forms.GroupBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.cmbDatabaseVersion = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlBankCode = new System.Windows.Forms.Panel();
            this.rdBankCodeNo = new System.Windows.Forms.RadioButton();
            this.rdBankCodeYes = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlProductCode = new System.Windows.Forms.Panel();
            this.rdProductCodeNo = new System.Windows.Forms.RadioButton();
            this.rdProductCodeYes = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.gbButton.SuspendLayout();
            this.gbDatabase.SuspendLayout();
            this.pnlNewDb.SuspendLayout();
            this.pnlGPRSInit.SuspendLayout();
            this.pnlLoyaltyProd.SuspendLayout();
            this.pnlTLEInit.SuspendLayout();
            this.pnlEMVInit.SuspendLayout();
            this.pnlNewInit.SuspendLayout();
            this.gbChooseDatabase.SuspendLayout();
            this.pnlBankCode.SuspendLayout();
            this.pnlProductCode.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Enabled = false;
            this.gbButton.Location = new System.Drawing.Point(12, 371);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(343, 53);
            this.gbButton.TabIndex = 3;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(223, 16);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSave.Location = new System.Drawing.Point(102, 16);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(115, 28);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbDatabase
            // 
            this.gbDatabase.Controls.Add(this.pnlNewDb);
            this.gbDatabase.Controls.Add(this.txtDbName);
            this.gbDatabase.Controls.Add(this.lblDbName);
            this.gbDatabase.Controls.Add(this.txtDbID);
            this.gbDatabase.Controls.Add(this.lblDbID);
            this.gbDatabase.Enabled = false;
            this.gbDatabase.Location = new System.Drawing.Point(12, 61);
            this.gbDatabase.Name = "gbDatabase";
            this.gbDatabase.Size = new System.Drawing.Size(343, 304);
            this.gbDatabase.TabIndex = 2;
            this.gbDatabase.TabStop = false;
            // 
            // pnlNewDb
            // 
            this.pnlNewDb.Controls.Add(this.pnlProductCode);
            this.pnlNewDb.Controls.Add(this.label4);
            this.pnlNewDb.Controls.Add(this.pnlBankCode);
            this.pnlNewDb.Controls.Add(this.label3);
            this.pnlNewDb.Controls.Add(this.pnlGPRSInit);
            this.pnlNewDb.Controls.Add(this.label1);
            this.pnlNewDb.Controls.Add(this.pnlLoyaltyProd);
            this.pnlNewDb.Controls.Add(this.lblLoyaltyProd);
            this.pnlNewDb.Controls.Add(this.pnlTLEInit);
            this.pnlNewDb.Controls.Add(this.lblTLEInit);
            this.pnlNewDb.Controls.Add(this.pnlEMVInit);
            this.pnlNewDb.Controls.Add(this.pnlNewInit);
            this.pnlNewDb.Controls.Add(this.lblEMVInit);
            this.pnlNewDb.Controls.Add(this.lblNewInit);
            this.pnlNewDb.Location = new System.Drawing.Point(22, 65);
            this.pnlNewDb.Name = "pnlNewDb";
            this.pnlNewDb.Size = new System.Drawing.Size(281, 233);
            this.pnlNewDb.TabIndex = 7;
            // 
            // pnlGPRSInit
            // 
            this.pnlGPRSInit.Controls.Add(this.rdGPRSInitNo);
            this.pnlGPRSInit.Controls.Add(this.rdGPRSInitYes);
            this.pnlGPRSInit.Location = new System.Drawing.Point(96, 133);
            this.pnlGPRSInit.Name = "pnlGPRSInit";
            this.pnlGPRSInit.Size = new System.Drawing.Size(182, 25);
            this.pnlGPRSInit.TabIndex = 11;
            // 
            // rdGPRSInitNo
            // 
            this.rdGPRSInitNo.AutoSize = true;
            this.rdGPRSInitNo.Location = new System.Drawing.Point(103, 3);
            this.rdGPRSInitNo.Name = "rdGPRSInitNo";
            this.rdGPRSInitNo.Size = new System.Drawing.Size(39, 17);
            this.rdGPRSInitNo.TabIndex = 1;
            this.rdGPRSInitNo.TabStop = true;
            this.rdGPRSInitNo.Text = "No";
            this.rdGPRSInitNo.UseVisualStyleBackColor = true;
            // 
            // rdGPRSInitYes
            // 
            this.rdGPRSInitYes.AutoSize = true;
            this.rdGPRSInitYes.Location = new System.Drawing.Point(11, 3);
            this.rdGPRSInitYes.Name = "rdGPRSInitYes";
            this.rdGPRSInitYes.Size = new System.Drawing.Size(43, 17);
            this.rdGPRSInitYes.TabIndex = 0;
            this.rdGPRSInitYes.TabStop = true;
            this.rdGPRSInitYes.Text = "Yes";
            this.rdGPRSInitYes.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 138);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "GPRS Init";
            // 
            // pnlLoyaltyProd
            // 
            this.pnlLoyaltyProd.Controls.Add(this.rdLoyaltyProdNo);
            this.pnlLoyaltyProd.Controls.Add(this.rdLoyaltyProdYes);
            this.pnlLoyaltyProd.Location = new System.Drawing.Point(96, 102);
            this.pnlLoyaltyProd.Name = "pnlLoyaltyProd";
            this.pnlLoyaltyProd.Size = new System.Drawing.Size(182, 25);
            this.pnlLoyaltyProd.TabIndex = 9;
            // 
            // rdLoyaltyProdNo
            // 
            this.rdLoyaltyProdNo.AutoSize = true;
            this.rdLoyaltyProdNo.Location = new System.Drawing.Point(103, 3);
            this.rdLoyaltyProdNo.Name = "rdLoyaltyProdNo";
            this.rdLoyaltyProdNo.Size = new System.Drawing.Size(39, 17);
            this.rdLoyaltyProdNo.TabIndex = 1;
            this.rdLoyaltyProdNo.TabStop = true;
            this.rdLoyaltyProdNo.Text = "No";
            this.rdLoyaltyProdNo.UseVisualStyleBackColor = true;
            // 
            // rdLoyaltyProdYes
            // 
            this.rdLoyaltyProdYes.AutoSize = true;
            this.rdLoyaltyProdYes.Location = new System.Drawing.Point(11, 3);
            this.rdLoyaltyProdYes.Name = "rdLoyaltyProdYes";
            this.rdLoyaltyProdYes.Size = new System.Drawing.Size(43, 17);
            this.rdLoyaltyProdYes.TabIndex = 0;
            this.rdLoyaltyProdYes.TabStop = true;
            this.rdLoyaltyProdYes.Text = "Yes";
            this.rdLoyaltyProdYes.UseVisualStyleBackColor = true;
            // 
            // lblLoyaltyProd
            // 
            this.lblLoyaltyProd.AutoSize = true;
            this.lblLoyaltyProd.Location = new System.Drawing.Point(3, 107);
            this.lblLoyaltyProd.Name = "lblLoyaltyProd";
            this.lblLoyaltyProd.Size = new System.Drawing.Size(80, 13);
            this.lblLoyaltyProd.TabIndex = 8;
            this.lblLoyaltyProd.Text = "Loyalty Product";
            // 
            // pnlTLEInit
            // 
            this.pnlTLEInit.Controls.Add(this.rdTLEInitNo);
            this.pnlTLEInit.Controls.Add(this.rdTLEInitYes);
            this.pnlTLEInit.Location = new System.Drawing.Point(96, 71);
            this.pnlTLEInit.Name = "pnlTLEInit";
            this.pnlTLEInit.Size = new System.Drawing.Size(182, 25);
            this.pnlTLEInit.TabIndex = 5;
            // 
            // rdTLEInitNo
            // 
            this.rdTLEInitNo.AutoSize = true;
            this.rdTLEInitNo.Location = new System.Drawing.Point(103, 3);
            this.rdTLEInitNo.Name = "rdTLEInitNo";
            this.rdTLEInitNo.Size = new System.Drawing.Size(39, 17);
            this.rdTLEInitNo.TabIndex = 1;
            this.rdTLEInitNo.TabStop = true;
            this.rdTLEInitNo.Text = "No";
            this.rdTLEInitNo.UseVisualStyleBackColor = true;
            // 
            // rdTLEInitYes
            // 
            this.rdTLEInitYes.AutoSize = true;
            this.rdTLEInitYes.Location = new System.Drawing.Point(11, 3);
            this.rdTLEInitYes.Name = "rdTLEInitYes";
            this.rdTLEInitYes.Size = new System.Drawing.Size(43, 17);
            this.rdTLEInitYes.TabIndex = 0;
            this.rdTLEInitYes.TabStop = true;
            this.rdTLEInitYes.Text = "Yes";
            this.rdTLEInitYes.UseVisualStyleBackColor = true;
            // 
            // lblTLEInit
            // 
            this.lblTLEInit.AutoSize = true;
            this.lblTLEInit.Location = new System.Drawing.Point(3, 76);
            this.lblTLEInit.Name = "lblTLEInit";
            this.lblTLEInit.Size = new System.Drawing.Size(44, 13);
            this.lblTLEInit.TabIndex = 4;
            this.lblTLEInit.Text = "TLE Init";
            // 
            // pnlEMVInit
            // 
            this.pnlEMVInit.Controls.Add(this.rdEMVInitNo);
            this.pnlEMVInit.Controls.Add(this.rdEMVInitYes);
            this.pnlEMVInit.Location = new System.Drawing.Point(96, 40);
            this.pnlEMVInit.Name = "pnlEMVInit";
            this.pnlEMVInit.Size = new System.Drawing.Size(182, 25);
            this.pnlEMVInit.TabIndex = 3;
            // 
            // rdEMVInitNo
            // 
            this.rdEMVInitNo.AutoSize = true;
            this.rdEMVInitNo.Location = new System.Drawing.Point(103, 3);
            this.rdEMVInitNo.Name = "rdEMVInitNo";
            this.rdEMVInitNo.Size = new System.Drawing.Size(39, 17);
            this.rdEMVInitNo.TabIndex = 1;
            this.rdEMVInitNo.TabStop = true;
            this.rdEMVInitNo.Text = "No";
            this.rdEMVInitNo.UseVisualStyleBackColor = true;
            // 
            // rdEMVInitYes
            // 
            this.rdEMVInitYes.AutoSize = true;
            this.rdEMVInitYes.Location = new System.Drawing.Point(11, 3);
            this.rdEMVInitYes.Name = "rdEMVInitYes";
            this.rdEMVInitYes.Size = new System.Drawing.Size(43, 17);
            this.rdEMVInitYes.TabIndex = 0;
            this.rdEMVInitYes.TabStop = true;
            this.rdEMVInitYes.Text = "Yes";
            this.rdEMVInitYes.UseVisualStyleBackColor = true;
            // 
            // pnlNewInit
            // 
            this.pnlNewInit.Controls.Add(this.rdNewInitNo);
            this.pnlNewInit.Controls.Add(this.rdNewInitYes);
            this.pnlNewInit.Location = new System.Drawing.Point(96, 9);
            this.pnlNewInit.Name = "pnlNewInit";
            this.pnlNewInit.Size = new System.Drawing.Size(182, 25);
            this.pnlNewInit.TabIndex = 1;
            // 
            // rdNewInitNo
            // 
            this.rdNewInitNo.AutoSize = true;
            this.rdNewInitNo.Location = new System.Drawing.Point(103, 3);
            this.rdNewInitNo.Name = "rdNewInitNo";
            this.rdNewInitNo.Size = new System.Drawing.Size(39, 17);
            this.rdNewInitNo.TabIndex = 1;
            this.rdNewInitNo.TabStop = true;
            this.rdNewInitNo.Text = "No";
            this.rdNewInitNo.UseVisualStyleBackColor = true;
            // 
            // rdNewInitYes
            // 
            this.rdNewInitYes.AutoSize = true;
            this.rdNewInitYes.Location = new System.Drawing.Point(11, 3);
            this.rdNewInitYes.Name = "rdNewInitYes";
            this.rdNewInitYes.Size = new System.Drawing.Size(43, 17);
            this.rdNewInitYes.TabIndex = 0;
            this.rdNewInitYes.TabStop = true;
            this.rdNewInitYes.Text = "Yes";
            this.rdNewInitYes.UseVisualStyleBackColor = true;
            // 
            // lblEMVInit
            // 
            this.lblEMVInit.AutoSize = true;
            this.lblEMVInit.Location = new System.Drawing.Point(3, 47);
            this.lblEMVInit.Name = "lblEMVInit";
            this.lblEMVInit.Size = new System.Drawing.Size(47, 13);
            this.lblEMVInit.TabIndex = 2;
            this.lblEMVInit.Text = "EMV Init";
            // 
            // lblNewInit
            // 
            this.lblNewInit.AutoSize = true;
            this.lblNewInit.Location = new System.Drawing.Point(3, 14);
            this.lblNewInit.Name = "lblNewInit";
            this.lblNewInit.Size = new System.Drawing.Size(46, 13);
            this.lblNewInit.TabIndex = 0;
            this.lblNewInit.Text = "New Init";
            // 
            // txtDbName
            // 
            this.txtDbName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDbName.Location = new System.Drawing.Point(112, 39);
            this.txtDbName.MaxLength = 15;
            this.txtDbName.Name = "txtDbName";
            this.txtDbName.Size = new System.Drawing.Size(210, 20);
            this.txtDbName.TabIndex = 3;
            // 
            // lblDbName
            // 
            this.lblDbName.AutoSize = true;
            this.lblDbName.Location = new System.Drawing.Point(19, 42);
            this.lblDbName.Name = "lblDbName";
            this.lblDbName.Size = new System.Drawing.Size(73, 13);
            this.lblDbName.TabIndex = 2;
            this.lblDbName.Text = "Version Name";
            // 
            // txtDbID
            // 
            this.txtDbID.Enabled = false;
            this.txtDbID.Location = new System.Drawing.Point(112, 13);
            this.txtDbID.MaxLength = 4;
            this.txtDbID.Name = "txtDbID";
            this.txtDbID.Size = new System.Drawing.Size(100, 20);
            this.txtDbID.TabIndex = 1;
            // 
            // lblDbID
            // 
            this.lblDbID.AutoSize = true;
            this.lblDbID.Location = new System.Drawing.Point(19, 16);
            this.lblDbID.Name = "lblDbID";
            this.lblDbID.Size = new System.Drawing.Size(56, 13);
            this.lblDbID.TabIndex = 0;
            this.lblDbID.Text = "Version ID";
            // 
            // gbChooseDatabase
            // 
            this.gbChooseDatabase.Controls.Add(this.btnEdit);
            this.gbChooseDatabase.Controls.Add(this.cmbDatabaseVersion);
            this.gbChooseDatabase.Controls.Add(this.label2);
            this.gbChooseDatabase.Location = new System.Drawing.Point(12, 12);
            this.gbChooseDatabase.Name = "gbChooseDatabase";
            this.gbChooseDatabase.Size = new System.Drawing.Size(343, 43);
            this.gbChooseDatabase.TabIndex = 4;
            this.gbChooseDatabase.TabStop = false;
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(247, 7);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(91, 28);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // cmbDatabaseVersion
            // 
            this.cmbDatabaseVersion.FormattingEnabled = true;
            this.cmbDatabaseVersion.Location = new System.Drawing.Point(112, 12);
            this.cmbDatabaseVersion.Name = "cmbDatabaseVersion";
            this.cmbDatabaseVersion.Size = new System.Drawing.Size(121, 21);
            this.cmbDatabaseVersion.TabIndex = 1;
            this.cmbDatabaseVersion.SelectedIndexChanged += new System.EventHandler(this.cmbDatabaseVersion_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Choose Database";
            // 
            // pnlBankCode
            // 
            this.pnlBankCode.Controls.Add(this.rdBankCodeNo);
            this.pnlBankCode.Controls.Add(this.rdBankCodeYes);
            this.pnlBankCode.Location = new System.Drawing.Point(96, 164);
            this.pnlBankCode.Name = "pnlBankCode";
            this.pnlBankCode.Size = new System.Drawing.Size(182, 25);
            this.pnlBankCode.TabIndex = 13;
            // 
            // rdBankCodeNo
            // 
            this.rdBankCodeNo.AutoSize = true;
            this.rdBankCodeNo.Location = new System.Drawing.Point(103, 3);
            this.rdBankCodeNo.Name = "rdBankCodeNo";
            this.rdBankCodeNo.Size = new System.Drawing.Size(39, 17);
            this.rdBankCodeNo.TabIndex = 1;
            this.rdBankCodeNo.TabStop = true;
            this.rdBankCodeNo.Text = "No";
            this.rdBankCodeNo.UseVisualStyleBackColor = true;
            // 
            // rdBankCodeYes
            // 
            this.rdBankCodeYes.AutoSize = true;
            this.rdBankCodeYes.Location = new System.Drawing.Point(11, 3);
            this.rdBankCodeYes.Name = "rdBankCodeYes";
            this.rdBankCodeYes.Size = new System.Drawing.Size(43, 17);
            this.rdBankCodeYes.TabIndex = 0;
            this.rdBankCodeYes.TabStop = true;
            this.rdBankCodeYes.Text = "Yes";
            this.rdBankCodeYes.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Bank Code";
            // 
            // pnlProductCode
            // 
            this.pnlProductCode.Controls.Add(this.rdProductCodeNo);
            this.pnlProductCode.Controls.Add(this.rdProductCodeYes);
            this.pnlProductCode.Location = new System.Drawing.Point(96, 195);
            this.pnlProductCode.Name = "pnlProductCode";
            this.pnlProductCode.Size = new System.Drawing.Size(182, 25);
            this.pnlProductCode.TabIndex = 15;
            // 
            // rdProductCodeNo
            // 
            this.rdProductCodeNo.AutoSize = true;
            this.rdProductCodeNo.Location = new System.Drawing.Point(103, 3);
            this.rdProductCodeNo.Name = "rdProductCodeNo";
            this.rdProductCodeNo.Size = new System.Drawing.Size(39, 17);
            this.rdProductCodeNo.TabIndex = 1;
            this.rdProductCodeNo.TabStop = true;
            this.rdProductCodeNo.Text = "No";
            this.rdProductCodeNo.UseVisualStyleBackColor = true;
            // 
            // rdProductCodeYes
            // 
            this.rdProductCodeYes.AutoSize = true;
            this.rdProductCodeYes.Location = new System.Drawing.Point(11, 3);
            this.rdProductCodeYes.Name = "rdProductCodeYes";
            this.rdProductCodeYes.Size = new System.Drawing.Size(43, 17);
            this.rdProductCodeYes.TabIndex = 0;
            this.rdProductCodeYes.TabStop = true;
            this.rdProductCodeYes.Text = "Yes";
            this.rdProductCodeYes.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Product Code";
            // 
            // FrmDatabaseVersion_Edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 436);
            this.Controls.Add(this.gbChooseDatabase);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbDatabase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmDatabaseVersion_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database Version - Edit";
            this.Load += new System.EventHandler(this.FrmDatabaseVersion_Edit_Load);
            this.gbButton.ResumeLayout(false);
            this.gbDatabase.ResumeLayout(false);
            this.gbDatabase.PerformLayout();
            this.pnlNewDb.ResumeLayout(false);
            this.pnlNewDb.PerformLayout();
            this.pnlGPRSInit.ResumeLayout(false);
            this.pnlGPRSInit.PerformLayout();
            this.pnlLoyaltyProd.ResumeLayout(false);
            this.pnlLoyaltyProd.PerformLayout();
            this.pnlTLEInit.ResumeLayout(false);
            this.pnlTLEInit.PerformLayout();
            this.pnlEMVInit.ResumeLayout(false);
            this.pnlEMVInit.PerformLayout();
            this.pnlNewInit.ResumeLayout(false);
            this.pnlNewInit.PerformLayout();
            this.gbChooseDatabase.ResumeLayout(false);
            this.gbChooseDatabase.PerformLayout();
            this.pnlBankCode.ResumeLayout(false);
            this.pnlBankCode.PerformLayout();
            this.pnlProductCode.ResumeLayout(false);
            this.pnlProductCode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbDatabase;
        private System.Windows.Forms.Panel pnlNewDb;
        private System.Windows.Forms.Panel pnlGPRSInit;
        private System.Windows.Forms.RadioButton rdGPRSInitNo;
        private System.Windows.Forms.RadioButton rdGPRSInitYes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlLoyaltyProd;
        private System.Windows.Forms.RadioButton rdLoyaltyProdNo;
        private System.Windows.Forms.RadioButton rdLoyaltyProdYes;
        private System.Windows.Forms.Label lblLoyaltyProd;
        private System.Windows.Forms.Panel pnlTLEInit;
        private System.Windows.Forms.RadioButton rdTLEInitNo;
        private System.Windows.Forms.RadioButton rdTLEInitYes;
        private System.Windows.Forms.Label lblTLEInit;
        private System.Windows.Forms.Panel pnlEMVInit;
        private System.Windows.Forms.RadioButton rdEMVInitNo;
        private System.Windows.Forms.RadioButton rdEMVInitYes;
        private System.Windows.Forms.Panel pnlNewInit;
        private System.Windows.Forms.RadioButton rdNewInitNo;
        private System.Windows.Forms.RadioButton rdNewInitYes;
        private System.Windows.Forms.Label lblEMVInit;
        private System.Windows.Forms.Label lblNewInit;
        private System.Windows.Forms.TextBox txtDbName;
        private System.Windows.Forms.Label lblDbName;
        private System.Windows.Forms.TextBox txtDbID;
        private System.Windows.Forms.Label lblDbID;
        private System.Windows.Forms.GroupBox gbChooseDatabase;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.ComboBox cmbDatabaseVersion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlProductCode;
        private System.Windows.Forms.RadioButton rdProductCodeNo;
        private System.Windows.Forms.RadioButton rdProductCodeYes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlBankCode;
        private System.Windows.Forms.RadioButton rdBankCodeNo;
        private System.Windows.Forms.RadioButton rdBankCodeYes;
        private System.Windows.Forms.Label label3;
    }
}