using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmDatabaseVersion_Delete : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Form to delete selected Version
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        public FrmDatabaseVersion_Delete(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        /// <summary>
        /// Load Form
        /// </summary>
        private void FrmDatabaseVersion_Delete_Load(object sender, EventArgs e)
        {
            InitCmb();
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        /// <summary>
        /// Initiate ComboBox with list of Versions from database
        /// </summary>
        protected void InitCmb()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDatabaseID);
        }

        /// <summary>
        /// Get DatabaseID from selected item in ComboBox
        /// </summary>
        /// <returns>string : DatabaseID</returns>
        protected string sGetDbID()
        {
            return (cmbDatabaseID.SelectedIndex >= 0) ? cmbDatabaseID.SelectedValue.ToString() : "";
        }

        /// <summary>
        /// Run Delete Function
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(sGetDbID()))
                MessageBox.Show(CommonMessage.sDbIdExistingEmpty);
            else
                if (CommonClass.isYesMessage(CommonMessage.sConfirmDeleteDbVersion, CommonMessage.sConfirmationTitle))
                    if (CommonClass.isYesMessage(CommonMessage.sConfirmDeleteDbVersionProfile, CommonMessage.sConfirmationTitle))
                        if (CommonClass.isYesMessage(CommonMessage.sConfirmDeleteDbVersionCardList, CommonMessage.sConfirmationTitle))
                            if (isDeleteData())
                            {
                                DataRowView drDatabaseName = (DataRowView)cmbDatabaseID.SelectedItem;

                                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, drDatabaseName["DatabaseName"].ToString(), 
                                                       "Version Deleted", "");
                                this.Close();
                            }
        }

        /// <summary>
        /// Delete all data from selected Version
        /// Determine whether data deleted successfully or not
        /// </summary>
        /// <returns>boolean : true if successfull</returns>
        protected bool isDeleteData()
        {
            Cursor.Current = Cursors.WaitCursor;
            string sErrMsg = "";
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPVersionDelete, oSqlConn))
            {
                SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
                try
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.CommandTimeout = 0;
                    oSqlCmd.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sGetDbID();
                    oSqlCmd.Transaction = oSqlTrans;
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    oSqlCmd.ExecuteNonQuery();
                    oSqlTrans.Commit();
                }
                catch (Exception ex)
                {
                    sErrMsg = ex.Message;
                    CommonClass.doWriteErrorFile(sErrMsg);
                    oSqlTrans.Rollback();
                }
            }
            Cursor.Current = Cursors.Default;
            return string.IsNullOrEmpty(sErrMsg);
        }
    }
}