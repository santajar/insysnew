﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using InSysClass;


namespace InSys
{
    public partial class FrmExportCard : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        string sTempTLVFormat, sDatabaseName;
        DataTable dtTempcardList = new DataTable();

        /// <summary>
        /// Form to Export Card to txt file
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_oSqlConnAuditTrail">SqlConnection : Connection string to Audit Trail database</param>
        public FrmExportCard(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        /// <summary>
        /// Call Function to load Data
        /// </summary>
        private void FrmExportCard_Load(object sender, EventArgs e)
        {
            InitCmb();
        }

        /// <summary>
        /// Load Data to fill combo box
        /// </summary>
        protected void InitCmb()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseName", "DatabaseName", ref cmbDatabase);
        }

        /// <summary>
        /// get error message
        /// </summary>
        protected bool isShowErrorMessage(string sErrMsg)
        {
            if (!string.IsNullOrEmpty(sErrMsg))
                MessageBox.Show(sErrMsg);
            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Get Database ID Value thxt will be used as Source Version
        /// </summary>
        /// <returns>string : Database ID</returns>
        protected string sGetDbId()
        {
            return (cmbDatabase.SelectedIndex >= 0) ? cmbDatabase.SelectedValue.ToString() : "";
        }

        /// <summary>
        /// Do Export Function
        /// </summary>
        private void btnExport_Click(object sender, EventArgs e)
        {
            InitData();
            if (cmbDatabase.SelectedIndex > -1)
                bwExportProgress.RunWorkerAsync();
        }

        /// <summary>
        /// Load Data
        /// </summary>
        private void InitData()
        {
                sDatabaseName = "";
                sDatabaseName = cmbDatabase.SelectedValue.ToString();
        }

        private void bwExportProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            string sFolderPath = Application.StartupPath + "\\FILE\\";
            if (!Directory.Exists(sFolderPath)) Directory.CreateDirectory(sFolderPath);
            string sFileName = null;
            string sProfileContent = dtGetProfileContent();
            sFileName = sFolderPath + sDatabaseName + "_CardRange.txt";
            File.Delete(sFileName);
            CommonLib.Write2File(sFileName, sProfileContent, false);
            //CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, "", "Export Card to " + sFileName, sProfileContent);
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", "Export Card to " + sFileName, sProfileContent);
            MessageBox.Show("Done");
        }

        /// <summary>
        /// Get Profile Content
        /// </summary>
        /// <returns></returns>
        protected string dtGetProfileContent()
        {
            string sContent = "";
            DataTable dtTemp = new DataTable();
            string sAllCard = "";
            foreach (DataGridViewRow dgvRow in dgvCardList.Rows)
            {
                if (Convert.ToBoolean(dgvRow.Cells[0].Value) == true)
                {
                    sAllCard = sAllCard + "'" + dgvRow.Cells[1].Value + "',";
                }
            }
            sAllCard = sAllCard.Substring(0, (sAllCard.Length) - 1);
            SqlCommand oCmd = new SqlCommand("select CardID, CardTag, CardTagLength, CardTagValue from tbProfileCard where CardID in (select CardID from tbProfileCardList where DatabaseID in (select DatabaseID from tbProfileTerminalDB where DatabaseName = '" + sDatabaseName + "') and CardName in (" + sAllCard + "))order by CardID", oSqlConn);
                      
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            new SqlDataAdapter(oCmd).Fill(dtTemp);
            oCmd.Dispose();
            sContent = GenerateTLVformat(dtTemp);
            return sContent; 
        }

        /// <summary>
        /// Generate String with TLV Format
        /// </summary>
        /// <param name="dtTemp">Data Table Source</param>
        /// <returns>string : TLV format message</returns>
        protected string GenerateTLVformat(DataTable dtTemp)
        {
            DataTable dtTempTLVFormat = new DataTable();
            sTempTLVFormat = "";
            string sTag, sTagValue,sLen, sTemp;
            int iIndex=0;
            foreach (DataRow oRow in dtTemp.Rows)
            {

                sTag = dtTemp.Rows[iIndex]["CardTag"].ToString();
                sTagValue = dtTemp.Rows[iIndex]["CardTagValue"].ToString();
                if (!string.IsNullOrEmpty(sTagValue))
                {
                    sLen = sTagValue.Length.ToString();
                    if (sLen.Length == 1)
                        sLen = "0" + sLen;
                    else
                        sLen = sTagValue.Length.ToString();
                    sTemp = sTag.ToUpper() + sLen + sTagValue;
                }
                else
                {
                    sTemp = sTag.ToUpper() + "00";
                }
                sTempTLVFormat = sTempTLVFormat + sTemp;
                iIndex = iIndex + 1;
            }
            return sTempTLVFormat;
        }

        /// <summary>
        /// Load Data when Database change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDatabase.SelectedIndex > -1)
            {
                InitData();
                if (isDatabaseNameValid())
                {
                    dtTempcardList.Clear();
                    SqlCommand oCmd = new SqlCommand("select CardName from tbProfileCardList where DatabaseID in (select DatabaseID from tbProfileTerminalDB where DatabaseName = '" + sDatabaseName + "' )order by CardID", oSqlConn);

                    if (oSqlConn.State == ConnectionState.Closed)
                        oSqlConn.Open();

                    new SqlDataAdapter(oCmd).Fill(dtTempcardList);
                    dgvCardList.DataSource = dtTempcardList;
                    oCmd.Dispose();
                }
            }
        }

        /// <summary>
        /// Check Database Name
        /// </summary>
        /// <returns>Bool : true or false</returns>
        private bool isDatabaseNameValid()
        {
            DataTable dtDatabaseName = new DataTable();
            SqlCommand oCmd = new SqlCommand("select DatabaseName from tbProfileTerminalDB where DatabaseName = '" + sDatabaseName + "'", oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                        oSqlConn.Open();

            new SqlDataAdapter(oCmd).Fill(dtDatabaseName);
            if (dtDatabaseName.Rows.Count>0)
                return true;
            else return false;
        }

        /// <summary>
        /// Check All card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkAll_CheckedChanged(object sender, EventArgs e)
        {
            if (dgvCardList.ColumnCount > 0)
                foreach (DataGridViewRow dgvRow in dgvCardList.Rows)
                    dgvRow.Cells["ColSelect"].Value = chkAll.Checked;
        }

        /// <summary>
        /// Check List of card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvCardList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvCardList.Focus();
            if (e.RowIndex >= 0)
                if (e.ColumnIndex == dgvCardList.Columns["ColSelect"].Index)
                    dgvCardList["ColSelect", e.RowIndex].Value = !Convert.ToBoolean(dgvCardList["ColSelect", e.RowIndex].Value);
        }

    }
}