﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using InSysClass;

namespace InSys
{
    public partial class FrmTag5 : Form
    {
       
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        private SqlDataReader MyReader = null;
        ArrayList alsItemID = new ArrayList();
        ArrayList alsKodeFinal = new ArrayList();
        ArrayList alsKodeAsli = new ArrayList();
        int iLengthTag;

        public FrmTag5(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }
        
        private void FrmTag5_Load(object sender, EventArgs e)
        {
            //add item into combobox
            try
            {

                SqlCommand mycommand = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
                MyReader = mycommand.ExecuteReader();
                while (MyReader.Read())
                {
                    cmbDbName.Items.Add(MyReader["DatabaseName"].ToString());
                }
                MyReader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
            
        }

        private void cmbDbName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //get ID Database
            
            SqlCommand mycommand2 = new SqlCommand("select DatabaseID from tbProfileTerminalDB Where DatabaseName = '" + cmbDbName.Text.ToString() + "'", oSqlConn);
            MyReader = mycommand2.ExecuteReader();
            while (MyReader.Read())
            {
                lblDBID.Text = MyReader["DatabaseID"].ToString();
            }
            MyReader.Close();

            //view Database
            
            SqlCommand mycommand1 = new SqlCommand("select DatabaseID, Tag from tbItemList Where DatabaseID = '" + lblDBID.Text + "'", oSqlConn);
            MyReader = mycommand1.ExecuteReader();
            DataTable DT1 = new DataTable();
            DT1.Load(MyReader);
            dgvView.DataSource = DT1;
            MyReader.Close();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            //get Tag Data
            if (cmbDbName.Text != "")
            {
                //DataTable dtItemlist = GetItemListValue();


                SqlCommand mycommand3 = new SqlCommand("select * from tbItemList Where DatabaseID = '" + lblDBID.Text + "'", oSqlConn);
                MyReader = mycommand3.ExecuteReader();
                alsItemID.Clear();
                alsKodeAsli.Clear();
                alsKodeFinal.Clear();
                while (MyReader.Read())
                {
                    string skode = MyReader["TAG"].ToString();
                    //iLengthTag = skode.Length;
                    string sKAwal = MyReader["TAG"].ToString().Substring(0, 2);
                    string sKAkhir = MyReader["TAG"].ToString().Substring(2, 2);
                    alsItemID.Add(MyReader["ItemID"].ToString());
                    alsKodeAsli.Add(MyReader["TAG"].ToString());
                    alsKodeFinal.Add(sKAwal + "0" + sKAkhir);

                }
                MyReader.Close();
                // cek lenght tag
                //if (iLengthTag == 4)
                //{
                    //update data tag
                    for (int ii = 0; ii < alsItemID.Count; ii++)
                    {
                        string sItemID = alsItemID[ii] as string;
                        string sKodeAsli = alsKodeAsli[ii] as string;
                        string sKodeFinal = alsKodeFinal[ii] as string;
                        //MessageBox.Show ("item ID = " + sItemID +" Tag Asli = " + sKodeAsli+" Tag Replace = " + sKodeFinal);

                        SqlCommand mycommand4 = new SqlCommand("UPDATE tbItemList SET Tag = '" + sKodeFinal + "' WHERE ItemID = '" + sItemID + "' and Tag = '" + sKodeAsli + "' and DatabaseID = '" + lblDBID.Text + "';", oSqlConn);
                        MyReader = mycommand4.ExecuteReader();
                        MyReader.Close();

                    }

                    //SqlCommand mycommand5 = new SqlCommand("UPDATE tbProfileTerminalDB SET LengthOfTag = '5' WHERE DatabaseID = '" + lblDBID.Text + "';", oSqlConn);
                    //MyReader = mycommand5.ExecuteReader();
                    //MyReader.Close();


                    SqlCommand mycommand1 = new SqlCommand("select DatabaseID, Tag from tbItemList Where DatabaseID = '" + lblDBID.Text + "'", oSqlConn);
                    MyReader = mycommand1.ExecuteReader();
                    DataTable DT1 = new DataTable();
                    DT1.Load(MyReader);
                    dgvView.DataSource = DT1;
                    MyReader.Close();
                    CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, cmbDbName.Text.ToString(), "Generate Tag 5", "Generate tag Item list pada database" + cmbDbName.Text.ToString());
                //}
                //else if (iLengthTag == 5)
                //{
                //    MessageBox.Show("Tag sudah di generate");
                //}
               
            }
            else {
               
            MessageBox.Show("pilih database name terlebih dahulu");
            }

            
                
        }

        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
