using System.Windows.Forms;

namespace InSys
{
    public partial class FrmPromptCombo : Form
    {
        /// <summary>
        /// PromptString Form constructor
        /// </summary>
        /// <param name="sTitle">string : Form Title</param>
        /// <param name="sLabel">string : Form Label</param>
        public FrmPromptCombo(string sTitle, string sLabel)
        {
            InitializeComponent();
            this.Text = sTitle;
            lblPrompt.Text = sLabel;
        }
    }
}