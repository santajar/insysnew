using System.Windows.Forms;

namespace InSys
{
    public partial class FrmPromptString : Form
    {
        /// <summary>
        /// PromptString Form constructor
        /// </summary>
        /// <param name="sTitle">string : Form Title</param>
        /// <param name="sLabel">string : Form Label</param>
        public FrmPromptString(string sTitle, string sLabel)
        {
            InitializeComponent();
            this.Text = sTitle;
            lblPrompt.Text = sLabel;
        }

        private void FrmPromptString_Load(object sender, System.EventArgs e)
        {
          
        }

        private void txtInput_TextChanged(object sender, System.EventArgs e)
        {
            
        }

        private void txtInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txtInput_Enter(object sender, System.EventArgs e)
        {
            
        }
    }
}