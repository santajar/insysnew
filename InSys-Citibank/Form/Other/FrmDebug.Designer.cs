namespace InSys
{
    partial class FrmDebug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDebug));
            this.ColumnItemName = new System.Windows.Forms.ColumnHeader();
            this.lvDB2 = new System.Windows.Forms.ListView();
            this.ColumnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.ColumnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.ColumnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.ColumnValue = new System.Windows.Forms.ColumnHeader();
            this.lvDB1 = new System.Windows.Forms.ListView();
            this.ColumnTag = new System.Windows.Forms.ColumnHeader();
            this.btnLoadDB1 = new System.Windows.Forms.Button();
            this.cmbDb1 = new System.Windows.Forms.ComboBox();
            this.btnLoadDB2 = new System.Windows.Forms.Button();
            this.cmbDb2 = new System.Windows.Forms.ComboBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // ColumnItemName
            // 
            this.ColumnItemName.Text = "ItemName";
            this.ColumnItemName.Width = 200;
            // 
            // lvDB2
            // 
            this.lvDB2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader1,
            this.ColumnHeader2,
            this.ColumnHeader3});
            this.lvDB2.FullRowSelect = true;
            this.lvDB2.Location = new System.Drawing.Point(500, 42);
            this.lvDB2.Name = "lvDB2";
            this.lvDB2.Size = new System.Drawing.Size(472, 288);
            this.lvDB2.TabIndex = 25;
            this.lvDB2.UseCompatibleStateImageBehavior = false;
            this.lvDB2.View = System.Windows.Forms.View.Details;
            // 
            // ColumnHeader1
            // 
            this.ColumnHeader1.Text = "Tag";
            // 
            // ColumnHeader2
            // 
            this.ColumnHeader2.Text = "ItemName";
            this.ColumnHeader2.Width = 200;
            // 
            // ColumnHeader3
            // 
            this.ColumnHeader3.Text = "Default Value";
            this.ColumnHeader3.Width = 200;
            // 
            // ColumnValue
            // 
            this.ColumnValue.Text = "Value";
            this.ColumnValue.Width = 200;
            // 
            // lvDB1
            // 
            this.lvDB1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnTag,
            this.ColumnItemName,
            this.ColumnValue});
            this.lvDB1.FullRowSelect = true;
            this.lvDB1.Location = new System.Drawing.Point(12, 42);
            this.lvDB1.Name = "lvDB1";
            this.lvDB1.Size = new System.Drawing.Size(472, 288);
            this.lvDB1.TabIndex = 24;
            this.lvDB1.UseCompatibleStateImageBehavior = false;
            this.lvDB1.View = System.Windows.Forms.View.Details;
            // 
            // ColumnTag
            // 
            this.ColumnTag.Text = "Tag";
            // 
            // btnLoadDB1
            // 
            this.btnLoadDB1.Location = new System.Drawing.Point(180, 10);
            this.btnLoadDB1.Name = "btnLoadDB1";
            this.btnLoadDB1.Size = new System.Drawing.Size(75, 23);
            this.btnLoadDB1.TabIndex = 1;
            this.btnLoadDB1.Text = "Load Item";
            this.btnLoadDB1.Visible = false;
            // 
            // cmbDb1
            // 
            this.cmbDb1.Location = new System.Drawing.Point(12, 10);
            this.cmbDb1.Name = "cmbDb1";
            this.cmbDb1.Size = new System.Drawing.Size(152, 21);
            this.cmbDb1.TabIndex = 0;
            this.cmbDb1.Visible = false;
            // 
            // btnLoadDB2
            // 
            this.btnLoadDB2.Location = new System.Drawing.Point(668, 10);
            this.btnLoadDB2.Name = "btnLoadDB2";
            this.btnLoadDB2.Size = new System.Drawing.Size(75, 23);
            this.btnLoadDB2.TabIndex = 3;
            this.btnLoadDB2.Text = "Load Item";
            // 
            // cmbDb2
            // 
            this.cmbDb2.Location = new System.Drawing.Point(500, 10);
            this.cmbDb2.Name = "cmbDb2";
            this.cmbDb2.Size = new System.Drawing.Size(152, 21);
            this.cmbDb2.TabIndex = 2;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(15, 344);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(956, 84);
            this.richTextBox1.TabIndex = 26;
            this.richTextBox1.Text = "";
            // 
            // FrmDebug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 451);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.lvDB2);
            this.Controls.Add(this.lvDB1);
            this.Controls.Add(this.btnLoadDB1);
            this.Controls.Add(this.cmbDb1);
            this.Controls.Add(this.btnLoadDB2);
            this.Controls.Add(this.cmbDb2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmDebug";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Debug & Testing";
            this.Load += new System.EventHandler(this.FrmDebug_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ColumnHeader ColumnItemName;
        internal System.Windows.Forms.ListView lvDB2;
        internal System.Windows.Forms.ColumnHeader ColumnHeader1;
        internal System.Windows.Forms.ColumnHeader ColumnHeader2;
        internal System.Windows.Forms.ColumnHeader ColumnHeader3;
        internal System.Windows.Forms.ColumnHeader ColumnValue;
        internal System.Windows.Forms.ListView lvDB1;
        internal System.Windows.Forms.ColumnHeader ColumnTag;
        internal System.Windows.Forms.Button btnLoadDB1;
        internal System.Windows.Forms.ComboBox cmbDb1;
        internal System.Windows.Forms.Button btnLoadDB2;
        internal System.Windows.Forms.ComboBox cmbDb2;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}