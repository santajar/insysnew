﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;
using System.Linq;

namespace InSys
{
    public partial class frmUpdateRD : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        DataTable dtAllList = new DataTable();
        public frmUpdateRD(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        private void frmUpdateRD_Load(object sender, EventArgs e)
        {
            LoadComboBox();
        }

        private void LoadComboBox()
        {
            InitComboBoxCity();
            InitComboBoxGroup();
            InitComboBoxRegion();
            InitComboBoxSoftwareName();
            InitComboBoxDatabaseName();
        }

        private void InitComboBoxRegion()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPBrowseRegion,"");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbRegion.DataSource = dtSoftware;
                cmbRegion.DisplayMember = "Region";
                cmbRegion.ValueMember = "IdRegionRD";
                cmbRegion.SelectedIndex = -1;
            }
        }

        private void InitComboBoxGroup()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPBrowseGroup,"");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbGroup.DataSource = dtSoftware;
                cmbGroup.DisplayMember = "Group";
                cmbGroup.ValueMember = "IdGroupRD";
                cmbGroup.SelectedIndex = -1;
            }
        }

        private void InitComboBoxCity()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPBrowseCity,"");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbCity.DataSource = dtSoftware;
                cmbCity.DisplayMember = "City";
                cmbCity.ValueMember = "IdCityRD";
                cmbCity.SelectedIndex = -1;
            }
        }

        private void InitComboBoxSoftwareName()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPAppPackageBrowse,"");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbSoftwareName.DataSource = dtSoftware;
                cmbSoftwareName.DisplayMember = "Software Name";
                cmbSoftwareName.ValueMember = "AppPackID";
                cmbSoftwareName.SelectedIndex = -1;
            }
        }

        private void InitComboBoxDatabaseName()
        {
            DataTable dtDatabase = dtGetData(CommonSP.sSPTerminalDBBrowse, "Where RemoteDownload = 1");
            if (dtDatabase.Rows.Count > 0)
            {
                cmbDatabaseName.DataSource = dtDatabase;
                cmbDatabaseName.DisplayMember = "DatabaseName";
                cmbDatabaseName.ValueMember = "DatabaseID";
                cmbDatabaseName.SelectedIndex = -1;
            }
        }

        private void InitComboBoxRemoteDownloadName()
        {
            cmbRDid.DataSource = null;
            DataTable dtSoftware = dtGetData(CommonSP.sSPRemoteDownloadBrowse, string.Format("Where DatabaseID = '{0}' AND RemoteDownloadTag in ('RD01','RD001')", cmbDatabaseName.SelectedValue.ToString()));
            if (dtSoftware.Rows.Count > 0)
            {
                cmbRDid.DataSource = dtSoftware;
                cmbRDid.DisplayMember = "RemoteDownloadName";
                cmbRDid.ValueMember = "RemoteDownloadName";
                cmbRDid.SelectedIndex = -1;
            }
        }

        private DataTable dtGetData(string sSP, string sCond)
        {
            SqlCommand oCmd = new SqlCommand(sSP, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCond;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);
            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();
            return dtTemp;
        }

        private void chkEnableRD_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateAllGroupRegionCity();
        }

        private void UpdateAllGroupRegionCity()
        {
            DataTable dtAllList = dtGetData(CommonSP.sSPBrowseAllColoumLinkListTID,"");  
            
            string sConditionRegion, sConditionGroup, sConditionCity,sConditionALL;
            if (cmbRegion.SelectedIndex > -1)
                sConditionRegion = "IdRegionRD = '"+cmbRegion.SelectedValue.ToString()+"'";
            else sConditionRegion = "";

            if (cmbGroup.SelectedIndex > -1)
                sConditionGroup = "IdGroupRD = '"+cmbGroup.SelectedValue.ToString()+"'";
            else sConditionGroup = "";

            if (cmbCity.SelectedIndex > -1)
                sConditionCity = "IdCityRD = '"+cmbCity.SelectedValue.ToString()+"'";
            else sConditionCity = "";

            if (!string.IsNullOrEmpty(sConditionRegion) || sConditionRegion.Length > 0)
            {
                sConditionALL = sConditionRegion;
                if (!string.IsNullOrEmpty(sConditionGroup) || sConditionGroup.Length > 0)
                {
                    sConditionALL = sConditionALL + " AND " + sConditionGroup;
                    if (!string.IsNullOrEmpty(sConditionCity) || sConditionCity.Length > 0)
                    {
                        sConditionALL = sConditionALL + " AND " + sConditionCity;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(sConditionCity) || sConditionCity.Length > 0)
                    {
                        sConditionALL = sConditionALL + " AND " + sConditionCity;
                    }
                }
            }
            else {
                if (!string.IsNullOrEmpty(sConditionGroup) || sConditionGroup.Length > 0)
                {
                    sConditionALL = sConditionGroup;
                    if (!string.IsNullOrEmpty(sConditionCity) || sConditionCity.Length > 0)
                    {
                        sConditionALL = sConditionALL + " AND " + sConditionCity;
                    }
                }
                else {
                    sConditionALL = sConditionCity;
                }
            }

            if (cmbDatabaseName.SelectedIndex > -1)
            {
                if (cmbRDid.SelectedIndex > -1)
                {
                    sConditionALL += string.Format(" AND DatabaseID = {0} ", cmbDatabaseName.SelectedValue.ToString());
                    string sTerminalID = "";
                    dtAllList.Select(sConditionALL)
                        .ToList<DataRow>()
                        .ForEach(AllList =>
                        {
                            sTerminalID = sTerminalID + "'" + AllList["TerminalID"].ToString() + "',";
                        });

                    if (!string.IsNullOrEmpty(sTerminalID))
                    {
                        UpdateData(sTerminalID.Substring(0, sTerminalID.Length - 1));
                    }
                    else
                        MessageBox.Show("No Terminal Profile Updated");
                }
                else
                    MessageBox.Show("Please Choose Remote Download Name.");
            }
            else
                MessageBox.Show("Please Choose Database Name."); 
        }

        private void UpdateData(string sTerminalID)
        {
            int IScheduleID, iCheckedEnableRD;
            if (cmbSoftwareName.SelectedIndex > -1)
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPScheduleSoftwareInsert, oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = "";
                    oCmd.Parameters.Add("@sScheduleType", SqlDbType.VarChar).Value = "";
                    oCmd.Parameters.Add("@sScheduleCheck", SqlDbType.VarChar).Value = "";
                    //oCmd.Parameters.Add("@dtScheduleTime", SqlDbType.DateTime).Value = dtpTime.Value;
                    oCmd.Parameters.Add("@iEnableDownload", SqlDbType.Bit).Value = 0;
                    oCmd.Parameters.Add("@iScheduleID", SqlDbType.Int).Direction = ParameterDirection.Output;

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                    oCmd.ExecuteNonQuery();
                    IScheduleID = Convert.ToInt32(oCmd.Parameters["@iScheduleID"].Value.ToString());
                }

                SqlCommand oCmdUpdate = new SqlCommand(string.Format("UPDATE tbListTIDRemoteDownload SET ScheduleID = '{0}' ,AppPackID = '{1}' WHERE TerminalID IN ({2})", IScheduleID, cmbSoftwareName.SelectedValue, sTerminalID),oSqlConn);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oCmdUpdate.ExecuteNonQuery();

                if (chkEnableRD.Checked == true)
                    iCheckedEnableRD = 1;
                else
                    iCheckedEnableRD = 0;

                //SqlCommand oCmdAllow = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET RemoteDownload = {0}, AllowDownload = 1 WHERE TerminalID IN ({1})", iCheckedEnableRD, sTerminalID), oSqlConn);
                SqlCommand oCmdAllow = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET RemoteDownload = {0}, AllowDownload = 0 WHERE TerminalID IN ({1})", iCheckedEnableRD, sTerminalID), oSqlConn);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oCmdAllow.ExecuteNonQuery();

                SqlCommand oCmdUpdateRemoteDonwloadNameinTerminal = new SqlCommand(string.Format("UPDATE tbProfileTerminal SET TerminalTagValue = '{0}', TerminalTagLength='{1}' WHERE TerminalID IN ({2}) AND TerminalTag in ('DE040','DE40')", cmbRDid.SelectedValue.ToString(), cmbRDid.SelectedValue.ToString().Length, sTerminalID), oSqlConn);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oCmdUpdateRemoteDonwloadNameinTerminal.ExecuteNonQuery();
                CommonClass.InputLog(oSqlConn, "", UserData.sUserID, cmbDatabaseName.SelectedText.ToString(), "Update Group, Region, City ", sTerminalID + " - Software : " + cmbSoftwareName.SelectedText.ToString() + " - Remote Download Name :" + cmbRDid.SelectedValue.ToString());
                MessageBox.Show("Update Success!");
                ClearAll();
            }
            else MessageBox.Show("Please Choose Software.");

        }

        private void ClearAll()
        {
            chkEnableRD.Checked = false;
            cmbSoftwareName.SelectedIndex = -1;
            string sDateTime = "00:00:00.000";
            //dtpTime.Value = Convert.ToDateTime(sDateTime);
            InitComboBoxRegion();
            InitComboBoxGroup();
            InitComboBoxCity();
            InitComboBoxDatabaseName();
            cmbRDid.SelectedIndex = -1;
            cmbRDid.DataSource = null;

        }

        private void cmbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblGroup_Click(object sender, EventArgs e)
        {

        }

        private void lblRegion_Click(object sender, EventArgs e)
        {

        }

        private void cmbCity_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblCity_Click(object sender, EventArgs e)
        {

        }

        private void cmbDatabaseName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDatabaseName.SelectedIndex > -1)
            {
                cmbRDid.Enabled = true;
                InitComboBoxRemoteDownloadName();
            }
        }
    }
}
