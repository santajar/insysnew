﻿namespace InSys
{
    partial class frmRemoteDownload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemoteDownload));
            this.dgvGrupRegionCity = new System.Windows.Forms.DataGridView();
            this.cmsGrupRegionCity = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTerminalIDToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblCategory = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.dgvTerminal = new System.Windows.Forms.DataGridView();
            this.cmsTerminal = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addTerminalIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteTerminalIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tcInfoTID = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtBuildNumber = new System.Windows.Forms.ComboBox();
            this.lblBuildNumber = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbSoftwareName = new System.Windows.Forms.ComboBox();
            this.cmbTID = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.dtpDownloadTime = new System.Windows.Forms.DateTimePicker();
            this.lblInfoSoftware = new System.Windows.Forms.Label();
            this.lblInfoTime = new System.Windows.Forms.Label();
            this.lblInfoTID = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupRegionCity)).BeginInit();
            this.cmsGrupRegionCity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).BeginInit();
            this.cmsTerminal.SuspendLayout();
            this.tcInfoTID.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvGrupRegionCity
            // 
            this.dgvGrupRegionCity.AllowUserToAddRows = false;
            this.dgvGrupRegionCity.AllowUserToDeleteRows = false;
            this.dgvGrupRegionCity.AllowUserToResizeColumns = false;
            this.dgvGrupRegionCity.AllowUserToResizeRows = false;
            this.dgvGrupRegionCity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvGrupRegionCity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvGrupRegionCity.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvGrupRegionCity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvGrupRegionCity.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvGrupRegionCity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGrupRegionCity.ContextMenuStrip = this.cmsGrupRegionCity;
            this.dgvGrupRegionCity.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvGrupRegionCity.Enabled = false;
            this.dgvGrupRegionCity.EnableHeadersVisualStyles = false;
            this.dgvGrupRegionCity.Location = new System.Drawing.Point(12, 42);
            this.dgvGrupRegionCity.MultiSelect = false;
            this.dgvGrupRegionCity.Name = "dgvGrupRegionCity";
            this.dgvGrupRegionCity.ReadOnly = true;
            this.dgvGrupRegionCity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvGrupRegionCity.RowHeadersVisible = false;
            this.dgvGrupRegionCity.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvGrupRegionCity.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGrupRegionCity.Size = new System.Drawing.Size(177, 292);
            this.dgvGrupRegionCity.TabIndex = 399;
            this.dgvGrupRegionCity.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvGrupRegionCity_MouseClick);
            // 
            // cmsGrupRegionCity
            // 
            this.cmsGrupRegionCity.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.renameToolStripMenuItem,
            this.addTerminalIDToolStripMenuItem1});
            this.cmsGrupRegionCity.Name = "contextMenuStrip1";
            this.cmsGrupRegionCity.Size = new System.Drawing.Size(158, 92);
            this.cmsGrupRegionCity.Opening += new System.ComponentModel.CancelEventHandler(this.cmsGrupRegionCity_Opening);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.renameToolStripMenuItem.Text = "Rename";
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
            // 
            // addTerminalIDToolStripMenuItem1
            // 
            this.addTerminalIDToolStripMenuItem1.Name = "addTerminalIDToolStripMenuItem1";
            this.addTerminalIDToolStripMenuItem1.Size = new System.Drawing.Size(157, 22);
            this.addTerminalIDToolStripMenuItem1.Text = "Add TerminalID";
            this.addTerminalIDToolStripMenuItem1.Click += new System.EventHandler(this.addTerminalIDToolStripMenuItem1_Click);
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(13, 13);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(49, 13);
            this.lblCategory.TabIndex = 401;
            this.lblCategory.Text = "Category";
            // 
            // cmbCategory
            // 
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Items.AddRange(new object[] {
            "City",
            "Group",
            "Region"});
            this.cmbCategory.Location = new System.Drawing.Point(68, 10);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(121, 21);
            this.cmbCategory.TabIndex = 402;
            this.cmbCategory.SelectedIndexChanged += new System.EventHandler(this.cmbCategory_SelectedIndexChanged);
            // 
            // dgvTerminal
            // 
            this.dgvTerminal.AllowUserToAddRows = false;
            this.dgvTerminal.AllowUserToDeleteRows = false;
            this.dgvTerminal.AllowUserToResizeColumns = false;
            this.dgvTerminal.AllowUserToResizeRows = false;
            this.dgvTerminal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvTerminal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTerminal.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvTerminal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTerminal.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTerminal.ContextMenuStrip = this.cmsTerminal;
            this.dgvTerminal.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvTerminal.Enabled = false;
            this.dgvTerminal.EnableHeadersVisualStyles = false;
            this.dgvTerminal.Location = new System.Drawing.Point(195, 42);
            this.dgvTerminal.MultiSelect = false;
            this.dgvTerminal.Name = "dgvTerminal";
            this.dgvTerminal.ReadOnly = true;
            this.dgvTerminal.RowHeadersVisible = false;
            this.dgvTerminal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvTerminal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTerminal.Size = new System.Drawing.Size(177, 292);
            this.dgvTerminal.TabIndex = 404;
            this.dgvTerminal.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTerminal_CellContentClick_1);
            this.dgvTerminal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvTerminal_KeyDown);
            this.dgvTerminal.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvTerminal_MouseClick);
            // 
            // cmsTerminal
            // 
            this.cmsTerminal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTerminalIDToolStripMenuItem,
            this.deleteTerminalIDToolStripMenuItem,
            this.editToolStripMenuItem});
            this.cmsTerminal.Name = "cmsTerminal";
            this.cmsTerminal.Size = new System.Drawing.Size(169, 70);
            this.cmsTerminal.Opening += new System.ComponentModel.CancelEventHandler(this.cmsTerminal_Opening);
            // 
            // addTerminalIDToolStripMenuItem
            // 
            this.addTerminalIDToolStripMenuItem.Name = "addTerminalIDToolStripMenuItem";
            this.addTerminalIDToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.addTerminalIDToolStripMenuItem.Text = "Add TerminalID";
            this.addTerminalIDToolStripMenuItem.Click += new System.EventHandler(this.addTerminalIDToolStripMenuItem_Click);
            // 
            // deleteTerminalIDToolStripMenuItem
            // 
            this.deleteTerminalIDToolStripMenuItem.Name = "deleteTerminalIDToolStripMenuItem";
            this.deleteTerminalIDToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.deleteTerminalIDToolStripMenuItem.Text = "Delete TerminalID";
            this.deleteTerminalIDToolStripMenuItem.Click += new System.EventHandler(this.deleteTerminalIDToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.editToolStripMenuItem.Text = "EditTerminalID";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // tcInfoTID
            // 
            this.tcInfoTID.Controls.Add(this.tabPage1);
            this.tcInfoTID.Location = new System.Drawing.Point(12, 346);
            this.tcInfoTID.Name = "tcInfoTID";
            this.tcInfoTID.SelectedIndex = 0;
            this.tcInfoTID.Size = new System.Drawing.Size(366, 131);
            this.tcInfoTID.TabIndex = 406;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtBuildNumber);
            this.tabPage1.Controls.Add(this.lblBuildNumber);
            this.tabPage1.Controls.Add(this.btnCancel);
            this.tabPage1.Controls.Add(this.cmbSoftwareName);
            this.tabPage1.Controls.Add(this.cmbTID);
            this.tabPage1.Controls.Add(this.btnSave);
            this.tabPage1.Controls.Add(this.dtpDownloadTime);
            this.tabPage1.Controls.Add(this.lblInfoSoftware);
            this.tabPage1.Controls.Add(this.lblInfoTime);
            this.tabPage1.Controls.Add(this.lblInfoTID);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(358, 105);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Info Remote Download";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtBuildNumber
            // 
            this.txtBuildNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtBuildNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.txtBuildNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.txtBuildNumber.Enabled = false;
            this.txtBuildNumber.FormattingEnabled = true;
            this.txtBuildNumber.Location = new System.Drawing.Point(117, 34);
            this.txtBuildNumber.Name = "txtBuildNumber";
            this.txtBuildNumber.Size = new System.Drawing.Size(55, 21);
            this.txtBuildNumber.TabIndex = 11;
            // 
            // lblBuildNumber
            // 
            this.lblBuildNumber.AutoSize = true;
            this.lblBuildNumber.Location = new System.Drawing.Point(16, 37);
            this.lblBuildNumber.Name = "lblBuildNumber";
            this.lblBuildNumber.Size = new System.Drawing.Size(67, 13);
            this.lblBuildNumber.TabIndex = 10;
            this.lblBuildNumber.Text = "BuildNumber";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(277, 65);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmbSoftwareName
            // 
            this.cmbSoftwareName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbSoftwareName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSoftwareName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSoftwareName.Enabled = false;
            this.cmbSoftwareName.FormattingEnabled = true;
            this.cmbSoftwareName.Location = new System.Drawing.Point(118, 63);
            this.cmbSoftwareName.Name = "cmbSoftwareName";
            this.cmbSoftwareName.Size = new System.Drawing.Size(132, 21);
            this.cmbSoftwareName.TabIndex = 8;
            this.cmbSoftwareName.SelectedIndexChanged += new System.EventHandler(this.cmbSoftwareName_SelectedIndexChanged);
            // 
            // cmbTID
            // 
            this.cmbTID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbTID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbTID.Enabled = false;
            this.cmbTID.FormattingEnabled = true;
            this.cmbTID.Location = new System.Drawing.Point(118, 6);
            this.cmbTID.MaxLength = 8;
            this.cmbTID.Name = "cmbTID";
            this.cmbTID.Size = new System.Drawing.Size(132, 21);
            this.cmbTID.TabIndex = 7;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(277, 39);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dtpDownloadTime
            // 
            this.dtpDownloadTime.AllowDrop = true;
            this.dtpDownloadTime.CustomFormat = "HH:mm ";
            this.dtpDownloadTime.Enabled = false;
            this.dtpDownloadTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDownloadTime.Location = new System.Drawing.Point(118, 82);
            this.dtpDownloadTime.Name = "dtpDownloadTime";
            this.dtpDownloadTime.ShowUpDown = true;
            this.dtpDownloadTime.Size = new System.Drawing.Size(88, 20);
            this.dtpDownloadTime.TabIndex = 3;
            this.dtpDownloadTime.Value = new System.DateTime(2014, 4, 22, 0, 0, 0, 0);
            this.dtpDownloadTime.Visible = false;
            // 
            // lblInfoSoftware
            // 
            this.lblInfoSoftware.AutoSize = true;
            this.lblInfoSoftware.Location = new System.Drawing.Point(17, 66);
            this.lblInfoSoftware.Name = "lblInfoSoftware";
            this.lblInfoSoftware.Size = new System.Drawing.Size(80, 13);
            this.lblInfoSoftware.TabIndex = 2;
            this.lblInfoSoftware.Text = "Software Name";
            // 
            // lblInfoTime
            // 
            this.lblInfoTime.AutoSize = true;
            this.lblInfoTime.Enabled = false;
            this.lblInfoTime.Location = new System.Drawing.Point(16, 84);
            this.lblInfoTime.Name = "lblInfoTime";
            this.lblInfoTime.Size = new System.Drawing.Size(81, 13);
            this.lblInfoTime.TabIndex = 1;
            this.lblInfoTime.Text = "Download Time";
            this.lblInfoTime.Visible = false;
            // 
            // lblInfoTID
            // 
            this.lblInfoTID.AutoSize = true;
            this.lblInfoTID.Location = new System.Drawing.Point(16, 10);
            this.lblInfoTID.Name = "lblInfoTID";
            this.lblInfoTID.Size = new System.Drawing.Size(58, 13);
            this.lblInfoTID.TabIndex = 0;
            this.lblInfoTID.Text = "TerminalID";
            // 
            // frmRemoteDownload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 491);
            this.Controls.Add(this.tcInfoTID);
            this.Controls.Add(this.dgvTerminal);
            this.Controls.Add(this.cmbCategory);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.dgvGrupRegionCity);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRemoteDownload";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remote Download";
            this.Load += new System.EventHandler(this.frmRemoteDownload_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvGrupRegionCity)).EndInit();
            this.cmsGrupRegionCity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).EndInit();
            this.cmsTerminal.ResumeLayout(false);
            this.tcInfoTID.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvGrupRegionCity;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.ContextMenuStrip cmsGrupRegionCity;
        private System.Windows.Forms.DataGridView dgvTerminal;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsTerminal;
        private System.Windows.Forms.ToolStripMenuItem addTerminalIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteTerminalIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.TabControl tcInfoTID;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lblInfoSoftware;
        private System.Windows.Forms.Label lblInfoTID;
        private System.Windows.Forms.ComboBox cmbSoftwareName;
        private System.Windows.Forms.ComboBox cmbTID;
        private System.Windows.Forms.ToolStripMenuItem addTerminalIDToolStripMenuItem1;
        private System.Windows.Forms.ComboBox txtBuildNumber;
        private System.Windows.Forms.Label lblBuildNumber;
        private System.Windows.Forms.DateTimePicker dtpDownloadTime;
        private System.Windows.Forms.Label lblInfoTime;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}