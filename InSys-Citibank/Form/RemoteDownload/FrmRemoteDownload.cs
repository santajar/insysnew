﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class frmRemoteDownload : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        DataTable dtListGroupRegionCity = new DataTable();
        DataTable dtListTID = new DataTable();
        DataTable dtInfoTIDSeclected = new DataTable();
        DataTable dtAllDataTID = new DataTable();
        bool isEdit=false;
        public frmRemoteDownload(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        private void frmRemoteDownload_Load(object sender, EventArgs e)
        {
            dtAllDataTID = new DataTable();
            dtAllDataTID = GetAllTID();
            initcomboboxTerminalList();
            initcomboboxSoftware();
            DisplayFunction();
        }

        private void DisplayFunction()
        {
            addToolStripMenuItem.Visible = addTerminalIDToolStripMenuItem1.Visible = addTerminalIDToolStripMenuItem.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Add);
            renameToolStripMenuItem.Visible = editToolStripMenuItem.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Edit);
            deleteToolStripMenuItem.Visible = deleteTerminalIDToolStripMenuItem.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Delete);
        }

        private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetRemoteDownload();
            dgvGrupRegionCity.Enabled = true;
            dgvTerminal.Enabled = true;
            //iCountCategory_Click = 0;
        }

        private void dgvGrupRegionCity_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && dgvGrupRegionCity.SelectedCells.Count == 1)
            {
            }
        }

        //private void dgvGrupRegionCity_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    LoadListTid();
        //    if (dgvTerminal.Rows.Count > 0)
        //        FillInfoRemoteDownload();
        //}

        private void LoadListTid()
        {
            if (!string.IsNullOrEmpty((string)cmbCategory.SelectedItem))
            {
                dgvTerminal.DataSource = null;
                string sSPChoosing;
                if ((string)cmbCategory.SelectedItem == "Group")
                { sSPChoosing = CommonSP.sSPListTerminalIDbyGroup; }
                else if ((string)cmbCategory.SelectedItem == "Region")
                { sSPChoosing = CommonSP.sSPListTerminalIDbyRegion; }
                else { sSPChoosing = CommonSP.sSPListTerminalIDbyCity; }

                using (SqlCommand oCmd = new SqlCommand(sSPChoosing, oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    if ((string)cmbCategory.SelectedItem != null)
                        oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = GetRegionGroupCityCondition((string)cmbCategory.SelectedItem);

                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn.Close();
                        oSqlConn.Open();
                    }
                    dtListTID = new DataTable();

                    new SqlDataAdapter(oCmd).Fill(dtListTID);
                    dgvTerminal.DataSource = dtListTID;
                    dgvTerminal.Columns[string.Format("{0}RDName", (string)cmbCategory.SelectedItem)].Visible = false;
                    dgvTerminal.Columns["RemoteDownload"].Visible = false;
                    oCmd.Dispose();
                }
                ClearInfoTID();
            }
        }

        private void dgvTerminal_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            ClearInfoTID();
            FillInfoRemoteDownload();
        }

        private void addTerminalIDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearInfoTID();
            LockInfoFunction();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            RegisTerTerminalID();
        }

        private void RegisTerTerminalID()
        {
            if (cmbTID.SelectedIndex != -1)
            {
                if (cmbSoftwareName.SelectedIndex != -1)
                {
                    dtAllDataTID = GetAllTID();
                    int iCountTidGroupRegionCity, iCountTid;
                    if ((string)cmbCategory.SelectedItem=="Group")
                    {
                        iCountTidGroupRegionCity = dtAllDataTID.Select(string.Format("IdGroupRD = '0' AND TerminalID = '{0}'", cmbTID.SelectedValue)).Length;
                    }
                    else if ((string)cmbCategory.SelectedItem == "Region")
                    {
                        iCountTidGroupRegionCity = dtAllDataTID.Select(string.Format("IdRegionRD = '0' AND TerminalID = '{0}'", cmbTID.SelectedValue)).Length;
                    }
                    else
                    {
                        iCountTidGroupRegionCity = dtAllDataTID.Select(string.Format("IdCityRD = '0' AND TerminalID = '{0}'", cmbTID.SelectedValue)).Length;
                    }
                    iCountTid = dtAllDataTID.Select(string.Format("TerminalID = '{0}'", cmbTID.SelectedValue)).Length;
                    //iCountTid = dtAllDataTID.Select(string.Format("Id{0}RD = '0' AND TerminalID = '{1}'", (string)cmbCategory.SelectedItem, cmbTID.SelectedValue)).Length;

                    if (isEdit == true)
                    {
                        InsertUpdateTerminalID(isEdit);
                        isEdit = false;
                        
                    }
                    else
                    {
                        if (iCountTidGroupRegionCity > 0 || iCountTid == 0)
                        {
                            InsertUpdateTerminalID(isEdit);
                        }
                        else
                            MessageBox.Show(string.Format("{0} Already Exist", cmbTID.SelectedValue));
                    }

                }
                else MessageBox.Show("Please Choose Softwarename");
            }
            else MessageBox.Show("Please Choose TerminalID");
        }

        private void InsertUpdateTerminalID(bool _IsEdit)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPRDInsertUpdateListTid, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = cmbTID.SelectedValue;
                oCmd.Parameters.Add("@sCategory", SqlDbType.VarChar).Value = (string)cmbCategory.SelectedItem;
                oCmd.Parameters.Add("@sCategoryName", SqlDbType.VarChar).Value = sGetGroupRegionCity();
                oCmd.Parameters.Add("@dtScheduleTime", SqlDbType.DateTime).Value = dtpDownloadTime.Value;
                oCmd.Parameters.Add("@sSoftwareName", SqlDbType.VarChar).Value = cmbSoftwareName.Text;
                oCmd.Parameters.Add("@BuildNumber", SqlDbType.Int).Value = Convert.ToInt32(cmbSoftwareName.SelectedValue);

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                oCmd.ExecuteNonQuery();
                CommonClass.InputLog(oSqlConn, cmbTID.SelectedValue.ToString(), UserData.sUserID, "", "Insert " + cmbTID.SelectedValue.ToString() + " in Group,Region,City ", "SET " + (string)cmbCategory.SelectedItem + " : " + sGetGroupRegionCity() + " - Software : " + cmbSoftwareName.Text + " - Build Number :" + cmbSoftwareName.SelectedValue.ToString());
                if (_IsEdit == true)
                    MessageBox.Show(string.Format("{0} has been updated",cmbTID.SelectedValue));
                else
                    MessageBox.Show(string.Format("{0} has been added", cmbTID.SelectedValue));
            }

            SqlCommand oCmdAllow = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET RemoteDownload = 1 WHERE TerminalID IN ('{0}')", cmbTID.SelectedValue), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oCmdAllow.ExecuteNonQuery();

            UnLockInfoFunction();
            ResetRemoteDownload();
        }
       
        /// <summary>
        /// Get All TID that registered for Remote Download
        /// </summary>

        private DataTable GetAllTID()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPBrowseAllColoumLinkListTID, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            DataTable dtTemp = new DataTable();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            return dtTemp;
        }
        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearInfoTID();
            UnLockInfoFunction();
        }
        
        

        #region "Main Function"
        private void LoadGroupRegionCity(string sCategory)
        {
            if (!string.IsNullOrEmpty(sCategory))
            {
                string sSPChoosing;
                if (sCategory == "Group")
                { sSPChoosing = CommonSP.sSPBrowseGroup; }
                else if (sCategory == "Region")
                { sSPChoosing = CommonSP.sSPBrowseRegion; }
                else { sSPChoosing = CommonSP.sSPBrowseCity; }
                try
                {
                    SqlCommand oSqlCmd = new SqlCommand(sSPChoosing, oSqlConn);
                    oSqlCmd.CommandType = CommandType.StoredProcedure;

                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn.Close();
                        oSqlConn.Open();
                    }
                    dtListGroupRegionCity = new DataTable();

                    new SqlDataAdapter(oSqlCmd).Fill(dtListGroupRegionCity);

                    dgvGrupRegionCity.DataSource = dtListGroupRegionCity;
                    dgvGrupRegionCity.Columns[string.Format("Id{0}RD", (string)cmbCategory.SelectedItem)].Visible = false;
                    if (dgvGrupRegionCity.Rows.Count > 0)
                        dgvGrupRegionCity.Rows[0].Selected = true;
                    oSqlCmd.Dispose();
                }
                catch (Exception ex)
                {
                    CommonClass.doWriteErrorFile(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please Choose Category");
            }
            
        }

        private void FillInfoRemoteDownload()
        {
            cmbTID.SelectedValue = GetTerminalID();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPGetInfoRD, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = GetTerminalID();

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                dtInfoTIDSeclected = new DataTable();

                new SqlDataAdapter(oCmd).Fill(dtInfoTIDSeclected);
                string sAppPackId = dtInfoTIDSeclected.Rows[0]["AppPackageName"].ToString();
                txtBuildNumber.Text = dtInfoTIDSeclected.Rows[0]["BuildNumber"].ToString();
                cmbSoftwareName.Text = sAppPackId;
                string sDateTime = dtInfoTIDSeclected.Rows[0]["ScheduleTime"].ToString();
                if (string.IsNullOrEmpty(sDateTime))
                    sDateTime = "00:00:00.000";
                dtpDownloadTime.Value = Convert.ToDateTime(sDateTime);
            }
        }

        private void AddListTID()
        {
            ClearInfoTID();
            LockInfoFunction();
        }

        private void EditListTID()
        {
            if (dgvTerminal.SelectedCells.Count > 0)
            {
                LockInfoFunction();
                cmbTID.Enabled = false;
                isEdit = true;
            }
            else MessageBox.Show("Please Select TerminalID First.");
        }

        private void RemoveListTID()
        {
            if (dgvTerminal.SelectedCells.Count > 0)
            {
                if (MessageBox.Show(string.Format("Are you sure want to Delete {0} ?", GetTerminalID()), "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string sTerminalID = GetTerminalID();
                    if (!string.IsNullOrEmpty(sTerminalID) || sTerminalID.Length != 0)
                    {
                        using (SqlCommand ocmd = new SqlCommand(CommonSP.sSPRemoveTIDGroupRegionCity, oSqlConn))
                        {
                            ocmd.CommandType = CommandType.StoredProcedure;
                            ocmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                            ocmd.Parameters.Add("@sCategory", SqlDbType.VarChar).Value = (string)cmbCategory.SelectedItem;

                            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                            ocmd.ExecuteNonQuery();
                            CommonClass.InputLog(oSqlConn, cmbTID.SelectedValue.ToString(), UserData.sUserID, "", "Delete " + sTerminalID + " in " +(string)cmbCategory.SelectedItem, "");
                            MessageBox.Show(string.Format("Delete {0} success", sTerminalID));
                        }
                    }
                    else
                        MessageBox.Show("Please Select TerminalID First");

                    SqlCommand oCmdAllow = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET RemoteDownload = 0 WHERE TerminalID IN ('{0}')", cmbTID.SelectedValue), oSqlConn);
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    oCmdAllow.ExecuteNonQuery();

                    ResetRemoteDownload();
                }
            }
            else MessageBox.Show("Please Select TerminalID First.");
        }

        #endregion
        #region "Function"
        private void ResetRemoteDownload()
        {
            LoadGroupRegionCity((string)cmbCategory.SelectedItem);
            dgvTerminal.DataSource = null;
            ClearInfoTID();
            LoadListTid();
            //if (dgvGrupRegionCity.SelectedCells == null)
            //    LoadListTid();
            if (dgvTerminal.Rows.Count > 0)
                FillInfoRemoteDownload();
            
        }

        private void UnLockInfoFunction()
        {
            cmbCategory.Enabled = true;
            dgvGrupRegionCity.Enabled = true;
            dgvTerminal.Enabled = true;
            dtpDownloadTime.Enabled = false;
            btnSave.Visible = false;
            btnCancel.Visible = false;
            cmbTID.Enabled = false;
            cmbSoftwareName.Enabled = false;
        }

        private void LockInfoFunction()
        {
            cmbCategory.Enabled = false;
            dgvGrupRegionCity.Enabled = false;
            dgvTerminal.Enabled = false;
            dtpDownloadTime.Enabled = true;
            btnSave.Visible = true;
            btnCancel.Visible = true;
            cmbTID.Enabled = true;
            cmbSoftwareName.Enabled = true;
        }

        private void ClearInfoTID()
        {
            string sDateTime = "00:00:00.000";
            dtpDownloadTime.Value = Convert.ToDateTime(sDateTime);
            cmbTID.SelectedIndex = -1;
            cmbSoftwareName.SelectedIndex = -1;
            txtBuildNumber.Text = "";
        }

        private string GetTerminalID()
        {
            return dtListTID.Rows[iGetTIDSelectectedIndex()]["TerminalID"].ToString();
        }

        private int iGetTIDSelectectedIndex()
        {
            //try
            //{
            if (dgvTerminal.CurrentRow == null)
                return 0;
            else
                return dgvTerminal.CurrentRow.Index;
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error.");
            //}
            //return -1;
        }

        private string GetRegionGroupCityCondition(string sCategory)
        {
            return string.Format(" WHERE {0}RDName = '{1}'", sCategory, sGetGroupRegionCity());
        }

        protected int iGetGRCSelectectedIndex()
        {
            if (dgvGrupRegionCity.CurrentRow == null)
                return 0;
            else
                return dgvGrupRegionCity.CurrentRow.Index;
        }

        protected string sGetGroupRegionCity()
        {
            if (dtListGroupRegionCity.Rows.Count > 0)
            {
                if ((string)cmbCategory.SelectedItem == "Group")
                {
                    return dtListGroupRegionCity.Rows[iGetGRCSelectectedIndex()]["Group"].ToString();
                }
                else if ((string)cmbCategory.SelectedItem == "Region")
                {
                    return dtListGroupRegionCity.Rows[iGetGRCSelectectedIndex()]["Region"].ToString();
                }
                else
                {
                    return dtListGroupRegionCity.Rows[iGetGRCSelectectedIndex()]["City"].ToString();
                }
            }
            else { return null; }
        }
        
        #endregion
        #region "Init ComboBox"
        private void initcomboboxSoftware()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPAppPackageBrowse);
            if (dtSoftware.Rows.Count > 0)
            {
                cmbSoftwareName.DataSource = dtSoftware;
                cmbSoftwareName.DisplayMember = "Software Name";
                cmbSoftwareName.ValueMember = "BuildNumber";
                cmbSoftwareName.SelectedIndex = -1;
            }
        }

        private void initcomboboxTerminalList()
        {
            DataTable dtTerminalList = dtGetData(CommonSP.sSPTerminalListBrowse);
            if (dtTerminalList.Rows.Count > 0)
            {
                cmbTID.DataSource = dtTerminalList;
                cmbTID.ValueMember = "TerminalID";
                cmbTID.DisplayMember = "TerminalID";
                cmbTID.SelectedIndex = -1;
            }
        }

        private DataTable dtGetData(string sSP)
        {
            SqlCommand oCmd = new SqlCommand(sSP, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);
            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();
            return dtTemp;
        }

        #endregion

      

        private void AddGroupRegionCity()
        {
            InsertCategoryName();
            ResetRemoteDownload();
        }

        private void InsertCategoryName()
        {

            FrmPromptString oFrmPromptString = new FrmPromptString(string.Format("Add {0}", (string)cmbCategory.SelectedItem), string.Format("Add New {0} Name", (string)cmbCategory.SelectedItem));
            MaskedTextBox oMaskedBox = CreateOMaskedBox(oFrmPromptString.txtInput.Location, oFrmPromptString.txtInput.Size);
            oFrmPromptString.Controls.Add(oMaskedBox);
            oFrmPromptString.txtInput.Visible = false;
            if (cmbCategory.SelectedIndex != -1 &&
                    oFrmPromptString.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(oMaskedBox.Text))
                {
                    if (dtListGroupRegionCity.Select().Length > 0)
                    {
                        if (dtListGroupRegionCity.Select(string.Format("{0} = '{1}'", (string)cmbCategory.SelectedItem, oMaskedBox.Text)).Length == 0)
                        {
                            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInsertGroupRegionCity, oSqlConn))
                            {
                                oCmd.CommandType = CommandType.StoredProcedure;
                                oCmd.Parameters.Add("@sCategory", SqlDbType.VarChar).Value = (string)cmbCategory.SelectedItem;
                                oCmd.Parameters.Add("@sCategoryName", SqlDbType.VarChar).Value = oMaskedBox.Text;

                                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                                oCmd.ExecuteNonQuery();
                                CommonClass.InputLog(oSqlConn, cmbTID.SelectedValue.ToString(), UserData.sUserID, "", "Add " + (string)cmbCategory.SelectedItem, "Insert new " + (string)cmbCategory.SelectedItem + " : " + oMaskedBox.Text);
                                MessageBox.Show(string.Format("{0} has been added", oMaskedBox.Text));
                            }
                        }
                        else MessageBox.Show(string.Format("{0} Already Exist", sGetGroupRegionCity()));
                    }
                    else
                    {
                        using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInsertGroupRegionCity, oSqlConn))
                        {
                            oCmd.CommandType = CommandType.StoredProcedure;
                            oCmd.Parameters.Add("@sCategory", SqlDbType.VarChar).Value = (string)cmbCategory.SelectedItem;
                            oCmd.Parameters.Add("@sCategoryName", SqlDbType.VarChar).Value = oMaskedBox.Text;

                            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                            oCmd.ExecuteNonQuery();
                            CommonClass.InputLog(oSqlConn, cmbTID.SelectedValue.ToString(), UserData.sUserID, "", "Add " + (string)cmbCategory.SelectedItem, "Insert new " + (string)cmbCategory.SelectedItem + " : " + oMaskedBox.Text);
                            MessageBox.Show(string.Format("{0} has been added", oMaskedBox.Text));
                        }
                    }
                }
                else
                    MessageBox.Show(string.Format("Please Fill {0} Name", (string)cmbCategory.SelectedItem));
            }
        }

      

        private void UpdateGroupRegionCityName()
        {
            try
            {
                if (dgvGrupRegionCity.SelectedCells.Count > 0)
                {
                    UpdateCategoryName();
                    ResetRemoteDownload();
                }
                else MessageBox.Show(string.Format("Please Select {0} Name.", (string)cmbCategory.SelectedValue));
            }
            catch (Exception RenameCategoryError)
            {
                MessageBox.Show(RenameCategoryError.Message);
            }
        }

        private void UpdateCategoryName()
        {
            FrmPromptString oFrmPromptString = new FrmPromptString(string.Format("Rename {0}", (string)cmbCategory.SelectedItem), string.Format(" New {0} Name", (string)cmbCategory.SelectedItem));
            MaskedTextBox oMaskedBox = CreateOMaskedBox(oFrmPromptString.txtInput.Location, oFrmPromptString.txtInput.Size);
            oFrmPromptString.Controls.Add(oMaskedBox);
            oFrmPromptString.txtInput.Visible = false;
            if (cmbCategory.SelectedIndex != -1 &&
                    MessageBox.Show(string.Format("Are you sure want to rename {0} : {1} ?", (string)cmbCategory.SelectedItem, sGetGroupRegionCity()), "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes &&
                    oFrmPromptString.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(oMaskedBox.Text))
                {
                    string sNewCategoryName = oMaskedBox.Text;
                    if (dtListGroupRegionCity.Select(string.Format("{0} = '{1}'", (string)cmbCategory.SelectedItem, sNewCategoryName)).Length == 0)
                    {
                        using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPUpdateGroupRegionCity, oSqlConn))
                        {
                            oCmd.CommandType = CommandType.StoredProcedure;
                            oCmd.Parameters.Add("@sCategory", SqlDbType.VarChar).Value = (string)cmbCategory.SelectedItem;
                            oCmd.Parameters.Add("@sNewCategoryName", SqlDbType.VarChar).Value = sNewCategoryName;
                            oCmd.Parameters.Add("@sOldCategoryName", SqlDbType.VarChar).Value = sGetGroupRegionCity();

                            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                            oCmd.ExecuteNonQuery();
                            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", "Rename " + sGetGroupRegionCity(), "Rename " + sGetGroupRegionCity() + " to " + sNewCategoryName + " in " + (string)cmbCategory.SelectedItem);
                            MessageBox.Show(string.Format("Rename {0} to {1} success",sGetGroupRegionCity(), sNewCategoryName));
                        }
                    }
                    else MessageBox.Show(string.Format("{0} already Exist.", sNewCategoryName));
                }
            }
        }

        

        private void RemoveGroupRegionCityName()
        {
            try
            {
                if (dgvGrupRegionCity.SelectedCells.Count > -1)
                {
                    RemoveCategoryName();
                    ResetRemoteDownload();
                }
                else MessageBox.Show(string.Format("Please Select {0}Name.", (string)cmbCategory.SelectedValue));
            }
            catch (Exception DeleteCategoryError)
            {
                MessageBox.Show(DeleteCategoryError.Message);
            }
        }

        private void RemoveCategoryName()
        {
            if (MessageBox.Show(string.Format("Are you sure want to Delete {0} ?", sGetGroupRegionCity()), "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPRemoveGroupRegionCity, oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sCategory", SqlDbType.VarChar).Value = (string)cmbCategory.SelectedItem;
                    oCmd.Parameters.Add("@sCategoryName", SqlDbType.VarChar).Value = sGetGroupRegionCity();

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                    oCmd.ExecuteNonQuery();
                    CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", "Delete " + (string)cmbCategory.SelectedItem, "Delete " + (string)cmbCategory.SelectedItem + " : " + sGetGroupRegionCity());
                    MessageBox.Show(string.Format("{0} has been deleted", sGetGroupRegionCity()));
                }
            }
        }

        /// <summary>
        /// Create a Masked Text Box.
        /// </summary>
        /// <param name="oPointLocation">The desired location of the Masked Text Box</param>
        /// <param name="oSize">The desired size of the Masked Text Box</param>
        /// <returns>The Masked Text Box</returns>
        private MaskedTextBox CreateOMaskedBox(Point oPointLocation, Size oSize)
        {
            MaskedTextBox oMaskedBox = new MaskedTextBox();
            oMaskedBox.AllowPromptAsInput = false;
            oMaskedBox.AsciiOnly = true;
            oMaskedBox.HidePromptOnLeave = true;
            oMaskedBox.Location = oPointLocation;
            oMaskedBox.PromptChar = ' ';
            oMaskedBox.RejectInputOnFirstFailure = true;
            oMaskedBox.ResetOnPrompt = false;
            oMaskedBox.ResetOnSpace = false;
            oMaskedBox.Size = oSize;
            oMaskedBox.TabIndex = 0;
            return oMaskedBox;
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddGroupRegionCity();
        }

        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (iCountCategory_Click > 0)
                UpdateGroupRegionCityName();
            //else MessageBox.Show(string.Format("Please Click {0}RDName first. Object is not set", (string)cmbCategory.SelectedItem));
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (iCountCategory_Click > 0)
                RemoveGroupRegionCityName();
            //else MessageBox.Show(string.Format("Please Click {0}RDName first. Object is not set", (string)cmbCategory.SelectedItem));
        }

        private void deleteTerminalIDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (iCountListTID_Click > 0)
            //{
                try
                {
                    //dtAllDataTID = GetAllTID();
                    RemoveListTID();
                }
                catch (Exception DeleteError)
                {
                    MessageBox.Show(DeleteError.Message);
                }
            //}
            //else MessageBox.Show(string.Format("Please Click TerminalID first. Object is not set."));
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (iCountListTID_Click > 0)
            //{
                try
                {
                    EditListTID();
                }
                catch (Exception EditError)
                {
                    MessageBox.Show(EditError.Message);
                }
            //}
            //else MessageBox.Show(string.Format("Please Click TerminalID first. Object is not set."));
        }

        private void addTerminalIDToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ClearInfoTID();
            LockInfoFunction();
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void cmbSoftwareName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSoftwareName.SelectedIndex > -1)
                txtBuildNumber.Text = cmbSoftwareName.SelectedValue.ToString();
            else
                txtBuildNumber.Text = "";
        }

        private void cmsTerminal_Opening(object sender, CancelEventArgs e)
        {
            if (dgvTerminal.DataSource == null || dgvTerminal.Rows.Count == 0)
            {
                deleteTerminalIDToolStripMenuItem.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Delete); ;
                editToolStripMenuItem.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Edit); 
            }
            else
            {
                deleteTerminalIDToolStripMenuItem.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Delete); ;
                editToolStripMenuItem.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Edit); 
            }
        }

        private void cmsGrupRegionCity_Opening(object sender, CancelEventArgs e)
        {
            if (dgvGrupRegionCity.DataSource == null)
            {
                cmsGrupRegionCity.Visible = false;
            }
            if (dgvGrupRegionCity.Rows.Count == 0)
            { 
                deleteToolStripMenuItem.Visible = false;
                renameToolStripMenuItem.Visible = false;
                addTerminalIDToolStripMenuItem1.Visible = false;
            }
        }

        private void dgvTerminal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                if (dgvTerminal.SelectedCells.Count > 0)
                {
                    LoadListTid();
                    e.SuppressKeyPress = true;
                }
        }

        private void dgvGrupRegionCity_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (dgvGrupRegionCity.SelectedCells.Count > 0)
                {
                    LoadListTid();
                    if (dgvTerminal.Rows.Count > 0)
                        FillInfoRemoteDownload();
                    
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

        }

        private void dgvTerminal_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (dgvTerminal.SelectedCells.Count > 0)
                {
                    ClearInfoTID();
                    FillInfoRemoteDownload();
                    
                }
            }
            catch (Exception ex)
            { }
            
        }

        private void dgvTerminal_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        } 

        
    }
}
