﻿namespace InSys
{
    partial class frmUpdateRD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUpdateRD));
            this.lblGroup = new System.Windows.Forms.Label();
            this.lblRegion = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.cmbCity = new System.Windows.Forms.ComboBox();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.cmbRegion = new System.Windows.Forms.ComboBox();
            this.chkEnableRD = new System.Windows.Forms.CheckBox();
            this.cmbSoftwareName = new System.Windows.Forms.ComboBox();
            this.lblSoftwareName = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.gbData = new System.Windows.Forms.GroupBox();
            this.cmbDatabaseName = new System.Windows.Forms.ComboBox();
            this.lblDatabaseName = new System.Windows.Forms.Label();
            this.cmbRDid = new System.Windows.Forms.ComboBox();
            this.lblRDid = new System.Windows.Forms.Label();
            this.gbData.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.Location = new System.Drawing.Point(12, 15);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(36, 13);
            this.lblGroup.TabIndex = 0;
            this.lblGroup.Text = "Group";
            this.lblGroup.Click += new System.EventHandler(this.lblGroup_Click);
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Location = new System.Drawing.Point(12, 42);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(41, 13);
            this.lblRegion.TabIndex = 1;
            this.lblRegion.Text = "Region";
            this.lblRegion.Click += new System.EventHandler(this.lblRegion_Click);
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(12, 69);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(24, 13);
            this.lblCity.TabIndex = 2;
            this.lblCity.Text = "City";
            this.lblCity.Click += new System.EventHandler(this.lblCity_Click);
            // 
            // cmbCity
            // 
            this.cmbCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCity.FormattingEnabled = true;
            this.cmbCity.Location = new System.Drawing.Point(63, 66);
            this.cmbCity.Name = "cmbCity";
            this.cmbCity.Size = new System.Drawing.Size(121, 21);
            this.cmbCity.TabIndex = 3;
            this.cmbCity.SelectedIndexChanged += new System.EventHandler(this.cmbCity_SelectedIndexChanged);
            // 
            // cmbGroup
            // 
            this.cmbGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(63, 12);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(121, 21);
            this.cmbGroup.TabIndex = 1;
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged);
            // 
            // cmbRegion
            // 
            this.cmbRegion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbRegion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbRegion.FormattingEnabled = true;
            this.cmbRegion.Location = new System.Drawing.Point(63, 39);
            this.cmbRegion.Name = "cmbRegion";
            this.cmbRegion.Size = new System.Drawing.Size(121, 21);
            this.cmbRegion.TabIndex = 2;
            this.cmbRegion.SelectedIndexChanged += new System.EventHandler(this.cmbRegion_SelectedIndexChanged);
            // 
            // chkEnableRD
            // 
            this.chkEnableRD.AutoSize = true;
            this.chkEnableRD.Checked = true;
            this.chkEnableRD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEnableRD.Location = new System.Drawing.Point(19, 19);
            this.chkEnableRD.Name = "chkEnableRD";
            this.chkEnableRD.Size = new System.Drawing.Size(150, 17);
            this.chkEnableRD.TabIndex = 4;
            this.chkEnableRD.Text = "Enable Remote Download";
            this.chkEnableRD.UseVisualStyleBackColor = true;
            this.chkEnableRD.CheckedChanged += new System.EventHandler(this.chkEnableRD_CheckedChanged);
            // 
            // cmbSoftwareName
            // 
            this.cmbSoftwareName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSoftwareName.FormattingEnabled = true;
            this.cmbSoftwareName.Location = new System.Drawing.Point(150, 99);
            this.cmbSoftwareName.Name = "cmbSoftwareName";
            this.cmbSoftwareName.Size = new System.Drawing.Size(160, 21);
            this.cmbSoftwareName.TabIndex = 9;
            // 
            // lblSoftwareName
            // 
            this.lblSoftwareName.AutoSize = true;
            this.lblSoftwareName.Location = new System.Drawing.Point(16, 102);
            this.lblSoftwareName.Name = "lblSoftwareName";
            this.lblSoftwareName.Size = new System.Drawing.Size(49, 13);
            this.lblSoftwareName.TabIndex = 10;
            this.lblSoftwareName.Text = "Software";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(353, 169);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 47);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // gbData
            // 
            this.gbData.Controls.Add(this.cmbDatabaseName);
            this.gbData.Controls.Add(this.lblDatabaseName);
            this.gbData.Controls.Add(this.cmbRDid);
            this.gbData.Controls.Add(this.lblRDid);
            this.gbData.Controls.Add(this.chkEnableRD);
            this.gbData.Controls.Add(this.lblSoftwareName);
            this.gbData.Controls.Add(this.cmbSoftwareName);
            this.gbData.Location = new System.Drawing.Point(15, 97);
            this.gbData.Name = "gbData";
            this.gbData.Size = new System.Drawing.Size(330, 132);
            this.gbData.TabIndex = 12;
            this.gbData.TabStop = false;
            // 
            // cmbDatabaseName
            // 
            this.cmbDatabaseName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDatabaseName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDatabaseName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDatabaseName.FormattingEnabled = true;
            this.cmbDatabaseName.Items.AddRange(new object[] {
            "ALL"});
            this.cmbDatabaseName.Location = new System.Drawing.Point(150, 45);
            this.cmbDatabaseName.Name = "cmbDatabaseName";
            this.cmbDatabaseName.Size = new System.Drawing.Size(121, 21);
            this.cmbDatabaseName.TabIndex = 5;
            this.cmbDatabaseName.SelectedIndexChanged += new System.EventHandler(this.cmbDatabaseName_SelectedIndexChanged);
            // 
            // lblDatabaseName
            // 
            this.lblDatabaseName.AutoSize = true;
            this.lblDatabaseName.Location = new System.Drawing.Point(16, 48);
            this.lblDatabaseName.Name = "lblDatabaseName";
            this.lblDatabaseName.Size = new System.Drawing.Size(81, 13);
            this.lblDatabaseName.TabIndex = 13;
            this.lblDatabaseName.Text = "DatabaseName";
            // 
            // cmbRDid
            // 
            this.cmbRDid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRDid.Enabled = false;
            this.cmbRDid.FormattingEnabled = true;
            this.cmbRDid.Location = new System.Drawing.Point(150, 72);
            this.cmbRDid.Name = "cmbRDid";
            this.cmbRDid.Size = new System.Drawing.Size(160, 21);
            this.cmbRDid.TabIndex = 6;
            // 
            // lblRDid
            // 
            this.lblRDid.AutoSize = true;
            this.lblRDid.Location = new System.Drawing.Point(18, 75);
            this.lblRDid.Name = "lblRDid";
            this.lblRDid.Size = new System.Drawing.Size(126, 13);
            this.lblRDid.TabIndex = 11;
            this.lblRDid.Text = "Remote Download Name";
            // 
            // frmUpdateRD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 236);
            this.Controls.Add(this.gbData);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.cmbRegion);
            this.Controls.Add(this.cmbGroup);
            this.Controls.Add(this.cmbCity);
            this.Controls.Add(this.lblCity);
            this.Controls.Add(this.lblRegion);
            this.Controls.Add(this.lblGroup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUpdateRD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update by Group,Region or City";
            this.Load += new System.EventHandler(this.frmUpdateRD_Load);
            this.gbData.ResumeLayout(false);
            this.gbData.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.ComboBox cmbCity;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.ComboBox cmbRegion;
        private System.Windows.Forms.CheckBox chkEnableRD;
        private System.Windows.Forms.ComboBox cmbSoftwareName;
        private System.Windows.Forms.Label lblSoftwareName;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.GroupBox gbData;
        private System.Windows.Forms.ComboBox cmbRDid;
        private System.Windows.Forms.Label lblRDid;
        private System.Windows.Forms.Label lblDatabaseName;
        private System.Windows.Forms.ComboBox cmbDatabaseName;
    }
}