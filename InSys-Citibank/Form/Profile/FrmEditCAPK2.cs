﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmEditCAPK2 : Form
    {
        /// <summary>
        /// Deterimines if form is in Editing mode or Adding mode.
        /// </summary>
        protected bool IsEdit;
        protected SqlConnection oSqlConn;
        protected string sDbId;
        protected string sDbName;
        
        
        protected string sCAPKPubIdx;    //PK01
        protected string sCAPKPubRID;      //PK02
        protected string sCAPKPubKeyData;   //PK03
        protected string sCAPKPubExp;  //PK04
        protected string sCAPKPubHash; //PK05
        protected string sCAPKPubAlgo;     //PK06

        protected const string CAPKPubIdx = "PK001";
        protected const string CAPKPubRID = "PK002";
        protected const string CAPKPubKeyData = "PK003";
        protected const string CAPPubExp = "PK004";
        protected const string CAPKPubHash = "PK005";
        protected const string CAPKPubAlgo = "PK006";

        /// <summary>
        /// Form to Add or modify CAPK data
        /// </summary>
        /// <param name="_isEdit">boolean : true if in Edit mode, else false in Add mode</param>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sDbId">string : DatabaseId</param>
        public FrmEditCAPK2(bool _isEdit, SqlConnection _oSqlConn, string _sDbId, string _sDbName) : this(_isEdit, _oSqlConn, _sDbId, _sDbName, "") { }
        public FrmEditCAPK2(bool _isEdit, SqlConnection _oSqlConn, string _sDbId, string _sDbName, string _sCAPKIndex)
        {
            IsEdit = _isEdit;
            oSqlConn = _oSqlConn;
            sDbId = _sDbId;
            sDbName = _sDbName;
            sCAPKPubIdx = _sCAPKIndex;
            InitializeComponent();
        }

        private void FrmEditCAPK2_Load(object sender, EventArgs e)
        {
            InitCAPK();
            SetDisplay();
            string sLog = CommonMessage.sFormOpened + this.Text;
            if (IsEdit)
                sLog += " : " + sCAPKPubIdx;

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, sLog, "");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                if (IsEdit)
                    UpdateCAPK(sGenCAPKContent);
                else
                    SaveCAPK(sGenCAPKContent);
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region "Properties"
        protected string sGetCAPKPubIdx { get { return txtPubIdx.Text; } }
        protected string sGetCAPKPubRid { get { return txtPubRid.Text; } }
        protected string sGetCAPKPubKeyData { get { return txtPubKeyData.Text; } }
        protected string sGetCAPKPubExp { get { return txtPubExp.Text; } }
        protected string sGetCAPKPubHash { get { return txtPubHash.Text; } }
        protected string sGetCAPKPubAlgo { get { return txtPubAlgo.Text; } }

        /// <summary>
        /// Generated CAPK content
        /// </summary>
        protected string sGenCAPKContent
        {
            get
            {
                return sGenCAPKIndex() + sGenCAPKRID() + sGenCAPKKeyData()
                    + sGenCAPKExp() + sGenCAPKHash() + sGenCAPKAlgo();
            }
        }

        /// <summary>
        /// Generated CAPK Index
        /// </summary>
        /// <returns>string : CAPK Index</returns>
        protected string sGenCAPKIndex()
        {
            return string.Format("{0}{1:000}{2}", CAPKPubIdx, sGetCAPKPubIdx.Length, sGetCAPKPubIdx);
        }

        /// <summary>
        /// Generated CAPK RID
        /// </summary>
        /// <returns>string : CAPK RID</returns>
        protected string sGenCAPKRID()
        {
            return string.Format("{0}{1:000}{2}", CAPKPubRID, sGetCAPKPubRid.Length, sGetCAPKPubRid);
        }

        /// <summary>
        /// Generated CAPK Length
        /// </summary>
        /// <returns>string : CAPK Length</returns>
        protected string sGenCAPKKeyData()
        {
            return string.Format("{0}{1:000}{2}", CAPKPubKeyData, sGetCAPKPubKeyData.Length, sGetCAPKPubKeyData);
        }

        /// <summary>
        /// Generated modulus result from CAPK
        /// </summary>
        /// <returns>string : CAPK Modulus</returns>
        protected string sGenCAPKExp()
        {
            return string.Format("{0}{1:000}{2}", CAPPubExp, sGetCAPKPubExp.Length, sGetCAPKPubExp);
        }

        /// <summary>
        /// Generated CAPK Exponent value
        /// </summary>
        /// <returns>string : CAPK Exponent</returns>
        protected string sGenCAPKHash()
        {
            return string.Format("{0}{1:000}{2}", CAPKPubHash, sGetCAPKPubHash.Length, sGetCAPKPubHash);
        }

        /// <summary>
        /// Generted CAPK Hash
        /// </summary>
        /// <returns>string : CAPK Hash</returns>
        protected string sGenCAPKAlgo()
        {
            return string.Format("{0}{1:000}{2}", CAPKPubAlgo, sGetCAPKPubAlgo.Length, sGetCAPKPubAlgo);
        }
        #endregion

        #region "Function"
        /// <summary>
        /// Initiate values from database to objects in form
        /// </summary>
        protected void InitCAPK()
        {
            DataTable dtCAPK = dtGetCAPK();
            foreach (DataRow drow in dtCAPK.Rows)
            {
                switch (drow["CAPKTag"].ToString())
                {
                    case CAPKPubIdx:
                        sCAPKPubIdx = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPKPubRID:
                        sCAPKPubRID = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPKPubKeyData:
                        sCAPKPubKeyData = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPPubExp:
                        sCAPKPubExp = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPKPubHash:
                        sCAPKPubHash = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPKPubAlgo:
                        sCAPKPubAlgo = drow["CAPKTagValue"].ToString();
                        break;
                }

            }
        }

        /// <summary>
        /// WHERE condition to filter data loaded from database
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sCondition()
        {
            return "WHERE DatabaseId = " + sDbId + " "
                + "AND CAPKIndex = '" + sCAPKPubIdx + "'";
        }

        /// <summary>
        /// Set the form's display.
        /// </summary>
        protected void SetDisplay()
        {
            if (!IsEdit) this.Text = "CAPK Management - Add";
            else this.Text = "CAPK Management - Edit";
            txtPubIdx.Enabled = txtPubRid.Enabled = txtPubKeyData.Enabled =
                txtPubExp.Enabled = txtPubHash.Enabled = txtPubAlgo.Enabled = 
                btnSave.Visible = btnCancel.Visible =
                UserPrivilege.IsAllowed(PrivilegeCode.PK, Privilege.Edit);

            txtPubIdx.Text = sCAPKPubIdx;
            txtPubRid.Text = sCAPKPubRID;
            txtPubKeyData.Text = sCAPKPubKeyData;
            txtPubExp.Text = sCAPKPubExp;
            txtPubHash.Text = sCAPKPubHash;
            txtPubAlgo.Text = sCAPKPubAlgo;
            txtPubIdx.ReadOnly = IsEdit;
        }

        /// <summary>
        /// Load data from database then stored in DataTable
        /// </summary>
        /// <returns>DataTable : contain data loaded from database</returns>
        protected DataTable dtGetCAPK()
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileCAPKBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sCondition();
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            oCmd.Dispose();
            return dtTemp;
        }

        /// <summary>
        /// Determine whether values entered by user valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValid()
        {
            if (txtPubIdx.Text.Length == 5 &&
                txtPubRid.Text.Length == 10 &&
                txtPubExp.Text.Length >= 2 &&
                txtPubHash.Text.Length == 40 &&
                txtPubAlgo.Text.Length == 2)
                if (!IsEdit)
                    if (IsValidCAPKIndex())
                    {
                        MessageBox.Show(CommonMessage.sErrCAPKInvalid);
                        return false;
                    }
                    else
                        return true;
                else
                    return true;
            else
            {
                MessageBox.Show("Index min. lenght = 4.\n RID min. lenght = 4.\n Exponent min. lenght = 4.\n Hash min. lenght = 4.\n Algo min. lenght = 4.");
                return false;
            }
        }

        /// <summary>
        /// Determine whether given CAPK Index value valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValidCAPKIndex()
        {
            sCAPKPubIdx = sGetCAPKPubIdx;
            DataTable dtTemp = dtGetCAPK();
            return dtTemp.Rows.Count > 0 ? true : false;
        }

        /// <summary>
        /// Save new data CAPK entered by user into database
        /// </summary>
        /// <param name="sCAPKContent">string : content of CAPK</param>
        protected void SaveCAPK(string sCAPKContent)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileCAPKInsert, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDbId;
            oCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sCAPKContent;
            oCmd.ExecuteNonQuery();
            oCmd.Dispose();

            string sLogDetail = string.Format("{0} : {1} \n", "CAPK Index", sGetCAPKPubIdx);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK RID", sGetCAPKPubRid);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK Key Data", sGetCAPKPubKeyData);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK Exp", sGetCAPKPubExp);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK Hash", sGetCAPKPubHash);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK Algo", sGetCAPKPubAlgo);

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Add CAPK : " + sGetCAPKPubIdx, sLogDetail);
        }

        /// <summary>
        /// Update existing CAPK value to database
        /// </summary>
        /// <param name="sCAPKContent">string : content of CAPK</param>
        protected void UpdateCAPK(string sCAPKContent)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileCAPKUpdate, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDbId;
            oCmd.Parameters.Add("@sCAPKIndex", SqlDbType.VarChar).Value = sCAPKPubIdx;
            oCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sCAPKContent;
            //oCmd.ExecuteNonQuery();

            string sLogDetail = "";

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                while (oRead.Read())
                {
                    sLogDetail += string.Format("{0} : {1} --> {2} \n", oRead["TagName"].ToString(),
                                    oRead["OldCAPKValue"].ToString(), oRead["NewCAPKValue"].ToString());
                }
                oRead.Close();
            }
            oCmd.Dispose();

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Update CAPK : " + sGetCAPKPubIdx, sLogDetail);
        }
        #endregion

        
    }
}
