namespace InSys
{
    partial class FrmExpand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExpand));
            this.ButtonIcon = new System.Windows.Forms.ImageList(this.components);
            this.IconList = new System.Windows.Forms.ImageList(this.components);
            this.RefreshTime = new System.Windows.Forms.Timer(this.components);
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnEMVManagement = new System.Windows.Forms.Button();
            this.btnAdvanceSearch = new System.Windows.Forms.Button();
            this.btnRDMan = new System.Windows.Forms.Button();
            this.btnProdCodeMan = new System.Windows.Forms.Button();
            this.btnPinPadKeyMan = new System.Windows.Forms.Button();
            this.btnGPRSMan = new System.Windows.Forms.Button();
            this.btnTLEMan = new System.Windows.Forms.Button();
            this.btnFindSN = new System.Windows.Forms.Button();
            this.btnBankCodeMan = new System.Windows.Forms.Button();
            this.btnCurrMan = new System.Windows.Forms.Button();
            this.btnSNReg = new System.Windows.Forms.Button();
            this.btnTMK = new System.Windows.Forms.Button();
            this.lblOther = new System.Windows.Forms.Label();
            this.btnLoyMan = new System.Windows.Forms.Button();
            this.btnLoyProdMan = new System.Windows.Forms.Button();
            this.lblManagementTitle = new System.Windows.Forms.Label();
            this.lblTerminalTitle = new System.Windows.Forms.Label();
            this.btnNewTerminal = new System.Windows.Forms.Button();
            this.btnRegister = new System.Windows.Forms.Button();
            this.btnTenLast = new System.Windows.Forms.Button();
            this.btnCardManagement = new System.Windows.Forms.Button();
            this.btnKeyMan = new System.Windows.Forms.Button();
            this.btnAIDMan = new System.Windows.Forms.Button();
            this.btnViewAll = new System.Windows.Forms.Button();
            this.tStripSearch = new System.Windows.Forms.ToolStrip();
            this.tStripTxtSearch = new System.Windows.Forms.ToolStripTextBox();
            this.tStripBtnSearch = new System.Windows.Forms.ToolStripButton();
            this.gbView = new System.Windows.Forms.GroupBox();
            this.lblTagDetail = new System.Windows.Forms.Label();
            this.lblDetail = new System.Windows.Forms.Label();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.tvTerminal = new System.Windows.Forms.TreeView();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvTerminal = new System.Windows.Forms.DataGridView();
            this.dgvTagDetail = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gbEdcInf = new System.Windows.Forms.GroupBox();
            this.tabcntrInformation = new System.Windows.Forms.TabControl();
            this.tabPageAutoInit = new System.Windows.Forms.TabPage();
            this.txtICCID = new System.Windows.Forms.TextBox();
            this.txtMemUsage = new System.Windows.Forms.TextBox();
            this.txtMCSN = new System.Windows.Forms.TextBox();
            this.txtReaderSN = new System.Windows.Forms.TextBox();
            this.txtPSAMSN = new System.Windows.Forms.TextBox();
            this.txtEDCSN = new System.Windows.Forms.TextBox();
            this.txtKernelVer = new System.Windows.Forms.TextBox();
            this.txtPABX = new System.Windows.Forms.TextBox();
            this.txtLastInit = new System.Windows.Forms.TextBox();
            this.txtOSVer = new System.Windows.Forms.TextBox();
            this.txtSoftwareVer = new System.Windows.Forms.TextBox();
            this.txtTID = new System.Windows.Forms.TextBox();
            this.lblMemUsage = new System.Windows.Forms.Label();
            this.lblMCSN = new System.Windows.Forms.Label();
            this.lblPSAMSN = new System.Windows.Forms.Label();
            this.lblReaderSN = new System.Windows.Forms.Label();
            this.lblEDCSN = new System.Windows.Forms.Label();
            this.lblKernelVersion = new System.Windows.Forms.Label();
            this.lblOSVer = new System.Windows.Forms.Label();
            this.lblSoftVer = new System.Windows.Forms.Label();
            this.lblPABX = new System.Windows.Forms.Label();
            this.lblLastInitTime = new System.Windows.Forms.Label();
            this.lblTID = new System.Windows.Forms.Label();
            this.lblICCID = new System.Windows.Forms.Label();
            this.tabPageInit = new System.Windows.Forms.TabPage();
            this.txtInitSoftwareVer = new System.Windows.Forms.TextBox();
            this.lblSoftwareVersion = new System.Windows.Forms.Label();
            this.txtTIDInit = new System.Windows.Forms.TextBox();
            this.lblTIDInit = new System.Windows.Forms.Label();
            this.txtEdcSNInit = new System.Windows.Forms.TextBox();
            this.lblEDCSNnInit = new System.Windows.Forms.Label();
            this.lvAppEDC = new System.Windows.Forms.ListView();
            this.btnRemovePackage = new System.Windows.Forms.Button();
            this.btnAddPackage = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gbButton.SuspendLayout();
            this.tStripSearch.SuspendLayout();
            this.gbView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTagDetail)).BeginInit();
            this.gbEdcInf.SuspendLayout();
            this.tabcntrInformation.SuspendLayout();
            this.tabPageAutoInit.SuspendLayout();
            this.tabPageInit.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonIcon
            // 
            this.ButtonIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ButtonIcon.ImageStream")));
            this.ButtonIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ButtonIcon.Images.SetKeyName(0, "");
            this.ButtonIcon.Images.SetKeyName(1, "");
            this.ButtonIcon.Images.SetKeyName(2, "");
            // 
            // IconList
            // 
            this.IconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconList.ImageStream")));
            this.IconList.TransparentColor = System.Drawing.Color.Transparent;
            this.IconList.Images.SetKeyName(0, "");
            this.IconList.Images.SetKeyName(1, "");
            this.IconList.Images.SetKeyName(2, "");
            this.IconList.Images.SetKeyName(3, "");
            this.IconList.Images.SetKeyName(4, "search.jpg");
            // 
            // RefreshTime
            // 
            this.RefreshTime.Interval = 600000;
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.AutoScroll = true;
            this.ContentPanel.Size = new System.Drawing.Size(965, 512);
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.BackColor = System.Drawing.SystemColors.Control;
            this.gbButton.Controls.Add(this.btnReset);
            this.gbButton.Controls.Add(this.btnEMVManagement);
            this.gbButton.Controls.Add(this.btnAdvanceSearch);
            this.gbButton.Controls.Add(this.btnRDMan);
            this.gbButton.Controls.Add(this.btnProdCodeMan);
            this.gbButton.Controls.Add(this.btnPinPadKeyMan);
            this.gbButton.Controls.Add(this.btnGPRSMan);
            this.gbButton.Controls.Add(this.btnTLEMan);
            this.gbButton.Controls.Add(this.btnFindSN);
            this.gbButton.Controls.Add(this.btnBankCodeMan);
            this.gbButton.Controls.Add(this.btnCurrMan);
            this.gbButton.Controls.Add(this.btnSNReg);
            this.gbButton.Controls.Add(this.btnTMK);
            this.gbButton.Controls.Add(this.lblOther);
            this.gbButton.Controls.Add(this.btnLoyMan);
            this.gbButton.Controls.Add(this.btnLoyProdMan);
            this.gbButton.Controls.Add(this.lblManagementTitle);
            this.gbButton.Controls.Add(this.lblTerminalTitle);
            this.gbButton.Controls.Add(this.btnNewTerminal);
            this.gbButton.Controls.Add(this.btnRegister);
            this.gbButton.Controls.Add(this.btnTenLast);
            this.gbButton.Controls.Add(this.btnCardManagement);
            this.gbButton.Controls.Add(this.btnKeyMan);
            this.gbButton.Controls.Add(this.btnAIDMan);
            this.gbButton.Controls.Add(this.btnViewAll);
            this.gbButton.Location = new System.Drawing.Point(839, 34);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(213, 675);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(131, 11);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 32);
            this.btnReset.TabIndex = 400;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnEMVManagement
            // 
            this.btnEMVManagement.BackColor = System.Drawing.SystemColors.Control;
            this.btnEMVManagement.FlatAppearance.BorderSize = 0;
            this.btnEMVManagement.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnEMVManagement.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnEMVManagement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEMVManagement.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEMVManagement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEMVManagement.Location = new System.Drawing.Point(12, 475);
            this.btnEMVManagement.Name = "btnEMVManagement";
            this.btnEMVManagement.Size = new System.Drawing.Size(201, 23);
            this.btnEMVManagement.TabIndex = 23;
            this.btnEMVManagement.Text = "EMV Management";
            this.btnEMVManagement.UseVisualStyleBackColor = false;
            this.btnEMVManagement.Click += new System.EventHandler(this.btnEMVManagement_Click);
            // 
            // btnAdvanceSearch
            // 
            this.btnAdvanceSearch.Location = new System.Drawing.Point(5, 12);
            this.btnAdvanceSearch.Name = "btnAdvanceSearch";
            this.btnAdvanceSearch.Size = new System.Drawing.Size(123, 31);
            this.btnAdvanceSearch.TabIndex = 22;
            this.btnAdvanceSearch.Text = "Advanced Search";
            this.btnAdvanceSearch.UseVisualStyleBackColor = true;
            this.btnAdvanceSearch.Click += new System.EventHandler(this.btnAdvanceSearch_Click);
            // 
            // btnRDMan
            // 
            this.btnRDMan.BackColor = System.Drawing.SystemColors.Control;
            this.btnRDMan.FlatAppearance.BorderSize = 0;
            this.btnRDMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnRDMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnRDMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRDMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRDMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRDMan.Location = new System.Drawing.Point(12, 400);
            this.btnRDMan.Name = "btnRDMan";
            this.btnRDMan.Size = new System.Drawing.Size(201, 23);
            this.btnRDMan.TabIndex = 21;
            this.btnRDMan.Text = "Remote Download Management";
            this.btnRDMan.UseVisualStyleBackColor = false;
            this.btnRDMan.Click += new System.EventHandler(this.btnRDMan_Click);
            // 
            // btnProdCodeMan
            // 
            this.btnProdCodeMan.BackColor = System.Drawing.SystemColors.Control;
            this.btnProdCodeMan.FlatAppearance.BorderSize = 0;
            this.btnProdCodeMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnProdCodeMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnProdCodeMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProdCodeMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProdCodeMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProdCodeMan.Location = new System.Drawing.Point(10, 450);
            this.btnProdCodeMan.Name = "btnProdCodeMan";
            this.btnProdCodeMan.Size = new System.Drawing.Size(201, 23);
            this.btnProdCodeMan.TabIndex = 20;
            this.btnProdCodeMan.Text = "Product Code Management";
            this.btnProdCodeMan.UseVisualStyleBackColor = false;
            this.btnProdCodeMan.Click += new System.EventHandler(this.btnProdCodeMan_Click);
            // 
            // btnPinPadKeyMan
            // 
            this.btnPinPadKeyMan.BackColor = System.Drawing.SystemColors.Control;
            this.btnPinPadKeyMan.FlatAppearance.BorderSize = 0;
            this.btnPinPadKeyMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnPinPadKeyMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnPinPadKeyMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPinPadKeyMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPinPadKeyMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPinPadKeyMan.Location = new System.Drawing.Point(6, 375);
            this.btnPinPadKeyMan.Name = "btnPinPadKeyMan";
            this.btnPinPadKeyMan.Size = new System.Drawing.Size(201, 23);
            this.btnPinPadKeyMan.TabIndex = 18;
            this.btnPinPadKeyMan.Text = "PIN Pad Key Management";
            this.btnPinPadKeyMan.UseVisualStyleBackColor = false;
            this.btnPinPadKeyMan.Click += new System.EventHandler(this.btnPinPadKeyMan_Click);
            // 
            // btnGPRSMan
            // 
            this.btnGPRSMan.BackColor = System.Drawing.SystemColors.Control;
            this.btnGPRSMan.FlatAppearance.BorderSize = 0;
            this.btnGPRSMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnGPRSMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnGPRSMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGPRSMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGPRSMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGPRSMan.Location = new System.Drawing.Point(6, 350);
            this.btnGPRSMan.Name = "btnGPRSMan";
            this.btnGPRSMan.Size = new System.Drawing.Size(201, 23);
            this.btnGPRSMan.TabIndex = 11;
            this.btnGPRSMan.Text = "GPRS/Eth Management";
            this.btnGPRSMan.UseVisualStyleBackColor = false;
            this.btnGPRSMan.Click += new System.EventHandler(this.btnGPRSMan_Click);
            // 
            // btnTLEMan
            // 
            this.btnTLEMan.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnTLEMan.FlatAppearance.BorderSize = 0;
            this.btnTLEMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnTLEMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnTLEMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTLEMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTLEMan.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnTLEMan.Location = new System.Drawing.Point(6, 250);
            this.btnTLEMan.Name = "btnTLEMan";
            this.btnTLEMan.Size = new System.Drawing.Size(201, 23);
            this.btnTLEMan.TabIndex = 8;
            this.btnTLEMan.Text = "TLE Management";
            this.btnTLEMan.Click += new System.EventHandler(this.btnTLEMan_Click);
            // 
            // btnFindSN
            // 
            this.btnFindSN.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnFindSN.FlatAppearance.BorderSize = 0;
            this.btnFindSN.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnFindSN.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnFindSN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFindSN.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFindSN.Image = ((System.Drawing.Image)(resources.GetObject("btnFindSN.Image")));
            this.btnFindSN.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFindSN.Location = new System.Drawing.Point(5, 626);
            this.btnFindSN.Name = "btnFindSN";
            this.btnFindSN.Size = new System.Drawing.Size(201, 23);
            this.btnFindSN.TabIndex = 17;
            this.btnFindSN.Text = "         Find SN";
            this.btnFindSN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFindSN.Visible = false;
            // 
            // btnBankCodeMan
            // 
            this.btnBankCodeMan.BackColor = System.Drawing.SystemColors.Control;
            this.btnBankCodeMan.FlatAppearance.BorderSize = 0;
            this.btnBankCodeMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnBankCodeMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnBankCodeMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBankCodeMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBankCodeMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBankCodeMan.Location = new System.Drawing.Point(10, 425);
            this.btnBankCodeMan.Name = "btnBankCodeMan";
            this.btnBankCodeMan.Size = new System.Drawing.Size(201, 23);
            this.btnBankCodeMan.TabIndex = 19;
            this.btnBankCodeMan.Text = "Bank Code Management";
            this.btnBankCodeMan.UseVisualStyleBackColor = false;
            this.btnBankCodeMan.Click += new System.EventHandler(this.btnBankCodeMan_Click);
            // 
            // btnCurrMan
            // 
            this.btnCurrMan.BackColor = System.Drawing.SystemColors.Control;
            this.btnCurrMan.FlatAppearance.BorderSize = 0;
            this.btnCurrMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnCurrMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnCurrMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCurrMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCurrMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCurrMan.Location = new System.Drawing.Point(6, 325);
            this.btnCurrMan.Name = "btnCurrMan";
            this.btnCurrMan.Size = new System.Drawing.Size(201, 23);
            this.btnCurrMan.TabIndex = 12;
            this.btnCurrMan.Text = "Currency Management";
            this.btnCurrMan.UseVisualStyleBackColor = false;
            this.btnCurrMan.Click += new System.EventHandler(this.btnCurrMan_Click);
            // 
            // btnSNReg
            // 
            this.btnSNReg.FlatAppearance.BorderSize = 0;
            this.btnSNReg.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSNReg.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnSNReg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSNReg.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSNReg.Image = ((System.Drawing.Image)(resources.GetObject("btnSNReg.Image")));
            this.btnSNReg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSNReg.Location = new System.Drawing.Point(5, 603);
            this.btnSNReg.Name = "btnSNReg";
            this.btnSNReg.Size = new System.Drawing.Size(201, 23);
            this.btnSNReg.TabIndex = 16;
            this.btnSNReg.Text = "         Terminal-SN";
            this.btnSNReg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSNReg.Visible = false;
            // 
            // btnTMK
            // 
            this.btnTMK.FlatAppearance.BorderSize = 0;
            this.btnTMK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnTMK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnTMK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTMK.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTMK.Image = ((System.Drawing.Image)(resources.GetObject("btnTMK.Image")));
            this.btnTMK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTMK.Location = new System.Drawing.Point(5, 578);
            this.btnTMK.Name = "btnTMK";
            this.btnTMK.Size = new System.Drawing.Size(201, 23);
            this.btnTMK.TabIndex = 15;
            this.btnTMK.Text = "         TMK Management";
            this.btnTMK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTMK.Visible = false;
            // 
            // lblOther
            // 
            this.lblOther.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblOther.Location = new System.Drawing.Point(0, 522);
            this.lblOther.Name = "lblOther";
            this.lblOther.Size = new System.Drawing.Size(211, 23);
            this.lblOther.TabIndex = 13;
            this.lblOther.Text = "O T H E R";
            this.lblOther.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLoyMan
            // 
            this.btnLoyMan.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoyMan.FlatAppearance.BorderSize = 0;
            this.btnLoyMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnLoyMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnLoyMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoyMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoyMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoyMan.Location = new System.Drawing.Point(6, 275);
            this.btnLoyMan.Name = "btnLoyMan";
            this.btnLoyMan.Size = new System.Drawing.Size(201, 23);
            this.btnLoyMan.TabIndex = 9;
            this.btnLoyMan.Text = "Loyalty Management";
            this.btnLoyMan.UseVisualStyleBackColor = false;
            this.btnLoyMan.Click += new System.EventHandler(this.btnLoyMan_Click);
            // 
            // btnLoyProdMan
            // 
            this.btnLoyProdMan.BackColor = System.Drawing.SystemColors.Control;
            this.btnLoyProdMan.FlatAppearance.BorderSize = 0;
            this.btnLoyProdMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnLoyProdMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnLoyProdMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoyProdMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoyProdMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoyProdMan.Location = new System.Drawing.Point(6, 300);
            this.btnLoyProdMan.Name = "btnLoyProdMan";
            this.btnLoyProdMan.Size = new System.Drawing.Size(201, 23);
            this.btnLoyProdMan.TabIndex = 10;
            this.btnLoyProdMan.Text = "Loyalty Product Management";
            this.btnLoyProdMan.UseVisualStyleBackColor = false;
            this.btnLoyProdMan.Click += new System.EventHandler(this.btnLoyProdMan_Click);
            // 
            // lblManagementTitle
            // 
            this.lblManagementTitle.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblManagementTitle.Location = new System.Drawing.Point(1, 150);
            this.lblManagementTitle.Name = "lblManagementTitle";
            this.lblManagementTitle.Size = new System.Drawing.Size(211, 23);
            this.lblManagementTitle.TabIndex = 4;
            this.lblManagementTitle.Text = "M A N A G E M E N T";
            this.lblManagementTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTerminalTitle
            // 
            this.lblTerminalTitle.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblTerminalTitle.Location = new System.Drawing.Point(1, 47);
            this.lblTerminalTitle.Name = "lblTerminalTitle";
            this.lblTerminalTitle.Size = new System.Drawing.Size(211, 23);
            this.lblTerminalTitle.TabIndex = 0;
            this.lblTerminalTitle.Text = "T E R M I N A L";
            this.lblTerminalTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNewTerminal
            // 
            this.btnNewTerminal.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnNewTerminal.FlatAppearance.BorderSize = 0;
            this.btnNewTerminal.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnNewTerminal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnNewTerminal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewTerminal.Image = ((System.Drawing.Image)(resources.GetObject("btnNewTerminal.Image")));
            this.btnNewTerminal.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnNewTerminal.Location = new System.Drawing.Point(6, 76);
            this.btnNewTerminal.Name = "btnNewTerminal";
            this.btnNewTerminal.Size = new System.Drawing.Size(201, 23);
            this.btnNewTerminal.TabIndex = 1;
            this.btnNewTerminal.Text = "         Create New";
            this.btnNewTerminal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNewTerminal.Click += new System.EventHandler(this.btnNewTerminal_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnRegister.FlatAppearance.BorderSize = 0;
            this.btnRegister.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnRegister.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegister.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegister.Image = ((System.Drawing.Image)(resources.GetObject("btnRegister.Image")));
            this.btnRegister.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegister.Location = new System.Drawing.Point(5, 552);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(201, 23);
            this.btnRegister.TabIndex = 14;
            this.btnRegister.Text = "         Register Profile";
            this.btnRegister.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // btnTenLast
            // 
            this.btnTenLast.FlatAppearance.BorderSize = 0;
            this.btnTenLast.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnTenLast.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnTenLast.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTenLast.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTenLast.Image = ((System.Drawing.Image)(resources.GetObject("btnTenLast.Image")));
            this.btnTenLast.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnTenLast.Location = new System.Drawing.Point(6, 98);
            this.btnTenLast.Name = "btnTenLast";
            this.btnTenLast.Size = new System.Drawing.Size(201, 23);
            this.btnTenLast.TabIndex = 2;
            this.btnTenLast.Text = "         View Last";
            this.btnTenLast.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTenLast.Click += new System.EventHandler(this.btnTenLast_Click);
            // 
            // btnCardManagement
            // 
            this.btnCardManagement.BackColor = System.Drawing.SystemColors.Control;
            this.btnCardManagement.FlatAppearance.BorderSize = 0;
            this.btnCardManagement.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnCardManagement.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnCardManagement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCardManagement.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCardManagement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCardManagement.Location = new System.Drawing.Point(6, 175);
            this.btnCardManagement.Name = "btnCardManagement";
            this.btnCardManagement.Size = new System.Drawing.Size(201, 23);
            this.btnCardManagement.TabIndex = 5;
            this.btnCardManagement.Text = "Card Management";
            this.btnCardManagement.UseVisualStyleBackColor = false;
            this.btnCardManagement.Click += new System.EventHandler(this.btnCardManagement_Click);
            // 
            // btnKeyMan
            // 
            this.btnKeyMan.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnKeyMan.FlatAppearance.BorderSize = 0;
            this.btnKeyMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnKeyMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnKeyMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKeyMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKeyMan.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnKeyMan.Location = new System.Drawing.Point(6, 225);
            this.btnKeyMan.Name = "btnKeyMan";
            this.btnKeyMan.Size = new System.Drawing.Size(201, 23);
            this.btnKeyMan.TabIndex = 7;
            this.btnKeyMan.Text = "Public Key Management";
            this.btnKeyMan.Click += new System.EventHandler(this.btnKeyMan_Click);
            // 
            // btnAIDMan
            // 
            this.btnAIDMan.FlatAppearance.BorderSize = 0;
            this.btnAIDMan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnAIDMan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnAIDMan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAIDMan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAIDMan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAIDMan.Location = new System.Drawing.Point(6, 200);
            this.btnAIDMan.Name = "btnAIDMan";
            this.btnAIDMan.Size = new System.Drawing.Size(201, 23);
            this.btnAIDMan.TabIndex = 6;
            this.btnAIDMan.Text = " AID Management";
            this.btnAIDMan.Click += new System.EventHandler(this.btnAIDMan_Click);
            // 
            // btnViewAll
            // 
            this.btnViewAll.FlatAppearance.BorderSize = 0;
            this.btnViewAll.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightSteelBlue;
            this.btnViewAll.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue;
            this.btnViewAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewAll.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewAll.Image = ((System.Drawing.Image)(resources.GetObject("btnViewAll.Image")));
            this.btnViewAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnViewAll.Location = new System.Drawing.Point(6, 120);
            this.btnViewAll.Name = "btnViewAll";
            this.btnViewAll.Size = new System.Drawing.Size(201, 23);
            this.btnViewAll.TabIndex = 3;
            this.btnViewAll.Text = "         View All ";
            this.btnViewAll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnViewAll.Click += new System.EventHandler(this.btnViewAll_Click);
            // 
            // tStripSearch
            // 
            this.tStripSearch.AllowMerge = false;
            this.tStripSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tStripSearch.AutoSize = false;
            this.tStripSearch.BackColor = System.Drawing.SystemColors.Control;
            this.tStripSearch.Dock = System.Windows.Forms.DockStyle.None;
            this.tStripSearch.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tStripSearch.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tStripTxtSearch,
            this.tStripBtnSearch});
            this.tStripSearch.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tStripSearch.Location = new System.Drawing.Point(836, 1);
            this.tStripSearch.Name = "tStripSearch";
            this.tStripSearch.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tStripSearch.Size = new System.Drawing.Size(246, 30);
            this.tStripSearch.TabIndex = 1;
            this.tStripSearch.Text = "toolStrip1";
            // 
            // tStripTxtSearch
            // 
            this.tStripTxtSearch.AutoSize = false;
            this.tStripTxtSearch.AutoToolTip = true;
            this.tStripTxtSearch.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.tStripTxtSearch.MaxLength = 8;
            this.tStripTxtSearch.Name = "tStripTxtSearch";
            this.tStripTxtSearch.Size = new System.Drawing.Size(200, 25);
            this.tStripTxtSearch.Text = "Search Terminal";
            this.tStripTxtSearch.ToolTipText = "Search Terminal";
            this.tStripTxtSearch.Enter += new System.EventHandler(this.tStripTxtSearch_Enter);
            this.tStripTxtSearch.Leave += new System.EventHandler(this.tStripTxtSearch_Leave);
            this.tStripTxtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tStripTxtSearch_KeyPress);
            // 
            // tStripBtnSearch
            // 
            this.tStripBtnSearch.AutoSize = false;
            this.tStripBtnSearch.AutoToolTip = false;
            this.tStripBtnSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tStripBtnSearch.BackgroundImage")));
            this.tStripBtnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tStripBtnSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tStripBtnSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tStripBtnSearch.Margin = new System.Windows.Forms.Padding(0);
            this.tStripBtnSearch.Name = "tStripBtnSearch";
            this.tStripBtnSearch.Size = new System.Drawing.Size(23, 25);
            this.tStripBtnSearch.Text = "toolStripButton1";
            this.tStripBtnSearch.Click += new System.EventHandler(this.tStripBtnSearch_Click);
            // 
            // gbView
            // 
            this.gbView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbView.Controls.Add(this.lblTagDetail);
            this.gbView.Controls.Add(this.lblDetail);
            this.gbView.Controls.Add(this.lblTerminal);
            this.gbView.Controls.Add(this.tvTerminal);
            this.gbView.Controls.Add(this.btnClose);
            this.gbView.Controls.Add(this.dgvTerminal);
            this.gbView.Controls.Add(this.dgvTagDetail);
            this.gbView.Location = new System.Drawing.Point(6, -1);
            this.gbView.Name = "gbView";
            this.gbView.Size = new System.Drawing.Size(827, 520);
            this.gbView.TabIndex = 0;
            this.gbView.TabStop = false;
            // 
            // lblTagDetail
            // 
            this.lblTagDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTagDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTagDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTagDetail.Location = new System.Drawing.Point(447, 14);
            this.lblTagDetail.Name = "lblTagDetail";
            this.lblTagDetail.Size = new System.Drawing.Size(373, 25);
            this.lblTagDetail.TabIndex = 2;
            this.lblTagDetail.Text = "TERMINAL DETAIL";
            this.lblTagDetail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDetail
            // 
            this.lblDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetail.Location = new System.Drawing.Point(197, 14);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Size = new System.Drawing.Size(250, 25);
            this.lblDetail.TabIndex = 1;
            this.lblDetail.Text = "TERMINAL TREE";
            this.lblDetail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTerminal
            // 
            this.lblTerminal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminal.Location = new System.Drawing.Point(7, 14);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(190, 25);
            this.lblTerminal.TabIndex = 0;
            this.lblTerminal.Text = "TERMINAL";
            this.lblTerminal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tvTerminal
            // 
            this.tvTerminal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tvTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvTerminal.Location = new System.Drawing.Point(197, 39);
            this.tvTerminal.Name = "tvTerminal";
            this.tvTerminal.Size = new System.Drawing.Size(250, 475);
            this.tvTerminal.TabIndex = 398;
            this.tvTerminal.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tvTerminal_BeforeLabelEdit);
            this.tvTerminal.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tvTerminal_AfterLabelEdit);
            this.tvTerminal.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvTerminal_AfterSelect);
            this.tvTerminal.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvTerminal_NodeMouseClick);
            this.tvTerminal.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvTerminal_NodeMouseDoubleClick);
            this.tvTerminal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tvTerminal_KeyPress);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(713, 22);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(10, 10);
            this.btnClose.TabIndex = 397;
            this.btnClose.TabStop = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgvTerminal
            // 
            this.dgvTerminal.AllowUserToAddRows = false;
            this.dgvTerminal.AllowUserToDeleteRows = false;
            this.dgvTerminal.AllowUserToResizeColumns = false;
            this.dgvTerminal.AllowUserToResizeRows = false;
            this.dgvTerminal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvTerminal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTerminal.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvTerminal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTerminal.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTerminal.ColumnHeadersVisible = false;
            this.dgvTerminal.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvTerminal.EnableHeadersVisualStyles = false;
            this.dgvTerminal.Location = new System.Drawing.Point(7, 39);
            this.dgvTerminal.MultiSelect = false;
            this.dgvTerminal.Name = "dgvTerminal";
            this.dgvTerminal.ReadOnly = true;
            this.dgvTerminal.RowHeadersVisible = false;
            this.dgvTerminal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvTerminal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvTerminal.Size = new System.Drawing.Size(190, 475);
            this.dgvTerminal.TabIndex = 397;
            this.dgvTerminal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvTerminal_KeyDown);
            this.dgvTerminal.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvTerminal_MouseClick);
            this.dgvTerminal.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgvTerminal_MouseUp);
            // 
            // dgvTagDetail
            // 
            this.dgvTagDetail.AllowUserToAddRows = false;
            this.dgvTagDetail.AllowUserToDeleteRows = false;
            this.dgvTagDetail.AllowUserToResizeRows = false;
            this.dgvTagDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTagDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvTagDetail.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvTagDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTagDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTagDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTagDetail.EnableHeadersVisualStyles = false;
            this.dgvTagDetail.Location = new System.Drawing.Point(447, 39);
            this.dgvTagDetail.MultiSelect = false;
            this.dgvTagDetail.Name = "dgvTagDetail";
            this.dgvTagDetail.ReadOnly = true;
            this.dgvTagDetail.RowHeadersVisible = false;
            this.dgvTagDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTagDetail.Size = new System.Drawing.Size(373, 475);
            this.dgvTagDetail.TabIndex = 399;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.ShowImageMargin = false;
            this.contextMenuStrip.Size = new System.Drawing.Size(36, 4);
            // 
            // gbEdcInf
            // 
            this.gbEdcInf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbEdcInf.Controls.Add(this.tabcntrInformation);
            this.gbEdcInf.Location = new System.Drawing.Point(6, 525);
            this.gbEdcInf.Name = "gbEdcInf";
            this.gbEdcInf.Size = new System.Drawing.Size(827, 184);
            this.gbEdcInf.TabIndex = 3;
            this.gbEdcInf.TabStop = false;
            this.gbEdcInf.Text = "EDC Information";
            // 
            // tabcntrInformation
            // 
            this.tabcntrInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabcntrInformation.Controls.Add(this.tabPageAutoInit);
            this.tabcntrInformation.Controls.Add(this.tabPageInit);
            this.tabcntrInformation.Location = new System.Drawing.Point(8, 19);
            this.tabcntrInformation.Name = "tabcntrInformation";
            this.tabcntrInformation.SelectedIndex = 0;
            this.tabcntrInformation.Size = new System.Drawing.Size(812, 150);
            this.tabcntrInformation.TabIndex = 22;
            // 
            // tabPageAutoInit
            // 
            this.tabPageAutoInit.Controls.Add(this.txtICCID);
            this.tabPageAutoInit.Controls.Add(this.txtMemUsage);
            this.tabPageAutoInit.Controls.Add(this.txtMCSN);
            this.tabPageAutoInit.Controls.Add(this.txtReaderSN);
            this.tabPageAutoInit.Controls.Add(this.txtPSAMSN);
            this.tabPageAutoInit.Controls.Add(this.txtEDCSN);
            this.tabPageAutoInit.Controls.Add(this.txtKernelVer);
            this.tabPageAutoInit.Controls.Add(this.txtPABX);
            this.tabPageAutoInit.Controls.Add(this.txtLastInit);
            this.tabPageAutoInit.Controls.Add(this.txtOSVer);
            this.tabPageAutoInit.Controls.Add(this.txtSoftwareVer);
            this.tabPageAutoInit.Controls.Add(this.txtTID);
            this.tabPageAutoInit.Controls.Add(this.lblMemUsage);
            this.tabPageAutoInit.Controls.Add(this.lblMCSN);
            this.tabPageAutoInit.Controls.Add(this.lblPSAMSN);
            this.tabPageAutoInit.Controls.Add(this.lblReaderSN);
            this.tabPageAutoInit.Controls.Add(this.lblEDCSN);
            this.tabPageAutoInit.Controls.Add(this.lblKernelVersion);
            this.tabPageAutoInit.Controls.Add(this.lblOSVer);
            this.tabPageAutoInit.Controls.Add(this.lblSoftVer);
            this.tabPageAutoInit.Controls.Add(this.lblPABX);
            this.tabPageAutoInit.Controls.Add(this.lblLastInitTime);
            this.tabPageAutoInit.Controls.Add(this.lblTID);
            this.tabPageAutoInit.Controls.Add(this.lblICCID);
            this.tabPageAutoInit.Location = new System.Drawing.Point(4, 22);
            this.tabPageAutoInit.Name = "tabPageAutoInit";
            this.tabPageAutoInit.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAutoInit.Size = new System.Drawing.Size(804, 124);
            this.tabPageAutoInit.TabIndex = 0;
            this.tabPageAutoInit.Tag = "autoinit";
            this.tabPageAutoInit.Text = "Initialize";
            this.tabPageAutoInit.UseVisualStyleBackColor = true;
            // 
            // txtICCID
            // 
            this.txtICCID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtICCID.Location = new System.Drawing.Point(653, 84);
            this.txtICCID.Name = "txtICCID";
            this.txtICCID.ReadOnly = true;
            this.txtICCID.Size = new System.Drawing.Size(147, 20);
            this.txtICCID.TabIndex = 45;
            // 
            // txtMemUsage
            // 
            this.txtMemUsage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMemUsage.Location = new System.Drawing.Point(653, 58);
            this.txtMemUsage.Name = "txtMemUsage";
            this.txtMemUsage.ReadOnly = true;
            this.txtMemUsage.Size = new System.Drawing.Size(147, 20);
            this.txtMemUsage.TabIndex = 43;
            // 
            // txtMCSN
            // 
            this.txtMCSN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMCSN.Location = new System.Drawing.Point(653, 32);
            this.txtMCSN.Name = "txtMCSN";
            this.txtMCSN.ReadOnly = true;
            this.txtMCSN.Size = new System.Drawing.Size(147, 20);
            this.txtMCSN.TabIndex = 41;
            // 
            // txtReaderSN
            // 
            this.txtReaderSN.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtReaderSN.Location = new System.Drawing.Point(357, 84);
            this.txtReaderSN.Name = "txtReaderSN";
            this.txtReaderSN.ReadOnly = true;
            this.txtReaderSN.Size = new System.Drawing.Size(131, 20);
            this.txtReaderSN.TabIndex = 37;
            // 
            // txtPSAMSN
            // 
            this.txtPSAMSN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPSAMSN.Location = new System.Drawing.Point(653, 6);
            this.txtPSAMSN.Name = "txtPSAMSN";
            this.txtPSAMSN.ReadOnly = true;
            this.txtPSAMSN.Size = new System.Drawing.Size(147, 20);
            this.txtPSAMSN.TabIndex = 39;
            // 
            // txtEDCSN
            // 
            this.txtEDCSN.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtEDCSN.Location = new System.Drawing.Point(357, 58);
            this.txtEDCSN.Name = "txtEDCSN";
            this.txtEDCSN.ReadOnly = true;
            this.txtEDCSN.Size = new System.Drawing.Size(131, 20);
            this.txtEDCSN.TabIndex = 35;
            // 
            // txtKernelVer
            // 
            this.txtKernelVer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtKernelVer.Location = new System.Drawing.Point(357, 33);
            this.txtKernelVer.Name = "txtKernelVer";
            this.txtKernelVer.ReadOnly = true;
            this.txtKernelVer.Size = new System.Drawing.Size(131, 20);
            this.txtKernelVer.TabIndex = 33;
            // 
            // txtPABX
            // 
            this.txtPABX.Location = new System.Drawing.Point(97, 58);
            this.txtPABX.Name = "txtPABX";
            this.txtPABX.ReadOnly = true;
            this.txtPABX.Size = new System.Drawing.Size(131, 20);
            this.txtPABX.TabIndex = 27;
            // 
            // txtLastInit
            // 
            this.txtLastInit.Location = new System.Drawing.Point(97, 32);
            this.txtLastInit.Name = "txtLastInit";
            this.txtLastInit.ReadOnly = true;
            this.txtLastInit.Size = new System.Drawing.Size(131, 20);
            this.txtLastInit.TabIndex = 25;
            // 
            // txtOSVer
            // 
            this.txtOSVer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtOSVer.Location = new System.Drawing.Point(357, 6);
            this.txtOSVer.Name = "txtOSVer";
            this.txtOSVer.ReadOnly = true;
            this.txtOSVer.Size = new System.Drawing.Size(131, 20);
            this.txtOSVer.TabIndex = 31;
            // 
            // txtSoftwareVer
            // 
            this.txtSoftwareVer.Location = new System.Drawing.Point(97, 84);
            this.txtSoftwareVer.Name = "txtSoftwareVer";
            this.txtSoftwareVer.ReadOnly = true;
            this.txtSoftwareVer.Size = new System.Drawing.Size(131, 20);
            this.txtSoftwareVer.TabIndex = 29;
            // 
            // txtTID
            // 
            this.txtTID.Location = new System.Drawing.Point(97, 6);
            this.txtTID.Name = "txtTID";
            this.txtTID.ReadOnly = true;
            this.txtTID.Size = new System.Drawing.Size(131, 20);
            this.txtTID.TabIndex = 23;
            // 
            // lblMemUsage
            // 
            this.lblMemUsage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMemUsage.AutoSize = true;
            this.lblMemUsage.Location = new System.Drawing.Point(519, 61);
            this.lblMemUsage.Name = "lblMemUsage";
            this.lblMemUsage.Size = new System.Drawing.Size(81, 13);
            this.lblMemUsage.TabIndex = 42;
            this.lblMemUsage.Text = "Memory Usage ";
            // 
            // lblMCSN
            // 
            this.lblMCSN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMCSN.AutoSize = true;
            this.lblMCSN.Location = new System.Drawing.Point(519, 35);
            this.lblMCSN.Name = "lblMCSN";
            this.lblMCSN.Size = new System.Drawing.Size(123, 13);
            this.lblMCSN.TabIndex = 40;
            this.lblMCSN.Text = "Merchant Card Serial No";
            // 
            // lblPSAMSN
            // 
            this.lblPSAMSN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPSAMSN.AutoSize = true;
            this.lblPSAMSN.Location = new System.Drawing.Point(519, 9);
            this.lblPSAMSN.Name = "lblPSAMSN";
            this.lblPSAMSN.Size = new System.Drawing.Size(83, 13);
            this.lblPSAMSN.TabIndex = 38;
            this.lblPSAMSN.Text = "PSAM Serial No";
            // 
            // lblReaderSN
            // 
            this.lblReaderSN.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblReaderSN.AutoSize = true;
            this.lblReaderSN.Location = new System.Drawing.Point(261, 87);
            this.lblReaderSN.Name = "lblReaderSN";
            this.lblReaderSN.Size = new System.Drawing.Size(88, 13);
            this.lblReaderSN.TabIndex = 36;
            this.lblReaderSN.Text = "Reader Serial No";
            // 
            // lblEDCSN
            // 
            this.lblEDCSN.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblEDCSN.AutoSize = true;
            this.lblEDCSN.Location = new System.Drawing.Point(261, 61);
            this.lblEDCSN.Name = "lblEDCSN";
            this.lblEDCSN.Size = new System.Drawing.Size(75, 13);
            this.lblEDCSN.TabIndex = 34;
            this.lblEDCSN.Text = "EDC Serial No";
            // 
            // lblKernelVersion
            // 
            this.lblKernelVersion.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblKernelVersion.AutoSize = true;
            this.lblKernelVersion.Location = new System.Drawing.Point(261, 35);
            this.lblKernelVersion.Name = "lblKernelVersion";
            this.lblKernelVersion.Size = new System.Drawing.Size(75, 13);
            this.lblKernelVersion.TabIndex = 32;
            this.lblKernelVersion.Text = "Kernel Version";
            // 
            // lblOSVer
            // 
            this.lblOSVer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblOSVer.AutoSize = true;
            this.lblOSVer.Location = new System.Drawing.Point(261, 9);
            this.lblOSVer.Name = "lblOSVer";
            this.lblOSVer.Size = new System.Drawing.Size(60, 13);
            this.lblOSVer.TabIndex = 30;
            this.lblOSVer.Text = "OS Version";
            // 
            // lblSoftVer
            // 
            this.lblSoftVer.AutoSize = true;
            this.lblSoftVer.Location = new System.Drawing.Point(1, 87);
            this.lblSoftVer.Name = "lblSoftVer";
            this.lblSoftVer.Size = new System.Drawing.Size(87, 13);
            this.lblSoftVer.TabIndex = 28;
            this.lblSoftVer.Text = "Software Version";
            // 
            // lblPABX
            // 
            this.lblPABX.AutoSize = true;
            this.lblPABX.Location = new System.Drawing.Point(4, 61);
            this.lblPABX.Name = "lblPABX";
            this.lblPABX.Size = new System.Drawing.Size(35, 13);
            this.lblPABX.TabIndex = 26;
            this.lblPABX.Text = "PABX";
            // 
            // lblLastInitTime
            // 
            this.lblLastInitTime.AutoSize = true;
            this.lblLastInitTime.Location = new System.Drawing.Point(4, 35);
            this.lblLastInitTime.Name = "lblLastInitTime";
            this.lblLastInitTime.Size = new System.Drawing.Size(70, 13);
            this.lblLastInitTime.TabIndex = 24;
            this.lblLastInitTime.Text = "Last Init Time";
            // 
            // lblTID
            // 
            this.lblTID.AutoSize = true;
            this.lblTID.Location = new System.Drawing.Point(4, 9);
            this.lblTID.Name = "lblTID";
            this.lblTID.Size = new System.Drawing.Size(61, 13);
            this.lblTID.TabIndex = 22;
            this.lblTID.Text = "Terminal ID";
            // 
            // lblICCID
            // 
            this.lblICCID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblICCID.AutoSize = true;
            this.lblICCID.Location = new System.Drawing.Point(519, 87);
            this.lblICCID.Name = "lblICCID";
            this.lblICCID.Size = new System.Drawing.Size(35, 13);
            this.lblICCID.TabIndex = 44;
            this.lblICCID.Text = "ICCID";
            // 
            // tabPageInit
            // 
            this.tabPageInit.Controls.Add(this.txtInitSoftwareVer);
            this.tabPageInit.Controls.Add(this.lblSoftwareVersion);
            this.tabPageInit.Controls.Add(this.txtTIDInit);
            this.tabPageInit.Controls.Add(this.lblTIDInit);
            this.tabPageInit.Controls.Add(this.txtEdcSNInit);
            this.tabPageInit.Controls.Add(this.lblEDCSNnInit);
            this.tabPageInit.Location = new System.Drawing.Point(4, 22);
            this.tabPageInit.Name = "tabPageInit";
            this.tabPageInit.Size = new System.Drawing.Size(804, 124);
            this.tabPageInit.TabIndex = 1;
            this.tabPageInit.Text = "Initialize Trail";
            this.tabPageInit.UseVisualStyleBackColor = true;
            // 
            // txtInitSoftwareVer
            // 
            this.txtInitSoftwareVer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInitSoftwareVer.Location = new System.Drawing.Point(616, 6);
            this.txtInitSoftwareVer.Name = "txtInitSoftwareVer";
            this.txtInitSoftwareVer.ReadOnly = true;
            this.txtInitSoftwareVer.Size = new System.Drawing.Size(131, 20);
            this.txtInitSoftwareVer.TabIndex = 51;
            // 
            // lblSoftwareVersion
            // 
            this.lblSoftwareVersion.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblSoftwareVersion.AutoSize = true;
            this.lblSoftwareVersion.Location = new System.Drawing.Point(520, 9);
            this.lblSoftwareVersion.Name = "lblSoftwareVersion";
            this.lblSoftwareVersion.Size = new System.Drawing.Size(84, 13);
            this.lblSoftwareVersion.TabIndex = 50;
            this.lblSoftwareVersion.Text = "SoftwareVersion";
            // 
            // txtTIDInit
            // 
            this.txtTIDInit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtTIDInit.Location = new System.Drawing.Point(104, 6);
            this.txtTIDInit.Name = "txtTIDInit";
            this.txtTIDInit.ReadOnly = true;
            this.txtTIDInit.Size = new System.Drawing.Size(131, 20);
            this.txtTIDInit.TabIndex = 49;
            // 
            // lblTIDInit
            // 
            this.lblTIDInit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTIDInit.AutoSize = true;
            this.lblTIDInit.Location = new System.Drawing.Point(8, 9);
            this.lblTIDInit.Name = "lblTIDInit";
            this.lblTIDInit.Size = new System.Drawing.Size(58, 13);
            this.lblTIDInit.TabIndex = 48;
            this.lblTIDInit.Text = "TerminalID";
            // 
            // txtEdcSNInit
            // 
            this.txtEdcSNInit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtEdcSNInit.Location = new System.Drawing.Point(359, 6);
            this.txtEdcSNInit.Name = "txtEdcSNInit";
            this.txtEdcSNInit.ReadOnly = true;
            this.txtEdcSNInit.Size = new System.Drawing.Size(131, 20);
            this.txtEdcSNInit.TabIndex = 47;
            // 
            // lblEDCSNnInit
            // 
            this.lblEDCSNnInit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblEDCSNnInit.AutoSize = true;
            this.lblEDCSNnInit.Location = new System.Drawing.Point(263, 9);
            this.lblEDCSNnInit.Name = "lblEDCSNnInit";
            this.lblEDCSNnInit.Size = new System.Drawing.Size(75, 13);
            this.lblEDCSNnInit.TabIndex = 46;
            this.lblEDCSNnInit.Text = "EDC Serial No";
            // 
            // lvAppEDC
            // 
            this.lvAppEDC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAppEDC.Location = new System.Drawing.Point(87, 9);
            this.lvAppEDC.Name = "lvAppEDC";
            this.lvAppEDC.Size = new System.Drawing.Size(572, 97);
            this.lvAppEDC.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvAppEDC.TabIndex = 1;
            this.lvAppEDC.UseCompatibleStateImageBehavior = false;
            this.lvAppEDC.View = System.Windows.Forms.View.List;
            // 
            // btnRemovePackage
            // 
            this.btnRemovePackage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemovePackage.Location = new System.Drawing.Point(665, 39);
            this.btnRemovePackage.Name = "btnRemovePackage";
            this.btnRemovePackage.Size = new System.Drawing.Size(147, 23);
            this.btnRemovePackage.TabIndex = 3;
            this.btnRemovePackage.Text = "Remove";
            this.btnRemovePackage.UseVisualStyleBackColor = true;
            this.btnRemovePackage.Click += new System.EventHandler(this.btnRemovePackage_Click);
            // 
            // btnAddPackage
            // 
            this.btnAddPackage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddPackage.Location = new System.Drawing.Point(665, 9);
            this.btnAddPackage.Name = "btnAddPackage";
            this.btnAddPackage.Size = new System.Drawing.Size(147, 23);
            this.btnAddPackage.TabIndex = 2;
            this.btnAddPackage.Text = "Add";
            this.btnAddPackage.UseVisualStyleBackColor = true;
            this.btnAddPackage.Click += new System.EventHandler(this.btnAddPackage_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            // 
            // FrmExpand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(1064, 716);
            this.Controls.Add(this.tStripSearch);
            this.Controls.Add(this.gbView);
            this.Controls.Add(this.gbEdcInf);
            this.Controls.Add(this.gbButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmExpand";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Terminal";
            this.Activated += new System.EventHandler(this.FrmExpand_Activated);
            this.gbButton.ResumeLayout(false);
            this.tStripSearch.ResumeLayout(false);
            this.tStripSearch.PerformLayout();
            this.gbView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTagDetail)).EndInit();
            this.gbEdcInf.ResumeLayout(false);
            this.tabcntrInformation.ResumeLayout(false);
            this.tabPageAutoInit.ResumeLayout(false);
            this.tabPageAutoInit.PerformLayout();
            this.tabPageInit.ResumeLayout(false);
            this.tabPageInit.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ImageList ButtonIcon;
        internal System.Windows.Forms.ImageList IconList;
        internal System.Windows.Forms.Timer RefreshTime;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnNewTerminal;
        internal System.Windows.Forms.Button btnRegister;
        internal System.Windows.Forms.Button btnTenLast;
        internal System.Windows.Forms.Button btnCardManagement;
        internal System.Windows.Forms.Button btnKeyMan;
        internal System.Windows.Forms.Button btnAIDMan;
        internal System.Windows.Forms.Button btnViewAll;
        private System.Windows.Forms.ToolStripTextBox tStripTxtSearch;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.ToolStripButton tStripBtnSearch;
        private System.Windows.Forms.ToolStrip tStripSearch;
        private System.Windows.Forms.GroupBox gbView;
        internal System.Windows.Forms.Label lblTagDetail;
        internal System.Windows.Forms.Label lblTerminal;
        internal System.Windows.Forms.Label lblDetail;
        internal System.Windows.Forms.TreeView tvTerminal;
        private System.Windows.Forms.Label lblTerminalTitle;
        private System.Windows.Forms.Label lblManagementTitle;
        private System.Windows.Forms.Label lblOther;
        internal System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        internal System.Windows.Forms.Button btnFindSN;
        internal System.Windows.Forms.Button btnSNReg;
        internal System.Windows.Forms.Button btnTMK;
        private System.Windows.Forms.GroupBox gbEdcInf;
        internal System.Windows.Forms.Button btnTLEMan;
        private System.Windows.Forms.TabControl tabcntrInformation;
        private System.Windows.Forms.TabPage tabPageAutoInit;
        private System.Windows.Forms.TextBox txtMemUsage;
        private System.Windows.Forms.TextBox txtMCSN;
        private System.Windows.Forms.TextBox txtReaderSN;
        private System.Windows.Forms.TextBox txtPSAMSN;
        private System.Windows.Forms.TextBox txtEDCSN;
        private System.Windows.Forms.TextBox txtKernelVer;
        private System.Windows.Forms.TextBox txtPABX;
        private System.Windows.Forms.TextBox txtLastInit;
        private System.Windows.Forms.TextBox txtOSVer;
        private System.Windows.Forms.TextBox txtSoftwareVer;
        private System.Windows.Forms.TextBox txtTID;
        private System.Windows.Forms.Label lblMemUsage;
        private System.Windows.Forms.Label lblMCSN;
        private System.Windows.Forms.Label lblPSAMSN;
        private System.Windows.Forms.Label lblReaderSN;
        private System.Windows.Forms.Label lblEDCSN;
        private System.Windows.Forms.Label lblKernelVersion;
        private System.Windows.Forms.Label lblOSVer;
        private System.Windows.Forms.Label lblSoftVer;
        private System.Windows.Forms.Label lblPABX;
        private System.Windows.Forms.Label lblLastInitTime;
        private System.Windows.Forms.Label lblTID;
        internal System.Windows.Forms.Button btnLoyProdMan;
        internal System.Windows.Forms.Button btnLoyMan;
        internal System.Windows.Forms.Button btnGPRSMan;
        internal System.Windows.Forms.Button btnCurrMan;
        private System.Windows.Forms.ListView lvAppEDC;
        private System.Windows.Forms.Button btnRemovePackage;
        private System.Windows.Forms.Button btnAddPackage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtICCID;
        private System.Windows.Forms.Label lblICCID;
        internal System.Windows.Forms.Button btnPinPadKeyMan;
        private System.Windows.Forms.TabPage tabPageInit;
        private System.Windows.Forms.TextBox txtEdcSNInit;
        private System.Windows.Forms.Label lblEDCSNnInit;
        private System.Windows.Forms.TextBox txtTIDInit;
        private System.Windows.Forms.Label lblTIDInit;
        private System.Windows.Forms.TextBox txtInitSoftwareVer;
        private System.Windows.Forms.Label lblSoftwareVersion;
        internal System.Windows.Forms.Button btnProdCodeMan;
        internal System.Windows.Forms.Button btnBankCodeMan;
        internal System.Windows.Forms.Button btnRDMan;
        internal System.Windows.Forms.Button btnEMVManagement;
        public System.Windows.Forms.DataGridView dgvTerminal;
        public System.Windows.Forms.DataGridView dgvTagDetail;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnAdvanceSearch;
    }
}