using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

using InSysClass;

namespace InSys
{
    public partial class FrmEditAID : Form
    {
        /// <summary>
        /// Determines if current mode is Editing or Adding
        /// </summary>
        protected bool IsEdit;
        protected SqlConnection oSqlConn;

        protected string sDbId;
        protected string sDbName;

        protected string sAIDName;
        protected string sAIDVersionList;
        protected string sAIDPrintedName;
        protected int iAIDCriteria;

        protected const string AIDName = "AI01";
        protected const string AIDVersionList = "AI02";
        protected const string AIDPrintedName = "AI03";
        protected const string AIDCriteria = "AI04";

        /// <summary>
        /// Form to add or edit AID Data
        /// </summary>
        /// <param name="_isEdit">boolean : true if edit mode, false if add mode</param>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sDbId">string : DatabaseID</param>
        public FrmEditAID(bool _isEdit, SqlConnection _oSqlConn, string _sDbId, string _sDbName) : this(_isEdit, _oSqlConn, _sDbId, _sDbName, "") { }
        public FrmEditAID(bool _isEdit, SqlConnection _oSqlConn, string _sDbId, string _sDbName , string _sAIDName)
        {
            InitializeComponent();
            IsEdit = _isEdit;
            oSqlConn = _oSqlConn;
            sDbId = _sDbId;
            sDbName = _sDbName;
            sAIDName = _sAIDName;
        }

        /// <summary>
        /// Call funciton SetDisplay.
        /// </summary>
        private void FrmEditAID_Load(object sender, EventArgs e)
        {
            InitAID();
            SetDisplay();
            string sLog = CommonMessage.sFormOpened + this.Text;
            if (IsEdit)
                sLog += " : " + sGetAIDListName;

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, sLog, "");
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            //CommonClass.inputLog(oSqlConn, "", "", "", CommonMessage.sUpdateAID, "");
            if (IsValid())
            {
                if (IsEdit)
                    UpdateAID(sGenAIDContent);
                else
                    SaveAID(sGenAIDContent);
                this.Close();
            }
        }

        /// <summary>
        /// Close the form.
        /// </summary>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region "Properties"
        /// <summary>
        /// Get AID List Value
        /// </summary>
        protected string sGetAIDListName
        {
            get
            {
                return txtAIDList.Text;
            }
        }
        
        /// <summary>
        /// AID Version List value
        /// </summary>
        protected string sGetAIDVersionList
        {
            get
            {
                return txtVersionList.Text;
            }
        }
        
        /// <summary>
        /// AID Printed Name value
        /// </summary>
        protected string sGetAIDPrintedName
        {
            get
            {
                return txtAIDPrintedName.Text;
            }
        }
        
        /// <summary>
        /// AID Criteria value
        /// </summary>
        protected string sGetAIDCriteria
        {
            get
            {
                return cmbMatchCriteria.SelectedIndex.ToString();
            }
        }
        
        /// <summary>
        /// AID Content value
        /// </summary>
        protected string sGenAIDContent
        {
            get
            {
                return sGenTagAIDList()
                    + sGetTagAIDVersionList()
                    + sGenTagAIDPrintedName()
                    + sGenTagAIDCriteria();
            }
        }

        /// <summary>
        /// Generate AID List TLV
        /// </summary>
        /// <returns>string :TLV AID List</returns>
        protected string sGenTagAIDList()
        {
            return string.Format("{0}{1:00}{2}", AIDName, sGetAIDListName.Length, sGetAIDListName);
        }

        /// <summary>
        /// Generate AID Version TLV 
        /// </summary>
        /// <returns>string : AID Version TLV</returns>
        protected string sGetTagAIDVersionList()
        {
            return string.Format("{0}{1:00}{2}", AIDVersionList, sGetAIDVersionList.Length, sGetAIDVersionList);
        }

        /// <summary>
        /// Generate AID Printed Name TLV value
        /// </summary>
        /// <returns>string : Aid Printed TLV</returns>
        protected string sGenTagAIDPrintedName()
        {
            return string.Format("{0}{1:00}{2}", AIDPrintedName, sGetAIDPrintedName.Length, sGetAIDPrintedName);
        }

        /// <summary>
        /// Generate AID Criteria TLV value
        /// </summary>
        /// <returns>string : AID Criteria</returns>
        protected string sGenTagAIDCriteria()
        {
            return string.Format("{0}01{1}", AIDCriteria, sGetAIDCriteria);
        }
        #endregion

        #region "Function"
        /// <summary>
        /// Set Form's display.
        /// </summary>
        public void SetDisplay()
        {
            if (!IsEdit)
                this.Text = "AID Management - Add";
            else
                this.Text = "AID Management - Edit";
            txtAIDList.Enabled = txtAIDPrintedName.Enabled =
                txtVersionList.Enabled = cmbMatchCriteria.Enabled =
                btnSave.Visible = UserPrivilege.IsAllowed(PrivilegeCode.AI, Privilege.Edit);

            txtAIDList.Text = sAIDName;
            txtAIDPrintedName.Text = sAIDPrintedName;
            txtVersionList.Text = sAIDVersionList;
            cmbMatchCriteria.SelectedIndex = iAIDCriteria;
            txtAIDList.ReadOnly = IsEdit;
        }

        /// <summary>
        /// Generate WHERE Condition to filter data from database
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sCondition()
        {
            return "WHERE DatabaseId = " + sDbId + " "
                + "AND AIDName = '" + sAIDName + "'";
        }

        /// <summary>
        /// Assign value from database to form
        /// </summary>
        protected void InitAID()
        {
            DataTable dtAID = dtGetAID();
            foreach (DataRow drow in dtAID.Rows)
            {
                switch (drow["AIDTag"].ToString())
                {
                    case AIDName :
                        sAIDName = drow["AIDTagValue"].ToString();
                        break;
                    case AIDVersionList:
                        sAIDVersionList = drow["AIDTagValue"].ToString();
                        break;
                    case AIDPrintedName:
                        sAIDPrintedName = drow["AIDTagValue"].ToString();
                        break;
                    case AIDCriteria:
                        iAIDCriteria = int.Parse(drow["AIDTagValue"].ToString());
                        break;
                }
            }
        }

        /// <summary>
        /// Load data AID from database and store it in DataTable
        /// </summary>
        /// <returns>DataTable : AID Data from database</returns>
        protected DataTable dtGetAID()
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileAIDBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sCondition();
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            oCmd.Dispose();
            return dtTemp;
        }

        /// <summary>
        /// Determine whether data entered valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValid()
        {
            if (!string.IsNullOrEmpty(txtAIDList.Text) &&
                !string.IsNullOrEmpty(txtVersionList.Text) &&
                !string.IsNullOrEmpty(txtAIDPrintedName.Text))
                if (!IsEdit)
                    if (IsValidAIDList())
                    {
                        MessageBox.Show(CommonMessage.sErrAIDInvalid);
                        return false;
                    }
                    else
                        return true;
                else
                    return true;
            else
            {
                MessageBox.Show(CommonMessage.sErrAIDEmptyField);
                return false;
            }
        }

        /// <summary>
        /// Determine whether data found and can be shown in form
        /// </summary>
        /// <returns>boolean : true if data found</returns>
        protected bool IsValidAIDList()
        {
            sAIDName = sGetAIDListName;
            DataTable dtTemp = dtGetAID();
            return dtTemp.Rows.Count > 0 ? true : false;
        }

        /// <summary>
        /// Add new AID data to database
        /// </summary>
        /// <param name="sContent">string : AID data content</param>
        protected void SaveAID(string sContent)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileAIDInsert, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDbId;
            oCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
            oCmd.ExecuteNonQuery();
            oCmd.Dispose();

            string sLogDetail = "";

            sLogDetail += string.Format("{0} : {1} \n", "AID List", sGetAIDListName);
            sLogDetail += string.Format("{0} : {1} \n", "Version List", sGetAIDVersionList);
            sLogDetail += string.Format("{0} : {1} \n", "AID Printed Name", sGetAIDPrintedName);
            sLogDetail += string.Format("{0} : {1} \n", "Matching Criteria", sGetAIDCriteria);

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Add AID : " + sGetAIDListName, sLogDetail);
        }

        /// <summary>
        /// Modifying AID data in database based on given value
        /// </summary>
        /// <param name="sContent">string : AID Data content which will be sent to database</param>
        protected void UpdateAID(string sContent)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileAIDUpdate, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDbId;
            oCmd.Parameters.Add("@sAIDName", SqlDbType.VarChar).Value = sGetAIDListName;
            oCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
            //oCmd.ExecuteNonQuery();

            string sLogDetail = "";
            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                while (oRead.Read())
                {
                    sLogDetail += string.Format("{0} : {1} --> {2} \n", oRead["TagName"].ToString(),
                        oRead["OldAIDValue"].ToString(), oRead["NewAIDValue"].ToString());
                                    
                }
                oRead.Close();
            }
            oCmd.Dispose();

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Update AID : " + sAIDName, sLogDetail);
        }
        #endregion
    }
}