namespace InSys
{
    partial class FrmSoftwarePackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSoftwarePackage));
            this.dgAppPackage = new System.Windows.Forms.DataGridView();
            this.cmsAppPackage = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbAppPackageView = new System.Windows.Forms.GroupBox();
            this.gbSetting = new System.Windows.Forms.GroupBox();
            this.txtBuildNumber = new System.Windows.Forms.TextBox();
            this.lblBuildNumber = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbDatabase = new System.Windows.Forms.ComboBox();
            this.txtPackageName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.chkListType = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtAppPackageDesc = new System.Windows.Forms.TextBox();
            this.txtAppPackageFilename = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgAppPackage)).BeginInit();
            this.cmsAppPackage.SuspendLayout();
            this.gbAppPackageView.SuspendLayout();
            this.gbSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgAppPackage
            // 
            this.dgAppPackage.AllowUserToAddRows = false;
            this.dgAppPackage.AllowUserToDeleteRows = false;
            this.dgAppPackage.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgAppPackage.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgAppPackage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAppPackage.ContextMenuStrip = this.cmsAppPackage;
            this.dgAppPackage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgAppPackage.Location = new System.Drawing.Point(3, 16);
            this.dgAppPackage.MultiSelect = false;
            this.dgAppPackage.Name = "dgAppPackage";
            this.dgAppPackage.ReadOnly = true;
            this.dgAppPackage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAppPackage.Size = new System.Drawing.Size(784, 235);
            this.dgAppPackage.TabIndex = 0;
            // 
            // cmsAppPackage
            // 
            this.cmsAppPackage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.cmsAppPackage.Name = "cmsAppPackage";
            this.cmsAppPackage.Size = new System.Drawing.Size(108, 70);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // gbAppPackageView
            // 
            this.gbAppPackageView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbAppPackageView.Controls.Add(this.dgAppPackage);
            this.gbAppPackageView.Location = new System.Drawing.Point(12, 12);
            this.gbAppPackageView.Name = "gbAppPackageView";
            this.gbAppPackageView.Size = new System.Drawing.Size(790, 254);
            this.gbAppPackageView.TabIndex = 1;
            this.gbAppPackageView.TabStop = false;
            // 
            // gbSetting
            // 
            this.gbSetting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSetting.Controls.Add(this.txtBuildNumber);
            this.gbSetting.Controls.Add(this.lblBuildNumber);
            this.gbSetting.Controls.Add(this.label5);
            this.gbSetting.Controls.Add(this.cmbDatabase);
            this.gbSetting.Controls.Add(this.txtPackageName);
            this.gbSetting.Controls.Add(this.label4);
            this.gbSetting.Controls.Add(this.btnCancel);
            this.gbSetting.Controls.Add(this.btnAdd);
            this.gbSetting.Controls.Add(this.chkListType);
            this.gbSetting.Controls.Add(this.label3);
            this.gbSetting.Controls.Add(this.btnBrowse);
            this.gbSetting.Controls.Add(this.txtAppPackageDesc);
            this.gbSetting.Controls.Add(this.txtAppPackageFilename);
            this.gbSetting.Controls.Add(this.label2);
            this.gbSetting.Controls.Add(this.label1);
            this.gbSetting.Location = new System.Drawing.Point(12, 272);
            this.gbSetting.Name = "gbSetting";
            this.gbSetting.Size = new System.Drawing.Size(790, 183);
            this.gbSetting.TabIndex = 2;
            this.gbSetting.TabStop = false;
            // 
            // txtBuildNumber
            // 
            this.txtBuildNumber.Location = new System.Drawing.Point(112, 42);
            this.txtBuildNumber.MaxLength = 4;
            this.txtBuildNumber.Name = "txtBuildNumber";
            this.txtBuildNumber.Size = new System.Drawing.Size(52, 20);
            this.txtBuildNumber.TabIndex = 4;
            this.txtBuildNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBuildNumber_KeyPress);
            // 
            // lblBuildNumber
            // 
            this.lblBuildNumber.AutoSize = true;
            this.lblBuildNumber.Location = new System.Drawing.Point(6, 45);
            this.lblBuildNumber.Name = "lblBuildNumber";
            this.lblBuildNumber.Size = new System.Drawing.Size(70, 13);
            this.lblBuildNumber.TabIndex = 13;
            this.lblBuildNumber.Text = "Build Number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Database Name";
            this.label5.Visible = false;
            // 
            // cmbDatabase
            // 
            this.cmbDatabase.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDatabase.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDatabase.FormattingEnabled = true;
            this.cmbDatabase.Location = new System.Drawing.Point(112, 143);
            this.cmbDatabase.Name = "cmbDatabase";
            this.cmbDatabase.Size = new System.Drawing.Size(242, 21);
            this.cmbDatabase.TabIndex = 11;
            this.cmbDatabase.Visible = false;
            // 
            // txtPackageName
            // 
            this.txtPackageName.Location = new System.Drawing.Point(112, 70);
            this.txtPackageName.MaxLength = 12;
            this.txtPackageName.Name = "txtPackageName";
            this.txtPackageName.Size = new System.Drawing.Size(242, 20);
            this.txtPackageName.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Software Name";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(507, 154);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(426, 154);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Save";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // chkListType
            // 
            this.chkListType.FormattingEnabled = true;
            this.chkListType.Location = new System.Drawing.Point(507, 13);
            this.chkListType.Name = "chkListType";
            this.chkListType.Size = new System.Drawing.Size(168, 154);
            this.chkListType.TabIndex = 7;
            this.chkListType.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(470, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Type";
            this.label3.Visible = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(373, 11);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 3;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtAppPackageDesc
            // 
            this.txtAppPackageDesc.Location = new System.Drawing.Point(112, 97);
            this.txtAppPackageDesc.Multiline = true;
            this.txtAppPackageDesc.Name = "txtAppPackageDesc";
            this.txtAppPackageDesc.Size = new System.Drawing.Size(242, 67);
            this.txtAppPackageDesc.TabIndex = 6;
            // 
            // txtAppPackageFilename
            // 
            this.txtAppPackageFilename.Location = new System.Drawing.Point(112, 13);
            this.txtAppPackageFilename.Name = "txtAppPackageFilename";
            this.txtAppPackageFilename.ReadOnly = true;
            this.txtAppPackageFilename.Size = new System.Drawing.Size(242, 20);
            this.txtAppPackageFilename.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Description";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Package Filename";
            // 
            // FrmSoftwarePackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 467);
            this.Controls.Add(this.gbSetting);
            this.Controls.Add(this.gbAppPackageView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmSoftwarePackage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Software Package";
            this.Load += new System.EventHandler(this.FrmSoftwarePackage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgAppPackage)).EndInit();
            this.cmsAppPackage.ResumeLayout(false);
            this.gbAppPackageView.ResumeLayout(false);
            this.gbSetting.ResumeLayout(false);
            this.gbSetting.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgAppPackage;
        private System.Windows.Forms.GroupBox gbAppPackageView;
        private System.Windows.Forms.GroupBox gbSetting;
        private System.Windows.Forms.TextBox txtAppPackageFilename;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtAppPackageDesc;
        private System.Windows.Forms.CheckedListBox chkListType;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ContextMenuStrip cmsAppPackage;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtPackageName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbDatabase;
        private System.Windows.Forms.TextBox txtBuildNumber;
        private System.Windows.Forms.Label lblBuildNumber;
    }
}