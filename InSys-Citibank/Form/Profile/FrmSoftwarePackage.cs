using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using InSysClass;
using System.IO.Compression;
using System.Linq;

namespace InSys
{
    public partial class FrmSoftwarePackage : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        public string sUID;
        protected string sAppPackagePath;
        protected string sAppPackageName;
        protected string sAppPackageNameOld;
        protected string sAppPackageFilename;
        protected bool isEdit = false;
        protected int iAppPackId;
        //protected int iBuildNumber;
        //protected string sDatabaseID = "0";
        int iCountSoftwareName;
        int iBrowseClickFlag = 0;
        protected string sLinkedServer;

        protected DataTable dtDatabaseList;

        /// <summary>
        /// Form do download software package
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sUID">string : Current active user</param>
        public FrmSoftwarePackage(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sUID)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUID = _sUID;
            InitializeComponent();
        }

        private string sGetRemoteDownloadConn()
        {
            throw new NotImplementedException();
        }

        private void FrmSoftwarePackage_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Init();
            CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, "", CommonMessage.sFormOpened + "Software Packate", "");
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdAppPackage = new OpenFileDialog();
            ofdAppPackage.Filter = "Package Files(*.tar)|*.tar|PackageFile Ingenico(*.agn)|*.agn|Config Files(*.CFG)|*.CFG";
            //ofdAppPackage.InitialDirectory = CommonVariable.PathMyComputer;
            ofdAppPackage.ShowDialog();
            if (!string.IsNullOrEmpty(ofdAppPackage.FileName))
            {
                sAppPackagePath = sGetAppPackagePath(ofdAppPackage.FileName);
                sAppPackageFilename = sGetAppPackageFilename(ofdAppPackage.FileName);
                txtAppPackageFilename.Text = sAppPackageFilename;
                iBrowseClickFlag = 1;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAppPackageFilename.Text))
            {
                if (!string.IsNullOrEmpty(txtBuildNumber.Text))
                {
                    if (CommonClass.isFormatValid(txtBuildNumber.Text, "N"))
                    {
                        if (!string.IsNullOrEmpty(txtPackageName.Text))
                        {
                            sAppPackageName = txtPackageName.Text;
                            sAppPackageFilename = txtAppPackageFilename.Text;

                            if (!string.IsNullOrEmpty(sAppPackageName))
                            {
                                if (!isEdit)
                                    SaveAppPackage();
                                else
                                {
                                    EditAppPackage();
                                    //EditAppPackage2(sSelectedAppPackage, iSelectedAppPackageId);
                                }
                            }
                            iBrowseClickFlag = 0;
                        }
                        else MessageBox.Show("Please Fill Software Name.");
                    }
                    else MessageBox.Show("Invalid Format Build Number.");
                }
                else MessageBox.Show("Please Fill Build Number.");
            }
            else MessageBox.Show("Please Choose File.");
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isEdit = false;
            if (IsRowSelected())
                SetDisplayView(false);
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsRowSelected())
            {
                iAppPackId = int.Parse(dgAppPackage.SelectedRows[0].Cells["AppPackId"].Value.ToString());
                isEdit = true;
                SetDisplayView(false);
                LoadSelectedAppPackage();
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsRowSelected()) RemoveAppPackage(sSelectedAppPackage, iSelectedAppPackageId);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (gbSetting.Enabled) Init();
            else this.Close();
        }

        #region "Function"
        /// <summary>
        /// Initiate datagridview and set form's display
        /// </summary>
        protected void Init()
        {
            dgAppPackage.DataSource = null;
            DataTable dtAppPackage = dtGetAppPackage();
            dgAppPackage.DataSource = dtAppPackage;
            dgAppPackage.Columns["AppPackID"].Visible = false;

            if (dtAppPackage.Rows.Count == 0 || dtAppPackage == null)
                SetDisplayView(false);
            else
                SetDisplayView(true);
            InitPrivilege();
            //InitDatabaseList();
        }

        /// <summary>
        /// Initiate items shown in contextmenustrip based on current user's privilege
        /// </summary>
        protected void InitPrivilege()
        {
            cmsAppPackage.Items["addToolStripMenuItem"].Visible = UserPrivilege.IsAllowed(PrivilegeCode.SW, Privilege.Add);
            cmsAppPackage.Items["editToolStripMenuItem"].Visible = UserPrivilege.IsAllowed(PrivilegeCode.SW, Privilege.Edit); ;
            cmsAppPackage.Items["deleteToolStripMenuItem"].Visible = UserPrivilege.IsAllowed(PrivilegeCode.SW, Privilege.Delete); ;
        }

        protected void InitDatabaseList()
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    dtDatabaseList = new DataTable();
                    dtDatabaseList.Load(oRead);
                    if (dtDatabaseList != null && dtDatabaseList.Rows.Count > 0)
                    {
                        cmbDatabase.DataSource = dtDatabaseList;
                        cmbDatabase.ValueMember = "DatabaseID";
                        cmbDatabase.DisplayMember = "DatabaseName";
                        cmbDatabase.SelectedIndex = -1;
                    }
                }
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Set form's display
        /// </summary>
        /// <param name="isEnable">boolean : true if enabled</param>
        protected void SetDisplayView(bool isEnable)
        {
            gbAppPackageView.Enabled = isEnable;
            gbSetting.Enabled = !isEnable;
            if (!isEnable)
                FillEdcType();
            ClearSetting();
        }

        /// <summary>
        /// Clear text in textbox
        /// </summary>
        protected void ClearSetting()
        {
            txtAppPackageFilename.Clear();
            txtPackageName.Clear();
            txtAppPackageDesc.Clear();
            txtBuildNumber.Clear();
        }

        /// <summary>
        /// Load application package and save it in DataTable
        /// </summary>
        /// <returns>DataTable : Application Package loaded from database</returns>
        protected DataTable dtGetAppPackage()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlDataReader oRead;
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageBrowse, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oRead = oCmd.ExecuteReader();

                if (oRead.HasRows)
                    dt.Load(oRead);

                oRead.Close();
                oRead.Dispose();
                oCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return dt;
        }

        /// <summary>
        /// Fill checklist with data from database
        /// </summary>
        protected void FillEdcType()
        {
            chkListType.DataSource = null;
            DataTable dtEdcType = dtGetEdcType();
            if (dtEdcType.Rows.Count != 0)
            {
                chkListType.DataSource = dtEdcType;
                chkListType.DisplayMember = "EdcTypeName";
                chkListType.ValueMember = "EdcTypeId";
            }
        }

        /// <summary>
        /// Load EDC Type Data from database and store it in DataTable
        /// </summary>
        /// <returns>DataTable : List of EDC Type from database</returns>
        protected DataTable dtGetEdcType()
        {
            SqlDataReader oRead;
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPEdcTypeBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oRead = oCmd.ExecuteReader();

            DataTable dt = new DataTable();
            if (oRead.HasRows)
                dt.Load(oRead);

            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();

            return dt;
        }

        /// <summary>
        /// Get the path where application packaged are saved
        /// </summary>
        /// <param name="sFullname">string : path name</param>
        /// <returns>string : Path of App Package</returns>
        protected string sGetAppPackagePath(string sFullname)
        {
            return sFullname.Substring(0, sFullname.LastIndexOf(@"\"));
        }

        /// <summary>
        /// Get the name of selected application package
        /// </summary>
        /// <param name="sFullname"></param>
        /// <returns></returns>
        protected string sGetAppPackageFilename(string sFullname)
        {
            return sFullname.Substring(sFullname.LastIndexOf(@"\") + 1);
        }

        /// <summary>
        /// Get the description of application package
        /// </summary>
        protected string sAppPackageDesc
        {
            get { return string.IsNullOrEmpty(txtAppPackageDesc.Text) ? "" : txtAppPackageDesc.Text; }
        }

        /// <summary>
        /// Get the description of application package
        /// </summary>
        protected string sBuildNumber
        {
            get { return string.IsNullOrEmpty(txtBuildNumber.Text) ? "" : txtBuildNumber.Text; }
        }

        /// <summary>
        /// Get the value of selected EDC Type
        /// </summary>
        /// <returns>string : EDC Type ID</returns>
        protected string sEdcTypeId()
        {
            string sReturn = null;
            if (chkListType.CheckedItems.Count != 0)
            {
                foreach (DataRowView rowview in chkListType.CheckedItems)
                {
                    if (!string.IsNullOrEmpty(sReturn))
                        sReturn = string.Format("{0};{1}", sReturn, rowview[chkListType.ValueMember].ToString());
                    else
                        sReturn = rowview[chkListType.ValueMember].ToString();
                }
            }
            return sReturn;
        }

        /// <summary>
        /// Save application package data to database
        /// </summary>
        protected void SaveAppPackage()
        {
            //DataTable OdtcheckSofwareofDatabase = new DataTable();
            //OdtcheckSofwareofDatabase = CheckSoftwareofDatabase();
            //iCountSoftwareName= OdtcheckSofwareofDatabase.Rows.Count;
            //DataTable dtSoftwarepackage = new DataTable();
            //dtSoftwarepackage = SoftwarePackageBrowse();
            //if (iCountSoftwareName == 0)
            //{

            if (IsSavePackage(sAppPackageName))
            {
                if (iBrowseClickFlag == 1)
                {
                    try
                    {
                        byte[] arrbContent = arrbFileContent();
                        int iSuccess = 0;
                        int iAppPackId = 0;
                        SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageAddContent, oSqlConn);
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.Parameters.Add("@iBuildNumber", SqlDbType.Int).Value = int.Parse(sBuildNumber);
                        oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = sAppPackageName;
                        oCmd.Parameters.Add("@sAppPackageFilename", SqlDbType.VarChar).Value = sAppPackageFilename;
                        oCmd.Parameters.Add("@sAppPackageDesc", SqlDbType.VarChar).Value = sAppPackageDesc;
                        oCmd.Parameters.Add("@bAppPackageContent", SqlDbType.VarBinary).Value = arrbContent;
                        //oCmd.Parameters.Add("@iDatabaseID", SqlDbType.Decimal).Value = int.Parse(sDatabaseID);
                        oCmd.Parameters.Add("@sEdcTypeId", SqlDbType.VarChar).Value = sEdcTypeId();

                        oCmd.Parameters.Add("@iSuccess", SqlDbType.Int).Value = iSuccess;
                        oCmd.Parameters["@iSuccess"].Direction = ParameterDirection.Output;
                        oCmd.Parameters.Add("@iAppPackId", SqlDbType.Int).Value = iAppPackId;
                        oCmd.Parameters["@iAppPackId"].Direction = ParameterDirection.Output;

                        if (oSqlConn.State != ConnectionState.Open)
                        {
                            oSqlConn.Close();
                            oSqlConn.Open();
                        }
                        oCmd.ExecuteNonQuery();
                        iSuccess = (int)oCmd.Parameters["@iSuccess"].Value;
                        if (!string.IsNullOrEmpty(oCmd.Parameters["@iAppPackId"].Value.ToString()) || oCmd.Parameters["@iAppPackId"].Value.ToString().Length > 0)
                            iAppPackId = (int)oCmd.Parameters["@iAppPackId"].Value;

                        SaveAppPackageTemp(arrbContent, iAppPackId);

                        if (iSuccess == 0)
                            MessageBox.Show("Duplicate App Package Name. Please choose other file or rename it.");
                        else
                            CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, "", "Applicaiton Package saved successfully",
                                string.Format("Save application {0} from {1}", sAppPackageName, sAppPackagePath));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    Init();
                }
                //else
                //    MessageBox.Show("Please choose file.");
            }
            //    }
            //    else MessageBox.Show("The DatabaseID " + sDatabaseID + " Already Have Software Package");
        }

        //private DataTable CheckSoftwareofDatabase()
        //{
        //    SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageBrowse, oSqlConn);
        //    oCmd.CommandType = CommandType.StoredProcedure;
        //    oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionOfSofwareName();
        //    SqlDataAdapter AdaptoCmd = new SqlDataAdapter(oCmd);
        //    DataTable OdtcheckSofwareofDatabase = new DataTable();
        //    AdaptoCmd.Fill(OdtcheckSofwareofDatabase);
        //    return OdtcheckSofwareofDatabase;
        //}

        //private object sConditionOfSofwareName()
        //{
        //    //return string.Format(" where tbAppPackageEDCList.DatabaseId = {0}",sDatabaseID);
        //}

        //private DataTable SoftwarePackageBrowse()
        //{
        //    SqlCommand oCmdBrowse = new SqlCommand(CommonSP.sSPAppPackageBrowse, oSqlConn);

        //}

        /// <summary>
        /// Edit Existing Package Information
        /// </summary>
        protected void EditAppPackage()
        {
            try
            {
                byte[] arrbContent;
                if (iBrowseClickFlag == 1)
                {
                    arrbContent = arrbFileContent();
                    DeleteAppPackageTemp(sAppPackageNameOld, iAppPackId);
                    SaveAppPackageTemp(arrbContent, iAppPackId);
                }
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageEdit, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@iAppPackId", SqlDbType.Int).Value = iAppPackId;
                oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = sAppPackageName;
                oCmd.Parameters.Add("@sAppPackageFilename", SqlDbType.VarChar).Value = sAppPackageFilename;
                oCmd.Parameters.Add("@iBuildNumber", SqlDbType.VarChar).Value = int.Parse(sBuildNumber);
                oCmd.Parameters.Add("@sAppPackageDesc", SqlDbType.VarChar).Value = sAppPackageDesc;
                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Init();
        }

        private void EditAppPackage2(string _sAppPackageName, int iAppPackId)
        {
            if (IsSavePackage(sAppPackageName))
            {
                //EditAppPackage(_sAppPackageName, iAppPackId);
                DeleteAppPackage(_sAppPackageName, iAppPackId);
                AddAppPackage(iAppPackId);
            }
        }

        /// <summary>
        /// Delate Data fom tbAppPackageList Temp by AppPackID
        /// </summary>
        private void DeleteAppPackageTemp(string _sAppPackageName, int iAppPackId)
        {
            try
            {
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageTempDelete, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = _sAppPackageName;
                oCmd.Parameters.Add("@iAppPackId", SqlDbType.Decimal).Value = iAppPackId;

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Add Package Software
        /// </summary>
        /// <param name="iAppPackIdDelete"></param>
        private void AddAppPackage(int iAppPackIdDelete)
        {
            try
            {
                byte[] arrbContent = arrbFileContent();
                int iSuccess = 0;
                int iAppPackId = 0;
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageAddContent, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@iBuildNumber", SqlDbType.Int).Value = Convert.ToInt32(sBuildNumber);
                oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = sAppPackageName;
                oCmd.Parameters.Add("@sAppPackageFilename", SqlDbType.VarChar).Value = sAppPackageFilename;
                oCmd.Parameters.Add("@sAppPackageDesc", SqlDbType.VarChar).Value = sAppPackageDesc;
                oCmd.Parameters.Add("@bAppPackageContent", SqlDbType.VarBinary).Value = arrbContent;
                //oCmd.Parameters.Add("@iDatabaseID", SqlDbType.Decimal).Value = int.Parse(sDatabaseID);
                oCmd.Parameters.Add("@sEdcTypeId", SqlDbType.VarChar).Value = sEdcTypeId();

                oCmd.Parameters.Add("@iSuccess", SqlDbType.Int).Value = iSuccess;
                oCmd.Parameters["@iSuccess"].Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@iAppPackId", SqlDbType.Int).Value = iAppPackId;
                oCmd.Parameters["@iAppPackId"].Direction = ParameterDirection.Output;

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                oCmd.ExecuteNonQuery();
                iSuccess = (int)oCmd.Parameters["@iSuccess"].Value;
                iAppPackId = (int)oCmd.Parameters["@iAppPackId"].Value;

                SaveAppPackageTemp(arrbContent, iAppPackId);
                UpdateAppPackIDTerminalSN(iAppPackIdDelete, iAppPackId);

                if (iSuccess == 0)
                    MessageBox.Show("Duplicate App Package Name. Please choose other file or rename it.");
                else
                    CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, "", "Applicaiton Package saved successfully",
                        string.Format("Save application {0} from {1}", sAppPackageName, sAppPackagePath));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Init();
        }

        /// <summary>
        /// Update App Package Software
        /// </summary>
        private void UpdateAppPackIDTerminalSN(int iAppPackIdDelete, int iAppPackId)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPRDListTIDUpdatebyAppPackageUpdate, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@iAppPackageDelete", SqlDbType.Int).Value = iAppPackIdDelete;
            oCmd.Parameters.Add("@iAppPackageUpdate", SqlDbType.Int).Value = iAppPackId;
            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }
            oCmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Delete App Package Software
        /// </summary>
        private void DeleteAppPackage(string _sAppPackageName, int iAppPackId)
        {
            try
            {
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageDelete, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = _sAppPackageName;
                oCmd.Parameters.Add("@iAppPackId", SqlDbType.Decimal).Value = iAppPackId;

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //protected void SaveAppPackage()
        //{
        //    if (IsSavePackage(sAppPackageName))
        //    {
        //        try
        //        {
        //            int iSuccess = 0;
        //            SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageAdd, oSqlConn);
        //            oCmd.CommandType = CommandType.StoredProcedure;
        //            oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = sAppPackageName;
        //            oCmd.Parameters.Add("@sAppPackagePath", SqlDbType.VarChar).Value = sAppPackagePath;
        //            oCmd.Parameters.Add("@sAppPackageDesc", SqlDbType.VarChar).Value = sAppPackageDesc;
        //            oCmd.Parameters.Add("@sEdcTypeId", SqlDbType.VarChar).Value = sEdcTypeId();
        //            oCmd.Parameters.Add("@iSuccess", SqlDbType.Int).Value = iSuccess;
        //            oCmd.Parameters["@iSuccess"].Direction = ParameterDirection.Output;

        //            if (oSqlConn.State != ConnectionState.Open)
        //            {
        //                oSqlConn.Close();
        //                oSqlConn.Open();
        //            }
        //            oCmd.ExecuteNonQuery();
        //            iSuccess = (int)oCmd.Parameters["@iSuccess"].Value;
        //            if (iSuccess == 0)
        //                MessageBox.Show("Duplicate App Package Name. Please choose other file or rename it.");
        //            else
        //                CommonClass.InputLog(oSqlConn, "", sUID, "", "Applicaiton Package saved successfully", 
        //                    string.Format("Save application {0} from {1}", sAppPackageName, sAppPackagePath));
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message);
        //        }
        //        Init();
        //    }
        //}

        protected byte[] arrbFileContent()
        {
            string sFilename = string.Format(@"{0}\{1}", sAppPackagePath, sAppPackageFilename);
            //string sFilezip = string.Format(@"{0}\{1}", Application.StartupPath, sAppPackageName);
            //File.Copy(sFilename, sFilezip, true);
            byte[] arrbContent = new byte[1024];

            FileStream fsRead = new FileStream(sFilename, FileMode.Open);
            arrbContent = new byte[fsRead.Length];
            fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
            fsRead.Close();

            //if (IsNetCompressSuccess(ref sFilezip))
            //{
            //    FileStream fsRead = new FileStream(sFilezip, FileMode.Open);
            //    arrbContent = new byte[fsRead.Length];
            //    fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
            //    fsRead.Close();
            //}

            //File.Delete(sFilezip);
            //File.Delete(sFilezip + ".gz");
            return arrbContent;
        }

        /// <summary>
        /// Bulk Insert into tbAppPackageList
        /// </summary>
        protected void SaveAppPackageTemp(byte[] _arrbContent, int iAppPackId)
        {
            string sContent = CommonLib.sByteArrayToHexString(_arrbContent).Replace(" ", "");

            DataTable dtAppTemp = new DataTable();
            dtAppTemp = dtGetAppPackageTemp(sContent, iAppPackId);
            SqlBulkCopy sqlBulk = new SqlBulkCopy(oSqlConn);
            sqlBulk.DestinationTableName = string.Format("tbAppPackageEDCListTemp");
            SqlBulkCopyColumnMapping mapId = new SqlBulkCopyColumnMapping("Id", "Id");
            SqlBulkCopyColumnMapping mapAppPackId = new SqlBulkCopyColumnMapping("AppPackId", "AppPackId");
            SqlBulkCopyColumnMapping mapAppPackageName = new SqlBulkCopyColumnMapping("AppPackageName", "AppPackageName");
            SqlBulkCopyColumnMapping mapAppPackageContent = new SqlBulkCopyColumnMapping("AppPackageContent", "AppPackageContent");
            sqlBulk.ColumnMappings.Add(mapId);
            sqlBulk.ColumnMappings.Add(mapAppPackId);
            sqlBulk.ColumnMappings.Add(mapAppPackageName);
            sqlBulk.ColumnMappings.Add(mapAppPackageContent);
            sqlBulk.WriteToServer(dtAppTemp);
        }

        protected DataTable dtGetAppPackageTemp(string _sContent, int iAppPackId)
        {
            DataSet dsTemp = new DataSet();
            DataTable dtTemp = new DataTable();
            string sQuery = string.Format("{0}",
                string.Format("SELECT Id, AppPackageName, AppPackageContent, AppPackId " +
                    "FROM tbAppPackageEDCListTemp " +
                    "WHERE AppPackageName='{0}' AND AppPackId={1}", sAppPackageName, iAppPackId));
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
                (new SqlDataAdapter(oCmd)).Fill(dsTemp);
            dtTemp = dsTemp.Tables[0].Copy();
            int iOffset = 0;
            int iLength = 800 * 2;
            int iId = 0;
            while (iOffset < _sContent.Length)
            {
                DataRow row = dtTemp.NewRow();
                row["Id"] = iId++;
                row["AppPackId"] = iAppPackId;
                row["AppPackageName"] = sAppPackageName;
                row["AppPackageContent"] = _sContent.Substring(iOffset,
                    iOffset + iLength < _sContent.Length ? iLength : _sContent.Length - iOffset);
                dtTemp.Rows.Add(row);
                iOffset += iLength;
            }
            return dtTemp;
        }

        /// <summary>
        /// Confirmation to save Package
        /// </summary>
        /// <param name="_sAppPackageName">Package's name that will be saved</param>
        /// <returns>boolean : true if saved</returns>
        protected bool IsSavePackage(string _sAppPackageName)
        {
            return MessageBox.Show(string.Format("Save '{0}' to Database?", _sAppPackageName), "", MessageBoxButtons.YesNo) == DialogResult.Yes ?
                true : false;
        }

        /// <summary>
        /// Confirmation to removed application package
        /// </summary>
        /// <param name="_sAppPackageName">string : application's name that will be removed</param>
        /// <returns>boolean : true if will be removed</returns>
        protected bool IsRemovePackage(string _sAppPackageName)
        {
            return MessageBox.Show(string.Format("Remove '{0}' from Database?", _sAppPackageName), "", MessageBoxButtons.YesNo) == DialogResult.Yes ?
                true : false;
        }

        /// <summary>
        /// Remove application pacakge data from database
        /// </summary>
        /// <param name="_sAppPackageName">string : application's name that will be removed</param>
        protected void RemoveAppPackage(string _sAppPackageName, int iAppPackId)
        {
            if (isAppIsUse(iAppPackId) == false)
            {
                if (IsRemovePackage(_sAppPackageName))
                {
                    try
                    {
                        SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageDelete, oSqlConn);
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = _sAppPackageName;
                        oCmd.Parameters.Add("@iAppPackId", SqlDbType.Int).Value = iAppPackId;

                        if (oSqlConn.State != ConnectionState.Open)
                        {
                            oSqlConn.Close();
                            oSqlConn.Open();
                        }
                        oCmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    Init();
                }
            }
            else
            {
                MessageBox.Show("Software in use.");
            }
        }

        /// <summary>
        /// Check App still in Use or not
        /// </summary>
        private bool isAppIsUse(int _iAppPackId)
        {
            bool bResult = true;
            try
            {
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPBrowseAllColoumLinkListTID, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE A.AppPackID = {0}", _iAppPackId);

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                SqlDataReader oRead = oCmd.ExecuteReader();
                if (oRead.HasRows)
                    bResult = true;
                else
                    bResult = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return bResult;
        }

        /// <summary>
        /// Determine whether there are rows selected or not
        /// </summary>
        /// <returns>boolean : true if some rows selected</returns>
        protected bool IsRowSelected()
        {
            return dgAppPackage.SelectedRows.Count > 0 ? true : false;
        }

        /// <summary>
        /// Application Package's Name
        /// </summary>
        protected string sSelectedAppPackage
        {
            get
            {
                int iRow = dgAppPackage.SelectedRows[0].Index;
                return dgAppPackage["Software Name", iRow].Value.ToString();
            }
        }

        protected int iSelectedAppPackageId
        {
            get
            {
                int iRow = dgAppPackage.SelectedRows[0].Index;
                return int.Parse(dgAppPackage["AppPackID", iRow].Value.ToString());
            }
        }

        /// <summary>
        /// Get Application Package data from database
        /// </summary>
        protected void LoadSelectedAppPackage()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageBrowseDetail, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = sSelectedAppPackage;

            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);
            txtBuildNumber.Text = dtTemp.Rows[0]["BuildNumber"].ToString();
            txtPackageName.Text = dtTemp.Rows[0]["AppPackageName"].ToString();
            txtAppPackageFilename.Text = dtTemp.Rows[0]["AppPackageFilename"].ToString();
            txtAppPackageDesc.Text = dtTemp.Rows[0]["AppPackageDesc"].ToString();
            CheckSelectedEdcType(dtTemp.Rows[0]["EdcTypeId"].ToString());
            sAppPackageNameOld = dtTemp.Rows[0]["AppPackageName"].ToString();
            //cmbDatabase.SelectedValue = dtTemp.Rows[0]["DatabaseId"].ToString();

            oCmd.Dispose();
            oRead.Close();
            oRead.Dispose();
            dtTemp.Dispose();
        }

        /// <summary>
        /// Fill EDC type 
        /// </summary>
        /// <param name="_sEdcType"></param>
        protected void CheckSelectedEdcType(string _sEdcType)
        {
            string[] arrEdcTypeId = _sEdcType.Split(';');
            foreach (string s in arrEdcTypeId)
            {
                for (int i = 0; i < chkListType.Items.Count; i++)
                {
                    DataRowView row = (DataRowView)chkListType.Items[i];
                    if (s == row[chkListType.ValueMember].ToString())
                        chkListType.SetItemChecked(i, true);
                }
            }
        }

        protected static bool IsNetCompressSuccess(ref string sFilename)
        {
            bool bReturn = false;
            try
            {
                FileInfo fi = new FileInfo(sFilename);
                using (FileStream inFile = fi.OpenRead())
                {
                    string sGzipFile = sFilename + ".gz";
                    if (File.Exists(sGzipFile))
                        File.Delete(sGzipFile);
                    using (FileStream outFile = File.Create(sGzipFile))
                    {
                        using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                        {
                            byte[] buffer = new byte[fi.Length];
                            inFile.Read(buffer, 0, Convert.ToInt32(fi.Length));
                            compress.Write(buffer, 0, Convert.ToInt32(fi.Length));
                            sFilename = sGzipFile;
                            bReturn = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("NetCompress " + ex.Message);
            }
            return bReturn;
        }
        #endregion

        private void txtBuildNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            int iNumberRange = e.KeyChar;
            if (iNumberRange >= 0 && iNumberRange < 10000)
            {
            }
            else
            {
                MessageBox.Show("You Can Only Enter Number!");
                e.Handled = true;
            }
        }
    }
}