namespace InSys
{
    partial class FrmFlexible
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFlexible));
            this.chkEnableIP = new System.Windows.Forms.CheckBox();
            this.chkStatusMaster = new System.Windows.Forms.CheckBox();
            this.chkAllowDL = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbStatus = new System.Windows.Forms.GroupBox();
            this.chkLogoIdle = new System.Windows.Forms.CheckBox();
            this.chkLogoPrint = new System.Windows.Forms.CheckBox();
            this.chkRemoteDownload = new System.Windows.Forms.CheckBox();
            this.chkInitCompress = new System.Windows.Forms.CheckBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.gbStatus.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkEnableIP
            // 
            this.chkEnableIP.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkEnableIP.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEnableIP.Location = new System.Drawing.Point(150, 15);
            this.chkEnableIP.Name = "chkEnableIP";
            this.chkEnableIP.Size = new System.Drawing.Size(80, 20);
            this.chkEnableIP.TabIndex = 2;
            this.chkEnableIP.Text = "Enable IP";
            this.chkEnableIP.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkEnableIP.Visible = false;
            this.chkEnableIP.CheckedChanged += new System.EventHandler(this.chkEnableIP_CheckedChanged);
            // 
            // chkStatusMaster
            // 
            this.chkStatusMaster.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkStatusMaster.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStatusMaster.Location = new System.Drawing.Point(79, 15);
            this.chkStatusMaster.Name = "chkStatusMaster";
            this.chkStatusMaster.Size = new System.Drawing.Size(65, 20);
            this.chkStatusMaster.TabIndex = 1;
            this.chkStatusMaster.Text = "Status Master";
            this.chkStatusMaster.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkStatusMaster.CheckedChanged += new System.EventHandler(this.chkStatusMaster_CheckedChanged);
            // 
            // chkAllowDL
            // 
            this.chkAllowDL.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkAllowDL.Checked = true;
            this.chkAllowDL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllowDL.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAllowDL.Location = new System.Drawing.Point(13, 15);
            this.chkAllowDL.Name = "chkAllowDL";
            this.chkAllowDL.Size = new System.Drawing.Size(60, 20);
            this.chkAllowDL.TabIndex = 0;
            this.chkAllowDL.Text = "Allow Download";
            this.chkAllowDL.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkAllowDL.CheckedChanged += new System.EventHandler(this.chkAllowDL_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(430, 15);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSave.Location = new System.Drawing.Point(342, 15);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 24);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbStatus
            // 
            this.gbStatus.Controls.Add(this.chkLogoIdle);
            this.gbStatus.Controls.Add(this.chkLogoPrint);
            this.gbStatus.Controls.Add(this.chkRemoteDownload);
            this.gbStatus.Controls.Add(this.chkInitCompress);
            this.gbStatus.Controls.Add(this.chkAllowDL);
            this.gbStatus.Controls.Add(this.chkStatusMaster);
            this.gbStatus.Controls.Add(this.chkEnableIP);
            this.gbStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbStatus.Location = new System.Drawing.Point(0, 0);
            this.gbStatus.Name = "gbStatus";
            this.gbStatus.Size = new System.Drawing.Size(850, 43);
            this.gbStatus.TabIndex = 0;
            this.gbStatus.TabStop = false;
            // 
            // chkLogoIdle
            // 
            this.chkLogoIdle.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoIdle.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLogoIdle.Location = new System.Drawing.Point(573, 15);
            this.chkLogoIdle.Name = "chkLogoIdle";
            this.chkLogoIdle.Size = new System.Drawing.Size(87, 20);
            this.chkLogoIdle.TabIndex = 7;
            this.chkLogoIdle.Text = "Logo Idle";
            this.chkLogoIdle.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoIdle.Visible = false;
            this.chkLogoIdle.CheckedChanged += new System.EventHandler(this.chkInitIdleScreen_CheckedChanged);
            // 
            // chkLogoPrint
            // 
            this.chkLogoPrint.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoPrint.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLogoPrint.Location = new System.Drawing.Point(481, 15);
            this.chkLogoPrint.Name = "chkLogoPrint";
            this.chkLogoPrint.Size = new System.Drawing.Size(86, 20);
            this.chkLogoPrint.TabIndex = 6;
            this.chkLogoPrint.Text = "Logo Print";
            this.chkLogoPrint.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoPrint.Visible = false;
            this.chkLogoPrint.CheckedChanged += new System.EventHandler(this.chkInitLogo_CheckedChanged);
            // 
            // chkRemoteDownload
            // 
            this.chkRemoteDownload.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkRemoteDownload.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoteDownload.Location = new System.Drawing.Point(346, 15);
            this.chkRemoteDownload.Name = "chkRemoteDownload";
            this.chkRemoteDownload.Size = new System.Drawing.Size(129, 20);
            this.chkRemoteDownload.TabIndex = 5;
            this.chkRemoteDownload.Text = "Remote Download";
            this.chkRemoteDownload.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkRemoteDownload.Visible = false;
            this.chkRemoteDownload.CheckedChanged += new System.EventHandler(this.chkRemoteDownload_CheckedChanged);
            // 
            // chkInitCompress
            // 
            this.chkInitCompress.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkInitCompress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInitCompress.Location = new System.Drawing.Point(236, 15);
            this.chkInitCompress.Name = "chkInitCompress";
            this.chkInitCompress.Size = new System.Drawing.Size(110, 20);
            this.chkInitCompress.TabIndex = 4;
            this.chkInitCompress.Text = "Init Compress";
            this.chkInitCompress.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkInitCompress.Visible = false;
            this.chkInitCompress.CheckedChanged += new System.EventHandler(this.chkInitCompress_CheckedChanged);
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbButton.Location = new System.Drawing.Point(0, 570);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(850, 48);
            this.gbButton.TabIndex = 11;
            this.gbButton.TabStop = false;
            // 
            // FrmFlexible
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(850, 618);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmFlexible";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Flexible";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmFlexible_FormClosing);
            this.Load += new System.EventHandler(this.FrmFlexible_Load);
            this.gbStatus.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.CheckBox chkEnableIP;
        internal System.Windows.Forms.CheckBox chkStatusMaster;
        internal System.Windows.Forms.CheckBox chkAllowDL;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbStatus;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.CheckBox chkInitCompress;
        internal System.Windows.Forms.CheckBox chkRemoteDownload;
        internal System.Windows.Forms.CheckBox chkLogoIdle;
        internal System.Windows.Forms.CheckBox chkLogoPrint;

    }
}