﻿namespace InSys
{
    partial class FrmEdcGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("JAKARTA");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("BANDUNG");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("SEMARANG");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("BALI");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("BCA", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("root", new System.Windows.Forms.TreeNode[] {
            treeNode5});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEdcGroup));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbTidList = new System.Windows.Forms.ListBox();
            this.cmsListTerminal = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addTerminalListTerminalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeTerminalListTerminalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.propertiesTerminalListTerminalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtTidList = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtHeaderTree = new System.Windows.Forms.TextBox();
            this.tvGroupRegion = new System.Windows.Forms.TreeView();
            this.cmsGroupRegion = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addSubToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSubToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesGroupRegionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.addTerminalGroupRegionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearchTerminalID = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.cmsListTerminal.SuspendLayout();
            this.panel1.SuspendLayout();
            this.cmsGroupRegion.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Location = new System.Drawing.Point(12, 47);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(584, 400);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lbTidList);
            this.panel2.Controls.Add(this.txtTidList);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(206, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(375, 381);
            this.panel2.TabIndex = 5;
            // 
            // lbTidList
            // 
            this.lbTidList.ContextMenuStrip = this.cmsListTerminal;
            this.lbTidList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbTidList.FormattingEnabled = true;
            this.lbTidList.Items.AddRange(new object[] {
            "JKT00001",
            "JKT00002",
            "JKT00003"});
            this.lbTidList.Location = new System.Drawing.Point(0, 26);
            this.lbTidList.Name = "lbTidList";
            this.lbTidList.Size = new System.Drawing.Size(375, 355);
            this.lbTidList.TabIndex = 3;
            // 
            // cmsListTerminal
            // 
            this.cmsListTerminal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTerminalListTerminalToolStripMenuItem,
            this.removeTerminalListTerminalToolStripMenuItem,
            this.toolStripMenuItem2,
            this.propertiesTerminalListTerminalToolStripMenuItem});
            this.cmsListTerminal.Name = "cmsListTID";
            this.cmsListTerminal.Size = new System.Drawing.Size(168, 98);
            // 
            // addTerminalListTerminalToolStripMenuItem
            // 
            this.addTerminalListTerminalToolStripMenuItem.Name = "addTerminalListTerminalToolStripMenuItem";
            this.addTerminalListTerminalToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.addTerminalListTerminalToolStripMenuItem.Text = "New Terminal";
            this.addTerminalListTerminalToolStripMenuItem.Click += new System.EventHandler(this.addTerminalListTerminalToolStripMenuItem_Click);
            // 
            // removeTerminalListTerminalToolStripMenuItem
            // 
            this.removeTerminalListTerminalToolStripMenuItem.Name = "removeTerminalListTerminalToolStripMenuItem";
            this.removeTerminalListTerminalToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.removeTerminalListTerminalToolStripMenuItem.Text = "Remove Terminal";
            this.removeTerminalListTerminalToolStripMenuItem.Click += new System.EventHandler(this.removeTerminalToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(164, 6);
            // 
            // propertiesTerminalListTerminalToolStripMenuItem
            // 
            this.propertiesTerminalListTerminalToolStripMenuItem.Name = "propertiesTerminalListTerminalToolStripMenuItem";
            this.propertiesTerminalListTerminalToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.propertiesTerminalListTerminalToolStripMenuItem.Text = "Properties";
            this.propertiesTerminalListTerminalToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // txtTidList
            // 
            this.txtTidList.BackColor = System.Drawing.Color.White;
            this.txtTidList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTidList.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTidList.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtTidList.Location = new System.Drawing.Point(0, 0);
            this.txtTidList.Name = "txtTidList";
            this.txtTidList.ReadOnly = true;
            this.txtTidList.Size = new System.Drawing.Size(375, 20);
            this.txtTidList.TabIndex = 2;
            this.txtTidList.Text = "LIST";
            this.txtTidList.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtHeaderTree);
            this.panel1.Controls.Add(this.tvGroupRegion);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 381);
            this.panel1.TabIndex = 4;
            // 
            // txtHeaderTree
            // 
            this.txtHeaderTree.BackColor = System.Drawing.Color.White;
            this.txtHeaderTree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHeaderTree.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtHeaderTree.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtHeaderTree.Location = new System.Drawing.Point(0, 0);
            this.txtHeaderTree.Name = "txtHeaderTree";
            this.txtHeaderTree.ReadOnly = true;
            this.txtHeaderTree.Size = new System.Drawing.Size(200, 20);
            this.txtHeaderTree.TabIndex = 1;
            this.txtHeaderTree.Text = "Group";
            this.txtHeaderTree.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tvGroupRegion
            // 
            this.tvGroupRegion.ContextMenuStrip = this.cmsGroupRegion;
            this.tvGroupRegion.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tvGroupRegion.FullRowSelect = true;
            this.tvGroupRegion.Location = new System.Drawing.Point(0, 26);
            this.tvGroupRegion.Name = "tvGroupRegion";
            treeNode1.Name = "nodeRegion1";
            treeNode1.Text = "JAKARTA";
            treeNode2.Name = "nodeRegion2";
            treeNode2.Text = "BANDUNG";
            treeNode3.Name = "nodeRegion3";
            treeNode3.Text = "SEMARANG";
            treeNode4.Name = "nodeRegion4";
            treeNode4.Text = "BALI";
            treeNode5.Name = "nodeGroup1";
            treeNode5.Text = "BCA";
            treeNode6.Name = "root";
            treeNode6.Tag = "root";
            treeNode6.Text = "root";
            this.tvGroupRegion.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode6});
            this.tvGroupRegion.Size = new System.Drawing.Size(200, 355);
            this.tvGroupRegion.TabIndex = 0;
            this.tvGroupRegion.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvGroupRegion_AfterSelect);
            // 
            // cmsGroupRegion
            // 
            this.cmsGroupRegion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addSubToolStripMenuItem,
            this.deleteSubToolStripMenuItem,
            this.propertiesGroupRegionToolStripMenuItem,
            this.toolStripMenuItem1,
            this.addTerminalGroupRegionToolStripMenuItem});
            this.cmsGroupRegion.Name = "cmsGroupRegion";
            this.cmsGroupRegion.Size = new System.Drawing.Size(149, 98);
            // 
            // addSubToolStripMenuItem
            // 
            this.addSubToolStripMenuItem.Name = "addSubToolStripMenuItem";
            this.addSubToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.addSubToolStripMenuItem.Text = "Add Sub";
            this.addSubToolStripMenuItem.Click += new System.EventHandler(this.addSubToolStripMenuItem_Click);
            // 
            // deleteSubToolStripMenuItem
            // 
            this.deleteSubToolStripMenuItem.Name = "deleteSubToolStripMenuItem";
            this.deleteSubToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.deleteSubToolStripMenuItem.Text = "Delete Sub";
            this.deleteSubToolStripMenuItem.Click += new System.EventHandler(this.deleteSubToolStripMenuItem_Click);
            // 
            // propertiesGroupRegionToolStripMenuItem
            // 
            this.propertiesGroupRegionToolStripMenuItem.Name = "propertiesGroupRegionToolStripMenuItem";
            this.propertiesGroupRegionToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.propertiesGroupRegionToolStripMenuItem.Text = "Properties";
            this.propertiesGroupRegionToolStripMenuItem.Click += new System.EventHandler(this.propertiesGroupRegionToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(145, 6);
            // 
            // addTerminalGroupRegionToolStripMenuItem
            // 
            this.addTerminalGroupRegionToolStripMenuItem.Name = "addTerminalGroupRegionToolStripMenuItem";
            this.addTerminalGroupRegionToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.addTerminalGroupRegionToolStripMenuItem.Text = "New Terminal";
            this.addTerminalGroupRegionToolStripMenuItem.Click += new System.EventHandler(this.addTerminalListTerminalToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.txtSearchTerminalID);
            this.groupBox1.Location = new System.Drawing.Point(12, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(584, 35);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(491, 8);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(87, 23);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearchTerminalID
            // 
            this.txtSearchTerminalID.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtSearchTerminalID.Location = new System.Drawing.Point(385, 10);
            this.txtSearchTerminalID.Name = "txtSearchTerminalID";
            this.txtSearchTerminalID.Size = new System.Drawing.Size(100, 20);
            this.txtSearchTerminalID.TabIndex = 0;
            this.txtSearchTerminalID.Text = "Terminal ID";
            this.txtSearchTerminalID.Enter += new System.EventHandler(this.txtSearchTerminalID_Enter);
            this.txtSearchTerminalID.Leave += new System.EventHandler(this.txtSearchTerminalID_Leave);
            // 
            // FrmEdcGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 459);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEdcGroup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Group";
            this.Load += new System.EventHandler(this.FrmEdcGroup_Load);
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.cmsListTerminal.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.cmsGroupRegion.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TreeView tvGroupRegion;
        private System.Windows.Forms.TextBox txtHeaderTree;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListBox lbTidList;
        private System.Windows.Forms.TextBox txtTidList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearchTerminalID;
        private System.Windows.Forms.ContextMenuStrip cmsGroupRegion;
        private System.Windows.Forms.ToolStripMenuItem addSubToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSubToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesGroupRegionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addTerminalGroupRegionToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsListTerminal;
        private System.Windows.Forms.ToolStripMenuItem removeTerminalListTerminalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesTerminalListTerminalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTerminalListTerminalToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
    }
}