﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmAdvanceSearch : Form
    {
        protected SqlConnection oSqlConn;
        protected DataTable dt;
        protected DataTable oDataTable;
        SqlDataAdapter da;

        public string sTagValue1;
        public string sTagValue2;
        string sDbID;

        public string sParameter1;
        public string sValue1;
        public string sParameter2;
        public string sValue2;
        string sFormIDValue1;
        int sFormIDValue1Int = 0;
        string sFormIDValue2;
        int sFormIDValue2Int = 0;
        public FrmExpand Frmxpand { get; set; } //allow form AdvanceSearch to acces component from form FrmExpand

        public FrmAdvanceSearch(SqlConnection _oSqlConn, string _sDbID, SqlDataAdapter _da)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            sDbID = _sDbID;
            da = _da; ///get DataAdapter from FrmExpand
        }

        private void AdvanceSearch_Load(object sender, EventArgs e)
        {
            ///begin fill combobox
            dt = new DataTable();
            da.Fill(dt);
            cbxparameter.DataSource = dt;
            cbxparameter.DisplayMember = "ItemName";
            cbxparameter.ValueMember = "Tag";
            button3.Visible = false;
            button4.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count >= 3)
            {
                MessageBox.Show("Max Rows Count Are 2 Rows");
            }
            else
                if (dataGridView1.Rows.Count < 2 )
                {
                    button3.Visible = true;
                    button4.Visible = true;
                    button3.Select();
                    sTagValue1 = cbxparameter.SelectedValue.ToString();
                    SqlDataAdapter daformid = new SqlDataAdapter(("select FormId from tbItemList Where Tag='" + sTagValue1 + "' AND DatabaseID='" + sDbID + "'"),oSqlConn);
                    DataTable dtformid = new DataTable();
                    daformid.Fill(dtformid);
                    sFormIDValue1 = dtformid.Rows[0][0].ToString();
                    sFormIDValue1Int = Int32.Parse(sFormIDValue1);
                    string sparameter = cbxparameter.Text;
                    string svalue = txtvalue.Text;
                    string[] rowstring = {sparameter, svalue};
                    dataGridView1.Rows.Add(rowstring);
                    txtvalue.Clear();
                    cbxparameter.Focus();
                    
               }
            else
                {
                    sTagValue2 = cbxparameter.SelectedValue.ToString();
                    SqlDataAdapter daformid2 = new SqlDataAdapter(("select FormId from tbItemList Where Tag='" + sTagValue2 + "' AND DatabaseID='" + sDbID + "'"), oSqlConn);
                    DataTable dtformid2 = new DataTable();
                    daformid2.Fill(dtformid2);

                    sFormIDValue2 = dtformid2.Rows[0][0].ToString();
                    sFormIDValue2Int = Int32.Parse(sFormIDValue2);
                    string sparameter = cbxparameter.Text;
                    string sValue1 = txtvalue.Text;
                   string[] rowstring = {sparameter, sValue1};
                    dataGridView1.Rows.Add(rowstring);
                    txtvalue.Clear();
                    cbxparameter.Focus();
                }
        }

        int RowCount;
        protected void sGetDataRow(string sRow0Col0, string sRow0Col1, string sRow1Col0, string sRow1Col1)
        {
            if (RowCount <= 2 )
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    sParameter1 = row.DataGridView.Rows[0].Cells[0].Value.ToString();
                    sValue1 = row.DataGridView.Rows[0].Cells[1].Value.ToString();
                }
            else
                foreach (DataGridViewRow row2 in dataGridView1.Rows)
                {
                    sParameter1 = row2.DataGridView.Rows[0].Cells[0].Value.ToString();
                    sValue1 = row2.DataGridView.Rows[0].Cells[1].Value.ToString();
                    sParameter2 = row2.DataGridView.Rows[1].Cells[0].Value.ToString();
                    sValue2 = row2.DataGridView.Rows[1].Cells[1].Value.ToString();
                }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            dt.Clear();
            RowCount = dataGridView1.Rows.Count;
            sGetDataRow("''", "' '", "' '", "' '");
            LoadDataAdvance(1);
            dataGridView1.Rows.Clear();
            txtvalue.Clear();
            this.Close();


         
            
        }

        protected void LoadDataAdvance(int iMode)
        {
            InitTreeAndListView();
            GetListTerminalAdvance();
            FillListTerminalAdvance();
        }

        protected void InitTreeAndListView()
        {
           
            Frmxpand.tvTerminal.Nodes.Clear();
            
            Frmxpand.dgvTerminal.DataSource = null;
            Frmxpand.dgvTagDetail.DataSource = null;
        }

        string ListTid;
        string ListTid2;
        protected void GetListTerminalAdvance()
        {
            using (SqlCommand cmd = new SqlCommand("spProfileTerminalListBrowseAdvanceSearch", oSqlConn)) 
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@sFormId1", SqlDbType.Int).Value = sFormIDValue1Int;
                cmd.Parameters.Add("@sFormId2", SqlDbType.Int).Value = sFormIDValue2Int;
                cmd.Parameters.Add("@sParameter1", SqlDbType.VarChar).Value = sTagValue1;
                cmd.Parameters.Add("@sParameter2", SqlDbType.VarChar).Value = sTagValue2;
                cmd.Parameters.Add("@sValue1", SqlDbType.VarChar).Value = sValue1;
                cmd.Parameters.Add("@sValue2", SqlDbType.VarChar).Value = sValue2;
                cmd.Parameters.Add("@sDbId", SqlDbType.VarChar).Value = sDbID;
                cmd.ExecuteNonQuery();

                bool isGetData = true;
                SqlDataAdapter sDT = new SqlDataAdapter(cmd);
                DataTable DTid = new DataTable();
                sDT.Fill(DTid);
                dt = new DataTable();
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                dt.Clear();
                dt.Load(cmd.ExecuteReader());
                dt.PrimaryKey = new DataColumn[] { dt.Columns["TerminalID"] };
                if (dt.Rows.Count == 0)
                {
                    isGetData = false;
                    MessageBox.Show("Empty Database");
                }
            }

        }

        protected void FillListTerminalAdvance()
        {
            Frmxpand.dgvTerminal.DataSource = dt;
            Frmxpand.dgvTerminal.ClearSelection();
            for (int iCol = 0; iCol < Frmxpand.dgvTerminal.Columns.Count; iCol++)
                if (Frmxpand.dgvTerminal.Columns[iCol].Name != "TerminalID")
                    Frmxpand.dgvTerminal.Columns[iCol].Visible = false;
        }

        protected string sDbConditionsAdvance(int iMode)
        {
            if (iMode == 0) return "Where DatabaseId =" + sDbID;
            else return "Where '" + sParameter1 + "' LIKE '%" + sValue1 + "%' AND DatabaseID = " + sDbID;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 1)
            {
                MessageBox.Show("Grid Null, cannot run process remove");
                
            }
            else
            {
                dataGridView1.Rows.RemoveAt(0);
            }
            
        }

        private void txtvalue_DragEnter(object sender, DragEventArgs e)
        {

        }

        private void txtvalue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(this, new EventArgs());
            }
        }

        private void txtvalue_TextChanged(object sender, EventArgs e)
        {

        }
       

      
      }
}
