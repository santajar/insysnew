namespace InSys
{
    partial class FrmMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMainMenu));
            this.mnuMain = new System.Windows.Forms.MainMenu(this.components);
            this.mnuApplication = new System.Windows.Forms.MenuItem();
            this.mnuUser = new System.Windows.Forms.MenuItem();
            this.mnuLogin = new System.Windows.Forms.MenuItem();
            this.mnuLogOff = new System.Windows.Forms.MenuItem();
            this.MenuItem15 = new System.Windows.Forms.MenuItem();
            this.mnuPassword = new System.Windows.Forms.MenuItem();
            this.MenuItemApplicationUser = new System.Windows.Forms.MenuItem();
            this.mnuSettingApp = new System.Windows.Forms.MenuItem();
            this.mnuDBConn = new System.Windows.Forms.MenuItem();
            this.MenuItemApplicationSetting = new System.Windows.Forms.MenuItem();
            this.mnuUserManagement = new System.Windows.Forms.MenuItem();
            this.mnuItemUserRight = new System.Windows.Forms.MenuItem();
            this.mnuUserGroup = new System.Windows.Forms.MenuItem();
            this.MenuItemApplicationUserManagement = new System.Windows.Forms.MenuItem();
            this.mnuExit = new System.Windows.Forms.MenuItem();
            this.mnuEdcTerminal = new System.Windows.Forms.MenuItem();
            this.mnuBrowse = new System.Windows.Forms.MenuItem();
            this.mnuSelectVs = new System.Windows.Forms.MenuItem();
            this.menuItemApplicationBrowse = new System.Windows.Forms.MenuItem();
            this.mnuSettingEdc = new System.Windows.Forms.MenuItem();
            this.mnuAllSN = new System.Windows.Forms.MenuItem();
            this.mnuAllowInit = new System.Windows.Forms.MenuItem();
            this.mnuCompressInit = new System.Windows.Forms.MenuItem();
            this.mnuAutoInit = new System.Windows.Forms.MenuItem();
            this.mnuLocationProfiling = new System.Windows.Forms.MenuItem();
            this.mnuAddLocation = new System.Windows.Forms.MenuItem();
            this.mnuLocationView = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.mnuDataUpload = new System.Windows.Forms.MenuItem();
            this.mnuTemplateDefinition = new System.Windows.Forms.MenuItem();
            this.mnuUploadDefinition = new System.Windows.Forms.MenuItem();
            this.mnuUpload = new System.Windows.Forms.MenuItem();
            this.menuItemApplicationDataUpload = new System.Windows.Forms.MenuItem();
            this.mnuAppPackage = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.mnuRemoteDownload = new System.Windows.Forms.MenuItem();
            this.mnUpdateRD = new System.Windows.Forms.MenuItem();
            this.menuItem21 = new System.Windows.Forms.MenuItem();
            this.mnLogoEDC = new System.Windows.Forms.MenuItem();
            this.mnLogoEDCPrint = new System.Windows.Forms.MenuItem();
            this.mnLogoEDCIdle = new System.Windows.Forms.MenuItem();
            this.mnuReport = new System.Windows.Forms.MenuItem();
            this.mnuAuditTrail = new System.Windows.Forms.MenuItem();
            this.mnuInitializeTrail = new System.Windows.Forms.MenuItem();
            this.mnuSoftwareInitTrail = new System.Windows.Forms.MenuItem();
            this.mnuTLV = new System.Windows.Forms.MenuItem();
            this.mnuQueryReport = new System.Windows.Forms.MenuItem();
            this.mnuMonitor = new System.Windows.Forms.MenuItem();
            this.mnuStartListen = new System.Windows.Forms.MenuItem();
            this.mnuEDCTracking = new System.Windows.Forms.MenuItem();
            this.mnuSerialDownload = new System.Windows.Forms.MenuItem();
            this.mnSoftwareProgress = new System.Windows.Forms.MenuItem();
            this.mnEchoPing = new System.Windows.Forms.MenuItem();
            this.mnuManagement = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.menuItem11 = new System.Windows.Forms.MenuItem();
            this.menuItem12 = new System.Windows.Forms.MenuItem();
            this.menuItem13 = new System.Windows.Forms.MenuItem();
            this.menuItem14 = new System.Windows.Forms.MenuItem();
            this.menuItem16 = new System.Windows.Forms.MenuItem();
            this.menuItem17 = new System.Windows.Forms.MenuItem();
            this.menuItem18 = new System.Windows.Forms.MenuItem();
            this.menuItem19 = new System.Windows.Forms.MenuItem();
            this.menuItem20 = new System.Windows.Forms.MenuItem();
            this.MenuItem4 = new System.Windows.Forms.MenuItem();
            this.mnuHelp = new System.Windows.Forms.MenuItem();
            this.mnuRegister = new System.Windows.Forms.MenuItem();
            this.MenuItemHelp = new System.Windows.Forms.MenuItem();
            this.mnuAbout = new System.Windows.Forms.MenuItem();
            this.mnuSuperAdmin = new System.Windows.Forms.MenuItem();
            this.mnuSUDatabase = new System.Windows.Forms.MenuItem();
            this.mnuSUVersion = new System.Windows.Forms.MenuItem();
            this.mnuSUVersion_Add = new System.Windows.Forms.MenuItem();
            this.mnuSUVersion_Edit = new System.Windows.Forms.MenuItem();
            this.mnuSUVersion_Delete = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItem = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItem_AddUpdate = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItem_Delete = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItemExport = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItemImport = new System.Windows.Forms.MenuItem();
            this.mnuSUCopyCardRange = new System.Windows.Forms.MenuItem();
            this.mnuSUCopyGPRS = new System.Windows.Forms.MenuItem();
            this.mnuSUCopyTLE = new System.Windows.Forms.MenuItem();
            this.mnSUCopyAID = new System.Windows.Forms.MenuItem();
            this.mnSUCopyCAPK = new System.Windows.Forms.MenuItem();
            this.mnCopyRD = new System.Windows.Forms.MenuItem();
            this.mnuSUCard = new System.Windows.Forms.MenuItem();
            this.mnuSUImportCard = new System.Windows.Forms.MenuItem();
            this.mnuSUExportCard = new System.Windows.Forms.MenuItem();
            this.mnuSUModify = new System.Windows.Forms.MenuItem();
            this.mnuSUData = new System.Windows.Forms.MenuItem();
            this.mnuSUImportData = new System.Windows.Forms.MenuItem();
            this.mnuSUExportData = new System.Windows.Forms.MenuItem();
            this.mnuSUExportToTxtFile = new System.Windows.Forms.MenuItem();
            this.mnuSUExportToDb = new System.Windows.Forms.MenuItem();
            this.mnuSUMoveData = new System.Windows.Forms.MenuItem();
            this.mnuSUCopyData = new System.Windows.Forms.MenuItem();
            this.mnuSUTable = new System.Windows.Forms.MenuItem();
            this.mnuSUAddTable = new System.Windows.Forms.MenuItem();
            this.mnuSUDeleteTable = new System.Windows.Forms.MenuItem();
            this.mnuSUFields = new System.Windows.Forms.MenuItem();
            this.mnuSUAddFields = new System.Windows.Forms.MenuItem();
            this.mnuSUUpdateFields = new System.Windows.Forms.MenuItem();
            this.mnuSUDeleteFields = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.mnuSUApplicationMgmt = new System.Windows.Forms.MenuItem();
            this.mnuSUSwProfile = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.mnuSUInitConn = new System.Windows.Forms.MenuItem();
            this.mnuSUSetFolder = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnuSUAudit = new System.Windows.Forms.MenuItem();
            this.mnuSUTag5 = new System.Windows.Forms.MenuItem();
            this.mnuSUQueryReport = new System.Windows.Forms.MenuItem();
            this.StatusBarApplication = new System.Windows.Forms.StatusBarPanel();
            this.StatusBarTime = new System.Windows.Forms.StatusBarPanel();
            this.StatusBarDate = new System.Windows.Forms.StatusBarPanel();
            this.TimerClock = new System.Windows.Forms.Timer(this.components);
            this.ToolBarMenu = new System.Windows.Forms.ToolBar();
            this.ToolBarLogin = new System.Windows.Forms.ToolBarButton();
            this.ToolBarLogOff = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton5 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarExit = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton1 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarAudit = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton2 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarSelectVs = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton3 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarUnited = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton4 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarDownload = new System.Windows.Forms.ToolBarButton();
            this.ImageIcon = new System.Windows.Forms.ImageList(this.components);
            this.StatusBar1 = new System.Windows.Forms.StatusBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarApplication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarDate)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuApplication,
            this.mnuEdcTerminal,
            this.mnuReport,
            this.mnuMonitor,
            this.mnuManagement,
            this.MenuItem4,
            this.mnuSuperAdmin});
            // 
            // mnuApplication
            // 
            this.mnuApplication.Index = 0;
            this.mnuApplication.MdiList = true;
            this.mnuApplication.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuUser,
            this.MenuItemApplicationUser,
            this.mnuSettingApp,
            this.MenuItemApplicationSetting,
            this.mnuUserManagement,
            this.mnuItemUserRight,
            this.mnuUserGroup,
            this.MenuItemApplicationUserManagement,
            this.mnuExit});
            this.mnuApplication.Text = "&Application";
            // 
            // mnuUser
            // 
            this.mnuUser.Index = 0;
            this.mnuUser.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuLogin,
            this.mnuLogOff,
            this.MenuItem15,
            this.mnuPassword});
            this.mnuUser.Text = "&User";
            // 
            // mnuLogin
            // 
            this.mnuLogin.Index = 0;
            this.mnuLogin.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftI;
            this.mnuLogin.Text = "Log&in";
            this.mnuLogin.Click += new System.EventHandler(this.mnuLogin_Click);
            // 
            // mnuLogOff
            // 
            this.mnuLogOff.Index = 1;
            this.mnuLogOff.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftO;
            this.mnuLogOff.Text = "Log&out";
            this.mnuLogOff.Click += new System.EventHandler(this.mnuLogOff_Click);
            // 
            // MenuItem15
            // 
            this.MenuItem15.Index = 2;
            this.MenuItem15.Text = "-";
            // 
            // mnuPassword
            // 
            this.mnuPassword.Index = 3;
            this.mnuPassword.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftC;
            this.mnuPassword.Text = "&Change Password";
            this.mnuPassword.Click += new System.EventHandler(this.mnuPassword_Click);
            // 
            // MenuItemApplicationUser
            // 
            this.MenuItemApplicationUser.Index = 1;
            this.MenuItemApplicationUser.Text = "-";
            // 
            // mnuSettingApp
            // 
            this.mnuSettingApp.Index = 2;
            this.mnuSettingApp.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuDBConn});
            this.mnuSettingApp.Text = "&Setting";
            // 
            // mnuDBConn
            // 
            this.mnuDBConn.Index = 0;
            this.mnuDBConn.Shortcut = System.Windows.Forms.Shortcut.F8;
            this.mnuDBConn.Text = "&Primary Database Connection";
            this.mnuDBConn.Click += new System.EventHandler(this.mnuDBConn_Click);
            // 
            // MenuItemApplicationSetting
            // 
            this.MenuItemApplicationSetting.Index = 3;
            this.MenuItemApplicationSetting.Text = "-";
            // 
            // mnuUserManagement
            // 
            this.mnuUserManagement.Index = 4;
            this.mnuUserManagement.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftM;
            this.mnuUserManagement.Text = "User &Management";
            this.mnuUserManagement.Click += new System.EventHandler(this.mnuUserManagement_Click);
            // 
            // mnuItemUserRight
            // 
            this.mnuItemUserRight.Index = 5;
            this.mnuItemUserRight.Text = "Item &Group Management";
            this.mnuItemUserRight.Visible = false;
            this.mnuItemUserRight.Click += new System.EventHandler(this.mnuItemUserRight_Click);
            // 
            // mnuUserGroup
            // 
            this.mnuUserGroup.Index = 6;
            this.mnuUserGroup.Text = "U&ser Group Management";
            this.mnuUserGroup.Visible = false;
            this.mnuUserGroup.Click += new System.EventHandler(this.mnuUserGroup_Click);
            // 
            // MenuItemApplicationUserManagement
            // 
            this.MenuItemApplicationUserManagement.Index = 7;
            this.MenuItemApplicationUserManagement.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 8;
            this.mnuExit.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftX;
            this.mnuExit.Text = "E&xit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuEdcTerminal
            // 
            this.mnuEdcTerminal.Index = 1;
            this.mnuEdcTerminal.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuBrowse,
            this.menuItemApplicationBrowse,
            this.mnuSettingEdc,
            this.menuItem3,
            this.mnuDataUpload,
            this.menuItemApplicationDataUpload,
            this.mnuAppPackage,
            this.menuItem7,
            this.mnuRemoteDownload,
            this.mnUpdateRD,
            this.menuItem21,
            this.mnLogoEDC});
            this.mnuEdcTerminal.Text = "&EDC Terminal";
            // 
            // mnuBrowse
            // 
            this.mnuBrowse.Index = 0;
            this.mnuBrowse.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSelectVs});
            this.mnuBrowse.Text = "&Browse";
            // 
            // mnuSelectVs
            // 
            this.mnuSelectVs.Index = 0;
            this.mnuSelectVs.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftB;
            this.mnuSelectVs.Text = "&Version Selection";
            this.mnuSelectVs.Click += new System.EventHandler(this.mnuSelectVs_Click);
            // 
            // menuItemApplicationBrowse
            // 
            this.menuItemApplicationBrowse.Index = 1;
            this.menuItemApplicationBrowse.Text = "-";
            // 
            // mnuSettingEdc
            // 
            this.mnuSettingEdc.Index = 2;
            this.mnuSettingEdc.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuAllSN,
            this.mnuAllowInit,
            this.mnuCompressInit,
            this.mnuAutoInit,
            this.mnuLocationProfiling});
            this.mnuSettingEdc.Text = "&Setting";
            // 
            // mnuAllSN
            // 
            this.mnuAllSN.Index = 0;
            this.mnuAllSN.Shortcut = System.Windows.Forms.Shortcut.F9;
            this.mnuAllSN.Text = "&SN Validation";
            this.mnuAllSN.Visible = false;
            this.mnuAllSN.Click += new System.EventHandler(this.mnuAllSN_Click);
            // 
            // mnuAllowInit
            // 
            this.mnuAllowInit.Index = 1;
            this.mnuAllowInit.Shortcut = System.Windows.Forms.Shortcut.F10;
            this.mnuAllowInit.Text = "&Allow Initialize";
            this.mnuAllowInit.Click += new System.EventHandler(this.mnuAllowInit_Click);
            // 
            // mnuCompressInit
            // 
            this.mnuCompressInit.Index = 2;
            this.mnuCompressInit.Shortcut = System.Windows.Forms.Shortcut.F12;
            this.mnuCompressInit.Text = "&Compress Initialize";
            this.mnuCompressInit.Click += new System.EventHandler(this.mnuCompressInit_Click);
            // 
            // mnuAutoInit
            // 
            this.mnuAutoInit.Index = 3;
            this.mnuAutoInit.Shortcut = System.Windows.Forms.Shortcut.F11;
            this.mnuAutoInit.Text = "Auto &Init";
            this.mnuAutoInit.Click += new System.EventHandler(this.mnuAutoInit_Click);
            // 
            // mnuLocationProfiling
            // 
            this.mnuLocationProfiling.Index = 4;
            this.mnuLocationProfiling.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuAddLocation,
            this.mnuLocationView});
            this.mnuLocationProfiling.Text = "&Location Profiling";
            // 
            // mnuAddLocation
            // 
            this.mnuAddLocation.Index = 0;
            this.mnuAddLocation.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftA;
            this.mnuAddLocation.Text = "&Add Location";
            this.mnuAddLocation.Click += new System.EventHandler(this.mnuAddLocation_Click);
            // 
            // mnuLocationView
            // 
            this.mnuLocationView.Index = 1;
            this.mnuLocationView.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftV;
            this.mnuLocationView.Text = "&View Profile Location";
            this.mnuLocationView.Click += new System.EventHandler(this.mnuLocationView_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 3;
            this.menuItem3.Text = "-";
            // 
            // mnuDataUpload
            // 
            this.mnuDataUpload.Index = 4;
            this.mnuDataUpload.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuTemplateDefinition,
            this.mnuUploadDefinition,
            this.mnuUpload});
            this.mnuDataUpload.Text = "&Data Upload";
            // 
            // mnuTemplateDefinition
            // 
            this.mnuTemplateDefinition.Enabled = false;
            this.mnuTemplateDefinition.Index = 0;
            this.mnuTemplateDefinition.Shortcut = System.Windows.Forms.Shortcut.Ctrl1;
            this.mnuTemplateDefinition.Text = "&Template Definition";
            this.mnuTemplateDefinition.Visible = false;
            this.mnuTemplateDefinition.Click += new System.EventHandler(this.mnuTemplateDefinition_Click);
            // 
            // mnuUploadDefinition
            // 
            this.mnuUploadDefinition.Enabled = false;
            this.mnuUploadDefinition.Index = 1;
            this.mnuUploadDefinition.Shortcut = System.Windows.Forms.Shortcut.Ctrl2;
            this.mnuUploadDefinition.Text = "&Upload Definition";
            this.mnuUploadDefinition.Visible = false;
            this.mnuUploadDefinition.Click += new System.EventHandler(this.mnuUploadDefinition_Click);
            // 
            // mnuUpload
            // 
            this.mnuUpload.Index = 2;
            this.mnuUpload.Shortcut = System.Windows.Forms.Shortcut.Ctrl3;
            this.mnuUpload.Text = "U&pload Data";
            this.mnuUpload.Click += new System.EventHandler(this.mnuUpload_Click);
            // 
            // menuItemApplicationDataUpload
            // 
            this.menuItemApplicationDataUpload.Index = 5;
            this.menuItemApplicationDataUpload.Text = "-";
            // 
            // mnuAppPackage
            // 
            this.mnuAppPackage.Index = 6;
            this.mnuAppPackage.Text = "Software &Package";
            this.mnuAppPackage.Visible = false;
            this.mnuAppPackage.Click += new System.EventHandler(this.mnuAppPackage_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 7;
            this.menuItem7.Text = "-";
            this.menuItem7.Visible = false;
            // 
            // mnuRemoteDownload
            // 
            this.mnuRemoteDownload.Index = 8;
            this.mnuRemoteDownload.Text = "Remote Download";
            this.mnuRemoteDownload.Click += new System.EventHandler(this.mnuRemoteDownload_Click);
            // 
            // mnUpdateRD
            // 
            this.mnUpdateRD.Index = 9;
            this.mnUpdateRD.Text = "Update Remote Download";
            this.mnUpdateRD.Click += new System.EventHandler(this.mnUpdateRD_Click);
            // 
            // menuItem21
            // 
            this.menuItem21.Index = 10;
            this.menuItem21.Text = "-";
            // 
            // mnLogoEDC
            // 
            this.mnLogoEDC.Index = 11;
            this.mnLogoEDC.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnLogoEDCPrint,
            this.mnLogoEDCIdle});
            this.mnLogoEDC.Text = "Logo EDC";
            // 
            // mnLogoEDCPrint
            // 
            this.mnLogoEDCPrint.Index = 0;
            this.mnLogoEDCPrint.Text = "&Print";
            this.mnLogoEDCPrint.Click += new System.EventHandler(this.mnLogoEDCPrint_Click);
            // 
            // mnLogoEDCIdle
            // 
            this.mnLogoEDCIdle.Index = 1;
            this.mnLogoEDCIdle.Text = "&Idle";
            this.mnLogoEDCIdle.Click += new System.EventHandler(this.mnLogoEDCIdle_Click);
            // 
            // mnuReport
            // 
            this.mnuReport.Index = 2;
            this.mnuReport.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuAuditTrail,
            this.mnuInitializeTrail,
            this.mnuSoftwareInitTrail,
            this.mnuTLV,
            this.mnuQueryReport});
            this.mnuReport.Text = "&Report";
            // 
            // mnuAuditTrail
            // 
            this.mnuAuditTrail.Index = 0;
            this.mnuAuditTrail.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftM;
            this.mnuAuditTrail.Text = "&Audit Trail";
            this.mnuAuditTrail.Click += new System.EventHandler(this.mnuAuditTrail_Click);
            // 
            // mnuInitializeTrail
            // 
            this.mnuInitializeTrail.Index = 1;
            this.mnuInitializeTrail.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftN;
            this.mnuInitializeTrail.Text = "&Initialize Trail";
            this.mnuInitializeTrail.Click += new System.EventHandler(this.mnuInitializeTrail_Click);
            // 
            // mnuSoftwareInitTrail
            // 
            this.mnuSoftwareInitTrail.Index = 2;
            this.mnuSoftwareInitTrail.Text = "Software Init Trail";
            this.mnuSoftwareInitTrail.Visible = false;
            this.mnuSoftwareInitTrail.Click += new System.EventHandler(this.mnuSoftwareInitTrail_Click);
            // 
            // mnuTLV
            // 
            this.mnuTLV.Index = 3;
            this.mnuTLV.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftP;
            this.mnuTLV.Text = "&Populate TLV Database";
            this.mnuTLV.Click += new System.EventHandler(this.mnuTLV_Click);
            // 
            // mnuQueryReport
            // 
            this.mnuQueryReport.Index = 4;
            this.mnuQueryReport.Text = "&Query Report";
            this.mnuQueryReport.Click += new System.EventHandler(this.mnuQueryReport_Click);
            // 
            // mnuMonitor
            // 
            this.mnuMonitor.Index = 3;
            this.mnuMonitor.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuStartListen,
            this.mnuEDCTracking,
            this.mnuSerialDownload,
            this.mnSoftwareProgress,
            this.mnEchoPing});
            this.mnuMonitor.Text = "&Utilities";
            // 
            // mnuStartListen
            // 
            this.mnuStartListen.Index = 0;
            this.mnuStartListen.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftU;
            this.mnuStartListen.Text = "&Start United";
            this.mnuStartListen.Click += new System.EventHandler(this.mnuStartListen_Click);
            // 
            // mnuEDCTracking
            // 
            this.mnuEDCTracking.Enabled = false;
            this.mnuEDCTracking.Index = 1;
            this.mnuEDCTracking.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftT;
            this.mnuEDCTracking.Text = "&EDC IP-Tracking";
            this.mnuEDCTracking.Visible = false;
            this.mnuEDCTracking.Click += new System.EventHandler(this.mnuEDC_Click);
            // 
            // mnuSerialDownload
            // 
            this.mnuSerialDownload.Enabled = false;
            this.mnuSerialDownload.Index = 2;
            this.mnuSerialDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftD;
            this.mnuSerialDownload.Text = "&M5100/X1000";
            this.mnuSerialDownload.Visible = false;
            this.mnuSerialDownload.Click += new System.EventHandler(this.mnuSerialDownload_Click);
            // 
            // mnSoftwareProgress
            // 
            this.mnSoftwareProgress.Index = 3;
            this.mnSoftwareProgress.Text = "Init Software Progress";
            this.mnSoftwareProgress.Visible = false;
            this.mnSoftwareProgress.Click += new System.EventHandler(this.mnuSoftwareProgress_Click);
            // 
            // mnEchoPing
            // 
            this.mnEchoPing.Enabled = false;
            this.mnEchoPing.Index = 4;
            this.mnEchoPing.Text = "Echo Ping";
            this.mnEchoPing.Visible = false;
            this.mnEchoPing.Click += new System.EventHandler(this.mnuEchoPing_Click);
            // 
            // mnuManagement
            // 
            this.mnuManagement.Index = 4;
            this.mnuManagement.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem8,
            this.menuItem9,
            this.menuItem10,
            this.menuItem11,
            this.menuItem12,
            this.menuItem13,
            this.menuItem14,
            this.menuItem16,
            this.menuItem17,
            this.menuItem18,
            this.menuItem19,
            this.menuItem20});
            this.mnuManagement.Text = "Management";
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 0;
            this.menuItem8.Text = "Card";
            // 
            // menuItem9
            // 
            this.menuItem9.Index = 1;
            this.menuItem9.Text = "AID";
            // 
            // menuItem10
            // 
            this.menuItem10.Index = 2;
            this.menuItem10.Text = "Public Key";
            // 
            // menuItem11
            // 
            this.menuItem11.Index = 3;
            this.menuItem11.Text = "TLE";
            // 
            // menuItem12
            // 
            this.menuItem12.Index = 4;
            this.menuItem12.Text = "Loyalty";
            // 
            // menuItem13
            // 
            this.menuItem13.Index = 5;
            this.menuItem13.Text = "Loyalty Product";
            // 
            // menuItem14
            // 
            this.menuItem14.Index = 6;
            this.menuItem14.Text = "GPRS/Ethernet";
            // 
            // menuItem16
            // 
            this.menuItem16.Index = 7;
            this.menuItem16.Text = "Currency";
            // 
            // menuItem17
            // 
            this.menuItem17.Index = 8;
            this.menuItem17.Text = "PIN Pad";
            // 
            // menuItem18
            // 
            this.menuItem18.Index = 9;
            this.menuItem18.Text = "Remote Download";
            // 
            // menuItem19
            // 
            this.menuItem19.Index = 10;
            this.menuItem19.Text = "Bank Code";
            // 
            // menuItem20
            // 
            this.menuItem20.Index = 11;
            this.menuItem20.Text = "Product Code";
            // 
            // MenuItem4
            // 
            this.MenuItem4.Index = 5;
            this.MenuItem4.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuHelp,
            this.mnuRegister,
            this.MenuItemHelp,
            this.mnuAbout});
            this.MenuItem4.Text = "&Help";
            // 
            // mnuHelp
            // 
            this.mnuHelp.Index = 0;
            this.mnuHelp.Shortcut = System.Windows.Forms.Shortcut.F1;
            this.mnuHelp.Text = "&Help";
            this.mnuHelp.Click += new System.EventHandler(this.mnuHelp_Click);
            // 
            // mnuRegister
            // 
            this.mnuRegister.Enabled = false;
            this.mnuRegister.Index = 1;
            this.mnuRegister.Text = "Register";
            this.mnuRegister.Click += new System.EventHandler(this.mnuRegister_Click);
            // 
            // MenuItemHelp
            // 
            this.MenuItemHelp.Index = 2;
            this.MenuItemHelp.Text = "-";
            // 
            // mnuAbout
            // 
            this.mnuAbout.Index = 3;
            this.mnuAbout.Text = "&About";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // mnuSuperAdmin
            // 
            this.mnuSuperAdmin.Index = 6;
            this.mnuSuperAdmin.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUDatabase,
            this.mnuSUModify,
            this.menuItem6,
            this.mnuSUApplicationMgmt,
            this.mnuSUSwProfile,
            this.menuItem2,
            this.mnuSUInitConn,
            this.mnuSUSetFolder,
            this.menuItem1,
            this.mnuSUAudit,
            this.mnuSUTag5,
            this.mnuSUQueryReport});
            this.mnuSuperAdmin.Text = "&Super User";
            // 
            // mnuSUDatabase
            // 
            this.mnuSUDatabase.Index = 0;
            this.mnuSUDatabase.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUVersion,
            this.mnuSUTagItem,
            this.mnuSUCopyCardRange,
            this.mnuSUCopyGPRS,
            this.mnuSUCopyTLE,
            this.mnSUCopyAID,
            this.mnSUCopyCAPK,
            this.mnCopyRD,
            this.mnuSUCard});
            this.mnuSUDatabase.Text = "&Database";
            // 
            // mnuSUVersion
            // 
            this.mnuSUVersion.Index = 0;
            this.mnuSUVersion.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUVersion_Add,
            this.mnuSUVersion_Edit,
            this.mnuSUVersion_Delete});
            this.mnuSUVersion.Text = "&Version";
            // 
            // mnuSUVersion_Add
            // 
            this.mnuSUVersion_Add.Index = 0;
            this.mnuSUVersion_Add.Text = "&Add";
            this.mnuSUVersion_Add.Click += new System.EventHandler(this.mnuSUVersion_Add_Click);
            // 
            // mnuSUVersion_Edit
            // 
            this.mnuSUVersion_Edit.Index = 1;
            this.mnuSUVersion_Edit.Text = "Edit";
            this.mnuSUVersion_Edit.Click += new System.EventHandler(this.mnuSUVersion_Edit_Click);
            // 
            // mnuSUVersion_Delete
            // 
            this.mnuSUVersion_Delete.Index = 2;
            this.mnuSUVersion_Delete.Text = "&Delete";
            this.mnuSUVersion_Delete.Click += new System.EventHandler(this.mnuSUVersion_Delete_Click);
            // 
            // mnuSUTagItem
            // 
            this.mnuSUTagItem.Index = 1;
            this.mnuSUTagItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUTagItem_AddUpdate,
            this.mnuSUTagItem_Delete,
            this.menuItem5,
            this.mnuSUTagItemExport,
            this.mnuSUTagItemImport});
            this.mnuSUTagItem.Text = "Tag / &Item";
            // 
            // mnuSUTagItem_AddUpdate
            // 
            this.mnuSUTagItem_AddUpdate.Index = 0;
            this.mnuSUTagItem_AddUpdate.Text = "&Add / Update";
            this.mnuSUTagItem_AddUpdate.Click += new System.EventHandler(this.mnuSUTagItem_AddUpdate_Click);
            // 
            // mnuSUTagItem_Delete
            // 
            this.mnuSUTagItem_Delete.Index = 1;
            this.mnuSUTagItem_Delete.Text = "&Delete";
            this.mnuSUTagItem_Delete.Click += new System.EventHandler(this.mnuSUTagItem_Delete_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 2;
            this.menuItem5.Text = "-";
            // 
            // mnuSUTagItemExport
            // 
            this.mnuSUTagItemExport.Index = 3;
            this.mnuSUTagItemExport.Text = "&Export";
            this.mnuSUTagItemExport.Click += new System.EventHandler(this.mnuSUTagItemExport_Click);
            // 
            // mnuSUTagItemImport
            // 
            this.mnuSUTagItemImport.Index = 4;
            this.mnuSUTagItemImport.Text = "&Import";
            this.mnuSUTagItemImport.Click += new System.EventHandler(this.mnuSUTagItemImport_Click);
            // 
            // mnuSUCopyCardRange
            // 
            this.mnuSUCopyCardRange.Index = 2;
            this.mnuSUCopyCardRange.Text = "Copy &Card Range";
            this.mnuSUCopyCardRange.Click += new System.EventHandler(this.mnuSUCopyCardRange_Click);
            // 
            // mnuSUCopyGPRS
            // 
            this.mnuSUCopyGPRS.Index = 3;
            this.mnuSUCopyGPRS.Text = "Copy &GPRS";
            this.mnuSUCopyGPRS.Click += new System.EventHandler(this.mnuSUCopyGPRS_Click);
            // 
            // mnuSUCopyTLE
            // 
            this.mnuSUCopyTLE.Index = 4;
            this.mnuSUCopyTLE.Text = "Copy &TLE";
            this.mnuSUCopyTLE.Click += new System.EventHandler(this.mnuSUCopyTLE_Click);
            // 
            // mnSUCopyAID
            // 
            this.mnSUCopyAID.Index = 5;
            this.mnSUCopyAID.Text = "Copy AID";
            this.mnSUCopyAID.Click += new System.EventHandler(this.mnSUCopyAID_Click);
            // 
            // mnSUCopyCAPK
            // 
            this.mnSUCopyCAPK.Index = 6;
            this.mnSUCopyCAPK.Text = "Copy CAPK";
            this.mnSUCopyCAPK.Click += new System.EventHandler(this.mnSUCopyCAPK_Click);
            // 
            // mnCopyRD
            // 
            this.mnCopyRD.Index = 7;
            this.mnCopyRD.Text = "Copy Remote Download";
            this.mnCopyRD.Click += new System.EventHandler(this.mnCopyRD_Click);
            // 
            // mnuSUCard
            // 
            this.mnuSUCard.Index = 8;
            this.mnuSUCard.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUImportCard,
            this.mnuSUExportCard});
            this.mnuSUCard.Text = "C&ard";
            // 
            // mnuSUImportCard
            // 
            this.mnuSUImportCard.Index = 0;
            this.mnuSUImportCard.Text = "&Import Card";
            this.mnuSUImportCard.Click += new System.EventHandler(this.mnuSUImportCard_Click);
            // 
            // mnuSUExportCard
            // 
            this.mnuSUExportCard.Index = 1;
            this.mnuSUExportCard.Text = "&Export Card";
            this.mnuSUExportCard.Click += new System.EventHandler(this.mnuSUExportCard_Click);
            // 
            // mnuSUModify
            // 
            this.mnuSUModify.Index = 1;
            this.mnuSUModify.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUData,
            this.mnuSUTable,
            this.mnuSUFields});
            this.mnuSUModify.Text = "&Modify";
            // 
            // mnuSUData
            // 
            this.mnuSUData.Index = 0;
            this.mnuSUData.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUImportData,
            this.mnuSUExportData,
            this.mnuSUMoveData,
            this.mnuSUCopyData});
            this.mnuSUData.Text = "&Data...";
            // 
            // mnuSUImportData
            // 
            this.mnuSUImportData.Index = 0;
            this.mnuSUImportData.Text = "&Import Text File to Profile";
            this.mnuSUImportData.Click += new System.EventHandler(this.mnuSUImportData_Click);
            // 
            // mnuSUExportData
            // 
            this.mnuSUExportData.Index = 1;
            this.mnuSUExportData.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUExportToTxtFile,
            this.mnuSUExportToDb});
            this.mnuSUExportData.Text = "&Export Profile to...";
            // 
            // mnuSUExportToTxtFile
            // 
            this.mnuSUExportToTxtFile.Index = 0;
            this.mnuSUExportToTxtFile.Text = "&Text File";
            this.mnuSUExportToTxtFile.Click += new System.EventHandler(this.mnuSUExportToTxtFile_Click);
            // 
            // mnuSUExportToDb
            // 
            this.mnuSUExportToDb.Index = 1;
            this.mnuSUExportToDb.Text = "&Database";
            this.mnuSUExportToDb.Click += new System.EventHandler(this.mnuSUExportToDb_Click);
            // 
            // mnuSUMoveData
            // 
            this.mnuSUMoveData.Index = 2;
            this.mnuSUMoveData.Text = "&Move Data";
            this.mnuSUMoveData.Visible = false;
            this.mnuSUMoveData.Click += new System.EventHandler(this.mnuMoveData_Click);
            // 
            // mnuSUCopyData
            // 
            this.mnuSUCopyData.Index = 3;
            this.mnuSUCopyData.Text = "&Copy Data";
            this.mnuSUCopyData.Click += new System.EventHandler(this.mnuSUCopyData_Click);
            // 
            // mnuSUTable
            // 
            this.mnuSUTable.Enabled = false;
            this.mnuSUTable.Index = 1;
            this.mnuSUTable.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUAddTable,
            this.mnuSUDeleteTable});
            this.mnuSUTable.Text = "Table...";
            this.mnuSUTable.Visible = false;
            // 
            // mnuSUAddTable
            // 
            this.mnuSUAddTable.Index = 0;
            this.mnuSUAddTable.Text = "Add Table";
            this.mnuSUAddTable.Click += new System.EventHandler(this.mnuSUAddTable_Click);
            // 
            // mnuSUDeleteTable
            // 
            this.mnuSUDeleteTable.Index = 1;
            this.mnuSUDeleteTable.Text = "Delete Table";
            this.mnuSUDeleteTable.Click += new System.EventHandler(this.mnuSUDeleteTable_Click);
            // 
            // mnuSUFields
            // 
            this.mnuSUFields.Enabled = false;
            this.mnuSUFields.Index = 2;
            this.mnuSUFields.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUAddFields,
            this.mnuSUUpdateFields,
            this.mnuSUDeleteFields});
            this.mnuSUFields.Text = "Fields...";
            this.mnuSUFields.Visible = false;
            // 
            // mnuSUAddFields
            // 
            this.mnuSUAddFields.Index = 0;
            this.mnuSUAddFields.Text = "Add Fields";
            this.mnuSUAddFields.Click += new System.EventHandler(this.mnuSUAddFields_Click);
            // 
            // mnuSUUpdateFields
            // 
            this.mnuSUUpdateFields.Index = 1;
            this.mnuSUUpdateFields.Text = "Update Fields";
            this.mnuSUUpdateFields.Click += new System.EventHandler(this.mnuSUUpdateFields_Click);
            // 
            // mnuSUDeleteFields
            // 
            this.mnuSUDeleteFields.Index = 2;
            this.mnuSUDeleteFields.Text = "Delete Fields";
            this.mnuSUDeleteFields.Click += new System.EventHandler(this.mnuSUDeleteFields_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 2;
            this.menuItem6.Text = "-";
            // 
            // mnuSUApplicationMgmt
            // 
            this.mnuSUApplicationMgmt.Index = 3;
            this.mnuSUApplicationMgmt.Text = "&Application";
            this.mnuSUApplicationMgmt.Click += new System.EventHandler(this.mnuSUApplicationMgmt_Click);
            // 
            // mnuSUSwProfile
            // 
            this.mnuSUSwProfile.Index = 4;
            this.mnuSUSwProfile.Text = "&Software Profile";
            this.mnuSUSwProfile.Click += new System.EventHandler(this.mnuSUSwProfile_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 5;
            this.menuItem2.Text = "-";
            // 
            // mnuSUInitConn
            // 
            this.mnuSUInitConn.Index = 6;
            this.mnuSUInitConn.Text = "&Init Connection";
            this.mnuSUInitConn.Click += new System.EventHandler(this.mnuSUInitConn_Click);
            // 
            // mnuSUSetFolder
            // 
            this.mnuSUSetFolder.Index = 7;
            this.mnuSUSetFolder.Text = "&Folder Path";
            this.mnuSUSetFolder.Click += new System.EventHandler(this.mnuSUSetFolder_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 8;
            this.menuItem1.Text = "-";
            // 
            // mnuSUAudit
            // 
            this.mnuSUAudit.Index = 9;
            this.mnuSUAudit.Text = "Audit &Trail";
            this.mnuSUAudit.Click += new System.EventHandler(this.mnuSUAudit_Click);
            // 
            // mnuSUTag5
            // 
            this.mnuSUTag5.Index = 10;
            this.mnuSUTag5.Text = "Tag Fi&ve";
            this.mnuSUTag5.Click += new System.EventHandler(this.mnuSUTag5_Click);
            // 
            // mnuSUQueryReport
            // 
            this.mnuSUQueryReport.Index = 11;
            this.mnuSUQueryReport.Text = "Query Report";
            this.mnuSUQueryReport.Click += new System.EventHandler(this.mnuQueryReport_Click);
            // 
            // StatusBarApplication
            // 
            this.StatusBarApplication.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.StatusBarApplication.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusBarApplication.Icon")));
            this.StatusBarApplication.Name = "StatusBarApplication";
            this.StatusBarApplication.Width = 570;
            // 
            // StatusBarTime
            // 
            this.StatusBarTime.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.StatusBarTime.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusBarTime.Icon")));
            this.StatusBarTime.Name = "StatusBarTime";
            this.StatusBarTime.Text = "waktu";
            this.StatusBarTime.Width = 71;
            // 
            // StatusBarDate
            // 
            this.StatusBarDate.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.StatusBarDate.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusBarDate.Icon")));
            this.StatusBarDate.Name = "StatusBarDate";
            this.StatusBarDate.Text = "tanggal";
            this.StatusBarDate.Width = 79;
            // 
            // TimerClock
            // 
            this.TimerClock.Enabled = true;
            this.TimerClock.Tick += new System.EventHandler(this.TimerClock_Tick);
            // 
            // ToolBarMenu
            // 
            this.ToolBarMenu.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.ToolBarMenu.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.ToolBarLogin,
            this.ToolBarLogOff,
            this.ToolBarButton5,
            this.ToolBarExit,
            this.ToolBarButton1,
            this.ToolBarAudit,
            this.ToolBarButton2,
            this.ToolBarSelectVs,
            this.ToolBarButton3,
            this.ToolBarUnited,
            this.ToolBarButton4,
            this.ToolBarDownload});
            this.ToolBarMenu.DropDownArrows = true;
            this.ToolBarMenu.ImageList = this.ImageIcon;
            this.ToolBarMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolBarMenu.Name = "ToolBarMenu";
            this.ToolBarMenu.ShowToolTips = true;
            this.ToolBarMenu.Size = new System.Drawing.Size(720, 42);
            this.ToolBarMenu.TabIndex = 5;
            this.ToolBarMenu.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.ToolBarMenu_ButtonClick);
            // 
            // ToolBarLogin
            // 
            this.ToolBarLogin.ImageIndex = 0;
            this.ToolBarLogin.Name = "ToolBarLogin";
            this.ToolBarLogin.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarLogin.ToolTipText = "Login";
            // 
            // ToolBarLogOff
            // 
            this.ToolBarLogOff.ImageIndex = 1;
            this.ToolBarLogOff.Name = "ToolBarLogOff";
            this.ToolBarLogOff.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarLogOff.ToolTipText = "Logout";
            // 
            // ToolBarButton5
            // 
            this.ToolBarButton5.Name = "ToolBarButton5";
            this.ToolBarButton5.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarExit
            // 
            this.ToolBarExit.ImageIndex = 13;
            this.ToolBarExit.Name = "ToolBarExit";
            this.ToolBarExit.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarExit.ToolTipText = "Exit";
            // 
            // ToolBarButton1
            // 
            this.ToolBarButton1.Name = "ToolBarButton1";
            this.ToolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarAudit
            // 
            this.ToolBarAudit.ImageIndex = 14;
            this.ToolBarAudit.Name = "ToolBarAudit";
            this.ToolBarAudit.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarAudit.ToolTipText = "Audit Trail";
            // 
            // ToolBarButton2
            // 
            this.ToolBarButton2.Name = "ToolBarButton2";
            this.ToolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarSelectVs
            // 
            this.ToolBarSelectVs.ImageIndex = 10;
            this.ToolBarSelectVs.Name = "ToolBarSelectVs";
            this.ToolBarSelectVs.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarSelectVs.ToolTipText = "Version Selection";
            // 
            // ToolBarButton3
            // 
            this.ToolBarButton3.Name = "ToolBarButton3";
            this.ToolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarUnited
            // 
            this.ToolBarUnited.ImageIndex = 12;
            this.ToolBarUnited.Name = "ToolBarUnited";
            this.ToolBarUnited.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarUnited.ToolTipText = "Start United";
            // 
            // ToolBarButton4
            // 
            this.ToolBarButton4.Name = "ToolBarButton4";
            this.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            this.ToolBarButton4.Visible = false;
            // 
            // ToolBarDownload
            // 
            this.ToolBarDownload.ImageIndex = 4;
            this.ToolBarDownload.Name = "ToolBarDownload";
            this.ToolBarDownload.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarDownload.ToolTipText = "Download";
            this.ToolBarDownload.Visible = false;
            // 
            // ImageIcon
            // 
            this.ImageIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageIcon.ImageStream")));
            this.ImageIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageIcon.Images.SetKeyName(0, "");
            this.ImageIcon.Images.SetKeyName(1, "");
            this.ImageIcon.Images.SetKeyName(2, "");
            this.ImageIcon.Images.SetKeyName(3, "");
            this.ImageIcon.Images.SetKeyName(4, "");
            this.ImageIcon.Images.SetKeyName(5, "");
            this.ImageIcon.Images.SetKeyName(6, "");
            this.ImageIcon.Images.SetKeyName(7, "");
            this.ImageIcon.Images.SetKeyName(8, "");
            this.ImageIcon.Images.SetKeyName(9, "exit(1).ico");
            this.ImageIcon.Images.SetKeyName(10, "database(1).ico");
            this.ImageIcon.Images.SetKeyName(11, "clipboard_audit.ico");
            this.ImageIcon.Images.SetKeyName(12, "Monitor.ico");
            this.ImageIcon.Images.SetKeyName(13, "stock_quit.ico");
            this.ImageIcon.Images.SetKeyName(14, "Document.ico");
            // 
            // StatusBar1
            // 
            this.StatusBar1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBar1.Location = new System.Drawing.Point(0, 293);
            this.StatusBar1.Name = "StatusBar1";
            this.StatusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.StatusBarApplication,
            this.StatusBarTime,
            this.StatusBarDate});
            this.StatusBar1.ShowPanels = true;
            this.StatusBar1.Size = new System.Drawing.Size(720, 26);
            this.StatusBar1.SizingGrip = false;
            this.StatusBar1.TabIndex = 4;
            this.StatusBar1.Text = "StatusBar1";
            // 
            // FrmMainMenu
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(720, 319);
            this.Controls.Add(this.ToolBarMenu);
            this.Controls.Add(this.StatusBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Menu = this.mnuMain;
            this.Name = "FrmMainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InSys";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMainMenu_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMainMenu_Closed);
            this.Load += new System.EventHandler(this.FrmMainMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarApplication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.MenuItem mnuApplication;
        internal System.Windows.Forms.MenuItem mnuLogin;
        internal System.Windows.Forms.MenuItem mnuLogOff;
        internal System.Windows.Forms.MenuItem MenuItem15;
        internal System.Windows.Forms.MenuItem mnuPassword;
        internal System.Windows.Forms.MenuItem MenuItemApplicationUser;
        internal System.Windows.Forms.MenuItem mnuDBConn;
        internal System.Windows.Forms.MenuItem mnuAllowInit;
        internal System.Windows.Forms.MenuItem mnuAllSN;
        internal System.Windows.Forms.MenuItem MenuItemApplicationSetting;
        internal System.Windows.Forms.MenuItem mnuUserManagement;
        internal System.Windows.Forms.MenuItem mnuTLV;
        internal System.Windows.Forms.MenuItem mnuExit;
        internal System.Windows.Forms.MenuItem mnuEDCTracking;
        internal System.Windows.Forms.MenuItem mnuMonitor;
        internal System.Windows.Forms.MenuItem mnuStartListen;
        internal System.Windows.Forms.MenuItem MenuItem4;
        internal System.Windows.Forms.MenuItem mnuHelp;
        internal System.Windows.Forms.MenuItem MenuItemHelp;
        internal System.Windows.Forms.MenuItem mnuAbout;
        internal System.Windows.Forms.MenuItem mnuSuperAdmin;
        internal System.Windows.Forms.MenuItem mnuRegister;
        internal System.Windows.Forms.StatusBarPanel StatusBarApplication;
        internal System.Windows.Forms.StatusBarPanel StatusBarTime;
        internal System.Windows.Forms.StatusBarPanel StatusBarDate;
        internal System.Windows.Forms.Timer TimerClock;
        internal System.Windows.Forms.ToolBar ToolBarMenu;
        internal System.Windows.Forms.ToolBarButton ToolBarLogin;
        internal System.Windows.Forms.ToolBarButton ToolBarLogOff;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator1;
        internal System.Windows.Forms.ToolBarButton ToolBarExit;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator2;
        internal System.Windows.Forms.ToolBarButton ToolBarAudit;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator3;
        internal System.Windows.Forms.ToolBarButton ToolBarSelectVs;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator4;
        internal System.Windows.Forms.ToolBarButton ToolBarUnited;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator5;
        internal System.Windows.Forms.ToolBarButton ToolBarDownload;
        internal System.Windows.Forms.ImageList ImageIcon;
        internal System.Windows.Forms.StatusBar StatusBar1;
        private System.Windows.Forms.MenuItem mnuDataUpload;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem mnuSUModify;
        private System.Windows.Forms.MenuItem mnuSUTable;
        private System.Windows.Forms.MenuItem mnuSUAddTable;
        private System.Windows.Forms.MenuItem mnuSUDeleteTable;
        private System.Windows.Forms.MenuItem mnuSUFields;
        private System.Windows.Forms.MenuItem mnuSUAddFields;
        private System.Windows.Forms.MenuItem mnuSUUpdateFields;
        private System.Windows.Forms.MenuItem mnuSUDeleteFields;
        private System.Windows.Forms.MenuItem mnuSUData;
        private System.Windows.Forms.MenuItem mnuSUImportData;
        private System.Windows.Forms.MenuItem mnuSUExportData;
        private System.Windows.Forms.MenuItem mnuSUExportToTxtFile;
        private System.Windows.Forms.MenuItem mnuSUExportToDb;
        private System.Windows.Forms.MenuItem mnuBrowse;
        private System.Windows.Forms.MenuItem mnuSelectVs;
        private System.Windows.Forms.MenuItem MenuItemApplicationUserManagement;
        internal System.Windows.Forms.MenuItem mnuSerialDownload;
        internal System.Windows.Forms.ToolBarButton ToolBarButton5;
        internal System.Windows.Forms.ToolBarButton ToolBarButton1;
        internal System.Windows.Forms.ToolBarButton ToolBarButton2;
        internal System.Windows.Forms.ToolBarButton ToolBarButton3;
        internal System.Windows.Forms.ToolBarButton ToolBarButton4;
        private System.Windows.Forms.MenuItem mnuUser;
        private System.Windows.Forms.MenuItem mnuSettingApp;
        internal System.Windows.Forms.MenuItem mnuAuditTrail;
        internal System.Windows.Forms.MenuItem mnuInitializeTrail;
        internal System.Windows.Forms.MenuItem mnuTemplateDefinition;
        internal System.Windows.Forms.MenuItem mnuUploadDefinition;
        internal System.Windows.Forms.MenuItem mnuUpload;
        internal System.Windows.Forms.MainMenu mnuMain;
        private System.Windows.Forms.MenuItem mnuLocationProfiling;
        private System.Windows.Forms.MenuItem mnuLocationView;
        private System.Windows.Forms.MenuItem mnuAddLocation;
        private System.Windows.Forms.MenuItem mnuSUDatabase;
        private System.Windows.Forms.MenuItem mnuSUVersion;
        private System.Windows.Forms.MenuItem mnuSUCopyCardRange;
        private System.Windows.Forms.MenuItem mnuSUTagItem;
        private System.Windows.Forms.MenuItem mnuSUTagItem_AddUpdate;
        private System.Windows.Forms.MenuItem mnuSUTagItem_Delete;
        private System.Windows.Forms.MenuItem mnuSUVersion_Add;
        private System.Windows.Forms.MenuItem mnuSUVersion_Delete;
        private System.Windows.Forms.MenuItem mnuCompressInit;
        private System.Windows.Forms.MenuItem mnuAutoInit;
        private System.Windows.Forms.MenuItem mnuSUSetFolder;
        private System.Windows.Forms.MenuItem mnuSUApplicationMgmt;
        private System.Windows.Forms.MenuItem mnuEdcTerminal;
        private System.Windows.Forms.MenuItem menuItemApplicationBrowse;
        private System.Windows.Forms.MenuItem mnuReport;
        private System.Windows.Forms.MenuItem mnuSettingEdc;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItemApplicationDataUpload;
        private System.Windows.Forms.MenuItem mnuSUInitConn;
        private System.Windows.Forms.MenuItem mnuSUSwProfile;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem mnuSUAudit;
        internal System.Windows.Forms.MenuItem mnuUserGroup;
        internal System.Windows.Forms.MenuItem mnuItemUserRight;
        private System.Windows.Forms.MenuItem menuItem5;
        private System.Windows.Forms.MenuItem mnuSUTagItemExport;
        private System.Windows.Forms.MenuItem mnuSUTagItemImport;
        private System.Windows.Forms.MenuItem mnuSUCopyGPRS;
        private System.Windows.Forms.MenuItem mnuSUCopyTLE;
        private System.Windows.Forms.MenuItem mnuSUTag5;
        private System.Windows.Forms.MenuItem mnuSUVersion_Edit;
        private System.Windows.Forms.MenuItem mnuSoftwareInitTrail;
        private System.Windows.Forms.MenuItem menuItem7;
        private System.Windows.Forms.MenuItem mnuSUQueryReport;
        private System.Windows.Forms.MenuItem mnuSUMoveData;
        private System.Windows.Forms.MenuItem mnuQueryReport;
        private System.Windows.Forms.MenuItem mnSoftwareProgress;
        private System.Windows.Forms.MenuItem mnEchoPing;
        private System.Windows.Forms.MenuItem mnuSUCard;
        private System.Windows.Forms.MenuItem mnuSUImportCard;
        private System.Windows.Forms.MenuItem mnuSUExportCard;
        private System.Windows.Forms.MenuItem mnuSUCopyData;
        private System.Windows.Forms.MenuItem mnUpdateRD;
        private System.Windows.Forms.MenuItem mnuAppPackage;
        private System.Windows.Forms.MenuItem mnuRemoteDownload;
        private System.Windows.Forms.MenuItem menuItem8;
        private System.Windows.Forms.MenuItem menuItem9;
        private System.Windows.Forms.MenuItem menuItem10;
        private System.Windows.Forms.MenuItem menuItem11;
        private System.Windows.Forms.MenuItem menuItem12;
        private System.Windows.Forms.MenuItem menuItem13;
        private System.Windows.Forms.MenuItem menuItem14;
        private System.Windows.Forms.MenuItem menuItem16;
        private System.Windows.Forms.MenuItem menuItem17;
        private System.Windows.Forms.MenuItem menuItem18;
        private System.Windows.Forms.MenuItem menuItem19;
        private System.Windows.Forms.MenuItem menuItem20;
        public System.Windows.Forms.MenuItem mnuManagement;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.MenuItem mnSUCopyAID;
        private System.Windows.Forms.MenuItem mnSUCopyCAPK;
        private System.Windows.Forms.MenuItem mnCopyRD;
        private System.Windows.Forms.MenuItem mnLogoEDC;
        private System.Windows.Forms.MenuItem mnLogoEDCPrint;
        private System.Windows.Forms.MenuItem mnLogoEDCIdle;
        private System.Windows.Forms.MenuItem menuItem21;
    }
}