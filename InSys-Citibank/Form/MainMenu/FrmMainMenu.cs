using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Globalization;
using InSysClass;

namespace InSys
{
    /// <summary>
    /// Main window
    /// </summary>
    public partial class FrmMainMenu : Form
    {
        private SqlConnection oSqlConn;
        private SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Main window constructor
        /// </summary>
        public FrmMainMenu()
        {
            InitializeComponent();
            DisplayDateTime();
            DisplayAll(false);
        }

        /// <summary>
        /// Runs while form is loading, set connection for application.
        /// Check current license and call form Login.
        /// </summary>
        private void FrmMainMenu_Load(object sender, EventArgs e)
        {
            this.Visible = true;

            //InitConnection();

            //CreateLicense();
            //doCheckLicense();
            //GetAuditTrailConnection();
            doLogin();
        }

        /// <summary>
        /// Dispose the form when form is closed.
        /// </summary>
        private void FrmMainMenu_Closed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
        }

        /// <summary>
        /// When form is closing, Application confirms to user if user want to closed the apps.
        /// If yes, close all forms in application and insert log to Audit Trail Log.
        /// </summary>
        private void FrmMainMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            CommonClass.UserAccessDelete(oSqlConn, UserData.sTerminalIdActive);
            if (e.CloseReason == CloseReason.UserClosing && CommonClass.isYesMessage(CommonMessage.sExitMessageText, CommonMessage.sExitMessageTitle))
            {
                if (!string.IsNullOrEmpty(UserData.sUserID)) //hanya write log jika ada yang sedang login
                    //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sLogoutSuccess, "");
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sLogoutSuccess, "");
                Application.ExitThread();
            }
            else if (e.CloseReason != CloseReason.ApplicationExitCall)
                e.Cancel = true;
        }

        /// <summary>
        /// If ToolBar menu is clicked, runs function based on their indexes.
        /// </summary>
        private void ToolBarMenu_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            int iButton = ToolBarMenu.Buttons.IndexOf(e.Button);
            ToolBarMenu.Buttons[iButton].Pushed = false;
            switch (iButton)
            {
                case 0: mnuLogin.PerformClick(); break;
                case 1: mnuLogOff.PerformClick(); break;
                case 3: mnuExit.PerformClick(); break;
                case 5: mnuAuditTrail.PerformClick(); break;
                case 7: mnuSelectVs.PerformClick(); break;
                case 9: mnuStartListen.PerformClick(); break;
                case 11: mnuSerialDownload.PerformClick(); break;
            }

        }

        /// <summary>
        /// Call form login to get UserID, UserName, and Password.
        /// </summary>
        //private void mnuLogin_Click(object sender, EventArgs e)
        //{
        //    FrmLogin fLogin = new FrmLogin(oSqlConn);
        //    try
        //    {
        //        fLogin.ShowDialog();
        //    }
        //    finally
        //    {
        //        UserData.sUserID = fLogin.sGetUserID();
        //        UserData.sUserName = fLogin.sGetUserName();
        //        UserData.sPassword = CommonClass.sGetEncrypt(fLogin.sGetPassword());
        //        UserData.isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
        //        UserData.sGroupID = fLogin.sGetGroupID();
        //        //iStatus = fLogin.iGetStatus();
        //    }

        //    if (fLogin.isCanLoginToSystem)
        //    {
        //        this.Visible = true;
        //        //doSetupDisplay(iStatus);
        //        doSetupDisplay();

        //        WriteLog(CommonMessage.sLoginSucccess);
        //        WriteStatus();
        //    }
        //    else
        //        Application.ExitThread();

        //    fLogin.Dispose();
        //}

        private void mnuLogin_Click(object sender, EventArgs e)
        {
            //FrmLogin2 fLogin2 = new FrmLogin2(oSqlConn);
            FrmLogin2 fLogin2 = new FrmLogin2();
            try
            {
                fLogin2.ShowDialog();
            }
            finally
            {
                UserData.sUserID = fLogin2.sGetUserID();
                UserData.sUserName = fLogin2.sGetUserName();
                UserData.sPassword = CommonClass.sGetEncrypt(fLogin2.sGetPassword());
                UserData.isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
                UserData.sGroupID = fLogin2.sGetGroupID();
                //iStatus = fLogin.iGetStatus();
            }

            if (fLogin2.isCanLoginToSystem)
            {
                this.Visible = true;
                //doSetupDisplay(iStatus);
                doSetupDisplay();

                oSqlConn = new SqlConnection(fLogin2.sConnString);
                oSqlConn.Open();

                oSqlConnAuditTrail = new SqlConnection(fLogin2.sConnStringAuditTrail);
                oSqlConnAuditTrail.Open();

                WriteLog(CommonMessage.sLoginSucccess);
                WriteStatus();
            }
            else
                Application.ExitThread();

            fLogin2.Dispose();
        }

        /// <summary>
        /// To let user to log off from application.
        /// </summary>
        private void mnuLogOff_Click(object sender, EventArgs e)
        {
            if (CommonClass.isYesMessage(CommonMessage.sLogoutText, CommonMessage.sLogoutTitle)) // User confirms
            {
                doLogOff("");
            }
        }

        /// <summary>
        /// Call form Change Password to let user change their password.
        /// </summary>
        private void mnuPassword_Click(object sender, EventArgs e)
        {
            bool isFromMenu = true;

            //FrmChangePassword fchangepass = new FrmChangePassword(oSqlConn,oSqlConnAuditTrail, UserData.sUserID, UserData.sUserName, UserData.sPassword, isFromMenu);
            FrmChangePassword fchangepass = new FrmChangePassword(oSqlConn, oSqlConnAuditTrail, UserData.sUserID, UserData.sUserName, UserData.sPassword, isFromMenu, false);
            try
            {
                fchangepass.ShowDialog();
                UserData.sPassword = fchangepass.sGetNewPassEncrypted();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Close the application.
        /// </summary>
        private void mnuExit_Click(object sender, EventArgs e)
        {
            if (CommonClass.isYesMessage(CommonMessage.sExitMessageText, CommonMessage.sExitMessageTitle)) // Confirmation.
            {
                if (!string.IsNullOrEmpty(UserData.sUserID)) //hanya write log jika ada yang sedang login
                    //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sLogoutSuccess, "");
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sLogoutSuccess, "");
                Application.ExitThread(); // Close all proccess.
            }
        }

        /// <summary>
        /// Call form DatabasePrimaryConnection to set application's Primary Connection.
        /// </summary>
        private void mnuDBConn_Click(object sender, EventArgs e)
        {
            bool IsLogoff = false;
            FrmDatabasePrimaryConnection fDbPrimary = new FrmDatabasePrimaryConnection();
            try
            {
                fDbPrimary.ShowDialog();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            finally
            {
                if (fDbPrimary.DialogResult == DialogResult.OK)
                {
                    if (isHaveConfiguration())
                    {
                        oSqlConn = fDbPrimary.oSqlConn;
                        IsLogoff = true;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(UserData.sUserID)) //hanya write log jika ada yang sedang login
                            //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sLogoutSuccess, CommonMessage.sLogOutSuccess_ConnStringChanged);
                            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sLogoutSuccess, CommonMessage.sLogOutSuccess_ConnStringChanged);
                        Application.ExitThread();
                    }
                }
                fDbPrimary.Dispose();
            }
            if (IsLogoff) doLogOff(CommonMessage.sLogOutSuccess_ConnStringChanged);
        }

        /// <summary>
        /// Open confirmation to allow initialitation.
        /// </summary>
        private void mnuAllowInit_Click(object sender, EventArgs e)
        {
            string sCanInitData = CommonVariable.IsCanInitData(oSqlConn).ToString();

            DialogResult dResult = MessageBox.Show(CommonMessage.sInitDataText + sCanInitData.ToUpper(),
                                                    CommonMessage.sInitDataTitle, MessageBoxButtons.YesNoCancel);

            if (dResult == DialogResult.Yes) CommonVariable.SetIsCanInitData(true, oSqlConn);
            else if (dResult == DialogResult.No) CommonVariable.SetIsCanInitData(false, oSqlConn);

            if (dResult != DialogResult.Cancel)
            {
                WriteLog("Change Initialize Data Status", string.Format("Initialize Status : {0} --> {1}", sCanInitData.ToUpper(),
                                                                       (dResult == DialogResult.Yes) ? "TRUE" : "FALSE"));
            }
        }

        /// <summary>
        /// Call from UserMngmt to manage application's users.
        /// </summary>
        private void mnuUserManagement_Click(object sender, EventArgs e)
        {
            FrmUserMngmt fUserMan = new FrmUserMngmt(oSqlConn, oSqlConnAuditTrail, UserData.sUserID);
            try
            {
                //this.ShowInTaskbar = false;
                fUserMan.ShowDialog();
            }
            finally
            {
                fUserMan.Dispose();
                //this.ShowInTaskbar = true
            }
        }

        private void mnuUserGroup_Click(object sender, EventArgs e)
        {
            FrmUserGroupMngmt fGroupMan = new FrmUserGroupMngmt(oSqlConn, UserData.sUserID);
            try
            {
                fGroupMan.ShowDialog();
            }
            finally
            {
                fGroupMan.Dispose();
            }
        }

        private void mnuItemUserRight_Click(object sender, EventArgs e)
        {
            FrmProfileGroup fProfile = new FrmProfileGroup(oSqlConn, oSqlConnAuditTrail, UserData.sUserID);
            try
            {
                fProfile.ShowDialog();
            }
            finally
            {
                fProfile.Dispose();
            }

        }

        /// <summary>
        /// Call form VersionSelection which show all versions in application.
        /// </summary>
        private void mnuSelectVs_Click(object sender, EventArgs e)
        {
            FrmVersionSelection fdbselection = new FrmVersionSelection(oSqlConn, oSqlConnAuditTrail, UserData.sUserID);
            CommonClass.doMinimizeChild(this.MdiChildren, fdbselection.Name);
            if (!isWindowOpened(fdbselection.Name))
            {
                fdbselection.MdiParent = this;
                try
                {
                    fdbselection.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                CommonClass.doShowChild(this.MdiChildren, fdbselection.Name, FormWindowState.Normal);
        }

        /// <summary>
        /// Call form United to show which profile is initiating.
        /// </summary>
        private void mnuStartListen_Click(object sender, EventArgs e)
        {
            bool isUnitedOpen = false;

            foreach (Form oForm in Application.OpenForms)
                if (oForm is FrmUnited) // If FrmUnited has already been opened.
                {
                    isUnitedOpen = true;
                    oForm.WindowState = FormWindowState.Normal;
                    oForm.Focus();
                    break;
                }

            if (!isUnitedOpen) // If FrmUnited is not open yet.
            {
                FrmUnited fUnited = new FrmUnited(oSqlConn, oSqlConnAuditTrail);
                try
                {
                    fUnited.Show();
                    fUnited.Focus();
                }
                catch
                {
                    fUnited.Dispose();
                }
            }
        }

        /// <summary>
        /// Call FrmLog to open Audit Trail Log.
        /// </summary>
        private void mnuAuditTrail_Click(object sender, EventArgs e)
        {
            FrmLog flog = new FrmLog(oSqlConn, oSqlConnAuditTrail, false);
            CommonClass.doMinimizeChild(this.MdiChildren, flog.Name);
            if (!isWindowOpened(flog.Name))
            {
                if (flog.isInitData())
                {
                    flog.MdiParent = this;
                    flog.Show();
                }
            }
            else
                foreach (Form a in this.MdiChildren)
                    if (a.Name == flog.Name)
                        if (((FrmLog)a).IsAuditSuperUser)
                        {
                            CommonClass.doCloseChild(this.MdiChildren, flog.Name);
                            if (flog.isInitData())
                            {
                                flog.MdiParent = this;
                                flog.Show();
                            }
                        }
                        else
                            CommonClass.doShowChild(this.MdiChildren, flog.Name, FormWindowState.Maximized);
        }

        /// <summary>
        /// Call FrmInitAudit to open Initialize Log.
        /// </summary>
        private void mnuInitializeTrail_Click(object sender, EventArgs e)
        {
            FrmInitAudit finitaudit = new FrmInitAudit(oSqlConn, oSqlConnAuditTrail);
            CommonClass.doMinimizeChild(this.MdiChildren, finitaudit.Name);
            if (!isWindowOpened(finitaudit.Name))
            {
                finitaudit.MdiParent = this;
                try
                {
                    finitaudit.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                CommonClass.doShowChild(this.MdiChildren, finitaudit.Name, FormWindowState.Maximized);
        }

        /// <summary>
        /// Call FrmTemplateDefinition.
        /// </summary>
        private void mnuTemplateDefinition_Click(object sender, EventArgs e)
        {
            new FrmTemplateDefinition(oSqlConn, oSqlConnAuditTrail).ShowDialog();
        }

        /// <summary>
        /// If TemplateDefinition is Exists, call FrmUploadDefinition, else call FrmTemplateDefinition.
        /// </summary>
        private void mnuUploadDefinition_Click(object sender, EventArgs e)
        {
            //if (!CommonClass.IsTemplateDefinitionExists(oSqlConn))
            //{
            //    if (CommonClass.isYesMessage(CommonMessage.sErrUploadText, CommonMessage.sErrUploadDefinitionTitle))
            //        mnuTemplateDefinition.PerformClick();
            //}
            //else
            //{
            new FrmUploadDefinition(oSqlConn, oSqlConnAuditTrail).ShowDialog();
            //}
        }

        /// <summary>
        /// If TemplateDefinition and UploadDefinition exists, call FrmUploadData, else call relevant form.
        /// </summary>
        private void mnuUpload_Click(object sender, EventArgs e)
        {
            //if (!CommonClass.IsTemplateDefinitionExists(oSqlConn))
            //{
            //    if (CommonClass.isYesMessage(CommonMessage.sErrUploadText, CommonMessage.sErrUploadTitle))
            //        mnuTemplateDefinition.PerformClick();
            //}
            //else if (!CommonClass.IsUploadDefinitionExists(oSqlConn))
            //{
            //    if (CommonClass.isYesMessage(CommonMessage.sErrUploadDataText, CommonMessage.sErrUploadDataSubTitle))
            //        mnuUploadDefinition.PerformClick();
            //}
            //else
            new FrmUploadData2(oSqlConn, oSqlConnAuditTrail).ShowDialog();
            //new FrmUploadData(oSqlConn).ShowDialog();
        }

        /// <summary>
        /// Call frmPing to see which EDC is online.
        /// </summary>
        private void mnuEDC_Click(object sender, EventArgs e)
        {
            FrmPing fping = new FrmPing(oSqlConn);
            CommonClass.doMinimizeChild(this.MdiChildren, fping.Name);
            if (!isWindowOpened(fping.Name))
            {
                fping.MdiParent = this;
                try
                {
                    fping.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                CommonClass.doShowChild(this.MdiChildren, fping.Name, FormWindowState.Normal);
        }

        /// <summary>
        /// Call FrmDownload to open Dowload Form
        /// </summary>
        private void mnuSerialDownload_Click(object sender, EventArgs e)
        {
            FrmSerialDownload fdownload = new FrmSerialDownload(oSqlConn);
            if (!isWindowOpened(fdownload.Name))
            {
                fdownload.MdiParent = this;
                try
                {
                    fdownload.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// Call About Form.
        /// </summary>
        private void mnuAbout_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Call Register Form to get license.
        /// </summary>
        private void mnuRegister_Click(object sender, EventArgs e)
        {
            FrmRegister fregister = new FrmRegister(oSqlConn);
            CommonClass.doMinimizeChild(this.MdiChildren, fregister.Name);
            fregister.ShowDialog();
            if (fregister.bRegistered)
            {
                mnuRegister.Enabled = false;
                WriteLog("Registration Succeeded");
            }
        }

        private void mnuAllSN_Click(object sender, EventArgs e)
        {

        }

        private void mnuLocationView_Click(object sender, EventArgs e)
        {
            FrmLocation flocation = new FrmLocation(oSqlConn, UserData.sUserID);
            CommonClass.doMinimizeChild(this.MdiChildren, flocation.Name);
            if (!isWindowOpened(flocation.Name))
            {
                flocation.MdiParent = this;
                try
                {
                    flocation.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                CommonClass.doShowChild(this.MdiChildren, flocation.Name, FormWindowState.Maximized);
        }

        private void mnuAddLocation_Click(object sender, EventArgs e)
        {
            FrmLocationInsert flocationinsert = new FrmLocationInsert(oSqlConn, UserData.sUserID);
            CommonClass.doMinimizeChild(this.MdiChildren, flocationinsert.Name);
            if (!isWindowOpened(flocationinsert.Name))
            {
                flocationinsert.MdiParent = this;
                try
                {
                    flocationinsert.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                CommonClass.doShowChild(this.MdiChildren, flocationinsert.Name, FormWindowState.Normal);
        }

        private void mnuSUTagItem_AddUpdate_Click(object sender, EventArgs e)
        {
            FrmTag fTagAddUpdate = new FrmTag(oSqlConn, oSqlConnAuditTrail, false);
            try
            {
                fTagAddUpdate.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fTagAddUpdate.Dispose();
            }
        }

        private void mnuSUTagItem_Delete_Click(object sender, EventArgs e)
        {
            FrmTag fTagDelete = new FrmTag(oSqlConn, oSqlConnAuditTrail, true);
            try
            {
                fTagDelete.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fTagDelete.Dispose();
            }
        }

        private void mnuSUCopyCardRange_Click(object sender, EventArgs e)
        {
            FrmCopyCard fCopy = new FrmCopyCard(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fCopy.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fCopy.Dispose();
            }
        }

        private void mnuSUCopyGPRS_Click(object sender, EventArgs e)
        {
            FrmCopyGPRS fCopy = new FrmCopyGPRS(oSqlConn, oSqlConnAuditTrail, "GPRS");
            try
            {
                fCopy.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fCopy.Dispose();
            }
        }

        private void mnuSUCopyTLE_Click(object sender, EventArgs e)
        {
            FrmCopyGPRS fCopy = new FrmCopyGPRS(oSqlConn, oSqlConnAuditTrail, "TLE");
            //FrmCopyTLE fCopy = new FrmCopyTLE(oSqlConn);
            try
            {
                fCopy.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fCopy.Dispose();
            }
        }

        private void mnuCompressInit_Click(object sender, EventArgs e)
        {
            FrmCompressInit fCompressInit = new FrmCompressInit(oSqlConn, oSqlConnAuditTrail, UserData.sUserID);
            fCompressInit.ShowDialog();
        }

        private void mnuAutoInit_Click(object sender, EventArgs e)
        {
            try
            {
                FrmAutoInit fAutoInit = new FrmAutoInit(oSqlConn, oSqlConnAuditTrail);
                fAutoInit.ShowDialog();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void mnuAppPackage_Click(object sender, EventArgs e)
        {
            FrmSoftwarePackage fSoftPackage = new FrmSoftwarePackage(oSqlConn, oSqlConnAuditTrail, UserData.sUserID);
            CommonClass.doMinimizeChild(this.MdiChildren, fSoftPackage.Name);
            if (!isWindowOpened(fSoftPackage.Name))
            {
                fSoftPackage.MdiParent = this;
                try
                {
                    fSoftPackage.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                CommonClass.doShowChild(this.MdiChildren, fSoftPackage.Name, FormWindowState.Normal);
        }

        private void mnuSoftwareInitTrail_Click(object sender, EventArgs e)
        {
            FrmInitAudit finitaudit = new FrmInitAudit(oSqlConn, oSqlConnAuditTrail);
            finitaudit.isAuditInit = false;
            CommonClass.doMinimizeChild(this.MdiChildren, finitaudit.Name);
            if (!isWindowOpened(finitaudit.Name))
            {
                finitaudit.MdiParent = this;
                try
                {
                    finitaudit.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                CommonClass.doShowChild(this.MdiChildren, finitaudit.Name, FormWindowState.Maximized);
        }

        #region "Super User Menu"
        /// <summary>
        /// Call the 
        /// </summary>
        private void mnuSUImportData_Click(object sender, EventArgs e)
        {
            FrmImportProfile fImport = new FrmImportProfile(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fImport.ShowDialog();
            }
            finally
            {
                fImport.Dispose();
            }
        }

        private void mnuSUExportToTxtFile_Click(object sender, EventArgs e)
        {
            FrmExportProfile fExport = new FrmExportProfile(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fExport.ShowDialog();
            }
            finally
            {
                fExport.Dispose();
            }
        }

        private void mnuSUExportToDb_Click(object sender, EventArgs e)
        {
            FrmMoveData fMove = new FrmMoveData(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fMove.ShowDialog();
            }
            finally
            {
                fMove.Dispose();
            }
        }

        private void mnuSUAddTable_Click(object sender, EventArgs e)
        {
            //FrmAddTable fAdd = new FrmAddTable(oSqlConn);
            //try
            //{
            //    fAdd.ShowDialog();
            //}
            //finally
            //{
            //    fAdd.Dispose();
            //}
        }

        private void mnuSUDeleteTable_Click(object sender, EventArgs e)
        {
            //FrmDeleteTable fDelete = new FrmDeleteTable(oSqlConn);
            //try
            //{
            //    fDelete.ShowDialog();
            //}
            //finally
            //{
            //    fDelete.Dispose();
            //}
        }

        private void mnuSUAddFields_Click(object sender, EventArgs e)
        {
            //FrmAddField fAdd = new FrmAddField(oSqlConn);
            //try
            //{
            //    fAdd.ShowDialog();
            //}
            //finally
            //{
            //    fAdd.Dispose();
            //}
        }

        private void mnuSUUpdateFields_Click(object sender, EventArgs e)
        {
            //FrmUpdateField fUpdate = new FrmUpdateField(oSqlConn);
            //try
            //{
            //    fUpdate.ShowDialog();
            //}
            //finally
            //{
            //    fUpdate.Dispose();
            //}
        }

        private void mnuSUDeleteFields_Click(object sender, EventArgs e)
        {
            //FrmDeleteField fDelete = new FrmDeleteField(oSqlConn);
            //try
            //{
            //    fDelete.ShowDialog();
            //}
            //finally
            //{
            //    fDelete.Dispose();
            //}
        }

        private void mnuSUSetFolder_Click(object sender, EventArgs e)
        {
            FrmSetFolder fSet = new FrmSetFolder(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fSet.ShowDialog();
            }
            finally
            {
                fSet.Dispose();
            }
        }

        private void mnuSUApplicationMgmt_Click(object sender, EventArgs e)
        {
            FrmApplicationsMgmt fApps = new FrmApplicationsMgmt(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fApps.ShowDialog();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            finally
            {
                fApps.Dispose();
            }
        }

        private void mnuSUInitConn_Click(object sender, EventArgs e)
        {
            try
            {
                FrmInitSetting fInit = new FrmInitSetting(oSqlConn, oSqlConnAuditTrail);
                fInit.ShowDialog();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void mnuSUSwProfile_Click(object sender, EventArgs e)
        {
            try
            {
                FrmSoftwareVersion fSwProfile = new FrmSoftwareVersion(oSqlConn, oSqlConnAuditTrail);
                fSwProfile.ShowDialog();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void mnuSUAudit_Click(object sender, EventArgs e)
        {
            FrmLog flog = new FrmLog(oSqlConn, oSqlConnAuditTrail, true);
            CommonClass.doMinimizeChild(this.MdiChildren, flog.Name);
            if (!isWindowOpened(flog.Name))
            {
                if (flog.isInitData())
                {
                    flog.MdiParent = this;
                    flog.Show();
                }
            }
            else
                foreach (Form a in this.MdiChildren)
                    if (a.Name == flog.Name)
                        if (!((FrmLog)a).IsAuditSuperUser)
                        {
                            CommonClass.doCloseChild(this.MdiChildren, flog.Name);
                            if (flog.isInitData())
                            {
                                flog.MdiParent = this;
                                flog.Show();
                            }
                        }
                        else
                            CommonClass.doShowChild(this.MdiChildren, flog.Name, FormWindowState.Maximized);
        }

        private void mnuSUTagItemExport_Click(object sender, EventArgs e)
        {
            FrmExportTag fExportTag = new FrmExportTag(oSqlConn, oSqlConnAuditTrail);
            fExportTag.ShowDialog();
        }

        private void mnuSUTagItemImport_Click(object sender, EventArgs e)
        {
            FrmImportTag fImportTag = new FrmImportTag(oSqlConn, oSqlConnAuditTrail);
            fImportTag.ShowDialog();
        }

        private void mnuSUTag5_Click(object sender, EventArgs e)
        {
            FrmTag5 fGenerateTag = new FrmTag5(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fGenerateTag.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fGenerateTag.Dispose();
            }

        }

        private void mnuSUVersion_Add_Click(object sender, EventArgs e)
        {
            FrmDatabaseVersion_Add fDbAdd = new FrmDatabaseVersion_Add(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fDbAdd.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fDbAdd.Dispose();
            }
        }

        private void mnuSUVersion_Edit_Click(object sender, EventArgs e)
        {
            FrmDatabaseVersion_Edit fDbEdit = new FrmDatabaseVersion_Edit(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fDbEdit.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fDbEdit.Dispose();
            }
        }

        private void mnuSUVersion_Delete_Click(object sender, EventArgs e)
        {
            FrmDatabaseVersion_Delete fDbDelete = new FrmDatabaseVersion_Delete(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fDbDelete.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fDbDelete.Dispose();
            }

        }
        #endregion

        /// <summary>
        /// Runs every time ticks, to set Datetime.
        /// </summary>
        private void TimerClock_Tick(object sender, EventArgs e)
        {
            DisplayDateTime();
        }

        private void mnuHelp_Click(object sender, EventArgs e)
        {

        }

        private void mnuTLV_Click(object sender, EventArgs e)
        {

        }

        private void mnuMoveData_Click(object sender, EventArgs e)
        {
            FrmMoveData fMoveData = new FrmMoveData(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fMoveData.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fMoveData.Dispose();
            }
        }

        private void mnuQueryReport_Click(object sender, EventArgs e)
        {
            FrmReport fQureyReport = new FrmReport(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fQureyReport.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fQureyReport.Dispose();
            }
        }

        #region "Function"
        #region "License"
        /// <summary>
        /// Create Application's license.
        /// </summary>
        private void CreateLicense()
        {
            LicenseIP oLIcense = new LicenseIP(Application.ProductName);
            oLIcense.CreateLicense("Tobias", "Ingenico");
        }

        /// <summary>
        /// Find valid license and check status TrialApplication.
        /// If both items invalid, application closed.
        /// </summary>
        private void doCheckLicense()
        {
            int iExpiryCount = 0;
            if (!IsTrialApplication(ref iExpiryCount))
            {
                if (!IsValidLicense())
                {
                    MessageBox.Show(CommonMessage.sUnregisteredExpiry);
                    Application.ExitThread();
                }
            }
            else
            {
                if (iExpiryCount < 0)
                {
                    MessageBox.Show(CommonMessage.sUnregisteredExpiry);
                    Application.ExitThread();
                }
            }
        }

        /// <summary>
        /// Determines if current License is valid.
        /// </summary>
        /// <returns>boolean : true if valid, else false</returns>
        protected bool IsValidLicense()
        {
            bool bReturn = false;
            LicenseIP oLicense = new LicenseIP(Application.ProductName);
            string sLicenseDate = null;
            if (oLicense.IsLicenseValid())
            {
                bReturn = true;
                //if (oLicense.IsLicenseWillExpiry(ref sLicenseDate))
                //    SendLicenseNotification(1, sLicenseDate);
            }
            else
                SendLicenseNotification(2, sLicenseDate);
            return bReturn;
        }

        /// <summary>
        /// Determine wheter current application is Trial or not.
        /// </summary>
        /// <returns>boolean : true if trial, else false</returns>
        protected bool IsTrialApplication(ref int _iExpiryCount)
        {
            //always registered client

            bool bReturn = false;
            //LicenseIP oLicense = new LicenseIP(Application.ProductName);
            //bReturn = oLicense.IsLicenseTrial(ref _iExpiryCount);
            //if (bReturn = oLicense.IsLicenseTrial())
            //    SendLicenseNotification(3, "Trial Application");
            return bReturn;
        }

        /// <summary>
        /// Determine wheter current application is Trial or not.
        /// </summary>
        /// <returns>boolean : true if trial, else false</returns>
        protected bool IsTrialApplication()
        {
            int iExpCount = 0;
            return IsTrialApplication(ref iExpCount);
        }

        /// <summary>
        /// Send Notification when Licensed will expired
        /// </summary>
        /// <param name="iNotifType">int : type of Notification</param>
        /// <param name="sMessage">string : Expiry Date</param>
        protected void SendLicenseNotification(int iNotifType, string sMessage)
        {
            string sBody;
            string sNotifType;
            sNotifType = iNotifType.ToString();
            if (iNotifType == 1)
                sBody = string.Format("InSys Client on : {0}, will expired on {1}. Please contact INTEGRA PRATAMA to renew license.",
                     Environment.MachineName, sMessage);
            else if (iNotifType == 2)
                sBody = string.Format("InSys Client on : {0}, allready expired. Please contact INTEGRA PRATAMA to renew license.",
                     Environment.MachineName);
            else
                sBody = string.Format("InSys Client on : {0}, is Trial Software. Please register.",
                     Environment.MachineName);

            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPSendNotification, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sNotifType", SqlDbType.VarChar).Value = sNotifType;
                oCmd.Parameters.Add("@sBody", SqlDbType.VarChar).Value = sBody;
                oCmd.ExecuteNonQuery();
            }
        }
        #endregion

        /// <summary> 
        /// get Audit Trail Connection
        /// </summary>
        private void GetAuditTrailConnection()
        {
            string sConnectionString = "Data Source=.;Initial Catalog=NewInsys_Audit_Trail;User ID=sa;Password=Scorfia123";
            oSqlConnAuditTrail = new SqlConnection(sConnectionString);
            try
            {
                oSqlConnAuditTrail.Open();
                MessageBox.Show("Audit Trail Connection Open");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //if (isAuditTraiHaveConfiguration())
            //{
            //    InitDataAuditTrail();
            //}
            //else
            //{
            //    MessageBox.Show("Not Have Audit Trail Configuration.");
            //    //Application.ExitThread();
            //}
        }

        /// <summary> 
        /// Call Form login to get User's data
        /// </summary>
        protected void doLogin()
        {
            if (isHaveConfiguration()) mnuLogin.PerformClick();
            else Application.ExitThread();
        }

        /// <summary>
        /// Determines wheter have configuration or not.
        /// </summary>
        /// <returns>boolean : true if configuration found, else false</returns>
        private bool isAuditTraiHaveConfiguration()
        {
            string sActiveDir = Application.StartupPath;
            string sFileName = sActiveDir + "\\AuditTrail.conf";

            if (File.Exists(sFileName)) return true;
            else return false;
        }

        /// <summary>
        /// Determines wheter have configuration or not.
        /// </summary>
        /// <returns>boolean : true if configuration found, else false</returns>
        protected bool isHaveConfiguration()
        {
            string sActiveDir = Application.StartupPath;
            string sFileName = sActiveDir + "\\Application.conf";

            if (File.Exists(sFileName)) return true;
            else return false;
        }

        /// <summary>
        /// Initiliaze the Database Connection
        /// </summary>
        protected void InitConnection()
        {
            if (!isHaveConfiguration())
                mnuDBConn.PerformClick();
            else
            {
                oSqlConn = new SqlConnection(new InitData().sGetConnString());
                oSqlConn.Open();
            }
        }

        /// <summary>
        /// Setup the display by user's privilege.
        /// </summary>
        protected void doSetupDisplay()
        {
            DisplayAll(true);

            mnuAllowInit.Visible = mnuAllowInit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IA);
            mnuAutoInit.Visible = mnuAutoInit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.OI);
            mnuCompressInit.Visible = mnuCompressInit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IC);
            mnuAllSN.Visible = mnuAllSN.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.SN);
            mnuLocationProfiling.Visible = mnuLocationProfiling.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.LO);
            mnuAddLocation.Visible = mnuAddLocation.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.LO, Privilege.Add);

            mnuAuditTrail.Visible = mnuAuditTrail.Enabled = ToolBarAudit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.AT);
            mnuInitializeTrail.Visible = mnuInitializeTrail.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IT);
            mnuTLV.Visible = mnuTLV.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.PT);
            //menuItemApplicationDataUpload.Visible = mnuAppPackage.Visible = mnuAppPackage.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.SW);

            mnuDataUpload.Visible = menuItemApplicationDataUpload.Visible =
                //(mnuTemplateDefinition.Visible = mnuTemplateDefinition.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.DU, (Privilege)(PrivilegeDigit.Length))) |
                //(mnuUploadDefinition.Visible = mnuUploadDefinition.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.DU, (Privilege)(PrivilegeDigit.Length + 1))) |
                (mnuUpload.Visible = mnuUpload.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.DU, (Privilege)(PrivilegeDigit.Length + 2)));

            mnuUserManagement.Visible = mnuUserManagement.Enabled = MenuItemApplicationUserManagement.Visible = UserPrivilege.IsAllowed(PrivilegeCode.UM);
            //mnuUserGroup.Visible = mnuUserGroup.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.UM);
            //mnuItemUserRight.Visible = mnuItemUserRight.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.UM);

            mnuEDCTracking.Visible = mnuEDCTracking.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.PE);
            mnuSerialDownload.Visible = mnuSerialDownload.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.DL);

            mnuAllSN.Enabled = mnuAllSN.Visible = false;
            mnuSettingApp.Visible = mnuSettingApp.Enabled = MenuItemApplicationSetting.Visible = UserPrivilege.IsAllowed(PrivilegeCode.SU);
            mnuQueryReport.Enabled = mnuQueryReport.Visible = UserPrivilege.IsAllowed(PrivilegeCode.QR);

            mnuRemoteDownload.Enabled = mnuRemoteDownload.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RD);
            mnUpdateRD.Enabled = mnUpdateRD.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RD);

            menuItem21.Enabled = menuItem21.Visible = mnLogoEDC.Enabled = mnLogoEDC.Visible = UserPrivilege.IsAllowed(PrivilegeCode.IP);

            mnuManagement.Enabled = mnuManagement.Visible = false;
            mnuSerialDownload.Enabled = mnuSerialDownload.Visible = false;
            mnEchoPing.Enabled = mnEchoPing.Visible = false;
            mnuEDCTracking.Enabled = mnuEDCTracking.Visible = false;
            mnuTLV.Enabled = mnuTLV.Visible = false;
            mnuLocationProfiling.Enabled = mnuLocationProfiling.Visible = false;
        }

        /// <summary>
        /// Default display
        /// </summary>
        /// <param name="isEnable">True/False display value</param>
        protected void DisplayAll(bool isEnable)
        {
            // menu
            mnuSuperAdmin.Visible = UserData.isSuperUser;

            mnuLogin.Enabled = !isEnable;
            mnuLogOff.Enabled = isEnable;
            mnuPassword.Enabled = isEnable;

            mnuSettingApp.Enabled = mnuSettingApp.Visible = MenuItemApplicationSetting.Visible = isEnable;
            mnuUserManagement.Enabled = mnuUserManagement.Visible = MenuItemApplicationUserManagement.Visible = isEnable;
            //mnuItemUserRight.Enabled = mnuItemUserRight.Visible = isEnable;
            //mnuUserGroup.Enabled = mnuUserGroup.Visible = isEnable;

            mnuEdcTerminal.Visible = isEnable;
            mnuBrowse.Enabled = mnuBrowse.Visible = MenuItemApplicationUserManagement.Visible = isEnable;
            mnuSettingEdc.Visible = isEnable;
            mnuAllowInit.Enabled = mnuAllowInit.Visible = isEnable;
            mnuAutoInit.Enabled = mnuAutoInit.Visible = isEnable;
            mnuAllSN.Enabled = mnuAllSN.Visible = isEnable;
            //if (mnuDataUpload.Visible = menuItemApplicationDataUpload.Visible = isEnable)
            //{   //this^ kind of 'if' takes left value as true/false. DO NOT replace '=' to '=='.
            //mnuTemplateDefinition.Visible = mnuTemplateDefinition.Enabled = isEnable;
            //mnuUploadDefinition.Visible = mnuUploadDefinition.Enabled = isEnable;
            mnuUpload.Visible = mnuUpload.Enabled = isEnable;
            //}

            mnuMonitor.Enabled = mnuMonitor.Visible = isEnable;
            mnuEDCTracking.Enabled = mnuEDCTracking.Visible = false;
            mnuLocationProfiling.Enabled = mnuLocationProfiling.Visible = false;

            mnuReport.Visible = isEnable;
            mnuAuditTrail.Enabled = mnuAuditTrail.Visible = ToolBarAudit.Enabled = isEnable;
            mnuInitializeTrail.Enabled = mnuInitializeTrail.Visible = isEnable;
            mnuTLV.Enabled = mnuTLV.Visible = isEnable;
            mnuManagement.Enabled = mnuManagement.Visible = false;
            mnuSerialDownload.Enabled = mnuSerialDownload.Visible = false;
            mnEchoPing.Enabled = mnEchoPing.Visible = false;

            //mnuSuperAdmin.Visible = bEnable;
            if (IsTrialApplication())
                mnuRegister.Enabled = true;

            // ToolBar
            ToolBarLogin.Enabled = !isEnable;
            ToolBarLogOff.Enabled = isEnable;
            ToolBarAudit.Enabled = isEnable;
            ToolBarSelectVs.Enabled = isEnable;
            ToolBarUnited.Enabled = isEnable;
            ToolBarDownload.Enabled = isEnable;

            if (!isEnable)
                foreach (Form oForm in this.MdiChildren)
                    oForm.Close();
        }

        /// <summary>
        /// InSys Client default display
        /// </summary>
        protected void DisplayClient()
        {
            mnuMonitor.Visible = false;
            mnuSerialDownload.Visible = false;

            ToolBarUnited.Visible = false;
            ToolBarDownload.Visible = false;
        }

        /// <summary>
        /// Set current time and date.
        /// </summary>
        protected void DisplayDateTime()
        {
            CultureInfo USFormat = new CultureInfo("en-US");
            StatusBarTime.Text = DateTime.Now.ToString("hh:mm:ss tt", USFormat);
            StatusBarDate.Text = DateTime.Now.ToString("dddd, MMM dd, yyyy", USFormat);
        }

        /// <summary>
        /// Write status to Audit Trail Logs, set window's title bar and current user.
        /// </summary>
        /// <param name="sLog">string : Logs description to inserted to Audit Trail Logs</param>
        protected void WriteLog(string sLog)
        {
            //CommonClass.InputLog(oSqlConn, "", string.IsNullOrEmpty(UserData.sUserID) ? "" : UserData.sUserID, "", sLog, "");
            CommonClass.InputLog(oSqlConnAuditTrail, "", string.IsNullOrEmpty(UserData.sUserID) ? "" : UserData.sUserID, "", sLog, "");
        }

        /// <summary>
        /// Write status to Audit Trail Logs, set window's title bar and current user.
        /// </summary>
        /// <param name="sLog">string : Logs description to inserted to Audit Trail Logs</param>
        /// <param name="sLogDetail">string : Detail Logs description to inserted to Audit Trail Logs</param>
        protected void WriteLog(string sLog, string sLogDetail)
        {
            //CommonClass.InputLog(oSqlConn, "", string.IsNullOrEmpty(UserData.sUserID) ? "" : UserData.sUserID, "", sLog, sLogDetail);
            CommonClass.InputLog(oSqlConnAuditTrail, "", string.IsNullOrEmpty(UserData.sUserID) ? "" : UserData.sUserID, "", sLog, sLogDetail);
        }

        /// <summary>
        /// Write Application's Status at Form's Header
        /// </summary>
        protected void WriteStatus()
        {
            this.Text = "InSys Client v." + Application.ProductVersion.ToString();
            if (IsTrialApplication())
                this.Text += " (Trial Version)";
            StatusBarApplication.Text = "User : " + UserData.sUserName;
        }

        /// <summary>
        /// Clear all user data saved in application.
        /// </summary>
        protected void doClearUser()
        {
            UserData.sUserID = null;
            UserData.sUserName = null;
            UserData.sPassword = null;
            UserData.isSuperUser = false;
            UserPrivilege.sPrivilege = "";
        }

        /// <summary>
        /// Determines if there are other MDI child form is opened.
        /// </summary>
        /// <param name="sFormName">string : current Form Name which would be opened</param>
        /// <returns></returns>
        public bool isWindowOpened(string sFormName)
        {
            bool isOpened = false;
            foreach (Form frmChild in this.MdiChildren)
            {
                if (sFormName == frmChild.Name)
                {
                    frmChild.Activate();
                    isOpened = true;
                }
            }
            return isOpened;
        }

        /// <summary>
        /// Log Off from application
        /// </summary>
        /// <param name="sLogDetail">string : Detail Log information</param>
        protected void doLogOff(string sLogDetail)
        {
            int iCount = 0;
            do
            {
                if (!(Application.OpenForms[iCount] is FrmMainMenu))
                {
                    Application.OpenForms[iCount].Close();
                    iCount = 0;
                }
                iCount++;
            }
            while (iCount < Application.OpenForms.Count);
            WriteLog(CommonMessage.sLogoutSuccess, sLogDetail);
            CommonClass.UserAccessDelete(oSqlConn, null);
            doClearUser();
            WriteStatus();

            DisplayAll(false);
        }
        #endregion

        private void mnuSoftwareProgress_Click(object sender, EventArgs e)
        {
            FrmInitSoftwareProgress fInitSoftwareProgress = new FrmInitSoftwareProgress(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fInitSoftwareProgress.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fInitSoftwareProgress.Dispose();
            }
        }

        private void mnuEchoPing_Click(object sender, EventArgs e)
        {
            FrmEchoPing fEchoPing = new FrmEchoPing(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fEchoPing.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fEchoPing.Dispose();
            }
        }

        private void mnuSUExportCard_Click(object sender, EventArgs e)
        {
            FrmExportCard fExportCard = new FrmExportCard(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fExportCard.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fExportCard.Dispose();
            }
        }

        private void mnuSUImportCard_Click(object sender, EventArgs e)
        {
            FrmImportCard fImpoertCard = new FrmImportCard(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fImpoertCard.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fImpoertCard.Dispose();
            }
        }

        private void mnuSUCopyData_Click(object sender, EventArgs e)
        {
            frmCopyData fCopyData = new frmCopyData(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fCopyData.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fCopyData.Dispose();
            }
        }

        private void mnuRemoteDownload_Click(object sender, EventArgs e)
        {

            frmRemoteDownload fRemoteDownload = new frmRemoteDownload(oSqlConn, oSqlConnAuditTrail);

            try
            {
                fRemoteDownload.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fRemoteDownload.Dispose();
            }
        }

        private void mnUpdateRD_Click(object sender, EventArgs e)
        {
            frmUpdateRD fUpdateRD = new frmUpdateRD(oSqlConn, oSqlConnAuditTrail);
            try
            {
                fUpdateRD.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fUpdateRD.Dispose();
            }
        }

        private void mnSUCopyAID_Click(object sender, EventArgs e)
        {
            FrmCopyGPRS fCopy = new FrmCopyGPRS(oSqlConn, oSqlConnAuditTrail, "AID");
            try
            {
                fCopy.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fCopy.Dispose();
            }
        }

        private void mnSUCopyCAPK_Click(object sender, EventArgs e)
        {
            FrmCopyGPRS fCopy = new FrmCopyGPRS(oSqlConn, oSqlConnAuditTrail, "CAPK");
            try
            {
                fCopy.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fCopy.Dispose();
            }
        }

        private void mnCopyRD_Click(object sender, EventArgs e)
        {
            FrmCopyGPRS fCopy = new FrmCopyGPRS(oSqlConn, oSqlConnAuditTrail, "Remote Download");
            try
            {
                fCopy.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fCopy.Dispose();
            }
        }

        private void mnLogoEDCPrint_Click(object sender, EventArgs e)
        {
            FrmImage fImage = new FrmImage(oSqlConn, oSqlConnAuditTrail, UserData.sUserID,"Print");
            try
            {
                fImage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnLogoEDCIdle_Click(object sender, EventArgs e)
        {
            FrmImage fImage = new FrmImage(oSqlConn, oSqlConnAuditTrail, UserData.sUserID,"Idle");
            try
            {
                fImage.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem22_Click(object sender, EventArgs e)
        {
            FrmCustomMenu FCustomMenu = new FrmCustomMenu(oSqlConn, oSqlConnAuditTrail, false);
            try
            {
                FCustomMenu.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}