namespace InSys
{
    partial class FrmUploadDefinition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUploadDefinition));
            this.gbHeader = new System.Windows.Forms.GroupBox();
            this.txtSourceFile = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.numStartingRow = new System.Windows.Forms.NumericUpDown();
            this.cmbDatabaseDestination = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbBody = new System.Windows.Forms.GroupBox();
            this.dgvUploadDefinition = new System.Windows.Forms.DataGridView();
            this.clmDestination = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmSource = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.clmTag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbFooter = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.ofdDefinition = new System.Windows.Forms.OpenFileDialog();
            this.tipUploadDefinition = new System.Windows.Forms.ToolTip(this.components);
            this.gbHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStartingRow)).BeginInit();
            this.gbBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUploadDefinition)).BeginInit();
            this.gbFooter.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbHeader
            // 
            this.gbHeader.Controls.Add(this.txtSourceFile);
            this.gbHeader.Controls.Add(this.btnBrowse);
            this.gbHeader.Controls.Add(this.numStartingRow);
            this.gbHeader.Controls.Add(this.cmbDatabaseDestination);
            this.gbHeader.Controls.Add(this.label3);
            this.gbHeader.Controls.Add(this.label2);
            this.gbHeader.Controls.Add(this.label1);
            this.gbHeader.Location = new System.Drawing.Point(12, 12);
            this.gbHeader.Name = "gbHeader";
            this.gbHeader.Size = new System.Drawing.Size(447, 65);
            this.gbHeader.TabIndex = 0;
            this.gbHeader.TabStop = false;
            // 
            // txtSourceFile
            // 
            this.txtSourceFile.Location = new System.Drawing.Point(128, 13);
            this.txtSourceFile.Name = "txtSourceFile";
            this.txtSourceFile.Size = new System.Drawing.Size(233, 20);
            this.txtSourceFile.TabIndex = 1;
            this.txtSourceFile.TextChanged += new System.EventHandler(this.txtSourceFile_TextChanged);
            this.txtSourceFile.MouseLeave += new System.EventHandler(this.txtSourceFile_MouseLeave);
            this.txtSourceFile.MouseEnter += new System.EventHandler(this.txtSourceFile_MouseEnter);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(367, 11);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 24);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "&Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // numStartingRow
            // 
            this.numStartingRow.Enabled = false;
            this.numStartingRow.Location = new System.Drawing.Point(128, 39);
            this.numStartingRow.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStartingRow.Name = "numStartingRow";
            this.numStartingRow.Size = new System.Drawing.Size(120, 20);
            this.numStartingRow.TabIndex = 3;
            this.numStartingRow.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cmbDatabaseDestination
            // 
            this.cmbDatabaseDestination.DisplayMember = "DatabaseName";
            this.cmbDatabaseDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDatabaseDestination.Enabled = false;
            this.cmbDatabaseDestination.FormattingEnabled = true;
            this.cmbDatabaseDestination.Location = new System.Drawing.Point(121, 65);
            this.cmbDatabaseDestination.Name = "cmbDatabaseDestination";
            this.cmbDatabaseDestination.Size = new System.Drawing.Size(121, 21);
            this.cmbDatabaseDestination.TabIndex = 4;
            this.cmbDatabaseDestination.ValueMember = "DatabaseID";
            this.cmbDatabaseDestination.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Database Destination";
            this.label3.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Starting Row";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Source Template (*.xls)";
            // 
            // gbBody
            // 
            this.gbBody.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gbBody.Controls.Add(this.dgvUploadDefinition);
            this.gbBody.Location = new System.Drawing.Point(12, 77);
            this.gbBody.Name = "gbBody";
            this.gbBody.Size = new System.Drawing.Size(447, 315);
            this.gbBody.TabIndex = 0;
            this.gbBody.TabStop = false;
            // 
            // dgvUploadDefinition
            // 
            this.dgvUploadDefinition.AllowUserToAddRows = false;
            this.dgvUploadDefinition.AllowUserToDeleteRows = false;
            this.dgvUploadDefinition.AllowUserToResizeRows = false;
            this.dgvUploadDefinition.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUploadDefinition.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUploadDefinition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUploadDefinition.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmDestination,
            this.clmSource,
            this.clmTag,
            this.clmID});
            this.dgvUploadDefinition.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvUploadDefinition.Location = new System.Drawing.Point(8, 13);
            this.dgvUploadDefinition.MultiSelect = false;
            this.dgvUploadDefinition.Name = "dgvUploadDefinition";
            this.dgvUploadDefinition.RowHeadersVisible = false;
            this.dgvUploadDefinition.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUploadDefinition.Size = new System.Drawing.Size(432, 293);
            this.dgvUploadDefinition.TabIndex = 0;
            // 
            // clmDestination
            // 
            this.clmDestination.HeaderText = "Destination Column";
            this.clmDestination.Name = "clmDestination";
            this.clmDestination.ReadOnly = true;
            this.clmDestination.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmSource
            // 
            this.clmSource.HeaderText = "Source Column";
            this.clmSource.Name = "clmSource";
            // 
            // clmTag
            // 
            this.clmTag.HeaderText = "Tag";
            this.clmTag.Name = "clmTag";
            this.clmTag.Visible = false;
            // 
            // clmID
            // 
            this.clmID.HeaderText = "ID";
            this.clmID.Name = "clmID";
            this.clmID.Visible = false;
            // 
            // gbFooter
            // 
            this.gbFooter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbFooter.Controls.Add(this.btnCancel);
            this.gbFooter.Controls.Add(this.btnSave);
            this.gbFooter.Location = new System.Drawing.Point(12, 392);
            this.gbFooter.Name = "gbFooter";
            this.gbFooter.Size = new System.Drawing.Size(447, 41);
            this.gbFooter.TabIndex = 0;
            this.gbFooter.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(365, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(284, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ofdDefinition
            // 
            this.ofdDefinition.Filter = "Excel Files (*.xls)|*.xls";
            // 
            // FrmUploadDefinition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(471, 445);
            this.Controls.Add(this.gbBody);
            this.Controls.Add(this.gbFooter);
            this.Controls.Add(this.gbHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUploadDefinition";
            this.Opacity = 0;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Upload Definition";
            this.Load += new System.EventHandler(this.FrmUploadDefinition_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmUploadDefinition_FormClosed);
            this.gbHeader.ResumeLayout(false);
            this.gbHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStartingRow)).EndInit();
            this.gbBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUploadDefinition)).EndInit();
            this.gbFooter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbHeader;
        private System.Windows.Forms.TextBox txtSourceFile;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.NumericUpDown numStartingRow;
        private System.Windows.Forms.ComboBox cmbDatabaseDestination;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbBody;
        private System.Windows.Forms.GroupBox gbFooter;
        private System.Windows.Forms.OpenFileDialog ofdDefinition;
        private System.Windows.Forms.DataGridView dgvUploadDefinition;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ToolTip tipUploadDefinition;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDestination;
        private System.Windows.Forms.DataGridViewComboBoxColumn clmSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTag;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmID;
    }
}