namespace InSys
{
    partial class FrmAutoInit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAutoInit));
            this.gbData = new System.Windows.Forms.GroupBox();
            this.lblSec = new System.Windows.Forms.Label();
            this.txtMaxConn = new System.Windows.Forms.TextBox();
            this.lblMaxConn = new System.Windows.Forms.Label();
            this.txtInitTimeOut = new System.Windows.Forms.TextBox();
            this.lblInitTO = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbData.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbData
            // 
            this.gbData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbData.Controls.Add(this.lblSec);
            this.gbData.Controls.Add(this.txtMaxConn);
            this.gbData.Controls.Add(this.lblMaxConn);
            this.gbData.Controls.Add(this.txtInitTimeOut);
            this.gbData.Controls.Add(this.lblInitTO);
            this.gbData.Location = new System.Drawing.Point(8, 3);
            this.gbData.Name = "gbData";
            this.gbData.Size = new System.Drawing.Size(390, 82);
            this.gbData.TabIndex = 0;
            this.gbData.TabStop = false;
            // 
            // lblSec
            // 
            this.lblSec.AutoSize = true;
            this.lblSec.Location = new System.Drawing.Point(324, 22);
            this.lblSec.Name = "lblSec";
            this.lblSec.Size = new System.Drawing.Size(55, 13);
            this.lblSec.TabIndex = 4;
            this.lblSec.Text = "Second(s)";
            // 
            // txtMaxConn
            // 
            this.txtMaxConn.Location = new System.Drawing.Point(139, 49);
            this.txtMaxConn.MaxLength = 6;
            this.txtMaxConn.Name = "txtMaxConn";
            this.txtMaxConn.Size = new System.Drawing.Size(179, 20);
            this.txtMaxConn.TabIndex = 3;
            // 
            // lblMaxConn
            // 
            this.lblMaxConn.AutoSize = true;
            this.lblMaxConn.Location = new System.Drawing.Point(11, 52);
            this.lblMaxConn.Name = "lblMaxConn";
            this.lblMaxConn.Size = new System.Drawing.Size(108, 13);
            this.lblMaxConn.TabIndex = 2;
            this.lblMaxConn.Text = "Maximum Connection";
            // 
            // txtInitTimeOut
            // 
            this.txtInitTimeOut.Location = new System.Drawing.Point(139, 19);
            this.txtInitTimeOut.MaxLength = 3;
            this.txtInitTimeOut.Name = "txtInitTimeOut";
            this.txtInitTimeOut.Size = new System.Drawing.Size(179, 20);
            this.txtInitTimeOut.TabIndex = 1;
            // 
            // lblInitTO
            // 
            this.lblInitTO.AutoSize = true;
            this.lblInitTO.Location = new System.Drawing.Point(11, 22);
            this.lblInitTO.Name = "lblInitTO";
            this.lblInitTO.Size = new System.Drawing.Size(67, 13);
            this.lblInitTO.TabIndex = 0;
            this.lblInitTO.Text = "Init Time Out";
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(8, 86);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(390, 62);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(283, 19);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(98, 32);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(179, 19);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 32);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmAutoInit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 154);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAutoInit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Auto Init";
            this.Load += new System.EventHandler(this.FrmAutoInit_Load);
            this.gbData.ResumeLayout(false);
            this.gbData.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbData;
        private System.Windows.Forms.TextBox txtMaxConn;
        private System.Windows.Forms.Label lblMaxConn;
        private System.Windows.Forms.TextBox txtInitTimeOut;
        private System.Windows.Forms.Label lblInitTO;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSec;
    }
}