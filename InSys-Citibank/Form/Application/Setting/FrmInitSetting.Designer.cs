namespace InSys
{
    partial class FrmInitSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInitSetting));
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbData = new System.Windows.Forms.GroupBox();
            this.txtSoftwarePort = new System.Windows.Forms.TextBox();
            this.lblSoftwarePort = new System.Windows.Forms.Label();
            this.txtMaxConn = new System.Windows.Forms.TextBox();
            this.lblMaxConn = new System.Windows.Forms.Label();
            this.txtInitPort = new System.Windows.Forms.TextBox();
            this.lblPortInit = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbModem1 = new System.Windows.Forms.ComboBox();
            this.cmbModem2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbModem3 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbModem4 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbModem5 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbParity = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbDataBit = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbStopBit = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbBaudRate = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbSerial = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chkEnableTcpIp = new System.Windows.Forms.CheckBox();
            this.chkEnableModem = new System.Windows.Forms.CheckBox();
            this.chkEnableSerial = new System.Windows.Forms.CheckBox();
            this.gbButton.SuspendLayout();
            this.gbData.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(9, 177);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(740, 62);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(633, 19);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(98, 32);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(529, 19);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 32);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbData
            // 
            this.gbData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.gbData.Controls.Add(this.chkEnableTcpIp);
            this.gbData.Controls.Add(this.txtSoftwarePort);
            this.gbData.Controls.Add(this.lblSoftwarePort);
            this.gbData.Controls.Add(this.txtMaxConn);
            this.gbData.Controls.Add(this.lblMaxConn);
            this.gbData.Controls.Add(this.txtInitPort);
            this.gbData.Controls.Add(this.lblPortInit);
            this.gbData.Location = new System.Drawing.Point(9, 1);
            this.gbData.Name = "gbData";
            this.gbData.Size = new System.Drawing.Size(293, 175);
            this.gbData.TabIndex = 0;
            this.gbData.TabStop = false;
            // 
            // txtSoftwarePort
            // 
            this.txtSoftwarePort.Location = new System.Drawing.Point(129, 90);
            this.txtSoftwarePort.MaxLength = 4;
            this.txtSoftwarePort.Name = "txtSoftwarePort";
            this.txtSoftwarePort.Size = new System.Drawing.Size(150, 20);
            this.txtSoftwarePort.TabIndex = 5;
            this.txtSoftwarePort.Tag = "PortDownload";
            // 
            // lblSoftwarePort
            // 
            this.lblSoftwarePort.AutoSize = true;
            this.lblSoftwarePort.Location = new System.Drawing.Point(11, 94);
            this.lblSoftwarePort.Name = "lblSoftwarePort";
            this.lblSoftwarePort.Size = new System.Drawing.Size(71, 13);
            this.lblSoftwarePort.TabIndex = 4;
            this.lblSoftwarePort.Text = "Software Port";
            // 
            // txtMaxConn
            // 
            this.txtMaxConn.Location = new System.Drawing.Point(129, 38);
            this.txtMaxConn.MaxLength = 6;
            this.txtMaxConn.Name = "txtMaxConn";
            this.txtMaxConn.Size = new System.Drawing.Size(150, 20);
            this.txtMaxConn.TabIndex = 1;
            this.txtMaxConn.Tag = "InitMaxConn";
            // 
            // lblMaxConn
            // 
            this.lblMaxConn.AutoSize = true;
            this.lblMaxConn.Location = new System.Drawing.Point(11, 42);
            this.lblMaxConn.Name = "lblMaxConn";
            this.lblMaxConn.Size = new System.Drawing.Size(108, 13);
            this.lblMaxConn.TabIndex = 0;
            this.lblMaxConn.Text = "Maximum Connection";
            // 
            // txtInitPort
            // 
            this.txtInitPort.Location = new System.Drawing.Point(129, 64);
            this.txtInitPort.MaxLength = 4;
            this.txtInitPort.Name = "txtInitPort";
            this.txtInitPort.Size = new System.Drawing.Size(150, 20);
            this.txtInitPort.TabIndex = 3;
            this.txtInitPort.Tag = "PortInit";
            // 
            // lblPortInit
            // 
            this.lblPortInit.AutoSize = true;
            this.lblPortInit.Location = new System.Drawing.Point(11, 68);
            this.lblPortInit.Name = "lblPortInit";
            this.lblPortInit.Size = new System.Drawing.Size(43, 13);
            this.lblPortInit.TabIndex = 2;
            this.lblPortInit.Text = "Init Port";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.chkEnableModem);
            this.groupBox1.Controls.Add(this.cmbModem5);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cmbModem4);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbModem3);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmbModem2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbModem1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(308, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(217, 175);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Modem-1";
            // 
            // cmbModem1
            // 
            this.cmbModem1.FormattingEnabled = true;
            this.cmbModem1.Location = new System.Drawing.Point(80, 38);
            this.cmbModem1.Name = "cmbModem1";
            this.cmbModem1.Size = new System.Drawing.Size(124, 21);
            this.cmbModem1.TabIndex = 1;
            // 
            // cmbModem2
            // 
            this.cmbModem2.FormattingEnabled = true;
            this.cmbModem2.Location = new System.Drawing.Point(80, 64);
            this.cmbModem2.Name = "cmbModem2";
            this.cmbModem2.Size = new System.Drawing.Size(124, 21);
            this.cmbModem2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Modem-2";
            // 
            // cmbModem3
            // 
            this.cmbModem3.FormattingEnabled = true;
            this.cmbModem3.Location = new System.Drawing.Point(80, 90);
            this.cmbModem3.Name = "cmbModem3";
            this.cmbModem3.Size = new System.Drawing.Size(124, 21);
            this.cmbModem3.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Modem-3";
            // 
            // cmbModem4
            // 
            this.cmbModem4.FormattingEnabled = true;
            this.cmbModem4.Location = new System.Drawing.Point(80, 117);
            this.cmbModem4.Name = "cmbModem4";
            this.cmbModem4.Size = new System.Drawing.Size(124, 21);
            this.cmbModem4.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Modem-4";
            // 
            // cmbModem5
            // 
            this.cmbModem5.FormattingEnabled = true;
            this.cmbModem5.Location = new System.Drawing.Point(80, 144);
            this.cmbModem5.Name = "cmbModem5";
            this.cmbModem5.Size = new System.Drawing.Size(124, 21);
            this.cmbModem5.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Modem-5";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.chkEnableSerial);
            this.groupBox2.Controls.Add(this.cmbParity);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbDataBit);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cmbStopBit);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cmbBaudRate);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.cmbSerial);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(531, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(217, 175);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // cmbParity
            // 
            this.cmbParity.FormattingEnabled = true;
            this.cmbParity.Items.AddRange(new object[] {
            "NONE",
            "EVEN",
            "ODD",
            "MARK",
            "SPACE"});
            this.cmbParity.Location = new System.Drawing.Point(80, 144);
            this.cmbParity.Name = "cmbParity";
            this.cmbParity.Size = new System.Drawing.Size(124, 21);
            this.cmbParity.TabIndex = 9;
            this.cmbParity.Text = "NONE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Parity";
            // 
            // cmbDataBit
            // 
            this.cmbDataBit.FormattingEnabled = true;
            this.cmbDataBit.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cmbDataBit.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cmbDataBit.Location = new System.Drawing.Point(80, 117);
            this.cmbDataBit.Name = "cmbDataBit";
            this.cmbDataBit.Size = new System.Drawing.Size(124, 21);
            this.cmbDataBit.TabIndex = 7;
            this.cmbDataBit.Text = "8";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "DataBit";
            // 
            // cmbStopBit
            // 
            this.cmbStopBit.FormattingEnabled = true;
            this.cmbStopBit.Items.AddRange(new object[] {
            "0",
            "1",
            "1.5",
            "2"});
            this.cmbStopBit.Location = new System.Drawing.Point(80, 90);
            this.cmbStopBit.Name = "cmbStopBit";
            this.cmbStopBit.Size = new System.Drawing.Size(124, 21);
            this.cmbStopBit.TabIndex = 5;
            this.cmbStopBit.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "StopBit";
            // 
            // cmbBaudRate
            // 
            this.cmbBaudRate.FormattingEnabled = true;
            this.cmbBaudRate.Items.AddRange(new object[] {
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "38400",
            "57600",
            "115200"});
            this.cmbBaudRate.Location = new System.Drawing.Point(80, 64);
            this.cmbBaudRate.Name = "cmbBaudRate";
            this.cmbBaudRate.Size = new System.Drawing.Size(124, 21);
            this.cmbBaudRate.TabIndex = 3;
            this.cmbBaudRate.Text = "9600";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "BaudRate";
            // 
            // cmbSerial
            // 
            this.cmbSerial.FormattingEnabled = true;
            this.cmbSerial.Location = new System.Drawing.Point(80, 38);
            this.cmbSerial.Name = "cmbSerial";
            this.cmbSerial.Size = new System.Drawing.Size(124, 21);
            this.cmbSerial.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Serial Port";
            // 
            // chkEnableTcpIp
            // 
            this.chkEnableTcpIp.AutoSize = true;
            this.chkEnableTcpIp.Location = new System.Drawing.Point(14, 16);
            this.chkEnableTcpIp.Name = "chkEnableTcpIp";
            this.chkEnableTcpIp.Size = new System.Drawing.Size(98, 17);
            this.chkEnableTcpIp.TabIndex = 6;
            this.chkEnableTcpIp.Text = "TCP/IP Enable";
            this.chkEnableTcpIp.UseVisualStyleBackColor = true;
            // 
            // chkEnableModem
            // 
            this.chkEnableModem.AutoSize = true;
            this.chkEnableModem.Location = new System.Drawing.Point(9, 16);
            this.chkEnableModem.Name = "chkEnableModem";
            this.chkEnableModem.Size = new System.Drawing.Size(91, 17);
            this.chkEnableModem.TabIndex = 10;
            this.chkEnableModem.Text = "PSTN Enable";
            this.chkEnableModem.UseVisualStyleBackColor = true;
            // 
            // chkEnableSerial
            // 
            this.chkEnableSerial.AutoSize = true;
            this.chkEnableSerial.Location = new System.Drawing.Point(9, 16);
            this.chkEnableSerial.Name = "chkEnableSerial";
            this.chkEnableSerial.Size = new System.Drawing.Size(95, 17);
            this.chkEnableSerial.TabIndex = 10;
            this.chkEnableSerial.Text = "RS232 Enable";
            this.chkEnableSerial.UseVisualStyleBackColor = true;
            // 
            // FrmInitSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(759, 248);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmInitSetting";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Connection Setting";
            this.Load += new System.EventHandler(this.FrmInitSetting_Load);
            this.gbButton.ResumeLayout(false);
            this.gbData.ResumeLayout(false);
            this.gbData.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbData;
        private System.Windows.Forms.TextBox txtSoftwarePort;
        private System.Windows.Forms.Label lblSoftwarePort;
        private System.Windows.Forms.TextBox txtMaxConn;
        private System.Windows.Forms.Label lblMaxConn;
        private System.Windows.Forms.TextBox txtInitPort;
        private System.Windows.Forms.Label lblPortInit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbModem5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbModem4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbModem3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbModem2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbModem1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbParity;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbDataBit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbStopBit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbBaudRate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbSerial;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkEnableTcpIp;
        private System.Windows.Forms.CheckBox chkEnableModem;
        private System.Windows.Forms.CheckBox chkEnableSerial;
    }
}