namespace InSys
{
    partial class FrmDatabasePrimaryConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDatabasePrimaryConnection));
            this.gbConnection = new System.Windows.Forms.GroupBox();
            this.txtDataSource = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblUserID = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.gbBtnSaveCancel = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbConnection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.gbBtnSaveCancel.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbConnection
            // 
            this.gbConnection.Controls.Add(this.txtDataSource);
            this.gbConnection.Controls.Add(this.lblServer);
            this.gbConnection.Controls.Add(this.PictureBox1);
            this.gbConnection.Controls.Add(this.txtPassword);
            this.gbConnection.Controls.Add(this.txtUserId);
            this.gbConnection.Controls.Add(this.lblPassword);
            this.gbConnection.Controls.Add(this.lblUserID);
            this.gbConnection.Controls.Add(this.btnTest);
            this.gbConnection.Controls.Add(this.txtDatabase);
            this.gbConnection.Controls.Add(this.lblDatabase);
            this.gbConnection.Location = new System.Drawing.Point(9, 2);
            this.gbConnection.Name = "gbConnection";
            this.gbConnection.Size = new System.Drawing.Size(332, 176);
            this.gbConnection.TabIndex = 0;
            this.gbConnection.TabStop = false;
            this.gbConnection.Enter += new System.EventHandler(this.gbConnection_Enter);
            // 
            // txtDataSource
            // 
            this.txtDataSource.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDataSource.Location = new System.Drawing.Point(153, 14);
            this.txtDataSource.Name = "txtDataSource";
            this.txtDataSource.Size = new System.Drawing.Size(168, 20);
            this.txtDataSource.TabIndex = 0;
            // 
            // lblServer
            // 
            this.lblServer.Location = new System.Drawing.Point(67, 14);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(100, 24);
            this.lblServer.TabIndex = 38;
            this.lblServer.Text = "Server";
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("PictureBox1.Image")));
            this.PictureBox1.Location = new System.Drawing.Point(9, 15);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(40, 40);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox1.TabIndex = 36;
            this.PictureBox1.TabStop = false;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(153, 108);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(168, 20);
            this.txtPassword.TabIndex = 3;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(153, 76);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(168, 20);
            this.txtUserId.TabIndex = 2;
            // 
            // lblPassword
            // 
            this.lblPassword.Location = new System.Drawing.Point(67, 108);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(100, 23);
            this.lblPassword.TabIndex = 9;
            this.lblPassword.Text = "Password";
            // 
            // lblUserID
            // 
            this.lblUserID.Location = new System.Drawing.Point(67, 76);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(100, 23);
            this.lblUserID.TabIndex = 8;
            this.lblUserID.Text = "User ID";
            // 
            // btnTest
            // 
            this.btnTest.Image = ((System.Drawing.Image)(resources.GetObject("btnTest.Image")));
            this.btnTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTest.Location = new System.Drawing.Point(181, 134);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(140, 32);
            this.btnTest.TabIndex = 4;
            this.btnTest.Text = "Test ";
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // txtDatabase
            // 
            this.txtDatabase.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDatabase.Location = new System.Drawing.Point(153, 45);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(168, 20);
            this.txtDatabase.TabIndex = 1;
            // 
            // lblDatabase
            // 
            this.lblDatabase.Location = new System.Drawing.Point(67, 48);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(100, 24);
            this.lblDatabase.TabIndex = 27;
            this.lblDatabase.Text = "Database";
            // 
            // gbBtnSaveCancel
            // 
            this.gbBtnSaveCancel.Controls.Add(this.btnCancel);
            this.gbBtnSaveCancel.Controls.Add(this.btnSave);
            this.gbBtnSaveCancel.Location = new System.Drawing.Point(3, 184);
            this.gbBtnSaveCancel.Name = "gbBtnSaveCancel";
            this.gbBtnSaveCancel.Size = new System.Drawing.Size(338, 53);
            this.gbBtnSaveCancel.TabIndex = 1;
            this.gbBtnSaveCancel.TabStop = false;
            this.gbBtnSaveCancel.Enter += new System.EventHandler(this.gbBtnSaveCancel_Enter);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(229, 15);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(98, 32);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(125, 15);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(98, 32);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save ";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmDatabasePrimaryConnection
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(351, 247);
            this.Controls.Add(this.gbConnection);
            this.Controls.Add(this.gbBtnSaveCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmDatabasePrimaryConnection";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Primary Database Connection";
            this.Load += new System.EventHandler(this.FrmDatabasePrimaryConnection_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmDatabasePrimaryConnection_KeyDown);
            this.gbConnection.ResumeLayout(false);
            this.gbConnection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.gbBtnSaveCancel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox gbConnection;
        internal System.Windows.Forms.TextBox txtPassword;
        internal System.Windows.Forms.TextBox txtUserId;
        internal System.Windows.Forms.Label lblPassword;
        internal System.Windows.Forms.Label lblUserID;
        internal System.Windows.Forms.Button btnTest;
        internal System.Windows.Forms.TextBox txtDatabase;
        internal System.Windows.Forms.Label lblDatabase;
        internal System.Windows.Forms.PictureBox PictureBox1;
        private System.Windows.Forms.GroupBox gbBtnSaveCancel;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.TextBox txtDataSource;
        internal System.Windows.Forms.Label lblServer;
    }
}