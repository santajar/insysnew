﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using InSysClass;
using System.IO;
using System.Configuration;

namespace InSys
{
    public partial class FrmLogin2 : Form
    {
        /// <summary>
        /// Local SQLConnection object
        /// </summary>
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        protected int iErrLogin = 0;
        protected int iMaxIncorrectPassword;
        protected bool bLogin = false;
        protected bool bSettingConnection = true;

        public string sConnString;
        public string sConnStringAuditTrail;

        public FrmLogin2()
        {
            InitializeComponent();
            bSettingConnection = bool.Parse(ConfigurationManager.AppSettings["Setting Connection Database"].ToString());
        }

        /// <summary>
        /// Determines if user can login or not.
        /// </summary>
        public bool isCanLoginToSystem = false;

        /// <summary>
        /// Determines if super user can login or not.
        /// </summary>
        public bool isCanLoginToSuperSystem = false;

        /// <summary>
        /// Current UserName
        /// </summary>
        protected string sUserName = "";

        /// <summary>
        /// Current UserID
        /// </summary>
        protected string sUserID = "";

        protected string sUserIDDatabase = "";

        string sPasswordEncrypt;

        /// <summary>
        /// Current Password
        /// </summary>
        protected string sPwd = "";
        protected string sPwdDatabase = "";

        protected string sGroupID = "";


        /// <summary>
        /// Codes for errors
        /// </summary>
        private enum ErrorCode
        {
            NoErr = 1, //avoid start from 0
            ErrLoginUserID,
            ErrLoginUserIDPasswordNotFound,
            ErrLockedUserID
        }

        public FrmLogin2(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        /// <summary>
        /// Get User ID
        /// </summary>
        /// <returns>string : UserID</returns>
        public string sGetUserID()
        {
            return txtUserID.Text/*.Trim()*//*.Replace("'", "''")*/;
        }

        /// <summary>
        /// Get Password
        /// </summary>
        /// <returns>string : Password</returns>
        public string sGetPassword()
        {
            return txtPassword.Text;//.Trim();
            //return sGetEncryptPassword();
        }

        /// <summary>
        /// Get User Name
        /// </summary>
        /// <returns>string : User Name</returns>
        public string sGetUserName()
        {
            return sUserName.ToUpper();
        }

        public string sGetGroupID()
        {
            return sGroupID;
        }

        /// <summary>
        /// Set an indicator indicating whether the objects can respond to user interaction.
        /// </summary>
        /// <param name="isEnabled">bool : The indicator value</param>
        private void SetDisplay(bool isEnabled)
        {
            this.Cursor = isEnabled ? Cursors.Default : Cursors.AppStarting;
            this.ControlBox = isEnabled;

            txtUserID.Enabled = isEnabled;
            txtPassword.Enabled = isEnabled;

            btnOK.Enabled = isEnabled;
            btnCancel.Enabled = isEnabled;
            if (bSettingConnection)
                btnDetail.Enabled = isEnabled;            
        }

        /// <summary>
        /// Set Connection
        /// </summary>
        protected void ChangeConnection()
        {
            oSqlConn = new SqlConnection(sGetConnString());
            oSqlConnAuditTrail = new SqlConnection(sGetConnStringAuditTrail());
        }

        /// <summary>
        /// Event function, runs when btnOK is clicked.
        /// Set UserID, Password, and display form.
        /// </summary>
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(sConnString)) sConnString = sGetConnString();
            oSqlConn = new SqlConnection(sConnString);
            oSqlConn.Open();

            if (string.IsNullOrEmpty(sConnStringAuditTrail)) sConnStringAuditTrail = sGetConnStringAuditTrail();
            oSqlConnAuditTrail = new SqlConnection(sConnStringAuditTrail);
            oSqlConnAuditTrail.Open();

            sUserID = sGetUserID();
            sPwd = sGetPassword();
            SetDisplay(false);
            bLogin = true;
            wrkLogin.RunWorkerAsync();
        }

        /// <summary>
        /// Save Data Connection
        /// </summary>
        protected void SaveData()
        {
            InitData oInitData = new InitData();
            oInitData.doWriteInitData(sGetDataSource(), sGetDatabase(), sGetUserIDSql(), sGetPasswordsql(), sGetDataSourceAuditTrail(), sGetDatabaseAuditTrail(), sGetUserIDSqlAuditTrail(), sGetPasswordsqlAuditTrail());
        }

        #region DataConnection

        protected string sGetDataSource()
        {
            return txtServer.Text.Trim();
        }

        protected string sGetDatabase()
        {
            return txtDatabase.Text.Trim();
        }

        public string sGetUserIDSql()
        {
            return txtUserSql.Text;
        }

        public string sGetPasswordsql()
        {
            return txtPasswordSql.Text;
        }

        protected string sGetDataSourceAuditTrail()
        {
            return txtServerAuditTrail.Text.Trim();
        }

        protected string sGetDatabaseAuditTrail()
        {
            return txtDatabaseAuditTrail.Text.Trim();
        }

        public string sGetUserIDSqlAuditTrail()
        {
            return txtUserAuditTrail.Text;
        }

        public string sGetPasswordsqlAuditTrail()
        {
            return txtPasswordAuditTrail.Text;
        }

        #endregion

        /// <summary>
        /// Validate UserID and Password. 
        /// </summary>
        /// <returns>boolean : true if UserID and Password valid, else false</returns>
        protected bool isValid()
        {
            if (sUserID.Length == 0)
            {
                wrkLogin.ReportProgress(100, ErrorCode.ErrLoginUserID);
                return false;
            }
            else
                if (sPwd.Length == 0)
                {
                    //CommonClass.showErrorMessageTxt(txtPassword, CommonMessage.sErrPassword);
                }
            return true;
        }


        /// <summary>
        /// Determine if The UserID is found in database.
        /// </summary>
        /// <returns>boolean : true if UserID is found, else false</returns>
        protected bool isCanLogin()
        {
            if (isUserIDFound()) // If is User
            {
                isCanLoginToSystem = true;
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Browse the database if UserID requested if Valid
        /// </summary>
        /// <returns>boolean : true if UserID is found, else false</returns>
        protected bool isUserIDFound()
        {
            bool isPass = false;
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        using (DataTable dtUser = new DataTable())
                        {
                            dtUser.Load(oRead);
                            if (dtUser.Rows.Count > 0) // UserID Found
                            {
                                isPass = true;
                                UserPrivilege.sPrivilege = dtUser.Rows[0]["UserRights"].ToString();
                                sUserName = dtUser.Rows[0]["UserName"].ToString();
                                if (dtUser.Columns.IndexOf("GroupID") > -1)
                                    if (dtUser.Columns["GroupID"] != null)
                                        sGroupID = dtUser.Rows[0]["GroupID"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return isPass;
        }

        /// <summary>
        /// Condition to limit UserID search based on UserID and Encrypted Password
        /// </summary>
        /// <returns>string : the filter string</returns>
        protected string sCondition()
        {
            return " WHERE UserID='" + sUserID + "' AND Password='" + sGetEncryptPassword() + "'";
        }

        /// <summary>
        /// Get the encryption of the Password inputed by user
        /// </summary>
        /// <returns>string : Result of Password Encryption</returns>
        protected string sGetEncryptPassword()
        {
            return CommonClass.sGetEncrypt(sPwd);
        }

        /// <summary>
        /// Event function, runs when btnCancel is clicked.
        /// Set isCanLogin to false an close the form.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            isCanLoginToSystem = false;
            this.Close();
        }

        /// <summary>
        /// Starts the login task in a new thread.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void wrkLogin_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!CheckDaysLocked())
            {
                if (isValid())
                    if (!isLocked())
                    {
                        if (isCanLogin())
                            wrkLogin.ReportProgress(100, ErrorCode.NoErr);
                        else
                            wrkLogin.ReportProgress(100, ErrorCode.ErrLoginUserIDPasswordNotFound);
                    }
                    else
                        wrkLogin.ReportProgress(100, ErrorCode.ErrLockedUserID);
            }
            else
                wrkLogin.ReportProgress(100, ErrorCode.ErrLockedUserID);
        }

        private bool CheckDaysLocked()
        {
            bool bReturn = true;
            try
            {
                SqlCommand oSqlCmd;
                oSqlCmd = new SqlCommand(CommonSP.sSPLockedUserIDbyDays, oSqlConn);

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = txtUserID.Text.Trim();
                oSqlCmd.Parameters.Add("@iLocked", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();

                bReturn = oSqlCmd.Parameters["@iLocked"].Value.ToString() == "1" ? true : false;
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }

            return bReturn;
        }

        /// <summary>
        /// Check UserID locked or not
        /// </summary>
        /// <returns>True if locked</returns>
        private bool isLocked()
        {
            bool iLen = true;
            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.isUserIDLocked('{0}')", sUserID), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    iLen = bool.Parse(oRead[0].ToString());
                oRead.Close();
            }
            return iLen;
        }

        /// <summary>
        /// Reports login task process to this thread.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void wrkLogin_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            iMaxIncorrectPassword = iGetMaxIncorrectPassword();
            if (e.ProgressPercentage == 100)
            {
                switch ((ErrorCode)e.UserState)
                {
                    case ErrorCode.NoErr:
                        this.Close();
                        iErrLogin = 0;
                        bLogin = false;
                        break;
                    case ErrorCode.ErrLoginUserID:
                        CommonClass.doShowErrorMessage(CommonMessage.sErrLoginUserID, txtUserID, ObjectType.TEXTBOX, ErrorType.ERROR);
                        iErrLogin = iErrLogin + 1;
                        if (iErrLogin >= iMaxIncorrectPassword)
                        {
                            LockUserID(txtUserID.Text);
                            CommonClass.doShowErrorMessage(CommonMessage.sErrLockedUserID, txtUserID, ObjectType.TEXTBOX, ErrorType.ERROR);
                        }
                        bLogin = false;
                        break;
                    case ErrorCode.ErrLoginUserIDPasswordNotFound:
                        CommonClass.doShowErrorMessage(CommonMessage.sErrLoginUserIDPasswordNotFound, txtUserID, ObjectType.TEXTBOX, ErrorType.ERROR);
                        iErrLogin = iErrLogin + 1;
                        if (iErrLogin >= iMaxIncorrectPassword)
                        {
                            LockUserID(txtUserID.Text);
                            CommonClass.doShowErrorMessage(CommonMessage.sErrLockedUserID, txtUserID, ObjectType.TEXTBOX, ErrorType.ERROR);
                        }
                        bLogin = false;
                        break;
                    case ErrorCode.ErrLockedUserID:
                        CommonClass.doShowErrorMessage(CommonMessage.sErrLockedUserID, txtUserID, ObjectType.TEXTBOX, ErrorType.ERROR);
                        bLogin = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private void LockUserID(string sUserID)
        {
            try
            {
                SqlCommand oSqlCmd;
                oSqlCmd = new SqlCommand(CommonSP.sSPLockedUserID, oSqlConn);

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = txtUserID.Text.Trim();

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Sets display back to original after login task finished (either success or fail).
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void wrkLogin_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay(true);
        }

        #region Connection string
        protected string sGetConnString()
        {
            return (sConnString = CommonClass.sConnString(sGetDataSource(), sGetDatabase(), sGetUserIDSql(), sGetPasswordsql()));
        }

        protected string sGetConnStringAuditTrail()
        {
            return (sConnStringAuditTrail = CommonClass.sConnString(sGetDataSourceAuditTrail(), sGetDatabaseAuditTrail(), sGetUserIDSqlAuditTrail(), sGetPasswordsqlAuditTrail()));
        }
        #endregion

        /// <summary>
        /// Determine Sql Connection open
        /// </summary>
        /// <returns>true : if can connect</returns>
        protected bool isCanConnect()
        {
            try
            {
                new SqlConnection(sGetConnString()).Open();
                new SqlConnection(sGetConnStringAuditTrail()).Open();
                MessageBox.Show(CommonMessage.sPrimeDbConnSuccess);
                return true;
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
        }

        private void btnDetail_Click(object sender, EventArgs e)
        {
            if (bSettingConnection)
            {
                btnDetail.Hide();
                btnHide.Show();
            }
            this.Height = 545;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            if (bSettingConnection)
            {
                btnHide.Hide();
                btnDetail.Show();
            }
            this.Height = 183;
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            ChangeEnable(true);
            ChangeLoginEnable(false);
            txtServer.Select();
            btnChange.Hide();
            btnSave.Show();

        }

        private void ChangeLoginEnable(bool sStatus)
        {
            txtUserID.Enabled = sStatus;
            txtPassword.Enabled = sStatus;
            btnOK.Enabled = sStatus;
            btnCancel.Enabled = sStatus;
            btnHide.Enabled = sStatus;
        }

        private void ChangeEnable(bool sStatus)
        {
            txtServer.Enabled = sStatus;
            txtDatabase.Enabled = sStatus;
            txtPasswordSql.Enabled = sStatus;
            txtUserSql.Enabled = sStatus;
            btnCheckMainConn.Enabled = sStatus;

            txtServerAuditTrail.Enabled = sStatus;
            txtDatabaseAuditTrail.Enabled = sStatus;
            txtPasswordAuditTrail.Enabled = sStatus;
            txtUserAuditTrail.Enabled = sStatus;
            btnCheckAuditTrailConn.Enabled = sStatus;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isCanConnect())
            {
                SaveData();
                ChangeConnection();
                ChangeEnable(false);
                ChangeLoginEnable(true);
                btnSave.Hide();
                btnChange.Show();
                this.Height = 186;
                if (bSettingConnection)
                {
                    btnHide.Hide();
                    btnDetail.Show();
                }
            }
            else this.DialogResult = DialogResult.None;
            CommonClass.InputLog(oSqlConn, "", string.IsNullOrEmpty(UserData.sUserID) ? "" : UserData.sUserID, "", CommonMessage.sUpdateDBPrimaryConn, "");
        } 

        private void FrmLogin2_Load(object sender, EventArgs e)
        {
            this.Height = 186;
            InitData();
            txtUserID.Select();
            btnHide.Hide();
            btnSave.Hide();
            btnDetail.Enabled = btnHide.Enabled =  bSettingConnection;
        }

        private int iGetMaxIncorrectPassword()
        {
            int iLen = 0;
            SqlCommand oCmd = new SqlCommand(string.Format("SELECT DBO.iGetTotalIncorrectPassword()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead != null && oRead.HasRows)
                {
                    DataTable temp = new DataTable();
                    temp.Load(oRead);
                    bool bNumber = int.TryParse(temp.Rows[0][0].ToString(), out iLen);
                    if (!bNumber) iLen = 0;
                }
            }
            return iLen;
        }

        protected void InitData()
        {
            InitData oInitData = new InitData();
            txtServer.Text = oInitData.sGetDataSource();
            txtDatabase.Text = oInitData.sGetDatabase();
            txtUserSql.Text = oInitData.sGetUserID();
            txtPasswordSql.Text = oInitData.sGetPassword();
            txtServerAuditTrail.Text = oInitData.sGetDataSourceAuditTrail();
            txtDatabaseAuditTrail.Text = oInitData.sGetDatabaseAuditTrail();
            txtUserAuditTrail.Text = oInitData.sGetUserIDAuditTrail();
            txtPasswordAuditTrail.Text = oInitData.sGetPasswordAuditTrail();
            if (UserData.isSuperUser)
                txtPassword.UseSystemPasswordChar = false;
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                sUserID = sGetUserID();
                sPwd = sGetPassword();
                SetDisplay(false);
                wrkLogin.RunWorkerAsync();
            }
        }

        private void btnCheckMainConn_Click(object sender, EventArgs e)
        {
            try
            {
                new SqlConnection(sGetConnString()).Open();
                MessageBox.Show(CommonMessage.sPrimeDbConnSuccess);
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void btnCheckAuditTrailConn_Click(object sender, EventArgs e)
        {
            try
            {
                new SqlConnection(sGetConnStringAuditTrail()).Open();
                MessageBox.Show(CommonMessage.sAuditTrailDBConnSucces);
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void FrmLogin2_Closing(object sender, FormClosingEventArgs e)
        {
            if (bLogin == true)
            {
                if (isCanLogin())
                {
                    //if (!PasswordIsComplex(sGetPassword(), 0))
                    //{
                    //    DialogResult DRPassword = MessageBox.Show("Your Password is not standart." + "\n" + "Do you want update your password now?", "Change Password", MessageBoxButtons.YesNo);
                    //    if (DRPassword == DialogResult.Yes)
                    //    {
                    //        UserData.sUserID = sUserID;
                    //        FrmChangePassword fChangePass = new FrmChangePassword(oSqlConn, oSqlConnAuditTrail, sUserID, sGetUserName(), sGetEncryptPassword(), true, false);
                    //        try
                    //        {
                    //            fChangePass.ShowDialog();
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            CommonClass.doWriteErrorFile(ex.Message);
                    //        }
                    //        finally
                    //        {
                    //            fChangePass.Dispose();
                    //        }

                    //    }
                    //}
                    if (isPasswordExpired())
                    {
                        UserData.sUserID = sUserID;
                        this.Hide();
                        FrmChangePassword fChangePass = new FrmChangePassword(oSqlConn, oSqlConnAuditTrail, sUserID, sGetUserName(), sGetEncryptPassword(), true, true);
                        try
                        {
                            fChangePass.ShowDialog();
                        }
                        catch (Exception ex)
                        {
                            CommonClass.doWriteErrorFile(ex.Message);
                        }
                        finally
                        {
                            fChangePass.Dispose();
                        }
                    }
                }
                bLogin = false;
            }
        }

        private bool isPasswordExpired()
        {
            bool bExp = false;
            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.isPasswordExpired('{0}')", sUserID), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    bExp = bool.Parse(oRead[0].ToString());
                oRead.Close();
            }
            return bExp;
        }

        //private bool PasswordIsComplex(string sPassword, int iFunction)
        //{
        //    int iValidConditions = 0;

        //    if (sPassword.Length >= 8 && sPassword.Length <= 15)
        //        iValidConditions++;

        //    if (iValidConditions == 0)
        //    {
        //        if (iFunction == 1)
        //            MessageBox.Show("Passwod Lenght Min 8 and Max 15.");
        //        return false;
        //    }

        //    foreach (char c in sPassword)
        //    {
        //        if (c >= 'a' && c <= 'z')
        //        {
        //            iValidConditions++;
        //            break;
        //        }
        //    }

        //    foreach (char c in sPassword)
        //    {
        //        if (c >= 'A' && c <= 'Z')
        //        {
        //            iValidConditions++;
        //            break;
        //        }
        //    }

        //    if (iValidConditions == 1 || iValidConditions == 2)
        //    {
        //        if (iFunction == 1)
        //            MessageBox.Show("Passwod must contain one Capital letter and one small letter.");
        //        return false;
        //    }

        //    foreach (char c in sPassword)
        //    {
        //        if (c >= '0' && c <= '9')
        //        {
        //            iValidConditions++;
        //            break;
        //        }
        //    }

        //    if (iValidConditions == 3)
        //    {
        //        if (iFunction == 1)
        //            MessageBox.Show("Password must contain one number.");
        //        return false;
        //    }

        //    if (iValidConditions == 4)
        //    {
        //        char[] special = { '@', '#', '$', '%', '^', '&', '+', '=' }; // or whatever

        //        if (sPassword.IndexOfAny(special) == -1)
        //        {
        //            if (iFunction == 1)
        //                MessageBox.Show("Password Must Contain Special Character (@,#,$,%,^,&,+,=)");
        //            return false;
        //        }
        //    }

        //    return true;
        //}

        public void UpdateData()
        {
            bool isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
            try
            {
                SqlCommand oSqlCmd;
                if (isSuperUser)
                    oSqlCmd = new SqlCommand(CommonSP.sSPSuperUserUpdate, oSqlConn);
                else
                    oSqlCmd = new SqlCommand(CommonSP.sSPUserUpdate, oSqlConn);

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sConditions", SqlDbType.VarChar).Value = sUpdateConditions();
                oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = txtUserID.Text.Trim();

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Create the condition string when updating data in database.
        /// </summary>
        /// <returns>string : Condition String</returns>
        public string sUpdateConditions()
        {
            return "SET PASSWORD = '" + sPasswordEncrypt
                            + "' WHERE USERID = '" + sGetUserID() + "'";
        }
    }
}