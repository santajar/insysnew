using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmChangePassword : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Current UserID
        /// </summary>
        protected string sUserID;

        /// <summary>
        /// CurrentUserName
        /// </summary>
        protected string sUserName;

        /// <summary>
        /// Set current password
        /// </summary>
        protected string sPassword;
        protected int iStatus;

        /// <summary>
        /// Is form is called from Main Menu
        /// </summary>
        protected bool isFromMenu;
        protected bool isForcePassword;

        public string sClearNewPassword;

        public FrmChangePassword(SqlConnection _oSqlConn,
                                 SqlConnection _oSqlConnAuditTrail,
                                 string _sUserID,
                                 string _sUserName,
                                 string _sPassword,
                                 bool _isFromMenu,
                                 bool _isForcePassword)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUserID = _sUserID;
            sUserName = _sUserName;
            sPassword = _sPassword;
            isFromMenu = _isFromMenu;
            isForcePassword = _isForcePassword;
            InitializeComponent();
        }

        /// <summary>
        /// Runs when form is loading, call function Initialize.
        /// </summary>
        private void FrmChangePassword_Load(object sender, EventArgs e)
        {
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + "Change Password", "");
            Initialize();
            if (isForcePassword == true)
            {
                btnCancel.Enabled = false;
                this.FormBorderStyle = FormBorderStyle.None;
            }
        }

        /// <summary>
        /// Initialize the objects in form
        /// </summary>
        protected void Initialize()
        {
            txtLoginName.Text = sUserID;
            if (!isFromMenu) txtOldPassword.Text = sPassword.Substring(0, 16);

            txtOldPassword.Enabled = isFromMenu;
        }

        /// <summary>
        /// Encrypts New Password and returns the result
        /// </summary>
        /// <returns>string : Encrypted New Password</returns>
        protected string sGetEncryptPassword()
        {
            return CommonClass.sGetEncrypt(txtConfirmNewPass.Text);
        }

        /// <summary>
        /// Get the encrypted new password
        /// </summary>
        /// <returns>string : Encrypted New Password</returns>
        public string sGetNewPassEncrypted()
        {
            return sPassword;
        }

        /// <summary>
        /// Get the old password from the textbox.
        /// </summary>
        /// <returns>string : Old password</returns>
        protected string sGetOldPassword()
        {
            return txtOldPassword.Text;//.Trim();
        }

        /// <summary>
        /// Get text from NewPassword textbox.
        /// </summary>
        /// <returns>string : NewPassword</returns>
        protected string sGetNewPassword()
        {
            return txtNewPassword.Text;//.Trim();
        }

        /// <summary>
        /// Get text from ConfirmPassword textbox.
        /// </summary>
        /// <returns>string : ConfirmPassword</returns>
        protected string sGetConfirmPassword()
        {
            return txtConfirmNewPass.Text;//.Trim();
        }

        /// <summary>
        /// Event function, runs when btnOK is clicked.
        /// Validating passwords and save the new password to database.
        /// Insert new log for updating password.
        /// </summary>
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (isValid()) // Validating
                    if (!isNewPasswordLikeOldPassword())
                    {
                        if (PasswordIsComplex())
                        {
                            if (CommonClass.isYesMessage(CommonMessage.sChangePassConfirmText, CommonMessage.sConfirmationTitle)) // Confirmation
                            {
                                if (isFromMenu)
                                {
                                    ChangePassword(); // Save new password
                                    //UpdateLastModifyPassword();
                                    //sPassword = sGetEncryptPassword();
                                    MessageBox.Show(CommonMessage.sChangePassSuccess);
                                    CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sChangePass, ""); // Insert new log
                                }
                                else
                                    sPassword = sGetConfirmPassword();

                                this.Dispose();
                            }
                        }
                    }
                    else
                        MessageBox.Show("New password Can not same with Old History.");
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void UpdateLastModifyPassword()
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSuperUserUpdate, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sConditions", SqlDbType.VarChar).Value = string.Format("SET LastModifyPassword = GETDATE() WHERE USERID = '{0}'", sUserID);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
            }
        }

        private bool isNewPasswordLikeOldPassword()
        {
            bool bExist = true;
            try
            {
                if (txtOldPassword.Text != sGetNewPassword())
                {
                    SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCheckHistoryPassword, oSqlConn);
                    oSqlCmd.CommandType = CommandType.StoredProcedure;

                    oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = sUserID;
                    oSqlCmd.Parameters.Add("@sEncrypPassword", SqlDbType.VarChar).Value = sGetEncryptPassword();
                    oSqlCmd.Parameters.Add("@bReturn", SqlDbType.Bit).Value = bExist;
                    oSqlCmd.Parameters["@bReturn"].Direction = ParameterDirection.Output;
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                    oSqlCmd.ExecuteNonQuery();
                    bExist = (bool)oSqlCmd.Parameters["@bReturn"].Value;

                    oSqlCmd.Dispose();
                }
                else
                {
                    bExist = true;
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return bExist;
        }

        private bool PasswordIsComplex()
        {
            int iValidConditions = 0;
            string sPassword = sGetNewPassword();
            int iMinPawsswordLength = iGetPasswordLenght("DBO.iGetMinLengthPassword()");
            int iMaxPasswordLength = iGetPasswordLenght("DBO.iGetMaxLengthPassword()");

            if (sPassword.Length >= iMinPawsswordLength && sPassword.Length <= iMaxPasswordLength)
                iValidConditions++;

            if (iValidConditions == 0)
            {
                MessageBox.Show(string.Format("New Passwod Lenght Min {0} and Max {1}.", iMinPawsswordLength, iMaxPasswordLength));
                return false;
            }

            foreach (char c in sPassword)
            {
                if (c >= 'a' && c <= 'z')
                {
                    iValidConditions++;
                    break;
                }
            }

            foreach (char c in sPassword)
            {
                if (c >= 'A' && c <= 'Z')
                {
                    iValidConditions++;
                    break;
                }
            }

            if (iValidConditions == 1 || iValidConditions == 2)
            {
                MessageBox.Show("New Passwod must contain one Capital letter and one small letter.");
                return false;
            }

            foreach (char c in sPassword)
            {
                if (c >= '0' && c <= '9')
                {
                    iValidConditions++;
                    break;
                }
            }

            if (iValidConditions == 3)
            {
                MessageBox.Show("New Password must contain one number.");
                return false;
            }

            if (iValidConditions == 4)
            {
                char[] special = { '@', '#', '$', '%', '^', '&', '+', '=' }; // or whatever

                if (sPassword.IndexOfAny(special) == -1)
                {
                    MessageBox.Show("New Password Must Contain Special Character (@,#,$,%,^,&,+,=)");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Get Min and Max Lenght Password
        /// </summary>
        /// <param name="sDatabaseID">string : Database ID Value</param>
        /// <returns>int : Lenght of Tag</returns>
        private int iGetPasswordLenght(string sFunction)
        {
            int iLen = 0;
            SqlCommand oCmd = new SqlCommand(string.Format("SELECT {0}", sFunction), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    iLen = int.Parse(oRead[0].ToString());
                oRead.Close();
            }
            return iLen;
        }

        /// <summary>
        /// Validates the passwords inputed by user.
        /// </summary>
        /// <returns>boolean : true if valid, else false</returns>
        protected bool isValid()
        {
            bool isValid = false;
            if (sGetOldPassword().Length <= 0) CommonClass.doShowErrorMessage(CommonMessage.sErrChangePassOldPassEmpty, txtOldPassword, ObjectType.TEXTBOX, ErrorType.ERROR);
            else if (sGetNewPassword().Length <= 0) CommonClass.doShowErrorMessage(CommonMessage.sErrChangePassNewPassEmpty, txtNewPassword, ObjectType.TEXTBOX, ErrorType.ERROR);
            //else if (!CommonClass.isFormatValid(sGetNewPassword(),"A")) CommonClass.doShowErrorMessage(CommonMessage.sErrUserManPassFormat,txtNewPassword, ObjectType.TEXTBOX, ErrorType.ERROR);
            else if (sGetConfirmPassword().Length <= 0) CommonClass.doShowErrorMessage(CommonMessage.sErrChangePassConfirmPassEmtyp, txtConfirmNewPass, ObjectType.TEXTBOX, ErrorType.ERROR);
            else if (isFromMenu && (sOldPasswordDb() != CommonClass.sGetEncrypt(sGetOldPassword()))) CommonClass.doShowErrorMessage(CommonMessage.sErrChangePassMatchOldPass, txtOldPassword, ObjectType.TEXTBOX, ErrorType.ERROR);
            else if (sGetNewPassword() != sGetConfirmPassword()) CommonClass.doShowErrorMessage(CommonMessage.sErrChangePassMatchNewPass, txtNewPassword, ObjectType.TEXTBOX, ErrorType.ERROR);
            else isValid = true;

            return isValid;
        }

        /// <summary>
        /// Get the saved password from the database.
        /// </summary>
        /// <returns>string : Old Password</returns>
        protected string sOldPasswordDb()
        {
            string sOldPwd = "";
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "WHERE UserID = '" + sUserID + "'";
            SqlDataReader oRead = oSqlCmd.ExecuteReader();
            if (oRead.Read())
                sOldPwd = oRead["Password"].ToString();
            oRead.Close();
            oRead.Dispose();
            oSqlCmd.Dispose();
            return sOldPwd;
        }

        /// <summary>
        /// Save the new password to database
        /// </summary>
        protected void ChangePassword()
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSuperUserUpdate, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;

                oSqlCmd.Parameters.Add("@sConditions", SqlDbType.VarChar).Value = sConditions();

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                oSqlCmd.ExecuteNonQuery();

                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Condition for updating password in database.
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sConditions()
        {
            return "SET PASSWORD = '" + sGetEncryptPassword()
                            + "', LastModifyPassword = GETDATE() WHERE USERID = '" + sUserID + "'";
        }

        /// <summary>
        /// Event function, runs when btnCancel is clicked.
        /// Close the form.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}