﻿namespace InSys
{
    partial class FrmLogin2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin2));
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblUserID = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.gbLogin = new System.Windows.Forms.GroupBox();
            this.btnCheckMainConn = new System.Windows.Forms.Button();
            this.txtPasswordSql = new System.Windows.Forms.TextBox();
            this.txtUserSql = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.wrkLogin = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnHide = new System.Windows.Forms.Button();
            this.btnDetail = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCheckAuditTrailConn = new System.Windows.Forms.Button();
            this.txtPasswordAuditTrail = new System.Windows.Forms.TextBox();
            this.txtUserAuditTrail = new System.Windows.Forms.TextBox();
            this.lblPasswordDBAuditTrail = new System.Windows.Forms.Label();
            this.lblUserDBAuditTrail = new System.Windows.Forms.Label();
            this.lblDBAuditTrail = new System.Windows.Forms.Label();
            this.lblServerAuditTrail = new System.Windows.Forms.Label();
            this.txtDatabaseAuditTrail = new System.Windows.Forms.TextBox();
            this.txtServerAuditTrail = new System.Windows.Forms.TextBox();
            this.gbDatabaseConnection = new System.Windows.Forms.GroupBox();
            this.gbLogin.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbDatabaseConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Image = ((System.Drawing.Image)(resources.GetObject("btnOK.Image")));
            this.btnOK.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnOK.Location = new System.Drawing.Point(40, 90);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(98, 30);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(144, 90);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(97, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblUserID
            // 
            this.lblUserID.AutoSize = true;
            this.lblUserID.Location = new System.Drawing.Point(27, 26);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(43, 13);
            this.lblUserID.TabIndex = 15;
            this.lblUserID.Text = "User ID";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(27, 58);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 16;
            this.lblPassword.Text = "Password";
            // 
            // txtUserID
            // 
            this.txtUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUserID.Location = new System.Drawing.Point(107, 23);
            this.txtUserID.MaxLength = 10;
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(190, 20);
            this.txtUserID.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(107, 55);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(190, 20);
            this.txtPassword.TabIndex = 2;
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPassword_KeyDown);
            // 
            // gbLogin
            // 
            this.gbLogin.Controls.Add(this.btnCheckMainConn);
            this.gbLogin.Controls.Add(this.txtPasswordSql);
            this.gbLogin.Controls.Add(this.txtUserSql);
            this.gbLogin.Controls.Add(this.label4);
            this.gbLogin.Controls.Add(this.label3);
            this.gbLogin.Controls.Add(this.label2);
            this.gbLogin.Controls.Add(this.label1);
            this.gbLogin.Controls.Add(this.txtDatabase);
            this.gbLogin.Controls.Add(this.txtServer);
            this.gbLogin.Location = new System.Drawing.Point(6, 19);
            this.gbLogin.Name = "gbLogin";
            this.gbLogin.Size = new System.Drawing.Size(371, 136);
            this.gbLogin.TabIndex = 22;
            this.gbLogin.TabStop = false;
            this.gbLogin.Text = "Newinsys Connection";
            // 
            // btnCheckMainConn
            // 
            this.btnCheckMainConn.Enabled = false;
            this.btnCheckMainConn.Location = new System.Drawing.Point(273, 19);
            this.btnCheckMainConn.Name = "btnCheckMainConn";
            this.btnCheckMainConn.Size = new System.Drawing.Size(95, 23);
            this.btnCheckMainConn.TabIndex = 21;
            this.btnCheckMainConn.Text = "Test Connection";
            this.btnCheckMainConn.UseVisualStyleBackColor = true;
            this.btnCheckMainConn.Click += new System.EventHandler(this.btnCheckMainConn_Click);
            // 
            // txtPasswordSql
            // 
            this.txtPasswordSql.Enabled = false;
            this.txtPasswordSql.Location = new System.Drawing.Point(97, 106);
            this.txtPasswordSql.Name = "txtPasswordSql";
            this.txtPasswordSql.PasswordChar = '*';
            this.txtPasswordSql.ShortcutsEnabled = false;
            this.txtPasswordSql.Size = new System.Drawing.Size(170, 20);
            this.txtPasswordSql.TabIndex = 4;
            // 
            // txtUserSql
            // 
            this.txtUserSql.Enabled = false;
            this.txtUserSql.Location = new System.Drawing.Point(97, 78);
            this.txtUserSql.Name = "txtUserSql";
            this.txtUserSql.Size = new System.Drawing.Size(170, 20);
            this.txtUserSql.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Password DB";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "User DB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Database";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server";
            // 
            // txtDatabase
            // 
            this.txtDatabase.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDatabase.Enabled = false;
            this.txtDatabase.Location = new System.Drawing.Point(97, 49);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(170, 20);
            this.txtDatabase.TabIndex = 2;
            // 
            // txtServer
            // 
            this.txtServer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtServer.Enabled = false;
            this.txtServer.Location = new System.Drawing.Point(97, 19);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(170, 20);
            this.txtServer.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(264, 303);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 25);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnChange
            // 
            this.btnChange.Location = new System.Drawing.Point(264, 303);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(110, 25);
            this.btnChange.TabIndex = 6;
            this.btnChange.Text = "Change Connection";
            this.btnChange.UseVisualStyleBackColor = true;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // wrkLogin
            // 
            this.wrkLogin.WorkerReportsProgress = true;
            this.wrkLogin.WorkerSupportsCancellation = true;
            this.wrkLogin.DoWork += new System.ComponentModel.DoWorkEventHandler(this.wrkLogin_DoWork);
            this.wrkLogin.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.wrkLogin_ProgressChanged);
            this.wrkLogin.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.wrkLogin_RunWorkerCompleted);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnHide);
            this.groupBox1.Controls.Add(this.btnOK);
            this.groupBox1.Controls.Add(this.btnDetail);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.txtUserID);
            this.groupBox1.Controls.Add(this.lblUserID);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.lblPassword);
            this.groupBox1.Location = new System.Drawing.Point(9, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(393, 136);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            // 
            // btnHide
            // 
            this.btnHide.Location = new System.Drawing.Point(248, 90);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(97, 29);
            this.btnHide.TabIndex = 8;
            this.btnHide.Text = "Hide";
            this.btnHide.UseVisualStyleBackColor = true;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // btnDetail
            // 
            this.btnDetail.Location = new System.Drawing.Point(247, 89);
            this.btnDetail.Name = "btnDetail";
            this.btnDetail.Size = new System.Drawing.Size(97, 30);
            this.btnDetail.TabIndex = 5;
            this.btnDetail.Text = "Detail";
            this.btnDetail.UseVisualStyleBackColor = true;
            this.btnDetail.Click += new System.EventHandler(this.btnDetail_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCheckAuditTrailConn);
            this.groupBox2.Controls.Add(this.txtPasswordAuditTrail);
            this.groupBox2.Controls.Add(this.txtUserAuditTrail);
            this.groupBox2.Controls.Add(this.lblPasswordDBAuditTrail);
            this.groupBox2.Controls.Add(this.lblUserDBAuditTrail);
            this.groupBox2.Controls.Add(this.lblDBAuditTrail);
            this.groupBox2.Controls.Add(this.lblServerAuditTrail);
            this.groupBox2.Controls.Add(this.txtDatabaseAuditTrail);
            this.groupBox2.Controls.Add(this.txtServerAuditTrail);
            this.groupBox2.Location = new System.Drawing.Point(6, 161);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(371, 136);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Audit Trail Connection";
            // 
            // btnCheckAuditTrailConn
            // 
            this.btnCheckAuditTrailConn.Enabled = false;
            this.btnCheckAuditTrailConn.Location = new System.Drawing.Point(273, 19);
            this.btnCheckAuditTrailConn.Name = "btnCheckAuditTrailConn";
            this.btnCheckAuditTrailConn.Size = new System.Drawing.Size(95, 23);
            this.btnCheckAuditTrailConn.TabIndex = 20;
            this.btnCheckAuditTrailConn.Text = "Test Connection";
            this.btnCheckAuditTrailConn.UseVisualStyleBackColor = true;
            this.btnCheckAuditTrailConn.Click += new System.EventHandler(this.btnCheckAuditTrailConn_Click);
            // 
            // txtPasswordAuditTrail
            // 
            this.txtPasswordAuditTrail.Enabled = false;
            this.txtPasswordAuditTrail.Location = new System.Drawing.Point(97, 106);
            this.txtPasswordAuditTrail.Name = "txtPasswordAuditTrail";
            this.txtPasswordAuditTrail.PasswordChar = '*';
            this.txtPasswordAuditTrail.ShortcutsEnabled = false;
            this.txtPasswordAuditTrail.Size = new System.Drawing.Size(170, 20);
            this.txtPasswordAuditTrail.TabIndex = 4;
            // 
            // txtUserAuditTrail
            // 
            this.txtUserAuditTrail.Enabled = false;
            this.txtUserAuditTrail.Location = new System.Drawing.Point(97, 78);
            this.txtUserAuditTrail.Name = "txtUserAuditTrail";
            this.txtUserAuditTrail.Size = new System.Drawing.Size(170, 20);
            this.txtUserAuditTrail.TabIndex = 3;
            // 
            // lblPasswordDBAuditTrail
            // 
            this.lblPasswordDBAuditTrail.AutoSize = true;
            this.lblPasswordDBAuditTrail.Location = new System.Drawing.Point(17, 109);
            this.lblPasswordDBAuditTrail.Name = "lblPasswordDBAuditTrail";
            this.lblPasswordDBAuditTrail.Size = new System.Drawing.Size(71, 13);
            this.lblPasswordDBAuditTrail.TabIndex = 19;
            this.lblPasswordDBAuditTrail.Text = "Password DB";
            // 
            // lblUserDBAuditTrail
            // 
            this.lblUserDBAuditTrail.AutoSize = true;
            this.lblUserDBAuditTrail.Location = new System.Drawing.Point(17, 81);
            this.lblUserDBAuditTrail.Name = "lblUserDBAuditTrail";
            this.lblUserDBAuditTrail.Size = new System.Drawing.Size(47, 13);
            this.lblUserDBAuditTrail.TabIndex = 18;
            this.lblUserDBAuditTrail.Text = "User DB";
            // 
            // lblDBAuditTrail
            // 
            this.lblDBAuditTrail.AutoSize = true;
            this.lblDBAuditTrail.Location = new System.Drawing.Point(17, 52);
            this.lblDBAuditTrail.Name = "lblDBAuditTrail";
            this.lblDBAuditTrail.Size = new System.Drawing.Size(53, 13);
            this.lblDBAuditTrail.TabIndex = 1;
            this.lblDBAuditTrail.Text = "Database";
            // 
            // lblServerAuditTrail
            // 
            this.lblServerAuditTrail.AutoSize = true;
            this.lblServerAuditTrail.Location = new System.Drawing.Point(17, 22);
            this.lblServerAuditTrail.Name = "lblServerAuditTrail";
            this.lblServerAuditTrail.Size = new System.Drawing.Size(38, 13);
            this.lblServerAuditTrail.TabIndex = 0;
            this.lblServerAuditTrail.Text = "Server";
            // 
            // txtDatabaseAuditTrail
            // 
            this.txtDatabaseAuditTrail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDatabaseAuditTrail.Enabled = false;
            this.txtDatabaseAuditTrail.Location = new System.Drawing.Point(97, 49);
            this.txtDatabaseAuditTrail.Name = "txtDatabaseAuditTrail";
            this.txtDatabaseAuditTrail.Size = new System.Drawing.Size(170, 20);
            this.txtDatabaseAuditTrail.TabIndex = 2;
            // 
            // txtServerAuditTrail
            // 
            this.txtServerAuditTrail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtServerAuditTrail.Enabled = false;
            this.txtServerAuditTrail.Location = new System.Drawing.Point(97, 19);
            this.txtServerAuditTrail.Name = "txtServerAuditTrail";
            this.txtServerAuditTrail.Size = new System.Drawing.Size(170, 20);
            this.txtServerAuditTrail.TabIndex = 1;
            // 
            // gbDatabaseConnection
            // 
            this.gbDatabaseConnection.Controls.Add(this.btnSave);
            this.gbDatabaseConnection.Controls.Add(this.btnChange);
            this.gbDatabaseConnection.Controls.Add(this.gbLogin);
            this.gbDatabaseConnection.Controls.Add(this.groupBox2);
            this.gbDatabaseConnection.Location = new System.Drawing.Point(12, 161);
            this.gbDatabaseConnection.Name = "gbDatabaseConnection";
            this.gbDatabaseConnection.Size = new System.Drawing.Size(390, 339);
            this.gbDatabaseConnection.TabIndex = 20;
            this.gbDatabaseConnection.TabStop = false;
            this.gbDatabaseConnection.Text = "Database Connection";
            // 
            // FrmLogin2
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 149);
            this.Controls.Add(this.gbDatabaseConnection);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmLogin2";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLogin2_Closing);
            this.Load += new System.EventHandler(this.FrmLogin2_Load);
            this.gbLogin.ResumeLayout(false);
            this.gbLogin.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbDatabaseConnection.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Label lblUserID;
        internal System.Windows.Forms.Label lblPassword;
        internal System.Windows.Forms.TextBox txtUserID;
        internal System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.GroupBox gbLogin;
        private System.ComponentModel.BackgroundWorker wrkLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Button btnDetail;
        private System.Windows.Forms.TextBox txtPasswordSql;
        private System.Windows.Forms.TextBox txtUserSql;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCheckAuditTrailConn;
        private System.Windows.Forms.TextBox txtPasswordAuditTrail;
        private System.Windows.Forms.TextBox txtUserAuditTrail;
        private System.Windows.Forms.Label lblPasswordDBAuditTrail;
        private System.Windows.Forms.Label lblUserDBAuditTrail;
        private System.Windows.Forms.Label lblDBAuditTrail;
        private System.Windows.Forms.Label lblServerAuditTrail;
        private System.Windows.Forms.TextBox txtDatabaseAuditTrail;
        private System.Windows.Forms.TextBox txtServerAuditTrail;
        private System.Windows.Forms.GroupBox gbDatabaseConnection;
        private System.Windows.Forms.Button btnCheckMainConn;
    }
}