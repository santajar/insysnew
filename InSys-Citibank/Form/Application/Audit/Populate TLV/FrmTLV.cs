using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace InSys
{
    public partial class FrmTLV : Form
    {
        protected SqlConnection oSqlConn;
        public FrmTLV(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        private void FrmTLV_Load(object sender, EventArgs e)
        {
            txtStatus.Text = "System ready. Click start button to begin.";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            //CommonClass.inputLog(oSqlConn, "", "", "", CommonMessage.sPopulateTLV, "");
        }
    }
}