namespace InSys
{
    partial class FrmFitur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFitur));
            this.Label1 = new System.Windows.Forms.Label();
            this.progressbarProcess = new System.Windows.Forms.ProgressBar();
            this.GroupXLS = new System.Windows.Forms.GroupBox();
            this.txtProgress = new System.Windows.Forms.TextBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.cbDatabase = new System.Windows.Forms.ComboBox();
            this.RadioButton3 = new System.Windows.Forms.RadioButton();
            this.RadioButton2 = new System.Windows.Forms.RadioButton();
            this.RadioButton1 = new System.Windows.Forms.RadioButton();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.sfdExport = new System.Windows.Forms.SaveFileDialog();
            this.dgridFitur = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bw = new System.ComponentModel.BackgroundWorker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.GroupXLS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgridFitur)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(16, 24);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 23);
            this.Label1.TabIndex = 95;
            this.Label1.Text = "Database";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progressbarProcess
            // 
            this.progressbarProcess.Location = new System.Drawing.Point(16, 88);
            this.progressbarProcess.Name = "progressbarProcess";
            this.progressbarProcess.Size = new System.Drawing.Size(496, 24);
            this.progressbarProcess.Step = 1;
            this.progressbarProcess.TabIndex = 94;
            // 
            // GroupXLS
            // 
            this.GroupXLS.Controls.Add(this.txtProgress);
            this.GroupXLS.Controls.Add(this.Label1);
            this.GroupXLS.Controls.Add(this.progressbarProcess);
            this.GroupXLS.Controls.Add(this.btnStop);
            this.GroupXLS.Controls.Add(this.btnExecute);
            this.GroupXLS.Controls.Add(this.cbDatabase);
            this.GroupXLS.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupXLS.Location = new System.Drawing.Point(12, 12);
            this.GroupXLS.Name = "GroupXLS";
            this.GroupXLS.Size = new System.Drawing.Size(657, 129);
            this.GroupXLS.TabIndex = 88;
            this.GroupXLS.TabStop = false;
            // 
            // txtProgress
            // 
            this.txtProgress.BackColor = System.Drawing.Color.Chartreuse;
            this.txtProgress.Location = new System.Drawing.Point(16, 60);
            this.txtProgress.Name = "txtProgress";
            this.txtProgress.ReadOnly = true;
            this.txtProgress.Size = new System.Drawing.Size(496, 21);
            this.txtProgress.TabIndex = 97;
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(528, 88);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(112, 24);
            this.btnStop.TabIndex = 93;
            this.btnStop.Text = "&Stop";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnExecute
            // 
            this.btnExecute.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExecute.Location = new System.Drawing.Point(528, 58);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(112, 24);
            this.btnExecute.TabIndex = 89;
            this.btnExecute.Text = "E&xecute";
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // cbDatabase
            // 
            this.cbDatabase.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDatabase.Location = new System.Drawing.Point(184, 24);
            this.cbDatabase.Name = "cbDatabase";
            this.cbDatabase.Size = new System.Drawing.Size(160, 23);
            this.cbDatabase.TabIndex = 88;
            // 
            // RadioButton3
            // 
            this.RadioButton3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton3.Location = new System.Drawing.Point(692, 72);
            this.RadioButton3.Name = "RadioButton3";
            this.RadioButton3.Size = new System.Drawing.Size(148, 24);
            this.RadioButton3.TabIndex = 92;
            this.RadioButton3.Text = "Undownloaded Data";
            this.RadioButton3.Visible = false;
            // 
            // RadioButton2
            // 
            this.RadioButton2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton2.Location = new System.Drawing.Point(692, 42);
            this.RadioButton2.Name = "RadioButton2";
            this.RadioButton2.Size = new System.Drawing.Size(152, 24);
            this.RadioButton2.TabIndex = 91;
            this.RadioButton2.Text = "Downloaded Data";
            this.RadioButton2.Visible = false;
            // 
            // RadioButton1
            // 
            this.RadioButton1.Checked = true;
            this.RadioButton1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton1.Location = new System.Drawing.Point(692, 12);
            this.RadioButton1.Name = "RadioButton1";
            this.RadioButton1.Size = new System.Drawing.Size(80, 24);
            this.RadioButton1.TabIndex = 90;
            this.RadioButton1.TabStop = true;
            this.RadioButton1.Text = "All Data";
            this.RadioButton1.Visible = false;
            // 
            // sfdExport
            // 
            this.sfdExport.DefaultExt = "xml";
            this.sfdExport.Filter = "Excel Files(*.xls)|*.xls";
            this.sfdExport.InitialDirectory = "Application.StartupPath";
            // 
            // dgridFitur
            // 
            this.dgridFitur.AllowUserToAddRows = false;
            this.dgridFitur.AllowUserToDeleteRows = false;
            this.dgridFitur.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgridFitur.Location = new System.Drawing.Point(16, 19);
            this.dgridFitur.Name = "dgridFitur";
            this.dgridFitur.ReadOnly = true;
            this.dgridFitur.Size = new System.Drawing.Size(624, 230);
            this.dgridFitur.TabIndex = 89;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgridFitur);
            this.groupBox1.Location = new System.Drawing.Point(12, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(657, 266);
            this.groupBox1.TabIndex = 90;
            this.groupBox1.TabStop = false;
            // 
            // bw
            // 
            this.bw.WorkerReportsProgress = true;
            this.bw.WorkerSupportsCancellation = true;
            this.bw.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw_DoWork);
            this.bw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bw_RunWorkerCompleted);
            this.bw.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bw_ProgressChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnClose);
            this.groupBox2.Location = new System.Drawing.Point(12, 434);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(657, 55);
            this.groupBox2.TabIndex = 91;
            this.groupBox2.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(528, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(112, 24);
            this.btnClose.TabIndex = 94;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FrmFitur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 425);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GroupXLS);
            this.Controls.Add(this.RadioButton3);
            this.Controls.Add(this.RadioButton1);
            this.Controls.Add(this.RadioButton2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmFitur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fitur";
            this.Load += new System.EventHandler(this.FrmFitur_Load);
            this.GroupXLS.ResumeLayout(false);
            this.GroupXLS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgridFitur)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.ProgressBar progressbarProcess;
        internal System.Windows.Forms.GroupBox GroupXLS;
        internal System.Windows.Forms.Button btnStop;
        internal System.Windows.Forms.RadioButton RadioButton3;
        internal System.Windows.Forms.RadioButton RadioButton2;
        internal System.Windows.Forms.RadioButton RadioButton1;
        internal System.Windows.Forms.Button btnExecute;
        internal System.Windows.Forms.ComboBox cbDatabase;
        private System.Windows.Forms.SaveFileDialog sfdExport;
        private System.Windows.Forms.DataGridView dgridFitur;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.ComponentModel.BackgroundWorker bw;
        private System.Windows.Forms.TextBox txtProgress;
        private System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.Button btnClose;
    }
}