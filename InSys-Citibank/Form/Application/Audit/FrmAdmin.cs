using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmAdmin : Form
    {
        public FrmAdmin()
        {
            InitializeComponent();
        }

        private void FrmAdmin_Load(object sender, EventArgs e)
        {
            TabColExcel.Dispose();
        }

        private void btnXportSum_Click(object sender, EventArgs e)
        {
            sfdExport.ShowDialog();
        }

        private void btnXportDetail_Click(object sender, EventArgs e)
        {
            sfdExport.ShowDialog();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void btnCloseDetail_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void btnCloseSum_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

    }
}