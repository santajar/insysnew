using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmVersionSelection : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected DataTable oDataTable = new DataTable();

        protected string sUserID;

        /// <summary>
        /// Form which show list of versions in Insys
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sUserID">String : Current active user</param>
        public FrmVersionSelection(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sUserID)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUserID = _sUserID;
            InitializeComponent();
        }

        /// <summary>
        /// Runs when form is closing.
        /// If Form Expand is still opened while FrmVersionSelection is closed then FrmExpand is closed too.
        /// </summary>
        private void FrmVersionSelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.MdiFormClosing)
                foreach (Form oForm in Application.OpenForms)
                    if (oForm is FrmExpand)
                    {
                        oForm.Close();
                        break;
                    }
        }

        /// <summary>
        /// Runs while form is loading, set data for form.
        /// </summary>
        private void FrmVersionSelection_Load(object sender, EventArgs e)
        {
            InitData();
            CommonClass.InputLog(oSqlConn, "", sUserID, "", CommonMessage.sFormOpened + "Version Selection", "");
        }

        /// <summary>
        /// Runs when form is activated.
        /// Minimize other form and set form's state to Normal.
        /// </summary>
        private void FrmVersionSelection_Activated(object sender, EventArgs e)
        {
            //CommonClass.doMinimizeChild(this.ParentForm.MdiChildren, this.Name);
            //this.WindowState = FormWindowState.Normal;
        }

        /// <summary>
        /// Initiate data, get data from database and show it on list view.
        /// </summary>
        protected void InitData()
        {
            LoadTerminalDB();
            DisplayTerminalDB();
        }

        /// <summary>
        /// Get data of versions from database.
        /// </summary>
        protected void LoadTerminalDB()
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                SqlDataReader oRead = oSqlCmd.ExecuteReader();

                oDataTable.Load(oRead);

                oSqlCmd.Dispose();
                oRead.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Show versions from database at list view.
        /// </summary>
        protected void DisplayTerminalDB()
        {
            int iRowCount = oDataTable.Rows.Count;
            for (int iCounter = 0; iCounter < iRowCount; iCounter++)
            {
                string sListDatabaseName = oDataTable.Rows[iCounter]["DatabaseName"].ToString();
                lvDb.Items.Add(sListDatabaseName);
                lvDb.Items[iCounter].ImageIndex = 2;
            }
        }
       
        /// <summary>
        /// Get selected index of selected version from list view.
        /// </summary>
        /// <returns>int : Selected index</returns>
        protected int iGetSelectedIndex()
        {
            return lvDb.SelectedIndices[0];
        }

        /// <summary>
        /// Get DatabaseID from selected version on List View.
        /// </summary>
        /// <returns>string : Database ID</returns>
        protected string sGetDatabaseID()
        {
            return oDataTable.Rows[iGetSelectedIndex()]["DatabaseID"].ToString();
        }

        /// <summary>
        /// Get DatabaseName from selected version on List View.
        /// </summary>
        /// <returns>string : DatabaseName</returns>
        protected string sGetDatabaseName()
        {
            return oDataTable.Rows[iGetSelectedIndex()]["DatabaseName"].ToString();
        }

        /// <summary>
        /// Get EMVInit boolean from selected version on List View
        /// </summary>
        /// <returns>bool : EMV(AID-CAPK) Init</returns>
        protected bool IsEMVInit()
        {
            return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["EMVInit"].ToString());
        }

        /// <summary>
        /// Get EMVInitManagement boolean from selected version on List View
        /// </summary>
        /// <returns>bool : EMV Init Management</returns>
        private bool isEMVInitManagement()
        {
            try
            {
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["EMVInitManagement"].ToString());
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Get EMVInitManagement boolean from selected version on List View
        /// </summary>
        /// <returns>bool : EMV Init Management</returns>
        private bool isCustomMenu()
        {
            try
            {
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["Custom_Menu"].ToString());
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Determine if selected versions has TLE tag or not
        /// </summary>
        /// <returns>Boolean : true if have TLV Tag</returns>
        protected bool IsTLEInit()
        {
            if (oDataTable.Columns.Contains("TLEInit"))
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["TLEInit"].ToString());
            else
                return false;
        }

        /// <summary>
        /// Determine if selected versions has LoyaltyProd tag or not
        /// </summary>
        /// <returns>Boolean : true if have LoyaltyProd Tag</returns>
        protected bool IsLoyaltyProd()
        {
            if (oDataTable.Columns.Contains("LoyaltyProd"))
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["LoyaltyProd"].ToString());
            else
                return false;
        }

        /// <summary>
        /// Determine if selected versions has GPRS tag or not
        /// </summary>
        /// <returns>Boolean : true if have GPRS Tag</returns>
        protected bool IsGPRS()
        {
            if (oDataTable.Columns.Contains("GPRSInit"))
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["GPRSInit"].ToString());
            else
                return false;
        }

        /// <summary>
        /// Determine if selected versions has Currency tag or not
        /// </summary>
        /// <returns>Boolean : true if have Currency Tag</returns>
        protected bool IsCurrency()
        {
            if (oDataTable.Columns.Contains("CurrencyInit"))
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["CurrencyInit"].ToString());
            else
                return false;
        }

        /// <summary>
        /// Determine if selected versions has PinPad tag or not
        /// </summary>
        /// <returns>Boolean : true if have PinPad Tag</returns>
        protected bool IsPinPad()
        {
            if (oDataTable.Columns.Contains("PinPad"))
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["PinPad"].ToString());
            else
                return false;
        }

        /// <summary>
        /// Determine if selected versions has RemoteDownload tag or not
        /// </summary>
        /// <returns>Boolean : true if have RemoteDownload Tag</returns>
        protected bool IsRemoteDownload()
        {
            if (oDataTable.Columns.Contains("RemoteDownload"))
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["RemoteDownload"].ToString());
            else
                return false;
        }

        /// <summary>
        /// Determine if selected versions has BankCode tag or not
        /// </summary>
        /// <returns>Boolean : true if have BankCode Tag</returns>
        protected bool IsBankCode()
        {
            if (oDataTable.Columns.Contains("BankCode"))
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["BankCode"].ToString());
            else
                return false;
        }

        /// <summary>
        /// Determine if selected versions has ProductCode tag or not
        /// </summary>
        /// <returns>Boolean : true if have ProductCode Tag</returns>
        protected bool IsProductCode()
        {
            if (oDataTable.Columns.Contains("ProductCode"))
                return bool.Parse(oDataTable.Rows[iGetSelectedIndex()]["ProductCode"].ToString());
            else
                return false;
        }

        /// <summary>
        /// Runs when items on List View is double clicked. 
        /// Open form Expand to show all profiles of selected version.
        /// </summary>
        private void lvDb_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (lvDb.SelectedItems.Count > 0) // Item selected
                {
                    foreach (Form oForm in this.MdiParent.MdiChildren)
                    {
                        if (oForm is FrmExpand) // If form expand already opened.
                        {
                            ((FrmExpand)oForm).sUID = sUserID;
                            ((FrmExpand)oForm).sDbID = sGetDatabaseID();
                            ((FrmExpand)oForm).sDbName = sGetDatabaseName();
                            ((FrmExpand)oForm).isEMVInit = IsEMVInit();
                            ((FrmExpand)oForm).isTLEInit = IsTLEInit();
                            ((FrmExpand)oForm).isLoyaltyProd = IsLoyaltyProd();
                            ((FrmExpand)oForm).isGPRS = IsGPRS();
                            ((FrmExpand)oForm).isCurrency = IsCurrency();
                            ((FrmExpand)oForm).isPinPad = IsPinPad();
                            ((FrmExpand)oForm).isRemoteDownload = IsRemoteDownload();
                            ((FrmExpand)oForm).isBankCode = IsBankCode();
                            ((FrmExpand)oForm).isProductCode = IsProductCode();
                            ((FrmExpand)oForm).isEMVInitManagement = isEMVInitManagement();
                            ((FrmExpand)oForm).isCustomMenu = isCustomMenu();
                            oForm.Activate();
                            oForm.WindowState = FormWindowState.Maximized;
                            return;
                        }
                    }

                    //FrmExpand fExpandTerminal = new FrmExpand(oSqlConn, sUserID, sGetDatabaseID(),
                    //    sGetDatabaseName(), IsEMVInit(), IsTLEInit(), IsLoyaltyProd(), IsGPRS(), IsCurrency(), IsPinPad(),IsRemoteDownload());

                    //FrmExpand fExpandTerminal = new FrmExpand(oSqlConn, sUserID, sGetDatabaseID(),
                    //    sGetDatabaseName(), IsEMVInit(), IsTLEInit(), IsLoyaltyProd(), IsGPRS(), IsCurrency(), IsPinPad(), IsRemoteDownload(),
                    //    IsBankCode(), IsProductCode(),isEMVInitManagement());

                    FrmExpand fExpandTerminal = new FrmExpand(oSqlConn, oSqlConnAuditTrail, sUserID, sGetDatabaseID(),
                        sGetDatabaseName(), IsEMVInit(), IsTLEInit(), IsLoyaltyProd(), IsGPRS(), IsCurrency(), IsPinPad(), IsRemoteDownload(),
                        IsBankCode(), IsProductCode(), isEMVInitManagement(),isCustomMenu());

                    fExpandTerminal.MdiParent = this.ParentForm;
                    
                    fExpandTerminal.Show();
                    fExpandTerminal.WindowState = FormWindowState.Maximized;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Runs when Expand context menu is clicked, call function to open form Expand.
        /// </summary>
        private void smiExpand_Click(object sender, EventArgs e)
        {
            lvDb_DoubleClick(sender, e);
        }

        /// <summary>
        /// Close the form.
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Expand selected version if item selected and enter key is pressed.
        /// </summary>
        private void lvDb_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Enter) lvDb_DoubleClick(sender, e);
        }

        private void lvDb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lvDb_Leave(object sender, EventArgs e)
        {
 
            
        }
    }
}