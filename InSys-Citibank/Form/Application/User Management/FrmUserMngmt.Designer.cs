namespace InSys
{
    partial class FrmUserMngmt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUserMngmt));
            this.gbAllUser = new System.Windows.Forms.GroupBox();
            this.dgUserView = new System.Windows.Forms.DataGridView();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.gbAllUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUserView)).BeginInit();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbAllUser
            // 
            this.gbAllUser.Controls.Add(this.dgUserView);
            this.gbAllUser.Location = new System.Drawing.Point(12, 3);
            this.gbAllUser.Name = "gbAllUser";
            this.gbAllUser.Size = new System.Drawing.Size(292, 418);
            this.gbAllUser.TabIndex = 1;
            this.gbAllUser.TabStop = false;
            // 
            // dgUserView
            // 
            this.dgUserView.AllowUserToAddRows = false;
            this.dgUserView.AllowUserToDeleteRows = false;
            this.dgUserView.AllowUserToResizeRows = false;
            this.dgUserView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUserView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgUserView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUserView.Location = new System.Drawing.Point(6, 10);
            this.dgUserView.MultiSelect = false;
            this.dgUserView.Name = "dgUserView";
            this.dgUserView.ReadOnly = true;
            this.dgUserView.RowHeadersVisible = false;
            this.dgUserView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgUserView.Size = new System.Drawing.Size(281, 403);
            this.dgUserView.TabIndex = 4;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnEdit);
            this.gbButton.Controls.Add(this.btnAdd);
            this.gbButton.Location = new System.Drawing.Point(12, 425);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(292, 45);
            this.gbButton.TabIndex = 3;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(197, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 27);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(101, 13);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(90, 27);
            this.btnEdit.TabIndex = 10;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(5, 13);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(90, 27);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // FrmUserMngmt
            // 
            this.AcceptButton = this.btnAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(313, 479);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbAllUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmUserMngmt";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "User Management";
            this.Load += new System.EventHandler(this.FrmUserMngmt_Load);
            this.gbAllUser.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUserView)).EndInit();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbAllUser;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnEdit;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView dgUserView;
    }
}