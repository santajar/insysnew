﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmProfileGroup : Form
    {
        protected SqlConnection oConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected string sUID;

        public FrmProfileGroup(SqlConnection _oConn, SqlConnection _oSqlConnAuditTrail, string _sUID)
        {
            oConn = _oConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUID = _sUID;
            InitializeComponent();
        }

        private void FrmProfileGroup_Load(object sender, EventArgs e)
        {
            doFillDataGridView();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                FrmItemUserRight fItemRight = new FrmItemUserRight(oConn, sUID, false, "");
                fItemRight.ShowDialog();
                doFillDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgGroup.SelectedCells.Count == 1)
                {
                    FrmItemUserRight fItemRight = new FrmItemUserRight(oConn, sUID, true, dgGroup.SelectedCells[0].Value.ToString());
                    fItemRight.ShowDialog(); 
                    doFillDataGridView();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region "Function"
        protected void doFillDataGridView()
        {
            dgGroup.DataSource = null;
            dgGroup.DataSource = dtGroupList().DefaultView.ToTable(true, "GroupID");
            //foreach (DataGridViewColumn dgCol in dgGroup.Columns)
            //    dgCol.Visible = false;
            //dgGroup.Columns["GroupID"].Visible = true;
            dgGroup.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        protected DataTable dtGroupList()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oComm = new SqlCommand(CommonSP.sSPUserGroupBrowse, oConn))
                {
                    oComm.CommandType = CommandType.StoredProcedure;
                    oComm.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "";
                    SqlDataAdapter oAdapt = new SqlDataAdapter(oComm);
                    oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dtTable;
        }
        #endregion
    }
}
