using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmUserMngmt_Edit : Form
    {
        /// <summary>
        /// Determines form's current mode, for Editing or Adding.
        /// </summary>
        protected bool isEdit;
        protected bool isAllowEdit;
        protected bool isSuperUser;
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected DataTable oDataTable = new DataTable();
        protected string sUID = "";

        public DataTable dtTempDataMenu = new DataTable();
        public bool bSoftwarePackage = false;
        public bool bLocation = false;
        public bool bUtilities = false;
        public bool bAID = false;
        public bool bKey = false;
        public bool bCardRange = false;
        public bool bTLE = false;
        public bool bGPRSManagemenet = false;
        public bool bPinPadManagement = false;
        public bool bRemoteDownload = false;
        public bool bEMVManagement = false;
        public bool bProductCode = false;
        public bool bBankCode = false;
        public bool bCurrenyManagement = false;
        public bool bLoyaltyProduct = false;
        public bool bAuditTrailWeb = false;
        public bool bSoftwareProgressWeb = false;
        public bool bSchedule = false;
        public bool bPopulateTLV = false;
        /// <summary>
        /// Encrypted password
        /// </summary>
        protected string sPasswordEncrypt = "";

        /// <summary>
        /// Needed as a flag to determine whether the user Edit or just View the selected user. Used for logging purpose.
        /// </summary>
        protected bool isLogged = false;

        protected struct CheckBox_Struct
        {
            readonly string sDesc;
            readonly string sKey;
            CheckBox chkView;
            CheckBox[] arrCheckBox;
            int iArraySize;

            public CheckBox_Struct(string _sDesc, string _sKey,CheckBox _chkView, CheckBox[] _arrCheckBox, int _iArraySize)
            {
                sDesc = _sDesc;
                sKey = _sKey;
                chkView = _chkView;
                arrCheckBox = _arrCheckBox;
                iArraySize = _iArraySize;
            }

            public string sDescription { get { return sDesc;} }
            public string sFieldKey { get { return sKey; } }
            public CheckBox chkMaster { get { return chkView; } }
            public CheckBox[] chkDetail{ get { return arrCheckBox; }}
            public int iSize { get { return iArraySize; } }
        }

        #region "ArrayCheckBox"
        protected CheckBox_Struct[] UserRight;
        protected CheckBox[] arrAllowInit;
        protected CheckBox[] arrAutoInit;
        protected CheckBox[] arrCompressInit;
        protected CheckBox[] arrSNValidation;
        protected CheckBox[] arrAuditTrail;
        protected CheckBox[] arrInitTrail;
        protected CheckBox[] arrPopulateTLV;
        protected CheckBox[] arrData;
        protected CheckBox[] arrUserMan;
        protected CheckBox[] arrLocation;   
        protected CheckBox[] arrMonitor;
        protected CheckBox[] arrDownload;
        protected CheckBox[] arrCreateTerminal;
        protected CheckBox[] arrRegOldFile;
        protected CheckBox[] arrSoftwarePackage;

        protected CheckBox[] arrAID;
        protected CheckBox[] arrCAPK;
        protected CheckBox[] arrCardRange;
        protected CheckBox[] arrTLE;
        protected CheckBox[] arrDE;
        protected CheckBox[] arrAA;
        protected CheckBox[] arrAE;
        protected CheckBox[] arrAC;
        protected CheckBox[] arrLP;
        protected CheckBox[] arrGP;
        protected CheckBox[] arrCurr;
        protected CheckBox[] arrSchedule;
        protected CheckBox[] arrQueryReport;
        protected CheckBox[] arrInitPicture;
        protected CheckBox[] arrPP;
        protected CheckBox[] arrKB; //mycode
        protected CheckBox[] arrKP; //mycode
        protected CheckBox[] arrRD;
        protected CheckBox[] arrEM;
        protected CheckBox[] arrAuditTrailWeb;
        protected CheckBox[] arrSoftwareTrailWeb;
        #endregion

        public FrmUserMngmt_Edit(bool _isEdit, SqlConnection _oSqlConn,SqlConnection _oSqlConnAuditTrail, bool _isSuperUser)
        {
            InitializeComponent();
            isEdit = _isEdit;
            isSuperUser = _isSuperUser;
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        #region "Load"
        /// <summary>
        /// Initialize all checkboxes in form and set the form's display.
        /// Runs while form is loading.
        /// </summary>
        private void FrmUserMngmt_Edit_Load(object sender, EventArgs e)
        {
            isAllowEdit = UserPrivilege.IsAllowed(PrivilegeCode.UM, Privilege.Edit);
            InitCheckBox();
            SetDisplayForm();
            SetDisplayMenu();
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        private void SetDisplayMenu()
        {
            dtTempDataMenu = GetDataMenu();
            foreach (DataRow dr in dtTempDataMenu.Rows)
            {
                if (dr["MenuName"].ToString() == "SoftwarePackage")
                    bSoftwarePackage = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "Location")
                    bLocation = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "Utilities")
                    bUtilities = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "AID")
                    bAID = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "Key")
                    bKey = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "CardRange")
                    bCardRange = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "TLE")
                    bTLE = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "GPRSManagemenet")
                    bGPRSManagemenet = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "PinPadManagement")
                    bPinPadManagement = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "RemoteDownload")
                    bRemoteDownload = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "EMVManagement")
                    bEMVManagement = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "ProductCode")
                    bProductCode = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "BankCode")
                    bBankCode = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "CurrencyManagement")
                    bCurrenyManagement = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "LoyaltyProduct")
                    bLoyaltyProduct = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "AuditTrailWeb")
                    bAuditTrailWeb = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "SoftwareProgressWeb")
                    bSoftwareProgressWeb = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "Schedule")
                    bSchedule = Convert.ToBoolean(dr["status"].ToString());
                else if (dr["MenuName"].ToString() == "PopulateTLV")
                    bPopulateTLV = Convert.ToBoolean(dr["status"].ToString());
            }

            #region "Menu"
                int iCount=0;
                #region "Schedule"
                chkSchedule.Visible = chkSchedule.Enabled = bSchedule;
                #endregion

                #region "PopulateTLV"
                chkPopulateTLV.Visible = chkPopulateTLV.Enabled = bPopulateTLV;
                if (!bSchedule)
                    chkPopulateTLV.Location = chkSchedule.Location;
                #endregion

                #region "Utilities"
                gbUtilities.Visible = gbUtilities.Enabled = bUtilities;
                if (bSoftwarePackage)
                {
                    if (!bLocation)
                        gbUtilities.Location = gbLocation.Location;
                }
                else
                {
                    if (bLocation)
                        gbUtilities.Location = gbLocation.Location;
                    else
                        gbUtilities.Location = gbSoftwarePackage.Location;
                }
                #endregion

                #region "Location"
                gbLocation.Visible = gbLocation.Enabled = bLocation;
                if (!bSoftwarePackage)
                    gbLocation.Location = gbSoftwarePackage.Location;
                #endregion

                #region "SoftwarePackage"
                gbSoftwarePackage.Visible = gbSoftwarePackage.Enabled = bSoftwarePackage;
                #endregion
            #endregion

            #region "Profile"
                #region "LoyaltyProduct"
                gbLoyaltyProd.Enabled = gbLoyaltyProd.Visible = bLoyaltyProduct;
                iCount = 0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;
                if (bKey)
                    iCount += 1;
                if (bCardRange)
                    iCount += 1;
                if (bRemoteDownload)
                    iCount += 1;
                if (bPinPadManagement)
                    iCount += 1;
                if (bEMVManagement)
                    iCount += 1;
                if (bProductCode)
                    iCount += 1;
                if (bBankCode)
                    iCount += 1;
                if (bCurrenyManagement)
                    iCount += 1;
                if (bLoyaltyProduct)
                    iCount += 1;

                if (iCount == 1)
                    gbLoyaltyProd.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbLoyaltyProd.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbLoyaltyProd.Location = gbAID.Location;
                else if (iCount == 4)
                    gbLoyaltyProd.Location = gbKey.Location;
                else if (iCount == 5)
                    gbLoyaltyProd.Location = gbCardRange.Location;
                else if (iCount == 6)
                    gbLoyaltyProd.Location = gbRemoteDownload.Location;
                else if (iCount == 7)
                    gbLoyaltyProd.Location = gbPinPad.Location;
                else if (iCount == 8)
                    gbLoyaltyProd.Location = gbEMVManagement.Location;
                else if (iCount == 9)
                    gbLoyaltyProd.Location = gbProductCode.Location;
                else if (iCount == 10)
                    gbLoyaltyProd.Location = gbBankCode.Location;
                else if (iCount == 11)
                    gbLoyaltyProd.Location = gbCurrency.Location;
                else if (iCount == 12)
                    gbLoyaltyProd.Location = gbLoyaltyProd.Location;
                #endregion

                #region "CurrencyManagement"
                gbCurrency.Enabled = gbCurrency.Visible = bCurrenyManagement;
                iCount = 0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;
                if (bKey)
                    iCount += 1;
                if (bCardRange)
                    iCount += 1;
                if (bRemoteDownload)
                    iCount += 1;
                if (bPinPadManagement)
                    iCount += 1;
                if (bEMVManagement)
                    iCount += 1;
                if (bProductCode)
                    iCount += 1;
                if (bBankCode)
                    iCount += 1;
                if (bCurrenyManagement)
                    iCount += 1;

                if (iCount == 1)
                    gbCurrency.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbCurrency.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbCurrency.Location = gbAID.Location;
                else if (iCount == 4)
                    gbCurrency.Location = gbKey.Location;
                else if (iCount == 5)
                    gbCurrency.Location = gbCardRange.Location;
                else if (iCount == 6)
                    gbCurrency.Location = gbRemoteDownload.Location;
                else if (iCount == 7)
                    gbCurrency.Location = gbPinPad.Location;
                else if (iCount == 8)
                    gbCurrency.Location = gbEMVManagement.Location;
                else if (iCount == 9)
                    gbCurrency.Location = gbProductCode.Location;
                else if (iCount == 10)
                    gbCurrency.Location = gbBankCode.Location;
                else if (iCount == 11)
                    gbCurrency.Location = gbCurrency.Location;
                #endregion

                #region "BankCode"
                gbBankCode.Enabled = gbBankCode.Visible = bBankCode;
                iCount = 0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;
                if (bKey)
                    iCount += 1;
                if (bCardRange)
                    iCount += 1;
                if (bRemoteDownload)
                    iCount += 1;
                if (bPinPadManagement)
                    iCount += 1;
                if (bEMVManagement)
                    iCount += 1;
                if (bProductCode)
                    iCount += 1;
                if (bBankCode)
                    iCount += 1;

                if (iCount == 1)
                    gbBankCode.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbBankCode.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbBankCode.Location = gbAID.Location;
                else if (iCount == 4)
                    gbBankCode.Location = gbKey.Location;
                else if (iCount == 5)
                    gbBankCode.Location = gbCardRange.Location;
                else if (iCount == 6)
                    gbBankCode.Location = gbRemoteDownload.Location;
                else if (iCount == 7)
                    gbBankCode.Location = gbPinPad.Location;
                else if (iCount == 8)
                    gbBankCode.Location = gbEMVManagement.Location;
                else if (iCount == 9)
                    gbBankCode.Location = gbProductCode.Location;
                else if (iCount == 10)
                    gbBankCode.Location = gbBankCode.Location;
                #endregion

                #region"ProductCode"
                gbProductCode.Visible = gbProductCode.Enabled = bProductCode;
                iCount = 0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;
                if (bKey)
                    iCount += 1;
                if (bCardRange)
                    iCount += 1;
                if (bRemoteDownload)
                    iCount += 1;
                if (bPinPadManagement)
                    iCount += 1;
                if (bEMVManagement)
                    iCount += 1;
                if (bProductCode)
                    iCount += 1;

                if (iCount == 1)
                    gbProductCode.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbProductCode.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbProductCode.Location = gbAID.Location;
                else if (iCount == 4)
                    gbProductCode.Location = gbKey.Location;
                else if (iCount == 5)
                    gbProductCode.Location = gbCardRange.Location;
                else if (iCount == 6)
                    gbProductCode.Location = gbRemoteDownload.Location;
                else if (iCount == 7)
                    gbProductCode.Location = gbPinPad.Location;
                else if (iCount == 8)
                    gbProductCode.Location = gbEMVManagement.Location;
                else if (iCount == 9)
                    gbProductCode.Location = gbProductCode.Location;
                #endregion

                #region "EMVManagement"
                gbEMVManagement.Visible = gbEMVManagement.Enabled = bEMVManagement;
                iCount = 0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;
                if (bKey)
                    iCount += 1;
                if (bCardRange)
                    iCount += 1;
                if (bRemoteDownload)
                    iCount += 1;
                if (bPinPadManagement)
                    iCount += 1;
                if (bEMVManagement)
                    iCount += 1;

                if (iCount == 1)
                    gbEMVManagement.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbEMVManagement.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbEMVManagement.Location = gbAID.Location;
                else if (iCount == 4)
                    gbEMVManagement.Location = gbKey.Location;
                else if (iCount == 5)
                    gbEMVManagement.Location = gbCardRange.Location;
                else if (iCount == 6)
                    gbEMVManagement.Location = gbRemoteDownload.Location;
                else if (iCount == 7)
                    gbEMVManagement.Location = gbPinPad.Location;
                else if (iCount == 8)
                    gbEMVManagement.Location = gbEMVManagement.Location;
                #endregion

                #region "PinPadManagement"
                gbPinPad.Visible = gbPinPad.Enabled = bPinPadManagement;
                iCount = 0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;
                if (bKey)
                    iCount += 1;
                if (bCardRange)
                    iCount += 1;
                if (bRemoteDownload)
                    iCount += 1;
                if (bPinPadManagement)
                    iCount += 1;

                if (iCount == 1)
                    gbPinPad.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbPinPad.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbPinPad.Location = gbAID.Location;
                else if (iCount == 4)
                    gbPinPad.Location = gbKey.Location;
                else if (iCount == 5)
                    gbPinPad.Location = gbCardRange.Location;
                else if (iCount == 6)
                    gbPinPad.Location = gbRemoteDownload.Location;
                else if (iCount == 7)
                    gbPinPad.Location = gbPinPad.Location;
                #endregion

                #region "RemoteDownload"
                gbRemoteDownload.Enabled = gbRemoteDownload.Visible = bRemoteDownload;
                iCount = 0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;
                if (bKey)
                    iCount += 1;
                if (bCardRange)
                    iCount += 1;
                if (bRemoteDownload)
                    iCount += 1;

                if (iCount == 1)
                    gbRemoteDownload.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbRemoteDownload.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbRemoteDownload.Location = gbAID.Location;
                else if (iCount == 4)
                    gbRemoteDownload.Location = gbKey.Location;
                else if (iCount == 5)
                    gbRemoteDownload.Location = gbCardRange.Location;
                else if (iCount == 6)
                    gbRemoteDownload.Location = gbRemoteDownload.Location;
                #endregion

                #region "CardRange"
                gbCardRange.Enabled=gbCardRange.Visible = bCardRange;
                iCount = 0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;
                if (bKey)
                    iCount += 1;
                if (bCardRange)
                    iCount += 1;

                if (iCount == 1)
                    gbCardRange.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbCardRange.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbCardRange.Location = gbAID.Location;
                else if (iCount == 4)
                    gbCardRange.Location = gbKey.Location;
                else if (iCount == 5)
                    gbCardRange.Location = gbCardRange.Location;
                #endregion

                #region"Key"
                gbKey.Visible = gbKey.Enabled = bKey;
                iCount = 0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;
                if (bKey)
                    iCount += 1;

                if (iCount == 1)
                    gbKey.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbKey.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbKey.Location = gbAID.Location;
                else if (iCount == 4)
                    gbKey.Location = gbKey.Location;
                #endregion

                #region "AID"
                gbAID.Visible = gbAID.Enabled = bAID;
                iCount=0;
                if (bGPRSManagemenet)
                    iCount += 1;
                if (bTLE)
                    iCount += 1;
                if (bAID)
                    iCount += 1;

                if (iCount == 1)
                    gbAID.Location = gbGPRSMan.Location;
                else if (iCount == 2)
                    gbAID.Location = gbTLEManagement.Location;
                else if (iCount == 3)
                    gbAID.Location = gbAID.Location;
                #endregion

                #region "TLE"
                gbTLEManagement.Visible = gbTLEManagement.Visible = bTLE;
                if (!bGPRSManagemenet)
                    gbTLEManagement.Location = gbGPRSMan.Location;
                #endregion

                #region "GPRSManagement"
                    gbGPRSMan.Visible = gbGPRSMan.Enabled = bGPRSManagemenet;
                #endregion

                #region"AuditTrailWeb"
                    chkAuditTrailWeb.Enabled = chkAuditTrailWeb.Visible = bAuditTrailWeb;
                #endregion

                #region"SoftwareProgressWeb"
                    chkSoftwareTrailWeb.Enabled = chkSoftwareTrailWeb.Visible = bSoftwareProgressWeb;
                    if (!bAuditTrailWeb)
                        chkSoftwareTrailWeb.Location = chkAuditTrailWeb.Location;
                #endregion

            #endregion
        }

        private DataTable GetDataMenu()
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPMenuNewInsysBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// Initiate the checkboxes for the CheckBox_Struct.
        /// </summary>
        public void InitCheckBox()
        {
            InitArrayCheckBox();
            InitCheckBoxStruct();
        }

        /// <summary>
        /// Set the CheckBox array which groups the checkboxes according to their relaitons.
        /// </summary>
        public void InitArrayCheckBox()
        {
            arrAllowInit = new CheckBox[] { null };
            arrAutoInit = new CheckBox[] { null };
            arrCompressInit = new CheckBox[] { null };
            arrSNValidation = new CheckBox[] { null };
            arrAuditTrail = new CheckBox[] { chkAuditTrailExport, chkAuditTrailImport, chkAuditTrailDelete };
            arrInitTrail = new CheckBox[] {chkInitTrailExport, chkInitTrailImport, chkInitTrailDelete};
            arrPopulateTLV = new CheckBox[] { null };
            arrSoftwarePackage = new CheckBox[] { chkSoftwareAdd, chkSoftwareEdit, chkSoftwareDelete };

            arrData = new CheckBox[] { chkTemplateDef, chkUploadDef, chkUpload };

            arrUserMan = new CheckBox[] { chkUserManAdd, chkUserManEdit, chkUserManDelete };
            arrLocation = new CheckBox[] { chkLocationAdd, chkLocationImport};
            arrMonitor = new CheckBox[] { null };
            arrDownload = new CheckBox[] { null };

            arrCreateTerminal = new CheckBox[] { null };
            arrRegOldFile = new CheckBox[] { null };

            arrAID = new CheckBox[] {  chkAIDAdd, chkAIDEdit, chkAIDDelete };
            arrCAPK = new CheckBox[] { chkKeyAdd, chkKeyEdit, chkKeyDelete };
            arrCardRange = new CheckBox[] { chkCardRangeAdd, chkCardRangeEdit, chkCardRangeDelete };
            arrTLE = new CheckBox[] { chkTLEAdd, chkTLEEdit, chkTLEDelete };
            
            arrDE = new CheckBox[] { chkTerminalAdd, chkTerminalEdit, chkTerminalDelete };
            arrAA = new CheckBox[] { chkAcqAdd, chkAcqEdit, chkAcqDelete };
            arrAE = new CheckBox[] { chkIssuerAdd, chkIssuerEdit, chkIssuerDelete };
            arrAC = new CheckBox[] { chkCardAdd, chkCardDelete };
            arrLP = new CheckBox[] { chkLoyProdAdd, chkLoyProdEdit, chkLoyProdDelete };
            arrGP = new CheckBox[] { chkGPRSAdd, chkGPRSEdit, chkGPRSDel };
            arrCurr = new CheckBox[] { chkCurrencyAdd, chkCurrencyEdit, chkCurrencyDelete };
            arrSchedule = new CheckBox[] { null };
            arrPP = new CheckBox[] { chkPinPadAdd, chkPinPadedit, chkPinPadDelete };
            arrKP = new CheckBox[] { ChkProductCodeAdd, ChkProductCodeEdit, ChkProductCodeDelete };
            arrKB = new CheckBox[] { ChkBankCodeAdd, ChkBankCodeEdit, ChkBankCodeDelete };
            arrRD = new CheckBox[] { chkRemoteDownloadAdd, chkRemoteDownloadEdit, chkRemoteDownloadDelete };
            arrEM = new CheckBox[] { chkEMVManagementAdd,chkEMVManagementEdit,chkEMVManagementDelete };
           
        }

        /// <summary>
        /// Initiate CheckboxStruct for each UserRighs.
        /// </summary>
        public void InitCheckBoxStruct()
        {
            UserRight = new CheckBox_Struct[]
            {
                new CheckBox_Struct ("Allow Init", PrivilegeCode.IA.ToString(), chkAllowInit,arrAllowInit, 0),
                new CheckBox_Struct ("Auto Init", PrivilegeCode.OI.ToString(), chkAutoInit, arrAutoInit, 0),
                new CheckBox_Struct ("Compress Init",PrivilegeCode.IC.ToString(),chkCompressInit,arrCompressInit,0),
                new CheckBox_Struct ("SN Valid", PrivilegeCode.SN.ToString(), chkSNValidation,arrSNValidation, 0),
                new CheckBox_Struct ("Audit Trail", PrivilegeCode.AT.ToString(), chkAuditTrailView, arrAuditTrail, 3),
                new CheckBox_Struct ("Initialize Trail", PrivilegeCode.IT.ToString(), chkInitTrailView, arrInitTrail, 3),
                new CheckBox_Struct ("Data Upload", PrivilegeCode.DU.ToString(),chkDataView, arrData, 3),
                new CheckBox_Struct ("User Management", PrivilegeCode.UM.ToString(), chkUserManView, arrUserMan, 3),
                new CheckBox_Struct ("CreateTerminal", PrivilegeCode.NP.ToString(), chkCreateTerminal, arrCreateTerminal, 0),
                new CheckBox_Struct ("Register Old File", PrivilegeCode.RP.ToString(), chkRegisterOldFile, arrRegOldFile, 0),
                new CheckBox_Struct ("Software Package", PrivilegeCode.SW.ToString(), chkSoftwareView, arrSoftwarePackage, 3),
                new CheckBox_Struct ("AID Management", PrivilegeCode.AI.ToString(), chkAIDView, arrAID, 3),
                new CheckBox_Struct ("Key Management", PrivilegeCode.PK.ToString(), chkKeyView, arrCAPK, 3),
                new CheckBox_Struct ("Card Range", PrivilegeCode.RC.ToString(), chkCardRangeView, arrCardRange, 3),
                new CheckBox_Struct ("TLE", PrivilegeCode.TL.ToString(), chkTLEView, arrTLE, 3),
                new CheckBox_Struct ("Terminal", PrivilegeCode.DE.ToString(), chkTerminalView, arrDE, 3),
                new CheckBox_Struct ("Acquirer", PrivilegeCode.AA.ToString(), chkAcqView, arrAA, 3),
                new CheckBox_Struct ("Issuer", PrivilegeCode.AE.ToString(), chkIssuerView, arrAE, 3),
                new CheckBox_Struct ("Card", PrivilegeCode.AC.ToString(), chkCardView, arrAC,2),
                new CheckBox_Struct ("GPRS Management", PrivilegeCode.GP.ToString(), chkGPRSView,arrGP,3),
                new CheckBox_Struct ("Currency Management", PrivilegeCode.CR.ToString(), chkCurrencyView, arrCurr, 3),
                new CheckBox_Struct ("Set Schedule", PrivilegeCode.SS.ToString(), chkSchedule, arrSchedule, 0),
                new CheckBox_Struct ("Query Report", PrivilegeCode.QR.ToString(), chkQueryReport, arrQueryReport, 0),
                new CheckBox_Struct ("Init Picture", PrivilegeCode.IP.ToString(), chkInitPicture, arrInitPicture, 0),
                new CheckBox_Struct ("PinPad Key Management", PrivilegeCode.PP.ToString(), chkPinPadView,arrPP,3),
                new CheckBox_Struct ("Remote Download", PrivilegeCode.RD.ToString(), chkRemoteDownloadView, arrRD, 3),
                new CheckBox_Struct ("EMV Management", PrivilegeCode.EM.ToString(), chkEMVManagementView, arrEM, 3),
                new CheckBox_Struct ("Audit Trail Web", PrivilegeCode.WA.ToString(), chkAuditTrailWeb, arrAuditTrailWeb, 0),
                new CheckBox_Struct ("Software Trail Web", PrivilegeCode.WS.ToString(), chkSoftwareTrailWeb, arrSoftwareTrailWeb, 0)
                
                //new CheckBox_Struct ("Populate TLV", PrivilegeCode.PT.ToString(), chkPopulateTLV, arrPopulateTLV, 0),
                //new CheckBox_Struct ("Location", PrivilegeCode.LO.ToString(), chkLocationView, arrLocation,2),
                //new CheckBox_Struct ("EDC_IP Tracking", PrivilegeCode.PE.ToString(), chkEdcIpTracking, arrMonitor, 0),
                //new CheckBox_Struct ("Download", PrivilegeCode.DL.ToString(), chkDownload, arrDownload, 0),
                //new CheckBox_Struct ("Loyalty Product", PrivilegeCode.LP.ToString(), chkLoyProdView, arrLP,3),
                //new CheckBox_Struct("Product Code", PrivilegeCode.KP.ToString(), ChkProductCodeView, arrKP, 3),
                //new CheckBox_Struct ("Bank Code ", PrivilegeCode.KB.ToString(), ChkBankCodeView, arrKB, 3),
            };
        }

        /// <summary>
        /// Set the form's displya, either for Add new data or Edit selected data
        /// </summary>
        public void SetDisplayForm()
        {
            if (!isEdit) DisplayAdd();
            else DisplayEdit();
        }

        /// <summary>
        /// Set the form's display from adding new data
        /// </summary>
        public void DisplayAdd()
        {
            this.Text = "User Management - Add";
            btnChangePass.Visible = isEdit;
            btnSave.Visible= !isEdit;
            btnReset.Visible = isEdit;
            btnDelete.Visible = isEdit;
            if (! btnReset.Visible) btnSave.Location = btnDelete.Location;
        }

        /// <summary>
        /// Set the form's display for viewing and editing selected data
        /// </summary>
        public void DisplayEdit()
        {
            InitData();
            SetReadOnly();

            if (!isAllowEdit) //If not allowed to edit
            {
                this.Text = "User Management - View";
                btnSave.Visible = isAllowEdit;
                btnChangePass.Visible = isAllowEdit;
                btnReset.Visible = isAllowEdit;
                btnDelete.Visible = UserPrivilege.IsAllowed(PrivilegeCode.UM, Privilege.Delete);
            }
            else // If allowed edit
            {
                this.Text = "User Management - Edit";
                btnSave.Visible = isEdit;
                btnReset.Visible = isEdit;
                btnChangePass.Visible = isEdit;
                btnDelete.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.UM, Privilege.Delete) &&
                    sUID.ToUpper() != UserData.sUserID;
            }
        }

        /// <summary>
        /// Set a value which indicating wheter objects on form can be edited or not.
        /// </summary>
        public void SetReadOnly() 
        {
            if (isAllowEdit)
            {
                txtUserID.Enabled = !isAllowEdit;
                txtPass.Enabled = !isAllowEdit;
            }
            else
            {
                gbUserInfo.Enabled = isAllowEdit;
                gbApplication.Enabled = isAllowEdit;
                gbUtilities.Enabled = isAllowEdit;
                gbProfile1.Enabled = isAllowEdit;
                gbProfile2.Enabled = isAllowEdit;
               
            }

        }

        /// <summary>
        /// Get UserID from other form
        /// </summary>
        public string sGetUID
        {
            set
            {
                sUID = value;
            }
        }

        /// <summary>
        /// Initialize data for the form
        /// </summary>
        public void InitData()
        {
            LoadDataUser();
            FillDataUser();
            InitUserRights();
        }

        /// <summary>
        /// Initialize checkboxes of user rights based on data from database
        /// </summary>
        public void InitUserRights()
        {
            string sUserRights = oDataTable.Rows[0]["UserRights"].ToString();

            for (int iCount = 0; iCount < UserRight.Length; iCount++)
            {
                int iCurrPos = -1;

                if (sUserRights.Contains(UserRight[iCount].sFieldKey))
                {
                    UserRight[iCount].chkMaster.Checked = true;
                    iCurrPos = sUserRights.IndexOf(UserRight[iCount].sFieldKey) + 2 + 2;
                    for (int iPos = 0; iPos < UserRight[iCount].iSize; iPos++)
                        UserRight[iCount].chkDetail[iPos].Checked = isStringToBool(sUserRights.Substring(iCurrPos + iPos, 1)); 
                }
            }
        }

        /// <summary>
        /// Show user's data on form
        /// </summary>
        public void FillDataUser()
        {
            if (oDataTable != null && oDataTable.Rows.Count == 1)
            {
                txtUserID.Enabled = !isEdit;
                txtUserID.Text = oDataTable.Rows[0]["UserID"].ToString();
                txtUserName.Text = oDataTable.Rows[0]["UserName"].ToString();
                sPasswordEncrypt = oDataTable.Rows[0]["Password"].ToString();
                txtPass.Text = sPasswordEncrypt.Substring(0, 16);
                if (oDataTable.Columns.Contains("Locked"))
                    chkLocked.Checked = oDataTable.Rows[0]["Locked"].ToString() == "True" ? true : false;
                if (oDataTable.Columns.Contains("Revoke"))
                    chkRevoke.Checked = oDataTable.Rows[0]["Revoke"].ToString() == "True" ? true : false;
            }
        }

        /// <summary>
        /// Get user's data from the database
        /// </summary>
        public void LoadDataUser()
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditions();
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                SqlDataReader oRead = oSqlCmd.ExecuteReader();
                oDataTable.Clear();
                oDataTable.Load(oRead);

                oSqlCmd.Dispose();
                oRead.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Determines which condition will be used, based on isEdit status.
        /// </summary>
        /// <returns>string : Condition string which will be used.</returns>
        public string sConditions()
        {
            if (!isEdit) return sAddConditions();
            else return sLoadConditions();
        }

        /// <summary>
        /// Condition which will be used when form is loading.
        /// </summary>
        /// <returns>string : Condition string</returns>
        public string sLoadConditions()
        {
            return "WHERE UserID = '" + sUID + "'";
        }

        /// <summary>
        /// Condition which will be used when inserting new data
        /// </summary>
        /// <returns>string : Condition string</returns>
        public string sAddConditions()
        {
            return "WHERE UserID = '" + sGetUserID() + "'";
        }

        #endregion

        #region "Check Changed"

        /// <summary>
        /// Enable or disable checkboxes based on specifc user right.
        /// </summary>
        /// <param name="sKeyName">string : UserRights key</param>
        /// <param name="isEnable">boolean : status enable for the objects</param>
        public void SetEnable(string sKeyName, bool isEnable)
        {
            int iIndex = 0;
            for (int iCount = 0; iCount < UserRight.Length; iCount++)
                if (UserRight[iCount].sFieldKey == sKeyName)
                {
                    iIndex = iCount;
                    break;
                }

            for (int iCount = 0; iCount < UserRight[iIndex].iSize; iCount++)
                UserRight[iIndex].chkDetail[iCount].Enabled = isEnable;
        }

        /// <summary>
        /// Set enable for Data Upload.
        /// </summary>
        private void chkDataView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("DU", chkDataView.Checked);
        }

        /// <summary>
        /// Set enable for User Management.
        /// </summary>
        private void chkUserManView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("UM", chkUserManView.Checked);
        }

        private void chkLocationView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("LO", chkLocationView.Checked);
        }

        private void chkSoftwareView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("SW", chkSoftwareView.Checked);
        }

        /// <summary>
        /// Set enable for AID Management.
        /// </summary>
        private void chkAIDView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("AI", chkAIDView.Checked);
        }

        /// <summary>
        /// Set enable for Key Management.
        /// </summary>
        private void chkKeyView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("PK", chkKeyView.Checked);
        }

        /// <summary>
        /// Set enable for CardRange Management.
        /// </summary>
        private void chkCardRangeView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("RC", chkCardRangeView.Checked);
        }

        /// <summary>
        /// Set enable for Terminal.
        /// </summary>
        private void chkTerminalView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("DE", chkTerminalView.Checked);
        }

        /// <summary>
        /// Set enable for Acquire.
        /// </summary>
        private void chkAcgView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("AA", chkAcqView.Checked);
        }

        /// <summary>
        /// Set enable for Issuer.
        /// </summary>
        private void chkIssuerView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("AE", chkIssuerView.Checked);
        }

        /// <summary>
        /// Set enable for Card.
        /// </summary>
        private void chkCardView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("AC", chkCardView.Checked);
        }

        /// <summary>
        /// Set enable for Audit Trail.
        /// </summary>
        private void chkAuditTrailView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("AT", chkAuditTrailView.Checked);
        }

        /// <summary>
        /// Set enable for TLE.
        /// </summary>
        private void chkTLEView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("TL", chkTLEView.Checked);
        }

        /// <summary>
        /// Set enable for Initialize Trail.
        /// </summary>
        private void chkInitTrailView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("IT", chkInitTrailView.Checked);
        }

        private void chkLoyProdView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("LP", chkLoyProdView.Checked);
        }

        private void chkGPRSView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("GP", chkGPRSView.Checked);
        }

        private void chkCurrencyView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("CR", chkCurrencyView.Checked);
        }

        private void chkPinPadView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("PP", chkPinPadView.Checked);
        }

        private void ChkBankCodeView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("KP", ChkBankCodeView.Checked);
        }

        private void ChkProductView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("KB", ChkProductCodeView.Checked);
        }

        private void chkRD_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("RD", chkRemoteDownloadView.Checked);
        }

        private void chkEMVManagementView_CheckedChanged(object sender, EventArgs e)
        {
            SetEnable("EM", chkEMVManagementView.Checked);
        }

        #endregion

        #region "Button"

        /// <summary>
        /// Get the text from UserID textbox.
        /// </summary>
        /// <returns>string : UserID</returns>
        public string sGetUserID()
        {
            return txtUserID.Text;
        }

        /// <summary>
        /// Get the text from UserName textbox.
        /// </summary>
        /// <returns>string : UserName</returns>
        public string sGetUserName()
        {
            return txtUserName.Text;
        }

        /// <summary>
        /// Get the text from Password textbox.
        /// </summary>
        /// <returns>string : Password</returns>
        public string sGetPassword()
        {
            return txtPass.Text;
        }

        /// <summary>
        /// Event function, runs when btnSave is clicked.
        /// Validating user's input then save the data to database
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (isValid() && isUserIdUnique())
                {
                    DialogResult drResult = MessageBox.Show(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle, MessageBoxButtons.YesNoCancel);

                    if (drResult == DialogResult.Yes)
                    {
                        if (!isEdit)
                        {
                            if (PasswordIsComplex())
                            {
                                SaveData();
                                if (sGetUserID().ToLower() == UserData.sUserID.ToLower()) UserData.sPassword = sPasswordEncrypt;
                                btnCancel.PerformClick();
                            }
                        }
                        else
                        {
                            UpdateData();
                            if (sGetUserID().ToLower() == UserData.sUserID.ToLower()) UserData.sPassword = sPasswordEncrypt;
                            btnCancel.PerformClick();
                        }
                        //if (sGetUserID().ToLower() == UserData.sUserID.ToLower()) UserData.sPassword = sPasswordEncrypt;
                        //btnCancel.PerformClick();
                    }
                    else if (drResult == DialogResult.No) btnCancel.PerformClick();
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private bool PasswordIsComplex()
        {
            int iValidConditions = 0;
            string sPassword = sGetPassword();

            if (sPassword.Length >= 8 && sPassword.Length <= 15)
                iValidConditions++;

            if (iValidConditions == 0)
            {
                MessageBox.Show("Passwod Lenght Min 8 and Max 15.");
                return false;
            }

            foreach (char c in sPassword)
            {
                if (c >= 'a' && c <= 'z')
                {
                    iValidConditions++;
                    break;
                }
            }

            foreach (char c in sPassword)
            {
                if (c >= 'A' && c <= 'Z')
                {
                    iValidConditions++;
                    break;
                }
            }

            if (iValidConditions == 1 || iValidConditions == 2)
            {
                MessageBox.Show("Passwod must contain one Capital letter and one small letter.");
                return false;
            }

            foreach (char c in sPassword)
            {
                if (c >= '0' && c <= '9')
                {
                    iValidConditions++;
                    break;
                }
            }
            
            if (iValidConditions == 3) 
            {
                MessageBox.Show("Password must contain one number.");
                return false;
            }
            
            if (iValidConditions == 4)
            {
                char[] special = { '@', '#', '$', '%', '^', '&', '+', '=' }; // or whatever

                if (sPassword.IndexOfAny(special) == -1)
                {
                    MessageBox.Show("Password Must Contain Special Character (@,#,$,%,^,&,+,=)");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determines wheter all user's input valid or not.
        /// </summary>
        /// <returns>boolean : true if valid, else false</returns>
        public bool isValid()
        {
            bool isValid = false;
            if (sGetUserID().Length < 1) CommonClass.doShowErrorMessage(CommonMessage.sErrLoginUserID, txtUserID, ObjectType.TEXTBOX, ErrorType.ERROR);
            else if (!CommonClass.isFormatValid(sGetUserID(), "A")) CommonClass.doShowErrorMessage(CommonMessage.sErrLoginUserIDFormat, txtUserID, ObjectType.TEXTBOX, ErrorType.ERROR);
            else if (sGetUserName().Trim().Length < 1) CommonClass.doShowErrorMessage(CommonMessage.sErrUserManUserName, txtUserName, ObjectType.TEXTBOX, ErrorType.ERROR);
            else if (!CommonClass.isFormatValid(sGetUserName(), "A1")) CommonClass.doShowErrorMessage(CommonMessage.sErrUserManUserNameFormat, txtUserName, ObjectType.TEXTBOX, ErrorType.ERROR);
            else if (sGetPassword().Trim().Length < 1) CommonClass.doShowErrorMessage(CommonMessage.sErrLoginPassword, txtPass, ObjectType.TEXTBOX, ErrorType.ERROR);
            //else if (!CommonClass.isFormatValid(sGetPassword(), "A")) CommonClass.doShowErrorMessage(CommonMessage.sErrUserManPassFormat, txtPass, ObjectType.TEXTBOX, ErrorType.ERROR);
            else if (!isUserIdUnique()) CommonClass.doShowErrorMessage(CommonMessage.sErrUserManUidDouble, null, ObjectType.NULL, ErrorType.ERROR);
            else isValid = true;
            return isValid;
        }

        /// <summary>
        /// Determines if UserID entered already exists in database
        /// </summary>
        /// <returns>boolean : true if UserID unique, else false</returns>
        public bool isUserIdUnique()
        {
            LoadDataUser();
            return !(oDataTable.Rows.Count > 0 && !isEdit);
        }

        /// <summary>
        /// Create UserRights in string format based on userrights' checkboxes
        /// </summary>
        /// <returns>string : UserRights for the user</returns>
        public string sCreateUserRights()
        {
            string sRights = "";
            for (int iCount = 0; iCount < UserRight.Length; iCount++)
            {
                if (UserRight[iCount].chkMaster.Checked)
                {
                    sRights += UserRight[iCount].sFieldKey + "00".Substring(1, 2 - UserRight[iCount].iSize.ToString().Length) + UserRight[iCount].iSize.ToString();
                    for (int iPos = 0; iPos < UserRight[iCount].iSize; iPos++)
                    {
                        sRights += sBoolToString(UserRight[iCount].chkDetail[iPos].Checked);
                    }
                }
            }

            if (isSuperUser && 
                (sGetUserID().ToUpper() == "GEMALTO") || sGetUserID().ToUpper() == "INGENICO") //Add rights as Super User
                sRights += "SU011";
            
            return sRights;
        }

        /// <summary>
        /// Converting boolean value to string.
        /// True = 1, False = 0
        /// </summary>
        /// <param name="isRights">boolean : value needed to be converted</param>
        /// <returns>string : result from the conversion</returns>
        public string sBoolToString(bool isRights)
        {
            if (isRights) return "1";
            else return "0";
        }

        /// <summary>
        /// Converting string to boolean value.
        /// "1" = True, "0" = False.
        /// </summary>
        /// <param name="sRights">string : value to be converted.</param>
        /// <returns>boolean : Conversion's result</returns>
        public bool isStringToBool(string sRights)
        {
            return (sRights == "1");
        }

        /// <summary>
        /// Insert new data to database.
        /// </summary>
        public void SaveData()
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserInsert, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sUID", SqlDbType.VarChar).Value = sGetUserID();
                oSqlCmd.Parameters.Add("@sUserName", SqlDbType.VarChar).Value = sGetUserName();
                oSqlCmd.Parameters.Add("@sPassword", SqlDbType.VarChar).Value = CommonClass.sGetEncrypt(sGetPassword());
                oSqlCmd.Parameters.Add("@sUserRights", SqlDbType.VarChar).Value = sCreateUserRights();
                if (oDataTable.Columns.Contains("Locked"))
                    oSqlCmd.Parameters.Add("@sLocked", SqlDbType.VarChar).Value = chkLocked.Checked;
                if (oDataTable.Columns.Contains("Revoke"))
                    oSqlCmd.Parameters.Add("@sRevoke", SqlDbType.VarChar).Value = chkRevoke.Checked;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                string sLogDetail = "";

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    while (oRead.Read())
                        sLogDetail += string.Format("{0} : {1} \n", oRead["TagName"].ToString(), oRead["UserRight"].ToString());

                    //oSqlCmd.ExecuteNonQuery();
                    oRead.Close();
                }
                oSqlCmd.Dispose();

                if (txtUserID.Text.Trim().ToUpper() != "ADMIN")
                {
                    CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", "Add User : " + txtUserID.Text.Trim().ToUpper(), sLogDetail);
                    isLogged = true;
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Update the data which user modified to database.
        /// </summary>
        public void UpdateData()
        {
            try
            {
                SqlCommand oSqlCmd;
                if (isSuperUser)
                    oSqlCmd = new SqlCommand(CommonSP.sSPSuperUserUpdate, oSqlConn);
                else
                    oSqlCmd = new SqlCommand(CommonSP.sSPUserUpdate, oSqlConn);

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sConditions", SqlDbType.VarChar).Value = sUpdateConditions();
                oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = txtUserID.Text.Trim();

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                string sLogDetail = "";
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    //oSqlCmd.ExecuteNonQuery();

                    while (oRead.Read())
                    {
                        sLogDetail += string.Format("{0} : {1} --> {2} \n", 
                            (!string.IsNullOrEmpty(oRead["OldTagName"].ToString())) ? oRead["OldTagName"].ToString(): oRead["NewTagName"].ToString(),
                            (string.IsNullOrEmpty(oRead["OldUserRight"].ToString())) ? " - " : oRead["OldUserRight"].ToString(), 
                            (string.IsNullOrEmpty(oRead["NewUserRight"].ToString())) ? " - " : oRead["NewUserRight"].ToString());
                    }
                    oRead.Close();
                }
                
                oSqlCmd.Dispose();

                if (txtUserID.Text.Trim().ToUpper() != "ADMIN" || isSuperUser)
                {
                    CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", "Edit User : " + txtUserID.Text.Trim().ToUpper(), sLogDetail);
                    isLogged = true;
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Create the condition string when updating data in database.
        /// </summary>
        /// <returns>string : Condition String</returns>
        public string sUpdateConditions()
        {
            return string.Format("{0}{1}{2}{3}{4}{5}{6}", "SET USERNAME = '" + sGetUserName(), "', PASSWORD = '" + sPasswordEncrypt, "', USERRIGHTS = '" + sCreateUserRights(), oDataTable.Columns.Contains("Locked") ? "', Locked = '" + chkLocked.Checked : "", oDataTable.Columns.Contains("Revoke") ? "', [Revoke] = '" + chkRevoke.Checked : "", oDataTable.Columns.Contains("Revoke") ? "', [LastModifyPassword] = '" + DateTime.Now : "", "' WHERE USERID = '" + sGetUserID() + "'");
            //return "SET USERNAME = '" + sGetUserName()
            //                + "', PASSWORD = '" + sPasswordEncrypt
            //                + "', USERRIGHTS = '" + sCreateUserRights()
            //                + "', Locked = '" + chkLocked.Checked
            //                + "' WHERE USERID = '" + sGetUserID() + "'";
        }

        /// <summary>
        /// Event function, runs when btnChangePass is clicked.
        /// Call ChangePassword form to change user's password.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChangePass_Click(object sender, EventArgs e)
        {
            bool isFromMenu = false;

            FrmChangePassword fChangePass = new FrmChangePassword(oSqlConn, oSqlConnAuditTrail, sUID, sGetUserName(), sPasswordEncrypt, isFromMenu, false);
            try
            {
                fChangePass.ShowDialog();
                sPasswordEncrypt = CommonClass.sGetEncrypt(fChangePass.sGetNewPassEncrypted());
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            finally
            {
                fChangePass.Dispose();
            }
        }

        /// <summary>
        /// Event function, runs when btnReset is clicked.
        /// Set all UserRights' checkbox to unchecked
        /// </summary>
        private void btnReset_Click(object sender, EventArgs e)
        {
            ProcessControls(this);
        }

        /// <summary>
        /// Search all Checkbox control in control parent and set the checkboxes to unchecked.
        /// </summary>
        /// <param name="ctrControl">Control : Control parent to search checkbox control</param>
        private void ProcessControls(Control ctrControl)
        {
            try
            {
                foreach (Control oCtr in ctrControl.Controls)
                {
                    if (oCtr.GetType() == typeof(CheckBox))
                    {
                        CheckBox oChkControl = (CheckBox)oCtr;
                        oChkControl.Checked = false;
                    }

                    if (oCtr.HasChildren)
                    {
                        ProcessControls(oCtr);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Event function, runs when btnDelete is clicked.
        /// Delete the selected User's data
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (CommonClass.isYesMessage(CommonMessage.sUserMandDeleteUserText + oDataTable.Rows[0]["UserID"].ToString() + "' ?", CommonMessage.sUserManDeleteTitle))
            {
                try
                {
                    DeleteUser();
                    this.Close();
                }
                catch (Exception ex)
                {
                    CommonClass.doWriteErrorFile(ex.Message);
                }
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        public void DeleteUser()
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserDelete, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sConditions", SqlDbType.VarChar).Value = sDeleteConditions();

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();

                if (txtUserID.Text.Trim().ToUpper() != "ADMIN")
                {
                    CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", "Delete User : " + txtUserID.Text.Trim().ToUpper(), "");
                    isLogged = true;
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Create conditions when deleting user data.
        /// </summary>
        /// <returns>string : Condition String</returns>
        public string sDeleteConditions()
        {
            return "WHERE USERID = '" + sGetUserID() + "'";
        }

        /// <summary>
        /// Event Function, runs when btnCancel is clicked.
        /// Close the form.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        /// <summary>
        /// Event function, runs when form is closing.
        /// Write log to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmUserMngmt_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isEdit && !isLogged) CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", "View User" + (txtUserID.Text.Trim().Length > 0 ? " : " + txtUserID.Text.Trim().ToUpper() : ""), "");
        }

        private void chkInitTrailDelete_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkUserManAdd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkCardAdd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkCardDelete_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkIssuerAdd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkCurrencyAdd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkLoyProdEdit_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ChkBankCode_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ChkProductCode_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void gbCard_Enter(object sender, EventArgs e)
        {

        }

        private void chkRegisterOldFile_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void gbProfile1_Enter(object sender, EventArgs e)
        {

        }

        private void gbUserInfo_Enter(object sender, EventArgs e)
        {

        }

        private void chkAuditTrailExport_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkAuditTrailDelete_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkAllowInit_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkSoftwareAdd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void gbRights_Enter(object sender, EventArgs e)
        {

        }

        private void ChkProductCodeDelete_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ChkBankCodeEdit_CheckedChanged(object sender, EventArgs e)
        {

        }

        

    }
}