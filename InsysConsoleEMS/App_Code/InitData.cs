using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Ini;
using InSysClass;

namespace InsysConsoleEMS
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword; 
        protected string sDirectory;

        public InitData(string _sDirectory)
        {
            sDirectory = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectory + "\\Application.config";
            string sEncryptFileName = sDirectory + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception)
                {
                    //Logs.doWriteErrorFile(sDirectory, ex.Message);
                }
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword);
        }
        
    }
}