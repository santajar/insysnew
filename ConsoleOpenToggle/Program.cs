﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using InSysClass;


namespace ConsoleOpenToggle
{
    class Program
    {
        static SqlConnection oConn = new SqlConnection();
        static string sConnString;
        static string sAppDirectory = Directory.GetCurrentDirectory();
        

        static void Main(string[] args)
        {
            oConn = InitConnection();
            int iMaxRetry = int.Parse(ConfigurationManager.AppSettings["MaxRetry"].ToString());
            if (oConn != null && oConn.State == ConnectionState.Open)
            {
                Console.WriteLine("Start Process");
                string sFilename = "Daftar_TID.txt";
                DataTable dtTemp = new DataTable();

                //string sTerminalIDList = null;
                string[] arrLines = File.ReadAllLines(sFilename);

                string sQuery;
                SqlCommand oCmd;
                int iSucces = 0, iNotSucces = 0;
                Logs.Write("Start Process");
                foreach (string sLine in arrLines)
                {
                    sQuery = string.Format("UPDATE tbProfileTerminalList SET AllowDownload = 1, MaxInitRetry={1} WHERE TerminalID IN ('{0}')", sLine, iMaxRetry);
                    oCmd = new SqlCommand(sQuery, oConn);

                    try
                    {
                        oCmd.ExecuteNonQuery();
                        iSucces = iSucces + 1;
                        Logs.Write(string.Format("Terminal ID : {0} Done.", sLine));
                    }
                    catch (Exception ex)
                    {
                        iNotSucces = iNotSucces + 1;
                        Logs.Write(string.Format("Terminal ID : {0} Error.", sLine));
                    }
                }
                Logs.Write(string.Format("Total Success : {0} | Total Error : {1} ", iSucces.ToString(), iNotSucces.ToString()));
                Console.WriteLine("Process Done");
            }
            else
            {
                Console.WriteLine("Database Connection Failed!...");
            }
        }

         /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        private static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }
    }
}
