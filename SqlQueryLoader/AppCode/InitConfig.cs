﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ini;

namespace SqlQueryLoader
{
    class InitConfig
    {
        protected string sDirectory = Environment.CurrentDirectory;
        protected string sFilename = @"\App.config";
        protected string sServer = null;
        protected string sDatabase = null;
        protected string sUser = null;
        protected string sPwd = null;

        IniFile oInifile;

        public InitConfig()
        {
            oInifile = new IniFile(sDirectory + sFilename);
            ConfigRead();
        }

        protected void ConfigRead()
        {
            sServer = oInifile.IniReadValue("DATABASE", "Server");
            sDatabase = oInifile.IniReadValue("DATABASE", "Database");
            sUser = oInifile.IniReadValue("DATABASE", "User");
            sPwd = oInifile.IniReadValue("DATABASE", "Pwd");
        }

        public void ConfigWrite()
        {
            oInifile.IniWriteValue("DATABASE", "Server", sServer);
            oInifile.IniWriteValue("DATABASE", "Database", sDatabase);
            oInifile.IniWriteValue("DATABASE", "User", sUser);
            oInifile.IniWriteValue("DATABASE", "Pwd", sPwd); 
        }

        public string ServerName 
        {
            get { return sServer; }
            set { sServer = value; }
        }

        public string DatabaseName
        {
            get { return sDatabase; }
            set { sDatabase = value; }
        }

        public string UserId
        {
            get { return sUser; }
            set { sUser = value; }
        }

        public string Password
        {
            get { return sPwd; }
            set { sPwd = value; }
        }
    }
}