﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;

namespace SqlQueryLoader
{
    class ClassCommon
    {
        public static string sConnectionString(
            string _sServer, string _sDatabase, string _sUsername, string _sPassword)
        {
            return string.Format("server={0};database={1};uid={2};pwd={3}",
                _sServer, _sDatabase, _sUsername, _sPassword);
        }

        public static SqlConnection InitConnection(string _sServer, string _sDatabase, string _sUsername, string _sPassword)
        {
            SqlConnection oSqlConn = new SqlConnection(sConnectionString(_sServer,_sDatabase,_sUsername,_sPassword));
            try
            {
                oSqlConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Connection : {0}", ex.Message);
            }
            return oSqlConn;
        }

        public static void SaveQueryFile(string _sFilename, string _sQuery)
        {
            if (!string.IsNullOrEmpty(_sQuery))
            {
                StreamWriter sw = new StreamWriter(_sFilename);
                sw.Write(_sQuery);
                sw.Close();
            }
        }

        public static void GetResultAndView(bool IsSaveResult, ref DataTable dtResult,
            SqlConnection oSqlConn, string sQuery)
        {
            DataTable dtTemp = new DataTable();
            using (SqlDataReader oRead = drGetResult(oSqlConn, sQuery))
                if (oRead.HasRows)
                    dtTemp.Load(oRead);

            if (IsSaveResult)
            {
                string sFilename = sResultFilename();
                if (!string.IsNullOrEmpty(sFilename))
                    ExcelLib.WriteExcel(drGetResult(oSqlConn, sQuery), sFilename);
            }
            if (dtTemp.Rows.Count > 0)
                dtResult = dtTemp;
        }

        public static SqlDataReader drGetResult(SqlConnection oSqlConn, string sQuery)
        {
            SqlDataReader oReadTemp;
            SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn);
            oCmd.CommandType = CommandType.Text;
            oCmd.CommandTimeout = 0;
            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }
            oReadTemp = oCmd.ExecuteReader();
            return oReadTemp;
        }

        public static string sResultFilename()
        {
            SaveFileDialog sfdResult = new SaveFileDialog();
            sfdResult.InitialDirectory = Environment.SpecialFolder.Desktop.ToString();
            sfdResult.Filter = "Excel Files(*.xlsx)|*.xlsx|Excel 97-2003 Format Files(*.xls)|*.xls";
            sfdResult.OverwritePrompt = true;
            sfdResult.AddExtension = true;
            if (sfdResult.ShowDialog() == DialogResult.OK)
                return sfdResult.FileName;

            return null;
        }
    }
}
