﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using Ini;
using InSysClass;

namespace SqlQueryLoader
{
    public partial class frmNewQueryLoader : Form
    {
        /// <summary>
        /// Query Loader v2.00
        /// </summary>
        public frmNewQueryLoader()
        {
            InitializeComponent();
        }
        
        #region "Properties"
        SqlConnection oSqlConn = new SqlConnection();
        DataTable dtResult = new DataTable();
        //SqlDataReader oRead = new SqlDataReader();
        string sQuery = null;
        string sFileQuery = null;
        string sDirQuery = null;

        string sServer = null;
        string sDatabase = null;
        string sUsername = null;
        string sPassword = null;

        bool isEnableDateFilter;
        string sDateStart;
        string sDateEnd;
        #endregion

        private void frmNewQueryLoader_Load(object sender, EventArgs e)
        {
            sDirQuery = string.Format(@"{0}\QUERY", Environment.CurrentDirectory);
            Init();
            btnConnect.PerformClick();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(oSqlConn.ConnectionString))
            {
                if (IsValidConnectionParams())
                {
                    oSqlConn = ClassCommon.InitConnection(sServer, sDatabase, sUsername, sPassword);
                    DisableInitConnection(true);
                }
            }
            else
            {
                oSqlConn.Close();
                oSqlConn = new SqlConnection();
                DisableInitConnection(false);
            }
        }

        private void btnSaveConn_Click(object sender, EventArgs e)
        {
            if (IsValidConnectionParams())
            {
                InitConfig oConfig = new InitConfig();
                oConfig.ServerName = sServer;
                oConfig.DatabaseName = sDatabase;
                oConfig.UserId = sUsername;
                oConfig.Password = sPassword;
                oConfig.ConfigWrite();
            }
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            dtResult.Clear();
            LoadQueryFiles();
            if (oSqlConn.State == ConnectionState.Open)
            {
                if (!string.IsNullOrEmpty(sQuery))
                {
                    sDateStart = string.Format("{0:yyyy-MM-dd}", dtpStartDate.Value);
                    sDateEnd = string.Format("{0:yyyy-MM-dd}", dtpEndDate.Value);
                    bool bAbort = false;
                    if (isEnableDateFilter )
                    {
                        if (IsDateValid())
                        {
                            sQuery = sQuery.Replace("where StatusDesc like '%complete'", "where StatusDesc like '%complete' AND InitTime BETWEEN CONVERT(date,'" + sDateStart + "')" + " AND CONVERT(date,'" + sDateEnd + "')");
                            //if (sQuery.IndexOf("[DATESTART]") > 0)
                            //    sQuery = sQuery.Replace("[DATESTART]", sDateStart);
                            //if (sQuery.IndexOf("[DATEEND]") > 0)
                            //    sQuery = sQuery.Replace("[DATEEND]", sDateEnd);
                        }
                        else
                        {
                            MessageBox.Show("End Date is must bigger than Start Date");
                            dtpEndDate.Focus();
                            bAbort = true;
                        }
                    }
                    if (!bAbort)
                    {
                        StatusGroupBox(true);
                        bwProcess.RunWorkerAsync(false);
                    }
                }
                else
                {
                    MessageBox.Show("Query tidak ada atau file query belum dipilih. Pilih file terlebih dahulu!");
                    cmbFileQuery.Select();
                }
            }
            else
            {
                MessageBox.Show("Koneksi database belum ada. Setting koneksi terlebih dahulu!");
                btnConnect.Focus();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dtResult.Rows.Count > 0)
            {
                dtResult.TableName = "Result";
                string sFilename = ClassCommon.sResultFilename();
                string sTemplateXLSX = Environment.CurrentDirectory + @"\template.xlsx";
                string sTemplateXLS = Environment.CurrentDirectory + @"\template.xls";
                if (!string.IsNullOrEmpty(sFilename))
                {
                    File.Delete(sFilename);
                    if (sFilename.Contains(".xlsx"))
                        File.Copy(sTemplateXLSX, sFilename);
                    else
                        File.Copy(sTemplateXLS, sFilename);
                    ExcelOleDb oXlsOle = new ExcelOleDb(sFilename);
                    oXlsOle.CreateWorkSheet(dtResult);
                    oXlsOle.InsertRow(dtResult);
                    oXlsOle.Dispose();
                }
            }
            else
                MessageBox.Show("No Data to save");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnEditQuery_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sFileQuery))
            {
                string sFilename = string.Format(@"{0}\{1}.sql", sDirQuery, sFileQuery);
                Process procNotepad = new Process();
                procNotepad.StartInfo = (new ProcessStartInfo("notepad", sFilename));
                procNotepad.Start();
                procNotepad.WaitForExit();
                procNotepad.Close();
                LoadQueryFiles();
            }
        }

        private void btnAddQuery_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdQuery = new OpenFileDialog();
            
            ofdQuery.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (ofdQuery.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(ofdQuery.FileName))
                {
                    string sFilename = string.Format(@"{0}\{1}", sDirQuery, ofdQuery.SafeFileName);
                    string sFilenameBak = sFilename.Replace(".sql", ".bak");
                    if (File.Exists(sFilename))
                    {
                        if (MessageBox.Show("Duplicate file detected, Replace?", "Save File", MessageBoxButtons.YesNo)
                            == DialogResult.Yes)
                        {
                            if (MessageBox.Show("Backup duplicate file?", "Save File", MessageBoxButtons.YesNo)
                                == DialogResult.Yes)
                                File.Copy(sFilename, sFilenameBak);
                            File.Delete(sFilename);
                            File.Copy(ofdQuery.FileName, sFilename);
                        }
                    }
                    else
                        File.Copy(ofdQuery.FileName, sFilename);
                }
            }
        }

        private void cmbFileQuery_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbFileQuery.Text))
            {
                sFileQuery = cmbFileQuery.Text;
                LoadQueryFiles();
            }
        }

        private void bwProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            ClassCommon.GetResultAndView((bool)e.Argument, ref dtResult, oSqlConn, sQuery);
        }

        private void bwProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StatusGroupBox(false);
        }

        private void chkFilterDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterDate.Checked)
                isEnableDateFilter = true;
            else
                isEnableDateFilter = false;
            dtpStartDate.Enabled = isEnableDateFilter;
            dtpEndDate.Enabled = isEnableDateFilter;
        }

        #region "Function"
        protected void Init()
        {
            InitConnection();
            InitDisplay();
            InitQueryList();
            InitVersion();
        }

        protected void InitConnection()
        {
            try
            {
                InitConfig oConfig = new InitConfig();
                sServer = oConfig.ServerName;
                sDatabase = oConfig.DatabaseName;
                sUsername = oConfig.UserId;
                sPassword = oConfig.Password;
            }
            catch (Exception) { }
        }

        protected void InitDisplay()
        {
            txtServer.Text = sServer;
            txtDatabase.Text = sDatabase;
            txtUsername.Text = sUsername;
            txtPassword.Text = sPassword;
        }

        protected void InitQueryList()
        {
            FileInfo[] arrfiQuery = (new DirectoryInfo(sDirQuery)).GetFiles("*.sql");
            foreach (FileInfo fiTemp in arrfiQuery)
                cmbFileQuery.Items.Add(fiTemp.Name.Replace(".sql",""));
        }

        protected void InitVersion()
        {
            string sVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            this.Text += " v." + sVersion;
        }

        protected void DisableInitConnection(bool IsDisable)
        {
            txtDatabase.Enabled = IsDisable ? false : true;
            txtServer.Enabled = IsDisable ? false : true;
            txtUsername.Enabled = IsDisable ? false : true;
            txtPassword.Enabled = IsDisable ? false : true;
            btnConnect.Text = IsDisable ? "Disconnect" : "Connect";
        }

        protected bool IsValidConnectionParams()
        {
            sServer = txtServer.Text;
            sDatabase = txtDatabase.Text;
            sUsername = txtUsername.Text;
            sPassword = txtPassword.Text;

            if (!string.IsNullOrEmpty(sServer))
            {
                if (!string.IsNullOrEmpty(sDatabase))
                {
                    if (!string.IsNullOrEmpty(sUsername))
                        return true;
                    else
                    {
                        MessageBox.Show("Isi kolom 'Username'!");
                        txtUsername.Select();
                    }
                }
                else
                {
                    MessageBox.Show("Isi kolom 'Database'!");
                    txtDatabase.Select();
                }
            }
            else
            {
                MessageBox.Show("Isi kolom 'Server'!");
                txtServer.Select();
            }
            return false;
        }

        protected void LoadQueryFiles()
        {
            string sFilename = string.Format(@"{0}\{1}.sql", sDirQuery, sFileQuery);
            if (!string.IsNullOrEmpty(sFilename))
            {
                if (File.Exists(sFilename))
                {
                    StreamReader sr = new StreamReader(sFilename);
                    sQuery = sr.ReadToEnd();
                    sr.Close();
                }
                else
                {
                    MessageBox.Show("File hilang, atau telah dihapus. Pilih kembali file.");
                    cmbFileQuery.Items.RemoveAt(cmbFileQuery.Items.IndexOf(sFileQuery));
                    cmbFileQuery.SelectedIndex = -1;
                }
            }
        }

        protected void StatusGroupBox(bool IsEnable)
        {
            gbDatabase.Enabled = !IsEnable;
            gbQuery.Enabled = !IsEnable;
            pbProcess.Style = IsEnable ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;
            if (IsEnable)
                dgvResult.DataSource = null;

            if (dtResult.Rows.Count > 0)
                dgvResult.DataSource = dtResult;
        }

        protected bool IsDateValid()
        {
            
            return dtpEndDate.Value.Subtract(dtpStartDate.Value).Days >= 0 ? true : false;
            //return dtpEndDate.Value.CompareTo(dtpStartDate.Value) >= 0 ? true : false;
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            string sfilename = string.Format(@"{0}\{1}_Report.csv", Environment.CurrentDirectory, DateTime.Now.ToString("yyMMddHHmmss"));
            Csv.Write(dtResult, sfilename);
            MessageBox.Show("file Exported succssessfully.");
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

       


    }
}
