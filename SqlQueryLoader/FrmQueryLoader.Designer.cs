namespace SqlQueryLoader
{
    partial class frmQueryLoader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbSetQuery = new System.Windows.Forms.GroupBox();
            this.tabctrlDbQuery = new System.Windows.Forms.TabControl();
            this.tabpageDatabase = new System.Windows.Forms.TabPage();
            this.btnConnect = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabpageQuery = new System.Windows.Forms.TabPage();
            this.btnRunSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnSaveQuery = new System.Windows.Forms.Button();
            this.rtbQuery = new System.Windows.Forms.RichTextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtFileQuery = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.ctmnustripResult = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.saveResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbProcess = new System.Windows.Forms.ProgressBar();
            this.bwProcess = new System.ComponentModel.BackgroundWorker();
            this.gbSetQuery.SuspendLayout();
            this.tabctrlDbQuery.SuspendLayout();
            this.tabpageDatabase.SuspendLayout();
            this.tabpageQuery.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.ctmnustripResult.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSetQuery
            // 
            this.gbSetQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSetQuery.Controls.Add(this.tabctrlDbQuery);
            this.gbSetQuery.Location = new System.Drawing.Point(12, 12);
            this.gbSetQuery.Name = "gbSetQuery";
            this.gbSetQuery.Size = new System.Drawing.Size(723, 181);
            this.gbSetQuery.TabIndex = 0;
            this.gbSetQuery.TabStop = false;
            // 
            // tabctrlDbQuery
            // 
            this.tabctrlDbQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabctrlDbQuery.Controls.Add(this.tabpageDatabase);
            this.tabctrlDbQuery.Controls.Add(this.tabpageQuery);
            this.tabctrlDbQuery.Location = new System.Drawing.Point(6, 19);
            this.tabctrlDbQuery.Name = "tabctrlDbQuery";
            this.tabctrlDbQuery.SelectedIndex = 0;
            this.tabctrlDbQuery.Size = new System.Drawing.Size(711, 156);
            this.tabctrlDbQuery.TabIndex = 0;
            // 
            // tabpageDatabase
            // 
            this.tabpageDatabase.Controls.Add(this.btnConnect);
            this.tabpageDatabase.Controls.Add(this.txtPassword);
            this.tabpageDatabase.Controls.Add(this.label4);
            this.tabpageDatabase.Controls.Add(this.txtUsername);
            this.tabpageDatabase.Controls.Add(this.label3);
            this.tabpageDatabase.Controls.Add(this.txtDatabase);
            this.tabpageDatabase.Controls.Add(this.label2);
            this.tabpageDatabase.Controls.Add(this.txtServer);
            this.tabpageDatabase.Controls.Add(this.label1);
            this.tabpageDatabase.Location = new System.Drawing.Point(4, 22);
            this.tabpageDatabase.Name = "tabpageDatabase";
            this.tabpageDatabase.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageDatabase.Size = new System.Drawing.Size(703, 130);
            this.tabpageDatabase.TabIndex = 1;
            this.tabpageDatabase.Text = "Database";
            this.tabpageDatabase.UseVisualStyleBackColor = true;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(20, 96);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 4;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(405, 45);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '#';
            this.txtPassword.Size = new System.Drawing.Size(188, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(335, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Password";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(405, 10);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(188, 20);
            this.txtUsername.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(335, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "User";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Location = new System.Drawing.Point(85, 45);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(173, 20);
            this.txtDatabase.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Database";
            // 
            // txtServer
            // 
            this.txtServer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtServer.Location = new System.Drawing.Point(85, 10);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(173, 20);
            this.txtServer.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server";
            // 
            // tabpageQuery
            // 
            this.tabpageQuery.Controls.Add(this.btnRunSave);
            this.tabpageQuery.Controls.Add(this.btnClose);
            this.tabpageQuery.Controls.Add(this.btnRun);
            this.tabpageQuery.Controls.Add(this.btnSaveQuery);
            this.tabpageQuery.Controls.Add(this.rtbQuery);
            this.tabpageQuery.Controls.Add(this.btnBrowse);
            this.tabpageQuery.Controls.Add(this.txtFileQuery);
            this.tabpageQuery.Controls.Add(this.label5);
            this.tabpageQuery.Location = new System.Drawing.Point(4, 22);
            this.tabpageQuery.Name = "tabpageQuery";
            this.tabpageQuery.Padding = new System.Windows.Forms.Padding(3);
            this.tabpageQuery.Size = new System.Drawing.Size(703, 130);
            this.tabpageQuery.TabIndex = 0;
            this.tabpageQuery.Text = "Query";
            this.tabpageQuery.UseVisualStyleBackColor = true;
            // 
            // btnRunSave
            // 
            this.btnRunSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunSave.Location = new System.Drawing.Point(622, 101);
            this.btnRunSave.Name = "btnRunSave";
            this.btnRunSave.Size = new System.Drawing.Size(75, 23);
            this.btnRunSave.TabIndex = 5;
            this.btnRunSave.Text = "Run && Save";
            this.btnRunSave.UseVisualStyleBackColor = true;
            this.btnRunSave.Click += new System.EventHandler(this.btnRunSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(622, 134);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRun
            // 
            this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRun.Location = new System.Drawing.Point(622, 70);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 2;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnSaveQuery
            // 
            this.btnSaveQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveQuery.Location = new System.Drawing.Point(622, 39);
            this.btnSaveQuery.Name = "btnSaveQuery";
            this.btnSaveQuery.Size = new System.Drawing.Size(75, 23);
            this.btnSaveQuery.TabIndex = 4;
            this.btnSaveQuery.Text = "Save Query";
            this.btnSaveQuery.UseVisualStyleBackColor = true;
            this.btnSaveQuery.Click += new System.EventHandler(this.btnSaveQuery_Click);
            // 
            // rtbQuery
            // 
            this.rtbQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbQuery.Location = new System.Drawing.Point(20, 36);
            this.rtbQuery.Name = "rtbQuery";
            this.rtbQuery.Size = new System.Drawing.Size(596, 88);
            this.rtbQuery.TabIndex = 3;
            this.rtbQuery.Text = "";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowse.Location = new System.Drawing.Point(622, 8);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtFileQuery
            // 
            this.txtFileQuery.Location = new System.Drawing.Point(93, 10);
            this.txtFileQuery.Name = "txtFileQuery";
            this.txtFileQuery.Size = new System.Drawing.Size(457, 20);
            this.txtFileQuery.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Query File";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dgvResult);
            this.groupBox2.Location = new System.Drawing.Point(12, 228);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(723, 219);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // dgvResult
            // 
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResult.ContextMenuStrip = this.ctmnustripResult;
            this.dgvResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvResult.Location = new System.Drawing.Point(3, 16);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.Size = new System.Drawing.Size(717, 200);
            this.dgvResult.TabIndex = 0;
            // 
            // ctmnustripResult
            // 
            this.ctmnustripResult.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveResultToolStripMenuItem});
            this.ctmnustripResult.Name = "ctmnustripResult";
            this.ctmnustripResult.Size = new System.Drawing.Size(134, 26);
            // 
            // saveResultToolStripMenuItem
            // 
            this.saveResultToolStripMenuItem.Name = "saveResultToolStripMenuItem";
            this.saveResultToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.saveResultToolStripMenuItem.Text = "Save Result";
            this.saveResultToolStripMenuItem.Click += new System.EventHandler(this.saveResultToolStripMenuItem_Click);
            // 
            // pbProcess
            // 
            this.pbProcess.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pbProcess.Location = new System.Drawing.Point(12, 199);
            this.pbProcess.Name = "pbProcess";
            this.pbProcess.Size = new System.Drawing.Size(723, 23);
            this.pbProcess.TabIndex = 2;
            // 
            // bwProcess
            // 
            this.bwProcess.WorkerReportsProgress = true;
            this.bwProcess.WorkerSupportsCancellation = true;
            this.bwProcess.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwProcess_DoWork);
            this.bwProcess.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwProcess_RunWorkerCompleted);
            // 
            // frmQueryLoader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 459);
            this.Controls.Add(this.pbProcess);
            this.Controls.Add(this.gbSetQuery);
            this.Controls.Add(this.groupBox2);
            this.Name = "frmQueryLoader";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Query Loader";
            this.Load += new System.EventHandler(this.frmQueryLoader_Load);
            this.gbSetQuery.ResumeLayout(false);
            this.tabctrlDbQuery.ResumeLayout(false);
            this.tabpageDatabase.ResumeLayout(false);
            this.tabpageDatabase.PerformLayout();
            this.tabpageQuery.ResumeLayout(false);
            this.tabpageQuery.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.ctmnustripResult.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSetQuery;
        private System.Windows.Forms.TabControl tabctrlDbQuery;
        private System.Windows.Forms.TabPage tabpageQuery;
        private System.Windows.Forms.TabPage tabpageDatabase;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtFileQuery;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox rtbQuery;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Button btnSaveQuery;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.DataGridView dgvResult;
        private System.Windows.Forms.ContextMenuStrip ctmnustripResult;
        private System.Windows.Forms.ToolStripMenuItem saveResultToolStripMenuItem;
        private System.Windows.Forms.Button btnRunSave;
        private System.Windows.Forms.ProgressBar pbProcess;
        private System.ComponentModel.BackgroundWorker bwProcess;
    }
}

