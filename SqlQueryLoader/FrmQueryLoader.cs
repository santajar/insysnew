using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;

namespace SqlQueryLoader
{
    public partial class frmQueryLoader : Form
    {
        SqlConnection oSqlConn = new SqlConnection();
        //SqlDataReader oRead;
        DataTable dtResult = new DataTable();
        string sQuery = null;

        public frmQueryLoader()
        {
            InitializeComponent();
        }

        private void frmQueryLoader_Load(object sender, EventArgs e)
        {
            InitDisplay();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(oSqlConn.ConnectionString))
            {
                if (IsValidConnectionParams())
                {
                    oSqlConn = ClassCommon.InitConnection(Server, Database, Username, Password);
                    DisableInitConnection(true);
                }
            }
            else
            {
                oSqlConn.Close();
                oSqlConn = new SqlConnection();
                DisableInitConnection(false);
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdQuery = new OpenFileDialog();
            //ofdQuery.InitialDirectory = Environment.SpecialFolder.Desktop.ToString();
            ofdQuery.InitialDirectory = Environment.CurrentDirectory + @"\QUERY";
            ofdQuery.Filter = "SQL Query Files(*.sql)|*.sql|Text Files(*.txt)|*.txt|All Files(*.*)|*.*";
            
            if (ofdQuery.ShowDialog() == DialogResult.OK)
            {
                FileQuery = ofdQuery.FileName;
                LoadQueryFiles();
            }
        }

        private void btnSaveQuery_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdQuery = new SaveFileDialog();
            sfdQuery.OverwritePrompt = true;
            sfdQuery.Filter = "SQL Query Files(*.sql)|*.sql|Text Files(*.txt)|*.txt|All Files(*.*)|*.*";
            if (sfdQuery.ShowDialog() == DialogResult.OK)
            {
                ClassCommon.SaveQueryFile(sfdQuery.FileName, sQuery);
            }
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            if (oSqlConn.State == ConnectionState.Open)
            {
                if (!string.IsNullOrEmpty(Query))
                {
                    StatusGroupBox(true);
                    bwProcess.RunWorkerAsync(false);
                }
                else
                {
                    MessageBox.Show("Query tidak ada atau file query belum dipilih. Pilih file terlebih dahulu!");
                    btnBrowse.Select();
                }
            }
            else
            {
                MessageBox.Show("Koneksi database belum ada. Setting koneksi terlebih dahulu!");
                tabctrlDbQuery.SelectedTab = tabpageDatabase;
            }
        }

        private void btnRunSave_Click(object sender, EventArgs e)
        {
            if (oSqlConn.State == ConnectionState.Open)
            {
                if (!string.IsNullOrEmpty(Query))
                {
                    StatusGroupBox(true);
                    bwProcess.RunWorkerAsync(true);
                }
                else
                {
                    MessageBox.Show("Query tidak ada atau file query belum dipilih. Pilih file terlebih dahulu!");
                    btnBrowse.Select();
                }
            }
            else
            {
                MessageBox.Show("Koneksi database belum ada. Setting koneksi terlebih dahulu!");
                tabctrlDbQuery.SelectedTab = tabpageDatabase;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void saveResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string sFilename = ClassCommon.sResultFilename();
            if (!string.IsNullOrEmpty(sFilename))
                ExcelLib.WriteExcel(ClassCommon.drGetResult(oSqlConn, txtFileQuery.Text), sFilename);
        }
        
        private void bwProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            ClassCommon.GetResultAndView((bool)e.Argument, ref dtResult, oSqlConn, sQuery);
        }

        private void bwProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StatusGroupBox(false);
        }

        #region "Properties"
        protected string Server { get { return txtServer.Text; } }
        protected string Database { get { return txtDatabase.Text; } }
        protected string Username { get { return txtUsername.Text; } }
        protected string Password { get { return txtPassword.Text; } }
        protected string FileQuery { get { return txtFileQuery.Text; } set { txtFileQuery.Text = value; } }
        protected string Query { get { return rtbQuery.Text; } set { rtbQuery.Text = value; } }
        #endregion

        #region "Function"
        protected void InitDisplay()
        {
            txtServer.Clear();
            txtDatabase.Clear();
            txtUsername.Clear();
            txtPassword.Clear();
        }

        protected void DisableInitConnection(bool IsDisable)
        {
            txtDatabase.Enabled = IsDisable ? false : true;
            txtServer.Enabled = IsDisable ? false : true;
            txtUsername.Enabled = IsDisable ? false : true;
            txtPassword.Enabled = IsDisable ? false : true;
            btnConnect.Text = IsDisable ? "Disconnect" : "Connect";
        }

        protected bool IsValidConnectionParams()
        {
            if (!string.IsNullOrEmpty(Server))
            {
                if (!string.IsNullOrEmpty(Database))
                {
                    if (!string.IsNullOrEmpty(Username))
                        return true;
                    else
                    {
                        MessageBox.Show("Isi kolom 'Username'!");
                        txtUsername.Select();
                    }
                }
                else
                {
                    MessageBox.Show("Isi kolom 'Database'!");
                    txtDatabase.Select();
                }
            }
            else
            {
                MessageBox.Show("Isi kolom 'Server'!");
                txtServer.Select();
            }
            return false;
        }

        protected void LoadQueryFiles()
        {
            if (!string.IsNullOrEmpty(FileQuery))
            {
                if (File.Exists(FileQuery))
                {
                    StreamReader sr = new StreamReader(FileQuery);
                    Query = sr.ReadToEnd();
                    sr.Close();
                }
                else
                    MessageBox.Show("File hilang, atau telah dihapus. Pilih kembali file.");
            }
        }

        protected void StatusGroupBox(bool IsEnable)
        {
            gbSetQuery.Enabled = !IsEnable;
            pbProcess.Style = IsEnable ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;
            if (IsEnable)
            {
                dgvResult.DataSource = null;
                sQuery = Query;
            }
            else
                if (dtResult.Rows.Count > 0)
                    dgvResult.DataSource = dtResult;
        }
        #endregion
    }
}