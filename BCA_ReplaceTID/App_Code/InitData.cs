﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ini;
using System.IO;
using InSysClass;

namespace BCA_ReplaceTID
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;
        protected string sDirectory;

        public InitData(string _sDirectory)
        {
            sDirectory = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectory + "\\Application.config";
            string sEncryptFileName = sDirectory + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception)
                {

                }
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnStringAsyncMARS(sDataSource, sDatabase, sUserID, sPassword);
        }

        public void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword)
        {
            string sFileName = sDirectory + "\\Application.config";
            string sEncryptFileName = sDirectory + "\\Application.conf";

            try
            {
                IniFile objIniFile = new IniFile(sFileName);
                objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
                objIniFile.IniWriteValue("Database", "Database", sDatabase);
                objIniFile.IniWriteValue("Database", "UserID", sUserID);
                objIniFile.IniWriteValue("Database", "Password", sPassword);
            }
            catch (Exception)
            {

            };
            EncryptionLib.EncryptFile(sEncryptFileName, sFileName);
            File.Delete(sFileName);
        }

    }
}
