using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Security.AccessControl;
using InSysClass;

namespace BCA_ReplaceTID
{
    /// <summary>
    /// Type of objects used in application
    /// </summary>
    enum ObjectType
    {
        COMBOBOX = 1,
        NULL,
        RADIOBUTTON,
        TEXTBOX
    }

    /// <summary>
    /// Type of error when showing error message
    /// </summary>
    enum ErrorType
    {
        ERROR = 1,
        WARNING
    }

    enum EmsTypeCommand
    {
        A,  //AddUpdate,
        D,  //Delete
        I,  //Inquiry
    }

    enum FormType
    {
        terminal = 1,
        acquirer = 2,
        issuer = 3,
        card = 4,
        relation = 5,
        tle = 6,
        ems = 7,
        loyaltypool = 8,
        loyaltyprod = 9,
        gprs = 10,
        currency = 11
    }

    enum TagType
    {
        de,
        dc,
        aa,
        ae,
        ac,
        ad,
        ai,
        pk,
        tl,
        gp
    }

    static class UserData
    {
        private static string _sUserID;        
        /// <summary>
        /// Get or Set UserID from login
        /// </summary>
        public static string sUserID
        {
            set { _sUserID = value; }
            get { return _sUserID; }
        }

        private static string _sUserName;
        /// <summary>
        /// Get or Set UserName from login
        /// </summary>
        public static string sUserName
        {
            set { _sUserName = value; }
            get { return _sUserName; }
        }

        private static string _sPassword;
        /// <summary>
        /// Get or Set Password from login
        /// </summary>
        public static string sPassword
        {
            set { _sPassword = value; }
            get { return _sPassword; }
        }

        private static bool _isSuperUser;
        /// <summary>
        /// Get or Set if User is a SuperUser
        /// </summary>
        public static bool isSuperUser
        {
            set { _isSuperUser = value; }
            get { return _isSuperUser; }
        }

        private static string _sTerminalIdActive;
        public static string sTerminalIdActive
        {
            get { return _sTerminalIdActive; }
            set { _sTerminalIdActive = value; }
        }

        private static string _sGroupID;
        public static string sGroupID
        {
            get { return _sGroupID; }
            set { _sGroupID = value; }
        }
    }

    class CommonClass
    {
        /// <summary>
        /// Add new Audit Trail Log to database.
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : connection to database</param>
        /// <param name="sTID">string : Terminal ID where the changes occur</param>
        /// <param name="sUsrID">string : User who made the modification</param>
        /// <param name="sDBName">string : Database Name which contains the Terminal ID</param>
        /// <param name="sActDesc">string : The changes that occur </param>
        /// <param name="sActDetl">string : Details of the modification </param>
        public static void InputLog(SqlConnection oSqlConn,string sTID, string sUsrID, string sDBName, string sActDesc, string sActDetl)
        {
            try
            {
                SqlParameter[] oSqlParam = new SqlParameter[5];
                oSqlParam[0] = new SqlParameter("@sTerminalID", System.Data.SqlDbType.VarChar, 8);
                oSqlParam[0].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[0].Value = sTID;
                oSqlParam[1] = new SqlParameter("@sUserId", System.Data.SqlDbType.VarChar, 10);
                oSqlParam[1].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[1].Value = sUsrID;
                oSqlParam[2] = new SqlParameter("@sDatabaseName", System.Data.SqlDbType.VarChar, 50);
                oSqlParam[2].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[2].Value = sDBName;
                oSqlParam[3] = new SqlParameter("@sActionDesc", System.Data.SqlDbType.VarChar, sActDesc.Length);
                oSqlParam[3].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[3].Value = sActDesc;
                oSqlParam[4] = new SqlParameter("@sActionDetail", System.Data.SqlDbType.VarChar, sActDetl.Length);
                oSqlParam[4].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[4].Value = (string.IsNullOrEmpty(sActDetl)) ? null : sActDetl;
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAuditTrailInsert, oSqlConn);
                if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
                oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                oSqlCmd.Parameters.AddRange(oSqlParam);
                oSqlCmd.ExecuteNonQuery();

                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Add new Audit Trail Log to database when Super User access SuperUser's menu
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : connection to database</param>
        /// <param name="sTID">string : Terminal ID where the changes occur</param>
        /// <param name="sUsrID">string : User who made the modification</param>
        /// <param name="sDBName">string : Database Name which contains the Terminal ID</param>
        /// <param name="sActDesc">string : The changes that occur </param>
        /// <param name="sActDetl">string : Details of the modification </param>
        public static void InputLogSU(SqlConnection oSqlConn, string sTID, string sUsrID, string sDBName, string sActDesc, string sActDetl)
        {
            try
            {
                SqlParameter[] oSqlParam = new SqlParameter[5];
                oSqlParam[0] = new SqlParameter("@sTerminalID", System.Data.SqlDbType.VarChar, 8);
                oSqlParam[0].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[0].Value = sTID;
                oSqlParam[1] = new SqlParameter("@sUserId", System.Data.SqlDbType.VarChar, 10);
                oSqlParam[1].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[1].Value = sUsrID;
                oSqlParam[2] = new SqlParameter("@sDatabaseName", System.Data.SqlDbType.VarChar, 50);
                oSqlParam[2].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[2].Value = sDBName;
                oSqlParam[3] = new SqlParameter("@sActionDesc", System.Data.SqlDbType.VarChar, sActDesc.Length);
                oSqlParam[3].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[3].Value = sActDesc;
                oSqlParam[4] = new SqlParameter("@sActionDetail", System.Data.SqlDbType.VarChar, sActDetl.Length);
                oSqlParam[4].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[4].Value = (string.IsNullOrEmpty(sActDetl)) ? null : sActDetl;
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAuditTrailSUInsert, oSqlConn);
                if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
                oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                oSqlCmd.Parameters.AddRange(oSqlParam);
                oSqlCmd.ExecuteNonQuery();

                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Display a message box for confirmation with specific text and specific title.
        /// </summary>
        /// <param name="sMessage">string : The message to display in message box</param>
        /// <param name="sTitle">string : The text to display in title bar of the message box</param>
        /// <returns>boolean : return true if user accept the confirmation, else return false</returns>
        public static bool isYesMessage(string sMessage, string sTitle)
        {
            return MessageBox.Show(sMessage, sTitle, MessageBoxButtons.YesNo) == DialogResult.Yes;
        }

        /// <summary>   
        /// Show message box with Error message and refouces to object that causes the error.
        /// </summary>
        /// <param name="sErrMsg">string : Error message to display in message box</param>
        /// <param name="sender">object : the object that caused the error</param>
        /// <param name="objType">ObjectType : Type of the objects</param>
        /// <param name="errType">ErrorType : Type of the error</param>
        public static void doShowErrorMessage(string sErrMsg, object sender, ObjectType objType, ErrorType errType)
        {
            if (errType == ErrorType.ERROR) MessageBox.Show(sErrMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else MessageBox.Show(sErrMsg, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            switch (objType)
            {
                case ObjectType.COMBOBOX : ((ComboBox)sender).Focus(); break;
                case ObjectType.NULL: break;
                case ObjectType.RADIOBUTTON: ((RadioButton)sender).Focus(); break;
                case ObjectType.TEXTBOX: ((TextBox)sender).Focus(); break;
            } 
        }

        /// <summary>
        /// Generate the connection string for Sql Connection
        /// </summary>
        /// <param name="sHost">string : server of the database</param>
        /// <param name="sDatabaseName">string : database that is used</param>
        /// <param name="sUserID">string :UserId to connect to database</param>
        /// <param name="sPassword">string : Password to connect to database</param>
        /// <returns>string : Connection String for Sql Connection</returns>
        public static string sConnString(string sHost, string sDatabaseName, string sUserID, string sPassword)
        {
            return "server=" + sHost + ";uid=" + sUserID + ";pwd=" + sPassword + ";database=" + sDatabaseName + "; Connection Lifetime=0;";
        }

        /// <summary>
        /// Cut message from EDC
        /// </summary>
        /// <param name="sValue">string : message from EDC</param>
        /// <returns>string : result of cutting</returns>
        private static string sCuttingMsgFromEDC(string sValue)
        {
            return sValue.Substring(2, sValue.Length - 6);
        }

        /// <summary>
        /// Convert Character string to a byte array value.
        /// </summary>
        /// <param name="sValue">string : Chararcter string will be converted</param>
        /// <returns>byte array : Result from the conversion</returns>
        private static byte[] CharStringtoByteArray(string sValue)
        {
            int iLenString = sValue.Length;
            byte[] arrbBuffer = new byte[iLenString];

            for (int iCount = 0; iCount < iLenString; iCount++)
            {
                arrbBuffer[iCount] = Convert.ToByte(Convert.ToInt32(Convert.ToChar(sValue.Substring(iCount, 1))));
            }
            return arrbBuffer;
        }

        public static byte[] arrbProcessingDataToSend(byte[] arrbValue)
        {
            string sResult = "";
            sResult = CommonLib.sByteArrayToHexString(arrbValue).Replace(" ", "");
            sResult = CommonClass.sCuttingMsgFromEDC(sResult);

            // length iso message 28byte
            // header specifierny 0028
            // seharusnya 001C
            sResult = sResult.Substring(4, sResult.Length - 4);
            string sLength = sLengthDataToHex(sResult.Length / 2);
            sResult = sLength + sResult;

            //proses perubahaan hexsa ke character
            byte[] arrbByte = CommonLib.HexStringToByteArray(sResult);
            //sResult = Common.ByteArrayToCharString(arrbByte);

            return arrbByte;
        }

        /// <summary>
        /// Convert length data from decimal to hex string.
        /// The format for the Hex value is 4 character. 
        /// If the length of conversion's result less than 4 then add character "0" if front of hex value.
        /// </summary>
        /// <param name="iLength">int : Lengt of the data</param>
        /// <returns>string : length of the data in Hex value</returns>
        private static string sLengthDataToHex(int iLength)
        {
            string sLen = CommonLib.sConvertDecToHex(iLength);
            switch (sLen.Length)
            {
                case 1: // if length = 1
                    sLen = "000" + sLen;
                    break;
                case 2:// if length = 2
                    sLen = "00" + sLen;
                    break;
                case 3:// if length = 3
                    sLen = "0" + sLen;
                    break;
                default: // if length = 4
                    break;
            };
            return sLen;
        }

        /// <summary>
        /// Create check sum from given value.
        /// Lenth of the check sum must be 2.
        /// </summary>
        /// <param name="sValue">string : value to be checked</param>
        /// <returns>string : check sum in Hex value</returns>
        private static string sCheckSum(string sValue)
        {
            Int32 iResult = 0;
            for (int iCount = 0; iCount < sValue.Length; iCount += 2)
                iResult = iResult ^ Convert.ToInt32(sValue.Substring(iCount, 2), 16);  // XOR

            string sResultBinary = Convert.ToString(iResult, 2);
            string sResultHex = Convert.ToString(Convert.ToInt32(sResultBinary, 2), 16);
            sResultHex = "00".Substring(0, 2 - sResultHex.Length) + sResultHex;
            return sResultHex;
        }

        private static string sAddingDataToEDCMsg(string sValue)
        {
            return "02" + sValue + "03" + sCheckSum(sValue + "03");
        }

        //Tes
        /// <summary>
        /// Converting Hex value to decimal
        /// </summary>
        /// <param name="sValue">string : Hex value for conversion</param>
        /// <returns>string : conversion's result in decimal value</returns>
        public static string sConvertHextoDec(string sValue)
        {
            string sResultValue = Convert.ToString(Convert.ToInt32(sValue.Substring(2, 2), 16));
            return "0000".Substring(0, 4 - sResultValue.Length) + sResultValue;
        }

        /// <summary>
        /// Converts the first 4 characters of given value into decimal.
        /// The given value is in Hex value.
        /// </summary>
        /// <param name="sValue">string : Hex value to be cut and converted</param>
        /// <returns>string : decimal value resulted from the conversion</returns>
        private static string sConvert4CharInFrontIntoDec(string sValue)
        {
            return sConvertHextoDec(sValue.Substring(0, 4)) + sValue.Substring(4, sValue.Length - 4);
        }

        public static byte[] sProcessingDataToWrite(byte[] arrbValue)
        {
            if (arrbValue != null)
            {
                string sResult = "";
                //byte[] arrbByte = CharStringtoByteArray(sValue);
                sResult = CommonLib.sByteArrayToHexString(arrbValue).Replace(" ", "");
                sResult = sConvert4CharInFrontIntoDec(sResult);
                sResult = sAddingDataToEDCMsg(sResult);
                //proses perubahaan hexsa ke character 
                byte[] arrbByte = CommonLib.HexStringToByteArray(sResult);

                return arrbByte;
            }
            else return null;
        }

        public static string sGetEncrypt(string sClearString)
        {
            return DataEncryptionClass.sGetMD5Result(sClearString);
        }

        /// <summary>
        /// Get the directory which holds error folder
        /// </summary>
        /// <returns>string : The directory path</returns>
        protected static string sGetErrorDirectory()
        {
            return Application.StartupPath + "\\LOGS";
        }

        /// <summary>
        /// Determines if the Error Folder is already exists or not
        /// </summary>
        /// <returns>boolean : true if folder is exist, else return false</returns>
        protected static bool isHaveLogDir()
        {
            if (Directory.Exists(sGetErrorDirectory())) 
                return true;
            else 
                return false;
        }

        /// <summary>
        /// Write error message to error file then show the error message in message box.
        /// </summary>
        /// <param name="sError">string : Message from the error</param>
        public static void doWriteErrorFile(string sError)
        {
            if (!isHaveLogDir()) Directory.CreateDirectory(sGetErrorDirectory());  // If folder not found then crete the error folder
            CommonLib.Log(sGetErrorDirectory(), sError); // Write error to file
            CommonClass.doShowErrorMessage(sError, null, ObjectType.NULL, ErrorType.ERROR); // Show error message in message box
        }

        /// <summary>
        /// Minimizing each opened child form.
        /// </summary>
        /// <param name="farrParent">form array : array of child form</param>
        /// <param name="sFormName">string : Form which want to be seen</param>
        public static void doMinimizeChild(Form[] farrParent, string sFormName)
        {
            foreach (Form a in farrParent)
            {
                if (a.Name != sFormName)
                    a.WindowState = FormWindowState.Minimized;
            }
        }

        public static void doShowChild(Form[] farrParent, string sFormName, FormWindowState oState)
        {
            foreach (Form a in farrParent)
                if (a.Name == sFormName)
                    a.WindowState = oState;
        }

        public static void doCloseChild(Form[] farrParent, string sFormName)
        {
            foreach (Form a in farrParent)
                if (a.Name == sFormName)
                    a.Close();
        }

        /// <summary>
        /// Determine if the given value match desired format.
        /// </summary>
        /// <param name="sText">string : value to be validated</param>
        /// <param name="sFormat">string : desired format for the value</param>
        /// <returns>boolean : true if value is valid, else return false</returns>
        public static bool isFormatValid(string sText, string sFormat)
        {
            string sValidInput = "!@#$%&*()_+-={}[]:\";'<>,.?|/\\ ";

            switch (sFormat)
            {
                case "H": return Regex.IsMatch(sText, "^[a-fA-F0-9]+$"); // Hexadecimal
                case "A": return Regex.IsMatch(sText, "^[a-zA-Z0-9]+$"); // Aplhanumeric
                case "A1": return Regex.IsMatch(sText, "^[a-zA-Z0-9\\s]+$"); // Alphanumeric and white space
                case "N": return Regex.IsMatch(sText, "^[0-9]+$"); // Numeric
                case "N1": return Regex.IsMatch(sText, "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])$"); // Ip address
                case "S": // Special characters
                    foreach (char c in sText.Trim().ToUpper().ToCharArray())
                        if (!sValidInput.Contains(c.ToString()) &&
                            (c < 'A' || c > 'Z') &&
                            (c < '0' || c > '9')) return false;
                    return true;
                case "T": return CommonLib.isValidDatetime(sText); // Datetime
            }
            return false;
        }

        /// <summary>
        /// Generate TLV value
        /// </summary>
        /// <param name="sTag">string : Tag title</param>
        /// <param name="sValue">string : Tag value</param>
        /// <param name="sLength">string : Length of tag value</param>
        /// <returns>string : TLV Value</returns>
        public static string sGenerateTLV(string sTag, string sValue, string sLength)
        {
            return sTag + ((string)sValue.Length.ToString()).PadLeft(int.Parse(sLength), '0') + sValue;
        }

        /// <summary>
        /// Determines if the template definition alread exists or not
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        /// <returns>boolean : true if template definition is found, else return false</returns>
        public static bool IsTemplateDefinitionExists(SqlConnection oSqlConn)
        {
            return
                (int)new SqlCommand(CommonSP.sSPUploadTagDECount, oSqlConn).ExecuteScalar() > 0 ||
                (int)new SqlCommand(CommonSP.sSPUploadTagAACount, oSqlConn).ExecuteScalar() > 0;
        }

        /// <summary>
        /// Determines if the upload definition alread exists or not
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        /// <returns>boolean : true if upload definition is found, else return false</returns>
        public static bool IsUploadDefinitionExists(SqlConnection oSqlConn)
        {
            return
                (int)new SqlCommand(CommonSP.sSPUploadTagDESourceCount, oSqlConn).ExecuteScalar() > 0 ||
                (int)new SqlCommand(CommonSP.sSPUploadTagAASourceCount, oSqlConn).ExecuteScalar() > 0;
        }

        /// <summary>
        /// Determines if Register old file is allowed or not.
        /// </summary>
        /// <param name="_sFilePath">string : path for the file</param>
        /// <param name="sTerminalId">string : Terminal ID wanted to be registered</param>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        /// <param name="iDbId">int : DatabaseID which contain sTerminalID</param>
        /// <returns>boolean : true if is allowed, else return false</returns>
        public static bool isCanRegisterOldFile(string _sFilePath, 
            string sTerminalId,
            SqlConnection oSqlConn,
            int iDbId)
        {
            string sFilePath = _sFilePath + sTerminalId + ".txt";
            string sFileValue = CommonLib.sReadTxtFile(sFilePath);
            if (sFileValue != null)
            {
                sFileValue = sFileValue.Replace("\"", "");
                int iIndexStart = sFileValue.IndexOf("DE0108") + "DE0108".Length;
                string sOldValue = sFileValue.Substring(iIndexStart, 8);
                StringBuilder sbFileValue = new StringBuilder(sFileValue);
                sFileValue = sbFileValue.Replace(sOldValue, sTerminalId, iIndexStart, sTerminalId.Length).ToString();
            }
            return (!string.IsNullOrEmpty(sFileValue) && isCanImportFileToDatabase(oSqlConn,iDbId, sTerminalId, sFileValue));
        }

        /// <summary>
        /// Determines if file can be imported to database or not.
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : Connection to Sql Database</param>
        /// <param name="iDbId">int : DatabaseID which contain sTerminalID</param>
        /// <param name="sTerminalId">string : Terminal ID wanted to be imported</param>
        /// <param name="sValue">string : Content of the file</param>
        /// <returns>boolean : true if file can be imported, else false</returns>
        protected static bool isCanImportFileToDatabase(SqlConnection oSqlConn,
            int iDbId,
            string sTerminalId, string sValue)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalImport, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = iDbId.ToString();
                oSqlCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalId;
                oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sValue;

                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Fill combobox with value from database
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : Connection to Sql Database</param>
        /// <param name="sSPName">string : stored procedure used to get the data from database</param>
        /// <param name="sCondition">string : Condition to filter the data</param>
        /// <param name="sMemberValue">string : used to set Value Member of the combo box</param>
        /// <param name="sDisplayText">string : used to set Display Member ot the combobox</param>
        /// <param name="oComboBox">ComboBox : ComboBox which want to filled</param>
        public static void FillComboBox(SqlConnection oSqlConn, string sSPName, string sCondition, string sMemberValue, string sDisplayText,
                ref ComboBox ocmbTemp)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(sSPName, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                if (!string.IsNullOrEmpty(sCondition)) oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
                DataTable oDataTable = new DataTable();
                oAdapt.Fill(oDataTable);
                ocmbTemp.DataSource = oDataTable;

                ocmbTemp.DisplayMember = sDisplayText;
                ocmbTemp.ValueMember = sMemberValue;

                ocmbTemp.SelectedIndex = -1;
                oAdapt.Dispose();
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }          
        }
    
        public static void FillListView(SqlConnection oSqlConn, string sSPName, string sCondition, string sDisplayText,
            ref ListView lvTemp)
        {
            
            SqlCommand oSqlCmd = new SqlCommand(sSPName, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            if (!string.IsNullOrEmpty(sCondition)) oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

            SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
            DataTable oDataTable = new DataTable();
            oAdapt.Fill(oDataTable);
            foreach (DataRow row in oDataTable.Rows)
                lvTemp.Items.Add(row[sDisplayText].ToString());

            oAdapt.Dispose();
            oSqlCmd.Dispose();
        }

        public static DataTable FilldgView(SqlConnection oSqlConn, string sSPName,  ref DataGridView dgTemp)
        { 
            SqlCommand oSqlCmd = new SqlCommand (sSPName, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            //if (!string.IsNullOrEmpty(sCondition1))
            //{
            //    oSqlCmd.Parameters.Add("@sLocationID", SqlDbType.VarChar).Value = sCondition1;
            //    oSqlCmd.Parameters.Add("@sLocationDesc", SqlDbType.VarChar).Value = sCondition2;
            //}
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
            DataTable dtTemp = new DataTable();            
            oAdapt.Fill(dtTemp);
            
            oAdapt.Dispose();
            oSqlCmd.Dispose();
            return dtTemp;
        }

        public static DataTable dtGetExcel(string sFilename)
        {
            string sConnString = string.Format("provider=Microsoft.Jet.OLEDB.4.0;data source={0};Extended Properties=Excel 8.0;", 
                sFilename);
            OleDbConnection oOleConn = new OleDbConnection(sConnString);
            OleDbCommand oOleCmd = new OleDbCommand("SELECT * FROM [Sheet1$]", oOleConn);

            oOleConn.Open();
            OleDbDataReader oRead = oOleCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            dtTemp.Load(oRead);
            oRead.Close();
            oRead.Dispose();
            oOleCmd.Dispose();
            oOleConn.Close();

            return dtTemp;
        }

        public static void SyncToEms(SqlConnection oSqlConn, string sTerminalId, EmsTypeCommand typeCmd)
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPXmlSyncEms, oSqlConn))
            {
                oSqlCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalId;
                oSqlCmd.Parameters.Add("@sCommand", SqlDbType.VarChar).Value = typeCmd.ToString();
                if (oSqlConn.State != ConnectionState.Open)
                    oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
            }
        }

        public static void AddFileSecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
        {
            // Create a new FileInfo object.
            FileInfo fInfo = new FileInfo(FileName);

            // Get a FileSecurity object that represents the 
            // current security settings.
            FileSecurity fSecurity = fInfo.GetAccessControl();

            // Add the FileSystemAccessRule to the security settings. 
            fSecurity.AddAccessRule(new FileSystemAccessRule(Account,
                                                            Rights,
                                                            ControlType));
            // Set the new access settings.
            fInfo.SetAccessControl(fSecurity);
        }

        public static void UserAccessInsert(SqlConnection _oSqlConn, string _sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPUserAccessInsert, _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = UserData.sUserID;
                oCmd.ExecuteNonQuery();
            }
        }

        public static void UserAccessDelete(SqlConnection _oSqlConn, string _sTerminalId)
        {
            if(!string.IsNullOrEmpty(UserData.sUserID))
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPUserAccessDelete, _oSqlConn))
                {
                    if (_oSqlConn.State != ConnectionState.Open)
                        _oSqlConn.Open();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = UserData.sUserID;
                    oCmd.ExecuteNonQuery();
                }
        }

        public static bool IsAllowUserAccess(SqlConnection _oSqlConn, string _sTerminalId, ref string sUserIdOnAccess)
        {
            bool isAllow = true;
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPUserAccessBrowse, _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@sOutput", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();
                
                sUserIdOnAccess = oCmd.Parameters["@sUserId"].Value.ToString();
                isAllow = oCmd.Parameters["@sOutput"].Value.ToString() == "1" ? false : true;
            }
            return isAllow;
        }

        public static Cursor DisplayWaitCursor(bool isWait)
        {
            return isWait ? Cursors.WaitCursor : Cursors.Arrow;
        }

        public static DataTable dtGetItemListEnabled(SqlConnection _oSqlConn)
        {
            DataTable dtTemp = new DataTable();
            using (SqlCommand oComm = new SqlCommand(CommonSP.sSPItemEnableBrowse, _oSqlConn))
            {
                oComm.CommandType = CommandType.StoredProcedure;
                oComm.Parameters.Add("@sGroupID", SqlDbType.VarChar).Value = UserData.sGroupID;
                using (SqlDataAdapter oAdapt = new SqlDataAdapter(oComm))
                    oAdapt.Fill(dtTemp);
            }
            return dtTemp;
        }
    }

    class CommonMessage
    {
        public static string sUnregisteredExpiry = "Unregistered or Expired Application.";
        public static string sConfirmationTitle = "Confirmation";
        public static string sConfirmationText = "Do you want to save the changes?";
        public static string sConfirmationDelete = "Are You Sure Want to Delete?";

        #region "Menu"
        //Login
        public static string sErrLoginUserID = "User ID still empty. Please fill User ID.";
        public static string sErrLoginUserIDFormat = "User ID must only contains alphanumeric.";
        public static string sErrLoginPassword = "Password still empty. Please fill Password.";
        public static string sErrLoginUserIDPasswordNotFound = "User ID or Password not match. Please contact Administator.";

        //Exit
        public static string sExitMessageText = "Are You Sure want to Exit?";
        public static string sExitMessageTitle = "Exit";

        //Logoff
        public static string sLogoutText = "Are You Sure want to Logout?";
        public static string sLogoutTitle = "Logout";

        //AllowInit
        public static string sInitDataText = "Use permission to Initialize Data?" + "\n" + "Status : ";
        public static string sInitDataTitle = "Initialize Data";

        //AutoInit
        public static string sErrorEmpty = "is still empty. Please fill";
        public static string sErrorFormat = "can only receive Numeric format. Please fill other value.";

        //User Management
        public static string sErrUserManUserName = "User Name still empty. Please fill User Name.";
        public static string sErrUserManUserNameFormat = "User Name must only contains alphanumeric.";
        public static string sErrUserManPassFormat = "Password must only contains alphanumeric.";
        public static string sErrUserManUidDouble = "User ID has already been used. Please change the User ID.";

        public static string sUserManDeleteTitle = "Delete User";
        public static string sUserMandDeleteUserText = "Are you sure want to delete user : '";

        //Change Password
        public static string sErrChangePassOldPassEmpty = "Old Password still Emtpy. Please Fill Your Old Password.";
        public static string sErrChangePassNewPassEmpty = "New Password still Emtpy. Please Fill Your New Password.";
        public static string sErrChangePassConfirmPassEmtyp = "Confirm Password still Emtpy. Please Fill Your Confirm Password.";
        public static string sErrChangePassMatchOldPass = "Old Password not match. Please Check Your Old Password";
        public static string sErrChangePassMatchNewPass = "New Password not match. Please Check Your New Password";
        public static string sChangePassConfirmText = "Are you sure want to change password?";
        public static string sChangePassSuccess = "Change Password Success.";

        //Primary DB
        public static string sErrPrimeDbDatabaseSource = "Server still empty. Please fill Server.";
        public static string sErrPrimeDbUserIDDB = "User ID still empty. Please fill User ID.";
        public static string sErrPrimeDbPasswordDB = "Password still empty. Please fill Password.";
        public static string sPrimeDbConnSuccess = "Connection to Database Success.";
        public static string sErrPrimeDbConnection = "Connection Failed. Please Contact Administrator.";

        //Database Template
        public static string sDatabaseTemplateTitle = "Choose Database as Template";
        public static string sDatabaseTemplateText = "Please select Database:";

        //"Upload"
        public static string sErrUploadDefinitionTitle = "Upload Definition";
        public static string sErrUploadTitle = "Upload.";
        public static string sErrUploadText = "Error Template Definition file or file is not exist.\nCreate Template Definition file?";

        public static string sErrUploadDataTitle = "Upload";
        public static string sErrUploadDataSubTitle = "Upload.";
        public static string sErrUploadDataText = "Error Map Definition file or file is not exist.\nCreate Map Definition file?";


        //"Export Profile to Text"
        public static string sErrExportProfileToText = "Database is Not selected or \"Profile/Master Profile\" is has not been selected";

        //"Debug"
        public static string sDebugTitle = "InSys";
        public static string sDebugMessage()
        {
            if (CommonVariable.IsDebugOn()) return "Disable DEBUG?";
            else return "Enable DEBUG?";
        }

        //"Testing"
        public static string sTerminal = "Terminal : ";
        public static string sInitPwd = "\nInit Pwd : ";
        public static string sF2Pwd = "\nF.2 Pwd : ";
        public static string sF99Pwd = "\nF.99 Pwd : ";

        //"Register"
        public static string sRegisterTitle = "Register Old File";
        public static string sRegisterText = "Please fill the Terminal ID";
        #endregion

        #region "Profile"
        //"Profile"
        public static string sCardDelConfirmText = "Are you sure want to delete card?";
        public static string sDelConfirmText = "Are you sure want to delete ";

        //"Copy Terminal"
        public static string sCopyTerminalTitle = "New Terminal ID";
        public static string sCopyTerminalText = "Please fill the new Terminal ID";

        //"SQL Error Message"
        public static string sSQLErrorPKConstraint = "Violation of PRIMARY KEY constraint";
        public static string sSQLErrorDuplicateKey = "Cannot insert duplicate key in object";

        #endregion

        #region "Log"
        public static string sRegisterSuccess = "Register Success";
        public static string sAccessLog = "Access Log";
        public static string sDeleteLog = "Delete Log";
        public static string sExportDataToExcel = "Export Data to Excel File";
        public static string sImportDataFromExcel = "Import Data from Excel File";
        public static string sUpdateDBPrimaryConn = "Update Primary Database Connection";
        public static string sChangePass = "User Change Password";
        public static string sLoginSucccess = "Login Success";
        public static string sLogoutSuccess = "User Logout";
        public static string sLogOutSuccess_ConnStringChanged = sLogoutSuccess + " caused by changing Connection String.";
        public static string sAddUserMngmt = "Add User Management";
        public static string sDeleteUserMngmt = "Delete User Management";
        public static string sEditUserMngmt = "Edit User Management";
        public static string sUpdateUserMngmt = "Update User Management";
        public static string sTrackEDC = "View EDC Tracker"; //"Browse"
        public static string sDownload = "Download File"; //"Download"
        public static string sViewUnited = "View United"; //"Monitor"
        public static string sViewUnitedDetail = "View United Detail";
        public static string sUpdDBConn = "Update Database Connection";
        public static string sDebugAndTesting = "Debug and Testing";
        public static string sFrmDebugOld = "Debug";
        public static string sLogInsys = "Log Insys";
        public static string sSetModem = "Setting Modem";
        public static string sAccessInitTrail = "Access Init Trail";
        public static string sInitTrailImportDetail = "Init Trail Import Detail";
        public static string sInitTrailImportSummary = "Init Trail Import Summary";
        public static string sInitTrailExportDetail = "Init Trail Export Detail";
        public static string sInitTrailExportSummary = "Init Trail Export Summary";
        public static string sInitTrailDeleteDetail = "Init Trail Delete Detail";
        public static string sInitTrailDeleteSummary = "Init Trail Delete Summary";
        public static string sExportProfileToText = "Export profile to Textfile";
        public static string sFormOpened = "View Form ";
        #endregion

        public static string sInputPassword = "Input Password";
        public static string sFrmPromptString = "Prompt String";
        public static string sViewSN = "View Terminal Serial Number";

        public static string sAutoInitPathEmpty = "Auto Init Report Path is still empty. Please fill Auto Init Report Path.";
        public static string sInitCompressEmpty = "Init Compress Path is still empty. Please fill Init Compress Path";

        #region "AID"
        public static string sErrAIDInvalid = "Invalid AID or AID List allready exist";
        public static string sErrAIDEmptyField = "AID Field cannot empty";
        #endregion

        #region "CAPK"
        public static string sErrCAPKInvalid = "Invalid CAPK or CAPK Index allready exist";
        public static string sErrCAPKEmptyField = "CAPK Field cannot empty";
        #endregion

        #region "Version"
        public static string sDbIDNotValid = "Database ID is not valid. Please input other value.";
        public static string sDbIdNewEmpty = "New Database ID is still empty. Please fill new Database ID.";
        public static string sDbIdExistingEmpty = "Existing Database ID is still empty. Please fill existing Database ID";
        public static string sDbSourceEqDbDest = "Database source is similar with Database Destination. Please choose other Database Destination.";
        public static string sDbNameEmpty = "Database Name is still empty. Please fill Database Name.";
        public static string sNotChecked = " is not selected.";
        public static string sIsEmpty = "is empty. Please fill";
        public static string sConfirmDeleteDbVersion = "Are you sure want to delete this version ?";
        public static string sConfirmDeleteDbVersionProfile = "Deleting this version will also delete all profiles in this Version.\nAre you sure want to delete this Version ?";
        public static string sConfirmDeleteDbVersionCardList = "Deleting this version will also delete all Card List in this Version.\nAre you sure want to delete this Version ?";
        #endregion

        #region "Application"
        public static string sFieldEmpty = " is still empty. Please fill ";
        public static string sAllowCompressEmpty = "Please select Allow Compress value.";
        public static string sFieldNotNumeric = " has invalid format (can only receive numbers).";
        #endregion

        //"Software Package"
        public static string sSoftwarePackageTitle = "Choose Software Package Name";
        public static string sSoftwarePackageText = "Please select Package name :";

    }

    class CommonSP
    {
        #region "Users"
        public static string sSPUserBrowse = "spUserLoginBrowse";
        public static string sSPUserInsert = "spUserLoginInsert";
        public static string sSPUserUpdate = "spUserLoginUpdate";
        public static string sSPUserDelete = "spUserLoginDelete";
        #endregion

        #region "SuperUser"
        public static string sSPSuperUserUpdate = "spSuperUserLoginUpdate";
        public static string sSPSuperUserBrowse = "spUserSuperLoginBrowse";
        #endregion

        #region "Application"
        public static string sSPApplicationBrowse = "spBitMapBrowse";
        public static string sSPApplicationInsert = "spBitMapInsert";
        public static string sSPApplicationUpdate = "spBitMapUpdate";
        public static string sSPApplicationDelete = "spBitMapDelete";
        #endregion

        #region "Profile"
        public static string sSPTerminalDBBrowse = "spProfileTerminalDbBrowse";
        public static string sSPTerminalListBrowse = "spProfileTerminalListBrowse";
        public static string sSPTerminalCompressBrowse = "spProfileTerminalInitCompressBrowse";
        public static string sSPRelationBrowse = "spProfileRelationBrowse";
        public static string sSPRelationDelete = "spProfileRelationDelete";
        public static string sSPProfileBrowse = "spProfileTerminalContentBrowse";
        public static string sSPItemBrowse = "spItemListBrowse";
        public static string sSPItemChildBrowse = "spItemListChildBrowse";
        public static string sSPObjBrowse = "spItemObjectBrowse";
        public static string sSPCmbBrowse = "spItemComboBoxBrowse";
        public static string sSPViewTerminalListBrowse = "spViewTerminalListBrowse";
        public static string sSPAutoInitLogInfoBrowse = "spAutoInitLogBrowse";

        public static string sSPTerminalBrowse = "spProfileTerminalBrowse";
        public static string sSPAcqBrowse = "spProfileAcquirerBrowse";
        public static string sSPIssBrowse = "spProfileIssuerBrowse";
        public static string sSPCardBrowse = "spProfileCardListBrowse";
        public static string sSPCardListBrowse = "spCardListBrowse";
        public static string sSTerminalListBrowse = "spProfileTerminalListBrowse";
        public static string sSPTerminalListBrowseLast = "spProfileTerminalListBrowseTopTen";

        public static string sSPTerminalCompressUpdate = "spProfileTerminalListCompressUpdate";

        public static string sSPProfileCopy = "spProfileTerminalCopyProcess";

        public static string sSPProfileDelete = "spProfileTerminalDeleteProcess";
        public static string sSPAcqDeleteByTID = "spProfileAcquirerDeleteProcess";
        public static string sSPIssDeleteByTID = "spProfileIssuerDeleteProcess";
        public static string sSPCardDeleteByTID = "spProfileRelationDeleteByIssuerOrCard";

        public static string sSPAcqRename = "spProfileAcquirerRenameProcess";
        public static string sSPIssRename = "spProfileIssuerRenameProcess";

        public static string sSPCardListInsert = "spProfileCardListInsert";
        public static string sSPCardListDelete = "spProfileCardListDelete";
        public static string sSPCardListUpdate = "spProfileCardListUpdate";

        public static string sSPTerminalInsert = "spProfileTerminalInsert";
        public static string sSPAcqInsert = "spProfileAcquirerInsert";
        public static string sSPIssInsert = "spProfileIssuerInsert";
        public static string sSPCardInsert = "spProfileRelationInsertProcess";

        public static string sSPTerminalUpdate = "spProfileTerminalUpdate";
        public static string sSPAcqUpdate = "spProfileAcquirerUpdate";
        public static string sSPIssUpdate = "spProfileIssuerUpdate";

        public static string sSPTerminalImport = "spProfileImport";
        public static string sSPRelationBrowseCondition = "spProfileRelationBrowseWithCondition";

        public static string sSPLocationBrowseWithTID = "spLocationBrowseWithTID";
        public static string sSPLocationUpdate = "spLocationUpdate";
        public static string sSPLocationBrowse = "spLocationBrowse";
        public static string sSPLocationInsert = "spLocationInsert";
        public static string sSPLocationDelete = "spLocationDelete";

		public static string sSPTerminalDbInsert = "spProfileTerminalDbInsert";
        public static string sSPVersionCopy = "spDatabaseVersionCopy";
        public static string sSPVersionDelete = "spDatabaseVersionDeleteProcess";

        public static string sSPCardListCopyByDbID = "spProfileCardListInsertByDbId";
        public static string sSPCardListCopyByID = "spProfileCardListInsertById";

        public static string sSPItemInsert = "spItemListInsert";
        public static string sSPItemUpdate = "spItemListUpdate";
        public static string sSPItemDelete = "spItemListDelete";
        public static string sSPItemDeleteAll = "spItemListDeleteByDbID";
        public static string sSPItemCmbValueInsert = "spItemComboBoxValueInsert";
        public static string sSPItemCmbValueDelete = "spItemComboBoxValueDelete";
        public static string sSPItemBrowseItemSeq = "spItemListBrowseValidItemSquence";

        public static string sSPMsItemFormBrowse = "spMsItemFormBrowse";

        public static string sSPBrowseProfileRelationAutoInitLogAppPackage = "spBrowseProfileRelationAutoInitLogAppPackage";
        #endregion

        #region "Audit"
        public static string sSPAuditTrailBrowse = "spAuditTrailBrowse";
        public static string sSPAuditTrailInsert = "spAuditTrailInsert";
        public static string sSPAuditTrailDelete = "spAuditTrailDelete";
        public static string sSPAuditTrailSUInsert = "spAuditTrailEncryptInsert";
        public static string sSPAuditTrailSUBrowse = "spAuditTrailEncryptBrowse";
        #endregion
        
        #region "InitTrail"
        public static string sSPInitTrailBrowse = "spAuditInitBrowse";
        public static string sSPInitTrailSummaryBrowse = "spAuditInitSummaryBrowse";

        public static string sSPInitTrailDelete = "spAuditInitDelete";
        public static string sSPInitTrailSummaryDelete = "spAuditInitSummaryDelete";
        #endregion

        #region "Auto Init"
        public static string sSPAutoInitBrowse = "spControlFlagAutoInitBrowse";
        public static string sSPAutoInitUpdate = "spControlFlagAutoInitUpdate";
        #endregion

        #region "Init Connection"
        public static string sSPInitConnBrowse = "spControlFlagInitBrowse";
        public static string sSPInitConnUpdate = "spControlFlagInitUpdate";
        #endregion

        #region "Monitoring"
        public static string sSPInitLogConnBrowse = "spInitLogConnBrowse";
        public static string sSPInitLogConnDetailBrowse = "spInitLogConnDetailBrowse"; 
        #endregion

        #region "Data Upload"
        public static string sSPUploadTagDECount = "spUploadTagDECount";
        public static string sSPUploadTagAACount = "spUploadTagAACount";

        public static string sSPUploadTagDESourceCount = "spUploadTagDESourceCount";
        public static string sSPUploadTagAASourceCount = "spUploadTagAASourceCount";

        public static string sSPUploadTagDEInsert = "spUploadTagDEInsert";
        public static string sSPUploadTagAAInsert = "spUploadTagAAInsert";

        public static string sSPUploadTagDEDelete = "spUploadTagDEDelete";
        public static string sSPUploadTagAADelete = "spUploadTagAADelete";

        public static string sSPUploadTagDEAAUnion = "spUploadTagDEAAUnion";

        public static string sSPUploadTagDEUpdateSourceColumn = "spUploadTagDEUpdateSourceColumn";
        public static string sSPUploadTagAAUpdateSourceColumn = "spUploadTagAAUpdateSourceColumn";

        public static string sSPUploadControlSet = "spUploadControlSet";
        public static string sSPUploadControlGet = "spUploadControlGet";

        public static string sSPUploadGetColumnsDE = "spUploadGetColumnsDE";
        public static string sSPUploadGetColumnsAA = "spUploadGetColumnsAA";

        public static string sSPDataUploadStartProcessAdd = "spDataUploadStartProcessAdd";
        public static string sSPDataUploadCreateTable = "spDataUploadCreateTable";
        public static string sSPDataUploadDropTable = "spDataUploadDropTable";
        public static string sSPDataUploadDelete = "spDataUploadDelete";

        public static string sSPUploadTagDEBrowse = "spUploadTagDEBrowse";
        public static string sSPUploadTagAABrowse = "spUploadTagAABrowse";

        public static string sSPDataUploadStartProcessUpdate = "spDataUploadStartProcessUpdate";
        public static string sSPDataUploadStartProcessDelete = "spDataUploadStartProcessDelete";
        #endregion

        #region "Control Flag"
        public static string sSPControlFlagAllowInitUpdate = "spControlFlagAllowInitUpdate";
        public static string sSPControlFlagAllowInitBrowse = "spControlFlagAllowInitBrowse";
        public static string sSPControlFlagUpdate = "spControlFlagUpdatePaths";
        public static string sSPControlFlagBrowse = "spControlFlagBrowseItem";
        #endregion

        #region "Export Profile"
        // "Export"
        public static string sSPExportProfileToText = "spProfileExportToText";
        public static string sSPProfileText = "spProfileText";
        public static string sSPProfileTextFull = "spProfileTextFull";
        #endregion

        #region "AID"
        public static string sSPProfileAIDBrowseList = "spProfileAIDBrowseList";
        public static string sSPProfileAIDBrowse = "spProfileAIDBrowse";
        public static string sSPProfileAIDInsert = "spProfileAIDInsert";
        public static string sSPProfileAIDDelete = "spProfileAIDDelete";
        public static string sSPProfileAIDUpdate = "spProfileAIDUpdate";
        #endregion

        #region "CAPK"
        public static string sSPProfileCAPKBrowseList = "spProfileCAPKBrowseList";
        public static string sSPProfileCAPKBrowse = "spProfileCAPKBrowse";
        public static string sSPProfileCAPKInsert = "spProfileCAPKInsert";
        public static string sSPProfileCAPKDelete = "spProfileCAPKDelete";
        public static string sSPProfileCAPKUpdate = "spProfileCAPKUpdate";
        #endregion

        #region "TLE"
        public static string sSPTLEListBrowse = "spProfileTLEListBrowse ";
        public static string sSPTLEBrowse = "spProfileTLEBrowse";
        public static string sSPTLEInsert = "spProfileTLEInsert";
        public static string sSPTLEUpdate = "spProfileTLEUpdate";
        public static string sSPTLEDelete = "spProfileTLEDelete";
        #endregion

        #region "Loyalty Pool"
        public static string sSPLoyPoolListBrowse = "spProfileLoyPoolListBrowse ";
        public static string sSPLoyPoolBrowse = "spProfileLoyPoolBrowse";
        public static string sSPLoyPoolInsert = "spProfileLoyPoolInsert";
        public static string sSPLoyPoolUpdate = "spProfileLoyPoolUpdate";
        public static string sSPLoyPoolDelete = "spProfileLoyPoolDelete";
        #endregion

        #region "Loyalty Product"
        public static string sSPLoyProdListBrowse = "spProfileLoyProdListBrowse ";
        public static string sSPLoyProdBrowse = "spProfileLoyProdBrowse";
        public static string sSPLoyProdInsert = "spProfileLoyProdInsert";
        public static string sSPLoyProdUpdate = "spProfileLoyProdUpdate";
        public static string sSPLoyProdDelete = "spProfileLoyProdDelete";
        #endregion

        #region "GPRS"
        public static string sSPGPRSListBrowse = "spProfileGPRSListBrowse ";
        public static string sSPGPRSBrowse = "spProfileGPRSBrowse";
        public static string sSPGPRSInsert = "spProfileGPRSInsert";
        public static string sSPGPRSUpdate = "spProfileGPRSUpdate";
        public static string sSPGPRSDelete = "spProfileGPRSDelete";
        #endregion

        #region "Currency"
        public static string sSPCurrencyListBrowse = "spProfileCurrencyListBrowse";
        public static string sSPCurrencyInsert = "spProfileCurrencyInsert";
        public static string sSPCurrencyBrowse = "spProfileCurrencyBrowse";
        public static string sSPCurrencyUpdate = "spProfileCurrencyUpdate";
        public static string sSPCurrencyDelete = "spProfileCurrencyDelete";
        #endregion

        #region "Software Package"
        public static string sSPAppPackageBrowse = "spAppPackageBrowse";
        public static string sSPAppPackageBrowseDetail = "spAppPackageBrowseDetail";
        public static string sSPAppPackageBrowseTerminalID = "spAppPackageBrowseTerminalID";
        public static string sSPAppPackageTerminalUpdate = "spAppPackageTerminalUpdate";
        public static string sSPAppPackageAdd = "spAppPackageAdd";
        public static string sSPAppPackageAddContent = "spAppPackageAddContent";
        public static string sSPAppPackageEdit = "spAppPackageEdit";
        public static string sSPAppPackageDelete = "spAppPackageDelete";
        #endregion

        #region "Edc Type"
        public static string sSPEdcTypeBrowse = "spEdcTypeBrowse";
        public static string sSPEdcTypeAdd = "spEdcTypeAdd";
        public static string sSPEdcTypeRemove = "spEdcTypeDelete";
        #endregion

        #region "Email Notification"
        public static string sSPSendNotification = "spSendNotification";
        #endregion

        #region "Software
        public static string sSPSoftwareAvailableBrowse = "spProfileSoftwareBrowseAvailable";
        public static string sSPSoftwareRegisteredBrowse = "spProfileSoftwareBrowseRegistered";
        public static string sSPSoftwareInsert = "spProfileSoftwareInsert";
        public static string sSPSoftwareDelete = "spProfileSoftwareDelete";
        #endregion

        #region "Synchronize To EMS"
        public static string sSPXmlSyncEms = "spXmlSyncEms";
        #endregion

        #region "Init Terminal"
        //public static string sSPInitTerminalInsert = "spInitTerminalInsert";
        public static string sSPInitTempClear = "spInitTempClear";
        #endregion

        #region "User Access"
        public static string sSPUserAccessInsert = "spUserAccessInsert";
        public static string sSPUserAccessDelete = "spUserAccessDelete";
        public static string sSPUserAccessBrowse = "spUserAccessBrowse";
        #endregion

        #region "User Group"
        public static string sSPUserGroupBrowse = "spUserGroupBrowse";
        public static string sSPItemDisabledBrowse = "spUserGroupItemDisabledBrowse";
        public static string sSPItemEnableBrowse = "spUserGroupItemEnableBrowse";
        public static string sSPUserGroupDelete = "spUserGroupDelete";
        #endregion
    }

    class CommonVariable
    {
        public const string PathMyComputer = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}";
        public const string PathMyDocuments ="::{450D8FBA-AD25-11D0-98A8-0800361B1103}";
        public const string PathMyNetworkPlaces = "::{208D2C60-3AEA-1069-A2D7-08002B30309D}";
        public const string PathPrinters = "::{2227A280-3AEA-1069-A2DE-08002B30309D}";
        public const string PathRecycleBin = "::{645FF040-5081-101B-9F08-00AA002F954E}";

        protected static bool isDebug;
        protected static bool isInitData;

        /// <summary>
        /// Determines if Debug is on or off
        /// </summary>
        /// <returns>boolean : true if on, else false</returns>
        public static bool IsDebugOn()
        {
            return isDebug;
        }

        /// <summary>
        /// Set status of debug
        /// </summary>
        /// <param name="_isDebug">boolean : status of debug, true if on</param>
        public static void SetisDebugOn(bool _isDebug)
        {
            isDebug = _isDebug;
        }

        /// <summary>
        /// Determines if Init data is allowed or not.
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        /// <returns>boolean : true if allowed, else false</returns>
        public static bool isCanInitData(SqlConnection oSqlConn)
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPControlFlagAllowInitBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oReader = oSqlCmd.ExecuteReader();
            if (oReader.Read())
                isInitData = oReader["Flag"].ToString() == "1" ? true : false;

            oReader.Close();
            oReader.Dispose();
            oSqlCmd.Dispose();

            return isInitData;
        }

        /// <summary>
        /// Set the status of Allow Init data
        /// </summary>
        /// <param name="_isInitData">boolean : true if InitAllowed, else false</param>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        public static void SetIsCanInitData(bool _isInitData, SqlConnection oSqlConn)
        {
            isInitData = _isInitData;
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPControlFlagAllowInitUpdate, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@bAllowInit", SqlDbType.Bit).Value = isInitData;

            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oSqlCmd.ExecuteNonQuery();
            oSqlCmd.Dispose();
        }
    }

}
