﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCA_ReplaceTID
{
    class StoreProcedure
    {
        public static string sReportLogInsert
        {
            get { return "spReplaceTIDLogInsert"; }
        }

        public static string sReplaceTID
        {
            get { return "spReplaceTIDBCA"; }
        }

        public static string sReportLogBrowse
        {
            get { return "spReplaceTIDLogBrowse"; }
        }
    }
}
