using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using InSysClass;

namespace BCA_ReplaceTID
{
    class DataEncryptionClass
    {
        public static string sGetMD5Result(string _sToEncrypt)
        {
            byte[] bSource = ASCIIEncoding.ASCII.GetBytes(_sToEncrypt);
            
            MD5CryptoServiceProvider oMD5 = new MD5CryptoServiceProvider();

            byte[] bHash = oMD5.ComputeHash(bSource);
            return CommonLib.sByteArrayToString(bHash);
        }                
    }
}
