using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using InSysClass;

namespace BCA_ReplaceTID
{
    /// <summary>
    /// Window Login
    /// </summary>
    public partial class FrmLogin : Form
    {
        /// <summary>
        /// Local SQLConnection object
        /// </summary>
        protected SqlConnection oSqlConn;

        /// <summary>
        /// Determines if user can login or not.
        /// </summary>
        public bool isCanLoginToSystem = false;

        /// <summary>
        /// Determines if super user can login or not.
        /// </summary>
        public bool isCanLoginToSuperSystem = false;

        /// <summary>
        /// Current UserName
        /// </summary>
        protected string sUserName = "";

        /// <summary>
        /// Current UserID
        /// </summary>
        protected string sUserID = "";
 
        /// <summary>
        /// Current Password
        /// </summary>
        protected string sPwd = "";

        protected string sGroupID = "";

        /// <summary>
        /// Codes for errors
        /// </summary>
        private enum ErrorCode
        {
            NoErr = 1, //avoid start from 0
            ErrLoginUserID,
            ErrLoginUserIDPasswordNotFound
        }

        /// <summary>
        /// Window Login constructor
        /// </summary>
        /// <param name="_oSqlConn">SQLConnection : SQL Server connection object parameter</param>
        public FrmLogin(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        /// <summary>
        /// Get User ID
        /// </summary>
        /// <returns>string : UserID</returns>
        public string sGetUserID()
        {
            return txtUserID.Text/*.Trim()*/.Replace("'", "''");
        }
        
        /// <summary>
        /// Get Password
        /// </summary>
        /// <returns>string : Password</returns>
        public string sGetPassword() 
        {
            return txtPassword.Text;//.Trim();
            //return sGetEncryptPassword();
        }

        /// <summary>
        /// Get User Name
        /// </summary>
        /// <returns>string : User Name</returns>
        public string sGetUserName()
        {
            return sUserName.ToUpper();
        }

        public string sGetGroupID()
        {
            return sGroupID;
        }

        /// <summary>
        /// Set an indicator indicating whether the objects can respond to user interaction.
        /// </summary>
        /// <param name="isEnabled">bool : The indicator value</param>
        private void SetDisplay(bool isEnabled)
        {
            this.Cursor = isEnabled ? Cursors.Default : Cursors.AppStarting;
            this.ControlBox = isEnabled;

            txtUserID.Enabled = isEnabled;
            txtPassword.Enabled = isEnabled;

            btnOK.Enabled = isEnabled;
            btnCancel.Enabled = isEnabled;
        }

        /// <summary>
        /// Event function, runs when btnOK is clicked.
        /// Set UserID, Password, and display form.
        /// </summary>
        private void btnOK_Click(object sender, EventArgs e)
        {
            sUserID = sGetUserID();
            sPwd = sGetPassword();

            SetDisplay(false);

            wrkLogin.RunWorkerAsync();
        }

        /// <summary>
        /// Validate UserID and Password. 
        /// </summary>
        /// <returns>boolean : true if UserID and Password valid, else false</returns>
        protected bool isValid()
        {
            if (sUserID.Length == 0)
            {
                wrkLogin.ReportProgress(100, ErrorCode.ErrLoginUserID);
                return false;
            }
            else
                if (sPwd.Length == 0)
                {
                    //CommonClass.showErrorMessageTxt(txtPassword, CommonMessage.sErrPassword);
                }
            return true;
        }

        /// <summary>
        /// Determine if The UserID is found in database.
        /// </summary>
        /// <returns>boolean : true if UserID is found, else false</returns>
        protected bool isCanLogin()
        {
            if (isUserIDFound()) // If is User
            {
                isCanLoginToSystem = true;
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Browse the database if UserID requested if Valid
        /// </summary>
        /// <returns>boolean : true if UserID is found, else false</returns>
        protected bool isUserIDFound()
        {
            bool isPass = false;
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        using (DataTable dtUser = new DataTable())
                        {
                            dtUser.Load(oRead);
                            if (dtUser.Rows.Count>0) // UserID Found
                            {
                                isPass = true;
                                UserPrivilege.sPrivilege = dtUser.Rows[0]["UserRights"].ToString();
                                sUserName = dtUser.Rows[0]["UserName"].ToString();
                                if (dtUser.Columns.IndexOf("GroupID") > -1)
                                    if (dtUser.Columns["GroupID"] != null)
                                        sGroupID = dtUser.Rows[0]["GroupID"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return isPass;
        }

        /// <summary>
        /// Condition to limit UserID search based on UserID and Encrypted Password
        /// </summary>
        /// <returns>string : the filter string</returns>
        protected string sCondition()
        {
            return " WHERE UserID='" + sUserID + "' AND Password='" + sGetEncryptPassword() + "'";
        }

        /// <summary>
        /// Get the encryption of the Password inputed by user
        /// </summary>
        /// <returns>string : Result of Password Encryption</returns>
        protected string sGetEncryptPassword()
        {
            return CommonClass.sGetEncrypt(sPwd);
        }

        /// <summary>
        /// Event function, runs when btnCancel is clicked.
        /// Set isCanLogin to false an close the form.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            isCanLoginToSystem = false;
            this.Close();
        }

        /// <summary>
        /// Starts the login task in a new thread.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void wrkLogin_DoWork(object sender, DoWorkEventArgs e)
        {
            if (isValid())
                if (isCanLogin())
                    wrkLogin.ReportProgress(100, ErrorCode.NoErr);
                else
                    wrkLogin.ReportProgress(100, ErrorCode.ErrLoginUserIDPasswordNotFound);
        }

        /// <summary>
        /// Reports login task process to this thread.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void wrkLogin_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 100)
            {
                switch ((ErrorCode)e.UserState)
                {
                    case ErrorCode.NoErr:
                        this.Close();
                        break;
                    case ErrorCode.ErrLoginUserID:
                        CommonClass.doShowErrorMessage(CommonMessage.sErrLoginUserID, txtUserID, ObjectType.TEXTBOX, ErrorType.ERROR);
                        break;
                    case ErrorCode.ErrLoginUserIDPasswordNotFound:
                        CommonClass.doShowErrorMessage(CommonMessage.sErrLoginUserIDPasswordNotFound, txtUserID, ObjectType.TEXTBOX, ErrorType.ERROR);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Sets display back to original after login task finished (either success or fail).
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void wrkLogin_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay(true);
        }
    }
}