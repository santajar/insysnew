﻿namespace BCA_ReplaceTID
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.gbMenu = new System.Windows.Forms.GroupBox();
            this.btnLogOff = new System.Windows.Forms.Button();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnReplaceTID = new System.Windows.Forms.Button();
            this.gbMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbMenu
            // 
            this.gbMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbMenu.Controls.Add(this.btnLogOff);
            this.gbMenu.Controls.Add(this.btnReport);
            this.gbMenu.Controls.Add(this.btnReplaceTID);
            this.gbMenu.Location = new System.Drawing.Point(9, 0);
            this.gbMenu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbMenu.Name = "gbMenu";
            this.gbMenu.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbMenu.Size = new System.Drawing.Size(388, 249);
            this.gbMenu.TabIndex = 0;
            this.gbMenu.TabStop = false;
            // 
            // btnLogOff
            // 

            this.btnLogOff.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnLogOff.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnLogOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogOff.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOff.Location = new System.Drawing.Point(3, 171);
            this.btnLogOff.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnLogOff.Name = "btnLogOff";
            this.btnLogOff.Size = new System.Drawing.Size(382, 62);
            this.btnLogOff.TabIndex = 1;
            this.btnLogOff.Text = "Log Off";
            this.btnLogOff.UseVisualStyleBackColor = false;

            this.btnReplaceTID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReplaceTID.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnReplaceTID.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnReplaceTID.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnReplaceTID.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnReplaceTID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReplaceTID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReplaceTID.Location = new System.Drawing.Point(2, 19);
            this.btnReplaceTID.Name = "btnReplaceTID";
            this.btnReplaceTID.Size = new System.Drawing.Size(255, 40);
            this.btnReplaceTID.TabIndex = 0;
            this.btnReplaceTID.Text = "Replace TID";
            this.btnReplaceTID.UseVisualStyleBackColor = false;

            this.btnLogOff.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnLogOff.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnLogOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogOff.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOff.Location = new System.Drawing.Point(2, 111);
            this.btnLogOff.Name = "btnLogOff";
            this.btnLogOff.Size = new System.Drawing.Size(255, 40);
            this.btnLogOff.TabIndex = 1;
            this.btnLogOff.Text = "Log Off";
            this.btnLogOff.UseVisualStyleBackColor = false;

            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnReport.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReport.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReport.Location = new System.Drawing.Point(3, 100);
            this.btnReport.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(382, 62);
            this.btnReport.TabIndex = 1;
            this.btnReport.Text = "Report Replace TID";
            this.btnReport.UseVisualStyleBackColor = false;
            // 
            // btnReplaceTID
            // 

            this.btnReplaceTID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReplaceTID.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnReplaceTID.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnReplaceTID.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnReplaceTID.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnReplaceTID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReplaceTID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReplaceTID.Location = new System.Drawing.Point(3, 29);
            this.btnReplaceTID.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnReplaceTID.Name = "btnReplaceTID";
            this.btnReplaceTID.Size = new System.Drawing.Size(382, 62);
            this.btnReplaceTID.TabIndex = 0;
            this.btnReplaceTID.Text = "Replace TID";
            this.btnReplaceTID.UseVisualStyleBackColor = false;

            this.btnLogOff.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnLogOff.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnLogOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogOff.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOff.Location = new System.Drawing.Point(2, 111);
            this.btnLogOff.Name = "btnLogOff";
            this.btnLogOff.Size = new System.Drawing.Size(255, 40);
            this.btnLogOff.TabIndex = 1;
            this.btnLogOff.Text = "Log Off";
            this.btnLogOff.UseVisualStyleBackColor = false;

            this.btnReplaceTID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReplaceTID.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnReplaceTID.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnReplaceTID.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnReplaceTID.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnReplaceTID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReplaceTID.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReplaceTID.Location = new System.Drawing.Point(2, 19);
            this.btnReplaceTID.Name = "btnReplaceTID";
            this.btnReplaceTID.Size = new System.Drawing.Size(255, 40);
            this.btnReplaceTID.TabIndex = 0;
            this.btnReplaceTID.Text = "Replace TID";
            this.btnReplaceTID.UseVisualStyleBackColor = false;
            this.btnReplaceTID.Click += new System.EventHandler(this.btnReplaceTID_Click_1);

            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;

            this.ClientSize = new System.Drawing.Size(381, 208);
            this.Controls.Add(this.gbMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(407, 268);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(407, 268);
            this.Name = "frmMenu";
            this.Text = "Menu";
            this.gbMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbMenu;
        private System.Windows.Forms.Button btnLogOff;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnReplaceTID;


    }
}