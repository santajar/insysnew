﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDuplicateProfile
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("START");
            try
            {
                int iTID = 0;
                int iCounter = 0;

                string sMax = ConfigurationManager.AppSettings["Max"].ToString();
                string sSqlConn = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                string sMaster = ConfigurationManager.AppSettings["Master"].ToString();

                SqlConnection sqlconn = new SqlConnection(sSqlConn);
                sqlconn.Open();
                if(!string.IsNullOrEmpty(sMaster) && !string.IsNullOrEmpty(sSqlConn) &&!string.IsNullOrEmpty(sMax))
                {
                    int iMax = int.Parse(sMax);
                    int iDatabaseID = 0;
                    using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, sqlconn))
                    {
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format("WHERE TerminalID='{0}'", sMaster);

                        using (SqlDataReader reader = sqlcmd.ExecuteReader())
                            if (reader.HasRows)
                                if (reader.Read())
                                    iDatabaseID = int.Parse(reader["DatabaseID"].ToString());
                    }

                    while (iCounter < iMax && iDatabaseID > 0)
                    {
                        string sTID = string.Format("{0:00000000}", iTID);
                        bool bExist = false;
                        using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, sqlconn))
                        {
                            sqlcmd.CommandType = CommandType.StoredProcedure;
                            sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format("WHERE TerminalID='{0}'", sTID);

                            using (SqlDataReader reader = sqlcmd.ExecuteReader())
                                if (reader.HasRows)
                                    bExist = true;
                        }
                        if (!bExist)
                        {
                            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPProfileCopy, sqlconn))
                            {
                                sqlcmd.CommandType = CommandType.StoredProcedure;
                                sqlcmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = iDatabaseID;
                                sqlcmd.Parameters.Add("@sOldTID", SqlDbType.VarChar).Value = sMaster;
                                sqlcmd.Parameters.Add("@sNewTID", SqlDbType.VarChar).Value = sTID;
                                sqlcmd.ExecuteNonQuery();
                                iCounter++;
                                Console.WriteLine("{0}.Copy '{1}' --> '{2}' ", iCounter, sMaster, sTID);
                            }
                        }
                        iTID++;
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("END");
            Console.Read();
        }
    }
}
