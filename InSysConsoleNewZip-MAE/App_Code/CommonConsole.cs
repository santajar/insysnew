using System;
using System.Globalization;
using System.IO;
using InSysClass;

namespace InSysConsoleNewZip
{
    public enum ConnType
    {
        TcpIP = 1,
        Serial = 2,
        Modem = 3
    }

    public enum ProcCode
    {
        ProfileStartEnd = 930000,
        ProfileCont = 930001,
        AutoProfileStartEnd = 960000,
        AutoProfileCont = 960001,
        Flazz = 940000,
        AutoPrompt = 950000,
        AutoCompletion = 970000,
        TerminalEcho = 800000,
        DownloadProfileStartEnd = 980000,
        DownloadProfileCont = 980001,
        ProfileStartMessage = 914000,
        ProfileStartMessageCont = 914001,
    }
    
    class CommonConsole
    {
        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sTimeHHMMSS()
        {
            return DateTime.Now.ToString("hhmmss", ciUSFormat);
        }

        public static string sDateMMddyy()
        {
            return DateTime.Now.ToString("MMddyy", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = CommonConsole.sFullTime();
            Console.BufferWidth = 1000; //untuk menangani case spasi kosong Init setelah remote 
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }

        public static string sGenerateISOSend(string sContent, ISO8583_MSGLib oIsoTemp,
            bool isLast, bool isHexContent,
            bool isEnable48, bool isEnableCompress, bool isEnableRemoteDownload,
            string sAppName, bool isInitMigrasi)
        {
            string sIsoResponse;
            ISO8583Lib oIso = new ISO8583Lib();
            oIsoTemp.MTI = "0810";
            oIsoTemp.sBinaryBitMap = "0010000000100000000000010000000000000010100000000000000000000000";
            ProcCode tempProcCode = (ProcCode)int.Parse(oIsoTemp.sBit[3].ToString());
            string sContentBit58 = "";

            switch (tempProcCode)
            {
                case ProcCode.ProfileStartMessage:
                case ProcCode.ProfileStartMessageCont:
                    oIsoTemp.sBinaryBitMap = "0010000000000000000000010000000000000010100001000000000000000000";
                    //oIso.SetField_Str(3, ref oIsoTemp, "914000");
                    oIso.SetField_Str(3, ref oIsoTemp,
                        isLast ? ProcCode.ProfileStartMessage.GetHashCode().ToString() : ProcCode.ProfileStartMessageCont.GetHashCode().ToString());
                    oIso.SetField_Str(39, ref oIsoTemp, "00");
                    oIso.SetField_Str(46, ref oIsoTemp, isHexContent ? sContent : CommonLib.sStringToHex(sContent));
                    //oIso.SetField_Str(11, ref oIsoTemp, "");
                    break;
                case ProcCode.ProfileStartEnd:
                case ProcCode.ProfileCont:
                    oIso.SetField_Str(3, ref oIsoTemp, 
                        isLast ? ProcCode.ProfileStartEnd.GetHashCode().ToString() : ProcCode.ProfileCont.GetHashCode().ToString());

                    if (isEnableCompress)
                        sContentBit58 = sContentBit58 + "1";
                    else
                        sContentBit58 = sContentBit58 + "0";
                    oIso.SetField_Str(58, ref oIsoTemp, sContentBit58);
                    break;
                case ProcCode.DownloadProfileStartEnd:
                case ProcCode.DownloadProfileCont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        isLast ? ProcCode.DownloadProfileStartEnd.GetHashCode().ToString() : ProcCode.DownloadProfileCont.GetHashCode().ToString());
                    break;
                case ProcCode.AutoProfileStartEnd:
                case ProcCode.AutoProfileCont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        isLast ? ProcCode.AutoProfileStartEnd.GetHashCode().ToString() : ProcCode.AutoProfileCont.GetHashCode().ToString());
                    break;
                case ProcCode.TerminalEcho:
                    oIso.SetField_Str(3, ref oIsoTemp, ProcCode.TerminalEcho.GetHashCode().ToString());
                    break;
                case ProcCode.AutoPrompt:
                    if (isEnableCompress)
                        sContentBit58 = sContentBit58 + CommonLib.sHexToStringUTF8("01");
                    else
                        sContentBit58 = sContentBit58 + CommonLib.sHexToStringUTF8("00");

                    if (!string.IsNullOrEmpty(sContent))
                    {
                        string sContenInitFlag;
                        if (isInitMigrasi)
                            sContenInitFlag = CommonLib.sHexToStringUTF8("02").ToString();
                        else
                            sContenInitFlag = sContent.Length == 1 ? CommonLib.sHexToStringUTF8("00") + sContent : sContent;
                        sContentBit58 = sContentBit58 + sContenInitFlag;
                    }
                    else sContentBit58 = sContentBit58 + CommonLib.sHexToStringUTF8("00");

                    if (isEnableRemoteDownload)
                        sContentBit58 = sContentBit58 + CommonLib.sHexToStringUTF8("01");
                    else
                        sContentBit58 = sContentBit58 + CommonLib.sHexToStringUTF8("00");

                    oIso.SetField_Str(58, ref oIsoTemp, sContentBit58);
                    break;
            }
            
            if (isEnable48)
            {
                oIso.SetField_Str(12, ref oIsoTemp, string.Format("{0}", DateTime.Now.ToString("hhmmss", ciINAFormat)));
                if (tempProcCode == ProcCode.TerminalEcho)
                    oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));
                else
                    if (string.IsNullOrEmpty(sAppName) ||
                        Program.dtISO8583.Select(string.Format("SoftwareName='{0}' AND StandartISO8583=1", sAppName)).Length == 0)
                        oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMddyy}", DateTime.Now));
                    else
                        oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));

                if (string.IsNullOrEmpty(sAppName) ||
                        Program.dtISO8583.Select(string.Format("SoftwareName='{0}' AND SentBit48=1", sAppName)).Length > 0)
                        oIso.SetField_Str(48, ref oIsoTemp, CommonLib.sStringToHex(oIsoTemp.sBit[48]));
            }
            oIso.SetField_Str(39, ref oIsoTemp, "00");

            //if (isEnableRemoteDownload)
            //    oIso.SetField_Str(57, ref oIsoTemp, "1");

            //if (isEnableCompress)
            //    oIso.SetField_Str(58, ref oIsoTemp, "1");

            if (tempProcCode != ProcCode.AutoPrompt)
                if (!string.IsNullOrEmpty(sContent))
                    oIso.SetField_Str(60, ref oIsoTemp, isHexContent ? sContent : CommonLib.sStringToHex(sContent));

            sIsoResponse = oIso.PackToISOHex(oIsoTemp);
            return sIsoResponse;
        }

        public static string sGenerateISOError(string sContent, ISO8583_MSGLib oIsoTemp)
        {
            return string.Format("{0}{1}{2}{3}", oIsoTemp.sTPDU, oIsoTemp.sOriginAddr, oIsoTemp.sDestNII, sContent);
        }

        public static void GetAppNameSN(string sBit48, ref string _sAppName, ref string _sSN)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            _sSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            int iLenAppName = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2), 4));
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2), iLenAppName * 2));
        }

        public static void GetAutoInitMessage(string sBit48, ref string _sLastInitTime, ref string _sPABX, ref string _sAppName, ref string _sOSVers, ref string _sKernelEmvVers, ref string _sSerialNum, ref string _sReaderSN, ref string _sPSAMSN, ref string _sMCSN, ref string _sMemUsg, ref string _sICCID)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iIndex= 0;
            _sLastInitTime = sHexBit48.Substring(iIndex,12);
            iIndex += 12;
            try
            {
                _sPABX = int.Parse(sHexBit48.Substring(iIndex, 8)).ToString();
            }
            catch (Exception)
            {
                _sPABX = "";
            }
            iIndex += 8;
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30)).TrimEnd();
            iIndex += 30;
            _sOSVers = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 30;
            _sKernelEmvVers = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 30;
            _sSerialNum=CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex,30));
            iIndex += 30;
            _sReaderSN=CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex,30));
            iIndex += 30;
            _sPSAMSN=CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex,20));
            iIndex += 20;
            _sMCSN=CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex,20));
            iIndex += 20;
            _sMemUsg=CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex,30));
            iIndex += 30;
            _sICCID = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 40));            
        }

        public static void GetSNAPPNameAutoInitMessage(string sBit48,ref string _sAppName,ref string _sSerialNum)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iIndex = 0;
            iIndex += 20;
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 90;
            _sSerialNum = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));

        }
        
        public static string[] arrsGetBit48Values(string sBit48)
        {
            string[] arrsValues = new string[3];
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iIndex = 0;

            // SN <SN Length><SN>
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            iIndex += 4;
            arrsValues[0] = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            iIndex += (iLenSN * 2);
            
            // AppName <AppName Length><AppName>
            int iLenAppName = int.Parse(sHexBit48.Substring(iIndex, 4));
            iIndex += 4;
            arrsValues[1] = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, iLenAppName * 2));
            iIndex += (iLenAppName * 2);

            // Tipe Comms <CommsId>
            arrsValues[2] = sHexBit48.Substring(iIndex, 4);
            
            return arrsValues;
        }

        public static bool IsOpenFileAllowed(string sFilename)
        {
            bool bAllow = false;
            FileStream fs = null;
            try
            {
                fs = File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite);
                fs.Close();
                bAllow = true;
            }
            catch (IOException)
            {
                bAllow = false;
            }
            return bAllow;
        }
        
        public static int iISOLenReceive(string sTempLen)
        {
            int iReturn = 0;
            iReturn = CommonLib.iHexStringToInt(sTempLen);
            return iReturn;
        }

        /// <summary>
        /// convert Receive iso Message from hex to Int
        /// </summary>
        /// <param name="sTempLen">string : Temporary Len</param>
        /// <param name="cType">ConType : Conection Type</param>
        /// <returns>INT</returns>
        public static int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            //if (cType == ConnType.TcpIP)
            iReturn = CommonLib.iHexStringToInt(sTempLen);
            //else if (cType == ConnType.Serial)
            //    iReturn = int.Parse(sTempLen);
            return iReturn;
        }        
    }
}