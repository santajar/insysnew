﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleQueryReport
{
    class LogFile
    {
        public string writeLog(string strLogFile, string strData)
        {
            string functionReturnValue = null;
            System.IO.StreamWriter s = new System.IO.StreamWriter(strLogFile, true);
            try
            {
                s.WriteLine(DateTime.Now.ToString());
                s.WriteLine(strData);
                s.WriteLine("#".PadRight(200, '#'));
                s.Close();
                functionReturnValue = "0";
            }
            catch (Exception ex)
            {
                functionReturnValue = ex.Message + " - " + strData;
            }
            finally
            {
                s = null;
            }
            return functionReturnValue;
        }

        public string write(string strLogFile, string strData)
        {
            string functionReturnValue = null;
            System.IO.StreamWriter s = new System.IO.StreamWriter(strLogFile, true);
            try
            {
                s.WriteLine(strData);
                s.Close();
                functionReturnValue = "0";
            }
            catch (Exception ex)
            {
                functionReturnValue = ex.Message + " - " + strData;
            }
            finally
            {
                s = null;
            }
            return functionReturnValue;
        }

    }
}
