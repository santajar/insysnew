﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using InSysClass;

namespace ConsoleQueryReport
{
    class Program
    {
        /*
        static string sSPGetCardverOfflineManual = "spGetCardverOfflineManual";

        static void Main(string[] args)
        {
            DateTime dtStartTime = DateTime.Now;
            string sLogPath;
            LogFile oLogFile;
            sLogPath = ConfigurationManager.AppSettings.Get("LogPath");
            
            //Console.WriteLine(logpath);

            if (!Directory.Exists(sLogPath))
                Directory.CreateDirectory(sLogPath);
            if (!sLogPath.EndsWith("\\"))
            {
                sLogPath += "\\";
            }
            sLogPath = sLogPath + String.Format("{0:yyyy-MM-dd}", DateTime.Now) + ".log";
            oLogFile = new LogFile();
            string sMessage;
            sMessage = "********************Start Of Process****************************";
            oLogFile.writeLog(sLogPath, sMessage);
            sMessage = "Start Time: " + dtStartTime.ToString();
            oLogFile.writeLog(sLogPath, sMessage);
            try
            {
                string sSqlConnString = ConfigurationManager.AppSettings.Get("SqlConnection");
                string sLastRn = ConfigurationManager.AppSettings.Get("Lastrntime");
                DbConnection oSqlConn;
                oSqlConn = new DbConnection(sSqlConnString);
                oSqlConn.defineLogPath = sLogPath;
                //String queryStoreProcedure = "select * from fnGetFDMFasilitas('" + lastrn + "')";
                //DataTable dt = dbSql.executeSql(queryStoreProcedure);
                DataTable dt = oSqlConn.dtExecuteProcedure(sSPGetCardverOfflineManual);
                if (dt !=null && dt.Rows.Count > 0)
                {
                    string sPathOutput = ConfigurationManager.AppSettings.Get("pathfile");
                    if (Directory.Exists(sPathOutput))
                    {
                        string sPathOutput2 = ConfigurationManager.AppSettings.Get("pathfile2"); 
                        if (!sPathOutput.EndsWith("\\"))
                        {
                            sPathOutput += "\\";
                        }
                        string sFilename = ConfigurationManager.AppSettings.Get("filename");
                        sPathOutput = sPathOutput + sFilename;
                        if (File.Exists(sPathOutput))
                            File.Delete(sPathOutput);
                        LogFile oLogFileResult = new LogFile();
                        foreach (DataRow dr in dt.Rows)
                        {
                            string sData = string.Empty;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                string sValue = dr[dc].ToString();
                                switch (dc.ColumnName)
                                {
                                    case "TID":
                                        sValue = string.IsNullOrEmpty(sValue.Trim()) ? "".PadLeft(8, ' ') : sValue;
                                        break;
                                    case "MID":
                                        sValue = string.IsNullOrEmpty(sValue.Trim()) ? "".PadLeft(9, ' ') : sValue;
                                        break;
                                    case "Tanggal Init":
                                    case "Tanggal Update":
                                        sValue = string.IsNullOrEmpty(sValue) ? "000000" : sGetDDMMYY(sValue);
                                        break;
                                    default:
                                        break;
                                }
                                //sData += dc.ColumnName.Contains("Tanggal") ?
                                //    string.IsNullOrEmpty(dr[dc].ToString()) ? "000000" : sGetDDMMYY(dr[dc].ToString()) :
                                //    dr[dc].ToString();
                                sData += sValue;
                            }
                            sData += " ".PadRight(9);
                            oLogFileResult.write(sPathOutput, sData);
                        }
                        oLogFileResult.write(sPathOutput, string.Format("{0:ddMMyy}{1:000000000000}{2}", DateTime.Now, dt.Rows.Count, " ".PadRight(32)));
                        Configuration configuration = ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);
                        configuration.AppSettings.Settings["Lastrntime"].Value = DateTime.Now.Day.ToString("00") + "/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString("00");
                        configuration.Save();

                        ConfigurationManager.RefreshSection("appSettings");

                        if (Directory.Exists(sPathOutput2))
                        {
                            if (!sPathOutput2.EndsWith("\\"))
                            {
                                sPathOutput2 += "\\";
                            }
                            sPathOutput2 = sPathOutput2 + sFilename;
                            if (File.Exists(sPathOutput2))
                                File.Delete(sPathOutput2);
                            File.Copy(sPathOutput, sPathOutput2);
                        }
                        else
                            Console.WriteLine("FAILED : Output path-2 does not exist");
                    }
                    else
                        Console.WriteLine("FAILED : Output path-1 does not exist");
                }
                else
                    Console.WriteLine("FAILED : Empty data result");
            }
            catch (FileNotFoundException ex)
            {
                oLogFile = new LogFile();
                string _sError;
                _sError = ex.Message + ex.StackTrace;
                oLogFile.writeLog(sLogPath, _sError);
                Console.WriteLine("FAILED : File Not Found");
            }
            catch (Exception ex)
            {
                oLogFile = new LogFile();
                string _sError;
                _sError = ex.Message + ex.StackTrace;
                oLogFile.writeLog(sLogPath, _sError);
                Console.WriteLine("FAILED : {0}", ex.Message.ToString());
            }
            finally
            {
                //Tex.Close();
                oLogFile = new LogFile();
                string _sError;
                DateTime endTime = DateTime.Now;
                _sError = "End Time : " + endTime.ToString();
                oLogFile.writeLog(sLogPath, _sError);
                TimeSpan durationTime = endTime.Subtract(dtStartTime);
                _sError = "Duration : " + durationTime.ToString();
                oLogFile.writeLog(sLogPath, _sError);
                _sError = "********************End Of Process****************************";
                oLogFile.writeLog(sLogPath, _sError);
                oLogFile = null;
            }
        }
        */

        static void Main(string[] args)
        {
            string sLogPath = string.Format(@"{0}\{1}", Environment.CurrentDirectory, ConfigurationManager.AppSettings.Get("LogPath"));
            string sTerminalHistory = string.Format("{0:yyyyMMdd}_{1}", DateTime.Now, ConfigurationManager.AppSettings.Get("Filename"));
            string sQueryFile = ConfigurationManager.AppSettings.Get("Query");
            string sCounter = ConfigurationManager.AppSettings.Get("Counter");
            string sConnection = ConfigurationManager.AppSettings.Get("SqlConnection");

            if (!string.IsNullOrEmpty(sQueryFile) && File.Exists(sQueryFile))
            {
                string sQueryContent = null;
                using (StreamReader reader = new StreamReader(sQueryFile))
                    sQueryContent = reader.ReadToEnd();
                if (!string.IsNullOrEmpty(sQueryContent))
                {
                    sQueryContent = sQueryContent.Replace("[number]", sCounter);
                    try
                    {
                        SqlConnection sqlconnServer = new SqlConnection(sConnection);
                        sqlconnServer.Open();

                        SqlTransaction sqltransCommand = sqlconnServer.BeginTransaction("Query");                        
                        SqlCommand cmd = new SqlCommand(sQueryContent, sqlconnServer,sqltransCommand);
                        cmd.CommandTimeout = 0;
                        try
                        {
                            DataTable dtTerminal = new DataTable();
                            using (SqlDataReader reader = cmd.ExecuteReader())
                                if (reader.HasRows) dtTerminal.Load(reader);
                            Csv.Write(dtTerminal, sTerminalHistory);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("GAGAL EXEC QUERY : {0}", ex.Message);
                            sqltransCommand.Rollback("Query");
                        }
                        sqltransCommand.Commit();
                        Console.WriteLine("SUKSES {0} TERMINAL", sCounter);
                    }
                    catch (SqlException sqlex)
                    {
                        Console.WriteLine("GAGAL SQL : {0}", sqlex.Message);
                    }
                }
            }
            Console.Read();
        }

        private static string[] ReadQueryArrayFile(string sQueryFile)
        {
            string sContent = null;
            using (StreamReader reader = new StreamReader(sQueryFile))
                sContent = reader.ReadToEnd();
            return sContent.Split(';');
        }

        static string sGetDDMMYY(string sDate)
        {
            return string.Format("{0:ddMMyy}", CommonLib.dtStringToDateTime(sDate.Substring(0,6)));
        }
    }
}
