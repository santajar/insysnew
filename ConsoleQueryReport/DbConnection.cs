﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;

namespace ConsoleQueryReport
{
    class DbConnection
    {
        private OleDbConnection con;
        LogFile insLogFile;
        string strLogPath;
        string _sError;
        string sConstr;

        public DbConnection(string connectStr)
        {
            con = new OleDbConnection(connectStr);
            con.Open();
            sConstr = connectStr;
        }

        public string defineLogPath
        {
            get { return strLogPath; }
            set { strLogPath = value; }
        }

        public DataTable dtExecuteSql(string sSql)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                OleDbCommand cmd = new OleDbCommand(sSql, con);
                cmd.CommandType = CommandType.Text;
                
                OleDbDataAdapter dtadapter = new OleDbDataAdapter(cmd);
                DataSet ds;
                ds = new DataSet();
                dtadapter.Fill(ds);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                insLogFile = new LogFile();
                _sError = ex.Message + ex.StackTrace;
                insLogFile.writeLog(strLogPath, _sError);
                insLogFile = null;
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public bool bExecuteQuery(string sSql)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                OleDbCommand cmd = new OleDbCommand(sSql, con);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                insLogFile = new LogFile();
                _sError = sSql + " IS FAILED.";
                insLogFile.writeLog(strLogPath, _sError);
                _sError += ex.Message + ex.StackTrace;
                insLogFile.writeLog(strLogPath, _sError);
                insLogFile = null;
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public bool bExecuteProcedure(string Procname)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                OleDbCommand cmd = new OleDbCommand(Procname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                insLogFile = new LogFile();
                _sError = Procname + " IS FAILED.";
                insLogFile.writeLog(strLogPath, _sError);
                _sError += ex.Message + ex.StackTrace;
                insLogFile.writeLog(strLogPath, _sError);
                insLogFile = null;
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }
        
        public DataTable dtExecuteProcedure(string Procname)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                OleDbCommand cmd = new OleDbCommand(Procname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.Add("@bIsAll", OleDbType.Integer).Value = 1;
                //cmd.Parameters.Add("@bIsAll", OleDbType.Integer).Value = 0;
                //cmd.ExecuteNonQuery();
                //return true;

                OleDbDataAdapter dtadapter = new OleDbDataAdapter(cmd);
                DataSet ds;
                ds = new DataSet();
                dtadapter.Fill(ds);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                insLogFile = new LogFile();
                _sError = Procname + " IS FAILED.";
                insLogFile.writeLog(strLogPath, _sError);
                _sError += ex.Message + ex.StackTrace;
                insLogFile.writeLog(strLogPath, _sError);
                insLogFile = null;
                //return false;
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        public bool bExecuteProceduress(string Procname, params OleDbParameter[] parameters)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                const int PARAMETER_NAME = 0;
                const int DIRECTION = 1;
                const int DB_TYPE = 2;
                const int VALUE = 3;
                int counterInt;
                OleDbParameter objParameter;
                OleDbCommand cmd = new OleDbCommand(Procname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (OleDbParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                cmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                insLogFile = new LogFile();
                _sError = Procname + " IS FAILED.";
                insLogFile.writeLog(strLogPath, _sError);
                _sError += ex.Message + ex.StackTrace;
                insLogFile.writeLog(strLogPath, _sError);
                insLogFile = null;
                return false;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }
        
        public DataTable dtExecuteProceduress(string Procname, params OleDbParameter[] parameters)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                const int PARAMETER_NAME = 0;
                const int DIRECTION = 1;
                const int DB_TYPE = 2;
                const int VALUE = 3;
                int counterInt;
                OleDbParameter objParameter;
                OleDbCommand cmd = new OleDbCommand(Procname, con);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (OleDbParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
                //cmd.ExecuteNonQuery();
                //return true;

                OleDbDataAdapter dtadapter = new OleDbDataAdapter(cmd);
                DataSet ds;
                ds = new DataSet();
                dtadapter.Fill(ds);
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                insLogFile = new LogFile();
                _sError = Procname + " IS FAILED.";
                insLogFile.writeLog(strLogPath, _sError);
                _sError += ex.Message + ex.StackTrace;
                insLogFile.writeLog(strLogPath, _sError);
                insLogFile = null;
                //return false;
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }

        private OleDbType GetOleDbType(Type sysType)
        {
            if (object.ReferenceEquals(sysType, typeof(string)))
            {
                return OleDbType.VarChar;
            }
            else if (object.ReferenceEquals(sysType, typeof(int)))
            {
                return OleDbType.Integer;
            }
            else if (object.ReferenceEquals(sysType, typeof(bool)))
            {
                return OleDbType.Boolean;
            }
            else if (object.ReferenceEquals(sysType, typeof(System.DateTime)))
            {
                return OleDbType.Date;
            }
            else if (object.ReferenceEquals(sysType, typeof(char)))
            {
                return OleDbType.Char;
            }
            else if (object.ReferenceEquals(sysType, typeof(decimal)))
            {
                return OleDbType.Decimal;
            }
            else if (object.ReferenceEquals(sysType, typeof(double)))
            {
                return OleDbType.Double;
            }
            else if (object.ReferenceEquals(sysType, typeof(float)))
            {
                return OleDbType.Single;
            }
            else if (object.ReferenceEquals(sysType, typeof(byte[])))
            {
                return OleDbType.Binary;
            }
            else if (object.ReferenceEquals(sysType, typeof(Guid)))
            {
                return OleDbType.Guid;
            }
            else
            {
                return OleDbType.Variant;
            }
        }
    }
}
