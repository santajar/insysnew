﻿CREATE PROCEDURE [dbo].[spEmsMsGetDefaultAttribute]
	@param1 int = 0,
	@param2 int = 0
AS
SELECT '2.00' [Version]
	, 'ADD_UPDATE' [Type]
	, 'UPLOAD' [Sender]
	, GETDATE() [Sender_ID]
