USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileTerminalCopy]    Script Date: 11/2/2017 4:44:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 24, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Copying profile content of terminal table
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalCopy]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
BEGIN

declare @cols nvarchar (MAX)
declare @query nvarchar (MAX)

SET @cols =STUFF((SELECT ',' + QUOTENAME(COLUMN_NAME) 
                    from [NewInSys_BCA-DEV].INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_NAME = N'tbProfileTerminal' and COLUMN_NAME not like ('T%')
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

set @query='
	INSERT INTO tbProfileTerminal(
		TerminalId,'+ @cols +'
		)
	SELECT '''+ @sNewTID +''','+ @cols +' 
	FROM tbProfileTerminal (NOLOCK)
	WHERE TerminalId='''+ @sOldTID +''''
	
exec (@query)

	-- TerminalName
	IF (dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sNewTID))=5)
	BEGIN
	UPDATE tbProfileTerminal
	SET DE001=@sNewTID
	WHERE TerminalId=@sNewTID
		--AND TerminalTag='DE001'
	END
	ELSE
	BEGIN
	UPDATE tbProfileTerminal
	SET DE01=@sNewTID
	WHERE TerminalId=@sNewTID
		--AND TerminalTag='DE01'
	END
END

GO


