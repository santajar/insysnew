﻿

-- ============================================================
-- Description:	 Insert new Remote Download Name into TbProfileRemoteDownload with different name
-- Ibnu Saefullah
-- Create : 23 Mei 2017, Copy Remote Download Name
-- ============================================================

CREATE PROCEDURE [dbo].[spProfileRemoteDownloadManagementCopy]
	@iDatabaseID VARCHAR(3),
	@sOldRDName VARCHAR(50),
	@sNewRDName VARCHAR(50)

AS
Declare
	@Content VARCHAR(MAX)=NULL,
	@iCount INT,
	@iIndex INT

SELECT @iCount= COUNT(*)
FROM tbProfileRemoteDownload WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID
	AND RemoteDownloadName = @sNewRDName


IF @iCount = 0
BEGIN
	INSERT INTO tbProfileRemoteDownload(RemoteDownloadName, DatabaseID, RemoteDownloadTag ,RemoteDownloadLengthOfTagLength, RemoteDownloadTagLength, RemoteDownloadTagValue)
	SELECT @sNewRDName, DatabaseID, RemoteDownloadTag ,RemoteDownloadLengthOfTagLength, RemoteDownloadTagLength, RemoteDownloadTagValue
	FROM tbProfileRemoteDownload WITH (NOLOCK)
	WHERE RemoteDownloadName = @sOldRDName AND DatabaseId=@iDatabaseID
	EXEC spItemComboBoxValueInsertRemoteDownload @sNewRDName, @iDatabaseID

	SET @iIndex=CHARINDEX('-',@sOldRDName)
	 
	IF (@iIndex = 0)
	BEGIN
		 UPDATE tbProfileRemoteDownload
		 SET RemoteDownloadTagValue = @sNewRDName, RemoteDownloadTagLength = LEN(@sNewRDName)
		 WHERE RemoteDownloadTag IN ('RD001','RD01') AND RemoteDownloadName = @sNewRDName
	END
	ELSE
	BEGIN
		 UPDATE tbProfileRemoteDownload
		 SET RemoteDownloadTagValue = SUBSTRING(@sNewRDName,0,@iIndex-1), RemoteDownloadTagLength = LEN(SUBSTRING(@sNewRDName,0,@iIndex-1))
		 WHERE RemoteDownloadTag IN ('RD001','RD01') AND RemoteDownloadName = @sNewRDName
	END
	 
END
ELSE
	SELECT 'Remote Download Name management Index Already Exist.'

