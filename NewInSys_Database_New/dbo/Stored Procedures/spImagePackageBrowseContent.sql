﻿CREATE PROCEDURE [dbo].[spImagePackageBrowseContent]
	@sFileName VARCHAR(50),
	@sType VARCHAR(10)
AS

SELECT A.ImageID,LogoType,[FileName],ID,ImageContent,UploadTime
FROM tbProfileImage A
JOIN tbProfileImageTemp B
ON A.ImageID=B.ImageID
WHERE [FileName] = @sFileName
	AND LogoType = @sType
ORDER BY A.ImageID,ID