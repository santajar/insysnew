﻿CREATE PROCEDURE [dbo].[spProfileSNInsert]
	@iDatabaseID INT,
	@sName VARCHAR(15),
	@sContent VARCHAR(500)
AS
SET NOCOUNT ON
DECLARE @iCount INT
SELECT @iCount=COUNT(*) FROM tbProfileSN WHERE DatabaseID=@iDatabaseID AND SN_Name=@sName

SET @iCount = @@ROWCOUNT

IF @iCount>0
	EXEC spProfileSNDelete @iDatabaseID, @sName

INSERT INTO tbProfileSN(
	DatabaseId,
	SN_Name,
	SN_Tag,
	SN_TagValue)
SELECT @iDatabaseID DatabaseId,
	[Name],
	Tag,
	TagValue
FROM dbo.fn_tbProfileTLV5(@sName,@sContent)

UPDATE tbProfileTerminalList
SET AllowDownload=1
WHERE TerminalID IN (
	SELECT TerminalID 
	FROM tbProfileTerminal WITH(NOLOCK)
	WHERE TerminalTag IN (
		SELECT Tag
		FROM dbo.tbItemList WITH(NOLOCK)
		WHERE DatabaseID=@iDatabaseID AND ItemName LIKE 'Remote Download Name')
	AND TerminalTagValue=@sName)
