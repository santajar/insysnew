﻿CREATE Procedure [dbo].[spUploadTagTemplate]
as
begin
	declare @iCountTemplateAA int, @iCountTemplateDE int
	select  @iCountTemplateAA=count(distinct Template) from tbUploadTagAA WITH (NOLOCK)
	select  @iCountTemplateDE=count(distinct Template) from tbUploadTagDE WITH (NOLOCK)

	if @iCountTemplateAA>@iCountTemplateDE
	 select Distinct Template from tbUploadTagAA WITH (NOLOCK)
	else
	select Distinct Template from tbUploadTagDE WITH (NOLOCK)

end