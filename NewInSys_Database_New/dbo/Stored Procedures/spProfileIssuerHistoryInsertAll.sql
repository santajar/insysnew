﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Modify date: 
-- Description:	
--				1. INSERT The History Issuer table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileIssuerHistoryInsertAll]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO tbProfileIssuerHistory(TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue,LastUpdate)
	SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue,CONVERT(VARCHAR(20),GETDATE(),113)
	FROM tbProfileIssuer WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
END