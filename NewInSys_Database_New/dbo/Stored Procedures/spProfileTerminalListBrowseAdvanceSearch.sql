﻿-- =============================================
-- Author:		IBNU SAEFULLAH
-- Create date: Jan 20, 2015
-- Modify date: 
-- Description:
-- =============================================




CREATE PROCEDURE [dbo].[spProfileTerminalListBrowseAdvanceSearch]
	@sParameter1 VARCHAR(MAX)=NULL, @sValue1 VARCHAR(MAX)= NULL,  @sFormId1 varchar(max),@sFormId2 varchar(max),
	@sParameter2 Varchar(MAX)=NULL, @sValue2 VARCHAR(MAX)=NULL, @sDbId varchar(max)

AS
DECLARE 
	@sTerminal varchar(MAX),
	@sAcquirer varchar (MAX),
	@sIssuer varchar (max)

--------------------------------(TbProfileterminal)--------------------------------------------

IF @sFormId1 = 1  AND @sFormId2 = 1
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileTerminal ON tbProfileTerminal.TerminalID = tbProfileTerminalList.TerminalID 
WHERE 
tbProfileTerminal.TerminalTag LIKE @sParameter1+'%' AND tbProfileTerminal.TerminalTagValue LIKE @sValue1+'%' 
AND tbProfileTerminalList.DatabaseID=@sDbId OR
tbProfileTerminal.TerminalTag LIKE @sParameter2+'%' AND tbProfileTerminal.TerminalTagValue LIKE @sValue2+'%' 
AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE


IF  @sFormId1 =1 AND @sFormId2=2
SELECT distinct tbProfileTerminalList.TerminalID, tbprofileterminal.TerminalID, tbprofileAcquirer.TerminalID  FROM tbProfileTerminalList 

INNER JOIN tbProfileAcquirer ON tbProfileAcquirer.TerminalID =  tbProfileTerminalList.TerminalID 
INNER JOIN tbProfileTerminal ON tbProfileTerminal.TerminalID  = tbProfileTerminalList.TerminalID 

WHERE 
tbProfileTerminal.TerminalTag LIKE @sParameter1+'%' AND tbProfileTerminal.TerminalTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId OR
tbProfileAcquirer.AcquirerTag LIKE @sParameter2+'%' AND tbProfileAcquirer.AcquirerTagValue LIKE @sValue2+'%'AND tbProfileTerminalList.DatabaseID=@sDbId

ELSE

IF @sFormId1 = 1 AND @sFormId2 = 3
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileTerminal ON tbProfileTerminal.TerminalID = tbProfileTerminalList.TerminalID
INNER JOIN tbProfileIssuer ON tbProfileIssuer.TerminalID = tbProfileTerminalList.TerminalID
WHERE  
tbProfileTerminal.TerminalTag LIKE @sParameter1+'%' AND tbProfileTerminal.TerminalTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId OR 
tbProfileIssuer.IssuerTag LIKE @sParameter2+'%' AND tbProfileIssuer.IssuerTagValue LIKE @sValue2+'%' AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE

IF @sFormId1 = 1  
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileTerminal ON tbProfileTerminal.TerminalID = tbProfileTerminalList.TerminalID 
WHERE tbProfileTerminal.TerminalTag LIKE @sParameter1+'%' AND tbProfileTerminal.TerminalTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE

--------------------------------(TbProfileAcquirer)--------------------------------------------

IF @sFormId1 = 2 AND @sFormid2 = 2
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileAcquirer ON tbProfileAcquirer.TerminalID = tbProfileTerminalList.TerminalID
WHERE tbProfileAcquirer.AcquirerTag LIKE @sParameter1+'%' AND tbProfileAcquirer.AcquirerTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId OR
tbProfileAcquirer.AcquirerTag LIKE @sParameter2+'%' AND tbProfileAcquirer.AcquirerTagValue LIKE @sValue2+'%'AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE


IF @sFormId1 = 2 AND @sFormId2 = 1
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileAcquirer ON tbProfileAcquirer.TerminalID = tbProfileTerminalList.TerminalID
INNER JOIN tbProfileTerminal ON tbProfileTerminal.TerminalID = tbProfileTerminalList.TerminalID
WHERE
tbProfileAcquirer.AcquirerTag LIKE @sParameter1+'%' AND tbProfileAcquirer.AcquirerTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId OR 
tbProfileTerminal.TerminalTag LIKE @sParameter2+'%' AND tbProfileTerminal.TerminalTagValue LIKE @sValue2+'%' AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE


IF @sFormId1 = 2 AND @sFormId2 = 3
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileAcquirer ON tbProfileAcquirer.TerminalID = tbProfileTerminalList.TerminalID
INNER JOIN tbProfileIssuer ON tbProfileIssuer.TerminalID = tbProfileTerminalList.TerminalID
WHERE
tbProfileAcquirer.AcquirerTag LIKE @sParameter1+'%' AND tbProfileAcquirer.AcquirerTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId OR 
tbProfileIssuer.IssuerTag LIKE @sParameter2+'%' AND tbProfileIssuer.IssuerTagValue LIKE @sValue2+'%' AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE


IF @sFormId1 = 2
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileAcquirer ON tbProfileAcquirer.TerminalID = tbProfileTerminalList.TerminalID
WHERE 
tbProfileAcquirer.AcquirerTag LIKE @sParameter1+'%' AND tbProfileAcquirer.AcquirerTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId
OR
tbProfileAcquirer.AcquirerTag LIKE @sParameter2+'%' AND tbProfileAcquirer.AcquirerTagValue LIKE @sValue2+'%' AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE

-----------------------------------------------------tbprofileissuer--------------------------------------------------
IF @sFormId1 = 3  AND @sFormId2=3
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileIssuer ON tbProfileIssuer.TerminalID = tbProfileTerminalList.TerminalID
WHERE 
tbProfileIssuer.IssuerTag LIKE @sParameter1+'%'  AND tbProfileIssuer.IssuerTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId OR
tbProfileIssuer.IssuerTag LIKE @sParameter2+'%'  AND tbProfileIssuer.IssuerTagValue LIKE @sValue2+'%' 
AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE



IF @sFormId1 = 3 AND @sFormId2 = 1 
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileIssuer ON tbProfileIssuer.TerminalID = tbProfileTerminalList.TerminalID
INNER JOIN tbProfileTerminal ON tbProfileTerminal.TerminalID = tbProfileTerminalList.TerminalID
WHERE 
tbProfileIssuer.IssuerTag LIKE @sParameter1+'%'  AND tbProfileIssuer.IssuerTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId OR 
tbProfileTerminal.TerminalTag LIKE @sParameter2+'%' AND tbProfileTerminal.TerminalTagValue LIKE @sValue2+'%' AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE


IF @sFormId1 = 3 AND @sFormId2 = 2 
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileIssuer ON tbProfileIssuer.TerminalID = tbProfileTerminalList.TerminalID
INNER JOIN tbProfileAcquirer ON tbProfileAcquirer.TerminalID = tbProfileTerminalList.TerminalID
WHERE 
tbProfileIssuer.IssuerTag LIKE @sParameter1+'%'  AND tbProfileIssuer.IssuerTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId OR 
tbProfileAcquirer.AcquirerTag LIKE @sParameter2+'%' AND tbProfileAcquirer.AcquirerTagValue LIKE @sValue2+'%' AND tbProfileTerminalList.DatabaseID=@sDbId
ELSE


IF @sFormId1 = 3 
SELECT distinct tbProfileTerminalList.TerminalID  FROM tbProfileTerminalList 
INNER JOIN tbProfileIssuer ON tbProfileIssuer.TerminalID = tbProfileTerminalList.TerminalID
WHERE tbProfileIssuer.IssuerTag LIKE @sParameter1+'%'  AND tbProfileIssuer.IssuerTagValue LIKE @sValue1+'%' AND tbProfileTerminalList.DatabaseID=@sDbId