﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 8, 2010
-- Modify date:
--				1. 
-- Description:	Delete from tbUserAccess based on UserId and TerminalId
-- =============================================
CREATE PROCEDURE [dbo].[spUserAccessDelete]
	@sTerminalID VARCHAR(8)=NULL,
	@sUserId VARCHAR(10)
AS
IF ISNULL(@sTerminalID,'') <> ''
	DELETE FROM	tbUserAccess
	WHERE TerminalId=@sTerminalID 
		AND UserId=@sUserId
ELSE
	DELETE FROM	tbUserAccess
	WHERE UserId=@sUserId