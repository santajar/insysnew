﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <30 Maret 2016>
-- Description:	<Add Card Relation to all TID in Database>
-- =============================================
CREATE PROCEDURE spProfileCardRelationAddbyIssuerAndDatabaseID
	@sDatabaseID VARCHAR(3),
	@sIssuerName VARCHAR(50),
	@sCardName VARCHAR(50),
	@sAcquirerName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
    
	DECLARE @sAcqTag VARCHAR(5),
			@sCardTag VARCHAR(5),
			@sIssTag VARCHAR(5)

	IF OBJECT_ID('Tempdb..#TempTID') IS NOT NULL
		DROP TABLE #TempTID
	SELECT TerminalID
	INTO #TempTID
	FROM tbProfileTerminalList
	WHERE DatabaseID = @sDatabaseID
		AND TerminalID IN 
			(SELECT TerminalID
			FROM tbProfileIssuer WITH (NOLOCK)
			WHERE IssuerName = @sIssuerName
				AND TerminalID IN 
					(SELECT TerminalID
					FROM tbProfileTerminalList WITH (NOLOCK)
					WHERE DatabaseID = @sDatabaseID)
			)
		AND TerminalID IN 
			(SELECT TerminalID
			FROM tbProfileAcquirer WITH (NOLOCK)
			WHERE AcquirerName = @sAcquirerName
				AND TerminalID IN 
					(SELECT TerminalID
					FROM tbProfileTerminalList WITH (NOLOCK)
					WHERE DatabaseID = @sDatabaseID)
			)

	SELECT TOP 1 @sIssTag = RelationTag
	FROM tbProfileRelation WITH (NOLOCK)
	WHERE TerminalID IN (SELECT TerminalID FROM #TempTID)
		AND RelationTag IN ('AD02','AD002')
		AND RelationTagValue = @sIssuerName
	
	IF (LEN(@sIssTag)=4)
	BEGIN
		SET @sCardTag = 'AD01'
		SET @sAcqTag = 'AD03'
	END
	ELSE
	BEGIN
		SET @sCardTag = 'AD001'
		SET @sAcqTag = 'AD003'
	END

	IF OBJECT_ID('Tempdb..#TempRelationInsert') IS NOT NULL
		DROP TABLE #TempRelationInsert
	CREATE TABLE #TempRelationInsert
	(
		RelationTag VARCHAR(5),
		RelationLengthOfTagLength INT,
		RelationTagLength INT,
		RelationTagValue VARCHAR(100)
	)

	INSERT INTO #TempRelationInsert(RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue) VALUES (@sCardTag,LEN(LEN(@sCardName)),LEN(@sCardName),@sCardName)--NAMA CARD BARU
	INSERT INTO #TempRelationInsert(RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue) VALUES (@sIssTag,LEN(LEN(@sIssuerName)),LEN(@sIssuerName),@sIssuerName)--NAMA ISSUER
	INSERT INTO #TempRelationInsert(RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue) VALUES (@sAcqTag,LEN(LEN(@sAcquirerName)),LEN(@sAcquirerName),@sAcquirerName)--NAMA ACQUIRER

	INSERT INTO tbProfileRelation (TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue)
	SELECT TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue
	FROM #TempTID A
	CROSS JOIN
	#TempRelationInsert B
	ORDER BY TerminalID,RelationTag

	DROP TABLE #TempRelationInsert
	DROP TABLE #TempTID
END