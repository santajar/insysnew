﻿
-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <20 Okt 2015>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[spSoftwareGroupPackageDelete]
	@sGroupName VARCHAR(100)= NULL
AS
BEGIN
	DELETE FROM tbSoftwareGroupPackage
	WHERE GroupName = @sGroupName
END