﻿


-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: may 30, 2017
-- Modify date:
--				1. 
-- Description:	Browse into tbSoftwareProgressCPL
-- =============================================

CREATE procedure [dbo].[spSoftwareProgressBrowseCPLDetail]
@iID INT 
AS
--DECLARE @sQuery VARCHAR(MAX) = 'SELECT
--	ID,
--	GroupName [Group],
--	TerminalID,
--	Software,
--	AppPackageName,
--	SerialNumber,
--	StartTime,
--	EndTime,
--	Percentage,
--	FinishInstallTime,
--	InstallStatus
--	FROM tbSoftwareProgressCPLDetail WITH (NOLOCK) '
--IF @sCondition IS NOT NULL
--EXEC (@sQuery + @sCondition+ ' order by id desc')
--ELSE
--EXEC (@sQuery + ' order by id desc')

SELECT * FROM tbSoftwareProgressCPLDetail
WHERE ID = @iID