﻿
CREATE PROCEDURE [dbo].[spProfileGPRSBrowse]
	@sCondition VARCHAR(100) = NULL
AS
	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt = 'SELECT GPRSTagID,
						GPRSName, 
						DatabaseID,
						GPRSTag as Tag,
						GPRSLengthOfTagLength,
						GPRSTagLength,
						GPRSTagValue as Value,
						[Description]
				FROM tbProfileGPRS WITH (NOLOCK) '
	SET @sStmt = @sStmt + ISNULL(@sCondition,'') + ' ORDER BY GPRSName, GPRSTag'
	EXEC (@sStmt)

