﻿
-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Jan 22, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spLogDataView]
	--@sDateFrom NVARCHAR(20),
	--@sDateTo NVARCHAR(20),
	@sDateFrom NVARCHAR(200),
	@sDateTo NVARCHAR(200),
	@sConditions VARCHAR(30),
	@sParameters VARCHAR(30),
	@sFlag VARCHAR(10)
	 
AS
BEGIN
DECLARE @Query NVARCHAR(max)
	SET NOCOUNT ON;
	IF @sFlag = 'CRITERIA'
	BEGIN
	SET @Query = 'SELECT * From tbProfileVerification Where  '+   @sConditions + ' LIKE ''%' +  @sParameters + '%''' 
 --print convert(varchar(max),@Query)
	END
	IF @sFlag = 'DATE'
	BEGIN
	SET @Query = 'SELECT * From tbProfileVerification Where LogDate BETWEEN CAST' + '('''+@sDateFrom +''' AS DATETIME)' + 'AND CAST' + '(''' +@sDateTo +''' AS DATETIME)'
	END
	IF @sFlag = 'ALL'
	BEGIN
  SET @Query = 'SELECT * From tbProfileVerification Where  '+   @sConditions + ' LIKE ''%' +  @sParameters + '%''' + 'AND LogDate BETWEEN CAST' + '('''+@sDateFrom +''' AS DATE)' + 'AND CAST' + '(''' +@sDateTo +''' AS DATE)'
	END
	--print convert(varchar(max),@Query)
	EXECUTE sp_executesql @Query
END