﻿

-- =============================================
-- Author:		SUPRIYADI SETYO
-- Create date: Agt 19, 2013
-- Modify date:
--				1. 
-- Description:	Insert into tbAuditInit
-- =============================================
CREATE PROCEDURE [dbo].[spAuditInitSoftwareInsert]
	@sTerminalID VARCHAR(8)=NULL,
	@sSoftware VARCHAR(25),
	@SoftwareDownload VARCHAR(25),
	@sSerialNum VARCHAR(20),
	@iIndex INT
AS
	SET NOCOUNT ON
INSERT INTO	tbAuditInitSoftware(
	TerminalId,
	Software,
	SoftwareDownload,
	SerialNumber,
	InitTime,
	IndexSoftware)
VALUES(
	@sTerminalID,
	@sSoftware,
	@SoftwareDownload,
	@sSerialNum,
	CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
	@iIndex)