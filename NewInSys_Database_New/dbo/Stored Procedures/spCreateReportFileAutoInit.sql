﻿





-- ==============================================================
-- Description:	Generate Report File contains data from AutoInit
-- ==============================================================

CREATE PROCEDURE [dbo].[spCreateReportFileAutoInit] 

AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Path VARCHAR(100),	
			@HeaderName VARCHAR(20),		
			@TmsName VARCHAR(20),
			@GetDate VARCHAR(20),
			@GetTime VARCHAR(20),
			@FullDate VARCHAR(20),
			@TypeFile VARCHAR(10),
			@ServerName VARCHAR(30)
		
	SELECT @Path = Flag 
	FROM tbControlFlag
	WHERE ItemName = 'Report AutoInit Path'
	
	SET @Path = @Path + '\' 	
	SET @HeaderName = '\EDCINFO'
	SET @TmsName = 'INSYS'
	SET @GetDate = CONVERT(VARCHAR,GETDATE(),12)
	SET @GetTime = REPLACE(CONVERT (VARCHAR,GETDATE(),108), ':', '') 
	SET @FullDate = @GetDate + @GetTime
	SET @TypeFile = '.txt'
	
	--Select Statement from tbAutoInitLog
	DECLARE @Query VARCHAR(MAX)
	SET @Query = '"SELECT CASE REPLACE(TerminalID,'' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(TerminalID, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(LastInitTime, '' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(LastInitTime, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(PABX, '' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(PABX, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(SoftwareVer, '' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(SoftwareVer, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(OSVer, '' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(OSVer, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(KernelVer, '' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(KernelVer, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(EDCSN, '' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(EDCSN, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(ReaderSN, '' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(ReaderSN, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(PSAMSN, '' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(PSAMSN, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(MCSN, '' '', '''') WHEN '' '' THEN NULL ELSE REPLACE(MCSN, '' '', '''') END, '
	SET @Query = @Query + 'CASE REPLACE(MemUsage, '' '', '''') WHEN '' '' THEN NULL ELSE  REPLACE(MemUsage, '' '', '''') END ' 
	SET @Query = @Query + 'FROM NewInSys.dbo.tbAutoInitLog"'
	print @Query 
	
	DECLARE @FileName VARCHAR(MAX)
	SET @FileName = @Path + @HeaderName + '-' + @TmsName + '-' + @FullDate + @TypeFile
	print @FileName 

	--Command to create Report File	
	DECLARE @bcpCommand VARCHAR(8000)				
	SET @bcpCommand = 'bcp '
	SET @bcpCommand = @bcpCommand + @Query
	SET @bcpCommand = @bcpCommand + ' queryout '
	SET @bcpCommand = @bcpCommand + @FileName 
	SET @bcpCommand = @bcpCommand + ' -S ' 
	--SET @bcpCommand = @bcpCommand + @@SERVERNAME 
	SET @bcpCommand = @bcpCommand + 'P300AP16'
	SET @bcpCommand = @bcpCommand + ' -T -c -t ";" '

	print @bcpCommand 
	EXEC master..xp_cmdshell @bcpCommand

	--Kalau table perlu diclear, jalankan perintah berikut
	--DELETE FROM tbAutoInitLog
END








