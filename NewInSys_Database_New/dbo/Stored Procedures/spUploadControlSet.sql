﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Set the starting row process of the defined template
-- =============================================
CREATE PROCEDURE [dbo].[spUploadControlSet]
	@iRow SMALLINT
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE tbControlFlag
	SET Flag=@iRow
	WHERE ItemName='StartingRow'
END
