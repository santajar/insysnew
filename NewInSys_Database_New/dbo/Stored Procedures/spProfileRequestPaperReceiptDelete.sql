﻿CREATE PROCEDURE [dbo].[spProfileRequestPaperReceiptDelete]
	@iDatabaseId INT,
	@sRequestPaperReceiptName VARCHAR(50),
	@bDeleteData BIT = 1
AS
	DECLARE @sRequestPaperReceiptID VARCHAR(20)
	SELECT @sRequestPaperReceiptID = ReqTagValue	
	FROM tbProfileReqPaperReceiptManagement WITH (NOLOCK)
	WHERE ReqName = @sRequestPaperReceiptName AND 
		  DatabaseID = @iDatabaseID AND 
		  ReqTag IN ('RQ01','RQ001')

	DECLARE @iCount INT =0
	SELECT @iCount = COUNT(*)
	FROM tbProfileTerminal
	WHERE TerminalID IN (SELECT TerminalID FROM tbProfileTerminalList WITH (NOLOCK) WHERE DatabaseID=@iDatabaseId)
		AND (TerminalTag IN 
				(SELECT TAG
				FROM tbItemList WITH (NOLOCK)
				WHERE DatabaseID = @iDatabaseId
					AND ItemName = 'Request Paper Receipt')) 
		AND TerminalTagValue = @sRequestPaperReceiptID
		and TerminalID in (select TerminalID from tbProfileTerminalList WITH (NOLOCK) where DatabaseID = @iDatabaseId)

	IF @bDeleteData = 1 AND @iCount > 0
		SELECT 'IP Primary still used in one or more terminals.'
	ELSE
	BEGIN
		DELETE FROM tbProfileReqPaperReceiptManagement
		WHERE DatabaseID = @iDatabaseId AND
			  ReqName = @sRequestPaperReceiptName

		EXEC spItemComboBoxValueInsertRequestPaperReceipt @sRequestPaperReceiptName, @iDatabaseId

		DECLARE @sDatabaseID VARCHAR(3)
		SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
		EXEC spProfileTerminalUpdateLastUpdateByTagAndDbID @sDatabaseID,'Request Paper Receipt',@sRequestPaperReceiptName
	END