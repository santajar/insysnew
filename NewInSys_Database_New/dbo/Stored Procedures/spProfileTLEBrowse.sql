﻿

CREATE PROCEDURE [dbo].[spProfileTLEBrowse]
	@sCondition VARCHAR(100) = NULL
--	@sDatabaseID VARCHAR(2),
--	@sTLEID VARCHAR(10)
AS
	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt = 'SELECT TLETagID,
						TLEName, 
						DatabaseID,
						TLETag as Tag,
						TLELengthOfTagLength,
						TLETagLength,
						TLETagValue as Value,
						[Description]
				FROM tbProfileTLE WITH (NOLOCK) '
	SET @sStmt = @sStmt + ISNULL(@sCondition,'') + ' ORDER BY DatabaseID, TLEName, Tag'
	EXEC (@sStmt)
--	WHERE DatabaseID = @sDatabaseID AND TLEID = @sTLEID






