﻿
CREATE PROCEDURE [dbo].[spUserGroupBrowse]
	@sCondition VARCHAR(MAX) = ''
AS
	DECLARE @sCommand VARCHAR(MAX)
	SET @sCommand = 'SELECT GroupID, Tag
					 FROM tbUserGroup WITH (NOLOCK) '
	SET @sCommand = @sCommand + @sCondition
	EXEC (@sCommand)

