﻿
CREATE PROCEDURE [dbo].[spProfileGPRSDelete]
	@iDatabaseId INT,
	@sGPRSName VARCHAR(50),
	@bDeleteData BIT = 1
AS
	DECLARE @sGPRSID VARCHAR(15)
	DECLARE @iCount INT

	IF( DBO.isTagLength4(@iDatabaseId) = 5 )
	BEGIN
		SELECT @sGPRSID = GPRSTagValue	
		FROM tbProfileGPRS WITH (NOLOCK)
		WHERE GPRSName = @sGPRSName AND 
			  DatabaseID = @iDatabaseID AND 
			  GPRSTag LIKE 'GP001'
	
		SELECT @iCount = COUNT(*)
		FROM tbProfileAcquirer WITH (NOLOCK)
		WHERE (AcquirerTag IN ('AA037','AA038','AA039','AA040','AA045','AA046','AA047','AA048') ) AND 
			  AcquirerTagValue = @sGPRSID
			  AND TerminalID IN 
				(SELECT TerminalID 
				FROM tbProfileTerminalList WITH (NOLOCK)
				WHERE DatabaseID = @iDatabaseId)
	END
	ELSE
	BEGIN
		SELECT @sGPRSID = GPRSTagValue	
		FROM tbProfileGPRS WITH (NOLOCK)
		WHERE GPRSName = @sGPRSName AND 
			  DatabaseID = @iDatabaseID AND 
			  GPRSTag LIKE 'GP01'
	
		SELECT @iCount = COUNT(*)
		FROM tbProfileAcquirer WITH (NOLOCK)
		WHERE (AcquirerTag IN ('AA37','AA38','AA39','AA40','AA45','AA46','AA47','AA48')) AND 
			  AcquirerTagValue = @sGPRSID
			  AND TerminalID IN 
				(SELECT TerminalID 
				FROM tbProfileTerminalList WITH (NOLOCK)
				WHERE DatabaseID = @iDatabaseId)
	END

	PRINT @iCount
	IF @bDeleteData = 1 AND @iCount > 0
		SELECT 'IP Primary still used in one or more terminals.'
	ELSE
	BEGIN
		DELETE FROM tbProfileGPRS
		WHERE DatabaseID = @iDatabaseId AND
			  GPRSName = @sGPRSName

		EXEC spItemComboBoxValueDeleteGPRS @sGPRSName, @iDatabaseId

		DECLARE @sDatabaseID VARCHAR(3)
		SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
		EXEC spProfileTerminalUpdateLastUpdateByDbID @sDatabaseID
	END