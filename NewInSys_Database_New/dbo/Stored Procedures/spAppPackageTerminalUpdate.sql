﻿/*Altered by chris 27 july 2012
	@sTerminalID CHAR(8) (fixed lenght)
	@sAppPackIDList VARCHAR(150) according to tbProfileTerminalList
*/
CREATE PROCEDURE [dbo].[spAppPackageTerminalUpdate]
	@sTerminalID CHAR(8),
	@sAppPackIDList VARCHAR(MAX)
AS
	SET NOCOUNT ON
	UPDATE tbProfileTerminalList
	  SET AppPackId=@sAppPackIDList
	WHERE TerminalID=@sTerminalID