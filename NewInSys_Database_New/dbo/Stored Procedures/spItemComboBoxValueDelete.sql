﻿
-- ======================================================================
-- Description:	Delete data from ItemComboBoxValue based from given value
-- ======================================================================

CREATE PROCEDURE [dbo].[spItemComboBoxValueDelete]
	@sDatabaseID SMALLINT,
	@sFormID SMALLINT,
	@iItemSequence	SMALLINT,
	@sDisplayValue VARCHAR(40),
	@sRealValue VARCHAR(20)

AS
	DELETE FROM tbItemComboBoxValue
	WHERE DatabaseID = @sDatabaseID AND
		  FormID = @sFormID AND
		  ItemSequence = @iItemSequence AND
		  DisplayValue = @sDisplayValue AND
		  RealValue = @sRealValue

