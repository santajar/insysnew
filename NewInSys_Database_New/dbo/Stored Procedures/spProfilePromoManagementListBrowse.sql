﻿
-- =============================================  
-- Author: Ibnu Saefullah
-- Create date: feb 18, 2016  
-- Description: view Promo Management List 
-- =============================================  
CREATE PROCEDURE [dbo].[spProfilePromoManagementListBrowse]  
 @sDatabaseId VARCHAR(3) = ''  
AS  
SELECT PromoManagementID, PromoManagementName  
FROM dbo.fn_viewPromoManagementId()
WHERE
DatabaseID = @sDatabaseId;