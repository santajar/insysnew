﻿
-- ============================================================
-- Description:	 Insert new AID into TbProfileAid with different name
-- Ibnu Saefullah
-- ============================================================

CREATE PROCEDURE [dbo].[spProfileAIDCopy]
	@iDatabaseID VARCHAR(3),
	@sOldAIDName VARCHAR(50),
	@sNewAIDName VARCHAR(50)

AS
Declare
	@Content VARCHAR(MAX)=NULL,
	@iCount INT

SELECT @iCount= COUNT(*)
FROM tbProfileAID WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID
	AND AIDName = @sNewAIDName


IF @iCount = 0
BEGIN
	INSERT INTO tbProfileAID(DatabaseId, AIDName, AIDTag ,AIDLengthOfTagLength, AIDTagLength, AIDTagValue)
	SELECT DatabaseId, @sNewAIDName, AIDTag, AIDLengthOfTagLength, AIDTagLength, AIDTagValue
	FROM tbProfileAID WITH (NOLOCK)
	WHERE AIDName = @sOldAIDName AND DatabaseId=@iDatabaseID
	 
	 UPDATE tbProfileAID
	 SET AIDTagValue = @sNewAIDName, AIDTagLength = LEN(@sNewAIDName)
	 WHERE AIDTag IN ('AI001','AI01') AND AIDName = @sNewAIDName
END