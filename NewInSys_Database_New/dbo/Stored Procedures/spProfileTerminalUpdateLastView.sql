﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 29, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Update the lastview column of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalUpdateLastView]
@sTerminalID VARCHAR(8)
AS
	UPDATE tbProfileTerminalList SET LastView = GETDATE() 
	WHERE TerminalID = @sTerminalID
