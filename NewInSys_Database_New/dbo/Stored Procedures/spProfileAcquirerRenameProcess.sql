﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 20, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. the process of renaming an acquirer on a profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerRenameProcess]
	@sTerminalID VARCHAR(8),
	@sOldKey VARCHAR(15),
	@sNewKey VARCHAR(15)
AS
BEGIN
	IF (SELECT COUNT(*) 
		FROM tbProfileRelation WITH (NOLOCK) 
		WHERE TerminalID = @sTerminalID 
			AND RelationTag IN ('AD03','AD003') 
			AND RelationTagValue = @sNewKey) = 0
	BEGIN
		EXEC spProfileRelationRenameAcquirer @sTerminalID, @sOldKey, @sNewKey
		EXEC spProfileAcquirerRename @sTerminalID, @sOldKey, @sNewKey
	END
END
