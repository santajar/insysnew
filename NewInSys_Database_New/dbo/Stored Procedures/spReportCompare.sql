﻿


CREATE PROCEDURE [dbo].[spReportCompare]
--	@sPath VARCHAR(300) = NULL
AS
DECLARE @sSQL VARCHAR(1000)

--SET @sPath = 'D:\Documents\BCA\New Insys\Program untuk QC\FDM BCAOKT2012_EMS.xls'
--SET @sSQL = 'SELECT [Product ID] TerminalID, [Merchant ID] MID, [Merchant Name] [MerchantName], [Alamat 1] Alamat1,  
--			[Alamat 2] Alamat2, [Flazz], [TopUp], Payment, [BCA Card] BCA_Card,
--			Visa, Master, Amex, Diners, Cicilan, [Cicilan Prog] Cicilan_Prog, Debit, Tunai
--			INTO TempXLS
--			FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'', ''Excel 8.0;Database=' + @sPath + ''', 
--							''SELECT [Product ID], [Merchant ID], [Merchant Name], [Alamat 1], 
--									[Alamat 2], [Flazz], [TopUp], Payment, [BCA Card],
--									Visa, Master, Amex, Diners, Cicilan, [Cicilan Prog], Debit, Tunai
--							 FROM [QC$]''
--							)'
--PRINT @sSQL

--IF (OBJECT_ID('NewInsys.dbo.TempXLS') IS NOT NULL)
--	DROP TABLE TempXLS


DECLARE @iRowCount INT
--
--SELECT [Product ID] TerminalID, [Merchant ID] MID, [Merchant Name] [MerchantName], [Alamat 1] Alamat1,  
--		[Alamat 2] Alamat2, [Flazz], [TopUp], Payment, [BCA Card] BCA_Card,
--		Visa, Master, Amex, Diners, Cicilan, [Cicilan Prog] Cicilan_Prog, Debit, Tunai
--INTO TempXLS
--FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0', @sDataSource, 
--				'SELECT [Product ID], [Merchant ID], [Merchant Name], [Alamat 1], 
--						[Alamat 2], [Flazz], [TopUp], Payment, [BCA Card],
--						Visa, Master, Amex, Diners, Cicilan, [Cicilan Prog], Debit, Tunai
--				 FROM [OKTOBER 2012$]'
--				)

--EXEC (@sSQL)

CREATE TABLE TempQC (
		TerminalID VARCHAR(8),
		MID VARCHAR(15),
		MerchantName VARCHAR(25),
		Alamat1 VARCHAR(25),
		Alamat2 VARCHAR(25),
		Flazz BIT,
		TopUp BIT,
		Payment BIT,
		BCACard BIT,
		Visa BIT,
		[Master] BIT,
		AMEX BIT,
		DINERS BIT,
		Reward BIT,
		Cicilan BIT,
		CicilanPr BIT,
		DEBIT BIT,
		TUNAI BIT,
		AcquirerTagValue VARCHAR(5),
		MasterDial BIT,
		[F2_Pwd] VARCHAR(10),
		AdminPwd VARCHAR(10),
		InitPwd VARCHAR(10),
		T1_BCA VARCHAR(15),
		T2_BCA VARCHAR(15),
		T1_Reward VARCHAR(15),
		T2_Reward VARCHAR(15),
		T1_Debit VARCHAR(15),
		T2_Debit VARCHAR(15),
		T1_Cicilan VARCHAR(15),
		T2_Cicilan VARCHAR(15),
		T1_CicilanPrB VARCHAR(15),
		T2_CicilanPrB VARCHAR(15),
		T1_CicilanPrC VARCHAR(15),
		T2_CicilanPrC VARCHAR(15),
		T1_CicilanPrD VARCHAR(15),
		T2_CicilanPrD VARCHAR(15),
		T1_CicilanPrE VARCHAR(15),
		T2_CicilanPrE VARCHAR(15),
		T1_CicilanSQ VARCHAR(15),
		T2_CicilanSQ VARCHAR(15),
		T1_CicilanSQPrB VARCHAR(15),
		T2_CicilanSQPrB VARCHAR(15),
		T1_CicilanSQPrC VARCHAR(15),
		T2_CicilanSQPrC VARCHAR(15),
		T1_CicilanSQPrD VARCHAR(15),
		T2_CicilanSQPrD VARCHAR(15),
		T1_CicilanSQPrE VARCHAR(15),
		T2_CicilanSQPrE   VARCHAR(15)
	)

INSERT INTO TempQC
EXEC [spQueryReport]
SET @iRowCount = @@ROWCOUNT

IF (@iRowCount > 0)
BEGIN
	SELECT a.TerminalID 
	INTO #TerminalList
	FROM TempXLS a
		INNER JOIN TempQC b
	ON a.TerminalID = b.TerminalID AND
		a.MID = b.MID AND
		a.MerchantName = b.MerchantName AND
		a.Alamat1 = b.Alamat1 AND
		a.Alamat2 = b.Alamat2 AND
		a.Flazz = b.Flazz AND
		a.TopUp = b.TopUp AND
		a.Payment = b.Payment AND
		a.BCA_Card = b.BCACard AND
		a.Visa = b.Visa AND
		a.Master = b.Master AND
		a.Amex = b.Amex AND
		a.Diners = b.Diners AND
		a.Cicilan = b.Cicilan AND
		a.Cicilan_Prog = b.CicilanPr AND
		a.Debit = b.Debit AND
		a.Tunai = b.Tunai

	SELECT TerminalId, 
			MID, 
			MerchantName, 
			Alamat1, 
			Alamat2, 
			Flazz, 
			TopUp, 
			Payment, 
			BCA_Card, 
			Visa, 
			Master, 
			Amex, 
			Diners, 
			Cicilan, 
			Cicilan_Prog, 
			Debit, 
			Tunai
	INTO #Result
	FROM TempXLS WHERE TerminalID NOT IN (SELECT TerminalID FROM #TerminalList)
	UNION ALL
	SELECT TerminalId, 
			MID, 
			MerchantName, 
			Alamat1, 
			Alamat2, 
			Flazz, 
			TopUp, 
			Payment, 
			BCACard, 
			Visa, 
			Master, 
			Amex, 
			Diners, 
			Cicilan, 
			CicilanPr, 
			Debit, 
			Tunai
	FROM TempQC WHERE TerminalID NOT IN (SELECT TerminalID FROM #TerminalList)	

	SELECT TerminalId, 
			MID, 
			MerchantName, 
			Alamat1, 
			Alamat2, 
			Flazz, 
			TopUp, 
			Payment, 
			BCA_Card, 
			Visa, 
			[Master], 
			Amex, 
			Diners, 
			Cicilan, 
			Cicilan_Prog, 
			Debit, 
			Tunai
	FROM #Result
	ORDER BY TerminalID
END
--IF (OBJECT_ID('TempQC') IS NOT NULL)
	DROP TABLE TempQC