﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Modify date: 
-- Description:	
--				1. INSERT The History Acquirer table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerHistoryInsert]
	@sTerminalID VARCHAR(8),
	@sAcquirerName VARCHAR(25)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO tbProfileAcquirerHistory(TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue,LastUpdate)
	SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue,CONVERT(VARCHAR(20),GETDATE(),113)
	FROM tbProfileAcquirer WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID AND AcquirerName = @sAcquirerName
END