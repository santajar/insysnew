﻿

-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: January 20, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spGetContentScramble]
	@sTerminalID VARCHAR(8)
AS
IF OBJECT_ID('TEMPDB..#TempContent') IS NOT NULL
DROP TABLE #TempContent
CREATE TABLE #TempContent
(
--CSI Varchar(8), TID Varchar(8), MID varchar(20), MERCHANTNAME Varchar(8),
--[ADDRESS] Varchar(40), CITY Varchar(25), PROFILECREATED varchar (30)
CONTENT VARCHAR(100)
)

INSERT INTO #TempContent (CONTENT) SELECT TerminalTagValue from tbProfileTerminal
where TerminalID=@sTerminalID AND TerminalTag In ('de01','de02','de03','de04','dc04')

INSERT INTO #TempContent (CONTENT) SELECT CreateDate  from tbProfileTerminalList
where TerminalID=@sTerminalID

DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CREDIT'
print 'icount' + COnvert(varchar(10),@iCountID)
IF @iCountID > 1
BEGIN
	INSERT INTO #TempContent (CONTENT) select AcquirerTagValue from tbProfileAcquirer 
	where TerminalID=@sTerminalID AND AcquirerTag IN ('aa04','aa05' ) AND AcquirerName='CREDIT'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'DEBIT'
IF @iCountID > 1
BEGIN
INSERT INTO #TempContent (CONTENT) select AcquirerTagValue from tbProfileAcquirer 
	where TerminalID=@sTerminalID AND AcquirerTag IN ('aa04','aa05' ) AND AcquirerName='DEBIT'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP103'
IF @iCountID > 1
BEGIN
INSERT INTO #TempContent (CONTENT) select AcquirerTagValue from tbProfileAcquirer 
	where TerminalID=@sTerminalID AND AcquirerTag IN ('aa04','aa05' ) AND AcquirerName='CILTAP103'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP206'
IF @iCountID > 1
BEGIN
INSERT INTO #TempContent (CONTENT) select AcquirerTagValue from tbProfileAcquirer 
	where TerminalID=@sTerminalID AND AcquirerTag IN ('aa04','aa05' ) AND AcquirerName='CILTAP206'
END

ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP312'
IF @iCountID > 1
BEGIN
INSERT INTO #TempContent (CONTENT) select AcquirerTagValue from tbProfileAcquirer 
	where TerminalID=@sTerminalID AND AcquirerTag IN ('aa04','aa05' ) AND AcquirerName='CILTAP312'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP418'
IF @iCountID > 1
BEGIN
INSERT INTO #TempContent (CONTENT) select AcquirerTagValue from tbProfileAcquirer 
	where TerminalID=@sTerminalID AND AcquirerTag IN ('aa04','aa05' ) AND AcquirerName='CILTAP418'
END



select * from #TempContent

--BEGIN
--	select TerminalTagValue as CONTENT 
--	from tbProfileTerminal where TerminalID=@sTerminalID 
--	AND TerminalTag In ('de01','de02','de03','de04','de09')
--END