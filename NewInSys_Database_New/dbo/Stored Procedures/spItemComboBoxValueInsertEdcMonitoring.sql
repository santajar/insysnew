﻿-- =============================================
-- Author		: Tobias SETYO
-- Create date	: March 16, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spItemComboBoxValueInsertEdcMonitoring]
	@sEdcMonitoringName VARCHAR(15),
	@sDatabaseId INT
AS
DECLARE @iCount INT=0

SELECT @iCount = COUNT (*)
FROM tbItemComboBoxValue WITH (NOLOCK)
WHERE DatabaseID = @sDatabaseId
	AND RealValue = @sEdcMonitoringName

IF @iCount = 0
BEGIN
	SELECT * 
	INTO #TempItemlist
	FROM tbItemList WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId
		AND ItemName IN ('TMS Monitoring Name')

INSERT INTO tbItemComboBoxValue(DatabaseId,
			FormId,
			ItemSequence,
			DisplayValue,
			RealValue)
SELECT DatabaseID [DatabaseId], 
	FormID [FormID],
	ItemSequence [ItemSequence],
	@sEdcMonitoringName [DisplayValue],
	@sEdcMonitoringName [RealValue]
FROM #TempItemlist

END
ELSE
 PRINT 'GPRS Name Already Exist'