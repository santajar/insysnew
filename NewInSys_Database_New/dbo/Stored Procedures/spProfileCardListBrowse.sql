﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 3, 2010
-- Modify date:
--				1. August 12, 2010, adding column CardTagLength
--				2. September 8, 2010, Var @sDatabaseID dan @sCardName diubah jadi @sCondition 
--				   sebagai pengganti statement 'WHERE'
-- Description:	Get The Card table content of Database
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCardListBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
BEGIN
	DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery = '
	SELECT 
		cl.CardName, 
		c.CardTag AS Tag, 
		c.CardTagLength, 
		c.CardTagValue AS Value
	FROM tbProfileCardList cl WITH (NOLOCK) JOIN tbProfileCard c WITH (NOLOCK) ON (cl.CardID=c.CardID) '
	print @sQuery + @sCondition+ ' ORDER BY DatabaseID, CardName, CardTag'
	EXEC (@sQuery + @sCondition+ ' ORDER BY DatabaseID, CardName, CardTag')
END

