﻿

-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: January 20, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spInsertProfileVerification]
	@sEdcType VARCHAR(10),
	@sDbVersion VARCHAR(10),
	@sTerminalID VARCHAR(8),  ---ini diambil dari CSI
	@sTicketNumber VARCHAR(15),
	@sTID VARCHAR(8),
	@sMerchantName VARCHAR(30),
	@sAddress VARCHAR(30),
	@sCity VARCHAR(30),
	@sProfileCreated NVARCHAR(20),
	@sSerialNumber VARCHAR(15),
	@sTechnisianName VARCHAR(35),
	@sRemarks VARCHAR(50),
	@sSPK VARCHAR(10),
	@sWO VARCHAR(10),
	@sOperator VARCHAR(15),
	@sStatus VARCHAR(10)
	

AS
BEGIN

--DECLARE @sDate DateTime
--DECLARE @VarDate Varchar(max)
--DECLARE @VarDate2 Varchar(max)
--DECLARE @VarDate3 Varchar(max)
--  SET @sDate = GETDATE()
--  SET @VarDate = CONVERT(varchar, @sDate,111)
--  SET @VarDate2 = SUBSTRING(@VarDate,1,10)
--  --SELECT @VarDate2
--  SET @VarDate3 = REPLACE(@VarDate2,'/','-')
  --SELECT @VarDate3

INSERT INTO tbProfileVerification 
(EdcType, DbVersion, TerminalID, TicketNumber, TID, MerchantName, Address, City, ProfileCreated, SerialNumber, TechnicianName, Remarks, SPK, WO, Operator, [Status], LogDate)
VALUES
(@sEdcType,	@sDbVersion, @sTerminalID, @sTicketNumber,@sTID, @sMerchantName,@sAddress, @sCity,@sProfileCreated, 	@sSerialNumber, 	@sTechnisianName,	@sRemarks,	@sSPK, @sWO,@sOperator,@sStatus, GETDATE())
--(@sEdcType,	@sDbVersion, @sTerminalID, @sTicketNumber,@sTID, @sMerchantName,@sAddress, @sCity,@sProfileCreated, 	@sSerialNumber, 	@sTechnisianName,	@sRemarks,	@sSPK, @sWO,@sOperator,@sStatus, CONVERT(VARCHAR(10),@VarDate3))
END
	exec [spGetRandomPassword] @sTerminalID