﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 20, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating relation table on Issuer name
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationRenameIssuer]
	@sTerminalID VARCHAR(8),
	@sOldIss VARCHAR(15),
	@sNewIss VARCHAR(15)
AS
BEGIN
SET NOCOUNT ON;
	EXEC spProfileRelationHistoryInsert @sTerminalID
	EXEC spProfileIssuerHistoryInsert @sTerminalID,@sOldIss

	DECLARE @iDBID INT
	SET @iDBID= dbo.iDatabaseIdByTerminalId(@sTerminalID)
	--IF (dbo.isTagLength4(@iDBID)=4)
	--BEGIN
		UPDATE TbProfileRelation
		SET RelationTagValue = @sNewIss
		WHERE TerminalID = @sTerminalID AND
			RelationTag IN ('AD02','AD002') AND
			RelationTagValue = @sOldIss
	--END
	--ELSE
	--BEGIN
	--	UPDATE TbProfileRelation
	--	SET RelationTagValue = @sNewIss
	--	WHERE TerminalID = @sTerminalID AND
	--		RelationTag = 'AD002' AND
	--		RelationTagValue = @sOldIss
	--END
END
