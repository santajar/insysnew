﻿CREATE PROCEDURE [dbo].[spProfileTerminalInitCompressBrowse] 
	@bIsCompress BIT
AS
	DECLARE @sStmt VARCHAR(500)
	DECLARE @sCondition VARCHAR(200)
	SET @sStmt = '
		SELECT DISTINCT DatabaseID 
		INTO #Compress
		FROM tbProfileTerminalList 
		WHERE ISNULL(InitCompress,0) = 1
		
		SELECT DISTINCT l.DatabaseID,
						DatabaseName,
						InitCompress
					FROM tbProfileTerminalList l WITH (NOLOCK)
					LEFT JOIN tbProfileTerminalDb b WITH (NOLOCK)
							ON l.DatabaseID = b.DatabaseID '

	IF @bIsCompress = 0
		SET @sStmt = @sStmt + 'WHERE ISNULL(InitCompress,0) = 0 
			AND l.DatabaseID NOT IN (SELECT DatabaseID FROM #Compress) '
	ELSE
		SET @sStmt = @sStmt + 'WHERE ISNULL(InitCompress,0) = 1'

	SET @sStmt = @sStmt + ' ORDER BY DatabaseName'
 
	EXEC (@sStmt + @sCondition)




