﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 25, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating control flag of allow init
-- =============================================
CREATE PROCEDURE [dbo].[spControlFlagAllowInitUpdate]
	@bAllowInit BIT
AS
	SET NOCOUNT ON
UPDATE tbControlFlag
SET Flag=@bAllowInit
WHERE ItemName='AllowInit'
