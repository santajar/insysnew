﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 28, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Start migrating process of Combobox type and value on Old database into NewInSys database
-- =============================================
CREATE PROCEDURE [dbo].[spItemComboBoxValueMigrate]
	@sDatabaseId VARCHAR(3)
AS
DECLARE @sDatabaseName VARCHAR(MAX)
SET @sDatabaseName=dbo.sBrDatabaseNameById(@sDatabaseId)

IF OBJECT_ID('[' + @sDatabaseName + ']..Value') IS NOT NULL
BEGIN
	IF OBJECT_ID('tempdb..#tbitemvalue') IS NOT NULL
		DROP TABLE #tbitemvalue
	CREATE TABLE #tbitemvalue(
		FormId INT,
		ItemId INT,
		[Value] VARCHAR(40),
		RealValue VARCHAR(20)
	)
	DECLARE @SqlStmt NVARCHAR(MAX)
	SET @sqlstmt = '
		SELECT *
		FROM [' + @sDatabaseName + ']..Value'
	INSERT INTO #tbitemvalue
	EXEC sp_executesql @sqlstmt

	INSERT INTO tbItemComboBoxValue(DatabaseID,
		FormId,
		ItemSequence,
		DisplayValue,
		RealValue)
	SELECT @sDatabaseId DatabaseID,
		FormId,
		ItemId ItemSequence,
		[Value] DisplayValue,
		RealValue
	FROM #tbitemvalue
END
