USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileIssuerCopy]    Script Date: 11/2/2017 4:47:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spProfileIssuerCopy]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
BEGIN
	-- select all value
declare @cols nvarchar (MAX)
declare @query nvarchar (MAX)

SET @cols =STUFF((SELECT ',' + QUOTENAME(COLUMN_NAME) 
                    from [NewInSys_BCA-DEV].INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_NAME = N'tbProfileIssuer' and COLUMN_NAME like ('AE%')
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

set @query='
	INSERT INTO tbProfileIssuer(
		TerminalId,IssuerName,'+ @cols +'
		)
	SELECT '''+ @sNewTID +''',IssuerName,'+ @cols +' 
	FROM tbProfileIssuer (NOLOCK)
	WHERE TerminalId='''+ @sOldTID +''''
	
exec (@query)
END

GO


