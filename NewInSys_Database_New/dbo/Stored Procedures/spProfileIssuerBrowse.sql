USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileIssuerBrowse]    Script Date: 11/2/2017 2:34:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 3, 2010
-- Modify date:
--				1. August 12, 2010, adding column IssuerTagLength
--				2. September 8, 2010, Var @sTerminalID dan @sIssuerName diubah jadi @sCondition 
--				   sebagai pengganti statement 'WHERE'
-- Description:	Get The Issuer table content of Terminal
-- =============================================
CREATE PROCEDURE [dbo].[spProfileIssuerBrowse]
	@sCondition VARCHAR(MAX)=NULL,
	@sTerminalID VARCHAR(8)
AS

CREATE  TABLE #IssuerUnpivot(
	[TerminalID] VARCHAR(8) NOT NULL,
	[IssuerName] VARCHAR(30) NOT NULL,
	[IssuerTag] VARCHAR(5) NOT NULL,
	[ItemName] VARCHAR(30) NOT NULL,
	[IssuerTagLength] INT NOT NULL,
	[IssuerTagValue] VARCHAR(150) NULL
)

	INSERT INTO #IssuerUnpivot
	EXEC spProfileUnpivotIssuer @sTerminalID

	DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery = 
		'SELECT TerminalID, 
				IssuerTag AS Tag, 
				IssuerTagLength, 
				IssuerTagValue AS Value
		FROM #IssuerUnpivot '

	IF @sCondition IS NULL
		EXEC (@sQuery)
	ELSE
		EXEC (@sQuery + @sCondition + ' ORDER BY TerminalID, IssuerName, IssuerTag')

	EXEC spProfileTerminalUpdateLastView @sTerminalID

	drop table #IssuerUnpivot


GO


