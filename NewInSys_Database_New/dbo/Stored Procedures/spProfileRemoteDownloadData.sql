﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <3 Nov 2014>
-- Description:	<List Data Download>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRemoteDownloadData]
	@sTerminalID VARCHAR(8)
AS
SET NOCOUNT ON;

SELECT list.TerminalID,app.AppPackageName,app.BuildNumber,app.AppPackageFilename,applist.Id,applist.AppPackageContent, app.UploadTime
FROM tbAppPackageEdcList app WITH (NOLOCK)
	JOIN tbAppPackageEDCListTemp applist WITH (NOLOCK)
		ON app.AppPackId = applist.AppPackId 
	JOIN tbListTIDRemoteDownload list WITH (NOLOCK)
		ON app.AppPackId = list.AppPackID
WHERE list.TerminalID = @sTerminalID 
ORDER BY Id