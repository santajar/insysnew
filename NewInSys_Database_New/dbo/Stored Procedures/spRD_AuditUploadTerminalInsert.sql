﻿CREATE PROCEDURE [dbo].[spRD_AuditUploadTerminalInsert]
	@sTerminalID VARCHAR(8)
AS
INSERT INTO tbRD_AuditUploadTerminal(TerminalID)
VALUES(@sTerminalID)
