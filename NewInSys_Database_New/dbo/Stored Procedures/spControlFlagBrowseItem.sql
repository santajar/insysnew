﻿

-- ==========================================================
-- Description:	Get FlagValue and ItemName for given ItemName
-- ==========================================================

CREATE PROCEDURE [dbo].[spControlFlagBrowseItem]
	@sItemName VARCHAR(50)
AS
	SET NOCOUNT ON
	SELECT ItemName, Flag
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName = @sItemName

