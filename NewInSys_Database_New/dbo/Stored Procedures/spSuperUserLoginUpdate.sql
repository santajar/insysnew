﻿


-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Jan 10, 2011
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating user login name or password
-- =============================================
CREATE PROCEDURE [dbo].[spSuperUserLoginUpdate]
	@sConditions VARCHAR(MAX),
	@sUserID VARCHAR(50) = ''

AS
	DECLARE @sOldUserRight VARCHAR(2000),
			@sNewUserRight VARCHAR(2000)

	SELECT @sOldUserRight = UserRights
	FROM tbUserLogin WITH (NOLOCK)
	WHERE UserId = @sUserID

	DECLARE @sSqlStmt VARCHAR(MAX)
	
	SET @sSqlStmt = 'UPDATE tbUserLogin '
	SET @sSqlStmt = @sSqlStmt + @sConditions
	
	EXEC(@sSqlStmt)


	SELECT @sNewUserRight = UserRights
	FROM tbUserLogin WITH (NOLOCK)
	WHERE UserId = @sUserID
	
	IF (@sUserID <> '')
		EXEC spUserLoginBrowseHistoryUpdate @sOldUserRight, @sNewUserRight

