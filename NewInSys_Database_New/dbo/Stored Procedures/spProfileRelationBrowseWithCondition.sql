﻿CREATE PROCEDURE [dbo].[spProfileRelationBrowseWithCondition]
	@sCondition VARCHAR(MAX) = NULL
AS
	DECLARE @sSqlStmt VARCHAR(MAX)
	SET @sSqlStmt = 'SELECT TerminalId,
						RelationTag,
						RelationTagLength,
						RelationTagValue
					FROM tbProfileRelation WITH (NOLOCK) '
	SET @sSqlStmt = @sSqlStmt + ISNULL(@sCondition,'')
	
	EXEC(@sSqlStmt)
