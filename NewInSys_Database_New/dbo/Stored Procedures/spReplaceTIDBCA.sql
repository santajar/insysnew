﻿
CREATE PROCEDURE [dbo].[spReplaceTIDBCA]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8),
	@sMIDDummy VARCHAR(15)=NULL
AS	
--BEGIN TRY
	
IF ((SELECT COUNT(*) FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE TerminalID = @sOldTID) < 1)
	SELECT 'Old TerminalID Not Found'
ELSE IF ((SELECT COUNT(*) FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE TerminalID = @sNewTID ) > 0)
	SELECT 'Unable to replace TID ' + @sOldTID + ' . Duplicate New TID ' + @sNewTID
ELSE		
BEGIN
	DECLARE @iDbID BIGINT
	SET @iDbID = DBO.iDatabaseIdByTerminalId(@sOldTID)
			
	EXEC spProfileTerminalCopyProcess @iDbID, @sOldTid, @sNewTID
			
	-- Update Old Merchant Name to Merchant Dummy
	IF ISNULL(@sMIDDummy,'')<>''
	BEGIN
		UPDATE tbProfileTerminal 
		SET TerminalTagValue = dbo.sDummyMerchantName(@sMIDDummy)
		WHERE TerminalID = @sOldTID AND
				TerminalTag IN ('DE02','DE002')
				
		-- Update Old MID as MID Dummy
		UPDATE tbProfileAcquirer 
		SET AcquirerTagValue = SUBSTRING(AcquirerTagValue, 1, 
									DATALENGTH(AcquirerTagValue) - DATALENGTH(@sMIDDummy)) + @sMIDDummy
		WHERE TerminalID = @sOldTID AND 
				AcquirerTag IN ('AA04','AA004')
	END
			
	UPDATE tbProfileAcquirer 
	SET AcquirerTagValue = (
		--CASE WHEN AcquirerName LIKE 'PROMO%' THEN SUBSTRING(AcquirerTagValue, 1, 3) + SUBSTRING(@sNewTID, 4, 5)
		--ELSE SUBSTRING(AcquirerTagValue, 1, 2) + SUBSTRING(@sNewTID, 3, 6)				
		--END
		CASE AcquirerName WHEN 'DINERS' THEN AcquirerTagValue
		ELSE (
			CASE WHEN AcquirerName LIKE 'PROMO%' THEN 
				SUBSTRING(AcquirerTagValue, 1, 1)+ SUBSTRING(@sNewTID, 2, 1) + SUBSTRING(AcquirerTagValue, 3, 1) + SUBSTRING(@sNewTID, 4, 5)
			ELSE SUBSTRING(AcquirerTagValue, 1, 1) + SUBSTRING(@sNewTID, 2, 7)
			END
			)
		END)				
	WHERE TerminalID = @sNewTID AND AcquirerTag IN ('AA05','AA005') AND AcquirerName IN (
		SELECT AcquirerName FROM tbProfileAcquirer WITH(NOLOCK)
		WHERE TerminalID=@sNewTID AND AcquirerTag IN ('AA04','AA004') AND ISNUMERIC(AcquirerTagValue)=1)
END
--END TRY
--BEGIN CATCH
--	SELECT ERROR_LINE() + ' ' + ERROR_MESSAGE()
--END CATCH								


