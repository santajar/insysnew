-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: 21 maret 2017
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[spViewSerialNumberListBrowse]
@sSerialNumber VARCHAR(30),
@sTerminalID VARCHAR(8),
@sFlag VARCHAR(4)
AS
IF @sFlag = 'TID'
BEGIN
	SELECT TerminalID, DatabaseID, SerialNumber
	FROM TbProfileSerialNumberList
	WHERE SerialNumber = @sSerialNumber
END

IF @sFlag = 'SN'
BEGIN
	SELECT SerialNumber From TbProfileSerialNumberList
	WHERE TerminalID = @sTerminalID
END


GO


