﻿/****** Object:  StoredProcedure [dbo].[spProfileRemoteDownloadListBrowse]    Script Date: 11/20/2013 2:14:38 PM ******/
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Nov 20, 2013
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Browse content tbRemoteDownload
-- =============================================
CREATE Procedure [dbo].[spProfileEMVManagementListBrowse]
	@sDatabaseId VARCHAR(3)
AS
	SELECT DISTINCT EMVManagementName as Name,
			DatabaseID			
	FROM tbProfileEMVManagement WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId