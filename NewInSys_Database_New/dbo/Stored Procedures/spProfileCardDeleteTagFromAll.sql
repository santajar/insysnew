﻿
CREATE PROCEDURE [dbo].[spProfileCardDeleteTagFromAll]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5)
AS
	DELETE FROM tbProfileCard
	WHERE CardID IN (SELECT CardID
						 FROM tbProfileCardList WITH (NOLOCK)
						 WHERE DatabaseID = @sDatabaseID
						)
		 AND CardTag = @sTag 





