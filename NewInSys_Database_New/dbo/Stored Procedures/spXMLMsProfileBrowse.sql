﻿CREATE PROCEDURE [dbo].[spXMLMsProfileBrowse]
	@sCond VARCHAR(MAX)=NULL
AS
DECLARE @sQuery VARCHAR(MAX)
SET @sQuery = '
SELECT EmsXmlVersion, 
	EmsXmlProfile,
	DatabaseId,
	MasterProfile
FROM tbEmsMsProfile WITH (NOLOCK) '

IF ISNULL(@sCond,'') <> ''
	SET @sQuery = @sQuery + @sCond
EXEC (@sQuery)	



