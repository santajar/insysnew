﻿
-- ===================================================================
-- Description:	Insert new FlagValue if ItemName not Found
--				If ItemName found, update FlagValue with current value
-- ===================================================================

CREATE PROCEDURE [dbo].[spControlFlagUpdate]
	@sItemName VARCHAR(50) = '',
	@sFlag VARCHAR(150) = ''
AS
	SET NOCOUNT ON
	DECLARE @iCount INT	
	
	SELECT @iCount = COUNT(*)
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName = @sItemName 

	PRINT (@iCount)
	IF(@iCount = 0)
		INSERT tbControlFlag(ItemName,Flag)
		VALUES (@sItemName, @sFlag)
	ELSE
 		UPDATE tbControlFlag
		SET Flag = @sFlag
		WHERE ItemName = @sItemName



