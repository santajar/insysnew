﻿CREATE PROCEDURE [dbo].[spProfileRelationMoveIssuerToBottom] @sTerminalId VARCHAR(8),
	@sIssuerName VARCHAR(25)
AS
SET NOCOUNT ON;

EXEC spProfileRelationHistoryInsert @sTerminalId

DECLARE @iRelationId INT
--IF (dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalID))=4)
--BEGIN
	SELECT @iRelationId = RelationTagID
	from tbProfileRelation
	WHERE TerminalID = @sTerminalId AND RelationTag IN ('AD02','AD002') AND RelationTagValue = @sIssuerName
--END
--ELSE
--BEGIN
--	SELECT @iRelationId = RelationTagID
--	from tbProfileRelation
--	WHERE TerminalID = @sTerminalId AND RelationTag = 'AD002' AND RelationTagValue = @sIssuerName
--END


SELECT TerminalID, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue, [Description]
INTO #tempRelation
FROM tbProfileRelation
WHERE RelationTagID BETWEEN @iRelationId - 1 AND @iRelationId + 1

DELETE FROM tbProfileRelation
WHERE RelationTagID BETWEEN @iRelationId - 1 AND @iRelationId + 1

INSERT INTO tbProfileRelation
	(TerminalID, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue, [Description])
SELECT TerminalID, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue, [Description]
FROM #tempRelation

--IF OBJECT_ID('tempdb..#tempRelation') IS NOT NULL
	DROP TABLE #tempRelation