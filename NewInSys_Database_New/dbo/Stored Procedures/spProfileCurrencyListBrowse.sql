﻿CREATE PROCEDURE [dbo].[spProfileCurrencyListBrowse]
	@sDatabaseId BIGINT
AS
	SELECT DISTINCT CurrencyName [Name]
	FROM tbProfileCurrency WITH (NOLOCK)
	WHERE DatabaseId = @sDatabaseId
	
