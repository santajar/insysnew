﻿

/*Altered by chris 27 July 2012
	-change @sTerminalID CHAR 8, because TerminalID lenght must be 8
	-@sAppPackID VARCHAR(150), according to its length in tbProfileTerminalList 
*/
CREATE PROCEDURE [dbo].[spAppPackageBrowseTerminalID] 
	@sTerminalID CHAR(8)
AS
	SET NOCOUNT ON
	DECLARE @sAppPackID VARCHAR(150)
	
	SELECT @sAppPackID = AppPackID
	 FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE TerminalId = @sTerminalID
	
	IF OBJECT_ID('tempdb..#AppPackId') IS NOT NULL
		DROP TABLE #AppPackId
	CREATE TABLE #AppPackId(AppPackID VARCHAR(5))

	WHILE CHARINDEX (',' , @sAppPackId) > 0
	BEGIN
		BEGIN
			-- sql statements here
			INSERT INTO #AppPackId VALUES (SUBSTRING(@sAppPackId,1,CHARINDEX (',' , @sAppPackId)-1))
			SET @sAppPackId = SUBSTRING(@sAppPackId,CHARINDEX (',' , @sAppPackId)+1,99999)
		END
	END

	SELECT @sTerminalID [TerminalID], AppPackID, AppPackageName
	FROM tbAppPackageEDCList WITH (NOLOCK)
	WHERE AppPackID IN (SELECT AppPackID FROM #AppPackId)