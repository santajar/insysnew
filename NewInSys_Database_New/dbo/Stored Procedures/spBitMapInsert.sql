﻿
-- =========================================
-- Description:	 Insert new data in tbBitMap
-- =========================================

CREATE PROCEDURE [dbo].[spBitMapInsert]
	@sApplicationName VARCHAR(30),
	@bAllowCompress BIT,
	@sBitMap VARCHAR(64),
	@sHexBitMap VARCHAR (16),
	@bStandartISO8583 BIT,
	@sLogDetail VARCHAR(200) OUTPUT
AS
	SET NOCOUNT ON
-- Search data for ApplicationName in table
	DECLARE @iCount INT
	SELECT @iCount = COUNT(*)
	FROM tbBitMap WITH (NOLOCK)
	WHERE ApplicationName = @sApplicationName

	IF @iCount = 0
	BEGIN
		INSERT INTO tbBitMap (ApplicationName, AllowCompress, BitMap, HexBitMap, StandartISO8583)
		VALUES (@sApplicationName, @bAllowCompress, @sBitMap, @sHexBitMap,@bStandartISO8583)


		EXEC spBitMapInsertGetLogDetail @sApplicationName, @sLogDetail OUTPUT
	END
	ELSE -- If Application Name found in table (ApplicationName is PK)
	BEGIN
		--SELECT 'Application Name has already used. Please use other Application Name.'
		SELECT 'Application Name has already used. Please use other Application Name.'
		SET  @sLogDetail = 'Double Application Name'
	END