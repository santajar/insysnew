﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 29, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. view all the terminal profile, with databaseid and databasename
-- =============================================
CREATE PROCEDURE [dbo].[spViewTerminalListBrowse]
@sTerminalID VARCHAR(8)
AS
	SELECT TerminalID, DatabaseID, DatabaseName
	FROM dbo.fn_viewTerminalList()
	WHERE TerminalID = @sTerminalID
