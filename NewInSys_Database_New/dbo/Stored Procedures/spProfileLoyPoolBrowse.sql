﻿

CREATE PROCEDURE [dbo].[spProfileLoyPoolBrowse]
	@sCondition VARCHAR(100) = NULL
AS
	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt = 'SELECT LoyPoolTagID,
						LoyPoolName, 
						DatabaseID,
						LoyPoolTag as Tag,
						LoyPoolLengthOfTagLength,
						LoyPoolTagLength,
						LoyPoolTagValue as Value,
						[Description]
				FROM tbProfileLoyPool WITH (NOLOCK) '
	SET @sStmt = @sStmt + ISNULL(@sCondition,'') + ' ORDER BY LoyPoolName, LoyPoolTag'
	EXEC (@sStmt)

