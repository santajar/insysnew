﻿CREATE Procedure [dbo].[spsMTIResponse]
	@sReturn NVARCHAR(MAX) OUTPUT
AS
DECLARE @sLinkDatabase NVARCHAR(50),
		@sQuery NVARCHAR(MAX),
		@ParmDefinition NVARCHAR(500)


SELECT @sLinkDatabase = FLAG
FROM tbControlFlag
WHERE ItemName= 'LinkedRemoteDownloadServer'

PRINT '11-'
SET @sQuery = N'SELECT @sParamReturn = Flag 
			FROM '+@sLinkDatabase+'tbControlflag 
			WITH (NOLOCK)
			WHERE ItemName= '''+'MTIResponse'+''''

SET @ParmDefinition = N'@sParamReturn VARCHAR(MAX) OUTPUT';

EXECUTE sp_executesql @sQuery,@ParmDefinition,@sParamReturn = @sReturn OUTPUT;