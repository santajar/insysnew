USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileTextFullTable]    Script Date: 11/2/2017 2:29:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spProfileTextFullTable]   
 @sTerminalID VARCHAR(8)  
AS  
SET NOCOUNT ON  
  
DECLARE @tbInitTerminal TABLE(  
 Id INT IDENTITY(1,1) PRIMARY KEY,  
 TerminalId VARCHAR(8),  
 Name VARCHAR(50),  
 Tag VARCHAR(5),  
 ItemName VARCHAR(24),  
 TagLength SMALLINT,  
 TagValue VARCHAR(1000)  
) 

CREATE  TABLE #AcquirerUnpivot(
	[TerminalID] VARCHAR(8) NOT NULL,
	[Name] VARCHAR(30) NOT NULL,
	[Tag] VARCHAR(5) NOT NULL,
	[ItemName] VARCHAR(30) NOT NULL,
	[TagLength] INT NOT NULL,
	[TagValue] VARCHAR(150) NULL
)


INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
exec sp_tbProfileContentSortNew @sTerminalID,null  
--SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
--FROM dbo.fn_tbProfileContentSortNew(@sTerminalId,NULL)  
--ORDER BY ID  
--print ('123')

INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
FROM dbo.fn_tbProfileContentTLE(@sTerminalId,null)

IF dbo.IsGPRSInit(@sTerminalId)=1
BEGIN
	IF OBJECT_ID('tempdb..#TempGPRS') IS NOT NULL  
	DROP TABLE #TempGPRS  
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
	INTO #TempGprs 
	FROM dbo.fn_tbProfileContentGPRS(@sTerminalID,NULL)

	INSERT INTO #AcquirerUnpivot
	EXEC spProfileUnpivotAcquirer @sTerminalID

	IF OBJECT_ID('tempdb..#TempAcq') IS NOT NULL  
	DROP TABLE #TempAcq  
	SELECT TagValue 
	INTO #TempAcq
	FROM #AcquirerUnpivot
	WHERE TerminalID = @sTerminalID


	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue
	FROM #TempGprs
	WHERE NAME IN
		(SELECT TagValue
		FROM #TempAcq)
END


DECLARE @iEMVInit BIT  
SET @iEMVInit=dbo.IsEMVInit(@sTerminalID)  
IF @iEMVInit=1  
BEGIN  
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentAID(@sTerminalID,NULL)  

 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentCAPK(@sTerminalId,NULL)  
END  

IF dbo.IsPinPadInit(@sTerminalId)=1  
BEGIN  
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentPinPad(@sTerminalID,NULL)  
END  

IF DBO.IsRemoteDownloadInit(@sTerminalID)=1
BEGIN
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentRemoteDownload(@sTerminalID,NULL)  
END

IF dbo.IsPromoManagementInit(@sTerminalID)=1
BEGIN
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
	FROM dbo.fn_tbProfileContentPromoManagement(@sTerminalID,NULL)
END

IF dbo.IsInitialFlazzManagementInit(@sTerminalID)=1
BEGIN
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
	FROM dbo.fn_tbProfileContentInitialFlazzManagement(@sTerminalID,NULL)
END
SELECT * FROM @tbInitTerminal  
ORDER BY Id


GO


