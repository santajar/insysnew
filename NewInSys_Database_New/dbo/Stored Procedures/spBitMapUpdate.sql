﻿



-- ======================================
-- Description:	 Update data in tbBitMap
-- ======================================

CREATE PROCEDURE [dbo].[spBitMapUpdate]
	@sApplicationName VARCHAR(30),
	@bAllowCompress BIT,
	@sBitMap VARCHAR(64),
	@sHexBitMap VARCHAR (16),
	@bStandartISO8583 BIT = 0,
	@sLogDetail VARCHAR(200) OUTPUT
AS
	SET NOCOUNT ON
DECLARE @iCount INT
	SELECT @iCount = COUNT(*)
	FROM tbBitMap 
	WHERE ApplicationName = @sApplicationName
	
	IF @iCount = 1
	BEGIN

		UPDATE tbBitMap 
		SET AllowCompress = @bAllowCompress,
			BitMap = @sBitMap,
			HexBitMap = @sHexBitMap,
			StandartISO8583 = @bStandartISO8583
		WHERE ApplicationName = @sApplicationName

		EXEC spBitMapUpdateGetLogDetail @sLogDetail OUTPUT, @sApplicationName
		
	END
	
	ELSE
		SELECT 'Application Name not found.'