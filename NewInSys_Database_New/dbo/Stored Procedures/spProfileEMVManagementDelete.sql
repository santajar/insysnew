﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <19 Maret 2015>
-- Description:	<Delete EMV Management>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileEMVManagementDelete]
	@iDatabaseID INT,
	@sEMVManagementName VARCHAR(50),
	@bDeleteData BIT = 1
AS
BEGIN
	DELETE
	FROM tbProfileEMVManagement
	WHERE DatabaseID = @iDatabaseID AND EMVManagementName = @sEMVManagementName
END