﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: 7 Sept 2015
-- Description:	Insert Console Log into Database
-- =============================================
Create PROCEDURE [dbo].[spConsoleLogInsert]
	@sLogType VARCHAR(100),
	@sDetailLog VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO tbLogConsole (LogTime,LogType,DetailLog)
	VALUES (GETDATE(),@sLogType,@sDetailLog)
    
END