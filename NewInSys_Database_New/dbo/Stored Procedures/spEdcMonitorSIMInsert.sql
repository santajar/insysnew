﻿CREATE PROCEDURE [dbo].[spEdcMonitorSIMInsert]
	@iID_Header INT,
	@sTerminalID VARCHAR(8),
	@sSoftwareVersion VARCHAR(50),
	@sMCCMNC VARCHAR(12),
	@sLAC VARCHAR(50),
	@sCellID VARCHAR(50),
	@iTowerNumber INT
AS
INSERT INTO tbEdcMonitorSIM(ID_Header
	,TerminalID
	,SoftwareVersion
	,MCC_MNC_Data
	,LAC_Data
	,Cell_ID_Data
	,TowerNumber)
VALUES(@iID_Header
	,@sTerminalID
	,@sSoftwareVersion
	,@sMCCMNC
	,@sLAC
	,@sCellID
	,@iTowerNumber)