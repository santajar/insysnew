﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 3, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating the profile initialization table
-- =============================================
CREATE PROCEDURE [dbo].[spInitEdit]
	@sCond VARCHAR(3000)
AS
DECLARE @sSql VARCHAR(MAX)
SET @sSql = 'UPDATE tbInit ' + @sCond
EXEC (@sSql)