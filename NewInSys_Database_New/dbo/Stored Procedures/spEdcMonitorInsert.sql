
GO

CREATE PROCEDURE [dbo].[spEdcMonitorInsert]
	@sRawMsg VARCHAR(MAX)=null,
	@sProcCode VARCHAR(6)=null,
	@sTerminalID VARCHAR(8)=null,
	@sSerialNumber VARCHAR(50)=null,
	@sSoftwareVersion VARCHAR(50)=null,
	@iLifetimeCounter INT=0,
	@iTotalAllTransaction INT=0,
	@iTotalSwipe INT=0,
	@iTotalDip INT=0,
	@sSettlementDateTime VARCHAR(12)=null,
	@sICCID VARCHAR(50)=null,
	@sTID VARCHAR(50)=null,
	@sMID VARCHAR(50)=null,
	@iID_Header INT = 0 OUTPUT
AS
DECLARE @iDatabaseID INT = 0
--SELECT @iDatabaseID=DatabaseID 
--FROM tbProfileTerminalList 
--WHERE TerminalID=@sTerminalID

DECLARE @tempID TABLE (ID INT)

INSERT INTO tbEdcMonitor(TerminalID
	,DatabaseID
	,MessageTimestamp
	,MessageRaw
	,ProcCode
	,SerialNumber
	,SoftwareVersion
	,LifetimeCounter
	,TotalAllTransaction
	,TotalSwipe
	,TotalDip
	,SettlementDateTime
	,ICCID
	,TID
	,MID
	)
OUTPUT inserted.ID INTO @tempID(ID)
VALUES (@sTerminalID
	,@iDatabaseID
	,GETDATE()
	,@sRawMsg
	,@sProcCode
	,@sSerialNumber
	,@sSoftwareVersion
	,@iLifetimeCounter
	,@iTotalAllTransaction
	,@iTotalSwipe
	,@iTotalDip
	,@sSettlementDateTime
	,@sICCID
	,@sTID
	,@sMID
	)

SELECT @iID_Header=ID FROM @tempID

GO

