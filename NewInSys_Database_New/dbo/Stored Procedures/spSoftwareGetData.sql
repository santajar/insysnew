﻿
-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <20 okt 2015>
-- Description:	<Get Data Software>
-- =============================================
CREATE PROCEDURE [dbo].[spSoftwareGetData]
	@sSoftwareName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT B.Id, A.AppPackageName,B.AppPackageContent, CONVERT(varchar(23), A.UploadTime,120) AS UploadTime
	FROM tbAppPackageEDCList A
	JOIN
	tbAppPackageEDCListTemp B
	ON A.AppPackId = B.AppPackId
	WHERE A.AppPackageName = @sSoftwareName
END