﻿

-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Description:	delete The History Issuer table content of Terminal
-- =============================================
cREATE PROCEDURE [dbo].[spProfileIssuerHistoryDelete]
	@sTerminalID VARCHAR(8),
	@sIssuerName VARCHAR(25)
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM tbProfileIssuerHistory 
	WHERE TerminalID = @sTerminalID AND IssuerName = @sIssuerName
END