﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <19 Maret 2015>
-- Description:	<Update EMV Management>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileEMVManagementUpdate]
	@iDatabaseID INT,
	@sEMVManagementName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileEMVManagementDelete @iDatabaseID,@sEMVManagementName
	EXEC spProfileEMVManagementInsert @iDatabaseID,@sEMVManagementName,@sContent
END