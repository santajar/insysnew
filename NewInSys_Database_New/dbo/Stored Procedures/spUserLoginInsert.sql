﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Agt 26, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Adding new user login
-- =============================================
CREATE PROCEDURE [dbo].[spUserLoginInsert]
@sUID VARCHAR(10),
@sUserName VARCHAR(25),
@sPassword VARCHAR(32),
@sUserRights VARCHAR(MAX),
@sLocked VARCHAR(10)= NULL,
@sRevoke VARCHAR(10) = NULL

AS
BEGIN
IF (@sLocked IS NULL)
	SET @sLocked = 0
IF (@sRevoke IS NULL)
	SET @sRevoke = 0

	INSERT INTO tbUserLogin (UserID, UserName, [Password], UserRights,LastModifyPassword,Locked,[Revoke])
	VALUES
	(@sUID,@sUserName,@sPassword,@sUserRights,GETDATE(),@sLocked,@sRevoke)

	EXEC spUserLoginBrowseHistoryInsert @sUserRights
END

