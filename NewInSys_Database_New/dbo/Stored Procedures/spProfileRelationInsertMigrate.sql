﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 5, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Migrating relation table of profile content process
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationInsertMigrate]
@sTerminalId VARCHAR(8)
AS

IF OBJECT_ID ('tempdb..#tempListAcq') IS NOT NULL 
	DROP TABLE #tempListAcq

CREATE TABLE #tempListAcq (
	ID INT IDENTITY(1,1),
	[Name] VARCHAR(50)
	)

INSERT INTO #tempListAcq  ([Name])
SELECT DISTINCT AcquirerName 
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE TerminalId = @sTerminalId

DECLARE @iTotalRow INT
DECLARE @iRowCount INT
DECLARE @iAcqFound INT
DECLARE @sAcqName VARCHAR(15)

SELECT @iTotalRow = COUNT(*) FROM #tempListAcq
SET @iRowCount = 1

WHILE (@iRowCount <= @iTotalRow )
BEGIN
	
	SELECT @sAcqName = [Name] FROM #tempListAcq WHERE ID = @iRowCount
	SELECT @iAcqFound = COUNT(RelationTagValue) FROM tbProfileRelation WITH (NOLOCK)
			WHERE TerminalId = @sTerminalID AND RelationTagValue = @sAcqName AND RelationTag IN ('AD03','AD003')
	
	IF (@iAcqFound = 0)
		EXEC spProfileRelationInsert @sTerminalID, @sAcqName, NULL, NULL
	SET @iRowCount = @iRowCount +1
END

DROP TABLE #tempListAcq




