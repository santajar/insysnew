﻿/*
Created by	: Tobias Setyo
Date		: July 5, 2017
Remarks		: Acquirer Terminal ID validation, special purpose for BNI
*/
CREATE PROCEDURE [dbo].[spGetTidAcquirer]
	@sTerminalID VARCHAR(8),
	@sAcquirerType VARCHAR(2),
	@sAcqTerminalID VARCHAR(8)
AS
SELECT DatabaseID,Tag,ItemName
INTO #itemlist  
FROM tbItemList WITH(NOLOCK)
WHERE DatabaseID in (SELECT DatabaseID FROM tbProfileTerminalList WITH(NOLOCK) 
		WHERE TerminalID = @sTerminalID) 
	AND FormID=2 AND ItemName IN ('Terminal ID','Acquirer Type')

DECLARE @sTagTID VARCHAR(15), @sTagAcqType VARCHAR(5)
SELECT @sTagTID=Tag FROM #itemlist WHERE ItemName = 'Terminal ID'
SELECT @sTagAcqType=Tag FROM #itemlist WHERE ItemName = 'Acquirer Type'

SELECT * 
FROM tbProfileAcquirer WITH(NOLOCK) 
WHERE TerminalID = @sTerminalID 
	AND AcquirerName = (SELECT AcquirerName FROM tbProfileAcquirer WITH(NOLOCK) WHERE TerminalID = @sTerminalID AND AcquirerTag = @sTagAcqType AND AcquirerTagValue = @sAcquirerType) 
	AND AcquirerTag = @sTagTID AND AcquirerTagValue = @sAcqTerminalID
