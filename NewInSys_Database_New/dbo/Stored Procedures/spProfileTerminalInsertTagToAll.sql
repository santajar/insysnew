﻿
CREATE PROCEDURE [dbo].[spProfileTerminalInsertTagToAll]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5),
	@iLengthofTagValueLength INT,
	@sTagValue VARCHAR(500),
	@iTagValueLength INT
AS
IF(dbo.isTagLength4(@sDatabaseID)=4)
BEGIN
	INSERT INTO tbProfileTerminal (TerminalID, TerminalTag, TerminalLengthOfTagLength, TerminalTagLength, TerminalTagValue)
	SELECT TerminalID, @sTag, @iLengthOfTagValueLength, @iTagValueLength, @sTagValue
	FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseID
END
ELSE
BEGIN
	INSERT INTO tbProfileTerminal (TerminalID, TerminalTag, TerminalLengthOfTagLength, TerminalTagLength, TerminalTagValue)
	SELECT TerminalID, SUBSTRING(@sTag,0,3)+'0'+SUBSTRING(@sTag,3,3), @iLengthOfTagValueLength, @iTagValueLength, @sTagValue
	FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseID
END