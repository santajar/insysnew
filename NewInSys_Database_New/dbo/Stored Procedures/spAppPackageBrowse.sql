﻿CREATE PROCEDURE [dbo].[spAppPackageBrowse]
@sCondition varchar(max) = null
AS
	SET NOCOUNT ON

declare @sQuery varchar(max) ='SELECT	AppPackID, 
	AppPackageName [Software Name],
	BuildNumber, 
	AppPackageFilename [Package Filename],
	AppPackageDesc [Description]	
FROM tbAppPackageEDCList WITH (NOLOCK) '

if @sCondition is null
	exec (@sQuery)
else
begin
	set @sQuery = @sQuery+ @sCondition
	exec(@sQuery)
end