﻿CREATE PROCEDURE [dbo].[sptbItemListCopy]
	@sDbSourceID SMALLINT,
	@sDbDestinationID SMALLINT
AS
	INSERT INTO tbItemList (DatabaseID,
							FormID, 
							ItemSequence,
							ItemName,
							ObjectID,
							DefaultValue,
							LengthOfTagValueLength,
							vAllowNull,
							vUpperCase,
							vType,
							vMinLength,
							vMaxLength,
							vMinValue,
							vMaxValue,
							ValidationMsg, 
							Tag
							)
	SELECT @sDbDestinationID,
			FormID,
			ItemSequence,
			ItemName,
			ObjectID,
			DefaultValue,
			LengthOfTagValueLength,
			vAllowNull,
			vUpperCase,
			vType,
			vMinLength,
			vMaxLength,
			vMinValue,
			vMaxValue,
			ValidationMsg, 
			Tag
	FROM tbItemList WITH (NOLOCK)
	WHERE DatabaseID = @sDbSourceID
