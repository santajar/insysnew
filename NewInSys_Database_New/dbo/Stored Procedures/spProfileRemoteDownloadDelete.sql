﻿
CREATE PROCEDURE [dbo].[spProfileRemoteDownloadDelete]
	@iDatabaseId INT,
	@sRemoteDownloadName VARCHAR(50),
	@bDeleteData BIT = 1
AS
	DECLARE @sRemoteDownloadID VARCHAR(20)
	SELECT @sRemoteDownloadID = RemoteDownloadTagValue	
	FROM tbProfileRemoteDownload WITH (NOLOCK)
	WHERE RemoteDownloadName = @sRemoteDownloadName AND 
		  DatabaseID = @iDatabaseID AND 
		  RemoteDownloadTag IN ('RD01','RD001')

	DECLARE @iCount INT =0
	SELECT @iCount = COUNT(*)
	FROM tbProfileTerminal
	WHERE TerminalID IN (SELECT TerminalID FROM tbProfileTerminalList WITH (NOLOCK) WHERE DatabaseID=@iDatabaseId)
		AND (TerminalTag IN 
				(SELECT TAG
				FROM tbItemList WITH (NOLOCK)
				WHERE DatabaseID = @iDatabaseId
					AND ItemName = 'Remote Download ID')) 
		AND TerminalTagValue = @sRemoteDownloadID
		and TerminalID in (select TerminalID from tbProfileTerminalList WITH (NOLOCK) where DatabaseID = @iDatabaseId)

	IF @bDeleteData = 1 AND @iCount > 0
		SELECT 'IP Primary still used in one or more terminals.'
	ELSE
	BEGIN
		DELETE FROM tbProfileRemoteDownload
		WHERE DatabaseID = @iDatabaseId AND
			  RemoteDownloadName = @sRemoteDownloadName

		EXEC spItemComboBoxValueDeleteRemoteDownload @sRemoteDownloadName, @iDatabaseId

		DECLARE @sDatabaseID VARCHAR(3)
		SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
		EXEC spProfileTerminalUpdateLastUpdateByTagAndDbID @sDatabaseID,'Remote Download Name',@sRemoteDownloadName
	END