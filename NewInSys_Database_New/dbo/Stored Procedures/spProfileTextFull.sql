USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileTextFull]    Script Date: 11/2/2017 5:21:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[spProfileTextFull] 
	@sTerminalID VARCHAR(8),
	@sContent VARCHAR(MAX) OUTPUT
AS
SET NOCOUNT ON

DECLARE @tbInitTerminal TABLE(
	Id INT IDENTITY(1,1) PRIMARY KEY,
	TerminalId VARCHAR(8),
	Name VARCHAR(50),
	Tag VARCHAR(5),
	ItemName VARCHAR(24),
	TagLength SMALLINT,
	TagValue VARCHAR(1000)
)

CREATE  TABLE #AcquirerUnpivot(
	[TerminalID] VARCHAR(8) NOT NULL,
	[Name] VARCHAR(30) NOT NULL,
	[Tag] VARCHAR(5) NOT NULL,
	[ItemName] VARCHAR(30) NOT NULL,
	[TagLength] INT NOT NULL,
	[TagValue] VARCHAR(150) NULL
)

INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
exec sp_tbProfileContentSortNew @sTerminalID,null 

INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
FROM dbo.fn_tbProfileContentTLE(@sTerminalId,null)

IF dbo.IsGPRSInit(@sTerminalId)=1
BEGIN
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
	INTO #TempGprs 
	FROM dbo.fn_tbProfileContentGPRS(@sTerminalID,NULL)

	INSERT INTO #AcquirerUnpivot
	EXEC spProfileUnpivotAcquirer @sTerminalID

	IF OBJECT_ID('tempdb..#TempAcq') IS NOT NULL  
	DROP TABLE #TempAcq  
	SELECT TagValue 
	INTO #TempAcq
	FROM #AcquirerUnpivot
	WHERE TerminalID = @sTerminalID


	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue
	FROM #TempGprs
	WHERE NAME IN
		(SELECT TagValue
		FROM #TempAcq)
END

 
IF dbo.IsPinPadInit(@sTerminalId)=1
BEGIN
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
	FROM dbo.fn_tbProfileContentPinPad(@sTerminalID,NULL)
END

--IF dbo.IsBankCodeInit(@sTerminalID)=1
--BEGIN
-- INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
-- SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
-- FROM dbo.fn_tbProfileContentBankCode(@sTerminalID,NULL)  
--END
 
--IF dbo.IsProductCodeInit(@sTerminalID)=1
--BEGIN
-- INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
-- SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
-- FROM dbo.fn_tbProfileContentProductCode(@sTerminalID,NULL)  
--END
  
IF DBO.IsRemoteDownloadInit(@sTerminalID)=1
BEGIN
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentRemoteDownload(@sTerminalID,NULL)  
END

IF dbo.IsPromoManagementInit(@sTerminalID)=1
BEGIN
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
	FROM dbo.fn_tbProfileContentPromoManagement(@sTerminalID,NULL)
END

DECLARE @sTempContent VARCHAR(MAX),
	@sTag VARCHAR(5),
	@sValue VARCHAR(500),
	@iIdStart INT,
	@iIdMax INT
SELECT @iIdStart = MIN(Id) FROM @tbInitTerminal
SELECT @iIdMax = MAX(Id) FROM @tbInitTerminal
SET @sTempContent = ''
WHILE @iIdStart<=@iIdMax
BEGIN
	SELECT @sTag = Tag, @sValue=TagValue
	FROM @tbInitTerminal
	WHERE Id=@iIdStart
	SET @sTempContent = @sTempContent + dbo.sGetTLVString(@sTag, @sValue)
	SET @iIdStart= @iIdStart + 1
END

DECLARE @iEMVInit BIT
SET @iEMVInit=dbo.IsEMVInit(@sTerminalID)
IF @iEMVInit=1
BEGIN
	DELETE @tbInitTerminal
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
	FROM dbo.fn_tbProfileContentAID(@sTerminalID,NULL)

	SELECT @iIdStart = MIN(Id) FROM @tbInitTerminal
	SELECT @iIdMax = MAX(Id) FROM @tbInitTerminal
	--SET @sTempContent = @sTempContent + 'AI'
	WHILE @iIdStart<=@iIdMax
	BEGIN
		SELECT @sTag = Tag, @sValue=TagValue
		FROM @tbInitTerminal
		WHERE Id=@iIdStart
		SET @sTempContent = @sTempContent + dbo.sGetTLVString(@sTag, @sValue)
		SET @iIdStart= @iIdStart + 1
	END

	DELETE @tbInitTerminal
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
	FROM dbo.fn_tbProfileContentCAPK(@sTerminalId,null)

	SELECT @iIdStart = MIN(Id) FROM @tbInitTerminal
	SELECT @iIdMax = MAX(Id) FROM @tbInitTerminal
	WHILE @iIdStart<=@iIdMax
	BEGIN
		SELECT @sTag = Tag, @sValue=TagValue
		FROM @tbInitTerminal
		WHERE Id=@iIdStart
		SET @sTempContent = @sTempContent + dbo.sGetTLVStringLEN3(@sTag, @sValue)
		SET @iIdStart= @iIdStart + 1
	END
END

SET @sContent = @sTempContent
GO


