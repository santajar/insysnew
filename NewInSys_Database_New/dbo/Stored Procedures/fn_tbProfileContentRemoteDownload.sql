USE [NewInSys_BCA-DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_tbProfileContentRemoteDownload]    Script Date: 11/2/2017 5:09:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fn_tbProfileContentRemoteDownload](  
 @sTerminalID VARCHAR(8),  
 @sConditions VARCHAR(MAX)  
 )  
RETURNS @tempTerminal TABLE(  
  TerminalId VARCHAR(8),  
  [Name] VARCHAR(15),  
  Tag VARCHAR(5),  
  ItemName VARCHAR(50),  
  TagLength VARCHAR(3),  
  TagValue VARCHAR(MAX)  
 )  
BEGIN  
INSERT INTO @tempTerminal (TerminalId,  
  [Name],  
  Tag,  
  ItemName,  
  TagLength,  
  TagValue )  
SELECT b.TerminalId,   
 a.RemoteDownloadName [Name],   
 a.RemoteDownloadTag Tag,  
 '' ItemName,   
 a.RemoteDownloadTagLength TagLength,  
 a.RemoteDownloadTagValue TagValue  
FROM tbProfileRemoteDownload a JOIN tbProfileTerminalList b  
ON a.DatabaseId=b.DatabaseId  
WHERE b.TerminalId=@sTerminalID  
	AND RemoteDownloadName IN 
		(SELECT C.DE040
		---(SELECT C.TerminalTagValue
		FROM tbProfileTerminal C
		JOIN tbProfileTerminalList D
		ON C.TerminalID=D.TerminalID
		JOIN tbItemList E
		ON D.DatabaseID= E.DatabaseID
		WHERE E.ItemName = 'Remote Download Name'
			AND C.TerminalID =@sTerminalID )
RETURN  
END
GO


