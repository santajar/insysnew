﻿

-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 24, 2010
-- Modify	  :
--				1. 
-- Description:	Prepare the Terminal profile for Init process
-- =============================================

CREATE PROCEDURE [dbo].[spInitStart]
	@sTerminalID varchar(8)
AS
EXEC spInitDelete @sTerminalID

--IF OBJECT_ID ('TempDb..#Profile', 'U') IS NOT NULL
--	DROP TABLE #Profile

--CREATE TABLE #Profile (
--	TerminalId VARCHAR(8),
--	[Name] VARCHAR(50),
--	Tag VARCHAR(4),
--	ItemName VARCHAR(50) NULL,
--	TagLength SMALLINT,
--	TagValue VARCHAR(1000)
--)

--INSERT INTO #Profile 
--SELECT TerminalID, [Name], Tag, ItemName, TagLength, TagValue
--FROM dbo.fn_tbProfileContentSort(@sTerminalID,NULL)
--ORDER BY ID

---- DECLARE needed variable
--DECLARE @sContent VARCHAR(MAX),
--	@sOldTag VARCHAR(4),
--	@sCurrTag VARCHAR(4),
--	@iMaxByte INT,
--	@iNewInit BIT,
--	@iEMVInit BIT,
--	@iTLEInit BIT,
--	@iLoyaltyInit BIT,
--	@iGPRSInit BIT

---- SET max byte
--SET @sContent=''
--SET @iMaxByte=dbo.iPacketLength()
--SET @iNewInit=dbo.IsNewInit(@sTerminalID)
--SET @iEMVInit=dbo.IsEMVInit(@sTerminalID)
--SET @iTLEInit = dbo.IsTLEInit(@sTerminalID)
--SET @iLoyaltyInit = dbo.IsLoyalty(@sTerminalID)
--SET @iGPRSInit = dbo.IsGPRSInit(@sTerminalID)

---- DECLARE CURSOR
--DECLARE profileCURSOR CURSOR FOR
--	SELECT TerminalId,Tag,TagValue FROM #Profile

---- DECLARE fetch variable
--DECLARE @sTid VARCHAR(8),
--	@sTLVHeader VARCHAR(50),
--	@sTag VARCHAR(4),
--	@sTagValue VARCHAR(1000),
--	@sNewContent VARCHAR(1000)

--OPEN profileCURSOR
--FETCH NEXT FROM profileCURSOR INTO @sTid,@sTag,@sTagValue

--SET @sOldTag=@sTag
--SET @sTLVHeader=dbo.sGetTLVString(@sTag,@sTagValue)

--WHILE @@FETCH_STATUS=0
--BEGIN
--	-- SET current tag
--	SET @sCurrTag = @sTag
--	SET @sNewContent=dbo.sGetTLVString(@sTag,@sTagValue)
	
--	-- compare curr tag with old tag
--	IF SUBSTRING(@sCurrTag,1,2)=SUBSTRING(@sOldTag,1,2)
--	BEGIN
--		-- jika sama,
--		-- cek length current content dengan maxbyte

--		IF LEN(@sContent+@sNewContent)<@iMaxByte
--		BEGIN
--			IF LEN(@sContent)=0
--			BEGIN
--				SET @sContent = @sNewContent
--				SET @sTLVHeader=@sNewContent
--			END
--			ELSE
--			BEGIN
--				SET @sContent = @sContent + @sNewContent
--			END
--		END
--		ELSE IF LEN(@sContent+@sNewContent) >= @iMaxByte
--		BEGIN
--			-- jika length current content lebih besar atau sama dengan max byte
--			-- simpan, dan reset content
--			INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--				VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--			IF @iNewInit=0
--				SET @sContent=@sTLVHeader+@sNewContent
--			ELSE
--				SET @sContent=@sNewContent
--		END
--	END
--	ELSE
--	BEGIN
--		-- jika berbeda tag, maka simpan dan reset content
--		INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--				VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--		SET @sContent=@sNewContent
		
--		SET @sTLVHeader=dbo.sGetTLVString(@sTag,@sTagValue)
--	END
--	SET @sOldTag=@sCurrTag

--	FETCH NEXT FROM profileCURSOR INTO @sTid,@sTag,@sTagValue

--	IF SUBSTRING(@sTag,1,2)=SUBSTRING(@sOldTag,1,2) AND SUBSTRING(@sTag,3,2)='01' AND @iNewInit=0
--	BEGIN	
--		INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--			VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--		SET @sContent=''
--	END
--	IF @@FETCH_STATUS != 0
--	BEGIN
--		INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--			VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--	END
--END
--CLOSE profileCURSOR
--DEALLOCATE profileCURSOR

----Start preparing the CAPK content into tbInit table


--DECLARE @iOffset INT,
--	@iLength INT

--SET @sNewContent= ''
--SELECT @sContent=TerminalContent
--FROM tbInitTerminal
--WHERE TerminalId=@sTerminalID
--SET @iOffset=0
--SET @iLength=LEN(@sContent)
--SET @sTagValue=''

--WHILE @iOffset<=@iLength
--BEGIN
--	SET @sTagValue = SUBSTRING(@sContent,@iOffset,@iMaxByte)
--	print @sTagValue
--	SET @iOffset = @iOffset + @iMaxByte
--	INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--	VALUES(@sTerminalID,@sTagValue,'00',0)
--END

--IF @iEMVInit=1
--BEGIN
--	IF OBJECT_ID ('TempDb..#ProfileCAPK', 'U') IS NOT NULL
--		DROP TABLE #ProfileCAPK
	
--	SELECT * INTO #ProfileAID FROM dbo.fn_tbProfileContentAID(@sTerminalID,NULL)
--	SELECT * INTO #ProfileCAPK FROM dbo.fn_tbProfileContentCAPK(@sTerminalID,NULL)
	
--	DECLARE cursorAID CURSOR FOR
--		SELECT TerminalId,Tag,TagValue FROM #ProfileAID

--	OPEN cursorAID
--	FETCH NEXT FROM cursorAID INTO @sTid,@sTag,@sTagValue
--	SET @sOldTag=@sTag
	
--	SET @sContent= 'AI'

--	WHILE @@FETCH_STATUS=0
--	BEGIN
--		SET @sContent = @sContent + dbo.sGetTLVString(@sTag,@sTagValue)
--		IF @sOldTag = 'AI04' --Tag Terakhir
--		BEGIN
--			INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--				VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--			SET @sContent= 'AI'
--		END

--		FETCH NEXT FROM cursorAID INTO @sTid,@sTag,@sTagValue
--		SET @sOldTag=@sTag
--	END
--	CLOSE cursorAID
--	DEALLOCATE cursorAID
--	SET @sTag=''
--	SET @sContent= ''

--	DECLARE cursorCAPK CURSOR FOR
--		SELECT TerminalId,Tag,TagValue FROM #ProfileCAPK

--	OPEN cursorCAPK
--	FETCH NEXT FROM cursorCAPK INTO @sTid,@sTag,@sTagValue
--	SET @sOldTag=@sTag

--	WHILE @@FETCH_STATUS=0
--	BEGIN
--		SET @sContent = @sContent + dbo.sGetCAPKTLVString(@sTag,@sTagValue)
		
--		IF @sOldTag = 'PK06'
--		BEGIN
--			INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--				VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--			SET @sContent= ''
--		END

--		FETCH NEXT FROM cursorCAPK INTO @sTid,@sTag,@sTagValue
--		SET @sOldTag=@sTag
--	END
--	CLOSE cursorCAPK
--	DEALLOCATE cursorCAPK
--END

----Start Preparing data for TLE content into tbInit
--IF @iTLEInit = 1
--BEGIN
--	IF OBJECT_ID ('TempDb..#ProfileTLE', 'U') IS NOT NULL
--		DROP TABLE #ProfileTLE
--	SELECT * INTO #ProfileTLE FROM dbo.fn_tbProfileContentTLE (@sTerminalID,NULL)
	
--	SET @sContent= ''
--	SET @sNewContent= ''
	
--	DECLARE cursorTLE CURSOR FOR
--		SELECT TerminalId,Tag,TagValue FROM #ProfileTLE

--	OPEN cursorTLE
--	FETCH NEXT FROM cursorTLE INTO @sTid,@sTag,@sTagValue
	
--	SET @sOldTag=@sTag
	
--	WHILE @@FETCH_STATUS=0
--	BEGIN
--		SET @sNewContent = dbo.sGetTLVString(@sTag,@sTagValue)

--		IF LEN(@sContent+@sNewContent)<@iMaxByte
--			SET @sContent = @sContent + @sNewContent
--		ELSE IF LEN(@sContent+@sNewContent) >= @iMaxByte
--		BEGIN
--			INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--				VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--			SET @sContent = @sNewContent
--		END

--		FETCH NEXT FROM cursorTLE INTO @sTid,@sTag,@sTagValue
--		SET @sOldTag=@sTag
--		IF @@FETCH_STATUS != 0
--		BEGIN
--			INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--				VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--		END
--	END
--	CLOSE cursorTLE
--	DEALLOCATE cursorTLE
--END

--IF @iLoyaltyInit = 1
--BEGIN
--	IF OBJECT_ID ('TempDb..#ProfileLoyaltyPool', 'U') IS NOT NULL
--		DROP TABLE #ProfileLoyaltyPool
--	IF OBJECT_ID ('TempDb..#ProfileLoyaltyProd', 'U') IS NOT NULL
--		DROP TABLE #ProfileLoyaltyProd
--	SELECT * INTO #ProfileLoyaltyPool FROM dbo.fn_tbProfileContentLoyaltyPool(@sTerminalID,NULL)
--	SELECT * INTO #ProfileLoyaltyProd FROM dbo.fn_tbProfileContentLoyaltyProd(@sTerminalID,NULL)
--END

----Start Preparing data for TLE content into tbInit
--IF @iGPRSInit = 1
--BEGIN
--	IF OBJECT_ID ('TempDb..#ProfileGPRS', 'U') IS NOT NULL
--		DROP TABLE #ProfileGPRS
--	SELECT * INTO #ProfileGPRS FROM dbo.fn_tbProfileContentGPRS(@sTerminalID,NULL)

--	SET @sContent= ''
--	SET @sNewContent= ''
	
--	DECLARE cursorGPRS CURSOR FOR
--		SELECT TerminalId,Tag,TagValue FROM #ProfileGPRS

--	OPEN cursorGPRS
--	FETCH NEXT FROM cursorGPRS INTO @sTid,@sTag,@sTagValue
	
--	SET @sOldTag=@sTag
	
--	WHILE @@FETCH_STATUS=0
--	BEGIN
--		SET @sNewContent = dbo.sGetTLVString(@sTag,@sTagValue)

--		IF LEN(@sContent+@sNewContent)<@iMaxByte
--			SET @sContent = @sContent + @sNewContent
--		ELSE IF LEN(@sContent+@sNewContent) >= @iMaxByte
--		BEGIN
--			INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--				VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--			SET @sContent = @sNewContent
--		END

--		FETCH NEXT FROM cursorGPRS INTO @sTid,@sTag,@sTagValue
--		SET @sOldTag=@sTag
--		IF @@FETCH_STATUS != 0
--		BEGIN
--			INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--				VALUES(@sTid,@sContent,SUBSTRING(@sOldTag,1,2),0)
--		END
--	END
--	CLOSE cursorGPRS
--	DEALLOCATE cursorGPRS
--END

print 'spInitStart'

INSERT INTO tbInit(TerminalId, Content, Tag, Flag)
SELECT TerminalId, Content, Tag, Flag
FROM tbInitTemp WITH (NOLOCK)
WHERE TerminalId = @sTerminalId AND Tag != 'GZ'
ORDER BY InitID