﻿
-- =============================================  
-- Author:  Christianto  
-- Create date: Apr 15, 2012  
-- Description: Copy existing profile TLE
-- ============================================= 
CREATE PROCEDURE [dbo].[spProfileTLECopy]  
	@NewDatabaseID VARCHAR(3),
	@OldDatabaseID  VARCHAR(3),
	@sTLEName VARCHAR(50)
AS  
DECLARE @iCountName INT,  
	@iCountID INT  
  
SELECT @iCountName = COUNT(*)  
	FROM tbProfileTLE  
	WHERE TLEName = @sTLEName AND DatabaseID = @NewDatabaseID  
IF (dbo.isTagLength4(@NewDatabaseID) = 4)
BEGIN
	print 'count 4'
	SELECT @iCountID = COUNT(*)  
		FROM tbProfileTLE WITH (NOLOCK)
		WHERE TLETag = 'TL01' AND TLETagValue = @sTLEName AND DatabaseID = @NewDatabaseID  
		print @iCountID
END
ELSE
BEGIN	
	SELECT @iCountID = COUNT(*)  
		FROM tbProfileTLE WITH (NOLOCK)
		WHERE TLETag = 'TL001' AND TLETagValue = @sTLEName AND DatabaseID = @NewDatabaseID  
END

IF @iCountName = 0  
	BEGIN  
	IF @iCountID = 0  
		BEGIN 
		DECLARE @sTLEID VARCHAR(2)
		IF (dbo.isTagLength4(@OldDatabaseID) = 4)
		BEGIN
			print '4'
			IF (dbo.isTagLength4(@NewDatabaseID) = 5)
			BEGIN
				INSERT INTO tbProfileTLE(  
					TLEName,  
					DatabaseID,  
					TLETag,  
					TLELengthOfTagLength,  
					TLETagLength,  
					TLETagValue)  
				SELECT   
					TLEName,  
					@NewDatabaseID,  
					SUBSTRING(TLETag,1,2)+'0'+SUBSTRING(TLETag,3,2),
					TLELengthOfTagLength,  
					TLETagLength,  
					TLETagValue  
				FROM tbProfileTLE
					WHERE DatabaseID = @OldDatabaseID AND TLEName = @sTLEName
  
				SELECT @sTLEID = TLETagValue   
					FROM tbProfileTLE  
					WHERE TLEName = @sTLEName AND DatabaseID = @NewDatabaseID AND TLETag = 'TL002' 
			END
			ELSE
			BEGIN
			print '4.4'
				INSERT INTO tbProfileTLE(  
					 TLEName,  
					 DatabaseID,  
					 TLETag,  
					 TLELengthOfTagLength,  
					 TLETagLength,  
					 TLETagValue)  
				SELECT   
					TLEName,  
					@NewDatabaseID,  
					TLETag,  
					TLELengthOfTagLength,  
					TLETagLength,  
					TLETagValue  
				FROM tbProfileTLE
					WHERE DatabaseID = @OldDatabaseID AND TLEName = @sTLEName
  
				SELECT @sTLEID = TLETagValue   
					FROM tbProfileTLE  
					WHERE TLEName = @sTLEName AND DatabaseID = @NewDatabaseID AND TLETag = 'TL02'  
			END
		END
		ELSE
		BEGIN
			IF (dbo.isTagLength4(@NewDatabaseID) = 4)
			BEGIN
				INSERT INTO tbProfileTLE(  
					 TLEName,  
					 DatabaseID,  
					 TLETag,  
					 TLELengthOfTagLength,  
					 TLETagLength,  
					 TLETagValue)  
				SELECT   
					TLEName,  
					@NewDatabaseID,  
					SUBSTRING(TLETag,1,2) + SUBSTRING(TLETag,4,2),
					TLELengthOfTagLength,  
					TLETagLength,  
					TLETagValue  
				FROM tbProfileTLE
					WHERE DatabaseID = @OldDatabaseID AND TLEName = @sTLEName
  
				SELECT @sTLEID = TLETagValue   
					FROM tbProfileTLE  
					WHERE TLEName = @sTLEName AND DatabaseID = @NewDatabaseID AND TLETag = 'TL02'  
			END
			ELSE
			BEGIN
				INSERT INTO tbProfileTLE(  
					 TLEName,  
					 DatabaseID,  
					 TLETag,  
					 TLELengthOfTagLength,  
					 TLETagLength,  
					 TLETagValue)  
				SELECT   
					TLEName,  
					@NewDatabaseID,  
					TLETag,  
					TLELengthOfTagLength,  
					TLETagLength,  
					TLETagValue  
				FROM tbProfileTLE
					WHERE DatabaseID = @OldDatabaseID AND TLEName = @sTLEName
  
				SELECT @sTLEID = TLETagValue   
					FROM tbProfileTLE WITH (NOLOCK)
					WHERE TLEName = @sTLEName AND DatabaseID = @NewDatabaseID AND TLETag = 'TL002'  
			END
		END
		
		DECLARE @sDatabaseId INT
		SET @sDatabaseId = CAST(@NewDatabaseID AS INT)
		EXEC spItemComboBoxValueInsertTLE @sTLEName, @sTLEID, @sDatabaseID
		
		EXEC spProfileTerminalUpdateLastUpdateByDbID @NewDatabaseId
		
		END  
	ELSE  
		SELECT 'TLE ID has already used. Please choose other TLE ID.'   
	END  
 ELSE  
	SELECT 'TLE-EFTSec has already used. Please choose other TLE-EFTSec.'