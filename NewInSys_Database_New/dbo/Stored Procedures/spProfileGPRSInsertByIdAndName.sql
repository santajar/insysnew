﻿-- =============================================  
-- Author:  Christianto  
-- Create date: Apr 15, 2012  
-- Description: COPY existing profile GPRS 
-- =============================================  

CREATE PROCEDURE [dbo].[spProfileGPRSInsertByIdAndName]  
	@NewDatabaseID VARCHAR(3),
	@OldDatabaseID  VARCHAR(3),
	@GPRSName VARCHAR(50)
AS  
DECLARE @iCountName INT,  
	@iCountID INT  
  
SELECT @iCountName = COUNT(*)  
	FROM tbProfileGPRS WITH (NOLOCK)  
	WHERE GPRSName = @GPRSName AND DatabaseID = @NewDatabaseID  
	
	SELECT @iCountID = COUNT(*)  
		FROM tbProfileGPRS  WITH (NOLOCK)
		WHERE GPRSTag IN ('GP01','GP001') AND GPRSTagValue = @GPRSName AND DatabaseID = @NewDatabaseID  

IF @iCountName = 0  
	BEGIN  
	IF @iCountID = 0  
		BEGIN  
		IF (dbo.isTagLength4 (@OldDatabaseID)=4)
		BEGIN
			print 'dblama = 4'
			IF (dbo.isTagLength4 (@NewDatabaseID)=5)
			BEGIN
				print 'dbbaru=5'
				INSERT INTO tbProfileGPRS
				(
					DatabaseID,
					GPRSName,    
					GPRSTag,  
					GPRSLengthOfTagLength,  
					GPRSTagLength,  
					GPRSTagValue
				)
				SELECT @NewDatabaseID AS DatabaseID, GPRSName, SUBSTRING(GPRSTag,1,2) +'0'+SUBSTRING(GPRSTag,3,2)GPRSTag, GPRSLengthOfTagLength, GPRSTagLength, GPRSTagValue
					FROM tbProfileGPRS WITH (NOLOCK)
					WHERE DatabaseID = @OldDatabaseID AND GPRSName = @GPRSName
			END
			ELSE
			BEGIN
				INSERT INTO tbProfileGPRS
				(
					DatabaseID,
					GPRSName,    
					GPRSTag,  
					GPRSLengthOfTagLength,  
					GPRSTagLength,  
					GPRSTagValue
				)
				SELECT @NewDatabaseID AS DatabaseID, GPRSName, GPRSTag, GPRSLengthOfTagLength, GPRSTagLength, GPRSTagValue
					FROM tbProfileGPRS WITH (NOLOCK)
					WHERE DatabaseID = @OldDatabaseID AND GPRSName = @GPRSName
			END
				
		END
		ELSE
		BEGIN
			IF (dbo.isTagLength4 (@NewDatabaseID)=4)
			BEGIN
				INSERT INTO tbProfileGPRS
					(
						DatabaseID,
						GPRSName,    
						GPRSTag,  
						GPRSLengthOfTagLength,  
						GPRSTagLength,  
						GPRSTagValue
					)
					SELECT @NewDatabaseID AS DatabaseID, GPRSName, SUBSTRING(GPRSTag,1,2) + SUBSTRING(GPRSTag,4,2)GPRSTag, GPRSLengthOfTagLength, GPRSTagLength, GPRSTagValue
						FROM tbProfileGPRS WITH (NOLOCK)
						WHERE DatabaseID = @OldDatabaseID AND GPRSName = @GPRSName
			END
			ELSE
			BEGIN
				INSERT INTO tbProfileGPRS
					(
						DatabaseID,
						GPRSName,    
						GPRSTag,  
						GPRSLengthOfTagLength,  
						GPRSTagLength,  
						GPRSTagValue
					)
					SELECT @NewDatabaseID AS DatabaseID, GPRSName, GPRSTag, GPRSLengthOfTagLength, GPRSTagLength, GPRSTagValue
						FROM tbProfileGPRS WITH (NOLOCK)
						WHERE DatabaseID = @OldDatabaseID AND GPRSName = @GPRSName
			END
		END
				DECLARE  @sDatabaseId INT 
				SET @sDatabaseId = CAST(@NewDatabaseID as INT)
				EXEC spItemComboBoxValueInsertGPRS @GPRSName, @sDatabaseId  
				
				EXEC spProfileTerminalUpdateLastUpdateByDbID @NewDatabaseID  
			
		END  
	ELSE  
		SELECT 'GPRS ID has already used. Please choose other GPRS ID.'   
	END  
ELSE  
	SELECT 'IP Primary has already used. Please choose other IP Primary.'