﻿
-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Okt 12, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spGetReportMonitor]
AS
	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#TempMonitorActive') IS NOT NULL  DROP TABLE #TempMonitorActive 
select * INTO #TempMonitorActive from tbEdcMonitor  where MessageTimestamp like '%'+ SUBSTRING(CAST(GETDATE() - 1 as VARCHAR),0,12) + '%'

IF OBJECT_ID('tempdb..#TempMonitorActiveDetail') IS NOT NULL  DROP TABLE #TempMonitorActiveDetail 
SELECT * INTO #TempMonitorActiveDetail FROM tbEdcMonitorDetail where ID_HEADER IN (SELECT ID From #TempMonitorActive )

IF OBJECT_ID('tempdb..#TempActive') IS NOT NULL  DROP TABLE #TempActive 
SELECT a.MessageTimestamp, a.TerminalID, a.SerialNumber, a.SoftwareVersion, b.CardName, b.CardTotalAmount, b.CardTotalTransaction, b.AcquirerName, b.TID, b.MID
INTO #TempActive FROM #TempMonitorActive a LEFT JOIN #TempMonitorActiveDetail b
ON a.TerminalID = b.TerminalID

IF OBJECT_ID('tempdb..#TempMonitorCIMBN') IS NOT NULL  DROP TABLE #TempMonitorCIMBN 
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempMonitorCIMBN From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From #TempActive WHERE TID IS NULL) AND AcquirerName='CIMBN'
IF OBJECT_ID('tempdb..#TempMonitorCREDIT') IS NOT NULL  DROP TABLE #TempMonitorCREDIT 
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempMonitorCREDIT From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From  #TempActive WHERE TID IS NULL) AND AcquirerName='CREDIT'
IF OBJECT_ID('tempdb..#TempMonitorDEBIT') IS NOT NULL  DROP TABLE #TempMonitorDEBIT 
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempMonitorDEBIT From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From  #TempActive WHERE TID IS NULL) AND AcquirerName='DEBIT'
IF OBJECT_ID('tempdb..#TempMonitorCILTAP') IS NOT NULL  DROP TABLE #TempMonitorCILTAP 
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempMonitorCILTAP From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From  #TempActive WHERE TID IS NULL) AND AcquirerName LIKE '%CILTAP%'

IF OBJECT_ID('tempdb..#TempResultCIMBN') IS NOT NULL  DROP TABLE #TempResultCIMBN 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultCIMBN FROM #TempMonitorCIMBN
Group By TerminalID

IF OBJECT_ID('tempdb..#TempResultCREDIT') IS NOT NULL  DROP TABLE #TempResultCREDIT 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultCREDIT FROM #TempMonitorCREDIT
Group By TerminalID

IF OBJECT_ID('tempdb..#TempResultDEBIT') IS NOT NULL  DROP TABLE #TempResultDEBIT 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultDEBIT FROM #TempMonitorDEBIT
Group By TerminalID

IF OBJECT_ID('tempdb..#TempResultCILTAP') IS NOT NULL  DROP TABLE #TempResultCILTAP 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultCILTAP FROM #TempMonitorCILTAP
Group By TerminalID




DELETE FROM #TempResultCREDIT WHERE TerminalID IN (SELECT TerminalID From #TempResultCIMBN)
DELETE FROM #TempResultDEBIT WHERE TerminalID IN (SELECT TerminalID From #TempResultCIMBN)
DELETE FROM #TempResultCILTAP WHERE TerminalID IN (SELECT TerminalID From #TempResultCIMBN)
DELETE FROM #TempResultDEBIT WHERE TerminalID IN (SELECT TerminalID From #TempResultCREDIT)
DELETE FROM #TempResultCILTAP WHERE TerminalID IN (SELECT TerminalID From #TempResultCREDIT)
DELETE FROM #TempResultCILTAP WHERE TerminalID IN (SELECT TerminalID From #TempResultDEBIT)

IF OBJECT_ID('tempdb..#tbTempTidMid') IS NOT NULL  
DROP TABLE #tbTempTidMid  
CREATE TABLE #tbTempTidMid
(TerminalID VARCHAR(8), TID VARCHAR(8), MID VARCHAR(20))

INSERT INTO #tbTempTidMid(TerminalID, TID, MID)
SELECT TerminalID, TID, MID FROM #TempResultCIMBN
INSERT INTO #tbTempTidMid(TerminalID, TID, MID)
SELECT TerminalID, TID, MID FROM #TempResultCREDIT
INSERT INTO #tbTempTidMid(TerminalID, TID, MID)
SELECT TerminalID, TID, MID FROM #TempResultDEBIT
INSERT INTO #tbTempTidMid(TerminalID, TID, MID)
SELECT TerminalID, TID, MID FROM #TempResultCILTAP


--select * from #TempActive
--select * from #tbTempTidMid
IF OBJECT_ID('tempdb..#TempActiveTemp') IS NOT NULL  DROP TABLE #TempActiveTemp  
select * INTO #TempActiveTemp from #TempActive WHERE TID IS NULL
--select * from #TempActiveTemp

IF OBJECT_ID('tempdb..#TempTempResult1') IS NOT NULL  DROP TABLE #TempTempResult1  
CREATE TABLE #TempTempResult1
(
MessageTimestamp VARCHAR(20), CSI VARCHAR(20), SerialNumber VARCHAR(20), SoftwareVersion VARCHAR(20), CardName VARCHAR(20), CardTotalAmount VARCHAR(20), CardTotalTransaction VARCHAR(20), AcquirerName VARCHAR(20), TerminalID VARCHAR(20) , MERCHANTID VARCHAR(20)
)

INSERT INTO #TempTempResult1 (MessageTimestamp, CSI, SerialNumber, SoftwareVersion, CardName, CardTotalAmount, CardTotalTransaction, AcquirerName, TerminalID, MERCHANTID)
SELECT a.MessageTimestamp, a.TerminalID, a.SerialNumber, a.SoftwareVersion, '0', '0', '0', '0', b.TID, b.MID FROM #TempActiveTemp a
CROSS JOIN #tbTempTidMid b
WHERE a.TerminalID = b.TerminalID

--select * from #TempTempResult1
--select * from #TempActive

DELETE  FROM #TempActive WHERE TID IS NULL

INSERT INTO #TempActive (MessageTimeStamp, TerminalID, SerialNumber, SoftwareVersion, CardName, CardTotalAmount, CardTotalTransaction, AcquirerName, TID, MID)
SELECT MessageTimeStamp, CSI, SerialNumber, SoftwareVersion, CardName, CardTotalAmount, CardTotalTransaction, AcquirerName, TerminalID, MERCHANTID FROM  #TempTempResult1

select MessageTimestamp Tanggal, TerminalID CSI, SerialNumber, SoftwareVersion, CardName, CardTotalAmount, CardTotalTransaction, AcquirerName, TID TerminalID, MID MerchantID from #TempActive