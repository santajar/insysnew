﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 7, 2010
-- Description:	Get Profile Database List
-- Table: TerminalDB
-- Modify @sQuery parameter
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalDbBrowse]
	 @sCondition VARCHAR(150) = ''
AS
BEGIN
	DECLARE @sQuery VARCHAR(8000)
	SET @sQuery = 'SELECT DatabaseID, 
		DatabaseName, 
		NewInit,
		EMVInit,
		EMVInitManagement,
		TLEInit,
		LoyaltyProd,
		GPRSInit,
		RemoteDownload,
		PinPad,
		PromoManagement,
		InitialFlazzManagement,
		CardType'

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'CurrencyInit' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		SET @sQuery= @sQuery + ', CurrencyInit '

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'BankCode' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		SET @sQuery= @sQuery + ', BankCode '

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'ProductCode' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		SET @sQuery= @sQuery + ', ProductCode '

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'Custom_CAPK_Name' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		SET @sQuery= @sQuery + ', Custom_CAPK_Name '

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'Custom_Menu' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		SET @sQuery= @sQuery + ', Custom_Menu '

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'EnableMonitor' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		SET @sQuery= @sQuery + ', EnableMonitor '

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'SNManagement' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		SET @sQuery= @sQuery + ', SNManagement '

	SET @sQuery = @sQuery +' FROM tbProfileTerminalDB WITH (NOLOCK) '+ @sCondition
	EXEC (@sQuery + ' ORDER BY DatabaseName')
END