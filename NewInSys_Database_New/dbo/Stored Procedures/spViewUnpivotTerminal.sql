USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spViewUnpivotTerminal]    Script Date: 11/2/2017 11:54:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spViewUnpivotTerminal]
		@TerminalID varchar(8)
as

declare @iDbID varchar (4),
		@cols nvarchar(MAX),
		@query nvarchar(max)
	
		
set @iDbID=(select DatabaseID 
			from tbProfileTerminalList 
			where TerminalID=@TerminalID)

SET @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=1 and DatabaseID=@iDbID
                    group by tag,ItemID
                    order by tag
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

--- select Terminal (terminal union masterprofile)
-- from table terminal
set @query='
select u.TerminalID,u.TerminalID[Name],u.[Tag],il.ItemName,len(u.[TagValue])[TagLength],len(len(u.[TagValue]))[LengthOfTagLength],u.[TagValue]
from tbProfileTerminal s
	unpivot
	(
		 [TagValue] 
		For Tag in ('+ @cols +') 
	) as u
	join tbProfileTerminalList tl
	on u.TerminalID=tl.TerminalID
	join tbItemList il
	on tl.DatabaseID=il.DatabaseID
where u.TerminalID='''+ @TerminalID +''' and u.Tag=il.Tag 

union

select tl2.TerminalID,tl2.TerminalID[Name],m.Tag,il2.ItemName,len(m.TagValue)[TagLength],len(len(m.TagValue))[LengthOfTagLength],m.TagValue
from MasterProfile m
	join tbItemList il2
	on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
	join tbProfileTerminalList tl2 
	on tl2.DatabaseID=il2.DatabaseID 
where tl2.TerminalID='''+ @TerminalID +''' and m.FormID=1 
	and m.Tag not in ( select u.[Tag]
	from tbProfileTerminal s
		unpivot
		(
			[TagValue] 
			For Tag in ('+ @cols +') 
		) as u 
	where u.TerminalID='''+ @TerminalID +''') 
order by Tag'

exec (@query)



GO


