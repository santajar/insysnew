﻿CREATE procedure [dbo].[spChartCity]
as
	SET NOCOUNT ON
truncate Table tbGroupChart
if OBJECT_ID('tempdb..#CityList') is not null
	drop table #CityList

create table #CityList
(
	Name varchar(25)
)

truncate table tbCityChart

insert into #CityList (Name)
select distinct CityRDName from tbCityRemoteDownload WITH (NOLOCK)
WHERE CityRDName != ''

declare @iCount int, @sName varchar(25)
declare ChartCursor cursor FOR
select Name from #CityList

open ChartCursor

fetch next from ChartCursor into @sName
set @iCount=0
while @@FETCH_STATUS=0
begin
	SELECT @iCount=Count(CityRDName) 
	FROM tbCityRemoteDownload A WITH (NOLOCK)
	JOIN tbListTIDRemoteDownload B WITH (NOLOCK)
	ON A.IdCityRD=B.IdCityRD
	WHERE A.CityRDName= @sName

	INSERT INTO tbCityChart (CityName,CityCount)
	VALUES ( @sName,@iCount)

	fetch next from ChartCursor into @sName
end

close ChartCursor
Deallocate ChartCursor
select * from tbCityChart WITH (NOLOCK)