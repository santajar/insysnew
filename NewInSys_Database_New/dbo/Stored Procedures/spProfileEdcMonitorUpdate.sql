﻿-- =============================================
-- Author		: Tobias SETYO
-- Create date	: March 8, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spProfileEdcMonitorUpdate]
	@iDatabaseID INT,
	@sEdcMonitorName VARCHAR(15),
	@sContent VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileEdcMonitorDelete @iDatabaseID, @sEdcMonitorName
	EXEC spProfileEdcMonitorInsert @iDatabaseID, @sEdcMonitorName, @sContent
END