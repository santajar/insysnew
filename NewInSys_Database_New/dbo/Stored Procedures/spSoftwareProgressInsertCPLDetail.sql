﻿

-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: may 30, 2017
-- Modify date:
--				1. 
-- Description:	Insert into tbSoftwareProgress
-- =============================================

CREATE procedure [dbo].[spSoftwareProgressInsertCPLDetail]
@sId int,
@sTerminalID varchar(8),
@sSoftware varchar(50),
@sSoftwareDownload varchar(50),
@sSoftwareDownloadGlobal varchar(50),
@sSerialNumber varchar(25),
@fIndex float,
@iTotalIndexGlobalTotal int


AS
DECLARE
	@iTotalIndex int,
	@fPercentage float,
	@sGroupName varchar(25),
	@iAppPackId INT,
	@iCount INT,
	@fPercentaseGlobal float,
	@fIndexGlobal float
SELECT @sGroupName =  GroupName From tbListTerminalIDPackage WHERE TerminalID = @sTerminalID


SELECT @iTotalIndex=COUNT(*) 
FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackageName =  @sSoftwareDownload 

----PRINT 'MASUK SOFTWARE PROGRESS INSERT'
----PRINT 'SOFTWARENAME = '+@sSoftware
----PRINT 'SOFTWAREDOWNLOAD = '+@sSoftwareDownload
----PRINT @fIndex
----PRINT CAST(@iTotalIndex  as varchar(20))
--PRINT '----'

SET @fPercentage = @fIndex / @iTotalIndex * 100

SELECT @iCount=COUNT(*)
FROM tbSoftwareProgressCPLDetail WITH (NOLOCK)
WHERE InstallStatus = 'On Progress'
	AND TerminalID = @sTerminalID
	AND Software = @sSoftware
	AND AppPackageName = @sSoftwareDownload
	AND SerialNumber= @sSerialNumber
	

IF (@iCount>0)
BEGIN
	--EXEC spSoftwareProgressUpdateCPLDetail
	--	@sId,
	--	@sTerminalID,
	--	@sSoftware,
	--	@sSoftwareDownload,
	--	@sSerialNumber,
	--	@fPercentage,
	--	'On Progress'
	UPDATE tbSoftwareprogressCPLDetail SET Percentage = @fPercentage WHERE TerminalID=@sTerminalID AND AppPackageName = @sSoftwareDownload
	AND ID = @sId
END
ELSE
BEGIN
	INSERT INTO tbSoftwareprogressCPLDetail 
				(ID, 
				GroupName,
				TerminalID,
				Software,
				AppPackageName,
				SerialNumber,
				StartTime,
				Percentage,
				InstallStatus)
	VALUES(@sId, @sGroupName,
		   @sTerminalID,
		   @sSoftware,
		   @sSoftwareDownload,
		   @sSerialNumber,
		   CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
		   @fPercentage,
		   'On Progress')
END

IF @fPercentage = 100 or  @fPercentage > 99
BEGIN
UPDATE tbSoftwareProgressCPLDetail
	SET InstallStatus = 'DONE', Percentage=100, FinishInstallTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP) 
	WHERE TerminalID = @sTerminalID 
		AND SerialNumber = @sSerialNumber 
		AND AppPackageName=@sSoftwareDownload 
		AND Software=@sSoftware 
		AND InstallStatus ='On Progress'
		--AND BuildNumber = @iBuildNumber 
		--AND Percentage= 100 

END

SELECT @fIndexGlobal = SUM(Percentage) FROM tbSoftwareProgressCPLDetail WHERE ID = @sId
SET @fPercentaseGlobal = @fIndexGlobal / @iTotalIndexGlobalTotal * 100
UPDATE tbSoftwareProgressCPL
SET Percentage = @fPercentaseGlobal WHERE
TerminalID = @sTerminalID 
AND SerialNumber = @sSerialNumber 
AND SoftwareDownload=@sSoftwareDownloadGlobal 
AND Software=@sSoftware 
AND InstallStatus ='On Progress'
AND ID = @sId

IF @fPercentaseGlobal = 100 or @fPercentaseGlobal > 99
BEGIN
UPDATE tbSoftwareProgressCPL
	SET InstallStatus = 'SUCESS', Percentage=100, FinishInstallTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP) 
	WHERE TerminalID = @sTerminalID 
		AND SerialNumber = @sSerialNumber 
		AND Software=@sSoftware 
		AND SoftwareDownload=@sSoftwareDownloadGlobal 
		AND InstallStatus ='On Progress'
		--AND BuildNumber = @iBuildNumber 
		--AND Percentage= 100 
END