﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Modify date: 
-- Description:	
--				1. Update The History Terminal table content of profile
-- =============================================
Create PROCEDURE [dbo].[spProfileRelationHistoryUpdate]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	EXEC spProfileRelationHistoryDelete @sTerminalID
	EXEC spProfileRelationHistoryInsert @sTerminalID
END