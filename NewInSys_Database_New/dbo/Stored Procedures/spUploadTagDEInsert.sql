﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 6, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Inserting Tag ID, and Column name to table tbUploadTagDE
--				2. Run on process create Data Upload Template
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagDEInsert]
	@sTag VARCHAR(5),
	@sColumnName VARCHAR(50),
	@bMandatory BIT,
	@iSourceColumn SMALLINT = NULL,
	@sTemplate varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO tbUploadTagDE
	(
		Tag,
		ColumnName,
		Mandatory,
		SourceColumn,
		Template
	)
	VALUES
	(
		@sTag,
		@sColumnName,
		@bMandatory,
		@iSourceColumn,
		@sTemplate
	)
END