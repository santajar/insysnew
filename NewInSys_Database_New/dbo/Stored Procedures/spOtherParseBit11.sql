﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. parse the ISO message value to get the bit-11 value
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseBit11] 
	@sSessionTime VARCHAR(MAX),
	@sBit11 VARCHAR(8) OUTPUT
AS
SET NOCOUNT ON
DECLARE @sSqlStmt NVARCHAR(MAX)

SET @sSqlStmt = N'SELECT @Bit11=BitValue FROM ##tempBitMap' + @sSessionTime + 
	' WHERE BitId=11'
EXEC sp_executesql @sSqlStmt,N'@Bit11 VARCHAR(8) OUTPUT',@Bit11=@sBit11 OUTPUT
