﻿
/*
Modified Date : 12 aug 2014
Modify By : Tobias Supriyadi
Description : - adding ICCID column to export
*/
CREATE PROCEDURE [dbo].[spAutoInitLogGenerateReport]
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Path VARCHAR(100),
			@HeaderName VARCHAR(20),		
			@GetDate VARCHAR(20),
			@GetTime VARCHAR(20),
			@FullDate VARCHAR(20),
			@TypeFile VARCHAR(10),
			@ServerName VARCHAR(30)

--	SET @Path = '"V:' + '\'

	SELECT @Path = Flag
	FROM tbControlFlag
	WHERE ItemName = 'Report AutoInit Path'
	
	--SET @Path = '"' + @Path + '\'
	SET @HeaderName = 'AutoInit'

	SET @GetDate = CONVERT(VARCHAR,GETDATE(),12)
	SET @GetTime = REPLACE(CONVERT (VARCHAR,GETDATE(),108), ':', '') 
	SET @FullDate = @GetDate + @GetTime
	SET @TypeFile = '.txt'
	
	--Select Statement from tbAutoInitLog
	DECLARE @Query VARCHAR(MAX)
	SET @Query = '"SELECT TerminalId + ''|'' + CASE WHEN LEN(RTRIM(ISNULL(LastInitTime, ''-''))) < 1 THEN ''-'' ELSE RTRIM(ISNULL(LastInitTime, ''-'')) END + ''|'' + '
	SET @Query = @Query + 'CASE WHEN LEN(RTRIM(ISNULL(PABX, ''-''))) < 1 THEN ''-'' ELSE  RTRIM(ISNULL(PABX, ''-'')) END + ''|'' +  '
	SET @Query = @Query + 'CASE WHEN LEN(RTRIM(ISNULL(SoftwareVer, ''-''))) < 1 THEN ''-'' ELSE RTRIM(ISNULL(SoftwareVer, ''-'')) END  + ''|'' + CASE WHEN LEN(RTRIM(ISNULL(OSVer, ''-''))) < 1 THEN ''-'' ELSE RTRIM(ISNULL(OSVer, ''-'')) END  + ''|'' + CASE WHEN LEN(RTRIM(ISNULL(KernelVer, ''-''))) < 1 THEN ''-'' ELSE  RTRIM(ISNULL(KernelVer, ''-'')) END + ''|'' + '
	SET @Query = @Query + 'CASE WHEN LEN(RTRIM(ISNULL(EDCSN, ''-''))) < 1 THEN ''-'' ELSE RTRIM(ISNULL(EDCSN, ''-'')) END  + ''|'' + CASE WHEN LEN(RTRIM(ISNULL(ReaderSN, ''-''))) < 1 THEN ''-'' ELSE RTRIM(ISNULL(ReaderSN, ''-'')) END  + ''|'' + CASE WHEN LEN(RTRIM(ISNULL(PSAMSN, ''-''))) < 1 THEN ''-'' ELSE  RTRIM(ISNULL(PSAMSN, ''-'')) END + ''|'' + '
	SET @Query = @Query + 'CASE WHEN LEN(RTRIM(ISNULL(MCSN, ''-''))) < 1 THEN ''-'' ELSE RTRIM(ISNULL(MCSN, ''-'')) END  + ''|'' + CASE WHEN LEN(RTRIM(ISNULL(MemUsage, ''-''))) < 1 THEN ''-'' ELSE RTRIM(ISNULL(MemUsage, ''-'')) END + ''|'' + '
	SET @Query = @Query + 'CASE WHEN LEN(RTRIM(ISNULL(ICCID, ''-''))) < 1 THEN ''-'' ELSE RTRIM(ISNULL(ICCID, ''-'')) END '
	SET @Query = @Query + 'FROM ' + DB_NAME() + '.dbo.tbAutoInitLog WITH (NOLOCK) "'
	print @Query 
	
	DECLARE @FileName VARCHAR(8000)
	SET @FileName = @Path + '\' + @HeaderName + '-' + @FullDate + @TypeFile

	print @FileName 

	--Command to create Report File	
	DECLARE @bcpCommand VARCHAR(8000)				
	SET @bcpCommand = 'bcp '
	SET @bcpCommand = @bcpCommand + @Query
	SET @bcpCommand = @bcpCommand + ' queryout '
	SET @bcpCommand = @bcpCommand + '"' + @FileName + '"' 
	SET @bcpCommand = @bcpCommand + ' -S ' 
	SET @bcpCommand = @bcpCommand + '10.4.192.24' 
	--SET @bcpCommand = @bcpCommand + @@SERVERNAME
	SET @bcpCommand = @bcpCommand + ' -T -c -t ";"  '

	print @bcpCommand 
	EXEC master..xp_cmdshell @bcpCommand

	--TRUNCATE TABLE tbAutoInitLog
END


















