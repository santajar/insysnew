﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Return rows number on tbUploadTagAA which is not null
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagAASourceCount]
	@sTemplate varchar(50)=NULL
AS
SET NOCOUNT ON;
IF ISNULL(@sTemplate,'')<>''
	SELECT COUNT(*) FROM tbUploadTagAA WHERE SourceColumn IS NOT NULL and Template = @sTemplate
ELSE
	SELECT COUNT(*) FROM tbUploadTagAA
