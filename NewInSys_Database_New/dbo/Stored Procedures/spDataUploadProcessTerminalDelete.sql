﻿-- =============================================
-- Author:		Kiki Ariady
-- Create date: Oct 7, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Process the terminal table of profile Data Upload
-- =============================================
CREATE PROCEDURE [dbo].[spDataUploadProcessTerminalDelete]
	@TerminalTable VARCHAR(MAX)
AS
DECLARE @sTID CHAR(8),
	@sTidMaster CHAR(8),
	@iOffsetStart INT, @iOffsetEnd INT,
	@sColumnName VARCHAR(50),
	@sTag VARCHAR(5),
	@sNewTagValue VARCHAR(MAX),
	@SqlStmt NVARCHAR(MAX),
	@sAction VARCHAR(50),
	@sActionDetail VARCHAR(50),
	@iIdStart INT,
	@iIdMax INT,
	@sQuery VARCHAR(MAX)

SET NOCOUNT ON

IF OBJECT_ID('tempdb..#tbUpload') IS NOT NULL
	DROP TABLE #tbUpload

CREATE TABLE #tbUpload (
	Id INT IDENTITY(1,1),
	TerminalId CHAR(8),
	[Master] CHAR(8)
)

SET NOCOUNT ON
SET @sQuery ='
	INSERT INTO #tbUpload(TerminalId,[Master])
	SELECT TerminalId, [Master] FROM ' + @TerminalTable + ' WITH (NOLOCK) '

EXEC (@sQuery)

SELECT @iIdStart = MIN(Id), @iIdMax = MAX(Id)
FROM #tbUpload

SET @sAction = 'Upload Delete'

WHILE @iIdStart <= @iIdMax
BEGIN
	SELECT @sTID = TerminalId
	FROM #tbUpload
	WHERE Id = @iIdStart
	IF dbo.IsTerminalIdExist(@sTID)=1
	BEGIN
		EXEC spProfileTerminalDeleteProcess @sTID
		SET @sActionDetail = 'Delete Success'
	END
	ELSE
	BEGIN
		SET @sActionDetail = 'Delete Failed. Profile ''' + @sTID + ''' is not exist.'
	END
	EXEC spUploadLogInsert @sTID, @sAction, @sActionDetail
	SET @iIdStart = @iIdStart + 1
END