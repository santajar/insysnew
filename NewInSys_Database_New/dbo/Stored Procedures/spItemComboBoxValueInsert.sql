﻿
-- ======================================================
-- Description:	Insert new item into tbItemComboBoxValue 
-- ======================================================

CREATE PROCEDURE [dbo].[spItemComboBoxValueInsert]
	@sDatabaseID SMALLINT,
	@sFormID SMALLINT,
	@iItemSequence	SMALLINT,
	@sDisplayValue VARCHAR(40),
	@sRealValue VARCHAR(20)
AS
	INSERT INTO tbItemComboBoxValue (DatabaseID,
									FormID, 
									ItemSequence,
									DisplayValue, 
									RealValue
									)
	VALUES (@sDatabaseID,
			@sFormID,
			@iItemSequence,
			@sDisplayValue,
			@sRealValue
		   )

