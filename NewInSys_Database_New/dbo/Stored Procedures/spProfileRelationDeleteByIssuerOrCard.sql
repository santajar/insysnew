USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileRelationDeleteByIssuerOrCard]    Script Date: 11/2/2017 3:41:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Kiky Ariady
-- Create date: Sept 17, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete relation of profile content
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationDeleteByIssuerOrCard]
	@sTerminalID VARCHAR(8),
	@sRelation VARCHAR(MAX),
	@sValueIssuer VARCHAR(50)
AS
SET NOCOUNT ON;
IF OBJECT_ID('tempdb..#Temp') IS NOT NULL
	DROP TABLE #Temp


	--DELETE FROM TbProfileRelation
	--WHERE TerminalID = @sTerminalID AND ValueAcquirer=@sRelation AND ValueIssuer=@sRelation

CREATE TABLE #Temp
(
	RelationtagID BIGINT,
	TerminalID VARCHAR(8),
	RelationTag VARCHAR(5),
	RelationLengthOfTagLength INT,
	RelationtagLength INT,
	TagValue VARCHAR(50),
	[Description] VARCHAR(50)
)
IF (dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalID))=5)
BEGIN
	INSERT INTO #Temp(RelationtagID,TerminalID,RelationTag,RelationLengthOfTagLength,RelationtagLength,TagValue,[Description])

select distinct u.ID,u.TerminalID,u.Tag,len(len(F.TagValue))[RelationLengthOfTagLength],len(F.TagValue)[TagLength],F.TagValue,''[Description] from 
(
	select ID,TerminalID,TagCard[Tag],ValueCard[TagValue] 
	from tbProfileRelation 

	union all
	select ID,TerminalID,TagIssuer[Tag],ValueIssuer[TagValue]
	from tbProfileRelation
	
	union all
	select ID,TerminalID,TagAcquirer[Tag],ValueAcquirer[TagValue]
	from tbProfileRelation 	
)u

		FULL OUTER JOIN dbo.fn_tbProfileTLV5(@sTerminalID, @sRelation) F
			ON
				u.TerminalID = F.TerminalID AND
				u.Tag = F.Tag AND
				u.TagValue = F.TagValue
	WHERE u.TerminalID = @sTerminalID
	order by u.ID,u.Tag
END
ELSE
BEGIN
	INSERT INTO #Temp(RelationtagID,TerminalID,RelationTag,RelationLengthOfTagLength,RelationtagLength,TagValue,[Description])

select u.ID,u.TerminalID,u.Tag,len(len(F.TagValue))[RelationLengthOfTagLength],len(F.TagValue)[TagLength],F.TagValue,''[Description] from 
(
	select ID,TerminalID,TagCard[Tag],ValueCard[TagValue] 
	from tbProfileRelation 

	union all
	select ID,TerminalID,TagIssuer[Tag],ValueIssuer[TagValue]
	from tbProfileRelation
	
	union all
	select ID,TerminalID,TagAcquirer[Tag],ValueAcquirer[TagValue]
	from tbProfileRelation 	
)u

		FULL OUTER JOIN dbo.fn_tbProfileTLV(@sTerminalID, @sRelation) F
			ON
				u.TerminalID = F.TerminalID AND
				u.Tag = F.Tag AND
				u.TagValue = F.TagValue
	WHERE u.TerminalID = @sTerminalID
	order by u.ID,u.Tag
END

--select * from #Temp

DELETE FROM tbProfileRelation
WHERE TerminalID = @sTerminalID

INSERT INTO tbProfileRelation
(
	[TerminalID],
	[TagCard],
	[ValueCard],
	[TagIssuer],
	[ValueIssuer],
	[TagAcquirer],
	[ValueAcquirer]
)
SELECT a.TerminalID,a.TagCard,a.ValueCard,b.TagIssuer,b.ValueIssuer,c.TagAcquirer,c.ValueAcquirer 
from 
	(select RelationtagID,TerminalID,RelationTag[TagCard],TagValue[ValueCard] 
		from #Temp  
		where RelationTag in ('AD001','AD01')
	) a 
	left outer join 
	(select RelationtagID,TerminalID,RelationTag[TagIssuer],TagValue[ValueIssuer] 
	from #Temp 
	where RelationTag in ('AD002','AD02')
	) b on a.TerminalID=b.TerminalID
	left outer join  
	(select RelationtagID,TerminalID,RelationTag[TagAcquirer],TagValue[ValueAcquirer] 
	from #Temp 
	where RelationTag in ('AD003','AD03')
	) c
	on a.RelationtagID=b.RelationtagID 
	where a.TerminalID=@sTerminalID and a.RelationtagID=b.RelationtagID and b.RelationtagID=c.RelationtagID
	order by a.RelationTagID,a.TagCard

DROP TABLE #Temp

EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID


GO


