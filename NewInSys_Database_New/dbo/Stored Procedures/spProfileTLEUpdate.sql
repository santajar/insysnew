﻿

CREATE PROCEDURE [dbo].[spProfileTLEUpdate]
	@iDatabaseID INT,
	@sTLEName VARCHAR(50),
	@sTLID VARCHAR(3),
	@sContent VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileTLEDelete @iDatabaseID, @sTLEName, 0
	EXEC spProfileTLEInsert @iDatabaseID, @sTLEName, @sTLID, @sContent
END






