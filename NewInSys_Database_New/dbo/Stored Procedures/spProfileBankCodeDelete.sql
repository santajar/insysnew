﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: SEPT 3, 2014
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Delete BANK CODE
-- =============================================
CREATE PROCEDURE [dbo].[spProfileBankCodeDelete]
	@iDatabaseId INT,
	@sBankCode VARCHAR(50),
	@bDeleteData BIT = 1
AS
DELETE FROM tbProfileBankCode
WHERE DatabaseID = @iDatabaseId AND BankCode = @sBankCode
