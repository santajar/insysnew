﻿-- =============================================
-- Author:		Tobias Setyo
-- Create date: Nov 14, 2016
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete the InitialFlazz management on table tbProfileInitialFlazz
--				2. Modify InitialFlazz Management for Form Flexsible
-- =============================================
CREATE PROCEDURE [dbo].[spProfileInitialFlazzDelete]
	@iDatabaseId INT,
	@sInitialFlazzName VARCHAR(50),
	@bDeleteData BIT = 1
AS
DELETE FROM tbProfileInitialFlazz
WHERE DatabaseId=@iDatabaseId AND InitialFlazzName=@sInitialFlazzName
EXEC [spItemComboBoxValueDeleteInitialFlazzName] @sInitialFlazzName, @iDatabaseId