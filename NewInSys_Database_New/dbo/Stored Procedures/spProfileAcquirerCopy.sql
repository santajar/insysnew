USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileAcquirerCopy]    Script Date: 11/2/2017 4:46:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Aug 19, 2010
-- Modify	  :
--				1. 
-- Description:	Copy The Acquirer table content of Terminal
-- =============================================
CREATE  PROCEDURE [dbo].[spProfileAcquirerCopy]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
BEGIN
declare @cols nvarchar (MAX)
declare @query nvarchar (MAX)

SET @cols =STUFF((SELECT ',' + QUOTENAME(COLUMN_NAME) 
                    from [NewInSys_BCA-DEV].INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_NAME = N'tbProfileAcquirer' and COLUMN_NAME like ('AA%')
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

set @query='
	INSERT INTO tbProfileAcquirer(
		TerminalId,AcquirerName,'+ @cols +'
		)
	SELECT '''+ @sNewTID +''',AcquirerName,'+ @cols +' 
	FROM tbProfileAcquirer (NOLOCK)
	WHERE TerminalId='''+ @sOldTID +''''
	
exec (@query)

END

GO


