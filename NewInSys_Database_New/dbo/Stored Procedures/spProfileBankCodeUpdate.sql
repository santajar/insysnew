﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: SEPT 3, 2014
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Update BANK CODE
-- =============================================
CREATE PROCEDURE [dbo].[spProfileBankCodeUpdate]
	@iDatabaseID INT,
	@sBankCode VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
EXEC spProfileBankCodeDelete @iDatabaseID, @sBankCode
EXEC spProfileBankCodeInsert @iDatabaseID, @sBankCode, @sContent
