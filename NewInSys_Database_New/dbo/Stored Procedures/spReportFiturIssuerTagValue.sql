﻿CREATE PROCEDURE [dbo].[spReportFiturIssuerTagValue]
	@sTerminalID VARCHAR(8),
	@sAcquirerName VARCHAR(25),
	@sTag VARCHAR(5),
	@iDefaultValue BIT = NULL,
	@iValue BIT OUTPUT
AS
--IF OBJECT_ID('TEMPDB..#TempAcqIssCard') IS NOT NULL
--	DROP TABLE #TempAcqIssCard	
--CREATE TABLE #TempAcqIssCard
--(
--	TerminalID VARCHAR(8),
--	AcqName VARCHAR(25),
--	Issname VARCHAR(25),
--	CrdName VARCHAR(25)
--)
--INSERT INTO #TempAcqIssCard
--EXEC SP_tbAcqIssCard @sTerminalID

IF @iDefaultValue IS NULL
	SET @iDefaultValue=0

SELECT IssuerTagId, IssuerTagValue
INTO #tempIss
FROM tbProfileIssuer WITH (NOLOCK)
WHERE TerminalId=@sTerminalID AND IssuerTag=@sTag
AND IssuerName IN (
	SELECT IssName FROM dbo.fn_tbAcqIssCard(@sTerminalID) WHERE AcqName=@sAcquirerName
)
AND IssuerTagValue <> @iDefaultValue

IF @@rowcount = 1
BEGIN
	SELECT @iValue = IssuerTagValue FROM #tempIss
	PRINT 'IF @@rowcount = 1'
END
ELSE
BEGIN
	SET @iValue = @iDefaultValue
	PRINT 'IF @@rowcount > 1'
END