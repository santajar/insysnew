﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: 11 13, 2013
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. pack the response message into ISO format message stlh Upgrade Software EDC
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseInstalMessage]
	@iCheckInit INT,
	@sNII VARCHAR(4),
	@sAddress VARCHAR(4),
	@sMTI VARCHAR(4),
	@sSessionTime VARCHAR(10),
	@sTerminalId VARCHAR(8) OUTPUT,
	@sContent VARCHAR(MAX) OUTPUT, 
	@sTag VARCHAR(MAX) OUTPUT, 
	@sISOMessage VARCHAR(MAX) OUTPUT, 
	@iErrorcode INT OUTPUT, 
	@sAppNameDownload VARCHAR(MAX) OUTPUT, 
	@sAppFileNameDownload VARCHAR(MAX) OUTPUT
AS
SET NOCOUNT ON
DECLARE 
	@sAppName VARCHAR(25),
	@bValidTerminalId BIT,
	@sProcCode VARCHAR(6),
	@sBit11 VARCHAR(MAX),
	@sBit48 VARCHAR(MAX),
	@sBit57 VARCHAR (MAX),
	@sBit58 VARCHAR (10),
	@sEdcSn NVARCHAR (25),
	@iCounterApp INT,
	@sAppName2Download VARCHAR(MAX),
	@sLastDownload VARCHAR(10),
	@iEnableDownload INT,
	@icountCompleteSoftware INT,
	@icountCompleteSoftware2 INT,
	@iBuildNumberEDC INT

IF dbo.IsInitMTI(@sMTI) = 1
BEGIN
	print 'masuk 990020 - Install Message'
	-- get the bit-bit value
	
	EXEC spOtherParseProcCode @sSessionTime, @sProcCode OUTPUT
	EXEC spOtherParseTerminalId @sSessionTime, @sTerminalId OUTPUT
	EXEC spOtherParseBit11 @sSessionTime, @sBit11 OUTPUT
	EXEC spOtherParseBit48InstallMessage @sSessionTime, @sBit48 OUTPUT, @sEdcSn OUTPUT, @sAppName OUTPUT, @sAppNameDownload OUTPUT
	
	PRINT '@sEdcSn : ' + @sEdcSn
	PRINT '@sAppName : ' + @sAppName
	PRINT '@sAppNameDownload : ' + @sAppNameDownload
	PRINT '@sTerminalId : ' + @sTerminalId

	DECLARE @sQueryCount NVARCHAR(MAX),
			@sQueryCount2 NVARCHAR(MAX),
			@sQueryRemoteDownload NVARCHAR(MAX),
			@ParmDefinition NVARCHAR(500),
			@sLinkDatabase NVARCHAR(50)

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag
	WHERE ItemName= 'LinkedRemoteDownloadServer'

	DECLARE @bBool BIT
	EXEC spIsValidTerminalID @sTerminalID, @bBool OUTPUT

	IF @bBool = 1
	BEGIN 
		EXEC spsGetbuildNumber @sTerminalId, @iBuildNumberEDC OUTPUT
		--SET @iBuildNumberEDC = dbo.sGetbuildNumber(@sTerminalId)

		SET @sQueryCount = N'
		SELECT @iParamcountCompleteSoftware=COUNT(*) 
		FROM '+@sLinkDatabase+'tbSoftwareProgress
		WHERE TerminalID = @sParamTerminalId 
			AND SoftwareDownload=@sParamAppNameDownload 
			AND Software=@sParamAppName 
			AND BuildNumber = @iParamBuildNumberEDC
			AND SerialNumber = @iParamEDCsn
			AND Percentage= 100'
		SET @ParmDefinition = N'@iParamcountCompleteSoftware INT OUTPUT, @sParamTerminalId VARCHAR(8),@sParamAppNameDownload VARCHAR(25),@sParamAppName VARCHAR(25),@iParamBuildNumberEDC INT, @iParamEDCsn VARCHAR(25) ';
		EXECUTE sp_executesql @sQueryCount,@ParmDefinition, @sParamTerminalId = @sTerminalID, @sParamAppNameDownload = @sAppNameDownload,@sParamAppName = @sAppName,@iParamBuildNumberEDC = @iBuildNumberEDC, @iParamcountCompleteSoftware= @icountCompleteSoftware OUTPUT, @iParamEDCsn = @sEdcSn;

		PRINT '@icountCompleteSoftware : ' + CONVERT(VARCHAR,@icountCompleteSoftware)

		IF @icountCompleteSoftware >0
		BEGIN
			PRINT 'ipdate1'
			print @sAppNameDownload
			print @sAppName
			PRINT @sTerminalId
			PRINT @sAppNameDownload
			PRINT @sAppName
			PRINT @iBuildNumberEDC
			PRINT @sEdcSn
			print '--'

			SET @sQueryCount2 = N'
			SELECT @iParamcountCompleteSoftware2=COUNT(*) 
			FROM '+@sLinkDatabase+'tbSoftwareProgress
			WHERE TerminalID = @sParamTerminalId 
				AND SoftwareDownload=@sParamAppNameDownload 
				AND Software=@sParamAppName 
				AND BuildNumber = @iParamBuildNumberEDC
				AND SerialNumber = @iParamEDCsn
				AND Percentage= 100 
				AND InstallStatus = '''+'On Progress'+''''
			SET @ParmDefinition = N'@iParamcountCompleteSoftware2 INT OUTPUT, @sParamTerminalId VARCHAR(8),@sParamAppNameDownload VARCHAR(25),@sParamAppName VARCHAR(25),@iParamBuildNumberEDC INT, @iParamEDCsn VARCHAR(25)';
			EXECUTE sp_executesql @sQueryCount2,@ParmDefinition,@iParamcountCompleteSoftware2 = @icountCompleteSoftware2 OUTPUT, @sParamTerminalId = @sTerminalId, @sParamAppNameDownload = @sAppNameDownload,@sParamAppName = @sAppName, @iParamBuildNumberEDC = @iBuildNumberEDC,@iParamEDCsn = @sEdcSn;

			PRINT '@icountCompleteSoftware2 : ' + CONVERT(VARCHAR,@icountCompleteSoftware2)
			
			IF @icountCompleteSoftware2 = 1
			BEGIN
				PRINT '-update-'
				PRINT @sTerminalId
				PRINT @sAppName
				PRINT @sAppNameDownload
				PRINT @sEdcSn
				PRINT @iCounterApp
				PRINT @iBuildNumberEDC
				
				--EXEC spSoftwareProgressUpdate @sTerminalId,@sAppName,@sAppNameDownload, @sEdcSn, @iCounterApp,@iBuildNumberEDC,'Success'
			DECLARE @sQueryUpdate2 NVARCHAR(MAX),
					@ParmDefinitionUpdate NVARCHAR(500)

			SET @sQueryUpdate2 = N'
			UPDATE '+@sLinkDatabase+'tbSoftwareProgress 
			SET InstallStatus = @sParamStatus, FinishInstallTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP)
			WHERE TerminalID = @sParamTerminalID 
				AND SerialNumber = @sParamSerialNumber 
				AND SoftwareDownload=@sParamSoftwareDownload 
				AND Software=@sParamSoftware 
				AND InstallStatus ='''+'On Progress'+'''
				AND BuildNumber = @iParamBuildNumber 
				AND Percentage= 100'
			SET @ParmDefinitionUpdate= N'@sParamStatus VARCHAR(50),@sParamTerminalID VARCHAR(8),@sParamSerialNumber VARCHAR(25),@sParamSoftwareDownload VARCHAR(50),@sParamSoftware VARCHAR(50),@iParamBuildNumber INT'
			EXECUTE sp_executesql @sQueryUpdate2,@ParmDefinitionUpdate, @sParamStatus = 'Success', @sParamTerminalID = @sTerminalID, @sParamSerialNumber = @sEdcSn,@sParamSoftwareDownload = @sAppNameDownload,@sParamSoftware = @sAppName,@iParamBuildNumber = @iBuildNumberEDC;

				SET @sQueryRemoteDownload = N'
					UPDATE '+@sLinkDatabase+'tbProfileTerminalList
					SET RemoteDownload=0
					WHERE TerminalID = @sParamTerminalId'
				SET @ParmDefinition = N'@sParamTerminalId VARCHAR(8)';
				EXECUTE sp_executesql @sQueryRemoteDownload,@ParmDefinition, @sParamTerminalId = @sTerminalId;

			END
			ELSE
			BEGIN
				DECLARE @sQueryGetTime NVARCHAR(MAX),
						@ParmDefinitionGetTime NVARCHAR(500),
						@dtFinishInstallTime Datetime
				
				SET @sQueryGetTime = N'
				SELECT TOP 1 @dtParamFinishInstallTime = FinishInstallTime
				FROM '+@sLinkDatabase+'tbSoftwareProgress 
				WHERE TerminalID = @sParamTerminalID 
					AND SerialNumber = @sParamSerialNumber 
					AND SoftwareDownload=@sParamSoftwareDownload 
					AND Software=@sParamSoftware 
					AND BuildNumber = @iParamBuildNumber 
					AND Percentage= 100
				ORDER BY ID DESC'
				SET @ParmDefinitionGetTime= N'@dtParamFinishInstallTime Datetime Output,@sParamTerminalID VARCHAR(8),@sParamSerialNumber VARCHAR(25),@sParamSoftwareDownload VARCHAR(50),@sParamSoftware VARCHAR(50),@iParamBuildNumber INT'
				EXECUTE sp_executesql @sQueryGetTime,@ParmDefinitionGetTime, @sParamTerminalID = @sTerminalID, @sParamSerialNumber = @sEdcSn,@sParamSoftwareDownload = @sAppNameDownload,@sParamSoftware = @sAppName,@iParamBuildNumber = @iBuildNumberEDC, @dtParamFinishInstallTime = @dtFinishInstallTime OUTPUT;
				
				DECLARE @sQueryUpdate3 NVARCHAR(MAX),
						@ParmDefinitionUpdate3 NVARCHAR(500)

				SET @sQueryUpdate3 = N'
				UPDATE '+@sLinkDatabase+'tbSoftwareProgress 
				SET FinishInstallTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP)
				WHERE TerminalID = @sParamTerminalID 
					AND SerialNumber = @sParamSerialNumber 
					AND SoftwareDownload=@sParamSoftwareDownload 
					AND Software=@sParamSoftware 
					AND InstallStatus ='''+'Success'+'''
					AND BuildNumber = @iParamBuildNumber 
					AND Percentage= 100
					AND FinishInstallTime = @dtParamFinishInstallTime'
				SET @ParmDefinitionUpdate3= N'@sParamTerminalID VARCHAR(8),@sParamSerialNumber VARCHAR(25),@sParamSoftwareDownload VARCHAR(50),@sParamSoftware VARCHAR(50),@iParamBuildNumber INT, @dtParamFinishInstallTime DATETIME'
				EXECUTE sp_executesql @sQueryUpdate3,@ParmDefinitionUpdate3, @sParamTerminalID = @sTerminalID, @sParamSerialNumber = @sEdcSn,@sParamSoftwareDownload = @sAppNameDownload,@sParamSoftware = @sAppName,@iParamBuildNumber = @iBuildNumberEDC, @dtParamFinishInstallTime = @dtFinishInstallTime;
				
					SET @sQueryRemoteDownload = N'
						UPDATE '+@sLinkDatabase+'tbProfileTerminalList
						SET RemoteDownload=0
						WHERE TerminalID = @sParamTerminalId'
					SET @ParmDefinition = N'@sParamTerminalId VARCHAR(8)';
					EXECUTE sp_executesql @sQueryRemoteDownload,@ParmDefinition, @sParamTerminalId = @sTerminalId;
			END

			SET @iErrorCode= 10
			SET @sAppName = 'EDCMessage'
			SET @iCounterApp= 0

			PRINT 'Bit48 : ' + @sBit48
		END
		ELSE IF @icountCompleteSoftware = 0 
		BEGIN
			SET @sContent = 'DOWNLOADSOFTWAREFIRST'
			SET @sBit11 = NULL
			SET @iErrorCode = 3
		END
	END
	ELSE
	BEGIN
		SET @sContent = 'INVALIDTID'
		SET @sBit11 = NULL
		SET @iErrorCode = 4
		
	END
END
ELSE
BEGIN
	SET @sContent = 'INVALIDMTI'
	SET @sBit11 = NULL
	SET @iErrorCode = 5
END

PRINT '-DATA MASUK sGenerateDownloadIsoSend'
PRINT @sTerminalID
PRINT @sProcCode
PRINT @sNII
PRINT @sAddress
PRINT @sBit11
PRINT @sBit48
PRINT @sContent
PRINT @sAppName
PRINT @iCounterApp
PRINT '---'


--SELECT @sISOMessage = dbo.sGenerateDownloadISOSend(@sTerminalID,
--	@sProcCode, @sNII, @sAddress, @sBit11, @sBit48, @sContent, @sAppName, @iCounterApp, 0)

EXEC spsGenerateDownloadISOSend @sTerminalID,@sProcCode, @sNII, @sAddress, @sBit11, @sBit48, @sContent, @sAppName, @iCounterApp, 0, @sISOMessage OUTPUT

PRINT 'DATA GENERATE'
print '@sTerminalID ' + @sTerminalID
PRINT 'Content : ' + ISNULL(@sContent,'null')
PRINT 'Procode : ' + @sProcCode
PRINT 'NII : ' + @sNII
PRINT 'Procode : ' + @sProcCode
PRINT '@sTag : ' + ISNULL(@sTag,'null') 
PRINT '@sAppNameDownload ' + ISNULL(@sAppNameDownload,'null') 
--print '@sBit48 : ' + ISNULL(@sBit48,'null')
print '@sBit11 : ' + ISNULL(@sBit11,'null')
print '@sISOMessage : ' + ISNULL(@sISOMessage,'null')
print '@sAppFilenameDownload : ' + ISNULL(@sAppFilenameDownload,'null')

--SELECT @sTerminalID [TERMINALID]
--	,@sContent [CONTENT]
--	,@sTag [TAG]
--	,@sISOMessage [ISO]
--	,@iErrorCode [ERRORCODE]
--	,@sAppNameDownload [APPNAME]
--	,@sAppFilenameDownload [APPFILENAME]