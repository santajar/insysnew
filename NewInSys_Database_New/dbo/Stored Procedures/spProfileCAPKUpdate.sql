﻿


-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 3, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Update the CAPK on table tbProfileAID
--				2. Modify CAPK for form flexible
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCAPKUpdate]
	@iDatabaseID INT,
	@sCAPKIndex VARCHAR(20),
	@sContent VARCHAR(MAX)
AS
	--IF (OBJECT_ID('tempdb..##OldCAPK') IS NOT NULL)
	--	DROP TABLE ##OldCAPK

	--SELECT CAPKTag, CAPKTagValue
	--INTO ##OldCAPK
	--FROM tbProfileCAPK
	--WHERE DatabaseID = @sDatabaseID AND
	--		CAPKIndex = @sCAPKIndex

	EXEC spProfileCAPKDelete @iDatabaseID, @sCAPKIndex
	EXEC spProfileCAPKInsert @iDatabaseID, @sCAPKIndex, @sContent

	--IF (OBJECT_ID('tempdb..##NewCAPK') IS NOT NULL)
	--	DROP TABLE ##NewCAPK

	--SELECT CAPKTag, CAPKTagValue
	--INTO ##NewCAPK
	--FROM tbProfileCAPK
	--WHERE DatabaseID = @sDatabaseID AND
	--		CAPKIndex = @sCAPKIndex

	--SELECT C.TagName, A.CAPKTagValue OldCAPKValue, b.CAPKTagValue NewCAPKValue
	--FROM ##OldCAPK A FULL OUTER JOIN ##NewCAPK B ON A.CAPKTag = B.CAPKTag
	--	LEFT JOIN TBITEMLISTAID_CAPK C ON A.CAPKTAG = C.TAG
	--WHERE ISNULL(A.CAPKTagValue,'') != ISNULL(B.CAPKTagValue,'')






