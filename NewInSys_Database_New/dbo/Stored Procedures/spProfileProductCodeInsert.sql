﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: SEPT 20, 2014
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Insert PRODUCT CODE
-- =============================================
CREATE PROCEDURE [dbo].[spProfileProductCodeInsert]
	@iDatabaseID INT,
	@sProductCode VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
	DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfileProductCode WITH (NOLOCK)
WHERE ProductCodeTag IN ('KP01','KP001') AND
		ProductCodeTagValue = @sProductCode AND
		DatabaseID = @iDatabaseID

IF @iCountID =0
BEGIN
	IF (dbo.isTagLength4(@iDatabaseID)= 4)
	BEGIN
		INSERT INTO tbProfileProductCode(
			[DatabaseId],
			[ProductCode],
			[ProductCodeTag],
			[ProductCodeLengthOfTagLength],
			[ProductCodeTagLength],
			[ProductCodeTagValue]
			)
		SELECT 
			@iDatabaseID [DatabaseId],
			[Name] [ProductCode],
			Tag [ProductCodeTag],
			LEN(TagLength) [ProductCodeLengthOfTagLength],
			TagLength [ProductCodeTagLength],
			TagValue [ProductCodeTagValue]
		FROM dbo.fn_tbProfileTLV(@sProductCode,@sContent)
	END
	ELSE
	BEGIN
		INSERT INTO tbProfileProductCode(
			[DatabaseId],
			[ProductCode],
			[ProductCodeTag],
			[ProductCodeLengthOfTagLength],
			[ProductCodeTagLength],
			[ProductCodeTagValue]
			)
		SELECT 
			@iDatabaseID [DatabaseId],
			[Name] [ProductCode],
			Tag [ProductCodeTag],
			LEN(TagLength) [ProductCodeLengthOfTagLength],
			TagLength [ProductCodeTagLength],
			TagValue [ProductCodeTagValue]
		FROM dbo.fn_tbProfileTLV5(@sProductCode,@sContent)
	END
END
