﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Agt 19, 2013
-- Modify date:
--				1. 
-- Description:	Insert into tbSoftwareProgress
-- =============================================

CREATE procedure [dbo].[spSoftwareProgressUpdate]
@sTerminalID varchar(8),
@sSoftware varchar(50),
@sSoftwareDownload varchar(50),
@sSerialNumber varchar(25),
@fIndex float,
@iBuildNumber INT,
@sStatus varchar(50) =null

AS
DECLARE
	@iTotalIndex int,
	@fPercentage float,
	@sEdcGroup varchar(25),
	@sEdcRegion varchar(25),
	@sEdcCity VARCHAR(25),
	@iApppackID INT,
	@iCount INT

SELECT @sEdcGroup = B.GroupRDName,@sEdcRegion =C.RegionRDName,@sEdcCity = D.CityRDName,@iApppackID = F.AppPackId
FROM (
	(SELECT *
	FROM tbListTIDRemoteDownload WITH (NOLOCK)) A
	LEFT JOIN
	(SELECT *
	FROM tbGroupRemoteDownload WITH (NOLOCK)) B
	ON A.IdGroupRD= B.IdGroupRD
	LEFT JOIN
	(SELECT * 
	FROM tbRegionRemoteDownload WITH (NOLOCK)) C
	ON A.IdRegionRD = C.IdRegionRD
	LEFT JOIN
	(SELECT *
	FROM tbCityRemoteDownload WITH (NOLOCK)) D
	ON A.IdCityRD = D.IdCityRD
	LEFT JOIN
	(SELECT * 
	FROM tbScheduleSoftware WITH (NOLOCK)) E
	ON A.ScheduleID = E.ScheduleID
	LEFT JOIN
	(SELECT *
	FROM tbAppPackageEDCList WITH (NOLOCK)) F
	ON A.AppPackID = F.AppPackId
)
WHERE A.TerminalID = @sTerminalID

SELECT @iTotalIndex=COUNT(*) 
FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackId = @iApppackID

SET @fPercentage = @fIndex/(@iTotalIndex-1)*100

PRINT '-masuk progress update'
PRINT @sSerialNumber
PRINT @sSoftwareDownload
PRINT @sSoftware
PRINT @fPercentage
PRINT @fIndex
PRINT @iTotalIndex
PRINT '--'

SELECT @iCount=COUNT(*)
FROM tbSoftwareProgress WITH (NOLOCK)
WHERE InstallStatus = 'On Progress'
	AND TerminalID = @sTerminalID
	AND Software = @sSoftware
	AND SoftwareDownload = @sSoftwareDownload
	AND SerialNumber= @sSerialNumber
	AND BuildNumber = @iBuildNumber

IF @fIndex IS NOT NULL
BEGIN
	IF @iCount = 1
	BEGIN
		UPDATE tbSoftwareProgress 
		SET Percentage = @fPercentage, EndTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP)
		WHERE TerminalID =@sTerminalID 
			AND SerialNumber = @sSerialNumber
			AND Software = @sSoftware  
			AND SoftwareDownload = @sSoftwareDownload 
			AND BuildNumber = @iBuildNumber
			AND InstallStatus = 'On Progress'
	END
	ELSE IF @iCount = 0
	BEGIN
		IF @fIndex=0
		BEGIN
		INSERT INTO tbSoftwareprogress 
				(EdcGroup,
				EdcRegion,
				EdcCity,
				TerminalID,
				Software,
				BuildNumber,
				SoftwareDownload,
				SerialNumber,
				StartTime,
				Percentage,
				InstallStatus)
		VALUES(@sEdcGroup,
			   @sEdcRegion,
			   @sEdcCity,
			   @sTerminalID,
			   @sSoftware,
			   @iBuildNumber,
			   @sSoftwareDownload,
			   @sSerialNumber,
			   CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
			   @fPercentage,
			   'On Progress')
		END
	END
END

IF @sStatus IS NOT NULL
BEGIN
	
	PRINT '-masuk status'
	PRINT @sStatus
	PRINT @sSerialNumber
	PRINT @sSoftwareDownload
	PRINT @sSoftware
	PRINT '---'

	UPDATE tbSoftwareProgress 
	SET InstallStatus = @sStatus, FinishInstallTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP)
	WHERE TerminalID = @sTerminalID 
		AND SerialNumber = @sSerialNumber 
		AND SoftwareDownload=@sSoftwareDownload 
		AND Software=@sSoftware 
		AND InstallStatus ='On Progress'
		AND BuildNumber = @iBuildNumber 
		AND Percentage= 100 
END