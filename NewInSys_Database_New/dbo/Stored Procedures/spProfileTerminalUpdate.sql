USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileTerminalUpdate]    Script Date: 11/2/2017 2:26:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 28, 2010
-- Modify date: 
--				1. Jul 04, 2012, add spInitTerminalInsert
-- Description:	
--				1. Updating the terminal profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalUpdate]
	@sTerminalID VARCHAR(8),
	@sDbID INT,
	@bAllowDownload BIT = 1,
	@bStatus BIT,
	@bEnableIP BIT,
	@sContent VARCHAR(MAX),
	--@bAutoInit BIT = '1',
	@bInitCompress BIT = '0',
	@bRemoteDownload BIT = '0',
	@bLogoPrint BIT ='0',
	@bLogoIdle BIT ='0'

AS
SET NOCOUNT ON

CREATE TABLE #TempTable
(
	TerminalID VARCHAR(8),
	[Name] VARCHAR(50),
	Tag VARCHAR(5),
	[LengthOfTagLength] INT,
	TagLength INT,
	TagValue VARCHAR(MAX)
)

IF (dbo.isTagLength4(@sDbID)=5)
BEGIN

	INSERT INTO #TempTable(TerminalID,[Name],Tag,[LengthOfTagLength] ,TagLength,TagValue)
	SELECT TerminalID,
			[Name],
			Tag,
			LEN(TagLength)[LengthOfTagLength] ,
			TagLength,
			TagValue
	FROM dbo.fn_tbProfileTLV5(@sTerminalID, @sContent)
END
ELSE
BEGIN

	INSERT INTO #TempTable(TerminalID,[Name],Tag,[LengthOfTagLength] ,TagLength,TagValue)
	SELECT TerminalID,
			[Name],
			Tag,
			LEN(TagLength)[LengthOfTagLength] ,
			TagLength,
			TagValue
	FROM dbo.fn_tbProfileTLV(@sTerminalID, @sContent)
END

DECLARE @sFind VARCHAR(5)
DECLARE @sReplace VARCHAR(5)
DECLARE @sContentLog VARCHAR(MAX)
SET @sFind = ''+ CHAR(39) +''
SET @sReplace = ''+CHAR(39)+CHAR(39)+''
SET @sContentLog = REPLACE(@sContent, @sFind, @sReplace)


CREATE TABLE #TEMPTABLEOLDNEW(
	ID int  IDENTITY(1,1) NOT NULL,
	TAG VARCHAR(5),
	ITEMNAME VARCHAR(50),
	OLDVALUE VARCHAR(150),
	NEWVALUE VARCHAR(150)
)

INSERT INTO #TEMPTABLEOLDNEW(TAG,ITEMNAME,OLDVALUE,NEWVALUE)
EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'DE', '', @sContentLog, 1
---------------
declare @sumTag int
declare @i int=1
declare @TAG varchar (5)
declare @Value nvarchar (max)
declare @query nvarchar (MAX)

SELECT @sumTag=(select count (TAG) from #TEMPTABLEOLDNEW)
while (@i<=@sumTag)
begin
	select @TAG=TAG from #TEMPTABLEOLDNEW WHERE ID=@i
	select @Value=NEWVALUE from #TEMPTABLEOLDNEW WHERE ID=@i
	SET @query ='update tbProfileTerminal SET dbo.[tbProfileTerminal].[' +  @TAG + ']=''' + @Value + ''' 
				where TerminalID='''+ @sTerminalID +'''' 
	exec (@query)
	SET @i = @i +  1
end
-----------------

--INSERT INTO [tbProfileTerminal]
--	   ([TerminalID]
--	   ,[TerminalTag]
--	   ,[TerminalLengthOfTagLength]
--	   ,[TerminalTagLength]
--	   ,[TerminalTagValue])
--SELECT TerminalID,
--			Tag,
--			[LengthOfTagLength],
--			TagLength,
--			TagValue	
--FROM #TempTable 
--WHERE Tag COLLATE DATABASE_DEFAULT NOT IN (
--					SELECT TerminalTag 
--					FROM tbProfileTerminal
--					WHERE TerminalID COLLATE DATABASE_DEFAULT = @sTerminalID
--				 )

--UPDATE Terminal 
--SET Terminal.TerminalLengthOfTagLength = Temp.LengthOfTagLength,
--	Terminal.TerminalTagLength = Temp.TagLength,
--	Terminal.TerminalTagValue =  Temp.TagValue
--	--Terminal.TerminalTagValue =  REPLACE(Temp.TagValue, @sFind, @sReplace)
--FROM tbProfileTerminal Terminal, #TempTable Temp
--WHERE Terminal.TerminalID = @sTerminalID AND 
--	  Terminal.TerminalID COLLATE DATABASE_DEFAULT = Temp.[Name] COLLATE DATABASE_DEFAULT AND 
--	  Terminal.TerminalTag COLLATE DATABASE_DEFAULT = Temp.Tag COLLATE DATABASE_DEFAULT

EXEC spProfileTerminalListUpdate @sTerminalID, @sDbID, @bAllowDownload, @bStatus, 
							 @bEnableIP, NULL, @bInitCompress, @bRemoteDownload

IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name IN (N'LogoPrint',N'LogoIdle') AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
BEGIN
	UPDATE tbProfileTerminalList
	SET LogoPrint = @bLogoPrint, LogoIdle = @bLogoIdle
	WHERE TerminalID = @sTerminalID
END

SELECT * FROM #TEMPTABLEOLDNEW
IF @@ROWCOUNT > 0
BEGIN
	EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
	EXEC spProfileTerminalUpdateLastUpdate @sTerminalID
END

DROP TABLE #TempTable
DROP TABLE #TEMPTABLEOLDNEW
GO


