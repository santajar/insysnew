﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 24, 2010
-- Modify date: 
--				1. August 12, 2010, change variable name from @sLastView to @bLastView
-- Description:	
--				1. Get the list profile based on condition
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalListBrowse]
	@sCondition VARCHAR(MAX)=NULL, @sCond VARCHAR(MAX)= NULL
AS
DECLARE 
	@sQuery varchar(MAX)

SET @sQuery = 'SELECT TerminalID,
		DatabaseID,
		AllowDownload,
		StatusMaster,
		LastView,
		EnableIP,
		IpAddress,
		LocationID'
	
	IF EXISTS ( SELECT * FROM SYS.COLUMNS
		WHERE Name = N'AutoInitTimeStamp' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	  )
		SET @sQuery = @sQuery + ', AutoInitTimeStamp'
	ELSE
		SET @sQuery = @sQuery + ', NULL [AutoInitTimeStamp]'

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'InitCompress' AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
		SET @sQuery = @sQuery + ', InitCompress'
	ELSE 
		SET @sQuery = @sQuery + ', 0 [InitCompress],'

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'AppPackID' AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
		SET @sQuery= @sQuery + ', AppPackID '
	ELSE 
		SET @sQuery = @sQuery + ','''' [AppPackID]'

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'RemoteDownload' AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
		SET @sQuery= @sQuery + ', RemoteDownload '
	ELSE 
		SET @sQuery = @sQuery + ','''' [RemoteDownload]'

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'LogoPrint' AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
		SET @sQuery= @sQuery + ', LogoPrint '
	ELSE 
		SET @sQuery = @sQuery + ','''' [LogoPrint]'

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'LogoIdle' AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
		SET @sQuery= @sQuery + ', LogoIdle '
	ELSE 
		SET @sQuery = @sQuery + ','''' [LogoIdle]'


		

		IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'LogoMerchant' AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
		SET @sQuery= @sQuery + ', LogoMerchant '
	ELSE 
		SET @sQuery = @sQuery + ','''' [LogoMerchant]'

		IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'LogoReceipt' AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
		SET @sQuery= @sQuery + ', LogoReceipt '
	ELSE 
		SET @sQuery = @sQuery + ','''' [LogoReceipt]'


		
	SET @sQuery = @sQuery + ' FROM tbProfileTerminalList WITH (NOLOCK) '
IF @sCondition IS NOT NULL
	EXEC (@sQuery+ @sCondition)
ELSE IF @sCond IS NOT NULL
	EXEC (@sQuery+ @sCond)
ELSE
	EXEC (@sQuery)

