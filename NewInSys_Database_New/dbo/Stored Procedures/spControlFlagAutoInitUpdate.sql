﻿



-- ==============================================================
-- Description:	Updating control flag for InitTimeOut and MaxConn
-- ==============================================================

CREATE PROCEDURE [dbo].[spControlFlagAutoInitUpdate]
	@sInitTimeOut VARCHAR(50),
	@sMaxConn VARCHAR(50),
	@sInitTimeOutFlag VARCHAR(64) = '',
	@sMaxConnFlag VARCHAR(64) = ''
AS
BEGIN
	SET NOCOUNT ON
	--CREATE TABLE #tbAutoInit
	--(
	--	ItemName VARCHAR(50),
	--   OldValue VARCHAR(64),
	--   NewValue VARCHAR(64)
	--)
	
	--INSERT INTO #tbAutoInit(ItemName,OldValue,NewValue)
	--EXEC sp_tbAutoInitFlag @sInitTimeOut, @sMaxConn, @sInitTimeOutFlag, @sMaxConnFlag
	
	SELECT * FROM dbo.fn_tbAutoInitFlag (@sInitTimeOut, @sMaxConn, @sInitTimeOutFlag, @sMaxConnFlag)
	
	EXEC spControlFlagUpdate @sInitTimeOut, @sInitTimeOutFlag
	EXEC spControlFlagUpdate @sMaxConn, @sMaxConnFlag
END




