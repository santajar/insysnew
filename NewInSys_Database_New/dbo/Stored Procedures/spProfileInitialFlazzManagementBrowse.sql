﻿-- =============================================
-- Author:		Tobias Setyo
-- Create date: Nov 14, 2016
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the content of Initial Flazz Management table
-- =============================================
CREATE PROCEDURE [dbo].[spProfileInitialFlazzManagementBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery='
	SELECT 
		InitialFlazzName,
		DatabaseID,
		InitialFlazzTag as Tag,
		InitialFlazzTagLength,
		InitialFlazzTagValue as Value
	FROM tbProfileInitialFlazz WITH (NOLOCK) '
	SET @sQuery = @sQuery + ISNULL(@sCondition,'') + ' ORDER BY InitialFlazzName, InitialFlazzTag'
EXEC (@sQuery)