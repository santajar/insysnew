﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUserLoginUpdateLastModifyPassword]
	@sUserID VARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE tbUserLogin
	SET LastModifyPassword = GETDATE()
	WHERE UserID = @sUserID
	
END