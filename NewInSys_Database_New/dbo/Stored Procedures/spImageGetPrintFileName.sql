﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spImageGetPrintFileName]
@sTerminalID VARCHAR(10),	@sLogo VARCHAR (100)

AS
BEGIN
	SET NOCOUNT ON;
	SELECT TerminalTagValue
	FROM tbProfileTerminalList A
	JOIN tbItemList B
	ON A.DatabaseID = B.DatabaseID
	JOIN tbProfileTerminal C
	ON A.TerminalID = C.TerminalID
		AND B.Tag=C.TerminalTag
	WHERE A.TerminalID = '' + @sTerminalID +''
		AND B.FormID = '1'
		--AND B.ItemName = 'Logo Print'
		AND B.ItemName = '' + @sLogo +''


--	@sTerminalID VARCHAR(8)
--AS
--BEGIN
--	SET NOCOUNT ON;

--	SELECT TerminalTagValue
--	FROM tbProfileTerminalList A
--	JOIN tbItemList B
--	ON A.DatabaseID = B.DatabaseID
--	JOIN tbProfileTerminal C
--	ON A.TerminalID = C.TerminalID
--		AND B.Tag=C.TerminalTag
--	WHERE A.TerminalID = @sTerminalID
--		AND B.FormID = '1'
--		AND B.ItemName = 'Logo Print'

END