﻿
-- =============================================  
-- Author:  Christianto  
-- Create date: Apr 15, 2012  
-- Description: view GPRS list  
-- =============================================  
CREATE PROCEDURE [dbo].[spGPRSListBrowse]  
 @sDatabaseId VARCHAR(3) = ''  
AS  
SELECT GPRSTagID,  
	--DatabaseId,  
	GPRSName  
FROM dbo.fn_viewGRPSId()  
WHERE DatabaseId = @sDatabaseId--case when @sDatabaseId = '' then DatabaseId else @sDatabaseId end