﻿CREATE PROCEDURE [dbo].[spProfileIssuerDelete]
	@sTerminalID VARCHAR(8),
	@sIssuer VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;

	EXEC spProfileIssuerHistoryInsert @sTerminalID,@sIssuer

    DELETE FROM TbProfileIssuer
	WHERE
		TerminalID = @sTerminalID AND
		IssuerName = @sIssuer

	EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

END
