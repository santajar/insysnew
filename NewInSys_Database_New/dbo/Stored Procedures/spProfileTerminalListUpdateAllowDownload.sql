﻿

CREATE PROCEDURE [dbo].[spProfileTerminalListUpdateAllowDownload]
	@iDatabaseId INT = NULL,
	@sTerminalID VARCHAR(8) = NULL
AS
	DECLARE @sStmt VARCHAR(300)
	SET @sStmt = 'UPDATE tbProfileTerminalList
				  SET AllowDownload = 1'

	--IF EXISTS ( SELECT * FROM SYS.COLUMNS
	--		WHERE Name = N'AutoInitTimeStamp' AND
	--		Object_ID = Object_ID (N'tbProfileTerminalList')
	--	  )
	--	SET @sStmt = @sStmt + ', AutoInitTimeStamp = GETDATE()'

	SET @sStmt = @sStmt +' WHERE'

	IF @sTerminalID IS NOT NULL
		SET @sStmt = @sStmt + ' TerminalID = ''' + @sTerminalID + ''''

	IF @sTerminalID IS NOT NULL AND 
		@iDatabaseId IS NOT NULL
		SET @sStmt = @sStmt +' AND'
		
	IF  @iDatabaseId IS NOT NULL
		SET @sStmt = @sStmt + ' DatabaseID = ' + CONVERT(VARCHAR, @iDatabaseId)
	
	EXEC (@sStmt)



