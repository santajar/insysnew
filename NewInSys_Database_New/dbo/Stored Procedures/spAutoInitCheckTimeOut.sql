﻿CREATE PROCEDURE [dbo].[spAutoInitCheckTimeOut]
AS
	SET NOCOUNT ON
	DECLARE @iTimeOutLength INT, @iCurrConn INT, @iTimeOut INT
	DECLARE @ConnTable TABLE( ItemName VARCHAR(50), 
							  Flag VARCHAR(10)
							)

	INSERT INTO @ConnTable(ItemName,Flag)
	EXEC spControlFlagBrowseItem 'AutoInitSumCurrConn'

	SELECT @iCurrConn = Flag FROM @ConnTable
	
	--print '@iCurrConn : '+ cast(@iCurrConn as varchar)

	DECLARE @TimeOutTable TABLE ( ItemName VARCHAR(50), 
								  Flag varchar(10))

	INSERT INTO @TimeOutTable(ItemName,Flag)
	EXEC spControlFlagBrowseItem 'AutoInitTimeOut'

	SELECT @iTimeOutLength = Flag FROM @TimeOutTable

--	print '@iTimeOutLength ' + cast(@iTimeOutLength as varchar)
--	print dbo.sStringYYMMDDToDateTime('000000000000')

	SELECT @iTimeOut = COUNT(TerminalID)
	FROM tbAutoInitLog WITH (NOLOCK)
	WHERE DATEDIFF(second,dbo.sStringYYMMDDToDateTime(LastInitTime),GETDATE()) > @iTimeOutLength

	-- print '@iTimeOut ' + cast(@iTimeOut as varchar)

	IF @iCurrConn > 0
	BEGIN
		SET @iCurrConn = (@iCurrConn - @iTimeOut)
		EXEC spControlFlagUpdate 'AutoInitSumCurrConn', @iCurrConn
	END
