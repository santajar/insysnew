﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 6, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. ????
-- =============================================
CREATE PROCEDURE [dbo].[spDataUploadCreateTable]
	@sTableName VARCHAR(MAX),
	@sColumnCollection VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt =
		'CREATE TABLE [dbo].[' + @sTableName + '](
			' + @sColumnCollection + '
		) ON [PRIMARY]'
	EXEC (@sStmt)
END
