﻿
CREATE PROCEDURE [dbo].[spOtherParseBit48InitFlazz]
	@sSessionTime VARCHAR(MAX),
	@sAppName VARCHAR(MAX) OUTPUT
AS
DECLARE @sSqlStmt NVARCHAR(MAX)
	DECLARE @sBit48 VARCHAR(MAX)

	SET @sSqlStmt = N'SELECT @sBit48=BitValue FROM ##tempBitMap' + @sSessionTime + 
		' WHERE BitId=48'
	EXEC sp_executesql @sSqlStmt,N'@sBit48 VARCHAR(MAX) OUTPUT',@sBit48=@sBit48 OUTPUT

	-- Format Bit 58 : 
		--Length : 2 byte
		--Channel ID : 2 byte
		--Len Channel ID : 2 byte
		--Issuer ID : 3 byte
		--PSAM ID : 4 byte
		--Exp Date : 3 byte
		--Blk Date : 3 byte
		--Act Date : 3 byte
		--Temporary : 16 byte
		--Random No : 4 byte

	DECLARE @sPSAMID VARCHAR(50)
	SET @sPSAMID = SUBSTRING (@sBit48, 19, 8)
	PRINT 'PSAM : ' + @sPSAMID
	EXEC spInitFlazzUpdate @sSessionTime, @sPSAMID
	SET @sAppName = 'BCAInitFlazz'
