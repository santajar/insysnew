﻿CREATE PROCEDURE [dbo].[spRD_GroupDetailInsert]
	@iGroupID INT,
	@sMID VARCHAR(15)
AS
SELECT *
INTO #rd_groupdetail
FROM tbRD_GroupDetail WITH(NOLOCK)
WHERE GROUP_ID=@iGroupID AND [Enabled]=1

SELECT DISTINCT TerminalID, @sMID MID, @iGroupID GROUPID
INTO #rd_groupdetail_tid
FROM dbo.tbProfileAcquirer WITH(NOLOCK)
WHERE TerminalID NOT IN (SELECT TerminalID FROM #rd_groupdetail)
	AND AcquirerTag IN ('AA04','AA004') AND AcquirerTagValue=@sMID

INSERT INTO tbRD_GroupDetail(TERMINALID,MID,GROUP_ID)
SELECT TerminalID,MID,GROUPID
FROM #rd_groupdetail_tid

UPDATE tbRD_GroupDetail
SET [Enabled]=0
WHERE TERMINALID IN (SELECT TERMINALID FROM #rd_groupdetail_tid)
	AND GROUP_ID <> @iGroupID
