﻿


CREATE PROCEDURE [dbo].[spProfileLoyPoolListBrowse] 
	@sDatabaseId VARCHAR(2)
AS
	SELECT DISTINCT LoyPoolName [Name],
			DatabaseID			
	FROM tbProfileLoyPool WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId 

