﻿CREATE PROCEDURE [dbo].[spReportFiturAcquirer]
	@sTerminalID VARCHAR(8),
	@sAcqName VARCHAR(25)
AS
DECLARE @sTID VARCHAR(8),
	@sMID VARCHAR(15),
	@sT1 VARCHAR(15),
	@sT2 VARCHAR(15),
	@sManualEntry BIT,
	@sOffline BIT,
	@sAdjust BIT,
	@sRefund BIT,
	@sCardVer BIT
 
SET @sTID = dbo.sReportFiturAcquirerTagValue(@sTerminalID,@sAcqName,'AA05')
SET @sMID = dbo.sReportFiturAcquirerTagValue(@sTerminalID,@sAcqName,'AA04')
SET @sT1 = dbo.sReportFiturAcquirerTagValue(@sTerminalID,@sAcqName,'AA06')
SET @sT2 = dbo.sReportFiturAcquirerTagValue(@sTerminalID,@sAcqName,'AA07')
EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE07',0,@sManualEntry OUTPUT
EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE11',0,@sOffline OUTPUT
EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE12',0,@sAdjust OUTPUT
EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE13',0,@sRefund OUTPUT
EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE14',0,@sCardVer OUTPUT



SELECT @sTerminalID TerminalID, 
	@sAcqName AcqName, 
	@sTID TID, 
	@sMID MID, 
	@sT1 T1, 
	@sT2 T2,
	@sManualEntry ManualEntry,
	@sOffline iOffline,
	@sAdjust Adjust,
	@sRefund Refund,
	@sCardVer CardVer
INTO #tbFiturAcquirer

--IF OBJECT_ID('TempDB..##tbFiturAcquirer') IS NOT NULL
	DROP TABLE #tbFiturAcquirer