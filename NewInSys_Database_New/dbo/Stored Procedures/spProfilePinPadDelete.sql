﻿/*
-- Author		: Tobias SETYO
-- Created Date	: Oct 8, 2013
-- Modify Date	: 1. <mmm dd, yyyy>, <description>
-- Description	: DELETE from tbProfilePinPad
*/
CREATE PROCEDURE spProfilePinPadDelete
	@iDatabaseID INT,
	@sPinPadName VARCHAR(23),
	@bDeleteData BIT = 1
AS
DELETE tbProfilePinPad
WHERE DatabaseID=@iDatabaseID AND PinPadName=@sPinPadName