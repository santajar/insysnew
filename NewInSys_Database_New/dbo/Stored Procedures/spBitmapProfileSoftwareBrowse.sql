﻿
CREATE PROCEDURE [dbo].[spBitmapProfileSoftwareBrowse]
AS
	SET NOCOUNT ON
SELECT a.DatabaseId,
	a.SoftwareName,
	b.AllowCompress,
	b.BitMap,
	b.StandartISO8583,
	b.SentBit48
FROM tbProfileSoftware a WITH (NOLOCK) JOIN tbBitMap b WITH (NOLOCK) ON a.SoftwareName=b.ApplicationName