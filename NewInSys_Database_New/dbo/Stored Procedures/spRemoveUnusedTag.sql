﻿CREATE PROCEDURE [dbo].[spRemoveUnusedTag]
	@sName VARCHAR(50)=NULL, @iFormID INT, @iDatabaseID INT
AS
SELECT Tag 
INTO #ItemList
FROM tbItemList
WHERE DatabaseID=@iDatabaseID and FormID=@iFormID

IF @iFormID = 1 
BEGIN 
	SELECT * INTO #deleteListDE
	FROM tbProfileTerminal 
	WHERE TerminalID=@sName AND TerminalTag NOT IN (SELECT Tag FROM #ItemList)
	
	DELETE FROM tbProfileTerminal
	WHERE TerminalID=@sName AND TerminalTag IN (SELECT TerminalTag FROM #deleteListDE)
END
ELSE IF @iFormID = 2
BEGIN 
	SELECT * INTO #deleteListAA
	FROM tbProfileAcquirer 
	WHERE TerminalID=@sName AND AcquirerTag NOT IN (SELECT Tag FROM #ItemList)
	
	DELETE FROM tbProfileAcquirer
	WHERE TerminalID=@sName AND AcquirerTag IN (SELECT AcquirerTag FROM #deleteListAA)
END
ELSE IF @iFormID = 3
BEGIN 
	SELECT * INTO #deleteListAE
	FROM tbProfileIssuer 
	WHERE TerminalID=@sName AND IssuerTag NOT IN (SELECT Tag FROM #ItemList)
	
	DELETE FROM tbProfileIssuer
	WHERE TerminalID=@sName AND IssuerTag IN (SELECT IssuerTag FROM #deleteListAE)
END
ELSE IF @iFormID = 4
BEGIN 
	SELECT * INTO #deleteListAC
	FROM tbProfileCard
	WHERE CardID = (SELECT CardID FROM tbProfileCardList WHERE DatabaseID=@iDatabaseID) 
		AND CardTag NOT IN (SELECT Tag FROM #ItemList)
	
	DELETE FROM tbProfileCard
	WHERE CardID = (SELECT CardID FROM tbProfileCardList WHERE DatabaseID=@iDatabaseID) 
		AND CardTag IN (SELECT CardTag FROM #deleteListAC)
END
