﻿CREATE PROCEDURE [dbo].[spReplaceTIDLogBrowse]
	@sCondition VARCHAR(5000) = NULL
AS
	DECLARE @sSql VARCHAR(1000)
	SET @sSql = ' SELECT Date, 
						UserId, 
						OldTID,
						NewTID,
						Status
				  FROM tbReplaceTIDLog WITH (NOLOCK) '

	EXEC (@sSql + @sCondition)

