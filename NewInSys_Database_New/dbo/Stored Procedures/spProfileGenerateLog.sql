USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileGenerateLog]    Script Date: 11/2/2017 2:25:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spProfileGenerateLog]
		@iDbID INT,
		@sTID VARCHAR(8),
		@sTag VARCHAR(3),
		@sAcqIssName VARCHAR(50),
		@sContent VARCHAR(MAX),
		@isEdit BIT
AS
DECLARE @sStmt VARCHAR(MAX),
		@sTableName VARCHAR(20),
		@cols AS NVARCHAR(MAX)

IF @sTag = 'DE'
	SET @sTableName = 'Terminal'
ELSE IF @sTag = 'AA'
	SET @sTableName = 'Acquirer'
ELSE IF @sTag = 'AE'
	SET @sTableName = 'Issuer'
ELSE 
	SET @sTableName = ''

IF (@sTag <> '')
	BEGIN
		IF @isEdit = 0
		BEGIN
			IF (@sTag='DE')
				BEGIN
				SET @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=1 and DatabaseID=@iDbID
                    group by tag,ItemID
                    order by ItemID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')
		--print @cols
				--statement
				SET @sStmt = '
				select u.[Tag],il.ItemName,u.[TagValue]
				from tbProfileTerminal s with(nolock)
					unpivot
					(
						 [TagValue] 
						For Tag in ('+ @cols +') 
					) as u
					join tbProfileTerminalList tl with(nolock) on u.TerminalID=tl.TerminalID
					join tbItemList il with(nolock) on tl.DatabaseID=il.DatabaseID
				where u.TerminalID='''+ @sTID +''' and u.Tag=il.Tag
			
				union
			
				select m.Tag,il2.ItemName,m.TagValue
				from MasterProfile m
					join tbItemList il2 with(nolock) on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
					join tbProfileTerminalList tl2 with(nolock)	on tl2.DatabaseID=il2.DatabaseID 
				where tl2.TerminalID='''+ @sTID +''' and m.FormID=1 
					and m.Tag not in ( select u.[Tag]
					from tbProfileTerminal s with(nolock)
						unpivot
						(
							[TagValue] 
							For Tag in ('+ @cols +') 
						) as u 
					where u.TerminalID='''+ @sTID +''')
				order by Tag'
				EXEC (@sStmt)
				END
			IF (@sTag='AA')
				BEGIN
				SELECT @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=2 and DatabaseID=@iDbID
                    group by tag,ItemID
                    order by ItemID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')
				--statement
				SET @sStmt = '
				select u.[tag],u.[AcquirerName],il.ItemName,u.[TagValue]
				from tbProfileAcquirer a with(nolock)
				unpivot
					(
						[TagValue]
						For Tag in ('+ @cols +')
					) as u
					join tbProfileTerminalList tl with(nolock)
					on u.TerminalID=tl.TerminalID
					join tbItemList il with(nolock)
					on tl.DatabaseID=il.DatabaseID
				where u.TerminalID='''+ @sTID +''' and u.Tag=il.Tag and u.AcquirerName='''+ @sAcqIssName +'''
			
				union
				
				select m.Tag,m.Name[AcquirerName],il2.ItemName,m.TagValue
				from MasterProfile m
					join tbItemList il2 with(nolock)
					on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
					join tbProfileTerminalList tl2 with(nolock)
					on tl2.DatabaseID=il2.DatabaseID
					join tbProfileAcquirer a with(nolock)
					on tl2.TerminalID=a.TerminalID and a.AcquirerName=m.Name
				where tl2.TerminalID='''+ @sTID +''' and m.FormID=2 and m.Name='''+ @sAcqIssName +'''
				
					and m.Tag not in ( select u.[Tag]
					from tbProfileAcquirer a
					unpivot
					(
						[TagValue]
						For Tag in ('+ @cols +')
					) as u
				where u.TerminalID='''+ @sTID +''' and u.AcquirerName='''+ @sAcqIssName +''') 
				order by Tag'
				EXEC (@sStmt)
				END
			IF (@sTag='AE')
				BEGIN
				SELECT @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=3 and DatabaseID=@iDbID
                    group by tag,ItemID
                    order by Tag
					FOR XML PATH(''), TYPE
					).value('.', 'NVARCHAR(MAX)') 
					,1,1,'')
				SET @sStmt = '
				select u.[tag],il.ItemName,u.[TagValue]
				from tbProfileIssuer a
				unpivot
					(
						[TagValue]
						For Tag in ('+ @cols +')
					) as u
					join tbProfileTerminalList tl with(nolock)
					on u.TerminalID=tl.TerminalID
					join tbItemList il 
					on tl.DatabaseID=il.DatabaseID
				where u.TerminalID='''+ @sTID +''' and u.Tag=il.Tag and u.IssuerName='''+@sAcqIssName+'''

				union
			
				select m.Tag,il2.ItemName,m.TagValue
				from MasterProfile m
					join tbItemList il2
					on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
					join tbProfileTerminalList tl2
					on tl2.DatabaseID=il2.DatabaseID
					join tbProfileIssuer a
					on tl2.TerminalID=a.TerminalID and a.IssuerName=m.Name
				where tl2.TerminalID='''+ @sTID +''' and m.FormID=3 and m.Name='''+@sAcqIssName+'''
					and m.Tag not in ( select u.[Tag]
					from tbProfileIssuer a
					unpivot
					(
						[TagValue]
						For Tag in ('+ @cols +')
					) as u
				where u.TerminalID='''+ @sTID +'''and u.IssuerName='''+@sAcqIssName+''') 
				order by Tag'

				EXEC (@sStmt)
				END
		END
	--END
	ELSE
	
	BEGIN
		IF (@sTag='DE')
				BEGIN
				SET @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=1 and DatabaseID=@iDbID
                    group by tag,ItemID
                    order by ItemID
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')
		
				--statement
				SET @sStmt = 'SELECT A.Tag, ItemName, dbo.Bit2Str(' + CONVERT(VARCHAR,@iDbID) + ',  A.Tag,A.TagValue) OldValue, '+
											  ' dbo.Bit2Str(' + CONVERT(VARCHAR,@iDbID) + ',  B.Tag, ' 
											  + 'B.TagValue) NewValue
					
				FROM (select u.[Tag],il.ItemName,u.[TagValue]
				from tbProfileTerminal s with(nolock)
					unpivot
					(
						 [TagValue] 
						For Tag in ('+ @cols +') 
					) as u
					join tbProfileTerminalList tl with(nolock)
					on u.TerminalID=tl.TerminalID
					join tbItemList il with(nolock)
					on tl.DatabaseID=il.DatabaseID
				where u.TerminalID='''+ @sTID +''' and u.Tag=il.Tag
			
				union
			
				select m.Tag,il2.ItemName,m.TagValue
				from MasterProfile m
					join tbItemList il2 with(nolock)
					on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
					join tbProfileTerminalList tl2 with(nolock)
					on tl2.DatabaseID=il2.DatabaseID 
				where tl2.TerminalID='''+ @sTID +''' and m.FormID=1 
					and m.Tag not in ( select u.[Tag]
					from tbProfileTerminal s with(nolock)
						unpivot
						(
							[TagValue] 
							For Tag in ('+ @cols +') 
						) as u 
					where u.TerminalID='''+ @sTID +''')
				'
				IF (dbo.isTagLength4(@iDbID)=4)
				BEGIN
				SET @sStmt = @sStmt + '
									) A
								FULL OUTER JOIN 
									( SELECT Tag, TagValue
									  FROM dbo.fn_tbProfileTLV(''' + @sTID + ''', '''+ @sContent + ''')
									) B
								 ON A.Tag = B.Tag
								 WHERE A.TagValue <> B.TagValue'					
				END	
				ELSE
				BEGIN
				SET @sStmt = @sStmt + '
						) A
					FULL OUTER JOIN 
						( SELECT Tag, TagValue
							FROM dbo.fn_tbProfileTLV5(''' + @sTID + ''', '''+ @sContent + ''')
						) B
						ON A.Tag = B.Tag
					WHERE A.TagValue <> B.TagValue'
				END
		
				EXEC (@sStmt)
				END
		IF (@sTag='AA')
		
			BEGIN
			SELECT @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
				from tbItemList
				where FormID=2 and DatabaseID=@iDbID
				group by tag,ItemID
				order by ItemID
		FOR XML PATH(''), TYPE
		).value('.', 'NVARCHAR(MAX)') 
	,1,1,'')
			--statement
			SET @sStmt = '
			SELECT A.Tag,A.AcquirerName,ItemName, dbo.Bit2Str(' + CONVERT(VARCHAR,@iDbID) + ',  A.Tag,A.TagValue) OldValue, '+
											  ' dbo.Bit2Str(' + CONVERT(VARCHAR,@iDbID) + ',  B.Tag, ' 
											  + 'B.TagValue) NewValue
					
			FROM (select u.[tag],u.[AcquirerName],il.ItemName,u.[TagValue]
			from tbProfileAcquirer a with(nolock)
			unpivot
				(
					[TagValue]
					For Tag in ('+ @cols +')
				) as u
				join tbProfileTerminalList tl with(nolock)
				on u.TerminalID=tl.TerminalID
				join tbItemList il with(nolock)
				on tl.DatabaseID=il.DatabaseID
			where u.TerminalID='''+ @sTID +''' and u.Tag=il.Tag and u.AcquirerName='''+ @sAcqIssName +'''
			
			union
				
			select m.Tag,m.Name[AcquirerName],il2.ItemName,m.TagValue
			from MasterProfile m
				join tbItemList il2 with(nolock)
				on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
				join tbProfileTerminalList tl2 with(nolock)
				on tl2.DatabaseID=il2.DatabaseID
				join tbProfileAcquirer a with(nolock)
				on tl2.TerminalID=a.TerminalID and a.AcquirerName=m.Name
			where tl2.TerminalID='''+ @sTID +''' and m.FormID=2  and m.Name='''+ @sAcqIssName +'''
				 
				and m.Tag not in ( select u.[Tag]
				from tbProfileAcquirer a with(nolock)
				unpivot
				(
					[TagValue]
					For Tag in ('+ @cols +')
				) as u
			where u.TerminalID='''+ @sTID +''' and u.AcquirerName='''+ @sAcqIssName +''')'
		
			IF (dbo.isTagLength4(@iDbID)=4)
				BEGIN
				SET @sStmt = @sStmt + '
									) A
								FULL OUTER JOIN 
									( SELECT Tag, TagValue
									  FROM dbo.fn_tbProfileTLV(''' + @sTID + ''', '''+ @sContent + ''')
									) B
								 ON A.Tag = B.Tag
								 WHERE A.TagValue <> B.TagValue'		
				END
				ELSE
				BEGIN
				SET @sStmt = @sStmt + '
						) A
					FULL OUTER JOIN 
						( SELECT Tag, TagValue
							FROM dbo.fn_tbProfileTLV5(''' + @sTID + ''', '''+ @sContent + ''')
						) B
						ON A.Tag = B.Tag
					WHERE A.TagValue <> B.TagValue'
				END
			EXEC (@sStmt)
			END
		IF (@sTag='AE')
		print 'satu1'
			BEGIN
			SELECT @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
				from tbItemList
				where FormID=3 and DatabaseID=@iDbID
				group by tag,ItemID
				order by ItemID
				FOR XML PATH(''), TYPE
				).value('.', 'NVARCHAR(MAX)') 
				,1,1,'')
			SET @sStmt = 'SELECT A.Tag, ItemName, dbo.Bit2Str(' + CONVERT(VARCHAR,@iDbID) + ',  A.Tag,A.TagValue) OldValue, '+
											  ' dbo.Bit2Str(' + CONVERT(VARCHAR,@iDbID) + ',  B.Tag, ' 
											  + 'B.TagValue) NewValue		
			FROM (select u.[tag],il.ItemName,u.[TagValue]
			from tbProfileIssuer a with(nolock)
			unpivot
				(
					[TagValue]
					For Tag in ('+ @cols +')
				) as u
				join tbProfileTerminalList tl with(nolock)
				on u.TerminalID=tl.TerminalID
				join tbItemList il with(nolock)
				on tl.DatabaseID=il.DatabaseID
			where u.TerminalID='''+ @sTID +''' and u.Tag=il.Tag and u.IssuerName='''+@sAcqIssName+'''

			union
			
			select m.Tag,il2.ItemName,m.TagValue
			from MasterProfile m
				join tbItemList il2 with(nolock)
				on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
				join tbProfileTerminalList tl2 with(nolock)
				on tl2.DatabaseID=il2.DatabaseID
				join tbProfileIssuer a with(nolock)
				on tl2.TerminalID=a.TerminalID and a.IssuerName=m.Name
			where tl2.TerminalID='''+ @sTID +''' and m.FormID=3 and m.Name='''+@sAcqIssName+'''
	  
				and m.Tag not in ( select u.[Tag]
				from tbProfileIssuer a with(nolock)
				unpivot
				(
					[TagValue]
					For Tag in ('+ @cols +')
				) as u
			where u.TerminalID='''+ @sTID +''' and u.IssuerName='''+@sAcqIssName+''')' 
			--order by Name,Tag'
			IF (dbo.isTagLength4(@iDbID)=4)
				BEGIN
				SET @sStmt = @sStmt + '
									) A
								FULL OUTER JOIN 
									( SELECT Tag, TagValue
									  FROM dbo.fn_tbProfileTLV(''' + @sTID + ''', '''+ @sContent + ''')
									) B
								 ON A.Tag = B.Tag
								 WHERE A.TagValue <> B.TagValue'		
				END
			ELSE
				BEGIN
				SET @sStmt = @sStmt + '
						) A
					FULL OUTER JOIN 
						( SELECT Tag, TagValue
							FROM dbo.fn_tbProfileTLV5(''' + @sTID + ''', '''+ @sContent + ''')
						) B
						ON A.Tag = B.Tag
					WHERE A.TagValue <> B.TagValue'
				END
				EXEC (@sStmt)
			END
	END
	END



GO


