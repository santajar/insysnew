USE [NewInSys_BCA-DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_viewAcquirerDetail]    Script Date: 11/2/2017 2:14:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_viewAcquirerDetail](@sTerminalId VARCHAR(8))
RETURNS @viewAcquirerDetail TABLE(
	TerminalId VARCHAR(8),
	Name VARCHAR(15),
	Tag VARCHAR(5),
	ItemName VARCHAR(24),
	TagLength INT,
	LengthOfTagLength INT,
	TagValue VARCHAR(150)
)
AS
BEGIN
Declare @iDbID varchar (4),
		@cols nvarchar(MAX),
		@query nvarchar(max)

set @iDbID=(select DatabaseID 
			from tbProfileTerminalList 
			where TerminalID=@sTerminalId)

SET @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=1 and DatabaseID=@iDbID
                    group by tag,ItemID
                    order by tag
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')
	
	set @query='
INSERT INTO @viewAcquirerDetail
select u.[TerminalID],u.[AcquirerName][Name],u.[tag],il.ItemName,len(u.[TagValue])[TagLength],len(len(u.[TagValue]))[LengthOfTagLength],u.[TagValue]
from tbProfileAcquirer a
unpivot
	(
		[TagValue]
		For Tag in ('+ @cols +')
	) as u
	join tbProfileTerminalList tl
	on u.TerminalID=tl.TerminalID
	join tbItemList il
	on tl.DatabaseID=il.DatabaseID
where u.TerminalID='''+ @sTerminalId +''' and u.Tag=il.Tag

union
--From table MasterProfile
select tl2.TerminalID,a.AcquirerName[Name],m.Tag,il2.ItemName,len(m.TagValue)[TagLength],len(len(m.TagValue))[LengthOfTagLength],m.TagValue
from MasterProfile m
	join tbItemList il2
	on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
	join tbProfileTerminalList tl2 
	on tl2.DatabaseID=il2.DatabaseID
	join tbProfileAcquirer a
	on tl2.TerminalID=a.TerminalID and a.AcquirerName=m.Name
where tl2.TerminalID='''+ @sTerminalId +''' and m.FormID=2 
--compare tag masterProfile vs tbProfileAcquirerNew_2 
	and (select concat(a.AcquirerName,''-'',m.Tag)) not in ( select concat (u.[AcquirerName],''-'',u.[Tag])
	from tbProfileAcquirer a
	unpivot
	(
		[TagValue]
		For Tag in ('+ @cols +')
	) as u
where u.TerminalID='''+ @sTerminalId +''') 
order by Name,Tag'
RETURN
END
GO


