﻿
CREATE PROCEDURE [dbo].[spProfileGPRSUpdate]
	@iDatabaseID INT,
	@sGPRSName VARCHAR(15),
	@sContent VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileGPRSDelete @iDatabaseID, @sGPRSName, 0
	EXEC spProfileGPRSInsert @iDatabaseID, @sGPRSName, @sContent
END

