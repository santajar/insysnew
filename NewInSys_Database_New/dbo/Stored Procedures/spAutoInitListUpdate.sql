﻿
-- =============================================
-- Author		:	Tobias Supriyadi
-- Create date	:	Mar 18, 2014
-- Modify date	:	
-- Description	:	Update tbAutoInitList
-- =============================================
CREATE PROCEDURE [dbo].[spAutoInitListUpdate]
	@sTerminalID VARCHAR(8), 
	@iEnabled INT
AS
	SET NOCOUNT ON
UPDATE tbAutoInitList
SET [Enabled] = @iEnabled, UpdatedDate = GETDATE()
WHERE TerminalID = @sTerminalID

