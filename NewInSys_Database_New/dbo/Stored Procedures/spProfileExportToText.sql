﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Nov 29, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Get all the profile content in 1 long string on defined databaseid
-- =============================================
CREATE PROCEDURE [dbo].[spProfileExportToText] 
	@sCond VARCHAR(MAX),
	@iIsFullTable BIT = 0
AS
SET NOCOUNT ON


IF OBJECT_ID('tbProfileExport','U') IS NOT NULL
	TRUNCATE TABLE tbProfileExport
ELSE 
	CREATE TABLE tbProfileExport(
			TerminalId VARCHAR(8),
			[Content] VARCHAR(MAX)
		)
	

DECLARE @tblTerminal TABLE(
	Id SMALLINT IDENTITY(1,1),
	TerminalId VARCHAR(8),
	DatabaseId SMALLINT,
	AllowDownload BIT,
	StatusMaster BIT,
	LastView DATETIME,
	EnableIP BIT,
	IpAddress VARCHAR(15),
	LocationID BIGINT,
	AutoInitTimeStamp DATETIME,
	InitCompress BIT,
	AppPackID VARCHAR(150),
	RemoteDownload BIT,
	LogoPrint BIT,
	LogoIdle BIT,
	LogoMerchant BIT,
	LogoReceipt BIT)
INSERT INTO @tblTerminal(
	TerminalId,
	DatabaseId,
	AllowDownload,
	StatusMaster,
	LastView,
	EnableIP,
	IpAddress,
	LocationID,
	AutoInitTimeStamp,
--	AutoInit,
	InitCompress,
	AppPackID,
	RemoteDownload,
	LogoPrint,
	LogoIdle,
	LogoMerchant,
	LogoReceipt
	)
EXEC spProfileTerminalListBrowse @sCond

DECLARE @iMaxRow SMALLINT,
	@iIndex SMALLINT
SELECT @iMaxRow=COUNT(*) FROM @tblTerminal
SET @iIndex=1

WHILE @iIndex <= @iMaxRow
BEGIN
	DECLARE @sTerminalId VARCHAR(8)
	SELECT @sTerminalId=TerminalId FROM @tblTerminal
	WHERE Id=@iIndex
	
	DECLARE @sContent VARCHAR(MAX)
	
	IF @iIsFullTable = 0
		EXEC spProfileText @sTerminalId, @sContent=@sContent OUTPUT
	ELSE
		EXEC spProfileTextFull @sTerminalId, @sContent=@sContent OUTPUT
	
	INSERT INTO tbProfileExport
	VALUES (@sTerminalId, @sContent)
	SET @iIndex = @iIndex + 1
END
SELECT * FROM tbProfileExport WITH (NOLOCK)



