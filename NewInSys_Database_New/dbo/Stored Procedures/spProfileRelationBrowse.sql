USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileRelationBrowse]    Script Date: 11/2/2017 2:21:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spProfileRelationBrowse]
	@sTerminalId VARCHAR(8)
AS

	select u.TerminalID,u.Tag[RelationTag],len(len(TagValue))[RelationLengthOfTagLength],len(TagValue)[RelationTagLength],TagValue[RelationTagValue] from 
(
	select ID,TerminalID,TagCard[Tag],ValueCard[TagValue] 
	from tbProfileRelation 

	union all
	select ID,TerminalID,TagIssuer[Tag],ValueIssuer[TagValue]
	from tbProfileRelation 
	
	union all
	select ID,TerminalID,TagAcquirer[Tag],ValueAcquirer[TagValue]
	from tbProfileRelation 	
)u
where u.TerminalID=@sTerminalId
order by u.ID,u.Tag

GO


