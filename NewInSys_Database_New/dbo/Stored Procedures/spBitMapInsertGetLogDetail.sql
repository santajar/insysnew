﻿
CREATE PROCEDURE [dbo].[spBitMapInsertGetLogDetail]
	@sApplicationName VARCHAR(50),
	@sLogDetail VARCHAR(200) OUTPUT
AS
	SET NOCOUNT ON

	--CREATE TABLE #tbBitMap
	--(
	--	ColumnName VARCHAR(50),
	--	ColumnValue VARCHAR(100)
	--)
	--INSERT INTO #tbBitMap
	--EXEC sp_tbBitMap @sApplicationName

	DECLARE @iIndex INT, @iCount INT
	SET @iIndex = 1
	SET @sLogDetail = ''

	--IF (OBJECT_id('Tempdb..##TempTableBitMapLog') IS NOT NULL)
		--DROP TABLE ##TempTableBitMapLog

	--edit from ## to #
	CREATE TABLE #TempTableBitMapLog ( ID INT IDENTITY,
							   ColumnName VARCHAR(50),
							   ColumnValue VARCHAR(100)
							  )
	INSERT INTO #TempTableBitMapLog (ColumnName, ColumnValue)	
	SELECT ColumnName, ColumnValue
	FROM dbo.FN_tbBitMap (@sApplicationName)

	SET @iCount = @@ROWCOUNT

	WHILE (@iIndex <= @iCount)
	BEGIN
		DECLARE @sName VARCHAR(50),
				@sValue VARCHAR(200)

		SELECT @sName = ColumnName,
				@sValue = ColumnValue
		FROM #TempTableBitMapLog
		WHERE ID = @iIndex

		SET @sLogDetail = @sLogDetail + @sName + ' : ' + @sValue + ' \n '
		SET @iIndex = @iIndex + 1
	END

	DROP TABLE #TempTableBitMapLog