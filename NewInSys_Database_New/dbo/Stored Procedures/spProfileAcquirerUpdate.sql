USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileAcquirerUpdate]    Script Date: 11/2/2017 3:42:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 24, 2010
-- Modify	  :
--				1. Jul 04, 2012, add spInitTerminalInsert
-- Description:	
--				1. Updating an acquirer on a profile
--				2. Add Tag Lenght validation
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerUpdate]
@sTerminalID VARCHAR(8),
@sContent VARCHAR(MAX)

AS

	IF (OBJECT_ID('TEMPDB..#TempAcq') IS NOT NULL)
		DROP TABLE #TempAcq
	CREATE TABLE #TempAcq
	(
		TerminalID VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		LengthOfTagLength INT,
		TagLength INT,
		TagValue VARCHAR(150)
	)

	IF (OBJECT_ID('TEMPDB..#TEMPTABLEOLDNEW') IS NOT NULL)
		DROP TABLE #TEMPTABLEOLDNEW
	CREATE TABLE #TEMPTABLEOLDNEW
	(
		ID int  IDENTITY(1,1) NOT NULL,
		TAG VARCHAR(5),
		AcquirerName VARCHAR(5),
		ITEMNAME VARCHAR(50),
		OLDVALUE VARCHAR(150),
		NEWVALUE VARCHAR(150)
	)

	DECLARE @sDbID VARCHAR(5),
			@sAcqName VARCHAR(50)

	declare @sumTagAcq int
	declare @iAcq int=1
	declare @TAGAcq varchar (5)
	declare @AcquirerName varchar (5)
	declare @ValueAcq nvarchar (max)
	declare @queryAcq nvarchar (MAX)

	SET @sDbID = dbo.iDatabaseIdByTerminalId(@sTerminalID)
	IF (dbo.isTagLength4(CAST(@sDbID AS INT)) = 4 )
	BEGIN
		INSERT INTO #TempAcq (TerminalID,[Name],Tag,[LengthOfTagLength],TagLength,TagValue)
		SELECT TerminalID,
				[Name],
				Tag,
				LEN(TagLength) [LengthOfTagLength],
				TagLength,
				TagValue
		FROM fn_tbprofiletlv(@sTerminalID, @sContent)
	END
	ELSE
	BEGIN
		INSERT INTO #TempAcq (TerminalID,[Name],Tag,[LengthOfTagLength],TagLength,TagValue)
		SELECT TerminalID,
				[Name],
				Tag,
				LEN(TagLength) [LengthOfTagLength],
				TagLength,
				TagValue
		FROM fn_tbprofiletlv5(@sTerminalID, @sContent)
	END
	
	--SELECT DISTINCT @sAcqName = [Name] FROM #TempAcq
	set @sAcqName=(select distinct Name from #TempAcq)


	INSERT INTO #TEMPTABLEOLDNEW(TAG,AcquirerName,ITEMNAME,OLDVALUE,NEWVALUE)	
	EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'AA', @sAcqName, @sContent, 1
	-------------------
SELECT @sumTagAcq=(select count (TAG) from #TEMPTABLEOLDNEW)
while (@iAcq<=@sumTagAcq)
begin

	select @TAGAcq=TAG from #TEMPTABLEOLDNEW WHERE ID=@iAcq
	select @ValueAcq=NEWVALUE from #TEMPTABLEOLDNEW WHERE ID=@iAcq
	SET @queryAcq ='update tbProfileAcquirer SET dbo.[tbProfileAcquirer].[' +  @TAGAcq + ']=''' + @ValueAcq + ''' 
				where TerminalID='''+ @sTerminalID +''' and AcquirerName='''+ @sAcqName +'''' 
	exec (@queryAcq)
	SET @iAcq = @iAcq +  1
	
end
	-------------------

	--INSERT INTO [tbProfileAcquirer]
	--	   ([TerminalID]
	--	   ,[AcquirerName]
	--	   ,[AcquirerTag]
	--	   ,[AcquirerLengthOfTagLength]
	--	   ,[AcquirerTagLength]
	--	   ,[AcquirerTagValue])
	--SELECT TerminalID, 
	--		[Name],
	--		Tag,
	--		LengthOfTagLength,
	--		TagLength,
	--		TagValue
	--FROM #TempAcq
	--WHERE Tag COLLATE DATABASE_DEFAULT NOT IN (
	--					SELECT AcquirerTag
	--					FROM tbProfileAcquirer
	--					WHERE TerminalID = @sTerminalID AND 
	--						  AcquirerName = [Name]
	--				 )


	--UPDATE Acquirer SET Acquirer.AcquirerLengthOfTagLength = Temp.LengthOfTagLength,
	--				    Acquirer.AcquirerTagLength = Temp.TagLength,
	--					Acquirer.AcquirerTagValue =  Temp.TagValue
	--FROM tbProfileAcquirer Acquirer, #TempAcq Temp
	--WHERE Acquirer.TerminalID = @sTerminalID AND 
	--	  Acquirer.AcquirerName COLLATE DATABASE_DEFAULT = Temp.[Name] COLLATE DATABASE_DEFAULT AND 
	--	  Acquirer.AcquirerTag COLLATE DATABASE_DEFAULT = Temp.Tag COLLATE DATABASE_DEFAULT

EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
EXEC spProfileTerminalUpdateLastUpdate @sTerminalID


GO


