﻿CREATE PROCEDURE [dbo].[spProfileCurrencyInsert]
	@iDatabaseId BIGINT,
	@iCurrencyId BIGINT,
	@sCurrencyName VARCHAR(5),
	@sContent VARCHAR(300)
AS
	DECLARE @iCountID INT,
			@iCountName INT
	
	SELECT @iCountId = COUNT(*)
	FROM tbProfileCurrency WITH (NOLOCK)
	WHERE DatabaseId = @iDatabaseId AND
			CurrencyTag = 'CR01' AND
			CurrencyTagValue = @iCurrencyId
			
	IF (@iCountId = 0)
	BEGIN
		SELECT @iCountName = COUNT(*)
		FROM tbProfileCurrency WITH (NOLOCK)
		WHERE DatabaseId = @iDatabaseId AND
				CurrencyName = @sCurrencyName
				
		IF (@iCountName = 0)
		BEGIN
			IF (@sContent IS NOT NULL)
			BEGIN
			INSERT INTO tbProfileCurrency (DatabaseId, 
											CurrencyName, 
											CurrencyTag, 
											CurrencyLengthOfTagLength, 
											CurrencyTagLength, 
											CurrencyTagValue)
			SELECT @iDatabaseId,
					@sCurrencyName,
					Tag, 
					LEN(TagLength),
					TagLength, 
					TagValue
			FROM dbo.fn_tbprofiletlv(@sCurrencyName, @sContent)
			
			DECLARE @sDatabaseID VARCHAR(3)
			SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
			EXEC spProfileTerminalUpdateLastUpdateByDbID @sDatabaseID
			END
			ELSE
				SELECT 'Content Is Null. Please Repeat the Process.'
			
		END
		ELSE
			SELECT 'Currency Name has already used. Please choose other Currency Name.'	
	END
	ELSE
		SELECT 'Currency ID has already used. Please choose other Currency ID.'
	
	
	

