USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileTerminalContentBrowse]    Script Date: 11/2/2017 2:11:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 2, 2010
-- Modify date:
--				1.Sept 18, 2012, adding mode 4 for relation
-- Description:	
--				1. view overall profile content
--				2. change Tag length from 4 to 5 
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalContentBrowse]
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX) = NULL,
	@sMode CHAR(1) = NULL
AS
BEGIN
	DECLARE @sSqlStmt VARCHAR(MAX)

	CREATE TABLE #tempTerminal (
		TerminalId VARCHAR(8),
		[Name] VARCHAR(50),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		LengthOfTagLength INT,
		TagValue VARCHAR(200)
	)
	
	IF ISNULL(@sMode,'')='' OR @sMode=0
	BEGIN
		INSERT INTO #tempTerminal (TerminalId,
			[Name],
			Tag,
			ItemName,
			TagLength,
			LengthOfTagLength,
			TagValue )
		exec spViewUnpivotTerminal @sTerminalID
	
	END

	IF ISNULL(@sMode,'')='' OR @sMode=3
	BEGIN
		INSERT INTO #tempTerminal(TerminalId,
			[Name],
			Tag,
			ItemName,
			TagLength,
			LengthOfTagLength,
			TagValue )
		SELECT TerminalId,
			[Name],
			Tag,
			ItemName,
			TagLength,
			LengthOfTagLength,
			TagValue 
		FROM dbo.fn_viewCardDetail(@sTerminalID)
		ORDER BY [Name],Tag
	END
	
	IF ISNULL(@sMode,'')='' OR @sMode=2
	BEGIN
		INSERT INTO #tempTerminal(TerminalId,
			[Name],
			Tag,
			ItemName,
			TagLength,
			LengthOfTagLength,
			TagValue )
		exec spViewUnpivotIssuer @sTerminalID

	END	

	IF ISNULL(@sMode,'')='' OR @sMode=1
	BEGIN
		INSERT INTO #tempTerminal(TerminalId,
			[Name],
			Tag,
			ItemName,
			TagLength,
			LengthOfTagLength,
			TagValue )
		exec spViewUnpivotAcquirer @sTerminalID

	END

	IF ISNULL(@sMode,'')='' OR @sMode=4
	BEGIN
		IF (dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalID))=5)
		BEGIN
			INSERT INTO #tempTerminal(TerminalId,
				Tag,
				ItemName,
				TagLength,
				LengthOfTagLength,
				TagValue )
			SELECT TerminalId, Tag, '',	TagLength, LengthOfTagLength, TagValue 
			FROM fn_viewRelationTLV5(@sTerminalID)
		END
		ELSE
		BEGIN
			INSERT INTO #tempTerminal(TerminalId,
				Tag,
				ItemName,
				TagLength,
				LengthOfTagLength,
				TagValue )
			SELECT TerminalId,
				Tag,
				'',
				TagLength,
				LengthOfTagLength,
				TagValue 
			FROM fn_viewRelationTLV(@sTerminalID)
		END
	END

SET @sSqlStmt = '
		SELECT 
			ROW_NUMBER() OVER(ORDER BY TerminalID) AS [Tag],
			Tag TagId,
			TerminalID,
			[Name],
			ItemName,
			TagLength, 
			LengthOfTagLength,
			TagValue 
		FROM #tempTerminal '

IF @sConditions IS NULL
	EXEC(@sSqlStmt)
ELSE
	EXEC(@sSqlStmt + @sConditions)
END

--IF OBJECT_ID ('TempDb..#tempTerminal', 'U') IS NOT NULL
		DROP TABLE #tempTerminal
GO


