﻿
-- =============================================
-- Author:		Kiki Ariady
-- Create date: Sept 17, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. delete acquire table on terminalid
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerDeleteProcess]
	@sTerminalID VARCHAR(8),
	@sAcquirer VARCHAR(15),
	@sRelation VARCHAR(MAX),
	@sIssuerList VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileAcquirerHistoryinsert @sTerminalID,@sAcquirer
	EXEC spProfileRelationDeleteByAcquirer @sTerminalID, @sRelation
	EXEC spProfileAcquirerDelete @sTerminalID, @sAcquirer
	EXEC spProfileIssuerDeleteByAcq @sTerminalID, @sIssuerList

	EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID 
END
