﻿
CREATE PROCEDURE [dbo].[spProfileGPRSInsert]
	@iDatabaseID INT,
	@sGPRSName VARCHAR(15),
	@sContent VARCHAR(MAX)=NULL
AS
	DECLARE @iCountName INT,
			@iCountID INT

	SELECT @iCountName = COUNT(*)
	FROM tbProfileGPRS WITH (NOLOCK)
	WHERE GPRSName = @sGPRSName AND
		  DatabaseID = @iDatabaseID
	IF (dbo.isTagLength4(@iDatabaseID)=5)
	BEGIN
		SELECT @iCountID = COUNT(*)
		FROM tbProfileGPRS WITH (NOLOCK)
		WHERE GPRSTag = 'GP001' AND
			  GPRSTagValue = @sGPRSName AND
			  DatabaseID = @iDatabaseID
	END
	ELSE
	BEGIN
		SELECT @iCountID = COUNT(*)
		FROM tbProfileGPRS WITH (NOLOCK)
		WHERE GPRSTag = 'GP01' AND
			  GPRSTagValue = @sGPRSName AND
			  DatabaseID = @iDatabaseID
	END
	IF @iCountName = 0
	BEGIN
		IF @iCountID = 0
		BEGIN
			IF @sContent IS NOT NULL
			BEGIN
				DECLARE @sGPRSID VARCHAR(2)
				IF (dbo.isTagLength4(@iDatabaseID)=5)
				BEGIN
					INSERT INTO tbProfileGPRS(
						GPRSName,
						DatabaseID,
						GPRSTag,
						GPRSLengthOfTagLength,
						GPRSTagLength,
						GPRSTagValue)
					SELECT 
						@sGPRSName,
						@iDatabaseID,
						Tag,
						LEN(TagLength),
						TagLength,
						TagValue
					FROM dbo.fn_tbprofiletlv5(@sGPRSName,@sContent)

					SELECT @sGPRSID = GPRSTagValue	
					FROM tbProfileGPRS WITH (NOLOCK)
					WHERE GPRSName = @sGPRSName AND 
						  DatabaseID = @iDatabaseID AND 
						  GPRSTag LIKE 'GP001'
				END
				ELSE
				BEGIN
					INSERT INTO tbProfileGPRS(
						GPRSName,
						DatabaseID,
						GPRSTag,
						GPRSLengthOfTagLength,
						GPRSTagLength,
						GPRSTagValue)
					SELECT 
						@sGPRSName,
						@iDatabaseID,
						Tag,
						LEN(TagLength),
						TagLength,
						TagValue
					FROM dbo.fn_tbprofiletlv(@sGPRSName,@sContent)

					SELECT @sGPRSID = GPRSTagValue	
					FROM tbProfileGPRS WITH (NOLOCK)
					WHERE GPRSName = @sGPRSName AND 
						  DatabaseID = @iDatabaseID AND 
						  GPRSTag LIKE 'GP01'
				END
				EXEC spItemComboBoxValueInsertGPRS @sGPRSName, @iDatabaseID

				DECLARE @sDatabaseID VARCHAR(3)
				SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
				EXEC spProfileTerminalUpdateLastUpdateByDbID @sDatabaseID

			END
		END
		ELSE
			SELECT 'GPRS ID has already used. Please choose other GPRS ID.' 
	END
	ELSE
		SELECT 'IP Primary has already used. Please choose other IP Primary.'

