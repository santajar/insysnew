﻿
CREATE PROCEDURE [dbo].[spProfileLoyPoolInsert]
	@iDatabaseID INT,
	@sLoyPoolName VARCHAR(50),
	@sContent VARCHAR(MAX)=NULL
AS
IF (dbo.isTagLength4(@iDatabaseID)=5)
BEGIN
	INSERT INTO tbProfileLoyPool(
		LoyPoolName,
		DatabaseID,
		LoyPoolTag,
		LoyPoolLengthOfTagLength,
		LoyPoolTagLength,
		LoyPoolTagValue)
	SELECT 
		@sLoyPoolName,
		@iDatabaseID,
		Tag,
		LEN(TagLength),
		TagLength,
		TagValue
	FROM dbo.fn_tbprofiletlv5(@sLoyPoolName,@sContent)
END
ELSE
BEGIN
	INSERT INTO tbProfileLoyPool(
		LoyPoolName,
		DatabaseID,
		LoyPoolTag,
		LoyPoolLengthOfTagLength,
		LoyPoolTagLength,
		LoyPoolTagValue)
	SELECT 
		@sLoyPoolName,
		@iDatabaseID,
		Tag,
		LEN(TagLength),
		TagLength,
		TagValue
	FROM dbo.fn_tbprofiletlv(@sLoyPoolName,@sContent)
END


