﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Oct 1, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Inserting the detail profile initialization log
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnDetailInsert]
@sTerminalID CHAR(8),
@sTag VARCHAR(5)
AS
DECLARE @iPercentage INT
SET @iPercentage = dbo.iInitPercentage(dbo.iTotalRowsDetails(@sTerminalID, @sTag), dbo.iProcessRowsDetails(@sTerminalID, @sTag))

INSERT INTO tbInitLogConnDetail ([Time], TerminalID, Tag, [Description], Percentage)
VALUES (GETDATE(), @sTerminalID, @sTag, dbo.sInitLogConnErrorMessage (@iPercentage) , @iPercentage)