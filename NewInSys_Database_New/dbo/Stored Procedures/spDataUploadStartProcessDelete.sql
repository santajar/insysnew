﻿-- =============================================
-- Author:		Kiki Ariady
-- Create date: Oct 7, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Process the profile Data Upload
-- =============================================
CREATE PROCEDURE [dbo].[spDataUploadStartProcessDelete]
	@TerminalTable VARCHAR(150),
	@AcquirerTable VARCHAR(150),
	@sUser VARCHAR(30)
AS
SET DEADLOCK_PRIORITY NORMAL
DECLARE @sCurrTime DATETIME
SET @sCurrTime=CURRENT_TIMESTAMP
DECLARE @SqlStmt NVARCHAR(MAX)
SET NOCOUNT ON

--IF OBJECT_ID('tbUploadTerminal') IS NOT NULL
--	DROP TABLE tbUploadTerminal
--SET @SqlStmt = 'SELECT * INTO tbUploadTerminal FROM ' + @TerminalTable

--EXEC sp_executesql @SqlStmt

--IF OBJECT_ID('tbUploadAcquirer') IS NOT NULL
--	DROP TABLE tbUploadAcquirer
--SET @SqlStmt = 'SELECT * INTO tbUploadAcquirer FROM ' + @AcquirerTable

--EXEC sp_executesql @SqlStmt

--select * from tbUploadTerminal
-- process upload terminal
EXEC spDataUploadProcessTerminalDelete @TerminalTable

--DROP TABLE tbUploadTerminal
--DROP TABLE tbUploadAcquirer

SELECT Id, [Time], TerminalId, Description, Remarks
FROM tbUploadLog WITH (NOLOCK)
WHERE [Time]>@sCurrTime