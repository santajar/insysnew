﻿
-- ============================================================
-- Description:	Delete data in tbItemComboBoxValue used in TLE 
-- ============================================================

CREATE PROCEDURE [dbo].[spItemComboBoxValueDeleteTLE]
	@sTLEName VARCHAR(15),
	@sTLEID VARCHAR(2),
	@sDatabaseId INT
AS
DELETE FROM tbItemComboBoxValue
WHERE DatabaseId=@sDatabaseId 
	AND DisplayValue=@sTLEName
	AND RealValue=@sTLEID




