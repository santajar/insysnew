﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Sept 30, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Inserting the detail profile initialization log
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnDetailInsertProcess]
@sTerminalID CHAR(8)
AS
	EXEC spInitLogConnDetailInsert @sTerminalID, 'DE'
	EXEC spInitLogConnDetailInsert @sTerminalID, 'DC'
	EXEC spInitLogConnDetailInsert @sTerminalID, 'AC'
	EXEC spInitLogConnDetailInsert @sTerminalID, 'AE'
	EXEC spInitLogConnDetailInsert @sTerminalID, 'AA'
	EXEC spInitLogConnDetailInsert @sTerminalID, 'AD'