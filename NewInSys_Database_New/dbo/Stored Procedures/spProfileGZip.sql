﻿
-- ============================================================
-- Description:	Get Zip Content Profile foc current Terminal ID
-- ============================================================

CREATE PROCEDURE [dbo].[spProfileGZip] @sTerminalID VARCHAR(8)
AS
SELECT TerminalId, TermContent
FROM tbInitCompress WITH (NOLOCK)
WHERE TerminalId=@sTerminalID



