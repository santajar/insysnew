﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 2, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete the AID on table tbProfileAID
--				2. Modify AID for Form Flexsible
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAIDDelete]
	@iDatabaseId INT,
	@sAIDName VARCHAR(50),
	@bDeleteData BIT = 1
AS
DELETE FROM tbProfileAID
WHERE DatabaseId=@iDatabaseId AND AIDName=@sAIDName
