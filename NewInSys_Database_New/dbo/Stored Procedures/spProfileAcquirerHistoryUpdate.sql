﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Description:	
--				1. Update The History Acquirer table content of profile
-- =============================================
create PROCEDURE [dbo].[spProfileAcquirerHistoryUpdate]
	@sTerminalID VARCHAR(8),
	@sAcquirerName VARCHAR(25)
AS
BEGIN
	SET NOCOUNT ON;

	EXEC spProfileAcquirerHistoryDelete @sTerminalID,@sAcquirerName
	EXEC spProfileAcquirerHistoryInsert @sTerminalID,@sAcquirerName
END