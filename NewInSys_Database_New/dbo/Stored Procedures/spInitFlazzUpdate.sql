﻿
-- ========================================================================
-- Description:	Update PSAM Serial No to tbAutoInit when Initiating Flazz
-- ========================================================================

CREATE PROCEDURE [dbo].[spInitFlazzUpdate]
	@sSessionTime VARCHAR(255),
	@sPSAMSN VARCHAR(50)
AS
	DECLARE @sSqlStmt NVARCHAR(MAX)
	DECLARE @sTID VARCHAR(50)

	SET @sSqlStmt = N'SELECT @sTID=BitValue FROM ##tempBitMap' + @sSessionTime + 
		' WHERE BitId=41'

	EXEC sp_executesql @sSqlStmt,N'@sTID VARCHAR(MAX) OUTPUT',@sTID=@sTID OUTPUT
	EXEC spOtherParseHexStringToString @sTID, @sTID OUTPUT

	DECLARE @iCount INT
	SELECT @iCount = COUNT(*)
	FROM tbAutoInitLog WITH (NOLOCK)
	WHERE TerminalID = @sTID

	IF (@iCount = 0) --If TID not exist, insert new row
	BEGIN
		INSERT INTO tbAutoInitLog (TerminalID, PSAMSN)
		VALUES (@sTID, @sPSAMSN)
	END
	ELSE -- Update PSAMSN
	BEGIN
		UPDATE tbAutoInitLog 
		SET PSAMSN = @sPSAMSN
		WHERE TerminalID = @sTID
	END