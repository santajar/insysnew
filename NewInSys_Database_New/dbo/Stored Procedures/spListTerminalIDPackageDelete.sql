﻿
-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <21 okt 2015>
-- Description:	<Browse List TerminalID>
-- =============================================
CREATE PROCEDURE [dbo].[spListTerminalIDPackageDelete]
	@sCondition VARCHAR(MAX)= NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sQuery VARCHAR(MAX);
    SET @sQuery = 'DELETE FROM tbListTerminalIDPackage '

	EXEC (@sQuery + @sCondition )

END