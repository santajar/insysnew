﻿CREATE PROCEDURE [dbo].[spInitLogConnUpdateConsole] 
@sTerminalID VARCHAR(8),
@iPercentage INT
AS
--DECLARE @iPercentage INT
--SET @iPercentage = dbo.iInitPercentage (@iTotalLoop, @iProcessRow)
DECLARE @sDate VARCHAR(30)
IF (@iPercentage = 0 OR @iPercentage = 100) 
	SELECT @sDate  = [Time] 
	FROM tbInitLogConn WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
ELSE
	SET @sDate = CONVERT(VARCHAR,GETDATE(),21)

UPDATE tbInitLogConn -- WITH (ROWLOCK)
	SET [Time] = @sDate,
		[Description] = dbo.sInitLogConnErrorMessage(@iPercentage),
		Percentage = @iPercentage
WHERE TerminalID = @sTerminalID
