﻿
-- =================================================================
-- Description:	Update data in tbAutoInitLog
--				If TID not found (in tbAutoInitLog), insert new row
--				else Update selected TID with new data
-- =================================================================

CREATE PROCEDURE [dbo].[spInitUpdate]
	@sTerminalID VARCHAR(8),
	@sLastInitTime VARCHAR(50)=NULL,
	@sPABX VARCHAR(50)=NULL,
	@sSoftwareVers VARCHAR(50)=NULL,
	@sOSVers VARCHAR(50)=NULL,
	@sKernelEmvVers VARCHAR(50)=NULL,
	@sEdcSN VARCHAR(50)=NULL,
	@sReaderSN VARCHAR(50)=NULL,
	@sPSAMSN VARCHAR(50)=NULL,
	@sMCSN VARCHAR(50)=NULL,
	@sMemUsg VARCHAR(50)=NULL,
	@sICCID VARCHAR(50)=NULL
AS
	-- Look up for current TID in table
	DECLARE @iCount INT
	SELECT @iCount = COUNT(*)
	FROM tbAutoInitLog WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID

	IF (@iCount = 0)
	BEGIN
		INSERT INTO tbAutoInitLog (TerminalID, 
									LastInitTime, 
									PABX, 
									SoftwareVer, 
									OsVer, 
									KernelVer, 
									EDCSN, 
									ReaderSN, 
									PSAMSN, 
									MCSN, 
									MemUsage,
									ICCID
								   )
		VALUES (@sTerminalID,
				@sLastInitTime,
				@sPABX,
				@sSoftwareVers,
				@sOSVers,
				@sKernelEmvVers,
				@sEdcSN,
				@sReaderSN,
				@sPSAMSN,
				@sMCSN,
				@sMemUsg,
				@sICCID
			   )
	END
	ELSE
	BEGIN
	UPDATE tbAutoInitLog 
	SET LastInitTime = @sLastInitTime,
		PABX = @sPABX, 
		SoftwareVer = @sSoftwareVers, 
		OsVer = @sOSVers, 
		KernelVer = @sKernelEmvVers, 
		EDCSN = @sEdcSN, 
		ReaderSN = @sReaderSN, 
		PSAMSN = @sPSAMSN, 
		MCSN = @sMCSN, 
		MemUsage = @sMemUsg,
		ICCID = @sICCID
	WHERE TerminalID = @sTerminalID
	END