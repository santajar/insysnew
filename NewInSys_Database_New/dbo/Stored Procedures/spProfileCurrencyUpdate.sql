﻿CREATE PROCEDURE [dbo].[spProfileCurrencyUpdate]
	@iDatabaseId BIGINT,
	@iCurrencyId BIGINT,
	@sCurrencyName VARCHAR(5),
	@sContent VARCHAR(300)
AS
	UPDATE tbProfileCurrency 
		SET tbProfileCurrency.CurrencyTagValue = b.TagValue,
			tbProfileCurrency.CurrencyTagLength = b.TagLength
	FROM ( SELECT TerminalId,
					Name, 
					Tag,
					TagLength,
					TagValue
			FROM dbo.fn_tbProfileTLV(@sCurrencyName, @sContent)
		) b
	WHERE tbProfileCurrency.DatabaseId = @iDatabaseId AND
			tbProfileCurrency.CurrencyName = b.TerminalId AND
			tbProfileCurrency.CurrencyTag = b.Tag

	DECLARE @sDatabaseID VARCHAR(3)
	SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
				EXEC spProfileTerminalUpdateLastUpdateByDbID @sDatabaseID

