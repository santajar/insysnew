USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileAcquirerInsert]    Script Date: 11/2/2017 3:53:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 23, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Insert new terminal into database (tbProfileTerminal and tbProfileTerminalList)
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerInsert]
	@sTerminalID VARCHAR(8),
	@sContent VARCHAR(MAX)
AS
DECLARE @sTag VARCHAR(8)
IF Object_ID ('TempDB..#TempAcq') IS NOT NULL
	DROP TABLE #TempAcq

CREATE TABLE #TempAcq
(
	TerminalID VARCHAR(8),
	[Name] VARCHAR(15),
	Tag VARCHAR(5),
	LenghtOfTagLength INT,
	TagLength INT,
	TagValue VARCHAR(150)
)

IF Object_ID ('TempDB..#TempA') IS NOT NULL
	DROP TABLE #TempA
CREATE TABLE #TempA
(
	TerminalID VARCHAR(8),
	[Name] VARCHAR(15),
	[AcquirerTag] VARCHAR(5),
	[AcquirerTagValue] VARCHAR(MAX)
)

declare @cols nvarchar (MAX)
declare @query nvarchar (MAX)

Declare @iDBID INT
	SELECT @iDBID=DatabaseID FROM tbProfileTerminalList WHERE TerminalID = @sTerminalID
IF (dbo.isTagLength4(@iDBID)=4)
BEGIN

	SET @sTag = 'AD03'
	INSERT INTO #TempAcq(TerminalID,[Name],Tag,LenghtOfTagLength,TagLength,TagValue)
		SELECT TerminalID,
		[Name],
		Tag,
		LEN(TagLength) AS LenghtOfTagLength,
		TagLength,
		TagValue
	FROM dbo.fn_TbProfileTLV(@sTerminalID,@sContent)

END
ELSE
BEGIN

	SET @sTag = 'AD003'
	INSERT INTO #TempAcq(TerminalID,[Name],Tag,LenghtOfTagLength,TagLength,TagValue)
		SELECT TerminalID,
		[Name],
		Tag,
		LEN(TagLength) AS LenghtOfTagLength,
		TagLength,
		TagValue
	FROM dbo.fn_TbProfileTLV5(@sTerminalID,@sContent)

END
DECLARE @sAcqName VARCHAR(50)

SELECT @iDBID=DatabaseID FROM tbProfileTerminalList WHERE TerminalID = @sTerminalID

--SELECT DISTINCT @sAcqName = [Name] FROM #TempAcq
set @sAcqName=(select distinct Name from #TempAcq)

	insert into #TempA(TerminalID,[Name],AcquirerTag,AcquirerTagValue)
	select a.TerminalID,a.Name,a.Tag,a.TagValue from #TempAcq a
	join tbProfileTerminalList b
	on a.TerminalID=b.TerminalID
	join MasterProfile c
	on b.DatabaseID=c.DatabaseID and a.Tag=c.Tag and a.Name=c.Name
	where c.FormID=2 and a.TagValue <> c.TagValue and c.Name=@sAcqName

SELECT @cols =STUFF((SELECT ',' + QUOTENAME(AcquirerTag) 
            from #TempA
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

set @query='
				insert into tbProfileAcquirer (TerminalID,AcquirerName,'+ @cols +')
				select [TerminalID],Name,'+ @cols +'
				from (
					select TerminalID,Name,AcquirerTag[Tag],AcquirerTagValue
					from #TempA

				) pv
				pivot
					(
						max (AcquirerTagValue)
						For Tag in ('+ @cols +')
					) as p'
exec (@query)



--INSERT INTO tbProfileAcquirer(TerminalID,
--	AcquirerName,
--	AcquirerTag, 
--	AcquirerLengthOfTagLength, 
--	AcquirerTagLength, 
--	AcquirerTagValue)
--SELECT TerminalID,
--	[Name],
--	Tag,
--	LenghtOfTagLength,
--	TagLength,
--	TagValue
--FROM #TempAcq

--Add Relation
EXEC spProfileRelationInsertProcess @sTerminalID,@sAcqName, NULL, NULL, @sTag

DROP TABLE  #TempAcq
drop table #TempA
EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
print 'sini'
EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

DECLARE @sDbID VARCHAR(5)
SET @sDbID = dbo.iDatabaseIdByTerminalId(@sTerminalID)

EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'AA', @sAcqName, @sContent, 0


GO


