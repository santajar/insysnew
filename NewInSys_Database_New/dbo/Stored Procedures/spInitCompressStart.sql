﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Mar 24, 2011
-- Modify	  :
--				1. 
-- Description:	Prepare the Terminal profile for Compressed Init process
-- =============================================
CREATE PROCEDURE [dbo].[spInitCompressStart]
	@sTerminalID char(8)
AS
-- Start Process
SET NOCOUNT ON
EXEC spInitDelete @sTerminalID

-- Start create full profile content
--IF OBJECT_ID ('TempDb..#Profile', 'U') IS NOT NULL
--	DROP TABLE #Profile
	
--CREATE TABLE #Profile (
--	IdRow INT IDENTITY(1,1),
--	TerminalId VARCHAR(8),
--	[Name] VARCHAR(50),
--	Tag VARCHAR(4),
--	ItemName VARCHAR(50) NULL,
--	TagLength SMALLINT,
--	TagValue VARCHAR(1000)
--)
--INSERT INTO #Profile(TerminalID, [Name], Tag, ItemName, TagLength, TagValue)
--SELECT TerminalID, [Name], Tag, ItemName, TagLength, TagValue
--FROM dbo.fn_tbProfileContentSortNew(@sTerminalID,NULL)
--ORDER BY ID

-- DECLARE needed variable

--DECLARE @sContent VARCHAR(MAX),
--	@sOldTag VARCHAR(4),
--	@sCurrTag VARCHAR(4),
--	@iMaxByte INT,
--	@iNewInit BIT,
--	@iEMVInit BIT,
--	@iTLEInit BIT,
--	@iLoyaltyInit BIT,
--	@iGPRSInit BIT,
--	@iIdStart INT,
--	@iIdMax INT

-- SET max byte
--SET @sContent=''
--SET @iMaxByte=dbo.iPacketLength()
----SET @iNewInit=dbo.IsNewInit(@sTerminalID)
--SET @iEMVInit=dbo.IsEMVInit(@sTerminalID)
----SET @iTLEInit = dbo.IsTLEInit(@sTerminalID)
--SET @iLoyaltyInit = dbo.IsLoyalty(@sTerminalID)
----SET @iGPRSInit = dbo.IsGPRSInit(@sTerminalID)

----SELECT @iIdStart=MIN(IdRow) FROM #Profile
----SELECT @iIdMax=MAX(IdRow) FROM #Profile

--DECLARE @sTid VARCHAR(8),
--	@sTLVHeader VARCHAR(50),
--	@sTag VARCHAR(4),
--	@sTagValue VARCHAR(1000),
--	@sNewContent VARCHAR(1000)

--WHILE @iIdStart<=@iIdMax
--BEGIN
--	SELECT @sTid=TerminalId,@sTag=Tag,@sTagValue=TagValue FROM #Profile
--	WHERE IdRow=@iIdStart
--	SET @sOldTag=@sTag
--	SET @sTLVHeader=dbo.sGetTLVString(@sTag,@sTagValue)

--	-- SET current tag
--	SET @sCurrTag = @sTag
--	SET @sNewContent=dbo.sGetTLVString(@sTag,@sTagValue)
--	SET @sContent = @sContent + @sNewContent
--	SET @iIdStart = @iIdStart + 1
--END

--SELECT @sContent = TerminalContent FROM tbInitTerminal
--WHERE TerminalId = @sTerminalID

---- Start preparing the CAPK content into tbInit table
--SET @sNewContent= ''
--IF @iEMVInit=1
--BEGIN
--	IF OBJECT_ID ('TempDb..#ProfileAID', 'U') IS NOT NULL
--		DROP TABLE #ProfileAID
--	CREATE TABLE #ProfileAID (
--		IdRow INT IDENTITY(1,1),
--		TerminalId VARCHAR(8),
--		[Name] VARCHAR(50),
--		Tag VARCHAR(4),
--		ItemName VARCHAR(50) NULL,
--		TagLength SMALLINT,
--		TagValue VARCHAR(1000)
--	)
--	INSERT INTO #ProfileAID(TerminalID, [Name], Tag, ItemName, TagLength, TagValue) 
--	SELECT * FROM dbo.fn_tbProfileContentAID(@sTerminalID,NULL)
	
--	SELECT @iIdStart=MIN(IdRow) FROM #ProfileAID
--	SELECT @iIdMax=MAX(IdRow) FROM #ProfileAID

--	SET @sContent = @sContent + 'AI'

--	WHILE @iIdStart<=@iIdMax
--	BEGIN
--		SELECT @sTid=TerminalId, @sTag=Tag, @sTagValue=TagValue FROM #ProfileAID
--		WHERE IdRow=@iIdStart
--		SET @sOldTag=@sTag
	
--		SET @sContent = @sContent + dbo.sGetTLVString(@sTag,@sTagValue)		
--		SET @iIdStart = @iIdStart + 1
--	END

--	SET @sTag=''
--	IF OBJECT_ID ('TempDb..#ProfileCAPK', 'U') IS NOT NULL
--		DROP TABLE #ProfileCAPK
--	CREATE TABLE #ProfileCAPK (
--		IdRow INT IDENTITY(1,1),
--		TerminalId VARCHAR(8),
--		[Name] VARCHAR(50),
--		Tag VARCHAR(4),
--		ItemName VARCHAR(50) NULL,
--		TagLength SMALLINT,
--		TagValue VARCHAR(1000)
--	)
--	INSERT INTO #ProfileCAPK(TerminalID, [Name], Tag, ItemName, TagLength, TagValue) 
--	SELECT * FROM dbo.fn_tbProfileContentCAPK(@sTerminalID,NULL)
	
--	SELECT @iIdStart=MIN(IdRow) FROM #ProfileCAPK
--	SELECT @iIdMax=MAX(IdRow) FROM #ProfileCAPK
	
--	WHILE @iIdStart<=@iIdMax
--	BEGIN
--		SELECT @sTid=TerminalId,@sTag=Tag,@sTagValue=TagValue FROM #ProfileCAPK
--		WHERE IdRow=@iIdStart
--		SET @sContent = @sContent + dbo.sGetCAPKTLVString(@sTag,@sTagValue)	
--	END
--END

--Start Preparing data for TLE content into tbInit
--IF @iTLEInit = 1
--BEGIN
--	IF OBJECT_ID ('TempDb..#ProfileTLE', 'U') IS NOT NULL
--		DROP TABLE #ProfileTLE
--	CREATE TABLE #ProfileTLE (
--		IdRow INT IDENTITY(1,1),
--		TerminalId VARCHAR(8),
--		[Name] VARCHAR(50),
--		Tag VARCHAR(4),
--		ItemName VARCHAR(50) NULL,
--		TagLength SMALLINT,
--		TagValue VARCHAR(1000)
--	)
--	INSERT INTO #ProfileTLE(TerminalID, [Name], Tag, ItemName, TagLength, TagValue)
--	SELECT * FROM dbo.fn_tbProfileContentTLE (@sTerminalID,NULL)
--	SELECT @iIdStart=MIN(IdRow) FROM #ProfileTLE
--	SELECT @iIdMax=MAX(IdRow) FROM #ProfileTLE
--	WHILE @iIdStart<=@iIdMax
--	BEGIN
--		SELECT @sTid=TerminalId,@sTag=Tag,@sTagValue=TagValue FROM #ProfileTLE
--		WHERE IdRow=@iIdStart
--		SET @sContent = @sContent + dbo.sGetTLVString(@sTag,@sTagValue)

--		SET @iIdStart = @iIdStart + 1
--	END
--END
--Start Preparing data for GPRS content into tbInit
--IF @iGPRSInit = 1
--BEGIN
--	IF OBJECT_ID ('TempDb..#ProfileGPRS', 'U') IS NOT NULL
--		DROP TABLE #ProfileGPRS
--	CREATE TABLE #ProfileGPRS (
--		IdRow INT IDENTITY(1,1),
--		TerminalId VARCHAR(8),
--		[Name] VARCHAR(50),
--		Tag VARCHAR(4),
--		ItemName VARCHAR(50) NULL,
--		TagLength SMALLINT,
--		TagValue VARCHAR(1000)
--	)
--	INSERT INTO #ProfileGPRS(TerminalID, [Name], Tag, ItemName, TagLength, TagValue)
--	SELECT * FROM dbo.fn_tbProfileContentGPRS(@sTerminalID,NULL)
--	SELECT @iIdStart=MIN(IdRow) FROM #ProfileGPRS
--	SELECT @iIdMax=MAX(IdRow) FROM #ProfileGPRS
--	SET @sNewContent= ''
	
--	WHILE @iIdStart<=@iIdMax
--	BEGIN
--		SELECT @sTid=TerminalId,@sTag=Tag,@sTagValue=TagValue FROM #ProfileGPRS
--		WHERE IdRow=@iIdStart
--		SET @sNewContent = dbo.sGetTLVString(@sTag,@sTagValue)

--		SET @sContent = @sContent + @sNewContent

--		SET @iIdStart = @iIdStart + 1
--	END
--END
-- End of create full profile content

-- Start prepare compress content

--DELETE FROM tbInitCompress WHERE TerminalID=@sTerminalID

--INSERT INTO tbInitCompress(TerminalID, TermContent)
--VALUES(@sTerminalID, @sContent)
-- End of prepare compress content

-- Start compress content
--DECLARE @sPath VARCHAR(MAX),
--	@sCommand VARCHAR(255),
--	@sFilename VARCHAR(255)

--select * from tbInitCompress where TerminalID=@sTerminalID
--print 'Hello Command shell...'

--SET @sPath = dbo.sGzipPath()
--SET @sCommand = 'cmd.exe /C ""' + @sPath + '\InSysConsoleGZip.exe" -T ' + @sTerminalID + ' -P "' + @sPath + '""'

--PRINT @sCommand

--IF OBJECT_ID ('TempDb..#TEST1', 'U') IS NOT NULL
--	DROP TABLE #TEST1
--CREATE TABLE #TEST1 (cmdoutput text)
--INSERT #TEST1 
--EXEC xp_cmdshell @sCommand
-- End of compress content

--EXEC spProfileGZipUpdate @sTerminalID, @sPath

-- Start prepare the compressed content init package
--DECLARE @sCompress VARCHAR(MAX)

--SELECT @sCompress = dbo.sStringToHexString(CompressedContent) 
--FROM tbInitCompress WHERE TerminalId=@sTerminalID

--print '@sCompress : ' + @sCompress

--DECLARE @iOffset INT,
--	@sTemp VARCHAR(MAX)

--SET @iOffset=1

--WHILE @iOffset <= len(@sCompress)
--BEGIN
--	SET @sTemp = substring(@sCompress,@iOffset,@iMaxByte)
--	-- Compressed Tag code is "GZ" a.k.a "GZIP"
--	INSERT INTO tbInit(TerminalId,[Content],Tag,Flag)
--		VALUES(@sTid,@sTemp,'GZ',0)

--	print '@sTemp : ' + @sTemp

--	SET @iOffset=@iOffset+@iMaxByte
	
--END
-- End of prepare the compressed content init package


INSERT INTO tbInit(TerminalId, Content, Tag, Flag)
SELECT TerminalId, Content, Tag, Flag
FROM tbInitTemp
WITH (NOLOCK)
WHERE TerminalId = @sTerminalId AND Tag = 'GZ'
ORDER BY InitID