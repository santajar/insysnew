﻿-- =============================================
-- Author:		<Author,,TEDIE SCORFIA>
-- Create date: <Create Date,,24 APRIL 2014>
-- Description:	<Description,,DELETE TERMINALID FROM TERMINALIDLIST REGION,GROUP OR CITY>
-- =============================================
CREATE PROCEDURE [dbo].[spRDDeleteTIDGroupRegionCity]
	@sTerminalID VARCHAR(8),
	@sCategory VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @iCountTID int
	
	SELECT @iCountTID = COUNT(*)
	FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID

	IF (@iCountTID = 0)
	BEGIN
		DELETE FROM tbListTIDRemoteDownload
		WHERE TerminalID = @sTerminalID
	END
	ELSE
	BEGIN
		DECLARE @iGroupID int, @iRegionID INT, @iCityID INT

		SELECT @iGroupID=IdGroupRD,@iRegionID=IdRegionRD,@iCityID = IdCityRD
		FROM tbListTIDRemoteDownload WITH (NOLOCK)
		WHERE TerminalID = @sTerminalID

		IF (@sCategory='Region')
		BEGIN
			UPDATE tbListTIDRemoteDownload
			SET IdRegionRD = 0
			WHERE TerminalID = @sTerminalID
		END
		ELSE IF (@sCategory='Group')
		BEGIN
			UPDATE tbListTIDRemoteDownload
			SET IdGroupRD = 0
			WHERE TerminalID = @sTerminalID
		END
		ELSE
		BEGIN
			UPDATE tbListTIDRemoteDownload
			SET IdCityRD = 0
			WHERE TerminalID = @sTerminalID
		END

		IF (@iGroupID = 0 AND @iRegionID = 0 AND @iCityID=0)
		BEGIN
			UPDATE tbListTIDRemoteDownload
			SET ScheduleID = 0,AppPackID=0
			WHERE TerminalID = @sTerminalID
		END

	END
END