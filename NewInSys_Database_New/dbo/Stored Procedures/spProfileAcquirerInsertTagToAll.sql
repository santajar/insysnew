﻿


CREATE PROCEDURE [dbo].[spProfileAcquirerInsertTagToAll]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5),
	@iLengthofTagValueLength INT,
	@sTagValue VARCHAR(500),
	@iTagValueLength INT
AS
	INSERT INTO tbProfileAcquirer (TerminalID, AcquirerName, AcquirerTag, AcquirerLengthOfTagLength, AcquirerTagLength, AcquirerTagValue)
	SELECT TerminalID, AcquirerName, @sTag, @iLengthOfTagValueLength, @iTagValueLength, @sTagValue
	FROM (	SELECT DISTINCT TerminalID, AcquirerName 
			FROM tbProfileAcquirer WITH (NOLOCK)
			WHERE TerminalID IN ( SELECT TerminalID
								  FROM tbProfileTerminalList WITH (NOLOCK)
								  WHERE DatabaseID = @sDatabaseID
								)
		 ) AS Temp