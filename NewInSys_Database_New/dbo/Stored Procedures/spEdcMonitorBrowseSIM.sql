﻿-- =============================================
-- Author		: Tobias Setyo
-- Create date	: July 17, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spEdcMonitorBrowseSIM]
	@sTimestamp VARCHAR(50),
	@sSerialNumber VARCHAR(50)
AS
SELECT TerminalID
	,SoftwareVersion [Software Version]
	,LEFT(MCC_MNC_Data,3) [MCC]
	,RIGHT(MCC_MNC_Data,2) [MNC]
	,LAC_Data [LAC]
	,Cell_ID_Data [CID]
FROM tbEdcMonitorSIM WITH(NOLOCK)
WHERE ID_Header = (SELECT ID FROM tbEdcMonitor WITH(NOLOCK) WHERE MessageTimestamp=CONVERT(datetime,@sTimestamp) AND SerialNumber=@sSerialNumber
)