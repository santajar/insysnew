USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileTerminalInsert]    Script Date: 11/2/2017 3:46:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 16, 2010
-- Modify date: 
--				1. August 12, 2010, change variable name from @sLastView to @bLastView
-- Description:	
--				1. Insert new terminal into database (tbProfileTerminal and tbProfileTerminalList)
--				2. Add Validation of Tag length 
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalInsert]
	@sTerminalID VARCHAR(8),
	@sDbID INT,
	@bAllowDownload BIT,
	@bStatus BIT,
	@bEnableIP BIT,
	@sContent VARCHAR(MAX),
--	@bAutoInit BIT = '1',
	@bInitCompress BIT = '0',
	@bRemoteDownload BIT ='0'

AS
CREATE TABLE #TempTerminal
(
	TerminalID VARCHAR(8),
	[Name] VARCHAR(50),
	[TerminalTag] VARCHAR(5),
	[TerminalLengthOfTagLength] INT,
	[TerminalTagLength] INT,
	[TerminalTagValue] VARCHAR(MAX)
)
CREATE TABLE #TempT
(
	TerminalID VARCHAR(8),
	[TerminalTag] VARCHAR(5),
	[TerminalTagValue] VARCHAR(MAX)
)
declare @cols nvarchar (MAX)
declare @query nvarchar (MAX)
DECLARE @dDate DATETIME
	IF @bAllowDownload = '1' 
		SET @dDate = GETDATE()
	ELSE
		SET @dDate = NULL

	INSERT INTO tbProfileTerminalList
		(TerminalID, DatabaseID, AllowDownload, 
		 StatusMaster, EnableIP , InitCompress, RemoteDownload) 
	VALUES 
		(@sTerminalID, @sDbID,@bAllowDownload,
		 @bStatus, @bEnableIP ,@bInitCompress,@bRemoteDownload)

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
		WHERE Name = N'AutoInitTimeStamp' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	  ) 
	BEGIN
		UPDATE tbProfileTerminalList
		SET AutoInitTimeStamp = @dDate
		WHERE TerminalID = @sTerminalID
	END
	
	IF (dbo.isTagLength4(@sDbID)=4)
	BEGIN
	INSERT INTO #TempTerminal 
		(TerminalID, TerminalTag, TerminalLengthOfTagLength, TerminalTagLength, TerminalTagValue)
	SELECT TerminalID,
			Tag,
			LEN(TagLength),
			TagLength,
			TagValue
	FROM dbo.fn_TbProfileTLV(@sTerminalID,@sContent)
	END
	ELSE
	BEGIN
	INSERT INTO #TempTerminal 
		(TerminalID, TerminalTag, TerminalLengthOfTagLength, TerminalTagLength, TerminalTagValue)
	SELECT TerminalID,
			Tag,
			LEN(TagLength),
			TagLength,
			TagValue
	FROM dbo.fn_TbProfileTLV5(@sTerminalID,@sContent)
	END

	insert into #TempT(TerminalID,TerminalTag,TerminalTagValue)
	select a.TerminalID,a.TerminalTag,a.TerminalTagValue from #TempTerminal a
	join tbProfileTerminalList b
	on a.TerminalID=b.TerminalID
	join MasterProfile c
	on b.DatabaseID=c.DatabaseID and a.TerminalTag=c.Tag
	where c.FormID=1 and a.TerminalTagValue <> c.TagValue

	SELECT @cols =STUFF((SELECT ',' + QUOTENAME(TerminalTag) 
            from #TempT
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

	set @query='
				insert into tbProfileTerminal (TerminalID,'+ @cols +')
				select TerminalID,'+ @cols +'
				from (
					select TerminalID,TerminalTag[Tag],TerminalTagValue
					from #TempT

				) pv
				pivot
					(
						max (TerminalTagValue)
						For Tag in ('+ @cols +')
					) as p'
exec (@query)


EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'DE', '', @sContent, 0
drop table #TempT
drop table #TempTerminal






GO


