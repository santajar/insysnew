﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,,16 April 2014>
-- Description:	<Description,,SP Insert Group Remote Download>
-- =============================================
CREATE PROCEDURE [dbo].[spRDInsertGroup]
	@sGroup VARCHAR(150)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @iCount INT
	
	SELECT @iCount = COUNT(*)
	FROM tbGroupRemoteDownload WITH (NOLOCK)
	WHERE GroupRDName = @sGroup

	IF @iCount= 0
	BEGIN
		INSERT tbGroupRemoteDownload(GroupRDName)
		VALUES (@sGroup)
	END
	
END