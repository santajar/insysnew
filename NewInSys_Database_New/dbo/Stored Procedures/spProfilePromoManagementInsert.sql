﻿-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Feb 10, 2016
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Insert new Promo Management into tbProfilePromoManagement
--				2. Modify to use form flexible`
-- =============================================
CREATE PROCEDURE [dbo].[spProfilePromoManagementInsert]
	@iDatabaseID INT,
	@sPromoManagementName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfilePromoManagement WITH (NOLOCK)
WHERE PromoManagementTag IN ('PR01','PR001') AND
		PromoManagementTagValue = @sPromoManagementName AND
		DatabaseID = @iDatabaseID

IF @iCountID =0
BEGIN
	IF (dbo.isTagLength4(@iDatabaseID)= 4)
	BEGIN
		INSERT INTO tbProfilePromoManagement(
			
			[PromoManagementName],
			[DatabaseID],
			[PromoManagementTag],
			[PromoManagementLength],
			[PromoManagementTagLength],
			[PromoManagementTagValue]
			)
		SELECT 
			
			[Name] [PromoManagementName],
			@iDatabaseID [DatabaseId],
			Tag [PromoManagementTag],
			LEN(TagLength) [PromoManagementLenth],
			TagLength [PromoManagementTagLength],
			TagValue [PromoManagementTagValue]

		FROM dbo.fn_TbProfileTLV(@sPromoManagementName,@sContent)
	END
	ELSE
	BEGIN
		INSERT INTO tbProfilePromoManagement(
			[PromoManagementName],
			[DatabaseID],
			[PromoManagementTag],
			[PromoManagementLength],
			[PromoManagementTagLength],
			[PromoManagementTagValue]
			)
		SELECT 
		
			[Name] [PromoManagementName],
				@iDatabaseID [DatabaseId],
			Tag [PromoManagementTag],
			LEN(TagLength) [PromoManagementLenth],
			TagLength [PromoManagementTagLength],
			TagValue [PromoManagementTagValue]

		FROM dbo.fn_TbProfileTLV5(@sPromoManagementName,@sContent)
	END
END
exec spItemComboBoxValueInsertPromoManagement @sPromoManagementName, @iDatabaseID
UPDATE tbProfileTerminalList
SET PromoManagement = '1'
WHERE DatabaseID = @iDatabaseID