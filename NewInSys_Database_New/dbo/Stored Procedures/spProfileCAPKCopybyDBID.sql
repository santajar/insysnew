﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <March 2 2015>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCAPKCopybyDBID] 
	@NewDatabaseID VARCHAR(3),
	@OldDatabaseID VARCHAR(3),
	@sCAPKIndex VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @iCount INT,
			@iOldTagLength INT,
			@iNewTagLength INT
	
	SELECT @iCount = COUNT(*)
	FROM tbProfileCAPK WITH (NOLOCK)
	WHERE DatabaseId = @NewDatabaseID AND CAPKIndex = @sCAPKIndex
	
	SET @iOldTagLength = dbo.isTagLength4(@OldDatabaseID)
	SET @iNewTagLength = dbo.isTagLength4(@NewDatabaseID)
	
	IF @iCount = 0
	BEGIN
		IF @iOldTagLength = @iNewTagLength
		BEGIN
			INSERT INTO tbProfileCAPK(DatabaseId,CAPKIndex,CAPKTag,CAPKLengthOfTagLength,CAPKTagLength,CAPKTagValue)
			SELECT @NewDatabaseID,CAPKIndex,CAPKTag,CAPKLengthOfTagLength,CAPKTagLength,CAPKTagValue
			FROM tbProfileCAPK WITH (NOLOCK)
			WHERE DatabaseId = @OldDatabaseID AND CAPKIndex = @sCAPKIndex
		END
		ELSE
		BEGIN
			IF @iOldTagLength > @iNewTagLength
			BEGIN
				INSERT INTO tbProfileCAPK(DatabaseId,CAPKIndex,CAPKTag,CAPKLengthOfTagLength,CAPKTagLength,CAPKTagValue)
				SELECT @NewDatabaseID,CAPKIndex,SUBSTRING(CAPKTag,1,2)+ SUBSTRING(CAPKTag,4,2),CAPKLengthOfTagLength,CAPKTagLength,CAPKTagValue
				FROM tbProfileCAPK WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND CAPKIndex = @sCAPKIndex
			END
			ELSE
			BEGIN
				INSERT INTO tbProfileCAPK(DatabaseId,CAPKIndex,CAPKTag,CAPKLengthOfTagLength,CAPKTagLength,CAPKTagValue)
				SELECT @NewDatabaseID,CAPKIndex,SUBSTRING(CAPKTag,1,2) +'0'+SUBSTRING(CAPKTag,3,2),CAPKLengthOfTagLength,CAPKTagLength,CAPKTagValue
				FROM tbProfileCAPK WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND CAPKIndex = @sCAPKIndex
			END
		END
	END
	ELSE
		SELECT 'CAPK Index has already used. Please choose other CAPK Index.'
	
	
END