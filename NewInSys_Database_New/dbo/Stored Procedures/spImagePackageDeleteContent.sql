﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spImagePackageDeleteContent]
	@sFileName VARCHAR(50),
	@sType VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @iImageID INT = 0,
			@iCount INT 
	
	SELECT @iCount = COUNT(*)
	FROM tbProfileImage WITH (NOLOCK)
	WHERE [FileName] = @sFileName
		AND LogoType = @sType

	SELECT @iImageID = ImageID
	FROM tbProfileImage WITH (NOLOCK)
	WHERE [FileName] =  @sFileName
		AND LogoType = @sType

	DELETE FROM tbItemComboBoxValue
	Where FormID = 1 AND DisplayValue= @sFileName

	DELETE FROM tbProfileImageTemp
	WHERE ImageID = @iImageID

	DELETE FROM tbProfileImage
	WHERE [FileName] =  @sFileName
		AND LogoType = @sType

	
	IF @iCount > 0
	BEGIN
		DECLARE @iDBid INT, @iFormID INT, @iItemSequence INT
		IF (@sType = 'Print')
		BEGIN
			DECLARE ItemListCursor CURSOR FOR
			SELECT DatabaseID,FormID,ItemSequence
			FROM tbItemList WITH (NOLOCK)
			WHERE FormID = 1
				AND itemname IN ('Logo Print')
		END
		--ELSE
		IF (@sType = 'Idle')
		BEGIN
			DECLARE ItemListCursor CURSOR FOR
			SELECT DatabaseID,FormID,ItemSequence
			FROM tbItemList WITH (NOLOCK)
			WHERE FormID = 1
				AND itemname IN ('Logo Idle')
		END
		IF (@sType = 'Merchant')
		BEGIN
			DECLARE ItemListCursor CURSOR FOR
			SELECT DatabaseID,FormID,ItemSequence
			FROM tbItemList WITH (NOLOCK)
			WHERE FormID = 1
				AND itemname IN ('Logo Merchant')
		END
		IF (@sType = 'Receipt')
		BEGIN
			DECLARE ItemListCursor CURSOR FOR
			SELECT DatabaseID,FormID,ItemSequence
			FROM tbItemList WITH (NOLOCK)
			WHERE FormID = 1
				AND itemname IN ('Logo Receipt')
		END

		OPEN ItemListCursor

		FETCH NEXT FROM ItemListCursor
		INTO @iDBid,@iFormID,@iItemSequence

		WHILE @@FETCH_STATUS = 0
		BEGIN
			DELETE FROM tbItemComboBoxValue
			WHERE DatabaseID = @iDBid
				AND FormID = @iFormID
				AND ItemSequence = @iItemSequence
				AND DisplayValue = @sFileName
				AND RealValue = @sFileName

			FETCH NEXT FROM ItemListCursor
			INTO @iDBid,@iFormID,@iItemSequence
		END
		CLOSE ItemListCursor
		DEALLOCATE ItemListCursor
	END
END