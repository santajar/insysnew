﻿
CREATE PROCEDURE [dbo].[spAppPackageEdit]
	@iAppPackId INT,
	@sAppPackageName VARCHAR(150),
	@sAppPackageFilename VARCHAR(50),
	@iBuildNumber INT,
	@sAppPackageDesc VARCHAR(250) = NULL,
	@sEdcTypeId VARCHAR(150) = NULL,
	@bAppPackageContent VARBINARY(MAX) = NULL

AS
	SET NOCOUNT ON
UPDATE tbAppPackageEDCListTemp
SET AppPackageName = @sAppPackageName
WHERE AppPackageName = (SELECT AppPackageName FROM tbAppPackageEDCList WITH (NOLOCK) WHERE AppPackId = @iAppPackId)

UPDATE tbAppPackageEDCList
SET	AppPackageName=@sAppPackageName,
	AppPackageFilename=@sAppPackageFilename,
	AppPackageDesc=@sAppPackageDesc,
	EdcTypeId=@sEdcTypeId,
	BuildNumber=@iBuildNumber,
	UploadTime = GETDATE()
WHERE AppPackId = @iAppPackId