﻿CREATE PROCEDURE [dbo].[spRequestPaperReceiptView]
	-- Add the parameters for the stored procedure here
	@sTerminalID VARCHAR(8)
AS
BEGIN
	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @sTerminalID=''
	BEGIN
	SELECT  *  FROM  tbProfileRequestPaper
	END
	ELSE
	BEGIN
	SELECT  *  FROM  tbProfileRequestPaper WHERE TerminalID = @sTerminalID
	END
END