﻿CREATE PROCEDURE [dbo].[spRD_GroupEnableDisable]
	@sGroupName VARCHAR(50),
	@iEnabled INT=0
AS
DECLARE  @Updated TABLE(ID INT)

UPDATE tbRD_Group
SET [Enabled]=@iEnabled, LastModify=GETDATE()
OUTPUT inserted.GROUP_ID INTO @Updated
WHERE GROUP_NAME=@sGroupName

UPDATE tbRD_GroupDetail
SET [Enabled]=@iEnabled, LastModify=GETDATE()
WHERE GROUP_ID IN (SELECT ID FROM @Updated)
