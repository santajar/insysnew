USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileTerminalUpdateTag]    Script Date: 11/2/2017 4:58:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Update the tagvalue of terminal table
--				2. changes length @stag from 4 -> 5
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalUpdateTag]
	@sTerminalID VARCHAR(8),
	@sTag VARCHAR(5),
	@sValue VARCHAR(MAX)
AS

declare @query nvarchar(max)
set @query='
UPDATE tbProfileTerminal SET '+ @sTag +' = '''+ @sValue +'''							 
WHERE TerminalID = '''+ @sTerminalID +''''

exec(@query)

GO


