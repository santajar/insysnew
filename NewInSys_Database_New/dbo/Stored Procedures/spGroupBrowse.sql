﻿/*
Name		: spGroupBrowse
Date		: 9 Oct 2013
Created by  : Tobias SETYO
Modify		:
				1. Oct 28, 2013, adding ScheduleID column
Description	:
	browse tbGroup
*/

CREATE PROCEDURE [dbo].[spGroupBrowse]
	@sCondition VARCHAR(500) = NULL
AS
DECLARE @sQuery VARCHAR(MAX)
SET @sQuery = 
'SELECT 
	GroupId [Id],
	ParentId,
	ChildId,
	GroupName [Name],
	TerminalIDList,
	Description
	
FROM tbGroup WITH (NOLOCK) '
IF ISNULL(@sCondition,'')=''
	EXEC (@sQuery)
ELSE
	EXEC (@sQuery + @sCondition)