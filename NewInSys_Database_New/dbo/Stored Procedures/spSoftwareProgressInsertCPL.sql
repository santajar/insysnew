﻿

-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: may 30, 2017
-- Modify date:
--				1. 
-- Description:	Insert into tbSoftwareProgressCPL
-- =============================================

CREATE procedure [dbo].[spSoftwareProgressInsertCPL]
@sTerminalID varchar(8),
@sSoftware varchar(50),
@sSoftwareDownload varchar(50),
@sSerialNumber varchar(25),
@fIndex float

AS
DECLARE
	@iTotalIndex int,
	@fPercentage float,
	@sGroupName varchar(25),
	@sSoftwareVer NVARCHAR(100),
	@sSoftwareVerName varchar(25),
	@sChar CHAR(5),
	@iAppPackId INT,
	@sId int,
	@iCount INT
SELECT @sGroupName =  GroupName From tbListTerminalIDPackage WHERE TerminalID = @sTerminalID

SELECT @sSoftwareVer = AppPackageContent FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackageName IN (Select SoftwareName From tbSoftwareGroupPackage WHERE GroupName =@sGroupName AND SoftwareName like '%.PAR')

SELECT @iTotalIndex=COUNT(*) 
FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackageName IN (Select SoftwareName From tbSoftwareGroupPackage WHERE GroupName =@sGroupName)
SET @sSoftwareVer = '0x' + @sSoftwareVer;
SET @sSoftwareVerName =   [dbo].[HexToAsci] (@sSoftwareVer)


SET @fPercentage = @fIndex / @iTotalIndex * 100

SELECT @iCount=COUNT(*)
FROM tbSoftwareProgressCPL WITH (NOLOCK)
WHERE InstallStatus = 'On Progress'
	AND TerminalID = @sTerminalID
	AND Software = @sSoftware
	AND SoftwareDownload = @sSoftwareVerName
	AND SerialNumber= @sSerialNumber
	

IF (@iCount>0)
BEGIN
UPDATE tbSoftwareprogressCPL SET Percentage = @fPercentage 
WHERE TerminalID=@sTerminalID AND 
SoftwareDownload = @sSoftwareVerName
END
ELSE
BEGIN
	INSERT INTO tbSoftwareprogressCPL (GroupName,TerminalID,Software,SoftwareDownload,SerialNumber,StartTime,InstallStatus)
	VALUES(	@sGroupName,
			@sTerminalID,
			@sSoftware,
		   --@sSoftwareDownload,
		 @sSoftwareVerName,
			@sSerialNumber,
			CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
			
			'On Progress')
END

SELECT @sId =  ID From tbSoftwareProgressCPL WITH (NOLOCK)
WHERE InstallStatus = 'On Progress'
	AND TerminalID = @sTerminalID
	AND Software = @sSoftware
	AND SoftwareDownload = @sSoftwareVerName
	AND SerialNumber= @sSerialNumber 



EXEC spSoftwareProgressInsertCPLDetail
@sId, @sTerminalID,@sSoftware,@sSoftwareDownload,@sSoftwareVerName, @sSerialNumber,@fIndex,@iTotalIndex