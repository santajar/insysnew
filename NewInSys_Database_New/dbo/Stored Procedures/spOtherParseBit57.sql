﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Jan 30, 2013
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. parse the ISO message value to get the bi-57 value, EDC serial number and application name
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseBit57] 
	@sSessionTime VARCHAR(MAX),
	@sBit57 VARCHAR(MAX) OUTPUT,
	@iCounter INT OUTPUT,
	@sAppName VARCHAR(MAX) OUTPUT,
	@sLastDownload VARCHAR(10) OUTPUT,
	@iBuildNumber INT OUTPUT
AS
SET NOCOUNT ON
DECLARE @sSqlStmt NVARCHAR(MAX)

SET @sSqlStmt = N'SELECT @Bit57=BitValue FROM ##tempBitMap' + @sSessionTime + 
	' WHERE BitId=57'
EXEC sp_executesql @sSqlStmt,N'@Bit57 VARCHAR(MAX) OUTPUT',@Bit57=@sBit57 OUTPUT

DECLARE @iLenApp INT
DECLARE @sTempApp VARCHAR(50)

SET @iCounter = CONVERT(INT,SUBSTRING(@sBit57,5,4))
print @icounter

SET @sLastDownload = SUBSTRING(@sBit57,9,10)
print @sLastDownload

SET @iLenApp = CONVERT(INT,SUBSTRING(@sBit57,19,4))
PRINT '@iLenApp='+CAST(@iLenApp AS VARCHAR)
IF @iLenApp > 0
BEGIN
	SET @sTempApp = SUBSTRING(@sBit57,23,@iLenApp*2)
	EXEC spOtherParseHexStringToString @sTempApp, @sAppName OUTPUT
	PRINT '@sAppName='+@sAppName

	SET @iBuildNumber = CONVERT(INT,SUBSTRING(@sBit57,23+(@iLenApp*2),4))
	PRINT @iBuildNumber
END

