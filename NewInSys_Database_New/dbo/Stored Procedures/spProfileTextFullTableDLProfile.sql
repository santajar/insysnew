﻿CREATE PROCEDURE [dbo].[spProfileTextFullTableDLProfile]   
 @sTerminalID VARCHAR(8)  
AS  
SET NOCOUNT ON  
  
DECLARE @tbInitTerminal TABLE(  
 Id INT IDENTITY(1,1) PRIMARY KEY,  
 TerminalId VARCHAR(8),  
 Name VARCHAR(50),  
 Tag VARCHAR(5),  
 ItemName VARCHAR(24),  
 TagLength SMALLINT,  
 TagValue VARCHAR(1000)  
)  
INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
FROM dbo.fn_tbProfileContentSortNew(@sTerminalId,NULL)  
ORDER BY ID  
  
IF OBJECT_ID('tempdb..#TempGPRS') IS NOT NULL  
DROP TABLE #TempGPRS  
CREATE TABLE #TempGPRS  
(  
 TerminalID VARCHAR (8),   
 Name VARCHAR(20),   
 Tag VARCHAR(5),   
 ItemName VARCHAR(20),   
 TagLength INT,   
 TagValue VARCHAR(MAX)  
)  
IF OBJECT_ID('tempdb..#tempTag') IS NOT NULL  
DROP TABLE #tempTag  
CREATE TABLE #tempTag  
(   
 IdRow INT IDENTITY(1,1),  
 TempTag VARCHAR(MAX)  
)  
INSERT #tempTag(TempTag)  
SELECT DISTINCT AcquirerTagValue FROM tbProfileAcquirer WITH(NOLOCK)
WHERE (AcquirerTag IN ('AA37','AA38','AA39','AA40','AA037','AA038','AA039','AA040','AA045','AA046','AA047','AA048'))
AND TerminalID = @sTerminalID  
  
--BEGIN Get GPRS  
DECLARE @TempTagCount INT, @TotalTempTag INT, @TagValidasi VARCHAR(MAX)  
DECLARE @C_TerminalID VARCHAR (8), @C_Name VARCHAR(20), @C_Tag VARCHAR(5),   
  @C_ItemName VARCHAR(20), @C_TagLength INT, @C_TagValue VARCHAR(MAX)   
       
INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
FROM dbo.fn_tbProfileContentTLE(@sTerminalID,NULL)  
  
IF dbo.IsGPRSInit(@sTerminalId)=1  
BEGIN   
	--INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
	--SELECT TerminalID,Name, Tag, ItemName, TagLength, TagValue 
	--FROM dbo.fn_tbProfileContentGPRS(@sTerminalID,NULL)  

 DECLARE TagValue_cursor CURSOR FOR  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentGPRS(@sTerminalID,NULL)  
  
 OPEN TagValue_cursor  
  
 SELECT DISTINCT @TotalTempTag = COUNT(*) FROM #tempTag  
     
 FETCH NEXT FROM TagValue_cursor  
 INTO @C_TerminalID, @C_Name, @C_Tag, @C_ItemName,@C_TagLength, @C_TagValue  
  
 WHILE @@FETCH_STATUS = 0  
 BEGIN  
  SET @TempTagCount = 1  
  WHILE @TempTagCount<=@TotalTempTag  
  BEGIN   
  SELECT @TagValidasi=TempTag FROM #tempTag WHERE IdRow = @TempTagCount  
   --1 tag ok  
   IF @C_TagValue = @TagValidasi AND (@C_Tag='GP01' OR @C_Tag='GP001') AND @C_Name = @TagValidasi  
   BEGIN  
   --PRINT 'test1'  
   INSERT INTO #TempGPRS(TerminalID,Name, Tag, ItemName, TagLength, TagValue)  
   VALUES (@C_TerminalID, @C_Name, @C_Tag, @C_ItemName,@C_TagLength, @C_TagValue)  
       
   FETCH NEXT FROM TagValue_cursor  
   INTO @C_TerminalID, @C_Name, @C_Tag, @C_ItemName,@C_TagLength, @C_TagValue  
    IF @C_Name = @TagValidasi AND (@C_Tag='GP02' OR @C_Tag='GP002')  
    BEGIN  
     --PRINT 'test2'  
     INSERT INTO #TempGPRS(TerminalID,Name, Tag, ItemName, TagLength, TagValue)  
     VALUES (@C_TerminalID, @C_Name, @C_Tag, @C_ItemName,@C_TagLength, @C_TagValue)  
         
     FETCH NEXT FROM TagValue_cursor  
     INTO @C_TerminalID, @C_Name, @C_Tag, @C_ItemName,@C_TagLength, @C_TagValue  
     IF (@C_Tag='GP03' OR @C_Tag='GP003') AND @C_Name = @TagValidasi  
     BEGIN   
     --PRINT 'test3'  
     INSERT INTO #TempGPRS(TerminalID,Name, Tag, ItemName, TagLength, TagValue)  
     VALUES (@C_TerminalID, @C_Name, @C_Tag, @C_ItemName,@C_TagLength, @C_TagValue)  
     END  
    END  
        
   END    
   ELSE  
   SET @TempTagCount = @TempTagCount +1  
  END      
  -- Get the next author.   
  PRINT @C_TagValue  
  FETCH NEXT FROM TagValue_cursor  
  INTO @C_TerminalID, @C_Name, @C_Tag, @C_ItemName,@C_TagLength, @C_TagValue     
 END  
  
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalID,Name, Tag, ItemName, TagLength, TagValue FROM #TempGPRS  
  
 CLOSE TagValue_cursor  
 DEALLOCATE TagValue_cursor  
END  
  
--INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
--SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
--FROM dbo.fn_tbProfileContentTLE(@sTerminalId,NULL)  
--UNION ALL  
--SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
--FROM dbo.fn_tbProfileContentGPRS(@sTerminalId,NULL)  
  
--IF (dbo.IsCurrencyInit(@sTerminalID) = 1)  
--BEGIN  
-- INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
-- SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
-- FROM dbo.fn_tbProfileContentCurrency(@sTerminalId,NULL)  
--END  
  
DECLARE @iEMVInit BIT  
SET @iEMVInit=dbo.IsEMVInitByDatabase(@sTerminalID) 
IF @iEMVInit=1  
BEGIN  
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentAID(@sTerminalID,NULL)  
  
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentCAPK(@sTerminalId,NULL)  
END  
  
IF dbo.IsPinPadInit(@sTerminalId)=1  
BEGIN  
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentPinPad(@sTerminalID,NULL)  
END  

IF DBO.IsRemoteDownloadInit(@sTerminalID)=1
BEGIN
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentRemoteDownload(@sTerminalID,NULL)  
END
  
SELECT * FROM @tbInitTerminal  
ORDER BY Id