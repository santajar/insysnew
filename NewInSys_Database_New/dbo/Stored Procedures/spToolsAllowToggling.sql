﻿


CREATE  PROCEDURE [dbo].[spToolsAllowToggling](
@RetVal NVARCHAR(100) OUTPUT,
@sTerminalID VARCHAR(8),
@sMaxInitRetry int,
--@sValue int = null,
@sDateTime VARCHAR(15)
)AS
BEGIN

SET NOCOUNT ON
--BEGIN
--IF @sValue=1
BEGIN
UPDATE tbProfileTerminal SET TerminalTagValue = @sDateTime WHERE TerminalTag IN ('DC003','DC03')  AND TerminalID = @sTerminalID
UPDATE tbProfileTerminalList SET AllowDownload=1,MaxInitRetry=@sMaxInitRetry WHERE TerminalID=@sTerminalID
END
IF @@ROWCOUNT > 0
BEGIN
SET @RetVal = 'Success'
END


----IF @sValue=0
----BEGIN
----UPDATE tbProfileTerminalList SET AllowDownload=0,MaxInitRetry=0 WHERE TerminalID=@sTerminalID
----END
------UPDATE tbProfileTerminalList SET AllowDownload=1, AutoInitTimeStamp=GETDATE(), MaxInitRetry=@sMaxInitRetry WHERE TerminalID=@sTerminalID
----UPDATE tbProfileTerminalList SET AllowDownload=0,MaxInitRetry=0 WHERE TerminalID=@sTerminalID
----IF @@ROWCOUNT > 0
----BEGIN
----SET @RetVal = 'Success'
----END
----END
--IF @sValue = 1
--BEGIN
--UPDATE tbProfileTerminalList SET AllowDownload=@sValue,MaxInitRetry=3 WHERE TerminalID=@sTerminalID
--END

END