﻿CREATE PROCEDURE [dbo].[spReportFiturView]
AS

IF OBJECT_ID('NewInSys..tbReportFitur') IS NOT NULL
BEGIN
	SELECT * FROM tbReportFitur WITH (NOLOCK)
END
ELSE
	SELECT 'Error on creating Report'