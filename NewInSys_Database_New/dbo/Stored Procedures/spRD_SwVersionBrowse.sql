﻿CREATE PROCEDURE [dbo].[spRD_SwVersionBrowse]
	@sCondition VARCHAR(500)=''
AS
DECLARE @sQuery VARCHAR(MAX)
SET @sQuery = '
SELECT SV_NAME, SV_ID, [ALL], GROUP_ID
FROM tbRD_SoftwareVersion 
WHERE [Enabled]=1
'
EXEC (@sQuery + @sCondition)
