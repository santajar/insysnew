USE [NewInSys_BCA-DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_viewRelationTLV]    Script Date: 11/2/2017 2:22:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_viewRelationTLV](@sTerminalId VARCHAR(8))
RETURNS @viewRelationTLV TABLE(
	[TerminalID] VARCHAR(8) NOT NULL,
	[Tag] VARCHAR(5) NOT NULL,
	[TagLength] INT NOT NULL,
	[LengthOfTagLength] INT NOT NULL,
	[TagValue] VARCHAR(150) NULL,
	[RelationTagID] BIGINT NULL,
	[DatabaseID] SMALLINT NULL
)
AS
BEGIN
	INSERT INTO @viewRelationTLV
	select u.TerminalID,u.Tag,len(TagValue)[TagLength],len(len(TagValue))[LengthOfTagLength],TagValue,u.ID[RelationTagID],tl.DatabaseID from 
(
	select ID,TerminalID,TagCard[Tag],ValueCard[TagValue] 
	from ProfileRelationNew 

	union all
	select ID,TerminalID,TagIssuer[Tag],ValueIssuer[TagValue]
	from ProfileRelationNew 
	
	union all
	select ID,TerminalID,TagAcquirer[Tag],ValueAcquirer[TagValue]
	from ProfileRelationNew 	
)u
join tbProfileTerminalList tl
on u.TerminalID=tl.TerminalID
where u.TerminalID=@sTerminalId
order by u.ID
	RETURN
END

GO


