﻿
CREATE PROCEDURE [dbo].[spProfileTLEDeleteTagFromAll]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5)
AS
	DELETE FROM tbProfileTLE
	WHERE DatabaseID = @sDatabaseID
		 AND TLETag = @sTag 

