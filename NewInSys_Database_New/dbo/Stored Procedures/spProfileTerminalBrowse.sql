USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileTerminalBrowse]    Script Date: 11/2/2017 11:59:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 3, 2010
-- Modify date: 
--				1. August 12, 2010, adding column TerminalTagLength
--				2. September 8, 2010, Var @sTerminalID diubah jadi @sCondition sebagai pengganti statement 'WHERE'
-- Description:	
--				1. view The Terminal table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalBrowse]
	@sCondition VARCHAR(MAX)=NULL,
	@sTerminalID VARCHAR(8)
AS
BEGIN
	CREATE  TABLE #viewTerminalDetail(
	[TerminalID] VARCHAR(8) NOT NULL,
	[TerminalTag] VARCHAR(5) NOT NULL,
	[TerminalTagLength] INT NOT NULL,
	[TerminalTagValue] VARCHAR(150) NULL
)
	CREATE  TABLE #terminaUnpivot(
	[TerminalID] VARCHAR(8) NOT NULL,
	[Name] VARCHAR(30) NOT NULL,
	[Tag] VARCHAR(5) NOT NULL,
	[ItemName] VARCHAR(30) NOT NULL,
	[TagLength] INT NOT NULL,
	[TagValue] VARCHAR(150) NULL
)

INSERT INTO #terminaUnpivot
EXEC spProfileUnpivotTerminal @sTerminalID

INSERT INTO #viewTerminalDetail
select TerminalID,Tag[TerminalTag],TagLength[TerminalTagLength],TagValue[TerminalTagValue] 
from #terminaUnpivot
	--RETURN
DECLARE @sQuery VARCHAR(MAX)
SET @sQuery='SELECT TerminalID, 
		TerminalTag AS Tag, 
		TerminalTagLength, 
		TerminalTagValue AS Value  from #viewTerminalDetail '
--print @sQuery
--exec select * from @viewTerminalDetail + @sCondition+ ' ORDER BY TerminalID,Tag'
exec (@sQuery + @sCondition+ ' ORDER BY TerminalID,TerminalTag')
EXEC spProfileTerminalUpdateLastView @sTerminalID
drop table #viewTerminalDetail
drop table #terminaUnpivot
END




GO


