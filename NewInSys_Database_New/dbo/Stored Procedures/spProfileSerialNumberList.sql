﻿-- =============================================
-- Author:		ibnu saefullah
-- Create date: 20 maret 2017
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[spProfileSerialNumberList]
	-- Add the parameters for the stored procedure here
	@sSerialNumber varchar(30)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT SerialNumber From tbProfileSerialNumberList WITH (NOLOCK) Where SerialNumber= '' + @sSerialNumber + ''
END