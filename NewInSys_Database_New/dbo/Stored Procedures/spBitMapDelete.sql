﻿
-- ======================================================================
-- Description:	 Delete data from tbBitMap based on given ApplicationName
-- ======================================================================

CREATE PROCEDURE [dbo].[spBitMapDelete]
	@sApplicationName VARCHAR(30)
AS
	SET NOCOUNT ON
	DELETE FROM tbBitMap
	WHERE ApplicationName = @sApplicationName