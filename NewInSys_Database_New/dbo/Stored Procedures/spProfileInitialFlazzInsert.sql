﻿-- =============================================
-- Author:		Tobias Setyo
-- Create date: Nov 14, 2016
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Insert new InitialFlazz Management into tbProfileInitialFlazz
--				2. Modify to use form flexible`
-- =============================================
CREATE PROCEDURE [dbo].[spProfileInitialFlazzInsert]
	@iDatabaseID INT,
	@sInitialFlazzName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfileInitialFlazz WITH (NOLOCK)
WHERE InitialFlazzTag IN ('IF01','IF001') AND
		InitialFlazzTagValue = @sInitialFlazzName AND
		DatabaseID = @iDatabaseID

IF @iCountID =0
BEGIN
	IF (dbo.isTagLength4(@iDatabaseID)= 4)
	BEGIN
		INSERT INTO tbProfileInitialFlazz(
			
			[InitialFlazzName],
			[DatabaseID],
			[InitialFlazzTag],
			[InitialFalzzLengthOfTagLength],
			[InitialFlazzTagLength],
			[InitialFlazzTagValue]
			)
		SELECT 
			
			[Name] [InitialFlazzName],
			@iDatabaseID [DatabaseId],
			Tag [InitialFlazzTag],
			LEN(TagLength) [InitialFalzzLengthOfTagLength],
			TagLength [InitialFlazzTagLength],
			TagValue [InitialFlazzTagValue]

		FROM dbo.fn_TbProfileTLV(@sInitialFlazzName,@sContent)
	END
	ELSE
	BEGIN
		INSERT INTO tbProfileInitialFlazz(
			[InitialFlazzName],
			[DatabaseID],
			[InitialFlazzTag],
			[InitialFalzzLengthOfTagLength],
			[InitialFlazzTagLength],
			[InitialFlazzTagValue]
			)
		SELECT 
		
			[Name] [InitialFlazzName],
				@iDatabaseID [DatabaseId],
			Tag [InitialFlazzTag],
			LEN(TagLength) [InitialFalzzLengthOfTagLength],
			TagLength [InitialFlazzTagLength],
			TagValue [InitialFlazzTagValue]

		FROM dbo.fn_TbProfileTLV5(@sInitialFlazzName,@sContent)
	END
END
exec spItemComboBoxValueInsertInitialFlazz @sInitialFlazzName, @iDatabaseID
--UPDATE tbProfileTerminalList
--SET InitialFlazz = '1'
--WHERE DatabaseID = @iDatabaseID