﻿CREATE Procedure [dbo].[spProfileRequestPaperReceiptListBrowse]
	@sDatabaseId VARCHAR(3)
AS
	SELECT DISTINCT ReqName as Name,
			DatabaseID			
	FROM tbProfileReqPaperReceiptManagement WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId