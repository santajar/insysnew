﻿CREATE PROCEDURE [dbo].[spAppPackageEdcListBrowse]
	@sCondition Varchar(max)= null

AS
	SET NOCOUNT ON

Declare @sQuery VARCHAR(MAX)

set @sQuery ='select AppPackId,BuildNumber,AppPackageName,AppPackageFilename,AppPackageContent,AppPackageDesc,EdcTypeID,UploadTime from tbAppPackageEDCList WITH (NOLOCK) '

if @sCondition is not null
	exec (@sQuery+@sCondition)
else
	exec(@sQuery)