﻿CREATE PROCEDURE [dbo].[spAppPackageBrowseDetail]
	@sAppPackageName VARCHAR(150)
AS
	SET NOCOUNT ON
SELECT BuildNumber,
	AppPackageName, 
	AppPackageFilename,
	AppPackageDesc,
	EdcTypeId
FROM tbAppPackageEdcList WITH (NOLOCK)
WHERE AppPackageName = @sAppPackageName