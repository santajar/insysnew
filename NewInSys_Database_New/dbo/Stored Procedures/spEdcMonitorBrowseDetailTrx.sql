﻿-- =============================================
-- Author		: Tobias Setyo
-- Create date	: March 16, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spEdcMonitorBrowseDetailTrx]
	@sTimestamp VARCHAR(50),
	@sSerialNumber VARCHAR(50)
AS
SELECT TerminalID
	,SoftwareVersion [Software Version]
	,SettlementDateTime [Settlement DateTime(hhMMyyhhmmss)]
	,CardName [Card Label]
	,CardTotalAmount [Total Amount]
	,CardTotalTransaction [Total Transaction]
FROM tbEdcMonitorDetail WITH(NOLOCK)
WHERE ID_Header = (SELECT ID FROM tbEdcMonitor WITH(NOLOCK) WHERE MessageTimestamp=CONVERT(datetime,@sTimestamp) AND SerialNumber=@sSerialNumber
)