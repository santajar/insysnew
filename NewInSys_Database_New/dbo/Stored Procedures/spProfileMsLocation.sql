﻿CREATE PROCEDURE [dbo].[spProfileMsLocation]
AS
SELECT LocationID,
	LocationDesc
FROM tbLocation WITH (NOLOCK)
