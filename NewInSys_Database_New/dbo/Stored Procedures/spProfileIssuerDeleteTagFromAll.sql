﻿
CREATE PROCEDURE [dbo].[spProfileIssuerDeleteTagFromAll]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5)
AS
	DELETE FROM tbProfileIssuer
	WHERE TerminalID IN (SELECT TerminalID 
						 FROM tbProfileTerminalList WITH (NOLOCK)
						 WHERE DatabaseID = @sDatabaseID
						)
		 AND IssuerTag = @sTag 




