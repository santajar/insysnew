﻿CREATE PROCEDURE [dbo].[spTbProfileContentSortNew](  
 @sTerminalID VARCHAR(8),  
 @sConditions VARCHAR(MAX)=NULL
 )  
AS
CREATE TABLE #tempTerminal(
		ID INT IDENTITY,
		TerminalId VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(500)
	)


DECLARE @sDatabaseID VARCHAR(4)
SELECT @sDatabaseID=DatabaseID 
FROM tbProfileTerminalList WITH(NOLOCK)
WHERE TerminalID=@sTerminalID

DECLARE @tbItemList TABLE(
	[ItemID] [int] NOT NULL,
	[DatabaseID] [smallint] NOT NULL,
	[FormID] [smallint] NOT NULL,
	[ItemSequence] [smallint] NOT NULL,
	[ItemName] [varchar](24) NOT NULL,
	[ObjectID] [smallint] NOT NULL,
	[DefaultValue] [text] NULL,
	[LengthofTagValueLength] [int] NOT NULL,
	[vAllowNull] [bit] NOT NULL,
	[vUpperCase] [bit] NOT NULL,
	[vType] [varchar](5) NULL,
	[vMinLength] [smallint] NOT NULL,
	[vMaxLength] [smallint] NOT NULL,
	[vMinValue] [int] NULL,
	[vMaxValue] [int] NULL,
	[vMasking] [bit] NOT NULL,
	[ValidationMsg] [varchar](50) NULL,
	[Tag] [varchar](5) NULL
)

INSERT INTO @tbItemList
SELECT * 
FROM tbItemList WITH(NOLOCK) WHERE DatabaseID=@sDatabaseID

INSERT INTO #tempTerminal(TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
SELECT 
	A.TerminalID,
	A.TerminalID Name, 
	A.TerminalTag AS Tag, 
	I.ItemName, 
	A.TerminalTagLength AS TagLength, 
    A.TerminalTagValue AS TagValue
FROM tbProfileTerminal A WITH(NOLOCK) INNER JOIN 
	(SELECT * FROM @tbItemList WHERE FormID=1) I ON A.TerminalTag=I.Tag
WHERE A.TerminalID=@sTerminalId AND A.TerminalTag LIKE 'DE%'
ORDER BY Name, A.TerminalTag

INSERT INTO #tempTerminal(TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
SELECT 
	A.TerminalID, 
	A.TerminalID Name, 
	A.TerminalTag AS Tag, 
	I.ItemName, 
	A.TerminalTagLength AS TagLength, 
    A.TerminalTagValue AS TagValue
FROM tbProfileTerminal A WITH(NOLOCK) INNER JOIN 
	(SELECT * FROM @tbItemList WHERE FormID=1) I ON A.TerminalTag=I.Tag
WHERE A.TerminalID=@sTerminalId AND A.TerminalTag LIKE 'DC%'

DECLARE @tbProfileRelation TABLE(
	[RelationTagID] BIGINT ,
	[TerminalID] VARCHAR(8) NOT NULL,
	[RelationTag] VARCHAR(5) NOT NULL,
	[RelationTagLength] INT NOT NULL,
	[RelationTagValue] VARCHAR(50) NULL,
	[Description] VARCHAR(50) NULL
	)
INSERT INTO @tbProfileRelation
	(RelationTagID,TerminalID,RelationTag,RelationTagLength,RelationTagValue )
SELECT RelationTagID,TerminalID,RelationTag,RelationTagLength,RelationTagValue 
FROM tbProfileRelation WITH(NOLOCK)
WHERE TerminalID=@sTerminalId

DECLARE @tbProfileCardList TABLE(
	[CardID] INT,
	[DatabaseID] INT NOT NULL,
	[CardName] VARCHAR(50) NOT NULL)
INSERT INTO @tbProfileCardList
SELECT * FROM tbProfileCardList WITH(NOLOCK)
WHERE DatabaseID=@sDatabaseID

INSERT INTO #tempTerminal(TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
SELECT     
	@sTerminalID,
	CL.CardName AS Name, 
	tbProfileCard.CardTag Tag, 
	I.ItemName, 
    tbProfileCard.CardTagLength TagLength, 
    tbProfileCard.CardTagValue TagValue
FROM
	@tbProfileCardList CL INNER JOIN
	tbProfileCard WITH(NOLOCK) ON CL.CardID = tbProfileCard.CardID INNER JOIN
	(SELECT * FROM @tbItemList WHERE FormID=4) I ON tbProfileCard.CardTag = I.Tag
WHERE 
	tbProfileCard.CardID IN (
	SELECT CardID 
	FROM @tbProfileCardList
	WHERE CardName IN ( 
			SELECT RelationTagValue FROM @tbProfileRelation
			WHERE RelationTag IN ('AD01','AD001') AND ISNULL(RelationTagValue,'')<>''
		)
)
ORDER BY Name, Tag

INSERT INTO #tempTerminal(TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
SELECT T.TerminalID, 
	T.IssuerName AS Name, 
	T.IssuerTag AS Tag, 
	I.ItemName, 
	T.IssuerTagLength AS TagLength, 
	T.IssuerTagValue AS TagValue
FROM 
	(SELECT * FROM tbProfileIssuer WITH(NOLOCK) WHERE TerminalID=@sTerminalID)T INNER JOIN 
	(SELECT * FROM @tbItemList WHERE FormID=3) I ON T.IssuerTag=I.Tag
ORDER BY Name, Tag

INSERT INTO #tempTerminal(TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
SELECT
	T.TerminalID, 
	T.AcquirerName AS Name, 
	T.AcquirerTag AS Tag, 
	I.ItemName, 
	T.AcquirerTagLength AS TagLength, 
    T.AcquirerTagValue AS TagValue
FROM tbProfileAcquirer T WITH(NOLOCK) INNER JOIN 
	(SELECT * FROM @tbItemList WHERE FormID=2) I ON T.AcquirerTag=I.Tag
WHERE T.TerminalID=@sTerminalId
ORDER BY Name, Tag

CREATE TABLE #tempRelation(
		Id INT IDENTITY,
		TerminalId VARCHAR(8),
		Tag VARCHAR(5),
		TagLength VARCHAR(3),
		TagValue VARCHAR(500),
		TagId BIGINT
	)

INSERT INTO #tempRelation(TerminalId,Tag,TagLength,TagValue,TagId)
SELECT 
	R.TerminalID, 
	R.RelationTag AS Tag, 
	R.RelationTagLength AS TagLength, 
	R.RelationTagValue AS TagValue, 
	R.RelationTagID
FROM
	(SELECT * FROM @tbItemList WHERE FormID=5) I INNER JOIN
	(SELECT * FROM @tbProfileRelation) R ON I.Tag = R.RelationTag
ORDER BY R.RelationTagID

INSERT INTO #tempTerminal(TerminalId,Tag,TagLength,TagValue)
SELECT 
	TerminalID, 
	Tag,
	TagLength,
	TagValue
FROM #tempRelation
WHERE ISNULL(TagID,'')<>'' AND TagID NOT IN
        (
			SELECT TagID
			FROM    
			(
				SELECT TagID
				FROM #tempRelation
				WHERE TagValue IS NULL AND Tag IN ('AD03','AD003')
				UNION ALL
				SELECT TagID - 1
				FROM #tempRelation
				WHERE (TagValue IS NULL) AND Tag IN ('AD03','AD003')
				UNION ALL
				SELECT TagID - 2
				FROM #tempRelation
				WHERE (TagValue IS NULL) AND Tag IN ('AD03','AD003')
			) viewRelationNull
		)

SELECT * FROM #tempTerminal
DROP TABLE #tempTerminal
DROP TABLE #tempRelation