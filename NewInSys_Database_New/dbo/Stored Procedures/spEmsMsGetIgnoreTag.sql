﻿CREATE PROCEDURE [dbo].[spEmsMsGetIgnoreTag]
	@sVersion VARCHAR(5)
AS
SELECT [ID]
    ,[EmsVersion]
    ,[Tag]
    ,[ItemName]
FROM [tbEmsMsIgnoreTag]
WHERE [EmsVersion] = @sVersion