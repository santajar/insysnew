﻿


-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 2, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Update the AID on table tbProfileAID
--				2. Modify for Form Flexsible
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAIDUpdate]
	@iDatabaseID INT,
	@sAIDName VARCHAR(20),
	@sContent VARCHAR(MAX)
AS

	--IF (OBJECT_ID('tempdb..##OldAID') IS NOT NULL)
	--	DROP TABLE ##OldAID

	--SELECT AIDTag, AIDTagValue
	--INTO ##OldAID
	--FROM tbProfileAID
	--WHERE DatabaseID = @sDatabaseID AND
	--		AIDName = @sAIDName

	EXEC spProfileAIDDelete @iDatabaseID, @sAIDName
	EXEC spProfileAIDInsert @iDatabaseID,@sAIDName, @sContent

	--IF (OBJECT_ID('tempdb..##NewAID') IS NOT NULL)
	--	DROP TABLE ##NewAID

	--SELECT AIDTag, AIDTagValue
	--INTO ##NewAID
	--FROM tbProfileAID
	--WHERE DatabaseID = @sDatabaseID AND
	--		AIDName = @sAIDName

	--SELECT C.TagName, A.AIDTagValue OldAIDValue, b.AIDTagValue NewAIDValue
	--FROM ##OldAID A FULL OUTER JOIN ##NewAID B ON A.AIDTag = B.AIDTag
	--	LEFT JOIN TBITEMLISTAID_CAPK C ON A.AIDTag = C.TAG
	--WHERE ISNULL(A.AIDTagValue,'') != ISNULL(B.AIDTagValue,'')



