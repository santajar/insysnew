﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spListTerminalIDPackageBrowse]
	@sCondition VARCHAR(MAX)= NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sQuery VARCHAR(MAX);
    SET @sQuery = 'SELECT TerminalID,GroupName FROM tbListTerminalIDPackage '

	EXEC (@sQuery + @sCondition )

END