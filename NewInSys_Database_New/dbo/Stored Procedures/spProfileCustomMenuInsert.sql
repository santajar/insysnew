﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[spProfileCustomMenuInsert]
	@sTerminalID VARCHAR(8),
	@sContent VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @iDBID INT
	SELECT @iDBID=DatabaseID 
	FROM tbProfileTerminalList WITH (NOLOCK) 
	WHERE TerminalID = @sTerminalID

	IF (dbo.isTagLength4(@iDBID)=5)
	BEGIN
		INSERT INTO tbProfileCustomMenu(TerminalID,CustomMenuTag,CustomMenuLengthOfTagLength,CustomMenuTagLength,CustomMenuTagValue)
			SELECT TerminalID,
			Tag,
			LEN(TagLength) AS LenghtOfTagLength,
			TagLength,
			TagValue
		FROM dbo.fn_TbProfileTLV5(@sTerminalID,@sContent)
		ORDER BY Tag
	END
	ELSE
	BEGIN
		INSERT INTO tbProfileCustomMenu(TerminalID,CustomMenuTag,CustomMenuLengthOfTagLength,CustomMenuTagLength,CustomMenuTagValue)
			SELECT TerminalID,
			Tag,
			LEN(TagLength) AS LenghtOfTagLength,
			TagLength,
			TagValue
		FROM dbo.fn_TbProfileTLV(@sTerminalID,@sContent)
		ORDER BY Tag
	END
END