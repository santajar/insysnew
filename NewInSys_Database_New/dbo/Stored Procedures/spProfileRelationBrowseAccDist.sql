﻿-- ===========================================================================
-- Author:		Tatang Sumarta
-- Create date: May 02, 2017
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Distinct  RelationTagValue accuirer on table tbProfileRelation
--				2. Modify for Form Flexsible web insys
-- ============================================================================
CREATE PROCEDURE [dbo].[spProfileRelationBrowseAccDist]
	@sTerminalId VARCHAR(8)
	
AS
	SELECT DISTINCT (RelationTagValue)
	FROM tbProfileRelation WITH (NOLOCK)
		WHERE TerminalId=@sTerminalId AND (RelationTag = 'AD03' OR RelationTag = 'AD003') AND RelationTagValue IS NOT NULL