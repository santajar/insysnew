﻿

-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. pack the response message into ISO format
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseISOPack]
	@iCheckInit INT,
	@sNII VARCHAR(4),
	@sAddress VARCHAR(4),
	@sMTI VARCHAR(4),
	@sSessionTime VARCHAR(10),
	@sTerminalId VARCHAR(8) OUTPUT
AS
SET NOCOUNT ON
print '[spOtherParseISOPack]'
DECLARE @sContent VARCHAR(max),
	@sTag VARCHAR(4),
	@sISOMessage VARCHAR(max)
DECLARE @bValidTerminalId BIT,
	@iErrorCode INT -- Error Init status, open tbMSErrorInit for detail.
DECLARE @sProcCode VARCHAR(6)

DECLARE @sBit11 VARCHAR(MAX)
DECLARE @sBit48 VARCHAR(MAX)
DECLARE @sBit58 VARCHAR (10)
DECLARE @sEdcSn VARCHAR(10)
DECLARE @sAppName VARCHAR(15)

--CREATE TABLE #temp
--(
--	InitId INT,
--	TerminalId VARCHAR(8),
--	Cont VARCHAR(MAX),
--	Tag VARCHAR(5),
--	Flag BIT
--)

--INSERT INTO #temp (InitId,TerminalId,Cont,Tag,Flag)
--EXEC sp_tbInitBrowse @sTerminalID

IF dbo.IsInitMTI(@sMTI) = 1
BEGIN
	-- get the bit-bit value
	EXEC spOtherParseProcCode @sSessionTime, @sProcCode OUTPUT
	EXEC spOtherParseTerminalId @sSessionTime, @sTerminalId OUTPUT
	EXEC spOtherParseBit11 @sSessionTime, @sBit11 OUTPUT
	--EXEC spOtherParseBit48 @sSessionTime, @sBit48 OUTPUT, @sEdcSn OUTPUT, @sAppName OUTPUT
	print '1test'
	IF dbo.IsValidTerminalID(@sTerminalID) = 1
	IF dbo.IsValidTerminalID(@sTerminalID) = 1
	BEGIN
		IF @iCheckInit=0 OR (@iCheckInit=1 AND dbo.IsInitAllowed(@sTerminalID) = 1) OR
			dbo.isProcodeAllowed (@sProcCode)= 1 
		BEGIN
			IF @sProcCode = '930000' OR @sProcCode = '930001' OR 
			   @sProcCode = '960000' OR @sProcCode = '960001' 
			BEGIN
				EXEC spOtherParseBit48 @sSessionTime, @sBit48 OUTPUT, @sEdcSn OUTPUT, @sAppName OUTPUT
				IF (dbo.isApplicationAllowInit(@sTerminalID, @sAppName) = 1)
				BEGIN
					IF dbo.IsInitStart(@sProcCode) = 1 -- UPDATE SAMA YG CONTINUE
					BEGIN
						IF dbo.isAllowCompress(@sAppName) = 1
						BEGIN
							IF dbo.IsCompressInit(@sTerminalID) = 0
								EXEC spInitStart @sTerminalID
							ELSE
								EXEC spInitCompressStart @sTerminalID
						END
						ELSE 
							EXEC spInitStart @sTerminalID

						EXEC spAuditInitInsert
							@sTerminalID,
							@sAppName,
							@sEdcSn,
							'Initialize Start'
						-- insert ke tbInitLogConn (gk pake deskripsi dan persentase)
						EXEC spInitLogConnNewInit @sTerminalID,'1'
					END

					DECLARE @iCurrInitId INT, @bFlag BIT
					
					IF dbo.isAllowCompress(@sAppName) = 1
						IF dbo.IsCompressInit(@sTerminalID) = 0 
							SET @sBit58 = '000130'
						ELSE
							SET @sBit58 = '000131'
	--!!!!!!!!!!!! WARNING COMPRESSED INIT!!!!!!!!!!!!!!!!

					SELECT @iCurrInitId=InitId,
						@sTag=Tag,
						@sContent=Cont
					FROM dbo.fn_tbInitBrowse (@sTerminalID)
					
					DECLARE @sCond VARCHAR(MAX)
					SELECT @sCond = dbo.sUpdateInitFlagCond(@sTerminalID, @iCurrInitId)


					EXEC spInitEdit @sCond
					
					SET @iErrorCode = dbo.iInitErrorCode(@sTerminalID, @iCurrInitId)
					
					IF @iErrorCode=1
					BEGIN
						EXEC spAuditInitInsert
							@sTerminalID,
							@sAppName,
							@sEdcSn,
							'Initialize Complete'
						-- update ke tbInitLogConn (desc n %) --> finish
						EXEC spInitLogConnNewInit @sTerminalID,'0'

						IF @sProcCode = '930001'
						BEGIN
							UPDATE tbProfileTerminalList
							SET AllowDownload = 0
							WHERE TerminalID = @sTerminalID
						END
					END
					ELSE IF @iErrorCode = 0
						 --update ke tbInitLogConn (desc n %) --> PROCESSING
						EXEC spInitLogConnNewInit @sTerminalID,'0'
				END
				ELSE
				BEGIN
					SET @sContent = 'APPLICATION NOT ALLOWED'
					SET @sBit11 = NULL
					SET @iErrorCode = 6
				END
			END
			ELSE IF @sProcCode = '940000' -- If Init Flazz
			BEGIN
				--EXEC spOtherParseBit48InitFlazz @sSessionTime
				EXEC spOtherParseBit48InitFlazz @sSessionTime, @sAppName OUTPUT
				SET @sContent = NULL
				SET @iErrorCode = 0
			END
			ELSE IF @sProcCode = '950000' -- If AutoInit
			BEGIN
				print 'autoinit'
				EXEC spOtherParseBit48AutoInit @sSessionTime, @sAppName OUTPUT
				
				IF (dbo.isApplicationAllowInit(@sTerminalID, @sAppName) = 1)
				BEGIN
					EXEC spAutoInitAllow @sTerminalID, @sContent OUTPUT
					IF dbo.IsCompressInit(@sTerminalID)=1
						SET @sContent = dbo.sStringToHexString(@sContent)
					SET @iErrorCode = 0
					SET @sBit58 = ''
				END
				ELSE
				BEGIN
					SET @sContent = 'APPLICATION NOT ALLOWED'
					SET @sBit11 = NULL
					SET @iErrorCode = 6
				END
			END
			ELSE IF @sProcCode = '970000' -- If Auto Init Success
			BEGIN
				-- Set AutoInit to False
				UPDATE tbProfileTerminalList
				SET AllowDownload = 0
				WHERE TerminalID = @sTerminalID

				SET @sContent = 'AUTO INIT SUCCESS'

					IF dbo.IsCompressInit(@sTerminalID)=1
					SET @sContent = dbo.sStringToHexString(@sContent)

				SET @iErrorCode = 0
			END
			ELSE
			BEGIN
				SET @sContent = 'INVALIDPROCCODE'
				SET @sBit11 = NULL
				SET @iErrorCode = 2
			END
		END
		ELSE
		BEGIN
			SET @sContent = 'NOTALLOWED'
			SET @sBit11 = NULL
			SET @iErrorCode = 3
		END
	END
	ELSE
	BEGIN
		SET @sContent = 'INVALIDTID'
		SET @sBit11 = NULL
		SET @iErrorCode = 4
	END
END
ELSE
BEGIN
	SET @sContent = 'INVALIDMTI'
	SET @sBit11 = NULL
	SET @iErrorCode = 5
END
PRINT 'Content : ' + ISNULL(@sContent,'null')
PRINT ('Procode : ' + @sProcCode)
PRINT ('NII : ' + @sNII)
PRINT ('BIT 11 : ' + ISNULL(@sBit11,'NULL'))

SELECT @sISOMessage = dbo.sGenerateISOSend(@sTerminalID,
	@sProcCode, @sNII, @sAddress, @sBit11, @sBit48, @sBit58,NULL, @sContent, @sAppName)

PRINT 'AppName ' + @sAppName
PRINT 'Allow Compress : ' + ISNULL(@sBit58, 'null')
print '@sBit48 : ' + ISNULL(@sBit48,'null')

print '@sISOMessage : ' + @sISOMessage

DELETE FROM tbInitResult
WHERE TERMINALID=@sTerminalID

INSERT INTO tbInitResult
SELECT @sTerminalID [TERMINALID],
	@sContent [CONTENT],
	@sTag [TAG],
	@sISOMessage [ISO],
	@iErrorCode [ERRORCODE]