﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Sept 30, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating the detail of initialization log
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnUpdateProcess]
	@sTerminalID VARCHAR(8)
AS
	--IF (dbo.iAllowUpdateIntLogConnDetail(@sTerminalID, 'DC') = 1)
	--	EXEC spInitLogConnDetailUpdate @sTerminalID, 'DC'
	--IF (dbo.iAllowUpdateIntLogConnDetail(@sTerminalID, 'DE') = 1)
	--	EXEC spInitLogConnDetailUpdate @sTerminalID, 'DE'
	--IF (dbo.iAllowUpdateIntLogConnDetail(@sTerminalID, 'AA') = 1)
	--	EXEC spInitLogConnDetailUpdate @sTerminalID, 'AA'
	--IF (dbo.iAllowUpdateIntLogConnDetail(@sTerminalID, 'AE') = 1)
	--	EXEC spInitLogConnDetailUpdate @sTerminalID, 'AE'
	--IF (dbo.iAllowUpdateIntLogConnDetail(@sTerminalID, 'AC') = 1)
	--	EXEC spInitLogConnDetailUpdate @sTerminalID, 'AC'
	--IF (dbo.iAllowUpdateIntLogConnDetail(@sTerminalID, 'AD') = 1)
	--	EXEC spInitLogConnDetailUpdate @sTerminalID, 'AD'
	EXEC spInitLogConnUpdate @sTerminalID