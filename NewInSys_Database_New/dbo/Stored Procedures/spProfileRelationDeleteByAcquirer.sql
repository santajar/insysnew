USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileRelationDeleteByAcquirer]    Script Date: 11/2/2017 2:44:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spProfileRelationDeleteByAcquirer]
	@sTerminalID VARCHAR(8),
	@sMessage VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM TbProfileRelation
	WHERE TerminalID = @sTerminalID AND ValueAcquirer=@sMessage
	--select * from tbProfileRelation
--IF(dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalID))=5)
--BEGIN
--delete from tbProfileAcquirer
--where TerminalID=@sTerminalID and AcquirerName=@sMessage

--	INSERT INTO TbProfileRelation
--	(
--		[TerminalID],
--		[RelationTag],
--		[RelationLengthOfTagLength],
--		[RelationTagLength],
--		[RelationtagValue]
--	)
--	SELECT
--		terminalid,
--		tag,
--		len(taglength),
--		taglength,
--		tagvalue
--	FROM dbo.Fn_TbProfileTLV5(@sTerminalID,@sMessage)
--END
--ELSE
--BEGIN
--	INSERT INTO TbProfileRelation
--	(
--		[TerminalID],
--		[RelationTag],
--		[RelationLengthOfTagLength],
--		[RelationTagLength],
--		[RelationtagValue]
--	)
--	SELECT
--		terminalid,
--		tag,
--		len(taglength),
--		taglength,
--		tagvalue
--	FROM dbo.Fn_TbProfileTLV(@sTerminalID,@sMessage)
--END
END


GO


