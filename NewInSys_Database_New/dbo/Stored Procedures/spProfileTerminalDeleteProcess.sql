﻿CREATE PROCEDURE [dbo].[spProfileTerminalDeleteProcess]
	@sTerminalID VARCHAR(8)
AS

---- Delete Terminal table
--IF (SELECT COUNT(TerminalID) FROM tbProfileTerminal WHERE TerminalID=@sTerminalID) > 0
--	EXEC spProfileTerminalDelete @sTerminalID
	
---- Delete Acquirer table
--IF (SELECT COUNT(TerminalID) FROM tbProfileAcquirer WHERE TerminalID=@sTerminalID) > 0
--	EXEC spProfileAcquirerDeleteByTID @sTerminalID
	
---- Delete Issuer table
--IF (SELECT COUNT(TerminalID) FROM tbProfileIssuer WHERE TerminalID=@sTerminalID) > 0
--	EXEC spProfileIssuerDeleteByTID @sTerminalID

---- Delete Relation table
--IF (SELECT COUNT(TerminalID) FROM tbProfileRelation WHERE TerminalID=@sTerminalID) > 0
--	EXEC spProfileRelationDelete @sTerminalID
	
-- Delete profile tid
--EXEC spProfileTerminalListDelete @sTerminalID
exec spProfileTerminalHistoryInsert @sTerminalID
exec spProfileAcquirerHistoryInsertAll @sTerminalID
exec spProfileIssuerHistoryInsertAll @sTerminalID
exec spProfileRelationHistoryInsert @sTerminalID

DELETE FROM tbProfileTerminalList WHERE TerminalID = @sTerminalID
EXEC spListTIDRemoteDownloadDeleteTID @sTerminalID