﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Sept 23, 2010
-- Modify	  :	
--				1. chris <JUL 27, 2012>, <change @sKey VARCHAR(MAX) to VARCHAR(50)>
-- Description:	
--				1. Viewing the Initialization Trail
-- =============================================
CREATE PROCEDURE [dbo].[spAuditInitBrowse]
	@sKey VARCHAR(MAX) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 10000
		TerminalId,
		Software,
		SerialNumber,
		InitTime,
		StatusDesc
	FROM tbAuditInit WITH (NOLOCK)
	WHERE
		TerminalId LIKE '%' + @sKey + '%' OR
		Software LIKE '%' + @sKey + '%' OR
		SerialNumber LIKE '%' + @sKey + '%' OR
		InitTime LIKE '%' + @sKey + '%' OR
		StatusDesc LIKE '%' + @sKey + '%'
	ORDER BY InitID DESC
END