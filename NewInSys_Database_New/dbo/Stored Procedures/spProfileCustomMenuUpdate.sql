﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <19 Maret 2015>
-- Description:	<Update EMV Management>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCustomMenuUpdate]
	@sTerminalID VARCHAR(8),
	@sContent VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileCustomMenuDelete @sTerminalID
	EXEC spProfileCustomMenuInsert @sTerminalID,@sContent
END