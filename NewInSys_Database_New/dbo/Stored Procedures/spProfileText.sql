﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Nov 29, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Get the profile content on 1 long string
-- =============================================
CREATE PROCEDURE [dbo].[spProfileText] 
	@sTerminalID VARCHAR(8),
	@sContent VARCHAR(MAX) OUTPUT
AS
SET NOCOUNT ON

DECLARE @tbInitTerminal TABLE(
	Id INT IDENTITY(1,1) PRIMARY KEY,
	TerminalId VARCHAR(8),
	Name VARCHAR(50),
	Tag VARCHAR(5),
	ItemName VARCHAR(24),
	TagLength SMALLINT,
	TagValue VARCHAR(1000)
)
--CREATE TABLE #tempProfileContentSortNew
--	(
--		TerminalId VARCHAR(8),
--		[Name] VARCHAR(20),
--		Tag VARCHAR(5),
--		ItemName VARCHAR(50),
--		TagLength VARCHAR(3),
--		TagValue VARCHAR(MAX)
--	)

--INSERT INTO #tempProfileContentSortNew(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
--EXEC sp_tbProfileContentSortNew @sTerminalId,null

--CREATE TABLE #tempProfileContentTLE
--	(
--		TerminalId VARCHAR(8),
--		[Name] VARCHAR(20),
--		Tag VARCHAR(5),
--		ItemName VARCHAR(50),
--		TagLength VARCHAR(3),
--		TagValue VARCHAR(MAX)
--	)

--INSERT INTO #tempProfileContentTLE (TerminalId, Name, Tag, ItemName, TagLength, TagValue )
--EXEC sp_tbProfileContentTLE @sTerminalId,null

--CREATE TABLE #tempProfileContentGPRS
--	(
--		TerminalId VARCHAR(8),
--		[Name] VARCHAR(20),
--		Tag VARCHAR(5),
--		ItemName VARCHAR(50),
--		TagLength VARCHAR(3),
--		TagValue VARCHAR(MAX)
--	)

--INSERT INTO #tempProfileContentGPRS (TerminalId, Name, Tag, ItemName, TagLength, TagValue )
--EXEC sp_ProfileContentGPRS @sTerminalId,null

INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
FROM dbo.fn_tbProfileContentSortNew (@sTerminalId,null)
UNION ALL
SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
FROM dbo.fn_tbProfileContentTLE (@sTerminalId,null)
UNION ALL
SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
FROM dbo.fn_tbProfileContentGPRS (@sTerminalId,null)

DECLARE @sTempContent VARCHAR(MAX),
	@sTag VARCHAR(5),
	@sValue VARCHAR(150),
	@iIdStart INT,
	@iIdMax INT
SELECT @iIdStart = MIN(Id) FROM @tbInitTerminal
SELECT @iIdMax = MAX(Id) FROM @tbInitTerminal
SET @sTempContent = ''
WHILE @iIdStart<=@iIdMax
BEGIN
	SELECT @sTag = Tag, @sValue=TagValue
	FROM @tbInitTerminal
	WHERE Id=@iIdStart
	SET @sTempContent = @sTempContent + dbo.sGetTLVString(@sTag, @sValue)
	SET @iIdStart= @iIdStart + 1
END

SET @sContent = @sTempContent
