USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spViewUnpivotIssuer]    Script Date: 11/2/2017 11:55:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[spViewUnpivotIssuer]
		@TerminalID varchar(8)
as
declare @iDbID varchar (4),
		@cols nvarchar(MAX),
		@query nvarchar(max)

set @iDbID=(select DatabaseID 
			from tbProfileTerminalList 
			where TerminalID=@TerminalID)

SET @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=3 and DatabaseID=@iDbID
                    group by tag,ItemID
                    order by tag
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

--select Issuer (Issuer union MasterProfile)
set @query='
select u.[TerminalID],u.[IssuerName][Name],u.[tag],il.ItemName,len(u.[TagValue])[TagLength],len(len(u.[TagValue]))[LengthOfTagLength],u.[TagValue]
from tbProfileIssuer a
unpivot
	(
		[TagValue]
		For Tag in ('+ @cols +')
	) as u
	join tbProfileTerminalList tl
	on u.TerminalID=tl.TerminalID
	join tbItemList il
	on tl.DatabaseID=il.DatabaseID
where u.TerminalID='''+ @TerminalID +''' and u.Tag=il.Tag
---------------//
union
--From table MasterProfile
select tl2.TerminalID,a.IssuerName[Name],m.Tag,il2.ItemName,len(m.TagValue)[TagLength],len(len(m.[TagValue]))[LengthOfTagLength],m.TagValue
from MasterProfile m
	join tbItemList il2
	on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
	join tbProfileTerminalList tl2 
	on tl2.DatabaseID=il2.DatabaseID
	join tbProfileIssuer a
	on tl2.TerminalID=a.TerminalID and a.IssuerName=m.Name
where tl2.TerminalID='''+ @TerminalID +''' and m.FormID=3
--compare tag masterProfile vs tbProfileIssuerNew_2  
	and (select concat(a.IssuerName,''-'',m.Tag)) not in ( select concat (u.[IssuerName],''-'',u.[Tag])
	from tbProfileIssuer a
	unpivot
	(
		[TagValue]
		For Tag in ('+ @cols +')
	) as u
where u.TerminalID='''+ @TerminalID +''') 
order by Name,Tag'

exec (@query)

GO


