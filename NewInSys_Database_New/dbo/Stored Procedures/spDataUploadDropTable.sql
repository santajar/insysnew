﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 6, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. ????
-- =============================================
CREATE PROCEDURE [dbo].[spDataUploadDropTable]
	@sTableName VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt =
		'DROP TABLE [dbo].[' + @sTableName + ']'
	IF OBJECT_ID ('[' + @sTableName + ']') IS NOT NULL
		EXEC (@sStmt)
END
