﻿CREATE PROCEDURE [dbo].[spEdcMonitorDetailInsert]
	@iID_Header INT,
	@sTerminalID VARCHAR(8),
	@sSoftwareVersion VARCHAR(50),
	@sSettlementDateTime VARCHAR(12),
	@sCardName VARCHAR(50),
	@iCardTotalAmount INT,
	@iCardTotalTrx INT
AS
INSERT INTO tbEdcMonitorDetail(ID_Header
	,TerminalID
	,SoftwareVersion
	,SettlementDateTime
	,CardName
	,CardTotalAmount
	,CardTotalTransaction)
VALUES(@iID_Header
	,@sTerminalID
	,@sSoftwareVersion
	,@sSettlementDateTime
	,@sCardName
	,@iCardTotalAmount
	,@iCardTotalTrx)