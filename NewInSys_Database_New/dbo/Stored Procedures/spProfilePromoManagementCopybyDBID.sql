﻿-- =============================================
-- Author:		<Ibnu Saefullah>
-- Create date: <Feb 18 2016>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spProfilePromoManagementCopybyDBID] 
	@NewDatabaseID VARCHAR(3),
	@OldDatabaseID VARCHAR(3),
	@sPromoManagementName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @iCount INT,
			@iOldTagLength INT,
			@iNewTagLength INT
	
	SELECT @iCount = COUNT(*)
	FROM tbProfilePromoManagement WITH (NOLOCK)
	WHERE DatabaseId = @NewDatabaseID AND PromoManagementName = @sPromoManagementName
	
	SET @iOldTagLength = dbo.isTagLength4(@OldDatabaseID)
	SET @iNewTagLength = dbo.isTagLength4(@NewDatabaseID)
	
	IF @iCount = 0
	BEGIN
		IF @iOldTagLength = @iNewTagLength
		BEGIN
			INSERT INTO tbProfilePromoManagement(DatabaseId,PromoManagementName, PromoManagementTag,PromoManagementLength, PromoManagementTagLength, PromoManagementTagValue)
			SELECT @NewDatabaseID,PromoManagementName, PromoManagementTag,PromoManagementLength, PromoManagementTagLength, PromoManagementTagValue
			FROM tbProfilePromoManagement WITH (NOLOCK)
			WHERE DatabaseId = @OldDatabaseID AND PromoManagementName = @sPromoManagementName
		END
		ELSE
		BEGIN
			IF @iOldTagLength > @iNewTagLength
			BEGIN
				INSERT INTO tbProfilePromoManagement(DatabaseId,PromoManagementName, PromoManagementTag,PromoManagementLength, PromoManagementTagLength, PromoManagementTagValue)
				SELECT @NewDatabaseID,PromoManagementName,SUBSTRING(PromoManagementTag,1,2)+ SUBSTRING(PromoManagementTag,4,2),PromoManagementLength,PromoManagementTagLength,PromoManagementTagValue
				FROM tbProfilePromoManagement WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND PromoManagementName = @sPromoManagementName
			END
			ELSE
			BEGIN
				INSERT INTO tbProfilePromoManagement(DatabaseId,PromoManagementName, PromoManagementTag,PromoManagementLength, PromoManagementTagLength, PromoManagementTagValue)
				SELECT @NewDatabaseID,PromoManagementName,SUBSTRING(PromoManagementTag,1,2) +'0'+SUBSTRING(PromoManagementTag,3,2),PromoManagementLength,PromoManagementTagLength,PromoManagementTagValue
				FROM tbProfilePromoManagement WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND PromoManagementName = @sPromoManagementName
			END
		END
	END
	ELSE
		SELECT 'PromoManagementName  has already used. Please choose other PromoManagementName Index.'
	
	
END