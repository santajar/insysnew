﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 26, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Import relation table into profile content
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationImport] 
	@sTerminalId VARCHAR(8),
	@sContent VARCHAR(MAX)
AS

SELECT * INTO #profile FROM dbo.fn_tbProfileTLV(@sTerminalId,@sContent)

----Relation
DECLARE @iCountRow INT
SELECT @iCountRow=COUNT(*) FROM #profile
	WHERE Tag LIKE 'AD%'
IF @iCountRow > 0
BEGIN
	INSERT INTO tbProfileRelation(
		TerminalId,
		RelationTag,
		RelationLengthOfTagLength,
		RelationTagLength,
		RelationTagValue)
	SELECT TerminalId,
		Tag,
		LEN(CAST(TagLength AS INT)), 
		TagLength,
		TagValue 
	FROM #profile
	WHERE Tag LIKE 'AD%'
	ORDER BY rowid
END
--IF OBJECT_ID('tempdb..#profile') IS NOT NULL
	DROP TABLE #profile
