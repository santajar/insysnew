﻿CREATE PROCEDURE [dbo].[spInitLogConnNewInitConsole]
@sTerminalID VARCHAR(8),
@iStartInit INT,
@iPercentage INT
AS 
	DECLARE @iCountTID INT
	SELECT @iCountTID = COUNT(*) FROM tbInitLogConn WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
	
	IF (@iCountTID = 0)
		EXEC spInitLogConnInsertProcess @sTerminalID	
	ELSE 
	BEGIN
		--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
		--BEGIN TRANSACTION
		--IF (@iStartInit = 1)
		--	UPDATE tbInitLogConnDetail SET Percentage = 0, [Time] = GETDATE() WHERE TerminalId = @sTerminalID
		EXEC spInitLogConnUpdateConsole @sTerminalID, @iPercentage 
		--COMMIT TRANSACTION
	END