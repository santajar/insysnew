﻿
CREATE PROCEDURE [dbo].[spUserGroupItemEnableBrowse]
	@sGroupID VARCHAR(30) = ''
AS
	SELECT GroupID, Tag, ItemName
	FROM tbUserGroup WITH (NOLOCK)
	WHERE GroupID = @sGroupID

