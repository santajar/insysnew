USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileIssuerDeleteByAcq]    Script Date: 11/2/2017 2:42:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spProfileIssuerDeleteByAcq]
	@sTerminalID VARCHAR(8),
	@sMessage VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;


delete from tbProfileIssuer
where TerminalID=@sTerminalID and IssuerName in (select ValueIssuer from tbProfileRelation where TerminalID=@sTerminalID and ValueAcquirer=@sMessage)

 --   DELETE FROM TbProfileIssuer
	--WHERE
	--	TerminalID = @sTerminalID AND
	--	IssuerName IN
	--	(
	--		SELECT String
	--		FROM Fn_TbAnyString(@sMessage)
	--	)
END

GO


