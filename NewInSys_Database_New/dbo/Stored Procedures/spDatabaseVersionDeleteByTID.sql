﻿
-- ===========================================================
-- Description:	Delete Terminal's Profile in given DatabaseID 
-- ===========================================================

CREATE PROCEDURE [dbo].[spDatabaseVersionDeleteByTID]
	@sDatabaseID SMALLINT
AS
	IF OBJECT_ID('tempdb..#TempTID') IS NOT NULL
		DROP TABLE #TempTID
	
	CREATE TABLE #TempTID (
		ID INT IDENTITY(1,1),
		TID VARCHAR(8)
	)

	--Get all Terminal in current DatabaseID
	INSERT INTO #TempTID (TID)
	SELECT TerminalID 
	FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseID

	DECLARE @iTotalRow INT
	DECLARE @iRowCount INT
	DECLARE @sCurrTID VARCHAR(8)

	SELECT @iTotalRow = COUNT(*) FROM #TempTID

	SET @iRowCount = 1
	WHILE (@iRowCount <= @iTotalRow )
	BEGIN
		SELECT @sCurrTID = TID
		FROM #TempTID
		WHERE ID = @iRowCount

		EXEC spProfileTerminalDeleteProcess @sCurrTID

		SET @iRowCount = @iRowCount +1
	END

	DROP TABLE #TempTID

