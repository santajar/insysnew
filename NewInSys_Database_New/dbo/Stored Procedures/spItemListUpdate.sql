﻿



-- ==================================
-- Description:	Update selected Tag
-- ==================================

CREATE PROCEDURE [dbo].[spItemListUpdate]
	@sDatabaseID SMALLINT,
	@sFormID SMALLINT,
	@iItemSequence SMALLINT,
	@sOldItemSequence SMALLINT = NULL,
	@sTag VARCHAR(5),
	@sTagName VARCHAR(24),
	@sOldObjectID SMALLINT = NULL,
	@sObjectID SMALLINT,
	@sDefaultValue TEXT = NULL,
	@iLengthofTagValueLength INT = 2,
	@vAllowNull BIT,
	@vUpperCase BIT,
	@vType VARCHAR(5),
	@vMinLength INT,
	@vMaxLength INT,
	@vMinValue INT,
	@vMaxValue INT,
	@vMasking BIT,
	@sValidationMsg VARCHAR(50)
AS

	--CREATE TABLE #OldTable
	--(
	--	ColumnName VARCHAR(50),
	--	ColumnValue VARCHAR(500)
	--)
	--INSERT INTO #OldTable (ColumnName, ColumnValue)
	--EXEC sp_tbItemList @sDatabaseID, @sTag

	UPDATE tbItemList SET ItemSequence = @iItemSequence,
						  ItemName = @sTagName,
						  ObjectID = @sObjectID,
						  DefaultValue = @sDefaultValue,
						  vAllowNull = @vAllowNull,
						  vUpperCase = @vUpperCase,
						  vType = @vType,
						  vMinLength = @vMinLength,
						  vMaxLength = @vMaxLength,
						  vMinValue = @vMinValue,
						  vMaxValue = @vMaxValue,
						  vMasking = @vMasking,
						  ValidationMsg = @sValidationMsg
	WHERE DatabaseID = @sDatabaseID AND 
		  FormID = @sFormID AND 
		  Tag = @sTag

	IF (@sOldObjectID <> @sObjectID)
		IF (@sOldObjectID = 4)
			EXEC spItemComboBoxValueDeleteBySequence @sDatabaseID, @sFormID, @sOldItemSequence

	IF (@sObjectID = 4)
		UPDATE tbItemComboBoxValue
		SET ItemSequence = @iItemSequence
		WHERE DatabaseID = @sDatabaseID AND 
			  FormID = @sFormID AND 
			  ItemSequence = @sOldItemSequence

	--CREATE TABLE #NewTable
	--(
	--	ColumnName VARCHAR(50),
	--	ColumnValue VARCHAR(500)
	--)
	--INSERT INTO #NewTable (ColumnName, ColumnValue)
	--EXEC sp_tbItemList @sDatabaseID, @sTag

	SELECT a.ColumnName, a.ColumnValue OldColumnValue, b.ColumnValue NewColumnValue
	FROM dbo.fn_tbItemList (@sDatabaseID, @sTag) a FULL OUTER JOIN dbo.fn_tbItemList (@sDatabaseID, @sTag) b ON a.ColumnName = b.ColumnName
	WHERE ISNULL(a.ColumnValue,'') <> ISNULL(b.ColumnValue,'')

--	IF (OBJECT_ID('tempdb..##OldTable') IS NOT NULL)
		--DROP TABLE #OldTable
--	IF (OBJECT_ID('tempdb..##NewTable') IS NOT NULL)
		--DROP TABLE #NewTable