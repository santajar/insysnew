﻿
CREATE PROCEDURE [dbo].[spProfileGPRSGenerateLog]
	@iDatabaseID INT,
	@sGPRSName VARCHAR(50),
	@sContent VARCHAR(MAX)=NULL,
	@isEdit BIT
AS
	


	SELECT  ItemName, GPRSTagValue, GPRSTag
	 INTO #GPRSTempLog
	FROM (SELECT GPRSName, DatabaseID, GPRSTag, GPRSTagValue
		  FROM tbProfileGPRS WITH (NOLOCK) WHERE databaseid = @iDatabaseID) A
		FULL OUTER JOIN
		(SELECT ItemName, Tag
		 FROM tbItemList WITH (NOLOCK)
		 WHERE DatabaseID = @iDatabaseID AND Tag LIKE 'TL%') B
		ON A.GPRSTag = B.Tag
	WHERE GPRSName = @sGPRSName

	IF @isEdit = 0
		SELECT ItemName, GPRSTagValue
		FROM #GPRSTempLog
	ELSE
		IF (dbo.isTagLength4(@iDatabaseID)=5)
			SELECT ItemName, GPRSTagValue OldValue, TagValue NewValue
			FROM #GPRSTempLog OLD FULL OUTER JOIN 
			  ( SELECT TagValue, Tag
				FROM dbo.fn_tbProfileTLV5(@sGPRSName,@sContent)
			  ) NEW
				ON OLD.GPRSTag = New.Tag
			WHERE GPRSTagValue != TagValue
		ELSE
			SELECT ItemName, GPRSTagValue OldValue, TagValue NewValue
			FROM #GPRSTempLog OLD FULL OUTER JOIN 
			  ( SELECT TagValue, Tag
				FROM dbo.fn_tbProfileTLV(@sGPRSName,@sContent)
			  ) NEW
				ON OLD.GPRSTag = New.Tag
			WHERE GPRSTagValue != TagValue

	
	--IF OBJECT_ID('tempdb..##GPRSTempLog') IS NOT NULL
		DROP TABLE #GPRSTempLog

