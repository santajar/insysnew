﻿


CREATE PROCEDURE [dbo].[spProfileRequestPaperReceiptUpdate]
	@iDatabaseID INT,
	@sRequestPaperReceiptName VARCHAR(15),
	@sContent VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileRequestPaperReceiptDelete @iDatabaseID, @sRequestPaperReceiptName, 0
	EXEC spProfileRequestPaperReceiptInsert @iDatabaseID, @sRequestPaperReceiptName, @sContent
END