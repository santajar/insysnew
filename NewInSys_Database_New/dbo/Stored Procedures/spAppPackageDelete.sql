﻿CREATE PROCEDURE [dbo].[spAppPackageDelete]
	@sAppPackageName VARCHAR(150),
	@iAppPackId int
AS
	SET NOCOUNT ON
DELETE FROM tbAppPackageEDCList
WHERE AppPackageName=@sAppPackageName and AppPackId = @iAppPackId

EXEC AppPackageEDCListTempDelete @sAppPackageName, @iAppPackId