﻿-- =============================================
-- Author:		<Author,,TEDIE SCORFIA>
-- Create date: <Create Date,,24 APRIL 2014>
-- Description:	<Description,,INSERT Process INTO tbListTIDRemoteDownload>
-- =============================================
CREATE PROCEDURE [dbo].[spRDInsertProcessListTid]
  @sTerminalID VARCHAR(8),@sCategory VARCHAR(10), @sCategoryName VARCHAR(150), @dtScheduleTime DATETIME,@sSoftwareName VARCHAR(20), @BuildNumber INT
AS
BEGIN
	DECLARE @iScheduleID INT, @iAppPackId INT, @iID int
		EXEC spScheduleSoftwareInsert NULL,'DAILY','EVERY DAY',@dtScheduleTime,'0',@iScheduleID OUTPUT
		print 'ScheduleID :' + CAST(@iScheduleID AS VARCHAR(10))
	

		SELECT @iAppPackId = AppPackId
		FROM tbAppPackageEDCList WITH (NOLOCK)
		WHERE AppPackageName = @sSoftwareName AND BuildNumber = @BuildNumber
		print 'AppPackId :' + CAST(@iAppPackId AS VARCHAR(10))
		

		IF @sCategory = 'Region'
		BEGIN
			SELECT @iID = IdRegionRD 
			FROM tbRegionRemoteDownload WITH (NOLOCK)
			WHERE RegionRDName = @sCategoryName
		END
		ELSE IF @sCategory = 'Group'
		BEGIN
			SELECT @iID = IdGroupRD 
			FROM tbGroupRemoteDownload WITH (NOLOCK)
			WHERE GroupRDName = @sCategoryName
		END
		ELSE
		BEGIN
			SELECT @iID = IdCityRD 
			FROM tbCityRemoteDownload WITH (NOLOCK)
			WHERE CityRDName = @sCategoryName
		END 
		print 'ID Category : '+ CAST(@iID AS VARCHAR(10))
		EXEC spRDInsertListTid @sTerminalID,@sCategory,@iID,@iScheduleID,@iAppPackId
END