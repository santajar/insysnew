﻿CREATE PROCEDURE [dbo].[spImagePackageAddContent]
	@sFileName VARCHAR(50),
	@sType VARCHAR(10),
	@iSuccess BIT OUTPUT,
	@iImageId INT OUTPUT
AS
DECLARE @iCountDelete INT,
		@iCountAdd INT
		

SELECT @iCountDelete = COUNT(*)
FROM tbProfileImage WITH (NOLOCK)
WHERE [FileName] = @sFileName
	AND LogoType = @sType

IF @iCountDelete>0
	EXEC spImagePackageDeleteContent @sFileName,@sType
--DELETE FROM tbProfileImage
--WHERE ImageName =  @sImageName

SELECT @iCountAdd = COUNT([FileName]) 
FROM tbProfileImage WITH (NOLOCK)
WHERE [FileName] = @sFileName
	AND LogoType = @sType

IF @iCountAdd = 0
BEGIN

	DECLARE @table table (id INT)
	INSERT INTO tbProfileImage(LogoType,[FileName],UploadTime)
	OUTPUT inserted.ImageID into @table
	VALUES(	@sType,@sFileName,GETDATE())

	SELECT @iImageId=id FROM @table
	SET @iSuccess = 1

	IF (@sType = 'Print')
	BEGIN
		INSERT INTO tbItemComboBoxValue(DatabaseID,FormID,ItemSequence,DisplayValue,RealValue)
		SELECT DatabaseID,FormID,ItemSequence,@sFileName,@sFileName 
		FROM tbItemList WITH (NOLOCK)
		WHERE FormID = 1
		AND itemname in ('Logo Print')
	END
	--ELSE
	IF (@sType = 'Idle')
	BEGIN
		INSERT INTO tbItemComboBoxValue(DatabaseID,FormID,ItemSequence,DisplayValue,RealValue)
		SELECT DatabaseID,FormID,ItemSequence,@sFileName,@sFileName 
		FROM tbItemList WITH (NOLOCK)
		WHERE FormID = 1
		AND itemname in ('Logo Idle')
	END

	IF (@sType = 'Merchant')
	BEGIN
		INSERT INTO tbItemComboBoxValue(DatabaseID,FormID,ItemSequence,DisplayValue,RealValue)
		SELECT DatabaseID,FormID,ItemSequence,@sFileName,@sFileName 
		FROM tbItemList WITH (NOLOCK)
		WHERE FormID = 1
		AND itemname in ('Logo Merchant')
	END
	IF (@sType = 'Receipt')
	BEGIN
		INSERT INTO tbItemComboBoxValue(DatabaseID,FormID,ItemSequence,DisplayValue,RealValue)
		SELECT DatabaseID,FormID,ItemSequence,@sFileName,@sFileName 
		FROM tbItemList WITH (NOLOCK)
		WHERE FormID = 1
		AND itemname in ('Logo Receipt')
	END

END
ELSE
	SET @iSuccess = 0