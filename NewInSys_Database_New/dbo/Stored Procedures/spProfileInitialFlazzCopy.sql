﻿
-- =============================================
-- Author:		Tobias Setyo
-- Create date: Nov 14, 2016
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. 
-- =============================================
CREATE PROCEDURE [dbo].[spProfileInitialFlazzCopy]
	@iDatabaseID VARCHAR(3),
	@sOldInitialFlazzName VARCHAR(50),
	@sNewInitialFlazzName VARCHAR(50)

AS
Declare
	@Content VARCHAR(MAX)=NULL,
	@iCount INT,
	@iIndex INT

SELECT @iCount= COUNT(*)
FROM tbProfileInitialFlazz WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID
	AND InitialFlazzName = @sNewInitialFlazzName


IF @iCount = 0
BEGIN
	INSERT INTO tbProfileInitialFlazz(InitialFlazzName, DatabaseID, InitialFlazzTag ,InitialFalzzLengthOfTagLength, InitialFlazzTagLength, InitialFlazzTagValue)
	SELECT @sNewInitialFlazzName, DatabaseID, InitialFlazzTag ,InitialFalzzLengthOfTagLength, InitialFlazzTagLength, InitialFlazzTagValue
	FROM tbProfileInitialFlazz WITH (NOLOCK)
	WHERE InitialFlazzName = @sOldInitialFlazzName AND DatabaseId=@iDatabaseID
	
	SET @iIndex=CHARINDEX('-',@sOldInitialFlazzName)
	 
	IF (@iIndex = 0)
	BEGIN
		 UPDATE tbProfileInitialFlazz
		 SET InitialFlazzTagValue = @sNewInitialFlazzName, InitialFlazzTagLength = LEN(@sNewInitialFlazzName)
		 WHERE InitialFlazzTag IN ('PM001','PM01') AND InitialFlazzName = @sNewInitialFlazzName
	END
	ELSE
	BEGIN
		 UPDATE tbProfileInitialFlazz
		 SET InitialFlazzTagValue = SUBSTRING(@sNewInitialFlazzName,0,@iIndex-1), InitialFlazzTagLength = LEN(SUBSTRING(@sNewInitialFlazzName,0,@iIndex-1))
		 WHERE InitialFlazzTag IN ('PM001','PM01') AND InitialFlazzName = @sNewInitialFlazzName
	END
	 
END
ELSE
	SELECT 'InitialFlazz management Index Already Exist.'