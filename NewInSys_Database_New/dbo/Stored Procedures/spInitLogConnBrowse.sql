﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Sept 30, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Viewing the profile initialization log
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnBrowse]
	@sCond VARCHAR(3000) = ''
AS
	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt =
		'SELECT TOP 1000
				[Time], 
				TerminalID, 
				[Description], 
				Percentage
		FROM tbInitLogConn WITH (NOLOCK) ' + @sCond + ' ORDER BY Time DESC '
	EXEC (@sStmt)