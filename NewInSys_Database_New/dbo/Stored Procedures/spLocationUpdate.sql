﻿
-- ==================================
-- Description: edit Location's data 
-- ==================================

CREATE PROCEDURE [dbo].[spLocationUpdate]
	@sTerminalID VARCHAR(8),
	@sLocationDesc VARCHAR(50)
AS
DECLARE @iLocationID INT
SET @iLocationID = 0

SELECT @iLocationID = LocationID FROM tbLocation WITH (NOLOCK)
WHERE LocationDesc = @sLocationDesc

IF @iLocationID <> 0
BEGIN
	UPDATE tbProfileTerminalList
	SET LocationID=@iLocationID
	WHERE TerminalID=@sTerminalID
END

