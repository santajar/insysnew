IF OBJECT_ID ('dbo.spEdcMonitorCSIInsertUpdate') IS NOT NULL
	DROP PROCEDURE dbo.spEdcMonitorCSIInsertUpdate
GO

CREATE PROCEDURE [dbo].[spEdcMonitorCSIInsertUpdate]
  ( @CSI VARCHAR(50), 
@TerminalID VARCHAR(8), 
@MerchantID VARCHAR(15),
@Result INT OUTPUT)
 AS 
 
BEGIN 

--DECLARE @CSI VARCHAR(50), 
--@TerminalID VARCHAR(8), 
--@MerchantID VARCHAR(15),
--@Result INT=0

--SET @CSI='TMTATANK' 
--SET @TerminalID='30000112'
--SET @MerchantID='0000815551923'
--SET @Result=0

SELECT * FROM tbEdcMonitorCSI WHERE  TerminalID = @CSI
IF @@ROWCOUNT <> 0 
 Begin 
	Set @Result=1;
	PRINT '1'
 End
ELSE
Begin
   INSERT INTO tbEdcMonitorCSI (TerminalID, TID, MID) VALUES (@CSI, @TerminalID, @MerchantID)  
   Set @Result=0;
   PRINT '0'
END

RETURN @Result;
END


GO

