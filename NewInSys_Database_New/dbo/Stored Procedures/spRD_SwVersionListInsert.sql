﻿CREATE PROCEDURE [dbo].[spRD_SwVersionListInsert]
	@iSV_ID INT,
	@sSVList_name VARCHAR(50),
	@binSVList_Content VARBINARY(MAX)
AS
INSERT INTO tbRD_SoftwareVersionList(SV_ID, SVList_Name, SVList_Content)
VALUES (@iSV_ID, @sSVList_name, @binSVList_Content)
