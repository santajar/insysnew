﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Agt 19, 2013
-- Modify date:
--				1. 
-- Description:	Browse into tbSoftwareProgress
-- =============================================

CREATE procedure [dbo].[spSoftwareProgressBrowse]
@sCondition VARCHAR(MAX) =  null
AS
DECLARE @sQuery VARCHAR(MAX) = 'SELECT
	EdcGroup [Group],
	EdcRegion [Region],
	EdcCity [City],
	TerminalID,
	Software,
	SoftwareDownload,
	BuildNumber,
	SerialNumber,
	StartTime,
	EndTime,
	Percentage,
	FinishInstallTime,
	InstallStatus
	FROM tbSoftwareProgress WITH (NOLOCK) '
IF @sCondition IS NOT NULL
EXEC (@sQuery + @sCondition+ ' order by id desc')
ELSE
EXEC (@sQuery + ' order by id desc')