﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. process the received message and then return the response the ISO
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseProcess] @sMessage VARCHAR(MAX),
	@iCheckInit INT
AS
SET NOCOUNT ON
SET DEADLOCK_PRIORITY HIGH
-- declare variables
-- require variables
DECLARE @sSqlStmt VARCHAR(MAX),
	@sNII VARCHAR(4),
	@sAddress VARCHAR(4),
	@sMTI VARCHAR(4),
	@sBitMap VARCHAR(16),
	@sBinaryBitMap VARCHAR(64),
	@sEdcMessage VARCHAR(MAX),
	@sSessionTime VARCHAR(10),
	@sTableTemp VARCHAR(100),
	@sTerminalId VARCHAR(8)
	
-- initialization
SET @sSessionTime=
	REPLACE(REPLACE(CONVERT(VARCHAR,GETDATE(),114),':',''),' ','')
SET @sTableTemp= '##tempBitMap' + @sSessionTime
SET @sNII=SUBSTRING(@sMessage,1,4)
SET @sAddress=SUBSTRING(@sMessage,5,4)
SET @sMTI=SUBSTRING(@sMessage,9,4)
SET @sBitMap=SUBSTRING(@sMessage,13,16)
SET @sEdcMessage=SUBSTRING(@sMessage,29,LEN(@sMessage)-28)

print 'Start ISO Unpack : ' + @sMessage

-- is MTI init or not
IF dbo.IsInitMTI(@sMTI) = 1
BEGIN
	print 'unpack the messages'
	SET @sBinaryBitMap = dbo.Hex2Bin(@sBitMap)
	EXEC spOtherParseISOUnpack @sSessionTime,@sBinaryBitMap,@sEdcMessage
END

print 'Start ISO Pack'

EXEC spOtherParseISOPack 
	@iCheckInit,
	@sNII,
	@sAddress,
	@sMTI,
	@sSessionTime,
	@sTerminalId OUTPUT

-- debug into table
INSERT INTO tbInitTrace
           ([ErrorCode]
           ,[TerminalId]
           ,[ISO]
		   ,[Tag]
           ,[TerminalContent])
SELECT ERRORCODE,TERMINALID,ISO,TAG,[CONTENT] FROM tbInitResult 
WITH (NOLOCK)
WHERE TERMINALID=@sTerminalId


DECLARE @iERRORCODE INT,
		@sTID VARCHAR(8),
		@sISO VARCHAR(MAX),
		@sTag VARCHAR(5),
		@sContent VARCHAR(MAX)

SELECT @iERRORCODE = @iERRORCODE,
		@sTID = TERMINALID,
		@sISO = ISO,
		@sTag = TAG,
		@sContent = [CONTENT]
FROM tbInitResult  
WITH (NOLOCK)
WHERE TERMINALID=@sTerminalId

EXEC spInitTraceInsert @iERRORCODE, @sTID, @sISO, @sTag, @sContent

print 'Success debug'

SELECT ERRORCODE,TERMINALID,ISO,TAG,[CONTENT] FROM tbInitResult  
WITH (NOLOCK)
WHERE TERMINALID=@sTID

SET @sSqlStmt='
		DROP TABLE ##tempBitMap'+ @sSessionTime 
EXEC (@sSqlStmt)