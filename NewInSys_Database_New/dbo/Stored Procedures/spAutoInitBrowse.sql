﻿
-- =========================================================
-- Description: View data from tbAutoInitLog for current TID
-- =========================================================

CREATE PROCEDURE [dbo].[spAutoInitBrowse]
	@sTerminalID CHAR(8)
AS
	SET NOCOUNT ON
	SELECT LastInitTime, 
			PABX,
			SoftwareVer, 
			OSVer, 
			KernelVer, 
			EDCSN,
			ReaderSN, 
			PSAMSN,
			MCSN,
			MemUsage
	FROM tbAutoInitLog WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID