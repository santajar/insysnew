﻿CREATE PROCEDURE [dbo].[spRD_AuditDownloadInsert]
	@sTerminalID VARCHAR(25),
	@sSerialNumber VARCHAR(50),
	@sInstallDate VARCHAR(50)='',
	@sFilename VARCHAR(50),
	@sSV_Name VARCHAR(50)='',
	@sFreeRAM VARCHAR(50)='',
	@sTotalRAM VARCHAR(50)=''
AS
INSERT INTO tbRD_AuditDownload([TerminalID]
	,[SerialNumber]
	,[InstallDate]
	,[Filename]
	,[SV_NAME]
	,[FreeRAM]
	,[TotalRAM]
	)
VALUES (@sTerminalID
	,@sSerialNumber
	,@sInstallDate
	,@sFilename
	,@sSV_Name
	,@sFreeRAM
	,@sTotalRAM)
