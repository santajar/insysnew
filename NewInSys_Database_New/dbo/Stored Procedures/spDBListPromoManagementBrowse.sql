﻿


-- =============================================  
-- Author:  Ibnu  Saefullah  
-- Create date: Feb 18, 2016  
-- Description: view DB list with PromoManagement
-- =============================================  
CREATE PROCEDURE [dbo].[spDBListPromoManagementBrowse]  
AS
SELECT DatabaseID, DatabaseName FROM tbProfileTerminalDB WITH (NOLOCK)
	WHERE PromoManagement = 1
	ORDER BY DatabaseName