﻿
-- ===========================================
-- Description:	Delete tag for given Database
-- ===========================================

CREATE PROCEDURE [dbo].[spItemListDeleteByDbID]
	@sDatabaseID SMALLINT
AS
	DELETE FROM tbItemList
	WHERE DatabaseID = @sDatabaseID 

	EXEC spItemComboBoxValueDeleteByDbID @sDatabaseID

