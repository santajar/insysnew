﻿
-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <30 okt 2015>
-- Description:	<Update Status Software List Download>
-- =============================================
CREATE PROCEDURE [dbo].[spTempSoftwareListUpdateDelete]
	@sTerminalID VARCHAR(8),
	@sSoftwareName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @iCountStatus INT

	UPDATE tbTempSoftwareListDownload
	SET SoftwareStatus = 'SUCCESS'
	WHERE TerminalID = @sTerminalID
		AND SoftwareName = @sSoftwareName

	SELECT @iCountStatus = COUNT(SoftwareStatus)
	FROM tbTempSoftwareListDownload WITH(NOLOCK)
	WHERE TerminalID = @sTerminalID
		AND SoftwareStatus = 'OnProgress'

	IF (@iCountStatus = 0)
	BEGIN
		DELETE FROM tbTempSoftwareListDownload
		WHERE TerminalID = @sTerminalID

		UPDATE tbProfileTerminalList
		SET InitMigrasi = '0',RemoteDownload = '0'
		WHERE TerminalID = @sTerminalID
	END


END