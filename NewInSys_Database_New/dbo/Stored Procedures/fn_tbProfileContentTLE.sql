USE [NewInSys_BCA-DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_tbProfileContentTLE]    Script Date: 11/2/2017 5:07:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_tbProfileContentTLE](  
 @sTerminalID VARCHAR(8),  
 @sConditions VARCHAR(MAX)  
 )  
RETURNS @tempTerminal TABLE(  
  TerminalId VARCHAR(8),  
  [Name] VARCHAR(15),  
  Tag VARCHAR(5),  
  ItemName VARCHAR(50),  
  TagLength VARCHAR(3),  
  TagValue VARCHAR(MAX)  
 )  
BEGIN  
-- DECLARE @sTag  varchar(5)
--DECLARE  @sTleName varchar(20)
--(

INSERT INTO @tempTerminal (TerminalId,  
  [Name],  
  Tag,  
  ItemName,  
  TagLength,  
  TagValue )  
SELECT b.TerminalId,   
 a.TLEName [Name],   
 a.TLETag Tag,  
 '' ItemName,   
 a.TLETagLength TagLength,  
 a.TLETagValue TagValue  
FROM tbProfileTLE a WITH (NOLOCK) JOIN tbProfileTerminalList b  WITH (NOLOCK)
ON a.DatabaseId=b.DatabaseId  
WHERE b.TerminalId=@sTerminalID AND  
 a.TLEName IN (  
  SELECT TLEName   
  FROM tbProfileTLE WITH (NOLOCK)
  WHERE (TLETag = 'TL02' OR TLETag = 'TL002') AND TLETagValue IN (  
  SELECT DISTINCT AA034   
  FROM tbProfileAcquirer WITH (NOLOCK)  
  WHERE TerminalID = @sTerminalID 
	--AND AcquirerTag IN 
	--	(SELECT Tag FROM tbItemList WITH (NOLOCK)
	--	WHERE ItemName ='TLE-EFTSec' 
	--		AND FormID = 2
	--		AND DatabaseID = A.DatabaseID)
	)AND DatabaseID = A.DatabaseID
)  
ORDER BY [Name], Tag  
RETURN  
END
GO


