﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <26 January 2016>
-- Description:	<Restore Profile data Acquirer>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerHistoryRestore]
	@sTerminalID VARCHAR(8),
	@sDate VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue
	INTO #TempAcquirer
	FROM tbProfileAcquirer WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID AND 
			AcquirerName IN (SELECT AcquirerName FROM tbProfileTerminalHistory WHERE TerminalID = @sTerminalID)

	DELETE FROM tbProfileAcquirer
	WHERE TerminalID = @sTerminalID AND AcquirerName IN (SELECT AcquirerName FROM tbProfileTerminalHistory WHERE TerminalID = @sTerminalID AND LastUpdate = @sDate)

	INSERT INTO tbProfileAcquirer (TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue)
	SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue
	FROM tbProfileAcquirerHistory
	WHERE TerminalID = @sTerminalID AND LastUpdate = @sDate

	INSERT INTO tbProfileAcquirerHistory(TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue,LastUpdate)
	SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue,CONVERT(VARCHAR(20),GETDATE(),113)
	FROM #TempAcquirer
	WHERE TerminalID = @sTerminalID

	EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
	EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

	DROP TABLE #TempAcquirer

    
END