﻿CREATE PROCEDURE [dbo].[spProfileCurrencyBrowse]
	@sCondition VARCHAR(200) = NULL
AS
	DECLARE @sStmt VARCHAR (200)
	SET @sStmt = '	SELECT DatabaseId, 
							CurrencyName, 
							CurrencyTag as Tag, 
							CurrencyLengthOfTagLength,
							CurrencyTagLength, 
							CurrencyTagValue as Value
					FROM tbProfileCurrency WITH (NOLOCK) ' 
	
	EXEC (@sStmt + @sCondition)

