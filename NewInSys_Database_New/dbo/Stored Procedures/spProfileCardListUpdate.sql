﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 27, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. updating Card range list
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCardListUpdate]
	@sDatabaseID VARCHAR(3),
	@sCardName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	
	--CREATE TABLE #TempTable
	--(
	--	ItemName VARCHAR(MAX),
	--	OldValue VARCHAR(MAX),
	--	NewValue VARCHAR(MAX)
	--)
	
	DECLARE @iDatabaseID INT
	SET @iDatabaseID = CAST(@sDatabaseId AS INT)
	DECLARE @iCardID INT
	
	--INSERT INTO #TempTable (ItemName,OldValue,NewValue)
	--EXEC sp_tbHistoryEditCard @iDatabaseID, @sCardName, @sContent, @iCardID

	
	SET @iCardID = dbo.iCardId(@iDatabaseID, @sCardName)
	PRINT 'MASUK TABLE fn_tbHistoryEditCard'
	PRINT @sContent
	PRINT @iCardID
	-- return table digunakan untuk input ke log.
	SELECT * 
	FROM dbo.fn_tbHistoryEditCard (@iDatabaseID, @sCardName, @sContent, @iCardID)
	PRINT 'MASUK spProfileCardListDelete'
	EXEC spProfileCardListDelete @iDatabaseID, @sCardName, 0
	PRINT 'MASUK spProfileCardListInsert'
	EXEC spProfileCardListInsert @sDatabaseID, @sCardName, @sContent
END
