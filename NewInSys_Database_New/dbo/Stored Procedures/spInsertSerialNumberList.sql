﻿-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: 21 maret 2017
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
-- =============================================
create PROCEDURE [dbo].[spInsertSerialNumberList]
@sTerminalID VARCHAR(8),
@sSerialNumber VARCHAR(30),
@sFlag VARCHAR(6)

AS
DECLARE @sDbId int

IF @sFlag = 'ADD'
BEGIN
	UPDATE TbProfileSerialNumberList SET SerialNumber = @sSerialNumber
	WHERE TerminalID = @sTerminalID

	IF @@ROWCOUNT = 0
	BEGIN
	Select @sDbId =  DatabaseID from tbProfileTerminalList WHere TerminalID = @sTerminalID
	INSERT INTO  TbProfileSerialNumberList (DataBaseID, TerminalID, SerialNumber)
	VALUES (@sDbId, @sTerminalID, @sSerialNumber )
	END
END
IF @sFlag = 'UPDATE'
BEGIN
UPDATE TbProfileSerialNumberList SET SerialNumber = @sSerialNumber
WHERE TerminalID = @sTerminalID
END