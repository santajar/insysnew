﻿

CREATE PROCEDURE [dbo].[spProfileTerminalListCompressUpdate]
	@sCondition VARCHAR(8000)=''
AS
	UPDATE tbProfileTerminalList
	SET InitCompress=0

	IF EXISTS (  SELECT * FROM SYS.COLUMNS
				WHERE Name = N'InitCompress' AND
				Object_ID = Object_ID (N'tbProfileTerminalList')
			  ) 
		AND @sCondition <> ''
	BEGIN
		DECLARE @sQuery VARCHAR(MAX)
		SET @sQuery = 'UPDATE tbProfileTerminalList
		SET InitCompress=1 ' + @sCondition

		EXEC (@sQuery)
	END




