﻿CREATE PROCEDURE [dbo].[spDownloadAppStart] 
	@sTerminalID VARCHAR(8),
	@sContent VARCHAR(MAX)=NULL
AS
DELETE FROM tbDownloadApp 
WHERE TerminalID = @sTerminalID
print '[spDownloadAppStart]'

INSERT INTO tbDownloadApp (TerminalID, [Content], Flag)
SELECT @sTerminalID, AppPackageContent, 0
FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackId IN (SELECT AppPackID FROM tbProfileTerminalList WITH (NOLOCK) WHERE TerminalID=@sTerminalID)
ORDER BY Id