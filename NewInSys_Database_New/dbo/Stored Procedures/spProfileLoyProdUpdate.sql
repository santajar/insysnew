﻿


CREATE PROCEDURE [dbo].[spProfileLoyProdUpdate]
	@iDatabaseID INT,
	@sLoyProdName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileLoyProdDelete @iDatabaseID, @sLoyProdName, 0
	EXEC spProfileLoyProdInsert @iDatabaseID, @sLoyProdName, @sContent
END




