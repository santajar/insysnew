USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileRelationCopy]    Script Date: 11/2/2017 4:49:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spProfileRelationCopy]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
BEGIN
	INSERT INTO tbProfileRelation(
		TerminalId,
		TagCard,
		ValueCard,
		TagIssuer,
		ValueIssuer,
		TagAcquirer,
		ValueAcquirer
		)
	SELECT @sNewTID,
		TagCard,
		ValueCard,
		TagIssuer,
		ValueIssuer,
		TagAcquirer,
		 ValueAcquirer
	FROM tbProfileRelation (NOLOCK)
	WHERE TerminalId=@sOldTID
	ORDER BY ID
END



GO


