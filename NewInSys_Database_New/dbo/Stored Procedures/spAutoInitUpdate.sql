﻿
-- =================================================================
-- Description:	Update data in tbAutoInitLog
--				If TID not found (in tbAutoInitLog), insert new row
--				else Update selected TID with new data
-- =================================================================

CREATE PROCEDURE [dbo].[spAutoInitUpdate]
	@sSessionTime VARCHAR(MAX),
	@sLastInitTime VARCHAR(50),
	@sPABX VARCHAR(50),
	@sSoftwareVers VARCHAR(50),
	@sOSVers VARCHAR(50),
	@sKernelEmvVers VARCHAR(50),
	@sEdcSN VARCHAR(50),
	@sReaderSN VARCHAR(50),
	@sPSAMSN VARCHAR(50),
	@sMCSN VARCHAR(50),
	@sMemUsg VARCHAR(50),
	@sICCID VARCHAR(50)=NULL
AS
	SET NOCOUNT ON
	DECLARE @sSqlStmt NVARCHAR(MAX)
	DECLARE @sTID VARCHAR(50)

	SET @sSqlStmt = N'SELECT @sTID=BitValue FROM #tempBitMap' + @sSessionTime + 
		' WHERE BitId=41'

	EXEC sp_executesql @sSqlStmt,N'@sTID VARCHAR(MAX) OUTPUT',@sTID=@sTID OUTPUT
	EXEC spOtherParseHexStringToString @sTID, @sTID OUTPUT

	-- Look up for current TID in table
	DECLARE @iCount INT
	SELECT @iCount = COUNT(*)
	FROM tbAutoInitLog
	WHERE TerminalID = @sTID

	IF (@iCount = 0)
	BEGIN
		INSERT INTO tbAutoInitLog (TerminalID, 
									LastInitTime, 
									PABX, 
									SoftwareVer, 
									OsVer, 
									KernelVer, 
									EDCSN, 
									ReaderSN, 
									PSAMSN, 
									MCSN, 
									MemUsage,
									ICCID
								   )
		VALUES (@sTID,
				@sLastInitTime,
				@sPABX,
				@sSoftwareVers,
				@sOSVers,
				@sKernelEmvVers,
				@sEdcSN,
				@sReaderSN,
				@sPSAMSN,
				@sMCSN,
				@sMemUsg,
				@sICCID
			   )
	END
	ELSE
	BEGIN
	UPDATE tbAutoInitLog 
	SET LastInitTime = @sLastInitTime,
		PABX = @sPABX, 
		SoftwareVer = @sSoftwareVers, 
		OsVer = @sOSVers, 
		KernelVer = @sKernelEmvVers, 
		EDCSN = @sEdcSN, 
		ReaderSN = @sReaderSN, 
		PSAMSN = @sPSAMSN, 
		MCSN = @sMCSN, 
		MemUsage = @sMemUsg,
		ICCID = @sICCID
	WHERE TerminalID = @sTID
	END

