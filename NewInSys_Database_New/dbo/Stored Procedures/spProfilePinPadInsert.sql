﻿/*
-- Author		: Tobias SETYO
-- Created Date	: Oct 8, 2013
-- Modify Date	: 1. <mmm dd, yyyy>, <description>
-- Description	: INSERT into tbProfilePinPad
*/
CREATE PROCEDURE [dbo].[spProfilePinPadInsert]
	@iDatabaseID INT,
	@sPinPadName VARCHAR(25),
	@sContent VARCHAR(MAX)=NULL
AS
DECLARE @iCountID INT

SELECT @iCountID = COUNT(*)
FROM tbProfilePinPad
WHERE PinPadTag IN ('PP01','PP001') AND
		PinPadTagValue = @sPinPadName AND
		DatabaseID = @iDatabaseID

IF @iCountID = 0
BEGIN
	IF ISNULL(@sContent,'')<>''
	BEGIN
		IF (dbo.isTagLength4(@iDatabaseID)=5)
		BEGIN
			INSERT INTO tbProfilePinPad(
				DatabaseID,
				PinPadName,
				PinPadTag,
				PinPadLengthOfTagLength,
				PinPadTagLength,
				PinPadTagValue)
			SELECT 			
				@iDatabaseID,
				@sPinPadName,
				Tag,
				LEN(TagLength),
				TagLength,
				TagValue
			FROM dbo.fn_tbProfileTLV5(@sPinPadName,@sContent)
		END
		ELSE
		BEGIN
			INSERT INTO tbProfilePinPad(
				DatabaseID,
				PinPadName,
				PinPadTag,
				PinPadLengthOfTagLength,
				PinPadTagLength,
				PinPadTagValue)
			SELECT 			
				@iDatabaseID,
				@sPinPadName,
				Tag,
				LEN(TagLength),
				TagLength,
				TagValue
			FROM dbo.fn_tbProfileTLV(@sPinPadName,@sContent)
		END
	END
END
ELSE
	SELECT 'PinPad Name has already used. Please choose other PinPad Name.'