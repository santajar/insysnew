﻿

-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 29, 2010
-- Modify date: 
--				1. Jan 12, 2011, update column IpAddress
-- Description:	
--				1. Updating the AllowDownload, StatusMaster, and EnableIp profile parameter
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalListUpdateContent] 
	@sTerminalID VARCHAR(8),
	@sDbID INT,
	@bAllowDownload BIT,
	@bStatus BIT,
	@bEnableIP BIT,
	@sIpAddress VARCHAR(15) = NULL,
--	@bAutoInit BIT = '1',
	@bInitCompress BIT = '0',
	@bRemoteDownload BIT ='0'
	--@sContent Varchar(MAX)
AS
--awal

SET NOCOUNT ON

DECLARE @dDate DATETIME

IF @bAllowDownload = 1
	SET @dDate = GETDATE()

UPDATE tbProfileTerminalList 
SET 
	--AllowContent = '0',
	StatusMaster = @bStatus,
	EnableIP = @bEnableIP,
	IpAddress = @sIpAddress
WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID

IF @bAllowDownload = 0 
	SET @dDate = GETDATE()

UPDATE tbProfileTerminalList 
SET 
	--AllowContent = '0',
	--AllowDownload = '0',
	StatusMaster = @bStatus,
	EnableIP = @bEnableIP,
	IpAddress = @sIpAddress
WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID

IF EXISTS ( 
	SELECT * FROM SYS.COLUMNS
		WHERE Name = N'AutoInitTimeStamp' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
	AND @bAllowDownload = '1' 
BEGIN
	UPDATE tbProfileTerminalList 
		SET AutoInitTimeStamp = @dDate
	WHERE TerminalID = @sTerminalID AND 
		  DatabaseID = @sDbID
END


IF EXISTS (  
	SELECT * FROM SYS.COLUMNS
	WHERE Name = N'InitCompress' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
BEGIN
	UPDATE tbProfileTerminalList 
		SET InitCompress = @bInitCompress
	WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID
END

IF EXISTS (  
	SELECT * FROM SYS.COLUMNS
	WHERE Name = N'RemoteDownload' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
BEGIN
	UPDATE tbProfileTerminalList 
		SET RemoteDownload = @bRemoteDownload
	WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID
END



EXEC spProfileTerminalUpdateLastUpdate @sTerminalID
--EXEC spProfileTerminalListUpdateRD @sTerminalID