﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <20 January 2016>
-- Description:	<Restore Profile data Terminal>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalHistoryRestore]
	@sTerminalID VARCHAR(8),
	@sDate VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue
	INTO #TempTerminal
	FROM tbProfileTerminal WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID

	DELETE FROM tbProfileTerminal
	WHERE TerminalID = @sTerminalID

	INSERT INTO tbProfileTerminal (TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue)
	SELECT TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue
	FROM tbProfileTerminalHistory
	WHERE TerminalID = @sTerminalID
		AND LastUpdate = @sDate

	INSERT INTO tbProfileTerminalHistory(TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue,LastUpdate)
	SELECT TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue, CONVERT(VARCHAR(20),GETDATE(),113)
	FROM #TempTerminal
	WHERE TerminalID = @sTerminalID

	EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
	EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

	DROP TABLE #TempTerminal

    
END