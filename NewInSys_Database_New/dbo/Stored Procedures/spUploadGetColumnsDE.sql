﻿

-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, Tedie, add template name
--				2. Sept 08, 2014, template default value
-- Description:	
--				1. Browse the terminal itemname of the defined template
-- =============================================
CREATE PROCEDURE [dbo].[spUploadGetColumnsDE]
	--@sTemplate VARCHAR(25)
	@sFile VARCHAR(25)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ColumnName
	FROM tbUploadTagDE WITH (NOLOCK)
	WHERE Template = @sFile
END
