﻿
CREATE PROCEDURE [dbo].[spProfileSoftwareBrowseAvailable]
	@iDatabaseID INT
AS
	SELECT ApplicationName
	FROM tbBitMap WITH (NOLOCK)
	WHERE ApplicationName NOT IN (
									SELECT SoftwareName
									FROM tbProfileSoftware WITH (NOLOCK)
									WHERE DatabaseID = @iDatabaseID
								 )

