﻿CREATE PROCEDURE [dbo].[spReplaceTIDLogInsert]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8),
	@sStatus VARCHAR(20),
	@sUserId VARCHAR(50)
AS
	INSERT INTO tbReplaceTIDLog(Date, UserId, OldTID, NewTID, Status)
	VALUES (GETDATE(), @sUserId, @sOldTID, @sNewTID, @sStatus)
		



