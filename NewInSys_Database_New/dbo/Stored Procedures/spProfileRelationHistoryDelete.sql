﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 27, 2016
-- Modify date: 
-- Description:	
--				1. DELETE The History Relation table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationHistoryDelete]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM tbProfileRelationHistory
	WHERE TerminalID = @sTerminalID
END