﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,,23 April 2014>
-- Description:	<Description,,Get Schedule and software name by TID>
-- =============================================
CREATE PROCEDURE [dbo].[spRDGetInfobyTID]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		A.TerminalID
		, A.ScheduleID
		, B.ScheduleTime
		, A.AppPackID
		, C.AppPackageName
		, C.BuildNumber
		, A.IdRegionRD
		, D.RegionRDName
		, A.IdGroupRD
		, E.GroupRDName
		, A.IdCityRD
		, F.CityRDName
	FROM (
		SELECT *
		FROM tbListTIDRemoteDownload WITH (NOLOCK)) A
		LEFT JOIN
		(SELECT *
		FROM tbScheduleSoftware WITH (NOLOCK)) B
		ON A.ScheduleID = B.ScheduleID
		LEFT JOIN
		(SELECT *
		FROM tbAppPackageEDCList WITH (NOLOCK)) C
		ON A.AppPackID=C.AppPackId
		LEFT JOIN
		(SELECT * 
		FROM tbRegionRemoteDownload WITH (NOLOCK))D
		ON A.IdRegionRD=D.IdRegionRD
		LEFT JOIN
		(SELECT *
		FROM tbGroupRemoteDownload WITH (NOLOCK))E
		ON A.IdGroupRD = E.IdGroupRD
		LEFT JOIN
		(SELECT *
		FROM tbCityRemoteDownload WITH (NOLOCK)) F
		ON A.IdCityRD = F.IdCityRD
	WHERE A.TerminalID = @sTerminalID
END