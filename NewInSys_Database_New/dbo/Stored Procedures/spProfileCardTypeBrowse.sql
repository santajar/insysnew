﻿-- =============================================
-- Author:		Tobias Setyo
-- Create date: Nov 14, 2016
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the content of Card Type Management table
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCardTypeBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery='
	SELECT 
		CardTypeName,
		DatabaseID,
		CardTypeTag as Tag,
		CardTypeTagLength,
		CardTypeTagValue as Value
	FROM tbProfileCardType WITH (NOLOCK) '
	SET @sQuery = @sQuery + ISNULL(@sCondition,'') + ' ORDER BY CardTypeName, CardTypeTag'
EXEC (@sQuery)