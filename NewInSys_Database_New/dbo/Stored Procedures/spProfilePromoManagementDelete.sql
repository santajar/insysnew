﻿-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Feb 10, 2016
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete the Promo management on table tbProfilePromoManagement
--				2. Modify Promo Management for Form Flexsible
-- =============================================
CREATE PROCEDURE [dbo].[spProfilePromoManagementDelete]
	@iDatabaseId INT,
	@sPromoManagementName VARCHAR(50),
	@bDeleteData BIT = 1
AS
DELETE FROM tbProfilePromoManagement
WHERE DatabaseId=@iDatabaseId AND PromoManagementName=@sPromoManagementName
EXEC [spItemComboBoxValueDeletePromoManagementName] @sPromoManagementName, @iDatabaseId