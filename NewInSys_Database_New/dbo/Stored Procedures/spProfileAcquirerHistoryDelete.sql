﻿

-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Description:	delete The History Acquirer table content of Terminal
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerHistoryDelete]
	@sTerminalID VARCHAR(8),
	@sAcquirerName VARCHAR(25)
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM tbProfileAcquirerHistory 
	WHERE TerminalID = @sTerminalID AND AcquirerName = @sAcquirerName
END