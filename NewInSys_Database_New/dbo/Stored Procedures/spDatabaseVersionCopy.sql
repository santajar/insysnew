﻿
-- ==============================================================
-- Description:	Create new Version by copying existing Version
-- ==============================================================

CREATE PROCEDURE [dbo].[spDatabaseVersionCopy]
	@sDbSourceID SMALLINT,
	@sDbDestinationID SMALLINT,
	@sDatabaseName VARCHAR(15)
AS
	EXEC spProfileTerminalDbCopy @sDbSourceID,@sDbDestinationID,@sDatabaseName
	EXEC spTbItemListCopy @sDbSourceID, @sDbDestinationID
	EXEC sptbItemComboBoxValueCopy @sDbSourceID, @sDbDestinationID

