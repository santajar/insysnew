﻿

-- =============================================  
-- Author:  Tedie Scorfia  
-- Create date: March 15, 2015  
-- Description: view DB list with RemoteDownload
-- =============================================  
CREATE PROCEDURE [dbo].[spDBListRDBrowse]  
AS
SELECT DatabaseID, DatabaseName FROM tbProfileTerminalDB WITH (NOLOCK)
	WHERE RemoteDownload = 1
	ORDER BY DatabaseName