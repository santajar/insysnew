﻿
-- ==================================
-- Description: Get all Location data
-- ==================================

CREATE PROCEDURE [dbo].[spLocationBrowse]
AS
SELECT LocationID,
	LocationDesc
FROM tbLocation WITH (NOLOCK)

