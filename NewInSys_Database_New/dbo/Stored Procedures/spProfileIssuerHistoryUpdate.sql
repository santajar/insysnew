﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Description:	
--				1. Update The History Issuer table content of profile
-- =============================================
cREATE PROCEDURE [dbo].[spProfileIssuerHistoryUpdate]
	@sTerminalID VARCHAR(8),
	@sIssuerName VARCHAR(25)
AS
BEGIN
	SET NOCOUNT ON;

	EXEC spProfileIssuerHistoryDelete @sTerminalID,@sIssuerName
	EXEC spProfileIssuerHistoryInsert @sTerminalID,@sIssuerName
END