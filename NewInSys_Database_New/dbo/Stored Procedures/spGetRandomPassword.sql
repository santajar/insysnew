﻿
-- =============================================  
-- Author:  Ibnu Saefullah  
-- Create date: Jan 15, 2017  
-- Description: Get Random Password
-- =============================================  
CREATE PROCEDURE [dbo].[spGetRandomPassword]
	 @sTerminalID nvarchar(8)

AS
BEGIN
SET NOCOUNT ON

DECLARE @rnd varchar(4)
SELECT @rnd = RIGHT('0000' + CAST(ABS(CHECKSUM(NEWID())) % 9999 AS varchar(4)), 4)
BEGIN
 SELECT * from tbProfileTerminal where TerminalID = @sTerminalID AND TerminalTag='DC04'
 IF @@ROWCOUNT > 0
 BEGIN
 UPDATE tbProfileTerminal SET TerminalTagValue = @rnd WHERE TerminalID = @sTerminalID AND TerminalTag='DC04'
 END
 ELSE 
 BEGIN
 INSERT INTO tbProfileTerminal (TerminalID, TerminalTag,TerminalLengthOfTagLength, TerminalTagLength, TerminalTagValue) 
 VALUES (@sTerminalID,'DC04','1','4', @rnd)
 END
 END
 END