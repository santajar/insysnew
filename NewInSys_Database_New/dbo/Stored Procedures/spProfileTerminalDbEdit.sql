﻿CREATE PROCEDURE [dbo].[spProfileTerminalDbEdit]
	@sDbID SMALLINT,
	@sDatabaseName VARCHAR(15),
	@sNewInit BIT,
	@sEMVInit BIT,
	@sTLEInit BIT,
	@sLoyaltyProd BIT=0,
	@sGPRSInit BIT=0,
	@sBankCode BIT=0,
	@sProductCode BIT=0
AS
	UPDATE tbProfileTerminalDb
	SET DatabaseName = @sDatabaseName,
		NewInit=@sNewInit,
		EMVInit=@sEMVInit,
		TLEInit=@sTLEInit,
		LoyaltyProd=@sLoyaltyProd,
		GPRSInit=@sGPRSInit,
		BankCode=@sBankCode,
		ProductCode=@sProductCode
	WHERE DatabaseID=@sDbID