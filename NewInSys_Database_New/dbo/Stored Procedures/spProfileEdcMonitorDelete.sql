﻿-- =============================================
-- Author		: Tobias SETYO
-- Create date	: March 8, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spProfileEdcMonitorDelete]
	@iDatabaseID INT,
	@sEdcMonitorName VARCHAR(15),
	@bDeleteData BIT = 1
AS
DELETE tbProfileEdcMonitor
WHERE DatabaseID = @iDatabaseID
	AND Name = @sEdcMonitorName
EXEC spItemComboBoxValueDeleteEdcMonitoring @sEdcMonitorName, @iDatabaseID