﻿/*
AUTHOR		: Tobias SETYO
CREATED DATE: Nov 11, 2013
MODIFY DATE	:
DESCRIPTION	: Browse distinct PinPadName on tbProfilePinPad
*/
CREATE PROCEDURE [dbo].[spProfilePinPadListBrowse]
	@sDatabaseId VARCHAR(3)
AS
SELECT DISTINCT PinPadName [Name],
		DatabaseID			
FROM tbProfilePinPad WITH (NOLOCK)
WHERE DatabaseID = @sDatabaseId