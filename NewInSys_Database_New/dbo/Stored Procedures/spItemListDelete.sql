﻿-- ====================================================
-- Description:	Delete tag for given Database and Form
-- ====================================================

CREATE PROCEDURE [dbo].[spItemListDelete]
	@sDatabaseID SMALLINT,
	@sFormID SMALLINT,
	@sObjectID SMALLINT,
	@iItemSequence SMALLINT,
	@sTag VARCHAR(5)
AS
DELETE FROM tbItemList
WHERE DatabaseID = @sDatabaseID AND 
	  FormID = @sFormID AND 
	  Tag = @sTag

IF (@sObjectID = 4) --If Object is ComboBox, delete ComboBoxItemValue
EXEC spItemComboBoxValueDeleteBySequence @sDatabaseID, @sFormID, @iItemSequence

IF @sFormID >='1' OR @sFormID <='3'
BEGIN
	DECLARE @sQuery VARCHAR(MAX), @sTable VARCHAR(MAX), @sColumn VARCHAR(MAX)
	IF @sFormID='1'
	BEGIN
		SET @sTable = 'tbProfileTerminal'
		SET @sColumn = 'TerminalTag'
	END
	ELSE IF @sFormID='2'
	BEGIN
		SET @sTable = 'tbProfileAcquirer'
		SET @sColumn = 'AcquirerTag'
	END	
	ELSE IF @sFormID='3'
	BEGIN
		SET @sTable = 'tbProfileIssuer'
		SET @sColumn = 'IssuerTag'
	END
	SET @sQuery = 'DELETE FROM ' + @sTable 
		+ ' WHERE TerminalID IN 
		(SELECT TerminalID FROM tbProfileTerminalList WITH (NOLOCK) WHERE DatabaseID = ''' + CAST(@sDatabaseID AS VARCHAR) + ''')'
		+ ' AND ' + @sColumn + ' = ''' + @sTag + ''''
	print @sQuery
	EXEC (@sQuery)
END