USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileIssuerUpdate]    Script Date: 11/2/2017 3:44:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spProfileIssuerUpdate]
@sTerminalID VARCHAR(8),
@sContent VARCHAR(MAX)
AS
DECLARE @sTID VARCHAR(8)
	DECLARE @sName VARCHAR(50)
	DECLARE @sTag VARCHAR(5)
	DECLARE @iLengthOfTagLength INT
	DECLARE @iTagLength INT
	DECLARE @sValue VARCHAR(150)

	IF (OBJECT_ID('TEMPDB..#TempIss') IS NOT NULL)
		DROP TABLE #TempIss
	CREATE TABLE #TempIss 
	(
		TerminalID VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		[LengthOfTagLength] INT,
		TagLength INT,
		TagValue VARCHAR(150)
	)

	IF (OBJECT_ID('TEMPDB..#TEMPTABLEOLDNEW') IS NOT NULL)
	DROP TABLE #TEMPTABLEOLDNEW

	CREATE TABLE #TEMPTABLEOLDNEW
	(
		ID int  IDENTITY(1,1) NOT NULL,
		TAG VARCHAR(5),
		--AcquirerName VARCHAR(5),
		ITEMNAME VARCHAR(50),
		OLDVALUE VARCHAR(150),
		NEWVALUE VARCHAR(150)
	)

	DECLARE @sDbID VARCHAR(5),
			@sIssName VARCHAR(50)

	declare @sumTagIss int
	declare @iIss int=1
	declare @TAGIss varchar (5)
	declare @ValueIss nvarchar (max)
	declare @queryIss nvarchar (MAX)

	SET @sDbID = dbo.iDatabaseIdByTerminalId(@sTerminalID)
	IF (DBO.isTagLength4(CAST(@sDbID AS INT))=5)
	BEGIN
		INSERT INTO #TempIss(TerminalID,[Name],	Tag,[LengthOfTagLength],TagLength,TagValue)
		SELECT TerminalID,
				[Name],
				Tag,
				LEN(TagLength) [LengthOfTagLength],
				TagLength,
				TagValue
		FROM fn_tbprofiletlv5(@sTerminalID, @sContent)
	END
	ELSE
	BEGIN
		INSERT INTO #TempIss(TerminalID,[Name],	Tag,[LengthOfTagLength],TagLength,TagValue)
		SELECT TerminalID,
				[Name],
				Tag,
				LEN(TagLength) [LengthOfTagLength],
				TagLength,
				TagValue
		FROM fn_tbprofiletlv(@sTerminalID, @sContent)
	END
	
	--SELECT DISTINCT @sIssName = [Name] FROM #TempIss
	set @sIssName=(select distinct Name from #TempIss)
	INSERT INTO #TEMPTABLEOLDNEW(TAG,ITEMNAME,OLDVALUE,NEWVALUE)
	EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'AE', @sIssName, @sContent, 1
	----------------------
	--print 'sini'
	SELECT @sumTagIss=(select count (TAG) from #TEMPTABLEOLDNEW)
	--print ('counter')
	while (@iIss<=@sumTagIss)
	begin

		select @TAGIss=TAG from #TEMPTABLEOLDNEW WHERE ID=@iIss
		select @ValueIss=NEWVALUE from #TEMPTABLEOLDNEW WHERE ID=@iIss
		SET @queryIss ='update tbProfileIssuer SET dbo.[tbProfileIssuer].[' +  @TAGIss + ']=''' + @ValueIss + ''' 
					where TerminalID='''+ @sTerminalID +''' and IssuerName='''+ @sIssName +'''' 
		exec (@queryIss)
		--print (@queryIss)
		SET @iIss = @iIss +  1
	
	end

EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
EXEC spProfileTerminalUpdateLastUpdate @sTerminalID


GO


