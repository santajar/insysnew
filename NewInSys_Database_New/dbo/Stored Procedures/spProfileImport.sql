﻿
CREATE PROCEDURE [dbo].[spProfileImport]  @sDatabaseId VARCHAR(3),
	@sTerminalId VARCHAR(8),
	@sContent VARCHAR(MAX)
AS
EXEC spProfileTerminalImport @sDatabaseId, @sTerminalId, @sContent
EXEC spProfileAcquirerImport @sDatabaseId, @sTerminalId, @sContent
EXEC spProfileIssuerImport @sDatabaseId, @sTerminalId, @sContent
EXEC spProfileRelationImport @sTerminalId, @sContent
EXEC spProfileRelationInsertMigrate @sTerminalId

