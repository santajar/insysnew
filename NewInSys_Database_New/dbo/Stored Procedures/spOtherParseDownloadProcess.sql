﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 13, 2011
-- Modify	  :
--				1. Sept 13, 2013, update table ##tempResult column APPNAME, APPFILENAME length to 150char
-- Description:	
--				1. process the received message and then return the response the ISO
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseDownloadProcess] 
	@sMessage VARCHAR(MAX),
	@iCheckInit INT
AS
SET NOCOUNT ON

DECLARE @sSqlStmt VARCHAR(MAX),
	@sNII VARCHAR(4),
	@sAddress VARCHAR(4),
	@sMTI VARCHAR(4),
	@sBitMap VARCHAR(16),
	@sBinaryBitMap VARCHAR(64),
	@sEdcMessage VARCHAR(MAX),
	@sSessionTime VARCHAR(10),
	@sTableTemp VARCHAR(100),
	@sTerminalId VARCHAR(8),
	@sProcCode varchar(6)

-- initialization
SET @sSessionTime=
	REPLACE(REPLACE(CONVERT(VARCHAR,GETDATE(),114),':',''),' ','')
SET @sTableTemp= '##tempBitMap' + @sSessionTime
SET @sNII=SUBSTRING(@sMessage,1,4)
SET @sAddress=SUBSTRING(@sMessage,5,4)
SET @sMTI=SUBSTRING(@sMessage,9,4)
SET @sBitMap=SUBSTRING(@sMessage,13,16)
SET @sEdcMessage=SUBSTRING(@sMessage,29,LEN(@sMessage)-28)

print 'Start ISO Unpack : ' + @sMessage
DECLARE @sLinkDatabase NVARCHAR(50)
SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

-- is MTI init or not
IF dbo.IsInitMTI(@sMTI) = 1
BEGIN
	-- unpack the messages
	SET @sBinaryBitMap = dbo.Hex2Bin(@sBitMap)
	EXEC spOtherParseISOUnpack @sSessionTime,@sBinaryBitMap,@sEdcMessage
END

print 'Start ISO Pack'
print @iCheckInit
print		@sNII
print		@sAddress
print		@sMTI
print		@sSessionTime
IF OBJECT_ID('tempdb..##tempResult') IS NOT NULL
	DROP TABLE ##tempResult

CREATE TABLE ##tempResult (
	TERMINALID VARCHAR(8) NULL,
	[CONTENT] VARCHAR(MAX) NULL,
	TAG VARCHAR(4) NULL,
	ISO VARCHAR(MAX),
	ERRORCODE INT,
	APPNAME VARCHAR(150),
	APPFILENAME VARCHAR(150)
)

EXEC spOtherParseProcCode @sSessionTime, @sProcCode OUTPUT
IF @sProcCode = '990010' OR @sProcCode = '990011'
begin
	INSERT INTO ##tempResult(TERMINALID, [CONTENT], TAG, ISO, ERRORCODE, APPNAME, APPFILENAME)
	EXEC spOtherParseDownloadISOPackSchedule 
		@iCheckInit,
		@sNII,
		@sAddress,
		@sMTI,
		@sSessionTime,
		@sTerminalId OUTPUT
end
ELSE IF @sProcCode = '990020'
begin

	PRINT 'SEBELUM ERROR'
	DECLARE @sTempContent VARCHAR(MAX), @sTempTag VARCHAR(MAX), @sTempISOMessage VARCHAR(MAX), @iTempErrorcode INT, @sTempAppNameDownload VARCHAR(MAX), @sTempAppFileNameDownload VARCHAR(MAX)
	
	EXEC spOtherParseInstalMessage
		@iCheckInit,
		@sNII,
		@sAddress,
		@sMTI,
		@sSessionTime,
		@sTerminalId OUTPUT,
		@sTempContent OUTPUT,
		@sTempTag OUTPUT,
		@sTempISOMessage OUTPUT,
		@iTempErrorcode OUTPUT,
		@sTempAppNameDownload OUTPUT,
		@sTempAppFileNameDownload OUTPUT


	INSERT INTO ##tempResult(TERMINALID, [CONTENT], TAG, ISO, ERRORCODE, APPNAME, APPFILENAME)
	VALUES (@sTerminalId,@sTempContent,@sTempTag,@sTempISOMessage,@iTempErrorcode,@sTempAppNameDownload,@sTempAppFileNameDownload)

end
else
begin
	INSERT INTO ##tempResult(TERMINALID, [CONTENT], TAG, ISO, ERRORCODE, APPNAME, APPFILENAME)
	EXEC spOtherParseDownloadISOPack
		@iCheckInit,
		@sNII,
		@sAddress,
		@sMTI,
		@sSessionTime,
		@sTerminalId OUTPUT
end

-- debug into table
DECLARE @sQuery VARCHAR(MAX)

SET @sQuery = N'
INSERT INTO '+@sLinkDatabase+'tbInitTrace
           ([ErrorCode]
           ,[TerminalId]
           ,[ISO]
		   ,[Tag]
           ,[TerminalContent])
SELECT ERRORCODE,TERMINALID,ISO,TAG,[CONTENT] FROM ##tempResult
WHERE TERMINALID = '''+@sTerminalId+''''

EXEC (@sQuery)

DECLARE @iERRORCODE INT,
		@sISO VARCHAR(MAX),
		@sTag VARCHAR(4),
		@sContent VARCHAR(MAX)

SELECT @iERRORCODE = @iERRORCODE,
		@sISO = ISO,
		@sTag = TAG,
		@sContent = [CONTENT]
FROM ##tempResult
WHERE TERMINALID = @sTerminalId

IF @sProcCode = '990020'
BEGIN
DECLARE @iLen int 
SET @iLen = LEN(@sISO)
UPDATE ##tempResult SET ISO = SUBSTRING(ISO,1,14) + '2038010002800000' + SUBSTRING(ISO,31, @iLen);
END

EXEC spInitTraceInsert @iERRORCODE, @sTerminalId, @sISO, @sTag, @sContent

print 'Success debug'



SELECT ERRORCODE,
	TERMINALID,
	ISO,
	TAG,
	[CONTENT],
	APPNAME,
	APPFILENAME
FROM ##tempResult
WHERE TERMINALID = @sTerminalId