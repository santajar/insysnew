﻿-- =============================================
-- Author:		Tobias Setyo
-- Create date: Nov 14, 2016
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the list of InitialFlazz
-- =============================================
CREATE PROCEDURE [dbo].[spProfileInitialFlazzManagementBrowseList]
	@sDatabaseId VARCHAR(3)
AS
SELECT DISTINCT
	InitialFlazzName as Name
FROM tbProfileInitialFlazz WITH (NOLOCK)
WHERE DatabaseID=@sDatabaseId