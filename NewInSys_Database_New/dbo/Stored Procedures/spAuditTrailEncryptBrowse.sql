--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAuditTrailEncryptBrowse]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [dbo].[spAuditTrailEncryptBrowse]
--GO

CREATE PROCEDURE [dbo].[spAuditTrailEncryptBrowse]
	@sCond VARCHAR(MAX)=''
WITH ENCRYPTION
AS
DECLARE @sStmt VARCHAR(MAX)
SET @sStmt = '
	SELECT LogID,
		AccessTime [AccessTime],
		UserID [UserID],
		DatabaseName [DatabaseName],
		ActionDescription [ActionDescription],
		Remarks [Remarks]
	FROM master.dbo.viewAuditTrail '
EXEC (@sStmt + @sCond)

