﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <March 2 2015>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAIDCopybyDBID] 
	@NewDatabaseID VARCHAR(3),
	@OldDatabaseID VARCHAR(3),
	@sAIDName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @iCount INT,
			@iOldTagLength INT,
			@iNewTagLength INT
	
	SELECT @iCount = COUNT(*)
	FROM tbProfileAID WITH (NOLOCK)
	WHERE DatabaseId = @NewDatabaseID AND AIDName = @sAIDName
	
	SET @iOldTagLength = dbo.isTagLength4(@OldDatabaseID)
	SET @iNewTagLength = dbo.isTagLength4(@NewDatabaseID)
	
	print @iOldTagLength
	print @iNewTagLength
	IF @iCount = 0
	BEGIN
		IF @iOldTagLength = @iNewTagLength
		BEGIN
			INSERT INTO tbProfileAID(DatabaseId,AIDName,AIDTag,AIDLengthOfTagLength,AIDTagLength,AIDTagValue)
			SELECT @NewDatabaseID,AIDName,AIDTag,AIDLengthOfTagLength,AIDTagLength,AIDTagValue
			FROM tbProfileAID WITH (NOLOCK)
			WHERE DatabaseId = @OldDatabaseID AND AIDName = @sAIDName
		END
		ELSE
		BEGIN
			IF @iOldTagLength > @iNewTagLength
			BEGIN
				INSERT INTO tbProfileAID(DatabaseId,AIDName,AIDTag,AIDLengthOfTagLength,AIDTagLength,AIDTagValue)
				SELECT @NewDatabaseID,AIDName,SUBSTRING(AIDTag,1,2)+ SUBSTRING(AIDTag,4,2),AIDLengthOfTagLength,AIDTagLength,AIDTagValue
				FROM tbProfileAID WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND AIDName = @sAIDName
			END
			ELSE
			BEGIN
				INSERT INTO tbProfileAID(DatabaseId,AIDName,AIDTag,AIDLengthOfTagLength,AIDTagLength,AIDTagValue)
				SELECT @NewDatabaseID,AIDName,SUBSTRING(AIDTag,1,2) +'0'+SUBSTRING(AIDTag,3,2),AIDLengthOfTagLength,AIDTagLength,AIDTagValue
				FROM tbProfileAID WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND AIDName = @sAIDName
			END
		END
	END
	ELSE
		SELECT 'AID Name has already used. Please choose other AID Name.'
	
	
END