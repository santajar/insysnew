﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Oct 1, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating the initialization log percentage progress for new profile initialization
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnNewInit]
@sTerminalID VARCHAR(8),
@iStartInit INT
AS 
	DECLARE @iCountTID INT
	SELECT @iCountTID = COUNT(*) FROM tbInitLogConn WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
	
	IF (@iCountTID = 0)
		EXEC spInitLogConnInsertProcess @sTerminalID	
	ELSE 
	BEGIN
		SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
		BEGIN TRANSACTION
		IF (@iStartInit = 1)
			UPDATE tbInitLogConnDetail SET Percentage = 0, [Time] = GETDATE() WHERE TerminalId = @sTerminalID
		EXEC spInitLogConnUpdateProcess @sTerminalID
		COMMIT TRANSACTION
	END