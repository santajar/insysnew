﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 30, 2010
-- Modify date:
--				1. 
-- Description:	Get The tbInit content
-- =============================================
CREATE PROCEDURE [dbo].[spInitBrowse]
	@sCond VARCHAR(3000)=NULL
AS
DECLARE @sStmt VARCHAR(MAX)
SET @sStmt=
	'SELECT TOP(1) InitId, 
		TerminalId,
		Content,
		Tag,
		Flag
	FROM tbInit WITH (NOLOCK) '

IF @sCond IS NULL
	EXEC (@sStmt)
ELSE
	EXEC (@sStmt+@sCond)