﻿CREATE PROCEDURE [dbo].[spUserLoginBrowseHistoryUpdate]
	@sOldUserRights VARCHAR(2000),
	@sNewUserRights VARCHAR(2000)
AS
	DECLARE @TempTableOld TABLE (OldUserTag VARCHAR(50), 
								 OldTagName VARCHAR(30), 
								 OldUserRight VARCHAR(200)
								)
	DECLARE @TempTableNew TABLE (NewUserTag VARCHAR(50), 
								 NewTagName VARCHAR(30), 
								 NewUserRight VARCHAR(200)
								)

	INSERT INTO @TempTableOld
	EXEC spUserLoginBrowseHistoryInsert @sOldUserRights
	
	INSERT INTO @TempTableNew
	EXEC spUserLoginBrowseHistoryInsert @sNewUserRights

	SELECT OldUserTag,
			OldTagName,
			OldUserRight,
			NewUserTag, 
			NewTagName,
			NewUserRight
	FROM @TempTableOld o FULL OUTER JOIN 
		 @TempTableNew n ON o.OldUserTag = n.NewUserTag
	WHERE ISNULL(o.OldUserRight,'') != ISNULL(n.NewUserRight,'')
 

