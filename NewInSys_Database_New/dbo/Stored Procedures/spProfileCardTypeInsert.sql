﻿-- =============================================
-- Author:		Tobias Setyo
-- Create date: Jan 26, 2016
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Insert new CardType Management into tbProfileInitialFlazz
--				2. Modify to use form flexible`
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCardTypeInsert]
	@iDatabaseID INT,
	@sCardTypeName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfileCardType WITH (NOLOCK)
WHERE CardTypeTag IN ('CT01','CT001') AND
		CardTypeTagValue = @sCardTypeName AND
		DatabaseID = @iDatabaseID

IF @iCountID =0
BEGIN
	IF (dbo.isTagLength4(@iDatabaseID)= 4)
	BEGIN
		INSERT INTO tbProfileCardType(
			[CardTypeName],
			[DatabaseID],
			[CardTypeTag],
			[CardTypeTagLength],
			[CardTypeTagValue]
			)
		SELECT 
			[Name] [CardTypeName],
			@iDatabaseID [DatabaseId],
			Tag [CardTypeTag],
			TagLength [CardTypeTagLength],
			TagValue [CardTypeTagValue]
		FROM dbo.fn_TbProfileTLV(@sCardTypeName,@sContent)
	END
	ELSE
	BEGIN
		INSERT INTO tbProfileCardType(
			[CardTypeName],
			[DatabaseID],
			[CardTypeTag],
			[CardTypeTagLength],
			[CardTypeTagValue]
			)
		SELECT 
			[Name] [CardTypeName],
				@iDatabaseID [DatabaseId],
			Tag [CardTypeTag],
			TagLength [CardTypeTagLength],
			TagValue [CardTypeTagValue]
		FROM dbo.fn_TbProfileTLV5(@sCardTypeName,@sContent)
	END
END