﻿-- =============================================
-- Author		: Tobias SETYO
-- Create date	: March 17, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spItemComboBoxValueDeleteEdcMonitoring]
	@sEdcMonitoringName VARCHAR(15),
	@sDatabaseId INT
AS
DELETE FROM tbItemComboBoxValue
WHERE DatabaseId=@sDatabaseId
	AND RealValue=@sEdcMonitoringName