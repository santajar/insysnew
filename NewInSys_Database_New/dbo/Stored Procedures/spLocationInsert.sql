﻿
-- ==============================================
-- Description: Add new Location into tbLocation
-- ==============================================

CREATE PROCEDURE [dbo].[spLocationInsert]
	@sLocationDesc VARCHAR(50),
	@sLocationID VARCHAR (15)
AS


DECLARE @iCount INT
SELECT @iCount = COUNT(*)
FROM tbLocation WITH (NOLOCK)
WHERE LocationID = @sLocationID

IF @iCount = 0
BEGIN
INSERT INTO tbLocation(LocationID ,LocationDesc)
VALUES (@sLocationID, @sLocationDesc)
END
ELSE
	SELECT 'ERROR '
