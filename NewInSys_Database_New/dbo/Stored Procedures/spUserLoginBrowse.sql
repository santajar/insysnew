﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Agt 24, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Search specified user login
-- =============================================
CREATE PROC [dbo].[spUserLoginBrowse] 
	@sCondition VARCHAR(200)=NULL
AS
BEGIN
	DECLARE @sSqlstmt VARCHAR(500)
	SET @sSqlstmt =
			'SELECT UserID,
					UserName,
				    Password,
                    UserRights,
                    GroupID,
                    Locked,
					[Revoke],
					EnableTools
			FROM tbUserLogin WITH (NOLOCK) ' 
	IF ( @sCondition IS NULL) EXEC(@sSqlstmt)
	ELSE EXEC(@sSqlstmt + @sCondition)
	
	IF CHARINDEX('UserID',@sCondition)>0 AND CHARINDEX('Password',@sCondition)>0
	BEGIN
		EXEC spUserLoginUpdateLastLogin @sCondition
		
		DECLARE @iStartIndex INT,
				@iLastIndex INT,
				@iCount INT,
				@sUserID VARCHAR(100)
				
		SET @iStartIndex = CHARINDEX('UserID',@sCondition)+8
		SET @iLastIndex = CHARINDEX(''' AND',@sCondition)
		
		SET @sUserID =  SUBSTRING(@sCondition,@iStartIndex, @iLastIndex-@iStartIndex)
		
		SELECT @iCount = COUNT (*)
		FROM tbUserLogin WITH (NOLOCK)
		WHERE UserID = @sUserID 
			AND (LastModifyPassword IS NULL OR LastModifyPassword = '')
		
		select @iCount
		IF @iCount = 1
			EXEC spUserLoginUpdateLastModifyPassword @sUserID
		
		
	END
END
