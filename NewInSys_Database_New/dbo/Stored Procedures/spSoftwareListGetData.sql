﻿
-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSoftwareListGetData]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO tbTempSoftwareListDownload (GroupName,SoftwareName,ScheduleType,ScheduleTime,TerminalID,SoftwareStatus)
	SELECT a.GroupName,SoftwareName,ScheduleType,ScheduleTime,TerminalID,'OnProgress' FROM tbSoftwareGroupPackage a
	JOIN tbListTerminalIDPackage b
	ON a.GroupName = b.GroupName
	WHERE b.TerminalID = @sTerminalID
    
	SELECT a.GroupName,SoftwareName,ScheduleType,ScheduleTime,TerminalID FROM tbSoftwareGroupPackage a
	JOIN tbListTerminalIDPackage b
	ON a.GroupName = b.GroupName
	WHERE b.TerminalID = @sTerminalID

END