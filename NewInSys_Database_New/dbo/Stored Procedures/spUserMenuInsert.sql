IF OBJECT_ID ('dbo.spUserMenuInsert') IS NOT NULL
	DROP PROCEDURE dbo.spUserMenuInsert
GO

CREATE PROCEDURE [dbo].[spUserMenuInsert]
	@sUserID VARCHAR(20)
AS

SELECT * FROM tbUserMenu WHERE UserID = @sUserID
IF @@ROWCOUNT = 1 
BEGIN 
	DELETE tbUserMenu WHERE userID = @sUserID 

	INSERT INTO dbo.tbUserMenu (MenuId, UserID)
	SELECT Id,@sUserID FROM tbMenu

END
ELSE

INSERT INTO dbo.tbUserMenu (MenuId, UserID)
SELECT Id,@sUserID FROM tbMenu

GO
