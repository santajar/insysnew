﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,, 8 mei 2014>
-- Description:	<Description,, Union all linked table in tblistTIDRemotedownload>
-- =============================================
CREATE PROCEDURE [dbo].[spRDBrowseAllColoumLinkListTID]
	@sCondition VARCHAR(4000) = NULL
AS
BEGIN
DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery = 'SELECT A.*,B.GroupRDName,C.RegionRDName,D.CityRDName,E.ScheduleTime,F.AppPackageName,G.DatabaseID
					FROM (
						(SELECT *
						FROM tbListTIDRemoteDownload WITH (NOLOCK)) A
						LEFT JOIN
						(SELECT *
						FROM tbGroupRemoteDownload WITH (NOLOCK)) B
						ON A.IdGroupRD= B.IdGroupRD
						LEFT JOIN
						(SELECT * 
						FROM tbRegionRemoteDownload WITH (NOLOCK)) C
						ON A.IdRegionRD = C.IdRegionRD
						LEFT JOIN
						(SELECT *
						FROM tbCityRemoteDownload WITH (NOLOCK)) D
						ON A.IdCityRD = D.IdCityRD
						LEFT JOIN
						(SELECT * 
						FROM tbScheduleSoftware WITH (NOLOCK)) E
						ON A.ScheduleID = E.ScheduleID
						LEFT JOIN
						(SELECT *
						FROM tbAppPackageEDCList WITH (NOLOCK)) F
						ON A.AppPackID = F.AppPackId
						LEFT JOIN
						(SELECT *
						FROM tbProfileTerminalList WITH (NOLOCK)) G
						ON A.TerminalID = G.TerminalID
						) '
	iF @sCondition IS NULL
		EXEC (@sQuery)
	ELSE
		EXEC (@sQuery + @sCondition)
END