﻿CREATE PROCEDURE [dbo].[spProfileSNBrowse]
	@sCondition VARCHAR(100) = NULL
AS

DECLARE @sStmt VARCHAR(MAX)
SET @sStmt = 'SELECT SN_Name
		, SN_Tag Tag
		, SN_TagValue Value
	FROM tbProfileSN WITH (NOLOCK) '
SET @sStmt = @sStmt + ISNULL(@sCondition,'') + ' ORDER BY SN_Tag'
EXEC (@sStmt)
