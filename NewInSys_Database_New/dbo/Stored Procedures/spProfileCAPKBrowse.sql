﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 3, 2010
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the content of CAPK table
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCAPKBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
DECLARE @sQuery VARCHAR(MAX)
SET @sQuery='
SELECT DatabaseId,
	CAPKIndex,
	CAPKTag as Tag,
	CAPKTagLength,
	CAPKTagValue as Value
FROM tbProfileCAPK WITH (NOLOCK) '
EXEC (@sQuery + @sCondition)
