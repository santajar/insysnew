﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Sept 30, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Inserting profile initialization log 
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnInsertProcess]
@sTerminalID CHAR(8)
AS
	EXEC spInitLogConnInsert @sTerminalID
	EXEC spInitLogConnDetailInsertProcess @sTerminalID