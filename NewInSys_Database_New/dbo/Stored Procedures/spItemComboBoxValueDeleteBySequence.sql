﻿
-- =======================================================================
-- Description:	Delete data based on DatabaseID, FormID, and ItemSequence
-- =======================================================================

CREATE PROCEDURE [dbo].[spItemComboBoxValueDeleteBySequence]
	@sDatabaseID SMALLINT,
	@sFormID SMALLINT,
	@iItemSequence SMALLINT

AS
	DELETE FROM tbItemComboBoxValue
	WHERE DatabaseID = @sDatabaseID AND 
		  FormID = @sFormID  AND
		  ItemSequence = @iItemSequence

