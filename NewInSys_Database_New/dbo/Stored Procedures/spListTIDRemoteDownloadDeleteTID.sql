﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spListTIDRemoteDownloadDeleteTID] 
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

    Delete 
	FROM tbListTIDRemoteDownload 
	WHERE TerminalID = @sTerminalID

END