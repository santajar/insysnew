﻿CREATE PROCEDURE [dbo].[spUserAccessReset]
AS
DELETE FROM tbUserAccess
WHERE TerminalID IN (
	SELECT tbUserAccess.TerminalID
	FROM tbUserAccess WITH (NOLOCK) JOIN tbProfileTerminalList WITH (NOLOCK)
		ON tbUserAccess.TerminalID = tbProfileTerminalList.TerminalID
	WHERE
		DATEDIFF(MINUTE, tbProfileTerminalList.LastView, GETDATE()) > 10
)