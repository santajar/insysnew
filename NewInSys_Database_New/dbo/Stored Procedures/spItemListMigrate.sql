﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 28, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Start migrating process of Item list on each Old database into NewInSys database
-- =============================================
CREATE PROCEDURE [dbo].[spItemListMigrate]
	@sDatabaseId VARCHAR(3)
AS
SET NOCOUNT ON
--DECLARE @sDatabaseId VARCHAR(3)
--SET @sDatabaseId='069'

DECLARE @sDatabaseName VARCHAR(MAX)
SET @sDatabaseName=dbo.sBrDatabaseNameById(@sDatabaseId)
	print(@sDatabaseName)

IF OBJECT_ID('[' + @sDatabaseName + ']..Item') IS NOT NULL
BEGIN
	DECLARE @SqlStmt NVARCHAR(MAX)
	SET @SqlStmt= 'SELECT ItemId,
		ItemName,
		FormId,
		ObjectId,
		DefaultValue,
		vIsNothing,
		vIsNumeric,
		vIsAlNum,
		vMinLenght,
		vMaxLenght,
		vMinValue,
		vMaxValue,
		vMustLenght,
		ValidationMsg,
		Tag
	FROM [' + @sDatabaseName + ']..Item ORDER BY FormId,ItemId'
	IF OBJECT_ID('tempdb..#tbitemlist') IS NOT NULL
		DROP TABLE #tbitemlist
	CREATE TABLE #tbitemlist(
		Id INT IDENTITY(1,1),
		ItemId INT,
		ItemName VARCHAR(30),
		FormId INT,
		ObjectId INT,
		DefaultValue VARCHAR(100),
		vIsNothing BIT,
		vIsNumeric INT,
		vIsAlNum INT,
		vMinLength INT,
		vMaxLength INT,
		vMinValue INT,
		vMaxValue INT,
		vMustLength INT,
		ValidationMsg VARCHAR(50),
		Tag VARCHAR(5)
	)
	INSERT INTO #tbitemlist
	EXEC sp_executesql @sqlstmt

	DECLARE @iRowCount INT, @iRowCurr INT
	DECLARE @vAllowNull VARCHAR(1)
	DECLARE @vType VARCHAR(1)
	DECLARE @vIsNothing INT
	DECLARE @vIsNumeric INT
	DECLARE @vIsAlNum INT
	DECLARE @sLengthFormat VARCHAR(2)
	DECLARE @sTag VARCHAR(5)
	DECLARE @vMustLength INT
	SELECT @iRowCount=COUNT(*) FROM #tbitemlist
	SET @iRowCurr=1
	INSERT INTO tbItemList(DatabaseID,
			FormId,
			ItemSequence,
			ItemName,
			ObjectId,
			DefaultValue,
			LengthofTagValueLength,
			vAllowNull,
			vUpperCase,
			vType,
			vMinLength,
			vMaxLength,
			vMinValue,
			vMaxValue,
			ValidationMsg,
			Tag)
	SELECT @sDatabaseId DatabaseID,
			FormId,
			ItemId ItemSquence,
			ItemName,
			ObjectId,
			DefaultValue,
			dbo.sLengthFormat(Tag) LengthofTagValueLength,
			dbo.sAllowNull(vIsNothing) vIsAllowNull,
			1 vUpperCase,
			dbo.sType(Tag,ObjectId,vIsNumeric,vIsAlNum) vType,
			vMinLength,
			vMaxLength,
			vMinValue,
			vMaxValue,
			ValidationMsg,
			Tag
	FROM #tbitemlist

	INSERT INTO tbItemList(DatabaseID,
			FormId,
			ItemSequence,
			ItemName,
			ObjectId,
			DefaultValue,
			LengthofTagValueLength,
			vAllowNull,
			vUpperCase,
			vType,
			vMinLength,
			vMaxLength,
			vMinValue,
			vMaxValue,
			ValidationMsg,
			Tag)
	VALUES(@sDatabaseId,
			5,
			1,
			'Card Name',
			'1',
			NULL,
			2,
			0,
			1,
			'S',
			0,
			20,
			0,
			0,
			'No Information',
			'AD01'
	)
	INSERT INTO tbItemList(DatabaseID,
			FormId,
			ItemSequence,
			ItemName,
			ObjectId,
			DefaultValue,
			LengthofTagValueLength,
			vAllowNull,
			vUpperCase,
			vType,
			vMinLength,
			vMaxLength,
			vMinValue,
			vMaxValue,
			ValidationMsg,
			Tag)
	VALUES(@sDatabaseId,
			5,
			2,
			'Issuer Name',
			1,
			NULL,
			2,
			0,
			1,
			'S',
			0,
			20,
			0,
			0,
			'No Information',
			'AD02'
	)
	INSERT INTO tbItemList(DatabaseID,
			FormId,
			ItemSequence,
			ItemName,
			ObjectId,
			DefaultValue,
			LengthofTagValueLength,
			vAllowNull,
			vUpperCase,
			vType,
			vMinLength,
			vMaxLength,
			vMinValue,
			vMaxValue,
			ValidationMsg,
			Tag)
	VALUES(@sDatabaseId,
			5,
			3,
			'Acquirer Name',
			1,
			NULL,
			2,
			0,
			1,
			'S',
			0,
			20,
			0,
			0,
			'No Information',
			'AD03'
	)
END