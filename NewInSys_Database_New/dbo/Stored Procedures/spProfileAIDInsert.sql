﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 2, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Insert new AID into tbProfileAID
--				2. Modify to use form flexible`
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAIDInsert]
	@iDatabaseID INT,
	@sAIDName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfileAID WITH (NOLOCK)
WHERE AIDTag IN ('AI01','AI001') AND
		AIDTagValue = @sAIDName AND
		DatabaseID = @iDatabaseID

IF @iCountID =0
BEGIN
	IF (dbo.isTagLength4(@iDatabaseID)= 4)
	BEGIN
		INSERT INTO tbProfileAID(
			DatabaseId,
			[AIDName],
			[AIDTag],
			[AIDLengthOfTagLength],
			[AIDTagLength],
			[AIDTagValue]
			)
		SELECT 
			@iDatabaseID [DatabaseId],
			[Name] [AIDName],
			Tag [AIDTag],
			LEN(TagLength) [AIDLengthOfTagLength],
			TagLength [AIDTagLength],
			TagValue [AIDTagValue]

		FROM dbo.fn_TbProfileTLV(@sAIDName,@sContent)
	END
	ELSE
	BEGIN
		INSERT INTO tbProfileAID(
			DatabaseId,
			[AIDName],
			[AIDTag],
			[AIDLengthOfTagLength],
			[AIDTagLength],
			[AIDTagValue]
			)
		SELECT 
			@iDatabaseID [DatabaseId],
			[Name] [AIDName],
			Tag [AIDTag],
			LEN(TagLength) [AIDLengthOfTagLength],
			TagLength [AIDTagLength],
			TagValue [AIDTagValue]

		FROM dbo.fn_TbProfileTLV5(@sAIDName,@sContent)
	END
END

UPDATE tbProfileTerminalList
SET EMVInit = '1'
WHERE DatabaseID = @iDatabaseID
