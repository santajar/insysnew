﻿-- =============================================
-- Author:		<Author,,Tedie>
-- Create date: <Create Date,, 12 AUG 2015>
-- Description:	<Description,,Check New Password with Old Password>
-- =============================================
Create PROCEDURE [dbo].[spCheckHistoryPassword]
	@sUserID VARCHAR(50),
	@sEncrypPassword VARCHAR(500),
	@bReturn BIT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @iCount INT

	SELECT @iCount=COUNT(*)
	FROM tbUserPasswordHistory WITH (NOLOCK)
	WHERE UserID = @sUserID 
		AND [Password]= @sEncrypPassword
   
   IF @iCount>0
	SET @bReturn = 1
   ELSE
	SET @bReturn = 0


END