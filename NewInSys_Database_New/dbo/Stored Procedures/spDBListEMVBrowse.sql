﻿-- =============================================  
-- Author:  TEDIE Scorfia  
-- Create date: March 2, 2015
-- Description: view DB list with EMVInit
-- =============================================  
CREATE PROCEDURE [dbo].[spDBListEMVBrowse]  
AS
SELECT DatabaseID, DatabaseName from tbProfileTerminalDB WITH (NOLOCK)
	WHERE EMVInit = 1
	ORDER BY DatabaseName