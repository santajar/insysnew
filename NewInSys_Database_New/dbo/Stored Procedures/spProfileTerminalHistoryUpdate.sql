﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Modify date: 
-- Description:	
--				1. Update The History Terminal table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalHistoryUpdate]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	EXEC spProfileTerminalHistoryDelete @sTerminalID
	EXEC spProfileTerminalHistoryInsert @sTerminalID
END