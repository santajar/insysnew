﻿-- ===========================================
-- Description:	Insert new tag to Database
-- ===========================================

CREATE PROCEDURE [dbo].[spItemListInsert]
	@sDatabaseID SMALLINT,
	@sFormID SMALLINT,
	@iItemSequence SMALLINT,
	@sOldItemSequence SMALLINT = NULL,
	@sTag VARCHAR(5),
	@sTagName VARCHAR(24),
	@sOldObjectID SMALLINT = NULL,
	@sObjectID SMALLINT,
	@sDefaultValue TEXT = NULL,
	@iLengthofTagValueLength INT = 2,
	@vAllowNull BIT,
	@vUpperCase BIT,
	@vType VARCHAR(5),
	@vMinLength INT,
	@vMaxLength INT,
	@vMinValue INT,
	@vMaxValue INT,
	@vMasking BIT = 0,
	@sValidationMsg VARCHAR(50)
AS
	--CREATE TABLE #tbItemList
	--(
	--	ColumnName VARCHAR(50),
	--	ColumnValue VARCHAR(500)
	--)
	
	--INSERT INTO #tbItemList(ColumnName,ColumnValue)
	--EXEC sp_tbItemList @sDatabaseID, @sTag
	
	DECLARE @iCount INT
	SELECT @iCount = COUNT(*)
	FROM tbItemList WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseID AND 
		  FormID = @sFormID AND
		  ItemSequence = @iItemSequence

	IF (@iCount = 1)
		UPDATE tbItemList SET ItemSequence = ItemSequence + 1
		WHERE DatabaseID = @sDatabaseID AND 
			  FormID = @sFormID AND
			  ItemSequence >= @iItemSequence

	INSERT INTO tbItemList (DatabaseID,
							FormID,
							ItemSequence,
							ItemName,
							ObjectID,
							DefaultValue,
							LengthOfTagValueLength,
							vAllowNull,
							vUpperCase,
							vType,
							vMinLength,
							vMaxLength,
							vMinValue,
							vMaxValue,
							vMasking,
							ValidationMsg,
							Tag
							)
	VALUES (@sDatabaseID,
			@sFormID,
			@iItemSequence,
			@sTagName,
			@sObjectID,
			@sDefaultValue,
			@iLengthofTagValueLength,
			@vAllowNull,
			@vUpperCase,
			@vType,
			@vMinLength,
			@vMaxLength,
			@vMinValue,
			@vMaxValue,
			@vMasking,
			@sValidationMsg,
			@sTag
			)


	SELECT ColumnName, ColumnValue
	FROM dbo.fn_tbItemList (@sDatabaseID, @sTag)