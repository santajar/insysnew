﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,,16 April 2014>
-- Description:	<Description,,SP Insert Region Remote Download>
-- =============================================
CREATE PROCEDURE [dbo].[spRDInsertRegion]
	@sRegion VARCHAR(150)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @iCount INT
	
	SELECT @iCount = COUNT(*)
	FROM tbRegionRemoteDownload WITH (NOLOCK)
	WHERE RegionRDName = @sRegion

	IF @iCount= 0
	BEGIN
		INSERT tbRegionRemoteDownload(RegionRDName)
		VALUES (@sRegion)
	END
	
END