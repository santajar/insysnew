﻿-- Description : Insert new terminal into database (tbProfileTerminal and tbProfileTerminalList)

CREATE PROCEDURE [dbo].[spProfileIssuerInsert]
	@sTerminalID VARCHAR(8),
	@sAcquirerName VARCHAR(50),
	@sContent VARCHAR(1000)
AS

EXEC spProfileAcquirerHistoryInsert @sTerminalID,@sAcquirerName

IF Object_ID ('TempDB..#TempIss') IS NOT NULL
		DROP TABLE #TempIss
	CREATE TABLE #TempIss
	(
		TerminalID VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		LenghtOfTagLength INT,
		TagLength INT,
		TagValue VARCHAR(150)
	)

Declare @iDBID INT
	SELECT @iDBID=DatabaseID 
	FROM tbProfileTerminalList WITH (NOLOCK) 
	WHERE TerminalID = @sTerminalID

IF (dbo.isTagLength4(@iDBID)=5)
BEGIN
print'5'
	INSERT INTO #TempIss(TerminalID,[Name],Tag,LenghtOfTagLength,TagLength,TagValue)
		SELECT TerminalID,
		[Name],
		Tag,
		LEN(TagLength) AS LenghtOfTagLength,
		TagLength,
		TagValue
	FROM dbo.fn_TbProfileTLV5(@sTerminalID,@sContent)
END
ELSE
BEGIN
print'4'
	INSERT INTO #TempIss(TerminalID,[Name],Tag,LenghtOfTagLength,TagLength,TagValue)
		SELECT TerminalID,
		[Name],
		Tag,
		LEN(TagLength) AS LenghtOfTagLength,
		TagLength,
		TagValue
	FROM dbo.fn_TbProfileTLV(@sTerminalID,@sContent)
END
	--SELECT TerminalID,
	--	[Name],
	--	Tag,
	--	LEN(TagLength) AS LenghtOfTagLength,
	--	TagLength,
	--	TagValue
	--INTO #TempIss
	--FROM dbo.fn_TbProfileTLV(@sTerminalID,@sContent)

	DECLARE @sIssName VARCHAR(50)

	SELECT DISTINCT @sIssName = [Name] FROM #TempIss

	INSERT INTO tbProfileIssuer(TerminalID,
		IssuerName,
		IssuerTag, 
		IssuerLengthOfTagLength, 
		IssuerTagLength, 
		IssuerTagValue)
	SELECT TerminalID,
		[Name],
		Tag,
		LEN(TagLength),
		TagLength,
		TagValue
	FROM #TempIss

	--Add Relation
	IF (dbo.isTagLength4(@iDBID)=5)
	BEGIN
		EXEC spProfileRelationInsertProcess @sTerminalID,@sAcquirerName, @sIssName, NULL, 'AD002'
	END
	ELSE
	BEGIN
		EXEC spProfileRelationInsertProcess @sTerminalID,@sAcquirerName, @sIssName, NULL, 'AD02'
	END


DROP TABLE  #TempIss

EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

DECLARE @sDbID VARCHAR(5)
SET @sDbID = dbo.iDatabaseIdByTerminalId(@sTerminalID)

EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'AE', @sIssName, @sContent, 0


