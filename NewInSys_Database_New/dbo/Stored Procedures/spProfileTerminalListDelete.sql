﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 24, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalListDelete]
	@sTerminalID VARCHAR(8)
AS
	DELETE FROM tbProfileTerminalList 
		WHERE TerminalID=@sTerminalID
