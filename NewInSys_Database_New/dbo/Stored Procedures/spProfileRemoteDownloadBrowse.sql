﻿

CREATE PROCEDURE [dbo].[spProfileRemoteDownloadBrowse]
	@sCondition VARCHAR(100) = NULL
AS
	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt = 'SELECT RemoteDownloadTagID,
						RemoteDownloadName, 
						DatabaseID,
						RemoteDownloadTag as Tag,
						RemoteDownloadLengthOfTagLength,
						RemoteDownloadTagLength,
						RemoteDownloadTagValue as Value,
						[Description]
				FROM tbProfileRemoteDownload  WITH (NOLOCK) '
	SET @sStmt = @sStmt + ISNULL(@sCondition,'') + ' ORDER BY RemoteDownloadTagID, RemoteDownloadTag'
	EXEC (@sStmt)