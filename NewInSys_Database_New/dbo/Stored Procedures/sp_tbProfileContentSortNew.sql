USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[sp_tbProfileContentSortNew]    Script Date: 11/2/2017 11:56:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_tbProfileContentSortNew]
 @sTerminalID VARCHAR(8), 
 @sConditions VARCHAR(MAX)
  
as 
BEGIN
IF OBJECT_ID('TEMPDB..#tempTerminal') IS NOT NULL
	DROP TABLE #tempTerminal

create Table #tempTerminal(
		ID INT IDENTITY,
		TerminalId VARCHAR(8),
		[Name] VARCHAR(50),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(500)
	)


DECLARE @sDatabaseID VARCHAR(4)
SELECT @sDatabaseID=DatabaseID 
FROM tbProfileTerminalList WITH(NOLOCK)
WHERE TerminalID=@sTerminalID

Declare @iDbID varchar (4),
		@cols nvarchar(MAX),
		@cols1 nvarchar(MAX),
		@cols2 nvarchar(MAX),
		@cols3 nvarchar(MAX),
		@query nvarchar(max),
		@query1 nvarchar(max),
		@query2 nvarchar(max),
		@query3 nvarchar(max)

set @iDbID=(select DatabaseID 
			from tbProfileTerminalList 
			where TerminalID=@sTerminalId)

SET @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=1 and DatabaseID=@iDbID and Tag like'DE%'
                    group by tag,ItemID
                    order by tag
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

SET @cols1 =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=3 and DatabaseID=@iDbID 
                    group by tag,ItemID
                    order by tag
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

SET @cols2 =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=2 and DatabaseID=@iDbID
                    group by tag,ItemID
                    order by tag
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

SET @cols3 =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=1 and DatabaseID=@iDbID and Tag like 'DC%'
                    group by tag,ItemID
                    order by tag
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

IF OBJECT_ID('TEMPDB..#tbItemList') IS NOT NULL
	DROP TABLE #tbItemList

	
create Table #tbItemList (
	[ItemID] [int] NOT NULL,
	[DatabaseID] [smallint] NOT NULL,
	[FormID] [smallint] NOT NULL,
	[ItemSequence] [smallint] NOT NULL,
	[ItemName] [varchar](24) NOT NULL,
	[ObjectID] [smallint] NOT NULL,
	[DefaultValue] [text] NULL,
	[LengthofTagValueLength] [int] NOT NULL,
	[vAllowNull] [bit] NOT NULL,
	[vUpperCase] [bit] NOT NULL,
	[vType] [varchar](5) NULL,
	[vMinLength] [smallint] NOT NULL,
	[vMaxLength] [smallint] NOT NULL,
	[vMinValue] [int] NULL,
	[vMaxValue] [int] NULL,
	[vMasking] [bit] NOT NULL,
	[ValidationMsg] [varchar](50) NULL,
	[Tag] [varchar](5) NULL
)

INSERT INTO #tbItemList
SELECT * 
FROM tbItemList WITH(NOLOCK) WHERE DatabaseID=@sDatabaseID

--start Terminal DE
set @query='
INSERT INTO #tempTerminal (TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
select u.TerminalID,u.TerminalID[Name],u.[Tag],il.ItemName,len(u.[TagValue])[TagLength],u.[TagValue]
from tbProfileTerminal s
	unpivot
	(
		 [TagValue] 
		For Tag in ('+ @cols +')
	) as u
	join tbProfileTerminalList tl
	on u.TerminalID=tl.TerminalID
	join tbItemList il
	on tl.DatabaseID=il.DatabaseID
where u.TerminalID='''+ @sTerminalId +''' and u.Tag=il.Tag and u.Tag like ''DE%''

union

select tl2.TerminalID,tl2.TerminalID[Name],m.Tag,il2.ItemName,len(m.TagValue)[TagLength],m.TagValue
from MasterProfile m
	join tbItemList il2
	on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
	join tbProfileTerminalList tl2 
	on tl2.DatabaseID=il2.DatabaseID 
where tl2.TerminalID='''+ @sTerminalId +''' and m.FormID=1 and m.Tag like ''DE%''
	and m.Tag not in ( select u.[Tag]
	from tbProfileTerminal s
		unpivot
		(
			[TagValue] 
			For Tag in ('+ @cols +')
		) as u 
	where u.TerminalID='''+ @sTerminalId +''') 
order by Tag'
------ end terminal DE
--print (@query)
exec (@query)
--start terminal DC
set @query1='
INSERT INTO #tempTerminal(TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
select u.TerminalID,u.TerminalID[Name],u.[Tag],il.ItemName,len(u.[TagValue])[TagLength],u.[TagValue]
from tbProfileTerminal s
	unpivot
	(
		 [TagValue] 
		For Tag in ('+ @cols3 +')
	) as u
	join tbProfileTerminalList tl
	on u.TerminalID=tl.TerminalID
	join tbItemList il
	on tl.DatabaseID=il.DatabaseID
where u.TerminalID='''+ @sTerminalId +''' and u.Tag=il.Tag and u.Tag like ''DC%''

union

select tl2.TerminalID,tl2.TerminalID[Name],m.Tag,il2.ItemName,len(m.TagValue)[TagLength],m.TagValue
from MasterProfile m
	join tbItemList il2
	on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
	join tbProfileTerminalList tl2 
	on tl2.DatabaseID=il2.DatabaseID 
where tl2.TerminalID='''+ @sTerminalId +''' and m.FormID=1  and m.Tag like ''DC%'' 
	and m.Tag not in ( select u.[Tag]
	from tbProfileTerminal s
		unpivot
		(
			[TagValue] 
			For Tag in ('+ @cols3 +')
		) as u 
	where u.TerminalID='''+ @sTerminalId +''') 
order by Tag'
--- end of Terminal DC
--print (@query1)
exec (@query1)

IF OBJECT_ID('TEMPDB..#tbProfileRelation') IS NOT NULL
	DROP TABLE #tbProfileRelation

create Table #tbProfileRelation(
	[RelationTagID] BIGINT ,
	[TerminalID] VARCHAR(8) NOT NULL,
	[RelationTag] VARCHAR(5) NOT NULL,
	[RelationTagLength] INT NOT NULL,
	[RelationTagValue] VARCHAR(50) NULL,
	[Description] VARCHAR(50) NULL
	)
INSERT INTO #tbProfileRelation(RelationTagID,TerminalID,RelationTag,RelationTagLength,RelationTagValue )
--start Realtion
select u.ID,u.TerminalID,u.Tag,len(TagValue)[TagLength],TagValue from 
(
	select ID,TerminalID,TagCard[Tag],ValueCard[TagValue] 
	from tbProfileRelation 

	union all
	select ID,TerminalID,TagIssuer[Tag],ValueIssuer[TagValue]
	from tbProfileRelation 
	
	union all
	select ID,TerminalID,TagAcquirer[Tag],ValueAcquirer[TagValue]
	from tbProfileRelation 	
)u
where u.TerminalID=@sTerminalID
order by u.ID
--end relation

DECLARE @tbProfileCardList TABLE(
	[CardID] INT,
	[DatabaseID] INT NOT NULL,
	[CardName] VARCHAR(50) NOT NULL)
INSERT INTO @tbProfileCardList
SELECT * FROM tbProfileCardList WITH(NOLOCK)
WHERE DatabaseID=@sDatabaseID

INSERT INTO #tempTerminal(TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
SELECT     
	@sTerminalID,
	CL.CardName AS Name, 
	tbProfileCard.CardTag Tag, 
	I.ItemName, 
    tbProfileCard.CardTagLength TagLength, 
    tbProfileCard.CardTagValue TagValue
FROM
	@tbProfileCardList CL INNER JOIN
	tbProfileCard WITH(NOLOCK) ON CL.CardID = tbProfileCard.CardID INNER JOIN
	(SELECT * FROM #tbItemList WHERE FormID=4) I ON tbProfileCard.CardTag = I.Tag
WHERE 
	tbProfileCard.CardID IN (
	SELECT CardID 
	FROM @tbProfileCardList
	WHERE CardName IN ( 
			SELECT RelationTagValue FROM #tbProfileRelation
			WHERE RelationTag IN ('AD01','AD001') AND ISNULL(RelationTagValue,'')<>''
		)
)
ORDER BY Name, Tag
--start issuer
set @query2='
INSERT INTO #tempTerminal(TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
--select Issuer (Issuer union MasterProfile)
select u.[TerminalID],u.[IssuerName][Name],u.[tag],il.ItemName,len(u.[TagValue])[TagLength],u.[TagValue]
from tbProfileIssuer a
unpivot
	(
		[TagValue]
		For Tag in ('+ @cols1 +')
	) as u
	join tbProfileTerminalList tl
	on u.TerminalID=tl.TerminalID
	join tbItemList il
	on tl.DatabaseID=il.DatabaseID
where u.TerminalID='''+ @sTerminalId +''' and u.Tag=il.Tag

union
--From table MasterProfile
select tl2.TerminalID,a.IssuerName[Name],m.Tag,il2.ItemName,len(m.TagValue)[TagLength],m.TagValue
from MasterProfile m
	join tbItemList il2
	on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
	join tbProfileTerminalList tl2 
	on tl2.DatabaseID=il2.DatabaseID
	join tbProfileIssuer a
	on tl2.TerminalID=a.TerminalID and a.IssuerName=m.Name
where tl2.TerminalID='''+ @sTerminalId +''' and m.FormID=3
--compare tag masterProfile vs tbProfileIssuerNew_2  
	and (select concat(a.IssuerName,''-'',m.Tag)) not in ( select concat (u.[IssuerName],''-'',u.[Tag])
	from tbProfileIssuer a
	unpivot
	(
		[TagValue]
		For Tag in ('+ @cols1 +')
	) as u
where u.TerminalID='''+ @sTerminalId +''') 
order by Name,Tag'

exec (@query2)
--end issuer
--start Acquirer
set @query3='
INSERT INTO #tempTerminal(TerminalId,[Name],Tag,ItemName,TagLength,TagValue)
select u.[TerminalID],u.[AcquirerName][Name],u.[tag],il.ItemName,len(u.[TagValue])[TagLength],u.[TagValue]
from tbProfileAcquirer a
unpivot
	(
		[TagValue]
		For Tag in ('+ @cols2 +')
	) as u
	join tbProfileTerminalList tl
	on u.TerminalID=tl.TerminalID
	join tbItemList il
	on tl.DatabaseID=il.DatabaseID
where u.TerminalID='''+ @sTerminalId +''' and u.Tag=il.Tag

union
--From table MasterProfile
select tl2.TerminalID,a.AcquirerName[Name],m.Tag,il2.ItemName,len(m.TagValue)[TagLength],m.TagValue
from MasterProfile m
	join tbItemList il2
	on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
	join tbProfileTerminalList tl2 
	on tl2.DatabaseID=il2.DatabaseID
	join tbProfileAcquirer a
	on tl2.TerminalID=a.TerminalID and a.AcquirerName=m.Name
where tl2.TerminalID='''+ @sTerminalId +''' and m.FormID=2 
--compare tag masterProfile vs tbProfileAcquirerNew_2 
	and (select concat(a.AcquirerName,''-'',m.Tag)) not in ( select concat (u.[AcquirerName],''-'',u.[Tag])
	from tbProfileAcquirer a
	unpivot
	(
		[TagValue]
		For Tag in ('+ @cols2 +')
	) as u
where u.TerminalID='''+ @sTerminalId +''') 
order by Name,Tag'
--print (@query3)
exec (@query3)
--end Acquirer

IF OBJECT_ID('TEMPDB..#tempRelation') IS NOT NULL
	DROP TABLE #tempRelation

create Table #tempRelation(
		Id INT IDENTITY,
		TerminalId VARCHAR(8),
		Tag VARCHAR(5),
		TagLength VARCHAR(3),
		TagValue VARCHAR(500),
		TagId BIGINT
	)

INSERT INTO #tempRelation(TerminalId,Tag,TagLength,TagValue,TagId)
SELECT 
	R.TerminalID, 
	R.RelationTag AS Tag, 
	R.RelationTagLength AS TagLength, 
	R.RelationTagValue AS TagValue, 
	R.RelationTagID
FROM
	(SELECT * FROM #tbItemList WHERE FormID=5) I INNER JOIN
	(SELECT * FROM #tbProfileRelation) R ON I.Tag = R.RelationTag
ORDER BY R.RelationTagID

INSERT INTO #tempTerminal(TerminalId,Tag,TagLength,TagValue)
SELECT 
	TerminalID, 
	Tag,
	TagLength,
	TagValue
FROM #tempRelation
WHERE ISNULL(TagID,'')<>'' AND TagID NOT IN
        (
			SELECT TagID
			FROM    
			(
				SELECT TagID
				FROM #tempRelation
				WHERE TagValue IS NULL AND Tag IN ('AD03','AD003')
				UNION ALL
				SELECT TagID - 1
				FROM #tempRelation
				WHERE (TagValue IS NULL) AND Tag IN ('AD03','AD003')
				UNION ALL
				SELECT TagID - 2
				FROM #tempRelation
				WHERE (TagValue IS NULL) AND Tag IN ('AD03','AD003')
			) viewRelationNull
		)
--RETURN
select TerminalId,Name,Tag,ItemName,TagLength,TagValue from #tempTerminal
order by ID
END


GO


