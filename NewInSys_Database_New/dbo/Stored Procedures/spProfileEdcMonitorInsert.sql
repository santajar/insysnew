﻿-- =============================================
-- Author		: Tobias SETYO
-- Create date	: March 8, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spProfileEdcMonitorInsert]
	@iDatabaseID INT,
	@sEdcMonitorName VARCHAR(15),
	@sContent VARCHAR(MAX)
AS
DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfileEdcMonitor WITH (NOLOCK)
WHERE Tag IN ('MT01','MT001') AND
		TagValue = @sEdcMonitorName AND
		DatabaseID = @iDatabaseID

IF (dbo.isTagLength4(@iDatabaseID) = 4)
BEGIN
	INSERT INTO tbProfileEdcMonitor(
		DatabaseId,
		Name,
		Tag,
		TagLength,
		TagValue
		)
	SELECT 
		@iDatabaseID [DatabaseId],
		[Name],
		Tag,
		TagLength,
		TagValue
	FROM dbo.fn_tbProfileTLV(@sEdcMonitorName,@sContent)
END
ELSE
BEGIN
print '2'
	INSERT INTO tbProfileEdcMonitor(
		DatabaseId,
		Name,
		Tag,
		TagLength,
		TagValue
		)
	SELECT 
		@iDatabaseID [DatabaseId],
		[Name],
		Tag,
		TagLength,
		TagValue
	FROM dbo.fn_tbProfileTLV5(@sEdcMonitorName,@sContent)
END
EXEC spItemComboBoxValueInsertEdcMonitoring @sEdcMonitorName, @iDatabaseID