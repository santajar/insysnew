﻿CREATE PROCEDURE [dbo].[spRD_GroupInsert]
	@sGroupName VARCHAR(50),
	@iID INT OUTPUT
AS
DECLARE @IdentityOutput table ( ID int )
INSERT INTO tbRD_Group([GROUP_NAME])
OUTPUT inserted.GROUP_ID INTO @IdentityOutput
VALUES(@sGroupName)

SELECT @iID=ID FROM @IdentityOutput