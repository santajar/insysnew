﻿
-- ==================================
-- Description: Delete Location Data 
-- ==================================

CREATE PROCEDURE [dbo].[spLocationDelete]	
	@sLocationID VARCHAR (15)
AS

DELETE FROM tbLocation 
WHERE LocationId = @sLocationID


	
