﻿
-- ========================================================================
-- Description:	Update FlagValue for ItemName contains paths
--				ItemName updated : AutoInitReportPath, and InitCompressPath
-- ========================================================================

CREATE PROCEDURE [dbo].[spControlFlagUpdatePaths]
	@sAutoInitItemName VARCHAR(50) = '',
	@sAutoInitFlag VARCHAR(150) = '',
	@sInitCompressItemName VARCHAR(50) = '',
	@sInitCompressFlag VARCHAR(150) = ''
AS
	SET NOCOUNT ON
	EXEC spControlFlagUpdate @sAutoInitItemName, @sAutoInitFlag
	EXEC spControlFlagUpdate @sInitCompressItemName, @sInitCompressFlag



