﻿


CREATE PROCEDURE [dbo].[spItemComboBoxValueInsertGPRS] 
	@sGPRSName VARCHAR(15),
	@sDatabaseId INT
AS
DECLARE @iCountGPRS INT=0

SELECT @iCountGPRS = COUNT (*)
FROM tbItemComboBoxValue WITH (NOLOCK)
WHERE DatabaseID = @sDatabaseId
	AND RealValue = @sGPRSName

IF @iCountGPRS = 0
BEGIN
	SELECT * 
	INTO #TempItemlist
	FROM tbItemList WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId
		AND ItemName IN ('Txn GPRS Primary',
						 'Txn GPRS Secondary',
						 'Stl GPRS Primary',
						 'Stl GPRS Secondary',
						 'Primary Txn Eth',
						 'Secondary Txn Eth',
						 'Primary Stl Eth',
						 'Secondary Stl Eth',
						 'Txn IP Primary',
						 'Txn IP Secondary',
						 'Stl IP Primary',
						 'Stl IP Secondary',
						 'Txn GPRS1 Primary',
						 'Txn GPRS1 Secondary',
						 'Stl GPRS1 Primary',
						 'Stl GPRS1 Secondary',
						 'Txn GPRS2 Primary',
						 'Txn GPRS2 Secondary',
						 'Stl GPRS2 Primary',
						 'Stl GPRS2 Secondary')

INSERT INTO tbItemComboBoxValue(DatabaseId,
			FormId,
			ItemSequence,
			DisplayValue,
			RealValue)
SELECT DatabaseID [DatabaseId], 
	FormID [FormID],
	ItemSequence [ItemSequence],
	@sGPRSName [DisplayValue],
	@sGPRSName [RealValue]
FROM #TempItemlist

END
ELSE
 PRINT 'GPRS Name Already Exist'