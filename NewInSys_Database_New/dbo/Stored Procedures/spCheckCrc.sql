﻿
-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Jan 25, 2017
-- Modify date:
--				1. 
-- Description:	Check App Crc
-- =============================================

CREATE PROCEDURE [dbo].[spCheckCrc]
@sAppName varchar(30)
AS
--BEGIN
DECLARE @sLinkDatabase NVARCHAR(50)
DECLARE @sQuery NVARCHAR(MAX)
DECLARE @bResult bit
	
	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'
	SET @sQuery  = 'SELECT CustomRDBit61 FROM '+ @sLinkDatabase + 'tbBitMap WHERE ApplicationName= ' + '''' + @sAppName +''''
	
	EXECUTE sp_executesql @sQuery