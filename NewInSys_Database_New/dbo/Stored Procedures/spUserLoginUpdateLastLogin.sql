﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUserLoginUpdateLastLogin]
	@sCondition VARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sSqlstmt VARCHAR(500)
	SET @sSqlstmt =
				'UPDATE tbUserLogin
				SET LastLogin = GETDATE() 
				 '
	EXEC(@sSqlstmt + @sCondition)
END