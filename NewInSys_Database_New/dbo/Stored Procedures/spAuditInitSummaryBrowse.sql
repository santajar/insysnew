﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Sept 23, 2010
-- Modify	  : 27 jul 2012 by chris
--				1. change sKey to varhar 50
-- Description:	
--				1. Viewing the Initialization Trail
-- =============================================
CREATE PROCEDURE [dbo].[spAuditInitSummaryBrowse]
	@sKey VARCHAR(2000) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 10000
		TID,
		Software,
		LastInit,
		InitTimes,
		SuccessTimes,
		FailedInit
	FROM tbAuditInitSummary WITH (NOLOCK)
	WHERE
		TID LIKE '%' + @sKey + '%' OR
		Software LIKE '%' + @sKey + '%' OR
		LastInit LIKE '%' + @sKey + '%' OR
		InitTimes LIKE '%' + @sKey + '%' OR
		SuccessTimes LIKE '%' + @sKey + '%' OR
		FailedInit LIKE '%' + @sKey + '%'
END