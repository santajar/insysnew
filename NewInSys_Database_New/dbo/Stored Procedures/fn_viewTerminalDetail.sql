USE [NewInSys_BCA-DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_viewTerminalDetail]    Script Date: 11/2/2017 2:12:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_viewTerminalDetail](@sTerminalId VARCHAR(8))
RETURNS @viewTerminalDetail TABLE(
	[TerminalID] VARCHAR(8) NOT NULL,
	[Tag] VARCHAR(5) NOT NULL,
	[ItemName] VARCHAR(24) NOT NULL,
	[TagLength] INT NOT NULL,
	[LengthOfTagLength] INT NOT NULL,
	[TagValue] VARCHAR(150) NULL
)
AS
BEGIN
Declare @iDbID varchar (4),
		@cols nvarchar(MAX),
		@query nvarchar(max)

set @iDbID=(select DatabaseID 
			from tbProfileTerminalList 
			where TerminalID=@sTerminalId)

SET @cols =STUFF((SELECT ',' + QUOTENAME(Tag) 
                    from tbItemList
					where FormID=1 and DatabaseID=@iDbID
                    group by tag,ItemID
                    order by tag
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

	set @query='
	INSERT INTO @viewTerminalDetail(TerminalID,Tag,ItemName,TagLength,LengthOfTagLength,TagValue)
	select u.TerminalID,u.[Tag],il.ItemName,len(u.[TagValue])[TagLength],len(len(u.[TagValue]))[LengthOfTagLength],u.[TagValue]
	from tbProfileTerminal s
		unpivot
		(
			 [TagValue] 
			For Tag in ('+ @cols +') 
		) as u
		join tbProfileTerminalList tl
		on u.TerminalID=tl.TerminalID
		join tbItemList il
		on tl.DatabaseID=il.DatabaseID
	where u.TerminalID='''+ @sTerminalId +''' and u.Tag=il.Tag

	union
-- from table MasterProfile
select tl2.TerminalID,m.Tag,il2.ItemName,len(m.TagValue)[TagLength],len(len(m.TagValue))[LengthOfTagLength],m.TagValue
from MasterProfile m
	join tbItemList il2
	on il2.DatabaseID=m.DatabaseID and il2.Tag=m.Tag
	join tbProfileTerminalList tl2 
	on tl2.DatabaseID=il2.DatabaseID 
where tl2.TerminalID='''+ @sTerminalId +''' and m.FormID=1 
	and m.Tag not in ( select u.[Tag]
	from tbProfileTerminal s
		unpivot
		(
			[TagValue] 
			For Tag in ('+ @cols +') 
		) as u 
	where u.TerminalID='''+ @sTerminalId +''')
order by Tag'

	RETURN
END

GO


