﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 6, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Clear the tbUploadTagDE table, when define new template
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagDEDelete]
@sCond varchar(max)
AS
BEGIN
	declare @sQuery varchar(max)
	SET NOCOUNT ON;
	set @sQuery = 'DELETE FROM tbUploadTagDE '
	if @sCond is not null
	begin
		set @sQuery = @sQuery + @sCond
		exec(@sQuery)
	end
	else 
	exec (@sQuery)
END