﻿
-- ===============================================
-- Description:	Delete data based on given DatabaseID
-- ===============================================

CREATE PROCEDURE [dbo].[spItemComboBoxValueDeleteByDbID]
	@sDatabaseID SMALLINT
AS
	DELETE FROM tbItemComboBoxValue
	WHERE DatabaseID = @sDatabaseID

