-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 3, 2010
-- Modify date:
--				1. August 12, 2010, adding column AcquierTagLength
--				2. September 8, 2010, Var @sTerminalID dihapus dan @sCondition sebagai pengganti statement 'WHERE'
-- Description:	Get The Acquirer table content of Terminal
-- Add Description at 1 September, 2010
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerBrowse]
	@sCondition VARCHAR(MAX)=NULL,
	@sTerminalID VARCHAR(8)
AS
CREATE  TABLE #AcquirerUnpivot(
	[TerminalID] VARCHAR(8) NOT NULL,
	[AcquirerName] VARCHAR(30) NOT NULL,
	[AcquirerTag] VARCHAR(5) NOT NULL,
	[ItemName] VARCHAR(30) NOT NULL,
	[AcquirerTagLength] INT NOT NULL,
	[AcquirerTagValue] VARCHAR(150) NULL
)

INSERT INTO #AcquirerUnpivot
EXEC spProfileUnpivotAcquirer @sTerminalID

DECLARE @sQuery VARCHAR(MAX)
SET @sQuery=
	'SELECT TerminalID, 
		AcquirerTag AS Tag, 
		AcquirerTagLength, 
		AcquirerTagValue AS Value 
	FROM #AcquirerUnpivot '

	EXEC (@sQuery + @sCondition + ' ORDER BY TerminalID, AcquirerName,AcquirerTag')
	EXEC spProfileTerminalUpdateLastView @sTerminalID

drop table #AcquirerUnpivot

GO


