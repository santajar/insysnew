USE [NewInSys_BCA-DEV]
GO

/****** Object:  StoredProcedure [dbo].[spProfileIssuerDeleteProcess]    Script Date: 11/2/2017 3:39:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spProfileIssuerDeleteProcess]
	@sTerminalID VARCHAR(8),
	@sIssuer VARCHAR(15),
	@sRelation VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileRelationDeleteByIssuerOrCard @sTerminalID, @sRelation, @sIssuer
	EXEC spProfileIssuerDelete @sTerminalID, @sIssuer

	EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
END

GO


