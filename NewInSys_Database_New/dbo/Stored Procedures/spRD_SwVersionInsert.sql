﻿CREATE PROCEDURE [dbo].[spRD_SwVersionInsert]
	@sSV_Name VARCHAR(50)
	, @bAllTerminal BIT=0
	, @iGroupID INT
	, @iID INT OUTPUT
AS
DECLARE @IdentityOutput table ( ID int )
INSERT INTO tbRD_SoftwareVersion(
	[SV_NAME],
	[ALL],
	[GROUP_ID])
OUTPUT inserted.SV_ID INTO @IdentityOutput
VALUES
	(@sSV_Name
	,@bAllTerminal
	,@iGroupID)

SELECT @iID=ID FROM @IdentityOutput