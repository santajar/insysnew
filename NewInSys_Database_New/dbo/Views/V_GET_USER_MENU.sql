
GO

CREATE VIEW v_get_user_menu AS
SELECT  
	um.UserID,
	um.MenuId,
	m.LinkText,
	m.ParentId,
	m.MenuURL
FROM 
	tbUserMenu um, 
	tbUserLogin ul, 
	tbMenu m 
WHERE 
	um.MenuId = m.Id 
	AND um.UserID = ul.UserID





GO

