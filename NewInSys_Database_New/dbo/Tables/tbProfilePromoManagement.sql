﻿CREATE TABLE [dbo].[tbProfilePromoManagement] (
    [PromoManagementID]        INT           IDENTITY (1, 1) NOT NULL,
    [PromoManagementName]      VARCHAR (20)  NOT NULL,
    [DatabaseID]               INT           NOT NULL,
    [PromoManagementTag]       VARCHAR (5)   NOT NULL,
    [PromoManagementLength]    INT           NOT NULL,
    [PromoManagementTagLength] INT           NOT NULL,
    [PromoManagementTagValue]  VARCHAR (255) NOT NULL,
    [Description]              VARCHAR (255) NULL
);

