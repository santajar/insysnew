CREATE TABLE [dbo].[tbAppPackageEDCListTemp] (
    [Id]                BIGINT        NOT NULL,
    [AppPackId]         BIGINT        NOT NULL,
    [AppPackageName]    VARCHAR (150) NOT NULL,
    [AppPackageContent] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tbAppPackageEDCListTemp_1] PRIMARY KEY CLUSTERED ([Id] ASC, [AppPackId] ASC, [AppPackageName] ASC)
);








GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20141218-095027]
    ON [dbo].[tbAppPackageEDCListTemp]([Id] ASC, [AppPackId] ASC)
    ON [Data];

