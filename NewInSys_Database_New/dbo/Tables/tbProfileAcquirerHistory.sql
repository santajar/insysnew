﻿CREATE TABLE [dbo].[tbProfileAcquirerHistory] (
    [AcquirerTagID]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [TerminalID]                VARCHAR (8)   NOT NULL,
    [AcquirerName]              VARCHAR (15)  NOT NULL,
    [AcquirerTag]               VARCHAR (5)   NOT NULL,
    [AcquirerLengthOfTagLength] INT           NOT NULL,
    [AcquirerTagLength]         INT           NOT NULL,
    [AcquirerTagValue]          VARCHAR (150) NULL,
    [Description]               VARCHAR (150) NULL,
    [LastUpdate]                VARCHAR (20)  NULL
);



