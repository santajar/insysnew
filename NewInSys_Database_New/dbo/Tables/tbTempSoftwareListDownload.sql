﻿CREATE TABLE [dbo].[tbTempSoftwareListDownload] (
    [ID]             BIGINT       IDENTITY (1, 1) NOT NULL,
    [TerminalID]     VARCHAR (8)  NOT NULL,
    [GroupName]      VARCHAR (50) NOT NULL,
    [SoftwareName]   VARCHAR (50) NOT NULL,
    [ScheduleType]   VARCHAR (2)  NOT NULL,
    [ScheduleTime]   VARCHAR (6)  NOT NULL,
    [SoftwareStatus] VARCHAR (50) CONSTRAINT [DF_tbTempSoftwareListDownload_SoftwareStatus] DEFAULT ('OnProgress') NOT NULL,
    CONSTRAINT [PK_tbTempSoftwareListDownload] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70)
);

