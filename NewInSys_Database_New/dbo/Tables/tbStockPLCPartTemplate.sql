﻿CREATE TABLE [dbo].[tbStockPLCPartTemplate] (
    [ID]             INT           IDENTITY (4000, 1) NOT NULL,
    [Description]    VARCHAR (255) NOT NULL,
    [PartNumber]     VARCHAR (255) NULL,
    [PartType]       VARCHAR (255) NULL,
    [Replacable]     BIT           NULL,
    [BOMID]          INT           NULL,
    [UserCreationID] VARCHAR (10)  NULL,
    [DateCreation]   DATETIME      NULL,
    [Timezone]       VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_PLCPartTemplatePLCBOM] FOREIGN KEY ([BOMID]) REFERENCES [dbo].[tbStockPLCBOM] ([ID]),
    CONSTRAINT [FK_PLCPartTemplateUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

