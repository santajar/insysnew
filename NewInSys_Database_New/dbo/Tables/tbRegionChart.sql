﻿CREATE TABLE [dbo].[tbRegionChart] (
    [IDChart]     INT          IDENTITY (1, 1) NOT NULL,
    [RegionName]  VARCHAR (25) NULL,
    [RegionCount] INT          NULL,
    CONSTRAINT [PK_tbRegionChart] PRIMARY KEY CLUSTERED ([IDChart] ASC)
);


