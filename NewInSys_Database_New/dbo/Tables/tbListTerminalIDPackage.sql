﻿CREATE TABLE [dbo].[tbListTerminalIDPackage] (
    [id]         INT          IDENTITY (1, 1) NOT NULL,
    [TerminalID] VARCHAR (8)  NOT NULL,
    [GroupName]  VARCHAR (50) NOT NULL
);

