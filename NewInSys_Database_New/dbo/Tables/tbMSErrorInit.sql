﻿CREATE TABLE [dbo].[tbMSErrorInit] (
    [ErrorCode]    TINYINT      NOT NULL,
    [ErrorMessage] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([ErrorCode] ASC)
);

