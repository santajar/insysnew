﻿CREATE TABLE [dbo].[tbProfileTLE] (
    [TLETagID]             INT           IDENTITY (1, 1) NOT NULL,
    [DatabaseID]           SMALLINT      NOT NULL,
    [TLEName]              VARCHAR (50)  NOT NULL,
    [TLETag]               VARCHAR (5)   NOT NULL,
    [TLELengthOfTagLength] INT           NOT NULL,
    [TLETagLength]         INT           NOT NULL,
    [TLETagValue]          VARCHAR (256) NOT NULL,
    [Description]          VARCHAR (255) NULL,
    CONSTRAINT [PK_tbProfileTLE] PRIMARY KEY CLUSTERED ([TLETagID] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON) ON [Data]
);








GO
CREATE NONCLUSTERED INDEX [Id_Name_Database]
    ON [dbo].[tbProfileTLE]([TLETagID] ASC, [DatabaseID] ASC, [TLEName] ASC, [TLETag] ASC, [TLETagValue] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

