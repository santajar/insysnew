﻿CREATE TABLE [dbo].[tbCityChart] (
    [IDCityChart] INT          IDENTITY (1, 1) NOT NULL,
    [CityName]    VARCHAR (25) NULL,
    [CityCount]   INT          NULL,
    CONSTRAINT [PK_tbCityChart] PRIMARY KEY CLUSTERED ([IDCityChart] ASC)
);

