USE [NewInSys_BCA-DEV]
GO

/****** Object:  Table [dbo].[tbProfileIssuer]    Script Date: 16/11/2017 17:14:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbProfileIssuer](
	[IssuerTagID] [bigint] IDENTITY(1,1) NOT NULL,
	[TerminalID] [varchar](8) NOT NULL,
	[IssuerName] [varchar](15) NOT NULL,
	[AE001] [varchar](150) NULL,
	[AE002] [varchar](150) NULL,
	[AE003] [varchar](150) NULL,
	[AE004] [varchar](150) NULL,
	[AE005] [varchar](150) NULL,
	[AE006] [varchar](150) NULL,
	[AE007] [varchar](150) NULL,
	[AE008] [varchar](150) NULL,
	[AE009] [varchar](150) NULL,
	[AE010] [varchar](150) NULL,
	[AE011] [varchar](150) NULL,
	[AE012] [varchar](150) NULL,
	[AE013] [varchar](150) NULL,
	[AE014] [varchar](150) NULL,
	[AE015] [varchar](150) NULL,
	[AE016] [varchar](150) NULL,
	[AE017] [varchar](150) NULL,
	[AE018] [varchar](150) NULL,
	[AE019] [varchar](150) NULL,
	[AE020] [varchar](150) NULL,
	[AE021] [varchar](150) NULL,
	[AE022] [varchar](150) NULL,
	[AE023] [varchar](150) NULL,
	[AE024] [varchar](150) NULL,
	[AE025] [varchar](150) NULL,
	[AE026] [varchar](150) NULL,
	[AE027] [varchar](150) NULL,
	[AE028] [varchar](150) NULL,
	[AE029] [varchar](150) NULL,
	[AE030] [varchar](150) NULL,
	[AE031] [varchar](150) NULL,
	[AE033] [varchar](150) NULL,
	[AE034] [varchar](150) NULL,
	[AE035] [varchar](150) NULL,
	[AE036] [varchar](150) NULL,
	[AE037] [varchar](150) NULL,
	[AE038] [varchar](150) NULL,
	[AE039] [varchar](150) NULL,
	[AE040] [varchar](150) NULL,
	[AE041] [varchar](150) NULL,
	[AE042] [varchar](150) NULL,
	[AE043] [varchar](150) NULL,
	[AE044] [varchar](150) NULL,
	[AE045] [varchar](150) NULL,
	[AE046] [varchar](150) NULL,
	[AE047] [varchar](150) NULL,
	[AE048] [varchar](150) NULL,
	[AE049] [varchar](150) NULL,
	[AE050] [varchar](150) NULL,
	[AE051] [varchar](150) NULL,
	[AE052] [varchar](150) NULL,
	[AE053] [varchar](150) NULL,
	[AE054] [varchar](150) NULL,
	[AE055] [varchar](150) NULL,
	[AE056] [varchar](150) NULL,
	[AE057] [varchar](150) NULL,
	[AE059] [varchar](150) NULL,
	[AE060] [varchar](150) NULL,
	[AE061] [varchar](150) NULL,
	[AE062] [varchar](150) NULL,
	[AE063] [varchar](150) NULL,
	[AE064] [varchar](150) NULL,
	[AE065] [varchar](150) NULL,
	[AE066] [varchar](150) NULL,
	[AE067] [varchar](150) NULL,
	[AE068] [varchar](150) NULL,
	[AE069] [varchar](150) NULL,
	[AE070] [varchar](150) NULL,
	[AE071] [varchar](150) NULL,
	[AE072] [varchar](150) NULL,
	[AE073] [varchar](150) NULL,
	[AE074] [varchar](150) NULL,
	[AE075] [varchar](150) NULL,
 CONSTRAINT [PK_IssuerNew_2] PRIMARY KEY CLUSTERED 
(
	[IssuerTagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


