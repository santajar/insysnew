﻿CREATE TABLE [dbo].[tbInitLogConn] (
    [Id]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [Time]        DATETIME      NOT NULL,
    [TerminalId]  VARCHAR (8)   NOT NULL,
    [Description] VARCHAR (150) NULL,
    [Percentage]  INT           NULL,
    CONSTRAINT [PK_tbInitLogConn_1] PRIMARY KEY CLUSTERED ([TerminalId] ASC) WITH (FILLFACTOR = 70)
);




GO
CREATE NONCLUSTERED INDEX [Id_TerminalId]
    ON [dbo].[tbInitLogConn]([Id] ASC, [TerminalId] ASC, [Time] ASC) WITH (FILLFACTOR = 70);

