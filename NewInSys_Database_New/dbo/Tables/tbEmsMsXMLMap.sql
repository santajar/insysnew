﻿CREATE TABLE [dbo].[tbEmsMsXMLMap] (
    [EMSMapId]            INT          IDENTITY (1, 1) NOT NULL,
    [EMSXMLVersion]       VARCHAR (50) NOT NULL,
    [EMSXMLColumn]        VARCHAR (50) NOT NULL,
    [EMSXMLFilter]        VARCHAR (50) NULL,
    [EMSXMLFormId]        SMALLINT     NULL,
    [EMSXMLTag]           VARCHAR (5)  NULL,
    [EMSXMLIsComplicated] BIT          NULL,
    [EMSXMLIsInqInit]     BIT          CONSTRAINT [DF_tbEmsMsXMLMap_EMSXMLIsInqInit] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbEmsMsXMLMap] PRIMARY KEY CLUSTERED ([EMSMapId] ASC, [EMSXMLVersion] ASC, [EMSXMLColumn] ASC)
);



