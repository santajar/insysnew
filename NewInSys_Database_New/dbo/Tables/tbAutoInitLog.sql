CREATE TABLE [dbo].[tbAutoInitLog] (
    [TerminalId]   VARCHAR (8)  NOT NULL,
    [LastInitTime] VARCHAR (30) NULL,
    [PABX]         VARCHAR (10) NULL,
    [SoftwareVer]  VARCHAR (50) NULL,
    [OSVer]        VARCHAR (50) NULL,
    [KernelVer]    VARCHAR (50) NULL,
    [EDCSN]        VARCHAR (50) NULL,
    [ReaderSN]     VARCHAR (50) NULL,
    [PSAMSN]       VARCHAR (50) NULL,
    [MCSN]         VARCHAR (50) NULL,
    [MemUsage]     VARCHAR (50) NULL,
    [ICCID]        VARCHAR (50) NULL,
    CONSTRAINT [PK_tbAutoInitLog] PRIMARY KEY CLUSTERED ([TerminalId] ASC) WITH (FILLFACTOR = 70) ON [Data_Audit]
);








GO
CREATE NONCLUSTERED INDEX [index-AutoInitLog]
    ON [dbo].[tbAutoInitLog]([TerminalId] ASC, [LastInitTime] ASC, [PABX] ASC, [SoftwareVer] ASC, [OSVer] ASC, [KernelVer] ASC, [EDCSN] ASC, [ReaderSN] ASC, [PSAMSN] ASC, [MCSN] ASC, [MemUsage] ASC, [ICCID] ASC) WITH (FILLFACTOR = 70)
    ON [Data_Audit];

