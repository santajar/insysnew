﻿CREATE TABLE [dbo].[tbItemObject] (
    [ObjectID]   SMALLINT     NOT NULL,
    [ObjectName] VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_Object] PRIMARY KEY CLUSTERED ([ObjectID] ASC)
);

