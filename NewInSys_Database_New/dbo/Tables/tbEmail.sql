﻿CREATE TABLE [dbo].[tbEmail] (
    [NotifType]       INT          NOT NULL,
    [Recipients1]     VARCHAR (50) NULL,
    [Recipients2]     VARCHAR (50) NULL,
    [Recipients3]     VARCHAR (50) NULL,
    [CopyRecipients1] VARCHAR (50) NULL,
    [CopyRecipients2] VARCHAR (50) NULL,
    [CopyRecipients3] VARCHAR (50) NULL,
    CONSTRAINT [PK_tbEmail_1] PRIMARY KEY CLUSTERED ([NotifType] ASC)
);

