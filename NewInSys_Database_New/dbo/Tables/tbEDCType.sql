﻿CREATE TABLE [dbo].[tbEDCType] (
    [EdcTypeID]   BIGINT        IDENTITY (1, 1) NOT NULL,
    [EdcTypeName] VARCHAR (50)  NOT NULL,
    [EdcTypeDesc] VARCHAR (150) NULL,
    CONSTRAINT [PK_tbEDCType] PRIMARY KEY CLUSTERED ([EdcTypeID] ASC)
);



