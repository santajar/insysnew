﻿CREATE TABLE [dbo].[tbEdcMonitorDetail] (
    [ID]                   INT          IDENTITY (1, 1) NOT NULL,
    [ID_Header]            INT          NOT NULL,
    [TerminalID]           VARCHAR (8)  NOT NULL,
    [SoftwareVersion]      VARCHAR (50) NOT NULL,
    [SettlementDateTime]   VARCHAR (12) NOT NULL,
    [CardName]             VARCHAR (50) NULL,
    [CardTotalAmount]      INT          NULL,
    [CardTotalTransaction] INT          NULL,
    [AcquirerName]         VARCHAR (15) NULL,
    [TID]                  VARCHAR (8)  NULL,
    [MID]                  VARCHAR (20) NULL,
    CONSTRAINT [PK__tbEdcMon__3214EC27B23FB903] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70)
);


