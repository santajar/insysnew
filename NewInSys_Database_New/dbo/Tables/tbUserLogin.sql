﻿CREATE TABLE [dbo].[tbUserLogin] (
    [UserID]             VARCHAR (10)  NOT NULL,
    [UserName]           VARCHAR (25)  NOT NULL,
    [Password]           VARCHAR (32)  NOT NULL,
    [UserRights]         VARCHAR (MAX) NOT NULL,
    [GroupID]            VARCHAR (30)  NULL,
    [Locked]             BIT           CONSTRAINT [DF_tbUserLogin_Locked] DEFAULT ((0)) NOT NULL,
    [LastModifyPassword] DATETIME      NULL,
    [LastLogin]          DATETIME      NULL,
    [Revoke]             BIT           CONSTRAINT [DF_tbUserLogin_Expiry] DEFAULT ((0)) NOT NULL,
	[ViewAuditUser]      BIT           DEFAULT ((0)) NOT NULL,
    [EnableTools] BIT NOT NULL DEFAULT ((0)), 
    CONSTRAINT [PK_tbUserLogin] PRIMARY KEY CLUSTERED ([UserID] ASC)
);



