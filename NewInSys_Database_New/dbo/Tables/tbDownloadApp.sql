﻿CREATE TABLE [dbo].[tbDownloadApp] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [TerminalID] VARCHAR (8)   NOT NULL,
    [Content]    VARCHAR (MAX) NOT NULL,
    [Flag]       BIT           NOT NULL,
    CONSTRAINT [PK_tbDownloadApp] PRIMARY KEY CLUSTERED ([ID] ASC)
);



