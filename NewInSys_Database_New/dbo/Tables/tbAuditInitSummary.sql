CREATE TABLE [dbo].[tbAuditInitSummary] (
    [TID]          CHAR (8)  NOT NULL,
    [Software]     CHAR (50) NULL,
    [LastInit]     CHAR (25) NULL,
    [InitTimes]    CHAR (10) NULL,
    [SuccessTimes] CHAR (10) NULL,
    [FailedInit]   CHAR (10) NULL,
    CONSTRAINT [PK_tbAuditInitSummary] PRIMARY KEY CLUSTERED ([TID] ASC) ON [Data_Audit]
);








GO
CREATE NONCLUSTERED INDEX [Index-AuditInitSummary]
    ON [dbo].[tbAuditInitSummary]([TID] ASC, [Software] ASC, [LastInit] ASC, [InitTimes] ASC, [SuccessTimes] ASC, [FailedInit] ASC)
    ON [Data_Audit];

