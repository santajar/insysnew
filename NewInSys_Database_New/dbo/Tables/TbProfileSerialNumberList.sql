﻿CREATE TABLE [dbo].[TbProfileSerialNumberList] (
    [DataBaseID]   INT          NOT NULL,
    [TerminalID]   VARCHAR (50) NOT NULL,
    [SerialNumber] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_TbProfileSerialNumber] PRIMARY KEY CLUSTERED ([SerialNumber] ASC)
);

