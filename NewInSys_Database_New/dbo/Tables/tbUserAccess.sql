﻿CREATE TABLE [dbo].[tbUserAccess] (
    [Id]         INT          IDENTITY (1, 1) NOT NULL,
    [TerminalID] CHAR (8)     NOT NULL,
    [UserID]     VARCHAR (10) NULL,
    CONSTRAINT [PK_tbUserAccess] PRIMARY KEY CLUSTERED ([Id] ASC)
);



