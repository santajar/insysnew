﻿CREATE TABLE [dbo].[tbStockPLCParty] (
    [ID]             INT           IDENTITY (1000, 1) NOT NULL,
    [Name]           VARCHAR (255) NOT NULL,
    [PartyReference] VARCHAR (255) NULL,
    [EstateOwnerID]  INT           NULL,
    [PartyType]      VARCHAR (255) NULL,
    [UserCreationID] VARCHAR (10)  NULL,
    [DateCreation]   DATETIME      NULL,
    [Timezone]       VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_PLCPartySponsor] FOREIGN KEY ([EstateOwnerID]) REFERENCES [dbo].[tbStockSponsor] ([SponsorID]),
    CONSTRAINT [FK_PLCPartyUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

