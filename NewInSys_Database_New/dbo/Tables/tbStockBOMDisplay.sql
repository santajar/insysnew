﻿CREATE TABLE [dbo].[tbStockBOMDisplay] (
    [ID]           INT           IDENTITY (11000, 1) NOT NULL,
    [DisplayColor] VARCHAR (255) NOT NULL,
    [Color]        VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70)
);

