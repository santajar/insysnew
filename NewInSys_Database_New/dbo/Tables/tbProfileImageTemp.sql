﻿CREATE TABLE [dbo].[tbProfileImageTemp] (
    [ID]           BIGINT        NOT NULL,
    [ImageID]      INT           NOT NULL,
    [ImageContent] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [FK_tbProfileImageTemp_tbProfileImage] FOREIGN KEY ([ImageID]) REFERENCES [dbo].[tbProfileImage] ([ImageID])
);

