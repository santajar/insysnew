CREATE TABLE [dbo].[tbAppPackageEDCList] (
    [AppPackId]          BIGINT          IDENTITY (1, 1) NOT NULL,
    [BuildNumber]        INT             NULL,
    [AppPackageName]     VARCHAR (MAX)   NOT NULL,
    [AppPackageFilename] VARCHAR (150)   NOT NULL,
    [AppPackageContent]  VARBINARY (MAX) NOT NULL,
    [AppPackageDesc]     VARCHAR (250)   NULL,
    [EdcTypeID]          VARCHAR (150)   NULL,
    [UploadTime]         DATETIME        NOT NULL,
    CONSTRAINT [PK_tbAppList] PRIMARY KEY CLUSTERED ([AppPackId] ASC)
);












GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20141218-094935]
    ON [dbo].[tbAppPackageEDCList]([AppPackId] ASC, [AppPackageFilename] ASC, [UploadTime] ASC)
    ON [Data];

