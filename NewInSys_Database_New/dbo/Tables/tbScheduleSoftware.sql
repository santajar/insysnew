﻿CREATE TABLE [dbo].[tbScheduleSoftware] (
    [ScheduleID]     INT          IDENTITY (1, 1) NOT NULL,
    [TerminalID]     VARCHAR (8)  NULL,
    [ScheduleType]   VARCHAR (10) NULL,
    [ScheduleCheck]  VARCHAR (10) NULL,
    [ScheduleTime]   DATETIME     NULL,
    [EnableDownload] BIT          NULL,
    CONSTRAINT [PK__tbSchedu__9C8A5B69684A1485] PRIMARY KEY CLUSTERED ([ScheduleID] ASC)
);





