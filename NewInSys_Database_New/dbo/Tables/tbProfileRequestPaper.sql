﻿CREATE TABLE [dbo].[tbProfileRequestPaper]
(
[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[TerminalID] [varchar](8) NULL,
	[Software] [varchar](15) NULL,
	[SerialNumber] [varchar](25) NULL,
	[RequestTime] [datetime] NULL,
	[ReceiptTime] [datetime] NULL,
	[Status] [varchar](25) NULL,
 CONSTRAINT [PK_tbProfileRequestPaper] PRIMARY KEY CLUSTERED 
(
	[RequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

