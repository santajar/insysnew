﻿CREATE TABLE [dbo].[tbSchemaCustomMenu] (
    [ID]         INT          IDENTITY (1, 1) NOT NULL,
    [DatabaseID] INT          NOT NULL,
    [Tag]        VARCHAR (5)  NOT NULL,
    [ItemName]   VARCHAR (25) NULL,
    [ParentTag]  VARCHAR (5)  NULL
);

