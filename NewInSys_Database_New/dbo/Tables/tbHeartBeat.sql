﻿CREATE TABLE [dbo].[tbHeartBeat] (
    [ID]                  INT           IDENTITY (1, 1) NOT NULL,
    [MessageTimestamp]    DATETIME      NULL,
    [MessageRaw]          VARCHAR (MAX) NULL,
    [ProcCode]            VARCHAR (6)   NULL,
    [DatabaseID]          INT           NOT NULL,
    [TerminalID]          VARCHAR (8)   NOT NULL,
    [SerialNumber]        VARCHAR (50)  NULL,
    [SoftwareVersion]     VARCHAR (50)  NULL,
    [LifetimeCounter]     INT           CONSTRAINT [DF__tbHeartBe__Lifet__49FDC4EB] DEFAULT ((0)) NULL,
    [TotalAllTransaction] INT           CONSTRAINT [DF__tbHeartBe__Total__4AF1E924] DEFAULT ((0)) NULL,
    [TotalSwipe]          INT           CONSTRAINT [DF__tbHeartBe__Total__4BE60D5D] DEFAULT ((0)) NULL,
    [TotalDip]            INT           CONSTRAINT [DF__tbHeartBe__Total__4CDA3196] DEFAULT ((0)) NULL,
    [SettlementDateTime]  VARCHAR (12)  NULL,
    [ICCID]               VARCHAR (50)  NULL,
    [TowerNumber]         INT           NULL,
    [MCC_MNC_Data]        VARCHAR (50)  NULL,
    [LAC_Data]            VARCHAR (50)  NULL,
    [Cell_ID_Data]        VARCHAR (50)  NULL,
    CONSTRAINT [PK__tbHeartB__3214EC27A31EFAE5] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70)
);

