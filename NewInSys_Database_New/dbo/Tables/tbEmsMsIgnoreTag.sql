﻿CREATE TABLE [dbo].[tbEmsMsIgnoreTag] (
    [ID]         BIGINT       IDENTITY (1, 1) NOT NULL,
    [EmsVersion] VARCHAR (4)  NOT NULL,
    [Tag]        VARCHAR (5)  NOT NULL,
    [ItemName]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tbEmsMsIgnoreTag] PRIMARY KEY CLUSTERED ([ID] ASC)
);

