
GO

CREATE TABLE dbo.tbMenu
	(
	Id             INT NOT NULL,
	LinkText       VARCHAR (50) NOT NULL,
	ActionName     VARCHAR (50) NULL,
	ControllerName VARCHAR (50) NULL,
	ParentId       INT CONSTRAINT DF_tbMenu_ParentId DEFAULT ((0)) NOT NULL,
	SortOrder      INT NOT NULL,
	Tag            VARCHAR (4) NULL,
	MenuURL        VARCHAR (100) NULL,
	CONSTRAINT PK_tbMenu PRIMARY KEY (Id)
	)
GO

