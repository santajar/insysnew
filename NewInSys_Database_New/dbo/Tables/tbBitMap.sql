﻿CREATE TABLE [dbo].[tbBitMap] (
    [ApplicationName]    VARCHAR (25) NOT NULL,
    [AllowCompress]      BIT          NOT NULL,
    [BitMap]             VARCHAR (64) NOT NULL,
    [HexBitMap]          VARCHAR (16) NOT NULL,
    [StandartISO8583]    BIT          CONSTRAINT [DF_tbBitMap_StandartISO8583] DEFAULT ((0)) NOT NULL,
    [BitMapRD]           VARCHAR (64) NULL,
    [HexBitMapRD]        VARCHAR (16) NULL,
    [SentBit48]          BIT          CONSTRAINT [DF_tbBitMap_SentBit48] DEFAULT ((1)) NOT NULL,
    [Custom48_1]         BIT          CONSTRAINT [DF_tbBitMap_Custom48_1] DEFAULT ((0)) NOT NULL,
    [CustomRDBit61]      BIT          DEFAULT ((0)) NOT NULL,
    [ExtraBitRD]         VARCHAR (50) NULL,
    [InitBySerialNumber] BIT          NULL,
    CONSTRAINT [PK_tbBitMap_1] PRIMARY KEY CLUSTERED ([ApplicationName] ASC)
);









