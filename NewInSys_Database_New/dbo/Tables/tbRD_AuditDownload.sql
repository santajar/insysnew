﻿CREATE TABLE [dbo].[tbRD_AuditDownload]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CreateDate] DATETIME NOT NULL DEFAULT GETDATE(), 
    [TerminalID] VARCHAR(25) NOT NULL, 
    [SerialNumber] VARCHAR(50) NOT NULL, 
    [InstallDate] VARCHAR(50) NULL, 
    [SV_NAME] VARCHAR(50) NULL, 
    [Filename] VARCHAR(50) NOT NULL, 
    [FreeRAM] VARCHAR(50) NULL, 
    [TotalRAM] VARCHAR(50) NULL
)
