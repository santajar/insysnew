﻿CREATE TABLE [dbo].[tbProfileEMVManagement] (
    [EMVManagementID]                INT           IDENTITY (1, 1) NOT NULL,
    [EMVManagementName]              VARCHAR (15)  NOT NULL,
    [DatabaseID]                     INT           NOT NULL,
    [EMVManagementTag]               VARCHAR (5)   NOT NULL,
    [EMVManagementLengthofTagLength] INT           NOT NULL,
    [EMVManagementTagLength]         INT           NOT NULL,
    [EMVManagementTagValue]          VARCHAR (255) NOT NULL,
    [Description]                    VARCHAR (255) NULL
);

