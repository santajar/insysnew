﻿CREATE TABLE [dbo].[TempXLS] (
    [Id]           INT          IDENTITY (1, 1) NOT NULL,
    [TerminalID]   VARCHAR (8)  NULL,
    [MID]          VARCHAR (15) NULL,
    [MerchantName] VARCHAR (25) NULL,
    [Alamat1]      VARCHAR (25) NULL,
    [Alamat2]      VARCHAR (25) NULL,
    [Flazz]        VARCHAR (5)  NULL,
    [TopUp]        VARCHAR (5)  NULL,
    [Payment]      VARCHAR (5)  NULL,
    [BCA_Card]     VARCHAR (5)  NULL,
    [Visa]         VARCHAR (5)  NULL,
    [Master]       VARCHAR (5)  NULL,
    [Amex]         VARCHAR (5)  NULL,
    [Diners]       VARCHAR (5)  NULL,
    [Cicilan]      VARCHAR (5)  NULL,
    [Cicilan_Prog] VARCHAR (5)  NULL,
    [Debit]        VARCHAR (5)  NULL,
    [Tunai]        VARCHAR (5)  NULL,
    CONSTRAINT [PK_TempXLS] PRIMARY KEY CLUSTERED ([Id] ASC)
);



