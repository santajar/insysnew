﻿CREATE TABLE [dbo].[tbUploadAcquirerList] (
    [Id]           INT          IDENTITY (1, 1) NOT NULL,
    [DatabaseId]   INT          NOT NULL,
    [AcquirerName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tbUploadAcquirerList] PRIMARY KEY CLUSTERED ([Id] ASC)
);



