﻿CREATE TABLE [dbo].[tbInitResult] (
    [IdResult]   INT           IDENTITY (1, 1) NOT NULL,
    [TerminalId] VARCHAR (8)   NOT NULL,
    [Content]    VARCHAR (MAX) NULL,
    [Tag]        VARCHAR (4)   NULL,
    [ISO]        VARCHAR (MAX) NOT NULL,
    [ERRORCODE]  INT           NOT NULL,
    CONSTRAINT [PK_tbInitResult] PRIMARY KEY CLUSTERED ([IdResult] ASC) WITH (FILLFACTOR = 70)
);








GO
CREATE NONCLUSTERED INDEX [TerminalId]
    ON [dbo].[tbInitResult]([IdResult] ASC, [TerminalId] ASC, [Tag] ASC) WITH (FILLFACTOR = 70);

