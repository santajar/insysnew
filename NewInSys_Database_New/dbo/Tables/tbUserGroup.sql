﻿CREATE TABLE [dbo].[tbUserGroup] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [GroupID]  VARCHAR (30) NOT NULL,
    [Tag]      VARCHAR (5)  NOT NULL,
    [ItemName] VARCHAR (30) NOT NULL,
    CONSTRAINT [PK_tbUserGroup_1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tbUserGroup_tbUserGroup] FOREIGN KEY ([Id]) REFERENCES [dbo].[tbUserGroup] ([Id])
);



