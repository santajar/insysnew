﻿CREATE TABLE [dbo].[tbProfileAID] (
    [AIDId]                SMALLINT     IDENTITY (1, 1) NOT NULL,
    [DatabaseId]           SMALLINT     NOT NULL,
    [AIDName]              VARCHAR (50) NOT NULL,
    [AIDTag]               VARCHAR (5)  NOT NULL,
    [AIDLengthOfTagLength] SMALLINT     NOT NULL,
    [AIDTagLength]         SMALLINT     NOT NULL,
    [AIDTagValue]          VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tbProfileAID] PRIMARY KEY CLUSTERED ([AIDId] ASC) ON [Data]
);








GO
CREATE NONCLUSTERED INDEX [Id_DatabaseId_Name]
    ON [dbo].[tbProfileAID]([AIDId] ASC, [DatabaseId] ASC, [AIDName] ASC, [AIDTag] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

