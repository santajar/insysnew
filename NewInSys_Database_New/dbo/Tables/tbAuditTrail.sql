CREATE TABLE [dbo].[tbAuditTrail] (
    [LogID]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [AccessTime]        VARCHAR (255) NOT NULL,
    [UserId]            VARCHAR (10)  NULL,
    [DatabaseName]      VARCHAR (50)  NULL,
    [ActionDescription] VARCHAR (255) NULL,
    CONSTRAINT [PK_tbAuditTrail] PRIMARY KEY CLUSTERED ([LogID] ASC) ON [Data_Audit]
);










GO
CREATE NONCLUSTERED INDEX [Index-AuditTrail]
    ON [dbo].[tbAuditTrail]([LogID] ASC, [AccessTime] ASC, [UserId] ASC, [DatabaseName] ASC, [ActionDescription] ASC)
    ON [Data_Audit];

