﻿CREATE TABLE [dbo].[tbProfileCurrency] (
    [CurrencyID]                BIGINT        IDENTITY (1, 1) NOT NULL,
    [DatabaseID]                BIGINT        NULL,
    [CurrencyName]              VARCHAR (5)   NULL,
    [CurrencyTag]               VARCHAR (5)   NOT NULL,
    [CurrencyLengthOfTagLength] INT           NOT NULL,
    [CurrencyTagLength]         INT           NOT NULL,
    [CurrencyTagValue]          VARCHAR (150) NULL,
    [Description]               VARCHAR (255) NULL,
    CONSTRAINT [PK_tbProfileCurrency] PRIMARY KEY CLUSTERED ([CurrencyID] ASC)
);



