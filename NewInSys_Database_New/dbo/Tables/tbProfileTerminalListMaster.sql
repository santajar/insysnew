﻿CREATE TABLE [dbo].[tbProfileTerminalListMaster] (
    [TerminalId]   VARCHAR (8) NOT NULL,
    [MsTerminalId] VARCHAR (8) NOT NULL,
    CONSTRAINT [PK_tbProfileTerminalListMaster] PRIMARY KEY CLUSTERED ([TerminalId] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON) ON [Data]
);

