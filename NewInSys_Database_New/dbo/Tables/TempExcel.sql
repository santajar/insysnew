﻿CREATE TABLE [dbo].[TempExcel] (
    [Id]      INT          IDENTITY (1, 1) NOT NULL,
    [TIDLama] VARCHAR (8)  NULL,
    [TIDBaru] VARCHAR (8)  NULL,
    [MIDLama] VARCHAR (15) NULL,
    CONSTRAINT [PK_TempExcel] PRIMARY KEY CLUSTERED ([Id] ASC)
);



