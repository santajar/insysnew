﻿CREATE TABLE [dbo].[tbLogConsole] (
    [ID]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [LogTime]   DATETIME      NULL,
    [LogType]   VARCHAR (100) NULL,
    [DetailLog] VARCHAR (MAX) NULL
);

