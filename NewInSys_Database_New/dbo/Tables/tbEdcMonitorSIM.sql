﻿CREATE TABLE [dbo].[tbEdcMonitorSIM]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ID_Header] INT NOT NULL, 
    [TerminalID] VARCHAR(8) NOT NULL, 
    [SoftwareVersion] VARCHAR(50) NOT NULL,
    [TowerNumber] INT NULL,
    [MCC_MNC_Data] VARCHAR(50) NULL, 
    [LAC_Data] VARCHAR(50) NULL, 
    [Cell_ID_Data] VARCHAR(50) NULL
)