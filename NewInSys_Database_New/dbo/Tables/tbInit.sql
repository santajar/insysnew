﻿CREATE TABLE [dbo].[tbInit] (
    [InitID]     BIGINT      IDENTITY (1, 1) NOT NULL,
    [TerminalId] VARCHAR (8) NOT NULL,
    [Content]    TEXT        NOT NULL,
    [Tag]        VARCHAR (5) NOT NULL,
    [Flag]       BIT         CONSTRAINT [DF_tbInit_Flag] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbInit] PRIMARY KEY CLUSTERED ([InitID] ASC) WITH (FILLFACTOR = 70)
);



