﻿CREATE TABLE [dbo].[tbUploadAcquirer] (
    [Id]                      INT           IDENTITY (1, 1) NOT NULL,
    [TerminalID]              VARCHAR (MAX) NULL,
    [AcquirerName]            VARCHAR (MAX) NULL,
    [Terminal ID]             VARCHAR (MAX) NULL,
    [Merchant ID]             VARCHAR (MAX) NULL,
    [Txn Primary Ph]          VARCHAR (MAX) NULL,
    [Txn Secondary Ph]        VARCHAR (MAX) NULL,
    [Stl Primary Phone]       VARCHAR (MAX) NULL,
    [Stl Secondary Ph]        VARCHAR (MAX) NULL,
    [Min Top UP Flazz]        VARCHAR (MAX) NULL,
    [Installment Description] VARCHAR (MAX) NULL,
    [MID Top Up Tunai]        VARCHAR (MAX) NULL,
    CONSTRAINT [PK_tbUploadAcquirer] PRIMARY KEY CLUSTERED ([Id] ASC)
);



