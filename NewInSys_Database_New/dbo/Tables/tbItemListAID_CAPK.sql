﻿CREATE TABLE [dbo].[tbItemListAID_CAPK] (
    [Tag]     VARCHAR (10) NOT NULL,
    [TagName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tbItemListAIDCAPK] PRIMARY KEY CLUSTERED ([Tag] ASC)
);

