﻿CREATE TABLE [dbo].[tbReplaceTIDLog] (
    [ID]     BIGINT       IDENTITY (1, 1) NOT NULL,
    [Date]   DATETIME     NULL,
    [UserId] VARCHAR (50) NULL,
    [OldTID] VARCHAR (8)  NULL,
    [NewTID] VARCHAR (8)  NULL,
    [Status] VARCHAR (20) NULL,
    CONSTRAINT [PK_tbReplaceTIDLog] PRIMARY KEY CLUSTERED ([ID] ASC)
);



