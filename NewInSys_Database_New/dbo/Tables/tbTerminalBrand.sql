﻿CREATE TABLE [dbo].[tbTerminalBrand] (
    [TerminalBrandId]          INT          IDENTITY (1, 1) NOT NULL,
    [TerminalBrandName]        VARCHAR (25) NOT NULL,
    [TerminalBrandDescription] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([TerminalBrandId] ASC)
);



