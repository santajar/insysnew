﻿CREATE TABLE [dbo].[tbStockTerminalModel] (
    [ID]         INT           IDENTITY (10000, 1) NOT NULL,
    [Type]       VARCHAR (255) NOT NULL,
    [ModelValue] VARCHAR (255) NOT NULL,
    [ModelName]  VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70)
);

