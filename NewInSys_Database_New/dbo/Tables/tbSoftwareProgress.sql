﻿CREATE TABLE [dbo].[tbSoftwareProgress] (
    [ID]                INT             IDENTITY (1, 1) NOT NULL,
    [EdcGroup]          VARCHAR (50)    NULL,
    [EdcRegion]         VARCHAR (50)    NULL,
    [EdcCity]           VARCHAR (50)    NULL,
    [TerminalID]        VARCHAR (8)     NULL,
    [Software]          VARCHAR (50)    NOT NULL,
    [BuildNumber]       INT             NULL,
    [SoftwareDownload]  VARCHAR (50)    NULL,
    [SerialNumber]      VARCHAR (25)    NOT NULL,
    [StartTime]         DATETIME        NOT NULL,
    [EndTime]           DATETIME        NULL,
    [Percentage]        DECIMAL (18, 2) NULL,
    [FinishInstallTime] DATETIME        NULL,
    [InstallStatus]     VARCHAR (50)    NULL,
    CONSTRAINT [PK_tbSoftwareProgress] PRIMARY KEY CLUSTERED ([ID] ASC)
);





