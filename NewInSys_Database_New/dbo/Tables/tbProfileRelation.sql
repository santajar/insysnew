
/****** Object:  Table [dbo].[tbProfileRelation]    Script Date: 16/11/2017 17:12:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbProfileRelation](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TerminalID] [varchar](8) NOT NULL,
	[TagCard] [varchar](5) NULL,
	[ValueCard] [varchar](50) NULL,
	[TagIssuer] [varchar](5) NULL,
	[ValueIssuer] [varchar](50) NULL,
	[TagAcquirer] [varchar](5) NULL,
	[ValueAcquirer] [varchar](50) NULL,
 CONSTRAINT [PK_tbProfileRelation_NEW] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


