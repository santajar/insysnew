CREATE TABLE [dbo].[tbAuditTrailDetail] (
    [LogID]   BIGINT        NOT NULL,
    [Remarks] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_tbAuditTrailDetail] PRIMARY KEY CLUSTERED ([LogID] ASC) WITH (FILLFACTOR = 70) ON [Data_Audit]
);














GO
CREATE NONCLUSTERED INDEX [index-AuditTrailDetail]
    ON [dbo].[tbAuditTrailDetail]([LogID] ASC) WITH (FILLFACTOR = 70)
    ON [Data_Audit];

