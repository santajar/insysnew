﻿CREATE TABLE [dbo].[tbStockSponsor] (
    [SponsorID]    INT           NOT NULL,
    [SponsorName]  VARCHAR (100) NOT NULL,
    [RegionID]     INT           NOT NULL,
    [RootEstateID] VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([SponsorID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_RegionID] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[tbStockRegion] ([RegionID]),
    UNIQUE NONCLUSTERED ([RootEstateID] ASC) WITH (FILLFACTOR = 70),
    UNIQUE NONCLUSTERED ([SponsorName] ASC) WITH (FILLFACTOR = 70)
);

