﻿CREATE TABLE [dbo].[tbProfileTerminalDB] (
    [DatabaseID]             SMALLINT     NOT NULL,
    [DatabaseName]           VARCHAR (25) NOT NULL,
    [NewInit]                BIT           NOT NULL DEFAULT ((1)),
    [LengthOfTag]            SMALLINT      NOT NULL DEFAULT 5,
    [EMVInit]                BIT           NOT NULL DEFAULT ((0)),
    [EMVInitManagement]      BIT           NOT NULL DEFAULT ((0)),
    [TLEInit]                BIT           NOT NULL DEFAULT ((0)),
    [LoyaltyProd]            BIT           NOT NULL DEFAULT ((0)),
    [GPRSInit]               BIT           NOT NULL DEFAULT ((0)),
    [CurrencyInit]           BIT           NOT NULL DEFAULT ((0)),
    [RemoteDownload]         BIT           NOT NULL DEFAULT ((0)),
    [PinPad]                 BIT           NOT NULL DEFAULT ((0)),
    [BankCode]               BIT           NOT NULL DEFAULT ((0)),
    [ProductCode]            BIT           NOT NULL DEFAULT ((0)),
    [Custom_CAPK_Name]       BIT           NOT NULL DEFAULT ((0)),
    [Custom_Menu]            BIT           NOT NULL DEFAULT ((0)),
    [PromoManagement]        BIT           NOT NULL DEFAULT ((0)),
    [InitialFlazzManagement] BIT           NOT NULL DEFAULT ((0)),
    [CardType]               BIT           NOT NULL DEFAULT ((0)),
    [EnableMonitor] BIT NOT NULL DEFAULT ((0)), 
    [CustomGPRSInit] BIT NULL DEFAULT ((0)), 
    [SNManagement] BIT NULL DEFAULT ((0)), 
    [RequestPaperReceipt] BIT NULL DEFAULT ((0)), 
    CONSTRAINT [PK_TerminalDB] PRIMARY KEY CLUSTERED ([DatabaseID] ASC) WITH (FILLFACTOR = 70)
);











GO
CREATE UNIQUE NONCLUSTERED INDEX [DatabaseId-Name]
    ON [dbo].[tbProfileTerminalDB]([DatabaseID] ASC, [DatabaseName] ASC, [NewInit] ASC, [TLEInit] ASC, [GPRSInit] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

