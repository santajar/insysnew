﻿CREATE TABLE [dbo].[tbMSCommsType] (
    [Id]            INT          NOT NULL,
    [CommsID]       INT          NOT NULL,
    [CommsTypeName] VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

