﻿CREATE TABLE [dbo].[tbSoftwareProgressCPL] (
    [ID]                INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [GroupName]         VARCHAR (50)    NULL,
    [TerminalID]        VARCHAR (8)     NULL,
    [Software]          VARCHAR (50)    NOT NULL,
    [SoftwareDownload]  VARCHAR (50)    NULL,
    [SerialNumber]      VARCHAR (25)    NOT NULL,
    [StartTime]         DATETIME        NOT NULL,
    [EndTime]           DATETIME        NULL,
    [Percentage]        DECIMAL (18, 2) NULL,
    [FinishInstallTime] DATETIME        NULL,
    [InstallStatus]     VARCHAR (50)    NULL,
    CONSTRAINT [PK_tbSoftwareProgressCPL] PRIMARY KEY CLUSTERED ([ID] ASC)
);

