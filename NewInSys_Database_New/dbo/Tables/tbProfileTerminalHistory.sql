﻿CREATE TABLE [dbo].[tbProfileTerminalHistory] (
    [TerminalTagID]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [TerminalID]                VARCHAR (8)   NOT NULL,
    [TerminalTag]               VARCHAR (5)   NOT NULL,
    [TerminalLengthOfTagLength] INT           CONSTRAINT [DF__tbProfile__Termi__5DA4CAE5] DEFAULT ((0)) NOT NULL,
    [TerminalTagLength]         INT           CONSTRAINT [DF__tbProfile__Termi__5E98EF1E] DEFAULT ((0)) NOT NULL,
    [TerminalTagValue]          VARCHAR (150) NULL,
    [Description]               VARCHAR (255) NULL,
    [LastUpdate]                VARCHAR (20)  NULL
);



