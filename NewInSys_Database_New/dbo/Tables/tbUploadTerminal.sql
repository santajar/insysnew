﻿CREATE TABLE [dbo].[tbUploadTerminal] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [TerminalID]      VARCHAR (MAX) NULL,
    [Merchant Name 1] VARCHAR (MAX) NULL,
    [Merchant Name 2] VARCHAR (MAX) NULL,
    [Merchant Name 3] VARCHAR (MAX) NULL,
    [Init Phone No]   VARCHAR (MAX) NULL,
    [Master]          VARCHAR (MAX) NULL,
    CONSTRAINT [PK_tbUploadTerminal] PRIMARY KEY CLUSTERED ([Id] ASC)
);



