
GO

/****** Object:  Table [dbo].[tbProfileAcquirer]    Script Date: 16/11/2017 17:13:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbProfileAcquirer](
	[AcquirerTagID] [bigint] IDENTITY(1,1) NOT NULL,
	[TerminalID] [varchar](8) NOT NULL,
	[AcquirerName] [varchar](15) NOT NULL,
	[AA001] [varchar](150) NULL,
	[AA002] [varchar](150) NULL,
	[AA003] [varchar](150) NULL,
	[AA004] [varchar](150) NULL,
	[AA005] [varchar](150) NULL,
	[AA006] [varchar](150) NULL,
	[AA007] [varchar](150) NULL,
	[AA008] [varchar](150) NULL,
	[AA009] [varchar](150) NULL,
	[AA010] [varchar](150) NULL,
	[AA011] [varchar](150) NULL,
	[AA012] [varchar](150) NULL,
	[AA013] [varchar](150) NULL,
	[AA014] [varchar](150) NULL,
	[AA015] [varchar](150) NULL,
	[AA016] [varchar](150) NULL,
	[AA017] [varchar](150) NULL,
	[AA018] [varchar](150) NULL,
	[AA019] [varchar](150) NULL,
	[AA020] [varchar](150) NULL,
	[AA021] [varchar](150) NULL,
	[AA022] [varchar](150) NULL,
	[AA023] [varchar](150) NULL,
	[AA024] [varchar](150) NULL,
	[AA025] [varchar](150) NULL,
	[AA026] [varchar](150) NULL,
	[AA027] [varchar](150) NULL,
	[AA028] [varchar](150) NULL,
	[AA029] [varchar](150) NULL,
	[AA030] [varchar](150) NULL,
	[AA031] [varchar](150) NULL,
	[AA032] [varchar](150) NULL,
	[AA033] [varchar](150) NULL,
	[AA034] [varchar](150) NULL,
	[AA035] [varchar](150) NULL,
	[AA036] [varchar](150) NULL,
	[AA037] [varchar](150) NULL,
	[AA038] [varchar](150) NULL,
	[AA039] [varchar](150) NULL,
	[AA040] [varchar](150) NULL,
	[AA041] [varchar](150) NULL,
	[AA042] [varchar](150) NULL,
	[AA043] [varchar](150) NULL,
	[AA044] [varchar](150) NULL,
	[AA045] [varchar](150) NULL,
	[AA046] [varchar](150) NULL,
	[AA047] [varchar](150) NULL,
	[AA048] [varchar](150) NULL,
	[AA049] [varchar](150) NULL,
	[AA050] [varchar](150) NULL,
	[AA051] [varchar](150) NULL,
	[AA052] [varchar](150) NULL,
	[AA053] [varchar](150) NULL,
	[AA054] [varchar](150) NULL,
	[AA055] [varchar](150) NULL,
	[AA056] [varchar](150) NULL,
	[AA057] [varchar](150) NULL,
	[AA058] [varchar](150) NULL,
	[AA059] [varchar](150) NULL,
	[AA065] [varchar](150) NULL,
 CONSTRAINT [PK_Acquirer_New2] PRIMARY KEY CLUSTERED 
(
	[AcquirerTagID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


