﻿CREATE TABLE [dbo].[tbProfileRemoteDownload] (
    [RemoteDownloadTagID]             INT           IDENTITY (1, 1) NOT NULL,
    [RemoteDownloadName]              VARCHAR (50)  NOT NULL,
    [DatabaseID]                      INT           NOT NULL,
    [RemoteDownloadTag]               VARCHAR (5)   NOT NULL,
    [RemoteDownloadLengthOfTagLength] INT           NOT NULL,
    [RemoteDownloadTagLength]         INT           NOT NULL,
    [RemoteDownloadTagValue]          VARCHAR (255) NOT NULL,
    [Description]                     VARCHAR (255) NULL,
    CONSTRAINT [PK_tbRemoteDownload] PRIMARY KEY CLUSTERED ([RemoteDownloadTagID] ASC)
);



