﻿CREATE TABLE [dbo].[tbRD_SoftwareVersionList]
(
	[SVList_ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [SV_ID] INT NOT NULL, 
    [SVList_Name] VARCHAR(50) NOT NULL, 
    [SVList_Content] VARBINARY(MAX) NOT NULL, 
    [CreateDate] DATETIME NOT NULL DEFAULT GETDATE(), 
    [LastModify] DATETIME NULL, 
    [Enabled] BIT NOT NULL DEFAULT ((1))
)
