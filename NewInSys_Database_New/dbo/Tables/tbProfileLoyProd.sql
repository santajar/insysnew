﻿CREATE TABLE [dbo].[tbProfileLoyProd] (
    [LoyProdTagID]             INT           IDENTITY (1, 1) NOT NULL,
    [LoyProdName]              VARCHAR (50)  NOT NULL,
    [DatabaseID]               SMALLINT      NOT NULL,
    [LoyProdTag]               VARCHAR (5)   NOT NULL,
    [LoyProdLengthOfTagLength] INT           NOT NULL,
    [LoyProdTagLength]         INT           NOT NULL,
    [LoyProdTagValue]          VARCHAR (256) NOT NULL,
    [Description]              VARCHAR (255) NULL,
    CONSTRAINT [PK_tbProfileLoyProd] PRIMARY KEY CLUSTERED ([LoyProdTagID] ASC)
);



