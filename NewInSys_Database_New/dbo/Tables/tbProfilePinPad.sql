﻿CREATE TABLE [dbo].[tbProfilePinPad] (
    [PinPadId]                INT           IDENTITY (1, 1) NOT NULL,
    [DatabaseID]              INT           NOT NULL,
    [PinPadName]              VARCHAR (23)  NOT NULL,
    [PinPadTag]               VARCHAR (5)   NOT NULL,
    [PinPadLengthOfTagLength] INT           NOT NULL,
    [PinPadTagLength]         INT           NOT NULL,
    [PinPadTagValue]          VARCHAR (150) NOT NULL,
    CONSTRAINT [PK_tbProfilePinPad] PRIMARY KEY CLUSTERED ([PinPadId] ASC)
);




GO
CREATE NONCLUSTERED INDEX [PinPadName]
    ON [dbo].[tbProfilePinPad]([PinPadId] ASC, [DatabaseID] ASC, [PinPadName] ASC, [PinPadTag] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

