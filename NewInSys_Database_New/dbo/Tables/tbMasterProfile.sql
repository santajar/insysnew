

/****** Object:  Table [dbo].[MasterProfile]    Script Date: 16/11/2017 17:11:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MasterProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DatabaseID] [int] NOT NULL,
	[FormID] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Tag] [varchar](5) NULL,
	[TagValue] [varchar](500) NULL,
 CONSTRAINT [PK_MasterProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


