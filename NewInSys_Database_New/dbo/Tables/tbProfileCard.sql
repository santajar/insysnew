﻿CREATE TABLE [dbo].[tbProfileCard] (
    [CardTagID]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [CardID]                INT           NOT NULL,
    [CardTag]               VARCHAR (5)   NOT NULL,
    [CardLengthOfTagLength] INT           NOT NULL,
    [CardTagLength]         INT           NOT NULL,
    [CardTagValue]          VARCHAR (150) NULL,
    [Description]           VARCHAR (255) NULL,
    CONSTRAINT [PK_Card_1] PRIMARY KEY CLUSTERED ([CardTagID] ASC) ON [Data],
    CONSTRAINT [FK_tbProfileCard_tbProfileCardList] FOREIGN KEY ([CardID]) REFERENCES [dbo].[tbProfileCardList] ([CardID])
);








GO
CREATE NONCLUSTERED INDEX [CardNameTag]
    ON [dbo].[tbProfileCard]([CardID] ASC, [CardTag] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

