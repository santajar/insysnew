﻿CREATE TABLE [dbo].[tbStockEMBatchLine] (
    [ID]             INT            IDENTITY (11000, 1) NOT NULL,
    [EMBatchID]      INT            NOT NULL,
    [CSVLine]        VARCHAR (8000) NULL,
    [Errors]         TEXT           NULL,
    [Status]         BIT            NOT NULL,
    [UserCreationID] VARCHAR (10)   NOT NULL,
    [DateCreation]   DATETIME       NOT NULL,
    [Timezone]       VARCHAR (255)  NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_EMBatchLineBatch] FOREIGN KEY ([EMBatchID]) REFERENCES [dbo].[tbStockEMBatch] ([ID]),
    CONSTRAINT [FK_EMBatchLineUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

