CREATE TABLE [dbo].[tbListTIDRemoteDownload] (
    [IdListTid]  BIGINT      IDENTITY (1, 1) NOT NULL,
    [TerminalID] VARCHAR (8) NOT NULL,
    [IdRegionRD] INT         NOT NULL,
    [IdGroupRD]  INT         NOT NULL,
    [IdCityRD]   INT         NOT NULL,
    [ScheduleID] INT         NOT NULL,
    [AppPackID]  INT         NOT NULL
);








GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20141218-094805]
    ON [dbo].[tbListTIDRemoteDownload]([IdListTid] ASC, [TerminalID] ASC, [IdRegionRD] ASC, [IdGroupRD] ASC, [IdCityRD] ASC)
    ON [Data];

