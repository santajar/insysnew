﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using Threading = System.Threading;
using Excel = Microsoft.Office.Interop.Excel;
using InSysClass;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConverterExceltoxml
{
    public partial class frmConvert : Form
    {
        DataTable dtTempTerminal, dtTempAcquirer, dtTempIssuer, dtTempCard, dtTempTag/*, dtTempItemCombobox*/;
        DataSet dsFullTemplate = new DataSet();
        DataTable dtFullTemplate = new DataTable();
        string sFileType = "xml";
        string sCitySignature;
        int iMaxTerminal;

        class XieParameter
        {
            public string Name { get; set; }
            public int Type { get; set; }
            public int Index { get; set; }
            public DataTable TableParameter { get; set; }
            public int CardAcquirerId { get; set; }
            public int CardIssuerId { get; set; }
        }

        List<XieParameter> listXieParameter = new List<XieParameter>();

        public frmConvert()
        {
            InitializeComponent();
        }

        private void frmConvert_Load(object sender, EventArgs e)
        {
            string sFilename = string.Format(@"{0}\TEMPLATE\full.xml", Environment.CurrentDirectory);            
            dsFullTemplate.ReadXml(sFilename);            
            dtFullTemplate = dsFullTemplate.Tables[6];
            InitXieParameter();
        }

        private void InitXieParameter()
        {
            for (int iType = 2; iType <= 4; iType++)
                for (int iIndex = 1; iIndex <= 50; iIndex++)
                {
                    string sFieldIDFilter = string.Format("fieldId LIKE 'FF0{0}.{1}.%'", iType, iIndex);
                    DataRow[] arrRow = dtFullTemplate.Select(sFieldIDFilter);
                    if (arrRow.Count() > 0)
                    {
                        XieParameter xieparamTemp = new XieParameter();
                        xieparamTemp.Type = iType;
                        xieparamTemp.TableParameter = arrRow.CopyToDataTable();

                        string sTag = iType == 2 ? "AA01" : iType == 3 ? "AE01" : "AC01";

                        xieparamTemp.Name = (xieparamTemp.TableParameter.Select(string.Format("fieldId LIKE '%{0}'", sTag))[0])["value"].ToString();
                        switch (iType)
                        {
                            case 2:
                                xieparamTemp.Index = int.Parse(
                                    (xieparamTemp.TableParameter.Select(string.Format("fieldId LIKE '%AA02'"))[0])["value"].ToString());
                                break;
                            case 3:
                                xieparamTemp.Index = int.Parse(
                                    (xieparamTemp.TableParameter.Select(string.Format("fieldId LIKE '%AE15'"))[0])["value"].ToString());
                                break;
                            case 4:
                                xieparamTemp.CardAcquirerId = int.Parse(
                                    (xieparamTemp.TableParameter.Select(string.Format("fieldId LIKE '%AC06'"))[0])["value"].ToString());
                                xieparamTemp.CardIssuerId = int.Parse(
                                    (xieparamTemp.TableParameter.Select(string.Format("fieldId LIKE '%AC07'"))[0])["value"].ToString());
                                break;
                        }
                        listXieParameter.Add(xieparamTemp);
                    }
                }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            ofdFile.InitialDirectory = Application.StartupPath;
            ofdFile.Filter = "Microsoft Excel Files|*.xlsx|Microsoft Excel 97-2003 Files|*.xls";
            if (ofdFile.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = ofdFile.FileName;
                //New Data Table
                dtTempTerminal = new DataTable();
                dtTempAcquirer = new DataTable();
                dtTempIssuer = new DataTable();
                dtTempCard = new DataTable();
                dtTempTag = new DataTable();
                //dtTempItemCombobox = new DataTable();

                OleDbConnection oOleConn = new OleDbConnection(OleDBConnLib.sConnACE12(txtFile.Text, ""));
                oOleConn.Open();
                //new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Query_1_Terminal]", oOleConn)).Fill(dtTempTerminal);
                //new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Query_2_Acquirer]", oOleConn)).Fill(dtTempAcquirer);
                //new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Query_3_Issuer_All]", oOleConn)).Fill(dtTempIssuer);
                //new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Query_4_Card_All]", oOleConn)).Fill(dtTempCard);
                //new OleDbDataAdapter(new OleDbCommand("SELECT * FROM tbItemList", oOleConn)).Fill(dtTempTag);
                //using excel
                new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Terminal$]", oOleConn)).Fill(dtTempTerminal);
                new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Acquirer$]", oOleConn)).Fill(dtTempAcquirer);
                //new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Issuer$]", oOleConn)).Fill(dtTempIssuer);
                //new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Card$]", oOleConn)).Fill(dtTempCard);
                new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Tag$]", oOleConn)).Fill(dtTempTag);
                //new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [ItemCombobox$]", oOleConn)).Fill(dtTempItemCombobox);
                oOleConn.Close();
            }
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sFileType) && !string.IsNullOrEmpty(txtFile.Text) && !string.IsNullOrEmpty(txtCitySignature.Text))
            {
                iMaxTerminal = int.TryParse(txtMaxTerminal.Text, out iMaxTerminal) ? int.Parse(txtMaxTerminal.Text) : 500;
                sCitySignature = txtCitySignature.Text.ToUpper();
                sFileType = cmbFileType.SelectedItem.ToString().ToLower();
                StartGenerateDataTabletoXML(dtTempTerminal, dtTempAcquirer, dtTempIssuer, dtTempCard, dtTempTag/*, dtTempItemCombobox*/);
                txtFile.Clear();
                txtCitySignature.Clear();
            }
        }

        private void StartGenerateDataTabletoXML(DataTable dtTempTerminal, DataTable dtTempAcquirer, DataTable dtTempIssuer, DataTable dtTempCard, DataTable dtTempTag/*, DataTable dtTempItemCombobox*/)
        {
            int iIndex = 0;
            int iTotalError = 0; 
            int iFileCounter = 0;
            int iTotalTerminal = 0;

            string sFilename = string.Format("{0}_{1}.{2}", ofdFile.SafeFileName.Replace('.', '_'), iFileCounter, sFileType);
            PrepareFileHeader(sFilename);
            foreach (DataRow drTerminal in dtTempTerminal.Rows)
            {
                string sTerminalID = drTerminal["Terminal ID"].ToString();
                iTotalTerminal++;
                if (!string.IsNullOrEmpty(sTerminalID))
                {
                    //string sFilename = string.Format("{0}.{1}", sTerminalID, sFileType);
                    //PrepareFileHeader(sFilename);
                    HeaderTerminal(sTerminalID, sFilename);
                    WriteXML("<parameterSet templateId=\"MDR_INSYS_TMS_01.00\" templateVersion=\"0101\">", sFilename);
                    //Terminal
                    //iTotalError = iTotalError + InsertTerminalTagValue(sTerminalID, dtTempTerminal, dtTempTag/*, dtTempItemCombobox*/);
                    //iTotalError = iTotalError + InsertTerminalTagValue2(sTerminalID, dtTempTerminal, dtTempTag);
                    iTotalError = iTotalError + iInsertTagValue2(sTerminalID, dtTempTerminal, dtTempTag, "FormID=1", 1, sFilename);
                    
                    //Card
                    //iTotalError = iTotalError + InsertCardTagValue(sTerminalID, dtTempCard, dtTempTag/*, dtTempItemCombobox*/);

                    DataTable dtAcquirer = new DataTable();
                    dtAcquirer = dtFullTemplate.Clone();

                    ////Issuer
                    //iTotalError = iTotalError + InsertIssuerTagValue(sTerminalID, dtTempIssuer, dtTempTag/*, dtTempItemCombobox*/);
                    //iTotalError = iTotalError + InsertTagValue2(sTerminalID, dtTempIssuer, dtTempTag, "FormID=3", 3);
                    
                    ////Acquirer
                    //iTotalError = iTotalError + InsertAcquirerTagValue(sTerminalID, dtTempAcquirer, dtTempTag/*, dtTempItemCombobox*/);
                    //iTotalError = iTotalError + InsertTagValue2(sTerminalID, dtTempAcquirer, dtTempTag, "FormID=2", 2);
                    UpdateAcquirerIssuer(sTerminalID, 2, dtTempAcquirer, dtTempTag, ref dtAcquirer);

                    DataTable dtCard = dtFullTemplate.Clone();
                    UpdateCard(dtAcquirer, ref dtCard);

                    if (dtCard != null && dtCard.Rows.Count > 0)
                    {
                        DataTable dtIssuer = dtFullTemplate.Clone();
                        UpdateIssuer(dtCard, ref dtIssuer);

                        if (dtIssuer != null && dtIssuer.Rows.Count > 0)
                            foreach (DataRow rowIssuer in dtIssuer.Rows)
                                dtAcquirer.ImportRow(rowIssuer);

                        if (dtCard != null && dtCard.Rows.Count > 0)
                            foreach (DataRow rowCard in dtCard.Rows)
                                dtAcquirer.ImportRow(rowCard);

                        WriteTableXml(sFilename, dtAcquirer);
                    }
                    WriteXML("</parameterSet>", sFilename);
                    WriteXML("</terminal>", sFilename);
                    iIndex = iIndex + 1;
                }
                if (iTotalTerminal >= iMaxTerminal)
                {
                    WriteXML("</estate>", sFilename);
                    WriteXML("</script>", sFilename);
                    iFileCounter++;
                    sFilename = string.Format("{0}_{1}.{2}", ofdFile.SafeFileName.Replace('.', '_'), iFileCounter, sFileType);
                    PrepareFileHeader(sFilename);
                    iTotalTerminal = 0;
                }
            }
            WriteXML("</estate>", sFilename);
            WriteXML("</script>", sFilename);
            if (iTotalError == 0)
            {
                MessageBox.Show("DONE.");
                //SaveFileDialog sfdXml = new SaveFileDialog();
                //sfdXml.InitialDirectory = string.Format(@"{0}\XML", Environment.CurrentDirectory);
                //sfdXml.Filter = "Xie Files(*.xie)|*.xie|Xml Files(*.xml)|*.xml";
                //sfdXml.ShowDialog();
                //if (!string.IsNullOrEmpty(sfdXml.FileName))
                //{
                //    string sFileResult = string.Format(@"{0}\XML\Ingestate.xml", Environment.CurrentDirectory);
                //    File.Copy(sFileResult, sfdXml.FileName, true);
                //    File.Delete(sFileResult);
                //}
            }
            else
            {
                MessageBox.Show("Some Data file lenght is not valid." + "\n" + "Please see error log.");
            }
            //WriteXML(string.Format("{0} Done", DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt")));
        }

        private void UpdateIssuer(DataTable dtCard, ref DataTable dtIssuer)
        {
            int iCounter = 0;
            foreach (DataRow rowCard in (dtCard.Select("fieldId LIKE '%AC07'")).CopyToDataTable().DefaultView.ToTable(true, "value").Rows)
            {
                int iIssuerId = int.Parse(rowCard["value"].ToString());
                List<XieParameter> listxieTemp = listXieParameter.FindAll(xie => xie.Type == 3 && xie.Index == iIssuerId);
                if (listxieTemp.Count > 0)
                {
                    foreach (XieParameter xieparamTemp in listxieTemp)
                    {
                        DataTable dtTemp = xieparamTemp.TableParameter;
                        foreach (DataRow rowTemp in dtTemp.Rows)
                        {
                            if (rowTemp["fieldId"].ToString().Contains("AE01"))
                                iCounter += 1;
                            string[] arrsFieldId = rowTemp["fieldId"].ToString().Split('.');
                            rowTemp["fieldId"] = string.Format("FF03.{0}.{1}", iCounter, arrsFieldId[2]);
                            dtIssuer.ImportRow(rowTemp);
                        }
                        dtIssuer.AcceptChanges();
                    }
                }
            }
        }

        private void UpdateCard(DataTable dtAcquirer, ref DataTable dtTempCard)
        {
            int iCounter = 0;
            foreach (DataRow rowAcquirer in dtAcquirer.Select("fieldId LIKE '%AA02'"))
            {
                int iAcquirerId = int.Parse(rowAcquirer["value"].ToString());
                List<XieParameter> listxieTemp = listXieParameter.FindAll(xie => xie.CardAcquirerId == iAcquirerId);
                if (listxieTemp.Count > 0)
                {
                    foreach (XieParameter xieparamTemp in listxieTemp)
                    {
                        DataTable dtTemp = xieparamTemp.TableParameter;
                        foreach (DataRow rowTemp in dtTemp.Rows)
                        {
                            if (rowTemp["fieldId"].ToString().Contains("AC01"))
                                iCounter += 1;
                            string[] arrsFieldId = rowTemp["fieldId"].ToString().Split('.');
                            rowTemp["fieldId"] = string.Format("FF04.{0}.{1}", iCounter, arrsFieldId[2]);
                            dtTempCard.ImportRow(rowTemp);
                        }
                        dtTempCard.AcceptChanges();
                    }
                }
            }
        }

        private void UpdateAcquirerIssuer(string sTerminalID, int iFormID, DataTable dtTempExcel, DataTable dtTag, ref DataTable dtAcquirer)
        {
            string sConditionFormID;
            string sConditionTerminalID = string.Format("[SN]='{0}'", sTerminalID);
            
            if (iFormID == 2)
                sConditionFormID = "FormID=2";
            else
                sConditionFormID = "FormID=3";

            int iCounter = 0;
            foreach (DataRow rowTempExcel in dtTempExcel.Select(sConditionTerminalID))
            {
                string sName = iFormID == 2 ? rowTempExcel["Acquirer"].ToString().TrimEnd() : rowTempExcel["Issuer Name"].ToString().TrimEnd();
                int iIndexList = listXieParameter.FindIndex(xie => xie.Name == sName && xie.Type == iFormID);
                if (iIndexList > -1)
                {
                    XieParameter xieparamTemp = listXieParameter[iIndexList];
                    DataTable dtTemp = xieparamTemp.TableParameter;

                    foreach (DataRow rowTag in dtTag.Select(sConditionFormID))
                    {
                        string sTag = rowTag["Tag"].ToString();
                        string sItemName = rowTag["ItemName"].ToString();
                        string sValue = rowTempExcel[sItemName].ToString();
                        ((dtTemp.Select(string.Format("fieldId LIKE '%{0}'", sTag)))[0])["value"] = sValue;
                    }
                    dtTemp.AcceptChanges();

                    foreach (DataRow rowTemp in dtTemp.Rows)
                    {
                        string[] arrsFieldId = rowTemp["fieldId"].ToString().Split('.');
                        if (arrsFieldId[2].Substring(2) == "01") iCounter += 1;
                        string sFieldId = string.Format("{0}.{1}.{2}", arrsFieldId[0], iCounter, arrsFieldId[2]);
                        rowTemp["fieldId"] = sFieldId;
                        dtAcquirer.ImportRow(rowTemp);
                    }
                }
            }
        }

        private void WriteTableXml(string sFilename, DataTable dtTemp)
        {
            foreach (DataRow row in dtTemp.Rows)
            {
                string sParameter = string.Format("<parameter fieldId=\"{0}\" {1} />",
                    row["fieldId"],
                    !string.IsNullOrEmpty(row["value"].ToString()) ? string.Format("value=\"{0}\"", row["value"]) : string.Format("isNull=\"{0}\"", row["isNull"]));
                WriteXML(sParameter, sFilename);
            }
        }

        private int iInsertTagValue2(string sTerminalID, DataTable dtTemp, DataTable dtTempTag, string sConditionTag, int iType, string sFilename)
        {
            int iError = 0; 
            int iIndex = 1;
            DataRow[] arrdrowTag = dtTempTag.Select(sConditionTag);
            DataRow[] arrdrowTerminal = dtTemp.Select(string.Format("[Terminal ID]='{0}'", sTerminalID));
            foreach (DataRow rowTerminal in arrdrowTerminal)
            {
                foreach (DataRow rowTag in arrdrowTag)
                {
                    string sItemName = string.Format("{0}", rowTag["ItemName"]);
                    iError += WriteToXML(sTerminalID, sItemName, rowTerminal[sItemName].ToString(), dtTempTag, iType, iIndex, sFilename);
                }
                iIndex++;
            }
            return iError;
        }

        private int InsertTerminalTagValue2(string sTerminalID, DataTable dtTempTerminal, DataTable dtTempTag)
        {
            int iError = 0;
            DataRow[] arrdrowTag = dtTempTag.Select("FormID=1");
            DataRow[] arrdrowTerminal = dtTempTerminal.Select(string.Format("[Terminal ID]='{0}'", sTerminalID));
            foreach (DataRow rowTerminal in arrdrowTerminal)
            {
                foreach (DataRow rowTag in arrdrowTag)
                {
                    string sItemName = string.Format("{0}", rowTag["ItemName"]);
                    iError += WriteToXML(sTerminalID, sItemName, rowTerminal[sItemName].ToString(), dtTempTag, 1, 0);
                }
            }
            return iError;
        }

        private int InsertTerminalTagValue(string sTerminalID, DataTable dtTempTerminal, DataTable dtTempTag/*, DataTable dtTempItemCombobox*/)
        {
            int iTerminalError = 0;
            string sTerminalIDValue = "", sMerchantName1Value = "", sMerchantName2Value = "", sMerchantName3Value = "", sMerchantName4Value = "", sCategoryValue = "", sMerchantPasswordValue = "", sMaxAmtDigitValue = "", sTimeOfSettlementValue = "", sLoyaltyValue = "", sPowerBuyValue = "", sAdminPasswordValue = "", sInitializationCtrlValue = "", sPushSettleinDaysValue = "", sEMVInformationValue = "", sInitializePasswordValue = "", sECRValue = "", sLoyaltySaleValue = "", sLoyaltyRedeemValue = "", sLoyaltyCardBalValue = "", sLoyaltyVoidValue = "", sLoyaltyNIIValue = "", sMonitorNIIValue = "", sMerchantAcc1Value = "", sMerchantAcc2Value = "", sMerchantAcc3Value = "", sMerchantAcc4Value = "", sDCCNIIValue = "", sDCCEnableValue = "", sOfflineEnableValue = "", sPreauthEnableValue = "", sManualEnableValue = "", sRefundEnableValue = "", sVoidEnableValue = "", sPrepaidEnableValue = "", sMiniatmEnableValue = "", sTransferEnableValue = "", sCashEnableValue = "", sPinPromtLimitValue = "", sMaskingPanAuditValue = "", sExpDateAuditValue = "", sPinSamValue = "", sCountRefundValue = "", sAmountRefundValue = "", sMaskingMerchantCopyValue = "", sMaskingCustomerCopyValue = "", sMaskingBankCopyValue = "", sMaxSOACommisionValue = "";
            string sDE01Name = "", sDE02Name = "", sDE03Name = "", sDE04Name = "", sDE05Name = "", sDE06Name = "", sDE07Name = "", sDE08Name = "", sDE09Name = "", sDE10Name = "", sDE11Name = "", sDE12Name = "", sDE13Name = "", sDE14Name = "", sDE15Name = "", sDE16Name = "", sDE17Name = "", sDE18Name = "", sDE19Name = "", sDE20Name = "", sDE21Name = "", sDE22Name = "", sDE23Name = "", sDE24Name = "", sDE25Name = "", sDE26Name = "", sDE27Name = "", sDE28Name = "", sDE29Name = "", sDE30Name = "", sDE31Name = "", sDE32Name = "", sDE33Name = "", sDE34Name = "", sDE35Name = "", sDE36Name = "", sDE37Name = "", sDE38Name = "", sDE39Name = "", sDE40Name = "", sDE41Name = "", sDE42Name = "", sDE43Name = "", sDE44Name = "", sDE45Name = "", sDE46Name = "", sDE47Name = "", sDE48Name = "";
            dtTempTag.Select()
                    .ToList<DataRow>()
                    .ForEach(Item =>
                        {
                            if (Item["Tag"].ToString() == "DE01")
                                sDE01Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE02")
                                sDE02Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE03")
                                sDE03Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE04")
                                sDE04Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE05")
                                sDE05Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE06")
                                sDE06Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE07")
                                sDE07Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE08")
                                sDE08Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE09")
                                sDE09Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE10")
                                sDE10Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE11")
                                sDE11Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE12")
                                sDE12Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE13")
                                sDE13Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE14")
                                sDE14Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE15")
                                sDE15Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE16")
                                sDE16Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE17")
                                sDE17Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE18")
                                sDE18Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE19")
                                sDE19Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE20")
                                sDE20Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE21")
                                sDE21Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE22")
                                sDE22Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE23")
                                sDE23Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE24")
                                sDE24Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE25")
                                sDE25Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE26")
                                sDE26Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE27")
                                sDE27Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE28")
                                sDE28Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE29")
                                sDE29Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE30")
                                sDE30Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE31")
                                sDE31Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE32")
                                sDE32Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE33")
                                sDE33Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE34")
                                sDE34Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE35")
                                sDE35Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE36")
                                sDE36Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE37")
                                sDE37Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE38")
                                sDE38Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE39")
                                sDE39Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE40")
                                sDE40Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE41")
                                sDE41Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE42")
                                sDE42Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE43")
                                sDE43Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE44")
                                sDE44Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE45")
                                sDE45Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE46")
                                sDE46Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE47")
                                sDE47Name = Item["ItemName"].ToString();
                            else if (Item["Tag"].ToString() == "DE48")
                                sDE48Name = Item["ItemName"].ToString();
                        });

            dtTempTerminal.Select(string.Format("[Terminal ID]= '{0}'", sTerminalID))
                .ToList<DataRow>()
                .ForEach(Terminal =>
                {
                    if (dtTempTerminal.Columns.Contains(sDE01Name))
                        sTerminalIDValue = Terminal[sDE01Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE02Name))
                        sMerchantName1Value = Terminal[sDE02Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE03Name))
                        sMerchantName2Value = Terminal[sDE03Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE04Name))
                        sMerchantName3Value = Terminal[sDE04Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE05Name))
                        sMerchantName4Value = Terminal[sDE05Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE06Name))
                        sCategoryValue = Terminal[sDE06Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE07Name))
                        sMerchantPasswordValue = Terminal[sDE07Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE08Name))
                        sMaxAmtDigitValue = Terminal[sDE08Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE09Name))
                        sTimeOfSettlementValue = Terminal[sDE09Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE10Name))
                        sLoyaltyValue = Terminal[sDE10Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE11Name))
                        sPowerBuyValue = Terminal[sDE11Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE12Name))
                        sAdminPasswordValue = Terminal[sDE12Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE13Name))
                        sInitializationCtrlValue = Terminal[sDE13Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE14Name))
                        sPushSettleinDaysValue = Terminal[sDE14Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE15Name))
                        sEMVInformationValue = Terminal[sDE15Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE16Name))
                        sInitializePasswordValue = Terminal[sDE16Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE17Name))
                        sECRValue = Terminal[sDE17Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE18Name))
                        sLoyaltySaleValue = Terminal[sDE18Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE19Name))
                        sLoyaltyRedeemValue = Terminal[sDE19Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE20Name))
                        sLoyaltyCardBalValue = Terminal[sDE20Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE21Name))
                        sLoyaltyVoidValue = Terminal[sDE21Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE22Name))
                        sLoyaltyNIIValue = Terminal[sDE22Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE23Name))
                        sMonitorNIIValue = Terminal[sDE23Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE24Name))
                        sMerchantAcc1Value = Terminal[sDE24Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE25Name))
                        sMerchantAcc2Value = Terminal[sDE25Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE26Name))
                        sMerchantAcc3Value = Terminal[sDE26Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE27Name))
                        sMerchantAcc4Value = Terminal[sDE27Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE28Name))
                        sDCCNIIValue = Terminal[sDE28Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE29Name))
                        sDCCEnableValue = Terminal[sDE29Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE30Name))
                        sOfflineEnableValue = Terminal[sDE30Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE31Name))
                        sPreauthEnableValue = Terminal[sDE31Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE32Name))
                        sManualEnableValue = Terminal[sDE32Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE33Name))
                        sRefundEnableValue = Terminal[sDE33Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE34Name))
                        sVoidEnableValue = Terminal[sDE34Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE35Name))
                        sPrepaidEnableValue = Terminal[sDE35Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE36Name))
                        sMiniatmEnableValue = Terminal[sDE36Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE37Name))
                        sTransferEnableValue = Terminal[sDE37Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE38Name))
                        sCashEnableValue = Terminal[sDE38Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE39Name))
                        sPinPromtLimitValue = Terminal[sDE39Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE40Name))
                        sMaskingPanAuditValue = Terminal[sDE40Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE41Name))
                        sExpDateAuditValue = Terminal[sDE41Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE42Name))
                        sPinSamValue = Terminal[sDE42Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE43Name))
                        sCountRefundValue = Terminal[sDE43Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE44Name))
                        sAmountRefundValue = Terminal[sDE44Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE45Name))
                        sMaskingMerchantCopyValue = Terminal[sDE45Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE46Name))
                        sMaskingCustomerCopyValue = Terminal[sDE46Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE47Name))
                        sMaskingBankCopyValue = Terminal[sDE47Name].ToString();
                    if (dtTempTerminal.Columns.Contains(sDE48Name))
                        sMaxSOACommisionValue = Terminal[sDE48Name].ToString();
                });

            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE01Name, sTerminalIDValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE02Name, sMerchantName1Value, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE03Name, sMerchantName2Value, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE04Name, sMerchantName3Value, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE05Name, sMerchantName4Value, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE06Name, sCategoryValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE07Name, sMerchantPasswordValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE08Name, sMaxAmtDigitValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE09Name, sTimeOfSettlementValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE10Name, sLoyaltyValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE11Name, sPowerBuyValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE12Name, sAdminPasswordValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE13Name, sInitializationCtrlValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE14Name, sPushSettleinDaysValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE15Name, sEMVInformationValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE16Name, sInitializePasswordValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE17Name, sECRValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE18Name, sLoyaltySaleValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE19Name, sLoyaltyRedeemValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE20Name, sLoyaltyCardBalValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE21Name, sLoyaltyVoidValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE22Name, sLoyaltyNIIValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE23Name, sMonitorNIIValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE24Name, sMerchantAcc1Value, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE25Name, sMerchantAcc2Value, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE26Name, sMerchantAcc3Value, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE27Name, sMerchantAcc4Value, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE28Name, sDCCNIIValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE29Name, sDCCEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE30Name, sOfflineEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE31Name, sPreauthEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE32Name, sManualEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE33Name, sRefundEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE34Name, sVoidEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE35Name, sPrepaidEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE36Name, sMiniatmEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE37Name, sTransferEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE38Name, sCashEnableValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE39Name, sPinPromtLimitValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE40Name, sMaskingPanAuditValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE41Name, sExpDateAuditValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE42Name, sPinSamValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE43Name, sCountRefundValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE44Name, sAmountRefundValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE45Name, sMaskingMerchantCopyValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE46Name, sMaskingCustomerCopyValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE47Name, sMaskingBankCopyValue, dtTempTag, 1, 0);
            iTerminalError = iTerminalError + WriteToXML(sTerminalID, sDE48Name, sMaxSOACommisionValue, dtTempTag, 1, 0);

            return iTerminalError;
        }

        private int InsertAcquirerTagValue(string sTerminalID, DataTable dtTempAcquirer, DataTable dtTempTag/*, DataTable dtTempItemCombobox*/)
        {
            int iIndex = 1, iAcquirerError = 0;
            string sAA01Value = "", sAA02Value = "", sAA03Value = "", sAA04Value = "", sAA05Value = "", sAA06Value = "", sAA07Value = "", sAA08Value = "", sAA09Value = "", sAA10Value = "", sAA11Value = "", sAA12Value = "", sAA13Value = "", sAA14Value = "", sAA15Value = "", sAA16Value = "", sAA17Value = "", sAA18Value = "", sAA19Value = "", sAA20Value = "";
            string sAA01Name = "", sAA02Name = "", sAA03Name = "", sAA04Name = "", sAA05Name = "", sAA06Name = "", sAA07Name = "", sAA08Name = "", sAA09Name = "", sAA10Name = "", sAA11Name = "", sAA12Name = "", sAA13Name = "", sAA14Name = "", sAA15Name = "", sAA16Name = "", sAA17Name = "", sAA18Name = "", sAA19Name = "", sAA20Name = "";
            dtTempTag.Select()
                    .ToList<DataRow>()
                    .ForEach(Item =>
                        {
                            if (Item["Tag"].ToString() == "AA01")
                                sAA01Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA02")
                                sAA02Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA03")
                                sAA03Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA04")
                                sAA04Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA05")
                                sAA05Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA06")
                                sAA06Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA07")
                                sAA07Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA08")
                                sAA08Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA09")
                                sAA09Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA10")
                                sAA10Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA11")
                                sAA11Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA12")
                                sAA12Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA13")
                                sAA13Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA14")
                                sAA14Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA15")
                                sAA15Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA16")
                                sAA16Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA17")
                                sAA17Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA18")
                                sAA18Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA19")
                                sAA19Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AA20")
                                sAA20Name = Item["ItemName"].ToString();
                        });

            dtTempAcquirer.Select(string.Format("[Terminal ID] = '{0}'", sTerminalID))
                .ToList<DataRow>()
                .ForEach(Acquirer =>
                    {
                        if (dtTempAcquirer.Columns.Contains(sAA01Name))
                        {
                            sAA01Value = Acquirer[sAA01Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA01Name, sAA01Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA02Name))
                        {
                            sAA02Value = Acquirer[sAA02Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA02Name, sAA02Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA03Name))
                        {
                            sAA03Value = Acquirer[sAA03Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA03Name, sAA03Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA04Name))
                        {
                            sAA04Value = Acquirer[sAA04Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA04Name, sAA04Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA05Name))
                        {
                            sAA05Value = Acquirer[sAA05Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA05Name, sAA05Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA06Name))
                        {
                            sAA06Value = Acquirer[sAA06Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA06Name, sAA06Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA07Name))
                        {
                            sAA07Value = Acquirer[sAA07Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA07Name, sAA07Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA08Name))
                        {
                            sAA08Value = Acquirer[sAA08Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA08Name, sAA08Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA09Name))
                        {
                            sAA09Value = Acquirer[sAA09Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA09Name, sAA09Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA10Name))
                        {
                            sAA10Value = Acquirer[sAA10Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA10Name, sAA10Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA11Name))
                        {
                            sAA11Value = Acquirer[sAA11Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA11Name, sAA11Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA12Name))
                        {
                            sAA12Value = Acquirer[sAA12Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA12Name, sAA12Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA13Name))
                        {
                            sAA13Value = Acquirer[sAA13Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA13Name, sAA13Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA14Name))
                        {
                            sAA14Value = Acquirer[sAA14Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA14Name, sAA14Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA15Name))
                        {
                            sAA15Value = Acquirer[sAA15Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA15Name, sAA15Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA16Name))
                        {
                            sAA16Value = Acquirer[sAA16Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA16Name, sAA16Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA17Name))
                        {
                            sAA17Value = Acquirer[sAA17Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA17Name, sAA17Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA18Name))
                        {
                            sAA18Value = Acquirer[sAA18Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA18Name, sAA18Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA19Name))
                        {
                            sAA19Value = Acquirer[sAA19Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA19Name, sAA19Value, dtTempTag, 2, iIndex);
                        }
                        if (dtTempAcquirer.Columns.Contains(sAA20Name))
                        {
                            sAA20Value = Acquirer[sAA20Name].ToString();
                            iAcquirerError = iAcquirerError + WriteToXML(sTerminalID, sAA20Name, sAA20Value, dtTempTag, 2, iIndex);
                        }
                        iIndex = iIndex + 1;
                    });

            return iAcquirerError;
        }

        private int InsertIssuerTagValue(string sTerminalID, DataTable dtTempIssuer, DataTable dtTempTag/*, DataTable dtTempItemCombobox*/)
        {
            int iIndex = 1, iIssuerError = 0;
            string sAE01Value = "", sAE02Value = "", sAE03Value = "", sAE04Value = "", sAE05Value = "", sAE06Value = "", sAE07Value = "", sAE08Value = "", sAE09Value = "", sAE10Value = "", sAE11Value = "", sAE12Value = "", sAE13Value = "", sAE14Value = "", sAE15Value = "", sAE16Value = "", sAE17Value = "", sAE18Value = "", sAE19Value = "", sAE20Value = "", sAE21Value = "", sAE22Value = "", sAE23Value = "";
            string sAE01Name = "", sAE02Name = "", sAE03Name = "", sAE04Name = "", sAE05Name = "", sAE06Name = "", sAE07Name = "", sAE08Name = "", sAE09Name = "", sAE10Name = "", sAE11Name = "", sAE12Name = "", sAE13Name = "", sAE14Name = "", sAE15Name = "", sAE16Name = "", sAE17Name = "", sAE18Name = "", sAE19Name = "", sAE20Name = "", sAE21Name = "", sAE22Name = "", sAE23Name = "";
            dtTempTag.Select()
                    .ToList<DataRow>()
                    .ForEach(Item =>
                        {
                            if (Item["Tag"].ToString() == "AE01")
                                sAE01Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE02")
                                sAE02Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE03")
                                sAE03Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE04")
                                sAE04Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE05")
                                sAE05Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE06")
                                sAE06Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE07")
                                sAE07Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE08")
                                sAE08Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE09")
                                sAE09Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE10")
                                sAE10Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE11")
                                sAE11Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE12")
                                sAE12Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE13")
                                sAE13Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE14")
                                sAE14Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE15")
                                sAE15Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE16")
                                sAE16Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE17")
                                sAE17Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE18")
                                sAE18Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE19")
                                sAE19Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE20")
                                sAE20Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE21")
                                sAE21Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE22")
                                sAE22Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AE23")
                                sAE23Name = Item["ItemName"].ToString();
                        });

            dtTempIssuer.Select(string.Format("[Terminal ID] = '{0}'", sTerminalID))
                .ToList<DataRow>()
                .ForEach(Issuer =>
                    {
                        if (dtTempIssuer.Columns.Contains(sAE01Name))
                        {
                            sAE01Value = Issuer[sAE01Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE01Name, sAE01Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE02Name))
                        {
                            sAE02Value = Issuer[sAE02Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE02Name, sAE02Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE03Name))
                        {
                            sAE03Value = Issuer[sAE03Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE03Name, sAE03Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE04Name))
                        {
                            sAE04Value = Issuer[sAE04Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE04Name, sAE04Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE05Name))
                        {
                            sAE05Value = Issuer[sAE05Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE05Name, sAE05Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE06Name))
                        {
                            sAE06Value = Issuer[sAE06Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE06Name, sAE06Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE07Name))
                        {
                            sAE07Value = Issuer[sAE07Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE07Name, sAE07Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE08Name))
                        {
                            sAE08Value = Issuer[sAE08Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE08Name, sAE08Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE09Name))
                        {
                            sAE09Value = Issuer[sAE09Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE09Name, sAE09Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE10Name))
                        {
                            sAE10Value = Issuer[sAE10Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE10Name, sAE10Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE11Name))
                        {
                            sAE11Value = Issuer[sAE11Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE11Name, sAE11Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE12Name))
                        {
                            sAE12Value = Issuer[sAE12Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE12Name, sAE12Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE13Name))
                        {
                            sAE13Value = Issuer[sAE13Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE13Name, sAE13Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE14Name))
                        {
                            sAE14Value = Issuer[sAE14Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE14Name, sAE14Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE15Name))
                        {
                            sAE15Value = Issuer[sAE15Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE15Name, sAE15Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE16Name))
                        {
                            sAE16Value = Issuer[sAE16Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE16Name, sAE16Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE17Name))
                        {
                            sAE17Value = Issuer[sAE17Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE17Name, sAE17Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE18Name))
                        {
                            sAE18Value = Issuer[sAE18Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE18Name, sAE18Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE19Name))
                        {
                            sAE19Value = Issuer[sAE19Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE19Name, sAE19Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE20Name))
                        {
                            sAE20Value = Issuer[sAE20Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE20Name, sAE20Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE21Name))
                        {
                            sAE21Value = Issuer[sAE21Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE21Name, sAE21Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE22Name))
                        {
                            sAE22Value = Issuer[sAE22Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE22Name, sAE22Value, dtTempTag, 3, iIndex);
                        }
                        if (dtTempIssuer.Columns.Contains(sAE23Name))
                        {
                            sAE23Value = Issuer[sAE23Name].ToString();
                            iIssuerError = iIssuerError + WriteToXML(sTerminalID, sAE23Name, sAE23Value, dtTempTag, 3, iIndex);
                        }
                        iIndex = iIndex + 1;
                    });

            return iIssuerError;
        }

        private int InsertCardTagValue(string sTerminalID, DataTable dtTempCard, DataTable dtTempTag/*, DataTable dtTempItemCombobox*/)
        {
            int iIndex = 1, iCardError = 0;
            string sAC01Value = "", sAC02Value = "", sAC03Value = "", sAC04Value = "", sAC05Value = "", sAC06Value = "", sAC07Value = "";
            string sAC01Name = "", sAC02Name = "", sAC03Name = "", sAC04Name = "", sAC05Name = "", sAC06Name = "", sAC07Name = "";
            dtTempTag.Select()
                    .ToList<DataRow>()
                    .ForEach(Item =>
                        {
                            if (Item["Tag"].ToString() == "AC01")
                                sAC01Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AC02")
                                sAC02Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AC03")
                                sAC03Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AC04")
                                sAC04Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AC05")
                                sAC05Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AC06")
                                sAC06Name = Item["ItemName"].ToString();
                            if (Item["Tag"].ToString() == "AC07")
                                sAC07Name = Item["ItemName"].ToString();
                        });
            dtTempCard.Select(string.Format("[Terminal ID] = '{0}'", sTerminalID))
                .ToList<DataRow>()
                .ForEach(Card =>
                    {
                        if (dtTempCard.Columns.Contains(sAC01Name))
                        {
                            sAC01Value = Card[sAC01Name].ToString();
                            iCardError = iCardError + WriteToXML(sTerminalID, sAC01Name, sAC01Value, dtTempTag, 4, iIndex);
                        }
                        if (dtTempCard.Columns.Contains(sAC02Name))
                        {
                            sAC02Value = Card[sAC02Name].ToString();
                            iCardError = iCardError + WriteToXML(sTerminalID, sAC02Name, sAC02Value, dtTempTag, 4, iIndex);
                        }
                        if (dtTempCard.Columns.Contains(sAC03Name))
                        {
                            sAC03Value = Card[sAC03Name].ToString();
                            iCardError = iCardError + WriteToXML(sTerminalID, sAC03Name, sAC03Value, dtTempTag, 4, iIndex);
                        }
                        if (dtTempCard.Columns.Contains(sAC04Name))
                        {
                            sAC04Value = Card[sAC04Name].ToString();
                            iCardError = iCardError + WriteToXML(sTerminalID, sAC04Name, sAC04Value, dtTempTag, 4, iIndex);
                        }
                        if (dtTempCard.Columns.Contains(sAC05Name))
                        {
                            sAC05Value = Card[sAC05Name].ToString();
                            iCardError = iCardError + WriteToXML(sTerminalID, sAC05Name, sAC05Value, dtTempTag, 4, iIndex);
                        }
                        if (dtTempCard.Columns.Contains(sAC06Name))
                        {
                            sAC06Value = Card[sAC06Name].ToString();
                            iCardError = iCardError + WriteToXML(sTerminalID, sAC06Name, sAC06Value, dtTempTag, 4, iIndex);
                        }
                        if (dtTempCard.Columns.Contains(sAC07Name))
                        {
                            sAC07Value = Card[sAC07Name].ToString();
                            iCardError = iCardError + WriteToXML(sTerminalID, sAC07Name, sAC07Value, dtTempTag, 4, iIndex);
                        }
                        iIndex = iIndex + 1;
                    });

            return iCardError;
        }

        private int WriteToXML(string sTerminalID, string sItemName, string sTagValue, DataTable dtTempTag, int iType, int iIndex, string sFilename)
        {
            string sTag = "";
            string sType, sID = "", sTypeColumn = "";
            int iMinLenght = 0, iMaxLenght = 0;
            string /*sFormat,*/sFormatFull = "";
            int isError = 0;
            if (iType == 1) { sType = "DE"; }
            else if (iType == 2) { sType = "AA"; sID = "FF02"; }
            else if (iType == 3) { sType = "AE"; sID = "FF03"; }
            else if (iType == 4) { sType = "AC"; sID = "FF04"; }
            else { sType = "AD"; sID = "ENUMAD"; }

            dtTempTag.Select(string.Format("ItemName = '{0}' AND Tag LIKE '{1}%'", sItemName, sType))
                .ToList<DataRow>()
                .ForEach(ItemList =>
                {
                    sTag = ItemList["Tag"].ToString();
                    if (string.IsNullOrEmpty(sTagValue) || sTagValue.Length == 0)
                    { sTagValue = ItemList["Default Value"].ToString(); }
                    sTypeColumn = ItemList["Type Column"].ToString();
                    iMinLenght = Convert.ToInt32(ItemList["MinLenght"].ToString());
                    iMaxLenght = Convert.ToInt32(ItemList["MaxLenght"].ToString());
                    sFormatFull = ItemList["Type"].ToString();

                });
            //Get Value Berdasarkan Type
            if (string.IsNullOrEmpty(sTag) || sTag.Length != 0)
            {

                if (sTagValue.Length >= iMinLenght && sTagValue.Length <= iMaxLenght)
                    isError = 0;
                else
                    isError = WriteErrorLog(string.Format("{0} Lenght is not valid", sTag), sTerminalID, sItemName);

                if (sTagValue.Contains('&')) sTagValue = sTagValue.Replace("&", "&amp;");
                if (sTagValue.Contains('<')) sTagValue = sTagValue.Replace("<", "&lt;");
                if (sTagValue.Contains('>')) sTagValue = sTagValue.Replace(">", "&gt;");
                if (sTagValue.Contains('"')) 
                    sTagValue = sTagValue.Replace("\"", "&quot;");

                if (iType == 1)
                    WriteXML(string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", sTag, sTagValue), sFilename);
                else
                    WriteXML(string.Format("<parameter fieldId=\"{0}.{1}.{2}\" value=\"{3}\"/>", sID, iIndex, sTag, sTagValue), sFilename);
            }
            return isError;
        }

        private int WriteToXML(string sTerminalID, string sItemName, string sTagValue, DataTable dtTempTag, int iType, int iIndex)
        {
            string sTag = "";
            string sType, sID = "", sTypeColumn = "";
            int iMinLenght = 0, iMaxLenght = 0;
            string /*sFormat,*/sFormatFull = "";
            int isError = 0;
            if (iType == 1) { sType = "DE"; }
            else if (iType == 2) { sType = "AA"; sID = "FF02"; }
            else if (iType == 3) { sType = "AE"; sID = "FF03"; }
            else if (iType == 4) { sType = "AC"; sID = "FF04"; }
            else { sType = "AD"; sID = "ENUMAD"; }

            dtTempTag.Select(string.Format("ItemName = '{0}' AND Tag LIKE '{1}%'", sItemName, sType))
                .ToList<DataRow>()
                .ForEach(ItemList =>
                {
                    sTag = ItemList["Tag"].ToString();
                    if (string.IsNullOrEmpty(sTagValue) || sTagValue.Length == 0)
                    { sTagValue = ItemList["Default Value"].ToString(); }
                    sTypeColumn = ItemList["Type Column"].ToString();
                    iMinLenght = Convert.ToInt32(ItemList["MinLenght"].ToString());
                    iMaxLenght = Convert.ToInt32(ItemList["MaxLenght"].ToString());
                    sFormatFull = ItemList["Type"].ToString();

                });
            //Get Value Berdasarkan Type
            if (string.IsNullOrEmpty(sTag) || sTag.Length != 0)
            {

                if (sTagValue.Length >= iMinLenght && sTagValue.Length <= iMaxLenght)
                    isError = 0;
                else
                    isError = WriteErrorLog(string.Format("{0} Lenght is not valid", sTag), sTerminalID, sItemName);

                if (iType == 1)
                    WriteXML(string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", sTag, sTagValue));
                else
                    WriteXML(string.Format("<parameter fieldId=\"{0}.{1}.{2}\" value=\"{3}\"/>", sID, iIndex, sTag, sTagValue));
            }
            return isError;
        }

        //public static bool isFormatValid(string sText, string sFormat)
        //{
        //    string sValidInput = "!@#$%&*()_+-={}[]:\";'<>,.?|/\\ ";

        //    switch (sFormat)
        //    {
        //        case "H": return Regex.IsMatch(sText, "^[a-fA-F0-9]+$"); // Hexadecimal
        //        case "A": return Regex.IsMatch(sText, "^[a-zA-Z0-9]+$"); // Aplhanumeric
        //        case "A1": return Regex.IsMatch(sText, "^[a-zA-Z0-9\\s]+$"); // Alphanumeric and white space
        //        case "A2": return Regex.IsMatch(sText, "^[a-zA-Z0-9]+$"); // Aplhanumeric in asterix display
        //        case "N": return Regex.IsMatch(sText, "^[0-9]+$"); // Numeric
        //        case "N1": return Regex.IsMatch(sText, "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])$"); // Ip address
        //        case "S": // Special characters
        //            foreach (char c in sText.Trim().ToUpper().ToCharArray())
        //                if (!sValidInput.Contains(c.ToString()) &&
        //                    (c < 'A' || c > 'Z') &&
        //                    (c < '0' || c > '9')) return false;
        //            return true;
        //        case "T": return CommonLib.isValidDatetime(sText); // Datetime
        //    }
        //    return false;
        //}

        private int WriteErrorLog(string sMessage, string sTerminalID, string sItemName)
        {

            string sXMLDirectory = string.Format(@"{0}\XML", Environment.CurrentDirectory);
            if (!Directory.Exists(sXMLDirectory))
                Directory.CreateDirectory(sXMLDirectory);
            string sFilelog = string.Format(@"{0}\{1}.log", sXMLDirectory, "ErrorLog");
            using (StreamWriter sw = new StreamWriter(sFilelog, true))
            {
                sw.WriteLine(string.Format("{0}, {1} {2}", sTerminalID, sItemName, sMessage));
            }
            return 1;
        }

        private void HeaderTerminal(string sTerminalID, string sFilename)
        {
            WriteXML(string.Format("<terminal signature=\"{0}\">", sTerminalID), sFilename);
            WriteXML(string.Format("<name value=\"Insys - {0}\"/>", sTerminalID),sFilename);
            WriteXML("<status value=\"enabled\"/>", sFilename);
            WriteXML("<type value=\"ISO8583_INSYS\"/>", sFilename);
            WriteXML("", sFilename);
            //WriteXML("<task name=\"Download Parameters\">");
            //WriteXML("<status value=\"enabled\"/>");
            //WriteXML("<priority value=\"500\"/>");
            //WriteXML("<permanent value=\"true\"/>");
            //WriteXML("<description>");
            //WriteXML("</description>");
            //WriteXML("<scenario>");
            //WriteXML("<freeText>");
            //WriteXML("<![CDATA[");
            //WriteXML("<SCENARIO>");
            //WriteXML("<downloadParameters application=\"\" templateId=\"MDR_INSYS_TMS_01.00\" templateVersion=\"0100\" />");
            //WriteXML("</SCENARIO>");
            //WriteXML("]]>");
            //WriteXML("</freeText>");
            //WriteXML("</scenario>");
            //WriteXML("</task>");
        }

        private void PrepareFileHeader(string sFilename)
        {
            File.Delete(string.Format(@"{0}\XML\{1}", Environment.CurrentDirectory, sFilename));
            //WriteXML(string.Format("{0} Start", DateTime.Now.ToStrring("MM/dd/yyyy hh:mm:ss.fff tt")));
            WriteXML("<?xml version=\"1.0\" encoding=\"utf-8\"?>", sFilename);
            WriteXML("<script version=\"1.0\">", sFilename);
            WriteXML("<context>", sFilename);
            WriteXML("<estate signature=\"SWI\" />", sFilename);
            WriteXML("</context>", sFilename);
            WriteXML(string.Format("<estate signature=\"{0}\">", sCitySignature), sFilename);
            WriteXML(string.Format("<name value=\"{0}\"/>", sCitySignature), sFilename);
            WriteXML("<status value=\"enabled\"/>", sFilename);
            WriteXML("<type value=\"ISO8583_INSYS\"/>", sFilename);
        }

        private void WriteXML(string sMessage, string sFilename)
        {
            string sXMLDirectory = string.Format(@"{0}\XML", Environment.CurrentDirectory);
            if (!Directory.Exists(sXMLDirectory))
                Directory.CreateDirectory(sXMLDirectory);
            string sFilelog = string.Format(@"{0}\{1}", sXMLDirectory, sFilename);
            using (StreamWriter sw = new StreamWriter(sFilelog, true))
            {
                sw.WriteLine(string.Format("{0}", sMessage));
            }
        }

        private void WriteXML(string sMessage)
        {
            string sXMLDirectory = string.Format(@"{0}\XML", Environment.CurrentDirectory);
            if (!Directory.Exists(sXMLDirectory))
                Directory.CreateDirectory(sXMLDirectory);
            string sFilelog = string.Format(@"{0}\Ingestate.xml", sXMLDirectory);
            using (StreamWriter sw = new StreamWriter(sFilelog, true))
            {
                sw.WriteLine(string.Format("{0}", sMessage));
            }
        }
    }
}