﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using InSysClass;
using Ini;

namespace ConsoleMigrasi316SSI
{
    class Program
    {
        static SqlConnection oSqlConn = new SqlConnection();

        static void Main(string[] args)
        {
            string sTargetDatabaseID;
            string sSourceDatabaseID;
            Console.Write("Target DatabaseID : "); 
            sTargetDatabaseID = Console.ReadLine();
            Console.Write("Source DatabaseID : ");
            sSourceDatabaseID = Console.ReadLine();
            if (!string.IsNullOrEmpty(sTargetDatabaseID) && !string.IsNullOrEmpty(sSourceDatabaseID))
            {
                InitConfig();
                DataTable dtTempTerminal = new DataTable();
                dtTempTerminal = dtTerminal(sSourceDatabaseID);
                foreach (DataRow row in dtTempTerminal.Rows)
                {
                    string sTerminalID = row["TerminalId"].ToString();
                    string sContent = sTerminalContent(sTerminalID);
                    string sMessage = string.Format("{0}, {1}", sTerminalID, string.IsNullOrEmpty(sContent) ? "kosong" : "isi");
                }
            }
            Console.Read();
        }

        static void InitConfig()
        {
            IniFile oIni = new IniFile(Environment.CurrentDirectory + @"\app.config");
            string sServer = oIni.IniReadValue("Database", "Server");
            string sDatabase = oIni.IniReadValue("Database", "Database");
            string sUser = oIni.IniReadValue("Database", "User");
            string sPwd = oIni.IniReadValue("Database", "Pwd");
            oSqlConn = new SqlConnection(SQLConnLib.sConnStringAsyncMARS(sServer, sDatabase, sUser, sPwd));
            try
            {
                oSqlConn.Open();
            }
            catch (Exception ex) 
            {
                Console.WriteLine("Error : {0}", ex.Message);
            }
        }

        static void WriteStatus(string sMessage)
        {
            Console.WriteLine("[{0:MMM dd, yyyy, hh:mm:ss.fff tt}] Error : {1}", DateTime.Now, sMessage);
        }

        static DataTable dtTerminal(string sDatabaseId)
        {
            DataTable dtTemp = new DataTable();
            string sCommand = string.Format("SELECT TerminalID FROM tbProfileTerminalList WHERE DatabaseId={0} ORDER BY TerminalId", sDatabaseId);
            SqlCommand oCmd = new SqlCommand(sCommand, oSqlConn);
            if (oSqlConn.State != ConnectionState.Open)
                oSqlConn.Open();
            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.HasRows)
                {
                    dtTemp.Load(oRead);
                }
            }
            return dtTemp;
        }

        static string sTerminalContent(string sTerminalId)
        {
            string sContent = "";
            SqlCommand oCmd = new SqlCommand("spProfileText", oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.CommandTimeout = 60000;
            oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalId;
            oCmd.Parameters.Add("@sContent", SqlDbType.VarChar,1000000);
            oCmd.Parameters["@sContent"].Direction = ParameterDirection.Output;
            if (oSqlConn.State != ConnectionState.Open)
                oSqlConn.Open();
            oCmd.ExecuteNonQuery();
            sContent = oCmd.Parameters["@sContent"].Value.ToString();
            return sContent;
        }
    }
}
