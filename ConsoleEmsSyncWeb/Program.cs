using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using ipXML;
using InSysClass;
using Ini;

namespace ConsoleSyncWebEms
{
    class Program
    {
        const string ArgTerminalId = "-T";
        const string ArgCommand = "-C";
        const string ArgPath = "-P";

        enum TypeCommand
        {
            A,  //AddUpdate,
            D,  //Delete
            I,  //Inquiry
        }

        static string sCurrDirectory = Directory.GetCurrentDirectory();
        static SqlConnection oSqlConn = new SqlConnection();

        static void Main(string[] args)
        {
            if (args.Length == 3)
            {
                string sArg1 = args[0];
                string sArg2 = args[1];
                string sArg3 = args[2];

                sCurrDirectory = sArg3.Replace(ArgPath, "");
                ShowWriteError(sCurrDirectory);

                oSqlConn = new SqlConnection(new InitData(sCurrDirectory).sGetConnString());
                oSqlConn.Open();

                if (oSqlConn.State == ConnectionState.Open)
                    ShowWriteError("OPEN");
                else
                    ShowWriteError("NOT OPEN");
                //sCurrDirectory = sAppDirectory(oSqlConn);

                string sTargetDir = sInitTargetPath(sCurrDirectory);

                string sTerminalId = sArg1.Substring(0, 2).ToUpper() == ArgTerminalId ? sGetTerminalId(sArg1) : sGetTerminalId(sArg2);
                TypeCommand oTypeCmd = sArg1.Substring(0, 2).ToUpper() == ArgCommand ? typeCmd(sArg1) : typeCmd(sArg2);
                
                ShowWriteError(sCurrDirectory);
                ShowWriteError("sTerminalId : " + sTerminalId);
                ShowWriteError("oTypeCmd : " + oTypeCmd.ToString());
                EMS_XML oEms = new EMS_XML();

                string sInfo = null;
                if (oTypeCmd.ToString() == TypeCommand.D.ToString())
                {
                    sInfo = "Processing Delete " + sTerminalId;
                    ShowWriteError(sInfo);
                    GenerateDeleteXml(sTerminalId, sTargetDir, ref oEms);
                    sInfo += "Success.";
                }
                else if (oTypeCmd.ToString() == TypeCommand.A.ToString())
                {
                    sInfo = "Processing AddUpdate " + sTerminalId;
                    ShowWriteError(sInfo);
                    GenerateAddUpdXml(sTerminalId, sTargetDir, ref oEms);
                    sInfo += "Success.";
                }
                else if (oTypeCmd.ToString() == TypeCommand.I.ToString())
                {
                    sInfo = "Processing Inquiry " + sTerminalId;
                    ShowWriteError(sInfo);
                    GenerateInqXml(sTerminalId, sTargetDir, ref oEms);
                    sInfo += "Success.";
                }
                else
                    sInfo = "Unknown Command Parameter";

                if (oEms != null)
                    SendEms(oEms);
                
                ShowWriteError(sInfo);
            }
            else if (args[0].ToLower() == "/h" || args[0] == "/?")
                ShowHelp();
            else
            {
                Console.WriteLine("Invalid command. Type /? or /H. To show Help");
                Console.Read();
            }
        }

        static string sInitTargetPath(string sPath)
        {
            string sEmsSyncConfig = sPath + @"\EmsSync.config";
            IniFile oIni = new IniFile(sEmsSyncConfig);
            return oIni.IniReadValue("EMS", "Target");
        }

        static void GenerateInqXml(string sTerminalId, string sPath, ref EMS_XML _oEms)
        {
            try
            {
                _oEms.Attribute.sSenders = "INSYS";
                _oEms.Attribute.sTypes = EMSCommonClass.XMLRequestType.INQ.ToString();
                _oEms.Attribute.sVersions = "1.00";
                _oEms.Attribute.sSenderID = string.Format("INSYS{0:yyyyMMddHHmmss}", DateTime.Now);

                EMS_XML.DataClass.FilterClass oFilter = new EMS_XML.DataClass.FilterClass();
                oFilter.sFilterFieldName="Terminal_INIT";
                oFilter.sValues=sTerminalId;
                _oEms.Data.doAddFilter(oFilter);

                string sFilename = string.Format(@"{0}\{1}.xml", sPath, _oEms.Attribute.sSenderID);
                _oEms.CreateXmlFile(sFilename, false);
                ShowWriteError(sFilename);
                ShowWriteError(_oEms.Attribute.sSenderID);
            }
            catch (Exception ex)
            {
                ShowWriteError("Error : " + ex.Message);
            }
        }

        static void GenerateDeleteXml(string sTerminalId, string sPath, ref EMS_XML _oEms)
        {
            try
            {
                _oEms.Attribute.sSenders = "INSYS";
                _oEms.Attribute.sTypes = EMSCommonClass.XMLRequestType.DELETE.ToString();
                _oEms.Attribute.sVersions = "1.00";
                _oEms.Attribute.sSenderID = string.Format("INSYS{0:yyyyMMddHHmmss}", DateTime.Now);

                EMS_XML.DataClass.TerminalClass oTerminal = new EMS_XML.DataClass.TerminalClass();
                oTerminal.AddColumnValue("Terminal_INIT", sTerminalId);
                _oEms.Data.doAddTerminal(oTerminal);

                string sFilename = string.Format(@"{0}\{1}.xml", sPath, _oEms.Attribute.sSenderID);
                _oEms.CreateXmlFile(sFilename, false);
                ShowWriteError(sFilename);
                ShowWriteError(_oEms.Attribute.sSenderID);
            }
            catch (Exception ex)
            {
                ShowWriteError("Error : " + ex.Message);
            }
        }

        static void GenerateAddUpdXml(string sTerminalId, string sPath,ref EMS_XML _oEms)
        {
            try
            {
                _oEms.Attribute.sSenders = "INSYS";
                _oEms.Attribute.sTypes = EMSCommonClass.XMLRequestType.INQ.ToString();
                _oEms.Attribute.sVersions = "1.00";
                _oEms.Attribute.sSenderID = string.Format("INSYS{0:yyyyMMddHHmmss}", DateTime.Now);

                EMS_XML.DataClass.FilterClass oFilter = new EMS_XML.DataClass.FilterClass();
                oFilter.sFilterFieldName = "Terminal_INIT";
                oFilter.sValues = sTerminalId;
                _oEms.Data.doAddFilter(oFilter);

                EMS_XML oEmsResult = new EMS_XML();
                oEmsResult.Attribute.sSenders = "INSYS";
                oEmsResult.Attribute.sTypes = EMSCommonClass.XMLRequestType.ADD_UPDATE.ToString();
                oEmsResult.Attribute.sVersions = "1.00";
                oEmsResult.Attribute.sSenderID = string.Format("INSYS{0:yyyyMMddHHmmss}", DateTime.Now);

                string sErrMsg = null;

                SqlEMS_XML oSqlXml = new SqlEMS_XML(oSqlConn, _oEms.Attribute.sSenderID, _oEms);
                if (oSqlXml.IsCreateTable(ref sErrMsg, ref oEmsResult))
                    if (oSqlXml.IsInsertRow(ref sErrMsg, ref oEmsResult))
                        if (oSqlXml.IsXMLInquiry(_oEms.Attribute.sSenderID, ref sErrMsg, ref oEmsResult, ""))
                        {
                            string sFilename = string.Format(@"{0}\{1}.xml", sPath, _oEms.Attribute.sSenderID);
                            oEmsResult.Attribute.sTypes = EMSCommonClass.XMLRequestType.ADD_UPDATE.ToString();
                            oEmsResult.CreateXmlFile(sFilename, false);
                            ShowWriteError(sFilename);
                            ShowWriteError(oEmsResult.Attribute.sSenderID);
                        }
            }
            catch (Exception ex)
            {
                ShowWriteError("Error : " + ex.Message);
            }
        }

        static void SendEms(EMS_XML _oEmsSender)
        {

            EmsWebService.Service emsService = new ConsoleSyncWebEms.EmsWebService.Service();
            EmsWebService.EMS_XML oEmsSender = new ConsoleSyncWebEms.EmsWebService.EMS_XML();

            EmsWebService.AttributeClass emsAttribute = new ConsoleSyncWebEms.EmsWebService.AttributeClass();
            emsAttribute.sSenders = _oEmsSender.Attribute.sSenders;
            emsAttribute.sTypes = _oEmsSender.Attribute.sTypes;
            emsAttribute.sVersions = _oEmsSender.Attribute.sVersions;
            emsAttribute.sSenderID = _oEmsSender.Attribute.sSenderID;

            EmsWebService.DataClass emsData = new ConsoleSyncWebEms.EmsWebService.DataClass();
            if (_oEmsSender.Data.ltFilter.Count > 0)
            {
                EmsWebService.FilterClass[] listEmsFilter = new ConsoleSyncWebEms.EmsWebService.FilterClass[_oEmsSender.Data.ltFilter.Count];
                int iCount = 0;
                foreach (EMS_XML.DataClass.FilterClass oFilterTemp in _oEmsSender.Data.ltFilter)
                {
                    EmsWebService.FilterClass emsFilter = new ConsoleSyncWebEms.EmsWebService.FilterClass();
                    emsFilter.sFilterFieldName = oFilterTemp.sFilterFieldName;
                    emsFilter.sValues = oFilterTemp.sValues;
                    listEmsFilter[iCount] = emsFilter;
                    iCount++;
                }
                emsData.ltFilter = listEmsFilter;
            }
            if (_oEmsSender.Data.ltTerminal.Count > 0)
            {
                EmsWebService.TerminalClass[] listEmsTerminal = new ConsoleSyncWebEms.EmsWebService.TerminalClass[_oEmsSender.Data.ltTerminal.Count];
                int iCount = 0;
                foreach (EMS_XML.DataClass.TerminalClass oTermTemp in _oEmsSender.Data.ltTerminal)
                {
                    EmsWebService.TerminalClass emsTerminal = new ConsoleSyncWebEms.EmsWebService.TerminalClass();
                    EmsWebService.ColumnValue[] emsListCollVal = new ConsoleSyncWebEms.EmsWebService.ColumnValue[oTermTemp.ColumnValueXML.Count];
                    int iCountColVal = 0;
                    foreach (EMS_XML.DataClass.TerminalClass.ColumnValue coll in oTermTemp.ColumnValueXML)
                    {
                        EmsWebService.ColumnValue emsCollVal = new ConsoleSyncWebEms.EmsWebService.ColumnValue();
                        emsCollVal.sColName = coll.sColName;
                        emsCollVal.sColValue = coll.sColValue;

                        emsListCollVal[iCountColVal] = emsCollVal;
                        iCountColVal++;
                    }
                    listEmsTerminal[iCount] = emsTerminal;
                    iCount++;
                }
                emsData.ltTerminal = listEmsTerminal;
            }
            oEmsSender.Attribute = emsAttribute;
            oEmsSender.Data = emsData;

            EmsWebService.EMS_XML oEmsOuput = emsService.sStartXMLWebService(oEmsSender);
            if (oEmsOuput.Data.ltTerminal.Length > 0)
                ShowWriteError("Ada Output");
        }
        
        static void ProcessItems<T>(IList<T> coll)
        {
            foreach (T item in coll)
            {
                System.Console.Write(item.ToString() + " ");
            }
            System.Console.WriteLine();
        }

        static string sGetTerminalId(string sArguments)
        {
            return sArguments.Substring(2, 8).ToUpper();
        }

        static TypeCommand typeCmd(string sArguments)
        {
            if (sArguments.Substring(2).ToUpper() == TypeCommand.A.ToString())
                return TypeCommand.A;
            else if (sArguments.Substring(2).ToUpper() == TypeCommand.D.ToString())
                return TypeCommand.D;
            else
                return TypeCommand.I;
        }

        static void ShowHelp()
        {
            string sHelpFile = sCurrDirectory + @"\Help.hlp";
            StreamReader sr = new StreamReader(sHelpFile);
            string sHelpText = sr.ReadToEnd();
            sr.Close();
            Console.WriteLine(sHelpText);
        }

        public static void ShowWriteError(string sArgs)
        {
            string sDirLog = sCurrDirectory + @"\LOG";
            if (!Directory.Exists(sDirLog))
                Directory.CreateDirectory(sDirLog);
            string sFilename = string.Format(@"{0}\{1:yyyyMMdd}.log", sDirLog, DateTime.Now);

            StreamWriter sw = new StreamWriter(sFilename, true);
            sw.WriteLine("[{0:yyyy-MM-dd HH:mm:ss}] {1}", DateTime.Now, sArgs);
            sw.Close();

            Console.WriteLine("[{0:yyyy-MM-dd HH:mm:ss}] {1}", DateTime.Now, sArgs);
        }

        static string sAppDirectory(SqlConnection _oSqlConn)
        {
            string sPath = null;
            SqlCommand oCmd = new SqlCommand("spXMLSyncEmsPath", _oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sPath", SqlDbType.VarChar).Direction = ParameterDirection.Output;
            if (_oSqlConn.State != ConnectionState.Open)
                _oSqlConn.Open();
            oCmd.ExecuteNonQuery();
            oCmd.Dispose();
            return sPath;
        }
    }
}
