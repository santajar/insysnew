﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.18444.
// 
#pragma warning disable 1591

namespace ConsoleSyncWebEms.EmsWebService {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="ServiceSoap", Namespace="http://www.integra-pratama.co.id/")]
    public partial class Service : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback sStartXMLWebServiceOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public Service() {
            this.Url = global::ConsoleSyncWebEms.Properties.Settings.Default.ConsoleEmsSyncWeb_XmlWebService_Service;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event sStartXMLWebServiceCompletedEventHandler sStartXMLWebServiceCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.integra-pratama.co.id/sStartXMLWebService", RequestNamespace="http://www.integra-pratama.co.id/", ResponseNamespace="http://www.integra-pratama.co.id/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public EMS_XML sStartXMLWebService(EMS_XML _oXML) {
            object[] results = this.Invoke("sStartXMLWebService", new object[] {
                        _oXML});
            return ((EMS_XML)(results[0]));
        }
        
        /// <remarks/>
        public void sStartXMLWebServiceAsync(EMS_XML _oXML) {
            this.sStartXMLWebServiceAsync(_oXML, null);
        }
        
        /// <remarks/>
        public void sStartXMLWebServiceAsync(EMS_XML _oXML, object userState) {
            if ((this.sStartXMLWebServiceOperationCompleted == null)) {
                this.sStartXMLWebServiceOperationCompleted = new System.Threading.SendOrPostCallback(this.OnsStartXMLWebServiceOperationCompleted);
            }
            this.InvokeAsync("sStartXMLWebService", new object[] {
                        _oXML}, this.sStartXMLWebServiceOperationCompleted, userState);
        }
        
        private void OnsStartXMLWebServiceOperationCompleted(object arg) {
            if ((this.sStartXMLWebServiceCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.sStartXMLWebServiceCompleted(this, new sStartXMLWebServiceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.integra-pratama.co.id/")]
    public partial class EMS_XML {
        
        private AttributeClass attributeField;
        
        private DataClass dataField;
        
        /// <remarks/>
        public AttributeClass Attribute {
            get {
                return this.attributeField;
            }
            set {
                this.attributeField = value;
            }
        }
        
        /// <remarks/>
        public DataClass Data {
            get {
                return this.dataField;
            }
            set {
                this.dataField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.integra-pratama.co.id/")]
    public partial class AttributeClass {
        
        private string sVersionsField;
        
        private string sTypesField;
        
        private string sSendersField;
        
        private string sSenderIDField;
        
        /// <remarks/>
        public string sVersions {
            get {
                return this.sVersionsField;
            }
            set {
                this.sVersionsField = value;
            }
        }
        
        /// <remarks/>
        public string sTypes {
            get {
                return this.sTypesField;
            }
            set {
                this.sTypesField = value;
            }
        }
        
        /// <remarks/>
        public string sSenders {
            get {
                return this.sSendersField;
            }
            set {
                this.sSendersField = value;
            }
        }
        
        /// <remarks/>
        public string sSenderID {
            get {
                return this.sSenderIDField;
            }
            set {
                this.sSenderIDField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.integra-pratama.co.id/")]
    public partial class FilterClass {
        
        private string sFilterFieldNameField;
        
        private string sValuesField;
        
        /// <remarks/>
        public string sFilterFieldName {
            get {
                return this.sFilterFieldNameField;
            }
            set {
                this.sFilterFieldNameField = value;
            }
        }
        
        /// <remarks/>
        public string sValues {
            get {
                return this.sValuesField;
            }
            set {
                this.sValuesField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.integra-pratama.co.id/")]
    public partial class ResultClass {
        
        private string sResultFieldNameField;
        
        private string sResultCodeField;
        
        private string sResultDescField;
        
        private string sTimeProcessField;
        
        /// <remarks/>
        public string sResultFieldName {
            get {
                return this.sResultFieldNameField;
            }
            set {
                this.sResultFieldNameField = value;
            }
        }
        
        /// <remarks/>
        public string sResultCode {
            get {
                return this.sResultCodeField;
            }
            set {
                this.sResultCodeField = value;
            }
        }
        
        /// <remarks/>
        public string sResultDesc {
            get {
                return this.sResultDescField;
            }
            set {
                this.sResultDescField = value;
            }
        }
        
        /// <remarks/>
        public string sTimeProcess {
            get {
                return this.sTimeProcessField;
            }
            set {
                this.sTimeProcessField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.integra-pratama.co.id/")]
    public partial class ColumnValue {
        
        private string sColNameField;
        
        private string sColValueField;
        
        /// <remarks/>
        public string sColName {
            get {
                return this.sColNameField;
            }
            set {
                this.sColNameField = value;
            }
        }
        
        /// <remarks/>
        public string sColValue {
            get {
                return this.sColValueField;
            }
            set {
                this.sColValueField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.integra-pratama.co.id/")]
    public partial class TerminalClass {
        
        private ColumnValue[] columnValueXMLField;
        
        private ResultClass resultField;
        
        /// <remarks/>
        public ColumnValue[] ColumnValueXML {
            get {
                return this.columnValueXMLField;
            }
            set {
                this.columnValueXMLField = value;
            }
        }
        
        /// <remarks/>
        public ResultClass Result {
            get {
                return this.resultField;
            }
            set {
                this.resultField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://www.integra-pratama.co.id/")]
    public partial class DataClass {
        
        private TerminalClass terminalField;
        
        private FilterClass filterField;
        
        private TerminalClass[] ltTerminalField;
        
        private FilterClass[] ltFilterField;
        
        /// <remarks/>
        public TerminalClass Terminal {
            get {
                return this.terminalField;
            }
            set {
                this.terminalField = value;
            }
        }
        
        /// <remarks/>
        public FilterClass Filter {
            get {
                return this.filterField;
            }
            set {
                this.filterField = value;
            }
        }
        
        /// <remarks/>
        public TerminalClass[] ltTerminal {
            get {
                return this.ltTerminalField;
            }
            set {
                this.ltTerminalField = value;
            }
        }
        
        /// <remarks/>
        public FilterClass[] ltFilter {
            get {
                return this.ltFilterField;
            }
            set {
                this.ltFilterField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    public delegate void sStartXMLWebServiceCompletedEventHandler(object sender, sStartXMLWebServiceCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class sStartXMLWebServiceCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal sStartXMLWebServiceCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public EMS_XML Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((EMS_XML)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591