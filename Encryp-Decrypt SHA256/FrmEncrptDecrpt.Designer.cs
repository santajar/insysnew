﻿namespace Encryp_Decrypt_SHA256
{
    partial class FrmEncrptDecrpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEncrptDecrpt));
            this.gbEncrypt = new System.Windows.Forms.GroupBox();
            this.btnEncryptClear = new System.Windows.Forms.Button();
            this.btnEncryptGenerate = new System.Windows.Forms.Button();
            this.lblEncryptOutput = new System.Windows.Forms.Label();
            this.lblEncryptInput = new System.Windows.Forms.Label();
            this.txtEncryptOutput = new System.Windows.Forms.TextBox();
            this.txtEncryptInput = new System.Windows.Forms.TextBox();
            this.gbDecrypt = new System.Windows.Forms.GroupBox();
            this.btnDecryptClear = new System.Windows.Forms.Button();
            this.btnDecryptGenerate = new System.Windows.Forms.Button();
            this.lblDecryptOutput = new System.Windows.Forms.Label();
            this.lblDecryptInput = new System.Windows.Forms.Label();
            this.txtDecryptOutput = new System.Windows.Forms.TextBox();
            this.txtDecryptInput = new System.Windows.Forms.TextBox();
            this.lblKeyEncrypt = new System.Windows.Forms.Label();
            this.txtKeyEncrypt = new System.Windows.Forms.TextBox();
            this.lblVectorEncrypt = new System.Windows.Forms.Label();
            this.txtVectorEncrypt = new System.Windows.Forms.TextBox();
            this.lblVectorDecrypt = new System.Windows.Forms.Label();
            this.txtVectorDecrypt = new System.Windows.Forms.TextBox();
            this.lblKeyDecrypt = new System.Windows.Forms.Label();
            this.txtKeyDecrypt = new System.Windows.Forms.TextBox();
            this.gbEncrypt.SuspendLayout();
            this.gbDecrypt.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbEncrypt
            // 
            this.gbEncrypt.Controls.Add(this.lblVectorEncrypt);
            this.gbEncrypt.Controls.Add(this.txtVectorEncrypt);
            this.gbEncrypt.Controls.Add(this.lblKeyEncrypt);
            this.gbEncrypt.Controls.Add(this.txtKeyEncrypt);
            this.gbEncrypt.Controls.Add(this.btnEncryptClear);
            this.gbEncrypt.Controls.Add(this.btnEncryptGenerate);
            this.gbEncrypt.Controls.Add(this.lblEncryptOutput);
            this.gbEncrypt.Controls.Add(this.lblEncryptInput);
            this.gbEncrypt.Controls.Add(this.txtEncryptOutput);
            this.gbEncrypt.Controls.Add(this.txtEncryptInput);
            this.gbEncrypt.Location = new System.Drawing.Point(12, 12);
            this.gbEncrypt.Name = "gbEncrypt";
            this.gbEncrypt.Size = new System.Drawing.Size(317, 241);
            this.gbEncrypt.TabIndex = 0;
            this.gbEncrypt.TabStop = false;
            this.gbEncrypt.Text = "Encrypt";
            // 
            // btnEncryptClear
            // 
            this.btnEncryptClear.Location = new System.Drawing.Point(151, 204);
            this.btnEncryptClear.Name = "btnEncryptClear";
            this.btnEncryptClear.Size = new System.Drawing.Size(75, 23);
            this.btnEncryptClear.TabIndex = 5;
            this.btnEncryptClear.Text = "Clear";
            this.btnEncryptClear.UseVisualStyleBackColor = true;
            this.btnEncryptClear.Click += new System.EventHandler(this.btnEncryptClear_Click);
            // 
            // btnEncryptGenerate
            // 
            this.btnEncryptGenerate.Location = new System.Drawing.Point(232, 204);
            this.btnEncryptGenerate.Name = "btnEncryptGenerate";
            this.btnEncryptGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnEncryptGenerate.TabIndex = 4;
            this.btnEncryptGenerate.Text = "Encrypt";
            this.btnEncryptGenerate.UseVisualStyleBackColor = true;
            this.btnEncryptGenerate.Click += new System.EventHandler(this.btnEncryptGenerate_Click);
            // 
            // lblEncryptOutput
            // 
            this.lblEncryptOutput.AutoSize = true;
            this.lblEncryptOutput.Location = new System.Drawing.Point(17, 107);
            this.lblEncryptOutput.Name = "lblEncryptOutput";
            this.lblEncryptOutput.Size = new System.Drawing.Size(39, 13);
            this.lblEncryptOutput.TabIndex = 3;
            this.lblEncryptOutput.Text = "Output";
            // 
            // lblEncryptInput
            // 
            this.lblEncryptInput.AutoSize = true;
            this.lblEncryptInput.Location = new System.Drawing.Point(17, 22);
            this.lblEncryptInput.Name = "lblEncryptInput";
            this.lblEncryptInput.Size = new System.Drawing.Size(31, 13);
            this.lblEncryptInput.TabIndex = 2;
            this.lblEncryptInput.Text = "Input";
            // 
            // txtEncryptOutput
            // 
            this.txtEncryptOutput.Location = new System.Drawing.Point(68, 104);
            this.txtEncryptOutput.Multiline = true;
            this.txtEncryptOutput.Name = "txtEncryptOutput";
            this.txtEncryptOutput.Size = new System.Drawing.Size(239, 94);
            this.txtEncryptOutput.TabIndex = 1;
            // 
            // txtEncryptInput
            // 
            this.txtEncryptInput.Location = new System.Drawing.Point(68, 19);
            this.txtEncryptInput.Name = "txtEncryptInput";
            this.txtEncryptInput.Size = new System.Drawing.Size(239, 20);
            this.txtEncryptInput.TabIndex = 0;
            // 
            // gbDecrypt
            // 
            this.gbDecrypt.Controls.Add(this.lblVectorDecrypt);
            this.gbDecrypt.Controls.Add(this.btnDecryptClear);
            this.gbDecrypt.Controls.Add(this.txtVectorDecrypt);
            this.gbDecrypt.Controls.Add(this.btnDecryptGenerate);
            this.gbDecrypt.Controls.Add(this.lblKeyDecrypt);
            this.gbDecrypt.Controls.Add(this.lblDecryptOutput);
            this.gbDecrypt.Controls.Add(this.txtKeyDecrypt);
            this.gbDecrypt.Controls.Add(this.lblDecryptInput);
            this.gbDecrypt.Controls.Add(this.txtDecryptOutput);
            this.gbDecrypt.Controls.Add(this.txtDecryptInput);
            this.gbDecrypt.Location = new System.Drawing.Point(335, 12);
            this.gbDecrypt.Name = "gbDecrypt";
            this.gbDecrypt.Size = new System.Drawing.Size(317, 241);
            this.gbDecrypt.TabIndex = 6;
            this.gbDecrypt.TabStop = false;
            this.gbDecrypt.Text = "Decrypt";
            // 
            // btnDecryptClear
            // 
            this.btnDecryptClear.Location = new System.Drawing.Point(151, 204);
            this.btnDecryptClear.Name = "btnDecryptClear";
            this.btnDecryptClear.Size = new System.Drawing.Size(75, 23);
            this.btnDecryptClear.TabIndex = 5;
            this.btnDecryptClear.Text = "Clear";
            this.btnDecryptClear.UseVisualStyleBackColor = true;
            this.btnDecryptClear.Click += new System.EventHandler(this.btnDecryptClear_Click);
            // 
            // btnDecryptGenerate
            // 
            this.btnDecryptGenerate.Location = new System.Drawing.Point(232, 204);
            this.btnDecryptGenerate.Name = "btnDecryptGenerate";
            this.btnDecryptGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnDecryptGenerate.TabIndex = 4;
            this.btnDecryptGenerate.Text = "Decrypt";
            this.btnDecryptGenerate.UseVisualStyleBackColor = true;
            this.btnDecryptGenerate.Click += new System.EventHandler(this.btnDecryptGenerate_Click);
            // 
            // lblDecryptOutput
            // 
            this.lblDecryptOutput.AutoSize = true;
            this.lblDecryptOutput.Location = new System.Drawing.Point(17, 107);
            this.lblDecryptOutput.Name = "lblDecryptOutput";
            this.lblDecryptOutput.Size = new System.Drawing.Size(39, 13);
            this.lblDecryptOutput.TabIndex = 3;
            this.lblDecryptOutput.Text = "Output";
            // 
            // lblDecryptInput
            // 
            this.lblDecryptInput.AutoSize = true;
            this.lblDecryptInput.Location = new System.Drawing.Point(17, 22);
            this.lblDecryptInput.Name = "lblDecryptInput";
            this.lblDecryptInput.Size = new System.Drawing.Size(31, 13);
            this.lblDecryptInput.TabIndex = 2;
            this.lblDecryptInput.Text = "Input";
            // 
            // txtDecryptOutput
            // 
            this.txtDecryptOutput.Location = new System.Drawing.Point(68, 104);
            this.txtDecryptOutput.Multiline = true;
            this.txtDecryptOutput.Name = "txtDecryptOutput";
            this.txtDecryptOutput.Size = new System.Drawing.Size(239, 94);
            this.txtDecryptOutput.TabIndex = 1;
            // 
            // txtDecryptInput
            // 
            this.txtDecryptInput.Location = new System.Drawing.Point(68, 19);
            this.txtDecryptInput.Name = "txtDecryptInput";
            this.txtDecryptInput.Size = new System.Drawing.Size(239, 20);
            this.txtDecryptInput.TabIndex = 0;
            // 
            // lblKeyEncrypt
            // 
            this.lblKeyEncrypt.AutoSize = true;
            this.lblKeyEncrypt.Location = new System.Drawing.Point(17, 48);
            this.lblKeyEncrypt.Name = "lblKeyEncrypt";
            this.lblKeyEncrypt.Size = new System.Drawing.Size(25, 13);
            this.lblKeyEncrypt.TabIndex = 7;
            this.lblKeyEncrypt.Text = "Key";
            // 
            // txtKeyEncrypt
            // 
            this.txtKeyEncrypt.Location = new System.Drawing.Point(68, 45);
            this.txtKeyEncrypt.Name = "txtKeyEncrypt";
            this.txtKeyEncrypt.Size = new System.Drawing.Size(239, 20);
            this.txtKeyEncrypt.TabIndex = 6;
            // 
            // lblVectorEncrypt
            // 
            this.lblVectorEncrypt.AutoSize = true;
            this.lblVectorEncrypt.Location = new System.Drawing.Point(17, 74);
            this.lblVectorEncrypt.Name = "lblVectorEncrypt";
            this.lblVectorEncrypt.Size = new System.Drawing.Size(38, 13);
            this.lblVectorEncrypt.TabIndex = 9;
            this.lblVectorEncrypt.Text = "Vector";
            // 
            // txtVectorEncrypt
            // 
            this.txtVectorEncrypt.Location = new System.Drawing.Point(68, 71);
            this.txtVectorEncrypt.Name = "txtVectorEncrypt";
            this.txtVectorEncrypt.Size = new System.Drawing.Size(239, 20);
            this.txtVectorEncrypt.TabIndex = 8;
            // 
            // lblVectorDecrypt
            // 
            this.lblVectorDecrypt.AutoSize = true;
            this.lblVectorDecrypt.Location = new System.Drawing.Point(17, 72);
            this.lblVectorDecrypt.Name = "lblVectorDecrypt";
            this.lblVectorDecrypt.Size = new System.Drawing.Size(38, 13);
            this.lblVectorDecrypt.TabIndex = 13;
            this.lblVectorDecrypt.Text = "Vector";
            // 
            // txtVectorDecrypt
            // 
            this.txtVectorDecrypt.Location = new System.Drawing.Point(68, 69);
            this.txtVectorDecrypt.Name = "txtVectorDecrypt";
            this.txtVectorDecrypt.Size = new System.Drawing.Size(239, 20);
            this.txtVectorDecrypt.TabIndex = 12;
            // 
            // lblKeyDecrypt
            // 
            this.lblKeyDecrypt.AutoSize = true;
            this.lblKeyDecrypt.Location = new System.Drawing.Point(17, 46);
            this.lblKeyDecrypt.Name = "lblKeyDecrypt";
            this.lblKeyDecrypt.Size = new System.Drawing.Size(24, 13);
            this.lblKeyDecrypt.TabIndex = 11;
            this.lblKeyDecrypt.Text = "key";
            // 
            // txtKeyDecrypt
            // 
            this.txtKeyDecrypt.Location = new System.Drawing.Point(68, 43);
            this.txtKeyDecrypt.Name = "txtKeyDecrypt";
            this.txtKeyDecrypt.Size = new System.Drawing.Size(239, 20);
            this.txtKeyDecrypt.TabIndex = 10;
            // 
            // FrmEncrptDecrpt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 265);
            this.Controls.Add(this.gbDecrypt);
            this.Controls.Add(this.gbEncrypt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEncrptDecrpt";
            this.Text = "Form Encrypt Decrypt SHA 256";
            this.gbEncrypt.ResumeLayout(false);
            this.gbEncrypt.PerformLayout();
            this.gbDecrypt.ResumeLayout(false);
            this.gbDecrypt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbEncrypt;
        private System.Windows.Forms.Button btnEncryptClear;
        private System.Windows.Forms.Button btnEncryptGenerate;
        private System.Windows.Forms.Label lblEncryptOutput;
        private System.Windows.Forms.Label lblEncryptInput;
        private System.Windows.Forms.TextBox txtEncryptOutput;
        private System.Windows.Forms.TextBox txtEncryptInput;
        private System.Windows.Forms.GroupBox gbDecrypt;
        private System.Windows.Forms.Button btnDecryptClear;
        private System.Windows.Forms.Button btnDecryptGenerate;
        private System.Windows.Forms.Label lblDecryptOutput;
        private System.Windows.Forms.Label lblDecryptInput;
        private System.Windows.Forms.TextBox txtDecryptOutput;
        private System.Windows.Forms.TextBox txtDecryptInput;
        private System.Windows.Forms.Label lblVectorEncrypt;
        private System.Windows.Forms.TextBox txtVectorEncrypt;
        private System.Windows.Forms.Label lblKeyEncrypt;
        private System.Windows.Forms.TextBox txtKeyEncrypt;
        private System.Windows.Forms.Label lblVectorDecrypt;
        private System.Windows.Forms.TextBox txtVectorDecrypt;
        private System.Windows.Forms.Label lblKeyDecrypt;
        private System.Windows.Forms.TextBox txtKeyDecrypt;
    }
}

