﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using InSysClass;
using System.Security;
using System.Security.Cryptography;
using System.IO;

namespace Encryp_Decrypt_SHA256
{
    public partial class FrmEncrptDecrpt : Form
    {
        string sEcrypt = "";
        string sDecrypt = "";

        public FrmEncrptDecrpt()
        {
            InitializeComponent();
        }

        private void btnEncryptClear_Click(object sender, EventArgs e)
        {
            ClearEncryptText();
        }

        private void ClearEncryptText()
        {
            txtEncryptInput.Text = "";
            txtEncryptOutput.Text = "";
        }

        private void btnDecryptClear_Click(object sender, EventArgs e)
        {
            ClearDecryptText();
        }

        private void ClearDecryptText()
        {
            txtDecryptInput.Text = "";
            txtDecryptOutput.Text = "";
        }

        private void btnEncryptGenerate_Click(object sender, EventArgs e)
        {
            string sKey = txtKeyEncrypt.Text;
            string sVector = txtVectorEncrypt.Text;
            if (!string.IsNullOrEmpty(sKey) && sKey.Length == 8)
                if (!string.IsNullOrEmpty(sVector) && sVector.Length == 8)
                    txtEncryptOutput.Text = EncryptionLib.EncryptSHA256(txtEncryptInput.Text, sKey);
                
                else
                    MessageBox.Show("Vector Cannot Empty and Lenght Have to 8");
            else
                MessageBox.Show("Key Cannot Empty and Lenght Have to 8");
        }

        private void btnDecryptGenerate_Click(object sender, EventArgs e)
        {
            string sKey = txtKeyDecrypt.Text;
            string sVector = txtVectorDecrypt.Text;
            if (!string.IsNullOrEmpty(sKey) && sKey.Length == 8)
                if (!string.IsNullOrEmpty(sVector) && sVector.Length == 8)
                {
                    MessageBox.Show("Key and Vector Decrypt must same with Encrpyt");
                    txtDecryptOutput.Text = EncryptionLib.DecryptSHA256(txtDecryptInput.Text, sKey);
                }
                else
                    MessageBox.Show("Vector Cannot Empty and Lenght Have to 8");
            else
                MessageBox.Show("Key Cannot Empty and Lenght Have to 8");
        }

       }
}
