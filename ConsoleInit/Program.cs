﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using InSysClass;
using Ini;

namespace ConsoleInit
{
    class Program
    {
        static SqlConnection oSqlConn = new SqlConnection();

        static void Main(string[] args)
        {
            InitConfig();
            //DataTable dtTerminalList = new DataTable();
            //dtTerminalList = dtFillTerminalList();
            List<string> ltTerminalId = GetListTerminalId(@"\Terminal.config");
            List<string> ltTerminalIdDone = GetListTerminalId(@"\Terminal_Done.config");

            int iCounter = 1;
            ShowStatus("Start");

            foreach (string sTerminalId in ltTerminalId)
            {
                //foreach (DataRow row in dtTerminalList.Rows)
                //{
                //    string sTerminalId = row["TerminalId"].ToString();
                //    if (!IsInitTerminalExist(sTerminalId))
                //    {
                try
                {
                    if (ltTerminalIdDone.Count == 0 || ltTerminalIdDone.IndexOf(sTerminalId) < 0)
                    {
                        string sUserId = null;
                        if (IsAllowUserAccess(oSqlConn, sTerminalId, ref sUserId))
                        {
                            UserAccessInsert(oSqlConn, sTerminalId);
                            ShowStatus(string.Format("Processing {0}. {1} Processing Start.", iCounter, sTerminalId));
                            SaveFullContentClass.SaveFullContent(oSqlConn, sTerminalId);
                            ShowStatus(string.Format("Processing {0}. {1} Processing Done.", iCounter, sTerminalId));
                            UserAccessDelete(oSqlConn, sTerminalId);
                            WriteTerminalId(sTerminalId);
                        }
                    }
                    iCounter++;
                }
                catch (Exception)
                { }
                //    }
                //}
            }

            //Console.Read();
        }

        static void InitConfig()
        {
            IniFile oIni = new IniFile(Environment.CurrentDirectory + @"\app.config");
            string sServer = oIni.IniReadValue("Database", "Server");
            string sDatabase = oIni.IniReadValue("Database", "Database");
            string sUser = oIni.IniReadValue("Database", "User");
            string sPwd = oIni.IniReadValue("Database", "Pwd");
            oSqlConn = new SqlConnection(SQLConnLib.sConnStringAsyncMARS(sServer, sDatabase, sUser, sPwd));
            try
            {
                oSqlConn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
            }
        }

        static DataTable dtFillTerminalList()
        {
            DataTable dtTemp = new DataTable();

            //string sQuery = "SELECT TerminalId FROM tbProfileTerminalList "
            //    + "WHERE StatusMaster=0 AND TerminalId NOT IN (SELECT DISTINCT TerminalId FROM tbInitTemp) "
            //    + "ORDER BY DatabaseId DESC, TerminalId DESC";
            string sQuery = "SELECT TerminalId FROM tbProfileTerminalList "
                + "WHERE DATEDIFF(DAY, LastView, GETDATE()) BETWEEN 1 AND 7";

            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open)
                    oSqlConn.Open();
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.HasRows)
                        if (oRead.Read())
                            dtTemp.Load(oRead);
                }
            }

            return dtTemp;
        }

        static List<string> GetListTerminalId(string _sFilename)
        {
            List<string> _arrsTemp = new List<string>();
            string sFilename = Environment.CurrentDirectory + _sFilename;
            StreamReader sr = new StreamReader(sFilename);
            while (!sr.EndOfStream)
                _arrsTemp.Add(sr.ReadLine());
            sr.Close();
            return _arrsTemp;
        }

        static bool IsInitTerminalExist(string _sTerminalId)
        {
            string sQuery = string.Format("SELECT 1 FROM tbInitTemp WHERE TerminalId='{0}'", _sTerminalId);
            bool isExist = false;
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open)
                    oSqlConn.Open();
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.HasRows)
                        isExist = true;
                }
            }
            return isExist;
        }

        static void ShowStatus(string sMessage)
        {
            Console.WriteLine("[{0:MMM dd, yyyy hh:mm:ss.fff tt}] {1}", DateTime.Now, sMessage);
        }

        static void UserAccessInsert(SqlConnection _oSqlConn, string _sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand("spUserAccessInsert", _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = "GEMALTO";
                oCmd.ExecuteNonQuery();
            }
        }

        static void UserAccessDelete(SqlConnection _oSqlConn, string _sTerminalId)
        {
            if (!string.IsNullOrEmpty("GEMALTO"))
                using (SqlCommand oCmd = new SqlCommand("spUserAccessDelete", _oSqlConn))
                {
                    if (_oSqlConn.State != ConnectionState.Open)
                        _oSqlConn.Open();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = "GEMALTO";
                    oCmd.ExecuteNonQuery();
                }
        }

        static bool IsAllowUserAccess(SqlConnection _oSqlConn, string _sTerminalId, ref string sUserIdOnAccess)
        {
            bool isAllow = true;
            using (SqlCommand oCmd = new SqlCommand("spUserAccessBrowse", _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@sOutput", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();

                sUserIdOnAccess = oCmd.Parameters["@sUserId"].Value.ToString();
                isAllow = oCmd.Parameters["@sOutput"].Value.ToString() == "1" ? false : true;
            }
            return isAllow;
        }

        static void WriteTerminalId(string _sTerminalId)
        {
            string sFilename = Environment.CurrentDirectory + @"\Terminal_Done.config";
            using (StreamWriter sw = new StreamWriter(sFilename, true))
                sw.WriteLine(_sTerminalId);
        }
    }
}
