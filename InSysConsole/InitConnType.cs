using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Ini;

namespace InSysConsole
{
    public class InitConsoleConnType
    {
        protected bool IsSerial;
        protected bool IsTCPIP;
        protected bool IsModem;

        protected string sSerialPort;
        protected int iSerialBaudrate;
        protected float fSerialStopbit;
        protected int iSerialDataBit;
        protected string sSerialParity;

        protected string sModemPort1;
        protected string sModemPort2;
        protected string sModemPort3;
        protected string sModemPort4;
        protected string sModemPort5;

        protected string sDirectory;
        protected IniFile oObjIniFile;

        public InitConsoleConnType(string _sDirectory)
        {
            sDirectory = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectory + "\\Port.config";
            
            if (File.Exists(sFileName))
            {
                oObjIniFile = new IniFile(sFileName);
                try
                {
                    IsSerial = oObjIniFile.IniReadValue("CONNECTION", "Serial") == "1" ? true : false;
                    IsTCPIP = oObjIniFile.IniReadValue("CONNECTION", "TCPIP") == "1" ? true : false;
                    IsModem = oObjIniFile.IniReadValue("CONNECTION", "Modem") == "1" ? true : false;

                    sSerialPort = oObjIniFile.IniReadValue("SERIALPORT", "Port");
                    iSerialBaudrate = int.Parse(oObjIniFile.IniReadValue("SERIALPORT", "BaudRate"));
                    fSerialStopbit = int.Parse(oObjIniFile.IniReadValue("SERIALPORT", "StopBit"));
                    iSerialDataBit = int.Parse(oObjIniFile.IniReadValue("SERIALPORT", "DataBit"));
                    sSerialParity = oObjIniFile.IniReadValue("SERIALPORT", "Parity");

                    sModemPort1 = oObjIniFile.IniReadValue("MODEM", "Port-1");
                    sModemPort2 = oObjIniFile.IniReadValue("MODEM", "Port-2");
                    sModemPort3 = oObjIniFile.IniReadValue("MODEM", "Port-3");
                    sModemPort4 = oObjIniFile.IniReadValue("MODEM", "Port-4");
                    sModemPort5 = oObjIniFile.IniReadValue("MODEM", "Port-5");
                }
                catch (Exception ex)
                {
                    Logs.doWriteErrorFile(sDirectory, ex.Message);
                }
            }
        }

        #region "SerialPort"
        public bool IsConnSerialActive()
        {
            return IsSerial;
        }

        public void SetSerialConn(bool IsActive)
        {
            oObjIniFile.IniWriteValue("CONNECTION", "Serial", IsActive ? "1" : "0");
        }

        public string SerialPort { get { return sSerialPort; } set { sSerialPort = value; oObjIniFile.IniWriteValue("SERIALPORT", "Port", value); } }
        public int SerialBaudRate { get { return iSerialBaudrate; } set { iSerialBaudrate = value; oObjIniFile.IniWriteValue("SERIALPORT", "BaudRate", value.ToString()); } }
        public float SerialStopBit { get { return fSerialStopbit; } set { fSerialStopbit = value; oObjIniFile.IniWriteValue("SERIALPORT", "StopBit", value.ToString()); } }
        public int SerialDataBit { get { return iSerialDataBit; } set { iSerialDataBit = value; oObjIniFile.IniWriteValue("SERIALPORT", "DataBit", value.ToString()); } }
        public string SerialParity { get { return sSerialParity; } set { sSerialParity = value; oObjIniFile.IniWriteValue("SERIALPORT", "Parity", value); } }
        #endregion

        #region "TcpIp"
        public bool IsConnTcpIpActive()
        {
            return IsTCPIP;
        }

        public void SetTcpIpConn(bool IsActive)
        {
            oObjIniFile.IniWriteValue("CONNECTION", "TCPIP", IsActive ? "1" : "0");
        }
        #endregion

        #region "Modem"
        public bool IsConnModemActive()
        {
            return IsModem;
        }

        public void SetModemConn(bool IsActive)
        {
            oObjIniFile.IniWriteValue("CONNECTION", "Modem", IsActive ? "1" : "0");
        }

        public string Modem1 { get { return sModemPort1; } set { sModemPort1 = value; oObjIniFile.IniWriteValue("MODEM", "Port-1", value); } }
        public string Modem2 { get { return sModemPort2; } set { sModemPort2 = value; oObjIniFile.IniWriteValue("MODEM", "Port-2", value); } }
        public string Modem3 { get { return sModemPort3; } set { sModemPort3 = value; oObjIniFile.IniWriteValue("MODEM", "Port-3", value); } }
        public string Modem4 { get { return sModemPort4; } set { sModemPort4 = value; oObjIniFile.IniWriteValue("MODEM", "Port-4", value); } }
        public string Modem5 { get { return sModemPort5; } set { sModemPort5 = value; oObjIniFile.IniWriteValue("MODEM", "Port-5", value); } }
        #endregion
    }
}
