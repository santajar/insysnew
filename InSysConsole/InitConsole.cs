using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Ini;

namespace InSysConsole
{
    class InitConsole
    {
        protected string sPort;

        public InitConsole()
        {
            doGetInitConsole();
        }

        protected void doGetInitConsole()
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Console.config";
            if (File.Exists(sFileName))
            {
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sPort = objIniFile.IniReadValue("InSys", "Port");
                }
                catch (Exception ex)
                {
                    Logs.doWriteErrorFile(ex.Message);
                };
            }
        }

        public int Port
        {
            get
            {
                return int.Parse(sPort);
            }
        }
    }
}
