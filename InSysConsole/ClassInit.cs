using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using InSysClass;

namespace InSysConsole
{
    class ClassInit
    {
        //string sQueryInitBrowse = "SELECT TOP(1) * FROM tbInit ";
        //string sQueryInitBrowseLast = "ORDER BY InitId DESC";
        //string sQueryInitCount = "SELECT COUNT(*) FROM tbInit ";

        string sQueryAllowInit = "SELECT COUNT(*) FROM tbProfileTerminalList ";

        string sQueryInitLogConnInsert = "INSERT INTO tbInitLogConn(TerminalId,[Description],Percentage) ";
        string sQueryInitLogConnEdit = "UPDATE tbInitLogConn ";
        string sQueryInitLogConnDelete = "DELETE FROM tbInitLogConn ";

        // Status Init; 0 : Clear/Empty, 1 : Init started, 2 : Init continues,
        // 3 : Init last send.
        int iStatusInit;
        string sTerminalId;
        byte[] arrbReceive;

        int iCurrentLoop;
        int iTotalLoop;
        string sTagProcess = null;
        string sProcCodeRecv;
        string sNII;
        string sBit11;
        string sBit48;
        string sAddress;

        //bool bInvalidTerminalId;
        //bool bInitNotAllow;

        //public static int iCountClass;
        public static bool bCheckAllowInit = false;
        public static SqlConnection oConn;
        
        public ClassInit()
        {
            Clear();
        }

        public ClassInit(byte[] _arrbReceive)
        {
            arrbReceive = _arrbReceive;
            ISO8583Lib oISO = new ISO8583Lib();
            ISO8583_MSGLib oIsoRecBit = new ISO8583_MSGLib();
            oISO.UnPackISO(arrbReceive, ref oIsoRecBit);

            sTerminalId = oIsoRecBit.sTerminalId;
            sProcCodeRecv = oIsoRecBit.sBit[3];
            sNII = oIsoRecBit.sDestNII;
            sAddress = oIsoRecBit.sOriginAddr;
            sBit11 = oIsoRecBit.sBit[11];
            sBit48 = sAddLenToMessage(CommonLib.sStringToHex(oIsoRecBit.sBit[48]));

            if (isValidMTI(oIsoRecBit.MTI))
            {
                if (isStartInit(sProcCodeRecv))
                {
                    DisplayToConsole();
                    iStatusInit = 1;
                }
                else if (isContinueInit(sProcCodeRecv))
                {
                    iStatusInit = 2;
                    iCurrentLoop = iGetCurrentLoop();
                    iTotalLoop = iGetTotalLoop();
                }
            }
        }

        public void Clear()
        {
            sTerminalId = null;
            //iStatusInit = 0;
            //iCurrInitId = 0;
            //iLastInitId = 0;
            //iTotalLoop = 0;
            sTagProcess = null;
        }

        //protected void StartInit()
        //{
        //    DeleteInitLogConn(); //delete record tbInitLogConn based on TerminalId
        //    FillTableInit(); // fill the tbInit

        //    //iStatusInit = 1; // set status to new init
        //    //iLastInitId = iGetLastInitId();
        //    //iTotalLoop = iGetTotalLoop();
        //    //iCountClass++;

        //    InsertInitLogConn();
        //}

        protected string sGetTerminalId
        {
            get
            {
                return sTerminalId;
            }
        }

        //protected void FillTableInit()
        //{
        //    try
        //    {
        //        if (oConn.State == ConnectionState.Closed)
        //            oConn.Open();
        //        SqlCommand oObjCmd = new SqlCommand(CommonSP.sSPInitStart, oConn);
        //        oObjCmd.CommandType = CommandType.StoredProcedure;
        //        oObjCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalId;
        //        oObjCmd.ExecuteNonQuery();
        //        oObjCmd.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        bInvalidTerminalId = true;
        //        Trace.Write("Start Init : " + ex.Message);
        //    }
        //}

        protected int iGetCurrentLoop()
        {
            int iInitIdTemp = 0;
            string sP1 = "SELECT DBO.iCurrentLoop('" + sTerminalId + "')";
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPSqlExec, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@p1", SqlDbType.VarChar).Value = sP1;

                SqlDataReader oObjRead = oCmd.ExecuteReader();
                if (oObjRead.Read())
                    iInitIdTemp = Convert.ToInt16(oObjRead.GetValue(0).ToString());
                
                oObjRead.Close();
                oObjRead.Dispose();

                oCmd.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Trace.Write("iGetLastInitId : " + ex.Message);
            }
            return iInitIdTemp;
        }

        protected int iGetTotalLoop()
        {
            int iTemp = 0;
            string sP1 = "SELECT DBO.iTotalLoop('" + sTerminalId + "')";
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();
                SqlCommand oObjCmd = new SqlCommand(CommonSP.sSPSqlExec, oConn);
                oObjCmd.CommandType = CommandType.StoredProcedure;
                oObjCmd.Parameters.Add("@p1", SqlDbType.VarChar).Value = sP1;

                SqlDataReader oObjRead = oObjCmd.ExecuteReader();
                if (oObjRead.Read())
                    iTemp = Convert.ToInt16(oObjRead.GetValue(0).ToString());

                oObjRead.Close();
                oObjRead.Dispose();

                oObjCmd.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Trace.Write("iGetCountLoop : " + ex.Message);
            }
            return iTemp;
        }

        protected int iCheckAllowInit
        {
            get
            {
                return (bCheckAllowInit == true ? 1 : 0);
            }
        }

        //protected string sGetInitMsg()
        //{
        //    string sTemp = null;
        //    string sCond = "WHERE TerminalId='" + sTerminalId + "'";
        //    try
        //    {
        //        if (oConn.State == ConnectionState.Closed)
        //            oConn.Open();
        //        SqlCommand oObjCmd = new SqlCommand(CommonSP.sSPInitBrowse, oConn);
        //        oObjCmd.CommandType = CommandType.StoredProcedure;
        //        oObjCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sCond;

        //        SqlDataReader oObjRead = oObjCmd.ExecuteReader();
        //        if (oObjRead.Read())
        //        {
        //            iCurrInitId = Convert.ToInt16(oObjRead.GetValue(0).ToString());
        //            sTemp = oObjRead.GetValue(2).ToString();
        //            sTagProcess = oObjRead.GetValue(3).ToString();

        //            iStatusInit = 2; // set status init to continues
        //        }
        //        oObjRead.Close();
        //        oObjRead.Dispose();

        //        oObjCmd.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        Trace.Write("sGetInitMsg : " + ex.Message);
        //    }

        //    return sTemp;
        //}

        protected byte[] arrbGetSendMessage()
        {
            string sContent = null,
                sISOMessage = null;
            int iErrorCode = 0;
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitMessage, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@iCheckInit", SqlDbType.Int).Value = iCheckAllowInit;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@sProcCode", SqlDbType.VarChar).Value = sProcCodeRecv;
                oCmd.Parameters.Add("@sNII", SqlDbType.VarChar).Value = sNII;
                oCmd.Parameters.Add("@sAdress", SqlDbType.VarChar).Value = sAddress;
                oCmd.Parameters.Add("@sBit11", SqlDbType.VarChar).Value = sBit11;
                oCmd.Parameters.Add("@sBit48", SqlDbType.VarChar).Value = sBit48;

                SqlDataReader oRead = oCmd.ExecuteReader();
                if (oRead.Read())
                {
                    sContent = oRead.GetValue(1).ToString();
                    sTagProcess = oRead.GetValue(2).ToString();
                    sISOMessage = oRead.GetValue(3).ToString();
                    iErrorCode = Convert.ToInt16(oRead.GetValue(4).ToString());               
                }
                oRead.Close();
                oRead.Dispose();
                oCmd.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Trace.Write("arrbGetSendMessage : " + ex.Message);
            }

            //Console.WriteLine("\niErrorCode : {0}, CurrentLoop : {1}\n", iErrorCode, iCurrentLoop);

            InitDisplay(iErrorCode);
            InitLogConn(iErrorCode);
            DisplayToConsole();

            string sTemp = sAddHexLenToMessage(sISOMessage + CommonLib.sStringToHex(sContent));
            return CommonLib.HexStringToByteArray(sTemp);
        }

        public byte[] arrbSendMsg()
        {
            return arrbGetSendMessage();
        }

        protected void InitDisplay(int _iErrorCode)
        {
            if (_iErrorCode == 1)
                sTagProcess = null;
            else if (_iErrorCode == 2)
                sTagProcess = "01";
            else if (_iErrorCode == 3)
                sTagProcess = "03";
        }

        protected void InitLogConn(int _iErrorCode)
        {
            if (_iErrorCode == 0)
            {
                if (iStatusInit == 1)
                {
                    DeleteInitLogConn();
                    InsertInitLogConn();
                }
                if (iStatusInit == 2)
                    UpdateInitLogConn();
            }
            else if (_iErrorCode == 3)
            {
                iCurrentLoop = iGetCurrentLoop();
                UpdateInitLogConn();
            }
        }

        //protected bool bInitInvalid
        //{
        //    get
        //    {
        //        return bInvalidTerminalId;
        //    }
        //}

        //protected bool isLastMessage()
        //{
        //    return (iCurrInitId == iLastInitId);
        //}

        protected void DeleteCurrInitId(int iCurrInitId)
        {
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitDelete, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = "AND InitId=" + iCurrInitId;
                oCmd.ExecuteNonQuery();
                oCmd.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Trace.Write("DeleteCurrInitId : " + ex.Message);
            }
        }

        protected void DeleteInit()
        {
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitDelete, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.ExecuteNonQuery();
                oCmd.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Trace.Write("DeleteInit : " + ex.Message);
            }
        }

        protected int iCalcPercent()
        {
            float fCurr = iCurrentLoop;
            float fTotal = iTotalLoop;
            return Convert.ToInt16((fCurr / fTotal) * 100);
        }

        protected string sProcessing
        {
            get
            {
                string sTemp = null;
                switch (sTagProcess)
                {
                    case null:
                    case "DE":
                        sTemp = "Terminal";
                        break;
                    case "DC":
                        sTemp = "Count";
                        break;
                    case "AA":
                        sTemp = "Acquirer";
                        break;
                    case "AE":
                        sTemp = "Issuer";
                        break;
                    case "AC":
                        sTemp = "Card";
                        break;
                    case "AD":
                        sTemp = "Relation";
                        break;
                    case "03":
                        sTemp = "Complete.";
                        break;
                    case "01":
                        sTemp = "Invalid TerminalId";
                        break;
                    case "02":
                        sTemp = "Init Not Allow";
                        break;
                };
                return sTemp;
            }
        }

        protected void InsertInitLogConn()
        {
            try
            {
                string sValues = sQueryInitLogConnInsert + 
                    "VALUES ('"+ sTerminalId+"','Processing Terminal',0)";

                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPSqlExec, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@p1", SqlDbType.VarChar).Value = sValues;
                oCmd.ExecuteNonQuery();
                oCmd.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("InsertInitLogConn : " + ex.Message);
                Trace.Write("InsertInitLogConn : " + ex.Message);
            }
        }

        protected void UpdateInitLogConn()
        {
            try
            {
                string sValues = sQueryInitLogConnEdit + 
                    "SET [Description]='Processing " + sProcessing + "',";
                sValues += "Percentage=" + iCalcPercent();
                sValues += "WHERE TerminalId='" + sTerminalId + "'";

                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPSqlExec, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@p1", SqlDbType.VarChar).Value = sValues;
                oCmd.ExecuteNonQuery();
                oCmd.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Trace.Write("UpdateInitLogConn : " + ex.Message);
            }
        }

        protected void DeleteInitLogConn()
        {
            try
            {
                string sValues = sQueryInitLogConnDelete + 
                    "WHERE TerminalId='" + sTerminalId + "'";

                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPSqlExec, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@p1", SqlDbType.VarChar).Value = sValues;
                oCmd.ExecuteNonQuery();
                oCmd.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Trace.Write("UpdateInitLogConn : " + ex.Message);
            }
        }

        protected void DisplayToConsole()
        {
            switch (sTagProcess)
            {
                case null:
                    CommonConsole.WriteToConsole(sTerminalId + " Start to Initialize");
                    break;
                case "00":
                case "01":
                case "02":
                    CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing);
                    break;
                default:
                    CommonConsole.WriteToConsole(sTerminalId + " Processing " + sProcessing);
                    break;
            }
        

        }

        protected static bool isValidMTI(string sMTI)
        {
            return (sMTI == "0800");
        }

        protected static bool isStartInit(string sProcCode)
        {
            return (sProcCode == "930000");
        }

        protected static bool isContinueInit(string sProcCode)
        {
            return (sProcCode == "930001");
        }

        protected string sAddHexLenToMessage(string sMessage)
        {
            string sHexLen = CommonLib.sConvertDecToHex(sMessage.Length / 2);
            return (sHexLen.PadLeft(4, '0') + sMessage);
        }

        protected string sAddLenToMessage(string sMessage)
        {
            return (Convert.ToString(sMessage.Length/2).PadLeft(4, '0') + sMessage);
        }
    }
}
