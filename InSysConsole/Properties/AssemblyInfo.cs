﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("InSysConsole")]
[assembly: AssemblyDescription("InSysConsole Debug version")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("PT. Integra Pratama")]
[assembly: AssemblyProduct("InSysConsole")]
[assembly: AssemblyCopyright("Copyright ©  PT. Integra Pratama 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("cd600fc2-917a-4ba7-9a42-0db81f5eb65c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.1.0.15")]
[assembly: AssemblyFileVersion("2.1.0.15")]
