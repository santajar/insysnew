﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using EmsXml = ipXML.EMS_XML;
using EmsCommon = ipXML.EMSCommonClass;
using Ini;
using ipXML;
using InSysClass;

namespace ConsoleEmsInquiryRange
{
    class Program
    {
        static DataTable dtTemplate = new DataTable();
        static string ErrorApp = null;

        static void Main(string[] args)
        {
            string sDateFrom = null;
            string sDateTo = null;
            DateTime dateFrom = new DateTime();
            DateTime dateTo = new DateTime();
            string sPathStartup = Environment.CurrentDirectory;
            string sPathResult = Environment.CurrentDirectory + @"\RESULT";
            string sPathLog = null;
            //int iError = 0;

            InitConfig(ref sPathLog, ref sPathResult, ref sDateFrom, ref sDateTo);
            try
            {
                //string sConnString = new InitData(sPathStartup).sGetConnString();
                string sConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection oSqlConn = new SqlConnection(sConnString);
                oSqlConn.Open();

                if (!Directory.Exists(sPathResult)) Directory.CreateDirectory(sPathResult);

                //while (string.IsNullOrEmpty(sDateFrom))
                //{
                //    Console.Write("Start Date(dd-MM-yyyy) : ");
                //    sDateFrom = Console.ReadLine();
                //    if (!DateTime.TryParseExact(sDateFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateFrom))
                //        sDateFrom = null;
                //}

                //while (string.IsNullOrEmpty(sDateTo))
                //{
                //    Console.Write("End Date(dd-MM-yyyy) : ");
                //    sDateTo = Console.ReadLine();
                //    if (!DateTime.TryParseExact(sDateTo, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTo))
                //        sDateTo = null;
                //}

                if (!DateTime.TryParseExact(sDateFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateFrom))
                    sDateFrom = null;
                if (!DateTime.TryParseExact(sDateTo, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTo))
                    sDateTo = null;

                doWriteLog(sPathLog, string.Format("START : Local Inquiry by date range {0:d/MM/yyyy} to {1:d/MM/yyyy}", dateFrom, dateTo));

                /*
                    <?xml version="1.0"?>
                    <TMScript>
                        <Attribute>
                            <Version>1.00</Version>
                            <Type>INQ</Type>
                            <Sender>EMS</Sender>
                            <Sender_ID>EMSTMS_Inq</Sender_ID>			
                        </Attribute>
                        <Data>
                            <Filters>
                                <Filter>
                                    <Field_Name>Terminal_Init</Field_Name>				
                                    <Values>CX433141</Values>	
                                </Filter>
                            </Filters>		
                        </Data>
                    </TMScript>
                 */
                
                DataTable dtTerminalList = dtGetTerminalListByDateRange(dateFrom, dateTo, oSqlConn);
                if (dtTerminalList.Rows.Count > 0)
                {
                    foreach (DataRow rowTerminal in dtTerminalList.Rows)
                    {
                        string sTerminalID = rowTerminal["TerminalID"].ToString();
                        try
                        {
                            EmsXml oEmsXmlResult = new EmsXml();
                            string sFileResult = string.Format(@"{0}\{1}.xml", sPathResult, sTerminalID);

                            List<DataTable> listTableEms = new List<DataTable>();
                            listTableEms.Add(dtFilter(sTerminalID));
                            listTableEms.Add(dtAttributeDefault(sTerminalID));

                            EMS_XML xmlEms = new EMS_XML(listTableEms);
                            EMS_Process processEms = new EMS_Process(xmlEms, oSqlConn);
                            oEmsXmlResult = processEms.emsInquiry(dtTemplate, ref ErrorApp);
                            oEmsXmlResult.CreateXmlFile(sFileResult, true);
                            string sStatus = string.IsNullOrEmpty(processEms.Error) ? "SUCCESS" : processEms.Error;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("FAILED, {0}", ex.Message);
                            doWriteLog(sPathLog, string.Format("FAILED, inquirying {0}. ErrorApp : {1}", sTerminalID, ErrorApp));
                            doWriteFailedList(sPathLog, sTerminalID);
                        }
                    }
                    Console.WriteLine("SUCCESS, inquirying " + dtTerminalList.Rows.Count.ToString() + " of TerminalID");
                    doWriteLog(sPathLog, string.Format("Inquiry date : {0:d/MM/yyyy} to {1:d/MM/yyyy} SUCCESS, inquirying {2} of TerminalID",
                        dateFrom, dateTo, dtTerminalList.Rows.Count));
                }
                else
                {
                    Console.WriteLine("SUCCESS, no TerminalID to inquiry");
                    doWriteLog(sPathLog, string.Format("Inquiry date : {0:d/MM/yyyy} to {1:d/MM/yyyy} SUCCESS, no TerminalID to inquiry",
                        dateFrom, dateTo));
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("FAILED DATABASE CONN, {0}", ex.Message);
                doWriteLog(sPathLog, string.Format("Inquiry date : {0:d/MM/yyyy} to {1:d/MM/yyyy} FAILED, {2}",
                    dateFrom, dateTo, ex.Message));
                Console.Read();
            }
            catch (Exception ex)
            {
                Console.WriteLine("FAILED, {0}", ex.Message);
                doWriteLog(sPathLog, string.Format("Inquiry date : {0:d/MM/yyyy} to {1:d/MM/yyyy} FAILED, {2}",
                    dateFrom, dateTo, ex.Message));
                Console.Read();
            }
            doWriteLog(sPathLog, string.Format("END : Local Inquiry by date range", dateFrom, dateTo));
        }

        static DataTable dtFilter(string sTerminalID)
        {
            string[] arr = new string[] { "Terminal_Init", sTerminalID };

            DataTable dtTempFilter = new DataTable();
            dtTempFilter.TableName = "Filter";
            dtTempFilter.Columns.Add("Field_Name");
            dtTempFilter.Columns.Add("Values");
            dtTempFilter.Rows.Add(arr);
            return dtTempFilter;
        }

        static DataTable dtAttributeDefault(string sTerminalID)
        {
            string[] arr = new string[] { "2.00", "INQ", "KONSOLIDASI", string.Format("INQUIRY_{0:yyMMddhhmmsstt}_{1}", DateTime.Now, sTerminalID) };

            DataTable dtTempAttribute = new DataTable();
            dtTempAttribute.TableName = "Attribute";
            dtTempAttribute.Columns.Add("Version");
            dtTempAttribute.Columns.Add("Type");
            dtTempAttribute.Columns.Add("Sender");
            dtTempAttribute.Columns.Add("Sender_ID");
            dtTempAttribute.Rows.Add(arr);
            return dtTempAttribute;
        }

        static void doWriteFailedList(string sLogDirectory, string sTerminalID)
        {
            string sLogFile = string.Format(@"{0}\FAILED_{1:yyyyMMdd}.log", sLogDirectory, DateTime.Now);
            if (!Directory.Exists(sLogDirectory)) Directory.CreateDirectory(sLogDirectory);
            using (StreamWriter sw = new StreamWriter(sLogFile, true))
                sw.WriteLine(string.Format("{0}", sTerminalID));
        }

        static void doWriteLog(string sLogDirectory, string sMessage)
        {
            //string sLogDirectory = Environment.CurrentDirectory + @"\LOG";
            string sLogFile = string.Format(@"{0}\{1:yyyyMMdd}.log", sLogDirectory, DateTime.Now);
            if (!Directory.Exists(sLogDirectory)) Directory.CreateDirectory(sLogDirectory);
            using (StreamWriter sw = new StreamWriter(sLogFile, true))
                sw.WriteLine(string.Format("[{0:yyyy MMM dd hh:mm:ss tt}] : {1}", DateTime.Now, sMessage));
        }

        static DataTable dtGetTerminalListByDateRange(DateTime _dtStart, DateTime _dtEnd, SqlConnection _oSqlConn)
        {
            DataTable dtTemp = new DataTable();
            string sQuery = string.Format("SELECT TerminalID FROM tbProfileTerminal WITH (NOLOCK) \n");
            sQuery += string.Format("WHERE TerminalID IN (SELECT TerminalID FROM tbProfileTerminalList WITH (NOLOCK) WHERE DatabaseID IN (SELECT DatabaseID FROM tbProfileTerminalDB)) \n");
            sQuery += string.Format("AND TerminalTag IN ('DC03','DC003') \n");
            sQuery += string.Format("AND DATEDIFF(day,dbo.dtGetLastUpdateDate(TerminalTagValue),CONVERT(datetime,'{0:yyyy-d-M}',103))<=0 \n", _dtStart);
            sQuery += string.Format("AND DATEDIFF(day,dbo.dtGetLastUpdateDate(TerminalTagValue),CONVERT(datetime,'{0:yyyy-d-M}',103))>=0 \n", _dtEnd);

            if (_oSqlConn.State != ConnectionState.Open) _oSqlConn.Open();
            using (SqlCommand oCmd = new SqlCommand(sQuery, _oSqlConn))
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        static void InitConfig(ref string _sXmlLog, ref string _sXmlResult, ref string _sStartDate, ref string _sEndDate)
        {
            string sXmlFile;
            sXmlFile = ConfigurationManager.AppSettings["File"].ToString();
            if (sXmlFile.Substring(1, 1) == @".\") sXmlFile = string.Format(@"{0}\{1}", Environment.CurrentDirectory, sXmlFile.Substring(3));
            
            _sXmlLog = ConfigurationManager.AppSettings["Log"].ToString();
            if (_sXmlLog.Substring(1, 1) == @".\") _sXmlLog = string.Format(@"{0}\{1}", Environment.CurrentDirectory, _sXmlLog.Substring(3));
            
            _sXmlResult = ConfigurationManager.AppSettings["Result"].ToString();
            if (_sXmlResult.Substring(1, 1) == @".\") _sXmlResult = string.Format(@"{0}\{1}", Environment.CurrentDirectory, _sXmlResult.Substring(3));
            
            if (!Directory.Exists(_sXmlLog))
                Directory.CreateDirectory(_sXmlLog);
            if (!Directory.Exists(sXmlFile))
                Directory.CreateDirectory(sXmlFile);
            if (!Directory.Exists(_sXmlResult))
                Directory.CreateDirectory(_sXmlResult);

            EmsCommon.DirectoryLog = _sXmlLog;
            EmsCommon.DirectoryXml = sXmlFile;
            EmsCommon.DirectoryResult = _sXmlResult;

            //_sStartDate = oEmsDir.StartDate;
            _sStartDate = ConfigurationManager.AppSettings["StartDate"].ToString();
            //_sEndDate = oEmsDir.EndDate;
            _sEndDate = ConfigurationManager.AppSettings["EndDate"].ToString();

            string sTemplate = ConfigurationManager.AppSettings["Template"];
            if (!string.IsNullOrEmpty(sTemplate))
                dtTemplate = CommonLib.ConvertCSVtoDataTable(sTemplate, ',');
        }
    }
    
    class InitEmsDir
    {
        protected string sXmlFile;
        protected string sXmlResult;
        protected string sXmlLog;
        protected string sStartDate;
        protected string sEndDate;

        public string XmlFile { get { return sXmlFile; } }
        public string XmlResult { get { return sXmlResult; } }
        public string XmlLog { get { return sXmlLog; } }
        public string StartDate { get { return sStartDate; } }
        public string EndDate { get { return sEndDate; } }

        public InitEmsDir()
        {
            string sFilename = Environment.CurrentDirectory + @"\EmsInqRange.config";
            if (File.Exists(sFilename))
                InitConfig(sFilename);
        }

        protected void InitConfig(string _sFilename)
        {
            IniFile oIniEmsDir = new IniFile(_sFilename);
            sXmlFile = oIniEmsDir.IniReadValue("Directory", "File");
            sXmlLog = oIniEmsDir.IniReadValue("Directory", "Log");
            sXmlResult = oIniEmsDir.IniReadValue("Directory", "Result");

            sStartDate = oIniEmsDir.IniReadValue("DATE", "StartDate");
            sEndDate = oIniEmsDir.IniReadValue("DATE", "EndDate");
        }
    }
}