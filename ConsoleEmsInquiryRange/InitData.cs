using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using InSysClass;
using Ini;

namespace ConsoleEmsInquiryRange
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;

        public InitData() : this(Environment.CurrentDirectory) { }

        public InitData(string sPath)
        {
            doGetInitData(sPath);
        }

        protected void doGetInitData(string sActiveDir)
        {
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception ex)
                {
                   Console.WriteLine("Error : " + ex.Message);
                };
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnString(sDataSource, sDatabase, sUserID, sPassword);
        }

        public void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword)
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            try
            {
                IniFile objIniFile = new IniFile(sFileName);
                objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
                objIniFile.IniWriteValue("Database", "Database", sDatabase);
                objIniFile.IniWriteValue("Database", "UserID", sUserID);
                objIniFile.IniWriteValue("Database", "Password", sPassword);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            };
            EncryptionLib.EncryptFile(sEncryptFileName, sFileName);
            //File.Delete(sFileName);
        }
    }
}
