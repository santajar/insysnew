﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoPushMail
{
    class ClassEmailNotif
    {

        public static string sGetEmailRecipients(SqlConnection oSqlConn, int iRecipientsType, string sNotifGroup, ref string sContent)
        {
            sContent = "";
            try
            {
                using (SqlCommand oCmd = new SqlCommand("spGetEmailRecipients", oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@RecipientsType", SqlDbType.Int).Value = iRecipientsType;
                    oCmd.Parameters.Add("@NotifGroup", SqlDbType.VarChar).Value = sNotifGroup;
                    SqlParameter sOutputContent = oCmd.Parameters.Add("@shasil", SqlDbType.VarChar, 65535);
                    sOutputContent.Direction = ParameterDirection.Output;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    oCmd.ExecuteNonQuery();
                    sContent = (string)oCmd.Parameters["@shasil"].Value;

                }
            }
            catch (Exception ex)
            {
                //Trace.Write(string.Format("Error spAutoInitAllow : {0}", ex.StackTrace));
            }
            return sContent;
        }

        public static string sGetSenderEmail(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetSenderEmail()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string sGetCorporateName(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetCorporateName()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }

        public static int iGetSenderPort(SqlConnection oSqlConn)
        {
            int iLen = 0;

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.iGetSenderPort()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    iLen = int.Parse(oRead[0].ToString());
                oRead.Close();
            }
            oCmd.Dispose();

            return iLen;
        }

        public static string sGetURLWeb(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetURLWeb()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string sGetSenderPassword(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetSenderPassword()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string sGetSenderHost(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetSenderHost()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
    }
}
