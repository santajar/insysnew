using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Ini;
using InSysClass;

namespace AutoPushMail
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;
        protected string sDirectory;

        public InitData(string _sDirectory)
        {
            sDirectory = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectory + "\\Application.config";
            string sEncryptFileName = sDirectory + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception ex)
                {
                    CommonClass.doWriteErrorFile(sDirectory + ex.Message);
                }
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword);
        }

        public string sGetConnString(int iMaxPoolSize)
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword, iMaxPoolSize);
        }
        
    }

    class InitDataAuditTrail
    {
        protected string sDataSourceAuditTrail;
        protected string sDatabaseAuditTrail;
        protected string sUserIDAuditTrail;
        protected string sPasswordAuditTrail;
        protected string sDirectoryAuditTrail;

        public InitDataAuditTrail(string _sDirectory)
        {
            sDirectoryAuditTrail = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectoryAuditTrail + "\\Application.config";
            string sEncryptFileName = sDirectoryAuditTrail + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSourceAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "DataSource");
                    sDatabaseAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "Database");
                    sUserIDAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "UserID");
                    sPasswordAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "Password");
                }
                catch (Exception ex)
                {
                    CommonClass.doWriteErrorFile( ex.Message);
                }
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSourceAuditTrail;
        }

        public string sGetDatabase()
        {
            return sDatabaseAuditTrail;
        }

        public string sGetUserID()
        {
            return sUserIDAuditTrail;
        }

        public string sGetPassword()
        {
            return sPasswordAuditTrail;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnString(sDataSourceAuditTrail, sDatabaseAuditTrail, sUserIDAuditTrail, sPasswordAuditTrail);
        }

        public string sGetConnString(int iMaxPoolSize)
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSourceAuditTrail, sDatabaseAuditTrail, sUserIDAuditTrail, sPasswordAuditTrail, iMaxPoolSize);
        }
      
    }
}