﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InSysClass;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using ClosedXML.Excel;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace AutoPushMail
{
    public partial class FrmProcess : Form
    {

        static SqlConnection oSqlConn = new SqlConnection();
        static SqlConnection oSqlConnAuditTrail = new SqlConnection();
        static string sConnString;
        static string sConnStringAuditTrail;
        static string sAppDirectory = Directory.GetCurrentDirectory();
        static string sDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
        static string sPath = "\\Document\\Email\\" + sDateTime + "\\";
        static string sLogDir = sAppDirectory + sPath;
        static string FileName1 = sDateTime+"DataInit.xlsx";
        static string FileName2 = sDateTime + "RekapDataInit.xlsx";

        DataTable MainTable = new DataTable();
        public FrmProcess()
        {
            InitializeComponent();
        }

        private void FrmProcess_Load(object sender, EventArgs e)
        {
            lbProgress.Text = "Initialize..";
            oSqlConn = InitConnection();
            oSqlConnAuditTrail = InitConnectionAuditTrail();
            if (oSqlConn != null && oSqlConn.State == ConnectionState.Open && oSqlConnAuditTrail != null && oSqlConnAuditTrail.State == ConnectionState.Open)
            {
                SendMail();
                lbProgress.Text = "Finished";
                this.Dispose();
            }

            Application.Exit();
        }

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                sConnString = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null; 
            }
            return oSqlTempConn;
        }

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnectionAuditTrail()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitDataAuditTrail oObjInit = new InitDataAuditTrail(sAppDirectory);
                sConnStringAuditTrail = oObjInit.sGetConnString();
                sConnStringAuditTrail = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnStringAuditTrail);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
            }
            return oSqlTempConn;
        }

        private void SendMail()
        {
            string bodyText = "";
            string sNotifGroup = "ReportInitDetail";
            string sEmailAddress1 = "";
            string sEmailAddress2 = "";
            string remarks = "Report Init";
            string strCorporateName = "";

            try
            {
                strCorporateName = ClassEmailNotif.sGetCorporateName(oSqlConn);

                string sTo = "";
                string sFrom = ClassEmailNotif.sGetSenderEmail(oSqlConn);
                int index = sFrom.IndexOf("@") ;
                int sFromLength = sFrom.Length;
                string sFromContent = sFrom.Substring(index, sFromLength- index);

                MailMessage MyMessage = new MailMessage();
                MyMessage.From = new MailAddress(ClassEmailNotif.sGetSenderEmail(oSqlConn), "Email Notification " + strCorporateName);
                sTo = ClassEmailNotif.sGetEmailRecipients(oSqlConn, 1, sNotifGroup, ref sEmailAddress1);
                string[] ToArray = sTo.ToString().Split(';');
                for (int i = 0; i < ToArray.Length; i++)
                {
                    if (ToArray[i] != "" && (ToArray[i].IndexOf("@") > 1))
                    {
                        MyMessage.To.Add(new MailAddress(ToArray[i]));
                    }
                }
                string sCC = "";
                sCC = ClassEmailNotif.sGetEmailRecipients(oSqlConn, 2, sNotifGroup, ref sEmailAddress2);
                string[] ccArray = sCC.ToString().Split(';');
                for (int i = 0; i < ccArray.Length; i++)
                {
                    if (ccArray[i] != "" && (ccArray[i].IndexOf("@") > 1))
                    {
                        MyMessage.CC.Add(new MailAddress(ccArray[i]));
                    }
                }
                bodyText = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
                      "<html xmlns='http://www.w3.org/1999/xhtml'>" +
                      "<head>" +
                      "<title></title>" +
                      "</head>" +
                      "<body>" +
                      "<div style='font-size: 12px; font-family: Tahoma, Geneva, sans-serif;'>" +
                      "<h3><b>Please do not reply. This is an automatic email notifier from Email System Ingenico</b></h3>" +
                      "<div>" +
                      "<br /><br />" +
                      "Dear&nbsp; All ," +
                      "<br /><br />" +
                      "Bersama ini kami lampirkan Data untuk " + remarks +
                      "</b><br />Atas perhatiannya kami ucapkan terima kasih.<br />" +
                      "<br />" +
                      "<br />" +
                    "Best Regards,<br /><br />" +
                    "Email System " + strCorporateName +
                    "<div>" +
                    "<br />" +
                      "<br />" +
                      "</div>" +
                      "</div>" +
                      "</body>" +
                      "</html>";               

                string subjectText = "";
                subjectText = remarks;

                ExportToExelDataInit();
                ExportToExelRekapDataInit();

                foreach (string file in Directory.GetFiles(sLogDir))
                {
                    MyMessage.Attachments.Add(new Attachment(file));
                }
                
                MyMessage.Subject = subjectText;
                MyMessage.IsBodyHtml = true;
                MyMessage.Priority = MailPriority.High;
                
                AlternateView HtmlView = AlternateView.CreateAlternateViewFromString(bodyText, null, "text/html");
                MyMessage.AlternateViews.Add(HtmlView);

                SmtpClient MyClient = new SmtpClient();
                MyClient.Host = ClassEmailNotif.sGetSenderHost(oSqlConn);
                //MyClient.Host = "smtp.office365.com";
                MyClient.Port = ClassEmailNotif.iGetSenderPort(oSqlConn);//465;
                MyClient.EnableSsl = true;
                MyClient.UseDefaultCredentials = false;
                MyClient.Credentials = new System.Net.NetworkCredential(ClassEmailNotif.sGetSenderEmail(oSqlConn), ClassEmailNotif.sGetSenderPassword(oSqlConn));
                //MyClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                try
                {
                    if (sFromContent=="@gmail.com") ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    MyClient.Send(MyMessage);
                    CommonClass.doWriteErrorFile("Auto Push Mail Send Done");
                }
                catch (SmtpException ex)
                {
                    CommonClass.doWriteErrorFile(ex.Message);
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void ExportToExelDataInit()
        {
            try
            {
                DataTable dtTemp = new DataTable();

                SqlCommand sqlcmd = new SqlCommand("spExportDataInit", oSqlConn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    if (reader.HasRows)
                        dtTemp.Load(reader);                

                DataSet ds = new DataSet();
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    sqlcmd.Connection = oSqlConn;
                    sda.SelectCommand = sqlcmd;
                    sda.Fill(ds);
                    ds.Tables[0].TableName = "Data";
                }
                
                if (!Directory.Exists(sLogDir)) Directory.CreateDirectory(sLogDir);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    DataTable dt = ds.Tables[i];
                    XLWorkbook wb = new XLWorkbook();
                    wb.Worksheets.Add(dt, dt.TableName);
                    wb.SaveAs(sLogDir + FileName1);
                }

                string allowedExtensions = ".xlsx";
                string fileName = FileName1;

                if (fileName != "" && fileName.IndexOf(".") > 0)
                {
                    bool extensionAllowed = false;
                    string fileExtension = fileName.Substring(fileName.LastIndexOf('.'), fileName.Length - fileName.LastIndexOf('.'));

                    string[] extensions = allowedExtensions.Split(',');
                    for (int a = 0; a < extensions.Length; a++)
                    {
                        if (extensions[a] == fileExtension)
                        {
                            extensionAllowed = true;
                            break;
                        }
                    } 
                }
                else
                {
                    CommonClass.doWriteErrorFile("Error - no file to download");
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }
        
        private void ExportToExelRekapDataInit()
        {
            try
            {
                DataTable dtTemp = new DataTable();

                SqlCommand sqlcmd = new SqlCommand("spExportRekapDataInit", oSqlConn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    if (reader.HasRows)
                        dtTemp.Load(reader);

                DataSet ds = new DataSet();
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    sqlcmd.Connection = oSqlConn;
                    sda.SelectCommand = sqlcmd;
                    sda.Fill(ds);
                    ds.Tables[0].TableName = "Rekap";
                }

                if (!Directory.Exists(sLogDir)) Directory.CreateDirectory(sLogDir);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    DataTable dt = ds.Tables[i];
                    XLWorkbook wb = new XLWorkbook();
                    wb.Worksheets.Add(dt, dt.TableName);
                    wb.SaveAs(sLogDir + FileName2);
                }

                string allowedExtensions = ".xlsx";
                string fileName = FileName2;

                if (fileName != "" && fileName.IndexOf(".") > 0)
                {
                    bool extensionAllowed = false;
                    string fileExtension = fileName.Substring(fileName.LastIndexOf('.'), fileName.Length - fileName.LastIndexOf('.'));

                    string[] extensions = allowedExtensions.Split(',');
                    for (int a = 0; a < extensions.Length; a++)
                    {
                        if (extensions[a] == fileExtension)
                        {
                            extensionAllowed = true;
                            break;
                        }
                    }
                }
                else
                {
                    CommonClass.doWriteErrorFile("Error - no file to download");
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }
    }
    
}
