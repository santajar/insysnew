﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCA_QueryReport
{
    class StoredProcedure
    {
        public static string sVersionBrowse { get { return "spProfileTerminalDbBrowse"; } }
        public static string sBrowseData { get { return "spProfileTerminalListBrowse"; } }
        public static string sQueryReport { get { return "spQueryReport"; } }
        public static string sCompareData { get { return "spReportCompare"; } }
        public static string sReplaceTID { get { return "spReportReplaceTID"; } }
        public static string sCleanTableCompare { get { return "spTempXLSClean"; } }
        public static string sCleanTableReplace { get { return "spFromExcelClean"; } }

    }
}
