﻿namespace InsysTools
{
    partial class frmQC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQC));
            this.gbHeader = new System.Windows.Forms.GroupBox();
            this.btnTemplateBrowse = new System.Windows.Forms.Button();
            this.txtTemplateFile = new System.Windows.Forms.TextBox();
            this.lblTemplate = new System.Windows.Forms.Label();
            this.btnSourceBrowse = new System.Windows.Forms.Button();
            this.txtSourceFile = new System.Windows.Forms.TextBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.rdReplace = new System.Windows.Forms.RadioButton();
            this.lblQueryType = new System.Windows.Forms.Label();
            this.rdQC = new System.Windows.Forms.RadioButton();
            this.ofdBrowseFile = new System.Windows.Forms.OpenFileDialog();
            this.gbProgress = new System.Windows.Forms.GroupBox();
            this.pgBar = new System.Windows.Forms.ProgressBar();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.svdSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.bwWorker = new System.ComponentModel.BackgroundWorker();
            this.gbHeader.SuspendLayout();
            this.gbProgress.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbHeader
            // 
            this.gbHeader.Controls.Add(this.btnTemplateBrowse);
            this.gbHeader.Controls.Add(this.txtTemplateFile);
            this.gbHeader.Controls.Add(this.lblTemplate);
            this.gbHeader.Controls.Add(this.btnSourceBrowse);
            this.gbHeader.Controls.Add(this.txtSourceFile);
            this.gbHeader.Controls.Add(this.lblFileName);
            this.gbHeader.Controls.Add(this.rdReplace);
            this.gbHeader.Controls.Add(this.lblQueryType);
            this.gbHeader.Controls.Add(this.rdQC);
            this.gbHeader.Location = new System.Drawing.Point(4, 1);
            this.gbHeader.Name = "gbHeader";
            this.gbHeader.Size = new System.Drawing.Size(412, 115);
            this.gbHeader.TabIndex = 0;
            this.gbHeader.TabStop = false;
            // 
            // btnTemplateBrowse
            // 
            this.btnTemplateBrowse.Location = new System.Drawing.Point(324, 41);
            this.btnTemplateBrowse.Name = "btnTemplateBrowse";
            this.btnTemplateBrowse.Size = new System.Drawing.Size(80, 30);
            this.btnTemplateBrowse.TabIndex = 8;
            this.btnTemplateBrowse.Text = "Browse";
            this.btnTemplateBrowse.UseVisualStyleBackColor = true;
            this.btnTemplateBrowse.Click += new System.EventHandler(this.btnTemplateBrowse_Click);
            // 
            // txtTemplateFile
            // 
            this.txtTemplateFile.Location = new System.Drawing.Point(96, 47);
            this.txtTemplateFile.Name = "txtTemplateFile";
            this.txtTemplateFile.Size = new System.Drawing.Size(220, 20);
            this.txtTemplateFile.TabIndex = 7;
            // 
            // lblTemplate
            // 
            this.lblTemplate.AutoSize = true;
            this.lblTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemplate.Location = new System.Drawing.Point(8, 48);
            this.lblTemplate.Name = "lblTemplate";
            this.lblTemplate.Size = new System.Drawing.Size(85, 15);
            this.lblTemplate.TabIndex = 6;
            this.lblTemplate.Text = "Template File:";
            // 
            // btnSourceBrowse
            // 
            this.btnSourceBrowse.Location = new System.Drawing.Point(324, 77);
            this.btnSourceBrowse.Name = "btnSourceBrowse";
            this.btnSourceBrowse.Size = new System.Drawing.Size(80, 30);
            this.btnSourceBrowse.TabIndex = 5;
            this.btnSourceBrowse.Text = "Browse";
            this.btnSourceBrowse.UseVisualStyleBackColor = true;
            this.btnSourceBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtSourceFile
            // 
            this.txtSourceFile.Location = new System.Drawing.Point(96, 83);
            this.txtSourceFile.Name = "txtSourceFile";
            this.txtSourceFile.Size = new System.Drawing.Size(220, 20);
            this.txtSourceFile.TabIndex = 4;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileName.Location = new System.Drawing.Point(8, 83);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(72, 15);
            this.lblFileName.TabIndex = 3;
            this.lblFileName.Text = "Source File:";
            // 
            // rdReplace
            // 
            this.rdReplace.AutoSize = true;
            this.rdReplace.Location = new System.Drawing.Point(193, 14);
            this.rdReplace.Name = "rdReplace";
            this.rdReplace.Size = new System.Drawing.Size(86, 17);
            this.rdReplace.TabIndex = 2;
            this.rdReplace.TabStop = true;
            this.rdReplace.Text = "Replace TID";
            this.rdReplace.UseVisualStyleBackColor = true;
            // 
            // lblQueryType
            // 
            this.lblQueryType.AutoSize = true;
            this.lblQueryType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQueryType.Location = new System.Drawing.Point(8, 14);
            this.lblQueryType.Name = "lblQueryType";
            this.lblQueryType.Size = new System.Drawing.Size(71, 15);
            this.lblQueryType.TabIndex = 1;
            this.lblQueryType.Text = "Query Type:";
            // 
            // rdQC
            // 
            this.rdQC.AutoSize = true;
            this.rdQC.Location = new System.Drawing.Point(94, 14);
            this.rdQC.Name = "rdQC";
            this.rdQC.Size = new System.Drawing.Size(93, 17);
            this.rdQC.TabIndex = 0;
            this.rdQC.TabStop = true;
            this.rdQC.Text = "Quality Control";
            this.rdQC.UseVisualStyleBackColor = true;
            this.rdQC.CheckedChanged += new System.EventHandler(this.rdQC_CheckedChanged);
            // 
            // gbProgress
            // 
            this.gbProgress.Controls.Add(this.pgBar);
            this.gbProgress.Location = new System.Drawing.Point(4, 117);
            this.gbProgress.Name = "gbProgress";
            this.gbProgress.Size = new System.Drawing.Size(412, 42);
            this.gbProgress.TabIndex = 1;
            this.gbProgress.TabStop = false;
            // 
            // pgBar
            // 
            this.pgBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgBar.Location = new System.Drawing.Point(4, 11);
            this.pgBar.Name = "pgBar";
            this.pgBar.Size = new System.Drawing.Size(403, 25);
            this.pgBar.TabIndex = 0;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(4, 159);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(412, 49);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCancel.Location = new System.Drawing.Point(322, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 30);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSave.Location = new System.Drawing.Point(236, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 30);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Compare";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // bwWorker
            // 
            this.bwWorker.WorkerReportsProgress = true;
            this.bwWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwWorker_DoWork);
            this.bwWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwWorker_ProgressChanged);
            this.bwWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwWorker_RunWorkerCompleted);
            // 
            // frmQC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 208);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbProgress);
            this.Controls.Add(this.gbHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(435, 250);
            this.Name = "frmQC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Query Report - Compare";
            this.gbHeader.ResumeLayout(false);
            this.gbHeader.PerformLayout();
            this.gbProgress.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbHeader;
        private System.Windows.Forms.RadioButton rdReplace;
        private System.Windows.Forms.Label lblQueryType;
        private System.Windows.Forms.RadioButton rdQC;
        private System.Windows.Forms.Button btnSourceBrowse;
        private System.Windows.Forms.TextBox txtSourceFile;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.OpenFileDialog ofdBrowseFile;
        private System.Windows.Forms.GroupBox gbProgress;
        private System.Windows.Forms.ProgressBar pgBar;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.SaveFileDialog svdSaveFile;
        private System.ComponentModel.BackgroundWorker bwWorker;
        private System.Windows.Forms.Button btnTemplateBrowse;
        private System.Windows.Forms.TextBox txtTemplateFile;
        private System.Windows.Forms.Label lblTemplate;
    }
}