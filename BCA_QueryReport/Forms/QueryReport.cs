﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Data.SqlClient;
using InSysClass;

namespace InsysTools
{
    public partial class QueryReport : Form
    {
        private SqlConnection oConn;
        private bool isView = false;
        private DataTable dtQuery;

        private string sDateFrom = null;
        private string sDateTo = null;

        public QueryReport()
        {
            InitializeComponent();
        }

        private void QueryReport_Load(object sender, EventArgs e)
        {
            try
            {
                //this.WindowState = FormWindowState.Maximized;
                doInitConnection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (sfdExcel.ShowDialog() != DialogResult.Cancel)
                {
                    isView = false;
                    doEnableForm(false);
                    bwWorker.RunWorkerAsync(sfdExcel.FileName);
                    pbProcess.Style = ProgressBarStyle.Marquee;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                sDateFrom = sStartDate();
                sDateTo = sEndDate();
                
                isView = true;
                doEnableForm(false);
                pbProcess.Style = ProgressBarStyle.Marquee;
                bwWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bwWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                dtQuery = new DataTable();
                dtQuery.Load(drDataQuery());

                if (!isView)
                {
                    ExcelLib.WriteExcel(drDataQuery(), e.Argument.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bwWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (isView)
                doFillDataGrid();
            doEnableForm(true);
            pbProcess.Style = ProgressBarStyle.Blocks;
            //MessageBox.Show("Finished");
        }

        private void bwWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //pbProcess.Value = e.ProgressPercentage;

        }

        protected string sStartDate()
        {
            return dtpFrom.Value.ToString("yyyy-MM-dd HH:mm:ss");
        }

        protected string sEndDate()
        {
            return dtpTo.Value.ToString("yyyy-MM-dd HH:mm:ss");
        }

        protected string sCondition()
        {
            return string.Format(" WHERE LastView >= '{0}' AND LastView <= '{1}'", sStartDate(), sEndDate());
        }

        protected void doInitConnection()
        {
            InitData oInit = new InitData(Directory.GetCurrentDirectory());
            oConn = new SqlConnection(oInit.sGetConnString());
            //MessageBox.Show(oInit.sGetConnString());
            oConn.Open();
        }

        protected void doEnableForm(bool isEnable)
        {
            gbHead.Enabled = isEnable;
            gbData.Enabled = isEnable;
        }

        protected SqlDataReader drDataQuery()
        {
            SqlDataReader oRead;
            SqlCommand oComm = new SqlCommand(CommonSP.sSPQueryReport, oConn);
            oComm.CommandType = CommandType.StoredProcedure;
            oComm.Parameters.Add("@sDateStart", SqlDbType.VarChar).Value = sDateFrom;
            oComm.Parameters.Add("@sDateEnd", SqlDbType.VarChar).Value = sDateTo;
            oRead = oComm.ExecuteReader(); 

            return oRead;
        }

        protected void doFillDataGrid()
        {
            dgvData.DataSource = dtQuery;
        }

        protected int iProgress(int iCurr, int iTotal)
        {
            return Convert.ToInt32((iCurr / Convert.ToSingle(iTotal)) * 100);
        }

    }
}
