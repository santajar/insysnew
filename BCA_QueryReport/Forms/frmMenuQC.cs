﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InsysTools
{
    public partial class frmMenuQC : Form
    {
        public frmMenuQC()
        {
            InitializeComponent();
        }

        private void btnViewData_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                QueryReport oQR = new QueryReport();
                oQR.ShowDialog();
                this.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Show();
            }
        }

        private void btnCompareData_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                frmQC oQR = new frmQC();
                oQR.ShowDialog();
                this.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Show();
            }

        }
    }
}
