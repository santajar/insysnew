﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;
using System.Threading;
using InSysClass;

namespace InsysTools
{
    public partial class frmQC : Form
    {
        protected string sSourceFile;
        protected string sTemplateFile;
        protected string sResultFile;
        protected SqlConnection oConn;

        public frmQC()
        {
            InitializeComponent();
            doInitateForm();
            doInitiateConnection();
            //doInitiateFileDialogFilter();
        }

        protected void doInitateForm()
        {
            txtSourceFile.ReadOnly = true;
            txtTemplateFile.ReadOnly = true;
            rdQC.Checked = true;
        }

        protected void doInitiateConnection()
        {
            try
            {
                oConn = new SqlConnection(new InitData(Application.StartupPath).sGetConnString());
                oConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                oConn.Dispose();
                this.Close();
            }
        }

        protected void doInitiateFileDialogFilter(bool isCompare)
        {
            ofdBrowseFile.Filter = "Excel 97-2003 (*.xls)|*.xls";
            //if (isCompare)
            //    svdSaveFile.Filter = "Excel 97-2003 (*.xls)|*.xls";
            //else
            svdSaveFile.Filter = "Excel 2007  (*.xlsx)|*.xlsx";
        }

        //private void rdReplace_CheckedChanged(object sender, EventArgs e)
        //{
        //    txtTemplateFile.Enabled = btnTemplateBrowse.Enabled = rdReplace.Checked;
        //}

        private void rdQC_CheckedChanged(object sender, EventArgs e)
        {
            if (rdQC.Checked)
                txtTemplateFile.Clear();

            txtTemplateFile.Enabled = btnTemplateBrowse.Enabled = rdReplace.Checked;
        }

        private void btnTemplateBrowse_Click(object sender, EventArgs e)
        {
            doBrowseFile(false);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            doBrowseFile(true);
        }

        protected void doBrowseFile(bool isSource)
        {
            try
            {
                if (ofdBrowseFile.ShowDialog() != DialogResult.Cancel)
                {
                    if (isSource)
                    {
                        string sFileExcel = txtSourceFile.Text = ofdBrowseFile.FileName;
                        sSourceFile = Application.StartupPath + "\\" + ofdBrowseFile.SafeFileName;
                        doCopyFile(sFileExcel, sSourceFile);
                    }
                    else
                    {
                        string sFileExcel = txtTemplateFile.Text = ofdBrowseFile.FileName;
                        sTemplateFile = Application.StartupPath + "\\" + ofdBrowseFile.SafeFileName;
                        doCopyFile(sFileExcel, sTemplateFile);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (svdSaveFile.ShowDialog() != DialogResult.Cancel)
            {
                doEnableForm(false);
                bwWorker.RunWorkerAsync(svdSaveFile.FileName);
                sResultFile = svdSaveFile.FileName;
                pgBar.Style = ProgressBarStyle.Marquee;
            }
        }

        private void bwWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                bwWorker.WorkerReportsProgress = true;

                if (rdQC.Checked)
                {
                    //ExcelLib.WriteExcelQC(drTableResultQC(e.Argument.ToString()), e.Argument.ToString());
                    doWriteExcel(e.Argument.ToString());

                }
                else if (rdReplace.Checked)
                {
                    resultReplaceTID(e.Argument.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected void doWriteExcel(string sFileName)
        {
            try
            {
                DataTable dtTable = dtResultQC(sSourceFile);
                //DataTable dtTable = dtReadExcel(sSourceFile);
                if (dtTable != null)
                {
                    dtTable.TableName = "Result";
                    string sTemplateXLSX = Application.StartupPath + @"\TemplateXlsx.xlsx";

                    File.Delete(sFileName);
                    File.Copy(sTemplateXLSX, sFileName);

                    NewExcelOleDb oXlsOle = new NewExcelOleDb(sFileName);
                    oXlsOle.CreateWorkSheet(dtTable);
                    oXlsOle.InsertRow(dtTable);
                    oXlsOle.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bwWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                doEnableForm(true);
                if (File.Exists(sSourceFile))
                    File.Delete(sSourceFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                pgBar.Style = ProgressBarStyle.Blocks;
                pgBar.Value = 100;
            }
        }

        private void bwWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //progressBar1.Value = e.ProgressPercentage;
        }

        protected void doEnableForm(bool isEnabled)
        {
            gbHeader.Enabled = isEnabled;
            gbButton.Enabled = isEnabled;
        }

        protected void doCopyFile(string sOriginalFile, string sCopiedFile)
        {
            if (File.Exists(sCopiedFile))
                File.Delete(sCopiedFile);
            File.Copy(sOriginalFile, sCopiedFile, true);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected DataTable dtResultQC(string sFile)
        {
            try
            {
                DataTable oTable = new DataTable();

                doUploadFileExcel();

                using (SqlCommand oComm = new SqlCommand(CommonSP.sSPCompareData, oConn))
                {
                    oComm.CommandType = CommandType.StoredProcedure;
                    //oComm.Parameters.Add("@sPath", SqlDbType.VarChar).Value = sFile;
                    oComm.CommandTimeout = 0;
                    SqlDataAdapter oAdapt = new SqlDataAdapter(oComm);

                    oAdapt.Fill(oTable);

                    return oTable;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        protected void doUploadFileExcel()
        {
            doCleanTable();

            SqlBulkCopy sqlBulk = new SqlBulkCopy(oConn);
            if (rdQC.Checked)
            {
                sqlBulk.DestinationTableName = "TempXLS";

                #region "Mapping"
                SqlBulkCopyColumnMapping mapProdID = new SqlBulkCopyColumnMapping("[Product ID]", "TerminalID");
                SqlBulkCopyColumnMapping mapMID = new SqlBulkCopyColumnMapping("[Merchant ID]", "MID");
                SqlBulkCopyColumnMapping mapMerchantName = new SqlBulkCopyColumnMapping("[Merchant Name]", "MerchantName");
                SqlBulkCopyColumnMapping mapAlamat1 = new SqlBulkCopyColumnMapping("[Alamat 1]", "Alamat1");
                SqlBulkCopyColumnMapping mapAlamat2 = new SqlBulkCopyColumnMapping("[Alamat 2]", "Alamat2");
                SqlBulkCopyColumnMapping mapFlazz = new SqlBulkCopyColumnMapping("[Flazz]", "Flazz");
                SqlBulkCopyColumnMapping mapTopUP = new SqlBulkCopyColumnMapping("[TopUp]", "TopUp");
                SqlBulkCopyColumnMapping mapPayment = new SqlBulkCopyColumnMapping("Payment", "Payment");
                SqlBulkCopyColumnMapping mapBCACard = new SqlBulkCopyColumnMapping("[BCA Card]", "BCA_Card");
                SqlBulkCopyColumnMapping mapVisa = new SqlBulkCopyColumnMapping("Visa", "Visa");
                SqlBulkCopyColumnMapping mapMaster = new SqlBulkCopyColumnMapping("Master", "Master");
                SqlBulkCopyColumnMapping mapAmex = new SqlBulkCopyColumnMapping("Amex", "Amex");
                SqlBulkCopyColumnMapping mapDiners = new SqlBulkCopyColumnMapping("Diners", "Diners");
                SqlBulkCopyColumnMapping mapCicilan = new SqlBulkCopyColumnMapping("Cicilan", "Cicilan");
                SqlBulkCopyColumnMapping mapCicilanProg = new SqlBulkCopyColumnMapping("[Cicilan Prog]", "Cicilan_Prog");
                SqlBulkCopyColumnMapping mapDebit = new SqlBulkCopyColumnMapping("Debit", "Debit");
                SqlBulkCopyColumnMapping mapTunai = new SqlBulkCopyColumnMapping("Tunai", "Tunai");

                sqlBulk.ColumnMappings.Add(mapProdID);
                sqlBulk.ColumnMappings.Add(mapMID);
                sqlBulk.ColumnMappings.Add(mapMerchantName);
                sqlBulk.ColumnMappings.Add(mapAlamat1);
                sqlBulk.ColumnMappings.Add(mapAlamat2);
                sqlBulk.ColumnMappings.Add(mapFlazz);
                sqlBulk.ColumnMappings.Add(mapTopUP);
                sqlBulk.ColumnMappings.Add(mapPayment);
                sqlBulk.ColumnMappings.Add(mapBCACard);
                sqlBulk.ColumnMappings.Add(mapVisa);
                sqlBulk.ColumnMappings.Add(mapMaster);
                sqlBulk.ColumnMappings.Add(mapAmex);
                sqlBulk.ColumnMappings.Add(mapDiners);
                sqlBulk.ColumnMappings.Add(mapCicilan);
                sqlBulk.ColumnMappings.Add(mapCicilanProg);
                sqlBulk.ColumnMappings.Add(mapDebit);
                sqlBulk.ColumnMappings.Add(mapTunai);
                #endregion
            }
            else
            {
                sqlBulk.DestinationTableName = "TempExcel";
                SqlBulkCopyColumnMapping mapTIDLama = new SqlBulkCopyColumnMapping("TIDLama", "TIDLama");
                SqlBulkCopyColumnMapping mapTIDBaru = new SqlBulkCopyColumnMapping("TIDBaru", "TIDBaru");
                SqlBulkCopyColumnMapping mapMID = new SqlBulkCopyColumnMapping("MIDLama", "MIDLama");
                sqlBulk.ColumnMappings.Add(mapTIDLama);
                sqlBulk.ColumnMappings.Add(mapTIDBaru);
                sqlBulk.ColumnMappings.Add(mapMID);
            }

            sqlBulk.WriteToServer(dtReadExcel(sSourceFile));
        }

        protected void doCleanTable()
        {
            using (SqlCommand oComm = new SqlCommand((rdQC.Checked) ? CommonSP.sSPCleanTableCompare :
                                                              CommonSP.sSPCleanTableReplace, oConn))
            {
                oComm.CommandType = CommandType.StoredProcedure;
                //oComm.Parameters.Add("@sPath", SqlDbType.VarChar).Value = sFile;
                oComm.CommandTimeout = 0;
                oComm.ExecuteNonQuery();
            }
        }

        protected SqlDataReader drTableResultQC(string sFile)
        {
            try
            {
                using (SqlCommand oComm = new SqlCommand(CommonSP.sSPCompareData, oConn))
                {
                    SqlDataReader oRead;           
                    oComm.CommandType = CommandType.StoredProcedure;
                    oComm.Parameters.Add("@sPath", SqlDbType.VarChar).Value = sFile;
                    oRead = oComm.ExecuteReader();

                    return oRead;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private void resultReplaceTID(string sOutputFile)
        {
            try
            {
                doUploadFileExcel();

                using (SqlCommand oComm = new SqlCommand(CommonSP.sSPReplaceTID, oConn))
                {
                    SqlDataReader myReaderLama = null;
                    oComm.CommandType = CommandType.StoredProcedure;
                    //oComm.Parameters.Add("@sPath", SqlDbType.VarChar).Value = txtSourceFile.Text.ToString();
                    oComm.Parameters.Add("@iFlag", SqlDbType.SmallInt).Value = 0;
                    oComm.CommandTimeout = 0;
                    myReaderLama = oComm.ExecuteReader();

                    //createExcel(myReaderLama, 2, "output.xls", false);
                    createExcel(myReaderLama, 2, sTemplateFile, false);
                }
                using (SqlCommand oComm = new SqlCommand(CommonSP.sSPReplaceTID, oConn))
                {
                    SqlDataReader myReaderBaru = null;

                    oComm.CommandType = CommandType.StoredProcedure;
                    //oComm.Parameters.Add("@sPath", SqlDbType.VarChar).Value = txtSourceFile.Text.ToString();
                    oComm.Parameters.Add("@iFlag", SqlDbType.SmallInt).Value = 1;
                    myReaderBaru = oComm.ExecuteReader();
                    //string[] sArrFile = sOutputFile.Split('\\');
                    //createExcel(myReaderBaru, 3, DateTime.Today.Date.ToString().Replace('/', ' ').Substring(0, 10) + ".xls", true);
                    createExcel(myReaderBaru, 3, sOutputFile.Substring(0, sOutputFile.Length -5) + "XY.xlsx" , true);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void createExcel(SqlDataReader myReaderLama, int iRow, string sFileName, bool isCheck)
        {
            //Excel.Application oXL;
            //Excel._Workbook oWB;
            ////Excel.Sheets sheet;
            //Excel._Worksheet oSheet;
            ////Excel.Range oRng;
            //object misValue = System.Reflection.Missing.Value;
            ////string strCurrentDir = System.Windows.Forms.Application.StartupPath + "\\";
            //try
            //{
            //    oXL = new Excel.Application();
            //    oXL.Visible = false;
            //    //Get a new workbook.
            //    //oWB = (Excel._Workbook)(oXL.Workbooks.Add(misValue));

            //    //string workbookPath = strCurrentDir + fileName;//"output.xls";
            //    oWB = oXL.Workbooks.Open(sFileName ,
            //            0, false, 5, "", "", true, Excel.XlPlatform.xlWindows, "",
            //            true, false, 0, true, false, false);

            //    oSheet = (Excel._Worksheet)oWB.ActiveSheet;
            //    if (myReaderLama.HasRows)
            //    {
            //        while (myReaderLama.Read())
            //        {
            //            // For each row, print the values of each column.
            //            for (int colNo = 0; colNo < myReaderLama.FieldCount; colNo++)
            //            {
            //                oSheet.Cells[iRow, colNo + 1] = myReaderLama.GetValue(colNo);
            //                if (isCheck && ((colNo + 1) != 1))
            //                {
            //                    Excel.Range cell = oSheet.get_Range(oSheet.Cells[iRow - 1, colNo + 1], oSheet.Cells[iRow - 1, colNo + 1]);
            //                    Excel.Range cell2 = oSheet.get_Range(oSheet.Cells[iRow, colNo + 1], oSheet.Cells[iRow, colNo + 1]);
            //                    if (cell.Value2.ToString() != cell2.Value2.ToString())
            //                    {
            //                        oSheet.get_Range(oSheet.Cells[iRow - 1, colNo + 1], oSheet.Cells[iRow, colNo + 1]).Cells.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightBlue);
            //                    }
            //                }
            //            }
            //            iRow += 2;
            //        }
            //    }

            //    //MessageBox.Show(DateTime.Today.Date.ToString());

            //    //string strFile = strCurrentDir + DateTime.Today.Date.ToString().Replace('/', ' ').Substring(0, 10) + ".xls";
            //    string sTempFile = "";
            //    if (isCheck)
            //        sTempFile = sResultFile.Substring(0, sResultFile.Length - 5);
            //    else 
            //        sTempFile = sResultFile.Substring(0, sResultFile.Length - 5) + "XY";


            //    oWB.SaveAs(sTempFile, Excel.XlFileFormat.xlWorkbookDefault, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            //    oWB.Close(true, misValue, misValue);
            //    oXL.Quit();

            //    GC.Collect();

            //    releaseObject(oSheet);
            //    releaseObject(oWB);
            //    releaseObject(oXL);

            //    if (isCheck && File.Exists(sTempFile + "XY.xlsx"))
            //        File.Delete(sTempFile + "XY.xlsx");
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //}

        }

        protected DataTable dtReadExcel(string sFileName)
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (NewExcelOleDb oOleDbConn = new NewExcelOleDb(sFileName))
                {
                    doAddColumnNames(oOleDbConn);

                    dtTable = oOleDbConn.dtReadExcel(rdQC.Checked ? "QC" : "Sheet1");
                    oOleDbConn.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                dtTable = null;
            }
            return dtTable;
        }

        protected void doAddColumnNames (NewExcelOleDb oOleDb)
        {
            if (rdQC.Checked)
            {
                oOleDb.AddColumnName("[Product ID]");
                oOleDb.AddColumnName("[Merchant ID]");
                oOleDb.AddColumnName("[Merchant Name]");
                oOleDb.AddColumnName("[Alamat 1]");
                oOleDb.AddColumnName("[Alamat 2]");
                oOleDb.AddColumnName("[Flazz]");
                oOleDb.AddColumnName("[TopUp]");
                oOleDb.AddColumnName("Payment");
                oOleDb.AddColumnName("[BCA Card]");
                oOleDb.AddColumnName("Visa");
                oOleDb.AddColumnName("Master");
                oOleDb.AddColumnName("Amex");
                oOleDb.AddColumnName("Diners");
                oOleDb.AddColumnName("Cicilan");
                oOleDb.AddColumnName("[Cicilan Prog]");
                oOleDb.AddColumnName("Debit");
                oOleDb.AddColumnName("Tunai");
            }
        }
    
    }
}
