using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;
using System.IO;
using InSysClass;
using System.Configuration;
using System.Threading;


namespace InSysConsoleSoftware
{
    class Program
    {
        static bool bCheckAllowInit;
        static SqlConnection oConn = new SqlConnection();
        static string sConnString;
        static string sAppDirectory = Directory.GetCurrentDirectory();
        public static List<ClassInit> ltClassInit = new List<ClassInit>();

        #region "TCP/IP Connection"
        static bool IsConnTcpIpActive;
        static Thread threadIP;
        #endregion

        #region "Dial-Up Modem Connection"
        //static bool IsConnModemActive;

        //static string sModemPort1;
        //static string sModemPort2;
        //static string sModemPort3;
        //static string sModemPort4;
        //static string sModemPort5;

        //static SerialModem oSerModem1;
        //static SerialModem oSerModem2;
        //static SerialModem oSerModem3;
        //static SerialModem oSerModem4;
        //static SerialModem oSerModem5;
        #endregion

        static Thread threadTerminalUpload;
        static Thread threadTerminalCompletion;

        static void Main(string[] args)
        {
            try
            {
                CommonConsole.WriteToConsole("Initializing DATABASE connection....");
                oConn = InitConnection();
                Thread.Sleep(5000);

                if (oConn != null && oConn.State == ConnectionState.Open)
                {
                    CommonConsole.WriteToConsole("DATABASE Ready");

                    InitControlAllowInit();
                    AsyncSocket.sConnString = sConnString;
                    AsyncSocket.bCheckAllowInit = bCheckAllowInit;
                    //SerialModem.oConn = oConn;
                    //SerialModem.bCheckAllowInit = bCheckAllowInit;

                    InitConnType();

                    // Start the TcpIp thread
                    if (IsConnTcpIpActive)
                    {
                        CommonConsole.WriteToConsole("Ready...");

                        InitThreadTCPIP();
                        threadIP = new Thread(new ThreadStart(AsyncSocket.StartListening));
                        threadIP.Start();
                    }

                    // Start the Modem thread
                    //if (IsConnModemActive)
                    //{
                    //    CommonConsole.WriteToConsole("Ready...");

                    //    InitSerialModem();
                    //}
                    bool bEnableFtps = bool.Parse(ConfigurationManager.AppSettings["FTPS Supported"].ToString());
                    if (bEnableFtps)
                    {
                        CommonConsole.WriteToConsole("Start Upload");

                        TerminalUpload.sqlconn = oConn;
                        if (TerminalUpload.sqlconn.State != ConnectionState.Open) TerminalUpload.sqlconn.Open();
                        threadTerminalUpload = new Thread(new ThreadStart(TerminalUpload.Do_Upload));
                        threadTerminalUpload.Start();

                        TerminalCompletion.sqlconn = oConn;
                        if (TerminalCompletion.sqlconn.State != ConnectionState.Open) TerminalCompletion.sqlconn.Open();
                        threadTerminalCompletion = new Thread(new ThreadStart(TerminalCompletion.Do_CompletionReader));
                        threadTerminalCompletion.Start();

                        CommonConsole.WriteToConsole("End Upload");
                    }
                }
                else
                {
                    CommonConsole.WriteToConsole("Database Connection Failed!...");
                    Console.WriteLine("Press ENTER to Terminate or R to Restart the program...");
                    string sKey = Console.ReadLine();
                    if (sKey.ToUpper() == "R")
                    {
                        Process oInSys = new Process();
                        oInSys.StartInfo.FileName = sAppDirectory + "\\InSysConsoleSoftware.exe";
                        oInSys.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonConsole.WriteToConsole(ex.Message);
                //Process oInSys = new Process();
                //oInSys.StartInfo.FileName = sAppDirectory + "\\InSysConsole.exe";
                //oInSys.Start();
            }
        }

        #region "Function"
        /// <summary>
        /// Initialize all the needed variable
        /// </summary>
        static void InitSocketClass()
        {
            int iPort = 1800;
            Int64 iMaxConn = 1000;
            InitSocketControl(ref iMaxConn, ref iPort);
            AsyncSocket.iPort = iPort;
            //AsyncSocket.iConnMax = iMaxConn;
            //AsyncSocket.oConn = oConn;
            //AsyncSocket.bCheckAllowInit = bCheckAllowInit;
        }

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception ex)
            {
                oSqlTempConn = null;
                //Trace.Write("InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }

        /// <summary>
        /// Get the Port number configuration
        /// </summary>
        static int Port
        {
            get
            {
                string sPort = null;
                if (oConn.State == ConnectionState.Closed) oConn.Open();
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPControlFlagBrowse, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sItemName", SqlDbType.VarChar).Value = "PortDownload";

                SqlDataAdapter oda = new SqlDataAdapter(oCmd);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                if (dt.Rows.Count > 0)
                    sPort = dt.Rows[0]["Flag"].ToString();
                oCmd.Dispose();
                oda.Dispose();
                dt.Dispose();

                return int.Parse(sPort);
            }
        }

        static void InitSocketControl(ref Int64 _iConnMax, ref int _iPortInit)
        {
            //int _iPortDownload;
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPInitConnBrowse, oConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sMaxConn", SqlDbType.VarChar).Value = "InitMaxConn";
                oSqlCmd.Parameters.Add("@sInitPort", SqlDbType.VarChar).Value = "PortInit";
                oSqlCmd.Parameters.Add("@sSoftwarePort", SqlDbType.VarChar).Value = "PortDownload";

                if (oConn.State != ConnectionState.Open)
                {
                    oConn.Close();
                    oConn.Open();
                }

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                    {
                        _iConnMax = int.Parse(oRead["InitMaxConn"].ToString());
                        //_iPortInit = int.Parse(oRead["PortInit"].ToString());
                        _iPortInit = int.Parse(oRead["PortDownload"].ToString());
                    }

                    oRead.Close();
                }
            }
        }

        /// <summary>
        /// Set the Allow Initialization configuration
        /// </summary>
        static void InitControlAllowInit()
        {
            bCheckAllowInit = bGetControlAllowInit();
        }

        /// <summary>
        /// Get the Allow Init configuration from the database
        /// </summary>
        /// <returns>bool : boolean allow to initialize or not</returns>
        static bool bGetControlAllowInit()
        {
            bool bTemp = false;
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();

                SqlCommand oCmd = new SqlCommand(CommonSP.sSPFlagControlAllowInit, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter oda = new SqlDataAdapter(oCmd);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                if (dt.Rows.Count > 0)
                    bTemp = Convert.ToInt16(dt.Rows[0][0].ToString()) == 1 ? true : false;

                oCmd.Dispose();
                oda.Dispose();
                dt.Dispose();
            }
            catch (Exception ex)
            {
                //Trace.Write("InitControlAllowInit : " + ex.Message);
            }
            return bTemp;
        }

        static void InitThreadTCPIP()
        {
            InitSocketClass();
        }

        static void InitConnType()
        {
            InSysConsoleSoftwareConnType oConnType = new InSysConsoleSoftwareConnType(sAppDirectory);
            IsConnTcpIpActive = oConnType.IsConnTcpIpActive();
            //IsConnModemActive = oConnType.IsConnModemActive();

            //sModemPort1 = oConnType.Modem1;
            //sModemPort2 = oConnType.Modem2;
            //sModemPort3 = oConnType.Modem3;
            //sModemPort4 = oConnType.Modem4;
            //sModemPort5 = oConnType.Modem5;
        }

        //static void InitSerialModem()
        //{
        //    if (!string.IsNullOrEmpty(sModemPort1))
        //        oSerModem1 = new SerialModem(sModemPort1);
        //    Thread.Sleep(100);
        //    if (!string.IsNullOrEmpty(sModemPort2))
        //        oSerModem2 = new SerialModem(sModemPort2);
        //    Thread.Sleep(100);
        //    if (!string.IsNullOrEmpty(sModemPort3))
        //        oSerModem3 = new SerialModem(sModemPort3);
        //    Thread.Sleep(100);
        //    if (!string.IsNullOrEmpty(sModemPort4))
        //        oSerModem4 = new SerialModem(sModemPort4);
        //    Thread.Sleep(100);
        //    if (!string.IsNullOrEmpty(sModemPort5))
        //        oSerModem5 = new SerialModem(sModemPort5);
        //    Thread.Sleep(100);
        //}
        #endregion
    }
}
