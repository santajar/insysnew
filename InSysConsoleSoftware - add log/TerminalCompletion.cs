﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InSysConsoleSoftware
{
    public class TerminalCompletion
    {
        public static SqlConnection sqlconn;

        static string sRmtDirCompletion = @"IN\COMPLETION\";
        static string sLocDirCompletion = Environment.CurrentDirectory + @"\COMPLETION";
        static ServiceRefInSys.ServiceInSysClient serviceInSys = new ServiceRefInSys.ServiceInSysClient();

        protected class DataCompletion
        {
            public string sTerminalID = null;
            public string sSerialNumber = null;
            public string sInstallDate = null;
            public string sSV_Name = null;
            public string sFreeRAM = null;
            public string sTotalRAM = null;
        }

        internal static void Do_CompletionReader()
        {
            try
            {
                while (true)
                {
                    // read completion folder
                    string[] arrsFilenames = null;
                    //arrsFilenames = CommonConsole.serviceInSys.DirectoryListFtp(sRmtDirCompletion);
                    arrsFilenames = serviceInSys.DirectoryListFtp(sRmtDirCompletion);
                    if (arrsFilenames != null && arrsFilenames.Length > 0)
                    {
                        try
                        {
                            CommonConsole.loggerConsoleSw.Info("Start CompletionReader"+ arrsFilenames);
                            CommonConsole.WriteToConsole("Start CompletionReader"+ arrsFilenames);

                            foreach (string sFilename in arrsFilenames)
                            {
                                string sConsoleMessage = null;

                                if (!string.IsNullOrEmpty(sFilename))
                                {
                                    // download file
                                    string sLocalFilename = sLocDirCompletion + @"\" + sFilename;
                                    string sRemoteFilename = sRmtDirCompletion + @"\" + sFilename;
                                    byte[] arrbContent = new byte[2048];
                                    int iLength = 0;
                                    //CommonConsole.serviceInSys.DownloadFtp(sRemoteFilename, ref arrbContent, ref iLength);
                                    serviceInSys.DownloadFtp(sRemoteFilename, ref arrbContent, ref iLength);
                                    using (FileStream fs = new FileStream(sLocalFilename, FileMode.Create))
                                        fs.Write(arrbContent, 0, iLength);

                                    if (File.Exists(sLocalFilename))
                                    {
                                        sConsoleMessage = string.Format("Download '{0}' Success", sFilename);

                                        CommonConsole.loggerConsoleSw.Info(sConsoleMessage);
                                        CommonConsole.WriteToConsole(sConsoleMessage);

                                        //CommonConsole.serviceInSys.DeleteFtp(sRemoteFilename);
                                        serviceInSys.DeleteFtp(sRemoteFilename);

                                        // extract file
                                        DataCompletion dataCompletion = new DataCompletion();
                                        Do_ReadCompletionData(ref dataCompletion, sLocalFilename);

                                        // insert log to database
                                        WriteCompletionLog(dataCompletion, sFilename);
                                        sConsoleMessage = string.Format("Read COMPLETION '{0}' Success", sFilename);
                                    }
                                    else
                                        sConsoleMessage = string.Format("Download '{0}' Failed", sFilename);

                                    CommonConsole.WriteToConsole(sConsoleMessage);
                                }
                                Thread.Sleep(1000);
                            }

                            CommonConsole.loggerConsoleSw.Info("End CompletionReader" + arrsFilenames);
                            CommonConsole.WriteToConsole("End CompletionReader" + arrsFilenames);
                        }
                        catch (Exception ex)
                        {
                            CommonConsole.loggerConsoleSw.Error(ex);
                        }
                    }
                    // add sleep threading, in miliseconds
                    Thread.Sleep(10000);
                }
            }
            catch (Exception ex)
            {
                CommonConsole.loggerConsoleSw.Error(ex);
            }
        }

        protected static void WriteCompletionLog(DataCompletion dataCompletion, string sFilename)
        {
            if (!string.IsNullOrEmpty(dataCompletion.sTerminalID) && !string.IsNullOrEmpty(dataCompletion.sSerialNumber))
            {
                if (sqlconn.State != ConnectionState.Open) sqlconn.Open();
                using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_AuditDownloadInsert, sqlconn))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = dataCompletion.sTerminalID;
                    sqlcmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = dataCompletion.sSerialNumber;
                    sqlcmd.Parameters.Add("@sInstallDate", SqlDbType.VarChar).Value = dataCompletion.sInstallDate;
                    sqlcmd.Parameters.Add("@sSV_Name", SqlDbType.VarChar).Value = dataCompletion.sSV_Name;
                    sqlcmd.Parameters.Add("@sFreeRAM", SqlDbType.VarChar).Value = dataCompletion.sFreeRAM;
                    sqlcmd.Parameters.Add("@sTotalRAM", SqlDbType.VarChar).Value = dataCompletion.sTotalRAM;
                    sqlcmd.Parameters.Add("@sFilename", SqlDbType.VarChar).Value = sFilename;

                    sqlcmd.ExecuteNonQuery();
                }
            }
        }

        protected static void Do_ReadCompletionData(ref DataCompletion dataCompletion, string sFilename)
        {
            string sContent = null;
            using (FileStream fs = new FileStream(sFilename, FileMode.Open, FileAccess.Read))
            {
                if (fs.Length > 0)
                {
                    using (StreamReader sr = new StreamReader(fs))
                        sContent = sr.ReadToEnd();

                    int iIndex = 0;
                    string sTag = null;
                    string sLength = null;
                    string sValue = null;

                    while (iIndex < sContent.Length)
                    {
                        if ((sContent.ToCharArray())[iIndex] != '\0')
                        {
                            sTag = sContent.Substring(iIndex, 5);
                            iIndex += 5;

                            sLength = sContent.Substring(iIndex, 3);
                            iIndex += 3;

                            sValue = sContent.Substring(iIndex, int.Parse(sLength));
                            iIndex += int.Parse(sLength);

                            switch (sTag)
                            {
                                case "CM001": dataCompletion.sTerminalID = sValue; break;
                                case "CM002": dataCompletion.sSerialNumber = sValue; break;
                                case "CM003": dataCompletion.sFreeRAM = sValue; break;
                                case "CM004": dataCompletion.sTotalRAM = sValue; break;
                                case "CM101": dataCompletion.sInstallDate = sValue; break;
                                case "CM102": dataCompletion.sSV_Name = sValue; break;
                            }
                        }
                    }
                }
            }
        }
    }
}
