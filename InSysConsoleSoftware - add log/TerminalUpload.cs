﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Data;
using InSysClass;
using System.IO;
using System.IO.Compression;

namespace InSysConsoleSoftware
{
    public class TerminalUpload
    {
        public static SqlConnection sqlconn;
        
        static string sDirProfile = Environment.CurrentDirectory + @"\PROFILE\";
        static string sDirZipProfile = Environment.CurrentDirectory + @"\ZIP\";
        static byte[] arrbKey = (new UTF8Encoding()).GetBytes("316E67336E316330");
        static ServiceRefInSys.ServiceInSysClient serviceInSys = new ServiceRefInSys.ServiceInSysClient();

        static int i;
        internal static void Do_Upload()
        {
            try
            {
                while (true)
                {
                    string sConsoleMessage = null;

                    if (!Directory.Exists(sDirProfile)) Directory.CreateDirectory(sDirProfile);
                    if (!Directory.Exists(sDirZipProfile)) Directory.CreateDirectory(sDirZipProfile);
                    bool bGroupedTerminalID = false;

                    // get software version with ALL flag
                    DataTable datatableTerminalList = new DataTable();
                    bool bAllTerminal = IsUploadAllTerminal();

                    // if ALL, get all TerminalID
                    if (bAllTerminal)
                        datatableTerminalList = datatableGetAllTerminalList();
                    // if not, get ENABLED TerminalID on group table
                    else
                    {
                        datatableTerminalList = datatableGetGroupTerminalList();
                        bGroupedTerminalID = true;
                        CommonConsole.loggerConsoleSw.Info("Start datatableGetGroupTerminalList No All => "+ (i++).ToString());
                        CommonConsole.WriteToConsole("Start datatableGetGroupTerminalList No All => " + (i++).ToString());
                    }

                    // start processing per TerminalID
                    if (datatableTerminalList != null && datatableTerminalList.Rows.Count > 0)
                    {
                        try
                        {
                            CommonConsole.loggerConsoleSw.Info("Start Do Upload ");
                            CommonConsole.WriteToConsole("Start Do Upload ");
                            // read TerminalID profile, zip it and upload it without extention filename
                            foreach (DataRow row in datatableTerminalList.Rows)
                            {
                                string sTerminalID = null;
                                if (datatableTerminalList.Columns.Contains("TerminalID")) sTerminalID = (string)row["TerminalID"];
                                else sTerminalID = (string)row["TID"];

                                string sContent = null;
                                List<ContentProfile> _ltCPTemp = ltGetContentProfile(sTerminalID, ref sContent);
                                if (_ltCPTemp != null && _ltCPTemp.Count > 0)
                                {
                                    #region "GZ part"
                                    // write to file locally and compress
                                    string sFilename = sDirProfile + sTerminalID + ".PRO";
                                    string sGzFilename = sDirZipProfile + sTerminalID + ".gz";

                                    if (File.Exists(sFilename))
                                        File.Delete(sFilename);
                                    CommonLib.Write2File(sFilename, sContent, false);

                                    if (IsNetCompressSuccess(sFilename, sGzFilename))
                                    {
                                        byte[] arrbContent;
                                        using (FileStream fsRead = new FileStream(sGzFilename, FileMode.Open))
                                        {
                                            arrbContent = new byte[fsRead.Length];
                                            fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
                                        }
                                        string sGzRemoteFilename = @"OUT\PROFILE\" + Path.GetFileNameWithoutExtension(sGzFilename);

                                        UploadToFtp(arrbContent, sGzRemoteFilename);
                                        sConsoleMessage = string.Format("'{0}' Upload TerminalID Profile Success", sGzRemoteFilename);
                                        CommonConsole.loggerConsoleSw.Info(sConsoleMessage);
                                        CommonConsole.WriteToConsole(sConsoleMessage);

                                        if (bGroupedTerminalID)
                                        {
                                            string sSV_Name = row["SV_NAME"].ToString();
                                            string sRDSFilename = @"OUT\RDS\" + Path.GetFileNameWithoutExtension(sGzFilename) + @"\SV.txt";

                                            UploadToFtp(ASCIIEncoding.ASCII.GetBytes(sSV_Name), sRDSFilename);
                                            sConsoleMessage = string.Format("'{0}' Upload TerminalID SV.txt Success", sGzRemoteFilename);
                                            CommonConsole.loggerConsoleSw.Info(sConsoleMessage);
                                            CommonConsole.WriteToConsole(sConsoleMessage);
                                        }

                                        WriteLog(sTerminalID);
                                    }
                                    #endregion
                                }
                                Thread.Sleep(1000);
                            }
                            CommonConsole.loggerConsoleSw.Info("End Do Upload ");
                            CommonConsole.WriteToConsole("End Do Upload ");
                        }
                        catch (Exception ex)
                        {
                            CommonConsole.loggerConsoleSw.Error(ex);
                        }
                    }
                    //sleep timer in miliseconds, to put some delay
                    Thread.Sleep(10000);
                }
            }
            catch (Exception ex)
            {
                CommonConsole.loggerConsoleSw.Error(ex);
            }
        }

        protected static void WriteLog(string sTerminalID)
        {
            if (sqlconn.State != ConnectionState.Open) sqlconn.Open();
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_AuditUploadTerminalInsert, sqlconn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                sqlcmd.ExecuteNonQuery();
            }
        }

        protected static void UploadToFtp(byte[] arrbContent, string sRemoteFilename)
        {
            //CommonConsole.serviceInSys.UploadFileFtp(sRemoteFilename, arrbContent, arrbContent.Length);
            serviceInSys.UploadFileFtp(sRemoteFilename, arrbContent, arrbContent.Length);
        }

        protected static bool IsNetCompressSuccess(string sFilename, string sGzFilename)
        {
            bool bReturn = false;
            if (File.Exists(sFilename))
            {
                try
                {
                    FileInfo fi = new FileInfo(sFilename);
                    using (FileStream inFile = fi.OpenRead())
                    {
                        if (File.Exists(sGzFilename))
                            File.Delete(sGzFilename);
                        using (FileStream outFile = File.Create(sGzFilename))
                        {
                            using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                            {
                                byte[] buffer = new byte[fi.Length];
                                inFile.Read(buffer, 0, Convert.ToInt32(fi.Length));
                                compress.Write(buffer, 0, Convert.ToInt32(fi.Length));
                                bReturn = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CommonConsole.loggerConsoleSw.Error(ex);
                }
            }
            return bReturn;
        }

        protected static List<ContentProfile> ltGetContentProfile(string _sTerminalId, ref string _sContent)
        {
            List<ContentProfile> ltCPTemp = new List<ContentProfile>();

            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileTextFullTable, sqlconn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                DataTable dtTemp = new DataTable();
                if (sqlconn.State != ConnectionState.Open) sqlconn.Open();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);

                string[] arrsMaskedTag = arrsGetMaskedTag(_sTerminalId);

                if (dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow rowTemp in dtTemp.Rows)
                    {
                        string sTag = rowTemp["Tag"].ToString();
                        string sValue = rowTemp["TagValue"].ToString();
                        try
                        {
                            if (arrsMaskedTag.Length > 0 && arrsMaskedTag.Contains(sTag))
                                sValue = EncryptionLib.Decrypt3DES(CommonLib.sHexToStringUTF8(sValue), arrbKey);

                        }
                        catch (Exception)
                        {
                            sValue = rowTemp["TagValue"].ToString();
                        }

                        string sContent;
                        if (sTag != "AE37" && sTag != "AE38" && sTag != "TL11" && sTag.Substring(0, 2) != "PK")
                            sContent = string.Format("{0}{1:00}{2}", sTag, sValue.Length, sValue);
                        else
                        {
                            if (sTag == "AE37" || sTag == "AE38")
                                sContent = string.Format("{0}{1:000}{2}", sTag, sValue.Length, sValue);
                            else if (sTag == "PK04" && sTag == "PK06")
                                sContent = string.Format("{0}{1}{2}",
                                    CommonLib.sStringToHex(sTag),
                                    CommonLib.sStringToHex(string.Format("{0:000}", sValue.Length)),
                                    CommonLib.sStringToHex(sValue));
                            else
                                sContent = string.Format("{0}{1}{2}",
                                    sTag,
                                    string.Format("{0:000}", rowTemp["TagLength"]),
                                    sValue);
                            //sContent = string.Format("{0}{1}{2}",
                            //        CommonLib.sStringToHex(sTag),
                            //        CommonLib.sStringToHex(string.Format("{0:000}", rowTemp["TagLength"])),
                            //        sValue);
                        }
                        _sContent += sContent;
                        ContentProfile oCPTemp = new ContentProfile(sTag.Substring(0, 2), sContent);
                        int iIndexSearch = -1;
                        if (ltCPTemp.Count == 0)
                            ltCPTemp.Add(oCPTemp);
                        else
                            if ((iIndexSearch = ltCPTemp.FindIndex(oCPSearch => oCPSearch.Tag == oCPTemp.Tag)) > -1)
                        {
                            ContentProfile oCPSearchResult = ltCPTemp[iIndexSearch];
                            ContentProfile oCPNew = new ContentProfile(oCPTemp.Tag, oCPSearchResult.Content + oCPTemp.Content);
                            ltCPTemp[iIndexSearch] = oCPNew;
                        }
                        else
                            ltCPTemp.Add(oCPTemp);
                    }
                }
            }
            return ltCPTemp;
        }

        protected static string[] arrsGetMaskedTag(string _sTerminalId)
        {
            List<string> ltTag = new List<string>();
            string sQuery = string.Format("SELECT * FROM tbItemList WHERE DatabaseID IN (SELECT DatabaseID FROM tbProfileTerminalList WHERE TerminalID='{0}') AND vMasking=1", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, sqlconn))
            {
                DataTable dtTemp = new DataTable();
                if (sqlconn.State != ConnectionState.Open) sqlconn.Open();
                new SqlDataAdapter(oCmd).Fill(dtTemp);
                if (dtTemp != null && dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow row in dtTemp.Rows)
                        ltTag.Add(row["Tag"].ToString());
                }
            }
            return ltTag.ToArray();
        }

        protected static DataTable datatableGetGroupTerminalList()
        {
            DataTable dtTemp = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand(CommonSP.sSPRD_GroupDetailBrowse, sqlconn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "WHERE a.[ENABLED]=1 ";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                        if (reader.HasRows)
                            dtTemp.Load(reader);
                }
            }
            catch (Exception ex)
            {
                CommonConsole.loggerConsoleSw.Error(ex);
            }
            return dtTemp;
        }

        protected static DataTable datatableGetAllTerminalList()
        {
            DataTable dtTemp = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, sqlconn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                        if (reader.HasRows)
                            dtTemp.Load(reader);
                }
            }
            catch (Exception ex)
            {
                CommonConsole.loggerConsoleSw.Error(ex);
            }
            return dtTemp;
        }

        protected static bool IsUploadAllTerminal()
        {
            bool bValue = false;
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_SwVersionBrowse, sqlconn))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = " AND [ALL]=1";
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    {
                        if (reader.HasRows && reader.Read())
                            bValue = bool.Parse(reader["ALL"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                CommonConsole.loggerConsoleSw.Error(ex);
            }
            return bValue;
        }
    }
}
