using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using InSysClass;
using Nito.KitchenSink.CRC;

namespace InSysConsoleSoftware
{
    public enum ConnType
    {
        TcpIP = 1,
        Serial = 2,
        Modem = 3
    }

    public enum ProcCode
    {
        DownloadAppStartEnd = 990000,
        DownloadAppContinue = 990001,
        InstallmessageStartEnd = 990010,
        DownloadScheduleStartEnd = 990020,
    }

    class CommonConsole
    {
        //public static ServiceRefInSys.ServiceInSysClient serviceInSys = new ServiceRefInSys.ServiceInSysClient();
        public static InSysLogClass loggerConsoleSw = new InSysLogClass(Environment.CurrentDirectory + @"\LOGS\", "CONSOLE_SW", false);

        public static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);

        public static string sFullTime()
        {
            //return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt");
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sTimeHHMMSS()
        {
            return DateTime.Now.ToString("hhmmss", ciUSFormat);
        }

        public static string sDateMMddyy()
        {
            return DateTime.Now.ToString("MMddyy", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = CommonConsole.sFullTime();
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }

        public static void GetAppNameSN(string sBit48, ref string _sAppName, ref string _sSN)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            //Console.WriteLine("HexBit48: {0}", sHexBit48);
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            //Console.WriteLine("Len 48: {0}", iLenSN.ToString());
            _sSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            //Console.WriteLine("SN: {0}", _sSN);
            int iLenAppName = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2), 4));
            //Console.WriteLine("LenAppName: {0}", iLenAppName);
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2), iLenAppName * 2));

            //Console.WriteLine("AppName: {0}", _sAppName);
        }

        public static void GetCounterAppDLLastDLBuildNumber(string sBit57, ref int _iCounterApp, ref string _sAppNameDownload, ref string _sLastDownload, ref int _iBuildNumber)
        {
            string sHexBit57 = CommonLib.sStringToHex(sBit57).Replace(" ", "");
            //Console.WriteLine(string.Format("Bit57 From EDC : {0}", sHexBit57));
            _iCounterApp = int.Parse(sHexBit57.Substring(0, 4));
            _sLastDownload = sHexBit57.Substring(4, 10);
            int iLenApp = int.Parse(sHexBit57.Substring(14, 4));
            if (iLenApp > 0)
            {
                _sAppNameDownload = CommonLib.sHexToStringUTF7(sHexBit57.Substring(18, iLenApp * 2));
                _iBuildNumber = int.Parse(sHexBit57.Substring(18 + iLenApp * 2, 4));
            }
        }

        //public static string sGenerateISOSend(string sContent, ISO8583_MSGLib oIsoTemp,
        //    bool isLast, bool isEnable48, bool isEnableRemoteDownload,
        //    string sAppName)
        //{
        //    string sIsoResponse;
        //    ISO8583Lib oIso = new ISO8583Lib();
        //    oIsoTemp.MTI = "0810";
        //    oIsoTemp.sBinaryBitMap = "0010000000100000000000010000000000000010100000000000000000000000";
        //    ProcCode tempProcCode = (ProcCode)int.Parse(oIsoTemp.sBit[3].ToString());
        //    switch (tempProcCode)
        //    {
        //        case ProcCode.DownloadAppStartEnd:
        //        case ProcCode.DownloadAppContinue:
        //            oIso.SetField_Str(3, ref oIsoTemp,
        //                isLast ? ProcCode.DownloadAppStartEnd.GetHashCode().ToString() : ProcCode.DownloadAppContinue.GetHashCode().ToString());
        //            break;
        //    }

        //    oIso.SetField_Str(39, ref oIsoTemp, "00");

        //    if (isEnable48)
        //    {
        //        oIso.SetField_Str(12, ref oIsoTemp, string.Format("{0}", DateTime.Now.ToString("hhmmss", ciINAFormat)));

        //        oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMddyy}", DateTime.Now));

        //        oIso.SetField_Str(48, ref oIsoTemp, CommonLib.sStringToHex(oIsoTemp.sBit[48]));
        //    }

        //    if (isEnableRemoteDownload)
        //        oIso.SetField_Str(57, ref oIsoTemp, "1");

        //    if (!string.IsNullOrEmpty(sContent))
        //        oIso.SetField_Str(60, ref oIsoTemp, sContent);

        //    sIsoResponse = oIso.PackToISOHex(oIsoTemp);
        //    return sIsoResponse;
        //}

        public static string sGenerateISOError(string sContent, ISO8583_MSGLib oIsoTemp)
        {
            return string.Format("{0}{1}{2}{3}", oIsoTemp.sTPDU, oIsoTemp.sOriginAddr, oIsoTemp.sDestNII, sContent);
        }

        public static string sGenerateISOSend(string sContent, ISO8583_MSGLib oIsoTemp, bool bComplete, bool isEnable48, int iCounterApp, int iTotalCounter, string sAppNameDownload, int iBuildNumber, string sAppFileNameDownload, string sBit11, bool bSendCrc)
        {
            string sIsoResponse;
            string sCounterApp, sTotalCounter, sBuildNumber, sLenAppDL, sLenApp;
            int iLenAppDL, iLenApp;
            ISO8583Lib oIso = new ISO8583Lib();
            oIsoTemp.MTI = "0810";
            string a = sAppFileNameDownload;
            string b = sAppNameDownload;
            //oIsoTemp.sBinaryBitMap = "0010000000100000000000010000000000000010100000000000000000000000";
            if (bSendCrc == true) //validation to App use CRC
            {
                //bitmapRD with CRC
                oIsoTemp.sBinaryBitMap = "0010000000100000000000010000000000000010100000000000000000010000"; //ditambah bit 61 untuk crc
            }
            else
            {
                //bitmapRD without CRC
                oIsoTemp.sBinaryBitMap = "0010000000100000000000010000000000000010100000000000000000000000";
            }
            ProcCode tempProcCode = (ProcCode)int.Parse(oIsoTemp.sBit[3].ToString());
            switch (tempProcCode)
            {
                case ProcCode.DownloadAppStartEnd:
                case ProcCode.DownloadAppContinue:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        bComplete ? ProcCode.DownloadAppStartEnd.GetHashCode().ToString() : ProcCode.DownloadAppContinue.GetHashCode().ToString());
                    break;
            }

            oIso.SetField_Str(39, ref oIsoTemp, "00");
            oIso.SetField_Str(11, ref oIsoTemp, sBit11);

            if (isEnable48)
            {
                oIso.SetField_Str(12, ref oIsoTemp, string.Format("{0}", DateTime.Now.ToString("HHmmss", ciINAFormat)));

                oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));

                oIso.SetField_Str(48, ref oIsoTemp, CommonLib.sStringToHex(oIsoTemp.sBit[48]));
            }

            sCounterApp = iCounterApp.ToString().PadLeft(4, '0');
            sTotalCounter = iTotalCounter.ToString().PadLeft(4, '0');
            sBuildNumber = iBuildNumber.ToString().PadLeft(4, '0');
            sLenAppDL = sAppNameDownload.Length.ToString().PadLeft(4, '0');
            sLenApp = sAppFileNameDownload.Length.ToString().PadLeft(4, '0');

            oIso.SetField_Str(57, ref oIsoTemp, iCounterApp == 0 ? string.Format("{0}{1}{2}{3}{4}{5}{6}", sCounterApp, sTotalCounter, sLenAppDL, sAppNameDownload, sBuildNumber, sLenApp, sAppFileNameDownload) : string.Format("{0}{1}{2}{3}{4}{5}", sCounterApp, sLenAppDL, sAppNameDownload, sBuildNumber, sLenApp, sAppFileNameDownload));

            if (!string.IsNullOrEmpty(sContent))
                oIso.SetField_Str(60, ref oIsoTemp, sContent);
            if (bSendCrc == true)
            {
                string sCrc = CalcCrc(sContent);
                oIso.SetField_Str(61, ref oIsoTemp, sCrc);
            }
            //else
            //{
            //oIso.SetField_Str(61, ref oIsoTemp, sc);
            // }

            //sIsoResponse = oIso.PackToISOHexDownload(oIsoTemp);
            sIsoResponse = oIso.PackToISOHex(oIsoTemp, 1);
            return sIsoResponse;
        }

        public static string CalcCrc(string _sContent)
        {
            byte[] arrbPlain = CommonLib.HexStringToByteArray(_sContent);
            CRC16 calcCrc = new CRC16();
            string sCRC = "";

            try
            {
                byte[] arrbCrcCount = calcCrc.ComputeHash(arrbPlain);
                sCRC = CommonLib.sByteArrayToHexString(arrbCrcCount).Replace(" ", "");
                //int iLenCrc = sCRC.Length;
                //sCRC = iLenCrc.ToString().PadLeft(4, '0') + sCRC;
                // sCRC = CommonLib.sByteArrayToHexString(arrbCrcCount);

            }
            catch (Exception ex)
            {
            }
            Trace.Write(string.Format("value CRC : {0} ", sCRC));
            return sCRC;
        }
    }
}