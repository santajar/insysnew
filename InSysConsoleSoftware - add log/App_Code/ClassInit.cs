using System.Linq;
using System.Data;
using System;

namespace InSysConsoleSoftware
{
    public class ContentProfile
    {
        string sTag;
        public string Tag { get { return sTag; } }
        string sContent;
        public string Content { get { return sContent; } }

        public ContentProfile(string _sTag, string _sContent)
        {
            sTag = _sTag;
            sContent = _sContent;
        }
    }

    public class ClassInit
    {
        protected string sTerminalID;
        protected DataTable dtDownloadResponse = new DataTable();

        protected int iCounterID;
        protected string sSoftwareDLName;
        protected int iBuildNumber;
        protected string sFileName;
        protected string sContent;
        protected string sUploadTime;

        public ClassInit() { }
        public ClassInit(string _sTID)
        {
            sTerminalID = _sTID;
        }

        public DataTable dtResponses
        {
            set { dtDownloadResponse = value; }
        }

        public string TerminalID { get { return sTerminalID; } set { sTerminalID = value; } }
        public int CounterID { set { iCounterID = value; } }

        public string SoftwareDLName { get { return sGetSoftwareDLName(iCounterID); } }
        public int BuildNumber { get { return sGetBuildNumber(iCounterID); } }
        public string FileName { get { return sGetFileName(iCounterID); } }
        public string Content { get { return sGetContent(iCounterID); } }
        public string UploadTime { get { return sGetUploadTime(iCounterID); } }
        public bool isLastCounter { get { return sGetLastCounter(iCounterID); } }
        public bool isCounterValid { get { return bGetBoolCounter(iCounterID); } }
        public int TotalCounter { get { return iGetTotalCounter(); } }

        private string sGetSoftwareDLName(int iCounterID)
        {
            string sTempSoftwareDLName = "";
            try
            {
                sTempSoftwareDLName = dtDownloadResponse.Select(string.Format("Id = {0}", iCounterID))[0]["AppPackageName"].ToString();
            }
            catch (Exception ex)
            {
                Trace.Write(ex.Message);
            }
            return sTempSoftwareDLName;
        }

        private int sGetBuildNumber(int iCounterID)
        {
            int iTempBuildNumber = 0;
            try
            {
                iTempBuildNumber = int.Parse(dtDownloadResponse.Select(string.Format("Id = {0}", iCounterID))[0]["BuildNumber"].ToString());
            }
            catch (Exception ex)
            {
                Trace.Write(ex.Message);
            }
            return iTempBuildNumber;
        }

        private string sGetFileName(int iCounterID)
        {
            string sTempFileName = "";
            try
            {
                sTempFileName = dtDownloadResponse.Select(string.Format("Id = {0}", iCounterID))[0]["AppPackageFilename"].ToString();
            }
            catch (Exception ex)
            {
                Trace.Write(ex.Message);
            }
            return sTempFileName;
        }

        private string sGetContent(int iCounterID)
        {
            string sTempContent = "";
            try
            {
                sTempContent = dtDownloadResponse.Select(string.Format("Id = {0}", iCounterID))[0]["AppPackageContent"].ToString();
            }
            catch (Exception ex)
            {
                Trace.Write(ex.Message);
            }
            return sTempContent;
        }

        private string sGetUploadTime(int iCounterID)
        {
            string sTempUploadTime = "";
            try
            {
                sTempUploadTime = dtDownloadResponse.Select(string.Format("Id = {0}", iCounterID))[0]["UploadTime"].ToString();
                DateTime dtTempTime = DateTime.Parse(sTempUploadTime);
                sTempUploadTime = dtTempTime.ToString("ddMMyyHHmm");
            }
            catch (Exception ex)
            {
                Trace.Write(ex.Message);
            }
            return sTempUploadTime;
        }

        private bool sGetLastCounter(int iCounterID)
        {
            bool isLastCounter = false;
            try
            {
                if (dtDownloadResponse.Rows.Count - 1 == iCounterID)
                    isLastCounter = true;
            }
            catch (Exception ex)
            {
                Trace.Write(ex.Message);
            }
            return isLastCounter;
        }

        private bool bGetBoolCounter(int iCounterID)
        {
            bool isValid = false;
            try
            {
                if (dtDownloadResponse.Select(string.Format("Id = {0}", iCounterID)).Length == 1)
                    isValid = true;
            }
            catch (Exception ex)
            {
                Trace.Write(ex.Message);
            }
            return isValid;
        }

        private int iGetTotalCounter()
        {
            int iTotalCounter = 0;
            try
            {
                iTotalCounter = int.Parse(dtDownloadResponse.Rows.Count.ToString()) - 1;
            }
            catch (Exception ex)
            {
                Trace.Write(ex.Message);
            }
            return iTotalCounter;
        }
    }
}
