using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using InSysClass;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Configuration;

namespace InSysConsoleSoftware
{
    // State object for reading client data asynchronously
    public class StateObject
    {
        // Client  socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 2324;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }
    
    public class AsyncSocket
    {
        #region "TCP/IP"
        // Thread signal.
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        public static int iPort;
        public static string sConnString;
        public static bool bCheckAllowInit;
        //public static Int64 iConnMax;
        protected static int iConnSum = 0;

        /// <summary>
        /// 
        /// </summary>
        public static void StartListening()
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[2324];

            // Establish the local endpoint for the socket.
            // The DNS name of the computer
            // running the listener is "host.contoso.com".
            //IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            //IPAddress ipAddress = ipHostInfo.AddressList[0];
            //IPEndPoint localEndPoint = new IPEndPoint(ipAddress, iPort);
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, iPort); //perubahan 7/30/2015
            //IPEndPoint localEndPoint = new IPEndPoint((IPAddress)((IPHostEntry)(Dns.Resolve("localhost"))).AddressList[0], iPort);

            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    //Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);
                    listener.NoDelay = false; //penambahan 7/30/2015
                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }

            }
            catch (Exception ex)
            {
                Trace.Write(string.Format("Error Catch StartListening : {0} ", ex.StackTrace));
            }
        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            //CommonConsole.WriteToConsole("Waiting for incoming Initialize...");
            // Signal the main thread to continue.
            allDone.Set();
            try
            {
                // Get the socket that handles the client request.
                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                CommonConsole.WriteToConsole(string.Format("Accepted connection from {0}:{1}\nReady to receive initialize...",
                    ((IPEndPoint)handler.RemoteEndPoint).Address, ((IPEndPoint)handler.RemoteEndPoint).Port));

                // Create the state object.
                StateObject state = new StateObject();
                state.workSocket = handler;

                listener.NoDelay = false; //penambahan 7/30/2015
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
            }
            catch (Exception ex)
            {
                Trace.Write(string.Format("Error Catch AcceptCallBack : {0} ", ex.StackTrace));
            }
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            try
            {
                //Trace.Write("EDC Masuk Koneksi");
                String content = String.Empty;
                bool bDebug = bool.Parse(ConfigurationManager.AppSettings["Debug"].ToString());

                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket handler = state.workSocket;

                // Read data from the client socket. 
                //int bytesRead = handler.EndReceive(ar);

                // below updated on June 19, 2012.
                SocketError errorCode;
                int bytesRead = handler.EndReceive(ar, out errorCode);
                if (errorCode != SocketError.Success)
                    bytesRead = 0;

                if (bytesRead > 0)
                {
                    try
                    {
                        //if (iConnSum <= iConnMax)
                        //{
                        //iConnSum++;
                        //Logs.doWriteErrorFile("|Receive|iConnSum : " + iConnSum.ToString());
                        // There  might be more data, so store the data received so far.
                        state.sb.Append(Encoding.ASCII.GetString(
                            state.buffer, 0, bytesRead));

                        // Check for end-of-file tag. If it is not there, read 
                        // more data.
                        content = state.sb.ToString();
                        //Console.WriteLine(content);

                        // Not all data received. Get more.
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                            new AsyncCallback(ReadCallback), state);

                        //#region "Debug Receive Message"
                        //// Start the Iso message processing
                        //byte[] arrbDebug = new byte[256];
                        //Array.Copy(state.buffer, arrbDebug, 256);
                        //Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|",
                        //    iConnSum, 
                        //    CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""))); //debug
                        //#endregion

                        //Response oResponse = new Response(oConn, bCheckAllowInit);
                        //byte[] arrbTemp = oResponse.arrbResponse(state.buffer);
                        byte[] arrbReceive = new byte[StateObject.BufferSize];
                        Array.Copy(state.buffer, arrbReceive, bytesRead);
                        if (bDebug)
                            Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|",
                                iConnSum,
                                CommonLib.sByteArrayToHexString(arrbReceive).Replace(" ", "")));

                        //byte[] arrbSend = (new Response()).arrbResponse(arrbReceive, ConnType.TcpIP, oConn, bCheckAllowInit);
                        //if (arrbSend != null && arrbSend[2] != 0)
                        //{
                        //    #region "Debug Send Message"
                        //    //Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2} ",
                        //    //    iConnSum,
                        //    //    CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""),
                        //    //    CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", ""))); //debug
                        //    #endregion
                        //    Send(handler, arrbSend);
                        //}

                       
                        string sArrbReceive = CommonLib.sByteArrayToHexString(arrbReceive).Replace(" ", "").ToString();
                        int iStartLength = 0;

                        //foreach (string sTemp in sArrbParameter)
                        while (iStartLength < sArrbReceive.Length)
                        {
                            string sParameter = sArrbReceive.Substring(iStartLength, 4).ToString();
                            if (sParameter == "0000")
                                break;
                            int iLengthMessage = int.Parse(CommonLib.sConvertHextoDec(sParameter));
                            string sTemp = sArrbReceive.Substring(iStartLength, (iLengthMessage * 2) + 4);
                            iStartLength = iStartLength + (iLengthMessage * 2) + 4;

                            if (bDebug)
                                Trace.Write(string.Format("|iConnSum : {0}|Recv Parsing : {1}|",
                                    iConnSum, sTemp));

                            byte[] arrbSubReceive = new byte[StateObject.BufferSize];
                            arrbSubReceive = CommonLib.HexStringToByteArray(sTemp);
                            
                            Queue<Response> queueIncoming = new Queue<Response>();
                            Response oIncomingMsg = new Response(arrbSubReceive, sConnString, bCheckAllowInit, ConnType.TcpIP);
                            queueIncoming.Enqueue(oIncomingMsg);

                            while (queueIncoming.Count > 0)
                            {
                                byte[] arrbSend = new byte[StateObject.BufferSize];
                                Response oTempResponse = queueIncoming.Dequeue();
                                arrbSend = oTempResponse.arrbResponse();


                                if (arrbSend != null && arrbSend[2] != 0)
                                {
                                    #region "Debug Send Message" //dicomment 29 juli 2015
                                    if (bDebug)
                                        Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2} ",
                                            "",
                                            CommonLib.sByteArrayToHexString(arrbReceive).Replace(" ", ""),
                                            CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", "")));
                                    #endregion

                                    Thread.Sleep(100);
                                    // Send the response
                                    Send(handler, arrbSend);
                                    //Thread.Sleep(200);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Trace.Write(string.Format("Error Catch1 ReadCallBack : {0} ", ex.StackTrace));
                    }
                }
            }
            catch (Exception ex)
            {
                //Logs.doWriteErrorFile("[TCP] ReadCallBack Error : " + ex.ToString());
                Trace.Write(string.Format("Error Catch2 ReadCallBack : {0} ", ex.StackTrace));
            }
        }

        private static void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void Send(Socket handler, byte[] byteData)
        {
            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                //Console.WriteLine("Sent {0} bytes to client.", bytesSent);
                handler.ReceiveBufferSize = 0;
                handler.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer, 0);
            }
            catch (Exception e)
            {
                //Logs.doWriteErrorFile("[TCP] SendCallback Error : " + e.ToString());
                Trace.Write(string.Format("Error Catch SendCallBack : {0} ", e.StackTrace));
            }
        }
        #endregion
    }
}
