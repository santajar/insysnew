using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using InSysClass;
using System.Configuration;

namespace InSysConsoleSoftware
{
    class Response
    {
        protected static bool bDebug = false;

        protected ConnType cType;
        protected bool bCheckAllowInit;
        protected SqlConnection oConn;
        protected byte[] arrbReceive;

        public Response(byte[] _arrbReceive, string _sConnString, bool _IsCheckAllowInit, ConnType _cType) : this(_arrbReceive, _sConnString, _IsCheckAllowInit, _cType, false) { }
        public Response(byte[] _arrbReceive, string _sConnString, bool _IsCheckAllowInit, ConnType _cType, bool _bDebug)
        {
            try
            {
                oConn = new SqlConnection();
                oConn = SQLConnLib.EstablishConnection(_sConnString);
                bCheckAllowInit = _IsCheckAllowInit;
                arrbReceive = _arrbReceive;
                cType = _cType;
                bDebug = _bDebug;
                bDebug = bool.Parse(ConfigurationManager.AppSettings["Debug"].ToString());
            }
            catch (Exception ex)
            {
                Trace.Write("Create Object Error Response: " + ex.StackTrace);
                oConn.Close();
                oConn.Dispose();
            }
        }

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public byte[] arrbResponse()
        {
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);

                if (!string.IsNullOrEmpty(sSendMessage))
                {
                    arrbTemp = new byte[iIsoLen + 2];
                    //arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);

                    sSendMessage = sSendMessage.Replace(" ", ""); // Remove all white space
                    byte[] arrbbuffer = new byte[sSendMessage.Length / 2];
                    for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                    {
                        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);
                    }
                    arrbTemp = arrbbuffer;
                }
            }
            catch (Exception ex)
            {
                if (bDebug)
                    Trace.Write(string.Format("Error: arrbResponse arrbResponse| {0}", ex.StackTrace));
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTerminalId = null;
            string sTag = null;
            int iErrorCode = -1;

            string sProcessCode = "990000";
            string sAppName = null;
            string sSerialNum = null;
            int iCounterApp = 0;
            string sAppNameDownload = null;
            string sLastDownload = null;
            int iBuildNumber = 0;
            string sAppName2Download = null;
            string sAppFileNameDownload = null;
            int iTotalCounter = 0;
            bool bSendCrc = false;
            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            Trace.Write(string.Format("sTemp : {0}", sTemp));

            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            try
            {
                // parse received ISO
                ISO8583Lib oIsoParse = new ISO8583Lib();
                ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
                byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);

                oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
                string sProcCode = oIsoMsg.sBit[3];
                sTerminalId = oIsoMsg.sBit[41];
                string sBit11 = oIsoMsg.sBit[11];
                string sCrc = oIsoMsg.sBit[61];

                int iIndexClassInit = -1;

                // kirim ke sp,...
                try
                {
                    if (oIsoMsg.MTI == "0800")
                    {
                        iErrorCode = 0;
                        string sContent = null;
                        ProcCode oProcCodeTemp = (ProcCode)int.Parse(sProcCode);
                        //Console.WriteLine("PROCODE = {0} : ", oProcCodeTemp);
                        switch (oProcCodeTemp)
                        {
                            case ProcCode.InstallmessageStartEnd:
                            case ProcCode.DownloadScheduleStartEnd:
                                #region "NOT 990000"
                                if (oConn.State != ConnectionState.Open) oConn.Open();
                                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPDownloadProcessMessage, oConn))
                                {
                                    bCheckAllowInit = true;
                                    oCmd.CommandType = CommandType.StoredProcedure;
                                    oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                                    //oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                                    oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                                    #region "Using DataReader"
                                    using (SqlDataReader reader = oCmd.ExecuteReader())
                                    {
                                        while (reader.Read())
                                        {
                                            iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                            sTerminalId = reader["TerminalId"].ToString();
                                            sTempReceive = reader["ISO"].ToString();
                                            //sTempReceive = sTempReceive.Substring(1,14) + 
                                            sTag = reader["Tag"].ToString();
                                            //Console.WriteLine(sTempReceive.ToString());
                                            //string scont = reader["Content"].ToString();
                                        }
                                    }
                                    //if (sProcCode == "990020")
                                    //{
                                    //Console.WriteLine(string.Format("Procode Completion {0}", sProcCode));
                                    //Console.WriteLine(string.Format("sTemp {0}", sTemp));
                                    //Console.WriteLine(string.Format("iErrorCode {0}", iErrorCode.ToString()));
                                    //Console.WriteLine(string.Format("sTempReceive {0}", sTempReceive.ToString()));
                                    //Console.WriteLine(string.Format("sTag {0}", sTag.ToString()));
                                    //}
                                    #endregion
                                }
                                #endregion
                                break;
                            case ProcCode.DownloadAppStartEnd:
                                // bagian ini perlu di-review unt penyederhanaan function-function dan flow
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    if (IsRegisterTerminalID(sTerminalId))
                                    {
                                        if (IsAllowRemoteDownload(sTerminalId))
                                        {
                                            DataTable dtDownloadTemp;
                                            bool bComplete = false;
                                            #region "990000"
                                            try
                                            {
                                                sProcessCode = sProcCode;
                                                if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                                {
                                                    CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                                    bSendCrc = bValidSendCrc(sAppName);
                                                }

                                                if (!string.IsNullOrEmpty(oIsoMsg.sBit[57]))
                                                {
                                                    CommonConsole.GetCounterAppDLLastDLBuildNumber(oIsoMsg.sBit[57], ref iCounterApp, ref sAppNameDownload, ref sLastDownload, ref iBuildNumber);
                                                    if (iCounterApp != 0 && string.IsNullOrEmpty(sAppNameDownload)) break; //tedie
                                                }

                                                if (Program.ltClassInit.Count == 0)
                                                {
                                                    dtDownloadTemp = new DataTable();
                                                    dtDownloadTemp = dtGetDownloadTemp(sTerminalId);
                                                    if (dtDownloadTemp.Rows.Count > 0)
                                                    {
                                                        ClassInit oClass = new ClassInit(sTerminalId);
                                                        oClass.CounterID = 0;
                                                        oClass.dtResponses = dtDownloadTemp;
                                                        Program.ltClassInit.Add(oClass);
                                                        iIndexClassInit = 0;
                                                    }
                                                }
                                                else
                                                {
                                                    iIndexClassInit = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                                                    if (iIndexClassInit == -1)
                                                    {
                                                        dtDownloadTemp = new DataTable();
                                                        dtDownloadTemp = dtGetDownloadTemp(sTerminalId);
                                                        if (dtDownloadTemp.Rows.Count > 0)
                                                        {
                                                            ClassInit oClass = new ClassInit(sTerminalId);
                                                            oClass.CounterID = 0;
                                                            oClass.dtResponses = dtDownloadTemp;
                                                            Program.ltClassInit.Add(oClass);
                                                            iIndexClassInit = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId); // search masuk ke index berapa.
                                                        }
                                                    }
                                                }

                                                //Check Update Content in DB
                                                if (iCounterApp > 1 && iIndexClassInit == 0)
                                                {
                                                    if (isAppValid(sTerminalId, Program.ltClassInit[iIndexClassInit].UploadTime.ToString(), sAppNameDownload) == false)
                                                    {
                                                        iCounterApp = 0;
                                                        sAppNameDownload = "";
                                                        sAppFileNameDownload = ""; //penambahan baru 7 - 27 -2015
                                                    }
                                                }

                                                if (iIndexClassInit > -1)
                                                {
                                                    Program.ltClassInit[iIndexClassInit].CounterID = iCounterApp;
                                                    if (string.IsNullOrEmpty(sAppNameDownload))
                                                        sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName;
                                                    else
                                                        sAppName2Download = Program.ltClassInit[iIndexClassInit].SoftwareDLName;

                                                    if (!string.IsNullOrEmpty(sAppNameDownload) && !string.IsNullOrEmpty(sAppName2Download))
                                                    {
                                                        if (sAppNameDownload != sAppName2Download)
                                                            sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName;
                                                    }

                                                    sAppFileNameDownload = Program.ltClassInit[iIndexClassInit].FileName;
                                                    if (sAppName != sAppNameDownload)
                                                    {
                                                        if (sAppNameDownload != Program.ltClassInit[iIndexClassInit].SoftwareDLName && sAppName != sAppNameDownload && sAppName != sAppName2Download && sAppNameDownload != sAppName2Download && !string.IsNullOrEmpty(sAppName2Download))
                                                        {
                                                            UpdateSoftwareProgress(sAppName2Download, sSerialNum, sAppName, sAppNameDownload);

                                                            sAppNameDownload = sAppName2Download;
                                                            iCounterApp = 0;

                                                            dtDownloadTemp = new DataTable();
                                                            dtDownloadTemp = dtGetDownloadTemp(sTerminalId);
                                                            if (dtDownloadTemp.Rows.Count > 0)
                                                            {
                                                                ClassInit oClass = new ClassInit(sTerminalId);
                                                                oClass.CounterID = iCounterApp;
                                                                oClass.dtResponses = dtDownloadTemp;

                                                                if (iIndexClassInit > -1) Program.ltClassInit.RemoveAt(iIndexClassInit);
                                                                Program.ltClassInit.Add(oClass);
                                                            }

                                                            Program.ltClassInit[iIndexClassInit].CounterID = iCounterApp;
                                                            sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName; //ditambahkan 30 juni 2015
                                                            sAppFileNameDownload = Program.ltClassInit[iIndexClassInit].FileName;
                                                        }

                                                        if (Program.ltClassInit[iIndexClassInit].isCounterValid)
                                                        {
                                                            if (iCounterApp > 0 && (!string.IsNullOrEmpty(sAppNameDownload)
                                                                && sAppNameDownload != sAppName2Download
                                                                && !string.IsNullOrEmpty(sAppName2Download))
                                                                || IsDownloadTimeValid(sLastDownload.Trim(), sAppNameDownload, sTerminalId, Program.ltClassInit[iIndexClassInit].UploadTime) == false)
                                                            {
                                                                iCounterApp = 0;

                                                                if (!string.IsNullOrEmpty(sAppName2Download)) sAppNameDownload = sAppName2Download;

                                                                dtDownloadTemp = new DataTable();
                                                                dtDownloadTemp = dtGetDownloadTemp(sTerminalId);
                                                                if (dtDownloadTemp.Rows.Count > 0)
                                                                {
                                                                    ClassInit oClass = new ClassInit(sTerminalId);
                                                                    oClass.CounterID = iCounterApp;
                                                                    oClass.dtResponses = dtDownloadTemp;

                                                                    if (iIndexClassInit > -1) Program.ltClassInit.RemoveAt(iIndexClassInit);
                                                                    Program.ltClassInit.Add(oClass);
                                                                    //Console.WriteLine("ADD Software Content 3");
                                                                }

                                                                Program.ltClassInit[iIndexClassInit].CounterID = iCounterApp;
                                                                sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName; //ditambahkan 30 juni 2015
                                                                sAppFileNameDownload = Program.ltClassInit[iIndexClassInit].FileName; //penambahan baru 27 juni 2015
                                                            }

                                                            if (sAppNameDownload != sGetUpdateAppName(sTerminalId))
                                                            {
                                                                sAppNameDownload = "";
                                                                sAppFileNameDownload = "";
                                                                iCounterApp = 0;
                                                                dtDownloadTemp = new DataTable();
                                                                dtDownloadTemp = dtGetDownloadTemp(sTerminalId);

                                                                if (dtDownloadTemp.Rows.Count > 0)
                                                                {

                                                                    if (iIndexClassInit > -1)
                                                                        Program.ltClassInit.RemoveAt(iIndexClassInit);

                                                                    ClassInit oClass = new ClassInit(sTerminalId);
                                                                    oClass.CounterID = iCounterApp = 0;
                                                                    oClass.dtResponses = dtDownloadTemp;

                                                                    Program.ltClassInit.Clear();
                                                                    Program.ltClassInit.Add(oClass);
                                                                }

                                                                Program.ltClassInit[iIndexClassInit].CounterID = iCounterApp;
                                                                sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName;
                                                                sAppFileNameDownload = Program.ltClassInit[iIndexClassInit].FileName;
                                                            }

                                                            if (iCounterApp == 0)
                                                            {
                                                                sContent = Program.ltClassInit[iIndexClassInit].Content;
                                                                iBuildNumber = Program.ltClassInit[iIndexClassInit].BuildNumber;
                                                            }
                                                            else
                                                            {
                                                                sContent = Program.ltClassInit[iIndexClassInit].Content;
                                                            }

                                                            // flag untuk attribute Remote Download
                                                            if (!string.IsNullOrEmpty(sContent))
                                                            {
                                                                if (Program.ltClassInit[iIndexClassInit].isLastCounter)
                                                                {
                                                                    bComplete = true;
                                                                }
                                                                AuditInitSoftwareInsert(sTerminalId, sAppName, sAppNameDownload, sSerialNum, iCounterApp);

                                                                if (iCounterApp == 0)
                                                                {
                                                                    SoftwareProgressInsertUpdate(sTerminalId, sAppName, sAppNameDownload, sSerialNum, iCounterApp, iBuildNumber, true);
                                                                }
                                                                else
                                                                {
                                                                    SoftwareProgressInsertUpdate(sTerminalId, sAppName, sAppNameDownload, sSerialNum, iCounterApp, iBuildNumber, false);
                                                                }
                                                            }

                                                            //Generate ISO Message
                                                            iTotalCounter = Program.ltClassInit[iIndexClassInit].TotalCounter;
                                                            sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, true, iCounterApp, iTotalCounter, sAppNameDownload, iBuildNumber, sAppFileNameDownload, sBit11, bSendCrc);

                                                            if (bComplete)
                                                            {
                                                                iErrorCode = 1;
                                                                Program.ltClassInit.RemoveAt(iIndexClassInit);
                                                            }
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            iErrorCode = 6;
                                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INDEXNOTFOUND"), oIsoMsg);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        iErrorCode = 8;
                                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("SOFTWAREVERSIONALREADYDOWNLOAD"), oIsoMsg);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Trace.Write(string.Format("line 3 {0}", ex.StackTrace));
                                            }
                                            #endregion                                            
                                        }
                                        else
                                        {
                                            iErrorCode = 3;
                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("DOWNLOADNOTALLOWED"), oIsoMsg);
                                        }
                                    }
                                    else
                                    {
                                        iErrorCode = 11;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("DOWNLOADNOTSUPPORTED"), oIsoMsg);
                                        if (bDebug)
                                            Trace.Write(string.Format("TerminalID : {0} Invalid | EDC Message : {1} ", sTerminalId, sTemp)); //dicomment 29 juli 2015
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    if (bDebug)
                                        Trace.Write(string.Format("TerminalID : {0} Invalid | EDC Message : {1} ", sTerminalId, sTemp));
                                }
                                break;
                            case ProcCode.DownloadAppContinue:
                                // bagian ini perlu di-review unt penyederhanaan function-function dan flow
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    if (IsRegisterTerminalID(sTerminalId))
                                    {
                                        if (IsAllowRemoteDownload(sTerminalId))
                                        {
                                            DataTable dtDownloadTemp;
                                            bool bComplete = false;
                                            #region "990001"
                                            try
                                            {
                                                sProcessCode = sProcCode;
                                                if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                                {
                                                    CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                                    bSendCrc = bValidSendCrc(sAppName);
                                                }

                                                if (!string.IsNullOrEmpty(oIsoMsg.sBit[57]))
                                                {
                                                    CommonConsole.GetCounterAppDLLastDLBuildNumber(oIsoMsg.sBit[57], ref iCounterApp, ref sAppNameDownload, ref sLastDownload, ref iBuildNumber);
                                                    if (iCounterApp != 0 && string.IsNullOrEmpty(sAppNameDownload)) break; //tedie
                                                }

                                                iIndexClassInit = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                                                //Check Update Content in DB
                                                if (iCounterApp > 1 && iIndexClassInit == 0)
                                                {
                                                    if (isAppValid(sTerminalId, Program.ltClassInit[iIndexClassInit].UploadTime.ToString(), sAppNameDownload) == false)
                                                    {
                                                        iCounterApp = 0;
                                                        sAppNameDownload = "";
                                                        sAppFileNameDownload = ""; //penambahan baru 7 - 27 -2015
                                                    }
                                                }

                                                if (iCounterApp == 0 || iIndexClassInit == -1)
                                                {
                                                    sAppNameDownload = ""; //penambahan baru 7-27-2015
                                                    sAppName2Download = ""; //penambahan baru 7-27-2015
                                                    sAppFileNameDownload = ""; //penambahan baru 7-27-2015

                                                    dtDownloadTemp = new DataTable();
                                                    dtDownloadTemp = dtGetDownloadTemp(sTerminalId);
                                                    if (dtDownloadTemp.Rows.Count > 0)
                                                    {
                                                        ClassInit oClass = new ClassInit(sTerminalId);
                                                        oClass.CounterID = 0;
                                                        oClass.dtResponses = dtDownloadTemp;

                                                        if (iIndexClassInit > -1) Program.ltClassInit.RemoveAt(iIndexClassInit);
                                                        Program.ltClassInit.Add(oClass);
                                                    }
                                                }

                                                if (iIndexClassInit > -1)
                                                {
                                                    Program.ltClassInit[iIndexClassInit].CounterID = iCounterApp;
                                                    if (string.IsNullOrEmpty(sAppNameDownload))
                                                        sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName;
                                                    else
                                                        sAppName2Download = Program.ltClassInit[iIndexClassInit].SoftwareDLName;

                                                    if (!string.IsNullOrEmpty(sAppNameDownload) && !string.IsNullOrEmpty(sAppName2Download))
                                                    {
                                                        if (sAppNameDownload != sAppName2Download)
                                                            sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName;
                                                    }

                                                    sAppFileNameDownload = Program.ltClassInit[iIndexClassInit].FileName;
                                                    if (sAppName != sAppNameDownload)
                                                    {
                                                        if (sAppNameDownload != Program.ltClassInit[iIndexClassInit].SoftwareDLName && sAppName != sAppNameDownload && sAppName != sAppName2Download && sAppNameDownload != sAppName2Download && !string.IsNullOrEmpty(sAppName2Download))
                                                        {
                                                            UpdateSoftwareProgress(sAppName2Download, sSerialNum, sAppName, sAppNameDownload);

                                                            sAppNameDownload = sAppName2Download;
                                                            iCounterApp = 0;

                                                            dtDownloadTemp = new DataTable();
                                                            dtDownloadTemp = dtGetDownloadTemp(sTerminalId);
                                                            if (dtDownloadTemp.Rows.Count > 0)
                                                            {
                                                                ClassInit oClass = new ClassInit(sTerminalId);
                                                                oClass.CounterID = iCounterApp;
                                                                oClass.dtResponses = dtDownloadTemp;

                                                                if (iIndexClassInit > -1) Program.ltClassInit.RemoveAt(iIndexClassInit);
                                                                Program.ltClassInit.Add(oClass);
                                                            }

                                                            Program.ltClassInit[iIndexClassInit].CounterID = iCounterApp;
                                                            sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName; //ditambahkan 30 juni 2015
                                                            sAppFileNameDownload = Program.ltClassInit[iIndexClassInit].FileName;
                                                        }

                                                        if (Program.ltClassInit[iIndexClassInit].isCounterValid)
                                                        {
                                                            if (iCounterApp > 0 && (!string.IsNullOrEmpty(sAppNameDownload)
                                                                && sAppNameDownload != sAppName2Download
                                                                && !string.IsNullOrEmpty(sAppName2Download))
                                                                || IsDownloadTimeValid(sLastDownload.Trim(), sAppNameDownload, sTerminalId, Program.ltClassInit[iIndexClassInit].UploadTime) == false)
                                                            {
                                                                iCounterApp = 0;

                                                                if (!string.IsNullOrEmpty(sAppName2Download)) sAppNameDownload = sAppName2Download;

                                                                dtDownloadTemp = new DataTable();
                                                                dtDownloadTemp = dtGetDownloadTemp(sTerminalId);
                                                                if (dtDownloadTemp.Rows.Count > 0)
                                                                {
                                                                    ClassInit oClass = new ClassInit(sTerminalId);
                                                                    oClass.CounterID = iCounterApp;
                                                                    oClass.dtResponses = dtDownloadTemp;

                                                                    if (iIndexClassInit > -1) Program.ltClassInit.RemoveAt(iIndexClassInit);
                                                                    Program.ltClassInit.Add(oClass);
                                                                    //Console.WriteLine("ADD Software Content 3");
                                                                }

                                                                Program.ltClassInit[iIndexClassInit].CounterID = iCounterApp;
                                                                sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName; //ditambahkan 30 juni 2015
                                                                sAppFileNameDownload = Program.ltClassInit[iIndexClassInit].FileName; //penambahan baru 27 juni 2015
                                                            }

                                                            if (sAppNameDownload != sGetUpdateAppName(sTerminalId))
                                                            {
                                                                sAppNameDownload = "";
                                                                sAppFileNameDownload = "";
                                                                iCounterApp = 0;
                                                                dtDownloadTemp = new DataTable();
                                                                dtDownloadTemp = dtGetDownloadTemp(sTerminalId);

                                                                if (dtDownloadTemp.Rows.Count > 0)
                                                                {

                                                                    if (iIndexClassInit > -1)
                                                                        Program.ltClassInit.RemoveAt(iIndexClassInit);

                                                                    ClassInit oClass = new ClassInit(sTerminalId);
                                                                    oClass.CounterID = iCounterApp = 0;
                                                                    oClass.dtResponses = dtDownloadTemp;

                                                                    Program.ltClassInit.Clear();
                                                                    Program.ltClassInit.Add(oClass);
                                                                }

                                                                Program.ltClassInit[iIndexClassInit].CounterID = iCounterApp;
                                                                sAppNameDownload = Program.ltClassInit[iIndexClassInit].SoftwareDLName;
                                                                sAppFileNameDownload = Program.ltClassInit[iIndexClassInit].FileName;
                                                            }

                                                            if (iCounterApp == 0)
                                                            {
                                                                sContent = Program.ltClassInit[iIndexClassInit].Content;
                                                                iBuildNumber = Program.ltClassInit[iIndexClassInit].BuildNumber;
                                                            }
                                                            else
                                                            {
                                                                sContent = Program.ltClassInit[iIndexClassInit].Content;
                                                            }

                                                            // flag untuk attribute Remote Download
                                                            if (!string.IsNullOrEmpty(sContent))
                                                            {
                                                                if (Program.ltClassInit[iIndexClassInit].isLastCounter)
                                                                {
                                                                    bComplete = true;
                                                                }
                                                                AuditInitSoftwareInsert(sTerminalId, sAppName, sAppNameDownload, sSerialNum, iCounterApp);

                                                                if (iCounterApp == 0)
                                                                {
                                                                    SoftwareProgressInsertUpdate(sTerminalId, sAppName, sAppNameDownload, sSerialNum, iCounterApp, iBuildNumber, true);
                                                                }
                                                                else
                                                                {
                                                                    SoftwareProgressInsertUpdate(sTerminalId, sAppName, sAppNameDownload, sSerialNum, iCounterApp, iBuildNumber, false);
                                                                }
                                                            }

                                                            //Generate ISO Message
                                                            iTotalCounter = Program.ltClassInit[iIndexClassInit].TotalCounter;
                                                            sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, true, iCounterApp, iTotalCounter, sAppNameDownload, iBuildNumber, sAppFileNameDownload, sBit11, bSendCrc);

                                                            if (bComplete)
                                                            {
                                                                iErrorCode = 1;
                                                                Program.ltClassInit.RemoveAt(iIndexClassInit);
                                                            }
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            iErrorCode = 6;
                                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INDEXNOTFOUND"), oIsoMsg);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        iErrorCode = 8;
                                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("SOFTWAREVERSIONALREADYDOWNLOAD"), oIsoMsg);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Trace.Write(string.Format("line 3 {0}", ex.StackTrace));
                                            }
                                            #endregion                                            
                                        }
                                        else
                                        {
                                            iErrorCode = 3;
                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("DOWNLOADNOTALLOWED"), oIsoMsg);
                                        }
                                    }
                                    else
                                    {
                                        iErrorCode = 11;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("DOWNLOADNOTSUPPORTED"), oIsoMsg);
                                        if (bDebug)
                                            Trace.Write(string.Format("TerminalID : {0} Invalid | EDC Message : {1} ", sTerminalId, sTemp)); //dicomment 29 juli 2015
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    if (bDebug)
                                        Trace.Write(string.Format("TerminalID : {0} Invalid | EDC Message : {1} ", sTerminalId, sTemp));
                                }
                                break;
                            default:
                                iErrorCode = 2;
                                sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDPROCCODE"), oIsoMsg);
                                if (bDebug)
                                    Trace.Write(string.Format("TerminalID : {0} Invalid | Proccode : {1} Invalid | EDC Message : {2} ", sTerminalId, sProcCode, sTemp)); //dicomment 29 juli 2015
                                break;
                        }
                    }
                    else
                    {
                        iErrorCode = 5;
                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDMTI"), oIsoMsg);
                        if (bDebug)
                            Trace.Write(string.Format("TerminalID : {0} | MTI : {1} Invalid | EDC Message : {2} ", sTerminalId, oIsoMsg.MTI, sTemp));
                    }

                    if (!string.IsNullOrEmpty(sTempReceive))
                    {
                        _iIsoLenResponse = sTempReceive.Length / 2;
                        sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag) + " " + iCounterApp.ToString() + " " + "-->" + " " + iTotalCounter.ToString());
                    }
                }
                catch (Exception ex)
                {
                    if (bDebug)
                    {
                        //Console.WriteLine(ex.StackTrace);
                        Trace.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}", ex.StackTrace, sTemp));
                    }
                }
            }
            catch (Exception ex)
            {
                if (bDebug)
                    Trace.Write("Parsing Error: " + ex.StackTrace);
            }
            //Console.Write(sTempReceive);
            return sTempReceive;
        }

        protected void UpdateSoftwareProgress(string sAppName2Download, string sSerialNum, string sAppName, string sAppNameDownload)
        {
            string sQueryUpdateSoftwareProgress = string.Format("UPDATE tbSoftwareProgress SET SoftwareDownload='{0}' WHERE SerialNumber='{1}' AND Software='{2}' AND SoftwareDownload='{3}' AND Percentage != 100 AND InstallStatus = 'On Progress'", sAppName2Download, sSerialNum, sAppName, sAppNameDownload);
            if (oConn.State == ConnectionState.Closed) oConn.Open();
            SqlCommand sCmdQuery = new SqlCommand(sQueryUpdateSoftwareProgress, oConn);
            sCmdQuery.ExecuteNonQuery();
        }

        protected void SoftwareProgressInsertUpdate(string sTerminalId, string sAppName, string sAppNameDownload, string sSerialNum, int iCounterApp, int iBuildNumber, bool bInsert)
        {
            if (oConn.State == ConnectionState.Closed) oConn.Open();
            string sCmd = bInsert ? CommonSP.sSPSoftwareProgressInsert : CommonSP.sSPSoftwareProgressUpdate;
            using (SqlCommand oCmdAuditInitSoftwareInsert = new SqlCommand(sCmd, oConn))
            {
                oCmdAuditInitSoftwareInsert.CommandType = CommandType.StoredProcedure;
                oCmdAuditInitSoftwareInsert.CommandTimeout = 30;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@sSoftware", SqlDbType.VarChar).Value = sAppName;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@sSoftwareDownload", SqlDbType.VarChar).Value = sAppNameDownload;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = sSerialNum;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@fIndex", SqlDbType.Int).Value = iCounterApp;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@iBuildNumber", SqlDbType.Int).Value = iBuildNumber;
                if (!bInsert)
                    oCmdAuditInitSoftwareInsert.Parameters.Add("@sStatus", SqlDbType.VarChar).Value = "On Progress";
                oCmdAuditInitSoftwareInsert.ExecuteNonQuery();
            }
        }

        protected void AuditInitSoftwareInsert(string sTerminalId, string sAppName, string sAppNameDownload, string sSerialNum, int iCounterApp)
        {
            if (oConn.State == ConnectionState.Closed) oConn.Open();
            using (SqlCommand oCmdAuditInitSoftwareInsert = new SqlCommand(CommonSP.sSPAuditInitSoftwareInsert, oConn))
            {

                oCmdAuditInitSoftwareInsert.CommandType = CommandType.StoredProcedure;
                oCmdAuditInitSoftwareInsert.CommandTimeout = 60;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@sSoftware", SqlDbType.VarChar).Value = sAppName;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@SoftwareDownload", SqlDbType.VarChar).Value = sAppNameDownload;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@sSerialNum", SqlDbType.VarChar).Value = sSerialNum;
                oCmdAuditInitSoftwareInsert.Parameters.Add("@iIndex", SqlDbType.Int).Value = iCounterApp;
                oCmdAuditInitSoftwareInsert.ExecuteNonQuery();
            }
        }

        protected bool bValidSendCrc(string _sAppName)
        {
            bool bSendCrc = false;
            //oConn = new SqlConnection();
            //oConn = SQLConnLib.EstablishConnection(_sConnString);

            //oSqlConn = _oSqlConn;
            SqlCommand cmd = new SqlCommand(CommonSP.sSPCheckCrc, oConn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataReader dr;
            cmd.Parameters.Add("@sAppName", SqlDbType.VarChar).Value = _sAppName;
            if (oConn.State == ConnectionState.Closed) oConn.Open();
            dr = cmd.ExecuteReader();

            //var sValuedr = dr[0].ToString();
            //if (sValuedr == "1")
            while (dr.Read())
            {
                bool iValuedr = dr.GetBoolean(0);
                if (iValuedr == true)
                {
                    bSendCrc = true;
                }
                if (iValuedr == false)
                {
                    bSendCrc = false;
                }
            }
            //else
            //{
            //  bSendCrc = false;
            //}
            dr.Close();
            cmd.Connection.Close();

            return bSendCrc;
        }

        protected bool IsRegisterTerminalID(string _sTerminalId)
        {
            bool isAllow = false;
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPIsRegisterRemoteDownload, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@bReturn", SqlDbType.Bit).Value = isAllow;
                    oCmd.Parameters["@bReturn"].Direction = ParameterDirection.Output;

                    if (oConn.State != ConnectionState.Open)
                    {
                        oConn.Close();
                        oConn.Open();
                    }

                    oCmd.ExecuteNonQuery();
                    isAllow = (bool)oCmd.Parameters["@bReturn"].Value;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                Trace.Write(string.Format("Error isRegisterTerminalID {0}", ex.StackTrace));
            }
            return isAllow;
        }

        /// <summary>
        /// Determine Application Name
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>string: application Name</returns>
        public string sGetUpdateAppName(string sTerminalId)
        {
            string sUpdateAppName = "";
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPsGetDownloadApplicationName, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.NVarChar).Value = sTerminalId;

                    if (oConn.State != ConnectionState.Open)
                    {
                        oConn.Close();
                        oConn.Open();
                    }
                    using (SqlDataReader oRead = oCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sUpdateAppName = oRead[0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            return sUpdateAppName;
        }

        /// <summary>
        /// Determine software Content Valid
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <param name="sAppDate">string : time Download in temp Class</param>
        /// <param name="sAppNameDownload">string : Application Download Name</param>
        /// <returns>bool: true if valid</returns>
        protected bool isAppValid(string sTerminalId, string sAppDate, string sAppNameDownload)
        {
            string sValid = "";
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPCheckDataValid, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalId;
                    oCmd.Parameters.Add("@sAppDateTime", SqlDbType.VarChar).Value = sAppDate;
                    oCmd.Parameters.Add("@sAppNameDownload", SqlDbType.VarChar).Value = sAppNameDownload;
                    DataTable dtTemp = new DataTable();
                    (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                    sValid = dtTemp.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }

            if (sValid == "False" || sValid == "false")
                return false;
            else
                return true;
        }

        /// <summary>
        /// Get Software Content
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <returns>Data Table : Software Content</returns>
        protected DataTable dtGetDownloadTemp(string _sTerminalId)
        {

            if (oConn.State == ConnectionState.Closed) oConn.Open();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileRemoteDownloadData, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                return dtTemp;
            }

        }

        protected int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            try
            {
                if (cType == ConnType.TcpIP)
                    iReturn = CommonLib.iHexStringToInt(sTempLen);
                else if (cType == ConnType.Serial)
                    iReturn = int.Parse(sTempLen);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                Trace.Write(string.Format("|Error: iISOLenReceive sTempLen : {0}|@sMessage : {1}", sTempLen, ex.ToString()));
            }
            return iReturn;
        }

        protected string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            try
            {
                if (cType == ConnType.TcpIP || cType == ConnType.Modem)
                    sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
                else if (cType == ConnType.Serial)
                    sReturn = iLen.ToString("0000");
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
                Trace.Write(string.Format("Error: sIsoLenSend| {0}", ex.StackTrace));
            }


            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Download Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Download Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                case 6:
                    sTemp = "Index Not Found";
                    break;
                case 7:
                    sTemp = "Download Schedule Complete";
                    break;
                case 8:
                    sTemp = "Software Version already Download";
                    break;
                case 9:
                    sTemp = "Download Software";
                    break;
                case 10:
                    sTemp = "Install Complete";
                    break;
                case 11:
                    sTemp = "Download Not Support";
                    break;
                default:
                    sTemp = "Unknown Error";
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
            }
            return sTemp;
        }

        /// <summary>
        /// Determine Valid Terminal ID
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <returns>Bool : True if Valid</returns>
        protected bool IsValidTerminalID(string _sTerminalId)
        {
            bool isAllow = false;
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPIsValidTerminalID, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@bReturn", SqlDbType.Bit).Value = isAllow;
                    oCmd.Parameters["@bReturn"].Direction = ParameterDirection.Output;

                    if (oConn.State != ConnectionState.Open)
                    {
                        oConn.Close();
                        oConn.Open();
                    }

                    oCmd.ExecuteNonQuery();
                    isAllow = (bool)oCmd.Parameters["@bReturn"].Value;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return isAllow;
        }

        /// <summary>
        /// Determine Allow Download
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : true if allow</returns>
        protected bool IsAllowRemoteDownload(string sTerminalId)
        {
            bool isAllow = false;
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPIsRemoteDownloadAllowed, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.NVarChar).Value = sTerminalId;
                    oCmd.Parameters.Add("@bReturn", SqlDbType.Bit).Value = isAllow;
                    oCmd.Parameters["@bReturn"].Direction = ParameterDirection.Output;

                    if (oConn.State != ConnectionState.Open)
                    {
                        //    oConn.Close();
                        oConn.Open();
                    }

                    oCmd.ExecuteNonQuery();
                    isAllow = (bool)oCmd.Parameters["@bReturn"].Value;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);


            }
            return isAllow;
        }

        /// <summary>
        /// Determine Valid Time of Software Download
        /// </summary>
        /// <param name="_sLastDownload">string : Last Download</param>
        /// <param name="_sAppNameDownload">string : Download Software</param>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <param name="_sUploadTime">string : Upload Time Software</param>
        /// <returns>Bool : true if valid</returns>
        protected bool IsDownloadTimeValid(string _sLastDownload, string _sAppNameDownload, string _sTerminalId, string _sUploadTime)
        {
            bool bIsValid = false;
            try
            {
                string sUploadTime = _sUploadTime;
                //format DDMMYYhhmmss
                if (int.Parse(sUploadTime.Substring(4, 2)) >= int.Parse(_sLastDownload.Substring(4, 2))) /*Years*/
                {
                    if (int.Parse(sUploadTime.Substring(2, 2)) >= int.Parse(_sLastDownload.Substring(2, 2))) /*Month*/
                    {
                        if (int.Parse(sUploadTime.Substring(0, 2)) >= int.Parse(_sLastDownload.Substring(0, 2))) /*Days*/
                        {
                            if (int.Parse(sUploadTime.Substring(6, 2)) >= int.Parse(_sLastDownload.Substring(6, 2))) /*Hours*/
                            {
                                if (int.Parse(sUploadTime.Substring(8, 2)) >= int.Parse(_sLastDownload.Substring(8, 2))) /*Minute*/
                                    bIsValid = false;
                                else bIsValid = true;
                            }
                            else bIsValid = true;
                        }
                        else bIsValid = true;
                    }
                    else bIsValid = true;
                }
                else bIsValid = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            return bIsValid;
        }
    }
}