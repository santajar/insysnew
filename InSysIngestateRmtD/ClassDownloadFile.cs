﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using InSysClass;

namespace InSysIngestateRmtD
{
    public class ClassDownloadFile
    {
        #region "Protected Static variable"
        protected static string sPathRepository = null;
        protected static int iMaxByte = 1024;
        #endregion

        #region "Public Static variable"
        public static int MaxByte { get { return iMaxByte; } set { iMaxByte = value; } }
        public static string PathRepository { get { return sPathRepository; } set { sPathRepository = value; } }
        #endregion

        #region "Protected variable"
        protected string sFilename;
        protected int iIndex = 0;
        protected int iIndexTotal = 0;
        protected string sFullname;
        protected long lLengthPackage;
        protected bool bLastPackage = false;
        protected DateTime dtLastModify;
        protected byte[] arrbFileContent;
        #endregion

        #region "Public variable"
        public string Filename { get { return sFilename; } }
        public byte[] Content { get { return arrbContentIndexOf(); } }
        public int Index { get { return iIndex; } set { iIndex = value; } }
        public bool LastPackage { get { return (iIndex * iMaxByte) + iMaxByte < lLengthPackage ? false : true; } }
        public DateTime LastModify { get { return dtLastModify; } }
        public int IndexTotal { get { return iIndexTotal; } }
        #endregion

        public ClassDownloadFile(string _sFilename, string _sFullname, int _iIndex)
        {
            iIndex = _iIndex;
            sFilename = _sFilename;
            sFullname = _sFullname;
            lLengthPackage = (new FileInfo(sFullname)).Length;
            dtLastModify = (new FileInfo(sFullname)).LastWriteTime;
            arrbFileContent = new byte[lLengthPackage];
            arrbFileContent = arrbReadFileContent(sFullname);
            iIndexTotal = ((int)lLengthPackage / iMaxByte) + ((int)lLengthPackage % iMaxByte > 0 ? 1 : 0);
        }

        protected byte[] arrbReadFileContent(string sFullname)
        {
            byte[] arrbTemp = new byte[lLengthPackage];
            using (FileStream fs = new FileStream(sFullname, FileMode.Open, FileAccess.Read))
                fs.Read(arrbTemp, 0, (int)(lLengthPackage));                
            return arrbTemp;
        }

        protected byte[] arrbContentIndexOf()
        {
            byte[] arrbTemp;

            /* 
             * versi 1 
             * load saat ditemukan filenya
             */
            //using (FileStream fs = new FileStream(sFullname, FileMode.Open, FileAccess.Read))
            //{
            //    fs.Seek(iIndex * iMaxByte, SeekOrigin.Begin);
            //    if (lLengthPackage < (long)((iIndex + 1) * iMaxByte))
            //    {
            //        int iLength = (int)(lLengthPackage - (long)(iIndex * iMaxByte));
            //        arrbTemp = new byte[iLength];
            //        fs.Read(arrbTemp, 0, iLength);
            //    }
            //    else fs.Read(arrbTemp, 0, iMaxByte);
            //}
            
            /* 
             * versi 2 
             */
            if (lLengthPackage < (long)((iIndex + 1) * iMaxByte))
            {
                int iLength = (int)(lLengthPackage - (long)(iIndex * iMaxByte));                
                arrbTemp = new byte[iLength];
                Array.Copy(arrbFileContent, iIndex * iMaxByte, arrbTemp, 0, iLength);
                //for (int iCount = 0; iCount < iLength; iCount++)
                //    arrbTemp[iCount] = arrbFileContent[(long)(iIndex * iMaxByte) + iCount];
            }
            else
            {
                arrbTemp = new byte[iMaxByte];
                Array.Copy(arrbFileContent, iIndex * iMaxByte, arrbTemp, 0, iMaxByte);
            }
            return arrbTemp;
        }
    }
}
