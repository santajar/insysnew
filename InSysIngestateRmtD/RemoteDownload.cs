﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using InSysClass;
using System.IO;

namespace InSysIngestateRmtD
{
    public class RemoteDownload
    {
        protected SqlConnection oSqlConn; 
        protected string sTerminalId;
        protected string sSerialNumber;
        protected string sFilename;
        protected string sFullname;
        protected int iIndexDownload = -1;
        protected int iTotalDownload;
        protected string sLastModifyTerminal;

        protected ClassDownloadFile oFileDownload;
        protected int iIndexDownloadFile = 0;

        public RemoteDownload(string sSqlConnString)
        {
            // TODO: Complete member initialization
            oSqlConn = new SqlConnection(sSqlConnString);
            oSqlConn.Open();
        }

        public byte[] arrbResponse(byte[] _arrbReceive)
        {
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                while (iIndexDownload != iIndexDownloadFile)
                {
                    string sSendMessage = sParseGenResponse(_arrbReceive, ref iIsoLen);
                    if (!string.IsNullOrEmpty(sSendMessage))
                    {
                        arrbTemp = new byte[iIsoLen + 2];

                        sSendMessage = sSendMessage.Replace(" ", "");
                        byte[] arrbBuffer = new byte[sSendMessage.Length / 2];
                        for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                            arrbBuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);

                        arrbTemp = arrbBuffer;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write("|[RESPONSE]|Error arrbResponse : {0}", ex.Message);
            }
            return arrbTemp;
        }

        protected string sParseGenResponse(byte[] _arrbReceive, ref int _iIsoLenResponse)
        {
            string sTemp = CommonLib.sByteArrayToHexString(_arrbReceive).Replace(" ", "");
            int iLen = iISOLenReceive(sTemp.Substring(0, 4));
            // buang total length & tpdu
            sTemp = sTemp.Substring(6, (iLen - 1) * 2);

            string sTempReceive = null;
            int iErrorCode = -1;
            string sTag = null;
            string sContent = null;
            string sExtMessage = null;
            try
            {
                // parse received ISO
                ISO8583Lib oIsoParse = new ISO8583Lib();
                ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
                byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
                oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
                string sProcCode = oIsoMsg.sBit[3];
                sTerminalId = oIsoMsg.sBit[41];
                GetInfo(oIsoMsg);

                try
                {
                    if (oIsoMsg.MTI == "0800")
                    {
                        ProcCode oProcCodeTemp = (ProcCode)int.Parse(sProcCode);
                        switch (oProcCodeTemp)
                        {
                            case ProcCode.RemoteDownload:
                            case ProcCode.RemoteDownloadCont:
                                sExtMessage = sFilename;
                                if (Program.IsValidDownloadFilename(ref sFullname))
                                {
                                    if (IsValidTerminalSignature())
                                    {
                                        ClassDownloadFile oFileDownloadTemp = Program.listRmtDownloadFile.Find(x => x.Filename == sFilename);
                                        if (oFileDownloadTemp != null)
                                        {
                                            oFileDownload = oFileDownloadTemp;
                                            if (!IsValidLastModifyFile(oFileDownload))
                                            {
                                                iIndexDownload = 0;
                                                oFileDownload = new ClassDownloadFile(sFilename, sFullname, iIndexDownload);
                                                int iIndexFile = Program.listRmtDownloadFile.FindIndex(x => x.Filename == sFilename);
                                                Program.listRmtDownloadFile[iIndexFile] = oFileDownload;
                                            }
                                            
                                            oFileDownload.Index = iIndexDownload;
                                            sContent = CommonLib.sByteArrayToHexString(oFileDownload.Content).Replace(" ", "");
                                            bool bLastPackage = oFileDownload.LastPackage;
                                            string sMessage57 = string.Format("{0:0000}{1:0000}{2:yyMMddHHmmss}", oFileDownload.Index
                                                , oFileDownload.IndexTotal
                                                , oFileDownload.LastModify);
                                            sTempReceive = sGenerateISOSend(sContent, oIsoMsg, bLastPackage, true, true, false, sMessage57);
                                            iErrorCode = bLastPackage ? 8 : 7;
                                            iIndexDownloadFile = oFileDownload.Index;
                                            oFileDownload.Index = 0;
                                        }
                                        else
                                        {
                                            ClassDownloadFile oFileDownload = new ClassDownloadFile(sFilename, sFullname, iIndexDownload);
                                            Program.listRmtDownloadFile.Add(oFileDownload);
                                            sContent = CommonLib.sByteArrayToHexString(oFileDownload.Content).Replace(" ", "");
                                            bool bLastPackage = oFileDownload.LastPackage;
                                            string sMessage57 = string.Format("{0:0000}{1:0000}{2:yyMMddHHmmss}", oFileDownload.Index
                                                , oFileDownload.IndexTotal
                                                , oFileDownload.LastModify);
                                            sTempReceive = sGenerateISOSend(sContent, oIsoMsg, bLastPackage, true, true, false, sMessage57);
                                            iErrorCode = bLastPackage ? 8 : 7;
                                            iIndexDownloadFile = oFileDownload.Index;
                                            oFileDownload.Index = 0;
                                        }
                                    }
                                    else
                                    {
                                        sTempReceive = Program.sGenerateISOError(CommonLib.sStringToHex("INVALIDSIGNATURE"), oIsoMsg);
                                        iErrorCode = 10;
                                    }
                                }
                                else
                                {
                                    sTempReceive = Program.sGenerateISOError(CommonLib.sStringToHex("INVALIDFILENAME"), oIsoMsg);
                                    iErrorCode = 9;
                                }
                                break;
                        }
                    }
                    else
                    {
                        iErrorCode = 5;
                        sTempReceive = Program.sGenerateISOError(CommonLib.sStringToHex("INVALIDMTI"), oIsoMsg);
                    }
                    if (!string.IsNullOrEmpty(sTempReceive))
                    {
                        // tambahkan total length
                        _iIsoLenResponse = sTempReceive.Length / 2;
                        sTempReceive = sISOLenSend(_iIsoLenResponse) + sTempReceive;
                        //Console.Write(string.Format("|[RESPONSE]|{0}|ErrorCode : {2}|sTempReceive : {1}", sTerminalId, sTempReceive, iErrorCode));

                        //if (iErrorCode == 0 || iErrorCode == 1)
                        //string sMessage = (!string.IsNullOrEmpty(sTerminalId) ? sTerminalId : "SIGNATURE_ID") + " " + sProcessing(iErrorCode, sTag, sExtMessage);
                        string sMessage = (!string.IsNullOrEmpty(sTerminalId) ? sTerminalId : "SIGNATURE_ID")
                            + " IndexDownload : " + iIndexDownload.ToString()
                            + " IndexDownloadFile : " + iIndexDownloadFile.ToString()
                            + " " + sProcessing(iErrorCode, sTag, sExtMessage);
                        Program.WriteToConsole(sMessage);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                        ex.ToString(), sTemp));
                }
            }
            catch (Exception ex)
            {
                Console.Write("Parsing Error: " + ex.Message);
            }
            return sTempReceive;
        }

        private bool IsValidLastModifyFile(ClassDownloadFile oFileDownloadTemp)
        {
            //if ((new FileInfo(sFullname)).LastWriteTime != oFileDownloadTemp.LastModify)
            if (string.Format("{0:yyMMddHHmmss}", (new FileInfo(sFullname)).LastWriteTime) != sLastModifyTerminal)
                return false;
            return true;
        }

        protected bool IsValidTerminalSignature()
        {
            bool bReturn = true;
            string sQuery = string.Format("SELECT SIGNATURE FROM TERMINAL WHERE SIGNATURE = '{0}' OR SIGNATURE = '{1}'"
                , sTerminalId, sSerialNumber);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                using (SqlDataReader read = oCmd.ExecuteReader())
                    if (!read.HasRows)
                        bReturn = false;
            }
            return bReturn;
        }

        protected void GetInfo(ISO8583_MSGLib oIsoMsg)
        {
            string sBit48 = oIsoMsg.sBit[48];
            string sBit57 = oIsoMsg.sBit[57];

            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            sSerialNumber = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            int iLenAppName = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2), 4));
            sFilename = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2), iLenAppName * 2));
            sFullname = sFilename;

            string sHexBit57 = CommonLib.sStringToHex(sBit57).Replace(" ", "");
            iIndexDownload = int.Parse(sHexBit57.Substring(0, 4));
            iTotalDownload = int.Parse(sHexBit57.Substring(4, 4));
            sLastModifyTerminal = sHexBit57.Substring(8);
        }

        protected int iISOLenReceive(string sTempLen)
        {
            int iReturn = 0;
            iReturn = CommonLib.iHexStringToInt(sTempLen);
            return iReturn;
        }

        //protected string sISOLenSend(int iLen, ConnType cType)
        //{
        //    string sReturn = null;
        //    if (cType == ConnType.TcpIP || cType == ConnType.Modem)
        //        sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
        //    else if (cType == ConnType.Serial)
        //        sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
        //    return sReturn;
        //}

        protected string sISOLenSend(int iLen)
        {
            string sReturn = null;
            sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            return sReturn;
        }

        protected string sProcessing(int iCode, string sTag) { return sProcessing(iCode, sTag, null); }

        protected string sProcessing(int iCode, string sTag, string sMessage)
        {
            string sTemp = null;
            switch (iCode)
            {
                #region "Init"
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Initialize Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Initialize Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                case 6:
                    sTemp = "Application Not Allow";
                    break;
                #endregion
                #region "Download"
                case 7:
                    sTemp = "Downloading " + sMessage;
                    break;
                case 8:
                    sTemp = "Download COMPLETE";
                    break;
                case 9: sTemp = "Invalid FILENAME"; break;
                case 10: sTemp = "Invalid SIGNATURE ID"; break;
                #endregion
                default:
                    sTemp = "Unknown Error Code : " + iCode.ToString();
                    break;
            };
            return sTemp;
        }

        protected string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
                case "AI":
                    sTemp = "AID";
                    break;
                case "PK":
                    sTemp = "CAPK";
                    break;
                case "PP":
                    sTemp = "PinPad";
                    break;
                default:
                    //sTemp = "Echo";
                    break;
            }
            return sTemp;
        }

        //protected string sGenerateISOSend(string sContent, ISO8583_MSGLib oIsoTemp,
        //    bool isLast, bool isHexContent,
        //    bool isEnable48, bool isEnableCompress,
        //    string sBit57Message)
        //{
        //}

        protected string sGenerateISOSend(string sContent, ISO8583_MSGLib oIsoTemp,
            bool isLast, bool isHexContent,
            bool isEnable48, bool isEnableCompress,
            string sBit57Message)
        {
            string sIsoResponse;
            ISO8583Lib oIso = new ISO8583Lib();
            oIsoTemp.MTI = "0810";
            oIsoTemp.sBinaryBitMap = "0010000000100000000000010000000000000010100000000000000000000000";
            ProcCode tempProcCode = (ProcCode)int.Parse(oIsoTemp.sBit[3].ToString());
            switch (tempProcCode)
            {
                case ProcCode.ProfileStartEnd:
                case ProcCode.ProfileCont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        isLast ? ProcCode.ProfileStartEnd.GetHashCode().ToString() : ProcCode.ProfileCont.GetHashCode().ToString());
                    break;
                case ProcCode.AutoProfileStartEnd:
                case ProcCode.AutoProfileCont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        isLast ? ProcCode.AutoProfileStartEnd.GetHashCode().ToString() : ProcCode.AutoProfileCont.GetHashCode().ToString());
                    break;
                case ProcCode.TerminalEcho:
                    oIso.SetField_Str(3, ref oIsoTemp, ProcCode.TerminalEcho.GetHashCode().ToString());
                    break;
                case ProcCode.RemoteDownload:
                case ProcCode.RemoteDownloadCont:
                    oIso.SetField_Str(3, ref oIsoTemp, 
                        isLast ? ProcCode.RemoteDownload.GetHashCode().ToString() : ProcCode.RemoteDownloadCont.GetHashCode().ToString());
                    oIso.SetField_Str(57, ref oIsoTemp, sBit57Message);
                    break;
            }
            
            if (isEnable48)
            {
                oIso.SetField_Str(12, ref oIsoTemp, string.Format("{0}", DateTime.Now.ToString("hhmmss", Program.ciINAFormat)));
                //if (tempProcCode == ProcCode.TerminalEcho)
                //    oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));
                //else
                //    oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMddyy}", DateTime.Now));
                oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));
                oIso.SetField_Str(48, ref oIsoTemp, CommonLib.sStringToHex(oIsoTemp.sBit[48]));
            }
            oIso.SetField_Str(39, ref oIsoTemp, "00");

            if (isEnableCompress)
                oIso.SetField_Str(58, ref oIsoTemp, "1");

            if (!string.IsNullOrEmpty(sContent))
                oIso.SetField_Str(60, ref oIsoTemp, isHexContent ? sContent : CommonLib.sStringToHex(sContent));

            sIsoResponse = oIso.PackToISOHex(oIsoTemp,1);
            return sIsoResponse;
        }
    }
}
