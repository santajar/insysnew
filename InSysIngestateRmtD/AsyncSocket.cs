﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using InSysClass;
using System.Data.SqlClient;

namespace InSysIngestateRmtD
{
    // State object for reading client data asynchronously
    public class StateObject
    {
        // Client  socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 1024;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }

    public class AsyncSocket
    {
        #region "TCP/IP"
        // Thread signal.
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        public static int iPort;
        public static string sSqlConnString;
        public static bool bCheckAllowInit;
        public static int iConnMax;
        protected static int iConnSum = 0;
        
        /// <summary>
        /// 
        /// </summary>
        public static void StartListening()
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.
            // The DNS name of the computer
            // running the listener is "host.contoso.com".
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, iPort);

            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(150);

                while (true)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    Program.WriteToConsole("Waiting for a connection...");
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);

                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("[TCP] StartListen Error : " + e.ToString());
            }
        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            Program.WriteToConsole("Waiting for incoming Initialize...");
            // Signal the main thread to continue.
            allDone.Set();

            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            Program.WriteToConsole(string.Format("Accepted connection from {0}:{1} Ready to receive initialize...",
                ((IPEndPoint)handler.RemoteEndPoint).Address, ((IPEndPoint)handler.RemoteEndPoint).Port));

            Thread.Sleep(200);

            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            try
            {
                String content = String.Empty;

                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket handler = state.workSocket;

                // Read data from the client socket. 
                //int bytesRead = handler.EndReceive(ar);

                // below updated on June 19, 2012.
                SocketError errorCode;
                int bytesRead = handler.EndReceive(ar, out errorCode);
                if (errorCode != SocketError.Success)
                    bytesRead = 0;

                if (bytesRead > 0)
                {
                    //if (iConnSum <= iConnMax)
                    //{
                    //    iConnSum++;

                    // There  might be more data, so store the data received so far.
                    state.sb.Append(Encoding.ASCII.GetString(
                        state.buffer, 0, bytesRead));

                    // Check for end-of-file tag. If it is not there, read 
                    // more data.
                    content = state.sb.ToString();

                    // Not all data received. Get more.
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReadCallback), state);

                    #region "Debug Receive Message"
                    // Start the Iso message processing
                    byte[] arrbDebug = new byte[bytesRead];
                    Array.Copy(state.buffer, arrbDebug, bytesRead);
                    #endregion

                    byte[] arrbReceive = new byte[StateObject.BufferSize];
                    arrbReceive = state.buffer;

                    RemoteDownload oRmtDownload = new RemoteDownload(sSqlConnString);
                    byte[] arrbSend = oRmtDownload.arrbResponse(arrbReceive);
                    oRmtDownload = null;

                    if (arrbSend != null && arrbSend[2] != 0)
                    {
                        #region "Debug Send Message"
                        if (Program.bDebug)
                            Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2} ",
                                iConnSum,
                                CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""),
                                CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", "")));
                        #endregion

                        Thread.Sleep(300);

                        // Send the response
                        Send(handler, arrbSend);
                        //Console.WriteLine(string.Format("|Sent : SUCCESS"));
                    }
                }
            }
            catch (Exception ex)
            {
                //Logs.doWriteErrorFile("[TCP] ReadCallBack Error : " + ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }

        private static void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void Send(Socket handler, byte[] byteData)
        {
            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                //Console.WriteLine("Sent {0} bytes to client.", bytesSent);
                handler.ReceiveBufferSize = 0;
                handler.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer, 0);
            }
            catch (Exception)
            {
                //Logs.doWriteErrorFile("[TCP] SendCallback Error : " + e.ToString());
                //Console.WriteLine(e.ToString());
            }
        }
        #endregion
    }
}
