﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using InSysClass;
using System.IO;
using System.Threading;
using System.Globalization;

namespace InSysIngestateRmtD
{
    class Program
    {
        /*
         * required to get file :
         * 1. filename
         * 2. signature id of terminal
         */
        static string sSqlConnString;
        static string sRepoPath = "";
        static Thread threadIP;

        public static List<ClassDownloadFile> listRmtDownloadFile = new List<ClassDownloadFile>();
        public static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);
        public static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        public static bool bDebug = false;

        static void Main(string[] args)
        {
            try
            {
                Init();
                threadIP = new Thread(new ThreadStart(AsyncSocket.StartListening));
                threadIP.Start();
            }
            catch (Exception ex)
            {
                Console.Write("ERROR, Main : {0}", ex.Message);
                Console.Read();
            }
        }

        static void Init()
        {
            sRepoPath = ConfigurationManager.AppSettings.Get("Ingestate Download Repository");
            sSqlConnString = ConfigurationManager.ConnectionStrings["Database"].ConnectionString;

            SqlConnection oConn = new SqlConnection(sSqlConnString);
            oConn.Open();
            if (oConn.State == ConnectionState.Open) oConn.Close();

            string sMaxByte = ConfigurationManager.AppSettings.Get("Max Byte");
            ClassDownloadFile.PathRepository = sRepoPath;
            ClassDownloadFile.MaxByte = int.Parse(sMaxByte);
            AsyncSocket.iPort = int.Parse(ConfigurationManager.AppSettings.Get("Socket Port"));
            AsyncSocket.iConnMax = int.Parse(ConfigurationManager.AppSettings.Get("Max Thread"));
            AsyncSocket.sSqlConnString = sSqlConnString;
            bDebug = bool.Parse(ConfigurationManager.AppSettings.Get("Debug"));
        }

        /// <summary>
        /// IsValidDownloadFilename, will check on the folder wether filename is exist or not
        /// </summary>
        /// <param name="_sFilename"></param>
        /// <returns>TRUE, if filename exist. FALSE otherwise</returns>
        public static bool IsValidDownloadFilename(ref string _sFilename)
        {
            DirectoryInfo diRepository = new DirectoryInfo(sRepoPath);
            FileInfo[] arrFileList = diRepository.GetFiles();            
            foreach (FileInfo fiFileDownload in arrFileList)
                if (fiFileDownload.Name.ToLower() == _sFilename.ToLower())
                {
                    _sFilename = fiFileDownload.FullName;
                    return true;
                }
            return false;
        }

        public static string sGenerateISOError(string sContent, ISO8583_MSGLib oIsoTemp)
        {
            return string.Format("{0}{1}{2}{3}", oIsoTemp.sTPDU, oIsoTemp.sOriginAddr, oIsoTemp.sDestNII, sContent);
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static bool IsOpenFileAllowed(string sFilename)
        {
            bool bAllow = false;
            FileStream fs = null;
            try
            {
                fs = File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite);
                fs.Close();
                bAllow = true;
            }
            catch (IOException)
            {
                bAllow = false;
            }
            return bAllow;
        }

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = sFullTime();
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }
    }

    public enum ProcCode
    {
        ProfileStartEnd = 930000,
        ProfileCont = 930001,
        AutoProfileStartEnd = 960000,
        AutoProfileCont = 960001,
        Flazz = 940000,
        AutoPrompt = 950000,
        AutoCompletion = 970000,
        TerminalEcho = 980000,
        RemoteDownload = 990000,
        RemoteDownloadCont = 990001,
    }
}
