﻿using InSysClass;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmSoftwareProgressCPL : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        bool sFlag;
        public FrmSoftwareProgressCPL(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        // protected void LoadData(string sCity, string sGroup, string sRegion, string sTerminalID, string sSoftware, string sSoftwareDownload, string sBuildNumber, string sSerialNumber, string sInstallStatus)
        protected void LoadData()
        {
            try
            {
                int iIndexDGV = 0;
                if (dgProgress.SelectedCells.Count > 1)
                    iIndexDGV = dgProgress.CurrentRow.Index;
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareProgressBrowseCPL, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                //if ((!string.IsNullOrEmpty(sCity)) || (!string.IsNullOrEmpty(sGroup)) || (!string.IsNullOrEmpty(sRegion)) || (!string.IsNullOrEmpty(sTerminalID)) || (!string.IsNullOrEmpty(sSoftware)) || (!string.IsNullOrEmpty(sSoftwareDownload)) || (!string.IsNullOrEmpty(sBuildNumber)) || (!string.IsNullOrEmpty(sSerialNumber)) || (!string.IsNullOrEmpty(sInstallStatus)) || (chkStartTime.Checked == true) || (chkEndTime.Checked == true) || (chkFinishInstallTime.Checked == true))
                //    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGetAllCond();

                if (oSqlConn.State != ConnectionState.Open)
                {
                    //oSqlConn.Close();
                    oSqlConn.Open();
                }
                SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
                DataTable oDataTableInitLogConn = new DataTable();

                oAdapt.Fill(oDataTableInitLogConn);
                if (iIndexDGV > oDataTableInitLogConn.Rows.Count)
                {
                    dgProgress.CurrentCell = dgProgress.Rows[0].Cells[0];
                    sFlag = true;
                }
                else sFlag = false;
                dgProgress.DataSource = oDataTableInitLogConn;
                //txtTotalOnProgress.Text = oDataTableInitLogConn.Select("InstallStatus = 'On Progress'").Length.ToString();
                //txtTotalSucces.Text = oDataTableInitLogConn.Select("InstallStatus = 'Success'").Length.ToString();

                if (oDataTableInitLogConn.Rows.Count == 0)
                {
                    //ResetAllParameter();
                }
            }
            catch (Exception)
            {
                //CommonClass.doWriteErrorFile(ex.Message);
                //if there's no connection, placing this would show error message box over and over everytime United tries to refresh its data
            }
        }

        protected void LoadDataDetail()
        {
            int iID = iSelectedCellGridView();
            if (iID > 0)
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareProgressBrowseCPLDetail, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;
                //if ((!string.IsNullOrEmpty(sCity)) || (!string.IsNullOrEmpty(sGroup)) || (!string.IsNullOrEmpty(sRegion)) || (!string.IsNullOrEmpty(sTerminalID)) || (!string.IsNullOrEmpty(sSoftware)) || (!string.IsNullOrEmpty(sSoftwareDownload)) || (!string.IsNullOrEmpty(sBuildNumber)) || (!string.IsNullOrEmpty(sSerialNumber)) || (!string.IsNullOrEmpty(sInstallStatus)) || (chkStartTime.Checked == true) || (chkEndTime.Checked == true) || (chkFinishInstallTime.Checked == true))
                //    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGetAllCond();

                if (oSqlConn.State != ConnectionState.Open)
                {
                    //   oSqlConn.Close();
                    oSqlConn.Open();
                }
                SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);

                DataTable oDataTableInitLogConn = new DataTable();

                oAdapt.Fill(oDataTableInitLogConn);
                dgProgress.DataSource = oDataTableInitLogConn;
            }
        }

        private void FrmSoftwareProgressCPL_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void dgProgress_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == dgProgress.Columns["btnDetail"].Index))
            {
                if (btnDetail.Text == "View Detail")
                {
                    LoadDataDetail();
                    btnDetail.Text = "View Master";
                }
                else
                {
                    LoadData();
                    btnDetail.Text = "View Detail";
                }
            }
        }


        private int iSelectedCellGridView()
        {
            int iID = 0;
            if (dgProgress.SelectedCells != null)
            {
                int selectedrowindex = dgProgress.CurrentCell.RowIndex;
                DataGridViewRow selectedRow = dgProgress.Rows[selectedrowindex];
                //iID = Convert.ToString(selectedRow.Cells["ID"].Value);
                iID = Convert.ToInt32(selectedRow.Cells["ID"].Value);
            }
            return iID;
        }

        private void tmRefresh_Tick(object sender, EventArgs e)
        {
            if (btnDetail.Text == "View Detail")
            {
                LoadData();
            }
            else
            {
                LoadDataDetail();
            }
        }
        //protected string sGetAllCond()
        //{
        //    string sAllCondition, sCondTemp;
        //    int i;

        //   // sCondTemp = sGetCond(sCity, sGroup, sRegion, sTerminalID, sSoftware, sSoftwareDownload, sBuildNumber, sSerialNumber, sInstallStatus);

        //   //sAllCondition = sCondTemp.Substring(0, sCondTemp.Length - 4);

        //    return sAllCondition;
        //}

    }
}
