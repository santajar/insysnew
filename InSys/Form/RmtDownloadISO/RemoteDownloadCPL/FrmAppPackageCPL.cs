﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using InSysClass;

namespace InSys
{
    public partial class FrmAppPackageCPL : Form
    {
        SqlConnection oSqlConn;
        SqlConnection oSqlConnAuditTrail;
        public string sUID;
        string sAppPackagePath;
        string sAppPackageName;
        string sAppPackageNameOld;
        string sAppPackageFilename;
        bool isEdit = false;
        int iAppPackId;

        int iBrowseClickFlag = 0;

        DataTable dtDatabaseList;
        public FrmAppPackageCPL(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sUID)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUID = _sUID;
            InitializeComponent();
        }

        private void FrmAppPackageCPL_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Init();
            CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, "", CommonMessage.sFormOpened + "Software Package", "");
        }

        protected void Init()
        {
            dgAppPackage.DataSource = null;
            DataTable dtAppPackage = dtGetAppPackage();
            dgAppPackage.DataSource = dtAppPackage;

            if (dtAppPackage.Rows.Count == 0 || dtAppPackage == null)
            {
                //dgAppPackage.Columns["AppPackID"].Visible = false;
                //txtPackageName.Enabled = false;
                SetDisplayView(false);
            }
            else
                SetDisplayView(true);
            InitPrivilege();
            //InitDatabaseList();
        }

        protected void InitPrivilege()
        {
            cmsAppPackage.Items["addToolStripMenuItem"].Visible = UserPrivilege.IsAllowed(PrivilegeCode.SW, Privilege.Add);
            cmsAppPackage.Items["editToolStripMenuItem"].Visible = UserPrivilege.IsAllowed(PrivilegeCode.SW, Privilege.Edit); ;
            cmsAppPackage.Items["deleteToolStripMenuItem"].Visible = UserPrivilege.IsAllowed(PrivilegeCode.SW, Privilege.Delete); ;
        }

        protected DataTable dtGetAppPackage()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlDataReader oRead;
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageBrowse, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oRead = oCmd.ExecuteReader();

                if (oRead.HasRows)
                    dt.Load(oRead);

                oRead.Close();
                oRead.Dispose();
                oCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return dt;
        }

        protected void SetDisplayView(bool isEnable)
        {
            gbAppPackageView.Enabled = isEnable;
            gbSetting.Enabled = !isEnable;
            //if (!isEnable)
            //    FillEdcType();
            ClearSetting();
        }

        protected void ClearSetting()
        {
            txtAppPackageFilename.Clear();
            txtPackageName.Clear();
            txtAppPackageDesc.Clear();
            txtBuiltNumber.Clear();
            //txtBuildNumber.Clear();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdAppPackage = new OpenFileDialog();
            //ofdAppPackage.Filter = "Package Files(*.tar)|*.tar|PackageFile Ingenico(*.agn)|*.agn|Config Files(*.CFG)|*.CFG|Tetra Files(*.P3A)|*.P3A|Version Files(*.PAR)|*.PAR";
            ofdAppPackage.Filter = "All Files(*.*)|*.*";
            ofdAppPackage.ShowDialog();
            if (!string.IsNullOrEmpty(ofdAppPackage.FileName))
            {
                sAppPackagePath = sGetAppPackagePath(ofdAppPackage.FileName);
                sAppPackageFilename = sGetAppPackageFilename(ofdAppPackage.FileName);
                txtAppPackageFilename.Text = sAppPackageFilename;
                iBrowseClickFlag = 1;
            }
        }

        protected string sGetAppPackagePath(string sFullname)
        {
            return sFullname.Substring(0, sFullname.LastIndexOf(@"\"));
        }

        protected string sGetAppPackageFilename(string sFullname)
        {
            return sFullname.Substring(sFullname.LastIndexOf(@"\") + 1);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAppPackageFilename.Text))
            {
                if (!string.IsNullOrEmpty(txtPackageName.Text))
                {
                    sAppPackageName = txtPackageName.Text;
                    sAppPackageFilename = txtAppPackageFilename.Text;
                    if (!string.IsNullOrEmpty(txtBuiltNumber.Text))
                    {
                        if (!string.IsNullOrEmpty(sAppPackageName))
                        {
                            if (!isSoftwareNameAlreadyExist(txtPackageName.Text))
                            {
                                if (!isSoftwareNameAlreadyExist(txtPackageName.Text))
                                {
                                    SaveAppPackage();
                                }
                                else MessageBox.Show("Software Name Already Exist.");
                            }
                            else
                            {
                                EditAppPackage();
                                //EditAppPackage2(sSelectedAppPackage, iSelectedAppPackageId);
                            }
                        }
                        iBrowseClickFlag = 0;
                    }
                    else MessageBox.Show("Please Fill BuiltNumber.");
                }
                else MessageBox.Show("Please Fill Software Name.");
            }
            else MessageBox.Show("Please Choose File.");
        }

        protected void EditAppPackage()
        {
            try
            {
                byte[] arrbContent;
                if (iBrowseClickFlag == 1)
                {
                    arrbContent = arrbFileContent();
                    DeleteAppPackageTemp(sAppPackageNameOld, iAppPackId);
                    SaveAppPackageTemp(arrbContent, iAppPackId);
                }
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageEdit, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@iAppPackId", SqlDbType.Int).Value = iAppPackId;
                oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = sAppPackageName;
                oCmd.Parameters.Add("@sAppPackageFilename", SqlDbType.VarChar).Value = sAppPackageFilename;
                oCmd.Parameters.Add("@iBuildNumber", SqlDbType.VarChar).Value = txtBuiltNumber.Text;
                oCmd.Parameters.Add("@sAppPackageDesc", SqlDbType.VarChar).Value = sAppPackageDesc;
                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Init();
        }

        private void DeleteAppPackageTemp(string _sAppPackageName, int iAppPackId)
        {
            try
            {
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageTempDelete, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = _sAppPackageName;
                oCmd.Parameters.Add("@iAppPackId", SqlDbType.Decimal).Value = iAppPackId;

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool isSoftwareNameAlreadyExist(string sSoftwareName)
        {
            bool bReturn = true;

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.isSoftwareNameAlreadyExist('{0}')", sSoftwareName), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            DataTable dtTemp = new DataTable();
            new SqlDataAdapter(oCmd).Fill(dtTemp);

            bReturn = Convert.ToBoolean(dtTemp.Rows[0][0].ToString());
            return bReturn;
        }

        protected void SaveAppPackage()
        {

            if (IsSavePackage(sAppPackageName))
            {
                if (iBrowseClickFlag == 1)
                {
                    try
                    {
                        byte[] arrbContent = arrbFileContent();
                        int iSuccess = 0;
                        int iAppPackId = 0;
                        SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageAddContent, oSqlConn);
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.Parameters.Add("@iBuildNumber", SqlDbType.Int).Value = Convert.ToInt32(txtBuiltNumber.Text);
                        oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = sAppPackageName;
                        oCmd.Parameters.Add("@sAppPackageFilename", SqlDbType.VarChar).Value = sAppPackageFilename;
                        oCmd.Parameters.Add("@sAppPackageDesc", SqlDbType.VarChar).Value = sAppPackageDesc;
                        oCmd.Parameters.Add("@bAppPackageContent", SqlDbType.VarBinary).Value = arrbContent;
                        //oCmd.Parameters.Add("@iDatabaseID", SqlDbType.Decimal).Value = int.Parse(sDatabaseID);
                        //oCmd.Parameters.Add("@sEdcTypeId", SqlDbType.VarChar).Value = sEdcTypeId();

                        oCmd.Parameters.Add("@iSuccess", SqlDbType.Int).Value = iSuccess;
                        oCmd.Parameters["@iSuccess"].Direction = ParameterDirection.Output;
                        oCmd.Parameters.Add("@iAppPackId", SqlDbType.Int).Value = iAppPackId;
                        oCmd.Parameters["@iAppPackId"].Direction = ParameterDirection.Output;
                        if (oSqlConn.State != ConnectionState.Open)
                        {
                            oSqlConn.Close();
                            oSqlConn.Open();
                        }
                        oCmd.ExecuteNonQuery();
                        iSuccess = (int)oCmd.Parameters["@iSuccess"].Value;
                        if (!string.IsNullOrEmpty(oCmd.Parameters["@iAppPackId"].Value.ToString()) || oCmd.Parameters["@iAppPackId"].Value.ToString().Length > 0)
                            iAppPackId = (int)oCmd.Parameters["@iAppPackId"].Value;

                        SaveAppPackageTemp(arrbContent, iAppPackId);

                        if (iSuccess == 0)
                            MessageBox.Show("Duplicate App Package Name. Please choose other file or rename it.");
                        else
                            CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, "", "Applicaiton Package saved successfully",
                                string.Format("Save application {0} from {1}", sAppPackageName, sAppPackagePath));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    Init();
                }
                //else
                //    MessageBox.Show("Please choose file.");
            }
            //    }
            //    else MessageBox.Show("The DatabaseID " + sDatabaseID + " Already Have Software Package");
        }

        protected string sAppPackageDesc
        {
            get { return string.IsNullOrEmpty(txtAppPackageDesc.Text) ? "" : txtAppPackageDesc.Text; }
        }

        protected bool IsSavePackage(string _sAppPackageName)
        {
            return MessageBox.Show(string.Format("Save '{0}' to Database?", _sAppPackageName), "", MessageBoxButtons.YesNo) == DialogResult.Yes ?
                true : false;
        }

        protected byte[] arrbFileContent()
        {
            string sFilename = string.Format(@"{0}\{1}", sAppPackagePath, sAppPackageFilename);
            //string sFilezip = string.Format(@"{0}\{1}", Application.StartupPath, sAppPackageName);
            //File.Copy(sFilename, sFilezip, true);
            byte[] arrbContent = new byte[1024];

            FileStream fsRead = new FileStream(sFilename, FileMode.Open);
            arrbContent = new byte[fsRead.Length];
            fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
            fsRead.Close();

            //if (IsNetCompressSuccess(ref sFilezip))
            //{
            //    FileStream fsRead = new FileStream(sFilezip, FileMode.Open);
            //    arrbContent = new byte[fsRead.Length];
            //    fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
            //    fsRead.Close();
            //}

            //File.Delete(sFilezip);
            //File.Delete(sFilezip + ".gz");
            return arrbContent;
        }

        protected void SaveAppPackageTemp(byte[] _arrbContent, int iAppPackId)
        {
            string sContent = CommonLib.sByteArrayToHexString(_arrbContent).Replace(" ", "");

            DataTable dtAppTemp = new DataTable();
            dtAppTemp = dtGetAppPackageTemp(sContent, iAppPackId);
            SqlBulkCopy sqlBulk = new SqlBulkCopy(oSqlConn);
            sqlBulk.DestinationTableName = string.Format("tbAppPackageEDCListTemp");
            SqlBulkCopyColumnMapping mapId = new SqlBulkCopyColumnMapping("Id", "Id");
            SqlBulkCopyColumnMapping mapAppPackId = new SqlBulkCopyColumnMapping("AppPackId", "AppPackId");
            SqlBulkCopyColumnMapping mapAppPackageName = new SqlBulkCopyColumnMapping("AppPackageName", "AppPackageName");
            SqlBulkCopyColumnMapping mapAppPackageContent = new SqlBulkCopyColumnMapping("AppPackageContent", "AppPackageContent");
            sqlBulk.ColumnMappings.Add(mapId);
            sqlBulk.ColumnMappings.Add(mapAppPackId);
            sqlBulk.ColumnMappings.Add(mapAppPackageName);
            sqlBulk.ColumnMappings.Add(mapAppPackageContent);
            sqlBulk.WriteToServer(dtAppTemp);
        }

        protected DataTable dtGetAppPackageTemp(string _sContent, int iAppPackId)
        {
            DataSet dsTemp = new DataSet();
            DataTable dtTemp = new DataTable();
            string sQuery = string.Format("{0}",
                string.Format("SELECT Id, AppPackageName, AppPackageContent, AppPackId " +
                    "FROM tbAppPackageEDCListTemp " +
                    "WHERE AppPackageName='{0}' AND AppPackId={1}", sAppPackageName, iAppPackId));
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
                (new SqlDataAdapter(oCmd)).Fill(dsTemp);
            dtTemp = dsTemp.Tables[0].Copy();
            int iOffset = 0;
            int iLength = 800 * 2;
            int iId = 0;
            while (iOffset < _sContent.Length)
            {
                DataRow row = dtTemp.NewRow();
                row["Id"] = iId++;
                row["AppPackId"] = iAppPackId;
                row["AppPackageName"] = sAppPackageName;
                row["AppPackageContent"] = _sContent.Substring(iOffset,
                    iOffset + iLength < _sContent.Length ? iLength : _sContent.Length - iOffset);
                dtTemp.Rows.Add(row);
                iOffset += iLength;
            }
            return dtTemp;
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsRowSelected()) RemoveAppPackage(sSelectedAppPackage, iSelectedAppPackageId);
        }

        protected string sSelectedAppPackage
        {
            get
            {
                int iRow = dgAppPackage.SelectedRows[0].Index;
                return dgAppPackage["Software Name", iRow].Value.ToString();
            }
        }

        protected int iSelectedAppPackageId
        {
            get
            {
                int iRow = dgAppPackage.SelectedRows[0].Index;
                return int.Parse(dgAppPackage["AppPackID", iRow].Value.ToString());
            }
        }

        protected bool IsRowSelected()
        {
            return dgAppPackage.SelectedRows.Count > 0 ? true : false;
        }

        protected void LoadSelectedAppPackage()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageBrowseDetail, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = sSelectedAppPackage;

            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);
            //txtBuildNumber.Text = dtTemp.Rows[0]["BuildNumber"].ToString();
            txtPackageName.Text = dtTemp.Rows[0]["AppPackageName"].ToString();
            txtAppPackageFilename.Text = dtTemp.Rows[0]["AppPackageFilename"].ToString();
            txtAppPackageDesc.Text = dtTemp.Rows[0]["AppPackageDesc"].ToString();
            txtBuiltNumber.Text = dtTemp.Rows[0]["BuildNumber"].ToString();
            //CheckSelectedEdcType(dtTemp.Rows[0]["EdcTypeId"].ToString());
            sAppPackageNameOld = dtTemp.Rows[0]["AppPackageName"].ToString();
            //cmbDatabase.SelectedValue = dtTemp.Rows[0]["DatabaseId"].ToString();

            oCmd.Dispose();
            oRead.Close();
            oRead.Dispose();
            dtTemp.Dispose();
        }

        protected void RemoveAppPackage(string _sAppPackageName, int iAppPackId)
        {
            if (isAppIsUse(_sAppPackageName) == false)
            {
                if (IsRemovePackage(_sAppPackageName))
                {
                    try
                    {
                        SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageDelete, oSqlConn);
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.Parameters.Add("@sAppPackageName", SqlDbType.VarChar).Value = _sAppPackageName;
                        oCmd.Parameters.Add("@iAppPackId", SqlDbType.Int).Value = iAppPackId;

                        if (oSqlConn.State != ConnectionState.Open)
                        {
                            oSqlConn.Close();
                            oSqlConn.Open();
                        }
                        oCmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    Init();
                }
            }
            else
            {
                MessageBox.Show("Software in use.");
            }
        }

        protected bool IsRemovePackage(string _sAppPackageName)
        {
            return MessageBox.Show(string.Format("Remove '{0}' from Database?", _sAppPackageName), "", MessageBoxButtons.YesNo) == DialogResult.Yes ?
                true : false;
        }

        private bool isAppIsUse(string _sAppPackageName)
        {
            bool bResult = true;
            try
            {
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPSoftwareGroupPackageBrowse, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE SoftwareName = '{0}'", _sAppPackageName);

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                SqlDataReader oRead = oCmd.ExecuteReader();
                if (oRead.HasRows)
                    bResult = true;
                else
                    bResult = false;
                oRead.Close();
                oRead.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return bResult;
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isEdit = false;
            if (IsRowSelected())
                SetDisplayView(false);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (gbSetting.Enabled) Init();
            else this.Close();
        }
    }
}
