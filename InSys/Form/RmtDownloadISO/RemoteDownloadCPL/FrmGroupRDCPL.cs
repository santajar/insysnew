﻿using InSysClass;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmGroupRDCPL : Form
    {
        SqlConnection oConn;
        SqlConnection oSqlConnAuditTrail;
        string sUID;
        string sType;
        public FrmGroupRDCPL()
        {
            InitializeComponent();
        }

        private void FrmGroupRDCPL_Load(object sender, EventArgs e)
        {
            if (sType == "Software")
            {
                btnAdd.Visible = true;
                btnEdit.Text = "Edit";
                this.Text = "Form Group Package";

            }
            else
            {
                btnAdd.Visible = false;
                btnEdit.Text = "Edit TID";
                this.Text = "Form TerminalID Package";

            }

            doFillDataGridView();
        }

        protected void doFillDataGridView()
        {
            dgGroup.DataSource = null;
            dgGroup.DataSource = dtGroupList().DefaultView.ToTable(true, "GroupName");
            dgGroup.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        protected DataTable dtGroupList()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oComm = new SqlCommand(CommonSP.sSPSoftwareGroupPackageBrowse, oConn))
                {
                    oComm.CommandType = CommandType.StoredProcedure;
                    oComm.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "";
                    SqlDataAdapter oAdapt = new SqlDataAdapter(oComm);
                    oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dtTable;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
          
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

        }
    }
}
