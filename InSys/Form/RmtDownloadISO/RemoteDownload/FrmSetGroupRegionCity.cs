﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class frmSetGroupRegionCity : Form
    {
        protected SqlConnection oSqlConn;
        protected string sTerminalID;
        public frmSetGroupRegionCity(SqlConnection _oSqlConn, string _sTerminalID)
        {
            oSqlConn = _oSqlConn;
            sTerminalID = _sTerminalID;
            InitializeComponent();
        }

        private void frmSetGroupRegionCity_Load(object sender, EventArgs e)
        {
            InitComboBoxCity();
            InitComboBoxGroup();
            InitComboBoxRegion();
            InitComboBoxSoftwareName();
        }
        private void InitComboBoxCity()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPBrowseCity, "");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbCity.DataSource = dtSoftware;
                cmbCity.DisplayMember = "City";
                cmbCity.ValueMember = "IdCityRD";
                cmbCity.SelectedIndex = -1;
            }
        }

        private void InitComboBoxRegion()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPBrowseRegion, "");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbRegion.DataSource = dtSoftware;
                cmbRegion.DisplayMember = "Region";
                cmbRegion.ValueMember = "IdRegionRD";
                cmbRegion.SelectedIndex = -1;
            }
        }

        private void InitComboBoxSoftwareName()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPAppPackageBrowse, "");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbSoftwareName.DataSource = dtSoftware;
                cmbSoftwareName.DisplayMember = "Software Name";
                cmbSoftwareName.ValueMember = "AppPackID";
                cmbSoftwareName.SelectedIndex = -1;
            }
        }

        private void InitComboBoxGroup()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPBrowseGroup, "");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbGroup.DataSource = dtSoftware;
                cmbGroup.DisplayMember = "Group";
                cmbGroup.ValueMember = "IdGroupRD";
                cmbGroup.SelectedIndex = -1;
            }
        }

        private DataTable dtGetData(string sSP, string sCond)
        {
            SqlCommand oCmd = new SqlCommand(sSP, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCond;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);
            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();
            return dtTemp;
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            if (cmbCity.SelectedIndex > -1 || cmbGroup.SelectedIndex > -1 || cmbRegion.SelectedIndex > -1)
            {
                string sCity = "", sGroup = "", sRegion = "";

                if (cmbCity.SelectedIndex > -1)
                    sCity = cmbCity.SelectedValue.ToString();
                if (cmbGroup.SelectedIndex > -1)
                    sGroup = cmbGroup.SelectedValue.ToString();
                if (cmbRegion.SelectedIndex > -1)
                    sRegion = cmbRegion.SelectedValue.ToString();
                if (cmbSoftwareName.SelectedIndex > -1)
                {
                    SqlCommand oCmd = new SqlCommand("INSERT INTO tbListTIDRemoteDownload (TerminalID,IdRegionRD,IdGroupRD,IdCityRD,AppPackID) VALUES ('" + sTerminalID + "','" + sCity + "','" + sGroup + "','" + sRegion + "','"+cmbSoftwareName.SelectedValue.ToString()+"')", oSqlConn);
                    //oCmd.CommandType = CommandType.StoredProcedure;
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    oCmd.ExecuteNonQuery();
                    this.Close();
                    this.Dispose();
                }
                else
                    MessageBox.Show("Please select Software.");
            }
            else
                MessageBox.Show("Please select Group, region or City.");
        }
    }
}
