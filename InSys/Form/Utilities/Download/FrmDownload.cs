using System;
using System.Data;
using System.Windows.Forms;
using System.IO.Ports;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using InSysClass;

namespace InSys
{
    public partial class FrmSerialDownload : Form
    {
        protected SqlConnection oSqlConn;

        enum DownloadType
        {
            DATA = 1,
            AM,
            AM_APP,
            APP
        }

        DownloadType oDownloadType;

        protected string sSoftwareFolder = Application.StartupPath + "\\SOFTWARE";
        protected string sFilenameAM;
        protected string sFilenameAPP;
        protected string sBatchFileAM;
        protected string sBatchFileApp;
        protected string sConfigFile;

        protected int iMaxByte = 300;

        const string SBIBLOAD = "SBIBLOAD";
        const string HDRTOOL = "HDRTOOL";

        public FrmSerialDownload(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        private void FrmDownload_Load(object sender, EventArgs e)
        {
            InitPortNames();
            InitComboBoxDatabase();
        }

        private void btnBrowseApp_Click(object sender, EventArgs e)
        {
            txtApp.Text = sFilenameAMorAPP(false);
        }

        private void btnBrowseAM_Click(object sender, EventArgs e)
        {
            txtAM.Text = sFilenameAMorAPP(true);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            switch (oDownloadType)
            {
                case DownloadType.DATA :
                    DownloadData();
                    break;
                case DownloadType.AM :
                    DownloadAM();
                    break;
                case DownloadType.APP :
                    DownloadAPP();
                    break;
                case DownloadType.AM_APP :
                    DownloadAM();
                    DownloadAPP();
                    break;
            }
        }

        private void cmbFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbDbProfile.Enabled = false;
            cmbProfile.Enabled = false;
            chkSplit.Enabled = false;

            if (cmbFile.Text.ToUpper() == "DATA" || cmbFile.Text.ToUpper() == "APPLICATION")
            {
                btnBrowseAM.Enabled = false;
                btnBrowseApp.Enabled = true;
                if (cmbFile.Text.ToUpper() == "DATA")
                {
                    cmbDbProfile.Enabled = true;
                    cmbProfile.Enabled = true;
                    chkSplit.Enabled = true;
                    oDownloadType = DownloadType.DATA;
                }
                else
                    oDownloadType = DownloadType.APP;
            }
            else if (cmbFile.Text.ToUpper() == "AM")
            {
                btnBrowseAM.Enabled = true;
                btnBrowseApp.Enabled = false;
                oDownloadType = DownloadType.AM;
            }
            else if (cmbFile.Text.ToUpper() == "AM + APPLICATION")
            {
                btnBrowseAM.Enabled = true;
                btnBrowseApp.Enabled = true;
                oDownloadType = DownloadType.AM_APP;
            }
        }

        private void cmbDbProfile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbProfile.Text))
                InitComboBoxProfile();
        }

        #region "Function"
        protected string[] arrsGetPortNames()
        {
            return SerialPort.GetPortNames();
        }

        protected void InitPortNames()
        {
            foreach (string sPortName in arrsGetPortNames())
            {
                cmbPort.Items.Add(sPortName);
            }
        }

        protected void InitComboBoxDatabase()
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;

            DataTable oTable = new DataTable();
            new SqlDataAdapter(oSqlCmd).Fill(oTable);

            cmbDbProfile.DisplayMember = "DatabaseName";
            cmbDbProfile.ValueMember = "DatabaseId";
            cmbDbProfile.DataSource = oTable;
            cmbDbProfile.SelectedIndex = -1;
        }

        protected void InitComboBoxProfile()
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sDbConditions();

            DataTable oTable = new DataTable();
            new SqlDataAdapter(oSqlCmd).Fill(oTable);

            cmbProfile.DataSource = null;
            cmbProfile.Items.Clear();
            cmbProfile.DisplayMember = "TerminalId";
            cmbProfile.ValueMember = "TerminalId";
            cmbProfile.DataSource = oTable;
            cmbProfile.SelectedIndex = -1;
        }

        protected string sDbConditions()
        {
            return "WHERE DATABASEID = " + cmbDbProfile.SelectedValue;
        }

        protected string sFilenameAMorAPP(bool IsAM)
        {
            ofdBrowse.InitialDirectory = sSoftwareFolder;
            if (IsAM)
            {
                ofdBrowse.Title = "Open AM File";
                ofdBrowse.Filter = "AM files (AM*BC.BAT)|AM*BC.BAT";
            }
            else
            {
                ofdBrowse.Title = "Open APP File";
                ofdBrowse.Filter = "5100V3 Application Files|5*BC.BAT|DECT Application Files|INTG*BC.BAT";
            }

            if (ofdBrowse.ShowDialog() != DialogResult.Cancel)
            {
                if (IsAM)
                {
                    sBatchFileAM = ofdBrowse.FileName;
                    sFilenameAM = sBatchFileAM.ToUpper().Replace(".BAT", ".001");
                }
                else
                {
                    sBatchFileApp = ofdBrowse.FileName;
                    sFilenameAPP = sBatchFileApp.ToUpper().Replace(".BAT", ".001");
                }
            }
            if (!string.IsNullOrEmpty(ofdBrowse.FileName))
                return ofdBrowse.FileName.Substring(sSoftwareFolder.Length + 1, ofdBrowse.FileName.Length - sSoftwareFolder.Length + 1 - 8);
            else
                return " ";
           
        }

        protected string sPortSpeed { get { return cmbSpeed.Text; } }
        protected string sPortName { get { return cmbPort.Text; } }
        protected string sTerminal { get { return cmbTerminal.Text; } }
        protected string sTerminalId { get { return cmbProfile.Text; } }
        protected bool IsSplitData { get { return chkSplit.Checked; } }
        protected string sNameAM { get { return txtAM.Text; } }
        protected string sNameApp { get { return txtApp.Text; } }

        protected bool IsFilenameAPPExist()
        {
            return (!string.IsNullOrEmpty(sFilenameAPP)) ? true : false;
        }

        protected bool IsFilenameAMExist()
        {
            return (!string.IsNullOrEmpty(sFilenameAM)) ? true : false;
        }

        protected bool IsTerminalIdSelected()
        {
            return (!string.IsNullOrEmpty(sTerminalId)) ? true : false;
        }

        protected void CompileData(string sFilenameData)
        {
            string sFilenameINI = sSoftwareFolder + "\\app_" + sNameApp + ".INI";
            RunShellCommand("HDRTOOL",
                "\"" + sFilenameData + "\" \"" + sFilenameINI + "\"");
        }

        protected void DownloadData()
        {
            if (IsFilenameAPPExist())
                if (IsTerminalIdSelected())
                {
                    if (MessageBox.Show(string.Format("Download Data '{0}'?", sTerminalId),
                        "Download", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DataTable dtTerminalID = dtGetProfileContent();
                        string sFilenameTerminalID = sSoftwareFolder + "\\" + sTerminalId + ".txt";
                        string sContentTerminalID = dtTerminalID.Rows[0]["Content"].ToString();

                        if (IsSplitData)
                        {
                            int iTotalSplitFile = iSplitFileTerminalID(sContentTerminalID);
                            for (int i = 1; i <= iTotalSplitFile; i++)
                            {
                                string sFilenameSplit = sSoftwareFolder + "\\"
                                    + sTerminalId + "_" + i.ToString() + ".txt";

                                CompileData(sFilenameSplit);
                                File.Delete(sFilenameSplit);

                                string sAppINI = sGetAppNameINIFile() + "B_.001";
                                UpdateBatchFileData(sSoftwareFolder + "\\DATA.BAT", sAppINI);
                                RunShellCommand(sSoftwareFolder + "\\DATA.BAT", "");
                            }
                        }
                        else
                        {
                            File.Delete(sFilenameTerminalID);
                            CommonLib.Write2File(sFilenameTerminalID, dtTerminalID.Columns[1].ToString(), false);
                            
                            CompileData(sFilenameTerminalID);

                            string sAppINI = sGetAppNameINIFile() + "B_.001";
                            UpdateBatchFileData(sSoftwareFolder + "\\DATA.BAT", sAppINI);
                            RunShellCommand(sSoftwareFolder + "\\DATA.BAT", "");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Error Terminal Id.\nPlease select Terminal Id");
                    cmbProfile.Focus();
                }
            else
            {
                MessageBox.Show("Error Application Name.\nPlease select Application Name");
                btnBrowseApp.Focus();
            }
        }

        protected void DownloadAM()
        {
            if (IsFilenameAMExist())
            {
                if (MessageBox.Show(string.Format("Download AM, '{0}'?", sFilenameAM), 
                    "Download", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    UpdateBatchFile(sBatchFileAM);
                    RunShellCommand(sBatchFileAM, "");
                }
            }
            else
            {
                MessageBox.Show("Error AM Name.\nPlease select AM Name");
                btnBrowseAM.Focus();
            }
        }

        protected void DownloadAPP()
        {
            if (IsFilenameAPPExist())
            {
                if (MessageBox.Show(string.Format("Download Application, '{0}'?", sFilenameAPP), 
                    "Download", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    UpdateBatchFile(sBatchFileApp);
                    RunShellCommand(sBatchFileApp, "");

                    string sEMVBatchFile = null;
                    if (sBatchFileApp.IndexOf("5100V3") > 0)
                        sEMVBatchFile = "EMV_V3BC.BAT";
                    else
                        sEMVBatchFile = "EMVBC.BAT";
                    UpdateBatchFile(sEMVBatchFile);

                    RunShellCommand(sEMVBatchFile, "");                   
                }
            }
            else
            {
                MessageBox.Show("Error Application Name.\nPlease select Application Name");
                btnBrowseApp.Focus();
            }
        }

        protected void RunShellCommand(string sFilename, string sParameter)
        {
            ProcessStartInfo procinfoBatchFile = new ProcessStartInfo(sFilename);
            Process procBatchFile = new Process();
            procBatchFile.StartInfo = procinfoBatchFile;
            if(!string.IsNullOrEmpty(sParameter)) procBatchFile.StartInfo.Arguments = sParameter;
            procBatchFile.Start();
            procBatchFile.WaitForExit();
        }

        protected void UpdateBatchFile(string sFilename)
        {
            StreamReader sr = new StreamReader(sSoftwareFolder + "\\" + sFilename);
            string sContentBatchFile = sr.ReadToEnd();
            sr.Close();

            string[] arrsContent = sContentBatchFile.Split(' ');
            string sContentNewBatchFile = null;
            foreach (string sTemp in arrsContent)
            {
                if (sTemp.Substring(0, 2) == "-T")
                    sContentNewBatchFile += "-T" + sTerminal.Substring(0, 1) + " ";
                else if (sTemp.Substring(0, 2) == "-S")
                    sContentNewBatchFile += "-S" + sPortSpeed + " ";
                else if (sTemp.Substring(0, 2) == "-P")
                    sContentNewBatchFile += "-P" + sPortName.Substring(3) + " ";
                else
                    sContentNewBatchFile += sTemp.ToUpper() + " ";
            }

            File.Delete(sFilename);
            StreamWriter sw = new StreamWriter(sSoftwareFolder + "\\" + sFilename);
            sw.WriteLine(sContentNewBatchFile);
            sw.Close();
        }

        protected void UpdateBatchFileData(string sFilename, string sAppNameINI)
        {
            string sContentBatchFile = string.Format("{0} -T{1} -S{2} -P{3} {4}",
                SBIBLOAD,
                sTerminal.Substring(0, 1),
                sPortSpeed,
                sPortName.Substring(3),
                sAppNameINI);
            File.Delete(sFilename);
            CommonLib.Write2File(sFilename, sContentBatchFile, false);
        }

        protected DataTable dtGetProfileContent()
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPExportProfileToText, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.CommandTimeout = 0;
            oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sCondition();

            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            new SqlDataAdapter(oCmd).Fill(dtTemp);

            oCmd.Dispose();
            return dtTemp;
        }

        protected string sCondition()
        {
            return "WHERE TerminalId='" + sTerminalId + "'";
        }

        protected int iSplitFileTerminalID(string sContent)
        {
            int iCounter = 0;
            int iOffset = 0;
            string sContentPart = "";
            string sFilenamePart = sSoftwareFolder + "\\" + sTerminalId + "_";
            while (iOffset < sContent.Length)
            {
                string sTag = sContent.Substring(iOffset, 4);
                iOffset += 4;
                
                int iLen;
                if (sTag != "AE37" && sTag != "AE38")
                {
                    iLen = int.Parse(sContent.Substring(iOffset, 2));
                    iOffset += 2;
                }
                else
                {
                    iLen = int.Parse(sContent.Substring(iOffset, 3));
                    iOffset += 3;
                }
                
                string sValue = sContent.Substring(iOffset, iLen);
                iOffset += iLen;
                
                if (sContentPart.Length + CommonLib.sGenerateTLV(sTag, sValue, 2).Length > iMaxByte
                    || iOffset == sContent.Length)
                {
                    iCounter += 1;
                    CommonLib.Write2File(sFilenamePart + iCounter.ToString() + ".txt", sContentPart, false);
                    sContentPart = null;
                    sContentPart += CommonLib.sGenerateTLV(sTag, sValue, iLen);
                }
                else
                    sContentPart += CommonLib.sGenerateTLV(sTag, sValue, iLen);
            }
            return iCounter;
        }

        protected string sGetAppNameINIFile()
        {
            string sFilename = sSoftwareFolder + "\\app_" + sNameApp + ".INI";
            string sTemp = null;
            StreamReader sr = new StreamReader(sFilename);
            string sContentINI = sr.ReadToEnd();
            sr.Close();

            int iOffset = sContentINI.IndexOf("Name[char 6]=");
            if (iOffset != 0)
            {
                iOffset += "Name[char 6]=".Length;
                sTemp = sContentINI.Substring(iOffset, 6);
            }
            return sTemp;
        }
        #endregion

        private void btncheck_Click(object sender, EventArgs e)
        {
            MessageBox.Show(ofdBrowse.FileName);  
        }
    }
}