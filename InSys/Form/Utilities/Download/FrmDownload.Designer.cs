namespace InSys
{
    partial class FrmSerialDownload
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSerialDownload));
            this.lblAM = new System.Windows.Forms.Label();
            this.txtApp = new System.Windows.Forms.TextBox();
            this.txtAM = new System.Windows.Forms.TextBox();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.cmbDbProfile = new System.Windows.Forms.ComboBox();
            this.chkSplit = new System.Windows.Forms.CheckBox();
            this.cmbProfile = new System.Windows.Forms.ComboBox();
            this.lblApplication = new System.Windows.Forms.Label();
            this.lblTID = new System.Windows.Forms.Label();
            this.btnBrowseAM = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDownload = new System.Windows.Forms.Button();
            this.cmbTerminal = new System.Windows.Forms.ComboBox();
            this.cmbSpeed = new System.Windows.Forms.ComboBox();
            this.cmbPort = new System.Windows.Forms.ComboBox();
            this.ofdBrowse = new System.Windows.Forms.OpenFileDialog();
            this.cmbFile = new System.Windows.Forms.ComboBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.lblFile = new System.Windows.Forms.Label();
            this.gbDownload = new System.Windows.Forms.GroupBox();
            this.btnBrowseApp = new System.Windows.Forms.Button();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btncheck = new System.Windows.Forms.Button();
            this.gbDownload.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAM
            // 
            this.lblAM.Location = new System.Drawing.Point(15, 106);
            this.lblAM.Name = "lblAM";
            this.lblAM.Size = new System.Drawing.Size(80, 24);
            this.lblAM.TabIndex = 66;
            this.lblAM.Text = "AM               :";
            // 
            // txtApp
            // 
            this.txtApp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtApp.Location = new System.Drawing.Point(101, 137);
            this.txtApp.Name = "txtApp";
            this.txtApp.ReadOnly = true;
            this.txtApp.Size = new System.Drawing.Size(128, 20);
            this.txtApp.TabIndex = 6;
            // 
            // txtAM
            // 
            this.txtAM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAM.Location = new System.Drawing.Point(101, 106);
            this.txtAM.Name = "txtAM";
            this.txtAM.ReadOnly = true;
            this.txtAM.Size = new System.Drawing.Size(128, 20);
            this.txtAM.TabIndex = 4;
            // 
            // lblDatabase
            // 
            this.lblDatabase.Location = new System.Drawing.Point(13, 44);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(80, 24);
            this.lblDatabase.TabIndex = 63;
            this.lblDatabase.Text = "Database      :";
            // 
            // cmbDbProfile
            // 
            this.cmbDbProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDbProfile.Enabled = false;
            this.cmbDbProfile.Location = new System.Drawing.Point(101, 44);
            this.cmbDbProfile.Name = "cmbDbProfile";
            this.cmbDbProfile.Size = new System.Drawing.Size(128, 21);
            this.cmbDbProfile.TabIndex = 2;
            this.cmbDbProfile.SelectedIndexChanged += new System.EventHandler(this.cmbDbProfile_SelectedIndexChanged);
            // 
            // chkSplit
            // 
            this.chkSplit.Enabled = false;
            this.chkSplit.Location = new System.Drawing.Point(101, 254);
            this.chkSplit.Name = "chkSplit";
            this.chkSplit.Size = new System.Drawing.Size(104, 24);
            this.chkSplit.TabIndex = 11;
            this.chkSplit.Text = "Split Download";
            // 
            // cmbProfile
            // 
            this.cmbProfile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProfile.Enabled = false;
            this.cmbProfile.Location = new System.Drawing.Point(101, 76);
            this.cmbProfile.Name = "cmbProfile";
            this.cmbProfile.Size = new System.Drawing.Size(128, 21);
            this.cmbProfile.TabIndex = 3;
            // 
            // lblApplication
            // 
            this.lblApplication.Location = new System.Drawing.Point(15, 137);
            this.lblApplication.Name = "lblApplication";
            this.lblApplication.Size = new System.Drawing.Size(80, 24);
            this.lblApplication.TabIndex = 60;
            this.lblApplication.Text = "Application   :";
            // 
            // lblTID
            // 
            this.lblTID.Location = new System.Drawing.Point(13, 76);
            this.lblTID.Name = "lblTID";
            this.lblTID.Size = new System.Drawing.Size(80, 24);
            this.lblTID.TabIndex = 59;
            this.lblTID.Text = "Terminal ID   :";
            // 
            // btnBrowseAM
            // 
            this.btnBrowseAM.Enabled = false;
            this.btnBrowseAM.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowseAM.Image")));
            this.btnBrowseAM.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnBrowseAM.Location = new System.Drawing.Point(235, 103);
            this.btnBrowseAM.Name = "btnBrowseAM";
            this.btnBrowseAM.Size = new System.Drawing.Size(88, 24);
            this.btnBrowseAM.TabIndex = 5;
            this.btnBrowseAM.Text = "       Browse";
            this.btnBrowseAM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBrowseAM.Click += new System.EventHandler(this.btnBrowseAM_Click);
            // 
            // btnExit
            // 
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnExit.Location = new System.Drawing.Point(100, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 24);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "       Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDownload
            // 
            this.btnDownload.Image = ((System.Drawing.Image)(resources.GetObject("btnDownload.Image")));
            this.btnDownload.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnDownload.Location = new System.Drawing.Point(6, 12);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(88, 24);
            this.btnDownload.TabIndex = 2;
            this.btnDownload.Text = "       Download";
            this.btnDownload.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // cmbTerminal
            // 
            this.cmbTerminal.DisplayMember = "S";
            this.cmbTerminal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTerminal.Items.AddRange(new object[] {
            "Serial",
            "Biberon"});
            this.cmbTerminal.Location = new System.Drawing.Point(101, 226);
            this.cmbTerminal.Name = "cmbTerminal";
            this.cmbTerminal.Size = new System.Drawing.Size(128, 21);
            this.cmbTerminal.TabIndex = 10;
            // 
            // cmbSpeed
            // 
            this.cmbSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSpeed.Items.AddRange(new object[] {
            "1200",
            "2400",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.cmbSpeed.Location = new System.Drawing.Point(101, 196);
            this.cmbSpeed.Name = "cmbSpeed";
            this.cmbSpeed.Size = new System.Drawing.Size(128, 21);
            this.cmbSpeed.TabIndex = 9;
            // 
            // cmbPort
            // 
            this.cmbPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPort.Location = new System.Drawing.Point(101, 166);
            this.cmbPort.Name = "cmbPort";
            this.cmbPort.Size = new System.Drawing.Size(128, 21);
            this.cmbPort.TabIndex = 8;
            // 
            // cmbFile
            // 
            this.cmbFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFile.Items.AddRange(new object[] {
            "Data",
            "AM",
            "AM + Application",
            "Application"});
            this.cmbFile.Location = new System.Drawing.Point(101, 13);
            this.cmbFile.Name = "cmbFile";
            this.cmbFile.Size = new System.Drawing.Size(128, 21);
            this.cmbFile.TabIndex = 1;
            this.cmbFile.SelectedIndexChanged += new System.EventHandler(this.cmbFile_SelectedIndexChanged);
            // 
            // lblTerminal
            // 
            this.lblTerminal.Location = new System.Drawing.Point(13, 226);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(80, 24);
            this.lblTerminal.TabIndex = 52;
            this.lblTerminal.Text = "Terminal       :";
            // 
            // lblSpeed
            // 
            this.lblSpeed.Location = new System.Drawing.Point(13, 196);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(80, 24);
            this.lblSpeed.TabIndex = 50;
            this.lblSpeed.Text = "Speed          :";
            // 
            // lblPort
            // 
            this.lblPort.Location = new System.Drawing.Point(13, 166);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(80, 24);
            this.lblPort.TabIndex = 48;
            this.lblPort.Text = "Port              :";
            // 
            // lblFile
            // 
            this.lblFile.Location = new System.Drawing.Point(13, 13);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(80, 24);
            this.lblFile.TabIndex = 47;
            this.lblFile.Text = "File                :";
            // 
            // gbDownload
            // 
            this.gbDownload.Controls.Add(this.btncheck);
            this.gbDownload.Controls.Add(this.btnBrowseApp);
            this.gbDownload.Controls.Add(this.btnBrowseAM);
            this.gbDownload.Controls.Add(this.lblFile);
            this.gbDownload.Controls.Add(this.lblAM);
            this.gbDownload.Controls.Add(this.lblPort);
            this.gbDownload.Controls.Add(this.txtApp);
            this.gbDownload.Controls.Add(this.lblSpeed);
            this.gbDownload.Controls.Add(this.txtAM);
            this.gbDownload.Controls.Add(this.lblTerminal);
            this.gbDownload.Controls.Add(this.lblDatabase);
            this.gbDownload.Controls.Add(this.cmbFile);
            this.gbDownload.Controls.Add(this.cmbDbProfile);
            this.gbDownload.Controls.Add(this.cmbPort);
            this.gbDownload.Controls.Add(this.chkSplit);
            this.gbDownload.Controls.Add(this.cmbSpeed);
            this.gbDownload.Controls.Add(this.cmbProfile);
            this.gbDownload.Controls.Add(this.cmbTerminal);
            this.gbDownload.Controls.Add(this.lblApplication);
            this.gbDownload.Controls.Add(this.lblTID);
            this.gbDownload.Location = new System.Drawing.Point(12, 9);
            this.gbDownload.Name = "gbDownload";
            this.gbDownload.Size = new System.Drawing.Size(338, 281);
            this.gbDownload.TabIndex = 0;
            this.gbDownload.TabStop = false;
            // 
            // btnBrowseApp
            // 
            this.btnBrowseApp.Enabled = false;
            this.btnBrowseApp.Image = ((System.Drawing.Image)(resources.GetObject("btnBrowseApp.Image")));
            this.btnBrowseApp.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnBrowseApp.Location = new System.Drawing.Point(235, 135);
            this.btnBrowseApp.Name = "btnBrowseApp";
            this.btnBrowseApp.Size = new System.Drawing.Size(88, 24);
            this.btnBrowseApp.TabIndex = 7;
            this.btnBrowseApp.Text = "       Browse";
            this.btnBrowseApp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBrowseApp.Click += new System.EventHandler(this.btnBrowseApp_Click);
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnDownload);
            this.gbButton.Controls.Add(this.btnExit);
            this.gbButton.Location = new System.Drawing.Point(12, 296);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(338, 42);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btncheck
            // 
            this.btncheck.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btncheck.Location = new System.Drawing.Point(235, 19);
            this.btncheck.Name = "btncheck";
            this.btncheck.Size = new System.Drawing.Size(75, 23);
            this.btncheck.TabIndex = 67;
            this.btncheck.Text = "check";
            this.btncheck.UseVisualStyleBackColor = true;
            this.btncheck.Click += new System.EventHandler(this.btncheck_Click);
            // 
            // FrmSerialDownload
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 352);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbDownload);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSerialDownload";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "MagIC Series Download-Initialize";
            this.Load += new System.EventHandler(this.FrmDownload_Load);
            this.gbDownload.ResumeLayout(false);
            this.gbDownload.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblAM;
        internal System.Windows.Forms.TextBox txtApp;
        internal System.Windows.Forms.TextBox txtAM;
        internal System.Windows.Forms.Label lblDatabase;
        internal System.Windows.Forms.ComboBox cmbDbProfile;
        internal System.Windows.Forms.CheckBox chkSplit;
        internal System.Windows.Forms.ComboBox cmbProfile;
        internal System.Windows.Forms.Label lblApplication;
        internal System.Windows.Forms.Label lblTID;
        internal System.Windows.Forms.Button btnBrowseAM;
        internal System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.Button btnDownload;
        internal System.Windows.Forms.ComboBox cmbTerminal;
        internal System.Windows.Forms.ComboBox cmbSpeed;
        internal System.Windows.Forms.ComboBox cmbPort;
        internal System.Windows.Forms.OpenFileDialog ofdBrowse;
        internal System.Windows.Forms.ComboBox cmbFile;
        internal System.Windows.Forms.Label lblTerminal;
        internal System.Windows.Forms.Label lblSpeed;
        internal System.Windows.Forms.Label lblPort;
        internal System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.GroupBox gbDownload;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnBrowseApp;
        private System.Windows.Forms.Button btncheck;
    }
}