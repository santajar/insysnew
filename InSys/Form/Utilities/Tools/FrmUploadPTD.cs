﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmUploadPTD : Form
    {
        SqlConnection oSqlConn;
        string sUserID;
        SqlConnection oSqlConnAuditTrail;

        public FrmUploadPTD(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConn;
            sUserID = _sUserID;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            groupBoxButton.Enabled = false;
            progressBarQueryCrp.Style = ProgressBarStyle.Marquee;
            this.Cursor = Cursors.AppStarting;
            backgroundWorkerCrp.RunWorkerAsync();
        }

        private void backgroundWorkerCrp_DoWork(object sender, DoWorkEventArgs e)
        {
            ExtractTidMid();
        }

        private void backgroundWorkerCrp_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBarQueryCrp.Style = ProgressBarStyle.Blocks;
            groupBoxButton.Enabled = true;
            this.Cursor = Cursors.Default;
            MessageBox.Show("DONE");
        }

        private void backgroundWorkerCrp_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        // generate TID-MID based on TID Init list
        private void ExtractTidMid()
        {
            backgroundWorkerCrp.ReportProgress(0);
            DateTime dateCurrent = DateTime.Now;
            try
            {
                if (oSqlConn.State != ConnectionState.Open)
                    oSqlConn.Open();
                string sQueryFile = ConfigurationManager.AppSettings["Query CRP"].ToString();
                string sQuery = (new StreamReader(sQueryFile)).ReadToEnd();
                if (!sQuery.Contains("UPDATE") && !sQuery.Contains("INSERT") && !sQuery.Contains("DELETE"))
                {
                    SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn);
                    oCmd.CommandTimeout = 0;
                    DataTable dtTemp = new DataTable();
                    (new SqlDataAdapter(oCmd)).Fill(dtTemp);

                    if (dtTemp != null && dtTemp.Rows.Count > 0)
                    {
                        string sOutputFile = ConfigurationManager.AppSettings["Output CRP"].ToString();
                        StreamWriter sw = new StreamWriter(sOutputFile, false);

                        foreach (DataRow row in dtTemp.Rows)
                            if (!string.IsNullOrEmpty(row[0].ToString()))
                                sw.Write("{0}\n", row[0]);

                        sw.Close();
                        InSysLogClass.Info("SUCCESS");
                        //Console.WriteLine("SUCCESS");
                    }
                    else
                    {
                        InSysLogClass.Info("INFO : No Record to write");
                    }
                }
                else
                {
                    InSysLogClass.Info("FAILED : UPDATE, INSERT, DELETE are NOT ALLOWED");
                }
            }
            catch (SqlException sqlex)
            {
                InSysLogClass.Error(sqlex);
            }
            catch (Exception ex)
            {
                InSysLogClass.Error(ex);
            }
            backgroundWorkerCrp.ReportProgress(100);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}