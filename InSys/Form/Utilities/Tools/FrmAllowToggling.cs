﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using InSysClass;

namespace InSys
{
    public partial class FrmAllowToggling : Form
    {
        string sTxtFileName;
        DataTable DtTerminal;
        SqlConnection oSqlConn;
        string sUserID;
        bool bFlagOpenFile = false;
        string sTerminalID = "";
        string sValueTagDC003 = "";
        public int iMaxRetry;
        
         static List<string> ListToGenerateReport = new List<string>();

        public FrmAllowToggling(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            sUserID = _sUserID;
            iMaxRetry = int.Parse(ConfigurationManager.AppSettings["MaxRetry"].ToString());
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        public void OpenFile()
        {
            lblPathFile.Visible = false;
            sTxtFileName = "";
            dataGridView1.DataSource = null;

            openfileAllow.Title = "Select File";
            openfileAllow.InitialDirectory = Environment.CurrentDirectory;
            openfileAllow.Filter = "Text files (*.txt)|*.txt";

            openfileAllow.FilterIndex = 1;
            openfileAllow.RestoreDirectory = true;
            try
            {
                if (openfileAllow.ShowDialog() == DialogResult.OK)
                {
                    sTxtFileName = openfileAllow.FileName;
                    InsertToDataTable(sTxtFileName);
                    bFlagOpenFile = true;
                    dataGridView1.DataSource = DtTerminal;
                    lblPathFile.Text = sTxtFileName;
                    lblPathFile.Visible = true;
                    lblCounter.Text = DtTerminal.Rows.Count.ToString();
                }
            }
            catch
            {
                MessageBox.Show("Failed Open File");
            }
        }

        public DataTable InsertToDataTable(string sFilePath)
        {
            DtTerminal = new DataTable();
            string[] columns = null;

            var lines = File.ReadAllLines(sFilePath);

            if (lines.Count() > 0)
            {
                columns = lines[0].Split(new char[] { ',' });
                foreach (var column in columns)
                    ////DtTerminal.Columns.Add(column);
                    DtTerminal.Columns.Add("TerminalID");
            }

            for (int i = 0; i < lines.Count(); i++)
            {
                DataRow dr = DtTerminal.NewRow();
                string[] values = lines[i].Split(new char[] { ',' });

                for (int j = 0; j < values.Count() && j < columns.Count(); j++)
                    dr[j] = values[j];

                DtTerminal.Rows.Add(dr);
            }
            return DtTerminal;
        }


        //private void SetDataTID(DataTable dt)
        //{
        //    string sTID = "";
        //    int iDtRows = dt.Rows.Count;
        //    int iLastRows = 0;
        //    foreach (DataRow dr in dt.Rows)
        //    {

        //        iLastRows = iLastRows + 1 ;
        //        if (iLastRows == iDtRows)
        //        {
        //            sTID += "'" + dr[0].ToString() + "'";
        //        }
        //        else
        //        {
        //            sTID += "'" + dr[0].ToString() + "',";
        //        }
               
        //    }
        //}

        private void StartUpdate(DataTable dt)
        {
            //string sQuery;
            SqlCommand oCmd;
            int iValue = chkAllow.Checked == true ? 1 : 0;
            foreach (DataRow dr in dt.Rows)
            {
                oCmd = new SqlCommand(CommonSP.sSPToolsAllowToggling, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                sTerminalID = dr[0].ToString();
                sValueTagDC003 = DateTime.Now.ToString("MMddyyHHmmss").Replace("/", "").Replace(".", "").Replace(":", "");
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oCmd.Parameters.Add("@sDateTime", SqlDbType.VarChar).Value = sValueTagDC003;
                oCmd.Parameters.Add("@sMaxInitRetry", SqlDbType.Int).Value = iMaxRetry;
                oCmd.Parameters.Add("@sValue", SqlDbType.Int).Value = iValue;
                SqlParameter returnParameter = new SqlParameter("@RetVal", SqlDbType.NVarChar, 100);
                returnParameter.Direction = ParameterDirection.Output;
                oCmd.Parameters.Add(returnParameter);
                try
                {
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    oCmd.ExecuteNonQuery();
                    string sReturn = (string)returnParameter.Value.ToString();

                   // iCounterRows = iCounterRows + 1;
                    if (sReturn == "Success")
                    {
                        if (iValue == 1)
                        {
                            ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + "Allow Initialize Success With MaxRetry : " + iMaxRetry.ToString());
                            CommonClass.InputLog(oSqlConn, "", sUserID, "", sTerminalID + " Allow Initialize By Tools With MaxRetry : " + iMaxRetry.ToString(), sTerminalID + " Allow Initialize Success With MaxRetry : " + iMaxRetry.ToString());
                        }
                        else
                        {
                            ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + "Uncheck Allow Initialize Success With MaxRetry : 0" );
                            CommonClass.InputLog(oSqlConn, "", sUserID, "", sTerminalID + "Uncheck Allow Initialize By Tools With MaxRetry : 0 ", sTerminalID + "Uncheck Allow Initialize Success With MaxRetry : 0");
                        }
                    }
                    else
                    {
                        if (iValue == 1)
                        {
                            ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + "Failed");
                            CommonClass.InputLog(oSqlConn, sTerminalID, sUserID, "", sTerminalID + " Allow Initialize By Tools   : " + iMaxRetry.ToString(), sTerminalID + " Allow Initialize Failed With " + iMaxRetry.ToString());
                        }
                        else
                        {
                            ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + "Failed");
                            CommonClass.InputLog(oSqlConn, "", sUserID, "", sTerminalID + "Uncheck Allow Initialize By Tools With MaxRetry : 0 ", sTerminalID + "Uncheck Allow Initialize failed With MaxRetry : 0");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ListToGenerateReport.Add(ex.StackTrace);
                }
            }
        }

        private void StartTagDC003(DataTable dt)
        {
            string sQuery;
            SqlCommand oCmd;
            int iSucces = 0, iNotSucces = 0;
            //Logs.Write("Start Process");
            foreach (DataRow dr in dt.Rows)
            {
                sQuery = string.Format("UPDATE tbProfileTerminal SET TerminalTagValue = '{0}' WHERE TerminalTag ='DC003' AND TerminalID IN ('{1}')", DateTime.Now.ToString("MMddyyHHms").Replace("/","").Replace(".","").Replace(":", "") , dr[0].ToString());
                oCmd = new SqlCommand(sQuery, oSqlConn);

                try
                {
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    oCmd.ExecuteNonQuery();
                    iSucces = iSucces + 1;
                    //Logs.Write(string.Format("Terminal ID : {0} Done.", sLine));
                }
                catch (Exception ex)
                {
                    iNotSucces = iNotSucces + 1;
                    InSysLogClass.Error(ex);
                }
            }
        }

        private void backgroundWorkerAllowToggling_DoWork(object sender, DoWorkEventArgs e)
        {            
            StartUpdate(DtTerminal);           
        }

        private void backgroundWorkerAllowToggling_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 100)
            {
                progressBar1.Style = ProgressBarStyle.Blocks;
                this.Cursor = Cursors.Default;
            }
        }

        private void backgroundWorkerAllowToggling_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay("STOP");
            lstResult.DataSource = ListToGenerateReport;
            MessageBox.Show("UNINTALLIZE ALLOW DONE");
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            SetDisplay("START");
            backgroundWorkerAllowToggling.RunWorkerAsync();    
        }

        private void SetDisplay(string sFlag)
        {
            if (sFlag == "START")
            {
                progressBar1.Value = 0;
                progressBar1.Maximum = 100;
                progressBar1.Style = ProgressBarStyle.Marquee;
                dataGridView1.Enabled = false;
                btnBrowse.Enabled = false;
                btnExecute.Enabled = false;
            }
            if (sFlag == "STOP")
            {
                btnBrowse.Enabled = true;
                btnExecute.Enabled = true;
                dataGridView1.Enabled = true;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 100;
            }
        }
    }
}
