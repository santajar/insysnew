﻿namespace InSys
{
    partial class FrmQC2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmQC2));
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbProgress = new System.Windows.Forms.GroupBox();
            this.pgBar = new System.Windows.Forms.ProgressBar();
            this.gbHeader = new System.Windows.Forms.GroupBox();
            this.btnSourceBrowse = new System.Windows.Forms.Button();
            this.txtSourceFile = new System.Windows.Forms.TextBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.gbConsole = new System.Windows.Forms.GroupBox();
            this.rtbConsole = new System.Windows.Forms.RichTextBox();
            this.bwWorker = new System.ComponentModel.BackgroundWorker();
            this.gbButton.SuspendLayout();
            this.gbProgress.SuspendLayout();
            this.gbHeader.SuspendLayout();
            this.gbConsole.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(16, 146);
            this.gbButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Name = "gbButton";
            this.gbButton.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Size = new System.Drawing.Size(733, 60);
            this.gbButton.TabIndex = 5;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCancel.Location = new System.Drawing.Point(613, 15);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(107, 37);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSave.Location = new System.Drawing.Point(499, 15);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 37);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Compare";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbProgress
            // 
            this.gbProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbProgress.Controls.Add(this.pgBar);
            this.gbProgress.Location = new System.Drawing.Point(16, 87);
            this.gbProgress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbProgress.Name = "gbProgress";
            this.gbProgress.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbProgress.Size = new System.Drawing.Size(733, 52);
            this.gbProgress.TabIndex = 4;
            this.gbProgress.TabStop = false;
            // 
            // pgBar
            // 
            this.pgBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgBar.Location = new System.Drawing.Point(5, 14);
            this.pgBar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pgBar.Name = "pgBar";
            this.pgBar.Size = new System.Drawing.Size(721, 31);
            this.pgBar.TabIndex = 0;
            // 
            // gbHeader
            // 
            this.gbHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbHeader.Controls.Add(this.btnSourceBrowse);
            this.gbHeader.Controls.Add(this.txtSourceFile);
            this.gbHeader.Controls.Add(this.lblFileName);
            this.gbHeader.Location = new System.Drawing.Point(16, 15);
            this.gbHeader.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbHeader.Name = "gbHeader";
            this.gbHeader.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbHeader.Size = new System.Drawing.Size(733, 65);
            this.gbHeader.TabIndex = 3;
            this.gbHeader.TabStop = false;
            // 
            // btnSourceBrowse
            // 
            this.btnSourceBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSourceBrowse.Location = new System.Drawing.Point(613, 15);
            this.btnSourceBrowse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSourceBrowse.Name = "btnSourceBrowse";
            this.btnSourceBrowse.Size = new System.Drawing.Size(107, 37);
            this.btnSourceBrowse.TabIndex = 5;
            this.btnSourceBrowse.Text = "Browse";
            this.btnSourceBrowse.UseVisualStyleBackColor = true;
            this.btnSourceBrowse.Click += new System.EventHandler(this.btnSourceBrowse_Click);
            // 
            // txtSourceFile
            // 
            this.txtSourceFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSourceFile.Location = new System.Drawing.Point(128, 20);
            this.txtSourceFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSourceFile.Name = "txtSourceFile";
            this.txtSourceFile.Size = new System.Drawing.Size(476, 22);
            this.txtSourceFile.TabIndex = 4;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileName.Location = new System.Drawing.Point(8, 23);
            this.lblFileName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(87, 18);
            this.lblFileName.TabIndex = 3;
            this.lblFileName.Text = "Source File:";
            // 
            // gbConsole
            // 
            this.gbConsole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbConsole.Controls.Add(this.rtbConsole);
            this.gbConsole.Location = new System.Drawing.Point(16, 214);
            this.gbConsole.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbConsole.Name = "gbConsole";
            this.gbConsole.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbConsole.Size = new System.Drawing.Size(733, 338);
            this.gbConsole.TabIndex = 6;
            this.gbConsole.TabStop = false;
            // 
            // rtbConsole
            // 
            this.rtbConsole.BackColor = System.Drawing.SystemColors.MenuText;
            this.rtbConsole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbConsole.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbConsole.ForeColor = System.Drawing.Color.Lime;
            this.rtbConsole.Location = new System.Drawing.Point(4, 19);
            this.rtbConsole.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rtbConsole.Name = "rtbConsole";
            this.rtbConsole.ReadOnly = true;
            this.rtbConsole.Size = new System.Drawing.Size(725, 315);
            this.rtbConsole.TabIndex = 0;
            this.rtbConsole.Text = "";
            // 
            // bwWorker
            // 
            this.bwWorker.WorkerReportsProgress = true;
            this.bwWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwWorker_DoWork);
            this.bwWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwWorker_ProgressChanged);
            this.bwWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwWorker_RunWorkerCompleted);
            // 
            // FrmQC2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 567);
            this.Controls.Add(this.gbConsole);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbProgress);
            this.Controls.Add(this.gbHeader);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmQC2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QC EMS";
            this.Load += new System.EventHandler(this.frmQC2_Load);
            this.gbButton.ResumeLayout(false);
            this.gbProgress.ResumeLayout(false);
            this.gbHeader.ResumeLayout(false);
            this.gbHeader.PerformLayout();
            this.gbConsole.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbProgress;
        private System.Windows.Forms.ProgressBar pgBar;
        private System.Windows.Forms.GroupBox gbHeader;
        private System.Windows.Forms.Button btnSourceBrowse;
        private System.Windows.Forms.TextBox txtSourceFile;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.GroupBox gbConsole;
        private System.Windows.Forms.RichTextBox rtbConsole;
        private System.ComponentModel.BackgroundWorker bwWorker;
    }
}