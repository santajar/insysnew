﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using InSysClass;

namespace InSys
{
    public partial class FrmNewQueryLoader : Form
    {
        /// <summary>
        /// Query Loader v2.00
        /// </summary>
        public FrmNewQueryLoader(SqlConnection _oSqlConn)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
        }

        #region "Properties"
        SqlConnection oSqlConn = new SqlConnection();
        DataTable dtResult = new DataTable();
        //SqlDataReader oRead = new SqlDataReader();
        string sQuery = null;
        string sFileQuery = null;
        string sDirQuery = null;

        string sServer = null;
        string sDatabase = null;
        string sUsername = null;
        string sPassword = null;

        bool isEnableDateFilter;
        string sDateStart;
        string sDateEnd;
        #endregion

        private void FrmNewQueryLoader_Load(object sender, EventArgs e)
        {
            sDirQuery = string.Format(@"{0}\QUERY", Environment.CurrentDirectory);
            Init();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            dtResult.Clear();
            LoadQueryFiles();
            if (oSqlConn.State == ConnectionState.Open)
            {
                if (!string.IsNullOrEmpty(sQuery))
                {
                    sDateStart = string.Format("{0:yyyy-MM-dd}", dtpStartDate.Value);
                    sDateEnd = string.Format("{0:yyyy-MM-dd}", dtpEndDate.Value);
                    bool bAbort = false;
                    if (isEnableDateFilter)
                    {
                        if (IsDateValid())
                        {
                            sQuery = sQuery.Replace("where StatusDesc like '%complete'", "where StatusDesc like '%complete' AND InitTime BETWEEN CONVERT(date,'" + sDateStart + "')" + " AND CONVERT(date,'" + sDateEnd + "')");
                            //if (sQuery.IndexOf("[DATESTART]") > 0)
                            //    sQuery = sQuery.Replace("[DATESTART]", sDateStart);
                            //if (sQuery.IndexOf("[DATEEND]") > 0)
                            //    sQuery = sQuery.Replace("[DATEEND]", sDateEnd);
                        }
                        else
                        {
                            MessageBox.Show("End Date is must bigger than Start Date");
                            dtpEndDate.Focus();
                            bAbort = true;
                        }
                    }
                    if (!bAbort)
                    {
                        StatusGroupBox(true);
                        bwProcess.RunWorkerAsync(false);
                    }
                }
                else
                {
                    MessageBox.Show("Query tidak ada atau file query belum dipilih. Pilih file terlebih dahulu!");
                    cmbFileQuery.Select();
                }
            }
            else
            {
                MessageBox.Show("Koneksi database belum ada. Setting koneksi terlebih dahulu!");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dtResult.Rows.Count > 0)
            {
                dtResult.TableName = "Result";
                string sFilename = sResultFilename();
                string sTemplateXLSX = Environment.CurrentDirectory + @"\template.xlsx";
                string sTemplateXLS = Environment.CurrentDirectory + @"\template.xls";
                if (!string.IsNullOrEmpty(sFilename))
                {
                    File.Delete(sFilename);
                    if (sFilename.Contains(".xlsx"))
                        File.Copy(sTemplateXLSX, sFilename);
                    else
                        File.Copy(sTemplateXLS, sFilename);
                    ExcelOleDb oXlsOle = new ExcelOleDb(sFilename);
                    oXlsOle.CreateWorkSheet(dtResult);
                    oXlsOle.InsertRow(dtResult);
                    oXlsOle.Dispose();
                }
            }
            else
                MessageBox.Show("No Data to save");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnEditQuery_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sFileQuery))
            {
                string sFilename = string.Format(@"{0}\{1}.sql", sDirQuery, sFileQuery);
                Process procNotepad = new Process();
                procNotepad.StartInfo = (new ProcessStartInfo("notepad", sFilename));
                procNotepad.Start();
                procNotepad.WaitForExit();
                procNotepad.Close();
                LoadQueryFiles();
            }
        }

        private void btnAddQuery_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdQuery = new OpenFileDialog();

            ofdQuery.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (ofdQuery.ShowDialog() == DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(ofdQuery.FileName))
                {
                    string sFilename = string.Format(@"{0}\{1}", sDirQuery, ofdQuery.SafeFileName);
                    string sFilenameBak = sFilename.Replace(".sql", ".bak");
                    if (File.Exists(sFilename))
                    {
                        if (MessageBox.Show("Duplicate file detected, Replace?", "Save File", MessageBoxButtons.YesNo)
                            == DialogResult.Yes)
                        {
                            if (MessageBox.Show("Backup duplicate file?", "Save File", MessageBoxButtons.YesNo)
                                == DialogResult.Yes)
                                File.Copy(sFilename, sFilenameBak);
                            File.Delete(sFilename);
                            File.Copy(ofdQuery.FileName, sFilename);
                        }
                    }
                    else
                        File.Copy(ofdQuery.FileName, sFilename);
                }
            }
        }

        private void cmbFileQuery_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbFileQuery.Text))
            {
                sFileQuery = cmbFileQuery.Text;
                LoadQueryFiles();
            }
        }

        private void bwProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            GetResultAndView((bool)e.Argument, ref dtResult, oSqlConn, sQuery);
        }

        private void bwProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StatusGroupBox(false);
        }

        private void chkFilterDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilterDate.Checked)
                isEnableDateFilter = true;
            else
                isEnableDateFilter = false;
            dtpStartDate.Enabled = isEnableDateFilter;
            dtpEndDate.Enabled = isEnableDateFilter;
        }

        #region "Function"
        protected void Init()
        {
            InitQueryList();
            InitVersion();
        }
        
        protected void InitQueryList()
        {
            FileInfo[] arrfiQuery = (new DirectoryInfo(sDirQuery)).GetFiles("*.sql");
            foreach (FileInfo fiTemp in arrfiQuery)
                cmbFileQuery.Items.Add(fiTemp.Name.Replace(".sql", ""));
        }

        protected void InitVersion()
        {
            string sVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            this.Text += " v." + sVersion;
        }
        
        protected void LoadQueryFiles()
        {
            string sFilename = string.Format(@"{0}\{1}.sql", sDirQuery, sFileQuery);
            if (!string.IsNullOrEmpty(sFilename))
            {
                if (File.Exists(sFilename))
                {
                    StreamReader sr = new StreamReader(sFilename);
                    sQuery = sr.ReadToEnd();
                    sr.Close();
                }
                else
                {
                    MessageBox.Show("File hilang, atau telah dihapus. Pilih kembali file.");
                    cmbFileQuery.Items.RemoveAt(cmbFileQuery.Items.IndexOf(sFileQuery));
                    cmbFileQuery.SelectedIndex = -1;
                }
            }
        }

        protected void StatusGroupBox(bool IsEnable)
        { 
            gbQuery.Enabled = !IsEnable;
            pbProcess.Style = IsEnable ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;
            if (IsEnable)
                dgvResult.DataSource = null;

            if (dtResult.Rows.Count > 0)
                dgvResult.DataSource = dtResult;
        }

        protected bool IsDateValid()
        {

            return dtpEndDate.Value.Subtract(dtpStartDate.Value).Days >= 0 ? true : false;
            //return dtpEndDate.Value.CompareTo(dtpStartDate.Value) >= 0 ? true : false;
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            string sfilename = string.Format(@"{0}\{1}_Report.csv", Environment.CurrentDirectory, DateTime.Now.ToString("yyMMddHHmmss"));
            Csv.Write(dtResult, sfilename);
            MessageBox.Show("file Exported succssessfully.");
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        public static string sResultFilename()
        {
            SaveFileDialog sfdResult = new SaveFileDialog();
            sfdResult.InitialDirectory = Environment.SpecialFolder.Desktop.ToString();
            sfdResult.Filter = "Excel Files(*.xlsx)|*.xlsx|Excel 97-2003 Format Files(*.xls)|*.xls";
            sfdResult.OverwritePrompt = true;
            sfdResult.AddExtension = true;
            if (sfdResult.ShowDialog() == DialogResult.OK)
                return sfdResult.FileName;

            return null;
        }

        public static void GetResultAndView(bool IsSaveResult, ref DataTable dtResult,
            SqlConnection oSqlConn, string sQuery)
        {
            DataTable dtTemp = new DataTable();
            using (SqlDataReader oRead = drGetResult(oSqlConn, sQuery))
                if (oRead.HasRows)
                    dtTemp.Load(oRead);

            if (IsSaveResult)
            {
                string sFilename = sResultFilename();
                if (!string.IsNullOrEmpty(sFilename))
                    ExcelLib.WriteExcel(drGetResult(oSqlConn, sQuery), sFilename);
            }
            if (dtTemp.Rows.Count > 0)
                dtResult = dtTemp;
        }

        public static SqlDataReader drGetResult(SqlConnection oSqlConn, string sQuery)
        {
            SqlDataReader oReadTemp;
            SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn);
            oCmd.CommandType = CommandType.Text;
            oCmd.CommandTimeout = 0;
            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }
            oReadTemp = oCmd.ExecuteReader();
            return oReadTemp;
        }
    }
}
