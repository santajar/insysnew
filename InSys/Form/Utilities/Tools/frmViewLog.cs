﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmViewLog : Form
    {
        protected SqlConnection oConn;
        protected DataTable dtLogData;

        public FrmViewLog(SqlConnection _oConn)
        {
            InitializeComponent();
            oConn = _oConn;
            sfdSaveFile.Filter = "Excel 2003 (*.xls)|*.xls";
            pbProgress.Visible = false;
        }

        private void btnViewLog_Click(object sender, EventArgs e)
        {
            doFillDataLog();
            doFillDataGridView();
        }

        protected void doFillDataGridView()
        {
            dgvData.DataSource = dtLogData;
        }

        protected void doFillDataLog()
        {
            using (SqlCommand oComm = new SqlCommand(CommonSP.sSPReportLogBrowse, oConn))
            {
                dtLogData = new DataTable();
                oComm.CommandType = CommandType.StoredProcedure;
                oComm.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGenerateConditon();
                using (SqlDataAdapter oAdapt = new SqlDataAdapter(oComm))
                    oAdapt.Fill(dtLogData);
            }

        }

        protected string sGenerateConditon()
        {
            return string.Format("WHERE DATEDIFF ( day, '{0}', Date) >= 0 AND DATEDIFF(day,Date, '{1}') >= 0 AND UserId = '{2}'",
                sStartDate(), sEndDate(), UserData.sUserID);
        }

        protected string sStartDate()
        {
            return dtpStartDate.Value.ToString();
        }

        protected string sEndDate()
        {
            return dtpEndDate.Value.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dgvData.RowCount < 1 || dtLogData == null)
                MessageBox.Show("There are no data to be saved.");
            else if (sfdSaveFile.ShowDialog() != DialogResult.Cancel)
            {
                doEnableForm(false);
                bgWorker.RunWorkerAsync(sfdSaveFile.FileName);
            }
        }

        protected void doEnableForm(bool isEnable)
        {
            gbFilter.Enabled = isEnable;
            gbData.Enabled = isEnable;
            gbButton.Enabled = isEnable;
            pbProgress.Visible = !isEnable;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ExcelLib.WriteExcel(dtLogData, e.Argument.ToString());
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            pbProgress.Visible = false;
            doEnableForm(true);
        }
    }
}

