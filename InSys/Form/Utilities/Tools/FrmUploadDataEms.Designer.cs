namespace InSys
{
    partial class FrmUploadDataEms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUploadDataEms));
            this.label1 = new System.Windows.Forms.Label();
            this.txtSourceFile = new System.Windows.Forms.TextBox();
            this.barUploadProgress = new System.Windows.Forms.ProgressBar();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.ofdUpload = new System.Windows.Forms.OpenFileDialog();
            this.tipUpload = new System.Windows.Forms.ToolTip(this.components);
            this.gbUserInput = new System.Windows.Forms.GroupBox();
            this.cmbTemplate = new System.Windows.Forms.ComboBox();
            this.txtTemplate = new System.Windows.Forms.Label();
            this.rbDelete = new System.Windows.Forms.RadioButton();
            this.rbUpdate = new System.Windows.Forms.RadioButton();
            this.rbAdd = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            this.wrkUpload = new System.ComponentModel.BackgroundWorker();
            this.gbUserInput.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Upload File (*.xml)";
            // 
            // txtSourceFile
            // 
            this.txtSourceFile.Location = new System.Drawing.Point(133, 46);
            this.txtSourceFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSourceFile.Name = "txtSourceFile";
            this.txtSourceFile.ReadOnly = true;
            this.txtSourceFile.Size = new System.Drawing.Size(509, 22);
            this.txtSourceFile.TabIndex = 11;
            this.txtSourceFile.TextChanged += new System.EventHandler(this.txtSourceFile_TextChanged);
            this.txtSourceFile.MouseEnter += new System.EventHandler(this.txtSourceFile_MouseEnter);
            this.txtSourceFile.MouseLeave += new System.EventHandler(this.txtSourceFile_MouseLeave);
            // 
            // barUploadProgress
            // 
            this.barUploadProgress.Location = new System.Drawing.Point(8, 17);
            this.barUploadProgress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.barUploadProgress.Name = "barUploadProgress";
            this.barUploadProgress.Size = new System.Drawing.Size(744, 28);
            this.barUploadProgress.TabIndex = 2;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(652, 43);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(100, 28);
            this.btnBrowse.TabIndex = 3;
            this.btnBrowse.Text = "&Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // ofdUpload
            // 
            this.ofdUpload.Filter = "Excel Files(*.xlsx)|*.xlsx";
            // 
            // gbUserInput
            // 
            this.gbUserInput.Controls.Add(this.cmbTemplate);
            this.gbUserInput.Controls.Add(this.txtTemplate);
            this.gbUserInput.Controls.Add(this.rbDelete);
            this.gbUserInput.Controls.Add(this.rbUpdate);
            this.gbUserInput.Controls.Add(this.rbAdd);
            this.gbUserInput.Controls.Add(this.btnBrowse);
            this.gbUserInput.Controls.Add(this.label1);
            this.gbUserInput.Controls.Add(this.txtSourceFile);
            this.gbUserInput.Location = new System.Drawing.Point(16, 15);
            this.gbUserInput.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUserInput.Name = "gbUserInput";
            this.gbUserInput.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUserInput.Size = new System.Drawing.Size(760, 112);
            this.gbUserInput.TabIndex = 0;
            this.gbUserInput.TabStop = false;
            // 
            // cmbTemplate
            // 
            this.cmbTemplate.FormattingEnabled = true;
            this.cmbTemplate.Location = new System.Drawing.Point(133, 14);
            this.cmbTemplate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbTemplate.Name = "cmbTemplate";
            this.cmbTemplate.Size = new System.Drawing.Size(307, 24);
            this.cmbTemplate.TabIndex = 15;
            this.cmbTemplate.Visible = false;
            this.cmbTemplate.SelectedIndexChanged += new System.EventHandler(this.cmbTemplate_SelectedIndexChanged);
            // 
            // txtTemplate
            // 
            this.txtTemplate.AutoSize = true;
            this.txtTemplate.Location = new System.Drawing.Point(8, 20);
            this.txtTemplate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtTemplate.Name = "txtTemplate";
            this.txtTemplate.Size = new System.Drawing.Size(119, 17);
            this.txtTemplate.TabIndex = 14;
            this.txtTemplate.Text = "Choose Template";
            this.txtTemplate.Visible = false;
            // 
            // rbDelete
            // 
            this.rbDelete.AutoSize = true;
            this.rbDelete.Location = new System.Drawing.Point(287, 80);
            this.rbDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbDelete.Name = "rbDelete";
            this.rbDelete.Size = new System.Drawing.Size(70, 21);
            this.rbDelete.TabIndex = 13;
            this.rbDelete.Text = "Delete";
            this.rbDelete.UseVisualStyleBackColor = true;
            this.rbDelete.Visible = false;
            this.rbDelete.CheckedChanged += new System.EventHandler(this.rbDelete_CheckedChanged);
            // 
            // rbUpdate
            // 
            this.rbUpdate.AutoSize = true;
            this.rbUpdate.Location = new System.Drawing.Point(139, 80);
            this.rbUpdate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbUpdate.Name = "rbUpdate";
            this.rbUpdate.Size = new System.Drawing.Size(75, 21);
            this.rbUpdate.TabIndex = 6;
            this.rbUpdate.Text = "Update";
            this.rbUpdate.UseVisualStyleBackColor = true;
            this.rbUpdate.CheckedChanged += new System.EventHandler(this.rbUpdate_CheckedChanged);
            // 
            // rbAdd
            // 
            this.rbAdd.AutoSize = true;
            this.rbAdd.Checked = true;
            this.rbAdd.Location = new System.Drawing.Point(12, 80);
            this.rbAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbAdd.Name = "rbAdd";
            this.rbAdd.Size = new System.Drawing.Size(54, 21);
            this.rbAdd.TabIndex = 12;
            this.rbAdd.TabStop = true;
            this.rbAdd.Text = "Add";
            this.rbAdd.UseVisualStyleBackColor = true;
            this.rbAdd.CheckedChanged += new System.EventHandler(this.rbAdd_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.barUploadProgress);
            this.groupBox2.Location = new System.Drawing.Point(16, 134);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(760, 55);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtStatus);
            this.groupBox3.Location = new System.Drawing.Point(16, 197);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(760, 283);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            // 
            // txtStatus
            // 
            this.txtStatus.BackColor = System.Drawing.SystemColors.Window;
            this.txtStatus.Location = new System.Drawing.Point(8, 12);
            this.txtStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStatus.Size = new System.Drawing.Size(743, 259);
            this.txtStatus.TabIndex = 5;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnUpload);
            this.gbButton.Location = new System.Drawing.Point(16, 487);
            this.gbButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Name = "gbButton";
            this.gbButton.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Size = new System.Drawing.Size(760, 52);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(652, 15);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnUpload
            // 
            this.btnUpload.Enabled = false;
            this.btnUpload.Location = new System.Drawing.Point(544, 15);
            this.btnUpload.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(100, 28);
            this.btnUpload.TabIndex = 0;
            this.btnUpload.Text = "&Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // wrkUpload
            // 
            this.wrkUpload.WorkerReportsProgress = true;
            this.wrkUpload.WorkerSupportsCancellation = true;
            this.wrkUpload.DoWork += new System.ComponentModel.DoWorkEventHandler(this.wrkUpload_DoWork);
            this.wrkUpload.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.wrkUpload_ProgressChanged);
            this.wrkUpload.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.wrkUpload_RunWorkerCompleted);
            // 
            // FrmUploadDataEms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(792, 554);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbUserInput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUploadDataEms";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Upload EMS";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmUploadDataEms_FormClosed);
            this.Load += new System.EventHandler(this.FrmUploadDataEms_Load);
            this.gbUserInput.ResumeLayout(false);
            this.gbUserInput.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSourceFile;
        private System.Windows.Forms.ProgressBar barUploadProgress;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.OpenFileDialog ofdUpload;
        private System.Windows.Forms.ToolTip tipUpload;
        private System.Windows.Forms.GroupBox gbUserInput;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnUpload;
        private System.ComponentModel.BackgroundWorker wrkUpload;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.RadioButton rbDelete;
        private System.Windows.Forms.RadioButton rbUpdate;
        private System.Windows.Forms.RadioButton rbAdd;
        private System.Windows.Forms.ComboBox cmbTemplate;
        private System.Windows.Forms.Label txtTemplate;
    }
}