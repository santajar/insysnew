﻿namespace InSys
{
    partial class FrmUploadRD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUploadRD));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.cmbRegion = new System.Windows.Forms.ComboBox();
            this.cmbCity = new System.Windows.Forms.ComboBox();
            this.cmbSoftwareName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnExecute = new System.Windows.Forms.Button();
            this.OfDFile = new System.Windows.Forms.OpenFileDialog();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBuildNumber = new System.Windows.Forms.Label();
            this.gbGroup = new System.Windows.Forms.GroupBox();
            this.gbRegion = new System.Windows.Forms.GroupBox();
            this.gbCity = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtSchedule = new System.Windows.Forms.DateTimePicker();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lstResult = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.gbGroup.SuspendLayout();
            this.gbRegion.SuspendLayout();
            this.gbCity.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Group Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Region";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 23);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "CIty";
            // 
            // cmbGroup
            // 
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(163, 23);
            this.cmbGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(160, 24);
            this.cmbGroup.TabIndex = 3;
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged_1);
            // 
            // cmbRegion
            // 
            this.cmbRegion.FormattingEnabled = true;
            this.cmbRegion.Location = new System.Drawing.Point(163, 23);
            this.cmbRegion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbRegion.Name = "cmbRegion";
            this.cmbRegion.Size = new System.Drawing.Size(160, 24);
            this.cmbRegion.TabIndex = 4;
            this.cmbRegion.SelectedIndexChanged += new System.EventHandler(this.cmbRegion_SelectedIndexChanged);
            // 
            // cmbCity
            // 
            this.cmbCity.FormattingEnabled = true;
            this.cmbCity.Location = new System.Drawing.Point(163, 22);
            this.cmbCity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbCity.Name = "cmbCity";
            this.cmbCity.Size = new System.Drawing.Size(160, 24);
            this.cmbCity.TabIndex = 5;
            this.cmbCity.SelectedIndexChanged += new System.EventHandler(this.cmbCity_SelectedIndexChanged);
            // 
            // cmbSoftwareName
            // 
            this.cmbSoftwareName.FormattingEnabled = true;
            this.cmbSoftwareName.Location = new System.Drawing.Point(159, 23);
            this.cmbSoftwareName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbSoftwareName.Name = "cmbSoftwareName";
            this.cmbSoftwareName.Size = new System.Drawing.Size(160, 24);
            this.cmbSoftwareName.TabIndex = 6;
            this.cmbSoftwareName.SelectedIndexChanged += new System.EventHandler(this.cmbSoftwareName_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Software Destinations";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(16, 374);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(187, 42);
            this.btnBrowse.TabIndex = 8;
            this.btnBrowse.Text = "BROWSE";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(16, 18);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(187, 343);
            this.dataGridView1.TabIndex = 0;
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(33, 144);
            this.btnExecute.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(117, 41);
            this.btnExecute.TabIndex = 10;
            this.btnExecute.Text = "EXEC";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // OfDFile
            // 
            this.OfDFile.FileName = "openFileDialog1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 68);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Build Number";
            // 
            // txtBuildNumber
            // 
            this.txtBuildNumber.AutoSize = true;
            this.txtBuildNumber.Location = new System.Drawing.Point(159, 68);
            this.txtBuildNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtBuildNumber.Name = "txtBuildNumber";
            this.txtBuildNumber.Size = new System.Drawing.Size(13, 17);
            this.txtBuildNumber.TabIndex = 12;
            this.txtBuildNumber.Text = "-";
            // 
            // gbGroup
            // 
            this.gbGroup.Controls.Add(this.cmbGroup);
            this.gbGroup.Controls.Add(this.label1);
            this.gbGroup.Location = new System.Drawing.Point(21, 4);
            this.gbGroup.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbGroup.Name = "gbGroup";
            this.gbGroup.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbGroup.Size = new System.Drawing.Size(332, 63);
            this.gbGroup.TabIndex = 13;
            this.gbGroup.TabStop = false;
            this.gbGroup.Text = "Category Group";
            // 
            // gbRegion
            // 
            this.gbRegion.Controls.Add(this.cmbRegion);
            this.gbRegion.Controls.Add(this.label2);
            this.gbRegion.Location = new System.Drawing.Point(21, 71);
            this.gbRegion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRegion.Name = "gbRegion";
            this.gbRegion.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRegion.Size = new System.Drawing.Size(332, 58);
            this.gbRegion.TabIndex = 14;
            this.gbRegion.TabStop = false;
            this.gbRegion.Text = "Category Region";
            // 
            // gbCity
            // 
            this.gbCity.Controls.Add(this.cmbCity);
            this.gbCity.Controls.Add(this.label3);
            this.gbCity.Location = new System.Drawing.Point(21, 137);
            this.gbCity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbCity.Name = "gbCity";
            this.gbCity.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbCity.Size = new System.Drawing.Size(332, 59);
            this.gbCity.TabIndex = 15;
            this.gbCity.TabStop = false;
            this.gbCity.Text = "Category City";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtSchedule);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnExecute);
            this.groupBox1.Controls.Add(this.cmbSoftwareName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtBuildNumber);
            this.groupBox1.Location = new System.Drawing.Point(376, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(332, 192);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Target Software";
            // 
            // dtSchedule
            // 
            this.dtSchedule.CustomFormat = "HH:mm";
            this.dtSchedule.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtSchedule.Location = new System.Drawing.Point(168, 103);
            this.dtSchedule.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtSchedule.Name = "dtSchedule";
            this.dtSchedule.Size = new System.Drawing.Size(84, 22);
            this.dtSchedule.TabIndex = 14;
            this.dtSchedule.Value = new System.DateTime(2017, 2, 22, 0, 0, 0, 0);
            this.dtSchedule.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(204, 145);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(116, 42);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 102);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Download Time";
            this.label6.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lstResult);
            this.panel1.Controls.Add(this.gbGroup);
            this.panel1.Controls.Add(this.gbRegion);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.gbCity);
            this.panel1.Location = new System.Drawing.Point(211, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(723, 407);
            this.panel1.TabIndex = 18;
            // 
            // lstResult
            // 
            this.lstResult.FormattingEnabled = true;
            this.lstResult.ItemHeight = 16;
            this.lstResult.Location = new System.Drawing.Point(21, 215);
            this.lstResult.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lstResult.Name = "lstResult";
            this.lstResult.Size = new System.Drawing.Size(685, 180);
            this.lstResult.TabIndex = 17;
            // 
            // FrmUploadRD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 431);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnBrowse);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmUploadRD";
            this.Text = "Upload RD";
            this.Load += new System.EventHandler(this.FrmUploadRD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.gbGroup.ResumeLayout(false);
            this.gbGroup.PerformLayout();
            this.gbRegion.ResumeLayout(false);
            this.gbRegion.PerformLayout();
            this.gbCity.ResumeLayout(false);
            this.gbCity.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbCity;
        private System.Windows.Forms.ComboBox cmbRegion;
        private System.Windows.Forms.ComboBox cmbSoftwareName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.OpenFileDialog OfDFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label txtBuildNumber;
        private System.Windows.Forms.GroupBox gbGroup;
        private System.Windows.Forms.GroupBox gbRegion;
        private System.Windows.Forms.GroupBox gbCity;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DateTimePicker dtSchedule;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox lstResult;
    }
}