﻿namespace InSys
{
    partial class FrmInitSoftwareProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInitSoftwareProgress));
            this.oDgvSoftwareTerminal = new System.Windows.Forms.DataGridView();
            this.TimerClock = new System.Windows.Forms.Timer(this.components);
            this.btnSearch = new System.Windows.Forms.Button();
            this.TimerRefresh = new System.Windows.Forms.Timer(this.components);
            this.lblGroup = new System.Windows.Forms.Label();
            this.lblRegion = new System.Windows.Forms.Label();
            this.lblTerminalID = new System.Windows.Forms.Label();
            this.lblSoftware = new System.Windows.Forms.Label();
            this.lblSoftwareDownload = new System.Windows.Forms.Label();
            this.lblSerialNumber = new System.Windows.Forms.Label();
            this.lblInstallStatus = new System.Windows.Forms.Label();
            this.txtGroup = new System.Windows.Forms.TextBox();
            this.txtRegion = new System.Windows.Forms.TextBox();
            this.txtTerminalID = new System.Windows.Forms.TextBox();
            this.txtSoftware = new System.Windows.Forms.TextBox();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.txtSoftwareDownload = new System.Windows.Forms.TextBox();
            this.dtpSStartTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEStartTime = new System.Windows.Forms.DateTimePicker();
            this.txtInstallStatus = new System.Windows.Forms.TextBox();
            this.dtpSEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEInstallTime = new System.Windows.Forms.DateTimePicker();
            this.dtpSInstallTime = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gbSearch = new System.Windows.Forms.GroupBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.txtBuildNumber = new System.Windows.Forms.TextBox();
            this.lblBuildNumber = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.chkFinishInstallTime = new System.Windows.Forms.CheckBox();
            this.chkEndTime = new System.Windows.Forms.CheckBox();
            this.chkStartTime = new System.Windows.Forms.CheckBox();
            this.sfdExport = new System.Windows.Forms.SaveFileDialog();
            this.txtTotalOnProgress = new System.Windows.Forms.TextBox();
            this.lblTotalTIDOnProgress = new System.Windows.Forms.Label();
            this.lblTotalEDCSuccess = new System.Windows.Forms.Label();
            this.txtTotalSucces = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.oDgvSoftwareTerminal)).BeginInit();
            this.gbSearch.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // oDgvSoftwareTerminal
            // 
            this.oDgvSoftwareTerminal.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.oDgvSoftwareTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oDgvSoftwareTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oDgvSoftwareTerminal.Location = new System.Drawing.Point(0, 0);
            this.oDgvSoftwareTerminal.Margin = new System.Windows.Forms.Padding(4);
            this.oDgvSoftwareTerminal.MultiSelect = false;
            this.oDgvSoftwareTerminal.Name = "oDgvSoftwareTerminal";
            this.oDgvSoftwareTerminal.ReadOnly = true;
            this.oDgvSoftwareTerminal.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.oDgvSoftwareTerminal.RowHeadersVisible = false;
            this.oDgvSoftwareTerminal.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.oDgvSoftwareTerminal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.oDgvSoftwareTerminal.Size = new System.Drawing.Size(1628, 644);
            this.oDgvSoftwareTerminal.TabIndex = 0;
            // 
            // TimerClock
            // 
            this.TimerClock.Enabled = true;
            this.TimerClock.Tick += new System.EventHandler(this.TimerClock_Tick);
            // 
            // btnSearch
            // 
            this.btnSearch.AutoSize = true;
            this.btnSearch.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSearch.Enabled = false;
            this.btnSearch.Location = new System.Drawing.Point(1517, 79);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(63, 27);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Visible = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // TimerRefresh
            // 
            this.TimerRefresh.Enabled = true;
            this.TimerRefresh.Interval = 30000;
            this.TimerRefresh.Tick += new System.EventHandler(this.TimerRefresh_Tick);
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.Location = new System.Drawing.Point(27, 53);
            this.lblGroup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(48, 17);
            this.lblGroup.TabIndex = 6;
            this.lblGroup.Text = "Group";
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Location = new System.Drawing.Point(27, 86);
            this.lblRegion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(53, 17);
            this.lblRegion.TabIndex = 7;
            this.lblRegion.Text = "Region";
            // 
            // lblTerminalID
            // 
            this.lblTerminalID.AutoSize = true;
            this.lblTerminalID.Location = new System.Drawing.Point(27, 117);
            this.lblTerminalID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTerminalID.Name = "lblTerminalID";
            this.lblTerminalID.Size = new System.Drawing.Size(76, 17);
            this.lblTerminalID.TabIndex = 8;
            this.lblTerminalID.Text = "TerminalID";
            // 
            // lblSoftware
            // 
            this.lblSoftware.AutoSize = true;
            this.lblSoftware.Location = new System.Drawing.Point(359, 27);
            this.lblSoftware.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSoftware.Name = "lblSoftware";
            this.lblSoftware.Size = new System.Drawing.Size(63, 17);
            this.lblSoftware.TabIndex = 9;
            this.lblSoftware.Text = "Software";
            // 
            // lblSoftwareDownload
            // 
            this.lblSoftwareDownload.AutoSize = true;
            this.lblSoftwareDownload.Location = new System.Drawing.Point(359, 55);
            this.lblSoftwareDownload.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSoftwareDownload.Name = "lblSoftwareDownload";
            this.lblSoftwareDownload.Size = new System.Drawing.Size(129, 17);
            this.lblSoftwareDownload.TabIndex = 10;
            this.lblSoftwareDownload.Text = "Software Download";
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.AutoSize = true;
            this.lblSerialNumber.Location = new System.Drawing.Point(359, 117);
            this.lblSerialNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(98, 17);
            this.lblSerialNumber.TabIndex = 11;
            this.lblSerialNumber.Text = "Serial Number";
            // 
            // lblInstallStatus
            // 
            this.lblInstallStatus.AutoSize = true;
            this.lblInstallStatus.Location = new System.Drawing.Point(769, 21);
            this.lblInstallStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInstallStatus.Name = "lblInstallStatus";
            this.lblInstallStatus.Size = new System.Drawing.Size(88, 17);
            this.lblInstallStatus.TabIndex = 15;
            this.lblInstallStatus.Text = "Install Status";
            // 
            // txtGroup
            // 
            this.txtGroup.Location = new System.Drawing.Point(132, 49);
            this.txtGroup.Margin = new System.Windows.Forms.Padding(4);
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Size = new System.Drawing.Size(200, 22);
            this.txtGroup.TabIndex = 17;
            // 
            // txtRegion
            // 
            this.txtRegion.Location = new System.Drawing.Point(132, 81);
            this.txtRegion.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Size = new System.Drawing.Size(200, 22);
            this.txtRegion.TabIndex = 18;
            // 
            // txtTerminalID
            // 
            this.txtTerminalID.Location = new System.Drawing.Point(132, 113);
            this.txtTerminalID.Margin = new System.Windows.Forms.Padding(4);
            this.txtTerminalID.Name = "txtTerminalID";
            this.txtTerminalID.Size = new System.Drawing.Size(200, 22);
            this.txtTerminalID.TabIndex = 19;
            // 
            // txtSoftware
            // 
            this.txtSoftware.Location = new System.Drawing.Point(512, 20);
            this.txtSoftware.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoftware.Name = "txtSoftware";
            this.txtSoftware.Size = new System.Drawing.Size(200, 22);
            this.txtSoftware.TabIndex = 20;
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.Location = new System.Drawing.Point(512, 114);
            this.txtSerialNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(200, 22);
            this.txtSerialNumber.TabIndex = 21;
            // 
            // txtSoftwareDownload
            // 
            this.txtSoftwareDownload.Location = new System.Drawing.Point(512, 50);
            this.txtSoftwareDownload.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoftwareDownload.Name = "txtSoftwareDownload";
            this.txtSoftwareDownload.Size = new System.Drawing.Size(200, 22);
            this.txtSoftwareDownload.TabIndex = 22;
            // 
            // dtpSStartTime
            // 
            this.dtpSStartTime.CustomFormat = "mmddyy";
            this.dtpSStartTime.Enabled = false;
            this.dtpSStartTime.Location = new System.Drawing.Point(893, 52);
            this.dtpSStartTime.Margin = new System.Windows.Forms.Padding(4);
            this.dtpSStartTime.Name = "dtpSStartTime";
            this.dtpSStartTime.Size = new System.Drawing.Size(265, 22);
            this.dtpSStartTime.TabIndex = 24;
            this.dtpSStartTime.Value = new System.DateTime(2013, 11, 1, 16, 11, 0, 0);
            // 
            // dtpEStartTime
            // 
            this.dtpEStartTime.CustomFormat = "mmddyy";
            this.dtpEStartTime.Enabled = false;
            this.dtpEStartTime.Location = new System.Drawing.Point(1201, 53);
            this.dtpEStartTime.Margin = new System.Windows.Forms.Padding(4);
            this.dtpEStartTime.Name = "dtpEStartTime";
            this.dtpEStartTime.Size = new System.Drawing.Size(265, 22);
            this.dtpEStartTime.TabIndex = 25;
            // 
            // txtInstallStatus
            // 
            this.txtInstallStatus.Location = new System.Drawing.Point(893, 17);
            this.txtInstallStatus.Margin = new System.Windows.Forms.Padding(4);
            this.txtInstallStatus.Name = "txtInstallStatus";
            this.txtInstallStatus.Size = new System.Drawing.Size(200, 22);
            this.txtInstallStatus.TabIndex = 26;
            // 
            // dtpSEndTime
            // 
            this.dtpSEndTime.CustomFormat = "mmddyy";
            this.dtpSEndTime.Enabled = false;
            this.dtpSEndTime.Location = new System.Drawing.Point(893, 84);
            this.dtpSEndTime.Margin = new System.Windows.Forms.Padding(4);
            this.dtpSEndTime.Name = "dtpSEndTime";
            this.dtpSEndTime.Size = new System.Drawing.Size(265, 22);
            this.dtpSEndTime.TabIndex = 27;
            this.dtpSEndTime.Value = new System.DateTime(2013, 11, 1, 16, 20, 0, 0);
            // 
            // dtpEEndTime
            // 
            this.dtpEEndTime.CustomFormat = "mmddyy";
            this.dtpEEndTime.Enabled = false;
            this.dtpEEndTime.Location = new System.Drawing.Point(1201, 84);
            this.dtpEEndTime.Margin = new System.Windows.Forms.Padding(4);
            this.dtpEEndTime.Name = "dtpEEndTime";
            this.dtpEEndTime.Size = new System.Drawing.Size(265, 22);
            this.dtpEEndTime.TabIndex = 28;
            // 
            // dtpEInstallTime
            // 
            this.dtpEInstallTime.CustomFormat = "mmddyy";
            this.dtpEInstallTime.Enabled = false;
            this.dtpEInstallTime.Location = new System.Drawing.Point(1203, 116);
            this.dtpEInstallTime.Margin = new System.Windows.Forms.Padding(4);
            this.dtpEInstallTime.Name = "dtpEInstallTime";
            this.dtpEInstallTime.Size = new System.Drawing.Size(265, 22);
            this.dtpEInstallTime.TabIndex = 30;
            // 
            // dtpSInstallTime
            // 
            this.dtpSInstallTime.CustomFormat = "mmddyy";
            this.dtpSInstallTime.Enabled = false;
            this.dtpSInstallTime.Location = new System.Drawing.Point(893, 116);
            this.dtpSInstallTime.Margin = new System.Windows.Forms.Padding(4);
            this.dtpSInstallTime.Name = "dtpSInstallTime";
            this.dtpSInstallTime.Size = new System.Drawing.Size(265, 22);
            this.dtpSInstallTime.TabIndex = 29;
            this.dtpSInstallTime.Value = new System.DateTime(2013, 11, 1, 16, 21, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1171, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 31;
            this.label1.Text = "to";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1172, 90);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 17);
            this.label2.TabIndex = 32;
            this.label2.Text = "to";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1172, 122);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 17);
            this.label3.TabIndex = 33;
            this.label3.Text = "to";
            // 
            // gbSearch
            // 
            this.gbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSearch.Controls.Add(this.txtTotalSucces);
            this.gbSearch.Controls.Add(this.btnExport);
            this.gbSearch.Controls.Add(this.lblTotalEDCSuccess);
            this.gbSearch.Controls.Add(this.lblTotalTIDOnProgress);
            this.gbSearch.Controls.Add(this.txtBuildNumber);
            this.gbSearch.Controls.Add(this.txtTotalOnProgress);
            this.gbSearch.Controls.Add(this.lblBuildNumber);
            this.gbSearch.Controls.Add(this.txtCity);
            this.gbSearch.Controls.Add(this.lblCity);
            this.gbSearch.Controls.Add(this.chkFinishInstallTime);
            this.gbSearch.Controls.Add(this.chkEndTime);
            this.gbSearch.Controls.Add(this.txtGroup);
            this.gbSearch.Controls.Add(this.label3);
            this.gbSearch.Controls.Add(this.txtSoftwareDownload);
            this.gbSearch.Controls.Add(this.lblSoftwareDownload);
            this.gbSearch.Controls.Add(this.label2);
            this.gbSearch.Controls.Add(this.lblInstallStatus);
            this.gbSearch.Controls.Add(this.txtRegion);
            this.gbSearch.Controls.Add(this.label1);
            this.gbSearch.Controls.Add(this.dtpEInstallTime);
            this.gbSearch.Controls.Add(this.txtTerminalID);
            this.gbSearch.Controls.Add(this.txtSerialNumber);
            this.gbSearch.Controls.Add(this.dtpSInstallTime);
            this.gbSearch.Controls.Add(this.txtSoftware);
            this.gbSearch.Controls.Add(this.btnSearch);
            this.gbSearch.Controls.Add(this.dtpEEndTime);
            this.gbSearch.Controls.Add(this.lblSerialNumber);
            this.gbSearch.Controls.Add(this.dtpSEndTime);
            this.gbSearch.Controls.Add(this.lblGroup);
            this.gbSearch.Controls.Add(this.txtInstallStatus);
            this.gbSearch.Controls.Add(this.lblSoftware);
            this.gbSearch.Controls.Add(this.lblRegion);
            this.gbSearch.Controls.Add(this.dtpSStartTime);
            this.gbSearch.Controls.Add(this.dtpEStartTime);
            this.gbSearch.Controls.Add(this.lblTerminalID);
            this.gbSearch.Controls.Add(this.chkStartTime);
            this.gbSearch.Location = new System.Drawing.Point(16, 15);
            this.gbSearch.Margin = new System.Windows.Forms.Padding(4);
            this.gbSearch.Name = "gbSearch";
            this.gbSearch.Padding = new System.Windows.Forms.Padding(4);
            this.gbSearch.Size = new System.Drawing.Size(1628, 224);
            this.gbSearch.TabIndex = 34;
            this.gbSearch.TabStop = false;
            this.gbSearch.Text = "Advance Search";
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(1517, 114);
            this.btnExport.Margin = new System.Windows.Forms.Padding(4);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(99, 28);
            this.btnExport.TabIndex = 35;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtBuildNumber
            // 
            this.txtBuildNumber.Location = new System.Drawing.Point(512, 81);
            this.txtBuildNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtBuildNumber.MaxLength = 4;
            this.txtBuildNumber.Name = "txtBuildNumber";
            this.txtBuildNumber.Size = new System.Drawing.Size(200, 22);
            this.txtBuildNumber.TabIndex = 40;
            // 
            // lblBuildNumber
            // 
            this.lblBuildNumber.AutoSize = true;
            this.lblBuildNumber.Location = new System.Drawing.Point(359, 84);
            this.lblBuildNumber.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBuildNumber.Name = "lblBuildNumber";
            this.lblBuildNumber.Size = new System.Drawing.Size(89, 17);
            this.lblBuildNumber.TabIndex = 39;
            this.lblBuildNumber.Text = "BuildNumber";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(132, 21);
            this.txtCity.Margin = new System.Windows.Forms.Padding(4);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(200, 22);
            this.txtCity.TabIndex = 38;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(27, 25);
            this.lblCity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(31, 17);
            this.lblCity.TabIndex = 37;
            this.lblCity.Text = "City";
            // 
            // chkFinishInstallTime
            // 
            this.chkFinishInstallTime.AutoSize = true;
            this.chkFinishInstallTime.Location = new System.Drawing.Point(745, 117);
            this.chkFinishInstallTime.Margin = new System.Windows.Forms.Padding(4);
            this.chkFinishInstallTime.Name = "chkFinishInstallTime";
            this.chkFinishInstallTime.Size = new System.Drawing.Size(142, 21);
            this.chkFinishInstallTime.TabIndex = 36;
            this.chkFinishInstallTime.Text = "Finish Install Time";
            this.chkFinishInstallTime.UseVisualStyleBackColor = true;
            this.chkFinishInstallTime.CheckedChanged += new System.EventHandler(this.chkFinishInstallTime_CheckedChanged);
            // 
            // chkEndTime
            // 
            this.chkEndTime.AutoSize = true;
            this.chkEndTime.Location = new System.Drawing.Point(745, 86);
            this.chkEndTime.Margin = new System.Windows.Forms.Padding(4);
            this.chkEndTime.Name = "chkEndTime";
            this.chkEndTime.Size = new System.Drawing.Size(90, 21);
            this.chkEndTime.TabIndex = 35;
            this.chkEndTime.Text = "End Time";
            this.chkEndTime.UseVisualStyleBackColor = true;
            this.chkEndTime.CheckedChanged += new System.EventHandler(this.chkEndTime_CheckedChanged);
            // 
            // chkStartTime
            // 
            this.chkStartTime.AutoSize = true;
            this.chkStartTime.Location = new System.Drawing.Point(745, 55);
            this.chkStartTime.Margin = new System.Windows.Forms.Padding(4);
            this.chkStartTime.Name = "chkStartTime";
            this.chkStartTime.Size = new System.Drawing.Size(95, 21);
            this.chkStartTime.TabIndex = 34;
            this.chkStartTime.Text = "Start Time";
            this.chkStartTime.UseVisualStyleBackColor = true;
            this.chkStartTime.CheckedChanged += new System.EventHandler(this.chkStartTime_CheckedChanged);
            // 
            // txtTotalOnProgress
            // 
            this.txtTotalOnProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalOnProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalOnProgress.Location = new System.Drawing.Point(231, 168);
            this.txtTotalOnProgress.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalOnProgress.Name = "txtTotalOnProgress";
            this.txtTotalOnProgress.ReadOnly = true;
            this.txtTotalOnProgress.Size = new System.Drawing.Size(52, 24);
            this.txtTotalOnProgress.TabIndex = 35;
            // 
            // lblTotalTIDOnProgress
            // 
            this.lblTotalTIDOnProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalTIDOnProgress.AutoSize = true;
            this.lblTotalTIDOnProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalTIDOnProgress.Location = new System.Drawing.Point(31, 172);
            this.lblTotalTIDOnProgress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalTIDOnProgress.Name = "lblTotalTIDOnProgress";
            this.lblTotalTIDOnProgress.Size = new System.Drawing.Size(173, 18);
            this.lblTotalTIDOnProgress.TabIndex = 36;
            this.lblTotalTIDOnProgress.Text = "Total TID OnProgress";
            // 
            // lblTotalEDCSuccess
            // 
            this.lblTotalEDCSuccess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalEDCSuccess.AutoSize = true;
            this.lblTotalEDCSuccess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalEDCSuccess.Location = new System.Drawing.Point(292, 172);
            this.lblTotalEDCSuccess.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalEDCSuccess.Name = "lblTotalEDCSuccess";
            this.lblTotalEDCSuccess.Size = new System.Drawing.Size(147, 18);
            this.lblTotalEDCSuccess.TabIndex = 37;
            this.lblTotalEDCSuccess.Text = "Total EDC Succes";
            // 
            // txtTotalSucces
            // 
            this.txtTotalSucces.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalSucces.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalSucces.Location = new System.Drawing.Point(462, 168);
            this.txtTotalSucces.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalSucces.Name = "txtTotalSucces";
            this.txtTotalSucces.ReadOnly = true;
            this.txtTotalSucces.Size = new System.Drawing.Size(52, 24);
            this.txtTotalSucces.TabIndex = 38;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.oDgvSoftwareTerminal);
            this.panel1.Location = new System.Drawing.Point(16, 246);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1628, 644);
            this.panel1.TabIndex = 35;
            // 
            // FrmInitSoftwareProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1656, 902);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gbSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmInitSoftwareProgress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Init Software Progress";
            this.Load += new System.EventHandler(this.FrmInitSoftwareCon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.oDgvSoftwareTerminal)).EndInit();
            this.gbSearch.ResumeLayout(false);
            this.gbSearch.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView oDgvSoftwareTerminal;
        private System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.Timer TimerClock;
        internal System.Windows.Forms.Timer TimerRefresh;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.Label lblTerminalID;
        private System.Windows.Forms.Label lblSoftware;
        private System.Windows.Forms.Label lblSoftwareDownload;
        private System.Windows.Forms.Label lblSerialNumber;
        private System.Windows.Forms.Label lblInstallStatus;
        private System.Windows.Forms.TextBox txtGroup;
        private System.Windows.Forms.TextBox txtRegion;
        private System.Windows.Forms.TextBox txtTerminalID;
        private System.Windows.Forms.TextBox txtSoftware;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.TextBox txtSoftwareDownload;
        private System.Windows.Forms.DateTimePicker dtpSStartTime;
        private System.Windows.Forms.DateTimePicker dtpEStartTime;
        private System.Windows.Forms.TextBox txtInstallStatus;
        private System.Windows.Forms.DateTimePicker dtpSEndTime;
        private System.Windows.Forms.DateTimePicker dtpEEndTime;
        private System.Windows.Forms.DateTimePicker dtpEInstallTime;
        private System.Windows.Forms.DateTimePicker dtpSInstallTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbSearch;
        private System.Windows.Forms.CheckBox chkFinishInstallTime;
        private System.Windows.Forms.CheckBox chkEndTime;
        private System.Windows.Forms.CheckBox chkStartTime;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.TextBox txtBuildNumber;
        private System.Windows.Forms.Label lblBuildNumber;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog sfdExport;
        private System.Windows.Forms.TextBox txtTotalOnProgress;
        private System.Windows.Forms.Label lblTotalTIDOnProgress;
        private System.Windows.Forms.Label lblTotalEDCSuccess;
        private System.Windows.Forms.TextBox txtTotalSucces;
        private System.Windows.Forms.Panel panel1;
    }
}