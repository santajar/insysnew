﻿namespace InSys
{
    partial class FrmRequestPaperReceipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tmRefreshReq = new System.Windows.Forms.Timer(this.components);
            this.timerRefresh = new System.Windows.Forms.Timer(this.components);
            this.sfdExport = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgReqPaper = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDeleteDetail = new System.Windows.Forms.Button();
            this.btnExportDetail = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnSearchTID = new System.Windows.Forms.Button();
            this.TxtSearchTID = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgReqPaper)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmRefreshReq
            // 
            this.tmRefreshReq.Enabled = true;
            this.tmRefreshReq.Interval = 20000;
            this.tmRefreshReq.Tick += new System.EventHandler(this.tmRefreshReq_Tick);
            // 
            // timerRefresh
            // 
            this.timerRefresh.Enabled = true;
            this.timerRefresh.Interval = 3000;
            this.timerRefresh.Tick += new System.EventHandler(this.timerRefresh_Tick);
            // 
            // sfdExport
            // 
            this.sfdExport.Filter = "Exel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.dgReqPaper);
            this.groupBox1.Location = new System.Drawing.Point(1, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(770, 352);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // dgReqPaper
            // 
            this.dgReqPaper.AllowUserToAddRows = false;
            this.dgReqPaper.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgReqPaper.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgReqPaper.Location = new System.Drawing.Point(10, 19);
            this.dgReqPaper.Name = "dgReqPaper";
            this.dgReqPaper.ReadOnly = true;
            this.dgReqPaper.Size = new System.Drawing.Size(748, 314);
            this.dgReqPaper.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.btnDeleteDetail);
            this.panel1.Controls.Add(this.btnExportDetail);
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Controls.Add(this.btnSearchTID);
            this.panel1.Controls.Add(this.TxtSearchTID);
            this.panel1.Location = new System.Drawing.Point(1, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 40);
            this.panel1.TabIndex = 4;
            // 
            // btnDeleteDetail
            // 
            this.btnDeleteDetail.Location = new System.Drawing.Point(688, 10);
            this.btnDeleteDetail.Name = "btnDeleteDetail";
            this.btnDeleteDetail.Size = new System.Drawing.Size(76, 23);
            this.btnDeleteDetail.TabIndex = 5;
            this.btnDeleteDetail.Text = "DELETE";
            this.btnDeleteDetail.UseVisualStyleBackColor = true;
            this.btnDeleteDetail.Click += new System.EventHandler(this.btnDeleteDetail_Click);
            // 
            // btnExportDetail
            // 
            this.btnExportDetail.Location = new System.Drawing.Point(524, 10);
            this.btnExportDetail.Name = "btnExportDetail";
            this.btnExportDetail.Size = new System.Drawing.Size(76, 23);
            this.btnExportDetail.TabIndex = 4;
            this.btnExportDetail.Text = "EXPORT";
            this.btnExportDetail.UseVisualStyleBackColor = true;
            this.btnExportDetail.Click += new System.EventHandler(this.btnExportDetail_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(434, 10);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(84, 23);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "REFRESH";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSearchTID
            // 
            this.btnSearchTID.Location = new System.Drawing.Point(175, 10);
            this.btnSearchTID.Name = "btnSearchTID";
            this.btnSearchTID.Size = new System.Drawing.Size(75, 23);
            this.btnSearchTID.TabIndex = 1;
            this.btnSearchTID.Text = "SEARCH";
            this.btnSearchTID.UseVisualStyleBackColor = true;
            this.btnSearchTID.Click += new System.EventHandler(this.btnSearchTID_Click);
            // 
            // TxtSearchTID
            // 
            this.TxtSearchTID.Location = new System.Drawing.Point(10, 10);
            this.TxtSearchTID.Name = "TxtSearchTID";
            this.TxtSearchTID.Size = new System.Drawing.Size(159, 20);
            this.TxtSearchTID.TabIndex = 0;
            // 
            // FrmRequestPaperReceipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 415);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Name = "FrmRequestPaperReceipt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "FrmRequestPaperReceipt";
            this.Load += new System.EventHandler(this.FrmRequestPaperReceipt_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgReqPaper)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer tmRefreshReq;
        internal System.Windows.Forms.Timer timerRefresh;
        private System.Windows.Forms.SaveFileDialog sfdExport;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgReqPaper;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDeleteDetail;
        private System.Windows.Forms.Button btnExportDetail;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnSearchTID;
        private System.Windows.Forms.TextBox TxtSearchTID;
    }
}