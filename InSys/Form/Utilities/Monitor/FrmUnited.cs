using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;
using InSysClass;

namespace InSys
{
    public partial class FrmUnited : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Needed as control variables at different events.
        /// </summary>
        protected bool isSearch = false;
        protected string sKey;

        /// <summary>
        /// Form to view the sumary progress of initiaton from EDC to Insys
        /// </summary>
        /// <param name="_oSqlConn"></param>
        public FrmUnited(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        /// <summary>
        /// Runs when form is loading, load data and set time.
        /// </summary>
        private void FrmUnited_Load(object sender, EventArgs e)
        {
            LoadData();
            DisplayTime();
            //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sFormOpened + "United", "");
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + "United", "");
        }

        /// <summary>
        /// Load data to form.
        /// </summary>
        protected void LoadData()
        {
            LoadData(null);
        }

        /// <summary>
        /// Load data and using sKey to filter the data.
        /// </summary>
        /// <param name="sKey">string : keyword to filter data</param>
        protected void LoadData(string sKey)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPInitLogConnBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                if (!string.IsNullOrEmpty(sKey)) oSqlCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sGetCond(sKey);
                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
                DataTable oDataTableInitLogConn = new DataTable();
                oAdapt.Fill(oDataTableInitLogConn);
                oDgTerminal.DataSource = oDataTableInitLogConn;
                SetDisplayDatagridView();
            }
            catch (Exception)
            {
                //CommonClass.doWriteErrorFile(ex.Message);
                //if there's no connection, placing this would show error message box over and over everytime United tries to refresh its data
            }
        }

        /// <summary>
        /// Generate condition statement used to filter database
        /// </summary>
        /// <param name="sKey">string : value to filter database</param>
        /// <returns>string : filter statement</returns>
        protected string sGetCond(string sKey)
        {
            return string.Format(
                //"WHERE Time LIKE '%{0}%' OR TerminalID LIKE '%{0}%' OR Description LIKE '%{0}%' OR Percentage LIKE '%{0}%'",
                "WHERE TerminalID LIKE '%{0}%'",
                sKey.Replace("'", "''"));
        }

        /// <summary>
        /// Set the formats for DataGridView
        /// </summary>
        protected void SetDisplayDatagridView()
        {
            oDgTerminal.Columns["Time"].DefaultCellStyle.Format = "dd MMMM yyyy  HH:mm:ss";
            oDgTerminal.Columns["Time"].SortMode = DataGridViewColumnSortMode.NotSortable;

            oDgTerminal.Columns["TerminalID"].SortMode = DataGridViewColumnSortMode.NotSortable;

            oDgTerminal.Columns["Description"].HeaderText = "Status";
            oDgTerminal.Columns["Description"].SortMode = DataGridViewColumnSortMode.NotSortable;

            oDgTerminal.Columns["Percentage"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            oDgTerminal.Columns["Percentage"].SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        /// <summary>
        /// Load data and set the current showed row
        /// </summary>
        private void timerRefresh_Tick(object sender, EventArgs e)
        {
            int iRowIndex = iGetRowIndex();
            int iIndex = oDgTerminal.FirstDisplayedScrollingRowIndex;

            if (isSearch) LoadData(sKey);
            else LoadData();

            if (oDgTerminal.RowCount > 0) oDgTerminal[1, iRowIndex].Selected = true;
            if (iIndex > -1) oDgTerminal.FirstDisplayedScrollingRowIndex = iIndex;
        }

        /// <summary>
        /// Get index from current row.
        /// </summary>
        /// <returns>int : Current row index</returns>
        protected int iGetRowIndex()
        {
            if (oDgTerminal.CurrentRow == null)
                return 0;
            else
                return oDgTerminal.CurrentRow.Index;
        }

        /// <summary>
        /// Call FrmUnitedDetail to show detail proccessing of initiation.
        /// </summary>
        private void btnView_Click(object sender, EventArgs e)
        {
            if (oDgTerminal.RowCount > 0)
            {
                FrmUnitedDetail fDetail = new FrmUnitedDetail(oSqlConn, oSqlConnAuditTrail, oDgTerminal["TerminalID", oDgTerminal.CurrentRow.Index].Value.ToString());
                string sFormName = "fDetail" + oDgTerminal["TerminalID", oDgTerminal.CurrentRow.Index].Value.ToString();
                try
                {
                    if (!isFormDetailOpen(sFormName))
                    {
                        fDetail.Name = sFormName;
                        fDetail.Show();
                    }
                }
                catch
                {
                    fDetail.Dispose();
                }
            }
        }

        /// <summary>
        /// Determines if United Detail Form is opened or not.
        /// </summary>
        /// <param name="sFormName">string : UnitedDetail form's name to be opened</param>
        /// <returns>boolean : true if already opened, else false</returns>
        protected bool isFormDetailOpen(string sFormName)
        {
            foreach (Form oForm in Application.OpenForms)
            {
                if (oForm.Name == sFormName)
                {
                    oForm.WindowState = FormWindowState.Normal;
                    oForm.Focus();
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Close the form
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Call DisplayTime function.
        /// </summary>
        private void timerClock_Tick(object sender, EventArgs e)
        {
            DisplayTime();
        }

        /// <summary>
        /// Show current time on form.
        /// </summary>
        protected void DisplayTime()
        {
            StatusTime.Text = DateTime.Now.ToString("hh:mm:ss tt", new CultureInfo("en-US"));
        }

        /// <summary>
        /// Refresh the percentage on DataGridView.
        /// </summary>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            int iRowIndex = iGetRowIndex();
            LoadData();
            oDgTerminal[1, iRowIndex].Selected = true;
            oDgTerminal.Select();
            isSearch = false;
        }

        /// <summary>
        /// Search specific TerminalID from DataGridView.
        /// </summary>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.None;
            if (txtSearch.Text.Trim().Length > 0)
            {
                LoadData(sKey = txtSearch.Text);
                isSearch = true;
            }
        }
    }
}