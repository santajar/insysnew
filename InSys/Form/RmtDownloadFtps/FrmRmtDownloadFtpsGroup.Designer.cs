﻿namespace InSys
{
    partial class FrmRmtDownloadFtpsGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRmtDownloadFtpsGroup));
            this.btnAddGroup = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBoxRD_Group = new System.Windows.Forms.GroupBox();
            this.panelButton = new System.Windows.Forms.Panel();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDeleteGroup = new System.Windows.Forms.Button();
            this.comboBoxGroup = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxRDGroup_Detail = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanelGroupDetail = new System.Windows.Forms.FlowLayoutPanel();
            this.panelAddGroup = new System.Windows.Forms.Panel();
            this.progressBarGetMID = new System.Windows.Forms.ProgressBar();
            this.cmbDatabase = new System.Windows.Forms.ComboBox();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnUploadMidGroup = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMidFilename = new System.Windows.Forms.TextBox();
            this.btnBrowseMidFilename = new System.Windows.Forms.Button();
            this.txtGroupName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panelDataGridViewGroupDetail = new System.Windows.Forms.Panel();
            this.dataGridViewGroupDetail = new System.Windows.Forms.DataGridView();
            this.backgroundWorkerGetMID = new System.ComponentModel.BackgroundWorker();
            this.groupBoxRD_Group.SuspendLayout();
            this.panelButton.SuspendLayout();
            this.groupBoxRDGroup_Detail.SuspendLayout();
            this.flowLayoutPanelGroupDetail.SuspendLayout();
            this.panelAddGroup.SuspendLayout();
            this.panelDataGridViewGroupDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroupDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddGroup
            // 
            this.btnAddGroup.Location = new System.Drawing.Point(11, 38);
            this.btnAddGroup.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddGroup.Name = "btnAddGroup";
            this.btnAddGroup.Size = new System.Drawing.Size(95, 26);
            this.btnAddGroup.TabIndex = 0;
            this.btnAddGroup.Text = "Add";
            this.btnAddGroup.UseVisualStyleBackColor = true;
            this.btnAddGroup.Click += new System.EventHandler(this.btnAddGroup_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(224, 38);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(95, 26);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBoxRD_Group
            // 
            this.groupBoxRD_Group.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxRD_Group.Controls.Add(this.panelButton);
            this.groupBoxRD_Group.Controls.Add(this.groupBoxRDGroup_Detail);
            this.groupBoxRD_Group.Location = new System.Drawing.Point(9, 10);
            this.groupBoxRD_Group.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxRD_Group.Name = "groupBoxRD_Group";
            this.groupBoxRD_Group.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxRD_Group.Size = new System.Drawing.Size(335, 555);
            this.groupBoxRD_Group.TabIndex = 3;
            this.groupBoxRD_Group.TabStop = false;
            // 
            // panelButton
            // 
            this.panelButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelButton.Controls.Add(this.txtName);
            this.panelButton.Controls.Add(this.label4);
            this.panelButton.Controls.Add(this.btnClose);
            this.panelButton.Controls.Add(this.btnDeleteGroup);
            this.panelButton.Controls.Add(this.btnAddGroup);
            this.panelButton.Controls.Add(this.comboBoxGroup);
            this.panelButton.Controls.Add(this.label1);
            this.panelButton.Location = new System.Drawing.Point(2, 15);
            this.panelButton.Margin = new System.Windows.Forms.Padding(2);
            this.panelButton.Name = "panelButton";
            this.panelButton.Size = new System.Drawing.Size(331, 101);
            this.panelButton.TabIndex = 4;
            // 
            // txtName
            // 
            this.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtName.Location = new System.Drawing.Point(82, 72);
            this.txtName.Margin = new System.Windows.Forms.Padding(2);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(217, 20);
            this.txtName.TabIndex = 9;
            this.txtName.Visible = false;
            this.txtName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtName_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Search";
            this.label4.Visible = false;
            // 
            // btnDeleteGroup
            // 
            this.btnDeleteGroup.Enabled = false;
            this.btnDeleteGroup.Location = new System.Drawing.Point(118, 38);
            this.btnDeleteGroup.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeleteGroup.Name = "btnDeleteGroup";
            this.btnDeleteGroup.Size = new System.Drawing.Size(95, 26);
            this.btnDeleteGroup.TabIndex = 7;
            this.btnDeleteGroup.Text = "Delete";
            this.btnDeleteGroup.UseVisualStyleBackColor = true;
            this.btnDeleteGroup.Click += new System.EventHandler(this.btnDeleteGroup_Click);
            // 
            // comboBoxGroup
            // 
            this.comboBoxGroup.FormattingEnabled = true;
            this.comboBoxGroup.Location = new System.Drawing.Point(82, 13);
            this.comboBoxGroup.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxGroup.Name = "comboBoxGroup";
            this.comboBoxGroup.Size = new System.Drawing.Size(217, 21);
            this.comboBoxGroup.TabIndex = 4;
            this.comboBoxGroup.SelectedIndexChanged += new System.EventHandler(this.comboBoxGroup_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Group";
            // 
            // groupBoxRDGroup_Detail
            // 
            this.groupBoxRDGroup_Detail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxRDGroup_Detail.Controls.Add(this.flowLayoutPanelGroupDetail);
            this.groupBoxRDGroup_Detail.Location = new System.Drawing.Point(2, 120);
            this.groupBoxRDGroup_Detail.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxRDGroup_Detail.Name = "groupBoxRDGroup_Detail";
            this.groupBoxRDGroup_Detail.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxRDGroup_Detail.Size = new System.Drawing.Size(331, 433);
            this.groupBoxRDGroup_Detail.TabIndex = 8;
            this.groupBoxRDGroup_Detail.TabStop = false;
            // 
            // flowLayoutPanelGroupDetail
            // 
            this.flowLayoutPanelGroupDetail.Controls.Add(this.panelAddGroup);
            this.flowLayoutPanelGroupDetail.Controls.Add(this.panelDataGridViewGroupDetail);
            this.flowLayoutPanelGroupDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelGroupDetail.Location = new System.Drawing.Point(2, 15);
            this.flowLayoutPanelGroupDetail.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanelGroupDetail.Name = "flowLayoutPanelGroupDetail";
            this.flowLayoutPanelGroupDetail.Size = new System.Drawing.Size(327, 416);
            this.flowLayoutPanelGroupDetail.TabIndex = 6;
            // 
            // panelAddGroup
            // 
            this.panelAddGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelAddGroup.Controls.Add(this.progressBarGetMID);
            this.panelAddGroup.Controls.Add(this.cmbDatabase);
            this.panelAddGroup.Controls.Add(this.lblDatabase);
            this.panelAddGroup.Controls.Add(this.btnCancel);
            this.panelAddGroup.Controls.Add(this.btnUploadMidGroup);
            this.panelAddGroup.Controls.Add(this.label2);
            this.panelAddGroup.Controls.Add(this.txtMidFilename);
            this.panelAddGroup.Controls.Add(this.btnBrowseMidFilename);
            this.panelAddGroup.Controls.Add(this.txtGroupName);
            this.panelAddGroup.Controls.Add(this.label3);
            this.panelAddGroup.Location = new System.Drawing.Point(2, 2);
            this.panelAddGroup.Margin = new System.Windows.Forms.Padding(2);
            this.panelAddGroup.Name = "panelAddGroup";
            this.panelAddGroup.Size = new System.Drawing.Size(320, 139);
            this.panelAddGroup.TabIndex = 6;
            this.panelAddGroup.Visible = false;
            // 
            // progressBarGetMID
            // 
            this.progressBarGetMID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarGetMID.Location = new System.Drawing.Point(15, 17);
            this.progressBarGetMID.Margin = new System.Windows.Forms.Padding(2);
            this.progressBarGetMID.Name = "progressBarGetMID";
            this.progressBarGetMID.Size = new System.Drawing.Size(294, 19);
            this.progressBarGetMID.TabIndex = 10;
            this.progressBarGetMID.Visible = false;
            // 
            // cmbDatabase
            // 
            this.cmbDatabase.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDatabase.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDatabase.FormattingEnabled = true;
            this.cmbDatabase.Location = new System.Drawing.Point(93, 13);
            this.cmbDatabase.Name = "cmbDatabase";
            this.cmbDatabase.Size = new System.Drawing.Size(216, 21);
            this.cmbDatabase.TabIndex = 7;
            this.cmbDatabase.Visible = false;
            this.cmbDatabase.SelectedIndexChanged += new System.EventHandler(this.cmbDatabase_SelectedIndexChanged);
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.Location = new System.Drawing.Point(11, 2);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(226, 13);
            this.lblDatabase.TabIndex = 8;
            this.lblDatabase.Text = "Progress Get MID, please wait until it\'s finished";
            this.lblDatabase.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(188, 104);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(95, 26);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnUploadMidGroup
            // 
            this.btnUploadMidGroup.Location = new System.Drawing.Point(27, 104);
            this.btnUploadMidGroup.Margin = new System.Windows.Forms.Padding(2);
            this.btnUploadMidGroup.Name = "btnUploadMidGroup";
            this.btnUploadMidGroup.Size = new System.Drawing.Size(95, 26);
            this.btnUploadMidGroup.TabIndex = 5;
            this.btnUploadMidGroup.Text = "Upload MID List";
            this.btnUploadMidGroup.UseVisualStyleBackColor = true;
            this.btnUploadMidGroup.Click += new System.EventHandler(this.btnUploadMidGroup_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 42);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Group Name";
            // 
            // txtMidFilename
            // 
            this.txtMidFilename.Location = new System.Drawing.Point(59, 71);
            this.txtMidFilename.Margin = new System.Windows.Forms.Padding(2);
            this.txtMidFilename.Name = "txtMidFilename";
            this.txtMidFilename.ReadOnly = true;
            this.txtMidFilename.Size = new System.Drawing.Size(170, 20);
            this.txtMidFilename.TabIndex = 3;
            // 
            // btnBrowseMidFilename
            // 
            this.btnBrowseMidFilename.Location = new System.Drawing.Point(238, 67);
            this.btnBrowseMidFilename.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrowseMidFilename.Name = "btnBrowseMidFilename";
            this.btnBrowseMidFilename.Size = new System.Drawing.Size(70, 26);
            this.btnBrowseMidFilename.TabIndex = 4;
            this.btnBrowseMidFilename.Text = "Browse";
            this.btnBrowseMidFilename.UseVisualStyleBackColor = true;
            this.btnBrowseMidFilename.Click += new System.EventHandler(this.btnBrowseMidFilename_Click);
            // 
            // txtGroupName
            // 
            this.txtGroupName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGroupName.Location = new System.Drawing.Point(93, 39);
            this.txtGroupName.Margin = new System.Windows.Forms.Padding(2);
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Size = new System.Drawing.Size(217, 20);
            this.txtGroupName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 73);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "MID List";
            // 
            // panelDataGridViewGroupDetail
            // 
            this.panelDataGridViewGroupDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelDataGridViewGroupDetail.Controls.Add(this.dataGridViewGroupDetail);
            this.panelDataGridViewGroupDetail.Location = new System.Drawing.Point(2, 145);
            this.panelDataGridViewGroupDetail.Margin = new System.Windows.Forms.Padding(2);
            this.panelDataGridViewGroupDetail.Name = "panelDataGridViewGroupDetail";
            this.panelDataGridViewGroupDetail.Size = new System.Drawing.Size(320, 296);
            this.panelDataGridViewGroupDetail.TabIndex = 8;
            // 
            // dataGridViewGroupDetail
            // 
            this.dataGridViewGroupDetail.AllowUserToAddRows = false;
            this.dataGridViewGroupDetail.AllowUserToDeleteRows = false;
            this.dataGridViewGroupDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGroupDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewGroupDetail.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewGroupDetail.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewGroupDetail.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewGroupDetail.Name = "dataGridViewGroupDetail";
            this.dataGridViewGroupDetail.ReadOnly = true;
            this.dataGridViewGroupDetail.RowHeadersVisible = false;
            this.dataGridViewGroupDetail.RowTemplate.Height = 24;
            this.dataGridViewGroupDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewGroupDetail.Size = new System.Drawing.Size(320, 296);
            this.dataGridViewGroupDetail.TabIndex = 7;
            this.dataGridViewGroupDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewGroupDetail_CellContentClick);
            // 
            // backgroundWorkerGetMID
            // 
            this.backgroundWorkerGetMID.WorkerReportsProgress = true;
            this.backgroundWorkerGetMID.WorkerSupportsCancellation = true;
            this.backgroundWorkerGetMID.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerGetMID_DoWork);
            this.backgroundWorkerGetMID.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerGetMID_RunWorkerCompleted);
            // 
            // FrmRmtDownloadFtpsGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 574);
            this.Controls.Add(this.groupBoxRD_Group);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximumSize = new System.Drawing.Size(374, 612);
            this.Name = "FrmRmtDownloadFtpsGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FTPS Remote Download - Terminal Group";
            this.Load += new System.EventHandler(this.FrmRmtDownloadFtpsGroup_Load);
            this.groupBoxRD_Group.ResumeLayout(false);
            this.panelButton.ResumeLayout(false);
            this.panelButton.PerformLayout();
            this.groupBoxRDGroup_Detail.ResumeLayout(false);
            this.flowLayoutPanelGroupDetail.ResumeLayout(false);
            this.panelAddGroup.ResumeLayout(false);
            this.panelAddGroup.PerformLayout();
            this.panelDataGridViewGroupDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroupDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddGroup;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBoxRD_Group;
        private System.Windows.Forms.ComboBox comboBoxGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelAddGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMidFilename;
        private System.Windows.Forms.Button btnBrowseMidFilename;
        private System.Windows.Forms.TextBox txtGroupName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelGroupDetail;
        private System.Windows.Forms.Button btnDeleteGroup;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnUploadMidGroup;
        private System.Windows.Forms.DataGridView dataGridViewGroupDetail;
        private System.Windows.Forms.GroupBox groupBoxRDGroup_Detail;
        private System.Windows.Forms.Panel panelDataGridViewGroupDetail;
        private System.Windows.Forms.Panel panelButton;
        private System.Windows.Forms.ComboBox cmbDatabase;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.ProgressBar progressBarGetMID;
        private System.ComponentModel.BackgroundWorker backgroundWorkerGetMID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtName;
    }
}