﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmRmtDownloadFtpsGroup : Form
    {
        SqlConnection oSqlConn;
        string sUserID;
        SqlConnection oSqlConnAuditTrail;
        string sFilename = null;
        DataTable dtMID = null;
        OpenFileDialog openfileMID = new OpenFileDialog();

        protected string sDatabaseId = null;
        protected string sDatabaseName = null;
        CheckBox headerCheckBox = new CheckBox();
        string sCondition;
        bool bAdd = false;

        public FrmRmtDownloadFtpsGroup(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = oSqlConnAuditTrail = _oSqlConn;
            sUserID = _sUserID;
        }

        private void FrmRmtDownloadFtpsGroup_Load(object sender, EventArgs e)
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
            AutoHeightDisplay();
            label4.Visible = false;
            txtName.Visible = false;
            btnDeleteGroup.Enabled = false;
        }

        private void InitComboBoxDatabase()
        {
            DataTable dtDatabaseName = dtGetDatabase();
            if (dtDatabaseName.Rows.Count > 0)
            {
                cmbDatabase.DataSource = dtDatabaseName;
                cmbDatabase.DisplayMember = "DatabaseName";
                cmbDatabase.ValueMember = "DatabaseId";
            }
        }

        private DataTable dtGetDatabase()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);

            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();
            return dtTemp;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            backgroundWorkerGetMID.CancelAsync();
            Close();
        }

        private void btnAddGroup_Click(object sender, EventArgs e)
        {
            panelButton.Enabled = false;
            panelAddGroup.Visible = true;
            label4.Visible = false;
            txtName.Visible = false;
            bAdd = true;
            //InitComboBoxDatabase();
            AutoHeightDisplay();
            btnBrowseMidFilename.Enabled = false;
            btnUploadMidGroup.Enabled = false;
            btnCancel.Enabled = false;
            GetMID();
        }

        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.AppStarting;
            groupBoxRD_Group.Enabled = false;
            var confirmDialog = MessageBox.Show("Are you sure to delete this group & relation Group to Software ?","Confirm Delete!",MessageBoxButtons.YesNo);
            // execute disable RD_Group dan RD_GroupDetail
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            if (comboBoxGroup.SelectedItem != null)
            {
                string sGroupName = (string)((DataRowView)comboBoxGroup.SelectedItem).Row["GROUP_NAME"];
                //using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupEnableDisable, oSqlConn)) for only disable
                //{
                //    sqlcmd.CommandType = CommandType.StoredProcedure;
                //    sqlcmd.Parameters.Add("@sGroupName", SqlDbType.VarChar).Value = sGroupName;
                //    sqlcmd.ExecuteNonQuery();
                //}
                int iGroupID = (int)((DataRowView)comboBoxGroup.SelectedItem).Row["Group_ID"];
                if ((!string.IsNullOrEmpty(iGroupID.ToString())))
                {
                    if (confirmDialog == DialogResult.Yes)
                    {
                        using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupDelete, oSqlConn)) // revision for delete
                        {
                            sqlcmd.CommandType = CommandType.StoredProcedure;
                            sqlcmd.Parameters.Add("@iGroupID", SqlDbType.Int).Value = iGroupID;
                            sqlcmd.ExecuteNonQuery();
                            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Delete RD ftp Group : " + iGroupID.ToString()+" ; "+ sGroupName, "");
                        }
                        label4.Visible = false;
                        txtName.Visible = false;
                        btnDeleteGroup.Enabled = false;

                    }
                }
                else MessageBox.Show("Group is Empty");
            }
            //----------------------------------------
            groupBoxRD_Group.Enabled = true;
            this.Cursor = Cursors.Default;
            panelButton.Enabled = true;
            panelAddGroup.Visible = false;
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
            AutoHeightDisplay();
        }

        private void btnBrowseMidFilename_Click(object sender, EventArgs e)
        {
            openfileMID.Filter = "CSV File(*.csv)|*.csv";
            if (openfileMID.ShowDialog() == DialogResult.OK)
            {
                sFilename = openfileMID.FileName;
                txtMidFilename.Text = openfileMID.SafeFileName;
                Csv.Read(sFilename, ref dtMID);
                if (dtMID == null || dtMID.Rows.Count == 0)
                {
                    MessageBox.Show("No Records available. Please check file content");
                    sFilename = null;
                    txtMidFilename.Clear();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            panelButton.Enabled = true;
            panelAddGroup.Visible = false;
            bAdd = false;
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
            AutoHeightDisplay();
            progressBarGetMID.Visible = false;
            lblDatabase.Visible = false;
            backgroundWorkerGetMID.CancelAsync();
        }

        private static bool isRowGroupIDDetail(SqlConnection oSqlConn, int iGroupID)
        {
            bool sFlag=false;

            string sQuery = string.Format("select top 1 * from tbRD_GroupDetail Where Group_ID = {0}", "'" + iGroupID + "'");
            SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                using (DataTable dt = new DataTable())
                {
                    dt.Load(oRead);
                    if (dt.Rows.Count > 0)                        
                        sFlag = true;
                    else sFlag = false;
                }
                oRead.Close();
            }
            oCmd.Dispose();

            return sFlag;
        }
        private void DeleteGroupID(SqlConnection oSqlConn, int iGroupID)
        {
            string sQuery = string.Format("Delete tbRD_Group WHERE Group_ID={0}", iGroupID);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
            }
        }
        protected DataTable dtGetMIDDB(int iDatabaseID )
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPRD_GetMID, oSqlConn))
            {
                oCmd.CommandTimeout = 240;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = iDatabaseID;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected DataTable dtGetMID()
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPRD_GetMID, oSqlConn))
            {
                oCmd.CommandTimeout = 360;
                oCmd.CommandType = CommandType.StoredProcedure;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                oCmd.Dispose();
            }
            return dtTemp;
        }

        protected DataTable dtGetMIDTemp()
        {
                string sQuery = string.Format("select TerminalID,AcquirerName,MID,CreateDate from tbRD_GetMIDTemp WITH (NOLOCK)");
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                DataTable dtTemp = new DataTable();
                using (SqlCommand sqlcmd = new SqlCommand(sQuery, oSqlConn))
                {
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                        if (reader.HasRows)
                        {
                            dtTemp.Load(reader);
                            reader.Close();
                        }
                    sqlcmd.Dispose();
                }

                return dtTemp;
        }

        protected DataTable dtGetMIDDB()
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPRD_GetMID, oSqlConn))
            {
                oCmd.CommandTimeout = 1200;
                oCmd.CommandType = CommandType.StoredProcedure;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }

        protected bool IsDuplicateMIDGroup(string sGroup_Name)
        {
            if (bAdd)
            {
                string sQuery = string.Format("select top 1 * from tbRD_Group WHERE Group_Name='{0}' and Enabled={1}", sGroup_Name,"1");
                DataTable dtIDGroup = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlCommand sqlcmd = new SqlCommand(sQuery, oSqlConn))
                {
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                        if (reader.HasRows)
                        {
                            dtIDGroup.Load(reader);
                            var row = dtIDGroup.Select(string.Format("Group_Name='{0}'", sGroup_Name));
                            if (row != null && row.Length > 0) { reader.Close(); sqlcmd.Dispose(); return true; }
                            
                        }
                    
                }
            }
            return false;
        }
        private void GetMID()
        {
            progressBarGetMID.Visible = true;
            lblDatabase.Visible = true;
            progressBarGetMID.Style = ProgressBarStyle.Marquee;
            backgroundWorkerGetMID.RunWorkerAsync();
        }
        protected DataTable PopulateDataGridView(string stxtName)
        {
            DataTable dtTemp = new DataTable();
            dataGridViewGroupDetail.DataSource = null;
                int iGroupID = (int)((DataRowView)comboBoxGroup.SelectedItem).Row["GROUP_ID"];
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();

                using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupDetailBrowse, oSqlConn))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = $"WHERE a.[ENABLED]=1 AND a.GROUP_ID='{iGroupID}' AND (a.TERMINALID LIKE '%{stxtName}%' OR a.MID LIKE '%{stxtName}%' )";
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                        if (reader.HasRows)
                        {
                            dtTemp.Load(reader);
                            dataGridViewGroupDetail.DataSource = dtTemp;
                            reader.Close();
                        }
                    sqlcmd.Dispose();
                }
            return dtTemp;

        }

        protected bool IsDuplicateTerminalGroup(int _iGroupID,string _sTerminalID)
        {
            if (bAdd)
            {
                string sQuery = string.Format("select top 1 * from tbRD_GroupDetail WHERE Group_ID='{0}' and TerminalID='{1}' and Enabled={2}", _iGroupID, _sTerminalID, "1");
                DataTable dtIDGroup = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlCommand sqlcmd = new SqlCommand(sQuery, oSqlConn))
                {
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                        if (reader.HasRows)
                        {
                            dtIDGroup.Load(reader);
                            var row = dtIDGroup.Select(string.Format("Group_ID={0} and TerminalID = '{1}' and Enabled={2}", _iGroupID,_sTerminalID, "1"));
                            if (row != null && row.Length > 0) { reader.Close(); sqlcmd.Dispose(); return true; }
                            
                        }
                    
                }
            }
            return false;
        }
        private void btnUploadMidGroup_Click(object sender, EventArgs e)
        {
            string sGroupName = txtGroupName.Text;
            if (!string.IsNullOrEmpty(sGroupName) && !string.IsNullOrEmpty(sFilename))
            {

                // InitDataBase();
                //if (string.IsNullOrEmpty(sDatabaseId)) sDatabaseId = "0";

                //var confirmDialog = MessageBox.Show("Are you sure to Upload MID from Database : "+ sDatabaseName  +" ", "Confirm Upload !", MessageBoxButtons.YesNo);
                var confirmDialog = MessageBox.Show("Are you sure to Upload MID ?", "Confirm Upload !", MessageBoxButtons.YesNo);
                if (confirmDialog == DialogResult.Yes)
                {
                    this.Cursor = Cursors.AppStarting;
                    groupBoxRD_Group.Enabled = false;
                    if (!IsDuplicateMIDGroup(sGroupName))
                    {
                        int iGroupID = 0;
                        try
                        {
                            // add upload MID list process
                            SaveRd_Group(sGroupName, ref iGroupID);

                        //==========================================

                            //SaveRd_GroupDetail(iGroupID, int.Parse(sDatabaseId));

                            #region

                            string[] linesITM = System.IO.File.ReadAllLines(sFilename);
                            List<string> listcontent = new List<string>();
                            foreach (string line in linesITM)
                            {
                                string text = line;
                                int lengthText = text.Length;
                                if (lengthText == 15)
                                {
                                    listcontent.Add(text.Substring(0, 15).Trim());
                                }
                            }
                            DataTable tableMIDTemp = new DataTable();
                            tableMIDTemp = dtGetMIDTemp();
                            if (tableMIDTemp != null)
                            {
                                foreach (string sMID in listcontent)
                                {
                                    string sMIDKey = sMID.Substring(0, 6);
                                    if (sMIDKey == "000885")
                                        sCondition = string.Format("AcquirerName IN ('DEBIT', 'FLAZZBCA') AND MID='{0}'", sMID);
                                    else
                                    if (sMIDKey == "000005")
                                        sCondition = string.Format("AcquirerName IN ('BCA','BCA1','BCA2') AND MID='{0}'", sMID);

                                    DataRow[] rows = tableMIDTemp.Select(sCondition);
                                    if (rows.Count() > 0)
                                    {
                                        foreach (DataRow dr in rows)
                                        {
                                            string sRowTerminalID = dr["TerminalID"].ToString();
                                            string sRowMID = dr["MID"].ToString();
                                            if (sRowMID== sMID)
                                            if (!IsDuplicateTerminalGroup(iGroupID, sRowTerminalID))
                                                SaveRd_GroupDetailTerminal(sRowTerminalID, sRowMID, iGroupID);
                                            //else MessageBox.Show("MID for TerminalID this Group : " + sMID + " TerminalID : " + sRowTerminalID);
                                        }
                                    }
                                }
                            }
                            else MessageBox.Show("Please Click button Get MID");
                            #endregion
                            if (!isRowGroupIDDetail(oSqlConn, iGroupID))
                            { DeleteGroupID(oSqlConn, iGroupID); MessageBox.Show("Terminal ID Not Found"); }
                        }
                        catch (Exception ex)
                        {
                            CommonClass.doWriteErrorFile("Exception Upload MID Group : " + ex.Message);
                            if (iGroupID != 0) if (!isRowGroupIDDetail(oSqlConn, iGroupID)) DeleteGroupID(oSqlConn, iGroupID);
                            MessageBox.Show("Cannot Process, Please Contact Administrator");
                        }
                        // -------------------------------
                    }
                    else MessageBox.Show("the group name has been used, please fill in another group name");
                }
                groupBoxRD_Group.Enabled = true;
                this.Cursor = Cursors.Default;
                panelButton.Enabled = true;
                panelAddGroup.Visible = false;
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
                AutoHeightDisplay();
            }
            else MessageBox.Show("Please fill GroupName and choose MID Filename");

            label4.Visible = false;
            txtName.Visible = false;
            btnDeleteGroup.Enabled = false;
        }


        private void HeaderCheckBox_Clicked(object sender, EventArgs e)
        {
            //Necessary to end the edit mode of the Cell.
            dataGridViewGroupDetail.EndEdit();

            //Loop and check and uncheck all row CheckBoxes based on Header Cell CheckBox.
            foreach (DataGridViewRow row in dataGridViewGroupDetail.Rows)
            {
                DataGridViewCheckBoxCell checkBox = (row.Cells["ENABLED"] as DataGridViewCheckBoxCell);
                checkBox.Value = headerCheckBox.Checked;
            }
        }

        SqlDataAdapter sAdapter;
        SqlCommandBuilder sBuilder;
        DataSet sDS;
        DataTable  sTable;
        public static DataTable tableMID;
        private void comboBoxGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridViewGroupDetail.DataSource = null;
                ComboBox comboboxTemp = (ComboBox)sender;

                if (comboboxTemp != null && comboboxTemp.SelectedItem != null)
                {
                    int iGroupID = (int)((DataRowView)comboBoxGroup.SelectedItem).Row["GROUP_ID"];
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();

                    using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupDetailBrowse, oSqlConn))
                    {
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = $"WHERE a.[ENABLED]=1 AND a.GROUP_ID='{iGroupID}'";
                        using (SqlDataReader reader = sqlcmd.ExecuteReader())
                            if (reader.HasRows)
                            {
                                DataTable dtTemp = new DataTable();
                                dtTemp.Load(reader);

                                dataGridViewGroupDetail.DataSource = dtTemp;
                                //dataGridViewGroupDetail.ReadOnly = false;
                                //dataGridViewGroupDetail.EditMode = DataGridViewEditMode.EditOnKeystroke;
                                reader.Close();
                            }
                        sqlcmd.Dispose();
                    }

                    //if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    //using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupDetailBrowse, oSqlConn))
                    //{
                    //    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //    sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = $"WHERE a.GROUP_ID='{iGroupID}'";//for checklist custom update
                    //    sAdapter = new SqlDataAdapter(sqlcmd);
                    //    sBuilder = new SqlCommandBuilder(sAdapter);
                    //    sDS = new DataSet();
                    //    sAdapter.Fill(sDS, "tbRD_GroupDetail");
                    //    sTable = sDS.Tables["tbRD_GroupDetail"];
                    //    //oSqlConn.Close();

                    //    dataGridViewGroupDetail.DataSource = sDS.Tables["tbRD_GroupDetail"];
                    //    dataGridViewGroupDetail.ReadOnly = false;
                    //    //dataGridViewGroupDetail.CellBeginEdit += dataGridViewGroupDetail_CellBeginEdit;
                    //    dataGridViewGroupDetail.EditMode = DataGridViewEditMode.EditOnKeystroke;
                    //}

                    ////Add a CheckBox Column to the DataGridView Header Cell.

                    ////Find the Location of Header Cell.
                    //Point headerCellLocation = this.dataGridViewGroupDetail.GetCellDisplayRectangle(0, -1, true).Location;

                    ////Place the Header CheckBox in the Location of the Header Cell.
                    //headerCheckBox.Location = new Point(headerCellLocation.X + 8, headerCellLocation.Y + 2);
                    //headerCheckBox.BackColor = Color.White;
                    //headerCheckBox.Size = new Size(18, 18);

                    ////Assign Click event to the Header CheckBox.
                    //headerCheckBox.Click += new EventHandler(HeaderCheckBox_Clicked);
                    //dataGridViewGroupDetail.Controls.Add(headerCheckBox);

                    ////Add a CheckBox Column to the DataGridView at the first position.
                    //DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                    //checkBoxColumn.HeaderText = "";
                    //checkBoxColumn.Width = 30;
                    //checkBoxColumn.Name = "ENABLED";
                    //dataGridViewGroupDetail.Columns.Insert(0, checkBoxColumn);

                    ////Assign Click event to the DataGridView Cell.
                    //dataGridViewGroupDetail.CellContentClick += new DataGridViewCellEventHandler(dataGridViewGroupDetail_CellClick);
                    btnDeleteGroup.Enabled = true;
                    label4.Visible = true;
                    txtName.Visible = true;
                }
                else dataGridViewGroupDetail.DataSource = null;
            }
            catch(Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        protected void SaveRd_Group(string sGroupName, ref int iGroupID)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupInsert, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sGroupName", SqlDbType.VarChar).Value = sGroupName;
                sqlcmd.Parameters.Add("@iID", SqlDbType.Int).Direction = ParameterDirection.Output;

                sqlcmd.ExecuteNonQuery();

                iGroupID = (int)sqlcmd.Parameters["@iID"].Value;
            }
        }

        protected void InitDataBase()
        {
            sDatabaseId = cmbDatabase.SelectedValue.ToString();
            sDatabaseName = cmbDatabase.Text;
        }

        protected void SaveRd_GroupDetail(int iGroupID, int iDatabaseID)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            foreach (DataRow row in dtMID.Rows)
            {
                string sMID = row[0].ToString();
                if (!string.IsNullOrEmpty(sMID) && long.Parse(sMID) > 0)
                    using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupDetailInsert, oSqlConn))
                    {
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add("@iGroupID", SqlDbType.Int).Value = iGroupID;
                        sqlcmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = iDatabaseID;
                        sqlcmd.Parameters.Add("@sMID", SqlDbType.VarChar).Value = sMID;

                        sqlcmd.ExecuteNonQuery();
                    }
            }
        }

        protected void SaveRd_GroupDetailTerminal(string TerminalID, string MID, int iGroupID)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();            
                if (!string.IsNullOrEmpty(MID) && long.Parse(MID) > 0)
                    using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupDetailInsertTerminal, oSqlConn))
                    {
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add("@iGroupID", SqlDbType.Int).Value = iGroupID;
                        sqlcmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = TerminalID;
                        sqlcmd.Parameters.Add("@sMID", SqlDbType.VarChar).Value = MID;
                        sqlcmd.ExecuteNonQuery();
                        CommonClass.InputLog(oSqlConn, TerminalID,sUserID,"","Upload RD ftp Group : "+ iGroupID + ";" + TerminalID + ";" + MID, iGroupID+" "+TerminalID + " "+MID+" ");
                    }
        }

        protected void AutoHeightDisplay()
        {
            comboBoxGroup.SelectedIndex = -1;
            if (comboBoxGroup.Items.Count == 0)
            {
                dataGridViewGroupDetail.DataSource = null;
                comboBoxGroup.Text = null;
            }
            if (panelAddGroup.Visible) panelDataGridViewGroupDetail.Height = flowLayoutPanelGroupDetail.Height - panelAddGroup.Height - 6;
            else panelDataGridViewGroupDetail.Height = flowLayoutPanelGroupDetail.Height - 6;
        }

        private void dataGridViewGroupDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2 && e.RowIndex >= 0)
                if ((bool)this.dataGridViewGroupDetail.CurrentCell.Value == true)
                {
                    //Use index of TimeOut column
                    this.dataGridViewGroupDetail.Rows[e.RowIndex].Cells[2].Value = false;
                    this.dataGridViewGroupDetail.CommitEdit(DataGridViewDataErrorContexts.Commit);


                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    if (comboBoxGroup.SelectedItem != null)
                    {
                        string sGroupName = (string)((DataRowView)comboBoxGroup.SelectedItem).Row["GROUP_NAME"];
                        int iGroupID = (int)((DataRowView)comboBoxGroup.SelectedItem).Row["Group_ID"];
                        var confirmDialog = MessageBox.Show("Are you sure to delete Terminal ID " + this.dataGridViewGroupDetail.Rows[e.RowIndex].Cells[1].Value + " from this group " + sGroupName + "? ", "Confirm Delete!", MessageBoxButtons.YesNo);

                        if ((!string.IsNullOrEmpty(iGroupID.ToString())))
                        {
                            if (confirmDialog == DialogResult.Yes)
                            {
                                string sQuery = string.Format("delete tbRD_GroupDetail Where Group_ID = {0} and terminalid = {1}", "'" + iGroupID + "'", "'"+this.dataGridViewGroupDetail.Rows[e.RowIndex].Cells[1].Value+ "'");
                                
                                using (SqlCommand sqlcmd = new SqlCommand(sQuery, oSqlConn)) // revision for delete
                                {
                                    sqlcmd.ExecuteNonQuery();
                                    CommonClass.InputLog(oSqlConn, "", sUserID, "", "Delete Terminal ID " + this.dataGridViewGroupDetail.Rows[e.RowIndex].Cells[1].Value+ " RD ftp Group : " + iGroupID.ToString() + " ; " + sGroupName, "");
                                }
                                dataGridViewGroupDetail.DataSource = this.PopulateDataGridView("");
                            } else
                            {
                                this.dataGridViewGroupDetail.Rows[e.RowIndex].Cells[2].Value = true;
                                this.dataGridViewGroupDetail.CommitEdit(DataGridViewDataErrorContexts.Commit);
                            }

                        }
                        else MessageBox.Show("Group is Empty");
                    }


                    //Set other columns values
                }
                else
                {
                    //Use index of TimeOut columns
                    this.dataGridViewGroupDetail.Rows[e.RowIndex].Cells[2].Value = true;
                    this.dataGridViewGroupDetail.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    //Set other columns values
                }

            //int chkBoxColIdx = 2; //Index of the column with checkbox.
            //if (e.ColumnIndex == chkBoxColIdx)
            //{
            //    dataGridViewGroupDetail.Rows[e.RowIndex].Cells[chkBoxColIdx].Value = !(bool)dataGridViewGroupDetail.Rows[e.RowIndex].Cells[chkBoxColIdx].Value;
            //    //dataGridViewGroupDetail.Update();
            //}

        }

        private void dataGridViewGroupDetail_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            //if (this.dataGridViewGroupDetail.CurrentCell is DataGridViewCheckBoxCell)
            //{
            //    btnSave.Visible = true;
            //    this.sAdapter.Update(sTable);
            //    this.dataGridViewGroupDetail.EndEdit();
            //}
        }

        private void dataGridViewGroupDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //Check to ensure that the row CheckBox is clicked.
            //if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            //{
            //    //Loop to verify whether all row CheckBoxes are checked or not.
            //    bool isChecked = true;
            //    foreach (DataGridViewRow row in dataGridViewGroupDetail.Rows)
            //    {
            //        if (Convert.ToBoolean(row.Cells["ENABLED"].EditedFormattedValue) == false)
            //        {
            //            isChecked = false;
            //            break;
            //        }
            //    }
            //    headerCheckBox.Checked = isChecked;
            //}
        }

        private void dataGridViewGroupDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridView DataGridViewGroupDetail = (DataGridView)sender;
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridViewGroupDetail_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.RowIndex >=0 && e.ColumnIndex>=0)
            {
                this.dataGridViewGroupDetail.Tag = this.dataGridViewGroupDetail.CurrentCell.Value;
            }
        }

        private void btnGetMID_Click(object sender, EventArgs e)
        {

         }

        private void backgroundWorkerGetMID_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                tableMID = new DataTable();
                //tableMID = dtGetMID(int.Parse(sDatabaseId));
                tableMID = dtGetMID();
                MessageBox.Show("Get MID Success, please continue to Upload MID");
                btnBrowseMidFilename.Enabled = true;
                btnUploadMidGroup.Enabled = true;
            }
            catch (Exception x)
            {
                MessageBox.Show("Cannot Process Get MID, Please Contact Administrator");
            }
        }

        private void backgroundWorkerGetMID_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBarGetMID.Style = ProgressBarStyle.Blocks;
            progressBarGetMID.Visible = false;
            lblDatabase.Visible = false;
            btnCancel.Enabled = true;
        }

        private void cmbDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox comboboxTemp = (ComboBox)sender;

            //if (comboboxTemp != null && comboboxTemp.SelectedItem != null)
            //{
            //    int iDatabaseID = (int)((DataRowView)cmbDatabase.SelectedItem).Row["DatabaseID"];
            //}
        }

        private void txtName_KeyUp(object sender, KeyEventArgs e)
        {
            dataGridViewGroupDetail.DataSource = this.PopulateDataGridView(txtName.Text);
        }

        //private void btnSave_Click(object sender, EventArgs e)
        //{
        //    //this.dataGridViewGroupDetail.BindingContext[sTable].EndCurrentEdit();
        //    //this.sAdapter.Update(sTable);


        //    //this.dataGridViewGroupDetail.EndEdit();
        //    //this.dataGridViewGroupDetail.BeginEdit();
        //    //dataGridViewGroupDetail.EditMode = DataGridViewEditMode.EditOnKeystroke;
        //}
    }
}
