﻿namespace InSys
{
    partial class FrmRmtDownloadFtps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRmtDownloadFtps));
            this.backgroundWorkerUpload = new System.ComponentModel.BackgroundWorker();
            this.progressBarUploadFile = new System.Windows.Forms.ProgressBar();
            this.groupBoxSwVersionListFile = new System.Windows.Forms.GroupBox();
            this.btnUploadCancel = new System.Windows.Forms.Button();
            this.panelSwVersionList = new System.Windows.Forms.Panel();
            this.txtSwVersionName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkUpgradeGroup = new System.Windows.Forms.CheckBox();
            this.dataGridViewFilelist = new System.Windows.Forms.DataGridView();
            this.gridbtnAddSoftware = new System.Windows.Forms.DataGridViewButtonColumn();
            this.gridbtnRemSoftware = new System.Windows.Forms.DataGridViewButtonColumn();
            this.chkUpgradeAll = new System.Windows.Forms.CheckBox();
            this.comboBoxGroup = new System.Windows.Forms.ComboBox();
            this.btnUpload = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBoxSwVersion = new System.Windows.Forms.GroupBox();
            this.dataGridViewSwVersion = new System.Windows.Forms.DataGridView();
            this.btnDelSwVersion = new System.Windows.Forms.Button();
            this.btnEditSwVersion = new System.Windows.Forms.Button();
            this.btnAddSwVersion = new System.Windows.Forms.Button();
            this.groupBoxSwVersionListFile.SuspendLayout();
            this.panelSwVersionList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFilelist)).BeginInit();
            this.groupBoxSwVersion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSwVersion)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorkerUpload
            // 
            this.backgroundWorkerUpload.WorkerReportsProgress = true;
            this.backgroundWorkerUpload.WorkerSupportsCancellation = true;
            this.backgroundWorkerUpload.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerUpload_DoWork);
            this.backgroundWorkerUpload.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerUpload_RunWorkerCompleted);
            // 
            // progressBarUploadFile
            // 
            this.progressBarUploadFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarUploadFile.Location = new System.Drawing.Point(226, 12);
            this.progressBarUploadFile.Name = "progressBarUploadFile";
            this.progressBarUploadFile.Size = new System.Drawing.Size(837, 23);
            this.progressBarUploadFile.TabIndex = 0;
            // 
            // groupBoxSwVersionListFile
            // 
            this.groupBoxSwVersionListFile.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSwVersionListFile.Controls.Add(this.btnUploadCancel);
            this.groupBoxSwVersionListFile.Controls.Add(this.panelSwVersionList);
            this.groupBoxSwVersionListFile.Controls.Add(this.btnUpload);
            this.groupBoxSwVersionListFile.Location = new System.Drawing.Point(226, 41);
            this.groupBoxSwVersionListFile.Name = "groupBoxSwVersionListFile";
            this.groupBoxSwVersionListFile.Size = new System.Drawing.Size(837, 516);
            this.groupBoxSwVersionListFile.TabIndex = 2;
            this.groupBoxSwVersionListFile.TabStop = false;
            // 
            // btnUploadCancel
            // 
            this.btnUploadCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUploadCancel.Location = new System.Drawing.Point(712, 473);
            this.btnUploadCancel.Name = "btnUploadCancel";
            this.btnUploadCancel.Size = new System.Drawing.Size(109, 32);
            this.btnUploadCancel.TabIndex = 8;
            this.btnUploadCancel.Text = "Cancel";
            this.btnUploadCancel.UseVisualStyleBackColor = true;
            this.btnUploadCancel.Click += new System.EventHandler(this.btnUploadCancel_Click);
            // 
            // panelSwVersionList
            // 
            this.panelSwVersionList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSwVersionList.Controls.Add(this.txtSwVersionName);
            this.panelSwVersionList.Controls.Add(this.label1);
            this.panelSwVersionList.Controls.Add(this.chkUpgradeGroup);
            this.panelSwVersionList.Controls.Add(this.dataGridViewFilelist);
            this.panelSwVersionList.Controls.Add(this.chkUpgradeAll);
            this.panelSwVersionList.Controls.Add(this.comboBoxGroup);
            this.panelSwVersionList.Location = new System.Drawing.Point(3, 18);
            this.panelSwVersionList.Name = "panelSwVersionList";
            this.panelSwVersionList.Size = new System.Drawing.Size(831, 442);
            this.panelSwVersionList.TabIndex = 7;
            // 
            // txtSwVersionName
            // 
            this.txtSwVersionName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSwVersionName.Location = new System.Drawing.Point(131, 13);
            this.txtSwVersionName.MaxLength = 50;
            this.txtSwVersionName.Name = "txtSwVersionName";
            this.txtSwVersionName.Size = new System.Drawing.Size(193, 22);
            this.txtSwVersionName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Software Version";
            // 
            // chkUpgradeGroup
            // 
            this.chkUpgradeGroup.AutoSize = true;
            this.chkUpgradeGroup.Location = new System.Drawing.Point(239, 58);
            this.chkUpgradeGroup.Name = "chkUpgradeGroup";
            this.chkUpgradeGroup.Size = new System.Drawing.Size(148, 21);
            this.chkUpgradeGroup.TabIndex = 6;
            this.chkUpgradeGroup.Text = "Upgrade by Group";
            this.chkUpgradeGroup.UseVisualStyleBackColor = true;
            this.chkUpgradeGroup.CheckedChanged += new System.EventHandler(this.chkUpgradeGroup_CheckedChanged);
            // 
            // dataGridViewFilelist
            // 
            this.dataGridViewFilelist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewFilelist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFilelist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gridbtnAddSoftware,
            this.gridbtnRemSoftware});
            this.dataGridViewFilelist.Location = new System.Drawing.Point(12, 101);
            this.dataGridViewFilelist.MultiSelect = false;
            this.dataGridViewFilelist.Name = "dataGridViewFilelist";
            this.dataGridViewFilelist.ReadOnly = true;
            this.dataGridViewFilelist.RowHeadersVisible = false;
            this.dataGridViewFilelist.RowTemplate.Height = 24;
            this.dataGridViewFilelist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewFilelist.Size = new System.Drawing.Size(806, 330);
            this.dataGridViewFilelist.TabIndex = 2;
            this.dataGridViewFilelist.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFilelist_CellContentClick);
            // 
            // gridbtnAddSoftware
            // 
            this.gridbtnAddSoftware.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.gridbtnAddSoftware.HeaderText = "Add";
            this.gridbtnAddSoftware.Name = "gridbtnAddSoftware";
            this.gridbtnAddSoftware.ReadOnly = true;
            this.gridbtnAddSoftware.Text = "Add";
            this.gridbtnAddSoftware.UseColumnTextForButtonValue = true;
            this.gridbtnAddSoftware.Width = 39;
            // 
            // gridbtnRemSoftware
            // 
            this.gridbtnRemSoftware.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.gridbtnRemSoftware.HeaderText = "Del";
            this.gridbtnRemSoftware.Name = "gridbtnRemSoftware";
            this.gridbtnRemSoftware.ReadOnly = true;
            this.gridbtnRemSoftware.Text = "Del";
            this.gridbtnRemSoftware.UseColumnTextForButtonValue = true;
            this.gridbtnRemSoftware.Width = 35;
            // 
            // chkUpgradeAll
            // 
            this.chkUpgradeAll.AutoSize = true;
            this.chkUpgradeAll.Location = new System.Drawing.Point(12, 58);
            this.chkUpgradeAll.Name = "chkUpgradeAll";
            this.chkUpgradeAll.Size = new System.Drawing.Size(163, 21);
            this.chkUpgradeAll.TabIndex = 3;
            this.chkUpgradeAll.Text = "Upgrade All Terminal";
            this.chkUpgradeAll.UseVisualStyleBackColor = true;
            this.chkUpgradeAll.CheckedChanged += new System.EventHandler(this.chkUpgradeAll_CheckedChanged);
            // 
            // comboBoxGroup
            // 
            this.comboBoxGroup.Enabled = false;
            this.comboBoxGroup.FormattingEnabled = true;
            this.comboBoxGroup.Location = new System.Drawing.Point(404, 56);
            this.comboBoxGroup.Name = "comboBoxGroup";
            this.comboBoxGroup.Size = new System.Drawing.Size(242, 24);
            this.comboBoxGroup.TabIndex = 5;
            // 
            // btnUpload
            // 
            this.btnUpload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpload.Location = new System.Drawing.Point(583, 473);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(109, 32);
            this.btnUpload.TabIndex = 0;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(6, 507);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(188, 32);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBoxSwVersion
            // 
            this.groupBoxSwVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxSwVersion.Controls.Add(this.dataGridViewSwVersion);
            this.groupBoxSwVersion.Controls.Add(this.btnDelSwVersion);
            this.groupBoxSwVersion.Controls.Add(this.btnEditSwVersion);
            this.groupBoxSwVersion.Controls.Add(this.btnAddSwVersion);
            this.groupBoxSwVersion.Controls.Add(this.btnClose);
            this.groupBoxSwVersion.Location = new System.Drawing.Point(12, 7);
            this.groupBoxSwVersion.Name = "groupBoxSwVersion";
            this.groupBoxSwVersion.Size = new System.Drawing.Size(200, 550);
            this.groupBoxSwVersion.TabIndex = 3;
            this.groupBoxSwVersion.TabStop = false;
            // 
            // dataGridViewSwVersion
            // 
            this.dataGridViewSwVersion.AllowUserToAddRows = false;
            this.dataGridViewSwVersion.AllowUserToDeleteRows = false;
            this.dataGridViewSwVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridViewSwVersion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSwVersion.ColumnHeadersVisible = false;
            this.dataGridViewSwVersion.Location = new System.Drawing.Point(3, 18);
            this.dataGridViewSwVersion.Name = "dataGridViewSwVersion";
            this.dataGridViewSwVersion.ReadOnly = true;
            this.dataGridViewSwVersion.RowHeadersVisible = false;
            this.dataGridViewSwVersion.RowTemplate.Height = 24;
            this.dataGridViewSwVersion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSwVersion.Size = new System.Drawing.Size(194, 446);
            this.dataGridViewSwVersion.TabIndex = 5;
            this.dataGridViewSwVersion.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSwVersion_CellClick);
            // 
            // btnDelSwVersion
            // 
            this.btnDelSwVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelSwVersion.Location = new System.Drawing.Point(140, 473);
            this.btnDelSwVersion.Name = "btnDelSwVersion";
            this.btnDelSwVersion.Size = new System.Drawing.Size(54, 28);
            this.btnDelSwVersion.TabIndex = 4;
            this.btnDelSwVersion.Text = "Del";
            this.btnDelSwVersion.UseVisualStyleBackColor = true;
            this.btnDelSwVersion.Click += new System.EventHandler(this.btnDelSwVersion_Click);
            // 
            // btnEditSwVersion
            // 
            this.btnEditSwVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditSwVersion.Location = new System.Drawing.Point(73, 473);
            this.btnEditSwVersion.Name = "btnEditSwVersion";
            this.btnEditSwVersion.Size = new System.Drawing.Size(54, 28);
            this.btnEditSwVersion.TabIndex = 3;
            this.btnEditSwVersion.Text = "Edit";
            this.btnEditSwVersion.UseVisualStyleBackColor = true;
            this.btnEditSwVersion.Click += new System.EventHandler(this.btnEditSwVersion_Click);
            // 
            // btnAddSwVersion
            // 
            this.btnAddSwVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddSwVersion.Location = new System.Drawing.Point(6, 473);
            this.btnAddSwVersion.Name = "btnAddSwVersion";
            this.btnAddSwVersion.Size = new System.Drawing.Size(54, 28);
            this.btnAddSwVersion.TabIndex = 2;
            this.btnAddSwVersion.Text = "Add";
            this.btnAddSwVersion.UseVisualStyleBackColor = true;
            this.btnAddSwVersion.Click += new System.EventHandler(this.btnAddSwVersion_Click);
            // 
            // FrmRmtDownloadFtps
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 569);
            this.Controls.Add(this.groupBoxSwVersion);
            this.Controls.Add(this.groupBoxSwVersionListFile);
            this.Controls.Add(this.progressBarUploadFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1093, 616);
            this.Name = "FrmRmtDownloadFtps";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Upload Software to FTPS";
            this.Load += new System.EventHandler(this.FrmRmtDownloadFtps_Load);
            this.groupBoxSwVersionListFile.ResumeLayout(false);
            this.panelSwVersionList.ResumeLayout(false);
            this.panelSwVersionList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFilelist)).EndInit();
            this.groupBoxSwVersion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSwVersion)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorkerUpload;
        private System.Windows.Forms.ProgressBar progressBarUploadFile;
        private System.Windows.Forms.GroupBox groupBoxSwVersionListFile;
        private System.Windows.Forms.DataGridView dataGridViewFilelist;
        private System.Windows.Forms.TextBox txtSwVersionName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkUpgradeAll;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.CheckBox chkUpgradeGroup;
        private System.Windows.Forms.ComboBox comboBoxGroup;
        private System.Windows.Forms.GroupBox groupBoxSwVersion;
        private System.Windows.Forms.Panel panelSwVersionList;
        private System.Windows.Forms.Button btnDelSwVersion;
        private System.Windows.Forms.Button btnEditSwVersion;
        private System.Windows.Forms.Button btnAddSwVersion;
        private System.Windows.Forms.DataGridView dataGridViewSwVersion;
        private System.Windows.Forms.Button btnUploadCancel;
        private System.Windows.Forms.DataGridViewButtonColumn gridbtnAddSoftware;
        private System.Windows.Forms.DataGridViewButtonColumn gridbtnRemSoftware;
    }
}