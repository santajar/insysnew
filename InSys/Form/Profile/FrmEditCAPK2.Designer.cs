﻿namespace InSys
{
    partial class FrmEditCAPK2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditCAPK2));
            this.gbItemCAPK = new System.Windows.Forms.GroupBox();
            this.txtPubAlgo = new System.Windows.Forms.TextBox();
            this.txtPubHash = new System.Windows.Forms.TextBox();
            this.txtPubExp = new System.Windows.Forms.TextBox();
            this.txtPubKeyData = new System.Windows.Forms.TextBox();
            this.txtPubRid = new System.Windows.Forms.TextBox();
            this.txtPubIdx = new System.Windows.Forms.TextBox();
            this.lblPubAlgo = new System.Windows.Forms.Label();
            this.lblPubHash = new System.Windows.Forms.Label();
            this.lblPubExp = new System.Windows.Forms.Label();
            this.lblPubKeyData = new System.Windows.Forms.Label();
            this.lblPubRid = new System.Windows.Forms.Label();
            this.lblPubIdx = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbItemCAPK.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbItemCAPK
            // 
            this.gbItemCAPK.Controls.Add(this.txtPubAlgo);
            this.gbItemCAPK.Controls.Add(this.txtPubHash);
            this.gbItemCAPK.Controls.Add(this.txtPubExp);
            this.gbItemCAPK.Controls.Add(this.txtPubKeyData);
            this.gbItemCAPK.Controls.Add(this.txtPubRid);
            this.gbItemCAPK.Controls.Add(this.txtPubIdx);
            this.gbItemCAPK.Controls.Add(this.lblPubAlgo);
            this.gbItemCAPK.Controls.Add(this.lblPubHash);
            this.gbItemCAPK.Controls.Add(this.lblPubExp);
            this.gbItemCAPK.Controls.Add(this.lblPubKeyData);
            this.gbItemCAPK.Controls.Add(this.lblPubRid);
            this.gbItemCAPK.Controls.Add(this.lblPubIdx);
            this.gbItemCAPK.Location = new System.Drawing.Point(12, 12);
            this.gbItemCAPK.Name = "gbItemCAPK";
            this.gbItemCAPK.Size = new System.Drawing.Size(349, 298);
            this.gbItemCAPK.TabIndex = 0;
            this.gbItemCAPK.TabStop = false;
            // 
            // txtPubAlgo
            // 
            this.txtPubAlgo.Location = new System.Drawing.Point(100, 272);
            this.txtPubAlgo.MaxLength = 2;
            this.txtPubAlgo.Name = "txtPubAlgo";
            this.txtPubAlgo.Size = new System.Drawing.Size(235, 20);
            this.txtPubAlgo.TabIndex = 12;
            // 
            // txtPubHash
            // 
            this.txtPubHash.Location = new System.Drawing.Point(100, 232);
            this.txtPubHash.MaxLength = 40;
            this.txtPubHash.Multiline = true;
            this.txtPubHash.Name = "txtPubHash";
            this.txtPubHash.Size = new System.Drawing.Size(235, 35);
            this.txtPubHash.TabIndex = 11;
            // 
            // txtPubExp
            // 
            this.txtPubExp.Location = new System.Drawing.Point(100, 206);
            this.txtPubExp.MaxLength = 5;
            this.txtPubExp.Name = "txtPubExp";
            this.txtPubExp.Size = new System.Drawing.Size(235, 20);
            this.txtPubExp.TabIndex = 10;
            // 
            // txtPubKeyData
            // 
            this.txtPubKeyData.Location = new System.Drawing.Point(100, 64);
            this.txtPubKeyData.MaxLength = 512;
            this.txtPubKeyData.Multiline = true;
            this.txtPubKeyData.Name = "txtPubKeyData";
            this.txtPubKeyData.Size = new System.Drawing.Size(235, 136);
            this.txtPubKeyData.TabIndex = 9;
            // 
            // txtPubRid
            // 
            this.txtPubRid.Location = new System.Drawing.Point(100, 38);
            this.txtPubRid.MaxLength = 10;
            this.txtPubRid.Name = "txtPubRid";
            this.txtPubRid.Size = new System.Drawing.Size(235, 20);
            this.txtPubRid.TabIndex = 8;
            // 
            // txtPubIdx
            // 
            this.txtPubIdx.Location = new System.Drawing.Point(100, 13);
            this.txtPubIdx.MaxLength = 5;
            this.txtPubIdx.Name = "txtPubIdx";
            this.txtPubIdx.Size = new System.Drawing.Size(235, 20);
            this.txtPubIdx.TabIndex = 7;
            // 
            // lblPubAlgo
            // 
            this.lblPubAlgo.AutoSize = true;
            this.lblPubAlgo.Location = new System.Drawing.Point(6, 275);
            this.lblPubAlgo.Name = "lblPubAlgo";
            this.lblPubAlgo.Size = new System.Drawing.Size(28, 13);
            this.lblPubAlgo.TabIndex = 6;
            this.lblPubAlgo.Text = "Algo";
            // 
            // lblPubHash
            // 
            this.lblPubHash.AutoSize = true;
            this.lblPubHash.Location = new System.Drawing.Point(6, 235);
            this.lblPubHash.Name = "lblPubHash";
            this.lblPubHash.Size = new System.Drawing.Size(32, 13);
            this.lblPubHash.TabIndex = 5;
            this.lblPubHash.Text = "Hash";
            // 
            // lblPubExp
            // 
            this.lblPubExp.AutoSize = true;
            this.lblPubExp.Location = new System.Drawing.Point(6, 209);
            this.lblPubExp.Name = "lblPubExp";
            this.lblPubExp.Size = new System.Drawing.Size(52, 13);
            this.lblPubExp.TabIndex = 4;
            this.lblPubExp.Text = "Exponent";
            // 
            // lblPubKeyData
            // 
            this.lblPubKeyData.AutoSize = true;
            this.lblPubKeyData.Location = new System.Drawing.Point(6, 67);
            this.lblPubKeyData.Name = "lblPubKeyData";
            this.lblPubKeyData.Size = new System.Drawing.Size(47, 13);
            this.lblPubKeyData.TabIndex = 3;
            this.lblPubKeyData.Text = "Modulus";
            // 
            // lblPubRid
            // 
            this.lblPubRid.AutoSize = true;
            this.lblPubRid.Location = new System.Drawing.Point(6, 41);
            this.lblPubRid.Name = "lblPubRid";
            this.lblPubRid.Size = new System.Drawing.Size(26, 13);
            this.lblPubRid.TabIndex = 2;
            this.lblPubRid.Text = "RID";
            // 
            // lblPubIdx
            // 
            this.lblPubIdx.AutoSize = true;
            this.lblPubIdx.Location = new System.Drawing.Point(6, 16);
            this.lblPubIdx.Name = "lblPubIdx";
            this.lblPubIdx.Size = new System.Drawing.Size(33, 13);
            this.lblPubIdx.TabIndex = 1;
            this.lblPubIdx.Text = "Index";
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(12, 316);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(349, 46);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(260, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(179, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmEditCAPK2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(379, 370);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbItemCAPK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmEditCAPK2";
            this.Load += new System.EventHandler(this.FrmEditCAPK2_Load);
            this.gbItemCAPK.ResumeLayout(false);
            this.gbItemCAPK.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbItemCAPK;
        private System.Windows.Forms.TextBox txtPubAlgo;
        private System.Windows.Forms.TextBox txtPubHash;
        private System.Windows.Forms.TextBox txtPubExp;
        private System.Windows.Forms.TextBox txtPubKeyData;
        private System.Windows.Forms.TextBox txtPubRid;
        private System.Windows.Forms.TextBox txtPubIdx;
        private System.Windows.Forms.Label lblPubAlgo;
        private System.Windows.Forms.Label lblPubHash;
        private System.Windows.Forms.Label lblPubExp;
        private System.Windows.Forms.Label lblPubKeyData;
        private System.Windows.Forms.Label lblPubRid;
        private System.Windows.Forms.Label lblPubIdx;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}