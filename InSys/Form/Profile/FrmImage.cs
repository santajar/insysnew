﻿using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;
using System.IO;

namespace InSys
{
    public partial class FrmImage : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        public string sUID;
        protected string sLogoPrintIdlePath;
        protected string sLogoPrintIdleFilename;
        protected string sLogoIdleIdlePath;
        protected string sLogoIdleIdleFilename;
        protected string sType;

        public FrmImage(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sUID, string _sType)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUID = _sUID;
            sType = _sType;
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofdBrowseLogoPrint = new OpenFileDialog();
                ofdBrowseLogoPrint.Filter = "Bitmaps(*.bmp)|*.bmp";
                //ofdAppPackage.InitialDirectory = CommonVariable.PathMyComputer;
                ofdBrowseLogoPrint.ShowDialog();
                if (!string.IsNullOrEmpty(ofdBrowseLogoPrint.FileName))
                {
                    txtBrowse.Text = sLogoPrintIdlePath = sGetAppPackagePath(ofdBrowseLogoPrint.FileName);
                    sLogoPrintIdleFilename = sGetAppPackageFilename(ofdBrowseLogoPrint.FileName);
                    Image imgNew = Image.FromFile(ofdBrowseLogoPrint.FileName);
                    if (ofdBrowseLogoPrint.FileName.Substring(ofdBrowseLogoPrint.FileName.Length - 4, 4).ToLower() == ".bmp")
                    {
                        if (sType == "Print")
                        {
                            if (IsGrayScale(imgNew))
                            {
                                //if (imgNew.Width > 384 || imgNew.Height < 64 || imgNew.Height > 96 || imgNew.Height % 8 != 0)
                                //MessageBox.Show("Max Size Print Logo, Height : (64-96) % 8 = 0, Width : 384");
                                if (imgNew.Width > 384 )  
                                MessageBox.Show("Max Size Print Logo Width : 384");
                                else
                                    SetDisplayImage(imgNew);
                            }
                            else
                            {
                                txtBrowse.Text = "";
                                MessageBox.Show("Print Logo Not Allow Color Image");
                            }
                        }
                        //else
                        if (sType == "Idle")
                        {
                            if (IsGrayScale(imgNew))
                            {
                              //  if (imgNew.Width > 128 || imgNew.Height > 44)
                                if (imgNew.Width > 128)
                                {
                                    txtBrowse.Text = "";
                                    //MessageBox.Show("Max Size Print Logo, Height : 44, Width : 128");
                                    MessageBox.Show("Max Size Print Logo Width : 128");
                                }
                                else
                                    SetDisplayImage(imgNew);
                            }
                            else
                            {
                                //if (imgNew.Width > 328 || imgNew.Height > 168)
                                    if (imgNew.Width > 328)
                                {
                                    txtBrowse.Text = "";
                                    //MessageBox.Show("Max Size Print Logo, Height : 168, Width : 328");
                                    MessageBox.Show("Max Size Print Logo Width : 328");
                                }
                                else
                                    SetDisplayImage(imgNew);
                            }
                        }


                        if (sType == "Merchant")
                        {
                            if (IsGrayScale(imgNew))
                            {
                                //if (imgNew.Width > 384 || imgNew.Height < 64 || imgNew.Height > 96 || imgNew.Height % 8 != 0)
                                //MessageBox.Show("Max Size Print Logo, Height : (64-96) % 8 = 0, Width : 384");
                                if (imgNew.Width > 384)
                                    MessageBox.Show("Max Size Print Merchant Width : 384");
                                else
                                    SetDisplayImage(imgNew);
                            }
                            else
                            {
                                txtBrowse.Text = "";
                                MessageBox.Show("Print Merchant Not Allow Color Image");
                            }
                        }


                        if (sType == "Receipt")
                        {
                            if (IsGrayScale(imgNew))
                            {
                                //if (imgNew.Width > 384 || imgNew.Height < 64 || imgNew.Height > 96 || imgNew.Height % 8 != 0)
                                //MessageBox.Show("Max Size Print Logo, Height : (64-96) % 8 = 0, Width : 384");
                                if (imgNew.Width > 384)
                                    MessageBox.Show("Max Size Print Receipt Width : 384");
                                else
                                    SetDisplayImage(imgNew);
                            }
                            else
                            {
                                txtBrowse.Text = "";
                                MessageBox.Show("Print Receipt Not Allow Color Image");
                            }
                        }



                    }
                    else
                    {
                        txtBrowse.Text = "";
                        MessageBox.Show("Image invalid Format. Please select Bitmap Image only.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static unsafe bool IsGrayScale(Image image)
        {
            using (var bmp = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb))
            {
                using (var g = Graphics.FromImage(bmp))
                {
                    g.DrawImage(image, 0, 0);
                }

                var data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);

                var pt = (int*)data.Scan0;
                var res = true;

                for (var i = 0; i < data.Height * data.Width; i++)
                {
                    var color = Color.FromArgb(pt[i]);

                    if (color.A != 0 && (color.R != color.G || color.G != color.B))
                    {
                        res = false;
                        break;
                    }
                }

                bmp.UnlockBits(data);

                return res;
            }
        }

        private void SetDisplayImage(Image imgNew)
        {
            pbImage.Image = imgNew;
            btnAdd.Enabled = true;
            btnDelete.Enabled = false;
            lbImage.Enabled = false;
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            bool isExist = isFileExist();
            bool isHasSpace= sLogoPrintIdleFilename.Contains(" ");

            if (!string.IsNullOrEmpty(txtBrowse.Text))
            {
                if (sLogoPrintIdleFilename.Length <= 15)
                {
                    if (!isHasSpace)
                    {
                        if (!isExist)
                        {
                                AddContentPackage();
                        }
                        else
                            MessageBox.Show("Image File Name Already Exist. Please Rename Image File Name");
                    }
                    else
                        MessageBox.Show("Image File Name can not have space. Please Rename Image File Name");
                }
                else
                {
                    MessageBox.Show("Image name Length cannot more than 15");
                }
            }
            else
            {
                MessageBox.Show("Please Choose Logo First.");
            }
        }

        private void AddContentPackage()
        {
            SaveLogoPrintIdle();
            btnAdd.Enabled = false;
            btnDelete.Enabled = true;
            txtBrowse.Text = "";
            pbImage.Image = null;
            lbImage.Enabled = true;
            LoadData();
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lbImage.SelectedIndex != -1)
            {
                if (!isFileInUsed(lbImage.SelectedItem.ToString()))
                {
                    DeleteImage(lbImage.SelectedItem.ToString());
                    LoadData();
                }
                else
                    MessageBox.Show("Can't Delete. Image in Use.");
            }
            else
                MessageBox.Show("Please Choose Image first");
        }

        private bool isFileExist()
        {
            bool isAllow = false;
            using (SqlCommand oCmd = new SqlCommand(string.Format("SELECT DBO.isImageFileExist('{0}')", sLogoPrintIdleFilename), oSqlConn))
            {
                DataTable dtTemp = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                oSqlConn.Close();
            }
            return isAllow;
        }

        private bool isFileInUsed(string sFileName)
        {
            bool isAllow = false;
            using (SqlCommand oCmd = new SqlCommand(string.Format("SELECT DBO.isImageFileInUsed('{0}')", sFileName), oSqlConn))
            {
                DataTable dtTemp = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                oSqlConn.Close();
            }
            return isAllow;
        }


        /// <summary>
        /// Get the path where application packaged are saved
        /// </summary>
        /// <param name="sFullname">string : path name</param>
        /// <returns>string : Path of App Package</returns>
        protected string sGetAppPackagePath(string sFullname)
        {
            return sFullname.Substring(0, sFullname.LastIndexOf(@"\"));
        }

        /// <summary>
        /// Get the name of selected application package
        /// </summary>
        /// <param name="sFullname"></param>
        /// <returns></returns>
        protected string sGetAppPackageFilename(string sFullname)
        {
            return sFullname.Substring(sFullname.LastIndexOf(@"\") + 1);
        }

        private void LoadData()
        {
            string sQuery = "";
            if (sType == "Print")
            {
                this.Text = "Logo Print";
                sQuery = "SELECT [FileName] FROM tbProfileImage WHERE LogoType = 'Print'";
            }
            //else
            if (sType == "Idle")
            {
                this.Text = "Logo Idle";
                sQuery = "SELECT [FileName] FROM tbProfileImage WHERE LogoType = 'Idle'";
            }
            if (sType == "Merchant")
            {
                this.Text = "Logo Merchant";
                sQuery = "SELECT [FileName] FROM tbProfileImage WHERE LogoType = 'Merchant'";
            }
            if (sType == "Receipt")
            {
                this.Text = "Logo Merchant";
                sQuery = "SELECT [FileName] FROM tbProfileImage WHERE LogoType = 'Receipt'";
            }
            lbImage.Items.Clear();
            SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            SqlDataReader oDataReader = oCmd.ExecuteReader();
            if (oDataReader.HasRows)
            {
                while (oDataReader.Read())
                {
                    lbImage.Items.Add(oDataReader[0].ToString());
                }
            }
            oDataReader.Close();
        }

        private void DeleteImage(string _sFileName)
        {
            try
            {
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPImagePackageDeleteContent, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sFileName", SqlDbType.VarChar).Value = _sFileName;
                oCmd.Parameters.Add("@sType", SqlDbType.VarChar).Value = sType;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                oCmd.ExecuteNonQuery();
                pbImage.Image = null;
                CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, "", "Delete Image successfully",
                       string.Format("Delete Image: {0}, Type: {1}", _sFileName, sType));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadImage(string _sFilename)
        {
            try
            {
                string sHexImage = "";
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPImagePackageBrowseContent, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sFileName", SqlDbType.VarChar).Value = _sFilename;
                oCmd.Parameters.Add("@sType", SqlDbType.VarChar).Value = sType;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                SqlDataReader oDataReader = oCmd.ExecuteReader();
                if (oDataReader.HasRows)
                {
                    while (oDataReader.Read())
                    {
                        sHexImage = sHexImage + oDataReader[4].ToString();
                    }
                }
                oDataReader.Close();
                byte[] bImage = CommonLib.HexStringToByteArray(sHexImage);
                pbImage.InitialImage = null;
                pbImage.Image = byteArrayToImage(bImage);
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SaveLogoPrintIdle()
        {
            SaveImage(imageToByteArray(pbImage.Image));
        }

        private void SaveImage(byte[] _arrbImage)
        {
            try
            {
                int iSuccess = 0;
                int iImageID = 0;
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPImagePackageAddContent, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sFileName", SqlDbType.VarChar).Value = sLogoPrintIdleFilename;
                oCmd.Parameters.Add("@sType", SqlDbType.VarChar).Value = sType;

                oCmd.Parameters.Add("@iSuccess", SqlDbType.Int).Value = iSuccess;
                oCmd.Parameters["@iSuccess"].Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@iImageId", SqlDbType.Int).Value = iImageID;
                oCmd.Parameters["@iImageId"].Direction = ParameterDirection.Output;

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                oCmd.ExecuteNonQuery();

                iSuccess = (int)oCmd.Parameters["@iSuccess"].Value;
                if (!string.IsNullOrEmpty(oCmd.Parameters["@iImageID"].Value.ToString()) || oCmd.Parameters["@iImageID"].Value.ToString().Length > 0)
                    iImageID = (int)oCmd.Parameters["@iImageID"].Value;

                SaveImageTemp(_arrbImage, iImageID);

                if (iSuccess == 0)
                    MessageBox.Show("Duplicate Image Name. Please choose other file or rename it.");
                else
                    CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, "", "Image saved successfully",
                        string.Format("Save Image from {0}", sLogoPrintIdlePath));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SaveImageTemp(byte[] _arrbImage, int iImageID)
        {
            string sContent = CommonLib.sByteArrayToHexString(_arrbImage).Replace(" ", "");

            DataTable dtImageTemp = new DataTable();
            dtImageTemp = dtGetImageTemp(sContent, iImageID);
            SqlBulkCopy sqlBulk = new SqlBulkCopy(oSqlConn);
            sqlBulk.DestinationTableName = string.Format("tbProfileImageTemp");
            SqlBulkCopyColumnMapping mapID = new SqlBulkCopyColumnMapping("ID", "ID");
            SqlBulkCopyColumnMapping mapImageID = new SqlBulkCopyColumnMapping("ImageID", "ImageID");
            SqlBulkCopyColumnMapping mapImageContent = new SqlBulkCopyColumnMapping("ImageContent", "ImageContent");
            sqlBulk.ColumnMappings.Add(mapID);
            sqlBulk.ColumnMappings.Add(mapImageID);
            sqlBulk.ColumnMappings.Add(mapImageContent);
            sqlBulk.WriteToServer(dtImageTemp);
        }

        private DataTable dtGetImageTemp(string sContent, int iImageID)
        {
            DataSet dsTemp = new DataSet();
            DataTable dtTemp = new DataTable();
            string sQuery = string.Format("SELECT ID, ImageID, ImageContent FROM tbProfileImageTemp WHERE ImageID={0}", iImageID);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
                (new SqlDataAdapter(oCmd)).Fill(dsTemp);
            dtTemp = dsTemp.Tables[0].Copy();
            int iOffset = 0;
            int iLength = 800 * 2;
            int iId = 0;
            while (iOffset < sContent.Length)
            {
                DataRow row = dtTemp.NewRow();
                row["ID"] = iId++;
                row["ImageID"] = iImageID;
                row["ImageContent"] = sContent.Substring(iOffset,
                    iOffset + iLength < sContent.Length ? iLength : sContent.Length - iOffset);
                dtTemp.Rows.Add(row);
                iOffset += iLength;
            }
            return dtTemp;
        }

        public byte[] imageToByteArray(Image iImage)
        {
            MemoryStream oMS = new MemoryStream();
            iImage.Save(oMS, System.Drawing.Imaging.ImageFormat.Bmp);
            return oMS.ToArray();
        }

        public Image byteArrayToImage(byte[] bArrayImage)
        {
            MemoryStream ms = new MemoryStream(bArrayImage);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void FrmImage_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void lbImage_Click(object sender, EventArgs e)
        {
            ListBox lb = sender as ListBox;
            if (lb.SelectedIndex != -1)
            {
                pbImage.Image = null;
                lbImage.SelectedIndex = lb.SelectedIndex;
                LoadImage(lbImage.SelectedItem.ToString());
            }
        }

        private void lbImage_SelectedIndexChanged(object sender, EventArgs e)
        {
            //LoadImage();
        }

    }
}
