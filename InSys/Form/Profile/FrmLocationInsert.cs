using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmLocationInsert : Form
    {
        protected SqlConnection oSqlConn;
        protected DataTable oDataTable;
        protected string sUser;       

        /// <summary>
        /// Form to insert or delete new Location to database
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sUser">string : current active user</param>
        public FrmLocationInsert(SqlConnection _oSqlConn, string _sUser)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            sUser = _sUser;
        }

        private void FrmLocationInsert_Load(object sender, EventArgs e)
        {
            //InitDisplay();
            InitDgUserView();
            CommonClass.InputLog(oSqlConn, "", sUser, "", CommonMessage.sFormOpened + this.Text, "");
        }

        private void FrmLocationInsert_Activated(object sender, EventArgs e)
        {
            //CommonClass.doMinimizeChild(this.ParentForm.MdiChildren, this.Name);
            //InitDisplay();
        }

        private void btnAddLocation_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                AddLocation();
                //AddLocationDesc();
                InitDgUserView();
                //InitDisplay();
                CommonClass.InputLog(oSqlConn, "", sUser, "", "Insert New Location : "+sLocationID, "");
            }
            else
                MessageBox.Show("New ID and New Location are required.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);                            
        }        

        #region "Function"
        protected string sLocationDesc { get { return txtNewLocation.Text; } }
        protected string sLocationID { get { return txtIDLocation.Text; } }
       /* protected void InitDisplay()
        {
            lvLocationDesc.Items.Clear();
            CommonClass.FillListView(oSqlConn, CommonSP.sSPLocationBrowse, "", "LocationDesc",ref lvLocationDesc);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.WindowState = FormWindowState.Normal; 
        }
        */ 

        /// <summary>
        /// Bound DataGridView with DataTable
        /// </summary>
        public void InitDgUserView()
        {
            oDataTable = CommonClass.FilldgView(oSqlConn, CommonSP.sSPLocationBrowse, ref dgUserViewLocation);
            dgUserViewLocation.DataSource = oDataTable;
        }

        /// <summary>
        /// Determine whether data entered valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValid()
        {
            return (!string.IsNullOrEmpty(txtIDLocation.Text) && !string.IsNullOrEmpty(txtNewLocation.Text));
        //    return (!string.IsNullOrEmpty(txtNewLocation.Text)) ? true : false;
        }
        
        /// <summary>
        /// Insert new Location to database
        /// </summary>
        protected void AddLocation()
        {
            string sErrMsg = "";
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPLocationInsert, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;

            if (CommonClass.isFormatValid(sLocationID, "A"))
            {
                oSqlCmd.Parameters.Add("@sLocationID", SqlDbType.VarChar).Value = sLocationID;
                oSqlCmd.Parameters.Add("@sLocationDesc", SqlDbType.VarChar).Value = sLocationDesc;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                SqlDataReader oReader = oSqlCmd.ExecuteReader();
                if (oReader.Read())
                {
                    sErrMsg = oReader[0].ToString();
                    
                }

                oReader.Close();
                oReader.Dispose();
                oSqlCmd.Dispose();
            }
            else
            {
                sErrMsg = "Location ID Combination AlphaNumeric";
               
            }

            if (!string.IsNullOrEmpty(sErrMsg))
            {
                MessageBox.Show(sErrMsg);
                txtIDLocation.Clear();
                txtIDLocation.Focus();
            }
        }

        private void dgUserViewLocation_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgUserViewLocation.Columns["ColDelete"].Index)
                if (dgUserViewLocation.RowCount > 0)
                    if (CommonClass.isYesMessage(CommonMessage.sConfirmationDelete, CommonMessage.sConfirmationTitle))
                    {
                        CommonClass.InputLog(oSqlConn, "", sUser, "", "Delete Location : " + sGetLocationID(), "");
                        DeleteLocationDesc();
                    }
        }

        /// <summary>
        /// Get Location ID
        /// </summary>
        /// <returns></returns>
        protected string sGetLocationID()
        {
            return dgUserViewLocation["LocationID", dgUserViewLocation.CurrentRow.Index].Value.ToString();
        }

        /// <summary>
        /// Delete Location data from database
        /// </summary>
        protected void DeleteLocationDesc()
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPLocationDelete, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sLocationID", SqlDbType.VarChar).Value = sGetLocationID();
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            oSqlCmd.ExecuteNonQuery();
            oSqlCmd.Dispose();

            InitDgUserView();
        }
        /*     
                protected void AddLocationDesc()
                {
                    SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPLocationInsert, oSqlConn);
                    oSqlCmd.CommandType = CommandType.StoredProcedure;            
                    oSqlCmd.Parameters.Add("@sLocationDesc", SqlDbType.VarChar).Value = sLocationDesc;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    oSqlCmd.ExecuteNonQuery();
                    oSqlCmd.Dispose();
                }
          */ 
        #endregion
    }
}