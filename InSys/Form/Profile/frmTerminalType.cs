﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using InSysClass;

namespace InSys
{
    public partial class FrmTerminalType : Form
    {
        protected SqlConnection oSqlConn;
        int isTrue = 0;
        string sTerminalType;
        string sTerminalDescription;
        string sTerminalBrand;
        public FrmTerminalType(SqlConnection _oSqlConn)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalBrandBrowse, " ", "Name", "Name", ref cmbTerminalBrand);
            //fillcmbTerminalBrand();
            loaddgvTerminalType();
            SetViewTerminalType(isTrue);
        }

        private void SetViewTerminalType(int isTrue)
        {
            if (isTrue == 1)
            {
                dgvTerminalType.Enabled = false;
                txtTerminalType.Enabled = true;
                txtTypeDescription.Enabled = true;
                cmbTerminalBrand.Enabled = true;
                btnAdd.Enabled = true;
                btnCancel.Enabled = true;
            }
            else { 
                dgvTerminalType.Enabled = true;
                txtTerminalType.Enabled = false;
                txtTypeDescription.Enabled = false;
                cmbTerminalBrand.Enabled = false;
                btnAdd.Enabled = false;
                btnCancel.Enabled = false;
                txtTerminalType.Text = "";
                txtTypeDescription.Text = "";
                cmbTerminalBrand.SelectedIndex = -1;
            }
        }

        private void loaddgvTerminalType()
        {
            DataTable dtTerminalType = new DataTable();
            SqlDataAdapter AdaptTerminalTypeBrowse = new SqlDataAdapter(CommonSP.sSPTerminalTypeBrowse, oSqlConn);
            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }
            AdaptTerminalTypeBrowse.Fill(dtTerminalType);
            dgvTerminalType.DataSource = dtTerminalType;     
            
        }

        //private void fillcmbTerminalBrand()
        //{
        //    SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPBrandBrowse, oSqlConn);
        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
        //    if (oSqlConn.State != ConnectionState.Open)
        //    {
        //        oSqlConn.Close();
        //        oSqlConn.Open();
        //    }
        //    SqlDataReader oRead = oSqlCmd.ExecuteReader();
        //    DataTable dtTemp = new DataTable();
        //    if (oRead.HasRows)
        //        dtTemp.Load(oRead);
        //    for (int i = 0; i < dtTemp.Rows.Count; i++)
        //    {
        //        cmbTerminalBrand.Items.Add(dtTemp.Rows[i][1].ToString());
        //    }
        //    //cmbTerminalBrand.Items.Add = dtTemp.Rows[0][""].ToString();
            
            
        //}

        private void btnAdd_Click(object sender, EventArgs e)
        {
            sTerminalType = txtTerminalType.Text;
            sTerminalDescription = txtTypeDescription.Text;
            
            if (TypeNameisExist(sTerminalType))
            {
                if (!string.IsNullOrEmpty(txtTerminalType.Text) && !string.IsNullOrEmpty(txtTypeDescription.Text) && cmbTerminalBrand.SelectedIndex != -1)
                {   
                    sTerminalBrand = cmbTerminalBrand.SelectedValue.ToString();
                    insertTerminalType(sTerminalType, sTerminalDescription, sTerminalBrand);
                    loaddgvTerminalType();
                    isTrue = 0;
                    SetViewTerminalType(isTrue);

                }
                else MessageBox.Show("Please Complete all Field.");
            }
            else MessageBox.Show("Terminal Type Name already exist.");
        }

        private bool TypeNameisExist(string sTerminalType)
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalTypeBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionTypeName(sTerminalType);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataAdapter AdaptTypeName = new SqlDataAdapter(oSqlCmd);
            DataTable dtTypeName = new DataTable();
            AdaptTypeName.Fill(dtTypeName);
            if (dtTypeName.Rows.Count == 0)
            {
                return true;
            }
            else return false;
        }

        private object sConditionTypeName(string sTerminalType)
        {
            return string.Format(" where TerminalTypeName = '{0}'", sTerminalType);
        }

        private void insertTerminalType(string sTerminalType, string sTerminalDescription, string sTerminalBrand)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalTypeInsert, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = sTerminalType;
                oSqlCmd.Parameters.Add("@sDescription", SqlDbType.VarChar).Value = sTerminalDescription;
                oSqlCmd.Parameters.Add("@sBrand", SqlDbType.VarChar).Value = sTerminalBrand;
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void cmsAdd_Click(object sender, EventArgs e)
        {
            isTrue = 1;
            SetViewTerminalType(isTrue);
        }

        private void cmsDelete_Click(object sender, EventArgs e)
        {
            if (IsRowSelected())
            {
                DeleteTerminaType(iSelectedTerminalTypeId, sSelectedTerminalTypename, sSelectedTerminalTypeBrand);
                loaddgvTerminalType();
            }
        }

        private void DeleteTerminaType(int iSelectedTerminalTypeId, string sSelectedTerminalTypename, string sSelectedTerminalTypeBrand)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalTypeDelete, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@iId", SqlDbType.VarChar).Value = iSelectedTerminalTypeId;
                oSqlCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = sSelectedTerminalTypename;
                oSqlCmd.Parameters.Add("@sBrand", SqlDbType.VarChar).Value = sSelectedTerminalTypeBrand;
                
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private bool IsRowSelected()
        {
            return dgvTerminalType.SelectedRows.Count > 0 ? true : false;
        }

        protected int iSelectedTerminalTypeId
        {
            get
            {
                int iRow = dgvTerminalType.SelectedRows[0].Index;
                return int.Parse(dgvTerminalType["Id", iRow].Value.ToString());
            }
        }
        protected string sSelectedTerminalTypename
        {
            get
            {
                int iRow = dgvTerminalType.SelectedRows[0].Index;
                return dgvTerminalType["Name", iRow].Value.ToString();
            }
        }
        protected string sSelectedTerminalTypeBrand
        {
            get
            {
                int iRow = dgvTerminalType.SelectedRows[0].Index;
                return dgvTerminalType["Brand", iRow].Value.ToString();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            isTrue = 0;
            SetViewTerminalType(isTrue);
        }

    }
}
