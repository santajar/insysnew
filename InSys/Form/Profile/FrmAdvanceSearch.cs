﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmAdvanceSearch : Form
    {
        protected SqlConnection oSqlConn;
        protected DataTable dt;
        protected DataTable oDataTable;
        
        public string sTagValue1;
        public string sTagValue2;
        string sDbID;

        public string sParameter1;
        public string sValue1;
        public string sParameter2;
        public string sValue2;
        string sFormIDValue1;
        int sFormIDValue1Int = 0;
        string sFormIDValue2;
        int sFormIDValue2Int = 0;
        public FrmExpand frmExpand { get; set; } //allow form AdvanceSearch to acces component from form FrmExpand

        SqlCommand sqlcmdItemList = new SqlCommand();
        public FrmAdvanceSearch(SqlConnection _oSqlConn, string _sDbID, SqlCommand _sqlcmd)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            sDbID = _sDbID;
            sqlcmdItemList = _sqlcmd;
            sqlcmdItemList.Connection = oSqlConn;
        }

        private void AdvanceSearch_Load(object sender, EventArgs e)
        {
            ///begin fill combobox
            //dt = new DataTable();
            //if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            //using(SqlDataReader readerItemList = sqlcmdItemList.ExecuteReader())
            //    if (readerItemList.HasRows)
            //        dt.Load(readerItemList);

            //cbxparameter.DataSource = dt;
            //cbxparameter.DisplayMember = "ItemName";
            //cbxparameter.ValueMember = "Tag";
            btnSearch.Visible = false;
            btnRemove.Visible = false;
        }

        private void btnOkAdd_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count >= 3)
            {
                MessageBox.Show("Max Rows Count Are 2 Rows");
            }
            else
            {
                if (dataGridView1.Rows.Count < 2)
                {
                    btnSearch.Visible = true;
                    btnRemove.Visible = true;
                    btnSearch.Select();
                    sTagValue1 = cbxparameter.SelectedValue.ToString();
                    SqlDataAdapter daformid = new SqlDataAdapter(("select FormId from tbItemList Where Tag='" + sTagValue1 + "' AND DatabaseID='" + sDbID + "'"), oSqlConn);
                    DataTable dtformid = new DataTable();
                    daformid.Fill(dtformid);
                    sFormIDValue1 = dtformid.Rows[0][0].ToString();
                    sFormIDValue1Int = Int32.Parse(sFormIDValue1);
                    string sparameter = cbxparameter.Text;
                    string svalue = txtvalue.Text;
                    string[] rowstring = { sparameter, svalue };
                    dataGridView1.Rows.Add(rowstring);
                    txtvalue.Clear();
                    cbxparameter.Focus();
                }
                else
                {
                    sTagValue2 = cbxparameter.SelectedValue.ToString();
                    SqlDataAdapter daformid2 = new SqlDataAdapter(("select FormId from tbItemList Where Tag='" + sTagValue2 + "' AND DatabaseID='" + sDbID + "'"), oSqlConn);
                    DataTable dtformid2 = new DataTable();
                    daformid2.Fill(dtformid2);

                    sFormIDValue2 = dtformid2.Rows[0][0].ToString();
                    sFormIDValue2Int = Int32.Parse(sFormIDValue2);
                    string sparameter = cbxparameter.Text;
                    string sValue1 = txtvalue.Text;
                    string[] rowstring = { sparameter, sValue1 };
                    dataGridView1.Rows.Add(rowstring);
                    txtvalue.Clear();
                    cbxparameter.Focus();
                }
            }
        }

        int RowCount;
        //protected void sGetDataRow(string sRow0Col0, string sRow0Col1, string sRow1Col0, string sRow1Col1)
        protected void sGetDataRow()
        {
            if (RowCount <= 2)
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    sParameter1 = row.DataGridView.Rows[0].Cells[0].Value.ToString();
                    sValue1 = row.DataGridView.Rows[0].Cells[1].Value.ToString();
                }
            else
                foreach (DataGridViewRow row2 in dataGridView1.Rows)
                {
                    sParameter1 = row2.DataGridView.Rows[0].Cells[0].Value.ToString();
                    sValue1 = row2.DataGridView.Rows[0].Cells[1].Value.ToString();
                    sParameter2 = row2.DataGridView.Rows[1].Cells[0].Value.ToString();
                    sValue2 = row2.DataGridView.Rows[1].Cells[1].Value.ToString();
                }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            dt.Clear();
            RowCount = dataGridView1.Rows.Count;
            //sGetDataRow("''", "' '", "' '", "' '");
            sGetDataRow();
            LoadDataAdvance();
            dataGridView1.Rows.Clear();
            txtvalue.Clear();
            this.Close();
        }



        protected void LoadDataAdvance()
        {
            //InitTreeAndListView();
            frmExpand.tvTerminal.Nodes.Clear();
            frmExpand.dgvTerminal.DataSource = null;
            frmExpand.dgvTagDetail.DataSource = null;
            GetListTerminalAdvance();
            FillListTerminalAdvance();
        }

        //protected void InitTreeAndListView()
        //{
        //    frmExpand.tvTerminal.Nodes.Clear();
        //    frmExpand.dgvTerminal.DataSource = null;
        //    frmExpand.dgvTagDetail.DataSource = null;
        //}
        
        protected void GetListTerminalAdvance()
        {
            using (SqlCommand cmd = new SqlCommand("spProfileTerminalListBrowseAdvanceSearch", oSqlConn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@sFormId1", SqlDbType.Int).Value = sFormIDValue1Int;
                cmd.Parameters.Add("@sFormId2", SqlDbType.Int).Value = sFormIDValue2Int;
                cmd.Parameters.Add("@sParameter1", SqlDbType.VarChar).Value = sTagValue1;
                cmd.Parameters.Add("@sParameter2", SqlDbType.VarChar).Value = sTagValue2;
                cmd.Parameters.Add("@sValue1", SqlDbType.VarChar).Value = sValue1;
                cmd.Parameters.Add("@sValue2", SqlDbType.VarChar).Value = sValue2;
                cmd.Parameters.Add("@sDbId", SqlDbType.VarChar).Value = sDbID;
                cmd.ExecuteNonQuery();

                bool isGetData = true;
                SqlDataAdapter sDT = new SqlDataAdapter(cmd);
                DataTable DTid = new DataTable();
                sDT.Fill(DTid);
                dt = new DataTable();
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                dt.Clear();
                dt.Load(cmd.ExecuteReader());
                dt.PrimaryKey = new DataColumn[] { dt.Columns["TerminalID"] };
                if (dt.Rows.Count == 0)
                {
                    isGetData = false;
                    MessageBox.Show("Empty Database");
                }
            }

        }

        protected void FillListTerminalAdvance()
        {
            frmExpand.tvTerminal.Nodes.Clear();
            frmExpand.odtTerminal = dt;
            frmExpand.dgvTerminal.DataSource = null;
            frmExpand.dgvTerminal.ClearSelection();
            frmExpand.dgvTerminal.DataSource = dt;
            frmExpand.odtTerminal = dt;
            //frmExpand.dgvTerminal.ClearSelection();
            for (int iCol = 0; iCol < frmExpand.dgvTerminal.Columns.Count; iCol++)
                if (frmExpand.dgvTerminal.Columns[iCol].Name != "TerminalID")
                    frmExpand.dgvTerminal.Columns[iCol].Visible = false;
        }

        protected string sDbConditionsAdvance(int iMode)
        {
            if (iMode == 0) return "Where DatabaseId =" + sDbID;
            else return "Where '" + sParameter1 + "' LIKE '%" + sValue1 + "%' AND DatabaseID = " + sDbID;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 1)
            {
                MessageBox.Show("Grid Null, cannot run process remove");
            }
            else
            {
                dataGridView1.Rows.RemoveAt(0);
            }
        }
        
        private void txtvalue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOkAdd_Click(this, new EventArgs());
            }
        }

        private void cmbFrmType_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetTagItemList();
        }

        protected string sFormID()
        {
            string sForm = "";
            string sFormType = cmbFrmType.Text;
            switch (sFormType)
            {
                case "Terminal":
                    sForm = "1";
                    break;
                case "Acquirer":
                    sForm = "2";
                    break;
                case "Issuer":
                    sForm = "3";
                    break;
                default:
                    sForm = "";
                    break;
            }
            return sForm;
        }

        protected void GetTagItemList()
        {
            dt = new DataTable();
            string sQuery = string.Format("SELECT Tag, ItemName FROM tbItemList WHERE DatabaseID = {0} AND FormID = {1}", sDbID,  sFormID());
            SqlCommand cmd = new SqlCommand(sQuery, oSqlConn);
            cmd.CommandType = CommandType.Text;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            dt.Clear();
            dt.Load(cmd.ExecuteReader());
            cbxparameter.DataSource = dt;
            cbxparameter.DisplayMember = "ItemName";
            cbxparameter.ValueMember = "Tag";

        }
    }
}