﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmViewHistory : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        public string sUserID;
        string sTerminalID;

        protected DataTable dtTerminalHistoryData;
        protected DataTable dtAcquirerHistoryData;
        protected DataTable dtIssuerHistoryData;
        protected DataTable dtRelationHistoryData;

        protected DataTable dtListDate;

        class Relations
        {
            public string sAcquirerName { get; set; }
            public string sIssuerName { get; set; }
            public string sCardName { get; set; }
        }
        List<Relations> ltRelations = new List<Relations>();

        string[] arrsSortedAcquirer;

        public FrmViewHistory(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sUID, string _sTerminalID )
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUserID = _sUID;
            sTerminalID = _sTerminalID;
            InitializeComponent();
        }

        private void frmViewHistory_Load(object sender, EventArgs e)
        {
            SetDisplay();
            LoadDate();
        }

        private void SetDisplay()
        {
            this.Text = string.Format("View History - {0}", sTerminalID);
        }

        private void LoadDate()
        {
            dtListDate = GetListDate();
            if (dtListDate != null && dtListDate.Rows.Count > 0)
            {
                cmbDate.DisplayMember = "LastUpdate";
                cmbDate.ValueMember = "LastUpdate";
                cmbDate.SelectedIndex = -1;
                cmbDate.DataSource = dtListDate;
                
            }
            
        }

        /// <summary>
        /// Get List Date from table Terminal,Acquirer,Issuer and relation
        /// </summary>
        /// <returns></returns>
        private DataTable GetListDate()
        {
            DataTable odtTemp= new DataTable();
            odtTemp.Columns.Add("LastUpdate", typeof(String));

            GetlistDateTerminal(odtTemp);
            GetlistDateAcquirer(odtTemp);
            GetlistDateIssuer(odtTemp);
            GetlistDateRelation(odtTemp);

            return odtTemp;
        }

        private void GetlistDateRelation(DataTable odtTemp)
        {
            DataRow drRow;
            SqlCommand ocmd = new SqlCommand(string.Format("SELECT DISTINCT LastUpdate FROM tbProfileRelationHistory WHERE TerminalID = '{0}' ", sTerminalID), oSqlConn);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlDataReader oRead = ocmd.ExecuteReader())
            {
                if (oRead.HasRows)
                {
                    DataTable dt = new DataTable();
                    dt.Load(oRead);
                    foreach (DataRow row in dt.Rows)
                    {
                        DataRow[] drFind = odtTemp.Select("LastUpdate = '" + row["LastUpdate"].ToString() + "'");
                        if (drFind.Length == 0)
                        {
                            drRow = odtTemp.NewRow();
                            drRow[0] = row["LastUpdate"].ToString();
                            odtTemp.Rows.Add(drRow);
                        }
                    }
                }
                oRead.Close();
            }
        }


        private void GetlistDateIssuer(DataTable odtTemp)
        {
            DataRow drRow;
            SqlCommand ocmd = new SqlCommand(string.Format("SELECT LastUpdate FROM tbProfileIssuerHistory WHERE TerminalID = '{0}' AND IssuerTag IN ('AE01','AE001') ", sTerminalID), oSqlConn);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlDataReader oRead = ocmd.ExecuteReader())
            {
                if (oRead.HasRows)
                {
                    DataTable dt = new DataTable();
                    dt.Load(oRead);
                    foreach (DataRow row in dt.Rows)
                    {
                        DataRow[] drFind = odtTemp.Select("LastUpdate = '" + row["LastUpdate"].ToString() + "'");
                        if (drFind.Length == 0)
                        {
                            drRow = odtTemp.NewRow();
                            drRow[0] = row["LastUpdate"].ToString();
                            odtTemp.Rows.Add(drRow);
                        }
                    }
                }
                oRead.Close();
            }
        }

        private void GetlistDateAcquirer(DataTable odtTemp)
        {
            DataRow drRow;
            SqlCommand ocmd = new SqlCommand(string.Format("SELECT LastUpdate FROM tbProfileAcquirerHistory WHERE TerminalID = '{0}' AND AcquirerTag IN ('AA01','AA001')  ", sTerminalID), oSqlConn);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlDataReader oRead = ocmd.ExecuteReader())
            {
                if (oRead.HasRows)
                {
                    DataTable dt = new DataTable();
                    dt.Load(oRead);
                    foreach (DataRow row in dt.Rows)
                    {
                        DataRow[] drFind = odtTemp.Select("LastUpdate = '" + row["LastUpdate"].ToString() + "'");
                        if (drFind.Length == 0)
                        {
                            drRow = odtTemp.NewRow();
                            drRow[0] = row["LastUpdate"].ToString();
                            odtTemp.Rows.Add(drRow);
                        }
                    }
                }
                oRead.Close();
            }
        }

        private void GetlistDateTerminal(DataTable odtTemp)
        {
            DataRow drRow;
            SqlCommand ocmd = new SqlCommand(string.Format("SELECT LastUpdate FROM tbProfileTerminalHistory WHERE TerminalID = '{0}'  AND TerminalTag IN ('DE01','DE001')", sTerminalID), oSqlConn);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlDataReader oRead = ocmd.ExecuteReader())
            {
                if (oRead.HasRows)
                {
                    DataTable dt = new DataTable();
                    dt.Load(oRead);
                    foreach (DataRow row in dt.Rows)
                    {
                        DataRow[] drFind = odtTemp.Select("LastUpdate = '" + row["LastUpdate"].ToString() + "'");
                        if (drFind.Length == 0)
                        {
                            drRow = odtTemp.NewRow();
                            drRow[0] = row["LastUpdate"].ToString();
                            odtTemp.Rows.Add(drRow);
                        }
                    }
                }
                oRead.Close();
            }
        }

        private void cmbDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDate.SelectedIndex > -1 && !string.IsNullOrEmpty(cmbDate.SelectedValue.ToString()))
            {
                LoadHistoryTerminal(sTerminalID);
                LoadHistoryAcquirer(sTerminalID);
                LoadHistoryIssuer(sTerminalID);
                LoadHistoryRelation(sTerminalID);
            }
        }

        private void LoadHistoryRelation(string sTerminalID)
        {
            if (cmbDate.SelectedIndex > -1 && !string.IsNullOrEmpty(cmbDate.SelectedValue.ToString()))
            {
                dtRelationHistoryData = GetHistoryRelationData();
                //dgvRelation.DataSource = dtRelationHistoryData;
                tvRelation.Nodes.Clear();
                if (dtRelationHistoryData.Rows.Count > 0 )
                    FillTreeTerminal(true);
            }
        }

        /// <summary>
        /// Fill Tree View with ProfileRelation data from database.
        /// </summary>
        /// <param name="isTree">boolean : determines if current node is Terminal node</param>
        protected void FillTreeTerminal(bool isTree)
        {
            ltRelations.Clear();
            int iJmlAcq = (dtRelationHistoryData.Rows.Count / 3);
            //sArrRelations = new string[iJmlAcq][];

            int iIndexRow = 0;
            for (int iCount = 0; iCount < iJmlAcq; iCount++)
            {
                Relations tempRelation = new Relations();
                //sArrRelations[iCount] = new string[3];
                for (int iIndexArr = 0; iIndexArr < 3; iIndexArr++)
                {
                    //tempRelation.FillValue(iIndexArr, odtRelation.Rows[iIndexRow]["RelationTagValue"].ToString());
                    //sArrRelations[iCount][iIndexArr] = oDataTableRelation.Rows[iIndexRow]["RelationTagValue"].ToString();
                    switch (iIndexArr)
                    {
                        case 0: tempRelation.sCardName = dtRelationHistoryData.Rows[iIndexRow]["RelationTagValue"].ToString(); break;
                        case 1: tempRelation.sIssuerName = dtRelationHistoryData.Rows[iIndexRow]["RelationTagValue"].ToString(); break;
                        case 2: tempRelation.sAcquirerName = dtRelationHistoryData.Rows[iIndexRow]["RelationTagValue"].ToString(); break;
                    }
                    iIndexRow++;
                }
                ltRelations.Add(tempRelation);
            }

            TreeNode oNodeTerminal = tvRelation.Nodes.Add(sTerminalID);
            if (isTree)
            {
                AddAcqNode(oNodeTerminal);

                int iTotalAcquirer = oNodeTerminal.Nodes.Count;
                arrsSortedAcquirer = new string[iTotalAcquirer];
                for (int i = 0; i < iTotalAcquirer; i++)
                    arrsSortedAcquirer[i] = oNodeTerminal.Nodes[i].Name;
            }
            oNodeTerminal.Expand();
        }

        /// <summary>
        /// Add Acquirer node to Tree View
        /// </summary>
        /// <param name="oNodeParent">TreeNode : Parent's node for current Acquirer Node</param>
        protected void AddAcqNode(TreeNode oNodeParent)
        {
            TreeNode oNodeAcq;
            foreach (Relations tempRelation in ltRelations)
                if (!oNodeParent.Nodes.ContainsKey(tempRelation.sAcquirerName))
                {
                    oNodeAcq = oNodeParent.Nodes.Add(tempRelation.sAcquirerName, tempRelation.sAcquirerName);
                    AddIssuerNode(oNodeAcq, tempRelation.sAcquirerName);
                }
        }

        /// <summary>
        /// Add Issuer node to Tree View
        /// </summary>
        /// <param name="oNodeParent">TreeNode : Parent's node for current Issuer Node</param>
        /// <param name="sAcq">string : Acquire's name for current Issuer</param>
        protected void AddIssuerNode(TreeNode oNodeParent, string sAcq)
        {
            TreeNode oNodeIss;
            foreach (Relations tempRelation in ltRelations)
                if (!string.IsNullOrEmpty(tempRelation.sIssuerName) && !oNodeParent.Nodes.ContainsKey(tempRelation.sIssuerName))
                    if (sAcq == tempRelation.sAcquirerName)
                    {
                        oNodeIss = oNodeParent.Nodes.Add(tempRelation.sIssuerName, tempRelation.sIssuerName);
                        AddCardNode(oNodeIss, tempRelation.sIssuerName);
                    }
        }

        /// <summary>
        /// Add Card node to Tree View
        /// </summary>
        /// <param name="oNodeParent">TreeNode : Parent's node for current Issuer Node</param>
        /// <param name="sIss">string : Issuer's name for current Issuer</param>
        protected void AddCardNode(TreeNode oNodeParent, string sIss)
        {
            TreeNode oNodeCard;

            foreach (Relations tempRelation in ltRelations)
                if (!string.IsNullOrEmpty(tempRelation.sCardName) && !oNodeParent.Nodes.ContainsKey(tempRelation.sCardName))
                    if (sIss == tempRelation.sIssuerName)
                        oNodeCard = oNodeParent.Nodes.Add(tempRelation.sCardName, tempRelation.sCardName);
        }

        private DataTable GetHistoryRelationData()
        {
            DataTable dtTemp = new DataTable();

            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileRelationHistoryBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = @sTerminalID;
            oCmd.Parameters.Add("@sDatetime", SqlDbType.VarChar).Value = cmbDate.SelectedValue.ToString();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            return dtTemp;
        }

        private void LoadHistoryIssuer(string sTerminalID)
        {
            if (cmbDate.SelectedIndex > -1 && !string.IsNullOrEmpty(cmbDate.SelectedValue.ToString()))
            {
                dtIssuerHistoryData = GetHistoryIssuerData();
                dgvIssuer.DataSource = dtIssuerHistoryData;
            }
        }

        private DataTable GetHistoryIssuerData()
        {
            DataTable dtTemp = new DataTable();

            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileIssuerHistoryBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = @sTerminalID;
            oCmd.Parameters.Add("@sDatetime", SqlDbType.VarChar).Value = cmbDate.SelectedValue.ToString();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            return dtTemp;
        }

        private void LoadHistoryAcquirer(string sTerminalID)
        {
            if (cmbDate.SelectedIndex > -1 && !string.IsNullOrEmpty(cmbDate.SelectedValue.ToString()))
            {
                dtAcquirerHistoryData = GetHistoryAcquirerData();
                dgvAcquirer.DataSource = dtAcquirerHistoryData;
            }
        }

        private DataTable GetHistoryAcquirerData()
        {
            DataTable dtTemp = new DataTable();

            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileAcquirerHistoryBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = @sTerminalID;
            oCmd.Parameters.Add("@sDatetime", SqlDbType.VarChar).Value = cmbDate.SelectedValue.ToString();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            return dtTemp;
        }

        private void LoadHistoryTerminal(string sTerminalID)
        {
            if (cmbDate.SelectedIndex > -1 && !string.IsNullOrEmpty(cmbDate.SelectedValue.ToString()))
            {
                dtTerminalHistoryData = GetHistoryTerminalData();
                dgvTerminal.DataSource = dtTerminalHistoryData;
            }
        }

        private DataTable GetHistoryTerminalData()
        {
            DataTable dtTemp = new DataTable();

            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileTerminalHistoryBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = @sTerminalID;
            oCmd.Parameters.Add("@sDatetime", SqlDbType.VarChar).Value = cmbDate.SelectedValue.ToString();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            return dtTemp;
        }

        private void btnRestore_Click(object sender, EventArgs e)
        {
            if (cmbDate.SelectedIndex > -1 )
            {
                if (MessageBox.Show("Are you sure want to Restore terminal : " + sTerminalID + " on date : " + cmbDate.SelectedValue.ToString() + " ? ", "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (isTagSame())
                    {
                        if (dtTerminalHistoryData.Rows.Count > 0)
                            RestoreTerminal(sTerminalID, cmbDate.SelectedValue.ToString());
                        if (dtAcquirerHistoryData.Rows.Count > 0)
                            RestoreAcquirer(sTerminalID, cmbDate.SelectedValue.ToString());
                        if (dtIssuerHistoryData.Rows.Count > 0)
                            RestoreIssuer(sTerminalID, cmbDate.SelectedValue.ToString());
                        if (dtRelationHistoryData.Rows.Count > 0)
                            RestoreRelation(sTerminalID, cmbDate.SelectedValue.ToString());

                        CommonClass.InputLog(oSqlConnAuditTrail, sTerminalID, UserData.sUserID, "", "Restore " + sTerminalID + " History" + " on date  " + cmbDate.SelectedValue.ToString(), "");

                        this.Close();
                        this.Dispose();
                    }
                    else
                        MessageBox.Show("Tag Not Same Please Restore with another History.");
                }
            }
            else
            {
                MessageBox.Show("Please Choose Date");
            }
        }

        private bool isTagSame()
        {
            bool isSame = false;
            string sQuery = string.Format("SELECT DBO.isHistoryTagSame('{0}','{1}')", sTerminalID,cmbDate.SelectedValue.ToString());
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                DataTable dtTemp = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isSame = bool.Parse(dtTemp.Rows[0][0].ToString());
                oSqlConn.Close();
            }
            return isSame;
        }

        private void RestoreRelation(string sTerminalID, string sDate)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileRelationHistoryRestore, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
            oCmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = sDate;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oCmd.ExecuteNonQuery();
        }

        private void RestoreIssuer(string sTerminalID, string sDate)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileIssuerHistoryRestore, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
            oCmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = sDate;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oCmd.ExecuteNonQuery();
        }

        private void RestoreAcquirer(string sTerminalID, string sDate)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileAcquirerHistoryRestore, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
            oCmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = sDate;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oCmd.ExecuteNonQuery();
        }

        private void RestoreTerminal(string sTerminalID, string sDate)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileTerminalHistoryRestore, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
            oCmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = sDate;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oCmd.ExecuteNonQuery();
        }


    }
}
