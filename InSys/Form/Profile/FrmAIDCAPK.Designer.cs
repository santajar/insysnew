namespace InSys
{
    partial class FrmAIDCAPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAIDCAPK));
            this.btnTesting = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lvAIDCAPK = new System.Windows.Forms.ListView();
            this.gbData = new System.Windows.Forms.GroupBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.gbData.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnTesting
            // 
            this.btnTesting.Location = new System.Drawing.Point(139, 41);
            this.btnTesting.Name = "btnTesting";
            this.btnTesting.Size = new System.Drawing.Size(115, 23);
            this.btnTesting.TabIndex = 3;
            this.btnTesting.Text = "Testing";
            this.btnTesting.Visible = false;
            this.btnTesting.Click += new System.EventHandler(this.btnTesting_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(139, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(115, 23);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(6, 41);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(115, 23);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(6, 12);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(115, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lvAIDCAPK
            // 
            this.lvAIDCAPK.Location = new System.Drawing.Point(6, 19);
            this.lvAIDCAPK.Name = "lvAIDCAPK";
            this.lvAIDCAPK.Size = new System.Drawing.Size(248, 280);
            this.lvAIDCAPK.TabIndex = 12;
            this.lvAIDCAPK.UseCompatibleStateImageBehavior = false;
            this.lvAIDCAPK.View = System.Windows.Forms.View.List;
            // 
            // gbData
            // 
            this.gbData.Controls.Add(this.lvAIDCAPK);
            this.gbData.Location = new System.Drawing.Point(12, 12);
            this.gbData.Name = "gbData";
            this.gbData.Size = new System.Drawing.Size(260, 307);
            this.gbData.TabIndex = 18;
            this.gbData.TabStop = false;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnAdd);
            this.gbButton.Controls.Add(this.btnEdit);
            this.gbButton.Controls.Add(this.btnTesting);
            this.gbButton.Controls.Add(this.btnDelete);
            this.gbButton.Location = new System.Drawing.Point(12, 320);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(260, 70);
            this.gbButton.TabIndex = 19;
            this.gbButton.TabStop = false;
            // 
            // FrmAIDCAPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 396);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmAIDCAPK";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.FrmAIDCAPK_Load);
            this.gbData.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnTesting;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnEdit;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.ListView lvAIDCAPK;
        private System.Windows.Forms.GroupBox gbData;
        private System.Windows.Forms.GroupBox gbButton;
    }
}