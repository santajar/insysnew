namespace InSys
{
    partial class FrmEditAID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditAID));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbMatchCriteria = new System.Windows.Forms.ComboBox();
            this.txtAIDPrintedName = new System.Windows.Forms.TextBox();
            this.txtVersionList = new System.Windows.Forms.TextBox();
            this.txtAIDList = new System.Windows.Forms.TextBox();
            this.lblMatchingCriteria = new System.Windows.Forms.Label();
            this.lblAIDPrintedName = new System.Windows.Forms.Label();
            this.lblVersionList = new System.Windows.Forms.Label();
            this.lblAIDList = new System.Windows.Forms.Label();
            this.gbHeader = new System.Windows.Forms.GroupBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.gbHeader.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(178, 11);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSave.Location = new System.Drawing.Point(57, 11);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(115, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // cmbMatchCriteria
            // 
            this.cmbMatchCriteria.Items.AddRange(new object[] {
            "EMV AID FULL MATCH",
            "EMV AID PARTIAL MATCH"});
            this.cmbMatchCriteria.Location = new System.Drawing.Point(124, 112);
            this.cmbMatchCriteria.Name = "cmbMatchCriteria";
            this.cmbMatchCriteria.Size = new System.Drawing.Size(168, 21);
            this.cmbMatchCriteria.TabIndex = 3;
            // 
            // txtAIDPrintedName
            // 
            this.txtAIDPrintedName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAIDPrintedName.Location = new System.Drawing.Point(124, 80);
            this.txtAIDPrintedName.MaxLength = 50;
            this.txtAIDPrintedName.Name = "txtAIDPrintedName";
            this.txtAIDPrintedName.Size = new System.Drawing.Size(168, 20);
            this.txtAIDPrintedName.TabIndex = 2;
            // 
            // txtVersionList
            // 
            this.txtVersionList.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVersionList.Location = new System.Drawing.Point(124, 48);
            this.txtVersionList.MaxLength = 5;
            this.txtVersionList.Name = "txtVersionList";
            this.txtVersionList.Size = new System.Drawing.Size(168, 20);
            this.txtVersionList.TabIndex = 1;
            // 
            // txtAIDList
            // 
            this.txtAIDList.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAIDList.Location = new System.Drawing.Point(124, 16);
            this.txtAIDList.MaxLength = 20;
            this.txtAIDList.Name = "txtAIDList";
            this.txtAIDList.Size = new System.Drawing.Size(168, 20);
            this.txtAIDList.TabIndex = 0;
            // 
            // lblMatchingCriteria
            // 
            this.lblMatchingCriteria.Location = new System.Drawing.Point(3, 115);
            this.lblMatchingCriteria.Name = "lblMatchingCriteria";
            this.lblMatchingCriteria.Size = new System.Drawing.Size(120, 23);
            this.lblMatchingCriteria.TabIndex = 14;
            this.lblMatchingCriteria.Text = "Matching Criteria     :";
            // 
            // lblAIDPrintedName
            // 
            this.lblAIDPrintedName.Location = new System.Drawing.Point(3, 80);
            this.lblAIDPrintedName.Name = "lblAIDPrintedName";
            this.lblAIDPrintedName.Size = new System.Drawing.Size(112, 23);
            this.lblAIDPrintedName.TabIndex = 13;
            this.lblAIDPrintedName.Text = "AID Printed Name   :";
            // 
            // lblVersionList
            // 
            this.lblVersionList.Location = new System.Drawing.Point(6, 48);
            this.lblVersionList.Name = "lblVersionList";
            this.lblVersionList.Size = new System.Drawing.Size(112, 23);
            this.lblVersionList.TabIndex = 12;
            this.lblVersionList.Text = "Version List             :";
            // 
            // lblAIDList
            // 
            this.lblAIDList.Location = new System.Drawing.Point(6, 16);
            this.lblAIDList.Name = "lblAIDList";
            this.lblAIDList.Size = new System.Drawing.Size(112, 23);
            this.lblAIDList.TabIndex = 11;
            this.lblAIDList.Text = "AID List                   :";
            // 
            // gbHeader
            // 
            this.gbHeader.Controls.Add(this.lblAIDList);
            this.gbHeader.Controls.Add(this.lblVersionList);
            this.gbHeader.Controls.Add(this.lblAIDPrintedName);
            this.gbHeader.Controls.Add(this.cmbMatchCriteria);
            this.gbHeader.Controls.Add(this.lblMatchingCriteria);
            this.gbHeader.Controls.Add(this.txtAIDPrintedName);
            this.gbHeader.Controls.Add(this.txtAIDList);
            this.gbHeader.Controls.Add(this.txtVersionList);
            this.gbHeader.Location = new System.Drawing.Point(9, 2);
            this.gbHeader.Name = "gbHeader";
            this.gbHeader.Size = new System.Drawing.Size(299, 145);
            this.gbHeader.TabIndex = 15;
            this.gbHeader.TabStop = false;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(9, 148);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(299, 40);
            this.gbButton.TabIndex = 16;
            this.gbButton.TabStop = false;
            // 
            // FrmEditAID
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(318, 196);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmEditAID";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.FrmEditAID_Load);
            this.gbHeader.ResumeLayout(false);
            this.gbHeader.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.ComboBox cmbMatchCriteria;
        internal System.Windows.Forms.TextBox txtAIDPrintedName;
        internal System.Windows.Forms.TextBox txtVersionList;
        internal System.Windows.Forms.TextBox txtAIDList;
        internal System.Windows.Forms.Label lblMatchingCriteria;
        internal System.Windows.Forms.Label lblAIDPrintedName;
        internal System.Windows.Forms.Label lblVersionList;
        internal System.Windows.Forms.Label lblAIDList;
        private System.Windows.Forms.GroupBox gbHeader;
        private System.Windows.Forms.GroupBox gbButton;
    }
}