﻿namespace InSys
{
    partial class FrmViewHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmViewHistory));
            this.lblDate = new System.Windows.Forms.Label();
            this.cmbDate = new System.Windows.Forms.ComboBox();
            this.gbHistory = new System.Windows.Forms.GroupBox();
            this.dgvIssuer = new System.Windows.Forms.DataGridView();
            this.dgvAcquirer = new System.Windows.Forms.DataGridView();
            this.dgvTerminal = new System.Windows.Forms.DataGridView();
            this.lblRelation = new System.Windows.Forms.Label();
            this.lblIssuer = new System.Windows.Forms.Label();
            this.lblAcquirer = new System.Windows.Forms.Label();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.tvRelation = new System.Windows.Forms.TreeView();
            this.btnRestore = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssuer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcquirer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(21, 9);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "Date";
            // 
            // cmbDate
            // 
            this.cmbDate.FormatString = "F";
            this.cmbDate.FormattingEnabled = true;
            this.cmbDate.Location = new System.Drawing.Point(71, 6);
            this.cmbDate.Name = "cmbDate";
            this.cmbDate.Size = new System.Drawing.Size(182, 21);
            this.cmbDate.TabIndex = 1;
            this.cmbDate.SelectedIndexChanged += new System.EventHandler(this.cmbDate_SelectedIndexChanged);
            // 
            // gbHistory
            // 
            this.gbHistory.Controls.Add(this.dgvIssuer);
            this.gbHistory.Controls.Add(this.dgvAcquirer);
            this.gbHistory.Controls.Add(this.dgvTerminal);
            this.gbHistory.Controls.Add(this.lblRelation);
            this.gbHistory.Controls.Add(this.lblIssuer);
            this.gbHistory.Controls.Add(this.lblAcquirer);
            this.gbHistory.Controls.Add(this.lblTerminal);
            this.gbHistory.Controls.Add(this.tvRelation);
            this.gbHistory.Location = new System.Drawing.Point(15, 36);
            this.gbHistory.Name = "gbHistory";
            this.gbHistory.Size = new System.Drawing.Size(1233, 619);
            this.gbHistory.TabIndex = 9;
            this.gbHistory.TabStop = false;
            this.gbHistory.Text = "History";
            // 
            // dgvIssuer
            // 
            this.dgvIssuer.AllowUserToAddRows = false;
            this.dgvIssuer.AllowUserToDeleteRows = false;
            this.dgvIssuer.AllowUserToResizeRows = false;
            this.dgvIssuer.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvIssuer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIssuer.Location = new System.Drawing.Point(620, 39);
            this.dgvIssuer.Name = "dgvIssuer";
            this.dgvIssuer.RowHeadersVisible = false;
            this.dgvIssuer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvIssuer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvIssuer.Size = new System.Drawing.Size(300, 560);
            this.dgvIssuer.TabIndex = 411;
            // 
            // dgvAcquirer
            // 
            this.dgvAcquirer.AllowUserToAddRows = false;
            this.dgvAcquirer.AllowUserToDeleteRows = false;
            this.dgvAcquirer.AllowUserToResizeRows = false;
            this.dgvAcquirer.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvAcquirer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAcquirer.Location = new System.Drawing.Point(314, 39);
            this.dgvAcquirer.Name = "dgvAcquirer";
            this.dgvAcquirer.RowHeadersVisible = false;
            this.dgvAcquirer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvAcquirer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAcquirer.Size = new System.Drawing.Size(300, 560);
            this.dgvAcquirer.TabIndex = 410;
            // 
            // dgvTerminal
            // 
            this.dgvTerminal.AllowUserToAddRows = false;
            this.dgvTerminal.AllowUserToDeleteRows = false;
            this.dgvTerminal.AllowUserToResizeRows = false;
            this.dgvTerminal.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTerminal.Location = new System.Drawing.Point(9, 39);
            this.dgvTerminal.Name = "dgvTerminal";
            this.dgvTerminal.RowHeadersVisible = false;
            this.dgvTerminal.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvTerminal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTerminal.Size = new System.Drawing.Size(300, 560);
            this.dgvTerminal.TabIndex = 407;
            // 
            // lblRelation
            // 
            this.lblRelation.AutoSize = true;
            this.lblRelation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelation.Location = new System.Drawing.Point(923, 18);
            this.lblRelation.Name = "lblRelation";
            this.lblRelation.Size = new System.Drawing.Size(62, 18);
            this.lblRelation.TabIndex = 406;
            this.lblRelation.Text = "Relation";
            // 
            // lblIssuer
            // 
            this.lblIssuer.AutoSize = true;
            this.lblIssuer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIssuer.Location = new System.Drawing.Point(617, 18);
            this.lblIssuer.Name = "lblIssuer";
            this.lblIssuer.Size = new System.Drawing.Size(48, 18);
            this.lblIssuer.TabIndex = 405;
            this.lblIssuer.Text = "Issuer";
            // 
            // lblAcquirer
            // 
            this.lblAcquirer.AutoSize = true;
            this.lblAcquirer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcquirer.Location = new System.Drawing.Point(311, 18);
            this.lblAcquirer.Name = "lblAcquirer";
            this.lblAcquirer.Size = new System.Drawing.Size(62, 18);
            this.lblAcquirer.TabIndex = 404;
            this.lblAcquirer.Text = "Acquirer";
            // 
            // lblTerminal
            // 
            this.lblTerminal.AutoSize = true;
            this.lblTerminal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminal.Location = new System.Drawing.Point(6, 18);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(65, 18);
            this.lblTerminal.TabIndex = 403;
            this.lblTerminal.Text = "Terminal";
            // 
            // tvRelation
            // 
            this.tvRelation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tvRelation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvRelation.Location = new System.Drawing.Point(926, 39);
            this.tvRelation.Name = "tvRelation";
            this.tvRelation.Size = new System.Drawing.Size(300, 560);
            this.tvRelation.TabIndex = 402;
            // 
            // btnRestore
            // 
            this.btnRestore.Location = new System.Drawing.Point(554, 661);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(75, 23);
            this.btnRestore.TabIndex = 10;
            this.btnRestore.Text = "Restore";
            this.btnRestore.UseVisualStyleBackColor = true;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(635, 661);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // frmViewHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1252, 690);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRestore);
            this.Controls.Add(this.gbHistory);
            this.Controls.Add(this.cmbDate);
            this.Controls.Add(this.lblDate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmViewHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmViewHistory";
            this.Load += new System.EventHandler(this.frmViewHistory_Load);
            this.gbHistory.ResumeLayout(false);
            this.gbHistory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssuer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcquirer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.ComboBox cmbDate;
        private System.Windows.Forms.GroupBox gbHistory;
        private System.Windows.Forms.Label lblRelation;
        private System.Windows.Forms.Label lblIssuer;
        private System.Windows.Forms.Label lblAcquirer;
        private System.Windows.Forms.Label lblTerminal;
        internal System.Windows.Forms.TreeView tvRelation;
        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView dgvTerminal;
        private System.Windows.Forms.DataGridView dgvIssuer;
        private System.Windows.Forms.DataGridView dgvAcquirer;
    }
}