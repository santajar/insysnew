namespace InSys
{
    partial class FrmLocationInsert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLocationInsert));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtIDLocation = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddLocation = new System.Windows.Forms.Button();
            this.txtNewLocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgUserViewLocation = new System.Windows.Forms.DataGridView();
            this.lvLocationDesc = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.ColDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUserViewLocation)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtIDLocation);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnAddLocation);
            this.groupBox1.Controls.Add(this.txtNewLocation);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(436, 76);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // txtIDLocation
            // 
            this.txtIDLocation.Location = new System.Drawing.Point(108, 13);
            this.txtIDLocation.MaxLength = 50;
            this.txtIDLocation.Name = "txtIDLocation";
            this.txtIDLocation.Size = new System.Drawing.Size(194, 20);
            this.txtIDLocation.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "New Location";
            // 
            // btnAddLocation
            // 
            this.btnAddLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddLocation.Location = new System.Drawing.Point(317, 26);
            this.btnAddLocation.Name = "btnAddLocation";
            this.btnAddLocation.Size = new System.Drawing.Size(105, 23);
            this.btnAddLocation.TabIndex = 3;
            this.btnAddLocation.Text = "Add";
            this.btnAddLocation.UseVisualStyleBackColor = true;
            this.btnAddLocation.Click += new System.EventHandler(this.btnAddLocation_Click);
            // 
            // txtNewLocation
            // 
            this.txtNewLocation.Location = new System.Drawing.Point(108, 42);
            this.txtNewLocation.MaxLength = 50;
            this.txtNewLocation.Name = "txtNewLocation";
            this.txtNewLocation.Size = new System.Drawing.Size(194, 20);
            this.txtNewLocation.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "New Id";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dgUserViewLocation);
            this.groupBox2.Controls.Add(this.lvLocationDesc);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 94);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(437, 357);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // dgUserViewLocation
            // 
            this.dgUserViewLocation.AllowUserToAddRows = false;
            this.dgUserViewLocation.AllowUserToDeleteRows = false;
            this.dgUserViewLocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUserViewLocation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColDelete});
            this.dgUserViewLocation.Location = new System.Drawing.Point(9, 35);
            this.dgUserViewLocation.Name = "dgUserViewLocation";
            this.dgUserViewLocation.ReadOnly = true;
            this.dgUserViewLocation.Size = new System.Drawing.Size(413, 316);
            this.dgUserViewLocation.TabIndex = 2;
            this.dgUserViewLocation.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUserViewLocation_CellContentClick);
            // 
            // lvLocationDesc
            // 
            this.lvLocationDesc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvLocationDesc.Location = new System.Drawing.Point(9, 51);
            this.lvLocationDesc.Name = "lvLocationDesc";
            this.lvLocationDesc.Size = new System.Drawing.Size(413, 300);
            this.lvLocationDesc.TabIndex = 1;
            this.lvLocationDesc.UseCompatibleStateImageBehavior = false;
            this.lvLocationDesc.View = System.Windows.Forms.View.List;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Location List";
            // 
            // ColDelete
            // 
            this.ColDelete.HeaderText = "Delete";
            this.ColDelete.Name = "ColDelete";
            this.ColDelete.ReadOnly = true;
            this.ColDelete.Text = "Delete";
            this.ColDelete.UseColumnTextForButtonValue = true;
            this.ColDelete.Width = 70;
            // 
            // FrmLocationInsert
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 463);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmLocationInsert";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert New Profile Location";
            this.Load += new System.EventHandler(this.FrmLocationInsert_Load);
            this.Activated += new System.EventHandler(this.FrmLocationInsert_Activated);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUserViewLocation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAddLocation;
        private System.Windows.Forms.TextBox txtNewLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvLocationDesc;
        private System.Windows.Forms.TextBox txtIDLocation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgUserViewLocation;
        private System.Windows.Forms.DataGridViewButtonColumn ColDelete;
    }
}