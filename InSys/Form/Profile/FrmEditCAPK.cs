using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmEditCAPK : Form
    {
        /// <summary>
        /// Deterimines if form is in Editing mode or Adding mode.
        /// </summary>
        protected bool IsEdit;
        protected SqlConnection oSqlConn;
        protected string sDbId;
        protected string sDbName;
        
        //EMV MEGA
        //PK01 CAPKIndex
        //PK02 CAPKRID
        //PK03 CAPKLength
        //PK04 CAPKModulus
        //PK05 CAPKExponent
        //PK06 CAPKHash
        protected string sCAPKIndex;    //PK01
        protected string sCAPKRID;      //PK02
        protected string sCAPKLength;   //PK03
        protected string sCAPKModulus;  //PK04
        protected string sCAPKExponent; //PK05
        protected string sCAPKHash;     //PK06

        protected const string CAPKIndex = "PK01";
        protected const string CAPKRID = "PK02";
        protected const string CAPKLength = "PK03";
        protected const string CAPKModulus = "PK04";
        protected const string CAPKExponent = "PK05";
        protected const string CAPKHash = "PK06";

        /// <summary>
        /// Form to Add or modify CAPK data
        /// </summary>
        /// <param name="_isEdit">boolean : true if in Edit mode, else false in Add mode</param>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sDbId">string : DatabaseId</param>
        public FrmEditCAPK(bool _isEdit, SqlConnection _oSqlConn, string _sDbId, string _sDbName) : this(_isEdit, _oSqlConn, _sDbId, _sDbName, "") { }
        public FrmEditCAPK(bool _isEdit, SqlConnection _oSqlConn, string _sDbId, string _sDbName, string _sCAPKIndex)
        {
            IsEdit = _isEdit;
            oSqlConn = _oSqlConn;
            sDbId = _sDbId;
            sDbName = _sDbName;
            sCAPKIndex = _sCAPKIndex;
            InitializeComponent();
        }

        /// <summary>
        /// Call SetDisplay function.
        /// </summary>
        private void FrmEditCAPK_Load(object sender, EventArgs e)
        {
            InitCAPK();
            SetDisplay();
            string sLog = CommonMessage.sFormOpened + this.Text;
            if (IsEdit)
                sLog += " : " + sCAPKIndex;

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, sLog, "");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                if (IsEdit)
                    UpdateCAPK(sGenCAPKContent);
                else
                    SaveCAPK(sGenCAPKContent);
                this.Close();
            }
        }

        /// <summary>
        /// Close the form.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region "Properties"
        protected string sGetCAPKIndex { get { return txtIndex.Text; } }
        protected string sGetCAPKHash { get { return txtHash.Text; } }
        protected string sGetCAPKModulus { get { return txtModulus.Text; } }
        protected string sGetCAPKLength { get { return txtLength.Text; } }
        protected string sGetCAPKRID { get { return txtRID.Text; } }
        protected string sGetCAPKExponent { get { return txtExponent.Text; } }

        /// <summary>
        /// Generated CAPK content
        /// </summary>
        protected string sGenCAPKContent
        {
            get
            {
                return sGenCAPKIndex() + sGenCAPKRID() + sGenCAPKLength()
                    + sGenCAPKModulus() + sGenCAPKExponent() + sGenCAPKHash();
            }
        }

        /// <summary>
        /// Generated CAPK Index
        /// </summary>
        /// <returns>string : CAPK Index</returns>
        protected string sGenCAPKIndex()
        {
            return string.Format("{0}{1:000}{2}", CAPKIndex, sGetCAPKIndex.Length, sGetCAPKIndex);
        }

        /// <summary>
        /// Generated CAPK RID
        /// </summary>
        /// <returns>string : CAPK RID</returns>
        protected string sGenCAPKRID()
        {
            return string.Format("{0}{1:000}{2}", CAPKRID, sGetCAPKRID.Length / 2, sGetCAPKRID);
        }

        /// <summary>
        /// Generated CAPK Length
        /// </summary>
        /// <returns>string : CAPK Length</returns>
        protected string sGenCAPKLength()
        {
            return string.Format("{0}{1:000}{2}", CAPKLength, sGetCAPKLength.Length, sGetCAPKLength);
        }

        /// <summary>
        /// Generated modulus result from CAPK
        /// </summary>
        /// <returns>string : CAPK Modulus</returns>
        protected string sGenCAPKModulus()
        {
            return string.Format("{0}{1:000}{2}", CAPKModulus, sGetCAPKModulus.Length / 2, sGetCAPKModulus);
        }

        /// <summary>
        /// Generated CAPK Exponent value
        /// </summary>
        /// <returns>string : CAPK Exponent</returns>
        protected string sGenCAPKExponent()
        {
            return string.Format("{0}{1:000}{2}", CAPKExponent, sGetCAPKExponent.Length, sGetCAPKExponent);
        }

        /// <summary>
        /// Generted CAPK Hash
        /// </summary>
        /// <returns>string : CAPK Hash</returns>
        protected string sGenCAPKHash()
        {
            return string.Format("{0}{1:000}{2}", CAPKHash, sGetCAPKHash.Length / 2, sGetCAPKHash);
        }
        #endregion

        #region "Function"
        /// <summary>
        /// Initiate values from database to objects in form
        /// </summary>
        protected void InitCAPK()
        {
            DataTable dtCAPK = dtGetCAPK();
            foreach (DataRow drow in dtCAPK.Rows)
            {
                switch (drow["CAPKTag"].ToString())
                {
                    case CAPKIndex :
                        sCAPKIndex = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPKExponent :
                        sCAPKExponent = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPKHash :
                        sCAPKHash = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPKLength:
                        sCAPKLength = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPKModulus:
                        sCAPKModulus = drow["CAPKTagValue"].ToString();
                        break;
                    case CAPKRID:
                        sCAPKRID = drow["CAPKTagValue"].ToString();
                        break;
                }
                
            }
        }

        /// <summary>
        /// WHERE condition to filter data loaded from database
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sCondition()
        {
            return "WHERE DatabaseId = " + sDbId + " "
                + "AND CAPKIndex = '" + sCAPKIndex + "'";
        }

        /// <summary>
        /// Set the form's display.
        /// </summary>
        protected void SetDisplay()
        {
            if (!IsEdit) this.Text = "CAPK Management - Add";
            else this.Text = "CAPK Management - Edit";
            txtExponent.Enabled = txtHash.Enabled = txtIndex.Enabled =
                txtLength.Enabled = txtModulus.Enabled = txtRID.Enabled = btnSave.Visible =
                UserPrivilege.IsAllowed(PrivilegeCode.PK, Privilege.Edit);
            
            txtExponent.Text = sCAPKExponent;
            txtHash.Text = sCAPKHash;
            txtIndex.Text = sCAPKIndex;
            txtLength.Text = sCAPKLength;
            txtRID.Text = sCAPKRID;
            txtModulus.Text = sCAPKModulus;
            txtIndex.ReadOnly = IsEdit;
        }

        /// <summary>
        /// Load data from database then stored in DataTable
        /// </summary>
        /// <returns>DataTable : contain data loaded from database</returns>
        protected DataTable dtGetCAPK()
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileCAPKBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sCondition();
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            oCmd.Dispose();
            return dtTemp;
        }

        /// <summary>
        /// Determine whether values entered by user valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValid()
        {
            if (!string.IsNullOrEmpty(txtIndex.Text) &&
                !string.IsNullOrEmpty(txtExponent.Text) &&
                !string.IsNullOrEmpty(txtHash.Text) &&
                !string.IsNullOrEmpty(txtLength.Text) &&
                !string.IsNullOrEmpty(txtModulus.Text) &&
                !string.IsNullOrEmpty(txtRID.Text))
                if (!IsEdit)
                    if (IsValidCAPKIndex())
                    {
                        MessageBox.Show(CommonMessage.sErrCAPKInvalid);
                        return false;
                    }
                    else
                        return true;
                else
                    return true;
            else
            {
                MessageBox.Show(CommonMessage.sErrCAPKEmptyField);
                return false;
            }
        }

        /// <summary>
        /// Determine whether given CAPK Index value valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValidCAPKIndex()
        {
            sCAPKIndex = sGetCAPKIndex;
            DataTable dtTemp = dtGetCAPK();
            return dtTemp.Rows.Count > 0 ? true : false;
        }

        /// <summary>
        /// Save new data CAPK entered by user into database
        /// </summary>
        /// <param name="sCAPKContent">string : content of CAPK</param>
        protected void SaveCAPK(string sCAPKContent)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileCAPKInsert, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDbId;
            oCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sCAPKContent;
            oCmd.ExecuteNonQuery();
            oCmd.Dispose();

            string sLogDetail = string.Format("{0} : {1} \n", "CAPK Index", sGetCAPKIndex);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK Exponent", sGetCAPKExponent);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK Length", sGetCAPKLength);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK Modulus", sGetCAPKModulus);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK RID", sGetCAPKRID);
            sLogDetail += string.Format("{0} : {1} \n", "CAPK Hash", sGetCAPKHash);
            
            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Add CAPK : "+sGetCAPKIndex, sLogDetail);
        }

        /// <summary>
        /// Update existing CAPK value to database
        /// </summary>
        /// <param name="sCAPKContent">string : content of CAPK</param>
        protected void UpdateCAPK(string sCAPKContent)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileCAPKUpdate, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDbId;
            oCmd.Parameters.Add("@sCAPKIndex", SqlDbType.VarChar).Value = sCAPKIndex;
            oCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sCAPKContent;   
            //oCmd.ExecuteNonQuery();

            string sLogDetail = "";

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                while (oRead.Read())
                {
                    sLogDetail += string.Format("{0} : {1} --> {2} \n", oRead["TagName"].ToString(),
                                    oRead["OldCAPKValue"].ToString(), oRead["NewCAPKValue"].ToString());
                }
                oRead.Close();
            }
            oCmd.Dispose();

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Update CAPK : " + sGetCAPKIndex, sLogDetail);
        }
        #endregion
    }
}