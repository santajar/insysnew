﻿namespace InSys
{
    partial class FrmEdcGroupProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEdcGroupProperties));
            this.tabCtrlGroupRegionTerminal = new System.Windows.Forms.TabControl();
            this.tabPageGroupRegion = new System.Windows.Forms.TabPage();
            this.lblSoftwareNameGroup = new System.Windows.Forms.Label();
            this.cmbSoftwareNameGroup = new System.Windows.Forms.ComboBox();
            this.chkEnableDownloadGroupRegion = new System.Windows.Forms.CheckBox();
            this.rtbGroupRegionDescription = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGroupRegionName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageTerminal = new System.Windows.Forms.TabPage();
            this.lblSoftwareName = new System.Windows.Forms.Label();
            this.cmbSoftwareName = new System.Windows.Forms.ComboBox();
            this.cmbTerminalType = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbTerminalBrand = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.rtbTerminalDescription = new System.Windows.Forms.RichTextBox();
            this.chkEnableDownloadSN = new System.Windows.Forms.CheckBox();
            this.txtTerminalName = new System.Windows.Forms.TextBox();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPageSchedule = new System.Windows.Forms.TabPage();
            this.panelSchedule = new System.Windows.Forms.Panel();
            this.panelScheduleMonthly = new System.Windows.Forms.Panel();
            this.dateTimePickerClockMonthly = new System.Windows.Forms.DateTimePicker();
            this.cmbMonthlyDate = new System.Windows.Forms.ComboBox();
            this.chkEnableMonthly = new System.Windows.Forms.CheckBox();
            this.panelScheduleWeekly = new System.Windows.Forms.Panel();
            this.dateTimePickerClockWeekly = new System.Windows.Forms.DateTimePicker();
            this.chkSaturday = new System.Windows.Forms.CheckBox();
            this.chkFriday = new System.Windows.Forms.CheckBox();
            this.chkThursday = new System.Windows.Forms.CheckBox();
            this.chkWednesday = new System.Windows.Forms.CheckBox();
            this.chkTuesday = new System.Windows.Forms.CheckBox();
            this.chkMonday = new System.Windows.Forms.CheckBox();
            this.chkSunday = new System.Windows.Forms.CheckBox();
            this.chkEnableWeekly = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePickerClockDaily = new System.Windows.Forms.DateTimePicker();
            this.chkEnableSchedule = new System.Windows.Forms.CheckBox();
            this.panelTabPages = new System.Windows.Forms.Panel();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tabCtrlGroupRegionTerminal.SuspendLayout();
            this.tabPageGroupRegion.SuspendLayout();
            this.tabPageTerminal.SuspendLayout();
            this.tabPageSchedule.SuspendLayout();
            this.panelSchedule.SuspendLayout();
            this.panelScheduleMonthly.SuspendLayout();
            this.panelScheduleWeekly.SuspendLayout();
            this.panelTabPages.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCtrlGroupRegionTerminal
            // 
            this.tabCtrlGroupRegionTerminal.Controls.Add(this.tabPageGroupRegion);
            this.tabCtrlGroupRegionTerminal.Controls.Add(this.tabPageTerminal);
            this.tabCtrlGroupRegionTerminal.Controls.Add(this.tabPageSchedule);
            this.tabCtrlGroupRegionTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlGroupRegionTerminal.Location = new System.Drawing.Point(0, 0);
            this.tabCtrlGroupRegionTerminal.Name = "tabCtrlGroupRegionTerminal";
            this.tabCtrlGroupRegionTerminal.SelectedIndex = 0;
            this.tabCtrlGroupRegionTerminal.Size = new System.Drawing.Size(588, 256);
            this.tabCtrlGroupRegionTerminal.TabIndex = 0;
            // 
            // tabPageGroupRegion
            // 
            this.tabPageGroupRegion.Controls.Add(this.lblSoftwareNameGroup);
            this.tabPageGroupRegion.Controls.Add(this.cmbSoftwareNameGroup);
            this.tabPageGroupRegion.Controls.Add(this.chkEnableDownloadGroupRegion);
            this.tabPageGroupRegion.Controls.Add(this.rtbGroupRegionDescription);
            this.tabPageGroupRegion.Controls.Add(this.label2);
            this.tabPageGroupRegion.Controls.Add(this.txtGroupRegionName);
            this.tabPageGroupRegion.Controls.Add(this.label1);
            this.tabPageGroupRegion.Location = new System.Drawing.Point(4, 22);
            this.tabPageGroupRegion.Name = "tabPageGroupRegion";
            this.tabPageGroupRegion.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGroupRegion.Size = new System.Drawing.Size(580, 230);
            this.tabPageGroupRegion.TabIndex = 0;
            this.tabPageGroupRegion.Text = "Group";
            this.tabPageGroupRegion.UseVisualStyleBackColor = true;
            // 
            // lblSoftwareNameGroup
            // 
            this.lblSoftwareNameGroup.AutoSize = true;
            this.lblSoftwareNameGroup.Location = new System.Drawing.Point(215, 49);
            this.lblSoftwareNameGroup.Name = "lblSoftwareNameGroup";
            this.lblSoftwareNameGroup.Size = new System.Drawing.Size(80, 13);
            this.lblSoftwareNameGroup.TabIndex = 8;
            this.lblSoftwareNameGroup.Text = "Software Name";
            // 
            // cmbSoftwareNameGroup
            // 
            this.cmbSoftwareNameGroup.FormattingEnabled = true;
            this.cmbSoftwareNameGroup.Location = new System.Drawing.Point(302, 45);
            this.cmbSoftwareNameGroup.Name = "cmbSoftwareNameGroup";
            this.cmbSoftwareNameGroup.Size = new System.Drawing.Size(121, 21);
            this.cmbSoftwareNameGroup.TabIndex = 7;
            // 
            // chkEnableDownloadGroupRegion
            // 
            this.chkEnableDownloadGroupRegion.AutoSize = true;
            this.chkEnableDownloadGroupRegion.Location = new System.Drawing.Point(82, 49);
            this.chkEnableDownloadGroupRegion.Name = "chkEnableDownloadGroupRegion";
            this.chkEnableDownloadGroupRegion.Size = new System.Drawing.Size(110, 17);
            this.chkEnableDownloadGroupRegion.TabIndex = 6;
            this.chkEnableDownloadGroupRegion.Text = "Enable Download";
            this.chkEnableDownloadGroupRegion.UseVisualStyleBackColor = true;
            // 
            // rtbGroupRegionDescription
            // 
            this.rtbGroupRegionDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbGroupRegionDescription.Location = new System.Drawing.Point(82, 78);
            this.rtbGroupRegionDescription.MaxLength = 500;
            this.rtbGroupRegionDescription.Name = "rtbGroupRegionDescription";
            this.rtbGroupRegionDescription.Size = new System.Drawing.Size(485, 146);
            this.rtbGroupRegionDescription.TabIndex = 3;
            this.rtbGroupRegionDescription.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Description";
            // 
            // txtGroupRegionName
            // 
            this.txtGroupRegionName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGroupRegionName.Location = new System.Drawing.Point(82, 10);
            this.txtGroupRegionName.MaxLength = 50;
            this.txtGroupRegionName.Name = "txtGroupRegionName";
            this.txtGroupRegionName.Size = new System.Drawing.Size(355, 20);
            this.txtGroupRegionName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // tabPageTerminal
            // 
            this.tabPageTerminal.Controls.Add(this.lblSoftwareName);
            this.tabPageTerminal.Controls.Add(this.cmbSoftwareName);
            this.tabPageTerminal.Controls.Add(this.cmbTerminalType);
            this.tabPageTerminal.Controls.Add(this.label8);
            this.tabPageTerminal.Controls.Add(this.cmbTerminalBrand);
            this.tabPageTerminal.Controls.Add(this.label7);
            this.tabPageTerminal.Controls.Add(this.rtbTerminalDescription);
            this.tabPageTerminal.Controls.Add(this.chkEnableDownloadSN);
            this.tabPageTerminal.Controls.Add(this.txtTerminalName);
            this.tabPageTerminal.Controls.Add(this.txtSerialNumber);
            this.tabPageTerminal.Controls.Add(this.label5);
            this.tabPageTerminal.Controls.Add(this.label4);
            this.tabPageTerminal.Controls.Add(this.label3);
            this.tabPageTerminal.Location = new System.Drawing.Point(4, 22);
            this.tabPageTerminal.Name = "tabPageTerminal";
            this.tabPageTerminal.Size = new System.Drawing.Size(580, 230);
            this.tabPageTerminal.TabIndex = 2;
            this.tabPageTerminal.Text = "Terminal";
            this.tabPageTerminal.UseVisualStyleBackColor = true;
            // 
            // lblSoftwareName
            // 
            this.lblSoftwareName.AutoSize = true;
            this.lblSoftwareName.Location = new System.Drawing.Point(301, 72);
            this.lblSoftwareName.Name = "lblSoftwareName";
            this.lblSoftwareName.Size = new System.Drawing.Size(80, 13);
            this.lblSoftwareName.TabIndex = 12;
            this.lblSoftwareName.Text = "Software Name";
            // 
            // cmbSoftwareName
            // 
            this.cmbSoftwareName.FormattingEnabled = true;
            this.cmbSoftwareName.Location = new System.Drawing.Point(387, 69);
            this.cmbSoftwareName.Name = "cmbSoftwareName";
            this.cmbSoftwareName.Size = new System.Drawing.Size(145, 21);
            this.cmbSoftwareName.TabIndex = 11;
            // 
            // cmbTerminalType
            // 
            this.cmbTerminalType.FormattingEnabled = true;
            this.cmbTerminalType.Location = new System.Drawing.Point(387, 96);
            this.cmbTerminalType.Name = "cmbTerminalType";
            this.cmbTerminalType.Size = new System.Drawing.Size(145, 21);
            this.cmbTerminalType.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(346, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Type";
            // 
            // cmbTerminalBrand
            // 
            this.cmbTerminalBrand.FormattingEnabled = true;
            this.cmbTerminalBrand.Location = new System.Drawing.Point(118, 98);
            this.cmbTerminalBrand.Name = "cmbTerminalBrand";
            this.cmbTerminalBrand.Size = new System.Drawing.Size(121, 21);
            this.cmbTerminalBrand.TabIndex = 8;
            this.cmbTerminalBrand.SelectedIndexChanged += new System.EventHandler(this.cmbTerminalBrand_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 101);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Brand";
            // 
            // rtbTerminalDescription
            // 
            this.rtbTerminalDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbTerminalDescription.Location = new System.Drawing.Point(118, 134);
            this.rtbTerminalDescription.Name = "rtbTerminalDescription";
            this.rtbTerminalDescription.Size = new System.Drawing.Size(439, 93);
            this.rtbTerminalDescription.TabIndex = 6;
            this.rtbTerminalDescription.Text = "";
            // 
            // chkEnableDownloadSN
            // 
            this.chkEnableDownloadSN.AutoSize = true;
            this.chkEnableDownloadSN.Location = new System.Drawing.Point(118, 71);
            this.chkEnableDownloadSN.Name = "chkEnableDownloadSN";
            this.chkEnableDownloadSN.Size = new System.Drawing.Size(110, 17);
            this.chkEnableDownloadSN.TabIndex = 5;
            this.chkEnableDownloadSN.Text = "Enable Download";
            this.chkEnableDownloadSN.UseVisualStyleBackColor = true;
            // 
            // txtTerminalName
            // 
            this.txtTerminalName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTerminalName.Location = new System.Drawing.Point(118, 40);
            this.txtTerminalName.MaxLength = 25;
            this.txtTerminalName.Name = "txtTerminalName";
            this.txtTerminalName.Size = new System.Drawing.Size(221, 20);
            this.txtTerminalName.TabIndex = 4;
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSerialNumber.Location = new System.Drawing.Point(118, 10);
            this.txtSerialNumber.MaxLength = 25;
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(221, 20);
            this.txtSerialNumber.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Serial Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Name";
            // 
            // tabPageSchedule
            // 
            this.tabPageSchedule.Controls.Add(this.panelSchedule);
            this.tabPageSchedule.Controls.Add(this.chkEnableSchedule);
            this.tabPageSchedule.Location = new System.Drawing.Point(4, 22);
            this.tabPageSchedule.Name = "tabPageSchedule";
            this.tabPageSchedule.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSchedule.Size = new System.Drawing.Size(580, 230);
            this.tabPageSchedule.TabIndex = 1;
            this.tabPageSchedule.Text = "Schedule";
            this.tabPageSchedule.UseVisualStyleBackColor = true;
            // 
            // panelSchedule
            // 
            this.panelSchedule.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSchedule.Controls.Add(this.panelScheduleMonthly);
            this.panelSchedule.Controls.Add(this.chkEnableMonthly);
            this.panelSchedule.Controls.Add(this.panelScheduleWeekly);
            this.panelSchedule.Controls.Add(this.chkEnableWeekly);
            this.panelSchedule.Controls.Add(this.label6);
            this.panelSchedule.Controls.Add(this.dateTimePickerClockDaily);
            this.panelSchedule.Enabled = false;
            this.panelSchedule.Location = new System.Drawing.Point(15, 38);
            this.panelSchedule.Name = "panelSchedule";
            this.panelSchedule.Size = new System.Drawing.Size(552, 186);
            this.panelSchedule.TabIndex = 3;
            // 
            // panelScheduleMonthly
            // 
            this.panelScheduleMonthly.Controls.Add(this.dateTimePickerClockMonthly);
            this.panelScheduleMonthly.Controls.Add(this.cmbMonthlyDate);
            this.panelScheduleMonthly.Enabled = false;
            this.panelScheduleMonthly.Location = new System.Drawing.Point(92, 124);
            this.panelScheduleMonthly.Name = "panelScheduleMonthly";
            this.panelScheduleMonthly.Size = new System.Drawing.Size(200, 44);
            this.panelScheduleMonthly.TabIndex = 13;
            // 
            // dateTimePickerClockMonthly
            // 
            this.dateTimePickerClockMonthly.CustomFormat = "HH:mm ";
            this.dateTimePickerClockMonthly.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerClockMonthly.Location = new System.Drawing.Point(81, 11);
            this.dateTimePickerClockMonthly.Name = "dateTimePickerClockMonthly";
            this.dateTimePickerClockMonthly.ShowUpDown = true;
            this.dateTimePickerClockMonthly.Size = new System.Drawing.Size(82, 20);
            this.dateTimePickerClockMonthly.TabIndex = 12;
            this.dateTimePickerClockMonthly.ValueChanged += new System.EventHandler(this.dateTimePickerClockMonthly_ValueChanged);
            // 
            // cmbMonthlyDate
            // 
            this.cmbMonthlyDate.FormattingEnabled = true;
            this.cmbMonthlyDate.Location = new System.Drawing.Point(11, 11);
            this.cmbMonthlyDate.Name = "cmbMonthlyDate";
            this.cmbMonthlyDate.Size = new System.Drawing.Size(50, 21);
            this.cmbMonthlyDate.TabIndex = 6;
            this.cmbMonthlyDate.SelectedIndexChanged += new System.EventHandler(this.cmbMonthlyDate_SelectedIndexChanged);
            // 
            // chkEnableMonthly
            // 
            this.chkEnableMonthly.AutoSize = true;
            this.chkEnableMonthly.Location = new System.Drawing.Point(23, 139);
            this.chkEnableMonthly.Name = "chkEnableMonthly";
            this.chkEnableMonthly.Size = new System.Drawing.Size(63, 17);
            this.chkEnableMonthly.TabIndex = 5;
            this.chkEnableMonthly.Text = "Monthly";
            this.chkEnableMonthly.UseVisualStyleBackColor = true;
            this.chkEnableMonthly.CheckedChanged += new System.EventHandler(this.chkEnableMonthly_CheckedChanged);
            // 
            // panelScheduleWeekly
            // 
            this.panelScheduleWeekly.Controls.Add(this.dateTimePickerClockWeekly);
            this.panelScheduleWeekly.Controls.Add(this.chkSaturday);
            this.panelScheduleWeekly.Controls.Add(this.chkFriday);
            this.panelScheduleWeekly.Controls.Add(this.chkThursday);
            this.panelScheduleWeekly.Controls.Add(this.chkWednesday);
            this.panelScheduleWeekly.Controls.Add(this.chkTuesday);
            this.panelScheduleWeekly.Controls.Add(this.chkMonday);
            this.panelScheduleWeekly.Controls.Add(this.chkSunday);
            this.panelScheduleWeekly.Enabled = false;
            this.panelScheduleWeekly.Location = new System.Drawing.Point(92, 43);
            this.panelScheduleWeekly.Name = "panelScheduleWeekly";
            this.panelScheduleWeekly.Size = new System.Drawing.Size(371, 60);
            this.panelScheduleWeekly.TabIndex = 4;
            // 
            // dateTimePickerClockWeekly
            // 
            this.dateTimePickerClockWeekly.CustomFormat = "HH:mm ";
            this.dateTimePickerClockWeekly.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerClockWeekly.Location = new System.Drawing.Point(279, 32);
            this.dateTimePickerClockWeekly.Name = "dateTimePickerClockWeekly";
            this.dateTimePickerClockWeekly.ShowUpDown = true;
            this.dateTimePickerClockWeekly.Size = new System.Drawing.Size(82, 20);
            this.dateTimePickerClockWeekly.TabIndex = 11;
            this.dateTimePickerClockWeekly.ValueChanged += new System.EventHandler(this.dateTimePickerClockWeekly_ValueChanged);
            // 
            // chkSaturday
            // 
            this.chkSaturday.AutoSize = true;
            this.chkSaturday.Location = new System.Drawing.Point(191, 32);
            this.chkSaturday.Name = "chkSaturday";
            this.chkSaturday.Size = new System.Drawing.Size(68, 17);
            this.chkSaturday.TabIndex = 10;
            this.chkSaturday.Tag = "saturday";
            this.chkSaturday.Text = "Saturday";
            this.chkSaturday.UseVisualStyleBackColor = true;
            this.chkSaturday.CheckedChanged += new System.EventHandler(this.chkDaysSchedule_CheckedChanged);
            // 
            // chkFriday
            // 
            this.chkFriday.AutoSize = true;
            this.chkFriday.Location = new System.Drawing.Point(105, 32);
            this.chkFriday.Name = "chkFriday";
            this.chkFriday.Size = new System.Drawing.Size(54, 17);
            this.chkFriday.TabIndex = 9;
            this.chkFriday.Tag = "friday";
            this.chkFriday.Text = "Friday";
            this.chkFriday.UseVisualStyleBackColor = true;
            this.chkFriday.CheckedChanged += new System.EventHandler(this.chkDaysSchedule_CheckedChanged);
            // 
            // chkThursday
            // 
            this.chkThursday.AutoSize = true;
            this.chkThursday.Location = new System.Drawing.Point(7, 32);
            this.chkThursday.Name = "chkThursday";
            this.chkThursday.Size = new System.Drawing.Size(70, 17);
            this.chkThursday.TabIndex = 8;
            this.chkThursday.Tag = "thursday";
            this.chkThursday.Text = "Thursday";
            this.chkThursday.UseVisualStyleBackColor = true;
            this.chkThursday.CheckedChanged += new System.EventHandler(this.chkDaysSchedule_CheckedChanged);
            // 
            // chkWednesday
            // 
            this.chkWednesday.AutoSize = true;
            this.chkWednesday.Location = new System.Drawing.Point(279, 9);
            this.chkWednesday.Name = "chkWednesday";
            this.chkWednesday.Size = new System.Drawing.Size(83, 17);
            this.chkWednesday.TabIndex = 7;
            this.chkWednesday.Tag = "wednesday";
            this.chkWednesday.Text = "Wednesday";
            this.chkWednesday.UseVisualStyleBackColor = true;
            this.chkWednesday.CheckedChanged += new System.EventHandler(this.chkDaysSchedule_CheckedChanged);
            // 
            // chkTuesday
            // 
            this.chkTuesday.AutoSize = true;
            this.chkTuesday.Location = new System.Drawing.Point(191, 9);
            this.chkTuesday.Name = "chkTuesday";
            this.chkTuesday.Size = new System.Drawing.Size(67, 17);
            this.chkTuesday.TabIndex = 6;
            this.chkTuesday.Tag = "tuesday";
            this.chkTuesday.Text = "Tuesday";
            this.chkTuesday.UseVisualStyleBackColor = true;
            this.chkTuesday.CheckedChanged += new System.EventHandler(this.chkDaysSchedule_CheckedChanged);
            // 
            // chkMonday
            // 
            this.chkMonday.AutoSize = true;
            this.chkMonday.Location = new System.Drawing.Point(105, 9);
            this.chkMonday.Name = "chkMonday";
            this.chkMonday.Size = new System.Drawing.Size(64, 17);
            this.chkMonday.TabIndex = 5;
            this.chkMonday.Tag = "monday";
            this.chkMonday.Text = "Monday";
            this.chkMonday.UseVisualStyleBackColor = true;
            this.chkMonday.CheckedChanged += new System.EventHandler(this.chkDaysSchedule_CheckedChanged);
            // 
            // chkSunday
            // 
            this.chkSunday.AutoSize = true;
            this.chkSunday.Location = new System.Drawing.Point(7, 9);
            this.chkSunday.Name = "chkSunday";
            this.chkSunday.Size = new System.Drawing.Size(62, 17);
            this.chkSunday.TabIndex = 4;
            this.chkSunday.Tag = "sunday";
            this.chkSunday.Text = "Sunday";
            this.chkSunday.UseVisualStyleBackColor = true;
            this.chkSunday.CheckedChanged += new System.EventHandler(this.chkDaysSchedule_CheckedChanged);
            // 
            // chkEnableWeekly
            // 
            this.chkEnableWeekly.AutoSize = true;
            this.chkEnableWeekly.Location = new System.Drawing.Point(23, 43);
            this.chkEnableWeekly.Name = "chkEnableWeekly";
            this.chkEnableWeekly.Size = new System.Drawing.Size(62, 17);
            this.chkEnableWeekly.TabIndex = 3;
            this.chkEnableWeekly.Text = "Weekly";
            this.chkEnableWeekly.UseVisualStyleBackColor = true;
            this.chkEnableWeekly.CheckedChanged += new System.EventHandler(this.chkWeekly_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Daily";
            // 
            // dateTimePickerClockDaily
            // 
            this.dateTimePickerClockDaily.AllowDrop = true;
            this.dateTimePickerClockDaily.Checked = false;
            this.dateTimePickerClockDaily.CustomFormat = "HH:mm ";
            this.dateTimePickerClockDaily.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerClockDaily.Location = new System.Drawing.Point(56, 7);
            this.dateTimePickerClockDaily.Name = "dateTimePickerClockDaily";
            this.dateTimePickerClockDaily.ShowCheckBox = true;
            this.dateTimePickerClockDaily.ShowUpDown = true;
            this.dateTimePickerClockDaily.Size = new System.Drawing.Size(104, 20);
            this.dateTimePickerClockDaily.TabIndex = 1;
            this.dateTimePickerClockDaily.ValueChanged += new System.EventHandler(this.dateTimePickerClockDaily_ValueChanged);
            // 
            // chkEnableSchedule
            // 
            this.chkEnableSchedule.AutoSize = true;
            this.chkEnableSchedule.Location = new System.Drawing.Point(15, 15);
            this.chkEnableSchedule.Name = "chkEnableSchedule";
            this.chkEnableSchedule.Size = new System.Drawing.Size(107, 17);
            this.chkEnableSchedule.TabIndex = 2;
            this.chkEnableSchedule.Text = "Enable Schedule";
            this.chkEnableSchedule.UseVisualStyleBackColor = true;
            this.chkEnableSchedule.CheckedChanged += new System.EventHandler(this.chkEnableSchedule_CheckedChanged);
            // 
            // panelTabPages
            // 
            this.panelTabPages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTabPages.Controls.Add(this.tabCtrlGroupRegionTerminal);
            this.panelTabPages.Location = new System.Drawing.Point(12, 12);
            this.panelTabPages.Name = "panelTabPages";
            this.panelTabPages.Size = new System.Drawing.Size(588, 256);
            this.panelTabPages.TabIndex = 1;
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(12, 274);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(588, 68);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(475, 19);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 31);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(353, 19);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 31);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmEdcGroupProperties
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(612, 354);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.panelTabPages);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEdcGroupProperties";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Properties";
            this.Load += new System.EventHandler(this.FrmEdcGroupProperties_Load);
            this.tabCtrlGroupRegionTerminal.ResumeLayout(false);
            this.tabPageGroupRegion.ResumeLayout(false);
            this.tabPageGroupRegion.PerformLayout();
            this.tabPageTerminal.ResumeLayout(false);
            this.tabPageTerminal.PerformLayout();
            this.tabPageSchedule.ResumeLayout(false);
            this.tabPageSchedule.PerformLayout();
            this.panelSchedule.ResumeLayout(false);
            this.panelSchedule.PerformLayout();
            this.panelScheduleMonthly.ResumeLayout(false);
            this.panelScheduleWeekly.ResumeLayout(false);
            this.panelScheduleWeekly.PerformLayout();
            this.panelTabPages.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCtrlGroupRegionTerminal;
        private System.Windows.Forms.TabPage tabPageGroupRegion;
        private System.Windows.Forms.TabPage tabPageSchedule;
        private System.Windows.Forms.Panel panelTabPages;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtbGroupRegionDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtGroupRegionName;
        private System.Windows.Forms.TabPage tabPageTerminal;
        private System.Windows.Forms.RichTextBox rtbTerminalDescription;
        private System.Windows.Forms.CheckBox chkEnableDownloadSN;
        private System.Windows.Forms.TextBox txtTerminalName;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelSchedule;
        private System.Windows.Forms.DateTimePicker dateTimePickerClockMonthly;
        private System.Windows.Forms.ComboBox cmbMonthlyDate;
        private System.Windows.Forms.CheckBox chkEnableMonthly;
        private System.Windows.Forms.Panel panelScheduleWeekly;
        private System.Windows.Forms.DateTimePicker dateTimePickerClockWeekly;
        private System.Windows.Forms.CheckBox chkSaturday;
        private System.Windows.Forms.CheckBox chkFriday;
        private System.Windows.Forms.CheckBox chkThursday;
        private System.Windows.Forms.CheckBox chkWednesday;
        private System.Windows.Forms.CheckBox chkTuesday;
        private System.Windows.Forms.CheckBox chkMonday;
        private System.Windows.Forms.CheckBox chkSunday;
        private System.Windows.Forms.CheckBox chkEnableWeekly;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePickerClockDaily;
        private System.Windows.Forms.CheckBox chkEnableSchedule;
        private System.Windows.Forms.Panel panelScheduleMonthly;
        private System.Windows.Forms.ComboBox cmbTerminalType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbTerminalBrand;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkEnableDownloadGroupRegion;
        private System.Windows.Forms.Label lblSoftwareName;
        private System.Windows.Forms.ComboBox cmbSoftwareName;
        private System.Windows.Forms.Label lblSoftwareNameGroup;
        private System.Windows.Forms.ComboBox cmbSoftwareNameGroup;
    }
}