﻿namespace InSys
{
    partial class FrmTerminalType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTerminalType));
            this.dgvTerminalType = new System.Windows.Forms.DataGridView();
            this.lblTerminalTypeName = new System.Windows.Forms.Label();
            this.txtTerminalType = new System.Windows.Forms.TextBox();
            this.lblTerminalTypeDescription = new System.Windows.Forms.Label();
            this.txtTypeDescription = new System.Windows.Forms.TextBox();
            this.lblTerminalBrand = new System.Windows.Forms.Label();
            this.cmbTerminalBrand = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cmsTerminalType = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmsAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsDelete = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminalType)).BeginInit();
            this.cmsTerminalType.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvTerminalType
            // 
            this.dgvTerminalType.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvTerminalType.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvTerminalType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTerminalType.ContextMenuStrip = this.cmsTerminalType;
            this.dgvTerminalType.Location = new System.Drawing.Point(12, 12);
            this.dgvTerminalType.MultiSelect = false;
            this.dgvTerminalType.Name = "dgvTerminalType";
            this.dgvTerminalType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTerminalType.Size = new System.Drawing.Size(601, 186);
            this.dgvTerminalType.TabIndex = 0;
            // 
            // lblTerminalTypeName
            // 
            this.lblTerminalTypeName.AutoSize = true;
            this.lblTerminalTypeName.Location = new System.Drawing.Point(12, 211);
            this.lblTerminalTypeName.Name = "lblTerminalTypeName";
            this.lblTerminalTypeName.Size = new System.Drawing.Size(74, 13);
            this.lblTerminalTypeName.TabIndex = 1;
            this.lblTerminalTypeName.Text = "Terminal Type";
            // 
            // txtTerminalType
            // 
            this.txtTerminalType.Location = new System.Drawing.Point(109, 208);
            this.txtTerminalType.Name = "txtTerminalType";
            this.txtTerminalType.Size = new System.Drawing.Size(193, 20);
            this.txtTerminalType.TabIndex = 2;
            // 
            // lblTerminalTypeDescription
            // 
            this.lblTerminalTypeDescription.AutoSize = true;
            this.lblTerminalTypeDescription.Location = new System.Drawing.Point(13, 234);
            this.lblTerminalTypeDescription.Name = "lblTerminalTypeDescription";
            this.lblTerminalTypeDescription.Size = new System.Drawing.Size(87, 13);
            this.lblTerminalTypeDescription.TabIndex = 3;
            this.lblTerminalTypeDescription.Text = "Type Description";
            // 
            // txtTypeDescription
            // 
            this.txtTypeDescription.Location = new System.Drawing.Point(109, 234);
            this.txtTypeDescription.Multiline = true;
            this.txtTypeDescription.Name = "txtTypeDescription";
            this.txtTypeDescription.Size = new System.Drawing.Size(193, 76);
            this.txtTypeDescription.TabIndex = 4;
            // 
            // lblTerminalBrand
            // 
            this.lblTerminalBrand.AutoSize = true;
            this.lblTerminalBrand.Location = new System.Drawing.Point(321, 212);
            this.lblTerminalBrand.Name = "lblTerminalBrand";
            this.lblTerminalBrand.Size = new System.Drawing.Size(78, 13);
            this.lblTerminalBrand.TabIndex = 5;
            this.lblTerminalBrand.Text = "Terminal Brand";
            // 
            // cmbTerminalBrand
            // 
            this.cmbTerminalBrand.FormattingEnabled = true;
            this.cmbTerminalBrand.Location = new System.Drawing.Point(405, 209);
            this.cmbTerminalBrand.Name = "cmbTerminalBrand";
            this.cmbTerminalBrand.Size = new System.Drawing.Size(193, 21);
            this.cmbTerminalBrand.TabIndex = 6;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(523, 287);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(442, 287);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cmsTerminalType
            // 
            this.cmsTerminalType.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmsAdd,
            this.cmsDelete});
            this.cmsTerminalType.Name = "cmsTerminalType";
            this.cmsTerminalType.Size = new System.Drawing.Size(108, 48);
            // 
            // cmsAdd
            // 
            this.cmsAdd.Name = "cmsAdd";
            this.cmsAdd.Size = new System.Drawing.Size(107, 22);
            this.cmsAdd.Text = "Add";
            this.cmsAdd.Click += new System.EventHandler(this.cmsAdd_Click);
            // 
            // cmsDelete
            // 
            this.cmsDelete.Name = "cmsDelete";
            this.cmsDelete.Size = new System.Drawing.Size(107, 22);
            this.cmsDelete.Text = "Delete";
            this.cmsDelete.Click += new System.EventHandler(this.cmsDelete_Click);
            // 
            // frmTerminalType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 322);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.cmbTerminalBrand);
            this.Controls.Add(this.lblTerminalBrand);
            this.Controls.Add(this.txtTypeDescription);
            this.Controls.Add(this.lblTerminalTypeDescription);
            this.Controls.Add(this.txtTerminalType);
            this.Controls.Add(this.lblTerminalTypeName);
            this.Controls.Add(this.dgvTerminalType);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmTerminalType";
            this.Text = "frmTerminalType";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminalType)).EndInit();
            this.cmsTerminalType.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvTerminalType;
        private System.Windows.Forms.Label lblTerminalTypeName;
        private System.Windows.Forms.TextBox txtTerminalType;
        private System.Windows.Forms.Label lblTerminalTypeDescription;
        private System.Windows.Forms.TextBox txtTypeDescription;
        private System.Windows.Forms.Label lblTerminalBrand;
        private System.Windows.Forms.ComboBox cmbTerminalBrand;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ContextMenuStrip cmsTerminalType;
        private System.Windows.Forms.ToolStripMenuItem cmsAdd;
        private System.Windows.Forms.ToolStripMenuItem cmsDelete;
    }
}