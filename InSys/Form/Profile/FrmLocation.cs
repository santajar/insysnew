using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmLocation : Form
    {

        protected SqlConnection oSqlConn;
        protected bool isEdit;
        protected string sUserID;
        DataTable dtExcel = new DataTable();

        /// <summary>
        /// Form to view list of EDC's Location
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to datbabase</param>
        /// <param name="_sUserID">string : current active user</param>
        public FrmLocation(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            sUserID = _sUserID;
        }

        private void FrmLocation_Load(object sender, EventArgs e)
        {
            InitDisplay();
            InitButton();
            CommonClass.InputLog(oSqlConn, "", sUserID, "", CommonMessage.sFormOpened + "Location", "");
        }

        private void FrmLocation_Activated(object sender, EventArgs e)
        {
            //CommonClass.doMinimizeChild(this.ParentForm.MdiChildren, this.Name);
            //InitDisplay();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (IsValid())
                LoadData();
            else
                MessageBox.Show("Please select the Database Name and Location.");
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (ofdExcel.ShowDialog() == DialogResult.OK)
            {
                if (IsValidExcel(ofdExcel.FileName, ref dtExcel))
                {
                    SetDisplay(false);
                    bwImport.RunWorkerAsync(dtExcel);
                }
            }
        }

        private void bwImport_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (DataRow row in dtExcel.Rows)
                UpdateLocation(row["TerminalID"].ToString(), row["Location"].ToString());
        }

        private void bwImport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay(true);
            MessageBox.Show("Process Done.");
            InitDisplay();
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Import Locations", "");
        }

        #region "Function"
        protected string sDatabaseID { get { return cmbDatabase.SelectedValue.ToString(); } }
        protected string sLocationID { get { return cmbLocation.SelectedValue.ToString(); } }

        /// <summary>
        /// Initiate Comboboxes's items and forms's display
        /// </summary>
        protected void InitDisplay()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName",ref cmbDatabase);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPLocationBrowse, "", "LocationID", "LocationDesc",ref cmbLocation);
            this.WindowState = FormWindowState.Maximized;
        }

        private void InitButton()
        {
            //Privilege.Edit = Privelege.Import
            //btnImport.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.LO, Privilege.Edit);
        }

        /// <summary>
        /// Determine enable status of objects in form
        /// </summary>
        /// <param name="bEnable">boolean : true if enabled</param>
        protected void SetDisplay(bool bEnable)
        {
            gbInput.Enabled = bEnable;
            gbDataGrid.Enabled = bEnable;
            pbImport.Visible = !bEnable;
            pbImport.Style = (bEnable) ? ProgressBarStyle.Blocks : ProgressBarStyle.Marquee;
        }

        /// <summary>
        /// Determine whether data entered valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValid()
        {
            return (!string.IsNullOrEmpty(cmbDatabase.Text) && !string.IsNullOrEmpty(cmbLocation.Text)) ? true : false;
        }

        /// <summary>
        /// Bound DataGridView with DataTable
        /// </summary>
        protected void LoadData()
        {
            dgProfilingLocation.DataSource = dtProfilingLocation();
            CommonClass.InputLog(oSqlConn, "", sUserID, cmbDatabase.SelectedItem.ToString(), "Form Location - View Locations", "");
        }

        /// <summary>
        /// Load Location data from database then save in DataTable
        /// </summary>
        /// <returns>DataTable : Location Data</returns>
        protected DataTable dtProfilingLocation()
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPLocationBrowseWithTID, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@iDbId", SqlDbType.Int).Value = int.Parse(sDatabaseID);
            oSqlCmd.Parameters.Add("@iLocationId", SqlDbType.Int).Value = int.Parse(sLocationID);

            SqlDataReader oRead = oSqlCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);

            oRead.Close();
            oRead.Dispose();

            return dtTemp;
        }

        /// <summary>
        /// Determine whether excel's format valid or not
        /// </summary>
        /// <param name="sFilename">string : excel's file name</param>
        /// <param name="dtTemp">DataTable : where data from excel file stored</param>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValidExcel(string sFilename, ref DataTable dtTemp)
        {
            bool bValid = false;
            dtTemp = CommonClass.dtGetExcel(sFilename);
            if (dtTemp.Columns.Contains("TerminalID"))
                if (dtTemp.Columns.Contains("Location"))
                    bValid = true;
                else
                    MessageBox.Show("There is no 'LOCATION' collumn in Excel File");
            else
                MessageBox.Show("There is no 'TERMINALID' collumn in Excel File");
            return bValid;
        }

        /// <summary>
        /// Update data location in database
        /// </summary>
        /// <param name="sTerminalID">string : TerminalID</param>
        /// <param name="sLocationDesc">string : Location Description</param>
        protected void UpdateLocation(string sTerminalID, string sLocationDesc)
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPLocationUpdate, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
            oSqlCmd.Parameters.Add("@sLocationDesc", SqlDbType.VarChar).Value = sLocationDesc;

            oSqlCmd.ExecuteNonQuery();

            oSqlCmd.Dispose();
        }
        #endregion
    }
}