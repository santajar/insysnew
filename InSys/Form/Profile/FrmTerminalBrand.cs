﻿using InSysClass;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmTerminalBrand : Form
    {
        protected SqlConnection oSqlConn;
        int iisTrue = 0;
        string sBrandName, sBrandDescription;

        public FrmTerminalBrand(SqlConnection _oSqlConn)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            SetViewTerminalBrand(iisTrue);
            LoadTerminalBrand();
        }

        private void SetViewTerminalBrand(int isTrue)
        {
            if (isTrue == 0)
            {
                dgvTerminalBrand.Enabled = true;
                txtBrandName.Enabled = false;
                txtBrandDescription.Enabled = false;
                btnAdd.Enabled = false;
                btnCancel.Enabled = false;
                txtBrandName.Text = "";
                txtBrandDescription.Text = "";
            }
            else
            {
                dgvTerminalBrand.Enabled = false;
                txtBrandName.Enabled = true;
                txtBrandDescription.Enabled = true;
                btnAdd.Enabled = true;
                btnCancel.Enabled = true;
                txtBrandName.Text = "";
                txtBrandDescription.Text = "";
            }
        }

        private void DeleteTerminalBrand(int iSelectedTerminalBrandId)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalBrandDelete, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sBrandId", SqlDbType.VarChar).Value = iSelectedTerminalBrandId;
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
                
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            iisTrue = 0;
            SetViewTerminalBrand(iisTrue);
        }

        private void AddTerminalBrandtoDatabase(string sBrandName, string sBrandDescription)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalBrandInsert, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = sBrandName;
                oSqlCmd.Parameters.Add("@sDescription", SqlDbType.VarChar).Value = sBrandDescription;
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void LoadTerminalBrand()
        {
            try
            {
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                DataTable dtTerminalBrand = new DataTable();
                SqlDataAdapter sdaTerminalBrand = new SqlDataAdapter(CommonSP.sSPTerminalBrandBrowse, oSqlConn);
                sdaTerminalBrand.Fill(dtTerminalBrand);
                dgvTerminalBrand.DataSource = dtTerminalBrand;
               
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        protected bool IsRowSelected()
        {
            return dgvTerminalBrand.SelectedRows.Count > 0 ? true : false;
        }

        protected int iSelectedTerminalBrandId
        {
            get
            {
                int iRow = dgvTerminalBrand.SelectedRows[0].Index;
                return int.Parse(dgvTerminalBrand["Id", iRow].Value.ToString());
            }
        }

        private void addToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            iisTrue = 1;
            SetViewTerminalBrand(iisTrue);
        }

        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (IsRowSelected())
            {
                DeleteTerminalBrand(iSelectedTerminalBrandId);
                LoadTerminalBrand();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            sBrandName = txtBrandName.Text;
            sBrandDescription = txtBrandDescription.Text;
            if (BrandNameisExist(sBrandName))
            {
                if (!string.IsNullOrEmpty(sBrandName) && !string.IsNullOrEmpty(sBrandDescription))
                {
                    AddTerminalBrandtoDatabase(sBrandName, sBrandDescription);
                    LoadTerminalBrand();
                    iisTrue = 0;
                    SetViewTerminalBrand(iisTrue);
                }
                else MessageBox.Show("Please Fill Terminal Brand Name and Brand Description");
            }
            else MessageBox.Show("Brand Name is already Exist");
        }

        private bool BrandNameisExist(string sBrandName)
        {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalBrandBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionBrandName(sBrandName);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                SqlDataAdapter AdaptBrandName = new SqlDataAdapter(oSqlCmd);
                DataTable dtBrandName = new DataTable();
                AdaptBrandName.Fill(dtBrandName);
                if (dtBrandName.Rows.Count == 0)
                { 
                    return true;
                }else return false;
            
        }

        private object sConditionBrandName(string sBrandName)
        {
            return string.Format(" where TerminalBrandName = '{0}'", sBrandName);
        }
        
    }
}
