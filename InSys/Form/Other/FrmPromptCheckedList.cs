using System.Windows.Forms;

namespace InSys
{
    public partial class FrmPromptCheckedList : Form
    {
        /// <summary>
        /// PromptString Form constructor
        /// </summary>
        /// <param name="sTitle">string : Form Title</param>
        /// <param name="sLabel">string : Form Label</param>
        public FrmPromptCheckedList(string sTitle, string sLabel)
        {
            InitializeComponent();
            this.Text = sTitle;
            lblPrompt.Text = sLabel;
        }
    }
}