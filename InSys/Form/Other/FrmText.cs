using System;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmText : Form
    {
        /// <summary>
        /// Debug window constructor
        /// </summary>
        public FrmText()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close the form.
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}