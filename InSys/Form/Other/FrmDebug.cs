using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace InSys
{
    public partial class FrmDebug : Form
    {
        protected SqlConnection oSqlConn;
        public FrmDebug(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        /// <summary>
        /// Runs while form is loading, insert logs to Audit Trail.
        /// </summary>
        private void FrmDebug_Load(object sender, EventArgs e)
        {
            CommonClass.InputLog(oSqlConn, "", "", "", CommonMessage.sDebugAndTesting, "");
        }
    }
}