using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace InSys
{
    public partial class FrmModem : Form
    {
        /// <summary>
        /// Local SQL Connection variable
        /// </summary>
        protected SqlConnection oSqlConn;

        /// <summary>
        /// Form Modem Constructor
        /// </summary>
        /// <param name="_oSqlConn">SQLConnection : SQL Connection parameter</param>
        public FrmModem(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        private void FrmModem_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Close the form.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        /// <summary>
        /// Insert logs to Audit Trail.
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            CommonClass.InputLog(oSqlConn, "", "", "", CommonMessage.sSetModem, "");
        }
    }
}