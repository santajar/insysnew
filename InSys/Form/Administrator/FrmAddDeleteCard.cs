﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmAddDeleteCard : Form
    {
        private SqlConnection oSqlConn;
        private SqlConnection oSqlConnAuditTrail;
        private string sType;
        private string sDatabaseID;
        DataTable dtTempAcquirerList;
        DataTable dtTempCardList;
        DataTable dtTempIssuerList;


        public FrmAddDeleteCard(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail,string _sType)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sType = _sType;
            InitializeComponent();
        }

        private void FrmAddDeleteCard_Load(object sender, EventArgs e)
        {
            InitCmbDatabaseID();
            SetDisplay();
            
        }
        /// <summary>
        /// Initiate ComboBox, fill with list of Versions from database
        /// </summary>
        protected void InitCmbDatabaseID()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDatabaseID);
        }

        private void SetDisplay()
        {
            if (sType == "Add")
            {
                this.Text = "Form Add Card By Issuer and Database";
                btnAddDelete.Text = "Add";
            }
            else
            {
                this.Text = "Form Delete Card By Issuer and Database";
                btnAddDelete.Text = "Delete";
            }
        }

        private void cmbDatabaseID_SelectedIndexChanged(object sender, EventArgs e)
        {
            dtTempAcquirerList = new DataTable();
            dtTempCardList = new DataTable();
            dtTempIssuerList = new DataTable();

            if (!string.IsNullOrEmpty(cmbDatabaseID.DisplayMember) && !string.IsNullOrEmpty(cmbDatabaseID.ValueMember) && cmbDatabaseID.SelectedIndex >-1)
            {
                sDatabaseID = cmbDatabaseID.SelectedValue.ToString();
                gbIssuerCard.Enabled = true;

                dtTempCardList = GetcmbCardList();
                if (dtTempCardList.Rows.Count > 0)
                {
                    cmbCardName.DataSource = dtTempCardList;
                    cmbCardName.DisplayMember = "Name";
                    cmbCardName.ValueMember = "Name";
                    cmbCardName.SelectedIndex = -1;
                }

                dtTempIssuerList = GetcmbIssuerList();
                if (dtTempIssuerList.Rows.Count > 0)
                {
                    cmbIssuer.DataSource = dtTempIssuerList;
                    cmbIssuer.DisplayMember = "IssuerName";
                    cmbIssuer.ValueMember = "IssuerName";
                    cmbIssuer.SelectedIndex = -1;
                }

                dtTempAcquirerList = GetcmbAcquirerList();
                if (dtTempAcquirerList.Rows.Count > 0)
                {
                    cmbAcquirer.DataSource = dtTempAcquirerList;
                    cmbAcquirer.DisplayMember = "AcquirerName";
                    cmbAcquirer.ValueMember = "AcquirerName";
                    cmbAcquirer.SelectedIndex = -1;
                }
            }
        }

        private DataTable GetcmbAcquirerList()
        {
            DataTable dTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(string.Format("SELECT DISTINCT AcquirerName FROM tbProfileAcquirer WITH (NOLOCK) WHERE AcquirerTag IN ('AA01','AA001') AND TerminalID IN (SELECT TerminalID FROM tbProfileTerminalList WHERE DatabaseID = '{0}') ORDER BY AcquirerName", sDatabaseID), oSqlConn);
            new SqlDataAdapter(oCmd).Fill(dTemp);

            return dTemp;
        }

        private DataTable GetcmbIssuerList()
        {
            DataTable dTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(string.Format("SELECT DISTINCT IssuerName FROM tbProfileIssuer WITH (NOLOCK) WHERE IssuerTag IN ('AE01','AE001') AND TerminalID IN (SELECT TerminalID FROM tbProfileTerminalList WHERE DatabaseID = '{0}') ORDER BY IssuerName", sDatabaseID), oSqlConn);
            new SqlDataAdapter(oCmd).Fill(dTemp);

            return dTemp;
        }

        private DataTable GetcmbCardList()
        {
            DataTable dTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPCardListBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDatabaseID;
            new SqlDataAdapter(oCmd).Fill(dTemp);

            return dTemp;
        }

        private void btnAddDelete_Click(object sender, EventArgs e)
        {
            if (cmbIssuer.SelectedIndex > -1 && cmbCardName.SelectedIndex > -1)
            {
                if (sType == "Add")
                    AddCardRelation();
                else
                    DeleteCardRelation();
                this.Close();
                this.Dispose();
            }
            else
                MessageBox.Show("Please choose IssuerName and Card Name");
        }

        private void DeleteCardRelation()
        {
            SqlCommand oCmd = new SqlCommand("spProfileCardRelationDeletebyIssuerAndDatabaseID", oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseID",SqlDbType.VarChar).Value = sDatabaseID;
            oCmd.Parameters.Add("@sIssuerName",SqlDbType.VarChar).Value = cmbIssuer.SelectedValue.ToString();
            oCmd.Parameters.Add("@sCardName",SqlDbType.VarChar).Value = cmbCardName.SelectedValue.ToString();
            oCmd.Parameters.Add("@sAcquirerName", SqlDbType.VarChar).Value = cmbAcquirer.SelectedValue.ToString();
            oCmd.ExecuteNonQuery();
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, cmbDatabaseID.SelectedText.ToString(), "Delete Card by Database : " + cmbCardName.SelectedValue.ToString(), "");
        }

        private void AddCardRelation()
        {
            SqlCommand oCmd = new SqlCommand("spProfileCardRelationAddbyIssuerAndDatabaseID", oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sDatabaseID;
            oCmd.Parameters.Add("@sIssuerName", SqlDbType.VarChar).Value = cmbIssuer.SelectedValue.ToString();
            oCmd.Parameters.Add("@sCardName", SqlDbType.VarChar).Value = cmbCardName.SelectedValue.ToString();
            oCmd.Parameters.Add("@sAcquirerName", SqlDbType.VarChar).Value = cmbAcquirer.SelectedValue.ToString();
            oCmd.ExecuteNonQuery();
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, cmbDatabaseID.SelectedText.ToString(), "Delete Card by Database : " + cmbCardName.SelectedValue.ToString(), "");
        }
    }
}
