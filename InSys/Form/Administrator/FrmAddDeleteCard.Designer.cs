﻿namespace InSys
{
    partial class FrmAddDeleteCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAddDeleteCard));
            this.lblDatabaseName = new System.Windows.Forms.Label();
            this.lblIssuer = new System.Windows.Forms.Label();
            this.lblCardName = new System.Windows.Forms.Label();
            this.cmbDatabaseID = new System.Windows.Forms.ComboBox();
            this.cmbIssuer = new System.Windows.Forms.ComboBox();
            this.cmbCardName = new System.Windows.Forms.ComboBox();
            this.btnAddDelete = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbIssuerCard = new System.Windows.Forms.GroupBox();
            this.cmbAcquirer = new System.Windows.Forms.ComboBox();
            this.lblAcquirer = new System.Windows.Forms.Label();
            this.gbIssuerCard.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDatabaseName
            // 
            this.lblDatabaseName.AutoSize = true;
            this.lblDatabaseName.Location = new System.Drawing.Point(10, 11);
            this.lblDatabaseName.Name = "lblDatabaseName";
            this.lblDatabaseName.Size = new System.Drawing.Size(81, 13);
            this.lblDatabaseName.TabIndex = 0;
            this.lblDatabaseName.Text = "DatabaseName";
            // 
            // lblIssuer
            // 
            this.lblIssuer.AutoSize = true;
            this.lblIssuer.Location = new System.Drawing.Point(9, 41);
            this.lblIssuer.Name = "lblIssuer";
            this.lblIssuer.Size = new System.Drawing.Size(35, 13);
            this.lblIssuer.TabIndex = 1;
            this.lblIssuer.Text = "Issuer";
            // 
            // lblCardName
            // 
            this.lblCardName.AutoSize = true;
            this.lblCardName.Location = new System.Drawing.Point(9, 67);
            this.lblCardName.Name = "lblCardName";
            this.lblCardName.Size = new System.Drawing.Size(57, 13);
            this.lblCardName.TabIndex = 2;
            this.lblCardName.Text = "CardName";
            // 
            // cmbDatabaseID
            // 
            this.cmbDatabaseID.FormattingEnabled = true;
            this.cmbDatabaseID.Location = new System.Drawing.Point(97, 8);
            this.cmbDatabaseID.Name = "cmbDatabaseID";
            this.cmbDatabaseID.Size = new System.Drawing.Size(166, 21);
            this.cmbDatabaseID.TabIndex = 3;
            this.cmbDatabaseID.SelectedIndexChanged += new System.EventHandler(this.cmbDatabaseID_SelectedIndexChanged);
            // 
            // cmbIssuer
            // 
            this.cmbIssuer.FormattingEnabled = true;
            this.cmbIssuer.Location = new System.Drawing.Point(96, 39);
            this.cmbIssuer.Name = "cmbIssuer";
            this.cmbIssuer.Size = new System.Drawing.Size(148, 21);
            this.cmbIssuer.TabIndex = 4;
            // 
            // cmbCardName
            // 
            this.cmbCardName.FormattingEnabled = true;
            this.cmbCardName.Location = new System.Drawing.Point(96, 63);
            this.cmbCardName.Name = "cmbCardName";
            this.cmbCardName.Size = new System.Drawing.Size(148, 21);
            this.cmbCardName.TabIndex = 5;
            // 
            // btnAddDelete
            // 
            this.btnAddDelete.Location = new System.Drawing.Point(88, 90);
            this.btnAddDelete.Name = "btnAddDelete";
            this.btnAddDelete.Size = new System.Drawing.Size(75, 23);
            this.btnAddDelete.TabIndex = 6;
            this.btnAddDelete.Text = "button1";
            this.btnAddDelete.UseVisualStyleBackColor = true;
            this.btnAddDelete.Click += new System.EventHandler(this.btnAddDelete_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(169, 90);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // gbIssuerCard
            // 
            this.gbIssuerCard.Controls.Add(this.cmbAcquirer);
            this.gbIssuerCard.Controls.Add(this.lblAcquirer);
            this.gbIssuerCard.Controls.Add(this.cmbIssuer);
            this.gbIssuerCard.Controls.Add(this.btnCancel);
            this.gbIssuerCard.Controls.Add(this.lblIssuer);
            this.gbIssuerCard.Controls.Add(this.btnAddDelete);
            this.gbIssuerCard.Controls.Add(this.lblCardName);
            this.gbIssuerCard.Controls.Add(this.cmbCardName);
            this.gbIssuerCard.Enabled = false;
            this.gbIssuerCard.Location = new System.Drawing.Point(9, 31);
            this.gbIssuerCard.Name = "gbIssuerCard";
            this.gbIssuerCard.Size = new System.Drawing.Size(254, 117);
            this.gbIssuerCard.TabIndex = 8;
            this.gbIssuerCard.TabStop = false;
            // 
            // cmbAcquirer
            // 
            this.cmbAcquirer.FormattingEnabled = true;
            this.cmbAcquirer.Location = new System.Drawing.Point(96, 16);
            this.cmbAcquirer.Name = "cmbAcquirer";
            this.cmbAcquirer.Size = new System.Drawing.Size(148, 21);
            this.cmbAcquirer.TabIndex = 9;
            // 
            // lblAcquirer
            // 
            this.lblAcquirer.AutoSize = true;
            this.lblAcquirer.Location = new System.Drawing.Point(9, 18);
            this.lblAcquirer.Name = "lblAcquirer";
            this.lblAcquirer.Size = new System.Drawing.Size(46, 13);
            this.lblAcquirer.TabIndex = 8;
            this.lblAcquirer.Text = "Acquirer";
            // 
            // FrmAddDeleteCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(271, 160);
            this.Controls.Add(this.gbIssuerCard);
            this.Controls.Add(this.cmbDatabaseID);
            this.Controls.Add(this.lblDatabaseName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAddDeleteCard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmAddDeleteCard";
            this.Load += new System.EventHandler(this.FrmAddDeleteCard_Load);
            this.gbIssuerCard.ResumeLayout(false);
            this.gbIssuerCard.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDatabaseName;
        private System.Windows.Forms.Label lblIssuer;
        private System.Windows.Forms.Label lblCardName;
        private System.Windows.Forms.ComboBox cmbDatabaseID;
        private System.Windows.Forms.ComboBox cmbIssuer;
        private System.Windows.Forms.ComboBox cmbCardName;
        private System.Windows.Forms.Button btnAddDelete;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox gbIssuerCard;
        private System.Windows.Forms.ComboBox cmbAcquirer;
        private System.Windows.Forms.Label lblAcquirer;
    }
}