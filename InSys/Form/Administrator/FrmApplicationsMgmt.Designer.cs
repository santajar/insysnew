namespace InSys
{
    partial class FrmApplicationsMgmt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmApplicationsMgmt));
            this.gbListApplication = new System.Windows.Forms.GroupBox();
            this.lbApplicationName = new System.Windows.Forms.ListBox();
            this.gbDetailData = new System.Windows.Forms.GroupBox();
            this.txtHexBitMap = new System.Windows.Forms.TextBox();
            this.txtBitMap = new System.Windows.Forms.TextBox();
            this.rdAllowCompressNo = new System.Windows.Forms.RadioButton();
            this.rdAllowCompressYes = new System.Windows.Forms.RadioButton();
            this.txtApplicationName = new System.Windows.Forms.TextBox();
            this.lblHexBitMap = new System.Windows.Forms.Label();
            this.lblBitMap = new System.Windows.Forms.Label();
            this.lblAllowCompress = new System.Windows.Forms.Label();
            this.lblAppsName = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chkStandartISO8583 = new System.Windows.Forms.CheckBox();
            this.gbListApplication.SuspendLayout();
            this.gbDetailData.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbListApplication
            // 
            this.gbListApplication.Controls.Add(this.lbApplicationName);
            this.gbListApplication.Location = new System.Drawing.Point(10, 2);
            this.gbListApplication.Name = "gbListApplication";
            this.gbListApplication.Size = new System.Drawing.Size(181, 175);
            this.gbListApplication.TabIndex = 0;
            this.gbListApplication.TabStop = false;
            // 
            // lbApplicationName
            // 
            this.lbApplicationName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbApplicationName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbApplicationName.FormattingEnabled = true;
            this.lbApplicationName.ItemHeight = 15;
            this.lbApplicationName.Location = new System.Drawing.Point(8, 12);
            this.lbApplicationName.Name = "lbApplicationName";
            this.lbApplicationName.ScrollAlwaysVisible = true;
            this.lbApplicationName.Size = new System.Drawing.Size(166, 154);
            this.lbApplicationName.TabIndex = 0;
            this.lbApplicationName.SelectedIndexChanged += new System.EventHandler(this.lbApplicationName_SelectedIndexChanged);
            // 
            // gbDetailData
            // 
            this.gbDetailData.Controls.Add(this.chkStandartISO8583);
            this.gbDetailData.Controls.Add(this.txtHexBitMap);
            this.gbDetailData.Controls.Add(this.txtBitMap);
            this.gbDetailData.Controls.Add(this.rdAllowCompressNo);
            this.gbDetailData.Controls.Add(this.rdAllowCompressYes);
            this.gbDetailData.Controls.Add(this.txtApplicationName);
            this.gbDetailData.Controls.Add(this.lblHexBitMap);
            this.gbDetailData.Controls.Add(this.lblBitMap);
            this.gbDetailData.Controls.Add(this.lblAllowCompress);
            this.gbDetailData.Controls.Add(this.lblAppsName);
            this.gbDetailData.Location = new System.Drawing.Point(197, 2);
            this.gbDetailData.Name = "gbDetailData";
            this.gbDetailData.Size = new System.Drawing.Size(371, 175);
            this.gbDetailData.TabIndex = 1;
            this.gbDetailData.TabStop = false;
            // 
            // txtHexBitMap
            // 
            this.txtHexBitMap.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHexBitMap.Location = new System.Drawing.Point(112, 124);
            this.txtHexBitMap.MaxLength = 16;
            this.txtHexBitMap.Name = "txtHexBitMap";
            this.txtHexBitMap.Size = new System.Drawing.Size(253, 20);
            this.txtHexBitMap.TabIndex = 8;
            // 
            // txtBitMap
            // 
            this.txtBitMap.Location = new System.Drawing.Point(112, 68);
            this.txtBitMap.MaxLength = 64;
            this.txtBitMap.Multiline = true;
            this.txtBitMap.Name = "txtBitMap";
            this.txtBitMap.Size = new System.Drawing.Size(253, 50);
            this.txtBitMap.TabIndex = 6;
            // 
            // rdAllowCompressNo
            // 
            this.rdAllowCompressNo.AutoSize = true;
            this.rdAllowCompressNo.Checked = true;
            this.rdAllowCompressNo.Location = new System.Drawing.Point(161, 45);
            this.rdAllowCompressNo.Name = "rdAllowCompressNo";
            this.rdAllowCompressNo.Size = new System.Drawing.Size(39, 17);
            this.rdAllowCompressNo.TabIndex = 4;
            this.rdAllowCompressNo.TabStop = true;
            this.rdAllowCompressNo.Text = "No";
            this.rdAllowCompressNo.UseVisualStyleBackColor = true;
            // 
            // rdAllowCompressYes
            // 
            this.rdAllowCompressYes.AutoSize = true;
            this.rdAllowCompressYes.Location = new System.Drawing.Point(112, 45);
            this.rdAllowCompressYes.Name = "rdAllowCompressYes";
            this.rdAllowCompressYes.Size = new System.Drawing.Size(43, 17);
            this.rdAllowCompressYes.TabIndex = 3;
            this.rdAllowCompressYes.Text = "Yes";
            this.rdAllowCompressYes.UseVisualStyleBackColor = true;
            // 
            // txtApplicationName
            // 
            this.txtApplicationName.Location = new System.Drawing.Point(112, 19);
            this.txtApplicationName.MaxLength = 25;
            this.txtApplicationName.Name = "txtApplicationName";
            this.txtApplicationName.Size = new System.Drawing.Size(253, 20);
            this.txtApplicationName.TabIndex = 1;
            // 
            // lblHexBitMap
            // 
            this.lblHexBitMap.AutoSize = true;
            this.lblHexBitMap.Location = new System.Drawing.Point(6, 127);
            this.lblHexBitMap.Name = "lblHexBitMap";
            this.lblHexBitMap.Size = new System.Drawing.Size(65, 13);
            this.lblHexBitMap.TabIndex = 7;
            this.lblHexBitMap.Text = "Hex Bit Map";
            // 
            // lblBitMap
            // 
            this.lblBitMap.AutoSize = true;
            this.lblBitMap.Location = new System.Drawing.Point(6, 71);
            this.lblBitMap.Name = "lblBitMap";
            this.lblBitMap.Size = new System.Drawing.Size(43, 13);
            this.lblBitMap.TabIndex = 5;
            this.lblBitMap.Text = "Bit Map";
            // 
            // lblAllowCompress
            // 
            this.lblAllowCompress.AutoSize = true;
            this.lblAllowCompress.Location = new System.Drawing.Point(6, 47);
            this.lblAllowCompress.Name = "lblAllowCompress";
            this.lblAllowCompress.Size = new System.Drawing.Size(81, 13);
            this.lblAllowCompress.TabIndex = 2;
            this.lblAllowCompress.Text = "Allow Compress";
            // 
            // lblAppsName
            // 
            this.lblAppsName.AutoSize = true;
            this.lblAppsName.Location = new System.Drawing.Point(6, 22);
            this.lblAppsName.Name = "lblAppsName";
            this.lblAppsName.Size = new System.Drawing.Size(90, 13);
            this.lblAppsName.TabIndex = 0;
            this.lblAppsName.Text = "Application Name";
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCopy);
            this.gbButton.Controls.Add(this.btnAdd);
            this.gbButton.Controls.Add(this.btnEdit);
            this.gbButton.Controls.Add(this.btnDelete);
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Location = new System.Drawing.Point(10, 183);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(558, 50);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnCopy
            // 
            this.btnCopy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCopy.Location = new System.Drawing.Point(226, 17);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(105, 23);
            this.btnCopy.TabIndex = 4;
            this.btnCopy.Text = "Copy";
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.Location = new System.Drawing.Point(8, 17);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(105, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(117, 17);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(105, 23);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(335, 17);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(105, 23);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(444, 17);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(105, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chkStandartISO8583
            // 
            this.chkStandartISO8583.AutoSize = true;
            this.chkStandartISO8583.Location = new System.Drawing.Point(112, 152);
            this.chkStandartISO8583.Name = "chkStandartISO8583";
            this.chkStandartISO8583.Size = new System.Drawing.Size(111, 17);
            this.chkStandartISO8583.TabIndex = 9;
            this.chkStandartISO8583.Text = "Standart ISO8583";
            this.chkStandartISO8583.UseVisualStyleBackColor = true;
            // 
            // FrmApplicationsMgmt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 245);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbDetailData);
            this.Controls.Add(this.gbListApplication);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmApplicationsMgmt";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Application Management";
            this.Load += new System.EventHandler(this.FrmApplications_Load);
            this.gbListApplication.ResumeLayout(false);
            this.gbDetailData.ResumeLayout(false);
            this.gbDetailData.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbListApplication;
        private System.Windows.Forms.GroupBox gbDetailData;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblHexBitMap;
        private System.Windows.Forms.Label lblBitMap;
        private System.Windows.Forms.Label lblAllowCompress;
        private System.Windows.Forms.Label lblAppsName;
        private System.Windows.Forms.TextBox txtHexBitMap;
        private System.Windows.Forms.TextBox txtBitMap;
        private System.Windows.Forms.RadioButton rdAllowCompressNo;
        private System.Windows.Forms.RadioButton rdAllowCompressYes;
        private System.Windows.Forms.TextBox txtApplicationName;
        private System.Windows.Forms.ListBox lbApplicationName;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.Button btnEdit;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.CheckBox chkStandartISO8583;
    }
}