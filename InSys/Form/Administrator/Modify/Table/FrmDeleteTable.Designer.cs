namespace InSys
{
    partial class FrmDeleteTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDeleteTable));
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.chkIncludeMs = new System.Windows.Forms.CheckBox();
            this.cmbTerminal = new System.Windows.Forms.ComboBox();
            this.cmbDbSource = new System.Windows.Forms.ComboBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.lblDbSource = new System.Windows.Forms.Label();
            this.gbCard = new System.Windows.Forms.GroupBox();
            this.txtAcquirer = new System.Windows.Forms.TextBox();
            this.txtIssuer = new System.Windows.Forms.TextBox();
            this.chkAcquirer = new System.Windows.Forms.CheckBox();
            this.chkIssuer = new System.Windows.Forms.CheckBox();
            this.cmbCardName = new System.Windows.Forms.ComboBox();
            this.lblCardName = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.gbSource.SuspendLayout();
            this.gbCard.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSource
            // 
            this.gbSource.Controls.Add(this.chkIncludeMs);
            this.gbSource.Controls.Add(this.cmbTerminal);
            this.gbSource.Controls.Add(this.cmbDbSource);
            this.gbSource.Controls.Add(this.lblTerminal);
            this.gbSource.Controls.Add(this.lblDbSource);
            this.gbSource.Location = new System.Drawing.Point(12, 12);
            this.gbSource.Name = "gbSource";
            this.gbSource.Size = new System.Drawing.Size(275, 119);
            this.gbSource.TabIndex = 1;
            this.gbSource.TabStop = false;
            this.gbSource.Text = "Source";
            // 
            // chkIncludeMs
            // 
            this.chkIncludeMs.AutoSize = true;
            this.chkIncludeMs.Location = new System.Drawing.Point(19, 90);
            this.chkIncludeMs.Name = "chkIncludeMs";
            this.chkIncludeMs.Size = new System.Drawing.Size(96, 17);
            this.chkIncludeMs.TabIndex = 4;
            this.chkIncludeMs.Text = "Include Master";
            this.chkIncludeMs.UseVisualStyleBackColor = true;
            // 
            // cmbTerminal
            // 
            this.cmbTerminal.FormattingEnabled = true;
            this.cmbTerminal.Location = new System.Drawing.Point(97, 60);
            this.cmbTerminal.Name = "cmbTerminal";
            this.cmbTerminal.Size = new System.Drawing.Size(160, 21);
            this.cmbTerminal.TabIndex = 3;
            // 
            // cmbDbSource
            // 
            this.cmbDbSource.FormattingEnabled = true;
            this.cmbDbSource.Location = new System.Drawing.Point(97, 24);
            this.cmbDbSource.Name = "cmbDbSource";
            this.cmbDbSource.Size = new System.Drawing.Size(160, 21);
            this.cmbDbSource.TabIndex = 2;
            // 
            // lblTerminal
            // 
            this.lblTerminal.AutoSize = true;
            this.lblTerminal.Location = new System.Drawing.Point(16, 63);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(47, 13);
            this.lblTerminal.TabIndex = 1;
            this.lblTerminal.Text = "Terminal";
            // 
            // lblDbSource
            // 
            this.lblDbSource.AutoSize = true;
            this.lblDbSource.Location = new System.Drawing.Point(16, 27);
            this.lblDbSource.Name = "lblDbSource";
            this.lblDbSource.Size = new System.Drawing.Size(53, 13);
            this.lblDbSource.TabIndex = 0;
            this.lblDbSource.Text = "Database";
            // 
            // gbCard
            // 
            this.gbCard.Controls.Add(this.txtAcquirer);
            this.gbCard.Controls.Add(this.txtIssuer);
            this.gbCard.Controls.Add(this.chkAcquirer);
            this.gbCard.Controls.Add(this.chkIssuer);
            this.gbCard.Controls.Add(this.cmbCardName);
            this.gbCard.Controls.Add(this.lblCardName);
            this.gbCard.Location = new System.Drawing.Point(293, 12);
            this.gbCard.Name = "gbCard";
            this.gbCard.Size = new System.Drawing.Size(275, 119);
            this.gbCard.TabIndex = 2;
            this.gbCard.TabStop = false;
            this.gbCard.Text = "Card";
            // 
            // txtAcquirer
            // 
            this.txtAcquirer.Location = new System.Drawing.Point(100, 88);
            this.txtAcquirer.Name = "txtAcquirer";
            this.txtAcquirer.Size = new System.Drawing.Size(160, 20);
            this.txtAcquirer.TabIndex = 8;
            // 
            // txtIssuer
            // 
            this.txtIssuer.Location = new System.Drawing.Point(100, 58);
            this.txtIssuer.Name = "txtIssuer";
            this.txtIssuer.Size = new System.Drawing.Size(160, 20);
            this.txtIssuer.TabIndex = 6;
            // 
            // chkAcquirer
            // 
            this.chkAcquirer.AutoSize = true;
            this.chkAcquirer.Location = new System.Drawing.Point(18, 90);
            this.chkAcquirer.Name = "chkAcquirer";
            this.chkAcquirer.Size = new System.Drawing.Size(65, 17);
            this.chkAcquirer.TabIndex = 7;
            this.chkAcquirer.Text = "Acquirer";
            this.chkAcquirer.UseVisualStyleBackColor = true;
            // 
            // chkIssuer
            // 
            this.chkIssuer.AutoSize = true;
            this.chkIssuer.Location = new System.Drawing.Point(18, 60);
            this.chkIssuer.Name = "chkIssuer";
            this.chkIssuer.Size = new System.Drawing.Size(54, 17);
            this.chkIssuer.TabIndex = 5;
            this.chkIssuer.Text = "Issuer";
            this.chkIssuer.UseVisualStyleBackColor = true;
            // 
            // cmbCardName
            // 
            this.cmbCardName.FormattingEnabled = true;
            this.cmbCardName.Location = new System.Drawing.Point(100, 24);
            this.cmbCardName.Name = "cmbCardName";
            this.cmbCardName.Size = new System.Drawing.Size(160, 21);
            this.cmbCardName.TabIndex = 3;
            // 
            // lblCardName
            // 
            this.lblCardName.AutoSize = true;
            this.lblCardName.Location = new System.Drawing.Point(15, 27);
            this.lblCardName.Name = "lblCardName";
            this.lblCardName.Size = new System.Drawing.Size(60, 13);
            this.lblCardName.TabIndex = 1;
            this.lblCardName.Text = "Card Name";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 137);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(556, 23);
            this.progressBar1.TabIndex = 3;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnStart);
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Controls.Add(this.btnReset);
            this.gbButton.Location = new System.Drawing.Point(12, 166);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(561, 41);
            this.gbButton.TabIndex = 4;
            this.gbButton.TabStop = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(243, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "&Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(455, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(349, 12);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(100, 23);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "&Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            // 
            // FrmDeleteTable
            // 
            this.AcceptButton = this.btnStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(582, 222);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.gbCard);
            this.Controls.Add(this.gbSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmDeleteTable";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Delete Table";
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.gbCard.ResumeLayout(false);
            this.gbCard.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.CheckBox chkIncludeMs;
        private System.Windows.Forms.ComboBox cmbTerminal;
        private System.Windows.Forms.ComboBox cmbDbSource;
        private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.Label lblDbSource;
        private System.Windows.Forms.GroupBox gbCard;
        private System.Windows.Forms.CheckBox chkAcquirer;
        private System.Windows.Forms.CheckBox chkIssuer;
        private System.Windows.Forms.ComboBox cmbCardName;
        private System.Windows.Forms.Label lblCardName;
        private System.Windows.Forms.TextBox txtIssuer;
        private System.Windows.Forms.TextBox txtAcquirer;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnStart;
    }
}