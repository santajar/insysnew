namespace InSys
{
    partial class FrmDeleteField
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDeleteField));
            this.tabFields = new System.Windows.Forms.TabControl();
            this.tabTerminal = new System.Windows.Forms.TabPage();
            this.btnDEAddTag = new System.Windows.Forms.Button();
            this.txtDEValue1 = new System.Windows.Forms.TextBox();
            this.txtDETag1 = new System.Windows.Forms.TextBox();
            this.lblDEValue = new System.Windows.Forms.Label();
            this.lblDETag = new System.Windows.Forms.Label();
            this.gbTerminalCondition = new System.Windows.Forms.GroupBox();
            this.chkDECard = new System.Windows.Forms.CheckBox();
            this.chkDEIssuer = new System.Windows.Forms.CheckBox();
            this.chkDEAcquirer = new System.Windows.Forms.CheckBox();
            this.txtDECardValue = new System.Windows.Forms.TextBox();
            this.txtDECardTag = new System.Windows.Forms.TextBox();
            this.txtDEIssuerValue = new System.Windows.Forms.TextBox();
            this.txtDEIssuerTag = new System.Windows.Forms.TextBox();
            this.txtDEAcquirerValue = new System.Windows.Forms.TextBox();
            this.txtDEAcquirerTag = new System.Windows.Forms.TextBox();
            this.txtDETerminalValue = new System.Windows.Forms.TextBox();
            this.txtDeTerminalTag = new System.Windows.Forms.TextBox();
            this.chkDETermintal = new System.Windows.Forms.CheckBox();
            this.tabAcquirer = new System.Windows.Forms.TabPage();
            this.btnAAAddTag = new System.Windows.Forms.Button();
            this.txtAAValue1 = new System.Windows.Forms.TextBox();
            this.txtAATag1 = new System.Windows.Forms.TextBox();
            this.lblAAValue = new System.Windows.Forms.Label();
            this.lblAATag = new System.Windows.Forms.Label();
            this.gbAcquirerCondition = new System.Windows.Forms.GroupBox();
            this.chkAACard = new System.Windows.Forms.CheckBox();
            this.chkAAIssuer = new System.Windows.Forms.CheckBox();
            this.chkAAAcquirer = new System.Windows.Forms.CheckBox();
            this.txtAACardValue = new System.Windows.Forms.TextBox();
            this.txtAACardTag = new System.Windows.Forms.TextBox();
            this.txtAAIssuerValue = new System.Windows.Forms.TextBox();
            this.txtAAIssuerTag = new System.Windows.Forms.TextBox();
            this.txtAAAcquirerValue = new System.Windows.Forms.TextBox();
            this.txtAAAcquirerTag = new System.Windows.Forms.TextBox();
            this.txtAATerminalValue = new System.Windows.Forms.TextBox();
            this.txtAATerminalTag = new System.Windows.Forms.TextBox();
            this.chkAATerminal = new System.Windows.Forms.CheckBox();
            this.tabIssuer = new System.Windows.Forms.TabPage();
            this.btnAEAddFields = new System.Windows.Forms.Button();
            this.txtAEValue1 = new System.Windows.Forms.TextBox();
            this.txtAETag1 = new System.Windows.Forms.TextBox();
            this.lblAEValue1 = new System.Windows.Forms.Label();
            this.lblAETag = new System.Windows.Forms.Label();
            this.gbIssuerCondition = new System.Windows.Forms.GroupBox();
            this.chkAECard = new System.Windows.Forms.CheckBox();
            this.chkAEIssuer = new System.Windows.Forms.CheckBox();
            this.chkAEAcquirer = new System.Windows.Forms.CheckBox();
            this.txtAECardValue = new System.Windows.Forms.TextBox();
            this.txtAECardTag = new System.Windows.Forms.TextBox();
            this.txtAEIssuerValue = new System.Windows.Forms.TextBox();
            this.txtAEIssuerTag = new System.Windows.Forms.TextBox();
            this.txtAEAcquirerValue = new System.Windows.Forms.TextBox();
            this.txtAEAcquirerTag = new System.Windows.Forms.TextBox();
            this.txtAETerminalValue = new System.Windows.Forms.TextBox();
            this.txtAETerminalTag = new System.Windows.Forms.TextBox();
            this.chkAETerminal = new System.Windows.Forms.CheckBox();
            this.tabCard = new System.Windows.Forms.TabPage();
            this.btnACAddFields = new System.Windows.Forms.Button();
            this.txtACValue1 = new System.Windows.Forms.TextBox();
            this.txtACTag1 = new System.Windows.Forms.TextBox();
            this.lblACValue = new System.Windows.Forms.Label();
            this.lblACTag = new System.Windows.Forms.Label();
            this.gbCardCondition = new System.Windows.Forms.GroupBox();
            this.chkACCard = new System.Windows.Forms.CheckBox();
            this.chkACIssuer = new System.Windows.Forms.CheckBox();
            this.chkACAcquirer = new System.Windows.Forms.CheckBox();
            this.txtACCardValue = new System.Windows.Forms.TextBox();
            this.txtACCardTag = new System.Windows.Forms.TextBox();
            this.txtACIssuerValue = new System.Windows.Forms.TextBox();
            this.txtACIssuerTag = new System.Windows.Forms.TextBox();
            this.txtACAcquirerValue = new System.Windows.Forms.TextBox();
            this.txtACAcquireTag = new System.Windows.Forms.TextBox();
            this.txtACTerminalValue = new System.Windows.Forms.TextBox();
            this.txtACTerminalTag = new System.Windows.Forms.TextBox();
            this.chkACTerminal = new System.Windows.Forms.CheckBox();
            this.txtKeterangan = new System.Windows.Forms.TextBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnExecuteAll = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.pbAddFields = new System.Windows.Forms.ProgressBar();
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.chkIncludeMs = new System.Windows.Forms.CheckBox();
            this.cmbTerminal = new System.Windows.Forms.ComboBox();
            this.cmbDbSource = new System.Windows.Forms.ComboBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.lblDbSource = new System.Windows.Forms.Label();
            this.tabFields.SuspendLayout();
            this.tabTerminal.SuspendLayout();
            this.gbTerminalCondition.SuspendLayout();
            this.tabAcquirer.SuspendLayout();
            this.gbAcquirerCondition.SuspendLayout();
            this.tabIssuer.SuspendLayout();
            this.gbIssuerCondition.SuspendLayout();
            this.tabCard.SuspendLayout();
            this.gbCardCondition.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.gbSource.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabFields
            // 
            this.tabFields.Controls.Add(this.tabTerminal);
            this.tabFields.Controls.Add(this.tabAcquirer);
            this.tabFields.Controls.Add(this.tabIssuer);
            this.tabFields.Controls.Add(this.tabCard);
            this.tabFields.Location = new System.Drawing.Point(10, 164);
            this.tabFields.Name = "tabFields";
            this.tabFields.SelectedIndex = 0;
            this.tabFields.Size = new System.Drawing.Size(874, 474);
            this.tabFields.TabIndex = 9;
            // 
            // tabTerminal
            // 
            this.tabTerminal.Controls.Add(this.btnDEAddTag);
            this.tabTerminal.Controls.Add(this.txtDEValue1);
            this.tabTerminal.Controls.Add(this.txtDETag1);
            this.tabTerminal.Controls.Add(this.lblDEValue);
            this.tabTerminal.Controls.Add(this.lblDETag);
            this.tabTerminal.Controls.Add(this.gbTerminalCondition);
            this.tabTerminal.Location = new System.Drawing.Point(4, 22);
            this.tabTerminal.Name = "tabTerminal";
            this.tabTerminal.Padding = new System.Windows.Forms.Padding(3);
            this.tabTerminal.Size = new System.Drawing.Size(866, 448);
            this.tabTerminal.TabIndex = 0;
            this.tabTerminal.Text = "Terminal (DE)";
            this.tabTerminal.UseVisualStyleBackColor = true;
            // 
            // btnDEAddTag
            // 
            this.btnDEAddTag.Location = new System.Drawing.Point(388, 38);
            this.btnDEAddTag.Name = "btnDEAddTag";
            this.btnDEAddTag.Size = new System.Drawing.Size(75, 23);
            this.btnDEAddTag.TabIndex = 7;
            this.btnDEAddTag.Text = "More...";
            this.btnDEAddTag.UseVisualStyleBackColor = true;
            // 
            // txtDEValue1
            // 
            this.txtDEValue1.Location = new System.Drawing.Point(132, 40);
            this.txtDEValue1.Name = "txtDEValue1";
            this.txtDEValue1.Size = new System.Drawing.Size(240, 20);
            this.txtDEValue1.TabIndex = 4;
            // 
            // txtDETag1
            // 
            this.txtDETag1.Location = new System.Drawing.Point(15, 40);
            this.txtDETag1.Name = "txtDETag1";
            this.txtDETag1.Size = new System.Drawing.Size(100, 20);
            this.txtDETag1.TabIndex = 3;
            // 
            // lblDEValue
            // 
            this.lblDEValue.AutoSize = true;
            this.lblDEValue.Location = new System.Drawing.Point(129, 16);
            this.lblDEValue.Name = "lblDEValue";
            this.lblDEValue.Size = new System.Drawing.Size(34, 13);
            this.lblDEValue.TabIndex = 2;
            this.lblDEValue.Text = "Value";
            // 
            // lblDETag
            // 
            this.lblDETag.AutoSize = true;
            this.lblDETag.Location = new System.Drawing.Point(12, 16);
            this.lblDETag.Name = "lblDETag";
            this.lblDETag.Size = new System.Drawing.Size(26, 13);
            this.lblDETag.TabIndex = 1;
            this.lblDETag.Text = "Tag";
            // 
            // gbTerminalCondition
            // 
            this.gbTerminalCondition.Controls.Add(this.chkDECard);
            this.gbTerminalCondition.Controls.Add(this.chkDEIssuer);
            this.gbTerminalCondition.Controls.Add(this.chkDEAcquirer);
            this.gbTerminalCondition.Controls.Add(this.txtDECardValue);
            this.gbTerminalCondition.Controls.Add(this.txtDECardTag);
            this.gbTerminalCondition.Controls.Add(this.txtDEIssuerValue);
            this.gbTerminalCondition.Controls.Add(this.txtDEIssuerTag);
            this.gbTerminalCondition.Controls.Add(this.txtDEAcquirerValue);
            this.gbTerminalCondition.Controls.Add(this.txtDEAcquirerTag);
            this.gbTerminalCondition.Controls.Add(this.txtDETerminalValue);
            this.gbTerminalCondition.Controls.Add(this.txtDeTerminalTag);
            this.gbTerminalCondition.Controls.Add(this.chkDETermintal);
            this.gbTerminalCondition.Location = new System.Drawing.Point(6, 370);
            this.gbTerminalCondition.Name = "gbTerminalCondition";
            this.gbTerminalCondition.Size = new System.Drawing.Size(854, 72);
            this.gbTerminalCondition.TabIndex = 50;
            this.gbTerminalCondition.TabStop = false;
            this.gbTerminalCondition.Text = "Condition";
            // 
            // chkDECard
            // 
            this.chkDECard.AutoSize = true;
            this.chkDECard.Location = new System.Drawing.Point(642, 19);
            this.chkDECard.Name = "chkDECard";
            this.chkDECard.Size = new System.Drawing.Size(48, 17);
            this.chkDECard.TabIndex = 9;
            this.chkDECard.Text = "Card";
            this.chkDECard.UseVisualStyleBackColor = true;
            // 
            // chkDEIssuer
            // 
            this.chkDEIssuer.AutoSize = true;
            this.chkDEIssuer.Location = new System.Drawing.Point(430, 19);
            this.chkDEIssuer.Name = "chkDEIssuer";
            this.chkDEIssuer.Size = new System.Drawing.Size(54, 17);
            this.chkDEIssuer.TabIndex = 6;
            this.chkDEIssuer.Text = "Issuer";
            this.chkDEIssuer.UseVisualStyleBackColor = true;
            // 
            // chkDEAcquirer
            // 
            this.chkDEAcquirer.AutoSize = true;
            this.chkDEAcquirer.Location = new System.Drawing.Point(218, 19);
            this.chkDEAcquirer.Name = "chkDEAcquirer";
            this.chkDEAcquirer.Size = new System.Drawing.Size(65, 17);
            this.chkDEAcquirer.TabIndex = 3;
            this.chkDEAcquirer.Text = "Acquirer";
            this.chkDEAcquirer.UseVisualStyleBackColor = true;
            // 
            // txtDECardValue
            // 
            this.txtDECardValue.Location = new System.Drawing.Point(748, 42);
            this.txtDECardValue.Name = "txtDECardValue";
            this.txtDECardValue.Size = new System.Drawing.Size(100, 20);
            this.txtDECardValue.TabIndex = 11;
            // 
            // txtDECardTag
            // 
            this.txtDECardTag.Location = new System.Drawing.Point(642, 42);
            this.txtDECardTag.Name = "txtDECardTag";
            this.txtDECardTag.Size = new System.Drawing.Size(100, 20);
            this.txtDECardTag.TabIndex = 10;
            // 
            // txtDEIssuerValue
            // 
            this.txtDEIssuerValue.Location = new System.Drawing.Point(536, 42);
            this.txtDEIssuerValue.Name = "txtDEIssuerValue";
            this.txtDEIssuerValue.Size = new System.Drawing.Size(100, 20);
            this.txtDEIssuerValue.TabIndex = 8;
            // 
            // txtDEIssuerTag
            // 
            this.txtDEIssuerTag.Location = new System.Drawing.Point(430, 42);
            this.txtDEIssuerTag.Name = "txtDEIssuerTag";
            this.txtDEIssuerTag.Size = new System.Drawing.Size(100, 20);
            this.txtDEIssuerTag.TabIndex = 7;
            // 
            // txtDEAcquirerValue
            // 
            this.txtDEAcquirerValue.Location = new System.Drawing.Point(324, 42);
            this.txtDEAcquirerValue.Name = "txtDEAcquirerValue";
            this.txtDEAcquirerValue.Size = new System.Drawing.Size(100, 20);
            this.txtDEAcquirerValue.TabIndex = 5;
            // 
            // txtDEAcquirerTag
            // 
            this.txtDEAcquirerTag.Location = new System.Drawing.Point(218, 42);
            this.txtDEAcquirerTag.Name = "txtDEAcquirerTag";
            this.txtDEAcquirerTag.Size = new System.Drawing.Size(100, 20);
            this.txtDEAcquirerTag.TabIndex = 4;
            // 
            // txtDETerminalValue
            // 
            this.txtDETerminalValue.Location = new System.Drawing.Point(112, 42);
            this.txtDETerminalValue.Name = "txtDETerminalValue";
            this.txtDETerminalValue.Size = new System.Drawing.Size(100, 20);
            this.txtDETerminalValue.TabIndex = 2;
            // 
            // txtDeTerminalTag
            // 
            this.txtDeTerminalTag.Location = new System.Drawing.Point(6, 42);
            this.txtDeTerminalTag.Name = "txtDeTerminalTag";
            this.txtDeTerminalTag.Size = new System.Drawing.Size(100, 20);
            this.txtDeTerminalTag.TabIndex = 1;
            // 
            // chkDETermintal
            // 
            this.chkDETermintal.AutoSize = true;
            this.chkDETermintal.Location = new System.Drawing.Point(6, 19);
            this.chkDETermintal.Name = "chkDETermintal";
            this.chkDETermintal.Size = new System.Drawing.Size(66, 17);
            this.chkDETermintal.TabIndex = 0;
            this.chkDETermintal.Text = "Terminal";
            this.chkDETermintal.UseVisualStyleBackColor = true;
            // 
            // tabAcquirer
            // 
            this.tabAcquirer.Controls.Add(this.btnAAAddTag);
            this.tabAcquirer.Controls.Add(this.txtAAValue1);
            this.tabAcquirer.Controls.Add(this.txtAATag1);
            this.tabAcquirer.Controls.Add(this.lblAAValue);
            this.tabAcquirer.Controls.Add(this.lblAATag);
            this.tabAcquirer.Controls.Add(this.gbAcquirerCondition);
            this.tabAcquirer.Location = new System.Drawing.Point(4, 22);
            this.tabAcquirer.Name = "tabAcquirer";
            this.tabAcquirer.Padding = new System.Windows.Forms.Padding(3);
            this.tabAcquirer.Size = new System.Drawing.Size(866, 448);
            this.tabAcquirer.TabIndex = 1;
            this.tabAcquirer.Text = "Acquirer (AA)";
            this.tabAcquirer.UseVisualStyleBackColor = true;
            // 
            // btnAAAddTag
            // 
            this.btnAAAddTag.Location = new System.Drawing.Point(388, 38);
            this.btnAAAddTag.Name = "btnAAAddTag";
            this.btnAAAddTag.Size = new System.Drawing.Size(75, 23);
            this.btnAAAddTag.TabIndex = 12;
            this.btnAAAddTag.Text = "More...";
            this.btnAAAddTag.UseVisualStyleBackColor = true;
            // 
            // txtAAValue1
            // 
            this.txtAAValue1.Location = new System.Drawing.Point(132, 40);
            this.txtAAValue1.Name = "txtAAValue1";
            this.txtAAValue1.Size = new System.Drawing.Size(240, 20);
            this.txtAAValue1.TabIndex = 11;
            // 
            // txtAATag1
            // 
            this.txtAATag1.Location = new System.Drawing.Point(15, 40);
            this.txtAATag1.Name = "txtAATag1";
            this.txtAATag1.Size = new System.Drawing.Size(100, 20);
            this.txtAATag1.TabIndex = 10;
            // 
            // lblAAValue
            // 
            this.lblAAValue.AutoSize = true;
            this.lblAAValue.Location = new System.Drawing.Point(129, 16);
            this.lblAAValue.Name = "lblAAValue";
            this.lblAAValue.Size = new System.Drawing.Size(34, 13);
            this.lblAAValue.TabIndex = 9;
            this.lblAAValue.Text = "Value";
            // 
            // lblAATag
            // 
            this.lblAATag.AutoSize = true;
            this.lblAATag.Location = new System.Drawing.Point(12, 16);
            this.lblAATag.Name = "lblAATag";
            this.lblAATag.Size = new System.Drawing.Size(26, 13);
            this.lblAATag.TabIndex = 8;
            this.lblAATag.Text = "Tag";
            // 
            // gbAcquirerCondition
            // 
            this.gbAcquirerCondition.Controls.Add(this.chkAACard);
            this.gbAcquirerCondition.Controls.Add(this.chkAAIssuer);
            this.gbAcquirerCondition.Controls.Add(this.chkAAAcquirer);
            this.gbAcquirerCondition.Controls.Add(this.txtAACardValue);
            this.gbAcquirerCondition.Controls.Add(this.txtAACardTag);
            this.gbAcquirerCondition.Controls.Add(this.txtAAIssuerValue);
            this.gbAcquirerCondition.Controls.Add(this.txtAAIssuerTag);
            this.gbAcquirerCondition.Controls.Add(this.txtAAAcquirerValue);
            this.gbAcquirerCondition.Controls.Add(this.txtAAAcquirerTag);
            this.gbAcquirerCondition.Controls.Add(this.txtAATerminalValue);
            this.gbAcquirerCondition.Controls.Add(this.txtAATerminalTag);
            this.gbAcquirerCondition.Controls.Add(this.chkAATerminal);
            this.gbAcquirerCondition.Location = new System.Drawing.Point(6, 370);
            this.gbAcquirerCondition.Name = "gbAcquirerCondition";
            this.gbAcquirerCondition.Size = new System.Drawing.Size(854, 72);
            this.gbAcquirerCondition.TabIndex = 1;
            this.gbAcquirerCondition.TabStop = false;
            this.gbAcquirerCondition.Text = "Condition";
            // 
            // chkAACard
            // 
            this.chkAACard.AutoSize = true;
            this.chkAACard.Location = new System.Drawing.Point(642, 19);
            this.chkAACard.Name = "chkAACard";
            this.chkAACard.Size = new System.Drawing.Size(48, 17);
            this.chkAACard.TabIndex = 9;
            this.chkAACard.Text = "Card";
            this.chkAACard.UseVisualStyleBackColor = true;
            // 
            // chkAAIssuer
            // 
            this.chkAAIssuer.AutoSize = true;
            this.chkAAIssuer.Location = new System.Drawing.Point(430, 19);
            this.chkAAIssuer.Name = "chkAAIssuer";
            this.chkAAIssuer.Size = new System.Drawing.Size(54, 17);
            this.chkAAIssuer.TabIndex = 6;
            this.chkAAIssuer.Text = "Issuer";
            this.chkAAIssuer.UseVisualStyleBackColor = true;
            // 
            // chkAAAcquirer
            // 
            this.chkAAAcquirer.AutoSize = true;
            this.chkAAAcquirer.Location = new System.Drawing.Point(218, 19);
            this.chkAAAcquirer.Name = "chkAAAcquirer";
            this.chkAAAcquirer.Size = new System.Drawing.Size(65, 17);
            this.chkAAAcquirer.TabIndex = 3;
            this.chkAAAcquirer.Text = "Acquirer";
            this.chkAAAcquirer.UseVisualStyleBackColor = true;
            // 
            // txtAACardValue
            // 
            this.txtAACardValue.Location = new System.Drawing.Point(748, 42);
            this.txtAACardValue.Name = "txtAACardValue";
            this.txtAACardValue.Size = new System.Drawing.Size(100, 20);
            this.txtAACardValue.TabIndex = 11;
            // 
            // txtAACardTag
            // 
            this.txtAACardTag.Location = new System.Drawing.Point(642, 42);
            this.txtAACardTag.Name = "txtAACardTag";
            this.txtAACardTag.Size = new System.Drawing.Size(100, 20);
            this.txtAACardTag.TabIndex = 10;
            // 
            // txtAAIssuerValue
            // 
            this.txtAAIssuerValue.Location = new System.Drawing.Point(536, 42);
            this.txtAAIssuerValue.Name = "txtAAIssuerValue";
            this.txtAAIssuerValue.Size = new System.Drawing.Size(100, 20);
            this.txtAAIssuerValue.TabIndex = 8;
            // 
            // txtAAIssuerTag
            // 
            this.txtAAIssuerTag.Location = new System.Drawing.Point(430, 42);
            this.txtAAIssuerTag.Name = "txtAAIssuerTag";
            this.txtAAIssuerTag.Size = new System.Drawing.Size(100, 20);
            this.txtAAIssuerTag.TabIndex = 7;
            // 
            // txtAAAcquirerValue
            // 
            this.txtAAAcquirerValue.Location = new System.Drawing.Point(324, 42);
            this.txtAAAcquirerValue.Name = "txtAAAcquirerValue";
            this.txtAAAcquirerValue.Size = new System.Drawing.Size(100, 20);
            this.txtAAAcquirerValue.TabIndex = 5;
            // 
            // txtAAAcquirerTag
            // 
            this.txtAAAcquirerTag.Location = new System.Drawing.Point(218, 42);
            this.txtAAAcquirerTag.Name = "txtAAAcquirerTag";
            this.txtAAAcquirerTag.Size = new System.Drawing.Size(100, 20);
            this.txtAAAcquirerTag.TabIndex = 4;
            // 
            // txtAATerminalValue
            // 
            this.txtAATerminalValue.Location = new System.Drawing.Point(112, 42);
            this.txtAATerminalValue.Name = "txtAATerminalValue";
            this.txtAATerminalValue.Size = new System.Drawing.Size(100, 20);
            this.txtAATerminalValue.TabIndex = 2;
            // 
            // txtAATerminalTag
            // 
            this.txtAATerminalTag.Location = new System.Drawing.Point(6, 42);
            this.txtAATerminalTag.Name = "txtAATerminalTag";
            this.txtAATerminalTag.Size = new System.Drawing.Size(100, 20);
            this.txtAATerminalTag.TabIndex = 1;
            // 
            // chkAATerminal
            // 
            this.chkAATerminal.AutoSize = true;
            this.chkAATerminal.Location = new System.Drawing.Point(6, 19);
            this.chkAATerminal.Name = "chkAATerminal";
            this.chkAATerminal.Size = new System.Drawing.Size(66, 17);
            this.chkAATerminal.TabIndex = 0;
            this.chkAATerminal.Text = "Terminal";
            this.chkAATerminal.UseVisualStyleBackColor = true;
            // 
            // tabIssuer
            // 
            this.tabIssuer.Controls.Add(this.btnAEAddFields);
            this.tabIssuer.Controls.Add(this.txtAEValue1);
            this.tabIssuer.Controls.Add(this.txtAETag1);
            this.tabIssuer.Controls.Add(this.lblAEValue1);
            this.tabIssuer.Controls.Add(this.lblAETag);
            this.tabIssuer.Controls.Add(this.gbIssuerCondition);
            this.tabIssuer.Location = new System.Drawing.Point(4, 22);
            this.tabIssuer.Name = "tabIssuer";
            this.tabIssuer.Size = new System.Drawing.Size(866, 448);
            this.tabIssuer.TabIndex = 2;
            this.tabIssuer.Text = "Issuer (AE)";
            this.tabIssuer.UseVisualStyleBackColor = true;
            // 
            // btnAEAddFields
            // 
            this.btnAEAddFields.Location = new System.Drawing.Point(388, 38);
            this.btnAEAddFields.Name = "btnAEAddFields";
            this.btnAEAddFields.Size = new System.Drawing.Size(75, 23);
            this.btnAEAddFields.TabIndex = 17;
            this.btnAEAddFields.Text = "More...";
            this.btnAEAddFields.UseVisualStyleBackColor = true;
            // 
            // txtAEValue1
            // 
            this.txtAEValue1.Location = new System.Drawing.Point(132, 40);
            this.txtAEValue1.Name = "txtAEValue1";
            this.txtAEValue1.Size = new System.Drawing.Size(240, 20);
            this.txtAEValue1.TabIndex = 16;
            // 
            // txtAETag1
            // 
            this.txtAETag1.Location = new System.Drawing.Point(15, 40);
            this.txtAETag1.Name = "txtAETag1";
            this.txtAETag1.Size = new System.Drawing.Size(100, 20);
            this.txtAETag1.TabIndex = 15;
            // 
            // lblAEValue1
            // 
            this.lblAEValue1.AutoSize = true;
            this.lblAEValue1.Location = new System.Drawing.Point(129, 16);
            this.lblAEValue1.Name = "lblAEValue1";
            this.lblAEValue1.Size = new System.Drawing.Size(34, 13);
            this.lblAEValue1.TabIndex = 14;
            this.lblAEValue1.Text = "Value";
            // 
            // lblAETag
            // 
            this.lblAETag.AutoSize = true;
            this.lblAETag.Location = new System.Drawing.Point(12, 16);
            this.lblAETag.Name = "lblAETag";
            this.lblAETag.Size = new System.Drawing.Size(26, 13);
            this.lblAETag.TabIndex = 13;
            this.lblAETag.Text = "Tag";
            // 
            // gbIssuerCondition
            // 
            this.gbIssuerCondition.Controls.Add(this.chkAECard);
            this.gbIssuerCondition.Controls.Add(this.chkAEIssuer);
            this.gbIssuerCondition.Controls.Add(this.chkAEAcquirer);
            this.gbIssuerCondition.Controls.Add(this.txtAECardValue);
            this.gbIssuerCondition.Controls.Add(this.txtAECardTag);
            this.gbIssuerCondition.Controls.Add(this.txtAEIssuerValue);
            this.gbIssuerCondition.Controls.Add(this.txtAEIssuerTag);
            this.gbIssuerCondition.Controls.Add(this.txtAEAcquirerValue);
            this.gbIssuerCondition.Controls.Add(this.txtAEAcquirerTag);
            this.gbIssuerCondition.Controls.Add(this.txtAETerminalValue);
            this.gbIssuerCondition.Controls.Add(this.txtAETerminalTag);
            this.gbIssuerCondition.Controls.Add(this.chkAETerminal);
            this.gbIssuerCondition.Location = new System.Drawing.Point(6, 370);
            this.gbIssuerCondition.Name = "gbIssuerCondition";
            this.gbIssuerCondition.Size = new System.Drawing.Size(854, 72);
            this.gbIssuerCondition.TabIndex = 1;
            this.gbIssuerCondition.TabStop = false;
            this.gbIssuerCondition.Text = "Condition";
            // 
            // chkAECard
            // 
            this.chkAECard.AutoSize = true;
            this.chkAECard.Location = new System.Drawing.Point(642, 19);
            this.chkAECard.Name = "chkAECard";
            this.chkAECard.Size = new System.Drawing.Size(48, 17);
            this.chkAECard.TabIndex = 9;
            this.chkAECard.Text = "Card";
            this.chkAECard.UseVisualStyleBackColor = true;
            // 
            // chkAEIssuer
            // 
            this.chkAEIssuer.AutoSize = true;
            this.chkAEIssuer.Location = new System.Drawing.Point(430, 19);
            this.chkAEIssuer.Name = "chkAEIssuer";
            this.chkAEIssuer.Size = new System.Drawing.Size(54, 17);
            this.chkAEIssuer.TabIndex = 6;
            this.chkAEIssuer.Text = "Issuer";
            this.chkAEIssuer.UseVisualStyleBackColor = true;
            // 
            // chkAEAcquirer
            // 
            this.chkAEAcquirer.AutoSize = true;
            this.chkAEAcquirer.Location = new System.Drawing.Point(218, 19);
            this.chkAEAcquirer.Name = "chkAEAcquirer";
            this.chkAEAcquirer.Size = new System.Drawing.Size(65, 17);
            this.chkAEAcquirer.TabIndex = 3;
            this.chkAEAcquirer.Text = "Acquirer";
            this.chkAEAcquirer.UseVisualStyleBackColor = true;
            // 
            // txtAECardValue
            // 
            this.txtAECardValue.Location = new System.Drawing.Point(748, 42);
            this.txtAECardValue.Name = "txtAECardValue";
            this.txtAECardValue.Size = new System.Drawing.Size(100, 20);
            this.txtAECardValue.TabIndex = 11;
            // 
            // txtAECardTag
            // 
            this.txtAECardTag.Location = new System.Drawing.Point(642, 42);
            this.txtAECardTag.Name = "txtAECardTag";
            this.txtAECardTag.Size = new System.Drawing.Size(100, 20);
            this.txtAECardTag.TabIndex = 10;
            // 
            // txtAEIssuerValue
            // 
            this.txtAEIssuerValue.Location = new System.Drawing.Point(536, 42);
            this.txtAEIssuerValue.Name = "txtAEIssuerValue";
            this.txtAEIssuerValue.Size = new System.Drawing.Size(100, 20);
            this.txtAEIssuerValue.TabIndex = 8;
            // 
            // txtAEIssuerTag
            // 
            this.txtAEIssuerTag.Location = new System.Drawing.Point(430, 42);
            this.txtAEIssuerTag.Name = "txtAEIssuerTag";
            this.txtAEIssuerTag.Size = new System.Drawing.Size(100, 20);
            this.txtAEIssuerTag.TabIndex = 7;
            // 
            // txtAEAcquirerValue
            // 
            this.txtAEAcquirerValue.Location = new System.Drawing.Point(324, 42);
            this.txtAEAcquirerValue.Name = "txtAEAcquirerValue";
            this.txtAEAcquirerValue.Size = new System.Drawing.Size(100, 20);
            this.txtAEAcquirerValue.TabIndex = 5;
            // 
            // txtAEAcquirerTag
            // 
            this.txtAEAcquirerTag.Location = new System.Drawing.Point(218, 42);
            this.txtAEAcquirerTag.Name = "txtAEAcquirerTag";
            this.txtAEAcquirerTag.Size = new System.Drawing.Size(100, 20);
            this.txtAEAcquirerTag.TabIndex = 4;
            // 
            // txtAETerminalValue
            // 
            this.txtAETerminalValue.Location = new System.Drawing.Point(112, 42);
            this.txtAETerminalValue.Name = "txtAETerminalValue";
            this.txtAETerminalValue.Size = new System.Drawing.Size(100, 20);
            this.txtAETerminalValue.TabIndex = 2;
            // 
            // txtAETerminalTag
            // 
            this.txtAETerminalTag.Location = new System.Drawing.Point(6, 42);
            this.txtAETerminalTag.Name = "txtAETerminalTag";
            this.txtAETerminalTag.Size = new System.Drawing.Size(100, 20);
            this.txtAETerminalTag.TabIndex = 1;
            // 
            // chkAETerminal
            // 
            this.chkAETerminal.AutoSize = true;
            this.chkAETerminal.Location = new System.Drawing.Point(6, 19);
            this.chkAETerminal.Name = "chkAETerminal";
            this.chkAETerminal.Size = new System.Drawing.Size(66, 17);
            this.chkAETerminal.TabIndex = 0;
            this.chkAETerminal.Text = "Terminal";
            this.chkAETerminal.UseVisualStyleBackColor = true;
            // 
            // tabCard
            // 
            this.tabCard.Controls.Add(this.btnACAddFields);
            this.tabCard.Controls.Add(this.txtACValue1);
            this.tabCard.Controls.Add(this.txtACTag1);
            this.tabCard.Controls.Add(this.lblACValue);
            this.tabCard.Controls.Add(this.lblACTag);
            this.tabCard.Controls.Add(this.gbCardCondition);
            this.tabCard.Location = new System.Drawing.Point(4, 22);
            this.tabCard.Name = "tabCard";
            this.tabCard.Size = new System.Drawing.Size(866, 448);
            this.tabCard.TabIndex = 3;
            this.tabCard.Text = "Card (AC)";
            this.tabCard.UseVisualStyleBackColor = true;
            // 
            // btnACAddFields
            // 
            this.btnACAddFields.Location = new System.Drawing.Point(388, 38);
            this.btnACAddFields.Name = "btnACAddFields";
            this.btnACAddFields.Size = new System.Drawing.Size(75, 23);
            this.btnACAddFields.TabIndex = 22;
            this.btnACAddFields.Text = "More...";
            this.btnACAddFields.UseVisualStyleBackColor = true;
            // 
            // txtACValue1
            // 
            this.txtACValue1.Location = new System.Drawing.Point(132, 40);
            this.txtACValue1.Name = "txtACValue1";
            this.txtACValue1.Size = new System.Drawing.Size(240, 20);
            this.txtACValue1.TabIndex = 21;
            // 
            // txtACTag1
            // 
            this.txtACTag1.Location = new System.Drawing.Point(15, 40);
            this.txtACTag1.Name = "txtACTag1";
            this.txtACTag1.Size = new System.Drawing.Size(100, 20);
            this.txtACTag1.TabIndex = 20;
            // 
            // lblACValue
            // 
            this.lblACValue.AutoSize = true;
            this.lblACValue.Location = new System.Drawing.Point(129, 16);
            this.lblACValue.Name = "lblACValue";
            this.lblACValue.Size = new System.Drawing.Size(34, 13);
            this.lblACValue.TabIndex = 19;
            this.lblACValue.Text = "Value";
            // 
            // lblACTag
            // 
            this.lblACTag.AutoSize = true;
            this.lblACTag.Location = new System.Drawing.Point(12, 16);
            this.lblACTag.Name = "lblACTag";
            this.lblACTag.Size = new System.Drawing.Size(26, 13);
            this.lblACTag.TabIndex = 18;
            this.lblACTag.Text = "Tag";
            // 
            // gbCardCondition
            // 
            this.gbCardCondition.Controls.Add(this.chkACCard);
            this.gbCardCondition.Controls.Add(this.chkACIssuer);
            this.gbCardCondition.Controls.Add(this.chkACAcquirer);
            this.gbCardCondition.Controls.Add(this.txtACCardValue);
            this.gbCardCondition.Controls.Add(this.txtACCardTag);
            this.gbCardCondition.Controls.Add(this.txtACIssuerValue);
            this.gbCardCondition.Controls.Add(this.txtACIssuerTag);
            this.gbCardCondition.Controls.Add(this.txtACAcquirerValue);
            this.gbCardCondition.Controls.Add(this.txtACAcquireTag);
            this.gbCardCondition.Controls.Add(this.txtACTerminalValue);
            this.gbCardCondition.Controls.Add(this.txtACTerminalTag);
            this.gbCardCondition.Controls.Add(this.chkACTerminal);
            this.gbCardCondition.Location = new System.Drawing.Point(6, 370);
            this.gbCardCondition.Name = "gbCardCondition";
            this.gbCardCondition.Size = new System.Drawing.Size(854, 72);
            this.gbCardCondition.TabIndex = 1;
            this.gbCardCondition.TabStop = false;
            this.gbCardCondition.Text = "Condition";
            // 
            // chkACCard
            // 
            this.chkACCard.AutoSize = true;
            this.chkACCard.Location = new System.Drawing.Point(642, 19);
            this.chkACCard.Name = "chkACCard";
            this.chkACCard.Size = new System.Drawing.Size(48, 17);
            this.chkACCard.TabIndex = 9;
            this.chkACCard.Text = "Card";
            this.chkACCard.UseVisualStyleBackColor = true;
            // 
            // chkACIssuer
            // 
            this.chkACIssuer.AutoSize = true;
            this.chkACIssuer.Location = new System.Drawing.Point(430, 19);
            this.chkACIssuer.Name = "chkACIssuer";
            this.chkACIssuer.Size = new System.Drawing.Size(54, 17);
            this.chkACIssuer.TabIndex = 6;
            this.chkACIssuer.Text = "Issuer";
            this.chkACIssuer.UseVisualStyleBackColor = true;
            // 
            // chkACAcquirer
            // 
            this.chkACAcquirer.AutoSize = true;
            this.chkACAcquirer.Location = new System.Drawing.Point(218, 19);
            this.chkACAcquirer.Name = "chkACAcquirer";
            this.chkACAcquirer.Size = new System.Drawing.Size(65, 17);
            this.chkACAcquirer.TabIndex = 3;
            this.chkACAcquirer.Text = "Acquirer";
            this.chkACAcquirer.UseVisualStyleBackColor = true;
            // 
            // txtACCardValue
            // 
            this.txtACCardValue.Location = new System.Drawing.Point(748, 42);
            this.txtACCardValue.Name = "txtACCardValue";
            this.txtACCardValue.Size = new System.Drawing.Size(100, 20);
            this.txtACCardValue.TabIndex = 11;
            // 
            // txtACCardTag
            // 
            this.txtACCardTag.Location = new System.Drawing.Point(642, 42);
            this.txtACCardTag.Name = "txtACCardTag";
            this.txtACCardTag.Size = new System.Drawing.Size(100, 20);
            this.txtACCardTag.TabIndex = 10;
            // 
            // txtACIssuerValue
            // 
            this.txtACIssuerValue.Location = new System.Drawing.Point(536, 42);
            this.txtACIssuerValue.Name = "txtACIssuerValue";
            this.txtACIssuerValue.Size = new System.Drawing.Size(100, 20);
            this.txtACIssuerValue.TabIndex = 8;
            // 
            // txtACIssuerTag
            // 
            this.txtACIssuerTag.Location = new System.Drawing.Point(430, 42);
            this.txtACIssuerTag.Name = "txtACIssuerTag";
            this.txtACIssuerTag.Size = new System.Drawing.Size(100, 20);
            this.txtACIssuerTag.TabIndex = 7;
            // 
            // txtACAcquirerValue
            // 
            this.txtACAcquirerValue.Location = new System.Drawing.Point(324, 42);
            this.txtACAcquirerValue.Name = "txtACAcquirerValue";
            this.txtACAcquirerValue.Size = new System.Drawing.Size(100, 20);
            this.txtACAcquirerValue.TabIndex = 5;
            // 
            // txtACAcquireTag
            // 
            this.txtACAcquireTag.Location = new System.Drawing.Point(218, 42);
            this.txtACAcquireTag.Name = "txtACAcquireTag";
            this.txtACAcquireTag.Size = new System.Drawing.Size(100, 20);
            this.txtACAcquireTag.TabIndex = 4;
            // 
            // txtACTerminalValue
            // 
            this.txtACTerminalValue.Location = new System.Drawing.Point(112, 42);
            this.txtACTerminalValue.Name = "txtACTerminalValue";
            this.txtACTerminalValue.Size = new System.Drawing.Size(100, 20);
            this.txtACTerminalValue.TabIndex = 2;
            // 
            // txtACTerminalTag
            // 
            this.txtACTerminalTag.Location = new System.Drawing.Point(6, 42);
            this.txtACTerminalTag.Name = "txtACTerminalTag";
            this.txtACTerminalTag.Size = new System.Drawing.Size(100, 20);
            this.txtACTerminalTag.TabIndex = 1;
            // 
            // chkACTerminal
            // 
            this.chkACTerminal.AutoSize = true;
            this.chkACTerminal.Location = new System.Drawing.Point(6, 19);
            this.chkACTerminal.Name = "chkACTerminal";
            this.chkACTerminal.Size = new System.Drawing.Size(66, 17);
            this.chkACTerminal.TabIndex = 0;
            this.chkACTerminal.Text = "Terminal";
            this.chkACTerminal.UseVisualStyleBackColor = true;
            // 
            // txtKeterangan
            // 
            this.txtKeterangan.Enabled = false;
            this.txtKeterangan.Location = new System.Drawing.Point(307, 11);
            this.txtKeterangan.Multiline = true;
            this.txtKeterangan.Name = "txtKeterangan";
            this.txtKeterangan.Size = new System.Drawing.Size(349, 117);
            this.txtKeterangan.TabIndex = 7;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnExecuteAll);
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Controls.Add(this.btnExecute);
            this.gbButton.Location = new System.Drawing.Point(10, 636);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(873, 41);
            this.gbButton.TabIndex = 10;
            this.gbButton.TabStop = false;
            // 
            // btnExecuteAll
            // 
            this.btnExecuteAll.Location = new System.Drawing.Point(555, 12);
            this.btnExecuteAll.Name = "btnExecuteAll";
            this.btnExecuteAll.Size = new System.Drawing.Size(100, 23);
            this.btnExecuteAll.TabIndex = 0;
            this.btnExecuteAll.Text = "Execute &All";
            this.btnExecuteAll.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(767, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(661, 12);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(100, 23);
            this.btnExecute.TabIndex = 1;
            this.btnExecute.Text = "&Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // pbAddFields
            // 
            this.pbAddFields.Location = new System.Drawing.Point(10, 134);
            this.pbAddFields.Name = "pbAddFields";
            this.pbAddFields.Size = new System.Drawing.Size(874, 23);
            this.pbAddFields.TabIndex = 8;
            // 
            // gbSource
            // 
            this.gbSource.Controls.Add(this.chkIncludeMs);
            this.gbSource.Controls.Add(this.cmbTerminal);
            this.gbSource.Controls.Add(this.cmbDbSource);
            this.gbSource.Controls.Add(this.lblTerminal);
            this.gbSource.Controls.Add(this.lblDbSource);
            this.gbSource.Location = new System.Drawing.Point(10, 9);
            this.gbSource.Name = "gbSource";
            this.gbSource.Size = new System.Drawing.Size(275, 119);
            this.gbSource.TabIndex = 6;
            this.gbSource.TabStop = false;
            this.gbSource.Text = "Source";
            // 
            // chkIncludeMs
            // 
            this.chkIncludeMs.AutoSize = true;
            this.chkIncludeMs.Location = new System.Drawing.Point(19, 90);
            this.chkIncludeMs.Name = "chkIncludeMs";
            this.chkIncludeMs.Size = new System.Drawing.Size(96, 17);
            this.chkIncludeMs.TabIndex = 3;
            this.chkIncludeMs.Text = "Include Master";
            this.chkIncludeMs.UseVisualStyleBackColor = true;
            // 
            // cmbTerminal
            // 
            this.cmbTerminal.FormattingEnabled = true;
            this.cmbTerminal.Location = new System.Drawing.Point(97, 60);
            this.cmbTerminal.Name = "cmbTerminal";
            this.cmbTerminal.Size = new System.Drawing.Size(160, 21);
            this.cmbTerminal.TabIndex = 2;
            // 
            // cmbDbSource
            // 
            this.cmbDbSource.FormattingEnabled = true;
            this.cmbDbSource.Location = new System.Drawing.Point(97, 24);
            this.cmbDbSource.Name = "cmbDbSource";
            this.cmbDbSource.Size = new System.Drawing.Size(160, 21);
            this.cmbDbSource.TabIndex = 1;
            // 
            // lblTerminal
            // 
            this.lblTerminal.AutoSize = true;
            this.lblTerminal.Location = new System.Drawing.Point(16, 63);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(47, 13);
            this.lblTerminal.TabIndex = 1;
            this.lblTerminal.Text = "Terminal";
            // 
            // lblDbSource
            // 
            this.lblDbSource.AutoSize = true;
            this.lblDbSource.Location = new System.Drawing.Point(16, 27);
            this.lblDbSource.Name = "lblDbSource";
            this.lblDbSource.Size = new System.Drawing.Size(53, 13);
            this.lblDbSource.TabIndex = 0;
            this.lblDbSource.Text = "Database";
            // 
            // FrmDeleteField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 687);
            this.Controls.Add(this.tabFields);
            this.Controls.Add(this.txtKeterangan);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.pbAddFields);
            this.Controls.Add(this.gbSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmDeleteField";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Delete Field";
            this.tabFields.ResumeLayout(false);
            this.tabTerminal.ResumeLayout(false);
            this.tabTerminal.PerformLayout();
            this.gbTerminalCondition.ResumeLayout(false);
            this.gbTerminalCondition.PerformLayout();
            this.tabAcquirer.ResumeLayout(false);
            this.tabAcquirer.PerformLayout();
            this.gbAcquirerCondition.ResumeLayout(false);
            this.gbAcquirerCondition.PerformLayout();
            this.tabIssuer.ResumeLayout(false);
            this.tabIssuer.PerformLayout();
            this.gbIssuerCondition.ResumeLayout(false);
            this.gbIssuerCondition.PerformLayout();
            this.tabCard.ResumeLayout(false);
            this.tabCard.PerformLayout();
            this.gbCardCondition.ResumeLayout(false);
            this.gbCardCondition.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabFields;
        private System.Windows.Forms.TabPage tabTerminal;
        private System.Windows.Forms.Button btnDEAddTag;
        private System.Windows.Forms.TextBox txtDEValue1;
        private System.Windows.Forms.TextBox txtDETag1;
        private System.Windows.Forms.Label lblDEValue;
        private System.Windows.Forms.Label lblDETag;
        private System.Windows.Forms.GroupBox gbTerminalCondition;
        private System.Windows.Forms.CheckBox chkDECard;
        private System.Windows.Forms.CheckBox chkDEIssuer;
        private System.Windows.Forms.CheckBox chkDEAcquirer;
        private System.Windows.Forms.TextBox txtDECardValue;
        private System.Windows.Forms.TextBox txtDECardTag;
        private System.Windows.Forms.TextBox txtDEIssuerValue;
        private System.Windows.Forms.TextBox txtDEIssuerTag;
        private System.Windows.Forms.TextBox txtDEAcquirerValue;
        private System.Windows.Forms.TextBox txtDEAcquirerTag;
        private System.Windows.Forms.TextBox txtDETerminalValue;
        private System.Windows.Forms.TextBox txtDeTerminalTag;
        private System.Windows.Forms.CheckBox chkDETermintal;
        private System.Windows.Forms.TabPage tabAcquirer;
        private System.Windows.Forms.Button btnAAAddTag;
        private System.Windows.Forms.TextBox txtAAValue1;
        private System.Windows.Forms.TextBox txtAATag1;
        private System.Windows.Forms.Label lblAAValue;
        private System.Windows.Forms.Label lblAATag;
        private System.Windows.Forms.GroupBox gbAcquirerCondition;
        private System.Windows.Forms.CheckBox chkAACard;
        private System.Windows.Forms.CheckBox chkAAIssuer;
        private System.Windows.Forms.CheckBox chkAAAcquirer;
        private System.Windows.Forms.TextBox txtAACardValue;
        private System.Windows.Forms.TextBox txtAACardTag;
        private System.Windows.Forms.TextBox txtAAIssuerValue;
        private System.Windows.Forms.TextBox txtAAIssuerTag;
        private System.Windows.Forms.TextBox txtAAAcquirerValue;
        private System.Windows.Forms.TextBox txtAAAcquirerTag;
        private System.Windows.Forms.TextBox txtAATerminalValue;
        private System.Windows.Forms.TextBox txtAATerminalTag;
        private System.Windows.Forms.CheckBox chkAATerminal;
        private System.Windows.Forms.TabPage tabIssuer;
        private System.Windows.Forms.Button btnAEAddFields;
        private System.Windows.Forms.TextBox txtAEValue1;
        private System.Windows.Forms.TextBox txtAETag1;
        private System.Windows.Forms.Label lblAEValue1;
        private System.Windows.Forms.Label lblAETag;
        private System.Windows.Forms.GroupBox gbIssuerCondition;
        private System.Windows.Forms.CheckBox chkAECard;
        private System.Windows.Forms.CheckBox chkAEIssuer;
        private System.Windows.Forms.CheckBox chkAEAcquirer;
        private System.Windows.Forms.TextBox txtAECardValue;
        private System.Windows.Forms.TextBox txtAECardTag;
        private System.Windows.Forms.TextBox txtAEIssuerValue;
        private System.Windows.Forms.TextBox txtAEIssuerTag;
        private System.Windows.Forms.TextBox txtAEAcquirerValue;
        private System.Windows.Forms.TextBox txtAEAcquirerTag;
        private System.Windows.Forms.TextBox txtAETerminalValue;
        private System.Windows.Forms.TextBox txtAETerminalTag;
        private System.Windows.Forms.CheckBox chkAETerminal;
        private System.Windows.Forms.TabPage tabCard;
        private System.Windows.Forms.Button btnACAddFields;
        private System.Windows.Forms.TextBox txtACValue1;
        private System.Windows.Forms.TextBox txtACTag1;
        private System.Windows.Forms.Label lblACValue;
        private System.Windows.Forms.Label lblACTag;
        private System.Windows.Forms.GroupBox gbCardCondition;
        private System.Windows.Forms.CheckBox chkACCard;
        private System.Windows.Forms.CheckBox chkACIssuer;
        private System.Windows.Forms.CheckBox chkACAcquirer;
        private System.Windows.Forms.TextBox txtACCardValue;
        private System.Windows.Forms.TextBox txtACCardTag;
        private System.Windows.Forms.TextBox txtACIssuerValue;
        private System.Windows.Forms.TextBox txtACIssuerTag;
        private System.Windows.Forms.TextBox txtACAcquirerValue;
        private System.Windows.Forms.TextBox txtACAcquireTag;
        private System.Windows.Forms.TextBox txtACTerminalValue;
        private System.Windows.Forms.TextBox txtACTerminalTag;
        private System.Windows.Forms.CheckBox chkACTerminal;
        private System.Windows.Forms.TextBox txtKeterangan;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnExecuteAll;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.ProgressBar pbAddFields;
        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.CheckBox chkIncludeMs;
        private System.Windows.Forms.ComboBox cmbTerminal;
        private System.Windows.Forms.ComboBox cmbDbSource;
        private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.Label lblDbSource;
    }
}