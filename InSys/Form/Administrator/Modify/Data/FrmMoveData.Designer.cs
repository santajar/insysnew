﻿namespace InSys
{
    partial class FrmMoveData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMoveData));
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.pbMoveData = new System.Windows.Forms.ProgressBar();
            this.gbDestination = new System.Windows.Forms.GroupBox();
            this.cmbDbDestination = new System.Windows.Forms.ComboBox();
            this.lblDbDestination = new System.Windows.Forms.Label();
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.rbNotMaster = new System.Windows.Forms.RadioButton();
            this.rbMaster = new System.Windows.Forms.RadioButton();
            this.rbAll = new System.Windows.Forms.RadioButton();
            this.chlTerminal = new System.Windows.Forms.CheckedListBox();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.chkMasterOnly = new System.Windows.Forms.CheckBox();
            this.cmbDbSource = new System.Windows.Forms.ComboBox();
            this.lblDbSource = new System.Windows.Forms.Label();
            this.bwMoveData = new System.ComponentModel.BackgroundWorker();
            this.gbButton.SuspendLayout();
            this.gbDestination.SuspendLayout();
            this.gbSource.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Controls.Add(this.btnExecute);
            this.gbButton.Location = new System.Drawing.Point(12, 436);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(363, 41);
            this.gbButton.TabIndex = 8;
            this.gbButton.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(257, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnExecute
            // 
            this.btnExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExecute.Location = new System.Drawing.Point(151, 12);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(100, 23);
            this.btnExecute.TabIndex = 0;
            this.btnExecute.Text = "&Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // pbMoveData
            // 
            this.pbMoveData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbMoveData.Location = new System.Drawing.Point(12, 412);
            this.pbMoveData.Name = "pbMoveData";
            this.pbMoveData.Size = new System.Drawing.Size(363, 23);
            this.pbMoveData.TabIndex = 7;
            // 
            // gbDestination
            // 
            this.gbDestination.Controls.Add(this.cmbDbDestination);
            this.gbDestination.Controls.Add(this.lblDbDestination);
            this.gbDestination.Location = new System.Drawing.Point(12, 12);
            this.gbDestination.Name = "gbDestination";
            this.gbDestination.Size = new System.Drawing.Size(363, 55);
            this.gbDestination.TabIndex = 6;
            this.gbDestination.TabStop = false;
            this.gbDestination.Text = "Destination";
            // 
            // cmbDbDestination
            // 
            this.cmbDbDestination.FormattingEnabled = true;
            this.cmbDbDestination.Location = new System.Drawing.Point(97, 23);
            this.cmbDbDestination.Name = "cmbDbDestination";
            this.cmbDbDestination.Size = new System.Drawing.Size(160, 21);
            this.cmbDbDestination.TabIndex = 2;
            this.cmbDbDestination.SelectedIndexChanged += new System.EventHandler(this.cmbDbDestination_SelectedIndexChanged);
            // 
            // lblDbDestination
            // 
            this.lblDbDestination.AutoSize = true;
            this.lblDbDestination.Location = new System.Drawing.Point(16, 27);
            this.lblDbDestination.Name = "lblDbDestination";
            this.lblDbDestination.Size = new System.Drawing.Size(53, 13);
            this.lblDbDestination.TabIndex = 0;
            this.lblDbDestination.Text = "Database";
            // 
            // gbSource
            // 
            this.gbSource.Controls.Add(this.rbNotMaster);
            this.gbSource.Controls.Add(this.rbMaster);
            this.gbSource.Controls.Add(this.rbAll);
            this.gbSource.Controls.Add(this.chlTerminal);
            this.gbSource.Controls.Add(this.chkSelectAll);
            this.gbSource.Controls.Add(this.chkMasterOnly);
            this.gbSource.Controls.Add(this.cmbDbSource);
            this.gbSource.Controls.Add(this.lblDbSource);
            this.gbSource.Location = new System.Drawing.Point(12, 73);
            this.gbSource.Name = "gbSource";
            this.gbSource.Size = new System.Drawing.Size(363, 343);
            this.gbSource.TabIndex = 5;
            this.gbSource.TabStop = false;
            this.gbSource.Text = "Source";
            // 
            // rbNotMaster
            // 
            this.rbNotMaster.AutoSize = true;
            this.rbNotMaster.Location = new System.Drawing.Point(133, 61);
            this.rbNotMaster.Name = "rbNotMaster";
            this.rbNotMaster.Size = new System.Drawing.Size(77, 17);
            this.rbNotMaster.TabIndex = 10;
            this.rbNotMaster.Text = "Not Master";
            this.rbNotMaster.UseVisualStyleBackColor = true;
            this.rbNotMaster.CheckedChanged += new System.EventHandler(this.rbNotMaster_CheckedChanged);
            // 
            // rbMaster
            // 
            this.rbMaster.AutoSize = true;
            this.rbMaster.Location = new System.Drawing.Point(70, 61);
            this.rbMaster.Name = "rbMaster";
            this.rbMaster.Size = new System.Drawing.Size(57, 17);
            this.rbMaster.TabIndex = 9;
            this.rbMaster.Text = "Master";
            this.rbMaster.UseVisualStyleBackColor = true;
            this.rbMaster.CheckedChanged += new System.EventHandler(this.rbMaster_CheckedChanged);
            // 
            // rbAll
            // 
            this.rbAll.AutoSize = true;
            this.rbAll.Checked = true;
            this.rbAll.Location = new System.Drawing.Point(19, 61);
            this.rbAll.Name = "rbAll";
            this.rbAll.Size = new System.Drawing.Size(36, 17);
            this.rbAll.TabIndex = 8;
            this.rbAll.TabStop = true;
            this.rbAll.Text = "All";
            this.rbAll.UseVisualStyleBackColor = true;
            this.rbAll.CheckedChanged += new System.EventHandler(this.rbAll_CheckedChanged);
            // 
            // chlTerminal
            // 
            this.chlTerminal.FormattingEnabled = true;
            this.chlTerminal.Location = new System.Drawing.Point(19, 84);
            this.chlTerminal.Name = "chlTerminal";
            this.chlTerminal.Size = new System.Drawing.Size(330, 244);
            this.chlTerminal.TabIndex = 7;
            this.chlTerminal.SelectedIndexChanged += new System.EventHandler(this.chlTerminal_SelectedIndexChanged);
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(279, 26);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(70, 17);
            this.chkSelectAll.TabIndex = 6;
            this.chkSelectAll.Text = "Select All";
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // chkMasterOnly
            // 
            this.chkMasterOnly.AutoSize = true;
            this.chkMasterOnly.Location = new System.Drawing.Point(279, 49);
            this.chkMasterOnly.Name = "chkMasterOnly";
            this.chkMasterOnly.Size = new System.Drawing.Size(82, 17);
            this.chkMasterOnly.TabIndex = 5;
            this.chkMasterOnly.Text = "Master Only";
            this.chkMasterOnly.UseVisualStyleBackColor = true;
            this.chkMasterOnly.Visible = false;
            this.chkMasterOnly.CheckedChanged += new System.EventHandler(this.chkMasterOnly_CheckedChanged);
            // 
            // cmbDbSource
            // 
            this.cmbDbSource.FormattingEnabled = true;
            this.cmbDbSource.Location = new System.Drawing.Point(97, 24);
            this.cmbDbSource.Name = "cmbDbSource";
            this.cmbDbSource.Size = new System.Drawing.Size(160, 21);
            this.cmbDbSource.TabIndex = 2;
            this.cmbDbSource.SelectedIndexChanged += new System.EventHandler(this.cmbDbSource_SelectedIndexChanged);
            // 
            // lblDbSource
            // 
            this.lblDbSource.AutoSize = true;
            this.lblDbSource.Location = new System.Drawing.Point(16, 27);
            this.lblDbSource.Name = "lblDbSource";
            this.lblDbSource.Size = new System.Drawing.Size(53, 13);
            this.lblDbSource.TabIndex = 0;
            this.lblDbSource.Text = "Database";
            // 
            // bwMoveData
            // 
            this.bwMoveData.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwMoveData_DoWork);
            this.bwMoveData.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwMoveData_ProgressChanged);
            this.bwMoveData.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwMoveData_RunWorkerCompleted);
            // 
            // FrmMoveData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 485);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.pbMoveData);
            this.Controls.Add(this.gbDestination);
            this.Controls.Add(this.gbSource);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMoveData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmMoveData";
            this.Load += new System.EventHandler(this.FrmMoveData_Load);
            this.gbButton.ResumeLayout(false);
            this.gbDestination.ResumeLayout(false);
            this.gbDestination.PerformLayout();
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.ProgressBar pbMoveData;
        private System.Windows.Forms.GroupBox gbDestination;
        private System.Windows.Forms.ComboBox cmbDbDestination;
        private System.Windows.Forms.Label lblDbDestination;
        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.CheckedListBox chlTerminal;
        private System.Windows.Forms.CheckBox chkSelectAll;
        private System.Windows.Forms.CheckBox chkMasterOnly;
        private System.Windows.Forms.ComboBox cmbDbSource;
        private System.Windows.Forms.Label lblDbSource;
        private System.ComponentModel.BackgroundWorker bwMoveData;
        private System.Windows.Forms.RadioButton rbNotMaster;
        private System.Windows.Forms.RadioButton rbMaster;
        private System.Windows.Forms.RadioButton rbAll;
    }
}