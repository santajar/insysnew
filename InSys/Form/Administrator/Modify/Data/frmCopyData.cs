﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class frmCopyData : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        string sDBIDMaster, sDBIDOriginal, sNewTID,sTempTID;
        int iIDFunction;
        public frmCopyData(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        private void frmCopyData_Load(object sender, EventArgs e)
        {
            LoadCmbDatabase();
        }

        private void rbMoveData_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMoveData.Checked == true)
            {
                txtNewTID.Enabled = false;
               
            }
        }

        private void rbCopyData_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCopyData.Checked == true)
            {
                txtNewTID.Enabled = true;
                
            }
        }

        private void cmbDBDest_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDBDest.SelectedIndex > -1)
            {
                sDBIDMaster = "";
                sDBIDMaster = cmbDBDest.SelectedValue.ToString();
                if (IsDBValid(sDBIDMaster))
                {
                    LoadDataTerminalID(sDBIDMaster, ref dgvTidMaster);
                    ViewDgvTID(dgvTidMaster);
                }
            }
        }

        private void cmbDBOriginal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDBOriginal.SelectedIndex > -1)
            {
                sDBIDOriginal = "";
                sDBIDOriginal = cmbDBOriginal.SelectedValue.ToString();
                if (IsDBValid(sDBIDOriginal))
                {
                    LoadDataTerminalID(sDBIDOriginal, ref dgvTidOriginal);
                    ViewDgvTID(dgvTidOriginal);
                }
            }
        }

        private void btnexecute_Click(object sender, EventArgs e)
        {
                if (cmbDBDest.SelectedIndex > -1 && cmbDBOriginal.SelectedIndex > -1)
                {
                        if (CountCheckedData(dgvTidMaster) == 1 && CountCheckedData(dgvTidOriginal) == 1)
                        {
                            if (GetIDFunction() == 0)
                            {
                                if (txtNewTID.TextLength == 8)
                                {
                                    if (isTIDExist())
                                    {
                                        try
                                        {
                                            ExecuteFunctionMoveAndCopyData();
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show("ERROR: " + ex.Message);
                                        }
                                    }
                                    else MessageBox.Show(txtNewTID.Text.ToUpper() + "Already Exist");
                                }
                                else MessageBox.Show("New Terminal ID Name must 8 digits");
                            }
                            else if (GetIDFunction() == 1)
                            {
                                try
                                {
                                    ExecuteFunctionMoveAndCopyData();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("ERROR: " + ex.Message);
                                }
                            }
                        }
                        else if (CountCheckedData(dgvTidMaster) == 0 && CountCheckedData(dgvTidMaster) == 0)
                            MessageBox.Show("Please Checked TerminalID in Destination and Original Coloumn Select");
                        else
                            MessageBox.Show("Please Checked 1 both TerminalID in Destination and Original Coloumn Select");

                }
                else MessageBox.Show("Choose Database First ");
            
        }

        #region Function
        /// <summary>
        /// Load Data in ComboBox
        /// </summary>
        private void LoadCmbDatabase()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseName", "DatabaseName", ref cmbDBDest);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseName", "DatabaseName", ref cmbDBOriginal);
        }

        /// <summary>
        /// Execute function Move or Copy to store Procedure
        /// </summary>
        private void ExecuteFunctionMoveAndCopyData()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileCopyDataTerminalID, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            if (GetIDFunction() == 0) oCmd.Parameters.Add("@sNewTerminalID", SqlDbType.VarChar).Value = txtNewTID.Text.ToUpper();
            oCmd.Parameters.Add("@sRefTerminalID", SqlDbType.VarChar).Value = getTIDfromDGV(dgvTidOriginal);
            oCmd.Parameters.Add("@sMasterTerminalID", SqlDbType.VarChar).Value = getTIDfromDGV(dgvTidMaster);
            oCmd.Parameters.Add("@iTypeFunction", SqlDbType.Int).Value = GetIDFunction();
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oCmd.ExecuteNonQuery();
            if (GetIDFunction() == 0)
                MessageBox.Show("Copy Data Succes");
            else if (GetIDFunction() == 1)
                MessageBox.Show("Move Data Succes");
            RefreshDataGridView();
        }

        /// <summary>
        /// refresh Grid View
        /// </summary>
        private void RefreshDataGridView()
        {
            LoadDataTerminalID(sDBIDOriginal, ref dgvTidOriginal);
            ViewDgvTID(dgvTidOriginal);
            LoadDataTerminalID(sDBIDMaster, ref dgvTidMaster);
            ViewDgvTID(dgvTidMaster);
        }

        /// <summary>
        /// Get ID of function
        /// </summary>
        /// <returns>int: 0 for Copy and 1 for Move</returns>
        private int GetIDFunction()
        {
            if (rbCopyData.Checked == true)
                iIDFunction = 0;
            if (rbMoveData.Checked == true)
                iIDFunction = 1;
            //if (rbMoveAll.Checked==true)
            //    iIDFunction=2;
            return iIDFunction;
        }

        /// <summary>
        /// Validate TerminalID
        /// </summary>
        /// <returns>bool: false if TID already Exist</returns>
        private bool isTIDExist()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = " Where TerminalID = '" + txtNewTID.Text.ToUpper() +"'";
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            DataTable dtTID = new DataTable();
            SqlDataAdapter oAdapt = new SqlDataAdapter(oCmd);
            oAdapt.Fill(dtTID);
            if (dtTID.Rows.Count > 0)
                return false;
            else
                return true;
        }
        /// <summary>
        /// Get Terminal ID from data grid view
        /// </summary>
        /// <param name="dgvTid">Grid View of TerminalID data</param>
        /// <returns>string : TerminalID</returns>
        private string getTIDfromDGV(DataGridView dgvTid)
        {
            sTempTID = "";
            foreach (DataGridViewRow dgvRow in dgvTid.Rows)
            {
                if (Convert.ToBoolean(dgvRow.Cells[0].Value) == true)
                {
                    sTempTID = dgvRow.Cells[1].Value.ToString();
                }
            }
            return sTempTID;
        }

        private int CountCheckedData(DataGridView dgvTid)
        {
            int iCountChecked;
            iCountChecked = 0;
            foreach (DataGridViewRow dgvRow in dgvTid.Rows)
            {
                if (Convert.ToBoolean(dgvRow.Cells[0].Value) == true)
                {
                    iCountChecked = iCountChecked + 1;
                }
            }
            return iCountChecked;
        }

        private void ViewDgvTID( DataGridView dgvTid)
        {
            dgvTid.Columns["DatabaseID"].Visible = false;
            dgvTid.Columns["AllowDownload"].Visible = false;
            dgvTid.Columns["StatusMaster"].Visible = false;
            dgvTid.Columns["LastView"].Visible = false;
            dgvTid.Columns["EnableIP"].Visible = false;
            dgvTid.Columns["IpAddress"].Visible = false;
            dgvTid.Columns["LocationID"].Visible = false;
            dgvTid.Columns["AutoInitTimeStamp"].Visible = false;
            dgvTid.Columns["InitCompress"].Visible = false;
            dgvTid.Columns["AppPackID"].Visible = false;

        }

        /// <summary>
        /// Validate DatabaseID
        /// </summary>
        /// <param name="sDBID">string: DatabaseID Value</param>
        /// <returns>bool: true if Databaseid valid</returns>
        private bool IsDBValid(string sDBID)
        {
            DataTable dtDatabaseName = new DataTable();
            SqlCommand oCmd = new SqlCommand("select DatabaseName from tbProfileTerminalDB where DatabaseName = '" + sDBID + "'", oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            new SqlDataAdapter(oCmd).Fill(dtDatabaseName);
            if (dtDatabaseName.Rows.Count > 0)
                return true;
            else return false;
        }
        /// <summary>
        /// Load Data Grid view with data TerminalID
        /// </summary>
        /// <param name="sDBIDMaster">string : Terminal ID Master</param>
        /// <param name="dgvTerminalID">Data Grid View : Terminal ID master</param>
        private void LoadDataTerminalID(string sDBIDMaster, ref DataGridView dgvTerminalID)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGetTIDCond(sDBIDMaster);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
                DataTable oDataTable = new DataTable();
                oDataTable.Clear();
                oAdapt.Fill(oDataTable);
                dgvTerminalID.DataSource = oDataTable;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

        }

        /// <summary>
        /// Get Condition 
        /// </summary>
        /// <param name="sDBName">string : Database ID</param>
        /// <returns>String: Terminal ID condition</returns>
        private string sGetTIDCond(string sDBName)
        {
            return string.Format(" Where DatabaseID in (select DatabaseID from tbProfileTerminalDB where DatabaseName ='{0}')", sDBName);
        }
        #endregion Function

        

    }
}
