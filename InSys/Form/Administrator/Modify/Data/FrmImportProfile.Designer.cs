namespace InSys
{
    partial class FrmImportProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmImportProfile));
            this.gbFolder = new System.Windows.Forms.GroupBox();
            this.chkUpdateAll = new System.Windows.Forms.CheckBox();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.cmbDbDestination = new System.Windows.Forms.ComboBox();
            this.lblDbDestination = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.lblFileFolder = new System.Windows.Forms.Label();
            this.gbListView = new System.Windows.Forms.GroupBox();
            this.lvTerminalName = new System.Windows.Forms.ListView();
            this.colTerminalName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvFileName = new System.Windows.Forms.ListView();
            this.colFileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pbImport = new System.Windows.Forms.ProgressBar();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.bwImport = new System.ComponentModel.BackgroundWorker();
            this.gbFolder.SuspendLayout();
            this.gbListView.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbFolder
            // 
            this.gbFolder.BackColor = System.Drawing.SystemColors.Control;
            this.gbFolder.Controls.Add(this.chkUpdateAll);
            this.gbFolder.Controls.Add(this.chkSelectAll);
            this.gbFolder.Controls.Add(this.cmbDbDestination);
            this.gbFolder.Controls.Add(this.lblDbDestination);
            this.gbFolder.Controls.Add(this.btnBrowse);
            this.gbFolder.Controls.Add(this.txtFolder);
            this.gbFolder.Controls.Add(this.lblFileFolder);
            this.gbFolder.Location = new System.Drawing.Point(12, 2);
            this.gbFolder.Name = "gbFolder";
            this.gbFolder.Size = new System.Drawing.Size(524, 111);
            this.gbFolder.TabIndex = 0;
            this.gbFolder.TabStop = false;
            // 
            // chkUpdateAll
            // 
            this.chkUpdateAll.AutoSize = true;
            this.chkUpdateAll.Location = new System.Drawing.Point(126, 85);
            this.chkUpdateAll.Name = "chkUpdateAll";
            this.chkUpdateAll.Size = new System.Drawing.Size(75, 17);
            this.chkUpdateAll.TabIndex = 5;
            this.chkUpdateAll.Text = "Update All";
            this.chkUpdateAll.UseVisualStyleBackColor = true;
            this.chkUpdateAll.CheckedChanged += new System.EventHandler(this.chkUpdateAll_CheckedChanged);
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(26, 85);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(73, 17);
            this.chkSelectAll.TabIndex = 4;
            this.chkSelectAll.Text = " Select All";
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // cmbDbDestination
            // 
            this.cmbDbDestination.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDbDestination.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDbDestination.FormattingEnabled = true;
            this.cmbDbDestination.Location = new System.Drawing.Point(152, 51);
            this.cmbDbDestination.Name = "cmbDbDestination";
            this.cmbDbDestination.Size = new System.Drawing.Size(200, 21);
            this.cmbDbDestination.TabIndex = 3;
            // 
            // lblDbDestination
            // 
            this.lblDbDestination.AutoSize = true;
            this.lblDbDestination.Location = new System.Drawing.Point(23, 54);
            this.lblDbDestination.Name = "lblDbDestination";
            this.lblDbDestination.Size = new System.Drawing.Size(109, 13);
            this.lblDbDestination.TabIndex = 3;
            this.lblDbDestination.Text = "Database Destination";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(418, 17);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(90, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(152, 19);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(250, 20);
            this.txtFolder.TabIndex = 1;
            // 
            // lblFileFolder
            // 
            this.lblFileFolder.AutoSize = true;
            this.lblFileFolder.Location = new System.Drawing.Point(23, 22);
            this.lblFileFolder.Name = "lblFileFolder";
            this.lblFileFolder.Size = new System.Drawing.Size(55, 13);
            this.lblFileFolder.TabIndex = 0;
            this.lblFileFolder.Text = "File Folder";
            // 
            // gbListView
            // 
            this.gbListView.Controls.Add(this.lvTerminalName);
            this.gbListView.Controls.Add(this.lvFileName);
            this.gbListView.Location = new System.Drawing.Point(13, 114);
            this.gbListView.Name = "gbListView";
            this.gbListView.Size = new System.Drawing.Size(523, 336);
            this.gbListView.TabIndex = 1;
            this.gbListView.TabStop = false;
            // 
            // lvTerminalName
            // 
            this.lvTerminalName.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTerminalName});
            this.lvTerminalName.Location = new System.Drawing.Point(269, 19);
            this.lvTerminalName.MultiSelect = false;
            this.lvTerminalName.Name = "lvTerminalName";
            this.lvTerminalName.Size = new System.Drawing.Size(241, 305);
            this.lvTerminalName.TabIndex = 1;
            this.lvTerminalName.UseCompatibleStateImageBehavior = false;
            this.lvTerminalName.View = System.Windows.Forms.View.Details;
            // 
            // colTerminalName
            // 
            this.colTerminalName.Text = "Terminal Name";
            this.colTerminalName.Width = 235;
            // 
            // lvFileName
            // 
            this.lvFileName.CheckBoxes = true;
            this.lvFileName.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFileName});
            this.lvFileName.Location = new System.Drawing.Point(12, 19);
            this.lvFileName.MultiSelect = false;
            this.lvFileName.Name = "lvFileName";
            this.lvFileName.Size = new System.Drawing.Size(241, 305);
            this.lvFileName.TabIndex = 0;
            this.lvFileName.UseCompatibleStateImageBehavior = false;
            this.lvFileName.View = System.Windows.Forms.View.Details;
            this.lvFileName.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvFileName_ItemChecked);
            this.lvFileName.SelectedIndexChanged += new System.EventHandler(this.lvFileName_SelectedIndexChanged);
            // 
            // colFileName
            // 
            this.colFileName.Text = "File Name";
            this.colFileName.Width = 235;
            // 
            // pbImport
            // 
            this.pbImport.Location = new System.Drawing.Point(12, 456);
            this.pbImport.Name = "pbImport";
            this.pbImport.Size = new System.Drawing.Size(524, 23);
            this.pbImport.TabIndex = 2;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Controls.Add(this.btnStart);
            this.gbButton.Location = new System.Drawing.Point(12, 479);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(524, 41);
            this.gbButton.TabIndex = 3;
            this.gbButton.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(418, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(312, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "&Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // bwImport
            // 
            this.bwImport.WorkerReportsProgress = true;
            this.bwImport.WorkerSupportsCancellation = true;
            this.bwImport.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwImport_DoWork);
            this.bwImport.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwImport_ProgressChanged);
            this.bwImport.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwImport_RunWorkerCompleted);
            // 
            // FrmImportProfile
            // 
            this.AcceptButton = this.btnStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(548, 528);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.pbImport);
            this.Controls.Add(this.gbListView);
            this.Controls.Add(this.gbFolder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmImportProfile";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Import Text File";
            this.Load += new System.EventHandler(this.FrmImportData_Load);
            this.gbFolder.ResumeLayout(false);
            this.gbFolder.PerformLayout();
            this.gbListView.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbFolder;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Label lblFileFolder;
        private System.Windows.Forms.ComboBox cmbDbDestination;
        private System.Windows.Forms.Label lblDbDestination;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.CheckBox chkSelectAll;
        private System.Windows.Forms.CheckBox chkUpdateAll;
        private System.Windows.Forms.GroupBox gbListView;
        private System.Windows.Forms.ListView lvFileName;
        private System.Windows.Forms.ListView lvTerminalName;
        private System.Windows.Forms.ProgressBar pbImport;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.ColumnHeader colTerminalName;
        private System.Windows.Forms.ColumnHeader colFileName;
        private System.ComponentModel.BackgroundWorker bwImport;
    }
}