﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmMoveData : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        int iDatabaseIdSource;
        int iDatabaseIdDest;
        List<string> ltTerminalId = new List<string>();
        DataSet dsTables;

        public FrmMoveData(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        private void FrmMoveData_Load(object sender, EventArgs e)
        {
            Init();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
                            SetDisplay(false);
                            InitProperties();
                            bwMoveData.RunWorkerAsync();
                            //CommonClass.inputLog(oSqlConn, "", "", "", CommonMessage.sExportDataToDB, "");
        }

        private void cmbDbSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.SelectedIndex >= 0)
                {
                    iDatabaseIdSource = int.Parse(cmbDbSource.SelectedValue.ToString());
                    FillTerminalList();
                }
        }

        private void cmbDbDestination_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbDestination.DisplayMember) && !string.IsNullOrEmpty(cmbDbDestination.ValueMember))
                if (cmbDbDestination.SelectedIndex >= 0)
                {
                    iDatabaseIdDest = int.Parse(cmbDbDestination.SelectedValue.ToString());
                    FillTerminalList();
                }
        }

        private void chkMasterOnly_CheckedChanged(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
            //    if (cmbDbSource.SelectedIndex >= 0)
            //    {
            //        iDatabaseIdSource = int.Parse(cmbDbSource.SelectedValue.ToString());
            //        FillTerminalList();
            //    }
        }

        private void rbAll_CheckedChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.SelectedIndex >= 0)
                {
                    iDatabaseIdSource = int.Parse(cmbDbSource.SelectedValue.ToString());
                    FillTerminalList();
                }
        }

        private void rbMaster_CheckedChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.SelectedIndex >= 0)
                {
                    iDatabaseIdSource = int.Parse(cmbDbSource.SelectedValue.ToString());
                    FillTerminalList();
                }

        }

        private void rbNotMaster_CheckedChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.SelectedIndex >= 0)
                {
                    iDatabaseIdSource = int.Parse(cmbDbSource.SelectedValue.ToString());
                    FillTerminalList();
                }

        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            bool bChecked = chkSelectAll.Checked;
            if (bChecked==true)
            for (int iIndex = 0; iIndex < chlTerminal.Items.Count; iIndex++)
            chlTerminal.SetItemChecked(iIndex, bChecked);
        }

        private void bwMoveData_DoWork(object sender, DoWorkEventArgs e)
        {
            int icount = 0;
            string svalue;
            int iTotalCount = chlTerminal.Items.Count;
            foreach (object checkeditem in chlTerminal.CheckedItems)
            {
                string checkItem = chlTerminal.GetItemCheckState(chlTerminal.Items.IndexOf(checkeditem)).ToString();
                if (checkItem == "Checked")
                {
                    icount = icount + 1;
                }
            }

            if (iDatabaseIdDest != 0)
                {
                    if (iDatabaseIdSource != 0)
                    {
                        if (icount != 0)
                        {
                            icount = 0;
                            svalue = null;
                            foreach (object checkeditem in chlTerminal.CheckedItems)
                            {
                                string checkItem = chlTerminal.GetItemCheckState(chlTerminal.Items.IndexOf(checkeditem)).ToString();
                                if (checkItem == "Checked")
                                {

                                    svalue = chlTerminal.Items[icount].ToString();

                                    SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPMoveData, oSqlConn);
                                    {
                                        oSqlCmd.CommandType = CommandType.StoredProcedure;
                                        oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = svalue;
                                        oSqlCmd.Parameters.Add("@iDBSourceID", SqlDbType.VarChar).Value = iDatabaseIdSource;
                                        oSqlCmd.Parameters.Add("@iDBDestID", SqlDbType.VarChar).Value = iDatabaseIdDest;
                                    }
                                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                                    oSqlCmd.ExecuteNonQuery();

                                    //MessageBox.Show(svalue);
                                    //string sQuery = string.Format("update tbProfileTerminalList set DatabaseID = {0} where TerminalID = '{1}'", iDatabaseIdDest, svalue);
                                    //SqlCommand ComUpdate = new SqlCommand(sQuery, oSqlConn);
                                    //ComUpdate.ExecuteNonQuery();
                                    icount = icount + 1;
                                }
                            } 
                        }
                        else { MessageBox.Show("Please Check Terminal"); }
                    }
                    else { MessageBox.Show("Please Choose Database Source"); }
                }
            else { MessageBox.Show("Please Choose Database Destination"); }
        }

        private void bwMoveData_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }

        private void bwMoveData_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay(true);
            FillTerminalList();
            chkSelectAll.Checked = false;
            
        }

        private void chlTerminal_SelectedIndexChanged(object sender, EventArgs e)
        {
            int icount=0;
            int iTotalCount = chlTerminal.Items.Count;
            foreach (object checkeditem in chlTerminal.CheckedItems)
            {
                string checkItem = chlTerminal.GetItemCheckState(chlTerminal.Items.IndexOf(checkeditem)).ToString();
                if (checkItem == "Checked")
                {
                    icount = icount + 1;
                }
            }

            if (icount != iTotalCount)
            {
                chkSelectAll.Checked = false;
            }
            else if (icount == iTotalCount)
            {
                chkSelectAll.Checked = true;
            }
        }
            
        #region "Function"
        /// <summary>
        /// Initialize Data Form Move Data
        /// </summary>
        private void Init()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbDestination);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
        }

        private void FillTerminalList()
        {
            chlTerminal.Items.Clear();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sDbConditions();
                using (SqlDataAdapter oAdapt = new SqlDataAdapter(oCmd))
                {
                    using (DataTable dtTerminalList = new DataTable())
                    {
                        oAdapt.Fill(dtTerminalList);
                        if (dtTerminalList.Rows.Count > 0)
                            foreach (DataRow row in dtTerminalList.Rows)
                                chlTerminal.Items.Add(row["TerminalID"].ToString());
                    }
                }
            }
        }

        private void InitProperties()
        {
            for (int i = 0; i < chlTerminal.Items.Count; i++)
                if (chlTerminal.GetItemChecked(i))
                    ltTerminalId.Add(chlTerminal.Items[i].ToString());
            InitTable();
        }

        /// <summary>
        /// Get Mapping Column
        /// </summary>
        private void InitTable()
        {
            dsTables = new DataSet();
            string sQuery = string.Format(
                "exec spItemListBrowse 'WHERE DatabaseID={0}'\n{1}\n{2}\n{3}\n{4}\n{5}", iDatabaseIdSource
                , "exec spItemListBrowse 'WHERE DatabaseID={6}'\n{1}\n{2}\n{3}\n{4}\n{5}"
                , "SELECT * FROM tbProfileTerminalList WHERE TerminalID IN ({7})"
                , "SELECT TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue FROM tbProfileTerminal WHERE TerminalID IN ({7})"
                , "SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID IN ({7})"
                , "SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue FROM tbProfileIssuer WHERE TerminalID  IN ({7})"
                , "SELECT TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue FROM tbProfileRelation WHERE TerminalID IN ({7})"
                , iDatabaseIdDest
                , sTerminalIdList());
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                SqlDataAdapter oAdapt = new SqlDataAdapter(oCmd);
                oAdapt.Fill(dsTables);
            }
        }

        /// <summary>
        /// Get Condition of Database
        /// </summary>
        /// <returns>string: Condition of Database</returns>
        private object sDbConditions()
        {
            if (rbMaster.Checked)
            {
                return string.Format("WHERE StatusMaster = 1 AND DatabaseID = {0}", iDatabaseIdSource);
            }
            else if (rbNotMaster.Checked)
            {
                return string.Format("WHERE StatusMaster = 0 AND DatabaseID = {0}", iDatabaseIdSource);
            }
            else
            {
                return string.Format("WHERE DatabaseID = {0}", iDatabaseIdSource);
            }

            
        }

        /// <summary>
        /// Set Display
        /// </summary>
        /// <param name="isEnable">Bool: if enable True</param>
        private void SetDisplay(bool isEnable)
        {
            gbButton.Enabled = isEnable;
            gbDestination.Enabled = isEnable;
            gbSource.Enabled = isEnable;
            pbMoveData.Style = isEnable ? ProgressBarStyle.Blocks : ProgressBarStyle.Marquee;
        }

        /// <summary>
        /// Get list TerminalID in string
        /// </summary>
        /// <returns>string : list Terminal ID</returns>
        protected string sTerminalIdList()
        {
            string sValue = null;
            foreach (string sTemp in ltTerminalId)
                sValue = string.IsNullOrEmpty(sValue) ? string.Format("'{0}'", sTemp) : string.Format("{0},'{1}'", sValue, sTemp);
            return sValue;
        }
        #endregion
    }
}
