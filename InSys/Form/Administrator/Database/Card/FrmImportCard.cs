﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using InSysClass;
namespace InSys
{
    public partial class FrmImportCard : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        string sFileContent,sFileName, sDatabaseID, sFileDirectory;
        /// <summary>
        /// Import Card to Database
        /// </summary>
        /// <param name="_oSqlConn">SQL Connection : Connection String to database</param>
        /// <param name="_oSqlConnAuditTrail">SQL Connection : Connection String to database</param>
        public FrmImportCard(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        /// <summary>
        /// Browse destination file
        /// </summary>
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofdDialogSource = new OpenFileDialog())
            {
                
                ofdDialogSource.ShowDialog();

                if (!string.IsNullOrEmpty(ofdDialogSource.FileName))
                {
                    sFileDirectory = ofdDialogSource.FileName;
                    txtDestionationFolder.Text = sFileDirectory;
                    sFileName = ofdDialogSource.SafeFileName;
                    sFileContent = ReadFileTxt(sFileDirectory);
                }
            }
        }

        /// <summary>
        /// Read file Value
        /// </summary>
        /// <param name="sFileName">sFileName: Contain Name File </param>
        /// <returns></returns>
        private string ReadFileTxt(string sFileName)
        {
            string sContent = "";
            StreamReader srReader = new StreamReader(sFileName);
            sContent = srReader.ReadToEnd();
            srReader.Close();
            return sContent;
        }

        /// <summary>
        /// Load Data in Form Impoert Card
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmImportCard_Load(object sender, EventArgs e)
        {
            InitCmb();
        }

        /// <summary>
        /// Fill ComboBox with Database Name
        /// </summary>
        protected void InitCmb()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDatabase);
        }

        /// <summary>
        /// Do Function Import
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImport_Click(object sender, EventArgs e)
        {
            int iTagLenght;
            
            if (!string.IsNullOrEmpty(txtDestionationFolder.Text) && cmbDatabase.SelectedIndex >-1)
            {
                sDatabaseID = cmbDatabase.SelectedValue.ToString();
                //if (isCardExist(sFileContent, sDatabaseID))
                //{
                    string sAllCard = "";
                    sAllCard = sGetAllCard(sFileContent, sDatabaseID);
                    iTagLenght = iGetTagLenght(sDatabaseID);
                    SqlCommand oCmd;
                    if (iTagLenght == 4)
                        oCmd = new SqlCommand("insert into tbProfileCardList (DatabaseID,CardName) select distinct " + sDatabaseID + ",Name from dbo.fn_tbProfileTLV ('','" + sFileContent + "') where Name in (" + sAllCard + ") insert into tbProfileCard (CardID,CardTag,CardLengthOfTagLength,CardTagLength,CardTagValue) select a.CardID,b.Tag,len(cast(b.TagLength as int)),cast(b.TagLength as int),TagValue from tbProfileCardList a join dbo.fn_tbProfileTLV('','" + sFileContent + "') b on a.CardName=b.Name where a.DatabaseID = " + sDatabaseID + "and b.Name in (" + sAllCard + ")", oSqlConn);
                    else
                        oCmd = new SqlCommand("insert into tbProfileCardList (DatabaseID,CardName) select distinct " + sDatabaseID + ",Name from dbo.fn_tbProfileTLV5 ('','" + sFileContent + "') where Name in (" + sAllCard + ") insert into tbProfileCard (CardID,CardTag,CardLengthOfTagLength,CardTagLength,CardTagValue) select a.CardID,b.Tag,len(cast(b.TagLength as int)),cast(b.TagLength as int),TagValue from tbProfileCardList a join dbo.fn_tbProfileTLV5('','" + sFileContent + "') b on a.CardName=b.Name where a.DatabaseID = " + sDatabaseID + "and b.Name in (" + sAllCard + ")", oSqlConn);

                    if (oSqlConn.State == ConnectionState.Closed)
                        oSqlConn.Open();

                    oCmd.ExecuteNonQuery();
                    oCmd.Dispose();
                    //File.Delete(sFileDirectory);
                    //CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, "", "Import Card ", sFileContent);
                    CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", "Import Card ", sFileContent);
                    MessageBox.Show("Done");
                //}
                //else MessageBox.Show("Card Name is exist");

            }
            else MessageBox.Show("Please Choose Database and File First");
        }

        /// <summary>
        /// Get All Card Value
        /// </summary>
        /// <param name="sFileContent">string : File Content</param>
        /// <param name="sDatabaseID">string : Database ID Value</param>
        /// <returns></returns>
        private string sGetAllCard(string sFileContent, string sDatabaseID)
        {
            string sCard = "";
            int iTagLenght;
            DataTable dtCardList = new DataTable();
            iTagLenght = iGetTagLenght(sDatabaseID);
            SqlCommand oCmd;
            if (iTagLenght == 4)
                oCmd = new SqlCommand("select distinct Name from dbo.fn_tbProfileTLV('','" + sFileContent + "') where Name not in (select CardName from tbProfileCardList where DatabaseID ="+sDatabaseID+")", oSqlConn);
            else
                oCmd = new SqlCommand("select distinct Name from dbo.fn_tbProfileTLV5('','" + sFileContent + "') where Name not in (select CardName from tbProfileCardList where DatabaseID =" + sDatabaseID + ")", oSqlConn);

            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();
            dtCardList.Clear();
            new SqlDataAdapter(oCmd).Fill(dtCardList);
            oCmd.Dispose();

            int iIndex = 0;
            foreach (DataRow oRow in dtCardList.Rows)
            {
                sCard =sCard+ "'"+dtCardList.Rows[iIndex]["Name"].ToString()+"',";
                iIndex = iIndex + 1;
            }
            sCard = sCard.Substring(0, (sCard.Length) - 1);
            return sCard;
        }

        /// <summary>
        /// Get Lenght of Tag 
        /// </summary>
        /// <param name="sDatabaseID">string : Database ID Value</param>
        /// <returns>int : Lenght of Tag</returns>
        private int iGetTagLenght(string sDatabaseID)
        {
            int iLen = 0;
            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.isTagLength4({0})",sDatabaseID), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    iLen = int.Parse(oRead[0].ToString());
                oRead.Close();
            }
            return iLen;
        }

        /// <summary>
        /// Check Card is exit or not
        /// </summary>
        /// <param name="sFileContent">string : File Content</param>
        /// <param name="sDatabaseID">string : Database ID</param>
        /// <returns></returns>
        private bool isCardExist(string sFileContent, string sDatabaseID)
        {
            DataTable dtCountCardList = new DataTable();
            SqlCommand oCmd = new SqlCommand("select * from tbProfileCardList where CardName in (select distinct Name from dbo.fn_tbProfileTLV('','" + sFileContent + "')) and DatabaseID=" + sDatabaseID, oSqlConn);
            
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            new SqlDataAdapter(oCmd).Fill(dtCountCardList);
            oCmd.Dispose();

            if (dtCountCardList.Rows.Count > 0)
                return false;
            else return true;
        }
    }
}
