using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmTag : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected bool isDelete;
        protected string sStatus = "";
        protected string sCurrDbID = "";

        /// <summary>
        /// Form to modify tags in versions
        /// </summary>
        /// <param name="_oSqlConn"></param>
        /// <param name="_isDelete"></param>
        public FrmTag(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, bool _isDelete)
        {
            InitializeComponent();

            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            isDelete = _isDelete;
        }

        /// <summary>
        /// Load Form Tag
        /// </summary>
        private void FrmTag_Load(object sender, EventArgs e)
        {
            InitCmb();
            InitDisplay();
            InitData();
            CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }
        
        /// <summary>
        /// Run Fun
        /// </summary>
        private void FrmTag_FormClosing(object sender, FormClosingEventArgs e)
        {
             e.Cancel = !isItemSequenceValid(sGetDbId());
        }

        private void btnAddComboBoxValue_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable oTable = (DataTable)dgCmbValue.DataSource;
                if (oTable != null)
                    if (string.IsNullOrEmpty(sGetFormValue()))
                        MessageBox.Show("Please choose Form first.");
                    else if (string.IsNullOrEmpty(sGetDisplayValue()) || string.IsNullOrEmpty(sGetRealValue()))
                        MessageBox.Show("Please fill Display Value or Real Value firs.");
                    else if (CommonClass.isYesMessage(string.Format("Are you sure want to use current FormID : {0}",
                            sGetFormValue()), CommonMessage.sConfirmationTitle))
                    {
                        DataRow drRow = oTable.NewRow();
                        drRow["DisplayValue"] = sGetDisplayValue();
                        drRow["RealValue"] = sGetRealValue();
                        oTable.Rows.Add(drRow);
                        dgCmbValue.Refresh();
                        txtCmbDisplayValue.Clear();
                        txtCmbRealValue.Clear();
                    }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void bntDeleteCmbValue_Click(object sender, EventArgs e)
        {
            dgCmbValue.Focus();
            if (dgCmbValue.RowCount > 0) dgCmbValue.Rows.Remove(dgCmbValue.CurrentRow);
        }

        /// <summary>
        /// Run Save Function
        /// </summary>
        private void btnAddSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!isDelete)
                {
                    if (dgViewTagItem.DataSource != null)
                        if (sStatus == "")
                            SetStatusToNew();
                        else
                            SaveData();
                }
                else
                    if (isDeleteData())
                        InitData();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Run Cancel Function
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (sStatus == "")
                this.Close();
            else
                SetStatusToNormal();
        }

        private void cmbDbId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbId.DisplayMember) && !string.IsNullOrEmpty(cmbDbId.ValueMember))
                if (cmbDbId.Items.Count > 0)
                    if (sCurrDbID != sGetDbId())
                        if (isItemSequenceValid(sCurrDbID))
                        {
                            sCurrDbID = sGetDbId();
                            InitData();
                        }
                        else
                            cmbDbId.SelectedValue = string.IsNullOrEmpty(sCurrDbID) ? null : sCurrDbID;
        }

        private void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            if (dgViewTagItem.ColumnCount > 0)
                foreach (DataGridViewRow dgvRow in dgViewTagItem.Rows)
                    dgvRow.Cells["ColDelete"].Value = chkCheckAll.Checked;
        }

        /// <summary>
        /// Refresh information when Data Grid View Content click
        /// </summary>
        private void dgViewTagItem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgViewTagItem.Focus();
            if (e.RowIndex >= 0)
                if ((e.ColumnIndex == dgViewTagItem.Columns["ColEdit"].Index) || (e.ColumnIndex == dgViewTagItem.Columns["ColDelete"].Index))
                    if (isDelete)
                        dgViewTagItem["ColDelete", e.RowIndex].Value = !Convert.ToBoolean(dgViewTagItem["ColDelete", e.RowIndex].Value);
                    else
                        SetStatusToEdit();
        }

        /// <summary>
        /// Reload Data base i=on Form ID
        /// </summary>
        private void dgViewTagItem_SelectionChanged(object sender, EventArgs e)
        {
            dgViewTagItem.Focus();
            ClearTagDetail();
            if (dgViewTagItem.RowCount > 0)
                if (dgViewTagItem.CurrentRow.Index >= 0)
                    FillTagDetail();
        }

        private void rdObjCmb_CheckedChanged(object sender, EventArgs e)
        {
            pnlCmbValue.Enabled = rdObjCmb.Checked;
            ClearComboBoxValue();
            if (rdObjCmb.Checked)
                InitCmbValue();
        }

        private void cmbFormType_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitData();
        }

        #region "Function"
        /// <summary>
        /// Initiate ComboBox to fill data from database
        /// </summary>
        protected void InitCmb()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbId);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPMsItemFormBrowse, "", "FormId", "FormName", ref cmbFormType, 0);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPMsItemFormBrowse, "WHERE FormID!=0", "FormId", "FormName", ref cmbForm);
        }

        /// <summary>
        /// Initiate Display Form
        /// </summary>
        protected void InitDisplay()
        {
            this.Text += (isDelete) ? "Delete" : "Add/Update";
            gbTagDetail.Enabled = false;
            dgViewTagItem.Columns["ColEdit"].Visible = false;
            dgViewTagItem.Columns["ColDelete"].Visible = false;
            chkCheckAll.Visible = isDelete;
            SetButtonDisplay();
        }

        /// <summary>
        /// Load data from database then show the on DataGridView
        /// </summary>
        protected void InitData()
        {
            if (cmbDbId.SelectedIndex >= 0)
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sGetTagCondition();

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        DataTable oTable = new DataTable();
                        oAdapt.Fill(oTable);
                        InitDataGrid(oTable);
                    }
                }
        }

        /// <summary>
        /// Initiate DataGridView, set its display and format
        /// </summary>
        /// <param name="oTable">DataTable : Data Source to be used</param>
        protected void InitDataGrid(DataTable oTable)
        {
            dgViewTagItem.DataSource = null;
            chkCheckAll.Checked = false;
            dgViewTagItem.DataSource = oTable;

            dgViewTagItem.Columns["ItemID"].Visible = false;
            dgViewTagItem.Columns["DatabaseID"].Visible = false;
            dgViewTagItem.Columns["ColEdit"].Visible = !isDelete;
            dgViewTagItem.Columns["ColDelete"].Visible = isDelete;

            if (isDelete) dgViewTagItem.Columns["ColDelete"].DisplayIndex = 0;
            dgViewTagItem.Columns["Tag"].DisplayIndex = 1;
            dgViewTagItem.Columns["ItemName"].DisplayIndex = 2;

            dgViewTagItem.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgViewTagItem.Columns["DefaultValue"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        }

        /// <summary>
        /// Initiate objects that used to display detail data of selected tag
        /// </summary>
        /// <param name="isEnable">boolean : true if objects is enabled, else false</param>
        protected void InitDataDetail(bool isEnable)
        {
            gbTagDetail.Enabled = isEnable;
            gbDatabase.Enabled = !isEnable;
            dgViewTagItem.Enabled = !isEnable;
            txtTagID.Enabled = (sStatus != "edit");
            SetButtonDisplay();
        }

        /// <summary>
        /// Initiate ComboBox, load data from database to be shown in ComboBox
        /// </summary>
        protected void InitCmbValue()
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCmbBrowse, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sGetCmbValueCondition();
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                {
                    DataTable oTable = new DataTable();
                    oAdapt.Fill(oTable);
                    FillDgCmbValue(oTable);
                }
            }
        }

        /// <summary>
        /// Get Selected DatabaseID value from combobox
        /// </summary>
        /// <returns>string : Database ID</returns>
        protected string sGetDbId()
        {
            return (cmbDbId.SelectedIndex >= 0) ? cmbDbId.SelectedValue.ToString() : "";
        }

        /// <summary>
        /// Generate condition string to filter data from database
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sGetTagCondition()
        {
            string sConditionString = string.Format("WHERE DatabaseID = {0}", sGetDbId());
            string sConditionDetail =
                cmbFormType.SelectedValue.ToString() == "0" ? null : string.Format("FormID={0}", cmbFormType.SelectedValue);
            //if (chkTerminal.Checked)
            //    sConditionDetail = sGetConditionDetail(sConditionDetail, "D");
            //if (chkAcquirer.Checked)
            //    sConditionDetail = sGetConditionDetail(sConditionDetail, "AA");
            //if (chkIssuer.Checked)
            //    sConditionDetail = sGetConditionDetail(sConditionDetail, "AE");
            //if (chkCard.Checked)
            //    sConditionDetail = sGetConditionDetail(sConditionDetail, "AC");
            //if (chkTLE.Checked)
            //    sConditionDetail = sGetConditionDetail(sConditionDetail, "TL");
            //if (chkLoyPool.Checked)
            //    sConditionDetail = sGetConditionDetail(sConditionDetail, "LA");
            //if (chkLoyProd.Checked)
            //    sConditionDetail = sGetConditionDetail(sConditionDetail, "LB");
            //if (chkGPRS.Checked)
            //    sConditionDetail = sGetConditionDetail(sConditionDetail, "GP");

            if (!string.IsNullOrEmpty(sConditionDetail))
                sConditionDetail = string.Format(" AND ( {0} )", sConditionDetail);
            return sConditionString + sConditionDetail;
        }

        /// <summary>
        /// Get Condition string to filter data based on given tag value
        /// </summary>
        /// <param name="sCondition">string : current Condition string</param>
        /// <param name="sTag">string : tag value which will be use to filter database</param>
        /// <returns>string : New Condition string</returns>
        protected string sGetConditionDetail(string sCondition, string sTag)
        {
            if (string.IsNullOrEmpty(sCondition))
                sCondition += string.Format(" Tag LIKE '{0}%'", sTag);
            else
                sCondition += string.Format(" OR Tag LIKE '{0}%'", sTag);
            return sCondition;
        }

        /// <summary>
        /// Set form status to Edit
        /// </summary>
        protected void SetStatusToEdit()
        {
            sStatus = "edit";
            InitDataDetail(true);
            SetButtonDisplay();
        }

        /// <summary>
        /// Set form status to Normal
        /// </summary>
        protected void SetStatusToNormal()
        {
            sStatus = "";
            InitDataDetail(false);
            SetButtonDisplay();
            FillTagDetail();
        }

        /// <summary>
        /// Set form status to New
        /// </summary>
        protected void SetStatusToNew()
        {
            sStatus = "new";
            InitDataDetail(true);
            ClearTagDetail();
        }

        /// <summary>
        /// Determine text that will be show in button
        /// </summary>
        protected void SetButtonDisplay()
        {
            btnAddSave.Text = (isDelete) ? "Delete Selected" : (sStatus == "") ? "Add" : "Save";
        }

        /// <summary>
        /// show Detail data of selected tag
        /// </summary>
        protected void FillTagDetail()
        {
            if (dgViewTagItem.RowCount > 0)
            {
                ClearTagDetail();
                dgViewTagItem.Focus();
                txtTagID.Text = sGetCurrValue("Tag");
                txtTagName.Text = sGetCurrValue("ItemName");
                txtDefaultValue.Text = sGetCurrValue("DefaultValue");
                txtItemSequence.Text = sGetCurrValue("ItemSequence");
                txtMinLength.Text = sGetCurrValue("vMinLength");
                txtMaxLength.Text = sGetCurrValue("vMaxLength");
                txtMinValue.Text = sGetCurrValue("vMinValue");
                txtMaxValue.Text = sGetCurrValue("vMaxValue");
                txtValidationMsg.Text = sGetCurrValue("ValidationMsg");
                SetComboBoxSelected(sGetCurrValue("FormID"), cmbForm);
                SetRadioButtonChecked(sGetCurrValue("ObjectName"), pnlObject);
                SetRadioButtonChecked(sGetCurrValue("vType"), pnlValueType);
                SetRadioButtonChecked(sGetCurrValue("vAllowNull"), pnlAllowNull);
                SetRadioButtonChecked(sGetCurrValue("vUpperCase"), pnlUpperCase);
                SetRadioButtonChecked(sGetCurrValue("vMasking"), pnlMasking);
            }
        }

        /// <summary>
        /// Clear all values shown in Detail Data region
        /// </summary>
        protected void ClearTagDetail()
        {
            txtTagID.Clear();
            txtTagName.Clear();
            txtDefaultValue.Clear();
            txtItemSequence.Clear();
            txtMinLength.Clear();
            txtMaxLength.Clear();
            txtMinValue.Clear();
            txtMaxValue.Clear();
            txtValidationMsg.Clear();
            //ClearRadioButtonChecked(pnlForm);
            ClearRadioButtonChecked(pnlObject);
            ClearRadioButtonChecked(pnlValueType);
            ClearRadioButtonChecked(pnlAllowNull);
            ClearRadioButtonChecked(pnlUpperCase);
        }

        /// <summary>
        /// Clear values in ComboBox
        /// </summary>
        protected void ClearComboBoxValue()
        {
            txtCmbDisplayValue.Clear();
            txtCmbRealValue.Clear();
            dgCmbValue.DataSource = null;
            //while (dgCmbValue.RowCount > 0)
            //    dgCmbValue.Rows.RemoveAt(0);
        }

        /// <summary>
        /// Get current value from DataGridView of given ColumnName
        /// </summary>
        /// <param name="sColumnName">string : Column name which value will be taken</param>
        /// <returns>string : Value of given ColumnName</returns>
        protected string sGetCurrValue(string sColumnName)
        {
            if (dgViewTagItem.RowCount <= 0 || dgViewTagItem.CurrentCell == null)
                return "";
            string sValue = dgViewTagItem.CurrentRow.Cells[sColumnName].Value.ToString();
            return (string.IsNullOrEmpty(sValue)) ? "" : sValue;
        }

        protected void SetComboBoxSelected(string sID, ComboBox oCombo)
        {
            oCombo.SelectedIndex = oCombo.Items.IndexOf(sID);
            foreach (DataRowView rowview in oCombo.Items)
                if (rowview["FormId"].ToString() == sID) oCombo.SelectedItem = rowview;
        }

        /// <summary>
        /// Set RadioButton CheckedStatus into true
        /// </summary>
        /// <param name="sID">string : Tag ID </param>
        /// <param name="oPanel">Panel : Panel which hold desired checkbox</param>
        protected void SetRadioButtonChecked(string sID, Panel oPanel)
        {
            Control.ControlCollection oCtrlColl = oPanel.Controls;
            foreach (Control oCtrl in oCtrlColl)
                if (oCtrl.Tag.ToString() == sID)
                {
                    ((RadioButton)oCtrl).Checked = true; break;
                }
        }

        /// <summary>
        /// Set RadioButton CheckedStatus into false
        /// </summary>
        /// <param name="oPanel">Panel : Panel which hold desired checkbox</param>
        protected void ClearRadioButtonChecked(Panel oPanel)
        {
            Control.ControlCollection oCtrlColl = oPanel.Controls;
            foreach (Control oCtrl in oCtrlColl)
                ((RadioButton)oCtrl).Checked = false;
        }

        /// <summary>
        /// Get Real Value from combobox
        /// </summary>
        /// <returns>string : ComboBox's Real Value</returns>
        protected string sGetRealValue()
        {
            return txtCmbRealValue.Text.Trim();
        }

        /// <summary>
        /// Get DisplayValue of ComboBox
        /// </summary>
        /// <returns>string : ComboBox's Display Value</returns>
        protected string sGetDisplayValue()
        {
            return txtCmbDisplayValue.Text.Trim();
        }

        /// <summary>
        /// Generate Condition string to filter data loaded from database
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sGetCmbValueCondition()
        {
            return string.Format("WHERE DatabaseID = {0} AND FormID = {1} AND ItemSequence = {2}",
                                    sGetDbId(), sGetCurrValue("FormID"), sGetCurrValue("ItemSequence"));
        }

        /// <summary>
        /// Fill DataGridView with given datasource
        /// </summary>
        /// <param name="oTable">DataTable : DataSource which will be binded into DGV</param>
        protected void FillDgCmbValue(DataTable oTable)
        {
            dgCmbValue.DataSource = oTable;

            foreach (DataGridViewColumn oCol in dgCmbValue.Columns)
                oCol.Visible = false;

            dgCmbValue.Columns["DisplayValue"].Visible = true;
            dgCmbValue.Columns["RealValue"].Visible = true;

            dgCmbValue.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        /// <summary>
        /// Save entered data
        /// </summary>
        protected void SaveData()
        {
            if (isValid())
                if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                    if (isSuccessSave())
                    {
                        SetStatusToNormal();
                        InitData();
                    }
        }

        /// <summary>
        /// Determine whether user entered data valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool isValid()
        {
            if (isNotEmpty())
                if (isTagValid())
                    if (isNumeric())
                        return true;
            return false;
        }

        /// <summary>
        /// Determine whether data entered empty or not
        /// </summary>
        /// <returns>boolean : true if not empty</returns>
        protected bool isNotEmpty()
        {
            string sErrMsg = "";

            if (string.IsNullOrEmpty(sGetTag()))
                sErrMsg = string.Format("{0} {1} {0}", "Tag ID", CommonMessage.sIsEmpty);
            else if (string.IsNullOrEmpty(sGetTagName()))
                sErrMsg = string.Format("{0} {1} {0}", "Tag Name", CommonMessage.sIsEmpty);
            else if (string.IsNullOrEmpty(sGetFormValue()))
                sErrMsg = string.Format("{0}{1}", "Form", CommonMessage.sIsEmpty);
            else if (string.IsNullOrEmpty(sGetRadioButtonCheckedValue(pnlValueType)))
                sErrMsg = string.Format("{0}{1}", "Value Type", CommonMessage.sNotChecked);
            else if (string.IsNullOrEmpty(sGetRadioButtonCheckedValue(pnlObject)))
                sErrMsg = string.Format("{0}{1}", "Object Type", CommonMessage.sNotChecked);
            else if (string.IsNullOrEmpty(sGetRadioButtonCheckedValue(pnlAllowNull)))
                sErrMsg = string.Format("{0}{1}", "Allow Null", CommonMessage.sNotChecked);
            else if (string.IsNullOrEmpty(sGetRadioButtonCheckedValue(pnlUpperCase)))
                sErrMsg = string.Format("{0}{1}", "Upper Case", CommonMessage.sNotChecked);
            else if (string.IsNullOrEmpty(sGetItemSequence()))
                sErrMsg = string.Format("{0} {1} {0}", "Item Sequence", CommonMessage.sIsEmpty);
            else if (string.IsNullOrEmpty(sGetMinLength()))
                sErrMsg = string.Format("{0} {1} {0}", "Minimum Length", CommonMessage.sIsEmpty);
            else if (string.IsNullOrEmpty(sGetMaxLength()))
                sErrMsg = string.Format("{0} {1} {0}", "Maximum Length", CommonMessage.sIsEmpty);
            else if (string.IsNullOrEmpty(sGetMinValue()))
                sErrMsg = string.Format("{0} {1} {0}", "Minimum Value", CommonMessage.sIsEmpty);
            else if (string.IsNullOrEmpty(sGetMaxValue()))
                sErrMsg = string.Format("{0} {1} {0}", "Maximum Value", CommonMessage.sIsEmpty);
            else if (string.IsNullOrEmpty(sGetValidationMsg()))
                sErrMsg = string.Format("{0} {1} {0}", "Validation Message", CommonMessage.sIsEmpty);
            
            return isShowErrorMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether enterd Tag value valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool isTagValid()
        {
            if (sStatus == "edit")
                return true;

            DataTable oTable = (DataTable)dgViewTagItem.DataSource;
            DataRow[] drRow = oTable.Select(string.Format("Tag LIKE '{0}'", sGetTag()));

            if (drRow.Length > 0)
                MessageBox.Show("Tag has already exists in Version. Please fill other value.");

            return (drRow.Length == 0);
        }

        /// <summary>
        /// Determine whether entered value is in numeric format or not
        /// </summary>
        /// <returns>boolean : true if numeric</returns>
        protected bool isNumeric()
        {
            string sErrMsg = "";
            if (!CommonClass.isFormatValid(sGetItemSequence(), "N"))
                sErrMsg = string.Format("{0} {1}", "Item Sequence", " can only received numeric. Please fill other value.");
            else if (!CommonClass.isFormatValid(sGetMinLength(), "N"))
                sErrMsg = string.Format("{0} {1}", "Minimum Length", " can only received numeric. Please fill other value.");
            else if (!CommonClass.isFormatValid(sGetMaxLength(), "N"))
                sErrMsg = string.Format("{0} {1}", "Maximum Length", " can only received numeric. Please fill other value.");
            else if (!CommonClass.isFormatValid(sGetMinValue(), "N"))
                sErrMsg = string.Format("{0} {1}", "Minimum Value", " can only received numeric. Please fill other value.");
            else if (!CommonClass.isFormatValid(sGetMaxValue(), "N"))
                sErrMsg = string.Format("{0} {1}", "Maximum Value", " can only received numeric. Please fill other value.");

            return isShowErrorMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether data saved successfully into database or not
        /// </summary>
        /// <returns>boolean : true is succeesful</returns>
        protected bool isSuccessSave()
        {
            string sErrMsg = "";
            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy HH:mm:ss"));
            string sLogDetail = "";
            try
            {
                if (string.IsNullOrEmpty((sErrMsg = sSaveItem(oSqlTrans, ref sLogDetail))))
                    if (string.IsNullOrEmpty((sErrMsg = sSaveCmbValue(oSqlTrans))))
                    {
                        oSqlTrans.Commit();
                        string sDbName = ((DataRowView)cmbDbId.SelectedItem)["DatabaseName"].ToString();
                        CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, (sStatus == "edit") ?
                                                "Modify Existing Tag : " : "Add New Tag : " + sGetTag(), sLogDetail);
                    }
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
            }

            if (!string.IsNullOrEmpty(sErrMsg))
            {
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }
            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Save Item value into database
        /// </summary>
        /// <param name="oSqlTrans">SqlTransaction : Transaction session used in current transaction</param>
        /// <returns>string : Error Message if there are any error in current transaction</returns>
        public string sSaveItem(SqlTransaction oSqlTrans, ref string sLogDetail)
        {
            string sErrMsg = "";
            using (SqlCommand oSqlCmd = new SqlCommand(sGetSP(), oSqlConn))
            {
                try
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sGetDbId();
                    oSqlCmd.Parameters.Add("@sFormID", SqlDbType.VarChar).Value = sGetFormValue();
                    oSqlCmd.Parameters.Add("@iItemSequence", SqlDbType.VarChar).Value = sGetItemSequence();
                    oSqlCmd.Parameters.Add("@sOldItemSequence", SqlDbType.VarChar).Value = sGetCurrValue("ItemSequence");
                    oSqlCmd.Parameters.Add("@sTag", SqlDbType.VarChar).Value = sGetTag();
                    oSqlCmd.Parameters.Add("@sTagName", SqlDbType.VarChar).Value = sGetTagName();
                    oSqlCmd.Parameters.Add("@sObjectID", SqlDbType.VarChar).Value = sGetObjectID(sGetRadioButtonCheckedValue(pnlObject));
                    oSqlCmd.Parameters.Add("@sOldObjectID", SqlDbType.VarChar).Value = sGetObjectID(sGetCurrValue("ObjectName"));
                    oSqlCmd.Parameters.Add("@sDefaultValue", SqlDbType.VarChar).Value = sGetDefaultValue();
                    oSqlCmd.Parameters.Add("@iLengthofTagValueLength", SqlDbType.VarChar).Value = "2";
                    oSqlCmd.Parameters.Add("@vAllowNull", SqlDbType.VarChar).Value = sGetBitValue(sGetRadioButtonCheckedValue(pnlAllowNull));
                    oSqlCmd.Parameters.Add("@vUpperCase", SqlDbType.VarChar).Value = sGetBitValue(sGetRadioButtonCheckedValue(pnlUpperCase));
                    oSqlCmd.Parameters.Add("@vType", SqlDbType.VarChar).Value = sGetRadioButtonCheckedValue(pnlValueType);
                    oSqlCmd.Parameters.Add("@vMinLength", SqlDbType.VarChar).Value = sGetMinLength();
                    oSqlCmd.Parameters.Add("@vMaxLength", SqlDbType.VarChar).Value = sGetMaxLength();
                    oSqlCmd.Parameters.Add("@vMinValue", SqlDbType.VarChar).Value = sGetMinValue();
                    oSqlCmd.Parameters.Add("@vMaxValue", SqlDbType.VarChar).Value = sGetMaxValue();
                    oSqlCmd.Parameters.Add("@vMasking", SqlDbType.VarChar).Value = sGetBitValue(sGetRadioButtonCheckedValue(pnlMasking));
                    oSqlCmd.Parameters.Add("@sValidationMsg", SqlDbType.VarChar).Value = sGetValidationMsg();
                    oSqlCmd.Transaction = oSqlTrans;

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    //oSqlCmd.ExecuteNonQuery();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        while (oRead.Read())
                        {
                            if (sStatus == "edit")
                                sLogDetail += string.Format("{0} : {1} --> {2} \n ", oRead["ColumnName"].ToString(),
                                                oRead["OldColumnValue"].ToString(), oRead["NewColumnValue"].ToString());
                            else
                                sLogDetail += string.Format("{0} : {1} \n ", oRead["ColumnName"].ToString(), oRead["ColumnValue"].ToString());
                        }
                        oRead.Close();
                    }
                }
                catch (Exception ex)
                {
                    sErrMsg = ex.Message;
                }
            }
            return sErrMsg;
        }

        /// <summary>
        /// Convert string value from text's bit value into numeric bit value
        /// </summary>
        /// <param name="sValue">string : Text bit value</param>
        /// <returns>string : Numeci bit value</returns>
        protected string sGetBitValue(string sValue)
        {
            if (sValue.ToLower() == "true") return "1";
            return "0";
        }

        /// <summary>
        /// Save values in ComboBoxValue
        /// </summary>
        /// <param name="oSqlTrans">SqlTransaction : session used in current transaction</param>
        /// <returns>string : return error message if there is any error</returns>
        public string sSaveCmbValue(SqlTransaction oSqlTrans)
        {
            string sErrMsg = "";
            try
            {
                SqlCommand oInsert = oSqlInsert();
                SqlCommand oDelete = oSqlDelete();

                oInsert.Transaction = oSqlTrans;
                oDelete.Transaction = oSqlTrans;

                using (SqlDataAdapter oAdapt = new SqlDataAdapter())
                {
                    oAdapt.InsertCommand = oInsert;
                    oAdapt.DeleteCommand = oDelete;

                    DataTable oTable = (DataTable)dgCmbValue.DataSource;
                    if (oTable != null)
                        oAdapt.Update(oTable);
                }
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
            }

            return sErrMsg;
        }

        /// <summary>
        /// Determine which stored procedure will be used in current operation
        /// </summary>
        /// <returns>string : Stored Procedure's name that will be used</returns>
        protected string sGetSP()
        {
            if (sStatus == "edit") return CommonSP.sSPItemUpdate;
            return CommonSP.sSPItemInsert;
        }

        /// <summary>
        /// SqlCommand use to insert new data into database
        /// </summary>
        /// <returns>SqlCommand : SQL Insert</returns>
        protected SqlCommand oSqlInsert()
        {
            SqlCommand oSqlInsert = new SqlCommand(CommonSP.sSPItemCmbValueInsert, oSqlConn);
            oSqlInsert.CommandType = CommandType.StoredProcedure;
            oSqlInsert.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sGetDbId();
            oSqlInsert.Parameters.Add("@sFormID", SqlDbType.VarChar).Value = sGetFormValue();
            oSqlInsert.Parameters.Add("@iItemSequence", SqlDbType.VarChar).Value = sGetItemSequence();
            oSqlInsert.Parameters.Add("@sDisplayValue", SqlDbType.VarChar).SourceColumn = "DisplayValue";
            oSqlInsert.Parameters.Add("@sRealValue ", SqlDbType.VarChar).SourceColumn = "RealValue";
            return oSqlInsert;
        }

        /// <summary>
        /// SqlCommand use to delete data from database
        /// </summary>
        /// <returns>SqlCommand : SQL Delete</returns>
        protected SqlCommand oSqlDelete()
        {
            SqlCommand oSqlDelete = new SqlCommand(CommonSP.sSPItemCmbValueDelete, oSqlConn);
            oSqlDelete.CommandType = CommandType.StoredProcedure;
            oSqlDelete.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sGetDbId();
            oSqlDelete.Parameters.Add("@sFormID", SqlDbType.VarChar).Value = sGetFormValue();
            oSqlDelete.Parameters.Add("@iItemSequence", SqlDbType.VarChar).Value = sGetItemSequence();
            oSqlDelete.Parameters.Add("@sDisplayValue", SqlDbType.VarChar).SourceColumn = "DisplayValue";
            oSqlDelete.Parameters.Add("@sRealValue ", SqlDbType.VarChar).SourceColumn = "RealValue";

            return oSqlDelete;

        }

        /// <summary>
        /// Determine whether Error Message will be showed or not
        /// </summary>
        /// <param name="sErrMsg"></param>
        /// <returns></returns>
        protected bool isShowErrorMessage(string sErrMsg)
        {
            if (!string.IsNullOrEmpty(sErrMsg))
                MessageBox.Show(sErrMsg);
            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Get Tag value
        /// </summary>
        /// <returns>string : Tag ID</returns>
        protected string sGetTag()
        {
            return txtTagID.Text.Trim();
        }

        /// <summary>
        /// Get Tag Name value
        /// </summary>
        /// <returns>string : Tag Name</returns>
        protected string sGetTagName()
        {
            return txtTagName.Text.Trim();
        }

        protected string sGetFormValue()
        {
            return cmbForm.SelectedValue.ToString();
        }

        /// <summary>
        /// Get Default Value
        /// </summary>
        /// <returns>string : Default value</returns>
        protected string sGetDefaultValue()
        {
            return txtDefaultValue.Text.Trim();
        }

        /// <summary>
        /// Get Item Sequence value
        /// </summary>
        /// <returns>string : item sequence</returns>
        protected string sGetItemSequence()
        {
            return txtItemSequence.Text.Trim();
        }

        /// <summary>
        /// Get whether radiobutton in given panel checked or not
        /// </summary>
        /// <param name="oPanel">Panel : Object which hold radiobuttons that will be examined</param>
        /// <returns>string : name of checked radio button</returns>
        protected string sGetRadioButtonCheckedValue(Panel oPanel)
        {
            Control.ControlCollection oCtrlColl = oPanel.Controls;
            foreach (Control oCtrl in oCtrlColl)
                if (((RadioButton)oCtrl).Checked) return oCtrl.Tag.ToString();
            return "";
        }

        /// <summary>
        /// Get ObjectID value
        /// </summary>
        /// <param name="sObjName">string : Object's name</param>
        /// <returns>string : Object Id's value</returns>
        protected string sGetObjectID(string sObjName)
        {
            switch (sObjName.ToLower())
            {
                case "textbox": return "1";
                case "radiobutton": return "2";
                case "checkbox": return "3";
                case "combobox": return "4";
                case "datetimepicker": return "5";
                default : return "0";
            }
        }

        /// <summary>
        /// Get Minimal length value
        /// </summary>
        /// <returns>string : minimal length</returns>
        protected string sGetMinLength()
        {
            return txtMinLength.Text.Trim();
        }

        /// <summary>
        /// Get Maximum length value
        /// </summary>
        /// <returns>string : maximum length</returns>
        protected string sGetMaxLength()
        {
            return txtMaxLength.Text.Trim();
        }

        /// <summary>
        /// Get Minimal Value
        /// </summary>
        /// <returns>string : Minimal Value</returns>
        protected string sGetMinValue()
        {
            return txtMinValue.Text.Trim();
        }

        /// <summary>
        /// Get Maximum Value
        /// </summary>
        /// <returns>string : maximum value</returns>
        protected string sGetMaxValue()
        {
            return txtMaxValue.Text.Trim();
        }

        /// <summary>
        /// Get Validation Message Value
        /// </summary>
        /// <returns>string : Validation Message</returns>
        protected string sGetValidationMsg()
        {
            return txtValidationMsg.Text.Trim();
        }

        /// <summary>
        /// Determine whether selected tag(s) deleted successfully from database
        /// </summary>
        /// <returns>boolean : true if successfyl</returns>
        protected bool isDeleteData()
        {
            string sErrMsg = "";
            string sLogDetail = "";
            string sDatabaseName = ((DataRowView)cmbDbId.SelectedItem)["DatabaseName"].ToString();
            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                    if (IsDeleteAll())
                        if (cmbFormType.SelectedValue.ToString() == "0")
                            DeleteAll(oSqlTrans, ref sLogDetail);
                        else
                            DeleteItems(oSqlTrans, ref sLogDetail);
                    else
                        DeleteItems(oSqlTrans, ref sLogDetail);                            
                oSqlTrans.Commit();
                CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, sDatabaseName, "Delete Tag", sLogDetail);
            }
            catch (Exception ex)
            {
                oSqlTrans.Rollback();
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
            }
            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Delete Tag Function
        /// </summary>
        private void DeleteItems(SqlTransaction oSqlTrans, ref string sLogDetail)
        {
            foreach (DataGridViewRow dgRow in dgViewTagItem.Rows)
                if (dgRow.Cells["ColDelete"].Value != null)
                    if (dgRow.Cells["ColDelete"].Value.ToString().ToLower() == "true")
                        DeletePerItem(oSqlTrans, dgRow.Cells["ObjectName"].Value.ToString(), dgRow.Cells["ItemSequence"].Value.ToString(),
                                     dgRow.Cells["FormID"].Value.ToString(), dgRow.Cells["Tag"].Value.ToString(),
                                     dgRow.Cells["ItemName"].Value.ToString(), ref sLogDetail);
        }

        /// <summary>
        /// Check All Tag for delete
        /// </summary>
        /// <returns></returns>
        private bool IsDeleteAll()
        {
            bool bValue = true;
            if (chkCheckAll.Checked)
            {
                foreach (DataGridViewRow dgRow in dgViewTagItem.Rows)
                    if (dgRow.Cells["ColDelete"].Value != null)
                        if (bool.Parse(dgRow.Cells["ColDelete"].Value.ToString()) == false)
                        {
                            bValue = false;
                            break;
                        }
            }
            return bValue;
        }

        /// <summary>
        /// Delete all tag in current selected DatabaseID
        /// </summary>
        /// <param name="oSqlTrans">SqlTransaction : session used in current transaction</param>
        protected void DeleteAll(SqlTransaction oSqlTrans, ref string sLogDetail)
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPItemDeleteAll, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sGetDbId();
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
                sLogDetail = "Delete All Tags";
            }
        }

        /// <summary>
        /// Delete selected tags in current version 
        /// </summary>
        /// <param name="oSqlTrans">SqlTransaction : session used in current transaction</param>
        /// <param name="sObjName">string : Object's name which value will be retrieved</param>
        /// <param name="sItemSequence">string : Item Squence of current tag</param>
        /// <param name="sFormID">String : Form ID </param>
        /// <param name="sTag">string : Tag ID</param>
        protected void DeletePerItem(SqlTransaction oSqlTrans, string sObjName, string sItemSequence, 
                                     string sFormID, string sTag, string sItemName, ref string sLogDetail)
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPItemDelete, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sGetDbId();
                oSqlCmd.Parameters.Add("@sFormID", SqlDbType.VarChar).Value = sFormID;
                oSqlCmd.Parameters.Add("@sObjectID", SqlDbType.VarChar).Value = sGetObjectID(sObjName);
                oSqlCmd.Parameters.Add("@iItemSequence", SqlDbType.VarChar).Value = sItemSequence;
                oSqlCmd.Parameters.Add("@sTag", SqlDbType.VarChar).Value = sTag;
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
                sLogDetail += string.Format("{0} : {1} \n ", sTag, sItemName);
            }
        }

        /// <summary>
        /// Determine whether ItemSequence valid or not
        /// </summary>
        /// <param name="sDatabaseID">string : DatabaseID</param>
        /// <returns>boolean: true if valid</returns>
        protected bool isItemSequenceValid(string sDatabaseID)
        {
            string sErrMsg = "";
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPItemBrowseItemSeq, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sDatabaseID;
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = string.Format("There is duplicate Item Sequence at Form {0}, Item Sequence {1}",
                                        oRead["Form"].ToString(), oRead["ItemSequence"].ToString());
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
            }
            return isShowErrorMessage(sErrMsg);
        }
        #endregion        
    }
}