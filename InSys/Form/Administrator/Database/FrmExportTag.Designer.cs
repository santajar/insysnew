﻿namespace InSys
{
    partial class FrmExportTag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExportTag));
            this.gbDatabase = new System.Windows.Forms.GroupBox();
            this.chkRelation = new System.Windows.Forms.CheckBox();
            this.chkGPRS = new System.Windows.Forms.CheckBox();
            this.chkLoyProd = new System.Windows.Forms.CheckBox();
            this.chkLoyPool = new System.Windows.Forms.CheckBox();
            this.chkIssuer = new System.Windows.Forms.CheckBox();
            this.chkTLE = new System.Windows.Forms.CheckBox();
            this.chkCard = new System.Windows.Forms.CheckBox();
            this.chkAcquirer = new System.Windows.Forms.CheckBox();
            this.chkTerminal = new System.Windows.Forms.CheckBox();
            this.chkCheckAll = new System.Windows.Forms.CheckBox();
            this.cmbDbId = new System.Windows.Forms.ComboBox();
            this.lblDbID = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.dgViewTagItem = new System.Windows.Forms.DataGridView();
            this.colSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gbTag = new System.Windows.Forms.GroupBox();
            this.gbDatabase.SuspendLayout();
            this.gbButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewTagItem)).BeginInit();
            this.gbTag.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDatabase
            // 
            this.gbDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDatabase.Controls.Add(this.chkRelation);
            this.gbDatabase.Controls.Add(this.chkGPRS);
            this.gbDatabase.Controls.Add(this.chkLoyProd);
            this.gbDatabase.Controls.Add(this.chkLoyPool);
            this.gbDatabase.Controls.Add(this.chkIssuer);
            this.gbDatabase.Controls.Add(this.chkTLE);
            this.gbDatabase.Controls.Add(this.chkCard);
            this.gbDatabase.Controls.Add(this.chkAcquirer);
            this.gbDatabase.Controls.Add(this.chkTerminal);
            this.gbDatabase.Controls.Add(this.chkCheckAll);
            this.gbDatabase.Controls.Add(this.cmbDbId);
            this.gbDatabase.Controls.Add(this.lblDbID);
            this.gbDatabase.Location = new System.Drawing.Point(11, 1);
            this.gbDatabase.Name = "gbDatabase";
            this.gbDatabase.Size = new System.Drawing.Size(1090, 64);
            this.gbDatabase.TabIndex = 4;
            this.gbDatabase.TabStop = false;
            // 
            // chkRelation
            // 
            this.chkRelation.AutoSize = true;
            this.chkRelation.Checked = true;
            this.chkRelation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRelation.Location = new System.Drawing.Point(572, 13);
            this.chkRelation.Name = "chkRelation";
            this.chkRelation.Size = new System.Drawing.Size(65, 17);
            this.chkRelation.TabIndex = 11;
            this.chkRelation.Text = "Relation";
            this.chkRelation.UseVisualStyleBackColor = true;
            this.chkRelation.Visible = false;
            // 
            // chkGPRS
            // 
            this.chkGPRS.AutoSize = true;
            this.chkGPRS.Checked = true;
            this.chkGPRS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGPRS.Location = new System.Drawing.Point(509, 13);
            this.chkGPRS.Name = "chkGPRS";
            this.chkGPRS.Size = new System.Drawing.Size(56, 17);
            this.chkGPRS.TabIndex = 10;
            this.chkGPRS.Text = "GPRS";
            this.chkGPRS.UseVisualStyleBackColor = true;
            this.chkGPRS.CheckedChanged += new System.EventHandler(this.chkControl_Changed);
            // 
            // chkLoyProd
            // 
            this.chkLoyProd.AutoSize = true;
            this.chkLoyProd.Checked = true;
            this.chkLoyProd.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoyProd.Location = new System.Drawing.Point(404, 13);
            this.chkLoyProd.Name = "chkLoyProd";
            this.chkLoyProd.Size = new System.Drawing.Size(99, 17);
            this.chkLoyProd.TabIndex = 9;
            this.chkLoyProd.Text = "Loyalty Product";
            this.chkLoyProd.UseVisualStyleBackColor = true;
            this.chkLoyProd.CheckedChanged += new System.EventHandler(this.chkControl_Changed);
            // 
            // chkLoyPool
            // 
            this.chkLoyPool.AutoSize = true;
            this.chkLoyPool.Checked = true;
            this.chkLoyPool.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLoyPool.Location = new System.Drawing.Point(315, 13);
            this.chkLoyPool.Name = "chkLoyPool";
            this.chkLoyPool.Size = new System.Drawing.Size(83, 17);
            this.chkLoyPool.TabIndex = 8;
            this.chkLoyPool.Text = "Loyalty Pool";
            this.chkLoyPool.UseVisualStyleBackColor = true;
            this.chkLoyPool.CheckedChanged += new System.EventHandler(this.chkControl_Changed);
            // 
            // chkIssuer
            // 
            this.chkIssuer.AutoSize = true;
            this.chkIssuer.Checked = true;
            this.chkIssuer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIssuer.Location = new System.Drawing.Point(149, 13);
            this.chkIssuer.Name = "chkIssuer";
            this.chkIssuer.Size = new System.Drawing.Size(54, 17);
            this.chkIssuer.TabIndex = 2;
            this.chkIssuer.Text = "Issuer";
            this.chkIssuer.UseVisualStyleBackColor = true;
            this.chkIssuer.CheckedChanged += new System.EventHandler(this.chkControl_Changed);
            // 
            // chkTLE
            // 
            this.chkTLE.AutoSize = true;
            this.chkTLE.Checked = true;
            this.chkTLE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTLE.Location = new System.Drawing.Point(263, 13);
            this.chkTLE.Name = "chkTLE";
            this.chkTLE.Size = new System.Drawing.Size(46, 17);
            this.chkTLE.TabIndex = 4;
            this.chkTLE.Text = "TLE";
            this.chkTLE.UseVisualStyleBackColor = true;
            this.chkTLE.CheckedChanged += new System.EventHandler(this.chkControl_Changed);
            // 
            // chkCard
            // 
            this.chkCard.AutoSize = true;
            this.chkCard.Checked = true;
            this.chkCard.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCard.Location = new System.Drawing.Point(209, 13);
            this.chkCard.Name = "chkCard";
            this.chkCard.Size = new System.Drawing.Size(48, 17);
            this.chkCard.TabIndex = 3;
            this.chkCard.Text = "Card";
            this.chkCard.UseVisualStyleBackColor = true;
            this.chkCard.CheckedChanged += new System.EventHandler(this.chkControl_Changed);
            // 
            // chkAcquirer
            // 
            this.chkAcquirer.AutoSize = true;
            this.chkAcquirer.Checked = true;
            this.chkAcquirer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAcquirer.Location = new System.Drawing.Point(78, 13);
            this.chkAcquirer.Name = "chkAcquirer";
            this.chkAcquirer.Size = new System.Drawing.Size(65, 17);
            this.chkAcquirer.TabIndex = 1;
            this.chkAcquirer.Text = "Acquirer";
            this.chkAcquirer.UseVisualStyleBackColor = true;
            this.chkAcquirer.CheckedChanged += new System.EventHandler(this.chkControl_Changed);
            // 
            // chkTerminal
            // 
            this.chkTerminal.AutoSize = true;
            this.chkTerminal.Checked = true;
            this.chkTerminal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTerminal.Location = new System.Drawing.Point(6, 13);
            this.chkTerminal.Name = "chkTerminal";
            this.chkTerminal.Size = new System.Drawing.Size(66, 17);
            this.chkTerminal.TabIndex = 0;
            this.chkTerminal.Text = "Terminal";
            this.chkTerminal.UseVisualStyleBackColor = true;
            this.chkTerminal.CheckedChanged += new System.EventHandler(this.chkControl_Changed);
            // 
            // chkCheckAll
            // 
            this.chkCheckAll.AutoSize = true;
            this.chkCheckAll.Location = new System.Drawing.Point(309, 38);
            this.chkCheckAll.Name = "chkCheckAll";
            this.chkCheckAll.Size = new System.Drawing.Size(71, 17);
            this.chkCheckAll.TabIndex = 7;
            this.chkCheckAll.Text = "Check All";
            this.chkCheckAll.UseVisualStyleBackColor = true;
            this.chkCheckAll.CheckedChanged += new System.EventHandler(this.chkCheckAll_CheckedChanged);
            // 
            // cmbDbId
            // 
            this.cmbDbId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDbId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDbId.FormattingEnabled = true;
            this.cmbDbId.Location = new System.Drawing.Point(93, 36);
            this.cmbDbId.Name = "cmbDbId";
            this.cmbDbId.Size = new System.Drawing.Size(210, 21);
            this.cmbDbId.TabIndex = 6;
            this.cmbDbId.SelectedIndexChanged += new System.EventHandler(this.cmbDbId_SelectedIndexChanged);
            // 
            // lblDbID
            // 
            this.lblDbID.AutoSize = true;
            this.lblDbID.Location = new System.Drawing.Point(3, 39);
            this.lblDbID.Name = "lblDbID";
            this.lblDbID.Size = new System.Drawing.Size(84, 13);
            this.lblDbID.TabIndex = 5;
            this.lblDbID.Text = "Database Name";
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnExport);
            this.gbButton.Location = new System.Drawing.Point(12, 638);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(1090, 49);
            this.gbButton.TabIndex = 7;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(968, 14);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(116, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnExport.Location = new System.Drawing.Point(837, 14);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(126, 28);
            this.btnExport.TabIndex = 0;
            this.btnExport.Text = "Export";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // dgViewTagItem
            // 
            this.dgViewTagItem.AllowUserToAddRows = false;
            this.dgViewTagItem.AllowUserToDeleteRows = false;
            this.dgViewTagItem.AllowUserToResizeRows = false;
            this.dgViewTagItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgViewTagItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgViewTagItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgViewTagItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSelect});
            this.dgViewTagItem.Location = new System.Drawing.Point(6, 11);
            this.dgViewTagItem.Name = "dgViewTagItem";
            this.dgViewTagItem.ReadOnly = true;
            this.dgViewTagItem.RowHeadersVisible = false;
            this.dgViewTagItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgViewTagItem.Size = new System.Drawing.Size(1077, 550);
            this.dgViewTagItem.TabIndex = 0;
            this.dgViewTagItem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgViewTagItem_CellContentClick);
            // 
            // colSelect
            // 
            this.colSelect.HeaderText = "Select";
            this.colSelect.Name = "colSelect";
            this.colSelect.ReadOnly = true;
            this.colSelect.Width = 60;
            // 
            // gbTag
            // 
            this.gbTag.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTag.Controls.Add(this.dgViewTagItem);
            this.gbTag.Location = new System.Drawing.Point(11, 65);
            this.gbTag.Name = "gbTag";
            this.gbTag.Size = new System.Drawing.Size(1090, 567);
            this.gbTag.TabIndex = 5;
            this.gbTag.TabStop = false;
            // 
            // FrmExportTag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1112, 697);
            this.Controls.Add(this.gbDatabase);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbTag);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmExportTag";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Export Tag";
            this.Load += new System.EventHandler(this.FrmExportTag_Load);
            this.gbDatabase.ResumeLayout(false);
            this.gbDatabase.PerformLayout();
            this.gbButton.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgViewTagItem)).EndInit();
            this.gbTag.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDatabase;
        private System.Windows.Forms.CheckBox chkGPRS;
        private System.Windows.Forms.CheckBox chkLoyProd;
        private System.Windows.Forms.CheckBox chkLoyPool;
        private System.Windows.Forms.CheckBox chkIssuer;
        private System.Windows.Forms.CheckBox chkTLE;
        private System.Windows.Forms.CheckBox chkCard;
        private System.Windows.Forms.CheckBox chkAcquirer;
        private System.Windows.Forms.CheckBox chkTerminal;
        private System.Windows.Forms.CheckBox chkCheckAll;
        private System.Windows.Forms.ComboBox cmbDbId;
        private System.Windows.Forms.Label lblDbID;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.DataGridView dgViewTagItem;
        private System.Windows.Forms.GroupBox gbTag;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colSelect;
        private System.Windows.Forms.CheckBox chkRelation;
    }
}