﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using InSysClass;

namespace InSys
{
    public partial class FrmExportTag : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected string sCurrDbID = "";

        enum ObjectType
        {
            TextBox = 1,
            RadioButton,
            CheckBox,
            ComboBox
        };

        /// <summary>
        /// initialize Form export Tag
        /// </summary>
        public FrmExportTag(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        /// <summary>
        /// Load Export Tag
        /// </summary>
        private void FrmExportTag_Load(object sender, EventArgs e)
        {
            InitCmb();
            InitData();
        }

        /// <summary>
        /// Run Export Function
        /// </summary>
        private void btnExport_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbdOutput = new FolderBrowserDialog();
            fbdOutput.RootFolder = Environment.SpecialFolder.MyComputer;
            fbdOutput.SelectedPath = Environment.CurrentDirectory + @"\FILE";
            if (fbdOutput.ShowDialog() == DialogResult.OK)
            {
                string sPath = fbdOutput.SelectedPath;
                string sDatabaseName = cmbDbId.Text;

                string sFilenameItemList = string.Format(@"{0}\{1}_ItemList.csv", sPath, sDatabaseName);
                string sFilecontentItemList = sGenerateItemListCSV();
                if (File.Exists(sFilenameItemList)) File.Delete(sFilenameItemList);
                using (StreamWriter sw = new StreamWriter(sFilenameItemList))
                    sw.Write(sFilecontentItemList);
                
                string sFilenameItemComboBox = string.Format(@"{0}\{1}_ItemComboBox.csv", sPath, sDatabaseName);
                string sFilecontentItemComboBox = sGenerateItemComboBoxListCSV();
                if (File.Exists(sFilenameItemComboBox)) File.Delete(sFilenameItemComboBox);
                using (StreamWriter sw = new StreamWriter(sFilenameItemComboBox))
                    sw.Write(sFilecontentItemComboBox);
            }
        }
        
        private void cmbDbId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbId.DisplayMember) && !string.IsNullOrEmpty(cmbDbId.ValueMember))
                if (cmbDbId.Items.Count > 0)
                    if (sCurrDbID != sGetDbId())
                        if (isItemSequenceValid(sCurrDbID))
                        {
                            sCurrDbID = sGetDbId();
                            InitData();
                        }
                        else
                            cmbDbId.SelectedValue = string.IsNullOrEmpty(sCurrDbID) ? null : sCurrDbID;
        }

        private void chkControl_Changed(object sender, EventArgs e)
        {
            InitData();
        }

        private void chkCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            if (dgViewTagItem.ColumnCount > 0)
                foreach (DataGridViewRow dgvRow in dgViewTagItem.Rows)
                    dgvRow.Cells["ColSelect"].Value = chkCheckAll.Checked;
        }

        private void dgViewTagItem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgViewTagItem.Focus();
            if (e.RowIndex >= 0)
                if (e.ColumnIndex == dgViewTagItem.Columns["ColSelect"].Index)
                    dgViewTagItem["ColSelect", e.RowIndex].Value = !Convert.ToBoolean(dgViewTagItem["ColSelect", e.RowIndex].Value);
        }
        
        #region "Function"
        /// <summary>
        /// Initiate ComboBox to fill data from database
        /// </summary>
        protected void InitCmb()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbId);
        }

        /// <summary>
        /// Load data from database then show the on DataGridView
        /// </summary>
        protected void InitData()
        {
            if (cmbDbId.SelectedIndex >= 0)
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sGetTagCondition();

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        DataTable oTable = new DataTable();
                        oAdapt.Fill(oTable);
                        InitDataGrid(oTable);
                    }
                }
        }

        /// <summary>
        /// Initiate DataGridView, set its display and format
        /// </summary>
        /// <param name="oTable">DataTable : Data Source to be used</param>
        protected void InitDataGrid(DataTable oTable)
        {
            dgViewTagItem.DataSource = null;
            chkCheckAll.Checked = false;
            dgViewTagItem.DataSource = oTable;

            dgViewTagItem.Columns["ItemID"].Visible = false;
            dgViewTagItem.Columns["DatabaseID"].Visible = false;

            dgViewTagItem.Columns["Tag"].DisplayIndex = 1;
            dgViewTagItem.Columns["ItemName"].DisplayIndex = 2;

            dgViewTagItem.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgViewTagItem.Columns["DefaultValue"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        }

        /// <summary>
        /// Determine whether ItemSequence valid or not
        /// </summary>
        /// <param name="sDatabaseID">string : DatabaseID</param>
        /// <returns>boolean: true if valid</returns>
        protected bool isItemSequenceValid(string sDatabaseID)
        {
            string sErrMsg = "";
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPItemBrowseItemSeq, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sDatabaseID;
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = string.Format("There is duplicate Item Sequence at Form {0}, Item Sequence {1}",
                                        oRead["Form"].ToString(), oRead["ItemSequence"].ToString());
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
            }
            return isShowErrorMessage(sErrMsg);
        }

        /// <summary>
        /// Generate condition string to filter data from database
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sGetTagCondition()
        {
            string sConditionString = string.Format("WHERE DatabaseID = {0}", sGetDbId());
            string sConditionDetail = "";
            if (chkTerminal.Checked)
                sConditionDetail = sGetConditionDetail(sConditionDetail, "D");
            if (chkAcquirer.Checked)
                sConditionDetail = sGetConditionDetail(sConditionDetail, "AA");
            if (chkIssuer.Checked)
                sConditionDetail = sGetConditionDetail(sConditionDetail, "AE");
            if (chkCard.Checked)
                sConditionDetail = sGetConditionDetail(sConditionDetail, "AC");
            if (chkRelation.Checked)
                sConditionDetail = sGetConditionDetail(sConditionDetail, "AD");
            if (chkTLE.Checked)
                sConditionDetail = sGetConditionDetail(sConditionDetail, "TL");
            if (chkLoyPool.Checked)
                sConditionDetail = sGetConditionDetail(sConditionDetail, "LA");
            if (chkLoyProd.Checked)
                sConditionDetail = sGetConditionDetail(sConditionDetail, "LB");
            if (chkGPRS.Checked)
                sConditionDetail = sGetConditionDetail(sConditionDetail, "GP");

            if (!string.IsNullOrEmpty(sConditionDetail))
                sConditionDetail = string.Format(" AND ( {0} )", sConditionDetail);
            return sConditionString + sConditionDetail;
        }

        /// <summary>
        /// Get Condition string to filter data based on given tag value
        /// </summary>
        /// <param name="sCondition">string : current Condition string</param>
        /// <param name="sTag">string : tag value which will be use to filter database</param>
        /// <returns>string : New Condition string</returns>
        protected string sGetConditionDetail(string sCondition, string sTag)
        {
            if (string.IsNullOrEmpty(sCondition))
                sCondition += string.Format(" Tag LIKE '{0}%'", sTag);
            else
                sCondition += string.Format(" OR Tag LIKE '{0}%'", sTag);
            return sCondition;
        }

        /// <summary>
        /// Get Selected DatabaseID value from combobox
        /// </summary>
        /// <returns>string : Database ID</returns>
        protected string sGetDbId()
        {
            return (cmbDbId.SelectedIndex >= 0) ? cmbDbId.SelectedValue.ToString() : "";
        }

        /// <summary>
        /// Determine whether Error Message will be showed or not
        /// </summary>
        /// <param name="sErrMsg"></param>
        /// <returns></returns>
        protected bool isShowErrorMessage(string sErrMsg)
        {
            if (!string.IsNullOrEmpty(sErrMsg))
                MessageBox.Show(sErrMsg);
            return string.IsNullOrEmpty(sErrMsg);
        }

        protected string sGenerateItemListCSV()
        {
            string sRows = null;
            string sColumn = null;

            foreach (DataGridViewColumn dgvColumn in dgViewTagItem.Columns)
            {
                if (dgvColumn.Index != 0)
                    if (dgvColumn.HeaderText.ToLower() != "itemid")
                    {
                        string sCol;
                        switch (dgvColumn.HeaderText.ToLower())
                        {
                            case "objectname":
                                sCol = "ObjectID";
                                break;
                            case "lengthoftaglength":
                                sCol = "LengthofTagValueLength";
                                break;
                            default:
                                sCol = dgvColumn.HeaderText;
                                break;
                        }
                        sColumn = string.IsNullOrEmpty(sColumn) ?
                            string.Format("{0}", sCol) :
                            string.Format("{0},{1}",sColumn,sCol);
                    }
            }

            foreach (DataGridViewRow dgvRow in dgViewTagItem.Rows)
            {
                if (Convert.ToBoolean(dgvRow.Cells[0].Value) == true)
                {
                    string sRowsTemp = null;
                    foreach (DataGridViewColumn dgvColumn in dgViewTagItem.Columns)
                    {
                        if (dgvColumn.Index != 0)
                            if (dgvColumn.HeaderText.ToLower() != "itemid")
                            {
                                string sValue = null;
                                if (dgvColumn.HeaderText.ToLower() == "objectname")
                                {
                                    string sType = dgvRow.Cells[dgvColumn.HeaderText].Value.ToString();
                                    switch (sType.ToLower())
                                    {
                                        case "textbox":
                                            sValue = ObjectType.TextBox.GetHashCode().ToString();
                                            break;
                                        case "checkbox":
                                            sValue = ObjectType.CheckBox.GetHashCode().ToString();
                                            break;
                                        case "combobox":
                                            sValue = ObjectType.ComboBox.GetHashCode().ToString();
                                            break;
                                        case "radiobutton":
                                            sValue = ObjectType.RadioButton.GetHashCode().ToString();
                                            break;
                                    }
                                }
                                else
                                    sValue = dgvRow.Cells[dgvColumn.HeaderText].Value.ToString();
                                sRowsTemp = string.IsNullOrEmpty(sRowsTemp) ?
                                    string.Format("{0}", sValue) :
                                    string.Format("{0},{1}",sRowsTemp,sValue);
                            }
                    }
                    sRows = string.IsNullOrEmpty(sRows) ? sRowsTemp : string.Format("{0}\n{1}", sRows, sRowsTemp);
                } 
            }

            return string.Format("{0}\n{1}", sColumn, sRows);
        }

        protected string sGenerateItemComboBoxListCSV()
        {
            string sRows = null;
            string sColumn = null;

            List<DataTable> ltDataTable = new List<DataTable>();

            foreach (DataGridViewRow dgvRow in dgViewTagItem.Rows)
            {
                if (Convert.ToBoolean(dgvRow.Cells[0].Value) == true)
                {
                    string sRowsTemp = null;
                    foreach (DataGridViewColumn dgvColumn in dgViewTagItem.Columns)
                    {
                        if (dgvColumn.Index != 0)
                            if (dgvColumn.HeaderText.ToLower() != "itemid")
                            {
                                if (dgvColumn.HeaderText.ToLower() == "objectname")
                                {
                                    string sType = dgvRow.Cells[dgvColumn.HeaderText].Value.ToString();
                                    switch (sType.ToLower())
                                    {
                                        case "combobox":
                                            DataTable dtItemComboBox = new DataTable();
                                            dtItemComboBox =
                                                dtComboBoxValue(dgvRow.Cells["FormId"].Value.ToString(), 
                                                dgvRow.Cells["ItemSequence"].Value.ToString());
                                            ltDataTable.Add(dtItemComboBox);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                    }
                }
            }

            if (ltDataTable != null)
            {
                DataTable dtTempColumn = new DataTable();
                dtTempColumn = ltDataTable[0];
                foreach (DataColumn col in dtTempColumn.Columns)
                {
                    if (col.ColumnName.ToLower() != "itemid")
                        sColumn = string.IsNullOrEmpty(sColumn) ?
                            string.Format("{0}",col.ColumnName) :
                            string.Format("{0},{1}",sColumn,col.ColumnName);
                }

                foreach (DataTable dtTemp in ltDataTable)
                {
                    foreach (DataRow row in dtTemp.Rows)
                    {
                        string sRowsTemp = null;
                        foreach (DataColumn col in dtTempColumn.Columns)
                        {
                            if (col.ColumnName.ToLower() != "itemid")
                                sRowsTemp = string.IsNullOrEmpty(sRowsTemp) ?
                                    string.Format("{0}", row[col.ColumnName].ToString()) :
                                    string.Format("{0},{1}",sRowsTemp,row[col.ColumnName].ToString());
                        }
                        sRows = string.IsNullOrEmpty(sRows) ? sRowsTemp : string.Format("{0}\n{1}", sRows, sRowsTemp);
                    }
                }
            }

            return string.Format("{0}\n{1}", sColumn, sRows);
        }
        
        /// <summary>
        /// Get Combo box Value by Itemlist
        /// </summary>
        /// <param name="sFormId">string Form ID value</param>
        /// <param name="sItemSequence">string Item Sequence Value</param>
        /// <returns>Data Table of Combo box value</returns>
        protected DataTable dtComboBoxValue(string sFormId, string sItemSequence)
        {
            DataTable dtTemp = new DataTable();
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCmbBrowse, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = 
                    string.Format("WHERE DatabaseID = {0} AND FormID = {1} AND ItemSequence = {2}",
                    sGetDbId(), sFormId, sItemSequence);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    oAdapt.Fill(dtTemp);
            }
            return dtTemp;
        }
        #endregion
    }
}
