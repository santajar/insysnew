﻿using InSysClass;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmDatabaseVersion_Edit : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected DataTable dtTerminalDB;

        /// <summary>
        /// Initialize Form Edit Database Version
        /// </summary>
        public FrmDatabaseVersion_Edit(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        /// <summary>
        /// Load Data
        /// </summary>
        private void FrmDatabaseVersion_Edit_Load(object sender, EventArgs e)
        {
            doInitiateData();
        }

        /// <summary>
        /// Load Data when database change
        /// </summary>
        private void cmbDatabaseVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDatabaseVersion.ValueMember) && !string.IsNullOrEmpty(cmbDatabaseVersion.DisplayMember))
            {
                if (!string.IsNullOrEmpty(cmbDatabaseVersion.Text))
                {
                    FillDataSelectedValue(int.Parse(cmbDatabaseVersion.SelectedValue.ToString()));
                }
            }
        }

        /// <summary>
        /// Run Edit Function
        /// </summary>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDatabaseVersion.Text))
                doSetDislay(true);
        }

        /// <summary>
        /// Run Save Function
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalDBEdit, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDbID", SqlDbType.VarChar).Value = txtDbID.Text;
                oCmd.Parameters.Add("@sDatabaseName", SqlDbType.VarChar).Value = txtDbName.Text;
                oCmd.Parameters.Add("@sNewInit", SqlDbType.VarChar).Value = rdNewInitYes.Checked ? "1" : "0";
                oCmd.Parameters.Add("@sEMVInit", SqlDbType.VarChar).Value = rdEMVInitYes.Checked ? "1" : "0";
                oCmd.Parameters.Add("@sTLEInit", SqlDbType.VarChar).Value = rdTLEInitYes.Checked ? "1" : "0";
                oCmd.Parameters.Add("@sLoyaltyProd", SqlDbType.VarChar).Value = rdLoyaltyProdYes.Checked ? "1" : "0";
                oCmd.Parameters.Add("@sGPRSInit", SqlDbType.VarChar).Value = rdGPRSInitYes.Checked ? "1" : "0";
                //oCmd.Parameters.Add("@sBankCode", SqlDbType.VarChar).Value = rdBankCodeYes.Checked ? "1" : "0";
                //oCmd.Parameters.Add("@sProductCode", SqlDbType.VarChar).Value = rdProductCodeYes.Checked ? "1" : "0";
                oCmd.ExecuteNonQuery();
            }
            doSetDislay(false);
        }

        /// <summary>
        /// Run Function Cancel
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            doSetDislay(false);
        }

        #region "Function"
        /// <summary>
        /// Initiate data
        /// </summary>
        protected void doInitiateData()
        {
            dtTerminalDB = new DataTable();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "";
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.HasRows)
                    {
                        dtTerminalDB.Load(oRead);
                        cmbDatabaseVersion.DataSource = dtTerminalDB;
                        cmbDatabaseVersion.DisplayMember = "DatabaseName";
                        cmbDatabaseVersion.ValueMember = "DatabaseID";
                        cmbDatabaseVersion.SelectedIndex = -1;
                    }
                }
            }
        }

        protected void FillDataSelectedValue(int iDatabaseID)
        {
            DataRow rowSelected = ((DataRow[])dtTerminalDB.Select(string.Format("DatabaseID={0}", iDatabaseID)))[0];
            txtDbID.Text = rowSelected["DatabaseID"].ToString();
            txtDbName.Text = rowSelected["DatabaseName"].ToString();
            if (bool.Parse(rowSelected["NewInit"].ToString())) rdNewInitYes.Checked = true;
            else rdNewInitNo.Checked = true;
            if (bool.Parse(rowSelected["EMVInit"].ToString())) rdEMVInitYes.Checked = true;
            else rdEMVInitNo.Checked = true;
            if (bool.Parse(rowSelected["TLEInit"].ToString())) rdTLEInitYes.Checked = true;
            else rdTLEInitNo.Checked = true;
            if (bool.Parse(rowSelected["LoyaltyProd"].ToString())) rdLoyaltyProdYes.Checked = true;
            else rdLoyaltyProdNo.Checked = true;
            if (bool.Parse(rowSelected["GPRSInit"].ToString())) rdGPRSInitYes.Checked = true;
            else rdGPRSInitNo.Checked = true;
            if (bool.Parse(rowSelected["PinPad"].ToString())) rdGPRSInitYes.Checked = true;
            else rdGPRSInitNo.Checked = true;

            //if (bool.Parse(rowSelected["BankCode"].ToString())) rdBankCodeYes.Checked = true;
            //else rdBankCodeNo.Checked = true;
            //if (bool.Parse(rowSelected["ProductCode"].ToString())) rdProductCodeYes.Checked = true;
            //else rdProductCodeNo.Checked = true;
        }

        /// <summary>
        /// Set Display
        /// </summary>
        /// <param name="isEnable"> Enable is True</param>
        protected void doSetDislay(bool isEnable)
        {
            gbChooseDatabase.Enabled = !isEnable;
            gbDatabase.Enabled = isEnable;
            gbButton.Enabled = isEnable;
        }
        #endregion
    }
}
