﻿namespace InSys
{
    partial class FrmImportTag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmImportTag));
            this.gbData = new System.Windows.Forms.GroupBox();
            this.btnBrowseItemcomboBox = new System.Windows.Forms.Button();
            this.btnBrowseItemList = new System.Windows.Forms.Button();
            this.txtItemComboBoxFile = new System.Windows.Forms.TextBox();
            this.txtItemListFile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDatabase = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.bwImport = new System.ComponentModel.BackgroundWorker();
            this.pbImportProcess = new System.Windows.Forms.ProgressBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rtbStatus = new System.Windows.Forms.RichTextBox();
            this.gbData.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbData
            // 
            this.gbData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbData.Controls.Add(this.btnBrowseItemcomboBox);
            this.gbData.Controls.Add(this.btnBrowseItemList);
            this.gbData.Controls.Add(this.txtItemComboBoxFile);
            this.gbData.Controls.Add(this.txtItemListFile);
            this.gbData.Controls.Add(this.label3);
            this.gbData.Controls.Add(this.label2);
            this.gbData.Controls.Add(this.cmbDatabase);
            this.gbData.Controls.Add(this.label1);
            this.gbData.Location = new System.Drawing.Point(12, 12);
            this.gbData.Name = "gbData";
            this.gbData.Size = new System.Drawing.Size(652, 111);
            this.gbData.TabIndex = 0;
            this.gbData.TabStop = false;
            // 
            // btnBrowseItemcomboBox
            // 
            this.btnBrowseItemcomboBox.Location = new System.Drawing.Point(323, 74);
            this.btnBrowseItemcomboBox.Name = "btnBrowseItemcomboBox";
            this.btnBrowseItemcomboBox.Size = new System.Drawing.Size(100, 23);
            this.btnBrowseItemcomboBox.TabIndex = 7;
            this.btnBrowseItemcomboBox.Tag = "itemcombobox";
            this.btnBrowseItemcomboBox.Text = "Browse";
            this.btnBrowseItemcomboBox.UseVisualStyleBackColor = true;
            this.btnBrowseItemcomboBox.Click += new System.EventHandler(this.BrowseFile);
            // 
            // btnBrowseItemList
            // 
            this.btnBrowseItemList.Location = new System.Drawing.Point(323, 44);
            this.btnBrowseItemList.Name = "btnBrowseItemList";
            this.btnBrowseItemList.Size = new System.Drawing.Size(100, 23);
            this.btnBrowseItemList.TabIndex = 6;
            this.btnBrowseItemList.Tag = "itemlist";
            this.btnBrowseItemList.Text = "Browse";
            this.btnBrowseItemList.UseVisualStyleBackColor = true;
            this.btnBrowseItemList.Click += new System.EventHandler(this.BrowseFile);
            // 
            // txtItemComboBoxFile
            // 
            this.txtItemComboBoxFile.Location = new System.Drawing.Point(124, 76);
            this.txtItemComboBoxFile.Name = "txtItemComboBoxFile";
            this.txtItemComboBoxFile.ReadOnly = true;
            this.txtItemComboBoxFile.Size = new System.Drawing.Size(183, 20);
            this.txtItemComboBoxFile.TabIndex = 5;
            // 
            // txtItemListFile
            // 
            this.txtItemListFile.Location = new System.Drawing.Point(124, 46);
            this.txtItemListFile.Name = "txtItemListFile";
            this.txtItemListFile.ReadOnly = true;
            this.txtItemListFile.Size = new System.Drawing.Size(183, 20);
            this.txtItemListFile.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "ItemComboBox File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "ItemList File";
            // 
            // cmbDatabase
            // 
            this.cmbDatabase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDatabase.FormattingEnabled = true;
            this.cmbDatabase.Location = new System.Drawing.Point(124, 13);
            this.cmbDatabase.Name = "cmbDatabase";
            this.cmbDatabase.Size = new System.Drawing.Size(160, 21);
            this.cmbDatabase.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Database";
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnImport);
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Location = new System.Drawing.Point(12, 126);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(652, 74);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnImport
            // 
            this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImport.Location = new System.Drawing.Point(444, 23);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(98, 29);
            this.btnImport.TabIndex = 1;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(548, 23);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(98, 29);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // bwImport
            // 
            this.bwImport.WorkerReportsProgress = true;
            this.bwImport.WorkerSupportsCancellation = true;
            this.bwImport.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwImport_DoWork);
            this.bwImport.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwImport_ProgressChanged);
            this.bwImport.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwImport_RunWorkerCompleted);
            // 
            // pbImportProcess
            // 
            this.pbImportProcess.Location = new System.Drawing.Point(12, 206);
            this.pbImportProcess.Name = "pbImportProcess";
            this.pbImportProcess.Size = new System.Drawing.Size(650, 23);
            this.pbImportProcess.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.rtbStatus);
            this.groupBox3.Location = new System.Drawing.Point(12, 235);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(652, 202);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // rtbStatus
            // 
            this.rtbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbStatus.Location = new System.Drawing.Point(3, 16);
            this.rtbStatus.Name = "rtbStatus";
            this.rtbStatus.ReadOnly = true;
            this.rtbStatus.Size = new System.Drawing.Size(646, 183);
            this.rtbStatus.TabIndex = 0;
            this.rtbStatus.Text = "";
            // 
            // FrmImportTag
            // 
            this.AcceptButton = this.btnImport;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(680, 449);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.pbImportProcess);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbData);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmImportTag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import Tag";
            this.Load += new System.EventHandler(this.FrmImportTag_Load);
            this.gbData.ResumeLayout(false);
            this.gbData.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbData;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBrowseItemcomboBox;
        private System.Windows.Forms.Button btnBrowseItemList;
        private System.Windows.Forms.TextBox txtItemComboBoxFile;
        private System.Windows.Forms.TextBox txtItemListFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbDatabase;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker bwImport;
        private System.Windows.Forms.ProgressBar pbImportProcess;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox rtbStatus;
    }
}