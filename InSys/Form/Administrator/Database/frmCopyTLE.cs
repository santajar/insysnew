﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmCopyTLE : Form
    {
        SqlConnection oSqlConn;

        public FrmCopyTLE(SqlConnection _oSqlConn)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
        }

        private void frmCopyTLE_Load(object sender, EventArgs e)
        {
            InitData();
            CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        /// <summary>
        /// Initiate Data from database to Form
        /// </summary>
        protected void InitData()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPDBListTLEBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPDBListTLEBrowse, "", "DatabaseID", "DatabaseName", ref CmbDbDest);
            FillChl();
        }

        /// <summary>
        /// Fill CheckedListBox with list of GPRS name from database
        /// </summary>
        protected void FillChl()
        {
            chlTLEList.DataSource = null;
            chlTLEList.Items.Clear();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTLECopyListBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sGetDbIdSource();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                        DataTable oDataTable = new DataTable();
                        oAdapt.Fill(oDataTable);

                        chlTLEList.DataSource = oDataTable;
                        chlTLEList.DisplayMember = "TLEName";
                        chlTLEList.ValueMember = "TLETagID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Get Database ID value that will be used as Source Version
        /// </summary>
        /// <returns>string : Database ID</returns>
        protected string sGetDbIdSource()
        {
            return (cmbDbSource.SelectedIndex >= 0) ? cmbDbSource.SelectedValue.ToString() : "";
        }

        /// <summary>
        /// Get Version ID value which will be used as Destination
        /// </summary>
        /// <returns>string : Version ID</returns>
        protected string sGetDbIdDest()
        {
            return (CmbDbDest.SelectedIndex >= 0) ? CmbDbDest.SelectedValue.ToString() : "";
        }

        private void cmbDbSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.Items.Count > 0)
                    FillChl();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                    if (isSaveData())
                        this.Close();
            }
        }

        /// <summary>
        /// Determine whether user entered data valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool isValid()
        {
            string sErrMsg = "";

            if (string.IsNullOrEmpty(sGetDbIdSource()))
                sErrMsg = string.Format("{0}{1}", "Database Source", CommonMessage.sNotChecked);
            else if (string.IsNullOrEmpty(sGetDbIdDest()))
                sErrMsg = string.Format("{0}{1}", "Database Destination", CommonMessage.sNotChecked);
            else if (sGetDbIdDest() == sGetDbIdSource())
                sErrMsg = CommonMessage.sDbSourceEqDbDest;

            if (!string.IsNullOrEmpty(sErrMsg))
                MessageBox.Show(sErrMsg);

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Determine whether data saved successfully to database or not
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        protected bool isSaveData()
        {
            if (chlTLEList.CheckedItems.Count > 0)
                return isSaveSelectedTLE();
            return false;
        }

        /// <summary>
        /// Determine whether selected TLE saved successfully from source version into destination version 
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        protected bool isSaveSelectedTLE()
        {
            string sErrMsg = "";
            string sDbName = ((DataRowView)cmbDbSource.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = string.Format("Copied TLE from {0} : ", sDbName.Replace(" ", ""));
            sDbName = ((DataRowView)CmbDbDest.SelectedItem)["DatabaseName"].ToString();

            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                foreach (DataRowView drView in chlTLEList.CheckedItems)
                    if (!string.IsNullOrEmpty((sErrMsg = SaveDataByID(drView["TLEName"].ToString(), oSqlTrans))))
                    {
                        oSqlTrans.Rollback();
                        CommonClass.doWriteErrorFile(sErrMsg);
                        return string.IsNullOrEmpty(sErrMsg);
                    }
                    else
                        sLogDetail += string.Format("{0} \n", drView["TLEName"].ToString());

                oSqlTrans.Commit();
                CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, sDbName, "Copy TLE", sLogDetail);
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Copy each selected card in database
        /// </summary>
        /// <param name="sTLEName">string : TLE's name that will be save</param>
        /// <param name="oSqlTrans">SqlTransaction : Sql Transaction session to save all selected TLE</param>
        /// <returns>string : error message while trying to save given TLE</returns>
        protected string SaveDataByID(string sTLEName, SqlTransaction oSqlTrans)
        {
            string sMessage = "";
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileTLECopy, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@NewDatabaseID", SqlDbType.VarChar).Value = sGetDbIdDest();
                oSqlCmd.Parameters.Add("@OldDatabaseID", SqlDbType.VarChar).Value = sGetDbIdSource();
                oSqlCmd.Parameters.Add("@sTLEName", SqlDbType.VarChar).Value = sTLEName;
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        sMessage = oRead[0].ToString();
                    oRead.Close();
                }
            }
            return sMessage;
        }
    }
}
