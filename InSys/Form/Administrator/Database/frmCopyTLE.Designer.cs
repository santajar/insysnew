﻿namespace InSys
{
    partial class FrmCopyTLE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCopyTLE));
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbCard = new System.Windows.Forms.GroupBox();
            this.chlTLEList = new System.Windows.Forms.CheckedListBox();
            this.CmbDbDest = new System.Windows.Forms.ComboBox();
            this.cmbDbSource = new System.Windows.Forms.ComboBox();
            this.lblDbDest = new System.Windows.Forms.Label();
            this.lblTLEName = new System.Windows.Forms.Label();
            this.lblDbSource = new System.Windows.Forms.Label();
            this.gbButton.SuspendLayout();
            this.gbCard.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(12, 313);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(378, 47);
            this.gbButton.TabIndex = 5;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(257, 14);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 28);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSave.Location = new System.Drawing.Point(136, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(115, 28);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbCard
            // 
            this.gbCard.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCard.Controls.Add(this.chlTLEList);
            this.gbCard.Controls.Add(this.CmbDbDest);
            this.gbCard.Controls.Add(this.cmbDbSource);
            this.gbCard.Controls.Add(this.lblDbDest);
            this.gbCard.Controls.Add(this.lblTLEName);
            this.gbCard.Controls.Add(this.lblDbSource);
            this.gbCard.Location = new System.Drawing.Point(12, 12);
            this.gbCard.Name = "gbCard";
            this.gbCard.Size = new System.Drawing.Size(378, 300);
            this.gbCard.TabIndex = 4;
            this.gbCard.TabStop = false;
            // 
            // chlTLEList
            // 
            this.chlTLEList.FormattingEnabled = true;
            this.chlTLEList.Location = new System.Drawing.Point(146, 52);
            this.chlTLEList.Name = "chlTLEList";
            this.chlTLEList.ScrollAlwaysVisible = true;
            this.chlTLEList.Size = new System.Drawing.Size(210, 184);
            this.chlTLEList.TabIndex = 6;
            // 
            // CmbDbDest
            // 
            this.CmbDbDest.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbDbDest.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbDbDest.FormattingEnabled = true;
            this.CmbDbDest.Location = new System.Drawing.Point(146, 244);
            this.CmbDbDest.Name = "CmbDbDest";
            this.CmbDbDest.Size = new System.Drawing.Size(210, 21);
            this.CmbDbDest.TabIndex = 5;
            // 
            // cmbDbSource
            // 
            this.cmbDbSource.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDbSource.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDbSource.FormattingEnabled = true;
            this.cmbDbSource.Location = new System.Drawing.Point(146, 25);
            this.cmbDbSource.Name = "cmbDbSource";
            this.cmbDbSource.Size = new System.Drawing.Size(210, 21);
            this.cmbDbSource.TabIndex = 3;
            this.cmbDbSource.SelectedIndexChanged += new System.EventHandler(this.cmbDbSource_SelectedIndexChanged);
            // 
            // lblDbDest
            // 
            this.lblDbDest.AutoSize = true;
            this.lblDbDest.Location = new System.Drawing.Point(18, 247);
            this.lblDbDest.Name = "lblDbDest";
            this.lblDbDest.Size = new System.Drawing.Size(109, 13);
            this.lblDbDest.TabIndex = 2;
            this.lblDbDest.Text = "Database Destination";
            // 
            // lblTLEName
            // 
            this.lblTLEName.AutoSize = true;
            this.lblTLEName.Location = new System.Drawing.Point(18, 52);
            this.lblTLEName.Name = "lblTLEName";
            this.lblTLEName.Size = new System.Drawing.Size(58, 13);
            this.lblTLEName.TabIndex = 1;
            this.lblTLEName.Text = "TLE Name";
            // 
            // lblDbSource
            // 
            this.lblDbSource.AutoSize = true;
            this.lblDbSource.Location = new System.Drawing.Point(18, 28);
            this.lblDbSource.Name = "lblDbSource";
            this.lblDbSource.Size = new System.Drawing.Size(90, 13);
            this.lblDbSource.TabIndex = 0;
            this.lblDbSource.Text = "Database Source";
            // 
            // FrmCopyTLE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 369);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbCard);
            this.Name = "FrmCopyTLE";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Copy TLE";
            this.Load += new System.EventHandler(this.frmCopyTLE_Load);
            this.gbButton.ResumeLayout(false);
            this.gbCard.ResumeLayout(false);
            this.gbCard.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbCard;
        private System.Windows.Forms.CheckedListBox chlTLEList;
        private System.Windows.Forms.ComboBox CmbDbDest;
        private System.Windows.Forms.ComboBox cmbDbSource;
        private System.Windows.Forms.Label lblDbDest;
        private System.Windows.Forms.Label lblTLEName;
        private System.Windows.Forms.Label lblDbSource;
    }
}