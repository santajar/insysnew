namespace InSys
{
    partial class FrmSoftwareVersion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSoftwareVersion));
            this.gbData = new System.Windows.Forms.GroupBox();
            this.cmbDatabaseID = new System.Windows.Forms.ComboBox();
            this.lblVersionName = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lbAvailableSW = new System.Windows.Forms.ListBox();
            this.lblAvailableSW = new System.Windows.Forms.Label();
            this.lbRegisteredSW = new System.Windows.Forms.ListBox();
            this.lblRegisteredSW = new System.Windows.Forms.Label();
            this.gbData.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbData
            // 
            this.gbData.Controls.Add(this.lbRegisteredSW);
            this.gbData.Controls.Add(this.lbAvailableSW);
            this.gbData.Controls.Add(this.lblRegisteredSW);
            this.gbData.Controls.Add(this.lblAvailableSW);
            this.gbData.Controls.Add(this.btnRemove);
            this.gbData.Controls.Add(this.btnAdd);
            this.gbData.Controls.Add(this.lblVersionName);
            this.gbData.Controls.Add(this.cmbDatabaseID);
            this.gbData.Location = new System.Drawing.Point(7, 2);
            this.gbData.Name = "gbData";
            this.gbData.Size = new System.Drawing.Size(446, 317);
            this.gbData.TabIndex = 0;
            this.gbData.TabStop = false;
            // 
            // cmbDatabaseID
            // 
            this.cmbDatabaseID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDatabaseID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDatabaseID.FormattingEnabled = true;
            this.cmbDatabaseID.Location = new System.Drawing.Point(147, 19);
            this.cmbDatabaseID.Name = "cmbDatabaseID";
            this.cmbDatabaseID.Size = new System.Drawing.Size(221, 21);
            this.cmbDatabaseID.TabIndex = 1;
            this.cmbDatabaseID.SelectedIndexChanged += new System.EventHandler(this.cmbDatabaseID_SelectedIndexChanged);
            // 
            // lblVersionName
            // 
            this.lblVersionName.AutoSize = true;
            this.lblVersionName.Location = new System.Drawing.Point(68, 22);
            this.lblVersionName.Name = "lblVersionName";
            this.lblVersionName.Size = new System.Drawing.Size(73, 13);
            this.lblVersionName.TabIndex = 0;
            this.lblVersionName.Text = "Version Name";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(202, 147);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(41, 30);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = ">";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(202, 183);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(41, 30);
            this.btnRemove.TabIndex = 5;
            this.btnRemove.Text = "<";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Location = new System.Drawing.Point(7, 321);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(446, 53);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(340, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 30);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(234, 15);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 30);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lbAvailableSW
            // 
            this.lbAvailableSW.FormattingEnabled = true;
            this.lbAvailableSW.Location = new System.Drawing.Point(7, 86);
            this.lbAvailableSW.Name = "lbAvailableSW";
            this.lbAvailableSW.ScrollAlwaysVisible = true;
            this.lbAvailableSW.Size = new System.Drawing.Size(189, 225);
            this.lbAvailableSW.TabIndex = 3;
            // 
            // lblAvailableSW
            // 
            this.lblAvailableSW.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblAvailableSW.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAvailableSW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.lblAvailableSW.Location = new System.Drawing.Point(7, 62);
            this.lblAvailableSW.Name = "lblAvailableSW";
            this.lblAvailableSW.Size = new System.Drawing.Size(189, 25);
            this.lblAvailableSW.TabIndex = 2;
            this.lblAvailableSW.Text = "Available Software Profile";
            this.lblAvailableSW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbRegisteredSW
            // 
            this.lbRegisteredSW.FormattingEnabled = true;
            this.lbRegisteredSW.Location = new System.Drawing.Point(249, 86);
            this.lbRegisteredSW.Name = "lbRegisteredSW";
            this.lbRegisteredSW.ScrollAlwaysVisible = true;
            this.lbRegisteredSW.Size = new System.Drawing.Size(189, 225);
            this.lbRegisteredSW.TabIndex = 7;
            // 
            // lblRegisteredSW
            // 
            this.lblRegisteredSW.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblRegisteredSW.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRegisteredSW.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.lblRegisteredSW.Location = new System.Drawing.Point(249, 62);
            this.lblRegisteredSW.Name = "lblRegisteredSW";
            this.lblRegisteredSW.Size = new System.Drawing.Size(189, 25);
            this.lblRegisteredSW.TabIndex = 6;
            this.lblRegisteredSW.Text = "Registered Software Profile";
            this.lblRegisteredSW.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmSoftwareVersion
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(460, 378);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(470, 414);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(470, 414);
            this.Name = "FrmSoftwareVersion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Software Profile Management";
            this.Load += new System.EventHandler(this.FrmSoftwareVersion_Load);
            this.gbData.ResumeLayout(false);
            this.gbData.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbData;
        private System.Windows.Forms.ComboBox cmbDatabaseID;
        private System.Windows.Forms.Label lblVersionName;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ListBox lbRegisteredSW;
        private System.Windows.Forms.Label lblAvailableSW;
        private System.Windows.Forms.ListBox lbAvailableSW;
        private System.Windows.Forms.Label lblRegisteredSW;
    }
}