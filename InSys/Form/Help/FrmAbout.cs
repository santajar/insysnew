using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

using System.Diagnostics;

namespace InSys
{
    public partial class FrmAbout : Form
    {
        protected int iScrollValue = 0; //Needed in bonus feature. (autoscroll) ...and as a timer limit also.
        protected SqlConnection oSqlConn;

        public FrmAbout(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        private void FrmAbout_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.FromArgb(253, 253, 253);
            lblProgramVersion.Text = Application.ProductName 
                + " v." + Application.ProductVersion 
                + Environment.NewLine + Application.CompanyName;
            webLicenseAgreement.Url = new Uri(Application.StartupPath + "\\LICENSE");
            webLicenseAgreement.Refresh(WebBrowserRefreshOption.Completely);
        }

        private void lnkWebsite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://" + lnkWebsite.Text);
        }

        private void webLicenseAgreement_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            tmrAbout.Start();
        }

        private void tmrAbout_Tick(object sender, EventArgs e)
        {
            tmrAbout.Interval = 100;
            if (webLicenseAgreement.Focused || iScrollValue > webLicenseAgreement.Document.Body.ScrollTop + 1) tmrAbout.Stop();
            else webLicenseAgreement.Document.Body.ScrollTop = iScrollValue++;
        }
    }
}