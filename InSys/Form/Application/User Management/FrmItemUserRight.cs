﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmItemUserRight : Form
    {
        protected SqlConnection oConn;
        protected string sUserID;
        protected bool isEdit = false;
        protected DataTable dtDisabled;
        protected DataTable dtEnabled;
        protected bool bDelete = false;
        /// <summary>
        /// Form edit user rights
        /// </summary>
        /// <param name="_oConn">Sql Connecntio</param>
        /// <param name="_sUID">string : UserID</param>
        /// <param name="_isEdit">string : Edit</param>
        /// <param name="_sGroupId">string : Group ID User</param>
        public FrmItemUserRight(SqlConnection _oConn, string _sUID, bool _isEdit, string _sGroupId)
        {
            InitializeComponent();
            oConn = _oConn;
            sUserID = _sUID;
            isEdit = _isEdit;
            if (!string.IsNullOrEmpty(_sGroupId)) sGroupID = _sGroupId;
        }

        private void FrmItemUserRight_Load(object sender, EventArgs e)
        {
            doInitiateForm();
            //doFillListBox(lbAvailable, "Disabled");
            //doFillListBox(lbEnable, "Enabled");
            initCmbDbID();
        }

        private void cmbDbId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbId.DisplayMember) && !string.IsNullOrEmpty(cmbDbId.ValueMember))
            {
                if (cmbDbId.Items.Count > 0)
                {
                    doFillListBox(lbAvailable, "Disabled");
                    doFillListBox(lbEnable, "Enabled");
                }
            }
        }

        /// <summary>
        /// Get Database ID
        /// </summary>
        /// <returns></returns>
        protected string sGetDbId()
        {
            return (cmbDbId.SelectedIndex >= 0) ? cmbDbId.SelectedValue.ToString() : "";
        }

        private void initCmbDbID()
        {
            CommonClass.FillComboBox(oConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbId);
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            if (lbAvailable.SelectedIndices.Count == 1)
                AddRemoveList(ref dtEnabled, ref dtDisabled, (DataRowView)lbAvailable.SelectedItem);
        }

        private void btnUnReg_Click(object sender, EventArgs e)
        {
            if (lbEnable.SelectedIndices.Count == 1)
                AddRemoveList(ref dtDisabled, ref dtEnabled, (DataRowView)lbEnable.SelectedItem);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dtEnabled.Rows.Count > 0)
                if (!isEdit && IsValidGroupId())
                    doSaveData();
                else
                    doSaveData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(string.Format("Are you sure want to delete GroupID '{0}'?", sGroupID)) == DialogResult.OK)
            {
                bDelete = true;
                doSaveData();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region "Function"
        /// <summary>
        /// initialize Data in Form User Right
        /// </summary>
        protected void doInitiateForm()
        {
            this.Text = (isEdit) ? this.Text + ": Edit User Group" : this.Text + ": Add User Group";
            txtGroupID.Enabled = !isEdit;
            btnDelete.Enabled = isEdit;
        }

        //protected void doFillDisabledItem()
        //{
        //    lbAvailable.DataSource = dtDisabledItem();
        //    lbAvailable.DisplayMember = "ItemName";
        //    lbAvailable.ValueMember = "Tag";
        //    lbAvailable.ClearSelected();
        //}

        //protected void doFillEnableItem()
        //{
        //    lbEnable.DataSource = dtEnableItem();
        //    lbEnable.DisplayMember = "ItemName";
        //    lbEnable.ValueMember = "Tag";
        //    lbEnable.ClearSelected();
        //}

        /// <summary>
        /// Fill data List Box
        /// </summary>
        /// <param name="oBox">List Box:</param>
        /// <param name="sLbName">string : Label Name</param>
        protected void doFillListBox(ListBox oBox, string sLbName)
        {
            DataTable dtTemp = dtItem(sLbName);
            doAssignDataTable(dtTemp, sLbName);
            oBox.DataSource = dtTemp;
            oBox.DisplayMember = "ItemName";
            oBox.ValueMember = "Tag";
            oBox.ClearSelected();
        }

        //protected DataTable dtDisabledItem()
        //{
        //    DataTable dtTable = new DataTable();
        //    using (SqlCommand oComm = new SqlCommand(CommonSP.sSPItemDisabledBrowse, oConn))
        //    {
        //        oComm.CommandType = CommandType.StoredProcedure;
        //        oComm.Parameters.Add("@sGroupID", SqlDbType.VarChar).Value = (isEdit) ? sGetGroupID() : "";
        //        using (SqlDataAdapter oAdapat = new SqlDataAdapter(oComm))
        //            oAdapat.Fill(dtTable);
        //    }
        //    return dtTable;
        //}

        //protected DataTable dtEnableItem()
        //{
        //    DataTable dtTable = new DataTable();

        //    using (SqlCommand oComm = new SqlCommand(CommonSP.sSPItemEnableBrowse, oConn))
        //    {
        //        oComm.CommandType = CommandType.StoredProcedure;
        //        oComm.Parameters.Add("@sGroupID", SqlDbType.VarChar).Value = (isEdit) ? sGetGroupID() : "";
        //        using (SqlDataAdapter oAdapat = new SqlDataAdapter(oComm))
        //            oAdapat.Fill(dtTable);
        //    }

        //    return dtTable;
        //}

        protected void AddRemoveList(ref DataTable oTableAdd, ref DataTable oTableDel, DataRowView drvView)
        {
            DataRow drRow = oTableAdd.NewRow();
            drRow["Tag"] = drvView["Tag"].ToString();
            drRow["ItemName"] = drvView["ItemName"].ToString();
            oTableAdd.Rows.Add(drRow);
            oTableDel.Rows.Remove(drvView.Row);
            lbAvailable.Refresh();
            lbEnable.Refresh();
            oTableAdd.AcceptChanges();
            oTableDel.AcceptChanges();
        }

        protected DataTable dtItem(string sListBoxName)
        {
            DataTable dtTable = new DataTable();
            using (SqlCommand oComm = new SqlCommand(sSP(sListBoxName), oConn))
            {
                oComm.CommandType = CommandType.StoredProcedure;
                oComm.Parameters.Add("@sGroupID", SqlDbType.VarChar).Value = (isEdit) ? sGroupID : "";
                oComm.Parameters.Add("@sDatabaseID", SqlDbType.VarChar).Value = sGetDbId();
                using (SqlDataAdapter oAdapat = new SqlDataAdapter(oComm))
                    oAdapat.Fill(dtTable);
            }
            return dtTable;
        }

        protected string sSP(string sListBoxName)
        {
            return (sListBoxName.ToLower().Contains("enabled")) ? CommonSP.sSPItemEnableBrowse : CommonSP.sSPItemDisabledBrowse;
        }

        protected void doAssignDataTable(DataTable dtTable, string sName)
        {
            if (sName.ToLower().Contains("enabled"))
                dtEnabled = dtTable;
            else
                dtDisabled = dtTable;
        }

        protected string sGroupID
        {
            get { return txtGroupID.Text.Trim(); }
            set { txtGroupID.Text = value; }
        }

        protected void doSaveData()
        {
            SqlTransaction oTrans = oConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                doDeleteTag(oTrans);
                if (bDelete== false)
                    doSaveTag(oTrans);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                oTrans.Rollback();
            }
            oTrans.Commit();
            bDelete = false;
            this.Close();
        }

        protected void doDeleteTag(SqlTransaction oTrans)
        {
            using (SqlCommand oComm = new SqlCommand(CommonSP.sSPUserGroupDelete, oConn))
            {
                oComm.CommandType = CommandType.StoredProcedure;
                oComm.Transaction = oTrans;
                oComm.Parameters.Add("@sGroupID", SqlDbType.VarChar).Value = (isEdit) ? sGroupID : "";
                oComm.ExecuteNonQuery();
            }
        }

        protected void doSaveTag(SqlTransaction oTrans)
        {
            DataColumn colGroupId = new DataColumn("GroupID");
            colGroupId.DefaultValue = sGroupID;
            dtEnabled.Columns.Remove(colGroupId.ColumnName);
            dtEnabled.Columns.Add(colGroupId);

            SqlBulkCopy sqlBulk = new SqlBulkCopy(oConn, SqlBulkCopyOptions.Default, oTrans);
            sqlBulk.DestinationTableName = "tbUserGroup";
            SqlBulkCopyColumnMapping mapGroupId = new SqlBulkCopyColumnMapping("GroupID", "GroupID");
            SqlBulkCopyColumnMapping mapItemName = new SqlBulkCopyColumnMapping("ItemName", "ItemName");
            SqlBulkCopyColumnMapping mapTag = new SqlBulkCopyColumnMapping("Tag", "Tag");
            sqlBulk.ColumnMappings.Add(mapGroupId);
            sqlBulk.ColumnMappings.Add(mapItemName);
            sqlBulk.ColumnMappings.Add(mapTag);
            sqlBulk.WriteToServer(dtEnabled);
        }

        protected bool IsValidGroupId()
        {
            bool bReturn = false;
            if (!string.IsNullOrEmpty(sGroupID))
            {
                using (DataTable dtTemp = dtItem("enabled"))
                {
                    if (dtTemp.Rows.Count == 0)
                        bReturn = true;
                }
            }
            return bReturn;
        }
        #endregion
    }
}
