﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmUserGroupMngmt : Form
    {
        protected SqlConnection oConn;
        protected string sUserID;

        protected DataTable dtListUserGroupDefault = new DataTable();
        protected DataTable dtListUserGroup = new DataTable();
        protected DataTable dtGroup = new DataTable();
        protected bool isSaved = true;

        public FrmUserGroupMngmt(SqlConnection _oConn, string _sUID)
        {
            oConn = _oConn;
            sUserID = _sUID;
            InitializeComponent();
        }

        private void FrmUserGroupMngmt_Load(object sender, EventArgs e)
        {
            InitForm();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsSaveSuccess())
                isSaved = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sSelectedGroupID))
            {
                int iIndex = lbAvailableUser.SelectedIndex;
                if (iIndex > -1)
                {
                    isSaved = false;
                    UpdateUserGroup(sSelectedGroupID, lbAvailableUser.Items[iIndex].ToString());
                    InitListAvailableUserGroup();
                    InitListRegisteredUserGroup(sSelectedGroupID);
                }
            }
        }

        private void btnUnReg_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sSelectedGroupID))
            {
                int iIndex = lbRegisteredUser.SelectedIndex;
                if (iIndex > -1)
                {
                    isSaved = false;
                    UpdateUserGroup(null, lbRegisteredUser.Items[iIndex].ToString());
                    InitListAvailableUserGroup();
                    InitListRegisteredUserGroup(sSelectedGroupID);
                }
            }
        }

        private void cmbUserGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sSelectedGroupID))
                InitListRegisteredUserGroup(sSelectedGroupID);
        }

        #region "Function"
        protected void InitForm()
        {
            InitUserAndGroup();
            InitComboUserGroup();
            InitListAvailableUserGroup();
        }

        protected void InitUserAndGroup()
        {
            using (SqlCommand oCmd = new SqlCommand(
                string.Format("EXEC {0}\nEXEC {1} 'WHERE UserRights NOT LIKE ''%SU011%'''",
                CommonSP.sSPUserGroupBrowse, CommonSP.sSPUserBrowse), oConn))
            {
                using (DataSet dsUserGroup = new DataSet())
                {
                    (new SqlDataAdapter(oCmd)).Fill(dsUserGroup);
                    dtGroup = new DataTable();
                    dtListUserGroup = new DataTable();
                    dtListUserGroupDefault = new DataTable();
                    dtGroup = dsUserGroup.Tables[0];
                    dtListUserGroup = dsUserGroup.Tables[1];
                    dtListUserGroup.TableName = "tbUserLogin";
                    dtListUserGroupDefault = dtListUserGroup.Copy(); ;
                }
            }
        }

        protected void InitComboUserGroup()
        {
            if (dtGroup.Rows.Count > 0)
            {
                cmbUserGroup.Items.Clear();
                cmbUserGroup.Items.AddRange((
                    from groupid in dtGroup.AsEnumerable()
                    select groupid.Field<string>("GroupID")
                    ).Distinct().ToArray());
            }
        }

        protected void InitListAvailableUserGroup()
        {
            if (dtListUserGroup.Rows.Count > 0)
            {
                lbAvailableUser.Items.Clear();
                lbAvailableUser.Items.AddRange((
                    from usergroup in dtListUserGroup.AsEnumerable()
                    where usergroup.Field<string>("GroupID") == null
                    orderby usergroup.Field<string>("UserID")
                    select usergroup.Field<string>("UserID")
                    ).ToArray());
            }
        }

        protected void InitListRegisteredUserGroup(string sGroupID)
        {
            lbRegisteredUser.Items.Clear();
            lbRegisteredUser.Items.AddRange((
                    from usergroup in dtListUserGroup.AsEnumerable()
                    where usergroup.Field<string>("GroupID") == sGroupID
                    orderby usergroup.Field<string>("UserID")
                    select usergroup.Field<string>("UserID")
                    ).ToArray());
        }

        protected string sSelectedGroupID { get { return cmbUserGroup.Text; } }

        protected void UpdateUserGroup(string _sGroupID, string _sUserID)
        {
            int iIndex = dtListUserGroup.Rows.IndexOf(( dtListUserGroup.Select(string.Format("UserID='{0}'", _sUserID)))[0]);
            dtListUserGroup.Rows[iIndex]["GroupID"] = _sGroupID;
        }

        protected bool IsSaveSuccess()
        {
            string sTransName = UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss");
            string sError = null;
            if (!ClassSqlBulkUpSert.isSqlBulkUpsertSuccess(
                oConn, dtListUserGroupDefault, dtListUserGroup,
                (new string[1] { "GroupID" }), sTransName, "Temp", ref sError))
            {
                MessageBox.Show(sError);
                return false;
            }
            
            return true;
        }
        #endregion
    }
}
