namespace InSys
{
    partial class FrmUserMngmt_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUserMngmt_Edit));
            this.gbUserInfo = new System.Windows.Forms.GroupBox();
            this.chkRevoke = new System.Windows.Forms.CheckBox();
            this.chkLocked = new System.Windows.Forms.CheckBox();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.lblPass = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblUID = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.gbRights = new System.Windows.Forms.GroupBox();
            this.tabUserRights = new System.Windows.Forms.TabControl();
            this.tabMenu = new System.Windows.Forms.TabPage();
            this.gbApplication = new System.Windows.Forms.GroupBox();
            this.gbOthers = new System.Windows.Forms.GroupBox();
            this.chkInitPicture = new System.Windows.Forms.CheckBox();
            this.chkQueryReport = new System.Windows.Forms.CheckBox();
            this.chkSchedule = new System.Windows.Forms.CheckBox();
            this.chkPopulateTLV = new System.Windows.Forms.CheckBox();
            this.gbUtilities = new System.Windows.Forms.GroupBox();
            this.chkDownload = new System.Windows.Forms.CheckBox();
            this.chkEdcIpTracking = new System.Windows.Forms.CheckBox();
            this.gbSoftwarePackage = new System.Windows.Forms.GroupBox();
            this.chkSoftwareDelete = new System.Windows.Forms.CheckBox();
            this.chkSoftwareEdit = new System.Windows.Forms.CheckBox();
            this.chkSoftwareAdd = new System.Windows.Forms.CheckBox();
            this.chkSoftwareView = new System.Windows.Forms.CheckBox();
            this.gbSetting = new System.Windows.Forms.GroupBox();
            this.chkAutoInit = new System.Windows.Forms.CheckBox();
            this.chkCompressInit = new System.Windows.Forms.CheckBox();
            this.chkAllowInit = new System.Windows.Forms.CheckBox();
            this.chkSNValidation = new System.Windows.Forms.CheckBox();
            this.gbLocation = new System.Windows.Forms.GroupBox();
            this.chkLocationImport = new System.Windows.Forms.CheckBox();
            this.chkLocationAdd = new System.Windows.Forms.CheckBox();
            this.chkLocationView = new System.Windows.Forms.CheckBox();
            this.gbInitTrail = new System.Windows.Forms.GroupBox();
            this.chkInitTrailImport = new System.Windows.Forms.CheckBox();
            this.chkInitTrailExport = new System.Windows.Forms.CheckBox();
            this.chkInitTrailDelete = new System.Windows.Forms.CheckBox();
            this.chkInitTrailView = new System.Windows.Forms.CheckBox();
            this.gbAuditTrail = new System.Windows.Forms.GroupBox();
            this.chkAuditTrailImport = new System.Windows.Forms.CheckBox();
            this.chkAuditTrailExport = new System.Windows.Forms.CheckBox();
            this.chkAuditTrailDelete = new System.Windows.Forms.CheckBox();
            this.chkAuditTrailView = new System.Windows.Forms.CheckBox();
            this.gbData = new System.Windows.Forms.GroupBox();
            this.chkDataView = new System.Windows.Forms.CheckBox();
            this.chkTemplateDef = new System.Windows.Forms.CheckBox();
            this.chkUpload = new System.Windows.Forms.CheckBox();
            this.chkUploadDef = new System.Windows.Forms.CheckBox();
            this.gbUserMan = new System.Windows.Forms.GroupBox();
            this.chkUserManDelete = new System.Windows.Forms.CheckBox();
            this.chkUserManEdit = new System.Windows.Forms.CheckBox();
            this.chkUserManAdd = new System.Windows.Forms.CheckBox();
            this.chkUserManView = new System.Windows.Forms.CheckBox();
            this.tabProfile = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkAuditTrailWeb = new System.Windows.Forms.CheckBox();
            this.chkSoftwareTrailWeb = new System.Windows.Forms.CheckBox();
            this.gbProfile2 = new System.Windows.Forms.GroupBox();
            this.gbPromoManagement = new System.Windows.Forms.GroupBox();
            this.chkPromoManagementDelete = new System.Windows.Forms.CheckBox();
            this.chkPromoManagementEdit = new System.Windows.Forms.CheckBox();
            this.chkPromoManagementAdd = new System.Windows.Forms.CheckBox();
            this.chkPromoManagementView = new System.Windows.Forms.CheckBox();
            this.gbEMVManagement = new System.Windows.Forms.GroupBox();
            this.chkEMVManagementDelete = new System.Windows.Forms.CheckBox();
            this.chkEMVManagementEdit = new System.Windows.Forms.CheckBox();
            this.chkEMVManagementAdd = new System.Windows.Forms.CheckBox();
            this.chkEMVManagementView = new System.Windows.Forms.CheckBox();
            this.gbRemoteDownload = new System.Windows.Forms.GroupBox();
            this.chkRemoteDownloadDelete = new System.Windows.Forms.CheckBox();
            this.chkRemoteDownloadEdit = new System.Windows.Forms.CheckBox();
            this.chkRemoteDownloadAdd = new System.Windows.Forms.CheckBox();
            this.chkRemoteDownloadView = new System.Windows.Forms.CheckBox();
            this.gbEdcMonitoring = new System.Windows.Forms.GroupBox();
            this.chkEdcMonitorEdit = new System.Windows.Forms.CheckBox();
            this.chkEdcMonitorView = new System.Windows.Forms.CheckBox();
            this.chkEdcMonitorAdd = new System.Windows.Forms.CheckBox();
            this.chkEdcMonitorDelete = new System.Windows.Forms.CheckBox();
            this.gbPinPad = new System.Windows.Forms.GroupBox();
            this.chkPinPadDelete = new System.Windows.Forms.CheckBox();
            this.chkPinPadedit = new System.Windows.Forms.CheckBox();
            this.chkPinPadAdd = new System.Windows.Forms.CheckBox();
            this.chkPinPadView = new System.Windows.Forms.CheckBox();
            this.gbInitialFlazz = new System.Windows.Forms.GroupBox();
            this.chkInitialFlazzDelete = new System.Windows.Forms.CheckBox();
            this.chkInitialFlazzEdit = new System.Windows.Forms.CheckBox();
            this.chkInitialFlazzAdd = new System.Windows.Forms.CheckBox();
            this.chkInitialFlazzView = new System.Windows.Forms.CheckBox();
            this.gbGPRSMan = new System.Windows.Forms.GroupBox();
            this.chkGPRSDel = new System.Windows.Forms.CheckBox();
            this.chkGPRSEdit = new System.Windows.Forms.CheckBox();
            this.chkGPRSAdd = new System.Windows.Forms.CheckBox();
            this.chkGPRSView = new System.Windows.Forms.CheckBox();
            this.gbCardTypeManagement = new System.Windows.Forms.GroupBox();
            this.chkCardTypeDelete = new System.Windows.Forms.CheckBox();
            this.chkCardTypeEdit = new System.Windows.Forms.CheckBox();
            this.chkCardTypeAdd = new System.Windows.Forms.CheckBox();
            this.chkCardTypeManView = new System.Windows.Forms.CheckBox();
            this.gbTLEManagement = new System.Windows.Forms.GroupBox();
            this.chkTLEDelete = new System.Windows.Forms.CheckBox();
            this.chkTLEEdit = new System.Windows.Forms.CheckBox();
            this.chkTLEAdd = new System.Windows.Forms.CheckBox();
            this.chkTLEView = new System.Windows.Forms.CheckBox();
            this.gbCard = new System.Windows.Forms.GroupBox();
            this.chkCardDelete = new System.Windows.Forms.CheckBox();
            this.chkCardAdd = new System.Windows.Forms.CheckBox();
            this.chkCardView = new System.Windows.Forms.CheckBox();
            this.gbKey = new System.Windows.Forms.GroupBox();
            this.chkKeyDelete = new System.Windows.Forms.CheckBox();
            this.chkKeyEdit = new System.Windows.Forms.CheckBox();
            this.chkKeyAdd = new System.Windows.Forms.CheckBox();
            this.chkKeyView = new System.Windows.Forms.CheckBox();
            this.gbAID = new System.Windows.Forms.GroupBox();
            this.chkAIDDelete = new System.Windows.Forms.CheckBox();
            this.chkAIDEdit = new System.Windows.Forms.CheckBox();
            this.chkAIDAdd = new System.Windows.Forms.CheckBox();
            this.chkAIDView = new System.Windows.Forms.CheckBox();
            this.gbCardRange = new System.Windows.Forms.GroupBox();
            this.chkCardRangeDelete = new System.Windows.Forms.CheckBox();
            this.chkCardRangeEdit = new System.Windows.Forms.CheckBox();
            this.chkCardRangeAdd = new System.Windows.Forms.CheckBox();
            this.chkCardRangeView = new System.Windows.Forms.CheckBox();
            this.gbIssuer = new System.Windows.Forms.GroupBox();
            this.chkIssuerDelete = new System.Windows.Forms.CheckBox();
            this.chkIssuerEdit = new System.Windows.Forms.CheckBox();
            this.chkIssuerAdd = new System.Windows.Forms.CheckBox();
            this.chkIssuerView = new System.Windows.Forms.CheckBox();
            this.gbAcq = new System.Windows.Forms.GroupBox();
            this.chkAcqDelete = new System.Windows.Forms.CheckBox();
            this.chkAcqEdit = new System.Windows.Forms.CheckBox();
            this.chkAcqAdd = new System.Windows.Forms.CheckBox();
            this.chkAcqView = new System.Windows.Forms.CheckBox();
            this.gbTerminal = new System.Windows.Forms.GroupBox();
            this.chkTerminalDelete = new System.Windows.Forms.CheckBox();
            this.chkTerminalEdit = new System.Windows.Forms.CheckBox();
            this.chkTerminalAdd = new System.Windows.Forms.CheckBox();
            this.chkTerminalView = new System.Windows.Forms.CheckBox();
            this.gbBankCode = new System.Windows.Forms.GroupBox();
            this.ChkBankCodeEdit = new System.Windows.Forms.CheckBox();
            this.ChkBankCodeView = new System.Windows.Forms.CheckBox();
            this.ChkBankCodeAdd = new System.Windows.Forms.CheckBox();
            this.ChkBankCodeDelete = new System.Windows.Forms.CheckBox();
            this.gbProfile1 = new System.Windows.Forms.GroupBox();
            this.chkCreateTerminal = new System.Windows.Forms.CheckBox();
            this.chkRegisterOldFile = new System.Windows.Forms.CheckBox();
            this.tipTextBoxUserInfo = new System.Windows.Forms.ToolTip(this.components);
            this.gbUserInfo.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.gbRights.SuspendLayout();
            this.tabUserRights.SuspendLayout();
            this.tabMenu.SuspendLayout();
            this.gbApplication.SuspendLayout();
            this.gbOthers.SuspendLayout();
            this.gbUtilities.SuspendLayout();
            this.gbSoftwarePackage.SuspendLayout();
            this.gbSetting.SuspendLayout();
            this.gbLocation.SuspendLayout();
            this.gbInitTrail.SuspendLayout();
            this.gbAuditTrail.SuspendLayout();
            this.gbData.SuspendLayout();
            this.gbUserMan.SuspendLayout();
            this.tabProfile.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbProfile2.SuspendLayout();
            this.gbPromoManagement.SuspendLayout();
            this.gbEMVManagement.SuspendLayout();
            this.gbRemoteDownload.SuspendLayout();
            this.gbEdcMonitoring.SuspendLayout();
            this.gbPinPad.SuspendLayout();
            this.gbInitialFlazz.SuspendLayout();
            this.gbGPRSMan.SuspendLayout();
            this.gbCardTypeManagement.SuspendLayout();
            this.gbTLEManagement.SuspendLayout();
            this.gbCard.SuspendLayout();
            this.gbKey.SuspendLayout();
            this.gbAID.SuspendLayout();
            this.gbCardRange.SuspendLayout();
            this.gbIssuer.SuspendLayout();
            this.gbAcq.SuspendLayout();
            this.gbTerminal.SuspendLayout();
            this.gbBankCode.SuspendLayout();
            this.gbProfile1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbUserInfo
            // 
            this.gbUserInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbUserInfo.Controls.Add(this.chkRevoke);
            this.gbUserInfo.Controls.Add(this.chkLocked);
            this.gbUserInfo.Controls.Add(this.btnChangePass);
            this.gbUserInfo.Controls.Add(this.txtPass);
            this.gbUserInfo.Controls.Add(this.lblPass);
            this.gbUserInfo.Controls.Add(this.txtUserName);
            this.gbUserInfo.Controls.Add(this.txtUserID);
            this.gbUserInfo.Controls.Add(this.lblUserName);
            this.gbUserInfo.Controls.Add(this.lblUID);
            this.gbUserInfo.Location = new System.Drawing.Point(9, 1);
            this.gbUserInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUserInfo.Name = "gbUserInfo";
            this.gbUserInfo.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUserInfo.Size = new System.Drawing.Size(1401, 121);
            this.gbUserInfo.TabIndex = 0;
            this.gbUserInfo.TabStop = false;
            // 
            // chkRevoke
            // 
            this.chkRevoke.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.chkRevoke.AutoSize = true;
            this.chkRevoke.Location = new System.Drawing.Point(932, 49);
            this.chkRevoke.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkRevoke.Name = "chkRevoke";
            this.chkRevoke.Size = new System.Drawing.Size(78, 21);
            this.chkRevoke.TabIndex = 8;
            this.chkRevoke.Text = "Revoke";
            this.chkRevoke.UseVisualStyleBackColor = true;
            // 
            // chkLocked
            // 
            this.chkLocked.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.chkLocked.AutoSize = true;
            this.chkLocked.Location = new System.Drawing.Point(932, 21);
            this.chkLocked.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkLocked.Name = "chkLocked";
            this.chkLocked.Size = new System.Drawing.Size(76, 21);
            this.chkLocked.TabIndex = 7;
            this.chkLocked.Text = "Locked";
            this.chkLocked.UseVisualStyleBackColor = true;
            // 
            // btnChangePass
            // 
            this.btnChangePass.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePass.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnChangePass.Location = new System.Drawing.Point(932, 80);
            this.btnChangePass.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(107, 28);
            this.btnChangePass.TabIndex = 6;
            this.btnChangePass.Text = "Change";
            this.btnChangePass.Click += new System.EventHandler(this.btnChangePass_Click);
            // 
            // txtPass
            // 
            this.txtPass.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtPass.Location = new System.Drawing.Point(649, 82);
            this.txtPass.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPass.MaxLength = 25;
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(239, 22);
            this.txtPass.TabIndex = 5;
            this.tipTextBoxUserInfo.SetToolTip(this.txtPass, "Password must only contains alphanumeric");
            this.txtPass.UseSystemPasswordChar = true;
            // 
            // lblPass
            // 
            this.lblPass.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblPass.AutoSize = true;
            this.lblPass.Location = new System.Drawing.Point(512, 86);
            this.lblPass.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(69, 17);
            this.lblPass.TabIndex = 4;
            this.lblPass.Text = "Password";
            // 
            // txtUserName
            // 
            this.txtUserName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtUserName.Location = new System.Drawing.Point(649, 50);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserName.MaxLength = 25;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(239, 22);
            this.txtUserName.TabIndex = 3;
            this.tipTextBoxUserInfo.SetToolTip(this.txtUserName, "Input User Name");
            // 
            // txtUserID
            // 
            this.txtUserID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtUserID.Location = new System.Drawing.Point(649, 18);
            this.txtUserID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserID.MaxLength = 10;
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(239, 22);
            this.txtUserID.TabIndex = 1;
            this.tipTextBoxUserInfo.SetToolTip(this.txtUserID, "Input User ID");
            // 
            // lblUserName
            // 
            this.lblUserName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(512, 54);
            this.lblUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(45, 17);
            this.lblUserName.TabIndex = 2;
            this.lblUserName.Text = "Name";
            // 
            // lblUID
            // 
            this.lblUID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblUID.AutoSize = true;
            this.lblUID.Location = new System.Drawing.Point(512, 22);
            this.lblUID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUID.Name = "lblUID";
            this.lblUID.Size = new System.Drawing.Size(55, 17);
            this.lblUID.TabIndex = 0;
            this.lblUID.Text = "User ID";
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnDelete);
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Controls.Add(this.btnReset);
            this.gbButton.Location = new System.Drawing.Point(9, 880);
            this.gbButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Name = "gbButton";
            this.gbButton.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Size = new System.Drawing.Size(1620, 57);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(1338, 16);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(133, 33);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(1479, 16);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(133, 33);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSave.Location = new System.Drawing.Point(1055, 16);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(133, 33);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.Image")));
            this.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReset.Location = new System.Drawing.Point(1196, 16);
            this.btnReset.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(133, 33);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Reset";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // gbRights
            // 
            this.gbRights.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbRights.Controls.Add(this.tabUserRights);
            this.gbRights.Location = new System.Drawing.Point(9, 130);
            this.gbRights.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRights.Name = "gbRights";
            this.gbRights.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRights.Size = new System.Drawing.Size(1401, 605);
            this.gbRights.TabIndex = 1;
            this.gbRights.TabStop = false;
            // 
            // tabUserRights
            // 
            this.tabUserRights.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabUserRights.Controls.Add(this.tabMenu);
            this.tabUserRights.Controls.Add(this.tabProfile);
            this.tabUserRights.Location = new System.Drawing.Point(8, 23);
            this.tabUserRights.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabUserRights.Name = "tabUserRights";
            this.tabUserRights.SelectedIndex = 0;
            this.tabUserRights.Size = new System.Drawing.Size(1385, 574);
            this.tabUserRights.TabIndex = 0;
            // 
            // tabMenu
            // 
            this.tabMenu.Controls.Add(this.gbApplication);
            this.tabMenu.Location = new System.Drawing.Point(4, 25);
            this.tabMenu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabMenu.Name = "tabMenu";
            this.tabMenu.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabMenu.Size = new System.Drawing.Size(1567, 724);
            this.tabMenu.TabIndex = 0;
            this.tabMenu.Text = "Menu";
            this.tabMenu.UseVisualStyleBackColor = true;
            // 
            // gbApplication
            // 
            this.gbApplication.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbApplication.Controls.Add(this.gbOthers);
            this.gbApplication.Controls.Add(this.gbUtilities);
            this.gbApplication.Controls.Add(this.gbSoftwarePackage);
            this.gbApplication.Controls.Add(this.gbSetting);
            this.gbApplication.Controls.Add(this.gbLocation);
            this.gbApplication.Controls.Add(this.gbInitTrail);
            this.gbApplication.Controls.Add(this.gbAuditTrail);
            this.gbApplication.Controls.Add(this.gbData);
            this.gbApplication.Controls.Add(this.gbUserMan);
            this.gbApplication.Location = new System.Drawing.Point(19, 15);
            this.gbApplication.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbApplication.Name = "gbApplication";
            this.gbApplication.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbApplication.Size = new System.Drawing.Size(1538, 524);
            this.gbApplication.TabIndex = 0;
            this.gbApplication.TabStop = false;
            this.gbApplication.Text = "Application";
            // 
            // gbOthers
            // 
            this.gbOthers.Controls.Add(this.chkInitPicture);
            this.gbOthers.Controls.Add(this.chkQueryReport);
            this.gbOthers.Controls.Add(this.chkSchedule);
            this.gbOthers.Controls.Add(this.chkPopulateTLV);
            this.gbOthers.Location = new System.Drawing.Point(25, 286);
            this.gbOthers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbOthers.Name = "gbOthers";
            this.gbOthers.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbOthers.Size = new System.Drawing.Size(1315, 52);
            this.gbOthers.TabIndex = 6;
            this.gbOthers.TabStop = false;
            this.gbOthers.Text = "Others";
            // 
            // chkInitPicture
            // 
            this.chkInitPicture.AutoSize = true;
            this.chkInitPicture.Location = new System.Drawing.Point(40, 23);
            this.chkInitPicture.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitPicture.Name = "chkInitPicture";
            this.chkInitPicture.Size = new System.Drawing.Size(96, 21);
            this.chkInitPicture.TabIndex = 10;
            this.chkInitPicture.Text = "Init Picture";
            this.chkInitPicture.UseVisualStyleBackColor = true;
            // 
            // chkQueryReport
            // 
            this.chkQueryReport.AutoSize = true;
            this.chkQueryReport.Location = new System.Drawing.Point(232, 23);
            this.chkQueryReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkQueryReport.Name = "chkQueryReport";
            this.chkQueryReport.Size = new System.Drawing.Size(116, 21);
            this.chkQueryReport.TabIndex = 9;
            this.chkQueryReport.Text = "Query Report";
            this.chkQueryReport.UseVisualStyleBackColor = true;
            // 
            // chkSchedule
            // 
            this.chkSchedule.AutoSize = true;
            this.chkSchedule.Location = new System.Drawing.Point(439, 23);
            this.chkSchedule.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkSchedule.Name = "chkSchedule";
            this.chkSchedule.Size = new System.Drawing.Size(89, 21);
            this.chkSchedule.TabIndex = 8;
            this.chkSchedule.Text = "Schedule";
            this.chkSchedule.UseVisualStyleBackColor = true;
            // 
            // chkPopulateTLV
            // 
            this.chkPopulateTLV.AutoSize = true;
            this.chkPopulateTLV.Location = new System.Drawing.Point(632, 23);
            this.chkPopulateTLV.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPopulateTLV.Name = "chkPopulateTLV";
            this.chkPopulateTLV.Size = new System.Drawing.Size(120, 21);
            this.chkPopulateTLV.TabIndex = 7;
            this.chkPopulateTLV.Text = "Populate TLV ";
            this.chkPopulateTLV.UseVisualStyleBackColor = true;
            // 
            // gbUtilities
            // 
            this.gbUtilities.Controls.Add(this.chkDownload);
            this.gbUtilities.Controls.Add(this.chkEdcIpTracking);
            this.gbUtilities.Location = new System.Drawing.Point(25, 457);
            this.gbUtilities.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUtilities.Name = "gbUtilities";
            this.gbUtilities.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUtilities.Size = new System.Drawing.Size(1315, 55);
            this.gbUtilities.TabIndex = 1;
            this.gbUtilities.TabStop = false;
            this.gbUtilities.Text = "Utilities";
            // 
            // chkDownload
            // 
            this.chkDownload.AutoSize = true;
            this.chkDownload.Location = new System.Drawing.Point(232, 23);
            this.chkDownload.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkDownload.Name = "chkDownload";
            this.chkDownload.Size = new System.Drawing.Size(92, 21);
            this.chkDownload.TabIndex = 1;
            this.chkDownload.Text = "Download";
            this.chkDownload.UseVisualStyleBackColor = true;
            // 
            // chkEdcIpTracking
            // 
            this.chkEdcIpTracking.AutoSize = true;
            this.chkEdcIpTracking.Location = new System.Drawing.Point(39, 27);
            this.chkEdcIpTracking.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEdcIpTracking.Name = "chkEdcIpTracking";
            this.chkEdcIpTracking.Size = new System.Drawing.Size(142, 21);
            this.chkEdcIpTracking.TabIndex = 0;
            this.chkEdcIpTracking.Text = "EDC IP - Tracking";
            this.chkEdcIpTracking.UseVisualStyleBackColor = true;
            // 
            // gbSoftwarePackage
            // 
            this.gbSoftwarePackage.Controls.Add(this.chkSoftwareDelete);
            this.gbSoftwarePackage.Controls.Add(this.chkSoftwareEdit);
            this.gbSoftwarePackage.Controls.Add(this.chkSoftwareAdd);
            this.gbSoftwarePackage.Controls.Add(this.chkSoftwareView);
            this.gbSoftwarePackage.Location = new System.Drawing.Point(25, 341);
            this.gbSoftwarePackage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbSoftwarePackage.Name = "gbSoftwarePackage";
            this.gbSoftwarePackage.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbSoftwarePackage.Size = new System.Drawing.Size(1315, 55);
            this.gbSoftwarePackage.TabIndex = 6;
            this.gbSoftwarePackage.TabStop = false;
            this.gbSoftwarePackage.Text = "Software Package";
            // 
            // chkSoftwareDelete
            // 
            this.chkSoftwareDelete.AutoSize = true;
            this.chkSoftwareDelete.Enabled = false;
            this.chkSoftwareDelete.Location = new System.Drawing.Point(632, 23);
            this.chkSoftwareDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkSoftwareDelete.Name = "chkSoftwareDelete";
            this.chkSoftwareDelete.Size = new System.Drawing.Size(105, 21);
            this.chkSoftwareDelete.TabIndex = 3;
            this.chkSoftwareDelete.Text = "Delete User";
            this.chkSoftwareDelete.UseVisualStyleBackColor = true;
            // 
            // chkSoftwareEdit
            // 
            this.chkSoftwareEdit.AutoSize = true;
            this.chkSoftwareEdit.Enabled = false;
            this.chkSoftwareEdit.Location = new System.Drawing.Point(439, 23);
            this.chkSoftwareEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkSoftwareEdit.Name = "chkSoftwareEdit";
            this.chkSoftwareEdit.Size = new System.Drawing.Size(88, 21);
            this.chkSoftwareEdit.TabIndex = 2;
            this.chkSoftwareEdit.Text = "Edit User";
            this.chkSoftwareEdit.UseVisualStyleBackColor = true;
            // 
            // chkSoftwareAdd
            // 
            this.chkSoftwareAdd.AutoSize = true;
            this.chkSoftwareAdd.Enabled = false;
            this.chkSoftwareAdd.Location = new System.Drawing.Point(233, 23);
            this.chkSoftwareAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkSoftwareAdd.Name = "chkSoftwareAdd";
            this.chkSoftwareAdd.Size = new System.Drawing.Size(89, 21);
            this.chkSoftwareAdd.TabIndex = 1;
            this.chkSoftwareAdd.Text = "Add User";
            this.chkSoftwareAdd.UseVisualStyleBackColor = true;
            // 
            // chkSoftwareView
            // 
            this.chkSoftwareView.AutoSize = true;
            this.chkSoftwareView.Location = new System.Drawing.Point(39, 23);
            this.chkSoftwareView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkSoftwareView.Name = "chkSoftwareView";
            this.chkSoftwareView.Size = new System.Drawing.Size(93, 21);
            this.chkSoftwareView.TabIndex = 0;
            this.chkSoftwareView.Text = "View User";
            this.chkSoftwareView.UseVisualStyleBackColor = true;
            this.chkSoftwareView.CheckedChanged += new System.EventHandler(this.chkSoftwareView_CheckedChanged);
            // 
            // gbSetting
            // 
            this.gbSetting.Controls.Add(this.chkAutoInit);
            this.gbSetting.Controls.Add(this.chkCompressInit);
            this.gbSetting.Controls.Add(this.chkAllowInit);
            this.gbSetting.Controls.Add(this.chkSNValidation);
            this.gbSetting.Location = new System.Drawing.Point(25, 107);
            this.gbSetting.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbSetting.Name = "gbSetting";
            this.gbSetting.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbSetting.Size = new System.Drawing.Size(1315, 55);
            this.gbSetting.TabIndex = 2;
            this.gbSetting.TabStop = false;
            this.gbSetting.Text = "Setting";
            // 
            // chkAutoInit
            // 
            this.chkAutoInit.AutoSize = true;
            this.chkAutoInit.Location = new System.Drawing.Point(439, 25);
            this.chkAutoInit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAutoInit.Name = "chkAutoInit";
            this.chkAutoInit.Size = new System.Drawing.Size(113, 21);
            this.chkAutoInit.TabIndex = 2;
            this.chkAutoInit.Text = "Auto Initialize";
            this.chkAutoInit.UseVisualStyleBackColor = true;
            // 
            // chkCompressInit
            // 
            this.chkCompressInit.AutoSize = true;
            this.chkCompressInit.Location = new System.Drawing.Point(232, 25);
            this.chkCompressInit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCompressInit.Name = "chkCompressInit";
            this.chkCompressInit.Size = new System.Drawing.Size(147, 21);
            this.chkCompressInit.TabIndex = 1;
            this.chkCompressInit.Text = "Compress Initialize";
            this.chkCompressInit.UseVisualStyleBackColor = true;
            // 
            // chkAllowInit
            // 
            this.chkAllowInit.AutoSize = true;
            this.chkAllowInit.Location = new System.Drawing.Point(39, 25);
            this.chkAllowInit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAllowInit.Name = "chkAllowInit";
            this.chkAllowInit.Size = new System.Drawing.Size(116, 21);
            this.chkAllowInit.TabIndex = 0;
            this.chkAllowInit.Text = "Allow Initialize";
            this.chkAllowInit.UseVisualStyleBackColor = true;
            // 
            // chkSNValidation
            // 
            this.chkSNValidation.AutoSize = true;
            this.chkSNValidation.Location = new System.Drawing.Point(632, 22);
            this.chkSNValidation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkSNValidation.Name = "chkSNValidation";
            this.chkSNValidation.Size = new System.Drawing.Size(115, 21);
            this.chkSNValidation.TabIndex = 3;
            this.chkSNValidation.Text = "SN Validation";
            this.chkSNValidation.UseVisualStyleBackColor = true;
            // 
            // gbLocation
            // 
            this.gbLocation.Controls.Add(this.chkLocationImport);
            this.gbLocation.Controls.Add(this.chkLocationAdd);
            this.gbLocation.Controls.Add(this.chkLocationView);
            this.gbLocation.Location = new System.Drawing.Point(25, 398);
            this.gbLocation.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbLocation.Name = "gbLocation";
            this.gbLocation.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbLocation.Size = new System.Drawing.Size(1315, 55);
            this.gbLocation.TabIndex = 5;
            this.gbLocation.TabStop = false;
            this.gbLocation.Text = "Location";
            // 
            // chkLocationImport
            // 
            this.chkLocationImport.AutoSize = true;
            this.chkLocationImport.Enabled = false;
            this.chkLocationImport.Location = new System.Drawing.Point(439, 22);
            this.chkLocationImport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkLocationImport.Name = "chkLocationImport";
            this.chkLocationImport.Size = new System.Drawing.Size(127, 21);
            this.chkLocationImport.TabIndex = 2;
            this.chkLocationImport.Text = "Import Location";
            this.chkLocationImport.UseVisualStyleBackColor = true;
            // 
            // chkLocationAdd
            // 
            this.chkLocationAdd.AutoSize = true;
            this.chkLocationAdd.Enabled = false;
            this.chkLocationAdd.Location = new System.Drawing.Point(233, 22);
            this.chkLocationAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkLocationAdd.Name = "chkLocationAdd";
            this.chkLocationAdd.Size = new System.Drawing.Size(113, 21);
            this.chkLocationAdd.TabIndex = 1;
            this.chkLocationAdd.Text = "Add Location";
            this.chkLocationAdd.UseVisualStyleBackColor = true;
            // 
            // chkLocationView
            // 
            this.chkLocationView.AutoSize = true;
            this.chkLocationView.Location = new System.Drawing.Point(39, 22);
            this.chkLocationView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkLocationView.Name = "chkLocationView";
            this.chkLocationView.Size = new System.Drawing.Size(117, 21);
            this.chkLocationView.TabIndex = 0;
            this.chkLocationView.Text = "View Location";
            this.chkLocationView.UseVisualStyleBackColor = true;
            this.chkLocationView.CheckedChanged += new System.EventHandler(this.chkLocationView_CheckedChanged);
            // 
            // gbInitTrail
            // 
            this.gbInitTrail.Controls.Add(this.chkInitTrailImport);
            this.gbInitTrail.Controls.Add(this.chkInitTrailExport);
            this.gbInitTrail.Controls.Add(this.chkInitTrailDelete);
            this.gbInitTrail.Controls.Add(this.chkInitTrailView);
            this.gbInitTrail.Location = new System.Drawing.Point(719, 20);
            this.gbInitTrail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbInitTrail.Name = "gbInitTrail";
            this.gbInitTrail.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbInitTrail.Size = new System.Drawing.Size(621, 82);
            this.gbInitTrail.TabIndex = 1;
            this.gbInitTrail.TabStop = false;
            this.gbInitTrail.Text = "Initialize Trail";
            // 
            // chkInitTrailImport
            // 
            this.chkInitTrailImport.AutoSize = true;
            this.chkInitTrailImport.Enabled = false;
            this.chkInitTrailImport.Location = new System.Drawing.Point(235, 52);
            this.chkInitTrailImport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitTrailImport.Name = "chkInitTrailImport";
            this.chkInitTrailImport.Size = new System.Drawing.Size(97, 21);
            this.chkInitTrailImport.TabIndex = 3;
            this.chkInitTrailImport.Text = "Import Log";
            this.chkInitTrailImport.UseVisualStyleBackColor = true;
            // 
            // chkInitTrailExport
            // 
            this.chkInitTrailExport.AutoSize = true;
            this.chkInitTrailExport.Enabled = false;
            this.chkInitTrailExport.Location = new System.Drawing.Point(41, 52);
            this.chkInitTrailExport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitTrailExport.Name = "chkInitTrailExport";
            this.chkInitTrailExport.Size = new System.Drawing.Size(98, 21);
            this.chkInitTrailExport.TabIndex = 2;
            this.chkInitTrailExport.Text = "Export Log";
            this.chkInitTrailExport.UseVisualStyleBackColor = true;
            // 
            // chkInitTrailDelete
            // 
            this.chkInitTrailDelete.AutoSize = true;
            this.chkInitTrailDelete.Enabled = false;
            this.chkInitTrailDelete.Location = new System.Drawing.Point(235, 23);
            this.chkInitTrailDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitTrailDelete.Name = "chkInitTrailDelete";
            this.chkInitTrailDelete.Size = new System.Drawing.Size(99, 21);
            this.chkInitTrailDelete.TabIndex = 1;
            this.chkInitTrailDelete.Text = "Delete Log";
            this.chkInitTrailDelete.UseVisualStyleBackColor = true;
            // 
            // chkInitTrailView
            // 
            this.chkInitTrailView.AutoSize = true;
            this.chkInitTrailView.Location = new System.Drawing.Point(41, 23);
            this.chkInitTrailView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitTrailView.Name = "chkInitTrailView";
            this.chkInitTrailView.Size = new System.Drawing.Size(87, 21);
            this.chkInitTrailView.TabIndex = 0;
            this.chkInitTrailView.Text = "View Log";
            this.chkInitTrailView.UseVisualStyleBackColor = true;
            this.chkInitTrailView.CheckedChanged += new System.EventHandler(this.chkInitTrailView_CheckedChanged);
            // 
            // gbAuditTrail
            // 
            this.gbAuditTrail.Controls.Add(this.chkAuditTrailImport);
            this.gbAuditTrail.Controls.Add(this.chkAuditTrailExport);
            this.gbAuditTrail.Controls.Add(this.chkAuditTrailDelete);
            this.gbAuditTrail.Controls.Add(this.chkAuditTrailView);
            this.gbAuditTrail.Location = new System.Drawing.Point(25, 20);
            this.gbAuditTrail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbAuditTrail.Name = "gbAuditTrail";
            this.gbAuditTrail.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbAuditTrail.Size = new System.Drawing.Size(685, 82);
            this.gbAuditTrail.TabIndex = 0;
            this.gbAuditTrail.TabStop = false;
            this.gbAuditTrail.Text = "Audit Trail";
            // 
            // chkAuditTrailImport
            // 
            this.chkAuditTrailImport.AutoSize = true;
            this.chkAuditTrailImport.Enabled = false;
            this.chkAuditTrailImport.Location = new System.Drawing.Point(233, 52);
            this.chkAuditTrailImport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAuditTrailImport.Name = "chkAuditTrailImport";
            this.chkAuditTrailImport.Size = new System.Drawing.Size(97, 21);
            this.chkAuditTrailImport.TabIndex = 3;
            this.chkAuditTrailImport.Text = "Import Log";
            this.chkAuditTrailImport.UseVisualStyleBackColor = true;
            // 
            // chkAuditTrailExport
            // 
            this.chkAuditTrailExport.AutoSize = true;
            this.chkAuditTrailExport.Enabled = false;
            this.chkAuditTrailExport.Location = new System.Drawing.Point(39, 52);
            this.chkAuditTrailExport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAuditTrailExport.Name = "chkAuditTrailExport";
            this.chkAuditTrailExport.Size = new System.Drawing.Size(98, 21);
            this.chkAuditTrailExport.TabIndex = 2;
            this.chkAuditTrailExport.Text = "Export Log";
            this.chkAuditTrailExport.UseVisualStyleBackColor = true;
            // 
            // chkAuditTrailDelete
            // 
            this.chkAuditTrailDelete.AutoSize = true;
            this.chkAuditTrailDelete.Enabled = false;
            this.chkAuditTrailDelete.Location = new System.Drawing.Point(233, 23);
            this.chkAuditTrailDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAuditTrailDelete.Name = "chkAuditTrailDelete";
            this.chkAuditTrailDelete.Size = new System.Drawing.Size(99, 21);
            this.chkAuditTrailDelete.TabIndex = 1;
            this.chkAuditTrailDelete.Text = "Delete Log";
            this.chkAuditTrailDelete.UseVisualStyleBackColor = true;
            // 
            // chkAuditTrailView
            // 
            this.chkAuditTrailView.AutoSize = true;
            this.chkAuditTrailView.Location = new System.Drawing.Point(39, 23);
            this.chkAuditTrailView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAuditTrailView.Name = "chkAuditTrailView";
            this.chkAuditTrailView.Size = new System.Drawing.Size(87, 21);
            this.chkAuditTrailView.TabIndex = 0;
            this.chkAuditTrailView.Text = "View Log";
            this.chkAuditTrailView.UseVisualStyleBackColor = true;
            this.chkAuditTrailView.CheckedChanged += new System.EventHandler(this.chkAuditTrailView_CheckedChanged);
            // 
            // gbData
            // 
            this.gbData.Controls.Add(this.chkDataView);
            this.gbData.Controls.Add(this.chkTemplateDef);
            this.gbData.Controls.Add(this.chkUpload);
            this.gbData.Controls.Add(this.chkUploadDef);
            this.gbData.Location = new System.Drawing.Point(25, 167);
            this.gbData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbData.Name = "gbData";
            this.gbData.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbData.Size = new System.Drawing.Size(1315, 55);
            this.gbData.TabIndex = 3;
            this.gbData.TabStop = false;
            this.gbData.Text = "Data";
            // 
            // chkDataView
            // 
            this.chkDataView.AutoSize = true;
            this.chkDataView.Location = new System.Drawing.Point(39, 22);
            this.chkDataView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkDataView.Name = "chkDataView";
            this.chkDataView.Size = new System.Drawing.Size(93, 21);
            this.chkDataView.TabIndex = 0;
            this.chkDataView.Text = "View Data";
            this.chkDataView.UseVisualStyleBackColor = true;
            this.chkDataView.CheckedChanged += new System.EventHandler(this.chkDataView_CheckedChanged);
            // 
            // chkTemplateDef
            // 
            this.chkTemplateDef.AutoSize = true;
            this.chkTemplateDef.Enabled = false;
            this.chkTemplateDef.Location = new System.Drawing.Point(233, 22);
            this.chkTemplateDef.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTemplateDef.Name = "chkTemplateDef";
            this.chkTemplateDef.Size = new System.Drawing.Size(152, 21);
            this.chkTemplateDef.TabIndex = 1;
            this.chkTemplateDef.Text = "Template Definition";
            this.chkTemplateDef.UseVisualStyleBackColor = true;
            // 
            // chkUpload
            // 
            this.chkUpload.AutoSize = true;
            this.chkUpload.Enabled = false;
            this.chkUpload.Location = new System.Drawing.Point(632, 22);
            this.chkUpload.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkUpload.Name = "chkUpload";
            this.chkUpload.Size = new System.Drawing.Size(75, 21);
            this.chkUpload.TabIndex = 3;
            this.chkUpload.Text = "Upload";
            this.chkUpload.UseVisualStyleBackColor = true;
            // 
            // chkUploadDef
            // 
            this.chkUploadDef.AutoSize = true;
            this.chkUploadDef.Enabled = false;
            this.chkUploadDef.Location = new System.Drawing.Point(439, 22);
            this.chkUploadDef.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkUploadDef.Name = "chkUploadDef";
            this.chkUploadDef.Size = new System.Drawing.Size(138, 21);
            this.chkUploadDef.TabIndex = 2;
            this.chkUploadDef.Text = "Upload Definition";
            this.chkUploadDef.UseVisualStyleBackColor = true;
            // 
            // gbUserMan
            // 
            this.gbUserMan.Controls.Add(this.chkUserManDelete);
            this.gbUserMan.Controls.Add(this.chkUserManEdit);
            this.gbUserMan.Controls.Add(this.chkUserManAdd);
            this.gbUserMan.Controls.Add(this.chkUserManView);
            this.gbUserMan.Location = new System.Drawing.Point(25, 228);
            this.gbUserMan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUserMan.Name = "gbUserMan";
            this.gbUserMan.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUserMan.Size = new System.Drawing.Size(1315, 55);
            this.gbUserMan.TabIndex = 4;
            this.gbUserMan.TabStop = false;
            this.gbUserMan.Text = "User Management";
            // 
            // chkUserManDelete
            // 
            this.chkUserManDelete.AutoSize = true;
            this.chkUserManDelete.Enabled = false;
            this.chkUserManDelete.Location = new System.Drawing.Point(632, 23);
            this.chkUserManDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkUserManDelete.Name = "chkUserManDelete";
            this.chkUserManDelete.Size = new System.Drawing.Size(105, 21);
            this.chkUserManDelete.TabIndex = 3;
            this.chkUserManDelete.Text = "Delete User";
            this.chkUserManDelete.UseVisualStyleBackColor = true;
            // 
            // chkUserManEdit
            // 
            this.chkUserManEdit.AutoSize = true;
            this.chkUserManEdit.Enabled = false;
            this.chkUserManEdit.Location = new System.Drawing.Point(439, 23);
            this.chkUserManEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkUserManEdit.Name = "chkUserManEdit";
            this.chkUserManEdit.Size = new System.Drawing.Size(88, 21);
            this.chkUserManEdit.TabIndex = 2;
            this.chkUserManEdit.Text = "Edit User";
            this.chkUserManEdit.UseVisualStyleBackColor = true;
            // 
            // chkUserManAdd
            // 
            this.chkUserManAdd.AutoSize = true;
            this.chkUserManAdd.Enabled = false;
            this.chkUserManAdd.Location = new System.Drawing.Point(233, 23);
            this.chkUserManAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkUserManAdd.Name = "chkUserManAdd";
            this.chkUserManAdd.Size = new System.Drawing.Size(89, 21);
            this.chkUserManAdd.TabIndex = 1;
            this.chkUserManAdd.Text = "Add User";
            this.chkUserManAdd.UseVisualStyleBackColor = true;
            // 
            // chkUserManView
            // 
            this.chkUserManView.AutoSize = true;
            this.chkUserManView.Location = new System.Drawing.Point(39, 23);
            this.chkUserManView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkUserManView.Name = "chkUserManView";
            this.chkUserManView.Size = new System.Drawing.Size(93, 21);
            this.chkUserManView.TabIndex = 0;
            this.chkUserManView.Text = "View User";
            this.chkUserManView.UseVisualStyleBackColor = true;
            this.chkUserManView.CheckedChanged += new System.EventHandler(this.chkUserManView_CheckedChanged);
            // 
            // tabProfile
            // 
            this.tabProfile.Controls.Add(this.groupBox2);
            this.tabProfile.Controls.Add(this.groupBox1);
            this.tabProfile.Controls.Add(this.gbProfile2);
            this.tabProfile.Controls.Add(this.gbProfile1);
            this.tabProfile.Location = new System.Drawing.Point(4, 25);
            this.tabProfile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabProfile.Name = "tabProfile";
            this.tabProfile.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabProfile.Size = new System.Drawing.Size(1377, 545);
            this.tabProfile.TabIndex = 1;
            this.tabProfile.Text = "Profile";
            this.tabProfile.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(919, 7);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(445, 55);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkAuditTrailWeb);
            this.groupBox1.Controls.Add(this.chkSoftwareTrailWeb);
            this.groupBox1.Location = new System.Drawing.Point(477, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(433, 55);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // chkAuditTrailWeb
            // 
            this.chkAuditTrailWeb.AutoSize = true;
            this.chkAuditTrailWeb.Location = new System.Drawing.Point(16, 23);
            this.chkAuditTrailWeb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAuditTrailWeb.Name = "chkAuditTrailWeb";
            this.chkAuditTrailWeb.Size = new System.Drawing.Size(127, 21);
            this.chkAuditTrailWeb.TabIndex = 0;
            this.chkAuditTrailWeb.Text = "Audit Trail Web";
            this.chkAuditTrailWeb.UseVisualStyleBackColor = true;
            // 
            // chkSoftwareTrailWeb
            // 
            this.chkSoftwareTrailWeb.AutoSize = true;
            this.chkSoftwareTrailWeb.Location = new System.Drawing.Point(227, 23);
            this.chkSoftwareTrailWeb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkSoftwareTrailWeb.Name = "chkSoftwareTrailWeb";
            this.chkSoftwareTrailWeb.Size = new System.Drawing.Size(179, 21);
            this.chkSoftwareTrailWeb.TabIndex = 1;
            this.chkSoftwareTrailWeb.Text = "Software Progress Web";
            this.chkSoftwareTrailWeb.UseVisualStyleBackColor = true;
            // 
            // gbProfile2
            // 
            this.gbProfile2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbProfile2.Controls.Add(this.gbPromoManagement);
            this.gbProfile2.Controls.Add(this.gbEMVManagement);
            this.gbProfile2.Controls.Add(this.gbRemoteDownload);
            this.gbProfile2.Controls.Add(this.gbEdcMonitoring);
            this.gbProfile2.Controls.Add(this.gbPinPad);
            this.gbProfile2.Controls.Add(this.gbInitialFlazz);
            this.gbProfile2.Controls.Add(this.gbGPRSMan);
            this.gbProfile2.Controls.Add(this.gbCardTypeManagement);
            this.gbProfile2.Controls.Add(this.gbTLEManagement);
            this.gbProfile2.Controls.Add(this.gbCard);
            this.gbProfile2.Controls.Add(this.gbKey);
            this.gbProfile2.Controls.Add(this.gbAID);
            this.gbProfile2.Controls.Add(this.gbCardRange);
            this.gbProfile2.Controls.Add(this.gbIssuer);
            this.gbProfile2.Controls.Add(this.gbAcq);
            this.gbProfile2.Controls.Add(this.gbTerminal);
            this.gbProfile2.Controls.Add(this.gbBankCode);
            this.gbProfile2.Location = new System.Drawing.Point(5, 70);
            this.gbProfile2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbProfile2.Name = "gbProfile2";
            this.gbProfile2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbProfile2.Size = new System.Drawing.Size(1360, 469);
            this.gbProfile2.TabIndex = 1;
            this.gbProfile2.TabStop = false;
            // 
            // gbPromoManagement
            // 
            this.gbPromoManagement.Controls.Add(this.chkPromoManagementDelete);
            this.gbPromoManagement.Controls.Add(this.chkPromoManagementEdit);
            this.gbPromoManagement.Controls.Add(this.chkPromoManagementAdd);
            this.gbPromoManagement.Controls.Add(this.chkPromoManagementView);
            this.gbPromoManagement.Location = new System.Drawing.Point(244, 310);
            this.gbPromoManagement.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbPromoManagement.Name = "gbPromoManagement";
            this.gbPromoManagement.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbPromoManagement.Size = new System.Drawing.Size(213, 142);
            this.gbPromoManagement.TabIndex = 12;
            this.gbPromoManagement.TabStop = false;
            // 
            // chkPromoManagementDelete
            // 
            this.chkPromoManagementDelete.AutoSize = true;
            this.chkPromoManagementDelete.Enabled = false;
            this.chkPromoManagementDelete.Location = new System.Drawing.Point(25, 108);
            this.chkPromoManagementDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPromoManagementDelete.Name = "chkPromoManagementDelete";
            this.chkPromoManagementDelete.Size = new System.Drawing.Size(71, 21);
            this.chkPromoManagementDelete.TabIndex = 3;
            this.chkPromoManagementDelete.Text = "Delete";
            this.chkPromoManagementDelete.UseVisualStyleBackColor = true;
            // 
            // chkPromoManagementEdit
            // 
            this.chkPromoManagementEdit.AutoSize = true;
            this.chkPromoManagementEdit.Enabled = false;
            this.chkPromoManagementEdit.Location = new System.Drawing.Point(25, 80);
            this.chkPromoManagementEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPromoManagementEdit.Name = "chkPromoManagementEdit";
            this.chkPromoManagementEdit.Size = new System.Drawing.Size(54, 21);
            this.chkPromoManagementEdit.TabIndex = 2;
            this.chkPromoManagementEdit.Text = "Edit";
            this.chkPromoManagementEdit.UseVisualStyleBackColor = true;
            // 
            // chkPromoManagementAdd
            // 
            this.chkPromoManagementAdd.AutoSize = true;
            this.chkPromoManagementAdd.Enabled = false;
            this.chkPromoManagementAdd.Location = new System.Drawing.Point(24, 52);
            this.chkPromoManagementAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPromoManagementAdd.Name = "chkPromoManagementAdd";
            this.chkPromoManagementAdd.Size = new System.Drawing.Size(55, 21);
            this.chkPromoManagementAdd.TabIndex = 1;
            this.chkPromoManagementAdd.Text = "Add";
            this.chkPromoManagementAdd.UseVisualStyleBackColor = true;
            // 
            // chkPromoManagementView
            // 
            this.chkPromoManagementView.AutoSize = true;
            this.chkPromoManagementView.Location = new System.Drawing.Point(8, 23);
            this.chkPromoManagementView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPromoManagementView.Name = "chkPromoManagementView";
            this.chkPromoManagementView.Size = new System.Drawing.Size(157, 21);
            this.chkPromoManagementView.TabIndex = 0;
            this.chkPromoManagementView.Text = "Promo Management";
            this.chkPromoManagementView.UseVisualStyleBackColor = true;
            this.chkPromoManagementView.CheckedChanged += new System.EventHandler(this.chkPromoManagementView_CheckedChanged);
            // 
            // gbEMVManagement
            // 
            this.gbEMVManagement.Controls.Add(this.chkEMVManagementDelete);
            this.gbEMVManagement.Controls.Add(this.chkEMVManagementEdit);
            this.gbEMVManagement.Controls.Add(this.chkEMVManagementAdd);
            this.gbEMVManagement.Controls.Add(this.chkEMVManagementView);
            this.gbEMVManagement.Location = new System.Drawing.Point(1139, 161);
            this.gbEMVManagement.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbEMVManagement.Name = "gbEMVManagement";
            this.gbEMVManagement.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbEMVManagement.Size = new System.Drawing.Size(213, 142);
            this.gbEMVManagement.TabIndex = 5;
            this.gbEMVManagement.TabStop = false;
            // 
            // chkEMVManagementDelete
            // 
            this.chkEMVManagementDelete.AutoSize = true;
            this.chkEMVManagementDelete.Enabled = false;
            this.chkEMVManagementDelete.Location = new System.Drawing.Point(23, 108);
            this.chkEMVManagementDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEMVManagementDelete.Name = "chkEMVManagementDelete";
            this.chkEMVManagementDelete.Size = new System.Drawing.Size(71, 21);
            this.chkEMVManagementDelete.TabIndex = 3;
            this.chkEMVManagementDelete.Text = "Delete";
            this.chkEMVManagementDelete.UseVisualStyleBackColor = true;
            // 
            // chkEMVManagementEdit
            // 
            this.chkEMVManagementEdit.AutoSize = true;
            this.chkEMVManagementEdit.Enabled = false;
            this.chkEMVManagementEdit.Location = new System.Drawing.Point(23, 80);
            this.chkEMVManagementEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEMVManagementEdit.Name = "chkEMVManagementEdit";
            this.chkEMVManagementEdit.Size = new System.Drawing.Size(54, 21);
            this.chkEMVManagementEdit.TabIndex = 2;
            this.chkEMVManagementEdit.Text = "Edit";
            this.chkEMVManagementEdit.UseVisualStyleBackColor = true;
            // 
            // chkEMVManagementAdd
            // 
            this.chkEMVManagementAdd.AutoSize = true;
            this.chkEMVManagementAdd.Enabled = false;
            this.chkEMVManagementAdd.Location = new System.Drawing.Point(23, 52);
            this.chkEMVManagementAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEMVManagementAdd.Name = "chkEMVManagementAdd";
            this.chkEMVManagementAdd.Size = new System.Drawing.Size(55, 21);
            this.chkEMVManagementAdd.TabIndex = 1;
            this.chkEMVManagementAdd.Text = "Add";
            this.chkEMVManagementAdd.UseVisualStyleBackColor = true;
            // 
            // chkEMVManagementView
            // 
            this.chkEMVManagementView.AutoSize = true;
            this.chkEMVManagementView.Location = new System.Drawing.Point(8, 23);
            this.chkEMVManagementView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEMVManagementView.Name = "chkEMVManagementView";
            this.chkEMVManagementView.Size = new System.Drawing.Size(145, 21);
            this.chkEMVManagementView.TabIndex = 0;
            this.chkEMVManagementView.Text = "EMV Management";
            this.chkEMVManagementView.UseVisualStyleBackColor = true;
            this.chkEMVManagementView.CheckedChanged += new System.EventHandler(this.chkEMVManagementView_CheckedChanged);
            // 
            // gbRemoteDownload
            // 
            this.gbRemoteDownload.Controls.Add(this.chkRemoteDownloadDelete);
            this.gbRemoteDownload.Controls.Add(this.chkRemoteDownloadEdit);
            this.gbRemoteDownload.Controls.Add(this.chkRemoteDownloadAdd);
            this.gbRemoteDownload.Controls.Add(this.chkRemoteDownloadView);
            this.gbRemoteDownload.Location = new System.Drawing.Point(696, 161);
            this.gbRemoteDownload.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRemoteDownload.Name = "gbRemoteDownload";
            this.gbRemoteDownload.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbRemoteDownload.Size = new System.Drawing.Size(213, 142);
            this.gbRemoteDownload.TabIndex = 4;
            this.gbRemoteDownload.TabStop = false;
            // 
            // chkRemoteDownloadDelete
            // 
            this.chkRemoteDownloadDelete.AutoSize = true;
            this.chkRemoteDownloadDelete.Enabled = false;
            this.chkRemoteDownloadDelete.Location = new System.Drawing.Point(23, 108);
            this.chkRemoteDownloadDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkRemoteDownloadDelete.Name = "chkRemoteDownloadDelete";
            this.chkRemoteDownloadDelete.Size = new System.Drawing.Size(71, 21);
            this.chkRemoteDownloadDelete.TabIndex = 3;
            this.chkRemoteDownloadDelete.Text = "Delete";
            this.chkRemoteDownloadDelete.UseVisualStyleBackColor = true;
            // 
            // chkRemoteDownloadEdit
            // 
            this.chkRemoteDownloadEdit.AutoSize = true;
            this.chkRemoteDownloadEdit.Enabled = false;
            this.chkRemoteDownloadEdit.Location = new System.Drawing.Point(23, 80);
            this.chkRemoteDownloadEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkRemoteDownloadEdit.Name = "chkRemoteDownloadEdit";
            this.chkRemoteDownloadEdit.Size = new System.Drawing.Size(54, 21);
            this.chkRemoteDownloadEdit.TabIndex = 2;
            this.chkRemoteDownloadEdit.Text = "Edit";
            this.chkRemoteDownloadEdit.UseVisualStyleBackColor = true;
            // 
            // chkRemoteDownloadAdd
            // 
            this.chkRemoteDownloadAdd.AutoSize = true;
            this.chkRemoteDownloadAdd.Enabled = false;
            this.chkRemoteDownloadAdd.Location = new System.Drawing.Point(23, 52);
            this.chkRemoteDownloadAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkRemoteDownloadAdd.Name = "chkRemoteDownloadAdd";
            this.chkRemoteDownloadAdd.Size = new System.Drawing.Size(55, 21);
            this.chkRemoteDownloadAdd.TabIndex = 1;
            this.chkRemoteDownloadAdd.Text = "Add";
            this.chkRemoteDownloadAdd.UseVisualStyleBackColor = true;
            // 
            // chkRemoteDownloadView
            // 
            this.chkRemoteDownloadView.AutoSize = true;
            this.chkRemoteDownloadView.Location = new System.Drawing.Point(8, 23);
            this.chkRemoteDownloadView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkRemoteDownloadView.Name = "chkRemoteDownloadView";
            this.chkRemoteDownloadView.Size = new System.Drawing.Size(145, 21);
            this.chkRemoteDownloadView.TabIndex = 0;
            this.chkRemoteDownloadView.Text = "Remote Download";
            this.chkRemoteDownloadView.UseVisualStyleBackColor = true;
            this.chkRemoteDownloadView.CheckedChanged += new System.EventHandler(this.chkRD_CheckedChanged);
            // 
            // gbEdcMonitoring
            // 
            this.gbEdcMonitoring.Controls.Add(this.chkEdcMonitorEdit);
            this.gbEdcMonitoring.Controls.Add(this.chkEdcMonitorView);
            this.gbEdcMonitoring.Controls.Add(this.chkEdcMonitorAdd);
            this.gbEdcMonitoring.Controls.Add(this.chkEdcMonitorDelete);
            this.gbEdcMonitoring.Location = new System.Drawing.Point(696, 310);
            this.gbEdcMonitoring.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbEdcMonitoring.Name = "gbEdcMonitoring";
            this.gbEdcMonitoring.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbEdcMonitoring.Size = new System.Drawing.Size(213, 142);
            this.gbEdcMonitoring.TabIndex = 9;
            this.gbEdcMonitoring.TabStop = false;
            // 
            // chkEdcMonitorEdit
            // 
            this.chkEdcMonitorEdit.AutoSize = true;
            this.chkEdcMonitorEdit.Location = new System.Drawing.Point(28, 80);
            this.chkEdcMonitorEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEdcMonitorEdit.Name = "chkEdcMonitorEdit";
            this.chkEdcMonitorEdit.Size = new System.Drawing.Size(54, 21);
            this.chkEdcMonitorEdit.TabIndex = 8;
            this.chkEdcMonitorEdit.Text = "Edit";
            this.chkEdcMonitorEdit.UseVisualStyleBackColor = true;
            // 
            // chkEdcMonitorView
            // 
            this.chkEdcMonitorView.AutoSize = true;
            this.chkEdcMonitorView.Location = new System.Drawing.Point(8, 23);
            this.chkEdcMonitorView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEdcMonitorView.Name = "chkEdcMonitorView";
            this.chkEdcMonitorView.Size = new System.Drawing.Size(105, 21);
            this.chkEdcMonitorView.TabIndex = 7;
            this.chkEdcMonitorView.Text = "Edc Monitor";
            this.chkEdcMonitorView.UseVisualStyleBackColor = true;
            // 
            // chkEdcMonitorAdd
            // 
            this.chkEdcMonitorAdd.AutoSize = true;
            this.chkEdcMonitorAdd.Location = new System.Drawing.Point(27, 52);
            this.chkEdcMonitorAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEdcMonitorAdd.Name = "chkEdcMonitorAdd";
            this.chkEdcMonitorAdd.Size = new System.Drawing.Size(55, 21);
            this.chkEdcMonitorAdd.TabIndex = 4;
            this.chkEdcMonitorAdd.Text = "Add";
            this.chkEdcMonitorAdd.UseVisualStyleBackColor = true;
            // 
            // chkEdcMonitorDelete
            // 
            this.chkEdcMonitorDelete.AutoSize = true;
            this.chkEdcMonitorDelete.Location = new System.Drawing.Point(28, 108);
            this.chkEdcMonitorDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEdcMonitorDelete.Name = "chkEdcMonitorDelete";
            this.chkEdcMonitorDelete.Size = new System.Drawing.Size(71, 21);
            this.chkEdcMonitorDelete.TabIndex = 6;
            this.chkEdcMonitorDelete.Text = "Delete";
            this.chkEdcMonitorDelete.UseVisualStyleBackColor = true;
            // 
            // gbPinPad
            // 
            this.gbPinPad.Controls.Add(this.chkPinPadDelete);
            this.gbPinPad.Controls.Add(this.chkPinPadedit);
            this.gbPinPad.Controls.Add(this.chkPinPadAdd);
            this.gbPinPad.Controls.Add(this.chkPinPadView);
            this.gbPinPad.Location = new System.Drawing.Point(917, 161);
            this.gbPinPad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbPinPad.Name = "gbPinPad";
            this.gbPinPad.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbPinPad.Size = new System.Drawing.Size(213, 142);
            this.gbPinPad.TabIndex = 11;
            this.gbPinPad.TabStop = false;
            // 
            // chkPinPadDelete
            // 
            this.chkPinPadDelete.AutoSize = true;
            this.chkPinPadDelete.Enabled = false;
            this.chkPinPadDelete.Location = new System.Drawing.Point(31, 108);
            this.chkPinPadDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPinPadDelete.Name = "chkPinPadDelete";
            this.chkPinPadDelete.Size = new System.Drawing.Size(71, 21);
            this.chkPinPadDelete.TabIndex = 3;
            this.chkPinPadDelete.Text = "Delete";
            this.chkPinPadDelete.UseVisualStyleBackColor = true;
            // 
            // chkPinPadedit
            // 
            this.chkPinPadedit.AutoSize = true;
            this.chkPinPadedit.Enabled = false;
            this.chkPinPadedit.Location = new System.Drawing.Point(31, 80);
            this.chkPinPadedit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPinPadedit.Name = "chkPinPadedit";
            this.chkPinPadedit.Size = new System.Drawing.Size(54, 21);
            this.chkPinPadedit.TabIndex = 2;
            this.chkPinPadedit.Text = "Edit";
            this.chkPinPadedit.UseVisualStyleBackColor = true;
            // 
            // chkPinPadAdd
            // 
            this.chkPinPadAdd.AutoSize = true;
            this.chkPinPadAdd.Enabled = false;
            this.chkPinPadAdd.Location = new System.Drawing.Point(31, 52);
            this.chkPinPadAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPinPadAdd.Name = "chkPinPadAdd";
            this.chkPinPadAdd.Size = new System.Drawing.Size(55, 21);
            this.chkPinPadAdd.TabIndex = 1;
            this.chkPinPadAdd.Text = "Add";
            this.chkPinPadAdd.UseVisualStyleBackColor = true;
            // 
            // chkPinPadView
            // 
            this.chkPinPadView.AutoSize = true;
            this.chkPinPadView.Location = new System.Drawing.Point(8, 23);
            this.chkPinPadView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPinPadView.Name = "chkPinPadView";
            this.chkPinPadView.Size = new System.Drawing.Size(161, 21);
            this.chkPinPadView.TabIndex = 0;
            this.chkPinPadView.Text = "PinPad Management";
            this.chkPinPadView.UseVisualStyleBackColor = true;
            this.chkPinPadView.CheckedChanged += new System.EventHandler(this.chkPinPadView_CheckedChanged);
            // 
            // gbInitialFlazz
            // 
            this.gbInitialFlazz.Controls.Add(this.chkInitialFlazzDelete);
            this.gbInitialFlazz.Controls.Add(this.chkInitialFlazzEdit);
            this.gbInitialFlazz.Controls.Add(this.chkInitialFlazzAdd);
            this.gbInitialFlazz.Controls.Add(this.chkInitialFlazzView);
            this.gbInitialFlazz.Location = new System.Drawing.Point(16, 310);
            this.gbInitialFlazz.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbInitialFlazz.Name = "gbInitialFlazz";
            this.gbInitialFlazz.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbInitialFlazz.Size = new System.Drawing.Size(213, 142);
            this.gbInitialFlazz.TabIndex = 10;
            this.gbInitialFlazz.TabStop = false;
            // 
            // chkInitialFlazzDelete
            // 
            this.chkInitialFlazzDelete.AutoSize = true;
            this.chkInitialFlazzDelete.Location = new System.Drawing.Point(31, 108);
            this.chkInitialFlazzDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitialFlazzDelete.Name = "chkInitialFlazzDelete";
            this.chkInitialFlazzDelete.Size = new System.Drawing.Size(71, 21);
            this.chkInitialFlazzDelete.TabIndex = 3;
            this.chkInitialFlazzDelete.Text = "Delete";
            this.chkInitialFlazzDelete.UseVisualStyleBackColor = true;
            // 
            // chkInitialFlazzEdit
            // 
            this.chkInitialFlazzEdit.AutoSize = true;
            this.chkInitialFlazzEdit.Location = new System.Drawing.Point(31, 80);
            this.chkInitialFlazzEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitialFlazzEdit.Name = "chkInitialFlazzEdit";
            this.chkInitialFlazzEdit.Size = new System.Drawing.Size(54, 21);
            this.chkInitialFlazzEdit.TabIndex = 2;
            this.chkInitialFlazzEdit.Text = "Edit";
            this.chkInitialFlazzEdit.UseVisualStyleBackColor = true;
            // 
            // chkInitialFlazzAdd
            // 
            this.chkInitialFlazzAdd.AutoSize = true;
            this.chkInitialFlazzAdd.Location = new System.Drawing.Point(31, 52);
            this.chkInitialFlazzAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitialFlazzAdd.Name = "chkInitialFlazzAdd";
            this.chkInitialFlazzAdd.Size = new System.Drawing.Size(55, 21);
            this.chkInitialFlazzAdd.TabIndex = 1;
            this.chkInitialFlazzAdd.Text = "Add";
            this.chkInitialFlazzAdd.UseVisualStyleBackColor = true;
            // 
            // chkInitialFlazzView
            // 
            this.chkInitialFlazzView.AutoSize = true;
            this.chkInitialFlazzView.Location = new System.Drawing.Point(8, 23);
            this.chkInitialFlazzView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitialFlazzView.Name = "chkInitialFlazzView";
            this.chkInitialFlazzView.Size = new System.Drawing.Size(185, 21);
            this.chkInitialFlazzView.TabIndex = 0;
            this.chkInitialFlazzView.Text = "Initial Flazz Management";
            this.chkInitialFlazzView.UseVisualStyleBackColor = true;
            this.chkInitialFlazzView.CheckedChanged += new System.EventHandler(this.chkInitialFlazzView_CheckedChanged);
            // 
            // gbGPRSMan
            // 
            this.gbGPRSMan.Controls.Add(this.chkGPRSDel);
            this.gbGPRSMan.Controls.Add(this.chkGPRSEdit);
            this.gbGPRSMan.Controls.Add(this.chkGPRSAdd);
            this.gbGPRSMan.Controls.Add(this.chkGPRSView);
            this.gbGPRSMan.Location = new System.Drawing.Point(915, 12);
            this.gbGPRSMan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbGPRSMan.Name = "gbGPRSMan";
            this.gbGPRSMan.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbGPRSMan.Size = new System.Drawing.Size(213, 142);
            this.gbGPRSMan.TabIndex = 9;
            this.gbGPRSMan.TabStop = false;
            // 
            // chkGPRSDel
            // 
            this.chkGPRSDel.AutoSize = true;
            this.chkGPRSDel.Enabled = false;
            this.chkGPRSDel.Location = new System.Drawing.Point(31, 108);
            this.chkGPRSDel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkGPRSDel.Name = "chkGPRSDel";
            this.chkGPRSDel.Size = new System.Drawing.Size(71, 21);
            this.chkGPRSDel.TabIndex = 3;
            this.chkGPRSDel.Text = "Delete";
            this.chkGPRSDel.UseVisualStyleBackColor = true;
            // 
            // chkGPRSEdit
            // 
            this.chkGPRSEdit.AutoSize = true;
            this.chkGPRSEdit.Enabled = false;
            this.chkGPRSEdit.Location = new System.Drawing.Point(31, 80);
            this.chkGPRSEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkGPRSEdit.Name = "chkGPRSEdit";
            this.chkGPRSEdit.Size = new System.Drawing.Size(54, 21);
            this.chkGPRSEdit.TabIndex = 2;
            this.chkGPRSEdit.Text = "Edit";
            this.chkGPRSEdit.UseVisualStyleBackColor = true;
            // 
            // chkGPRSAdd
            // 
            this.chkGPRSAdd.AutoSize = true;
            this.chkGPRSAdd.Enabled = false;
            this.chkGPRSAdd.Location = new System.Drawing.Point(31, 52);
            this.chkGPRSAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkGPRSAdd.Name = "chkGPRSAdd";
            this.chkGPRSAdd.Size = new System.Drawing.Size(55, 21);
            this.chkGPRSAdd.TabIndex = 1;
            this.chkGPRSAdd.Text = "Add";
            this.chkGPRSAdd.UseVisualStyleBackColor = true;
            // 
            // chkGPRSView
            // 
            this.chkGPRSView.AutoSize = true;
            this.chkGPRSView.Location = new System.Drawing.Point(8, 23);
            this.chkGPRSView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkGPRSView.Name = "chkGPRSView";
            this.chkGPRSView.Size = new System.Drawing.Size(155, 21);
            this.chkGPRSView.TabIndex = 0;
            this.chkGPRSView.Text = "GPRS Management";
            this.chkGPRSView.UseVisualStyleBackColor = true;
            this.chkGPRSView.CheckedChanged += new System.EventHandler(this.chkGPRSView_CheckedChanged);
            // 
            // gbCardTypeManagement
            // 
            this.gbCardTypeManagement.Controls.Add(this.chkCardTypeDelete);
            this.gbCardTypeManagement.Controls.Add(this.chkCardTypeEdit);
            this.gbCardTypeManagement.Controls.Add(this.chkCardTypeAdd);
            this.gbCardTypeManagement.Controls.Add(this.chkCardTypeManView);
            this.gbCardTypeManagement.Location = new System.Drawing.Point(465, 310);
            this.gbCardTypeManagement.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbCardTypeManagement.Name = "gbCardTypeManagement";
            this.gbCardTypeManagement.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbCardTypeManagement.Size = new System.Drawing.Size(213, 142);
            this.gbCardTypeManagement.TabIndex = 8;
            this.gbCardTypeManagement.TabStop = false;
            // 
            // chkCardTypeDelete
            // 
            this.chkCardTypeDelete.AutoSize = true;
            this.chkCardTypeDelete.Enabled = false;
            this.chkCardTypeDelete.Location = new System.Drawing.Point(31, 108);
            this.chkCardTypeDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardTypeDelete.Name = "chkCardTypeDelete";
            this.chkCardTypeDelete.Size = new System.Drawing.Size(71, 21);
            this.chkCardTypeDelete.TabIndex = 3;
            this.chkCardTypeDelete.Text = "Delete";
            this.chkCardTypeDelete.UseVisualStyleBackColor = true;
            // 
            // chkCardTypeEdit
            // 
            this.chkCardTypeEdit.AutoSize = true;
            this.chkCardTypeEdit.Enabled = false;
            this.chkCardTypeEdit.Location = new System.Drawing.Point(31, 80);
            this.chkCardTypeEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardTypeEdit.Name = "chkCardTypeEdit";
            this.chkCardTypeEdit.Size = new System.Drawing.Size(54, 21);
            this.chkCardTypeEdit.TabIndex = 2;
            this.chkCardTypeEdit.Text = "Edit";
            this.chkCardTypeEdit.UseVisualStyleBackColor = true;
            // 
            // chkCardTypeAdd
            // 
            this.chkCardTypeAdd.AutoSize = true;
            this.chkCardTypeAdd.Enabled = false;
            this.chkCardTypeAdd.Location = new System.Drawing.Point(31, 52);
            this.chkCardTypeAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardTypeAdd.Name = "chkCardTypeAdd";
            this.chkCardTypeAdd.Size = new System.Drawing.Size(55, 21);
            this.chkCardTypeAdd.TabIndex = 1;
            this.chkCardTypeAdd.Text = "Add";
            this.chkCardTypeAdd.UseVisualStyleBackColor = true;
            // 
            // chkCardTypeManView
            // 
            this.chkCardTypeManView.AutoSize = true;
            this.chkCardTypeManView.Location = new System.Drawing.Point(8, 23);
            this.chkCardTypeManView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardTypeManView.Name = "chkCardTypeManView";
            this.chkCardTypeManView.Size = new System.Drawing.Size(182, 21);
            this.chkCardTypeManView.TabIndex = 0;
            this.chkCardTypeManView.Text = "Card Type Management";
            this.chkCardTypeManView.UseVisualStyleBackColor = true;
            this.chkCardTypeManView.CheckedChanged += new System.EventHandler(this.chkCardTypeManView_CheckedChanged);
            // 
            // gbTLEManagement
            // 
            this.gbTLEManagement.Controls.Add(this.chkTLEDelete);
            this.gbTLEManagement.Controls.Add(this.chkTLEEdit);
            this.gbTLEManagement.Controls.Add(this.chkTLEAdd);
            this.gbTLEManagement.Controls.Add(this.chkTLEView);
            this.gbTLEManagement.Location = new System.Drawing.Point(1139, 12);
            this.gbTLEManagement.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbTLEManagement.Name = "gbTLEManagement";
            this.gbTLEManagement.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbTLEManagement.Size = new System.Drawing.Size(213, 142);
            this.gbTLEManagement.TabIndex = 3;
            this.gbTLEManagement.TabStop = false;
            // 
            // chkTLEDelete
            // 
            this.chkTLEDelete.AutoSize = true;
            this.chkTLEDelete.Enabled = false;
            this.chkTLEDelete.Location = new System.Drawing.Point(31, 108);
            this.chkTLEDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTLEDelete.Name = "chkTLEDelete";
            this.chkTLEDelete.Size = new System.Drawing.Size(71, 21);
            this.chkTLEDelete.TabIndex = 3;
            this.chkTLEDelete.Text = "Delete";
            this.chkTLEDelete.UseVisualStyleBackColor = true;
            // 
            // chkTLEEdit
            // 
            this.chkTLEEdit.AutoSize = true;
            this.chkTLEEdit.Enabled = false;
            this.chkTLEEdit.Location = new System.Drawing.Point(31, 80);
            this.chkTLEEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTLEEdit.Name = "chkTLEEdit";
            this.chkTLEEdit.Size = new System.Drawing.Size(54, 21);
            this.chkTLEEdit.TabIndex = 2;
            this.chkTLEEdit.Text = "Edit";
            this.chkTLEEdit.UseVisualStyleBackColor = true;
            // 
            // chkTLEAdd
            // 
            this.chkTLEAdd.AutoSize = true;
            this.chkTLEAdd.Enabled = false;
            this.chkTLEAdd.Location = new System.Drawing.Point(31, 52);
            this.chkTLEAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTLEAdd.Name = "chkTLEAdd";
            this.chkTLEAdd.Size = new System.Drawing.Size(55, 21);
            this.chkTLEAdd.TabIndex = 1;
            this.chkTLEAdd.Text = "Add";
            this.chkTLEAdd.UseVisualStyleBackColor = true;
            // 
            // chkTLEView
            // 
            this.chkTLEView.AutoSize = true;
            this.chkTLEView.Location = new System.Drawing.Point(8, 23);
            this.chkTLEView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTLEView.Name = "chkTLEView";
            this.chkTLEView.Size = new System.Drawing.Size(60, 21);
            this.chkTLEView.TabIndex = 0;
            this.chkTLEView.Text = "TLE ";
            this.chkTLEView.UseVisualStyleBackColor = true;
            this.chkTLEView.CheckedChanged += new System.EventHandler(this.chkTLEView_CheckedChanged);
            // 
            // gbCard
            // 
            this.gbCard.Controls.Add(this.chkCardDelete);
            this.gbCard.Controls.Add(this.chkCardAdd);
            this.gbCard.Controls.Add(this.chkCardView);
            this.gbCard.Location = new System.Drawing.Point(693, 12);
            this.gbCard.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbCard.Name = "gbCard";
            this.gbCard.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbCard.Size = new System.Drawing.Size(213, 142);
            this.gbCard.TabIndex = 7;
            this.gbCard.TabStop = false;
            // 
            // chkCardDelete
            // 
            this.chkCardDelete.AutoSize = true;
            this.chkCardDelete.Enabled = false;
            this.chkCardDelete.Location = new System.Drawing.Point(31, 68);
            this.chkCardDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardDelete.Name = "chkCardDelete";
            this.chkCardDelete.Size = new System.Drawing.Size(71, 21);
            this.chkCardDelete.TabIndex = 2;
            this.chkCardDelete.Text = "Delete";
            this.chkCardDelete.UseVisualStyleBackColor = true;
            // 
            // chkCardAdd
            // 
            this.chkCardAdd.AutoSize = true;
            this.chkCardAdd.Enabled = false;
            this.chkCardAdd.Location = new System.Drawing.Point(31, 44);
            this.chkCardAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardAdd.Name = "chkCardAdd";
            this.chkCardAdd.Size = new System.Drawing.Size(55, 21);
            this.chkCardAdd.TabIndex = 1;
            this.chkCardAdd.Text = "Add";
            this.chkCardAdd.UseVisualStyleBackColor = true;
            // 
            // chkCardView
            // 
            this.chkCardView.AutoSize = true;
            this.chkCardView.Location = new System.Drawing.Point(9, 23);
            this.chkCardView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardView.Name = "chkCardView";
            this.chkCardView.Size = new System.Drawing.Size(60, 21);
            this.chkCardView.TabIndex = 0;
            this.chkCardView.Text = "Card";
            this.chkCardView.UseVisualStyleBackColor = true;
            this.chkCardView.CheckedChanged += new System.EventHandler(this.chkCardView_CheckedChanged);
            // 
            // gbKey
            // 
            this.gbKey.Controls.Add(this.chkKeyDelete);
            this.gbKey.Controls.Add(this.chkKeyEdit);
            this.gbKey.Controls.Add(this.chkKeyAdd);
            this.gbKey.Controls.Add(this.chkKeyView);
            this.gbKey.Location = new System.Drawing.Point(244, 161);
            this.gbKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbKey.Name = "gbKey";
            this.gbKey.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbKey.Size = new System.Drawing.Size(213, 142);
            this.gbKey.TabIndex = 1;
            this.gbKey.TabStop = false;
            // 
            // chkKeyDelete
            // 
            this.chkKeyDelete.AutoSize = true;
            this.chkKeyDelete.Enabled = false;
            this.chkKeyDelete.Location = new System.Drawing.Point(37, 108);
            this.chkKeyDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkKeyDelete.Name = "chkKeyDelete";
            this.chkKeyDelete.Size = new System.Drawing.Size(71, 21);
            this.chkKeyDelete.TabIndex = 3;
            this.chkKeyDelete.Text = "Delete";
            this.chkKeyDelete.UseVisualStyleBackColor = true;
            // 
            // chkKeyEdit
            // 
            this.chkKeyEdit.AutoSize = true;
            this.chkKeyEdit.Enabled = false;
            this.chkKeyEdit.Location = new System.Drawing.Point(37, 80);
            this.chkKeyEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkKeyEdit.Name = "chkKeyEdit";
            this.chkKeyEdit.Size = new System.Drawing.Size(54, 21);
            this.chkKeyEdit.TabIndex = 2;
            this.chkKeyEdit.Text = "Edit";
            this.chkKeyEdit.UseVisualStyleBackColor = true;
            // 
            // chkKeyAdd
            // 
            this.chkKeyAdd.AutoSize = true;
            this.chkKeyAdd.Enabled = false;
            this.chkKeyAdd.Location = new System.Drawing.Point(37, 52);
            this.chkKeyAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkKeyAdd.Name = "chkKeyAdd";
            this.chkKeyAdd.Size = new System.Drawing.Size(55, 21);
            this.chkKeyAdd.TabIndex = 1;
            this.chkKeyAdd.Text = "Add";
            this.chkKeyAdd.UseVisualStyleBackColor = true;
            // 
            // chkKeyView
            // 
            this.chkKeyView.AutoSize = true;
            this.chkKeyView.Location = new System.Drawing.Point(8, 23);
            this.chkKeyView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkKeyView.Name = "chkKeyView";
            this.chkKeyView.Size = new System.Drawing.Size(182, 21);
            this.chkKeyView.TabIndex = 0;
            this.chkKeyView.Text = "Public Key Management";
            this.chkKeyView.UseVisualStyleBackColor = true;
            this.chkKeyView.CheckedChanged += new System.EventHandler(this.chkKeyView_CheckedChanged);
            // 
            // gbAID
            // 
            this.gbAID.Controls.Add(this.chkAIDDelete);
            this.gbAID.Controls.Add(this.chkAIDEdit);
            this.gbAID.Controls.Add(this.chkAIDAdd);
            this.gbAID.Controls.Add(this.chkAIDView);
            this.gbAID.Location = new System.Drawing.Point(16, 161);
            this.gbAID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbAID.Name = "gbAID";
            this.gbAID.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbAID.Size = new System.Drawing.Size(213, 142);
            this.gbAID.TabIndex = 0;
            this.gbAID.TabStop = false;
            // 
            // chkAIDDelete
            // 
            this.chkAIDDelete.AutoSize = true;
            this.chkAIDDelete.Enabled = false;
            this.chkAIDDelete.Location = new System.Drawing.Point(37, 108);
            this.chkAIDDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAIDDelete.Name = "chkAIDDelete";
            this.chkAIDDelete.Size = new System.Drawing.Size(71, 21);
            this.chkAIDDelete.TabIndex = 3;
            this.chkAIDDelete.Text = "Delete";
            this.chkAIDDelete.UseVisualStyleBackColor = true;
            // 
            // chkAIDEdit
            // 
            this.chkAIDEdit.AutoSize = true;
            this.chkAIDEdit.Enabled = false;
            this.chkAIDEdit.Location = new System.Drawing.Point(37, 80);
            this.chkAIDEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAIDEdit.Name = "chkAIDEdit";
            this.chkAIDEdit.Size = new System.Drawing.Size(54, 21);
            this.chkAIDEdit.TabIndex = 2;
            this.chkAIDEdit.Text = "Edit";
            this.chkAIDEdit.UseVisualStyleBackColor = true;
            // 
            // chkAIDAdd
            // 
            this.chkAIDAdd.AutoSize = true;
            this.chkAIDAdd.Enabled = false;
            this.chkAIDAdd.Location = new System.Drawing.Point(37, 52);
            this.chkAIDAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAIDAdd.Name = "chkAIDAdd";
            this.chkAIDAdd.Size = new System.Drawing.Size(55, 21);
            this.chkAIDAdd.TabIndex = 1;
            this.chkAIDAdd.Text = "Add";
            this.chkAIDAdd.UseVisualStyleBackColor = true;
            // 
            // chkAIDView
            // 
            this.chkAIDView.AutoSize = true;
            this.chkAIDView.Location = new System.Drawing.Point(8, 23);
            this.chkAIDView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAIDView.Name = "chkAIDView";
            this.chkAIDView.Size = new System.Drawing.Size(52, 21);
            this.chkAIDView.TabIndex = 0;
            this.chkAIDView.Text = "AID";
            this.chkAIDView.UseVisualStyleBackColor = true;
            this.chkAIDView.CheckedChanged += new System.EventHandler(this.chkAIDView_CheckedChanged);
            // 
            // gbCardRange
            // 
            this.gbCardRange.Controls.Add(this.chkCardRangeDelete);
            this.gbCardRange.Controls.Add(this.chkCardRangeEdit);
            this.gbCardRange.Controls.Add(this.chkCardRangeAdd);
            this.gbCardRange.Controls.Add(this.chkCardRangeView);
            this.gbCardRange.Location = new System.Drawing.Point(465, 161);
            this.gbCardRange.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbCardRange.Name = "gbCardRange";
            this.gbCardRange.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbCardRange.Size = new System.Drawing.Size(213, 142);
            this.gbCardRange.TabIndex = 2;
            this.gbCardRange.TabStop = false;
            // 
            // chkCardRangeDelete
            // 
            this.chkCardRangeDelete.AutoSize = true;
            this.chkCardRangeDelete.Enabled = false;
            this.chkCardRangeDelete.Location = new System.Drawing.Point(31, 108);
            this.chkCardRangeDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardRangeDelete.Name = "chkCardRangeDelete";
            this.chkCardRangeDelete.Size = new System.Drawing.Size(71, 21);
            this.chkCardRangeDelete.TabIndex = 3;
            this.chkCardRangeDelete.Text = "Delete";
            this.chkCardRangeDelete.UseVisualStyleBackColor = true;
            // 
            // chkCardRangeEdit
            // 
            this.chkCardRangeEdit.AutoSize = true;
            this.chkCardRangeEdit.Enabled = false;
            this.chkCardRangeEdit.Location = new System.Drawing.Point(31, 80);
            this.chkCardRangeEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardRangeEdit.Name = "chkCardRangeEdit";
            this.chkCardRangeEdit.Size = new System.Drawing.Size(54, 21);
            this.chkCardRangeEdit.TabIndex = 2;
            this.chkCardRangeEdit.Text = "Edit";
            this.chkCardRangeEdit.UseVisualStyleBackColor = true;
            // 
            // chkCardRangeAdd
            // 
            this.chkCardRangeAdd.AutoSize = true;
            this.chkCardRangeAdd.Enabled = false;
            this.chkCardRangeAdd.Location = new System.Drawing.Point(31, 52);
            this.chkCardRangeAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardRangeAdd.Name = "chkCardRangeAdd";
            this.chkCardRangeAdd.Size = new System.Drawing.Size(55, 21);
            this.chkCardRangeAdd.TabIndex = 1;
            this.chkCardRangeAdd.Text = "Add";
            this.chkCardRangeAdd.UseVisualStyleBackColor = true;
            // 
            // chkCardRangeView
            // 
            this.chkCardRangeView.AutoSize = true;
            this.chkCardRangeView.Location = new System.Drawing.Point(8, 23);
            this.chkCardRangeView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCardRangeView.Name = "chkCardRangeView";
            this.chkCardRangeView.Size = new System.Drawing.Size(192, 21);
            this.chkCardRangeView.TabIndex = 0;
            this.chkCardRangeView.Text = "Card Range Management";
            this.chkCardRangeView.UseVisualStyleBackColor = true;
            this.chkCardRangeView.CheckedChanged += new System.EventHandler(this.chkCardRangeView_CheckedChanged);
            // 
            // gbIssuer
            // 
            this.gbIssuer.Controls.Add(this.chkIssuerDelete);
            this.gbIssuer.Controls.Add(this.chkIssuerEdit);
            this.gbIssuer.Controls.Add(this.chkIssuerAdd);
            this.gbIssuer.Controls.Add(this.chkIssuerView);
            this.gbIssuer.Location = new System.Drawing.Point(465, 12);
            this.gbIssuer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbIssuer.Name = "gbIssuer";
            this.gbIssuer.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbIssuer.Size = new System.Drawing.Size(213, 142);
            this.gbIssuer.TabIndex = 6;
            this.gbIssuer.TabStop = false;
            // 
            // chkIssuerDelete
            // 
            this.chkIssuerDelete.AutoSize = true;
            this.chkIssuerDelete.Enabled = false;
            this.chkIssuerDelete.Location = new System.Drawing.Point(37, 108);
            this.chkIssuerDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkIssuerDelete.Name = "chkIssuerDelete";
            this.chkIssuerDelete.Size = new System.Drawing.Size(71, 21);
            this.chkIssuerDelete.TabIndex = 3;
            this.chkIssuerDelete.Text = "Delete";
            this.chkIssuerDelete.UseVisualStyleBackColor = true;
            // 
            // chkIssuerEdit
            // 
            this.chkIssuerEdit.AutoSize = true;
            this.chkIssuerEdit.Enabled = false;
            this.chkIssuerEdit.Location = new System.Drawing.Point(37, 80);
            this.chkIssuerEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkIssuerEdit.Name = "chkIssuerEdit";
            this.chkIssuerEdit.Size = new System.Drawing.Size(54, 21);
            this.chkIssuerEdit.TabIndex = 2;
            this.chkIssuerEdit.Text = "Edit";
            this.chkIssuerEdit.UseVisualStyleBackColor = true;
            // 
            // chkIssuerAdd
            // 
            this.chkIssuerAdd.AutoSize = true;
            this.chkIssuerAdd.Enabled = false;
            this.chkIssuerAdd.Location = new System.Drawing.Point(37, 52);
            this.chkIssuerAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkIssuerAdd.Name = "chkIssuerAdd";
            this.chkIssuerAdd.Size = new System.Drawing.Size(55, 21);
            this.chkIssuerAdd.TabIndex = 1;
            this.chkIssuerAdd.Text = "Add";
            this.chkIssuerAdd.UseVisualStyleBackColor = true;
            // 
            // chkIssuerView
            // 
            this.chkIssuerView.AutoSize = true;
            this.chkIssuerView.Location = new System.Drawing.Point(8, 23);
            this.chkIssuerView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkIssuerView.Name = "chkIssuerView";
            this.chkIssuerView.Size = new System.Drawing.Size(68, 21);
            this.chkIssuerView.TabIndex = 0;
            this.chkIssuerView.Text = "Issuer";
            this.chkIssuerView.UseVisualStyleBackColor = true;
            this.chkIssuerView.CheckedChanged += new System.EventHandler(this.chkIssuerView_CheckedChanged);
            // 
            // gbAcq
            // 
            this.gbAcq.Controls.Add(this.chkAcqDelete);
            this.gbAcq.Controls.Add(this.chkAcqEdit);
            this.gbAcq.Controls.Add(this.chkAcqAdd);
            this.gbAcq.Controls.Add(this.chkAcqView);
            this.gbAcq.Location = new System.Drawing.Point(244, 12);
            this.gbAcq.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbAcq.Name = "gbAcq";
            this.gbAcq.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbAcq.Size = new System.Drawing.Size(213, 142);
            this.gbAcq.TabIndex = 5;
            this.gbAcq.TabStop = false;
            // 
            // chkAcqDelete
            // 
            this.chkAcqDelete.AutoSize = true;
            this.chkAcqDelete.Enabled = false;
            this.chkAcqDelete.Location = new System.Drawing.Point(37, 108);
            this.chkAcqDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAcqDelete.Name = "chkAcqDelete";
            this.chkAcqDelete.Size = new System.Drawing.Size(71, 21);
            this.chkAcqDelete.TabIndex = 3;
            this.chkAcqDelete.Text = "Delete";
            this.chkAcqDelete.UseVisualStyleBackColor = true;
            // 
            // chkAcqEdit
            // 
            this.chkAcqEdit.AutoSize = true;
            this.chkAcqEdit.Enabled = false;
            this.chkAcqEdit.Location = new System.Drawing.Point(37, 80);
            this.chkAcqEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAcqEdit.Name = "chkAcqEdit";
            this.chkAcqEdit.Size = new System.Drawing.Size(54, 21);
            this.chkAcqEdit.TabIndex = 2;
            this.chkAcqEdit.Text = "Edit";
            this.chkAcqEdit.UseVisualStyleBackColor = true;
            // 
            // chkAcqAdd
            // 
            this.chkAcqAdd.AutoSize = true;
            this.chkAcqAdd.Enabled = false;
            this.chkAcqAdd.Location = new System.Drawing.Point(37, 52);
            this.chkAcqAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAcqAdd.Name = "chkAcqAdd";
            this.chkAcqAdd.Size = new System.Drawing.Size(55, 21);
            this.chkAcqAdd.TabIndex = 1;
            this.chkAcqAdd.Text = "Add";
            this.chkAcqAdd.UseVisualStyleBackColor = true;
            // 
            // chkAcqView
            // 
            this.chkAcqView.AutoSize = true;
            this.chkAcqView.Location = new System.Drawing.Point(8, 23);
            this.chkAcqView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAcqView.Name = "chkAcqView";
            this.chkAcqView.Size = new System.Drawing.Size(83, 21);
            this.chkAcqView.TabIndex = 0;
            this.chkAcqView.Text = "Acquirer";
            this.chkAcqView.UseVisualStyleBackColor = true;
            this.chkAcqView.CheckedChanged += new System.EventHandler(this.chkAcgView_CheckedChanged);
            // 
            // gbTerminal
            // 
            this.gbTerminal.Controls.Add(this.chkTerminalDelete);
            this.gbTerminal.Controls.Add(this.chkTerminalEdit);
            this.gbTerminal.Controls.Add(this.chkTerminalAdd);
            this.gbTerminal.Controls.Add(this.chkTerminalView);
            this.gbTerminal.Location = new System.Drawing.Point(16, 12);
            this.gbTerminal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbTerminal.Name = "gbTerminal";
            this.gbTerminal.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbTerminal.Size = new System.Drawing.Size(213, 142);
            this.gbTerminal.TabIndex = 4;
            this.gbTerminal.TabStop = false;
            // 
            // chkTerminalDelete
            // 
            this.chkTerminalDelete.AutoSize = true;
            this.chkTerminalDelete.Enabled = false;
            this.chkTerminalDelete.Location = new System.Drawing.Point(37, 108);
            this.chkTerminalDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTerminalDelete.Name = "chkTerminalDelete";
            this.chkTerminalDelete.Size = new System.Drawing.Size(71, 21);
            this.chkTerminalDelete.TabIndex = 3;
            this.chkTerminalDelete.Text = "Delete";
            this.chkTerminalDelete.UseVisualStyleBackColor = true;
            // 
            // chkTerminalEdit
            // 
            this.chkTerminalEdit.AutoSize = true;
            this.chkTerminalEdit.Enabled = false;
            this.chkTerminalEdit.Location = new System.Drawing.Point(37, 80);
            this.chkTerminalEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTerminalEdit.Name = "chkTerminalEdit";
            this.chkTerminalEdit.Size = new System.Drawing.Size(54, 21);
            this.chkTerminalEdit.TabIndex = 2;
            this.chkTerminalEdit.Text = "Edit";
            this.chkTerminalEdit.UseVisualStyleBackColor = true;
            // 
            // chkTerminalAdd
            // 
            this.chkTerminalAdd.AutoSize = true;
            this.chkTerminalAdd.Enabled = false;
            this.chkTerminalAdd.Location = new System.Drawing.Point(37, 52);
            this.chkTerminalAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTerminalAdd.Name = "chkTerminalAdd";
            this.chkTerminalAdd.Size = new System.Drawing.Size(55, 21);
            this.chkTerminalAdd.TabIndex = 1;
            this.chkTerminalAdd.Text = "Add";
            this.chkTerminalAdd.UseVisualStyleBackColor = true;
            // 
            // chkTerminalView
            // 
            this.chkTerminalView.AutoSize = true;
            this.chkTerminalView.Location = new System.Drawing.Point(8, 23);
            this.chkTerminalView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkTerminalView.Name = "chkTerminalView";
            this.chkTerminalView.Size = new System.Drawing.Size(85, 21);
            this.chkTerminalView.TabIndex = 0;
            this.chkTerminalView.Text = "Terminal";
            this.chkTerminalView.UseVisualStyleBackColor = true;
            this.chkTerminalView.CheckedChanged += new System.EventHandler(this.chkTerminalView_CheckedChanged);
            // 
            // gbBankCode
            // 
            this.gbBankCode.Controls.Add(this.ChkBankCodeEdit);
            this.gbBankCode.Controls.Add(this.ChkBankCodeView);
            this.gbBankCode.Controls.Add(this.ChkBankCodeAdd);
            this.gbBankCode.Controls.Add(this.ChkBankCodeDelete);
            this.gbBankCode.Location = new System.Drawing.Point(916, 310);
            this.gbBankCode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbBankCode.Name = "gbBankCode";
            this.gbBankCode.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbBankCode.Size = new System.Drawing.Size(215, 142);
            this.gbBankCode.TabIndex = 7;
            this.gbBankCode.TabStop = false;
            // 
            // ChkBankCodeEdit
            // 
            this.ChkBankCodeEdit.AutoSize = true;
            this.ChkBankCodeEdit.Location = new System.Drawing.Point(27, 80);
            this.ChkBankCodeEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkBankCodeEdit.Name = "ChkBankCodeEdit";
            this.ChkBankCodeEdit.Size = new System.Drawing.Size(54, 21);
            this.ChkBankCodeEdit.TabIndex = 7;
            this.ChkBankCodeEdit.Text = "Edit";
            this.ChkBankCodeEdit.UseVisualStyleBackColor = true;
            // 
            // ChkBankCodeView
            // 
            this.ChkBankCodeView.AutoSize = true;
            this.ChkBankCodeView.Location = new System.Drawing.Point(8, 23);
            this.ChkBankCodeView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkBankCodeView.Name = "ChkBankCodeView";
            this.ChkBankCodeView.Size = new System.Drawing.Size(99, 21);
            this.ChkBankCodeView.TabIndex = 6;
            this.ChkBankCodeView.Text = "Bank Code";
            this.ChkBankCodeView.UseVisualStyleBackColor = true;
            // 
            // ChkBankCodeAdd
            // 
            this.ChkBankCodeAdd.AutoSize = true;
            this.ChkBankCodeAdd.Location = new System.Drawing.Point(27, 52);
            this.ChkBankCodeAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkBankCodeAdd.Name = "ChkBankCodeAdd";
            this.ChkBankCodeAdd.Size = new System.Drawing.Size(55, 21);
            this.ChkBankCodeAdd.TabIndex = 3;
            this.ChkBankCodeAdd.Text = "Add";
            this.ChkBankCodeAdd.UseVisualStyleBackColor = true;
            // 
            // ChkBankCodeDelete
            // 
            this.ChkBankCodeDelete.AutoSize = true;
            this.ChkBankCodeDelete.Location = new System.Drawing.Point(27, 108);
            this.ChkBankCodeDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ChkBankCodeDelete.Name = "ChkBankCodeDelete";
            this.ChkBankCodeDelete.Size = new System.Drawing.Size(71, 21);
            this.ChkBankCodeDelete.TabIndex = 5;
            this.ChkBankCodeDelete.Text = "Delete";
            this.ChkBankCodeDelete.UseVisualStyleBackColor = true;
            // 
            // gbProfile1
            // 
            this.gbProfile1.Controls.Add(this.chkCreateTerminal);
            this.gbProfile1.Controls.Add(this.chkRegisterOldFile);
            this.gbProfile1.Location = new System.Drawing.Point(5, 7);
            this.gbProfile1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbProfile1.Name = "gbProfile1";
            this.gbProfile1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbProfile1.Size = new System.Drawing.Size(451, 55);
            this.gbProfile1.TabIndex = 0;
            this.gbProfile1.TabStop = false;
            // 
            // chkCreateTerminal
            // 
            this.chkCreateTerminal.AutoSize = true;
            this.chkCreateTerminal.Location = new System.Drawing.Point(16, 23);
            this.chkCreateTerminal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkCreateTerminal.Name = "chkCreateTerminal";
            this.chkCreateTerminal.Size = new System.Drawing.Size(131, 21);
            this.chkCreateTerminal.TabIndex = 0;
            this.chkCreateTerminal.Text = "Create Terminal";
            this.chkCreateTerminal.UseVisualStyleBackColor = true;
            // 
            // chkRegisterOldFile
            // 
            this.chkRegisterOldFile.AutoSize = true;
            this.chkRegisterOldFile.Location = new System.Drawing.Point(244, 23);
            this.chkRegisterOldFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkRegisterOldFile.Name = "chkRegisterOldFile";
            this.chkRegisterOldFile.Size = new System.Drawing.Size(127, 21);
            this.chkRegisterOldFile.TabIndex = 1;
            this.chkRegisterOldFile.Text = "Register Profile";
            this.chkRegisterOldFile.UseVisualStyleBackColor = true;
            // 
            // FrmUserMngmt_Edit
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1423, 742);
            this.Controls.Add(this.gbRights);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbUserInfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmUserMngmt_Edit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmUserMngmt_Edit_FormClosing);
            this.Load += new System.EventHandler(this.FrmUserMngmt_Edit_Load);
            this.gbUserInfo.ResumeLayout(false);
            this.gbUserInfo.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.gbRights.ResumeLayout(false);
            this.tabUserRights.ResumeLayout(false);
            this.tabMenu.ResumeLayout(false);
            this.gbApplication.ResumeLayout(false);
            this.gbOthers.ResumeLayout(false);
            this.gbOthers.PerformLayout();
            this.gbUtilities.ResumeLayout(false);
            this.gbUtilities.PerformLayout();
            this.gbSoftwarePackage.ResumeLayout(false);
            this.gbSoftwarePackage.PerformLayout();
            this.gbSetting.ResumeLayout(false);
            this.gbSetting.PerformLayout();
            this.gbLocation.ResumeLayout(false);
            this.gbLocation.PerformLayout();
            this.gbInitTrail.ResumeLayout(false);
            this.gbInitTrail.PerformLayout();
            this.gbAuditTrail.ResumeLayout(false);
            this.gbAuditTrail.PerformLayout();
            this.gbData.ResumeLayout(false);
            this.gbData.PerformLayout();
            this.gbUserMan.ResumeLayout(false);
            this.gbUserMan.PerformLayout();
            this.tabProfile.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbProfile2.ResumeLayout(false);
            this.gbPromoManagement.ResumeLayout(false);
            this.gbPromoManagement.PerformLayout();
            this.gbEMVManagement.ResumeLayout(false);
            this.gbEMVManagement.PerformLayout();
            this.gbRemoteDownload.ResumeLayout(false);
            this.gbRemoteDownload.PerformLayout();
            this.gbEdcMonitoring.ResumeLayout(false);
            this.gbEdcMonitoring.PerformLayout();
            this.gbPinPad.ResumeLayout(false);
            this.gbPinPad.PerformLayout();
            this.gbInitialFlazz.ResumeLayout(false);
            this.gbInitialFlazz.PerformLayout();
            this.gbGPRSMan.ResumeLayout(false);
            this.gbGPRSMan.PerformLayout();
            this.gbCardTypeManagement.ResumeLayout(false);
            this.gbCardTypeManagement.PerformLayout();
            this.gbTLEManagement.ResumeLayout(false);
            this.gbTLEManagement.PerformLayout();
            this.gbCard.ResumeLayout(false);
            this.gbCard.PerformLayout();
            this.gbKey.ResumeLayout(false);
            this.gbKey.PerformLayout();
            this.gbAID.ResumeLayout(false);
            this.gbAID.PerformLayout();
            this.gbCardRange.ResumeLayout(false);
            this.gbCardRange.PerformLayout();
            this.gbIssuer.ResumeLayout(false);
            this.gbIssuer.PerformLayout();
            this.gbAcq.ResumeLayout(false);
            this.gbAcq.PerformLayout();
            this.gbTerminal.ResumeLayout(false);
            this.gbTerminal.PerformLayout();
            this.gbBankCode.ResumeLayout(false);
            this.gbBankCode.PerformLayout();
            this.gbProfile1.ResumeLayout(false);
            this.gbProfile1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.GroupBox gbUserInfo;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtUserID;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblUID;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.GroupBox gbRights;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabControl tabUserRights;
        private System.Windows.Forms.TabPage tabMenu;
        private System.Windows.Forms.TabPage tabProfile;
        private System.Windows.Forms.GroupBox gbProfile2;
        private System.Windows.Forms.GroupBox gbCard;
        private System.Windows.Forms.CheckBox chkCardDelete;
        private System.Windows.Forms.CheckBox chkCardAdd;
        private System.Windows.Forms.CheckBox chkCardView;
        private System.Windows.Forms.GroupBox gbKey;
        private System.Windows.Forms.CheckBox chkKeyDelete;
        private System.Windows.Forms.CheckBox chkKeyEdit;
        private System.Windows.Forms.CheckBox chkKeyAdd;
        private System.Windows.Forms.CheckBox chkKeyView;
        private System.Windows.Forms.GroupBox gbAID;
        private System.Windows.Forms.CheckBox chkAIDDelete;
        private System.Windows.Forms.CheckBox chkAIDEdit;
        private System.Windows.Forms.CheckBox chkAIDAdd;
        private System.Windows.Forms.CheckBox chkAIDView;
        private System.Windows.Forms.GroupBox gbCardRange;
        private System.Windows.Forms.CheckBox chkCardRangeDelete;
        private System.Windows.Forms.CheckBox chkCardRangeEdit;
        private System.Windows.Forms.CheckBox chkCardRangeAdd;
        private System.Windows.Forms.CheckBox chkCardRangeView;
        private System.Windows.Forms.GroupBox gbIssuer;
        private System.Windows.Forms.CheckBox chkIssuerDelete;
        private System.Windows.Forms.CheckBox chkIssuerEdit;
        private System.Windows.Forms.CheckBox chkIssuerAdd;
        private System.Windows.Forms.CheckBox chkIssuerView;
        private System.Windows.Forms.GroupBox gbAcq;
        private System.Windows.Forms.CheckBox chkAcqDelete;
        private System.Windows.Forms.CheckBox chkAcqEdit;
        private System.Windows.Forms.CheckBox chkAcqAdd;
        private System.Windows.Forms.CheckBox chkAcqView;
        private System.Windows.Forms.GroupBox gbTerminal;
        private System.Windows.Forms.CheckBox chkTerminalDelete;
        private System.Windows.Forms.CheckBox chkTerminalEdit;
        private System.Windows.Forms.CheckBox chkTerminalAdd;
        private System.Windows.Forms.CheckBox chkTerminalView;
        private System.Windows.Forms.GroupBox gbProfile1;
        private System.Windows.Forms.CheckBox chkCreateTerminal;
        private System.Windows.Forms.CheckBox chkRegisterOldFile;
        private System.Windows.Forms.CheckBox chkDownload;
        private System.Windows.Forms.GroupBox gbApplication;
        private System.Windows.Forms.CheckBox chkAuditTrailView;
        private System.Windows.Forms.CheckBox chkTemplateDef;
        private System.Windows.Forms.GroupBox gbUserMan;
        private System.Windows.Forms.CheckBox chkUserManDelete;
        private System.Windows.Forms.CheckBox chkUserManEdit;
        private System.Windows.Forms.CheckBox chkUserManAdd;
        private System.Windows.Forms.CheckBox chkUserManView;
        private System.Windows.Forms.CheckBox chkInitTrailView;
        private System.Windows.Forms.CheckBox chkPopulateTLV;
        private System.Windows.Forms.CheckBox chkUploadDef;
        private System.Windows.Forms.CheckBox chkAllowInit;
        private System.Windows.Forms.CheckBox chkUpload;
        private System.Windows.Forms.CheckBox chkSNValidation;
        private System.Windows.Forms.GroupBox gbUtilities;
        private System.Windows.Forms.CheckBox chkEdcIpTracking;
        private System.Windows.Forms.GroupBox gbData;
        private System.Windows.Forms.CheckBox chkDataView;
        internal System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.ToolTip tipTextBoxUserInfo;
        internal System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox gbAuditTrail;
        private System.Windows.Forms.CheckBox chkInitTrailDelete;
        private System.Windows.Forms.CheckBox chkAuditTrailDelete;
        private System.Windows.Forms.GroupBox gbInitTrail;
        private System.Windows.Forms.GroupBox gbLocation;
        private System.Windows.Forms.CheckBox chkLocationImport;
        private System.Windows.Forms.CheckBox chkLocationAdd;
        private System.Windows.Forms.CheckBox chkLocationView;
        private System.Windows.Forms.CheckBox chkInitTrailImport;
        private System.Windows.Forms.CheckBox chkInitTrailExport;
        private System.Windows.Forms.CheckBox chkAuditTrailImport;
        private System.Windows.Forms.CheckBox chkAuditTrailExport;
        private System.Windows.Forms.CheckBox chkCompressInit;
        private System.Windows.Forms.GroupBox gbSetting;
        private System.Windows.Forms.CheckBox chkAutoInit;
        private System.Windows.Forms.GroupBox gbTLEManagement;
        private System.Windows.Forms.CheckBox chkTLEDelete;
        private System.Windows.Forms.CheckBox chkTLEEdit;
        private System.Windows.Forms.CheckBox chkTLEAdd;
        private System.Windows.Forms.CheckBox chkTLEView;
        private System.Windows.Forms.GroupBox gbSoftwarePackage;
        private System.Windows.Forms.CheckBox chkSoftwareDelete;
        private System.Windows.Forms.CheckBox chkSoftwareEdit;
        private System.Windows.Forms.CheckBox chkSoftwareAdd;
        private System.Windows.Forms.CheckBox chkSoftwareView;
        private System.Windows.Forms.GroupBox gbCardTypeManagement;
        private System.Windows.Forms.CheckBox chkCardTypeDelete;
        private System.Windows.Forms.CheckBox chkCardTypeEdit;
        private System.Windows.Forms.CheckBox chkCardTypeAdd;
        private System.Windows.Forms.CheckBox chkCardTypeManView;
        private System.Windows.Forms.GroupBox gbGPRSMan;
        private System.Windows.Forms.CheckBox chkGPRSDel;
        private System.Windows.Forms.CheckBox chkGPRSEdit;
        private System.Windows.Forms.CheckBox chkGPRSAdd;
        private System.Windows.Forms.CheckBox chkGPRSView;
        private System.Windows.Forms.GroupBox gbInitialFlazz;
        private System.Windows.Forms.CheckBox chkInitialFlazzDelete;
        private System.Windows.Forms.CheckBox chkInitialFlazzEdit;
        private System.Windows.Forms.CheckBox chkInitialFlazzAdd;
        private System.Windows.Forms.CheckBox chkInitialFlazzView;
        private System.Windows.Forms.CheckBox chkSchedule;
        private System.Windows.Forms.CheckBox chkQueryReport;
        private System.Windows.Forms.GroupBox gbPinPad;
        private System.Windows.Forms.CheckBox chkPinPadDelete;
        private System.Windows.Forms.CheckBox chkPinPadedit;
        private System.Windows.Forms.CheckBox chkPinPadAdd;
        private System.Windows.Forms.CheckBox chkPinPadView;
        private System.Windows.Forms.GroupBox gbBankCode;//mycode   
        private System.Windows.Forms.CheckBox ChkBankCodeDelete;//mycode
        private System.Windows.Forms.CheckBox ChkBankCodeEdit;//mycode
        private System.Windows.Forms.CheckBox ChkBankCodeAdd;//mycode
        private System.Windows.Forms.CheckBox ChkBankCodeView;//mycode
        private System.Windows.Forms.GroupBox gbEdcMonitoring;//mycode
        private System.Windows.Forms.CheckBox chkEdcMonitorDelete;//mycode
        private System.Windows.Forms.CheckBox chkEdcMonitorEdit;//mycode
        private System.Windows.Forms.CheckBox chkEdcMonitorAdd;//mycode
        private System.Windows.Forms.CheckBox chkEdcMonitorView;
        private System.Windows.Forms.GroupBox gbRemoteDownload;
        private System.Windows.Forms.CheckBox chkRemoteDownloadDelete;
        private System.Windows.Forms.CheckBox chkRemoteDownloadEdit;
        private System.Windows.Forms.CheckBox chkRemoteDownloadAdd;
        private System.Windows.Forms.CheckBox chkRemoteDownloadView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkAuditTrailWeb;
        private System.Windows.Forms.CheckBox chkSoftwareTrailWeb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox gbEMVManagement;
        private System.Windows.Forms.CheckBox chkEMVManagementDelete;
        private System.Windows.Forms.CheckBox chkEMVManagementEdit;
        private System.Windows.Forms.CheckBox chkEMVManagementAdd;
        private System.Windows.Forms.CheckBox chkEMVManagementView;
        private System.Windows.Forms.CheckBox chkLocked;
        private System.Windows.Forms.CheckBox chkRevoke;
        private System.Windows.Forms.CheckBox chkInitPicture;
        private System.Windows.Forms.GroupBox gbOthers;
        private System.Windows.Forms.GroupBox gbPromoManagement;
        private System.Windows.Forms.CheckBox chkPromoManagementDelete;
        private System.Windows.Forms.CheckBox chkPromoManagementEdit;
        private System.Windows.Forms.CheckBox chkPromoManagementAdd;
        private System.Windows.Forms.CheckBox chkPromoManagementView;
    }
}