﻿namespace InSys
{
    partial class FrmItemUserRight
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmItemUserRight));
            this.gbItem = new System.Windows.Forms.GroupBox();
            this.cmbDbId = new System.Windows.Forms.ComboBox();
            this.lblDatabaseID = new System.Windows.Forms.Label();
            this.lbEnable = new System.Windows.Forms.ListBox();
            this.btnUnReg = new System.Windows.Forms.Button();
            this.txtGroupID = new System.Windows.Forms.TextBox();
            this.btnReg = new System.Windows.Forms.Button();
            this.lbGroupID = new System.Windows.Forms.Label();
            this.lblRegisteredUID = new System.Windows.Forms.Label();
            this.lbAvailable = new System.Windows.Forms.ListBox();
            this.lblAvailable = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbItem.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbItem
            // 
            this.gbItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbItem.Controls.Add(this.cmbDbId);
            this.gbItem.Controls.Add(this.lblDatabaseID);
            this.gbItem.Controls.Add(this.lbEnable);
            this.gbItem.Controls.Add(this.btnUnReg);
            this.gbItem.Controls.Add(this.txtGroupID);
            this.gbItem.Controls.Add(this.btnReg);
            this.gbItem.Controls.Add(this.lbGroupID);
            this.gbItem.Controls.Add(this.lblRegisteredUID);
            this.gbItem.Controls.Add(this.lbAvailable);
            this.gbItem.Controls.Add(this.lblAvailable);
            this.gbItem.Location = new System.Drawing.Point(7, 1);
            this.gbItem.Name = "gbItem";
            this.gbItem.Size = new System.Drawing.Size(404, 454);
            this.gbItem.TabIndex = 0;
            this.gbItem.TabStop = false;
            // 
            // cmbDbId
            // 
            this.cmbDbId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDbId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDbId.FormattingEnabled = true;
            this.cmbDbId.Location = new System.Drawing.Point(88, 39);
            this.cmbDbId.Name = "cmbDbId";
            this.cmbDbId.Size = new System.Drawing.Size(163, 21);
            this.cmbDbId.TabIndex = 15;
            this.cmbDbId.SelectedIndexChanged += new System.EventHandler(this.cmbDbId_SelectedIndexChanged);
            // 
            // lblDatabaseID
            // 
            this.lblDatabaseID.AutoSize = true;
            this.lblDatabaseID.Location = new System.Drawing.Point(9, 44);
            this.lblDatabaseID.Name = "lblDatabaseID";
            this.lblDatabaseID.Size = new System.Drawing.Size(67, 13);
            this.lblDatabaseID.TabIndex = 14;
            this.lblDatabaseID.Text = "Database ID";
            // 
            // lbEnable
            // 
            this.lbEnable.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbEnable.FormattingEnabled = true;
            this.lbEnable.Location = new System.Drawing.Point(229, 100);
            this.lbEnable.Name = "lbEnable";
            this.lbEnable.ScrollAlwaysVisible = true;
            this.lbEnable.Size = new System.Drawing.Size(165, 342);
            this.lbEnable.TabIndex = 10;
            // 
            // btnUnReg
            // 
            this.btnUnReg.Location = new System.Drawing.Point(190, 248);
            this.btnUnReg.Name = "btnUnReg";
            this.btnUnReg.Size = new System.Drawing.Size(35, 30);
            this.btnUnReg.TabIndex = 13;
            this.btnUnReg.Text = "<";
            this.btnUnReg.UseVisualStyleBackColor = true;
            this.btnUnReg.Click += new System.EventHandler(this.btnUnReg_Click);
            // 
            // txtGroupID
            // 
            this.txtGroupID.Location = new System.Drawing.Point(88, 13);
            this.txtGroupID.Name = "txtGroupID";
            this.txtGroupID.Size = new System.Drawing.Size(210, 20);
            this.txtGroupID.TabIndex = 1;
            // 
            // btnReg
            // 
            this.btnReg.Location = new System.Drawing.Point(190, 212);
            this.btnReg.Name = "btnReg";
            this.btnReg.Size = new System.Drawing.Size(35, 30);
            this.btnReg.TabIndex = 12;
            this.btnReg.Text = ">";
            this.btnReg.UseVisualStyleBackColor = true;
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            // 
            // lbGroupID
            // 
            this.lbGroupID.AutoSize = true;
            this.lbGroupID.Location = new System.Drawing.Point(9, 16);
            this.lbGroupID.Name = "lbGroupID";
            this.lbGroupID.Size = new System.Drawing.Size(50, 13);
            this.lbGroupID.TabIndex = 0;
            this.lbGroupID.Text = "Group ID";
            // 
            // lblRegisteredUID
            // 
            this.lblRegisteredUID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblRegisteredUID.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblRegisteredUID.Location = new System.Drawing.Point(229, 75);
            this.lblRegisteredUID.Name = "lblRegisteredUID";
            this.lblRegisteredUID.Size = new System.Drawing.Size(165, 25);
            this.lblRegisteredUID.TabIndex = 11;
            this.lblRegisteredUID.Text = "Enabled Item";
            this.lblRegisteredUID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAvailable
            // 
            this.lbAvailable.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbAvailable.FormattingEnabled = true;
            this.lbAvailable.Location = new System.Drawing.Point(9, 100);
            this.lbAvailable.Name = "lbAvailable";
            this.lbAvailable.ScrollAlwaysVisible = true;
            this.lbAvailable.Size = new System.Drawing.Size(165, 342);
            this.lbAvailable.TabIndex = 8;
            // 
            // lblAvailable
            // 
            this.lblAvailable.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblAvailable.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblAvailable.Location = new System.Drawing.Point(9, 75);
            this.lblAvailable.Name = "lblAvailable";
            this.lblAvailable.Size = new System.Drawing.Size(165, 25);
            this.lblAvailable.TabIndex = 9;
            this.lblAvailable.Text = "Disabled Item";
            this.lblAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnDelete);
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(7, 456);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(404, 49);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(223, 14);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 27);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(314, 14);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 27);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(132, 14);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 27);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmItemUserRight
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 508);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbItem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(430, 550);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(430, 550);
            this.Name = "FrmItemUserRight";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Item User Right";
            this.Load += new System.EventHandler(this.FrmItemUserRight_Load);
            this.gbItem.ResumeLayout(false);
            this.gbItem.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbItem;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lbGroupID;
        private System.Windows.Forms.TextBox txtGroupID;
        private System.Windows.Forms.Button btnUnReg;
        private System.Windows.Forms.Button btnReg;
        private System.Windows.Forms.Label lblRegisteredUID;
        private System.Windows.Forms.ListBox lbAvailable;
        private System.Windows.Forms.ListBox lbEnable;
        private System.Windows.Forms.Label lblAvailable;
        internal System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ComboBox cmbDbId;
        private System.Windows.Forms.Label lblDatabaseID;
    }
}