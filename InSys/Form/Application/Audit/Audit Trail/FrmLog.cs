using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using InSysClass;

namespace InSys
{
    public partial class FrmLog : Form
    {
        /// <summary>
        /// Local SQL Connection variable
        /// </summary>
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// SQLCommand variable to capture last filter cond. used by button refresh.
        /// </summary>
        protected SqlCommand oLastComm;
        /// <summary>
        /// string variable to capture last filter cond. used by button refresh.
        /// </summary>
        protected string sLastCond;
        /// <summary>
        /// int variable to capture dgv's cols width right after databind.
        /// </summary>
        protected int[] iColWidth;

        protected bool IsSuperUserAudit = false;

        /// <summary>
        /// Audit Trail window constructor
        /// </summary>
        /// <param name="_oSqlConn">SQLConnection : </param>
        /// <param name="_IsSuperUserAudit">Boolean is accessing AuditTrail SuperUser </param>
        public FrmLog(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, bool _IsSuperUserAudit)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            IsSuperUserAudit = _IsSuperUserAudit;
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sAccessLog, "");
        }

        #region Custom Function(s)
        /// <summary>
        /// Initiate button Clear based on its privilege
        /// </summary>
        private void InitButton()
        {
            btnExport.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.AT, Privilege.Add);
            btnImport.Enabled = IsSuperUserAudit ? false : UserPrivilege.IsAllowed(PrivilegeCode.AT, Privilege.Edit);
            btnDelete.Enabled = IsSuperUserAudit ? false : UserPrivilege.IsAllowed(PrivilegeCode.AT, Privilege.Delete);
        }

        /// <summary>
        /// Try to Load data from database and display it at DataGridView
        /// </summary>
        /// <returns>boolean : true if successfull, else false</returns>
        public bool isInitData()
        {
            try
            {
                oLastComm = GetFilterCommand();
                return isRefreshDataGridView();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        /// <summary>
        /// Get Filter condition from FrmFilterLog and create an SqlCommand to load data from database.
        /// </summary>
        /// <returns>SqlCommand : Commands to be executed in database.</returns>
        private SqlCommand GetFilterCommand()
        {
            FrmFilterLog ffilterlog = new FrmFilterLog(oSqlConn);
            ffilterlog.ShowDialog();
            if (ffilterlog.DialogResult == DialogResult.OK) // Get parameter from ffilterlog
            {
                SqlCommand oSqlCmd = UserData.isSuperUser && IsSuperUserAudit ?
                    new SqlCommand(CommonSP.sSPAuditTrailSUBrowse, oSqlConn) :
                    new SqlCommand(CommonSP.sSPAuditTrailBrowse, oSqlConn);

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sLastCond =
                    ffilterlog.sGetConditionString();

                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                oSqlCmd.Connection = oSqlConn;
                return oSqlCmd;
            }
            return null;
        }
      
        /// <summary>
        /// Set datagridviewcolumn's width 
        /// </summary>
        private void InitColWidth()
        {
            iColWidth = new int[dgData.ColumnCount];
            for (int iCol = 0; iCol < dgData.ColumnCount; iCol++)
                iColWidth[iCol] = dgData.Columns[iCol].Width;
            iColWidth[dgData.ColumnCount - 1] = TxtDetailLog.Width;
        }

        /// <summary>
        /// Set the Detail Log.
        /// </summary>
        private void FillTxtDetailLog()
        {
            if (dgData.RowCount > 0 && dgData.Columns["Remarks"] != null)
                TxtDetailLog.Lines = dgData.CurrentRow.Cells["Remarks"].Value.ToString().Split('\n');
        }

        /// <summary>
        /// Set the DataSource for DataGridView and hide Column "Remarks".
        /// </summary>
        /// <returns>boolean : true if successfull, else false</returns>
        private bool isRefreshDataGridView()
        {
            if (oLastComm != null)
            {
                try
                {
                    DataTable oTable = new DataTable();
                    new SqlDataAdapter(oLastComm).Fill(oTable); // Fill DataTable
                    this.dgData.DataSource = oTable; // Set DataSource
                    dgData.Columns["Remarks"].Visible = false;
                    return true; // Success
                }
                catch (Exception oException)
                {
                    MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return false;
        }

        /// <summary>
        /// Create an XML file to save Log Data.
        /// </summary>
        /// <param name="sSheetName">string : File Name</param>
        /// <param name="oReader">SqlDataReader : Data which will be exported</param>
        /// <param name="sFilePath">string : Where the file will be saved</param>
        private void CreateFile(string sSheetName, SqlDataReader oReader, string sFilePath)
        {
            XmlExport oExport = new XmlExport("Sheet1");
            oExport.Author = UserData.sUserID; // Set Author    
            oExport.CreationTime = DateTime.Now; // Set Time
            oExport.Write(GetAuditColumnCollection(), oReader, sFilePath); // Create File
        }

        /// <summary>
        /// Get Columns for Audit Log and set the type of each columns.
        /// </summary>
        /// <returns>XmlColumn array : Columns for Xml File</returns>
        private XmlColumn[] GetAuditColumnCollection()
        {
            XmlColumn[] oColumn = new XmlColumn[iColWidth.Length];
            for (int iCol = 0; iCol < iColWidth.Length; iCol++)
                oColumn[iCol] = new XmlColumn(iColWidth[iCol], iCol == 0 ? XmlType.Number : XmlType.String);
            return oColumn;
        }

        /// <summary>
        /// Get the type of columns from imported files.
        /// </summary>
        /// <returns>XmlType array : Array of Xml Column Type</returns>
        private XmlType[] GetImportColumnsType()
        {
            XmlType[] eType = new XmlType[iColWidth.Length];
            for (int iType = 0; iType < iColWidth.Length; iType++)
                eType[iType] = iType == 0 ? XmlType.Number : XmlType.String;
            return eType;
        }

        /// <summary>
        /// Get path to save DeleteLog file.
        /// </summary>
        /// <returns>string : File path</returns>
        private string GetDeleteLogPath()
        {
            string sDelLogDir = Application.StartupPath + "\\DELETE LOGS";
            if (!Directory.Exists(sDelLogDir)) Directory.CreateDirectory(sDelLogDir);
            return sDelLogDir + "\\AUDIT " + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00") + " " + DateTime.Now.Hour.ToString("00") + ";" + DateTime.Now.Minute.ToString("00") + ";" + DateTime.Now.Second.ToString("00") + "," + DateTime.Now.Millisecond.ToString("000") + ".xml";
        }

        /// <summary>
        /// Delete log from database.
        /// </summary>
        private void DeleteLogFromDB()
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oDelCom = new SqlCommand(CommonSP.sSPAuditTrailDelete, oSqlConn);
            oDelCom.CommandType = CommandType.StoredProcedure;
            oDelCom.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sLastCond;
            oDelCom.ExecuteNonQuery();
        }
        #endregion

        /// <summary>
        /// Set buttuns in form and select DataGridView.
        /// </summary>
        private void FrmLog_Load(object sender, EventArgs e)
        {
            InitButton();
            dgData.Select();
        }

        /// <summary>
        /// When form activated, minimize other forms and maximized current form
        /// </summary>
        private void FrmLog_Activated(object sender, EventArgs e)
        {
            //CommonClass.doMinimizeChild(this.ParentForm.MdiChildren, this.Name);
            //this.WindowState = FormWindowState.Maximized;
            //dgData.Select();
        }

        /// <summary>
        /// Reset filter for Logs.
        /// </summary>
        private void btnFilter_Click(object sender, EventArgs e)
        {
            isInitData();
            dgData.Select();
            FillTxtDetailLog();
        }

        /// <summary>
        /// Imports logs from selected file.
        /// </summary>
        private void btnImport_Click(object sender, EventArgs e)
        {
            bool isImportSuccess = false;
            try
            {
                if (ofdImport.ShowDialog() == DialogResult.OK) // Select file
                {
                    dgData.DataSource = new XmlImport(ofdImport.FileName).Read(GetImportColumnsType());
                    isImportSuccess = true;
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sImportDataFromExcel, "Opened from " + ofdImport.FileName);
                }
            }
            catch (Exception oException)
            {
                string sErrMsg = isImportSuccess ? oException.Message : "Content format error.";
                MessageBox.Show(sErrMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            dgData.Select();
            FillTxtDetailLog();
        }

        /// <summary>
        /// Export logs to file.
        /// </summary>
        private void btnExport_Click(object sender, EventArgs e)
        {
            SqlCommand oSqlCmd = GetFilterCommand(); 
            if (oSqlCmd != null)
            {
                //SqlDataReader oReader = oSqlComm.ExecuteReader();
                //try
                //{
                //    if (sfdExport.ShowDialog() == DialogResult.OK) // Set file path
                //    {
                //        CreateFile("Sheet1", oReader, sfdExport.FileName); // Create Xml File
                //        oReader.Close();
                //        CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sExportDataToExcel, "Saved as " + sfdExport.FileName);
                //    }
                //}
                //catch (Exception oException)
                //{
                //    MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
                //if (!oReader.IsClosed) oReader.Close();
                sfdExport.Filter = "Comma Separated Values(*.csv)|*.csv";
                if (sfdExport.ShowDialog() == DialogResult.OK)
                    using (DataTable dtTemp = new DataTable())
                    {
                        (new SqlDataAdapter(oSqlCmd)).Fill(dtTemp);

                        //File.Delete(sfdExport.FileName);
                        //File.Copy(string.Format(@"{0}\TEMPLATE\template.xls", Application.StartupPath), sfdExport.FileName);
                        //ExcelOleDb excelOleDb = new ExcelOleDb(sfdExport.FileName);
                        //dtTemp.TableName = "AuditTrail";
                        //excelOleDb.CreateWorkSheet(dtTemp);
                        //excelOleDb.InsertRow(dtTemp);
                        //excelOleDb.Dispose();

                        // change format to csv
                        Csv.Write(dtTemp, sfdExport.FileName, '|');
                    }
            }
            dgData.Select();
            FillTxtDetailLog();
        }

        /// <summary>
        /// Delete Logs based on selected filters.
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            SqlCommand oSqlComm = GetFilterCommand(); // Get the filter
            if (oSqlComm != null && MessageBox.Show("Are you sure want to delete the logs?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string sDeleteLogPath = "";
                SqlDataReader oReader = oSqlComm.ExecuteReader();
                try
                {
                    CreateFile("Deleted Audit Logs", oReader, sDeleteLogPath = GetDeleteLogPath()); // Create DeleteLog file
                    oReader.Close();

                    DeleteLogFromDB(); // Delete log from database

                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sDeleteLog, "Deleted Audit Logs Saved at " + sDeleteLogPath);
                    isRefreshDataGridView();
                }
                catch (Exception oException)
                {
                    File.Delete(sDeleteLogPath);
                    MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (!oReader.IsClosed) oReader.Close();
            }
            dgData.Select();
            FillTxtDetailLog();
        }

        /// <summary>
        /// Refresh DataGridView, retake data from database to be displayed on DataGridView
        /// </summary>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            isRefreshDataGridView();
            FillTxtDetailLog();
            dgData.Select();
        }

        /// <summary>
        /// Runs when DataSource list changes, set the width of DataGridViewColumns.
        /// </summary>
        private void dgData_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            TxtDetailLog.Text = "";
            InitColWidth();
        }

        /// <summary>
        /// Runs when current selection changes, to set detail logs based on current selection.
        /// </summary>
        private void dgData_SelectionChanged(object sender, EventArgs e)
        {
            FillTxtDetailLog();
        }

        public bool IsAuditSuperUser { get { return IsSuperUserAudit; } }
    }
}