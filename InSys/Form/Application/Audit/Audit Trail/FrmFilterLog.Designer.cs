namespace InSys
{
    partial class FrmFilterLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFilterLog));
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnOK = new System.Windows.Forms.Button();
            this.cmbDatabase = new System.Windows.Forms.ComboBox();
            this.cmbUser = new System.Windows.Forms.ComboBox();
            this.SaveFileDialogExport = new System.Windows.Forms.SaveFileDialog();
            this.txtActionDesc = new System.Windows.Forms.TextBox();
            this.lblActionDesc = new System.Windows.Forms.Label();
            this.dtpDateTo = new System.Windows.Forms.DateTimePicker();
            this.dtpDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lblDateForm = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.gbFilterView = new System.Windows.Forms.GroupBox();
            this.gbButton.SuspendLayout();
            this.gbFilterView.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnCancel.Location = new System.Drawing.Point(120, 13);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(84, 23);
            this.BtnCancel.TabIndex = 36;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BtnOK
            // 
            this.BtnOK.Image = ((System.Drawing.Image)(resources.GetObject("BtnOK.Image")));
            this.BtnOK.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnOK.Location = new System.Drawing.Point(8, 13);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(84, 23);
            this.BtnOK.TabIndex = 35;
            this.BtnOK.Text = "OK";
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // cmbDatabase
            // 
            this.cmbDatabase.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDatabase.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDatabase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDatabase.Location = new System.Drawing.Point(111, 88);
            this.cmbDatabase.Name = "cmbDatabase";
            this.cmbDatabase.Size = new System.Drawing.Size(200, 21);
            this.cmbDatabase.TabIndex = 32;
            // 
            // cmbUser
            // 
            this.cmbUser.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbUser.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUser.Location = new System.Drawing.Point(111, 64);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(200, 21);
            this.cmbUser.TabIndex = 31;
            // 
            // txtActionDesc
            // 
            this.txtActionDesc.Location = new System.Drawing.Point(111, 112);
            this.txtActionDesc.Multiline = true;
            this.txtActionDesc.Name = "txtActionDesc";
            this.txtActionDesc.Size = new System.Drawing.Size(200, 64);
            this.txtActionDesc.TabIndex = 34;
            // 
            // lblActionDesc
            // 
            this.lblActionDesc.AutoSize = true;
            this.lblActionDesc.Location = new System.Drawing.Point(12, 115);
            this.lblActionDesc.Name = "lblActionDesc";
            this.lblActionDesc.Size = new System.Drawing.Size(93, 13);
            this.lblActionDesc.TabIndex = 33;
            this.lblActionDesc.Text = "Action Description";
            this.lblActionDesc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpDateTo
            // 
            this.dtpDateTo.CustomFormat = "MMM dd yyyy";
            this.dtpDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateTo.Location = new System.Drawing.Point(111, 40);
            this.dtpDateTo.Name = "dtpDateTo";
            this.dtpDateTo.Size = new System.Drawing.Size(200, 20);
            this.dtpDateTo.TabIndex = 30;
            // 
            // dtpDateFrom
            // 
            this.dtpDateFrom.CustomFormat = "MMM dd yyyy";
            this.dtpDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateFrom.Location = new System.Drawing.Point(111, 16);
            this.dtpDateFrom.Name = "dtpDateFrom";
            this.dtpDateFrom.Size = new System.Drawing.Size(200, 20);
            this.dtpDateFrom.TabIndex = 29;
            this.dtpDateFrom.Value = new System.DateTime(2006, 8, 30, 11, 59, 0, 0);
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.Location = new System.Drawing.Point(52, 91);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(56, 13);
            this.lblDatabase.TabIndex = 28;
            this.lblDatabase.Text = "Database ";
            this.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(76, 67);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(29, 13);
            this.lblUser.TabIndex = 27;
            this.lblUser.Text = "User";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDateTo
            // 
            this.lblDateTo.AutoSize = true;
            this.lblDateTo.Location = new System.Drawing.Point(59, 44);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(49, 13);
            this.lblDateTo.TabIndex = 26;
            this.lblDateTo.Text = "Date To ";
            this.lblDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDateForm
            // 
            this.lblDateForm.AutoSize = true;
            this.lblDateForm.Location = new System.Drawing.Point(49, 20);
            this.lblDateForm.Name = "lblDateForm";
            this.lblDateForm.Size = new System.Drawing.Size(56, 13);
            this.lblDateForm.TabIndex = 25;
            this.lblDateForm.Text = "Date From";
            this.lblDateForm.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.BtnCancel);
            this.gbButton.Controls.Add(this.BtnOK);
            this.gbButton.Location = new System.Drawing.Point(130, 195);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(210, 42);
            this.gbButton.TabIndex = 37;
            this.gbButton.TabStop = false;
            // 
            // gbFilterView
            // 
            this.gbFilterView.Controls.Add(this.lblDateForm);
            this.gbFilterView.Controls.Add(this.lblDateTo);
            this.gbFilterView.Controls.Add(this.cmbDatabase);
            this.gbFilterView.Controls.Add(this.lblUser);
            this.gbFilterView.Controls.Add(this.cmbUser);
            this.gbFilterView.Controls.Add(this.lblDatabase);
            this.gbFilterView.Controls.Add(this.txtActionDesc);
            this.gbFilterView.Controls.Add(this.dtpDateFrom);
            this.gbFilterView.Controls.Add(this.lblActionDesc);
            this.gbFilterView.Controls.Add(this.dtpDateTo);
            this.gbFilterView.Location = new System.Drawing.Point(12, 7);
            this.gbFilterView.Name = "gbFilterView";
            this.gbFilterView.Size = new System.Drawing.Size(328, 185);
            this.gbFilterView.TabIndex = 38;
            this.gbFilterView.TabStop = false;
            // 
            // FrmFilterLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 246);
            this.Controls.Add(this.gbFilterView);
            this.Controls.Add(this.gbButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmFilterLog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Filter View";
            this.Load += new System.EventHandler(this.FrmFilterLog_Load);
            this.gbButton.ResumeLayout(false);
            this.gbFilterView.ResumeLayout(false);
            this.gbFilterView.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnOK;
        internal System.Windows.Forms.ComboBox cmbDatabase;
        internal System.Windows.Forms.ComboBox cmbUser;
        internal System.Windows.Forms.SaveFileDialog SaveFileDialogExport;
        internal System.Windows.Forms.TextBox txtActionDesc;
        internal System.Windows.Forms.Label lblActionDesc;
        internal System.Windows.Forms.DateTimePicker dtpDateTo;
        internal System.Windows.Forms.DateTimePicker dtpDateFrom;
        internal System.Windows.Forms.Label lblDatabase;
        internal System.Windows.Forms.Label lblUser;
        internal System.Windows.Forms.Label lblDateTo;
        internal System.Windows.Forms.Label lblDateForm;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.GroupBox gbFilterView;
    }
}