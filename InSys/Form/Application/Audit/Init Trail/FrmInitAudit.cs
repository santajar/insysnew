using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using InSysClass;
using System.Data.Odbc;

namespace InSys
{
    public partial class FrmInitAudit : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected bool isExcelMode = false; //Needed to determine the datasource of datagrid.
        protected string sImportSource;     //Needed to capture the location of the imported file right after import. Used by button Search.
        public bool isAuditInit = true;

        int icol;
        OdbcDataAdapter oOledbDataAdapter = new OdbcDataAdapter();
        /// <summary>
        /// Form to view Initiation Log from EDC to Insys
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        public FrmInitAudit(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
            TabColExcel.Dispose();
            TabAuditInitSum.Dispose();
            this.WindowState = FormWindowState.Maximized;
        }

        /// <summary>
        /// Runs when form is loading; load data, set DataGridView, and Insert Log
        /// </summary>
        private void FrmInitAudit_Load(object sender, EventArgs e)
        {
            try
            {
                InitData(); // Get Data
                RefreshView(); // Set DataGridView
                // Insert Log
                if (isAuditInit)
                {
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sAccessInitTrail, "");
                    TabAuditInitDetail.Text = TabAuditInitDetail.Text.Replace("Software", "Profile");
                }
                else
                {
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sAccessInitSoftwareTrail, "");
                    TabAuditInitDetail.Text = TabAuditInitDetail.Text.Replace("Profile", "Software");
                }
                dgInitDetail.Select();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Runs when form is activated, show form in maximized state.
        /// </summary>
        private void FrmInitAudit_Activated(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            dgInitDetail.Select();
        }

        #region Custom Function(s)
        /// <summary>
        /// Initiate buttons of form based on UserRights
        /// </summary>
        private void InitButton()
        {
            btnExportDetail.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Add);
            btnExportSummary.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Add);
            btnImportDetail.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Edit);
            btnImportSummary.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Edit);
            btnDeleteDetail.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Delete);
            btnDeleteSummary.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Delete);
        }

        /// <summary>
        /// Initiate Button.
        /// </summary>
        private void InitData()
        {
            InitButton();
        }

        /// <summary>
        /// Set DataSource for DataGridView.
        /// </summary>
        /// <param name="oSqlComm">SqlCommand : Command to load data from database.</param>
        /// <param name="oDGV">DataGridView : one which will be assigned</param>
        private void SetDgView(SqlCommand oSqlComm, DataGridView oDGV)
        {
            DataTable oTable = new DataTable();
            new SqlDataAdapter(oSqlComm).Fill(oTable); // Fill DataTable
            oDGV.DataSource = oTable;
        }

        /// <summary>
        /// Refresh all DataGridViews on Form, reload data from database to DataGridView's DataSource.
        /// </summary>
        private void RefreshView()
        {
            txtTIDSum.Text = "";
            txtTIDdetail.Text = "";
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            if (isAuditInit)
            {
                SetDgView(new SqlCommand(CommonSP.sSPInitTrailBrowse, oSqlConn), dgInitDetail);
                SetDgView(new SqlCommand(CommonSP.sSPInitTrailSummaryBrowse, oSqlConn), dgInitSum);
            }
            else
            {
                SetDgView(new SqlCommand(CommonSP.sSPInitSoftwareTrailBrowse, oSqlConn), dgInitDetail);
            }
            SetImportView(false);
        }

        /// <summary>
        /// Get Columns for Init Log and set the type of each columns.
        /// </summary>
        /// <param name="oDGV">DataGridView : one which will be used</param>
        /// <returns>XmlColumn Array : Columns for Xml File</returns>
        private XmlColumn[] oGetColumnCollection(DataGridView oDGV)
        {
            XmlColumn[] oCol = new XmlColumn[oDGV.ColumnCount];
            for (int iCol = 0; iCol < oDGV.ColumnCount; iCol++)
                oCol[iCol] = new XmlColumn(oDGV.Columns[iCol].Width, XmlType.String);
            return oCol;
        }

        /// <summary>
        /// Get the type of columns from imported files.
        /// </summary>
        /// <param name="oDGV">DataGridView : one which will be used</param>
        /// <returns>XmlType array : Array of Xml Column Type</returns>
        private XmlType[] eGetImportColumnTypes(DataGridView oDGV)
        {
            XmlType[] eType = new XmlType[oDGV.ColumnCount];
            for (int iType = 0; iType < oDGV.ColumnCount; iType++)
                eType[iType] = XmlType.String;
            return eType;
        }

        /// <summary>
        /// Export logs to file.
        /// </summary>
        /// <param name="oSqlComm">SqlCommand : Commands to get data from database</param>
        /// <param name="oDGV">DataGridView : DataGrid source will be exported </param>
        /// <param name="sFilePath">string : Path to save the file</param>
        /// <param name="sLog">string :LogDescription to add new audit log</param>
        private void ExportFile(SqlCommand oSqlComm, DataGridView oDGV, string sFilePath, string sLog)
        {
            try
            {
                //XmlExport oExport = new XmlExport("Sheet1");
                //oExport.Author = UserData.sUserID;
                //oExport.CreationTime = DateTime.Now;

                //SqlDataReader oReader = oSqlComm.ExecuteReader();
                //oExport.Write(oGetColumnCollection(oDGV), oReader, sFilePath);
                //oReader.Close();
                //oDGV.Select();

                // change format to csv
                DataTable dtTemp = new DataTable();
                using (SqlDataReader reader = oSqlComm.ExecuteReader())
                    if (reader.HasRows)
                    {
                        dtTemp.Load(reader);
                        Csv.Write(dtTemp, sFilePath, '|');
                    }

                CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", sLog, "Saved as " + sFilePath);
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Set Form objects based on import status and UserRights
        /// </summary>
        /// <param name="isImport"></param>
        private void SetImportView(bool isImport)
        {
            btnDeleteDetail.Enabled = !isImport &&
                UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Delete);
            btnDeleteSummary.Enabled = !isImport &&
                UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Delete);

            btnExportDetail.Enabled = !isImport &&
               UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Add);
            btnExportSummary.Enabled = !isImport &&
                UserPrivilege.IsAllowed(PrivilegeCode.IT, Privilege.Add);

            isExcelMode = isImport;
        }

        /// <summary>
        ///  Imports logs from selected file.
        /// </summary>
        /// <param name="sFilePath">string : Path to save the file</param>
        /// <param name="oDGV">DataGridView : DataGrid destination where data imported will be shown</param>
        /// <param name="sLog">string :LogDescription to add new audit log</param>
        private void ImportFile(string sFilePath, DataGridView oDGV, string sLog)
        {
            bool isImportSuccess = false;
            try
            {
                oDGV.DataSource = new XmlImport(sImportSource = sFilePath).Read(eGetImportColumnTypes(oDGV));
                isImportSuccess = true;
                oDGV.Select();
                CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", sLog, "Opened from " + ofdImport.FileName);
                SetImportView(true);
            }
            catch (Exception oException)
            {
                string sErrMsg = isImportSuccess ? oException.Message : "Content format error.";
                MessageBox.Show(sErrMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Filters data grid view based on imported Excel file.
        /// </summary>
        /// <param name="sKey">Key to search</param>
        /// <param name="oTargetDGV">The corresponding data grid view</param>
        private void Search(string sKey, DataGridView oTargetDGV)
        {
            if (sKey.Trim().Length > 0) oTargetDGV.DataSource = new XmlImport(sImportSource).Read(eGetImportColumnTypes(oTargetDGV), sKey);
        }

        /// <summary>
        /// Filters data grid view.
        /// </summary>
        /// <param name="sKey">Key to search</param>
        /// <param name="sStoredProcedure">Stored procedure name</param>
        /// <param name="oTargetDGV">The corresponding data grid view</param>
        private void Search(string sKey, string sStoredProcedure, DataGridView oTargetDGV)
        {
            if (sKey.Trim().Length > 0)
            {
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                SqlCommand oSqlComm = new SqlCommand(sStoredProcedure, oSqlConn);
                oSqlComm.CommandType = CommandType.StoredProcedure;
                oSqlComm.Parameters.Add("@sKey", SqlDbType.VarChar).Value = sKey;

                SetDgView(oSqlComm, oTargetDGV);
            }
        }

        /// <summary>
        /// Delete Logs from oSourceDGV
        /// </summary>
        /// <param name="sStoredProcedure">string : Stored Procedure name</param>
        /// <param name="oSourceDGV">DataGridView : DataGrid source where logs will be deleted</param>
        /// <param name="sSavedLogsPath">string : Path where file for deleted logs saved</param>
        /// <param name="sLogMsg">string : Message saved in file</param>
        private void Delete(string sStoredProcedure, DataGridView oSourceDGV, string sSavedLogsPath, string sLogMsg)
        {
            try
            {
                FrmFilterInitTrail frmFilter = new FrmFilterInitTrail();
                if (frmFilter.ShowDialog() == DialogResult.OK && MessageBox.Show("Are you sure want to delete the logs?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    SqlParameter[] oSqlParameter = new SqlParameter[2];

                    oSqlParameter[0] = new SqlParameter("@dDateFrom", SqlDbType.DateTime);
                    oSqlParameter[0].Direction = ParameterDirection.Input;
                    oSqlParameter[0].Value = frmFilter.dateTimePickerFrom.Value;

                    oSqlParameter[1] = new SqlParameter("@dDateTo", SqlDbType.DateTime);
                    oSqlParameter[1].Direction = ParameterDirection.Input;
                    oSqlParameter[1].Value = frmFilter.dateTimePickerTo.Value;

                    SqlCommand oSqlComm = new SqlCommand(sStoredProcedure);
                    oSqlComm.CommandType = CommandType.StoredProcedure;
                    oSqlComm.Parameters.AddRange(oSqlParameter);

                    if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                    oSqlComm.Connection = oSqlConn;

                    SqlDataReader oReader = oSqlComm.ExecuteReader();
                    XmlExport oExport = new XmlExport("Sheet1");
                    oExport.Author = UserData.sUserID;
                    oExport.CreationTime = DateTime.Now;
                    oExport.Write(oGetColumnCollection(oSourceDGV), oReader, sSavedLogsPath);
                    oReader.Close();

                    RefreshView();
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", sLogMsg, "Deleted Logs Saved at " + sSavedLogsPath);
                }
            }
            catch (Exception oException)
            {
                File.Delete(sSavedLogsPath);
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Get the path where DeleteLogs file will be saved.
        /// </summary>
        /// <param name="sFileName">string : Name for the file</param>
        /// <returns>string : File's path</returns>
        private string sGetDeleteLogPath(string sFileName)
        {
            string sDelLogDir = Application.StartupPath + "\\DELETE LOGS";
            if (!Directory.Exists(sDelLogDir)) Directory.CreateDirectory(sDelLogDir);
            return sDelLogDir + "\\INIT " + sFileName + " " + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00") + " " + DateTime.Now.Hour.ToString("00") + ";" + DateTime.Now.Minute.ToString("00") + ";" + DateTime.Now.Second.ToString("00") + "," + DateTime.Now.Millisecond.ToString("000") + ".xml";
        }
        #endregion

        /// <summary>
        /// Exports Initialize Log summaries.
        /// </summary>
        private void btnExportSummary_Click(object sender, EventArgs e)
        {
            sfdExport.Filter = "Comma Separated Values(*.csv)|*.csv";
            if (sfdExport.ShowDialog() == DialogResult.OK) ExportFile(new SqlCommand(CommonSP.sSPInitTrailSummaryBrowse, oSqlConn), dgInitSum, sfdExport.FileName, CommonMessage.sInitTrailExportSummary);
            dgInitSum.Select();
        }

        /// <summary>
        /// Exports Initialize Log details.
        /// </summary>
        private void btnExportDetail_Click(object sender, EventArgs e)
        {
            sfdExport.Filter = "Comma Separated Values(*.csv)|*.csv";
            if (sfdExport.ShowDialog() == DialogResult.OK)
                if (isAuditInit)
                    ExportFile(new SqlCommand(CommonSP.sSPInitTrailBrowse, oSqlConn), dgInitDetail, sfdExport.FileName, CommonMessage.sInitTrailExportDetail);
                else
                    ExportFile(new SqlCommand(CommonSP.sSPInitSoftwareTrailBrowse, oSqlConn), dgInitDetail, sfdExport.FileName, CommonMessage.sInitSoftwareTrailExportDetail);
            dgInitDetail.Select();
        }

        /// <summary>
        /// Close the form from form Initialize Excel.
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        /// <summary>
        /// Close the form from form Initialize Logs Detail.
        /// </summary>
        private void btnCloseDetail_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        /// <summary>
        /// Close the form from form Initialize Logs Sumarry.
        /// </summary>
        private void btnCloseSum_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        /// <summary>
        /// Export data to Excel.
        /// </summary>
        private void btnExecute_Click(object sender, EventArgs e)
        {
            CommonClass.InputLog(oSqlConnAuditTrail, "", "", "", CommonMessage.sExportDataToExcel, "");
        }

        /// <summary>
        /// Searching in Initialize Logs detail.
        /// </summary>
        private void btnSearchDetail_Click(object sender, EventArgs e)
        {
            try
            {
                if (isExcelMode) Search(txtTIDdetail.Text, dgInitDetail);
                else
                    if (isAuditInit)
                    Search(txtTIDdetail.Text, CommonSP.sSPInitTrailBrowse, dgInitDetail);
                else
                    Search(txtTIDdetail.Text, CommonSP.sSPInitSoftwareTrailBrowse, dgInitDetail);
                dgInitDetail.Select();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Runs when focus on txtTIDdetail and enter is pressed, search logs based on keys in txtTIDdetail on Initialize Detail.
        /// </summary>
        private void txtTIDdetail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
                btnSearchDetail.PerformClick();
            txtTIDdetail.Focus();
        }

        /// <summary>
        /// Search TID when button search on Initialize Summary is clicked.
        /// </summary>
        private void btnSearchSum_Click(object sender, EventArgs e)
        {
            try
            {
                if (isExcelMode) Search(txtTIDSum.Text, dgInitSum);
                else Search(txtTIDSum.Text, CommonSP.sSPInitTrailSummaryBrowse, dgInitSum);
                dgInitSum.Select();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Runs when focus on txtTIDdetail and enter is pressed, search logs based on keys in txtTIDdetail on Initialize Summary.
        /// </summary>
        private void txtTIDSum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
                btnSearchSum.PerformClick();
            txtTIDSum.Focus();
        }

        /// <summary>
        /// Imports logs detail when button is clicked.
        /// </summary>
        private void btnImportDetail_Click(object sender, EventArgs e)
        {
            //if (ofdImport.ShowDialog() == DialogResult.OK) ImportFile(ofdImport.FileName, dgInitDetail, CommonMessage.sInitTrailImportDetail);
            //dgInitDetail.Select();

            ofdImport.Filter = "Comma Separated Values(*.csv)|*.csv";
            //if (ofdImport.ShowDialog() == DialogResult.OK)
            //    if (isAuditInit)
            //        ImportFile(ofdImport.FileName, dgInitDetail, CommonMessage.sInitTrailImportDetail);
            //    else
            //        ImportFile(ofdImport.FileName, dgInitDetail, CommonMessage.sInitSoftwareTrailImportDetail);
            //dgInitDetail.Select();
            if (ofdImport.ShowDialog() == DialogResult.OK)
            {
                string sFileName = ofdImport.FileName.Trim();
                string sPath = sFileName.Substring(0, sFileName.LastIndexOf("\\"));
                string sSaveFile = ofdImport.SafeFileName;
                string sFormat = "CSVDelimited";
                bool bHeader = true;
                //MessageBox.Show(sSaveFile);

                try
                {
                    FileStream fsOutput = new FileStream(sPath + "\\schema.ini", FileMode.Create, FileAccess.Write);
                    StreamWriter srOutput = new StreamWriter(fsOutput);
                    string s1, s2, s3;
                    s1 = "[" + sSaveFile + "]";
                    s2 = "ColNameHeader=" + bHeader.ToString();
                    s3 = "Format=" + sFormat;
                    srOutput.WriteLine(s1.ToString() + '\n' + s2.ToString() + '\n' + s3.ToString() /*+ '\n' + s4.ToString() + '\n' + s5.ToString()*/);
                    srOutput.Close();
                    fsOutput.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                { }
                ConnectCSV(sSaveFile);
                EmptyInitTrailTemp();
                SaveToDatabase();
            }

        }

        private void EmptyInitTrailTemp()
        {
            SqlCommand cmdDeleteTemp = new SqlCommand(CommonSP.sSPDeleteTempInitTrail, oSqlConn);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            cmdDeleteTemp.ExecuteNonQuery();
        }

        private void SaveToDatabase()
        {
            if (dgInitDetail.RowCount > 1)
            {
                for (int x = 0; x <= dgInitDetail.RowCount - 1; x++)
                {
                    if (!string.IsNullOrEmpty(dgInitDetail.Rows[x].Cells[0].Value.ToString()))

                    {


                        SqlCommand cmdSave = new SqlCommand(CommonSP.sSPInsertInitTrailTemp, oSqlConn);
                        {
                            cmdSave.CommandType = CommandType.StoredProcedure;
                            cmdSave.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = dgInitDetail.Rows[x].Cells[0].Value;
                            cmdSave.Parameters.Add("@sSoftware", SqlDbType.VarChar).Value = dgInitDetail.Rows[x].Cells[1].Value;
                            cmdSave.Parameters.Add("@sSerialNum", SqlDbType.VarChar).Value = dgInitDetail.Rows[x].Cells[2].Value;
                            cmdSave.Parameters.Add("@sInitTime", SqlDbType.VarChar).Value = dgInitDetail.Rows[x].Cells[3].Value;
                            cmdSave.Parameters.Add("@sStatus", SqlDbType.VarChar).Value = dgInitDetail.Rows[x].Cells[4].Value;
                        }
                        if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                        cmdSave.ExecuteNonQuery();

                    }
                }

            }
        }

        public DataSet ConnectCSV(string filetable)
        {
            DataSet ds = new DataSet();
            try
            {
                // You can get connected to driver either by using DSN or connection string

                // Create a connection string as below, if you want to use DSN less connection. The DBQ attribute sets the path of directory which contains CSV files
                string strConnString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" + ofdImport.FileName.Trim().Substring(0, ofdImport.FileName.Trim().LastIndexOf("\\")) + ";Extensions=asc,csv,tab,txt;Persist Security Info=False";
                string sql_select;
                System.Data.Odbc.OdbcConnection conn;

                //Create connection to CSV file
                conn = new System.Data.Odbc.OdbcConnection(strConnString.Trim());

                // For creating a connection using DSN, use following line
                //conn	=	new System.Data.Odbc.OdbcConnection(DSN="MyDSN");

                //Open the connection 
                conn.Open();
                //Fetch records from CSV
                sql_select = "select * from [" + filetable + "]";

                oOledbDataAdapter = new System.Data.Odbc.OdbcDataAdapter(sql_select, conn);
                //Fill dataset with the records from CSV file
                oOledbDataAdapter.Fill(ds, "Import File");


                //Set the datagrid properties
                dgInitDetail.DataSource = ds;
                dgInitDetail.DataMember = "Import File";
                dgInitDetail.Columns.Remove("NoName");
                //Close Connection to CSV file
                conn.Close();
            }

            catch (Exception err) //Error
            {
                MessageBox.Show(err.Message);
            }
            return ds;

        }


        /// <summary>
        /// Imports logs summary when button is clicked.
        /// </summary>
        private void btnImportSummary_Click(object sender, EventArgs e)
        {
            if (ofdImport.ShowDialog() == DialogResult.OK) ImportFile(ofdImport.FileName, dgInitSum, CommonMessage.sInitTrailImportSummary);
            dgInitSum.Select();
        }

        /// <summary>
        /// Delete logs detail when button is clicked.
        /// </summary>
        private void btnDeleteDetail_Click(object sender, EventArgs e)
        {
            if (isAuditInit)
                Delete(CommonSP.sSPInitTrailDelete, dgInitDetail, sGetDeleteLogPath("DETAIL"), CommonMessage.sInitTrailDeleteDetail);
            else
                Delete(CommonSP.sSPInitSoftwareTrailDelete, dgInitDetail, sGetDeleteLogPath("DETAIL"), CommonMessage.sInitSoftwareTrailDeleteDetail);
        }

        /// <summary>
        /// Delete logs summary when button is clicked.
        /// </summary>
        private void btnDeleteSummary_Click(object sender, EventArgs e)
        {
            Delete(CommonSP.sSPInitTrailSummaryDelete, dgInitSum, sGetDeleteLogPath("SUMMARY"), CommonMessage.sInitTrailDeleteSummary);
        }

        /// <summary>
        /// Refres DataGridView detail, reload data from database.
        /// </summary>
        private void btnFreshLIDetail_Click(object sender, EventArgs e)
        {
            try
            {
                RefreshView();
                dgInitDetail.Select();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Refresh DataGridView summary, reload data from database to be shown.
        /// </summary>
        private void btnFreshLISum_Click(object sender, EventArgs e)
        {
            try
            {
                RefreshView();
                dgInitSum.Select();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}