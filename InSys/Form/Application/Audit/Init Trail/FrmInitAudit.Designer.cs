namespace InSys
{
    partial class FrmInitAudit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInitAudit));
            this.TabAuditInitDetail = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSearchDetail = new System.Windows.Forms.Button();
            this.txtTIDdetail = new System.Windows.Forms.TextBox();
            this.dgInitDetail = new System.Windows.Forms.DataGridView();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnDeleteDetail = new System.Windows.Forms.Button();
            this.btnImportDetail = new System.Windows.Forms.Button();
            this.btnExportDetail = new System.Windows.Forms.Button();
            this.btnCloseDetail = new System.Windows.Forms.Button();
            this.btnFreshLIDetail = new System.Windows.Forms.Button();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.TabAuditInit = new System.Windows.Forms.TabControl();
            this.TabColExcel = new System.Windows.Forms.TabPage();
            this.btnClose = new System.Windows.Forms.Button();
            this.gbXLS = new System.Windows.Forms.GroupBox();
            this.btnExecute = new System.Windows.Forms.Button();
            this.rdUndownloadedData = new System.Windows.Forms.RadioButton();
            this.rdDownloadedData = new System.Windows.Forms.RadioButton();
            this.rdAllData = new System.Windows.Forms.RadioButton();
            this.cmbDb = new System.Windows.Forms.ComboBox();
            this.TabAuditInitSum = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnSearchSum = new System.Windows.Forms.Button();
            this.txtTIDSum = new System.Windows.Forms.TextBox();
            this.dgInitSum = new System.Windows.Forms.DataGridView();
            this.gbButtonSum = new System.Windows.Forms.GroupBox();
            this.btnDeleteSummary = new System.Windows.Forms.Button();
            this.btnImportSummary = new System.Windows.Forms.Button();
            this.btnExportSummary = new System.Windows.Forms.Button();
            this.btnCloseSum = new System.Windows.Forms.Button();
            this.btnFreshLISum = new System.Windows.Forms.Button();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.sfdExport = new System.Windows.Forms.SaveFileDialog();
            this.ofdImport = new System.Windows.Forms.OpenFileDialog();
            this.TabAuditInitDetail.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitDetail)).BeginInit();
            this.gbButton.SuspendLayout();
            this.TabAuditInit.SuspendLayout();
            this.TabColExcel.SuspendLayout();
            this.gbXLS.SuspendLayout();
            this.TabAuditInitSum.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitSum)).BeginInit();
            this.gbButtonSum.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // TabAuditInitDetail
            // 
            this.TabAuditInitDetail.Controls.Add(this.groupBox2);
            this.TabAuditInitDetail.Controls.Add(this.dgInitDetail);
            this.TabAuditInitDetail.Controls.Add(this.gbButton);
            this.TabAuditInitDetail.Location = new System.Drawing.Point(4, 26);
            this.TabAuditInitDetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TabAuditInitDetail.Name = "TabAuditInitDetail";
            this.TabAuditInitDetail.Size = new System.Drawing.Size(1029, 523);
            this.TabAuditInitDetail.TabIndex = 1;
            this.TabAuditInitDetail.Text = "Init Profile Log Detail";
            this.TabAuditInitDetail.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnSearchDetail);
            this.groupBox2.Controls.Add(this.txtTIDdetail);
            this.groupBox2.Location = new System.Drawing.Point(715, 4);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(288, 52);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // btnSearchDetail
            // 
            this.btnSearchDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchDetail.Location = new System.Drawing.Point(205, 16);
            this.btnSearchDetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSearchDetail.Name = "btnSearchDetail";
            this.btnSearchDetail.Size = new System.Drawing.Size(76, 28);
            this.btnSearchDetail.TabIndex = 1;
            this.btnSearchDetail.Text = "Search";
            this.btnSearchDetail.Click += new System.EventHandler(this.btnSearchDetail_Click);
            // 
            // txtTIDdetail
            // 
            this.txtTIDdetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTIDdetail.Location = new System.Drawing.Point(8, 17);
            this.txtTIDdetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTIDdetail.Name = "txtTIDdetail";
            this.txtTIDdetail.Size = new System.Drawing.Size(188, 25);
            this.txtTIDdetail.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtTIDdetail, "Enter text to search.");
            this.txtTIDdetail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTIDdetail_KeyPress);
            // 
            // dgInitDetail
            // 
            this.dgInitDetail.AllowUserToAddRows = false;
            this.dgInitDetail.AllowUserToDeleteRows = false;
            this.dgInitDetail.AllowUserToResizeRows = false;
            this.dgInitDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgInitDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgInitDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgInitDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInitDetail.Location = new System.Drawing.Point(21, 66);
            this.dgInitDetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgInitDetail.MultiSelect = false;
            this.dgInitDetail.Name = "dgInitDetail";
            this.dgInitDetail.ReadOnly = true;
            this.dgInitDetail.RowHeadersVisible = false;
            this.dgInitDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgInitDetail.Size = new System.Drawing.Size(981, 390);
            this.dgInitDetail.TabIndex = 0;
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnDeleteDetail);
            this.gbButton.Controls.Add(this.btnImportDetail);
            this.gbButton.Controls.Add(this.btnExportDetail);
            this.gbButton.Controls.Add(this.btnCloseDetail);
            this.gbButton.Controls.Add(this.btnFreshLIDetail);
            this.gbButton.Location = new System.Drawing.Point(21, 457);
            this.gbButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Name = "gbButton";
            this.gbButton.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Size = new System.Drawing.Size(981, 54);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnDeleteDetail
            // 
            this.btnDeleteDetail.Location = new System.Drawing.Point(512, 17);
            this.btnDeleteDetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDeleteDetail.Name = "btnDeleteDetail";
            this.btnDeleteDetail.Size = new System.Drawing.Size(160, 30);
            this.btnDeleteDetail.TabIndex = 4;
            this.btnDeleteDetail.Text = "Delete";
            this.btnDeleteDetail.UseVisualStyleBackColor = true;
            this.btnDeleteDetail.Click += new System.EventHandler(this.btnDeleteDetail_Click);
            // 
            // btnImportDetail
            // 
            this.btnImportDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImportDetail.Location = new System.Drawing.Point(176, 17);
            this.btnImportDetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnImportDetail.Name = "btnImportDetail";
            this.btnImportDetail.Size = new System.Drawing.Size(160, 30);
            this.btnImportDetail.TabIndex = 3;
            this.btnImportDetail.Text = "Import";
            this.btnImportDetail.UseVisualStyleBackColor = true;
            this.btnImportDetail.Click += new System.EventHandler(this.btnImportDetail_Click);
            // 
            // btnExportDetail
            // 
            this.btnExportDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportDetail.Location = new System.Drawing.Point(344, 17);
            this.btnExportDetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExportDetail.Name = "btnExportDetail";
            this.btnExportDetail.Size = new System.Drawing.Size(160, 30);
            this.btnExportDetail.TabIndex = 2;
            this.btnExportDetail.Text = "Export";
            this.ToolTip1.SetToolTip(this.btnExportDetail, "Export to Excel(*.xls) Files");
            this.btnExportDetail.Click += new System.EventHandler(this.btnExportDetail_Click);
            // 
            // btnCloseDetail
            // 
            this.btnCloseDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseDetail.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCloseDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseDetail.Location = new System.Drawing.Point(813, 17);
            this.btnCloseDetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCloseDetail.Name = "btnCloseDetail";
            this.btnCloseDetail.Size = new System.Drawing.Size(160, 30);
            this.btnCloseDetail.TabIndex = 0;
            this.btnCloseDetail.Text = "&Close";
            this.btnCloseDetail.Click += new System.EventHandler(this.btnCloseDetail_Click);
            // 
            // btnFreshLIDetail
            // 
            this.btnFreshLIDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFreshLIDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFreshLIDetail.Location = new System.Drawing.Point(8, 17);
            this.btnFreshLIDetail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFreshLIDetail.Name = "btnFreshLIDetail";
            this.btnFreshLIDetail.Size = new System.Drawing.Size(160, 30);
            this.btnFreshLIDetail.TabIndex = 1;
            this.btnFreshLIDetail.Text = "&Refresh";
            this.btnFreshLIDetail.Click += new System.EventHandler(this.btnFreshLIDetail_Click);
            // 
            // lblDatabase
            // 
            this.lblDatabase.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabase.Location = new System.Drawing.Point(21, 30);
            this.lblDatabase.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(133, 28);
            this.lblDatabase.TabIndex = 95;
            this.lblDatabase.Text = "Database";
            this.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Progress
            // 
            this.Progress.Location = new System.Drawing.Point(8, 138);
            this.Progress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(679, 20);
            this.Progress.Step = 1;
            this.Progress.TabIndex = 94;
            // 
            // TabAuditInit
            // 
            this.TabAuditInit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabAuditInit.Controls.Add(this.TabColExcel);
            this.TabAuditInit.Controls.Add(this.TabAuditInitDetail);
            this.TabAuditInit.Controls.Add(this.TabAuditInitSum);
            this.TabAuditInit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabAuditInit.Location = new System.Drawing.Point(16, 15);
            this.TabAuditInit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TabAuditInit.Name = "TabAuditInit";
            this.TabAuditInit.SelectedIndex = 0;
            this.TabAuditInit.Size = new System.Drawing.Size(1037, 553);
            this.TabAuditInit.TabIndex = 2;
            // 
            // TabColExcel
            // 
            this.TabColExcel.Controls.Add(this.btnClose);
            this.TabColExcel.Controls.Add(this.gbXLS);
            this.TabColExcel.Location = new System.Drawing.Point(4, 26);
            this.TabColExcel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TabColExcel.Name = "TabColExcel";
            this.TabColExcel.Size = new System.Drawing.Size(1029, 523);
            this.TabColExcel.TabIndex = 0;
            this.TabColExcel.Text = "Excel";
            this.TabColExcel.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(567, 244);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(149, 30);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gbXLS
            // 
            this.gbXLS.Controls.Add(this.btnExecute);
            this.gbXLS.Controls.Add(this.lblDatabase);
            this.gbXLS.Controls.Add(this.Progress);
            this.gbXLS.Controls.Add(this.rdUndownloadedData);
            this.gbXLS.Controls.Add(this.rdDownloadedData);
            this.gbXLS.Controls.Add(this.rdAllData);
            this.gbXLS.Controls.Add(this.cmbDb);
            this.gbXLS.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbXLS.Location = new System.Drawing.Point(21, 20);
            this.gbXLS.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbXLS.Name = "gbXLS";
            this.gbXLS.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbXLS.Size = new System.Drawing.Size(695, 203);
            this.gbXLS.TabIndex = 0;
            this.gbXLS.TabStop = false;
            this.gbXLS.Text = "Export to Excel Files";
            // 
            // btnExecute
            // 
            this.btnExecute.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExecute.Location = new System.Drawing.Point(8, 166);
            this.btnExecute.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(149, 30);
            this.btnExecute.TabIndex = 5;
            this.btnExecute.Text = "E&xecute";
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // rdUndownloadedData
            // 
            this.rdUndownloadedData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdUndownloadedData.Location = new System.Drawing.Point(352, 79);
            this.rdUndownloadedData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdUndownloadedData.Name = "rdUndownloadedData";
            this.rdUndownloadedData.Size = new System.Drawing.Size(197, 30);
            this.rdUndownloadedData.TabIndex = 4;
            this.rdUndownloadedData.Text = "Undownloaded Data";
            // 
            // rdDownloadedData
            // 
            this.rdDownloadedData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdDownloadedData.Location = new System.Drawing.Point(139, 79);
            this.rdDownloadedData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdDownloadedData.Name = "rdDownloadedData";
            this.rdDownloadedData.Size = new System.Drawing.Size(203, 30);
            this.rdDownloadedData.TabIndex = 3;
            this.rdDownloadedData.Text = "Downloaded Data";
            // 
            // rdAllData
            // 
            this.rdAllData.Checked = true;
            this.rdAllData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdAllData.Location = new System.Drawing.Point(21, 79);
            this.rdAllData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdAllData.Name = "rdAllData";
            this.rdAllData.Size = new System.Drawing.Size(107, 30);
            this.rdAllData.TabIndex = 2;
            this.rdAllData.TabStop = true;
            this.rdAllData.Text = "All Data";
            // 
            // cmbDb
            // 
            this.cmbDb.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDb.Location = new System.Drawing.Point(245, 30);
            this.cmbDb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbDb.Name = "cmbDb";
            this.cmbDb.Size = new System.Drawing.Size(212, 25);
            this.cmbDb.TabIndex = 1;
            // 
            // TabAuditInitSum
            // 
            this.TabAuditInitSum.Controls.Add(this.groupBox4);
            this.TabAuditInitSum.Controls.Add(this.dgInitSum);
            this.TabAuditInitSum.Controls.Add(this.gbButtonSum);
            this.TabAuditInitSum.Location = new System.Drawing.Point(4, 26);
            this.TabAuditInitSum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TabAuditInitSum.Name = "TabAuditInitSum";
            this.TabAuditInitSum.Size = new System.Drawing.Size(1029, 523);
            this.TabAuditInitSum.TabIndex = 2;
            this.TabAuditInitSum.Text = "Init Profile Log Summary";
            this.TabAuditInitSum.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.btnSearchSum);
            this.groupBox4.Controls.Add(this.txtTIDSum);
            this.groupBox4.Location = new System.Drawing.Point(736, 4);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(267, 52);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            // 
            // btnSearchSum
            // 
            this.btnSearchSum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchSum.Location = new System.Drawing.Point(205, 16);
            this.btnSearchSum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSearchSum.Name = "btnSearchSum";
            this.btnSearchSum.Size = new System.Drawing.Size(53, 28);
            this.btnSearchSum.TabIndex = 1;
            this.btnSearchSum.Text = "&Go";
            this.btnSearchSum.Click += new System.EventHandler(this.btnSearchSum_Click);
            // 
            // txtTIDSum
            // 
            this.txtTIDSum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTIDSum.Location = new System.Drawing.Point(8, 17);
            this.txtTIDSum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTIDSum.Name = "txtTIDSum";
            this.txtTIDSum.Size = new System.Drawing.Size(188, 25);
            this.txtTIDSum.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtTIDSum, "Enter text to search.");
            this.txtTIDSum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTIDSum_KeyPress);
            // 
            // dgInitSum
            // 
            this.dgInitSum.AllowUserToAddRows = false;
            this.dgInitSum.AllowUserToDeleteRows = false;
            this.dgInitSum.AllowUserToResizeRows = false;
            this.dgInitSum.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgInitSum.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgInitSum.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgInitSum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInitSum.Location = new System.Drawing.Point(21, 66);
            this.dgInitSum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgInitSum.MultiSelect = false;
            this.dgInitSum.Name = "dgInitSum";
            this.dgInitSum.ReadOnly = true;
            this.dgInitSum.RowHeadersVisible = false;
            this.dgInitSum.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInitSum.Size = new System.Drawing.Size(981, 390);
            this.dgInitSum.TabIndex = 0;
            // 
            // gbButtonSum
            // 
            this.gbButtonSum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButtonSum.Controls.Add(this.btnDeleteSummary);
            this.gbButtonSum.Controls.Add(this.btnImportSummary);
            this.gbButtonSum.Controls.Add(this.btnExportSummary);
            this.gbButtonSum.Controls.Add(this.btnCloseSum);
            this.gbButtonSum.Controls.Add(this.btnFreshLISum);
            this.gbButtonSum.Location = new System.Drawing.Point(21, 457);
            this.gbButtonSum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButtonSum.Name = "gbButtonSum";
            this.gbButtonSum.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButtonSum.Size = new System.Drawing.Size(981, 54);
            this.gbButtonSum.TabIndex = 2;
            this.gbButtonSum.TabStop = false;
            // 
            // btnDeleteSummary
            // 
            this.btnDeleteSummary.Location = new System.Drawing.Point(512, 17);
            this.btnDeleteSummary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDeleteSummary.Name = "btnDeleteSummary";
            this.btnDeleteSummary.Size = new System.Drawing.Size(160, 30);
            this.btnDeleteSummary.TabIndex = 4;
            this.btnDeleteSummary.Text = "Delete";
            this.btnDeleteSummary.UseVisualStyleBackColor = true;
            this.btnDeleteSummary.Click += new System.EventHandler(this.btnDeleteSummary_Click);
            // 
            // btnImportSummary
            // 
            this.btnImportSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnImportSummary.Location = new System.Drawing.Point(176, 17);
            this.btnImportSummary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnImportSummary.Name = "btnImportSummary";
            this.btnImportSummary.Size = new System.Drawing.Size(160, 30);
            this.btnImportSummary.TabIndex = 3;
            this.btnImportSummary.Text = "Import";
            this.btnImportSummary.UseVisualStyleBackColor = true;
            this.btnImportSummary.Click += new System.EventHandler(this.btnImportSummary_Click);
            // 
            // btnExportSummary
            // 
            this.btnExportSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExportSummary.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportSummary.Location = new System.Drawing.Point(344, 17);
            this.btnExportSummary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExportSummary.Name = "btnExportSummary";
            this.btnExportSummary.Size = new System.Drawing.Size(160, 30);
            this.btnExportSummary.TabIndex = 2;
            this.btnExportSummary.Text = "Export";
            this.ToolTip1.SetToolTip(this.btnExportSummary, "Export to Excel(*.xls) Files");
            this.btnExportSummary.Click += new System.EventHandler(this.btnExportSummary_Click);
            // 
            // btnCloseSum
            // 
            this.btnCloseSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseSum.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCloseSum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseSum.Location = new System.Drawing.Point(813, 17);
            this.btnCloseSum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCloseSum.Name = "btnCloseSum";
            this.btnCloseSum.Size = new System.Drawing.Size(160, 30);
            this.btnCloseSum.TabIndex = 0;
            this.btnCloseSum.Text = "&Close";
            this.btnCloseSum.Click += new System.EventHandler(this.btnCloseSum_Click);
            // 
            // btnFreshLISum
            // 
            this.btnFreshLISum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFreshLISum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFreshLISum.Location = new System.Drawing.Point(8, 17);
            this.btnFreshLISum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFreshLISum.Name = "btnFreshLISum";
            this.btnFreshLISum.Size = new System.Drawing.Size(160, 30);
            this.btnFreshLISum.TabIndex = 1;
            this.btnFreshLISum.Text = "&Refresh";
            this.btnFreshLISum.Click += new System.EventHandler(this.btnFreshLISum_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(324, 14);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Export";
            this.ToolTip1.SetToolTip(this.button3, "Export to Excel(*.xls) Files");
            // 
            // textBox1
            // 
            this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(6, 14);
            this.textBox1.MaxLength = 8;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(72, 25);
            this.textBox1.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.textBox1, "Enter the TID and Press Go button to search");
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(610, 466);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Excel";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(425, 198);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 24);
            this.button1.TabIndex = 93;
            this.button1.Text = "&Close";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton5);
            this.groupBox1.Controls.Add(this.radioButton6);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(521, 165);
            this.groupBox1.TabIndex = 88;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Export to Excel Files";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(204, 134);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 24);
            this.button2.TabIndex = 89;
            this.button2.Text = "E&xecute";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 95;
            this.label2.Text = "Database";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(16, 112);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(496, 16);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 94;
            // 
            // radioButton4
            // 
            this.radioButton4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton4.Location = new System.Drawing.Point(264, 64);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(148, 24);
            this.radioButton4.TabIndex = 92;
            this.radioButton4.Text = "Undownloaded Data";
            // 
            // radioButton5
            // 
            this.radioButton5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton5.Location = new System.Drawing.Point(104, 64);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(152, 24);
            this.radioButton5.TabIndex = 91;
            this.radioButton5.Text = "Downloaded Data";
            // 
            // radioButton6
            // 
            this.radioButton6.Checked = true;
            this.radioButton6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton6.Location = new System.Drawing.Point(16, 64);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(80, 24);
            this.radioButton6.TabIndex = 90;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "All Data";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.Location = new System.Drawing.Point(184, 24);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(160, 25);
            this.comboBox1.TabIndex = 88;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.button7);
            this.tabPage2.Controls.Add(this.dataGrid1);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(610, 466);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Audit Init Detail";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Location = new System.Drawing.Point(16, 382);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(576, 44);
            this.groupBox3.TabIndex = 96;
            this.groupBox3.TabStop = false;
            // 
            // button4
            // 
            this.button4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(450, 14);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(120, 24);
            this.button4.TabIndex = 95;
            this.button4.Text = "&Summary";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(155, 14);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(120, 23);
            this.button5.TabIndex = 1;
            this.button5.Text = "&Refresh";
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(84, 15);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(40, 23);
            this.button6.TabIndex = 1;
            this.button6.Text = "&Go";
            // 
            // button7
            // 
            this.button7.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(238, 432);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(120, 24);
            this.button7.TabIndex = 94;
            this.button7.Text = "&Close";
            // 
            // dataGrid1
            // 
            this.dataGrid1.DataMember = "";
            this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGrid1.Location = new System.Drawing.Point(16, 16);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.PreferredColumnWidth = 100;
            this.dataGrid1.PreferredRowHeight = 15;
            this.dataGrid1.ReadOnly = true;
            this.dataGrid1.RowHeaderWidth = 15;
            this.dataGrid1.Size = new System.Drawing.Size(576, 360);
            this.dataGrid1.TabIndex = 0;
            // 
            // sfdExport
            // 
            this.sfdExport.Filter = "XML Spreadsheet(*.xml)|*.xml";
            // 
            // ofdImport
            // 
            this.ofdImport.Filter = "XML Spreadsheet(*.xml)|*.xml";
            // 
            // FrmInitAudit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 574);
            this.Controls.Add(this.TabAuditInit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(1030, 262);
            this.Name = "FrmInitAudit";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Audit Init";
            this.Activated += new System.EventHandler(this.FrmInitAudit_Activated);
            this.Load += new System.EventHandler(this.FrmInitAudit_Load);
            this.TabAuditInitDetail.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitDetail)).EndInit();
            this.gbButton.ResumeLayout(false);
            this.TabAuditInit.ResumeLayout(false);
            this.TabColExcel.ResumeLayout(false);
            this.gbXLS.ResumeLayout(false);
            this.TabAuditInitSum.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitSum)).EndInit();
            this.gbButtonSum.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TabPage TabAuditInitDetail;
        internal System.Windows.Forms.Button btnCloseDetail;
        internal System.Windows.Forms.Button btnExportDetail;
        internal System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.Button btnFreshLIDetail;
        internal System.Windows.Forms.TextBox txtTIDdetail;
        internal System.Windows.Forms.Button btnSearchDetail;
        internal System.Windows.Forms.Label lblDatabase;
        internal System.Windows.Forms.ProgressBar Progress;
        internal System.Windows.Forms.TabControl TabAuditInit;
        internal System.Windows.Forms.TabPage TabColExcel;
        internal System.Windows.Forms.GroupBox gbXLS;
        internal System.Windows.Forms.RadioButton rdUndownloadedData;
        internal System.Windows.Forms.RadioButton rdDownloadedData;
        internal System.Windows.Forms.RadioButton rdAllData;
        internal System.Windows.Forms.Button btnExecute;
        internal System.Windows.Forms.ComboBox cmbDb;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.TabPage TabAuditInitSum;
        private System.Windows.Forms.GroupBox gbButtonSum;
        internal System.Windows.Forms.Button btnExportSummary;
        internal System.Windows.Forms.Button btnFreshLISum;
        internal System.Windows.Forms.TextBox txtTIDSum;
        internal System.Windows.Forms.Button btnSearchSum;
        internal System.Windows.Forms.Button btnCloseSum;
        internal System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.Button button1;
        internal System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Button button2;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.ProgressBar progressBar1;
        internal System.Windows.Forms.RadioButton radioButton4;
        internal System.Windows.Forms.RadioButton radioButton5;
        internal System.Windows.Forms.RadioButton radioButton6;
        internal System.Windows.Forms.ComboBox comboBox1;
        internal System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Button button3;
        internal System.Windows.Forms.Button button4;
        internal System.Windows.Forms.Button button5;
        internal System.Windows.Forms.TextBox textBox1;
        internal System.Windows.Forms.Button button6;
        internal System.Windows.Forms.Button button7;
        internal System.Windows.Forms.DataGrid dataGrid1;
        private System.Windows.Forms.SaveFileDialog sfdExport;
        private System.Windows.Forms.DataGridView dgInitDetail;
        private System.Windows.Forms.DataGridView dgInitSum;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnImportDetail;
        private System.Windows.Forms.Button btnImportSummary;
        private System.Windows.Forms.OpenFileDialog ofdImport;
        private System.Windows.Forms.Button btnDeleteDetail;
        private System.Windows.Forms.Button btnDeleteSummary;
    }
}