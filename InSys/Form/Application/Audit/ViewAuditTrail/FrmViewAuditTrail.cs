﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
//using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Globalization;


namespace InSys
{
    public partial class FrmViewAuditTrail : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        SqlDataAdapter da = new SqlDataAdapter();
        DataTable dtSource = new DataTable();
        public static string sCase;
        public static string sFlag;
        public static string sType;
        public FrmViewAuditTrail(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            sCase = "View"; sFlag = "Start"; sType = "View";
            StartAndStopProgress(sFlag, sType);
            backgroundWorker1.RunWorkerAsync();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            sCase = "Export"; sFlag = "Start"; sType = "Export";
            StartAndStopProgress(sFlag, sType);
            backgroundWorker1.RunWorkerAsync();
        }

        public void StartAndStopProgress(string _sFlag, string _sType)
        {
            if (_sFlag == "Start")
            {
                btnView.Enabled = false;
                btnExport.Enabled = false;
                progressBar1.Value = 0;
                progressBar1.Maximum = 100;
                progressBar1.Style = ProgressBarStyle.Marquee;
            }
            else
            {
                btnView.Enabled = true;
                btnExport.Enabled = true;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 100;
                if (_sType == "View")
                {
                    dataGridView1.DataSource = dtSource;
                }
                if (_sType == "Export")
                {

                    MessageBox.Show("Export Data Audit User Done");
                }

            }
        }

        public DataTable dtGetData()
        {
            dtSource.Rows.Clear();
            string stgl = Convert.ToDateTime(dtpDateFrom.Value).ToString("dd/MM/yyyy");
            SqlCommand cmd = new SqlCommand("spAuditInitView", oSqlConn);
            cmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = dtpDateFrom.Value.ToString("MM/dd/yyyy");
            cmd.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = cmd;
            da.Fill(dtSource);
            return dtSource;

        }

        private void CreateFileExcel(DataTable _dt)
        {
            DataTable dt = _dt;
            string sDirectoryAppFile = string.Format(@"{0}\{1}ReportAuditUser.xls", Directory.GetCurrentDirectory(), DateTime.Now.ToString("ddMMyyyy"));
            ExcelOleDb excelAuditTrail = new ExcelOleDb(sDirectoryAppFile);
            excelAuditTrail.CreateWorkSheet(dt);

            //int i = 1;
            //int j = 1;
            //Excel.Application xlApp = new Excel.Application();
            ////  string sPathFile = Directory.GetCurrentDirectory();
            //xlApp.Application.Workbooks.Add(Type.Missing);
            //xlApp.Cells.NumberFormat = "@";
            //foreach (DataColumn col in dt.Columns)
            //{
            //    xlApp.Cells[i, j] = col.ColumnName;
            //    j++;
            //}
            //i++;

            //foreach (DataRow row in dt.Rows)
            //{
            //    for (int k = 1; k < dt.Columns.Count + 1; k++)
            //    {

            //        xlApp.Cells[i, k] = row[k - 1].ToString();
            //    }
            //    i++;

            //}
            //xlApp.ActiveWorkbook.SaveCopyAs(sDirectoryAppFile);
            //xlApp.ActiveWorkbook.Saved = true;
            //xlApp.Quit();
        }



        private CultureInfo SetCultureInfo(string sCultName)
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(sCultName);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                SetCultureInfo("en-US");
                if (sCase == "Export")
                {
                    CreateFileExcel(dtGetData());
                }
                else
                {
                    dtGetData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 100)
            {
                progressBar1.Style = ProgressBarStyle.Blocks;
                this.Cursor = Cursors.Default;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StartAndStopProgress("Stop", sType);
        }
    }
}
