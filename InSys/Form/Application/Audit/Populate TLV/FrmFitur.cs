using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using InSysClass;

namespace InSys
{
    public partial class FrmFitur : Form
    {
        protected SqlConnection oSqlConn;
        protected string sDatabaseId = null;

        public FrmFitur(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        private void FrmFitur_Load(object sender, EventArgs e)
        {
            DataTable dtDatabase = new DataTable();
            InitComboBoxDatabase(ref dtDatabase);
            LoadComboBoxDatabase(dtDatabase);
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            progressbarProcess.Style = ProgressBarStyle.Marquee;

            ResetAll();
            sDatabaseId = DatabaseId;
            btnExecute.Enabled = false;
            cbDatabase.Enabled = false;

            bw.RunWorkerAsync();

            //LoadViewFitur();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Abort Process?", "Abort", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                bw.CancelAsync();
                this.Close();
                this.Dispose();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            bw.CancelAsync();
            this.Close();
            this.Dispose();
        }

        protected void InitConnection()
        {
            if (!isHaveConfiguration())
            {
                MessageBox.Show("Error Connection string file.");
                this.Close();
            }
            else
                oSqlConn = new SqlConnection(new InitData().sGetConnString());
        }

        protected bool isHaveConfiguration()
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";

            if (File.Exists(sFileName)) return true;
            else return false;
        }

        protected void InitComboBoxDatabase(ref DataTable dtDatabase)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            if (oSqlConn.State == ConnectionState.Closed)
                    oSqlConn.Open();
            SqlDataReader oReadDb = oCmd.ExecuteReader();
            if (oReadDb.HasRows)
                dtDatabase.Load(oReadDb);
            oReadDb.Close();
            oReadDb.Dispose();
        }

        protected void LoadComboBoxDatabase(DataTable dtDatabase)
        {
            if (dtDatabase.Rows.Count > 0)
            {
                cbDatabase.DataSource = dtDatabase;
                cbDatabase.DisplayMember = dtDatabase.Columns["DatabaseName"].ToString();
                cbDatabase.ValueMember = dtDatabase.Columns["DatabaseId"].ToString();
            }
        }

        protected string DatabaseId
        {
            get
            {
                return cbDatabase.SelectedValue.ToString();
            }
        }

        class TerminalProgress
        {
            public int iCount;
            public static int iTotal;
            public TerminalProgress(int _iCount)
            {
                iCount = _iCount;
            }
        }

        protected void StartProcess()
        {
            if(!string.IsNullOrEmpty(sDatabaseId))
            {
                for (int i = 0; i < 5; i++)
                    Thread.Sleep(1000);
            }
        }

        protected void ResetAll()
        {
            dgridFitur.DataSource = null;
            progressbarProcess.Value = 0;
        }

        protected void LoadViewFitur()
        {
            //SqlCommand oCmd = new SqlCommand(sSPFiturView, oSqlConn);
            //oCmd.CommandType = CommandType.StoredProcedure;
            //oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDatabaseId;
            //if (oSqlConn.State == ConnectionState.Closed)
            //    oSqlConn.Open();

            //SqlDataReader oReadFitur;

            //oReadFitur = oCmd.ExecuteReader();
            
            //if (oReadFitur.HasRows)
            //{
            //    DataTable dtViewFitur = new DataTable();
            //    //ExportViewFitur(oReadFitur);
            //    ExportViewFitur(oReadFitur);
            //    oReadFitur.Close();
            //    oReadFitur = null;

            //    oReadFitur = oCmd.ExecuteReader();
            //    dtViewFitur.Load(oReadFitur);
            //    dgridFitur.DataSource = dtViewFitur;
            //}

            //oReadFitur.Close();
        }

        protected void ExportViewFitur(SqlDataReader _oRead)
        {
            string sFilename = null;
            if (sfdExport.ShowDialog() == DialogResult.OK)
            {
                sFilename = sfdExport.FileName;
                ExcelLib.WriteExcel(_oRead, sFilename);
                //XmlExport oXML = new XmlExport("Sheet1");
                //oXML.Author = "Insys";
                //oXML.Write(InitXMLColumnCollection(), _oRead, sFilename);
            }
        }
        
        private XmlColumn[] InitXMLColumnCollection()
        {
            XmlColumn[] oColumn = new XmlColumn[10];
            for (int iCol = 0; iCol < 10; iCol++)
                oColumn[iCol] = new XmlColumn(100, XmlType.String);
            return oColumn;
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            StartProcess();

            if (bw.CancellationPending)
            {
                e.Cancel = true;
                bw.ReportProgress(100);
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (!bw.CancellationPending)
            {
                TerminalProgress oTemp = e.UserState as TerminalProgress;
                int iCount = oTemp.iCount;
                txtProgress.Text = string.Format("Processing {0} of {1} terminal.",
                    iCount, TerminalProgress.iTotal);
            }
            else
            {
                progressbarProcess.Style = ProgressBarStyle.Blocks;
                btnExecute.Enabled = true;
                cbDatabase.Enabled = true;
                txtProgress.Clear();
            }
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressbarProcess.Style = ProgressBarStyle.Blocks;
            MessageBox.Show("Process Done. Press 'OK', to continue save and view the result");
            LoadViewFitur();
            btnExecute.Enabled = true;
            cbDatabase.Enabled = true;
        }
    }
}