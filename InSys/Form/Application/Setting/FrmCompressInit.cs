using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmCompressInit : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected string sUserID;
        protected List <string> arrsDbCompressed;

        /// <summary>
        /// Form to determine Terminals that can use Compress Init
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sUserID">string : Current active user</param>
        public FrmCompressInit(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUserID = _sUserID;
        }

        private void FrmCompressInit_Load(object sender, EventArgs e)
        {
            InitData();
            CommonClass.InputLog(oSqlConnAuditTrail, "", sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (IsDbSelected())
                if (IsDbCompressed() == false)
                {
                    DataRowView rowview = (DataRowView)lbDb.SelectedItem;
                    DataTable dtTemp = lbDbCompress.DataSource as DataTable;
                    DataRow row = dtTemp.NewRow();
                    row = rowview.Row;
                    dtTemp.ImportRow(row);
                    lbDbCompress.DataSource = dtTemp;

                    UpdateArrDbCompressed();
                }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDbCompressSelected())
            {
                DataRowView rowview = lbDbCompress.SelectedItem as DataRowView;
                DataTable dtTemp = lbDbCompress.DataSource as DataTable;
                dtTemp.Rows.RemoveAt(lbDbCompress.SelectedIndex);
                lbDbCompress.DataSource = dtTemp;

                UpdateArrDbCompressed();
            }
        }

        private void btnSaveClose_Click_1(object sender, EventArgs e)
        {
            UpdateDbCompressed();
            CommonClass.InputLog(oSqlConnAuditTrail, "", sUserID, "", "Save List of Versions which allowed Compress Initialization", "");
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region "Function"
        /// <summary>
        /// Initiate data, get data from database and show it on list view.
        /// </summary>
        protected void InitData()
        {
            try
            {
                DisplayTerminalDB();
                DisplayTerminalDBCompress();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Load Terminal data from database
        /// </summary>
        /// <param name="sCondition">string : Condition string to filter data</param>
        /// <returns>DataTable : contains data loaded from database</returns>
        protected DataTable dtGetTerminalDB(string sCondition)
        {
            DataTable oDataTable = new DataTable();
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalCompressBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@bIsCompress", SqlDbType.VarChar).Value = sCondition;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                SqlDataReader oRead = oSqlCmd.ExecuteReader();

                oDataTable.Load(oRead);

                oSqlCmd.Dispose();
                oRead.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return oDataTable;
        }

        /// <summary>
        /// Show versions from database at list view.
        /// </summary>
        protected void DisplayTerminalDB()
        {
            DataTable oDataTable = dtGetTerminalDB("0");
            lbDb.DataSource = oDataTable;
            lbDb.DisplayMember = oDataTable.Columns["DatabaseName"].ToString();
            lbDb.ValueMember = oDataTable.Columns["DatabaseID"].ToString();
        }

        /// <summary>
        /// Show versions from database that can use Compress Init at list view
        /// </summary>
        protected void DisplayTerminalDBCompress()
        {
            DataTable oDataTable = dtGetTerminalDB("1");
            lbDbCompress.DataSource = oDataTable;
            lbDbCompress.DisplayMember = oDataTable.Columns["DatabaseName"].ToString();
            lbDbCompress.ValueMember = oDataTable.Columns["DatabaseID"].ToString();

            UpdateArrDbCompressed();
        }

        /// <summary>
        /// Determine whether version selected or not
        /// </summary>
        /// <returns>boolean : true if selected</returns>
        protected bool IsDbSelected()
        {
            DataRowView rowview = (DataRowView)lbDb.SelectedItem;
            return !string.IsNullOrEmpty(rowview["DatabaseName"].ToString()) ? true : false;
        }

        /// <summary>
        /// The name of selected version
        /// </summary>
        protected string sDbSelected
        {
            get
            {
                DataRowView rowview = (DataRowView)lbDb.SelectedItem;
                return rowview["DatabaseName"].ToString();
            }
        }

        /// <summary>
        /// Determine whether Version allowed Compress Init or not
        /// </summary>
        /// <returns>boolean : true if allowed</returns>
        protected bool IsDbCompressed()
        {
            bool bReturn = false;
            if (lbDbCompress.Items.Count > 0)
            {
                for (int i = 0; i < lbDbCompress.Items.Count;i++ )
                {
                    if (sDbSelected == lbDbCompress.Items[i].ToString())
                    {
                        bReturn = true;
                        break;
                    }
                }
            }
            return bReturn;
        }

        /// <summary>
        /// Determine whether Version which allowed Compress Init selected or not
        /// </summary>
        /// <returns>boolean : true if selected</returns>
        protected bool IsDbCompressSelected()
        {
            DataRowView rowview = lbDbCompress.SelectedItem as DataRowView;
            return !string.IsNullOrEmpty(rowview["DatabaseName"].ToString()) ? true : false;
        }

        /// <summary>
        /// Update list of versions that allowed Compress Init
        /// </summary>
        protected void UpdateArrDbCompressed()
        {
            arrsDbCompressed = new List<string>();
            for (int i = 0; i < lbDbCompress.Items.Count; i++)
            {
                DataRowView rowview = (DataRowView)lbDbCompress.Items[i];
                arrsDbCompressed.Add(rowview["DatabaseName"].ToString());
            }
        }

        /// <summary>
        /// Generate Condition string which will be used in database to filter data
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sCondition()
        {
            string sCond = null;
            int iIndexDb = -1;

            if (arrsDbCompressed.Count > 0)
            {
                sCond = "WHERE";
                for (int i = 0; i < arrsDbCompressed.Count; i++)
                {
                    DataRowView rowView;
                    if ((iIndexDb = lbDb.FindString(arrsDbCompressed[i], 0)) == -1)
                        rowView = (DataRowView)lbDbCompress.Items[lbDbCompress.FindString(arrsDbCompressed[i], 0)];
                    else
                        rowView = (DataRowView)lbDb.Items[iIndexDb];

                    sCond = string.Format("{0} DatabaseID='{1}'", sCond, rowView["DatabaseID"]);
                    if (i != arrsDbCompressed.Count - 1)
                        sCond = string.Format("{0} OR ", sCond);
                }
            }
            return sCond;
        }

        /// <summary>
        /// Update Version Data in database
        /// </summary>
        protected void UpdateDbCompressed()
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalCompressUpdate, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();

            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oSqlCmd.ExecuteNonQuery();
            oSqlCmd.Dispose();
        }
        #endregion
    }
}