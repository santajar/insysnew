using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.NetworkInformation;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmPing : Form
    {
        /// <summary>
        ///  Can be used to notify when the operation completes
        /// </summary>
        protected AutoResetEvent resetEvent = new AutoResetEvent(false);

        protected SqlConnection oSqlConn;
        protected string sTable;
        protected string sField;

        protected int iCount;
        protected string[] arrsIpAddress;
        protected string[,] arrsTerminalId;

        protected Thread threadPingIp;
        protected bool bThreading;

        protected bool isRestart = false;

        delegate void SetTextCallback(string text);
        delegate void UpdateTextCallback(string text, int i);

        protected Ping pingSender;

        /// <summary>
        /// Form to view List of active EDC
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection String to Database</param>
        public FrmPing(SqlConnection _oSqlConn)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
        }

        private void FormPing_Load(object sender, EventArgs e)
        {
            try
            {
                InitIpAddress();
                StartThread();
                CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sFormOpened + "Ping EDC", "");
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void FormPing_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (threadPingIp.IsAlive)
                threadPingIp.Abort();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            threadPingIp = new Thread(StartPing);
            threadPingIp.Start();
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (bThreading)
                if (threadPingIp.ThreadState == ThreadState.Running)
                    threadPingIp.Abort();
            InitIpAddress();
            StartThread();
        }

        #region "Init"
        private void InitIpAddress()
        {
            arrsIpAddress = arrsGetIpAddress(ref arrsTerminalId);
        }

        private string[] arrsGetIpAddress(ref string[,] arrsTempTerminal)
        {
            SqlCommand oObjCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn);
            oObjCmd.CommandType = CommandType.StoredProcedure;
            oObjCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sCondition();
            SqlDataReader oRead;
            DataTable dtTerminalIp = new DataTable();

            oRead = oObjCmd.ExecuteReader();
            dtTerminalIp.Load(oRead);
            oRead.Close();

            iCount = dtTerminalIp.Rows.Count;
            arrsIpAddress = new string[iCount];
            arrsTerminalId = new string[iCount, 2];

            string[] arrsTempIp = new string[iCount];

            int i = 0;
            foreach (DataRow drow in dtTerminalIp.Rows)
            {
                arrsTempTerminal[i, 0] = drow["TerminalId"].ToString();
                arrsTempTerminal[i, 1] = "Red";
                arrsTempIp[i] = drow["IpAddress"].ToString();
                i++;
            }
            return arrsTempIp;
        }
        #endregion 

        #region "Function"
        /// <summary>
        /// Return a where condition to browse table
        /// </summary>
        /// <returns>string : condition statement</returns>
        protected string sCondition()
        {
            return "WHERE EnableIp=1 AND IpAddress IS NOT NULL";
        }

        /// <summary>
        /// Determine whether form will be restarted or not
        /// </summary>
        /// <returns>boolean : true if restart</returns>
        public bool IsRestartForm()
        {
            return isRestart;
        }

        private void StartThread()
        {
            try
            {
                // start to ping
                if (iCount != 0)
                {
                    threadPingIp = new Thread(StartPing);
                    bThreading = true;
                    threadPingIp.Start();
                }
            }
            catch (Exception)
            {
                //MessageBox.Show("StartThread : " + ex.Message);
            }
        }

        private void StartPing()
        {
            try
            {
                while (true)
                {
                    for (int i = 0;i<iCount;i++)
                    {
                        if (arrsIpAddress[i] != "0.0.0.0")
                        {
                            SendPingAsync(arrsIpAddress[i]);
                        }
                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show("StartPing : " + ex.Message);
            }
        }

        private void SendPingAsync(string sIpAddress)
        {
            pingSender = new Ping();

            // Create an event handler for ping complete
            pingSender.PingCompleted += new PingCompletedEventHandler(pingSender_Complete);

            // Create a buffer of 32 bytes of data to be transmitted.
            byte[] packetData = Encoding.ASCII.GetBytes("................................");

            // Jump though 50 routing nodes tops, and don't fragment the packet
            PingOptions packetOptions = new PingOptions(50, true);

            // Send the ping asynchronously
            pingSender.SendAsync(sIpAddress, 1000, packetData, packetOptions, resetEvent);
        }

        private void pingSender_Complete(object sender, PingCompletedEventArgs e)
        {
            // If the operation was canceled, display a message to the user.
            if (e.Cancelled)
            {
                //lbResponseShow("Ping was canceled...");

                // The main thread can resume
                ((AutoResetEvent)e.UserState).Set();
            }
            else if (e.Error != null)
            {
                //lbResponseShow("An error occured: " + e.Error);

                // The main thread can resume
                ((AutoResetEvent)e.UserState).Set();
            }
            else
            {
                PingReply pingResponse = e.Reply;
                // Call the method that displays the ping results, and pass the information with it
                ShowPingResults(pingResponse);
            }
            
        }

        /// <summary>
        /// Show result of ping 
        /// </summary>
        /// <param name="pingResponse">PingReply : object contains ping response data</param>
        public void ShowPingResults(PingReply pingResponse)
        {
            if (pingResponse.Status == IPStatus.Success)
            {
                // We got a response, let's see the statistics
                //lbResponseShow("Reply from " + pingResponse.Address.ToString()
                //    + ": bytes=" + pingResponse.Buffer.Length
                //    + " time=" + pingResponse.RoundtripTime
                //    + " TTL=" + pingResponse.Options.Ttl);

                string sIpAddress = pingResponse.Address.ToString();
                string sTerminal = sGetIpTerminalName(sIpAddress, "Green");
                int iIndexResponse = iGetIndexlbResponse(sTerminal, sIpAddress);

                if (iIndexResponse != -1)
                    lbResponseUpdate(string.Format("{0}  ({1})", sTerminal, sIpAddress), iIndexResponse);
                else
                    lbResponseShow(string.Format("{0}  ({1})", sTerminal, sIpAddress));
            }
            else
            {
                // The packet didn't get back as expected, explain why
                //lbResponseShow(pingResponse.Address.ToString() 
                //    + " Ping was unsuccessful: " 
                //    + pingResponse.Status);

                string sIpAddress = pingResponse.Address.ToString();
                string sTerminal = sGetIpTerminalName(sIpAddress, "Red");
                int iIndexResponse = iGetIndexlbResponse(sTerminal, sIpAddress);

                if (iIndexResponse != -1)
                    lbResponseUpdate(string.Format("{0}  ({1})", sTerminal, sIpAddress), iIndexResponse);
                else
                    lbResponseShow(string.Format("{0}  ({1})", sTerminal, sIpAddress));
            }
            
            //Thread.Sleep(5000);
        }

        private void lbResponseShow(string sText)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.lbResponse.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(lbResponseShow);
                this.Invoke(d, new object[] { sText });
            }
            else
            {
                this.lbResponse.Items.Add(sText);
                //this.lbResponse.SelectedIndex = this.lbResponse.Items.Count - 1;
                this.lbResponse.DrawMode = DrawMode.OwnerDrawFixed;
                this.lbResponse.DrawItem += new DrawItemEventHandler(lbResponse_DrawItem);
            }
        }

        private void lbResponseUpdate(string sText, int iIndex)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.lbResponse.InvokeRequired)
            {
                UpdateTextCallback d = new UpdateTextCallback(lbResponseUpdate);
                this.Invoke(d, new object[] { sText, iIndex });
            }
            else
            {
                this.lbResponse.Items.RemoveAt(iIndex);
                this.lbResponse.DrawMode = DrawMode.OwnerDrawFixed;
                this.lbResponse.DrawItem += new DrawItemEventHandler(lbResponse_DrawItem);
                this.lbResponse.Items.Insert(iIndex, sText);
                //this.lbResponse.SelectedIndex = iIndex;
            }
        }

        private int iGetIndexIp(string _sIp)
        {
            for (int i = 0; i < iCount; i++)
            {
                if (IPAddress.Parse(arrsIpAddress[i]).ToString() == _sIp)
                    return i;
            }
            return -1;
        }

        private int iGetIndexTerminal(string _sTerminal)
        {
            for (int i = 0; i < iCount; i++)
                if (_sTerminal.Contains(arrsTerminalId[i, 0]))
                    return i;
            return -1;
        }

        private int iGetIndexlbResponse(string _sTerminalName, string _sIpAddr)
        {
            for (int i = 0; i < lbResponse.Items.Count; i++)
            {
                if (this.lbResponse.Items[i].ToString() == string.Format("{0}  ({1})", _sTerminalName, _sIpAddr))
                    return i;
            }
            return -1;
        }

        private string sGetIpTerminalName(string _sIp, string sColor)
        {
            int i = iGetIndexIp(_sIp);
            if (i != -1)
            {
                arrsTerminalId[i, 1] = sColor;
                return arrsTerminalId[i, 0];
            }
            else
                return null;
        }
        #endregion

        #region "Display"
        private void lbResponse_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                // Draw the background of the ListBox control for each item.
                // Create a new Brush and initialize to a Black colored brush
                // by default
                e.DrawBackground();
                int iIndexTerm = iGetIndexTerminal(((ListBox)sender).Items[e.Index].ToString());
                string sColor = "Red";
                if (iIndexTerm != -1)
                {
                    sColor = arrsTerminalId[iIndexTerm, 1];

                    //Determine the color of the brush to draw
                    Brush myBrush;
                    if (sColor.ToUpper() == "GREEN") myBrush = Brushes.Green;
                    else myBrush = Brushes.Red;

                    //myBrush = new SolidBrush(Color.FromName(sColor));

                    Font fdraw = e.Font;
                    fdraw = new Font(fdraw.FontFamily, fdraw.Size, FontStyle.Bold);

                    // Draw the current item text based on the current 
                    // Font and the custom brush settings
                    e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(),
                        fdraw, myBrush, e.Bounds, StringFormat.GenericDefault);

                    // If the ListBox has focus, draw a focus rectangle 
                    // around the selected item
                    e.DrawFocusRectangle();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("1: " + ex.Message);
            }
        }
        #endregion
    }
}