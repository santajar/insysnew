namespace InSys
{
    partial class FrmVersionSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVersionSelection));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lvDb = new System.Windows.Forms.ListView();
            this.cmsVersions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addNewVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteVersionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClose = new System.Windows.Forms.Button();
            this.cmsVersions.SuspendLayout();
            this.SuspendLayout();
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "database_icon.jpg");
            this.ImageList1.Images.SetKeyName(2, "database(1).ico");
            // 
            // lvDb
            // 
            this.lvDb.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lvDb.ContextMenuStrip = this.cmsVersions;
            this.lvDb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvDb.LabelWrap = false;
            this.lvDb.LargeImageList = this.ImageList1;
            this.lvDb.Location = new System.Drawing.Point(0, 0);
            this.lvDb.MultiSelect = false;
            this.lvDb.Name = "lvDb";
            this.lvDb.Size = new System.Drawing.Size(412, 349);
            this.lvDb.SmallImageList = this.ImageList1;
            this.lvDb.StateImageList = this.ImageList1;
            this.lvDb.TabIndex = 2;
            this.lvDb.UseCompatibleStateImageBehavior = false;
            this.lvDb.View = System.Windows.Forms.View.SmallIcon;
            this.lvDb.DoubleClick += new System.EventHandler(this.lvDb_DoubleClick);
            this.lvDb.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.lvDb_PreviewKeyDown);
            // 
            // cmsVersions
            // 
            this.cmsVersions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewVersionToolStripMenuItem,
            this.renameVersionToolStripMenuItem,
            this.deleteVersionToolStripMenuItem});
            this.cmsVersions.Name = "contextMenuStrip1";
            this.cmsVersions.Size = new System.Drawing.Size(166, 70);
            // 
            // addNewVersionToolStripMenuItem
            // 
            this.addNewVersionToolStripMenuItem.Name = "addNewVersionToolStripMenuItem";
            this.addNewVersionToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.addNewVersionToolStripMenuItem.Text = "Add New Version";
            this.addNewVersionToolStripMenuItem.Visible = false;
            // 
            // renameVersionToolStripMenuItem
            // 
            this.renameVersionToolStripMenuItem.Name = "renameVersionToolStripMenuItem";
            this.renameVersionToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.renameVersionToolStripMenuItem.Text = "Rename Version";
            this.renameVersionToolStripMenuItem.Visible = false;
            // 
            // deleteVersionToolStripMenuItem
            // 
            this.deleteVersionToolStripMenuItem.Name = "deleteVersionToolStripMenuItem";
            this.deleteVersionToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.deleteVersionToolStripMenuItem.Text = "Delete Version";
            this.deleteVersionToolStripMenuItem.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(390, 327);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(10, 10);
            this.btnClose.TabIndex = 3;
            this.btnClose.TabStop = false;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // FrmVersionSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(412, 349);
            this.Controls.Add(this.lvDb);
            this.Controls.Add(this.btnClose);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmVersionSelection";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Version Selection";
            this.Activated += new System.EventHandler(this.FrmVersionSelection_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmVersionSelection_FormClosing);
            this.Load += new System.EventHandler(this.FrmVersionSelection_Load);
            this.cmsVersions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ImageList ImageList1;
        internal System.Windows.Forms.ListView lvDb;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ContextMenuStrip cmsVersions;
        private System.Windows.Forms.ToolStripMenuItem addNewVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameVersionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteVersionToolStripMenuItem;
    }
}