using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmAcquirerList : Form
    {
        protected SqlConnection oSqlConn;
        protected string sDbId;
        protected DataTable dtAcquirerNameList = new DataTable();

        public FrmAcquirerList(SqlConnection _oSqlConn, string _sDbId)
        {
            oSqlConn = _oSqlConn;
            sDbId = _sDbId;
            InitializeComponent();
            InitData();
            LoadData();
        }

        /// <summary>
        /// Initializes the form.
        /// </summary>
        private void InitData()
        {
            InitCheckListBox();
            InitAcquirerNameList();
        }

        /// <summary>
        /// Initializes the Check List Box
        /// </summary>
        private void InitCheckListBox()
        {
            clbAcquirerList.Items.Add("All");
        }

        /// <summary>
        /// Initializes Acquirer Name List
        /// </summary>
        private void InitAcquirerNameList()
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPUploadAcquirerList, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@iDatabaseId", SqlDbType.Int).Value = int.Parse(sDbId);
                using (SqlDataReader reader = oCmd.ExecuteReader())
                {
                    dtAcquirerNameList.Load(reader);
                }
            }
        }

        #region HardCode
        /// <summary>
        /// Fills the check list box with Acquirers' name
        /// </summary>
        private void LoadData()
        {
            //clbAcquirerList.Items.AddRange(sAcquirerList);
            foreach (DataRow row in dtAcquirerNameList.Rows)
                clbAcquirerList.Items.Add(row["AcquirerName"].ToString());
            sAcquirerList = new string[clbAcquirerList.Items.Count - 1];
            for (int i = 1; i < clbAcquirerList.Items.Count; i++)
                sAcquirerList[i - 1] = clbAcquirerList.Items[i].ToString();
        }
        
        /// <summary>
        /// Acquirer list
        /// </summary>
        public string[] sAcquirerList =
            {
                "BCA",
                //"BCA2",
                "AMEX",
                "DINERS",
                "DEBIT",
                "LOYALTY",
                "FLAZZBCA",
                "PROMO A",
                "PROMO B",
                "PROMO C",
                "PROMO D",
                "PROMO E",
                "PROMO F",
                "PROMO G",
                "PROMO H",
                "PROMO I",
                "PROMO J"
            };

        /// <summary>
        /// A list variable to store selected items in the current Acquirer
        /// </summary>
        public List<string> sSelectedItems;
        #endregion

        /// <summary>
        /// Ticks the check box if it's been selected before.
        /// </summary>
        /// <param name="sSelectedAcquirers">Acquirer name</param>
        public void SetCheckListView(string[] sSelectedAcquirers)
        {
            clbAcquirerList.ClearSelected();
            sSelectedItems = new List<string>(sSelectedAcquirers);
            for (int iIndex = 0; iIndex < clbAcquirerList.Items.Count; iIndex++)
                clbAcquirerList.SetItemChecked(iIndex, false);
            for (int iIndex = 1; iIndex < clbAcquirerList.Items.Count; iIndex++)
                if (sSelectedItems.Contains(clbAcquirerList.Items[iIndex].ToString()))
                    clbAcquirerList.SetItemChecked(iIndex, true);
        }

        /// <summary>
        /// Commands the computer what to do on button Save click event.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            sSelectedItems = new List<string>();
            //for (int iIndex = 0; iIndex < sAcquirerList.Length; iIndex++)
            //    if (clbAcquirerList.GetItemChecked(iIndex + 1))
            //        sSelectedItems.Add(clbAcquirerList.Items[iIndex + 1].ToString());
            foreach (string sAcquirerChecked in clbAcquirerList.CheckedItems)
                sSelectedItems.Add(sAcquirerChecked);
                
            this.Close();
        }

        /// <summary>
        /// Ticks all the check box if check box All is ticked. Unticks checkbox All if a checkbox is unticked.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void clbAcquirerList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.Index == 0 && e.NewValue == CheckState.Checked)
                for (int iIndex = 1; iIndex < clbAcquirerList.Items.Count; iIndex++)
                    clbAcquirerList.SetItemChecked(iIndex, true);
            else if (e.Index == 0 && e.NewValue == CheckState.Unchecked)
                for (int iIndex = 1; iIndex < clbAcquirerList.Items.Count; iIndex++)
                    clbAcquirerList.SetItemChecked(iIndex, false);
            //else if (e.Index > 0 && e.CurrentValue != e.NewValue && e.NewValue == CheckState.Unchecked)
            //    clbAcquirerList.SetItemChecked(0, true);
            //else if (e.Index > 0 && e.CurrentValue != e.NewValue)
            //    clbAcquirerList.SetItemChecked(0, false);
        }
    }
}