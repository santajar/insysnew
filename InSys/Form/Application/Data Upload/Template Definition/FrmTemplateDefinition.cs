using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmTemplateDefinition : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        private FrmAcquirerList frmAcqList;     //needed to get/set selected acquirers throughout process.
        private string[][] sAcquirerSetting;    //needed to store selected acquirers throughout process.
        public string sDBName;                 //needed to store database name from previous form.
        private string sDBID;                   //needed to store database id from previous form.

        /// <summary>
        /// Form to make Template Definiton
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection String to database</param>
        public FrmTemplateDefinition(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        /// <summary>
        /// Commands the computer what to do on this form load event.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void FrmTemplateDefinition_Load(object sender, EventArgs e)
        {
            try
            {
                if (InitData()) LoadData();
                else this.Close();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        /// <summary>
        /// Commands the computer what to do on button Choose Acquirer click event.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void btnChooseAcq_Click(object sender, EventArgs e)
        {
            try
            {
                frmAcqList.SetCheckListView(sAcquirerSetting[dgvAcquirer.SelectedRows[0].Index]);
                if (frmAcqList.ShowDialog() == DialogResult.OK)
                {
                    for (int iIndex = 0; iIndex < sAcquirerSetting[dgvAcquirer.CurrentRow.Index].Length; iIndex++)
                        sAcquirerSetting[dgvAcquirer.CurrentRow.Index][iIndex] = null;
                    for (int iIndex = 0; iIndex < frmAcqList.sSelectedItems.Count; iIndex++)
                        if (frmAcqList.sSelectedItems.ToArray()[iIndex].ToUpper() != "ALL")
                        {
                            //Ditambah pemeriksaan apakah list Acquirer termasuk "All"
                            //Jika ada All, index akan dikurang 1 karena looping sudah jalan 1x saat pemeriksaan if diatas
                            sAcquirerSetting[dgvAcquirer.CurrentRow.Index][iIndex - (frmAcqList.sSelectedItems.Contains("All") ? 1 : 0)] 
                                = frmAcqList.sSelectedItems.ToArray()[iIndex];
                        }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Commands the computer what to do on button Save click event.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            //string sContent;
            //if (!File.Exists(Application.StartupPath + "\\Template.config") || MessageBox.Show("Template Definition file already exists. Replace existing file?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
            //    if ((sContent = sGenerateTemplate()) != null)
            //    {
            //        File.WriteAllText(Application.StartupPath + "\\Template.config", sContent);
            //        CommonClass.inputLog(oSqlConn, "", UserData.sUserID, sDBName, "Create Template Defintion", "Saved at " + Application.StartupPath + "\\Template.config");
            //        this.Close();
            //    }
            //}
            try
            {
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                if (!CommonClass.IsTemplateDefinitionExists(oSqlConn,sDBName) || MessageBox.Show("Template Definition file already exists. Update Template Definition?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    CreateTemplate();
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, sDBName, "Create Template Defintion", "");
                    this.Close();
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void tabTemplateDef_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (tabTemplateDef.SelectedTab == tabTerminal)
            //{
            //    isTerminalActive = true;
            //    isAcqActive = false;
            //}
            //else if (tabTemplateDef.SelectedTab == tabAcquirer)
            //{
            //    isTerminalActive = false;
            //    isAcqActive = true;
            //}
        }

        #region "Function"
        /// <summary>
        /// Checks in the database wether a template definition has already exist or not.
        /// </summary>
        /// <returns>If there's no template definition yet it'll return true. If there's already a template definition it'll ask user wether they still want to create a template definition or not, returns true if they choose yes, returns false if they choose no.</returns>
        private bool InitData()
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            //if (!CommonClass.IsTemplateDefinitionExists(oSqlConn) || MessageBox.Show("Template Definition file already exists. Update Template Definition?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
                FrmPromptString frmPrompt = new FrmPromptString(CommonMessage.sDatabaseTemplateTitle, CommonMessage.sDatabaseTemplateText);
                
                // Penambahan AutoCompleteMode dan AutoCompleteSource pada ComboBox untuk mempermudah pemihilhan database

                ComboBox cmbPrompt = new ComboBox();

                cmbPrompt.TabIndex = 0;
                cmbPrompt.ValueMember = "DatabaseID";
                cmbPrompt.DisplayMember = "DatabaseName";
                cmbPrompt.DropDownStyle = ComboBoxStyle.DropDown;
                cmbPrompt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                cmbPrompt.AutoCompleteSource = AutoCompleteSource.ListItems;
                cmbPrompt.Location = frmPrompt.txtInput.Location;
                cmbPrompt.Size = frmPrompt.txtInput.Size;
                frmPrompt.txtInput.Visible = false;
                frmPrompt.Controls.Add(cmbPrompt);

                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
                oSqlComm.CommandType = CommandType.StoredProcedure;

                DataTable oTable = new DataTable();
                new SqlDataAdapter(oSqlComm).Fill(oTable);
                cmbPrompt.DataSource = oTable;
                cmbPrompt.SelectedIndex = -1;

                if (frmPrompt.ShowDialog() == DialogResult.OK)
                {
                    if (cmbPrompt.SelectedIndex > -1)
                    {
                        sDBID = cmbPrompt.SelectedValue.ToString();
                        sDBName = cmbPrompt.Text;
                        this.Opacity = 1;
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("No Database was selected.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else return false;
            //}
            //else return false;
        }

        /// <summary>
        /// Populates the data grid view and loads acquirer settings.
        /// </summary>
        private void LoadData()
        {
            LoadDataGrid();
            LoadAcquirerSetting();
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + "Template Definition", "");
        }

        /// <summary>
        /// Populates data to the data grid view
        /// </summary>
        private void LoadDataGrid()
        {
            LoadDataGrid(CommonLib.NodeLevel.Terminal, dgvTerminal);
            SetDisplayDgvTerminal();

            LoadDataGrid(CommonLib.NodeLevel.Acquirer, dgvAcquirer);
            SetDisplayDgvAcquirer();
        }

        /// <summary>
        /// Sets display for terminal region in the data grid view after it's been populated.
        /// </summary>
        private void SetDisplayDgvTerminal()
        {
            dgvTerminal[1, 0].Value = dgvTerminal[1, 0].Value.ToString().Replace(" ", "");
            dgvTerminal[0, 0].Value = dgvTerminal[2, 0].Value = true;
            dgvTerminal.Rows[0].ReadOnly = true;
        }

        /// <summary>
        /// Sets display for acquirer region in the data grid view after it's been populated.
        /// </summary>
        private void SetDisplayDgvAcquirer()
        {
            dgvAcquirer.Rows.RemoveAt(0); //Acquirer
            dgvAcquirer.Rows.RemoveAt(0); //Batch ID
            dgvAcquirer.Rows.RemoveAt(0); //Host No.
        }

        /// <summary>
        /// Populates data to the data grid view.
        /// </summary>
        /// <param name="eType">What kind of data to be populated</param>
        /// <param name="oDGV">The data grid view destination</param>
        private void LoadDataGrid(CommonLib.NodeLevel eType, DataGridView oDGV)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;
            oSqlComm.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sGetCondition(eType);

            SqlDataReader oReader = oSqlComm.ExecuteReader();
            while (oReader.Read())
            {
                oDGV.Rows.Add();
                oDGV[1, oDGV.RowCount - 1].Value = oReader.GetValue(4).ToString();
                oDGV[3, oDGV.RowCount - 1].Value = "Mandatory";
                oDGV[4, oDGV.RowCount - 1].Value = oReader.GetValue(17).ToString();
            }
            oReader.Close();
        }

        /// <summary>
        /// Allocates the size of each index of the sAcquirerSetting variable.
        /// </summary>
        private void LoadAcquirerSetting()
        {
            frmAcqList = new FrmAcquirerList(oSqlConn, sDBID);
            sAcquirerSetting = new string[dgvAcquirer.RowCount][];
            for (int iIndex = 0; iIndex < dgvAcquirer.RowCount; sAcquirerSetting[iIndex++] = new string[frmAcqList.sAcquirerList.Length]) ;
        }

        /// <summary>
        /// Generates the condition to be sent to the database.
        /// </summary>
        /// <param name="eType">Terminal/Acquirer</param>
        /// <returns>The condition string.</returns>
        private string sGetCondition(CommonLib.NodeLevel eType)
        {
            switch (eType)
            {
                case CommonLib.NodeLevel.Terminal:
                    return "WHERE DatabaseID ='" + sDBID + "' AND Tag LIKE 'DE__' order by tag  ";
                case CommonLib.NodeLevel.Acquirer:
                    return "WHERE DatabaseID ='" + sDBID + "' AND Tag LIKE 'AA__' order by tag ";
                default:
                    return null;
            }
        }

        //private int iGetCheckCount(DataGridView oDGV)
        //{
        //    int iCheckCount = 0;
        //    foreach (DataGridViewRow oRow in oDGV.Rows)
        //        if (oRow.Cells[0].Value != null && (bool)oRow.Cells[0].Value)
        //            iCheckCount += 1;
        //    return iCheckCount;
        //}

        //private string sGetTemplate(string sHeader, int iCount, DataGridView oDGV, string sCol)
        //{
        //    StringBuilder sContent = new StringBuilder("[" + sHeader + "]\n");
        //    sContent.Append("Counter=" + iCount.ToString() + "\n");
        //    int iCol = 1;
        //    foreach (DataGridViewRow oRow in oDGV.Rows)
        //    {
        //        if (oRow.Cells[0].Value != null && (bool)oRow.Cells[0].Value)
        //        {
        //            sContent.Append("Col-");
        //            sContent.Append((iCol++).ToString());
        //            sContent.Append("=");
        //            sContent.Append(oRow.Cells[sCol].Value == null ? "False" : oRow.Cells[sCol].Value.ToString());
        //            sContent.Append("\n");
        //        }
        //    }
        //    return sContent.ToString();
        //}

        //private string sGetTemplate(string sHeader, int iCount, string sSubHeader, string[] sSubHeaderContent, DataGridView oDGV, string[][] sAcquirerSetting)
        //{
        //    StringBuilder sContent = new StringBuilder("[" + sHeader + "]\n");

        //    sContent.Append("Counter=" + iCount.ToString() + "\n");

        //    sContent.Append(sSubHeader + "=");
        //    for (int iIndex = 0; iIndex < sSubHeaderContent.Length; iIndex++)
        //    {
        //        sContent.Append(sSubHeaderContent[iIndex]);
        //        if (iIndex < sSubHeaderContent.Length - 1) sContent.Append(",");
        //    }
        //    sContent.Append("\n");

        //    int iCol = 1;
        //    for (int iIndex = 0; iIndex < oDGV.RowCount; iIndex++)
        //    {
        //        if (oDGV.Rows[iIndex].Cells[0].Value != null && (bool)oDGV.Rows[iIndex].Cells[0].Value)
        //        {
        //            sContent.Append("Col-" + (iCol++).ToString() + "=");

        //            int iColIndex = 0;
        //            string sSubContent = "";
        //            while (iColIndex < sSubHeaderContent.Length && sAcquirerSetting[iIndex][iColIndex] != null) sSubContent += sAcquirerSetting[iIndex][iColIndex++] + ",";

        //            if (sSubContent.Length < 1)
        //            {
        //                tabTemplateDef.SelectedTab = tabAcquirer;
        //                oDGV.Rows[iIndex].Selected = true;
        //                MessageBox.Show("Please choose Acquirer for " + oDGV.Rows[iIndex].Cells[1].Value.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        //                return null;
        //            }

        //            sContent.Append(sSubContent.Remove(sSubContent.Length - 1) + "\n");
        //        }
        //    }

        //    return sContent.ToString();
        //}

        //private string sGenerateTemplate()
        //{
        //    int iTerminalColCount = iGetCheckCount(dgvTerminal);
        //    int iAcquirerColCount = iGetCheckCount(dgvAcquirer);

        //    string sSubContent;
        //    StringBuilder sContent = new StringBuilder();
        //    if ((sSubContent = sGetTemplate("Terminal", iTerminalColCount, dgvTerminal, "clmTerminalItemText")) == null) return null;
        //    sContent.Append(sSubContent + "\n");

        //    if ((sSubContent = sGetTemplate("Terminal Tag", iTerminalColCount, dgvTerminal, "clmTerminalTag")) == null) return null;
        //    sContent.Append(sSubContent + "\n");

        //    if ((sSubContent = sGetTemplate("Terminal Mandatory", iTerminalColCount, dgvTerminal, "clmTerminalMandatory")) == null) return null;
        //    sContent.Append(sSubContent + "\n");

        //    if ((sSubContent = sGetTemplate("Acquirer", iAcquirerColCount, dgvAcquirer, "clmAcquirerItemText")) == null) return null;
        //    sContent.Append(sSubContent + "\n");

        //    if ((sSubContent = sGetTemplate("Acquirer Tag", iAcquirerColCount, dgvAcquirer, "clmAcquirerTag")) == null) return null;
        //    sContent.Append(sSubContent + "\n");

        //    if ((sSubContent = sGetTemplate("Acquirer Condition", iAcquirerColCount, "Acquirer", frmAcqList.sAcquirerList, dgvAcquirer, sAcquirerSetting)) == null) return null;
        //    sContent.Append(sSubContent + "\n");

        //    if ((sSubContent = sGetTemplate("Acquirer Mandatory", iAcquirerColCount, dgvAcquirer, "clmAcquirerMandatory")) == null) return null;
        //    sContent.Append(sSubContent);

        //    return sContent.ToString();
        //}

        /// <summary>
        /// Erases the existing template in the database.
        /// </summary>
        /// <param name="sStoredProcedure">Stored procedure name</param>
        private void ClearTemplate(string sStoredProcedure)
        {
            //if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            //new SqlCommand(sStoredProcedure, oSqlConn).ExecuteNonQuery();
        }

        /// <summary>
        /// Transfers all the DE (Terminal) data from the Data Grid View to a table variable.
        /// </summary>
        /// <param name="sColumnCollection">Column format for the returned table</param>
        /// <returns>The table.</returns>
        private DataTable GetTableDE(string[] sColumnCollection)
        {
            DataTable oTable = new DataTable();
            object[] oCells = new object[4];

            foreach (string sCol in sColumnCollection)
                oTable.Columns.Add(sCol);

            foreach (DataGridViewRow oRow in dgvTerminal.Rows)
            {
                if (oRow.Cells[0].Value != null && (bool)oRow.Cells[0].Value)
                {
                    oCells[0] = sDBName.ToString();
                    oCells[1] = oRow.Cells["clmTerminalTag"].Value.ToString();
                    oCells[2] = oRow.Cells["clmTerminalItemText"].Value.ToString();
                    oCells[3] = (oRow.Cells["clmTerminalMandatory"].Value == null ?
                        false : (bool)oRow.Cells["clmTerminalMandatory"].Value);
                    oTable.Rows.Add(oCells);
                }
            }

            //Add "Master"
            oCells[0] = sDBName.ToString();
            oCells[1] = "";
            oCells[2] = "Master";
            oCells[3] = true;
            
            oTable.Rows.Add(oCells);

            return oTable;
        }

        /// <summary>
        /// Transfers all the AA (Acquirer) data from the Data Grid View to a table variable.
        /// </summary>
        /// <param name="sColumnCollection">Column format for the returned table</param>
        /// <returns>The table.</returns>
        private DataTable GetTableAA(string[] sColumnCollection)
        {
            DataTable oTable = new DataTable();
            object[] oCells = new object[5];

            foreach (string sCol in sColumnCollection)
                oTable.Columns.Add(sCol);

            for (int iAcqCtr = 0; iAcqCtr < frmAcqList.sAcquirerList.Length; iAcqCtr++)
            {
                for (int iRowCtr = 0; iRowCtr < dgvAcquirer.RowCount; iRowCtr++)
                {
                    if (dgvAcquirer[0, iRowCtr].Value != null && (bool)dgvAcquirer[0, iRowCtr].Value && new List<string>(sAcquirerSetting[iRowCtr]).Contains(frmAcqList.sAcquirerList[iAcqCtr]))
                    {
                        oCells[0] = sDBName.ToString();
                        oCells[1] = frmAcqList.sAcquirerList[iAcqCtr];
                        oCells[2] = dgvAcquirer["clmAcquirerTag", iRowCtr].Value.ToString();
                        oCells[3] = dgvAcquirer["clmAcquirerItemText", iRowCtr].Value.ToString();
                        oCells[4] = (dgvAcquirer["clmAcquirerMandatory", iRowCtr].Value == null ?
                            false : (bool)dgvAcquirer["clmAcquirerMandatory", iRowCtr].Value);
                        oTable.Rows.Add(oCells);
                    }
                }
            }

            return oTable;
        }

        /// <summary>
        /// Sends the DE (Terminal) template to the database.
        /// </summary>
        private void InsertTemplateDE()
        {
            SqlDataAdapter oAdapt = new SqlDataAdapter();
            string[] sColumns =
                {
                    "Template" ,
                    "Tag",
                    "ColumnName",
                    "Mandatory"
                };

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            oAdapt.InsertCommand = new SqlCommand(CommonSP.sSPUploadTagDEInsert, oSqlConn);
            oAdapt.InsertCommand.CommandType = CommandType.StoredProcedure;

            oAdapt.InsertCommand.Parameters.Add("@sTag", SqlDbType.VarChar, 4).SourceColumn = sColumns[1];
            oAdapt.InsertCommand.Parameters.Add("@sColumnName", SqlDbType.VarChar, 50).SourceColumn = sColumns[2];
            oAdapt.InsertCommand.Parameters.Add("@bMandatory", SqlDbType.Bit).SourceColumn = sColumns[3];
            oAdapt.InsertCommand.Parameters.Add("@sTemplate", SqlDbType.VarChar, 30).SourceColumn = sColumns[0];

            oAdapt.Update(GetTableDE(sColumns));
        }

        /// <summary>
        /// Sends the AA (Acquirer) template to the database.
        /// </summary>
        private void InsertTemplateAA()
        {
            SqlDataAdapter oAdapt = new SqlDataAdapter();
            string[] sColumns =
                {
                    "Template",
                    "Acquirer",
                    "Tag",
                    "ColumnName",
                    "Mandatory"
                };

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            oAdapt.InsertCommand = new SqlCommand(CommonSP.sSPUploadTagAAInsert, oSqlConn);
            oAdapt.InsertCommand.CommandType = CommandType.StoredProcedure;

            oAdapt.InsertCommand.Parameters.Add("@sAcquirerName", SqlDbType.VarChar, 50).SourceColumn = sColumns[1];
            oAdapt.InsertCommand.Parameters.Add("@sTag", SqlDbType.VarChar, 4).SourceColumn = sColumns[2];
            oAdapt.InsertCommand.Parameters.Add("@sColumnName", SqlDbType.VarChar, 50).SourceColumn = sColumns[3];
            oAdapt.InsertCommand.Parameters.Add("@bMandatory", SqlDbType.Bit).SourceColumn = sColumns[4];
            oAdapt.InsertCommand.Parameters.Add("@sTemplate", SqlDbType.VarChar, 30).SourceColumn = sColumns[0];

            oAdapt.Update(GetTableAA(sColumns));
        }

        /// <summary>
        /// Erases Terminal and Acquirer template in the database.
        /// </summary>
        private void ClearTemplates()
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            using (SqlCommand cmdClearDE = new SqlCommand(CommonSP.sSPUploadTagDEDelete, oSqlConn))
            {
                cmdClearDE.CommandType = CommandType.StoredProcedure;
                cmdClearDE.Parameters.Add("@sCond", SqlDbType.VarChar).Value = " where Template= '" + sDBName + "'";
                cmdClearDE.ExecuteNonQuery();
            }
            using (SqlCommand cmdClearAA = new SqlCommand(CommonSP.sSPUploadTagAADelete, oSqlConn))
            {
                cmdClearAA.CommandType = CommandType.StoredProcedure;
                cmdClearAA.Parameters.Add("@sCond", SqlDbType.VarChar).Value = " where Template= '" + sDBName + "'";
                cmdClearAA.ExecuteNonQuery();
            }

            //ClearTemplate(CommonSP.sSPUploadTagDE1Delete);
            //ClearTemplate(CommonSP.sSPUploadTagAA1Delete);
        }

        /// <summary>
        /// Sends the new template definition to the database.
        /// </summary>
        private void InsertTemplates()
        {
            InsertTemplateDE();
            InsertTemplateAA();
        }

        /// <summary>
        /// All the process of creating the template definition.
        /// </summary>
        private void CreateTemplate()
        {
            ClearTemplates();
            InsertTemplates();

            //Menulis Template ke excel
            ExcelLib.WriteExcel(dtGenerateTableExcel(), Application.StartupPath + "\\Upload\\TemplateDefinition_" + sDBName + ".xls");
        }

        /// <summary>
        /// Genereate DataTable from selected Tag in Terminal and Acquirer used to create Excel File
        /// </summary>
        /// <returns>DataTable: Contains all tag selected for Template Excel File</returns>
        private DataTable dtGenerateTableExcel()
        {
            DataTable dtTable = new DataTable();

            #region "Generate Columns"
            foreach (DataGridViewRow dgRow in dgvTerminal.Rows)
            {
                if (dgRow.Cells[0].Value != null && (bool)dgRow.Cells[0].Value)
                    dtTable.Columns.Add(dgRow.Cells["clmTerminalItemText"].Value.ToString());
            }

            dtTable.Columns.Add("Master");
            dtTable.Columns.Add("Database");

            for (int iAcqCtr = 0; iAcqCtr < frmAcqList.sAcquirerList.Length; iAcqCtr++)
            {
                for (int iRowCtr = 0; iRowCtr < dgvAcquirer.RowCount; iRowCtr++)
                {
                    if (dgvAcquirer[0, iRowCtr].Value != null && (bool)dgvAcquirer[0, iRowCtr].Value && 
                            new List<string>(sAcquirerSetting[iRowCtr]).Contains(frmAcqList.sAcquirerList[iAcqCtr]))
                        dtTable.Columns.Add(frmAcqList.sAcquirerList[iAcqCtr].ToString() + 
                            "_" + dgvAcquirer["clmAcquirerItemText", iRowCtr].Value.ToString());
                }
            }
            #endregion

            return dtTable;
        }
        #endregion
    }
}