namespace InSys
{
    partial class FrmTemplateDefinition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTemplateDefinition));
            this.gbTemplateDef = new System.Windows.Forms.GroupBox();
            this.tabTemplateDef = new System.Windows.Forms.TabControl();
            this.tabTerminal = new System.Windows.Forms.TabPage();
            this.dgvTerminal = new System.Windows.Forms.DataGridView();
            this.clmTerminalItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmTerminalItemText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTerminalMandatory = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmTerminalMandatoryText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTerminalTag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clbTagMandatory = new System.Windows.Forms.CheckedListBox();
            this.clbTagName = new System.Windows.Forms.CheckedListBox();
            this.tabAcquirer = new System.Windows.Forms.TabPage();
            this.dgvAcquirer = new System.Windows.Forms.DataGridView();
            this.clmAcquirerItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmAcquirerItemText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmAcquirerMandatory = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmAcquirerMandatoryText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmAcquirerTag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clbAcquirerMandatory = new System.Windows.Forms.CheckedListBox();
            this.clbItemAcquirerName = new System.Windows.Forms.CheckedListBox();
            this.btnChooseAcq = new System.Windows.Forms.Button();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbTemplateDef.SuspendLayout();
            this.tabTemplateDef.SuspendLayout();
            this.tabTerminal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).BeginInit();
            this.tabAcquirer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcquirer)).BeginInit();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbTemplateDef
            // 
            this.gbTemplateDef.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTemplateDef.Controls.Add(this.tabTemplateDef);
            this.gbTemplateDef.Location = new System.Drawing.Point(4, 8);
            this.gbTemplateDef.Name = "gbTemplateDef";
            this.gbTemplateDef.Size = new System.Drawing.Size(607, 432);
            this.gbTemplateDef.TabIndex = 0;
            this.gbTemplateDef.TabStop = false;
            // 
            // tabTemplateDef
            // 
            this.tabTemplateDef.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabTemplateDef.Controls.Add(this.tabTerminal);
            this.tabTemplateDef.Controls.Add(this.tabAcquirer);
            this.tabTemplateDef.Location = new System.Drawing.Point(4, 10);
            this.tabTemplateDef.Name = "tabTemplateDef";
            this.tabTemplateDef.SelectedIndex = 0;
            this.tabTemplateDef.Size = new System.Drawing.Size(601, 418);
            this.tabTemplateDef.TabIndex = 0;
            this.tabTemplateDef.SelectedIndexChanged += new System.EventHandler(this.tabTemplateDef_SelectedIndexChanged);
            // 
            // tabTerminal
            // 
            this.tabTerminal.BackColor = System.Drawing.SystemColors.Control;
            this.tabTerminal.Controls.Add(this.dgvTerminal);
            this.tabTerminal.Controls.Add(this.clbTagMandatory);
            this.tabTerminal.Controls.Add(this.clbTagName);
            this.tabTerminal.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabTerminal.Location = new System.Drawing.Point(4, 22);
            this.tabTerminal.Name = "tabTerminal";
            this.tabTerminal.Padding = new System.Windows.Forms.Padding(3);
            this.tabTerminal.Size = new System.Drawing.Size(593, 392);
            this.tabTerminal.TabIndex = 0;
            this.tabTerminal.Text = "Terminal";
            // 
            // dgvTerminal
            // 
            this.dgvTerminal.AllowUserToAddRows = false;
            this.dgvTerminal.AllowUserToDeleteRows = false;
            this.dgvTerminal.AllowUserToResizeRows = false;
            this.dgvTerminal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTerminal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTerminal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmTerminalItem,
            this.clmTerminalItemText,
            this.clmTerminalMandatory,
            this.clmTerminalMandatoryText,
            this.clmTerminalTag});
            this.dgvTerminal.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvTerminal.Location = new System.Drawing.Point(6, 6);
            this.dgvTerminal.MultiSelect = false;
            this.dgvTerminal.Name = "dgvTerminal";
            this.dgvTerminal.RowHeadersVisible = false;
            this.dgvTerminal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTerminal.Size = new System.Drawing.Size(581, 380);
            this.dgvTerminal.TabIndex = 0;
            // 
            // clmTerminalItem
            // 
            this.clmTerminalItem.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmTerminalItem.HeaderText = "";
            this.clmTerminalItem.Name = "clmTerminalItem";
            this.clmTerminalItem.Width = 5;
            // 
            // clmTerminalItemText
            // 
            this.clmTerminalItemText.HeaderText = "Item";
            this.clmTerminalItemText.Name = "clmTerminalItemText";
            this.clmTerminalItemText.ReadOnly = true;
            this.clmTerminalItemText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmTerminalMandatory
            // 
            this.clmTerminalMandatory.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmTerminalMandatory.HeaderText = "";
            this.clmTerminalMandatory.Name = "clmTerminalMandatory";
            this.clmTerminalMandatory.Width = 5;
            // 
            // clmTerminalMandatoryText
            // 
            this.clmTerminalMandatoryText.HeaderText = "Mandatory";
            this.clmTerminalMandatoryText.Name = "clmTerminalMandatoryText";
            this.clmTerminalMandatoryText.ReadOnly = true;
            this.clmTerminalMandatoryText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmTerminalTag
            // 
            this.clmTerminalTag.HeaderText = "Tag";
            this.clmTerminalTag.Name = "clmTerminalTag";
            this.clmTerminalTag.ReadOnly = true;
            this.clmTerminalTag.Visible = false;
            // 
            // clbTagMandatory
            // 
            this.clbTagMandatory.Enabled = false;
            this.clbTagMandatory.FormattingEnabled = true;
            this.clbTagMandatory.HorizontalScrollbar = true;
            this.clbTagMandatory.Location = new System.Drawing.Point(311, 25);
            this.clbTagMandatory.Name = "clbTagMandatory";
            this.clbTagMandatory.ScrollAlwaysVisible = true;
            this.clbTagMandatory.Size = new System.Drawing.Size(254, 334);
            this.clbTagMandatory.TabIndex = 3;
            // 
            // clbTagName
            // 
            this.clbTagName.Enabled = false;
            this.clbTagName.FormattingEnabled = true;
            this.clbTagName.HorizontalScrollbar = true;
            this.clbTagName.Location = new System.Drawing.Point(26, 25);
            this.clbTagName.Name = "clbTagName";
            this.clbTagName.ScrollAlwaysVisible = true;
            this.clbTagName.Size = new System.Drawing.Size(254, 334);
            this.clbTagName.TabIndex = 2;
            // 
            // tabAcquirer
            // 
            this.tabAcquirer.BackColor = System.Drawing.SystemColors.Control;
            this.tabAcquirer.Controls.Add(this.dgvAcquirer);
            this.tabAcquirer.Controls.Add(this.clbAcquirerMandatory);
            this.tabAcquirer.Controls.Add(this.clbItemAcquirerName);
            this.tabAcquirer.Controls.Add(this.btnChooseAcq);
            this.tabAcquirer.Location = new System.Drawing.Point(4, 22);
            this.tabAcquirer.Name = "tabAcquirer";
            this.tabAcquirer.Padding = new System.Windows.Forms.Padding(3);
            this.tabAcquirer.Size = new System.Drawing.Size(593, 392);
            this.tabAcquirer.TabIndex = 1;
            this.tabAcquirer.Text = "Acquirer";
            // 
            // dgvAcquirer
            // 
            this.dgvAcquirer.AllowUserToAddRows = false;
            this.dgvAcquirer.AllowUserToDeleteRows = false;
            this.dgvAcquirer.AllowUserToResizeRows = false;
            this.dgvAcquirer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAcquirer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAcquirer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAcquirer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmAcquirerItem,
            this.clmAcquirerItemText,
            this.clmAcquirerMandatory,
            this.clmAcquirerMandatoryText,
            this.clmAcquirerTag});
            this.dgvAcquirer.Location = new System.Drawing.Point(6, 6);
            this.dgvAcquirer.MultiSelect = false;
            this.dgvAcquirer.Name = "dgvAcquirer";
            this.dgvAcquirer.RowHeadersVisible = false;
            this.dgvAcquirer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAcquirer.Size = new System.Drawing.Size(436, 380);
            this.dgvAcquirer.TabIndex = 0;
            // 
            // clmAcquirerItem
            // 
            this.clmAcquirerItem.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmAcquirerItem.HeaderText = "";
            this.clmAcquirerItem.Name = "clmAcquirerItem";
            this.clmAcquirerItem.Width = 5;
            // 
            // clmAcquirerItemText
            // 
            this.clmAcquirerItemText.HeaderText = "Item";
            this.clmAcquirerItemText.Name = "clmAcquirerItemText";
            this.clmAcquirerItemText.ReadOnly = true;
            this.clmAcquirerItemText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmAcquirerMandatory
            // 
            this.clmAcquirerMandatory.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmAcquirerMandatory.HeaderText = "";
            this.clmAcquirerMandatory.Name = "clmAcquirerMandatory";
            this.clmAcquirerMandatory.Width = 5;
            // 
            // clmAcquirerMandatoryText
            // 
            this.clmAcquirerMandatoryText.HeaderText = "Mandatory";
            this.clmAcquirerMandatoryText.Name = "clmAcquirerMandatoryText";
            this.clmAcquirerMandatoryText.ReadOnly = true;
            this.clmAcquirerMandatoryText.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clmAcquirerTag
            // 
            this.clmAcquirerTag.HeaderText = "Tag";
            this.clmAcquirerTag.Name = "clmAcquirerTag";
            this.clmAcquirerTag.ReadOnly = true;
            this.clmAcquirerTag.Visible = false;
            // 
            // clbAcquirerMandatory
            // 
            this.clbAcquirerMandatory.Enabled = false;
            this.clbAcquirerMandatory.FormattingEnabled = true;
            this.clbAcquirerMandatory.Location = new System.Drawing.Point(238, 18);
            this.clbAcquirerMandatory.Name = "clbAcquirerMandatory";
            this.clbAcquirerMandatory.Size = new System.Drawing.Size(204, 349);
            this.clbAcquirerMandatory.TabIndex = 4;
            // 
            // clbItemAcquirerName
            // 
            this.clbItemAcquirerName.Enabled = false;
            this.clbItemAcquirerName.FormattingEnabled = true;
            this.clbItemAcquirerName.Location = new System.Drawing.Point(13, 18);
            this.clbItemAcquirerName.Name = "clbItemAcquirerName";
            this.clbItemAcquirerName.Size = new System.Drawing.Size(204, 349);
            this.clbItemAcquirerName.TabIndex = 3;
            // 
            // btnChooseAcq
            // 
            this.btnChooseAcq.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChooseAcq.Location = new System.Drawing.Point(448, 18);
            this.btnChooseAcq.Name = "btnChooseAcq";
            this.btnChooseAcq.Size = new System.Drawing.Size(139, 23);
            this.btnChooseAcq.TabIndex = 2;
            this.btnChooseAcq.Text = "Choose Acquirer";
            this.btnChooseAcq.UseVisualStyleBackColor = true;
            this.btnChooseAcq.Click += new System.EventHandler(this.btnChooseAcq_Click);
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(4, 446);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(607, 43);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(449, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(150, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(293, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(150, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmTemplateDefinition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(615, 494);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbTemplateDef);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTemplateDefinition";
            this.Opacity = 0;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Template Definition";
            this.Load += new System.EventHandler(this.FrmTemplateDefinition_Load);
            this.gbTemplateDef.ResumeLayout(false);
            this.tabTemplateDef.ResumeLayout(false);
            this.tabTerminal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).EndInit();
            this.tabAcquirer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcquirer)).EndInit();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbTemplateDef;
        private System.Windows.Forms.TabControl tabTemplateDef;
        private System.Windows.Forms.TabPage tabTerminal;
        private System.Windows.Forms.TabPage tabAcquirer;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnChooseAcq;
        private System.Windows.Forms.CheckedListBox clbTagMandatory;
        private System.Windows.Forms.CheckedListBox clbTagName;
        private System.Windows.Forms.CheckedListBox clbAcquirerMandatory;
        private System.Windows.Forms.CheckedListBox clbItemAcquirerName;
        private System.Windows.Forms.DataGridView dgvTerminal;
        private System.Windows.Forms.DataGridView dgvAcquirer;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmTerminalItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTerminalItemText;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmTerminalMandatory;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTerminalMandatoryText;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTerminalTag;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmAcquirerItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAcquirerItemText;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmAcquirerMandatory;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAcquirerMandatoryText;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmAcquirerTag;
    }
}