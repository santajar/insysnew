﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InSys.ServiceInSys {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceInSys.IServiceInSys")]
    public interface IServiceInSys {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/Add", ReplyAction="http://tempuri.org/IServiceInSys/AddResponse")]
        double Add(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/Add", ReplyAction="http://tempuri.org/IServiceInSys/AddResponse")]
        System.Threading.Tasks.Task<double> AddAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/Substract", ReplyAction="http://tempuri.org/IServiceInSys/SubstractResponse")]
        double Substract(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/Substract", ReplyAction="http://tempuri.org/IServiceInSys/SubstractResponse")]
        System.Threading.Tasks.Task<double> SubstractAsync(double a, double b);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/UploadFile", ReplyAction="http://tempuri.org/IServiceInSys/UploadFileResponse")]
        bool UploadFile(string Filename, byte[] Content, int Length);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/UploadFile", ReplyAction="http://tempuri.org/IServiceInSys/UploadFileResponse")]
        System.Threading.Tasks.Task<bool> UploadFileAsync(string Filename, byte[] Content, int Length);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/UploadFileFtp", ReplyAction="http://tempuri.org/IServiceInSys/UploadFileFtpResponse")]
        bool UploadFileFtp(string Filename, byte[] Content, int Length);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/UploadFileFtp", ReplyAction="http://tempuri.org/IServiceInSys/UploadFileFtpResponse")]
        System.Threading.Tasks.Task<bool> UploadFileFtpAsync(string Filename, byte[] Content, int Length);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/DeleteSubDirectoryFtp", ReplyAction="http://tempuri.org/IServiceInSys/DeleteSubDirectoryFtpResponse")]
        bool DeleteSubDirectoryFtp(string MainDirectory);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/DeleteSubDirectoryFtp", ReplyAction="http://tempuri.org/IServiceInSys/DeleteSubDirectoryFtpResponse")]
        System.Threading.Tasks.Task<bool> DeleteSubDirectoryFtpAsync(string MainDirectory);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/DirectoryListFtp", ReplyAction="http://tempuri.org/IServiceInSys/DirectoryListFtpResponse")]
        string[] DirectoryListFtp(string Directory);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/DirectoryListFtp", ReplyAction="http://tempuri.org/IServiceInSys/DirectoryListFtpResponse")]
        System.Threading.Tasks.Task<string[]> DirectoryListFtpAsync(string Directory);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/DownloadFtp", ReplyAction="http://tempuri.org/IServiceInSys/DownloadFtpResponse")]
        InSys.ServiceInSys.DownloadFtpResponse DownloadFtp(InSys.ServiceInSys.DownloadFtpRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/DownloadFtp", ReplyAction="http://tempuri.org/IServiceInSys/DownloadFtpResponse")]
        System.Threading.Tasks.Task<InSys.ServiceInSys.DownloadFtpResponse> DownloadFtpAsync(InSys.ServiceInSys.DownloadFtpRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/DeleteFtp", ReplyAction="http://tempuri.org/IServiceInSys/DeleteFtpResponse")]
        void DeleteFtp(string Filename);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/DeleteFtp", ReplyAction="http://tempuri.org/IServiceInSys/DeleteFtpResponse")]
        System.Threading.Tasks.Task DeleteFtpAsync(string Filename);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/LoginFtp", ReplyAction="http://tempuri.org/IServiceInSys/LoginFtpResponse")]
        bool LoginFtp();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/LoginFtp", ReplyAction="http://tempuri.org/IServiceInSys/LoginFtpResponse")]
        System.Threading.Tasks.Task<bool> LoginFtpAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/EmsAddUpdTms", ReplyAction="http://tempuri.org/IServiceInSys/EmsAddUpdTmsResponse")]
        InSys.ServiceInSys.EmsAddUpdTmsResponse EmsAddUpdTms(InSys.ServiceInSys.EmsAddUpdTmsRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/EmsAddUpdTms", ReplyAction="http://tempuri.org/IServiceInSys/EmsAddUpdTmsResponse")]
        System.Threading.Tasks.Task<InSys.ServiceInSys.EmsAddUpdTmsResponse> EmsAddUpdTmsAsync(InSys.ServiceInSys.EmsAddUpdTmsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/EmsInqTms", ReplyAction="http://tempuri.org/IServiceInSys/EmsInqTmsResponse")]
        InSys.ServiceInSys.EmsInqTmsResponse EmsInqTms(InSys.ServiceInSys.EmsInqTmsRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/EmsInqTms", ReplyAction="http://tempuri.org/IServiceInSys/EmsInqTmsResponse")]
        System.Threading.Tasks.Task<InSys.ServiceInSys.EmsInqTmsResponse> EmsInqTmsAsync(InSys.ServiceInSys.EmsInqTmsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/EmsDelTms", ReplyAction="http://tempuri.org/IServiceInSys/EmsDelTmsResponse")]
        InSys.ServiceInSys.EmsDelTmsResponse EmsDelTms(InSys.ServiceInSys.EmsDelTmsRequest request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/EmsDelTms", ReplyAction="http://tempuri.org/IServiceInSys/EmsDelTmsResponse")]
        System.Threading.Tasks.Task<InSys.ServiceInSys.EmsDelTmsResponse> EmsDelTmsAsync(InSys.ServiceInSys.EmsDelTmsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/ExportToProfile", ReplyAction="http://tempuri.org/IServiceInSys/ExportToProfileResponse")]
        void ExportToProfile(string sTerminalID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/ExportToProfile", ReplyAction="http://tempuri.org/IServiceInSys/ExportToProfileResponse")]
        System.Threading.Tasks.Task ExportToProfileAsync(string sTerminalID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/UploadFileNotAllow", ReplyAction="http://tempuri.org/IServiceInSys/UploadFileNotAllowResponse")]
        void UploadFileNotAllow(string sTerminalID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IServiceInSys/UploadFileNotAllow", ReplyAction="http://tempuri.org/IServiceInSys/UploadFileNotAllowResponse")]
        System.Threading.Tasks.Task UploadFileNotAllowAsync(string sTerminalID);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="DownloadFtp", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class DownloadFtpRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string RemoteFile;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=1)]
        public byte[] Content;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=2)]
        public int Length;
        
        public DownloadFtpRequest() {
        }
        
        public DownloadFtpRequest(string RemoteFile, byte[] Content, int Length) {
            this.RemoteFile = RemoteFile;
            this.Content = Content;
            this.Length = Length;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="DownloadFtpResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class DownloadFtpResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public byte[] Content;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=1)]
        public int Length;
        
        public DownloadFtpResponse() {
        }
        
        public DownloadFtpResponse(byte[] Content, int Length) {
            this.Content = Content;
            this.Length = Length;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="EmsAddUpdTms", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class EmsAddUpdTmsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public ipXML.EMS_XML emsXml;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=1)]
        public string sStatus;
        
        public EmsAddUpdTmsRequest() {
        }
        
        public EmsAddUpdTmsRequest(ipXML.EMS_XML emsXml, string sStatus) {
            this.emsXml = emsXml;
            this.sStatus = sStatus;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="EmsAddUpdTmsResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class EmsAddUpdTmsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string sStatus;
        
        public EmsAddUpdTmsResponse() {
        }
        
        public EmsAddUpdTmsResponse(string sStatus) {
            this.sStatus = sStatus;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="EmsInqTms", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class EmsInqTmsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public ipXML.EMS_XML emsXml;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=1)]
        public string sStatus;
        
        public EmsInqTmsRequest() {
        }
        
        public EmsInqTmsRequest(ipXML.EMS_XML emsXml, string sStatus) {
            this.emsXml = emsXml;
            this.sStatus = sStatus;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="EmsInqTmsResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class EmsInqTmsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string sStatus;
        
        public EmsInqTmsResponse() {
        }
        
        public EmsInqTmsResponse(string sStatus) {
            this.sStatus = sStatus;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="EmsDelTms", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class EmsDelTmsRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public ipXML.EMS_XML emsXml;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=1)]
        public string sStatus;
        
        public EmsDelTmsRequest() {
        }
        
        public EmsDelTmsRequest(ipXML.EMS_XML emsXml, string sStatus) {
            this.emsXml = emsXml;
            this.sStatus = sStatus;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="EmsDelTmsResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class EmsDelTmsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string sStatus;
        
        public EmsDelTmsResponse() {
        }
        
        public EmsDelTmsResponse(string sStatus) {
            this.sStatus = sStatus;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceInSysChannel : InSys.ServiceInSys.IServiceInSys, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceInSysClient : System.ServiceModel.ClientBase<InSys.ServiceInSys.IServiceInSys>, InSys.ServiceInSys.IServiceInSys {
        
        public ServiceInSysClient() {
        }
        
        public ServiceInSysClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceInSysClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceInSysClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceInSysClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public double Add(double a, double b) {
            return base.Channel.Add(a, b);
        }
        
        public System.Threading.Tasks.Task<double> AddAsync(double a, double b) {
            return base.Channel.AddAsync(a, b);
        }
        
        public double Substract(double a, double b) {
            return base.Channel.Substract(a, b);
        }
        
        public System.Threading.Tasks.Task<double> SubstractAsync(double a, double b) {
            return base.Channel.SubstractAsync(a, b);
        }
        
        public bool UploadFile(string Filename, byte[] Content, int Length) {
            return base.Channel.UploadFile(Filename, Content, Length);
        }
        
        public System.Threading.Tasks.Task<bool> UploadFileAsync(string Filename, byte[] Content, int Length) {
            return base.Channel.UploadFileAsync(Filename, Content, Length);
        }
        
        public bool UploadFileFtp(string Filename, byte[] Content, int Length) {
            return base.Channel.UploadFileFtp(Filename, Content, Length);
        }
        
        public System.Threading.Tasks.Task<bool> UploadFileFtpAsync(string Filename, byte[] Content, int Length) {
            return base.Channel.UploadFileFtpAsync(Filename, Content, Length);
        }
        
        public bool DeleteSubDirectoryFtp(string MainDirectory) {
            return base.Channel.DeleteSubDirectoryFtp(MainDirectory);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteSubDirectoryFtpAsync(string MainDirectory) {
            return base.Channel.DeleteSubDirectoryFtpAsync(MainDirectory);
        }
        
        public string[] DirectoryListFtp(string Directory) {
            return base.Channel.DirectoryListFtp(Directory);
        }
        
        public System.Threading.Tasks.Task<string[]> DirectoryListFtpAsync(string Directory) {
            return base.Channel.DirectoryListFtpAsync(Directory);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        InSys.ServiceInSys.DownloadFtpResponse InSys.ServiceInSys.IServiceInSys.DownloadFtp(InSys.ServiceInSys.DownloadFtpRequest request) {
            return base.Channel.DownloadFtp(request);
        }
        
        public void DownloadFtp(string RemoteFile, ref byte[] Content, ref int Length) {
            InSys.ServiceInSys.DownloadFtpRequest inValue = new InSys.ServiceInSys.DownloadFtpRequest();
            inValue.RemoteFile = RemoteFile;
            inValue.Content = Content;
            inValue.Length = Length;
            InSys.ServiceInSys.DownloadFtpResponse retVal = ((InSys.ServiceInSys.IServiceInSys)(this)).DownloadFtp(inValue);
            Content = retVal.Content;
            Length = retVal.Length;
        }
        
        public System.Threading.Tasks.Task<InSys.ServiceInSys.DownloadFtpResponse> DownloadFtpAsync(InSys.ServiceInSys.DownloadFtpRequest request) {
            return base.Channel.DownloadFtpAsync(request);
        }
        
        public void DeleteFtp(string Filename) {
            base.Channel.DeleteFtp(Filename);
        }
        
        public System.Threading.Tasks.Task DeleteFtpAsync(string Filename) {
            return base.Channel.DeleteFtpAsync(Filename);
        }
        
        public bool LoginFtp() {
            return base.Channel.LoginFtp();
        }
        
        public System.Threading.Tasks.Task<bool> LoginFtpAsync() {
            return base.Channel.LoginFtpAsync();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        InSys.ServiceInSys.EmsAddUpdTmsResponse InSys.ServiceInSys.IServiceInSys.EmsAddUpdTms(InSys.ServiceInSys.EmsAddUpdTmsRequest request) {
            return base.Channel.EmsAddUpdTms(request);
        }
        
        public void EmsAddUpdTms(ipXML.EMS_XML emsXml, ref string sStatus) {
            InSys.ServiceInSys.EmsAddUpdTmsRequest inValue = new InSys.ServiceInSys.EmsAddUpdTmsRequest();
            inValue.emsXml = emsXml;
            inValue.sStatus = sStatus;
            InSys.ServiceInSys.EmsAddUpdTmsResponse retVal = ((InSys.ServiceInSys.IServiceInSys)(this)).EmsAddUpdTms(inValue);
            sStatus = retVal.sStatus;
        }
        
        public System.Threading.Tasks.Task<InSys.ServiceInSys.EmsAddUpdTmsResponse> EmsAddUpdTmsAsync(InSys.ServiceInSys.EmsAddUpdTmsRequest request) {
            return base.Channel.EmsAddUpdTmsAsync(request);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        InSys.ServiceInSys.EmsInqTmsResponse InSys.ServiceInSys.IServiceInSys.EmsInqTms(InSys.ServiceInSys.EmsInqTmsRequest request) {
            return base.Channel.EmsInqTms(request);
        }
        
        public void EmsInqTms(ipXML.EMS_XML emsXml, ref string sStatus) {
            InSys.ServiceInSys.EmsInqTmsRequest inValue = new InSys.ServiceInSys.EmsInqTmsRequest();
            inValue.emsXml = emsXml;
            inValue.sStatus = sStatus;
            InSys.ServiceInSys.EmsInqTmsResponse retVal = ((InSys.ServiceInSys.IServiceInSys)(this)).EmsInqTms(inValue);
            sStatus = retVal.sStatus;
        }
        
        public System.Threading.Tasks.Task<InSys.ServiceInSys.EmsInqTmsResponse> EmsInqTmsAsync(InSys.ServiceInSys.EmsInqTmsRequest request) {
            return base.Channel.EmsInqTmsAsync(request);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        InSys.ServiceInSys.EmsDelTmsResponse InSys.ServiceInSys.IServiceInSys.EmsDelTms(InSys.ServiceInSys.EmsDelTmsRequest request) {
            return base.Channel.EmsDelTms(request);
        }
        
        public void EmsDelTms(ipXML.EMS_XML emsXml, ref string sStatus) {
            InSys.ServiceInSys.EmsDelTmsRequest inValue = new InSys.ServiceInSys.EmsDelTmsRequest();
            inValue.emsXml = emsXml;
            inValue.sStatus = sStatus;
            InSys.ServiceInSys.EmsDelTmsResponse retVal = ((InSys.ServiceInSys.IServiceInSys)(this)).EmsDelTms(inValue);
            sStatus = retVal.sStatus;
        }
        
        public System.Threading.Tasks.Task<InSys.ServiceInSys.EmsDelTmsResponse> EmsDelTmsAsync(InSys.ServiceInSys.EmsDelTmsRequest request) {
            return base.Channel.EmsDelTmsAsync(request);
        }
        
        public void ExportToProfile(string sTerminalID) {
            base.Channel.ExportToProfile(sTerminalID);
        }
        
        public System.Threading.Tasks.Task ExportToProfileAsync(string sTerminalID) {
            return base.Channel.ExportToProfileAsync(sTerminalID);
        }
        
        public void UploadFileNotAllow(string sTerminalID) {
            base.Channel.UploadFileNotAllow(sTerminalID);
        }
        
        public System.Threading.Tasks.Task UploadFileNotAllowAsync(string sTerminalID) {
            return base.Channel.UploadFileNotAllowAsync(sTerminalID);
        }
    }
}
