namespace InSys
{
    partial class FrmDatabasePrimaryConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDatabasePrimaryConnection));
            this.btnCancel = new System.Windows.Forms.Button();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.optSQL = new System.Windows.Forms.RadioButton();
            this.optWindows = new System.Windows.Forms.RadioButton();
            this.btnSaveConn = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.txtInitialCatalog = new System.Windows.Forms.TextBox();
            this.txtDataSource = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(291, 105);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(144, 32);
            this.btnCancel.TabIndex = 36;
            this.btnCancel.Text = "Cancel";
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("PictureBox1.Image")));
            this.PictureBox1.Location = new System.Drawing.Point(19, 17);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(40, 40);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox1.TabIndex = 35;
            this.PictureBox1.TabStop = false;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.txtPassword);
            this.GroupBox1.Controls.Add(this.txtUserId);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Location = new System.Drawing.Point(11, 121);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(272, 88);
            this.GroupBox1.TabIndex = 32;
            this.GroupBox1.TabStop = false;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(88, 56);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(168, 20);
            this.txtPassword.TabIndex = 5;
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(88, 24);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(168, 20);
            this.txtUserId.TabIndex = 4;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(16, 55);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(100, 23);
            this.Label4.TabIndex = 9;
            this.Label4.Text = "Password";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(16, 23);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 23);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "Login Name";
            // 
            // optSQL
            // 
            this.optSQL.Checked = true;
            this.optSQL.Location = new System.Drawing.Point(19, 97);
            this.optSQL.Name = "optSQL";
            this.optSQL.Size = new System.Drawing.Size(176, 24);
            this.optSQL.TabIndex = 30;
            this.optSQL.TabStop = true;
            this.optSQL.Text = "SQL Server Authentication";
            // 
            // optWindows
            // 
            this.optWindows.Location = new System.Drawing.Point(19, 73);
            this.optWindows.Name = "optWindows";
            this.optWindows.Size = new System.Drawing.Size(160, 24);
            this.optWindows.TabIndex = 31;
            this.optWindows.Text = "Windows Authentication";
            // 
            // btnSaveConn
            // 
            this.btnSaveConn.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveConn.Image")));
            this.btnSaveConn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveConn.Location = new System.Drawing.Point(291, 57);
            this.btnSaveConn.Name = "btnSaveConn";
            this.btnSaveConn.Size = new System.Drawing.Size(144, 32);
            this.btnSaveConn.TabIndex = 34;
            this.btnSaveConn.Text = "Save Connection";
            // 
            // btnTest
            // 
            this.btnTest.Image = ((System.Drawing.Image)(resources.GetObject("btnTest.Image")));
            this.btnTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTest.Location = new System.Drawing.Point(291, 9);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(144, 32);
            this.btnTest.TabIndex = 33;
            this.btnTest.Text = "Test Connection";
            // 
            // txtInitialCatalog
            // 
            this.txtInitialCatalog.Location = new System.Drawing.Point(155, 41);
            this.txtInitialCatalog.Name = "txtInitialCatalog";
            this.txtInitialCatalog.ReadOnly = true;
            this.txtInitialCatalog.Size = new System.Drawing.Size(128, 20);
            this.txtInitialCatalog.TabIndex = 28;
            this.txtInitialCatalog.Text = "DbTMS";
            // 
            // txtDataSource
            // 
            this.txtDataSource.Location = new System.Drawing.Point(155, 9);
            this.txtDataSource.Name = "txtDataSource";
            this.txtDataSource.Size = new System.Drawing.Size(128, 20);
            this.txtDataSource.TabIndex = 26;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(75, 41);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(100, 24);
            this.Label2.TabIndex = 29;
            this.Label2.Text = "Initial Catalog";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(75, 9);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 24);
            this.Label1.TabIndex = 27;
            this.Label1.Text = "Data Source";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 217);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.PictureBox1);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.optSQL);
            this.Controls.Add(this.optWindows);
            this.Controls.Add(this.btnSaveConn);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.txtInitialCatalog);
            this.Controls.Add(this.txtDataSource);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Name = "Form1";
            this.Text = "Primary Database Connection";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.TextBox txtPassword;
        internal System.Windows.Forms.TextBox txtUserId;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.RadioButton optSQL;
        internal System.Windows.Forms.RadioButton optWindows;
        internal System.Windows.Forms.Button btnSaveConn;
        internal System.Windows.Forms.Button btnTest;
        internal System.Windows.Forms.TextBox txtInitialCatalog;
        internal System.Windows.Forms.TextBox txtDataSource;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
    }
}