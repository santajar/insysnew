namespace TMS_InSys
{
    partial class FrmEditAID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditAID));
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.CmbMatchCriteria = new System.Windows.Forms.ComboBox();
            this.TxtAIDPritedName = new System.Windows.Forms.TextBox();
            this.TxtVersionList = new System.Windows.Forms.TextBox();
            this.TxtAIDList = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnCancel.Location = new System.Drawing.Point(172, 145);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 19;
            this.BtnCancel.Text = "Cancel";
            // 
            // BtnSave
            // 
            this.BtnSave.Image = ((System.Drawing.Image)(resources.GetObject("BtnSave.Image")));
            this.BtnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnSave.Location = new System.Drawing.Point(76, 145);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 18;
            this.BtnSave.Text = "Save";
            // 
            // CmbMatchCriteria
            // 
            this.CmbMatchCriteria.Items.AddRange(new object[] {
            "EMV AID FULL MATCH",
            "EMV AID PARTIAL MATCH"});
            this.CmbMatchCriteria.Location = new System.Drawing.Point(124, 97);
            this.CmbMatchCriteria.Name = "CmbMatchCriteria";
            this.CmbMatchCriteria.Size = new System.Drawing.Size(168, 21);
            this.CmbMatchCriteria.TabIndex = 17;
            // 
            // TxtAIDPritedName
            // 
            this.TxtAIDPritedName.Location = new System.Drawing.Point(124, 65);
            this.TxtAIDPritedName.MaxLength = 50;
            this.TxtAIDPritedName.Name = "TxtAIDPritedName";
            this.TxtAIDPritedName.Size = new System.Drawing.Size(168, 20);
            this.TxtAIDPritedName.TabIndex = 16;
            // 
            // TxtVersionList
            // 
            this.TxtVersionList.Location = new System.Drawing.Point(124, 41);
            this.TxtVersionList.MaxLength = 5;
            this.TxtVersionList.Name = "TxtVersionList";
            this.TxtVersionList.Size = new System.Drawing.Size(168, 20);
            this.TxtVersionList.TabIndex = 15;
            // 
            // TxtAIDList
            // 
            this.TxtAIDList.Location = new System.Drawing.Point(124, 9);
            this.TxtAIDList.MaxLength = 20;
            this.TxtAIDList.Name = "TxtAIDList";
            this.TxtAIDList.Size = new System.Drawing.Size(168, 20);
            this.TxtAIDList.TabIndex = 10;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(12, 105);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(120, 23);
            this.Label4.TabIndex = 14;
            this.Label4.Text = "Matching Criteria     :";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(12, 73);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(112, 23);
            this.Label3.TabIndex = 13;
            this.Label3.Text = "AID Printed Name   :";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(15, 41);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(112, 23);
            this.Label2.TabIndex = 12;
            this.Label2.Text = "Version List             :";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(12, 9);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(112, 23);
            this.Label1.TabIndex = 11;
            this.Label1.Text = "AID List                   :";
            // 
            // FrmEditAID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 179);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.CmbMatchCriteria);
            this.Controls.Add(this.TxtAIDPritedName);
            this.Controls.Add(this.TxtVersionList);
            this.Controls.Add(this.TxtAIDList);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Name = "FrmEditAID";
            this.Text = "Edit AID";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.ComboBox CmbMatchCriteria;
        internal System.Windows.Forms.TextBox TxtAIDPritedName;
        internal System.Windows.Forms.TextBox TxtVersionList;
        internal System.Windows.Forms.TextBox TxtAIDList;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
    }
}