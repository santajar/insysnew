namespace InSys
{
    partial class FrmPing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbConnect = new System.Windows.Forms.ListBox();
            this.lbDisConnect = new System.Windows.Forms.ListBox();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.TimerPing = new System.Windows.Forms.Timer(this.components);
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnReload = new System.Windows.Forms.Button();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Button1 = new System.Windows.Forms.Button();
            this.gbConnect = new System.Windows.Forms.GroupBox();
            this.gbDisConnect = new System.Windows.Forms.GroupBox();
            this.gbConnect.SuspendLayout();
            this.gbDisConnect.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbConnect
            // 
            this.lbConnect.BackColor = System.Drawing.Color.Lime;
            this.lbConnect.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConnect.ItemHeight = 15;
            this.lbConnect.Location = new System.Drawing.Point(16, 19);
            this.lbConnect.Name = "lbConnect";
            this.lbConnect.Size = new System.Drawing.Size(256, 394);
            this.lbConnect.TabIndex = 0;
            // 
            // lbDisConnect
            // 
            this.lbDisConnect.BackColor = System.Drawing.Color.Red;
            this.lbDisConnect.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDisConnect.ItemHeight = 15;
            this.lbDisConnect.Location = new System.Drawing.Point(16, 19);
            this.lbDisConnect.Name = "lbDisConnect";
            this.lbDisConnect.Size = new System.Drawing.Size(256, 394);
            this.lbDisConnect.TabIndex = 1;
            // 
            // tbSearch
            // 
            this.tbSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSearch.Location = new System.Drawing.Point(620, 84);
            this.tbSearch.MaxLength = 8;
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(112, 21);
            this.tbSearch.TabIndex = 17;
            this.tbSearch.Text = "TextBox1";
            // 
            // TimerPing
            // 
            this.TimerPing.Interval = 3000;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(620, 52);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(112, 23);
            this.btnSearch.TabIndex = 16;
            this.btnSearch.Text = "Search Terminal";
            this.ToolTip1.SetToolTip(this.btnSearch, "Press the Refresh button to continue tracking");
            // 
            // btnReload
            // 
            this.btnReload.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReload.Location = new System.Drawing.Point(620, 12);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(112, 23);
            this.btnReload.TabIndex = 15;
            this.btnReload.Text = "Refresh";
            // 
            // Button1
            // 
            this.Button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button1.Location = new System.Drawing.Point(628, 268);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(75, 23);
            this.Button1.TabIndex = 14;
            this.Button1.Text = "Button1";
            this.Button1.Visible = false;
            // 
            // gbConnect
            // 
            this.gbConnect.BackColor = System.Drawing.SystemColors.Control;
            this.gbConnect.Controls.Add(this.lbConnect);
            this.gbConnect.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbConnect.Location = new System.Drawing.Point(12, 12);
            this.gbConnect.Name = "gbConnect";
            this.gbConnect.Size = new System.Drawing.Size(288, 432);
            this.gbConnect.TabIndex = 12;
            this.gbConnect.TabStop = false;
            this.gbConnect.Text = "Connect";
            // 
            // gbDisConnect
            // 
            this.gbDisConnect.BackColor = System.Drawing.SystemColors.Control;
            this.gbDisConnect.Controls.Add(this.lbDisConnect);
            this.gbDisConnect.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDisConnect.Location = new System.Drawing.Point(316, 12);
            this.gbDisConnect.Name = "gbDisConnect";
            this.gbDisConnect.Size = new System.Drawing.Size(288, 432);
            this.gbDisConnect.TabIndex = 13;
            this.gbDisConnect.TabStop = false;
            this.gbDisConnect.Text = "DisConnect";
            // 
            // FrmPing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 451);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.gbConnect);
            this.Controls.Add(this.gbDisConnect);
            this.Name = "FrmPing";
            this.Text = "EDC Tracker";
            this.gbConnect.ResumeLayout(false);
            this.gbDisConnect.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ListBox lbConnect;
        internal System.Windows.Forms.ListBox lbDisConnect;
        internal System.Windows.Forms.TextBox tbSearch;
        internal System.Windows.Forms.Timer TimerPing;
        internal System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.Button btnReload;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.GroupBox gbConnect;
        internal System.Windows.Forms.GroupBox gbDisConnect;
    }
}