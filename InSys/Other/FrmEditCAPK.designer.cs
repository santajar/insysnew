namespace TMS_InSys
{
    partial class FrmEditCAPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditCAPK));
            this.TxtHash = new System.Windows.Forms.TextBox();
            this.txtRID = new System.Windows.Forms.TextBox();
            this.TxtModulus = new System.Windows.Forms.TextBox();
            this.TxtLength = new System.Windows.Forms.TextBox();
            this.TxtExponent = new System.Windows.Forms.TextBox();
            this.TxtIndex = new System.Windows.Forms.TextBox();
            this.LblHash = new System.Windows.Forms.Label();
            this.LblRID = new System.Windows.Forms.Label();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TxtHash
            // 
            this.TxtHash.Location = new System.Drawing.Point(126, 233);
            this.TxtHash.MaxLength = 40;
            this.TxtHash.Multiline = true;
            this.TxtHash.Name = "TxtHash";
            this.TxtHash.Size = new System.Drawing.Size(232, 48);
            this.TxtHash.TabIndex = 22;
            this.TxtHash.Text = "TextBox2";
            // 
            // txtRID
            // 
            this.txtRID.Location = new System.Drawing.Point(126, 209);
            this.txtRID.MaxLength = 12;
            this.txtRID.Name = "txtRID";
            this.txtRID.Size = new System.Drawing.Size(232, 20);
            this.txtRID.TabIndex = 21;
            this.txtRID.Text = "TextBox1";
            // 
            // TxtModulus
            // 
            this.TxtModulus.Location = new System.Drawing.Point(126, 81);
            this.TxtModulus.Multiline = true;
            this.TxtModulus.Name = "TxtModulus";
            this.TxtModulus.Size = new System.Drawing.Size(232, 120);
            this.TxtModulus.TabIndex = 20;
            this.TxtModulus.Text = "TextBox4";
            // 
            // TxtLength
            // 
            this.TxtLength.Location = new System.Drawing.Point(126, 57);
            this.TxtLength.MaxLength = 5;
            this.TxtLength.Name = "TxtLength";
            this.TxtLength.Size = new System.Drawing.Size(100, 20);
            this.TxtLength.TabIndex = 18;
            this.TxtLength.Text = "TextBox3";
            // 
            // TxtExponent
            // 
            this.TxtExponent.Location = new System.Drawing.Point(126, 33);
            this.TxtExponent.MaxLength = 6;
            this.TxtExponent.Name = "TxtExponent";
            this.TxtExponent.Size = new System.Drawing.Size(100, 20);
            this.TxtExponent.TabIndex = 15;
            this.TxtExponent.Text = "TextBox2";
            // 
            // TxtIndex
            // 
            this.TxtIndex.Location = new System.Drawing.Point(126, 9);
            this.TxtIndex.MaxLength = 2;
            this.TxtIndex.Name = "TxtIndex";
            this.TxtIndex.Size = new System.Drawing.Size(100, 20);
            this.TxtIndex.TabIndex = 13;
            this.TxtIndex.Text = "TextBox1";
            // 
            // LblHash
            // 
            this.LblHash.Location = new System.Drawing.Point(14, 233);
            this.LblHash.Name = "LblHash";
            this.LblHash.Size = new System.Drawing.Size(100, 23);
            this.LblHash.TabIndex = 26;
            this.LblHash.Text = "Hash";
            // 
            // LblRID
            // 
            this.LblRID.Location = new System.Drawing.Point(14, 209);
            this.LblRID.Name = "LblRID";
            this.LblRID.Size = new System.Drawing.Size(100, 23);
            this.LblRID.TabIndex = 25;
            this.LblRID.Text = "RID";
            // 
            // BtnCancel
            // 
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnCancel.Location = new System.Drawing.Point(214, 305);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 24;
            this.BtnCancel.Text = "Cancel";
            // 
            // BtnSave
            // 
            this.BtnSave.Image = ((System.Drawing.Image)(resources.GetObject("BtnSave.Image")));
            this.BtnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnSave.Location = new System.Drawing.Point(94, 305);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 23;
            this.BtnSave.Text = "Save";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(14, 80);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(100, 23);
            this.Label4.TabIndex = 19;
            this.Label4.Text = "Modulus";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(14, 58);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 23);
            this.Label3.TabIndex = 17;
            this.Label3.Text = "Length";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(14, 34);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(100, 16);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "Exponent";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(14, 9);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 23);
            this.Label1.TabIndex = 14;
            this.Label1.Text = "Index";
            // 
            // FrmEditCAPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 343);
            this.Controls.Add(this.TxtHash);
            this.Controls.Add(this.txtRID);
            this.Controls.Add(this.TxtModulus);
            this.Controls.Add(this.TxtLength);
            this.Controls.Add(this.TxtExponent);
            this.Controls.Add(this.TxtIndex);
            this.Controls.Add(this.LblHash);
            this.Controls.Add(this.LblRID);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Name = "FrmEditCAPK";
            this.Text = "FrmCAPK";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox TxtHash;
        internal System.Windows.Forms.TextBox txtRID;
        internal System.Windows.Forms.TextBox TxtModulus;
        internal System.Windows.Forms.TextBox TxtLength;
        internal System.Windows.Forms.TextBox TxtExponent;
        internal System.Windows.Forms.TextBox TxtIndex;
        internal System.Windows.Forms.Label LblHash;
        internal System.Windows.Forms.Label LblRID;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
    }
}