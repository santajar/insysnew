namespace InSys
{
    partial class FrmLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLog));
            this.BtnImport = new System.Windows.Forms.Button();
            this.BtnExport = new System.Windows.Forms.Button();
            this.TxtDetailLog = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.OpenFileDialogImport = new System.Windows.Forms.OpenFileDialog();
            this.BtnFilter = new System.Windows.Forms.Button();
            this.cmdClear = new System.Windows.Forms.Button();
            this.gridData = new System.Windows.Forms.DataGrid();
            this.cmdRefresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnImport
            // 
            this.BtnImport.Image = ((System.Drawing.Image)(resources.GetObject("BtnImport.Image")));
            this.BtnImport.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnImport.Location = new System.Drawing.Point(392, 7);
            this.BtnImport.Name = "BtnImport";
            this.BtnImport.Size = new System.Drawing.Size(120, 24);
            this.BtnImport.TabIndex = 23;
            this.BtnImport.Text = "Import";
            // 
            // BtnExport
            // 
            this.BtnExport.Image = ((System.Drawing.Image)(resources.GetObject("BtnExport.Image")));
            this.BtnExport.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnExport.Location = new System.Drawing.Point(512, 7);
            this.BtnExport.Name = "BtnExport";
            this.BtnExport.Size = new System.Drawing.Size(120, 24);
            this.BtnExport.TabIndex = 22;
            this.BtnExport.Text = "Export";
            // 
            // TxtDetailLog
            // 
            this.TxtDetailLog.Location = new System.Drawing.Point(8, 407);
            this.TxtDetailLog.Multiline = true;
            this.TxtDetailLog.Name = "TxtDetailLog";
            this.TxtDetailLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TxtDetailLog.Size = new System.Drawing.Size(744, 104);
            this.TxtDetailLog.TabIndex = 20;
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(8, 386);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 13);
            this.Label1.TabIndex = 21;
            this.Label1.Text = "Detail";
            // 
            // BtnFilter
            // 
            this.BtnFilter.Image = ((System.Drawing.Image)(resources.GetObject("BtnFilter.Image")));
            this.BtnFilter.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnFilter.Location = new System.Drawing.Point(272, 7);
            this.BtnFilter.Name = "BtnFilter";
            this.BtnFilter.Size = new System.Drawing.Size(120, 24);
            this.BtnFilter.TabIndex = 19;
            this.BtnFilter.Text = "Filter";
            // 
            // cmdClear
            // 
            this.cmdClear.Image = ((System.Drawing.Image)(resources.GetObject("cmdClear.Image")));
            this.cmdClear.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.cmdClear.Location = new System.Drawing.Point(632, 7);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(120, 24);
            this.cmdClear.TabIndex = 18;
            this.cmdClear.Text = "Delete LOG";
            // 
            // gridData
            // 
            this.gridData.DataMember = "";
            this.gridData.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.gridData.Location = new System.Drawing.Point(8, 39);
            this.gridData.Name = "gridData";
            this.gridData.ReadOnly = true;
            this.gridData.Size = new System.Drawing.Size(744, 344);
            this.gridData.TabIndex = 17;
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Image = ((System.Drawing.Image)(resources.GetObject("cmdRefresh.Image")));
            this.cmdRefresh.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.cmdRefresh.Location = new System.Drawing.Point(8, 7);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(264, 24);
            this.cmdRefresh.TabIndex = 16;
            this.cmdRefresh.Text = "(&R)EFRESH LIST";
            // 
            // FrmLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 518);
            this.Controls.Add(this.BtnImport);
            this.Controls.Add(this.BtnExport);
            this.Controls.Add(this.TxtDetailLog);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.BtnFilter);
            this.Controls.Add(this.cmdClear);
            this.Controls.Add(this.gridData);
            this.Controls.Add(this.cmdRefresh);
            this.Name = "FrmLog";
            this.Text = "User Access Log";
            ((System.ComponentModel.ISupportInitialize)(this.gridData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button BtnImport;
        internal System.Windows.Forms.Button BtnExport;
        internal System.Windows.Forms.TextBox TxtDetailLog;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.OpenFileDialog OpenFileDialogImport;
        internal System.Windows.Forms.Button BtnFilter;
        internal System.Windows.Forms.Button cmdClear;
        internal System.Windows.Forms.DataGrid gridData;
        internal System.Windows.Forms.Button cmdRefresh;
    }
}