namespace InSys
{
    partial class FrmModem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmModem));
            this.Button1 = new System.Windows.Forms.Button();
            this.GroupBox8 = new System.Windows.Forms.GroupBox();
            this.optSync = new System.Windows.Forms.RadioButton();
            this.optAsync = new System.Windows.Forms.RadioButton();
            this.optBEll = new System.Windows.Forms.RadioButton();
            this.optV32Bis = new System.Windows.Forms.RadioButton();
            this.optTone = new System.Windows.Forms.RadioButton();
            this.Button2 = new System.Windows.Forms.Button();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.GroupBox7 = new System.Windows.Forms.GroupBox();
            this.optV32Modulation = new System.Windows.Forms.RadioButton();
            this.optV22BIS = new System.Windows.Forms.RadioButton();
            this.optV22CCITT = new System.Windows.Forms.RadioButton();
            this.optV21CCITT = new System.Windows.Forms.RadioButton();
            this.optBest = new System.Windows.Forms.RadioButton();
            this.GroupBox6 = new System.Windows.Forms.GroupBox();
            this.optPulse = new System.Windows.Forms.RadioButton();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.optCCITT = new System.Windows.Forms.RadioButton();
            this.GroupBox9 = new System.Windows.Forms.GroupBox();
            this.txtTPDUorigin = new System.Windows.Forms.TextBox();
            this.txtTPDUId = new System.Windows.Forms.TextBox();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.GroupBox8.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.GroupBox7.SuspendLayout();
            this.GroupBox6.SuspendLayout();
            this.GroupBox5.SuspendLayout();
            this.GroupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // Button1
            // 
            this.Button1.Image = ((System.Drawing.Image)(resources.GetObject("Button1.Image")));
            this.Button1.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.Button1.Location = new System.Drawing.Point(28, 412);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(88, 23);
            this.Button1.TabIndex = 55;
            this.Button1.Text = "Save";
            // 
            // GroupBox8
            // 
            this.GroupBox8.Controls.Add(this.optSync);
            this.GroupBox8.Controls.Add(this.optAsync);
            this.GroupBox8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox8.Location = new System.Drawing.Point(8, 232);
            this.GroupBox8.Name = "GroupBox8";
            this.GroupBox8.Size = new System.Drawing.Size(232, 48);
            this.GroupBox8.TabIndex = 44;
            this.GroupBox8.TabStop = false;
            this.GroupBox8.Text = "ASYNC / SYNC";
            // 
            // optSync
            // 
            this.optSync.Checked = true;
            this.optSync.Location = new System.Drawing.Point(120, 16);
            this.optSync.Name = "optSync";
            this.optSync.Size = new System.Drawing.Size(104, 24);
            this.optSync.TabIndex = 1;
            this.optSync.TabStop = true;
            this.optSync.Text = "Synchronous";
            // 
            // optAsync
            // 
            this.optAsync.Location = new System.Drawing.Point(16, 16);
            this.optAsync.Name = "optAsync";
            this.optAsync.Size = new System.Drawing.Size(104, 24);
            this.optAsync.TabIndex = 0;
            this.optAsync.Text = "Asynchronous";
            // 
            // optBEll
            // 
            this.optBEll.Checked = true;
            this.optBEll.Location = new System.Drawing.Point(120, 16);
            this.optBEll.Name = "optBEll";
            this.optBEll.Size = new System.Drawing.Size(56, 24);
            this.optBEll.TabIndex = 1;
            this.optBEll.TabStop = true;
            this.optBEll.Text = "Bell";
            // 
            // optV32Bis
            // 
            this.optV32Bis.Location = new System.Drawing.Point(120, 64);
            this.optV32Bis.Name = "optV32Bis";
            this.optV32Bis.Size = new System.Drawing.Size(72, 24);
            this.optV32Bis.TabIndex = 5;
            this.optV32Bis.Text = "V.32 Bis";
            // 
            // optTone
            // 
            this.optTone.Checked = true;
            this.optTone.Location = new System.Drawing.Point(120, 16);
            this.optTone.Name = "optTone";
            this.optTone.Size = new System.Drawing.Size(56, 24);
            this.optTone.TabIndex = 1;
            this.optTone.TabStop = true;
            this.optTone.Text = "Tone";
            // 
            // Button2
            // 
            this.Button2.Image = ((System.Drawing.Image)(resources.GetObject("Button2.Image")));
            this.Button2.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.Button2.Location = new System.Drawing.Point(148, 412);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(88, 23);
            this.Button2.TabIndex = 56;
            this.Button2.Text = "Cancel";
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.GroupBox7);
            this.GroupBox4.Controls.Add(this.GroupBox6);
            this.GroupBox4.Controls.Add(this.GroupBox5);
            this.GroupBox4.Controls.Add(this.GroupBox8);
            this.GroupBox4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox4.Location = new System.Drawing.Point(12, 12);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(248, 296);
            this.GroupBox4.TabIndex = 53;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "Modem Setting";
            // 
            // GroupBox7
            // 
            this.GroupBox7.Controls.Add(this.optV32Bis);
            this.GroupBox7.Controls.Add(this.optV32Modulation);
            this.GroupBox7.Controls.Add(this.optV22BIS);
            this.GroupBox7.Controls.Add(this.optV22CCITT);
            this.GroupBox7.Controls.Add(this.optV21CCITT);
            this.GroupBox7.Controls.Add(this.optBest);
            this.GroupBox7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox7.Location = new System.Drawing.Point(0, 128);
            this.GroupBox7.Name = "GroupBox7";
            this.GroupBox7.Size = new System.Drawing.Size(240, 96);
            this.GroupBox7.TabIndex = 2;
            this.GroupBox7.TabStop = false;
            this.GroupBox7.Text = "MODULATION";
            // 
            // optV32Modulation
            // 
            this.optV32Modulation.Location = new System.Drawing.Point(120, 40);
            this.optV32Modulation.Name = "optV32Modulation";
            this.optV32Modulation.Size = new System.Drawing.Size(112, 24);
            this.optV32Modulation.TabIndex = 4;
            this.optV32Modulation.Text = "V.32 Modulation";
            // 
            // optV22BIS
            // 
            this.optV22BIS.Location = new System.Drawing.Point(120, 16);
            this.optV22BIS.Name = "optV22BIS";
            this.optV22BIS.Size = new System.Drawing.Size(104, 24);
            this.optV22BIS.TabIndex = 3;
            this.optV22BIS.Text = "V.22 Bis";
            // 
            // optV22CCITT
            // 
            this.optV22CCITT.Checked = true;
            this.optV22CCITT.Location = new System.Drawing.Point(16, 64);
            this.optV22CCITT.Name = "optV22CCITT";
            this.optV22CCITT.Size = new System.Drawing.Size(104, 24);
            this.optV22CCITT.TabIndex = 2;
            this.optV22CCITT.TabStop = true;
            this.optV22CCITT.Text = "V.22 ( CCITT )";
            // 
            // optV21CCITT
            // 
            this.optV21CCITT.Location = new System.Drawing.Point(16, 40);
            this.optV21CCITT.Name = "optV21CCITT";
            this.optV21CCITT.Size = new System.Drawing.Size(104, 24);
            this.optV21CCITT.TabIndex = 1;
            this.optV21CCITT.Text = "V.21 ( CCITT )";
            // 
            // optBest
            // 
            this.optBest.Location = new System.Drawing.Point(16, 16);
            this.optBest.Name = "optBest";
            this.optBest.Size = new System.Drawing.Size(104, 24);
            this.optBest.TabIndex = 0;
            this.optBest.Text = "Best";
            // 
            // GroupBox6
            // 
            this.GroupBox6.Controls.Add(this.optTone);
            this.GroupBox6.Controls.Add(this.optPulse);
            this.GroupBox6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox6.Location = new System.Drawing.Point(8, 72);
            this.GroupBox6.Name = "GroupBox6";
            this.GroupBox6.Size = new System.Drawing.Size(232, 48);
            this.GroupBox6.TabIndex = 1;
            this.GroupBox6.TabStop = false;
            this.GroupBox6.Text = "PULSE / TONE";
            // 
            // optPulse
            // 
            this.optPulse.Location = new System.Drawing.Point(16, 16);
            this.optPulse.Name = "optPulse";
            this.optPulse.Size = new System.Drawing.Size(104, 24);
            this.optPulse.TabIndex = 0;
            this.optPulse.Text = "Pulse";
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.optBEll);
            this.GroupBox5.Controls.Add(this.optCCITT);
            this.GroupBox5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox5.Location = new System.Drawing.Point(8, 16);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(232, 48);
            this.GroupBox5.TabIndex = 0;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Text = "CCITT / BELL";
            // 
            // optCCITT
            // 
            this.optCCITT.Location = new System.Drawing.Point(16, 16);
            this.optCCITT.Name = "optCCITT";
            this.optCCITT.Size = new System.Drawing.Size(72, 24);
            this.optCCITT.TabIndex = 0;
            this.optCCITT.Text = "CCITT";
            // 
            // GroupBox9
            // 
            this.GroupBox9.Controls.Add(this.txtTPDUorigin);
            this.GroupBox9.Controls.Add(this.txtTPDUId);
            this.GroupBox9.Controls.Add(this.Label19);
            this.GroupBox9.Controls.Add(this.Label12);
            this.GroupBox9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox9.Location = new System.Drawing.Point(12, 316);
            this.GroupBox9.Name = "GroupBox9";
            this.GroupBox9.Size = new System.Drawing.Size(248, 80);
            this.GroupBox9.TabIndex = 54;
            this.GroupBox9.TabStop = false;
            this.GroupBox9.Text = "TPDU Setting";
            // 
            // txtTPDUorigin
            // 
            this.txtTPDUorigin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTPDUorigin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTPDUorigin.Location = new System.Drawing.Point(128, 48);
            this.txtTPDUorigin.MaxLength = 4;
            this.txtTPDUorigin.Name = "txtTPDUorigin";
            this.txtTPDUorigin.Size = new System.Drawing.Size(100, 20);
            this.txtTPDUorigin.TabIndex = 3;
            // 
            // txtTPDUId
            // 
            this.txtTPDUId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTPDUId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTPDUId.Location = new System.Drawing.Point(128, 24);
            this.txtTPDUId.MaxLength = 2;
            this.txtTPDUId.Name = "txtTPDUId";
            this.txtTPDUId.Size = new System.Drawing.Size(100, 20);
            this.txtTPDUId.TabIndex = 2;
            // 
            // Label19
            // 
            this.Label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label19.Location = new System.Drawing.Point(16, 48);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(100, 23);
            this.Label19.TabIndex = 1;
            this.Label19.Text = "Original Address ";
            // 
            // Label12
            // 
            this.Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(16, 24);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(100, 23);
            this.Label12.TabIndex = 0;
            this.Label12.Text = "TPDU ID ";
            // 
            // FrmModem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 444);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.GroupBox4);
            this.Controls.Add(this.GroupBox9);
            this.Name = "FrmModem";
            this.Text = "Modem";
            this.Load += new System.EventHandler(this.FrmModem_Load);
            this.GroupBox8.ResumeLayout(false);
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox7.ResumeLayout(false);
            this.GroupBox6.ResumeLayout(false);
            this.GroupBox5.ResumeLayout(false);
            this.GroupBox9.ResumeLayout(false);
            this.GroupBox9.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.GroupBox GroupBox8;
        internal System.Windows.Forms.RadioButton optSync;
        internal System.Windows.Forms.RadioButton optAsync;
        internal System.Windows.Forms.RadioButton optBEll;
        internal System.Windows.Forms.RadioButton optV32Bis;
        internal System.Windows.Forms.RadioButton optTone;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.GroupBox GroupBox7;
        internal System.Windows.Forms.RadioButton optV32Modulation;
        internal System.Windows.Forms.RadioButton optV22BIS;
        internal System.Windows.Forms.RadioButton optV22CCITT;
        internal System.Windows.Forms.RadioButton optV21CCITT;
        internal System.Windows.Forms.RadioButton optBest;
        internal System.Windows.Forms.GroupBox GroupBox6;
        internal System.Windows.Forms.RadioButton optPulse;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal System.Windows.Forms.RadioButton optCCITT;
        internal System.Windows.Forms.GroupBox GroupBox9;
        internal System.Windows.Forms.TextBox txtTPDUorigin;
        internal System.Windows.Forms.TextBox txtTPDUId;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label12;
    }
}