namespace TMS_InSys
{
    partial class FrmTesting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.txtByteCount = new System.Windows.Forms.TextBox();
            this.Button6 = new System.Windows.Forms.Button();
            this.Button5 = new System.Windows.Forms.Button();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(100, 100);
            this.TextBox1.Multiline = true;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(304, 88);
            this.TextBox1.TabIndex = 15;
            this.TextBox1.Text = "TextBox1";
            // 
            // txtByteCount
            // 
            this.txtByteCount.Location = new System.Drawing.Point(100, 12);
            this.txtByteCount.Multiline = true;
            this.txtByteCount.Name = "txtByteCount";
            this.txtByteCount.Size = new System.Drawing.Size(304, 88);
            this.txtByteCount.TabIndex = 14;
            this.txtByteCount.Text = "txtByteCount";
            // 
            // Button6
            // 
            this.Button6.Location = new System.Drawing.Point(12, 162);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(72, 24);
            this.Button6.TabIndex = 13;
            this.Button6.Text = "Button6";
            // 
            // Button5
            // 
            this.Button5.Location = new System.Drawing.Point(12, 132);
            this.Button5.Name = "Button5";
            this.Button5.Size = new System.Drawing.Size(72, 24);
            this.Button5.TabIndex = 12;
            this.Button5.Text = "decrypt des";
            // 
            // Button4
            // 
            this.Button4.Location = new System.Drawing.Point(12, 102);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(72, 24);
            this.Button4.TabIndex = 11;
            this.Button4.Text = "encrypt des";
            // 
            // Button3
            // 
            this.Button3.Location = new System.Drawing.Point(12, 72);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(72, 24);
            this.Button3.TabIndex = 10;
            this.Button3.Text = "Button3";
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(12, 42);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(72, 24);
            this.Button2.TabIndex = 9;
            this.Button2.Text = "Button2";
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(12, 12);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(72, 24);
            this.Button1.TabIndex = 8;
            this.Button1.Text = "Button1";
            // 
            // FrmTesting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 194);
            this.Controls.Add(this.TextBox1);
            this.Controls.Add(this.txtByteCount);
            this.Controls.Add(this.Button6);
            this.Controls.Add(this.Button5);
            this.Controls.Add(this.Button4);
            this.Controls.Add(this.Button3);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Name = "FrmTesting";
            this.Text = "Testing";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.TextBox txtByteCount;
        internal System.Windows.Forms.Button Button6;
        internal System.Windows.Forms.Button Button5;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
    }
}