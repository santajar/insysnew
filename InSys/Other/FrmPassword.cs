using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmPassword : Form
    {
        public FrmPassword()
        {
            InitializeComponent();
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text == "")
                MessageBox.Show("Please Fill Your Username");
            else if (txtPassword.Text == "")
                MessageBox.Show("Please Fill Your Password");
            else
                MessageBox.Show("Success");
        }
    }
}