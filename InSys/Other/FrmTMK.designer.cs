namespace TMS_InSys
{
    partial class FrmTMK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTMK));
            this.btnChkSum = new System.Windows.Forms.Button();
            this.tbChkSUM = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.tbKey2 = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.tbGroup = new System.Windows.Forms.TextBox();
            this.tbKey1 = new System.Windows.Forms.TextBox();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnChkSum
            // 
            this.btnChkSum.Location = new System.Drawing.Point(252, 106);
            this.btnChkSum.Name = "btnChkSum";
            this.btnChkSum.Size = new System.Drawing.Size(72, 24);
            this.btnChkSum.TabIndex = 22;
            this.btnChkSum.Text = "Check SUM";
            // 
            // tbChkSUM
            // 
            this.tbChkSUM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbChkSUM.Location = new System.Drawing.Point(124, 106);
            this.tbChkSUM.MaxLength = 16;
            this.tbChkSUM.Name = "tbChkSUM";
            this.tbChkSUM.ReadOnly = true;
            this.tbChkSUM.Size = new System.Drawing.Size(104, 20);
            this.tbChkSUM.TabIndex = 29;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(12, 106);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 23);
            this.Label3.TabIndex = 28;
            this.Label3.Text = "Check SUM";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbKey2
            // 
            this.tbKey2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbKey2.Location = new System.Drawing.Point(124, 74);
            this.tbKey2.MaxLength = 16;
            this.tbKey2.Name = "tbKey2";
            this.tbKey2.Size = new System.Drawing.Size(104, 20);
            this.tbKey2.TabIndex = 21;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(12, 74);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(100, 23);
            this.Label2.TabIndex = 27;
            this.Label2.Text = "TMK Part-2";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbGroup
            // 
            this.tbGroup.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbGroup.Location = new System.Drawing.Point(124, 10);
            this.tbGroup.MaxLength = 2;
            this.tbGroup.Name = "tbGroup";
            this.tbGroup.Size = new System.Drawing.Size(104, 20);
            this.tbGroup.TabIndex = 19;
            // 
            // tbKey1
            // 
            this.tbKey1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbKey1.Location = new System.Drawing.Point(124, 42);
            this.tbKey1.MaxLength = 16;
            this.tbKey1.Name = "tbKey1";
            this.tbKey1.Size = new System.Drawing.Size(104, 20);
            this.tbKey1.TabIndex = 20;
            // 
            // BtnCancel
            // 
            this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnCancel.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancel.Image")));
            this.BtnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnCancel.Location = new System.Drawing.Point(132, 138);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(88, 23);
            this.BtnCancel.TabIndex = 24;
            this.BtnCancel.Text = "Cancel";
            // 
            // BtnSave
            // 
            this.BtnSave.Image = ((System.Drawing.Image)(resources.GetObject("BtnSave.Image")));
            this.BtnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnSave.Location = new System.Drawing.Point(12, 138);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(88, 23);
            this.BtnSave.TabIndex = 23;
            this.BtnSave.Text = "Save";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(12, 42);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(100, 23);
            this.Label4.TabIndex = 26;
            this.Label4.Text = "TMK Part-1";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(12, 9);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 23);
            this.Label1.TabIndex = 25;
            this.Label1.Text = "Group";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmTMK
            // 
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Name = "FrmTMK";
            this.Load += new System.EventHandler(this.FrmTMK_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnChkSum;
        internal System.Windows.Forms.TextBox tbChkSUM;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox tbKey2;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox tbGroup;
        internal System.Windows.Forms.TextBox tbKey1;
        internal System.Windows.Forms.Button BtnCancel;
        internal System.Windows.Forms.Button BtnSave;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label1;
    }
}