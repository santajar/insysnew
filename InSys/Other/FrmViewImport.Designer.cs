namespace InSys
{
    partial class FrmViewImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmViewImport));
            this.BtnImport = new System.Windows.Forms.Button();
            this.BtnFilter = new System.Windows.Forms.Button();
            this.TimerRefresh = new System.Windows.Forms.Timer(this.components);
            this.LblDetail = new System.Windows.Forms.Label();
            this.TxtDdetailImport = new System.Windows.Forms.TextBox();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.DataGridImport = new System.Windows.Forms.DataGrid();
            this.OpenFileDialogImport2 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridImport)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnImport
            // 
            this.BtnImport.Image = ((System.Drawing.Image)(resources.GetObject("BtnImport.Image")));
            this.BtnImport.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnImport.Location = new System.Drawing.Point(274, 2);
            this.BtnImport.Name = "BtnImport";
            this.BtnImport.Size = new System.Drawing.Size(288, 24);
            this.BtnImport.TabIndex = 14;
            this.BtnImport.Text = "Import";
            // 
            // BtnFilter
            // 
            this.BtnFilter.Image = ((System.Drawing.Image)(resources.GetObject("BtnFilter.Image")));
            this.BtnFilter.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.BtnFilter.Location = new System.Drawing.Point(2, 2);
            this.BtnFilter.Name = "BtnFilter";
            this.BtnFilter.Size = new System.Drawing.Size(264, 24);
            this.BtnFilter.TabIndex = 13;
            this.BtnFilter.Text = "Filter";
            // 
            // TimerRefresh
            // 
            this.TimerRefresh.Interval = 500;
            // 
            // LblDetail
            // 
            this.LblDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDetail.Location = new System.Drawing.Point(2, 229);
            this.LblDetail.Name = "LblDetail";
            this.LblDetail.Size = new System.Drawing.Size(100, 16);
            this.LblDetail.TabIndex = 12;
            this.LblDetail.Text = "DETAIL";
            // 
            // TxtDdetailImport
            // 
            this.TxtDdetailImport.Location = new System.Drawing.Point(2, 253);
            this.TxtDdetailImport.Multiline = true;
            this.TxtDdetailImport.Name = "TxtDdetailImport";
            this.TxtDdetailImport.Size = new System.Drawing.Size(560, 88);
            this.TxtDdetailImport.TabIndex = 11;
            // 
            // Timer1
            // 
            this.Timer1.Enabled = true;
            this.Timer1.Interval = 5000;
            // 
            // DataGridImport
            // 
            this.DataGridImport.DataMember = "";
            this.DataGridImport.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.DataGridImport.Location = new System.Drawing.Point(2, 29);
            this.DataGridImport.Name = "DataGridImport";
            this.DataGridImport.Size = new System.Drawing.Size(560, 192);
            this.DataGridImport.TabIndex = 10;
            // 
            // FrmViewImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 343);
            this.Controls.Add(this.BtnImport);
            this.Controls.Add(this.BtnFilter);
            this.Controls.Add(this.LblDetail);
            this.Controls.Add(this.TxtDdetailImport);
            this.Controls.Add(this.DataGridImport);
            this.Name = "FrmViewImport";
            this.Text = "FrmViewImport";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridImport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button BtnImport;
        internal System.Windows.Forms.Button BtnFilter;
        internal System.Windows.Forms.Timer TimerRefresh;
        internal System.Windows.Forms.Label LblDetail;
        internal System.Windows.Forms.TextBox TxtDdetailImport;
        internal System.Windows.Forms.Timer Timer1;
        internal System.Windows.Forms.DataGrid DataGridImport;
        internal System.Windows.Forms.OpenFileDialog OpenFileDialogImport2;
    }
}