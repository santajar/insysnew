using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO.Ports;
using System.Threading;
using InSysClass;

namespace InSysConsoleGZip
{
    public class SerialModem : IDisposable
    {
        protected string sModemPort;

        protected const byte TPDU = 0x60;
        protected const string MTI = "0800";
        protected const string ProcCodeStart = "930000";
        protected const string ProcCodeContinue = "930001";

        protected SerialPort oSerialPort;

        protected bool IsStartReceive;

        protected string sISOMessage;
        protected string sISOMessageOld;

        protected byte[] arrbSendOld;

        /// <summary>
        /// SerialModem class constructor
        /// </summary>
        /// <param name="_sModemPort">string : Modem port name</param>
        public SerialModem(string _sModemPort)
        {
            sModemPort = _sModemPort;

            InitModem();
            if (oSerialPort.IsOpen)
            {
                InitConfigModem();
                oSerialPort.DataReceived += new SerialDataReceivedEventHandler(this.oSerialPort_DataReceived);
            }
        }

        /// <summary>
        /// Modem Serialport Deconstructor, close the port and dispose it.
        /// </summary>
        public void Dispose()
        {
            if (oSerialPort.IsOpen) oSerialPort.Close();
            oSerialPort.Dispose();
        }

        /// <summary>
        /// Init the serialport modem configuration
        /// </summary>
        protected void InitModem()
        {
            oSerialPort = new SerialPort(sModemPort);

            oSerialPort.PortName = sModemPort;
            oSerialPort.BaudRate = 19200;
            oSerialPort.StopBits = StopBits.One;
            oSerialPort.Parity = Parity.None;
            oSerialPort.DataBits = 8;
            oSerialPort.Handshake = Handshake.None;
            oSerialPort.ReadBufferSize = 1024;
            //oSerialPort.ReceivedBytesThreshold = 1024;

            oSerialPort.Open();

            Thread.Sleep(100);
            if (oSerialPort.IsOpen)
            {
                oSerialPort.DiscardInBuffer();
                oSerialPort.DiscardOutBuffer();

                oSerialPort.DtrEnable = true;
                oSerialPort.RtsEnable = true;
            }
        }

        /// <summary>
        /// Init the serialport modem AT command configuration.
        /// </summary>
        protected void InitConfigModem()
        {
            oSerialPort.Write("ATE0B0&C0&D0&G0S0=1S10=50\r");
            oSerialPort.Write("ATW1\r");

            // US Robotics
            oSerialPort.Write("AT&M0\r");

            oSerialPort.Write("AT&W0");
        }

        /// <summary>
        /// Serialport modem data received event.
        /// </summary>
        protected void oSerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(500);
            if (oSerialPort.BytesToRead > 0)
            {
                int iBytesToRead = oSerialPort.BytesToRead;
                byte[] arrbRead = new byte[iBytesToRead];
                oSerialPort.Read(arrbRead, 0, iBytesToRead);

                //Trace.Write("Receive.txt", "Receive : \n" + CommonLib.sByteArrayToString(arrbRead));

                if (IsStartReceive == false)
                {
                    if (IsTPDUDetected(ref arrbRead))
                    {
                        IsStartReceive = true;
                        sISOMessage = null;
                        sISOMessage = CommonLib.sByteArrayToString(arrbRead);

                        if (IsStartToProcess(arrbRead))
                        {
                            IsStartReceive = false;
                            //Trace.Write("Modem.txt", "Receive1 : \n" + sISOMessage);

                            if (IsDataValid(sISOMessage))
                            {
                                GetAndSendResponse(sISOMessage);
                            }
                            sISOMessage = null;
                        }
                    }
                }
                else
                {
                    sISOMessage += CommonLib.sByteArrayToString(arrbRead);
                    if (IsStartToProcess(arrbRead))
                    {
                        IsStartReceive = false;
                        //Trace.Write("Modem.txt", "Receive2 : \n" + sISOMessage);

                        if(IsDataValid(sISOMessage))
                        {
                            GetAndSendResponse(sISOMessage);
                        }
                        sISOMessage = null;
                    }
                }
            }
        }

        /// <summary>
        /// To check the received message contains valid MTI & initialize ProcCode
        /// </summary>
        /// <param name="arrbTemp">byte[] : array of byte of received message</param>
        /// <returns>bool : TRUE, if MTI & initialize ProcCode detected</returns>
        protected bool IsTPDUDetected(ref byte[] arrbTemp)
        {
            string sTemp = CommonLib.sByteArrayToString(arrbTemp);
            if (sTemp.Contains(MTI) && (sTemp.Contains(ProcCodeStart) || sTemp.Contains(ProcCodeContinue)))
            {
                int iOffsetStart = sTemp.IndexOf(MTI) - 10;
                int iOffsetEnd = sTemp.IndexOf("4243415831303030") + 16;

                if (iOffsetEnd == 0)
                    iOffsetEnd = sTemp.Length;

                int iLength = iOffsetEnd - iOffsetStart;
                byte[] arrbReturn = new byte[iLength];
                arrbReturn = CommonLib.HexStringToByteArray(sTemp.Substring(iOffsetStart, iLength));

                Array.Copy(arrbReturn, arrbTemp, arrbReturn.Length);
                return true;
            }
            else
                return false;

            //if (arrbTemp[0] == TPDU)
            //    return true;
            //else
            //    return false;
        }

        /// <summary>
        /// To check, is all the message allready received or not
        /// </summary>
        /// <param name="arrbTemp">byte[] : array of byte of received message</param>
        /// <returns>bool : TRUE, if start to process the received message.</returns>
        protected bool IsStartToProcess(byte[] arrbTemp)
        {
            string sTemp = CommonLib.sHexToString(CommonLib.sByteArrayToString(arrbTemp));
            if (sTemp.Contains("BCAX1000"))
                return true;
            else
                return false;
        }

        /// <summary>
        /// To check the received message is contains MTI & initialize ProcCode
        /// </summary>
        /// <param name="_sISOTemp">string : hexstring received ISO message</param>
        /// <returns>bool : TRUE, if the received ISO message contains of valid MTI and intialize ProcCode</returns>
        protected bool IsDataValid(string _sISOTemp)
        {
            string sISOTemp = _sISOTemp;
            if (sISOTemp.Substring(10, 4) == MTI)
            {
                if (sISOTemp.Substring(30, 6) == ProcCodeStart 
                    || sISOTemp.Substring(30, 6) == ProcCodeContinue)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Function to get the ISO response message and send to the EDC
        /// </summary>
        /// <param name="_sISOTemp"></param>
        protected void GetAndSendResponse(string _sISOTemp)
        {
            try
            {
                byte[] arrbSend = new byte[1024];
                //if(!string.IsNullOrEmpty(sISOMessageOld))
                if (_sISOTemp != sISOMessageOld)
                {
                    byte[] arrbReceive = CommonLib.HexStringToByteArray(_sISOTemp);
                    byte[] arrbResponse = ResponseGZip.arrbResponse(arrbReceive, ConnType.Modem);
                    arrbSend = new byte[arrbResponse.Length + 2];
                    Array.Copy(arrbResponse, arrbSend, arrbResponse.Length);
                    Array.Copy(CommonLib.arrbStringtoByteArray("\r\n"), 0, arrbSend, arrbResponse.Length, 2);

                    arrbSendOld = new byte[arrbSend.Length];
                    arrbSend.CopyTo(arrbSendOld, 0);
                    sISOMessageOld = _sISOTemp;
                }
                else
                {
                    arrbSend = new byte[arrbSendOld.Length];
                    Array.Copy(arrbSendOld, arrbSend, arrbSendOld.Length);
                }

                oSerialPort.Write(arrbSend, 0, arrbSend.Length);
                Trace.Write("Modem.txt", "Send : \n" + CommonLib.sByteArrayToHexString(arrbSend));
            }
            catch (Exception ex)
            {
                Console.Write("error : " + ex.Message);
            }
        }
    }
}
