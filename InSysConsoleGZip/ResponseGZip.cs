using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Security.AccessControl;
using System.IO.Compression;
using InSysClass;

namespace InSysConsoleGZip
{
    class ResponseGZip
    {
        public static SqlConnection oConn;
        public static bool bCheckAllowInit;
        protected static int iMaxPacketLength = 300;
        static List<Terminal> listTerminal = new List<Terminal>();
        const string ProcCodeFirstLast = "930000";
        const string ProcCodeNext = "930001";

        class Terminal
        {
            protected string sTerminalId;
            protected int iCounter;
            protected long lCompressedLength;

            public Terminal()
            {
                this.sTerminalId = null;
                this.iCounter = 0;
            }

            public string TerminalId
            {
                get { return sTerminalId; }
                set { sTerminalId = value; }
            }

            public int Counter
            {
                get { return iCounter; }
                set { iCounter = value; }
            }

            public long CompressedLength
            {
                get { return lCompressedLength; }
                set { lCompressedLength = value; }
            }
        }

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public static byte[] arrbResponse(byte[] arrbReceive, ConnType cType)
        {
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);
                arrbTemp = new byte[iIsoLen + 2];
                arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);

                //arrbTemp = CommonLib.HexStringToByteArray("");    //testing kirim null.
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
                Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected static string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTerminalId = null;
            string sTag = null;
            int iErrorCode = -1;
            string sTemp = "";

            // convert _arrbRecv ke hexstring,
            //string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            //if (cType != ConnType.Modem)
            //{
            //    int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);
            //    // buang total length & tpdu
            //    sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            //}
            //else
            //    sTemp = sTemp.Substring(2);

            byte[] arrbTemp = new byte[_arrbRecv.Length - 2];
            Array.Copy(_arrbRecv, 2, arrbTemp, 0, _arrbRecv.Length - 2);
            
            ISO8583Lib iso = new ISO8583Lib();
            ISO8583_MSGLib isomsg = new ISO8583_MSGLib();
            iso.UnPackISO(arrbTemp, ref isomsg);
            
            Console.WriteLine(isomsg.sTerminalId);
            string sResponse = null;
            //Trace.Write(" Recv : " + sTemp);

            // Start processing right here
            if (IsInitMTI(isomsg.MTI))
            {
                string sBit60 = "";
                if (IsFirstLastInit(isomsg.sBit[3]))
                {
                    SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileText, oConn);
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 0;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = isomsg.sTerminalId;
                    oCmd.Parameters.Add("@sContent", SqlDbType.VarChar,100000);
                    oCmd.Parameters["@sContent"].Direction = ParameterDirection.Output;
                    string sContent;
                    // kirim ke sp,...
                    try
                    {
                        oCmd.ExecuteNonQuery();
                        sContent = oCmd.Parameters["@sContent"].Value.ToString();
                        SavePacket(isomsg.sTerminalId, sContent);
                        CompressPacket(isomsg.sTerminalId);
                        TerminalStartInit(isomsg.sTerminalId);

                        //sBit60 = CommonLib.sByteArrayToHexString(arrbReadPacket(isomsg.sTerminalId, 0)).Replace(" ","");
                        sBit60 = string.Format("{0:%s}", arrbReadPacket(isomsg.sTerminalId, 0));
                    }
                    catch (Exception ex)
                    {                        
                        Console.WriteLine(ex.Message);                        
                    }

                    oCmd.Dispose();
                }
                else if (IsNextInit(isomsg.sBit[3])) 
                {
                    int iCounter = TerminalContinueInit(isomsg.sTerminalId);
                    //sBit60 = CommonLib.sByteArrayToHexString(arrbReadPacket(isomsg.sTerminalId, iCounter)).Replace(" ", "");
                    sBit60 = string.Format("{0:%s}", arrbReadPacket(isomsg.sTerminalId, 0));
                }

                ISO8583_MSGLib isomsgresp = isomsg;
                isomsgresp.MTI = "0810";

                if (isomsg.sBit[3] == ProcCodeFirstLast)
                    iso.SetField_Str(3, ref isomsgresp, ProcCodeNext);
                else if (isomsg.sBit[3] == ProcCodeNext && IsSendingLastPacket(isomsg.sTerminalId) == false)
                    iso.SetField_Str(3, ref isomsgresp, ProcCodeNext);
                else if (isomsg.sBit[3] == ProcCodeNext && IsSendingLastPacket(isomsg.sTerminalId) == true)
                    iso.SetField_Str(3, ref isomsgresp, ProcCodeFirstLast);

                iso.SetField_Str(12, ref isomsgresp, DateTime.Now.ToString("hhmmss"));
                iso.SetField_Str(13, ref isomsgresp, DateTime.Now.ToString("MMddyy"));
                iso.SetField_Str(39, ref isomsgresp, "00");
                iso.SetField_Str(60, ref isomsgresp, sBit60);
                sResponse = CommonLib.sByteArrayToHexString(iso.PackToISO(isomsgresp));
                sTemp = sResponse.Replace(" ", "");

                // tambahkan total length
                _iIsoLenResponse = sTemp.Length / 2;
                sTemp = sISOLenSend(_iIsoLenResponse, cType) + sTemp;

                //Console.WriteLine(sResponse);

                //CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));

                //Trace.Write(sTerminalId + ".txt",
                //    string.Format("{0} {1}", CommonConsole.sFullTime(), sProcessing(iErrorCode, sTag)));
            }
            return sTemp;
        }

        protected static int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            if (cType == ConnType.TcpIP)
                iReturn = CommonLib.iHexStringToInt(sTempLen);
            else if (cType == ConnType.Serial)
                iReturn = int.Parse(sTempLen);
            return iReturn;
        }

        protected static string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected static string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Initialize Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Initialize Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                default:
                    sTemp = "Unknown Error";
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected static string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
            }
            return sTemp;
        }

        protected static bool IsInitMTI(string sMTI)
        {
            return (sMTI == "0800") ? true : false;
        }

        protected static bool IsFirstLastInit(string sProcCode)
        {
            return (sProcCode == ProcCodeFirstLast) ? true : false;
        }

        protected static bool IsNextInit(string sProcCode)
        {
            return (sProcCode == ProcCodeNext) ? true : false;
        }

        protected static void SavePacket(string sFilename, string sContent)
        {
            CommonLib.Write2File(Directory.GetCurrentDirectory() + "\\" + sFilename, sContent, false);
        }

        protected static void CompressPacket(string sFilename)
        {
            try
            {
                string sDirectory = Directory.GetCurrentDirectory() + "\\INIT";

                //File.Delete(Directory.GetCurrentDirectory() + "\\" + sFilename + ".gz");
                //File.Delete(sDirectory + "\\" + sFilename + ".gz");

                ProcessStartInfo procinfoBatchFile = new ProcessStartInfo(sDirectory + "\\gzip.exe");
                Process procBatchFile = new Process();
                procBatchFile.StartInfo = procinfoBatchFile;
                procBatchFile.StartInfo.Arguments = "-f " + sFilename;
                procBatchFile.Start();
                procBatchFile.WaitForExit();

                if (File.Exists(Directory.GetCurrentDirectory() + "\\" + sFilename + ".gz"))
                {
                    CommonConsole.AddFileSecurity(Directory.GetCurrentDirectory() + "\\" + sFilename + ".gz", "Everyone",
                        FileSystemRights.FullControl, AccessControlType.Allow);

                    File.Copy(Directory.GetCurrentDirectory() + "\\" + sFilename + ".gz",
                        sDirectory + "\\" + sFilename + ".gz", true);

                    CommonConsole.AddFileSecurity(sDirectory + "\\" + sFilename + ".gz", "Everyone",
                        FileSystemRights.FullControl, AccessControlType.Allow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CompressPacket : " + ex.Message);
            }
        }

        protected static byte[] arrbReadPacket(string sFilename, int iCounter)
        {
            string sFilenameGzip = Directory.GetCurrentDirectory() + "\\INIT\\" + "12345678";
            //string sFilenameGzip = Directory.GetCurrentDirectory() + "\\INIT\\" + sFilename + ".gz";
            FileStream fs = new FileStream(sFilenameGzip, FileMode.Open);

            byte[] arrbPacketTemp = new byte[fs.Length];
            byte[] arrbPacket = new byte[iMaxPacketLength];
            try
            {
                fs.Read(arrbPacketTemp, 0, (int)fs.Length);

                arrbPacket = new byte[iCountPacketLength((int)fs.Length, iCounter)];
                
                Array.Copy(arrbPacketTemp, 0 + (iCounter * iMaxPacketLength), 
                    arrbPacket, 0, 
                    iCountPacketLength((int)fs.Length, iCounter));
                CommonLib.Write2File(Directory.GetCurrentDirectory() + "\\INIT\\" + "12345678-2", arrbPacket);
            }
            catch (Exception ex)
            {
                Console.WriteLine("arrbReadPacket : " + ex.Message + "\niCounter : " + iCounter.ToString());
            }
            fs.Close();
            return arrbPacket;
        }

        protected static int iCountPacketLength(int iLength, int iCounter)
        {
            return (iLength > (0 + (iCounter * iMaxPacketLength) + iMaxPacketLength)) ?
            iMaxPacketLength : iLength - (0 + (iCounter * iMaxPacketLength));
        }

        protected static void TerminalStartInit(string sTerminalId)
        {
            string sFilenameGzip = Directory.GetCurrentDirectory() + "\\INIT\\" + sTerminalId;
            FileStream fs = new FileStream(sFilenameGzip, FileMode.Open);

            Terminal tempTerminal = new Terminal();
            tempTerminal.TerminalId = sTerminalId;
            tempTerminal.Counter = 0;
            tempTerminal.CompressedLength = fs.Length;
            
            //List<string> s = new List<string>();
            //s.Exists(delegate(string sCOba)
            //{
            //    if (sCOba == "test") return true;
            //    else return false;
            //});

            int iIndex=listTerminal.FindIndex(delegate(Terminal term)
            {
                return term.TerminalId == sTerminalId;
            });
            if (iIndex != -1)
                listTerminal.RemoveAt(iIndex);

            listTerminal.Add(tempTerminal);
            fs.Close();
        }

        protected static int TerminalContinueInit(string sTerminalId)
        {
            int iIndex = listTerminal.FindIndex(delegate(Terminal term)
            {
                if (term.TerminalId == sTerminalId) return true;
                else return false;
            });
            listTerminal[iIndex].Counter += 1;
            return listTerminal[iIndex].Counter;
        }

        protected static bool IsSendingLastPacket(string sTerminalId)
        {
            bool bReturn = false;
            int iIndex = listTerminal.FindIndex(delegate(Terminal term)
            {
                if (term.TerminalId == sTerminalId) return true;
                else return false;
            });
            Terminal oTempTerminal = listTerminal[iIndex];
            return (oTempTerminal.CompressedLength - (oTempTerminal.Counter * iMaxPacketLength) < iMaxPacketLength) 
                ? true : false;
        }
    }
}
