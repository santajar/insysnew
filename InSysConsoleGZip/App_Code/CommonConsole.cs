using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Security.AccessControl;
using System.IO;

namespace InSysConsoleGZip
{
    public enum ConnType
    {
        TcpIP = 1,
        Serial = 2,
        Modem = 3
    }

    class CommonConsole
    {
        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sTimeHHMMSS()
        {
            return DateTime.Now.ToString("hhmmss", ciUSFormat);
        }

        public static string sDateMMddyy()
        {
            return DateTime.Now.ToString("MMddyy", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = CommonConsole.sFullTime();
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }
        
        // Adds an ACL entry on the specified file for the specified account.
        public static void AddFileSecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
        {
            // Create a new FileInfo object.
            FileInfo fInfo = new FileInfo(FileName);

            // Get a FileSecurity object that represents the 
            // current security settings.
            FileSecurity fSecurity = fInfo.GetAccessControl();

            // Add the FileSystemAccessRule to the security settings. 
            fSecurity.AddAccessRule(new FileSystemAccessRule(Account,
                                                            Rights,
                                                            ControlType));
            // Set the new access settings.
            fInfo.SetAccessControl(fSecurity);
        }

        // Removes an ACL entry on the specified file for the specified account.
        public static void RemoveFileSecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
        {
            // Create a new FileInfo object.
            FileInfo fInfo = new FileInfo(FileName);

            // Get a FileSecurity object that represents the 
            // current security settings.
            FileSecurity fSecurity = fInfo.GetAccessControl();

            // Add the FileSystemAccessRule to the security settings. 
            fSecurity.RemoveAccessRule(new FileSystemAccessRule(Account,
                                                            Rights,
                                                            ControlType));
            // Set the new access settings.
            fInfo.SetAccessControl(fSecurity);
        }
    }
    
    class CommonSP
    {
        public static string sSPFlagControlAllowInit = "spControlFlagAllowInitBrowse";
        public static string sSPProcessMessage = "spOtherParseProcess";
        public static string sSPPortInit = "spOtherParsePortInit";
        public static string sSPProfileText = "spProfileText";

        public static string sSPProfileGZip = "spProfileGZip";
        public static string sSPProfileGZipUpdate = "spProfileGZipUpdate";
    }
}