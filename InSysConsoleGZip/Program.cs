using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Security.AccessControl;
using InSysClass;

namespace InSysConsoleGZip
{
    class Program
    {
        static bool bCheckAllowInit;
        static SqlConnection oConn = new SqlConnection();
        static string sConnString;
        static string sAppDirectory;

        /* Command line Parameter :
         * -T Terminal Id Name
         * -S Terminal Id Content
         * -H or /? or ? Display Help
         */
        static class CmdParam
        {
            static string sHelp1 = "-H";
            static string sHelp2 = "/?";
            static string sHelp3 = "?";
            static string sTerminal = "-T";
            static string sContent = "-S";
            static string sPath = "-P";

            public static string Help1 { get { return sHelp1; } }
            public static string Help2 { get { return sHelp2; } }
            public static string Help3 { get { return sHelp3; } }
            public static string Terminal { get { return sTerminal; } }
            public static string Content { get { return sContent; } }
        }

        /* =================================================
         * Flow aplikasi :
         * =================================================
         * 1. edc request initialize, accept by InSysConsole
         * 2. start processing request to DB
         * 3. database check, compress file Yes or No
         * 4. if compress, DB run InSysConsoleGZip
         * 5. read response from table and sent
         * =================================================
         */

        static void Main(string[] args)
        {
            string sTerminalId;
            string sContent;

            try
            {
                //foreach (string s in args)
                //    Console.WriteLine(s);
                
                if (IsParamValid(args[0]))
                {
                    if (args[0] == CmdParam.Help1 || args[0] == CmdParam.Help2 || args[0] == CmdParam.Help3)
                        ShowHelp();
                    else if (args[0] == CmdParam.Terminal)
                    {
                        if (IsParamValueValid(args[1]))
                        {   
                            sTerminalId = args[1];
                            sAppDirectory = args[3];
                            
                            oConn = InitConnection();

                            string sTerminalFile = string.Format(@"{0}\{1}", sAppDirectory, sTerminalId);
                            Console.WriteLine(sTerminalFile);

                            sContent = sGetContentTerminal(sTerminalId);

                            //Console.WriteLine("sTerminalFile : " + sTerminalFile);
                            //Console.WriteLine("sContent : " + sContent);

                            if (File.Exists(sTerminalFile)) File.Delete(sTerminalFile);
                            CommonLib.Write2File(sTerminalFile, sContent, false);

                            if (File.Exists(sTerminalFile))
                            {
                                if (IsCompressSuccess(sTerminalFile, sTerminalId))
                                {

                                }
                            }
                            else
                                Console.WriteLine("Terminal file does not exist.");
                        }
                        else
                            Console.WriteLine("Invalid TerminalId Name. Only Alphanumeric allowed.");
                    }
                }
                else
                    Console.WriteLine("Unknown Parameter!");
            }
            catch (Exception ex)
            {
                CommonConsole.WriteToConsole(ex.Message);
                Console.Read();
            }
            Console.Read();
        }

        #region "Function"
        /// <summary>
        /// Initialize all the needed variable
        /// </summary>
        //static void InitSocketClass()
        //{
        //    AsyncSocket.iPort = Port;
        //}

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception ex)
            {
                oSqlTempConn = null;
                Trace.Write("InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }

        /// <summary>
        /// Get the Port number configuration
        /// </summary>
        //static int Port
        //{
        //    get
        //    {
        //        string sPort = null;
        //        if (oConn.State == ConnectionState.Closed) oConn.Open();
        //        SqlCommand oCmd = new SqlCommand(CommonSP.sSPPortInit, oConn);
        //        oCmd.CommandType = CommandType.StoredProcedure;

        //        SqlDataAdapter oda = new SqlDataAdapter(oCmd);
        //        DataTable dt = new DataTable();
        //        oda.Fill(dt);
        //        if (dt.Rows.Count > 0)
        //            sPort = dt.Rows[0][0].ToString();
        //        oCmd.Dispose();
        //        oda.Dispose();
        //        dt.Dispose();

        //        return int.Parse(sPort);
        //    }
        //}

        /// <summary>
        /// Set the Allow Initialization configuration
        /// </summary>
        //static void InitControlAllowInit()
        //{
        //    bCheckAllowInit = bGetControlAllowInit();
        //}

        /// <summary>
        /// Get the Allow Init configuration from the database
        /// </summary>
        /// <returns>bool : boolean allow to initialize or not</returns>
        //static bool bGetControlAllowInit()
        //{
        //    bool bTemp = false;
        //    try
        //    {
        //        if (oConn.State == ConnectionState.Closed)
        //            oConn.Open();

        //        SqlCommand oCmd = new SqlCommand(CommonSP.sSPFlagControlAllowInit, oConn);
        //        oCmd.CommandType = CommandType.StoredProcedure;

        //        SqlDataAdapter oda = new SqlDataAdapter(oCmd);
        //        DataTable dt = new DataTable();
        //        oda.Fill(dt);
        //        if (dt.Rows.Count > 0)
        //            bTemp = Convert.ToInt16(dt.Rows[0][0].ToString()) == 1 ? true : false;

        //        oCmd.Dispose();
        //        oda.Dispose();
        //        dt.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        Trace.Write("InitControlAllowInit : " + ex.Message);
        //    }
        //    return bTemp;
        //}

        static void ShowHelp()
        {
            Console.WriteLine("Usage : ");
            Console.WriteLine("[-T <terminal id name>]");
            Console.WriteLine("[-P <Init Folder Path>]");
        }

        static bool IsParamValid(string sParam)
        {
            return (sParam == "-T" || sParam == "-P" || sParam == "-H" || sParam == "/?" || sParam == "?") ?
                true : false;
        }

        static bool IsParamValueValid(string sParamValue)
        {
            return (sParamValue.Length == 8 && Regex.IsMatch(sParamValue, "^[0-9a-zA-z]+$")) ? true : false;
        }

        static string sGetContentTerminal(string _sTerminalId)
        {
            string sTemp = null;
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileGZip, oConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;

            if (oConn.State != ConnectionState.Open)
                oConn.Open();

            SqlDataReader oRead = oCmd.ExecuteReader();

            if (oRead.Read())
                sTemp = oRead["TermContent"].ToString();

            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();
            return sTemp;
        }

        static bool IsCompressSuccess(string sFilename, string sTerminalId)
        {
            Console.WriteLine("IsCompressSuccess");
            Console.WriteLine("sFilename : " + sFilename);

            bool bReturn = false;
            if (File.Exists(sFilename))
            {
                //Thread.Sleep(2000);

                try
                {
                    Console.WriteLine("procstartinfo");
                    ProcessStartInfo procstartinfo = new ProcessStartInfo(sAppDirectory + @"\gzip.exe");
                    Process processGZip = new Process();
                    processGZip.StartInfo = procstartinfo;

                    string sGzipFile = sFilename + ".gz";

                    if (File.Exists(sGzipFile))
                        File.Delete(sGzipFile);

                    //string sParam = string.Format(@"-n '{0}'", sFilename);
                    string sParam = string.Format(@"-n '{0}' -c > '{1}'", sFilename, sGzipFile);
                    Console.WriteLine(sParam);
                    processGZip.StartInfo.Arguments = sParam;

                    processGZip.Start();
                    processGZip.WaitForExit();
                    processGZip.Close();
                    processGZip.Dispose();

                    if (File.Exists(sGzipFile))
                    {
                        CommonConsole.AddFileSecurity(sGzipFile, "Everyone",
                            FileSystemRights.FullControl, AccessControlType.Allow);
                        bReturn = true;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("CompressPacket : " + ex.Message);
                }
            }
            else
                Console.WriteLine("CompressPacket : File not exist");
            return bReturn;
        }
        #endregion
    }
}
