﻿using InSysClass;
using ipXML;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using Vi.Log4Vi;
using System.IO.Compression;

namespace WcfService_InSys
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ContentProfile
    {
        string sTag;
        public string Tag { get { return sTag; } }
        string sContent;
        public string Content { get { return sContent; } }

        public ContentProfile(string _sTag, string _sContent)
        {
            sTag = _sTag;
            sContent = _sContent;
        }
    }

    public class ServiceInSys : IServiceInSys
    {
        //public string GetData(int value)
        //{
        //    return string.Format("You entered: {0}", value);
        //}

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}

        public static string rootDir = WebConfigurationManager.AppSettings["LOG"].ToString();
        public Logger websvcLog = new Logger(rootDir, "WEBSVC_INSYS", false);
        public string sConnString = WebConfigurationManager.ConnectionStrings["Database"].ConnectionString;
        public bool bDebug = bool.Parse(WebConfigurationManager.AppSettings["Debug"].ToString());
        string sFileResult = "";

        static byte[] arrbKey = new UTF8Encoding().GetBytes("316E67336E316330");

        protected static List<ContentProfile> ltGetContentProfile(string _sTerminalId, ref string _sContent)
        {
            string sConnString = WebConfigurationManager.ConnectionStrings["Database"].ConnectionString;
            SqlConnection oSqlConn = new SqlConnection(sConnString);
            List<ContentProfile> ltCPTemp = new List<ContentProfile>();

            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileTextFullTable, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                DataTable dtTemp = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtTemp);

                string[] arrsMaskedTag = arrsGetMaskedTag(_sTerminalId);

                if (dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow rowTemp in dtTemp.Rows)
                    {
                        string sTag = rowTemp["Tag"].ToString();
                        string sValue = rowTemp["TagValue"].ToString();
                        try
                        {
                            if (arrsMaskedTag.Length > 0 && arrsMaskedTag.Contains(sTag))
                                sValue = EncryptionLib.Decrypt3DES(CommonLib.sHexToStringUTF8(sValue), arrbKey);

                        }
                        catch (Exception)
                        {
                            sValue = rowTemp["TagValue"].ToString();
                        }

                        string sContent;
                        if (sTag != "AE37" && sTag != "AE38" && sTag != "TL11" && sTag.Substring(0, 2) != "PK")
                            sContent = string.Format("{0}{1:00}{2}", sTag, sValue.Length, sValue);
                        else
                        {
                            if (sTag == "AE37" || sTag == "AE38")
                                sContent = string.Format("{0}{1:000}{2}", sTag, sValue.Length, sValue);
                            else if (sTag == "PK04" && sTag == "PK06")
                                sContent = string.Format("{0}{1}{2}",
                                    CommonLib.sStringToHex(sTag),
                                    CommonLib.sStringToHex(string.Format("{0:000}", sValue.Length)),
                                    CommonLib.sStringToHex(sValue));
                            else
                                sContent = string.Format("{0}{1}{2}",
                                    sTag,
                                    string.Format("{0:000}", rowTemp["TagLength"]),
                                    sValue);
                        }
                        _sContent += sContent;
                        ContentProfile oCPTemp = new ContentProfile(sTag.Substring(0, 2), sContent);
                        int iIndexSearch = -1;
                        if (ltCPTemp.Count == 0)
                            ltCPTemp.Add(oCPTemp);
                        else
                            if ((iIndexSearch = ltCPTemp.FindIndex(oCPSearch => oCPSearch.Tag == oCPTemp.Tag)) > -1)
                        {
                            ContentProfile oCPSearchResult = ltCPTemp[iIndexSearch];
                            ContentProfile oCPNew = new ContentProfile(oCPTemp.Tag, oCPSearchResult.Content + oCPTemp.Content);
                            ltCPTemp[iIndexSearch] = oCPNew;
                        }
                        else
                            ltCPTemp.Add(oCPTemp);
                    }
                }
            }
            return ltCPTemp;
        }
        protected static string[] arrsGetMaskedTag(string _sTerminalId)
        {
            string sConnString = WebConfigurationManager.ConnectionStrings["Database"].ConnectionString;
            SqlConnection oSqlConn = new SqlConnection(sConnString);
            List<string> ltTag = new List<string>();
            string sQuery = string.Format("SELECT * FROM tbItemList WHERE DatabaseID IN (SELECT DatabaseID FROM tbProfileTerminalList WHERE TerminalID='{0}') AND vMasking=1", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                DataTable dtTemp = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtTemp);
                if (dtTemp != null && dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow row in dtTemp.Rows)
                        ltTag.Add(row["Tag"].ToString());
                }
            }
            return ltTag.ToArray();
        }

        public void ExportToProfile(string sTerminalID)
        {
            try
            {
                SqlConnection oSqlConn = new SqlConnection(sConnString);
                string sDestDir = WebConfigurationManager.AppSettings["Destination Folder"].ToString();
                string sDirProfile = sDestDir + "\\PROFILE\\";
                if (!Directory.Exists(sDirProfile)) Directory.CreateDirectory(sDirProfile);

                string sDirEDCInfo = sDestDir + "INFO\\";
                if (!Directory.Exists(sDirEDCInfo)) Directory.CreateDirectory(sDirEDCInfo);

                //string sDirStatusInit = sDestDir + "\\STATUS\\";
                //if (!Directory.Exists(sDirStatusInit)) Directory.CreateDirectory(sDirStatusInit);

                string sContent = null;
                List<ContentProfile> _ltCPTemp = ltGetContentProfile(sTerminalID, ref sContent);
                if (_ltCPTemp != null && _ltCPTemp.Count > 0)
                {
                    string sExtentionProfile = WebConfigurationManager.AppSettings["ExtentionProfile"].ToString();
                    if (sExtentionProfile == "gz")
                    {
                        string sFilename = sDirEDCInfo + sTerminalID + ".PRO";
                        if (File.Exists(sFilename)) File.Delete(sFilename);
                        CommonLib.Write2File(sFilename, sContent, false);

                        string sGzFilename = sDirProfile + sTerminalID;
                        if (IsNetCompressSuccess(sFilename, sGzFilename))
                        {
                            byte[] arrbContent;
                            using (FileStream fsRead = new FileStream(sGzFilename, FileMode.Open))
                            {
                                arrbContent = new byte[fsRead.Length];
                                fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
                            }
                            string sGzRemoteFilename = @"\PROFILE\" + Path.GetFileNameWithoutExtension(sGzFilename);
                            UploadFileFtp(sGzRemoteFilename, arrbContent, arrbContent.Length);

                            //WriteLog(sTerminalID);
                        }
                    }
                    else if (sExtentionProfile == "txt")
                    {
                        string sFilenameTxt = sDirProfile + sTerminalID;
                        if (File.Exists(sFilenameTxt)) File.Delete(sFilenameTxt);
                        CommonLib.Write2File(sFilenameTxt, sContent, false);

                        byte[] arrbContentTxt;
                        using (FileStream fsRead = new FileStream(sFilenameTxt, FileMode.Open))
                        {
                            arrbContentTxt = new byte[fsRead.Length];
                            fsRead.Read(arrbContentTxt, 0, Convert.ToInt32(fsRead.Length));
                        }
                        string sTxtRemoteFilename = @"\PROFILE\" + Path.GetFileNameWithoutExtension(sFilenameTxt);
                        UploadFileFtp(sTxtRemoteFilename, arrbContentTxt, arrbContentTxt.Length);
                    }

                    #region
                    //string sFileStatusTxt = sDirStatusInit + sTerminalID + ".TXT";
                    //if (File.Exists(sFileStatusTxt)) File.Delete(sFileStatusTxt);
                    //CommonLib.Write2File(sFileStatusTxt, "INIT ALLOWED", true);
                    //string sTxtRemoteStatusInit = @"\STATUS\" + Path.GetFileName(sFileStatusTxt);

                    //byte[] arrbContentStatusTxt;
                    //using (FileStream fsRead = new FileStream(sFileStatusTxt, FileMode.Open))
                    //{
                    //    arrbContentStatusTxt = new byte[fsRead.Length];
                    //    fsRead.Read(arrbContentStatusTxt, 0, Convert.ToInt32(fsRead.Length));
                    //}
                    //UploadFileFtp(sTxtRemoteStatusInit, arrbContentStatusTxt, arrbContentStatusTxt.Length);
                    #endregion


                }
            }
            catch (Exception ex) { websvcLog.Error(ex); }
            return;
        }

        public void UploadFileNotAllow(string sTerminalID)
        {
            try
            {
                SqlConnection oSqlConn = new SqlConnection(sConnString);
                string sDestDir = WebConfigurationManager.AppSettings["Destination Folder"].ToString();

                string sDirEDCInfo = sDestDir + "\\INFO\\";
                if (Directory.Exists(sDirEDCInfo))
                {
                    string sFilename = sDirEDCInfo + sTerminalID + ".PRO";
                    if (File.Exists(sFilename)) File.Delete(sFilename);
                }

                string sDirStatusInit = sDestDir + "\\PROFILE\\";
                if (!Directory.Exists(sDirStatusInit)) Directory.CreateDirectory(sDirStatusInit);
                string sFileStatusTxt = sDirStatusInit + sTerminalID;
                if (File.Exists(sFileStatusTxt)) File.Delete(sFileStatusTxt);
                CommonLib.Write2File(sFileStatusTxt, "NOTALLOWED", true);

                byte[] arrbContentStatusTxt;
                using (FileStream fsRead = new FileStream(sFileStatusTxt, FileMode.Open))
                {
                    arrbContentStatusTxt = new byte[fsRead.Length];
                    fsRead.Read(arrbContentStatusTxt, 0, Convert.ToInt32(fsRead.Length));
                }
                string sTxtStatusInit = @"\PROFILE\" + Path.GetFileName(sFileStatusTxt);
                UploadFileFtp(sTxtStatusInit, arrbContentStatusTxt, arrbContentStatusTxt.Length);

                #region
                //string sExtentionProfile = WebConfigurationManager.AppSettings["ExtentionProfile"].ToString();
                //if (sExtentionProfile == "txt")
                //{
                //    string sDirProfile = sDestDir + "\\PROFILE\\";
                //    string sFilenameTxt = sDirProfile + sTerminalID ;
                //    if (Directory.Exists(sDirProfile))
                //    {
                //        if (File.Exists(sFilenameTxt))
                //            File.Delete(sFilenameTxt);
                //    }
                //    string sTxtRemoteFilename = @"\PROFILE\" + Path.GetFileNameWithoutExtension(sFilenameTxt);
                //    DeleteFtp(sTxtRemoteFilename);
                //}
                //else
                //if (sExtentionProfile == "gz")
                //{
                //    string sDirProfileGz = sDestDir + "\\PROFILE\\";
                //    string sFileGz = sDirProfileGz + sTerminalID;
                //    if (Directory.Exists(sDirProfileGz))
                //    {
                //        if (File.Exists(sFileGz))File.Delete(sFileGz);                        
                //    }
                //    string sRemoteFileGz = @"\PROFILE\" + Path.GetFileNameWithoutExtension(sFileGz);
                //    DeleteFtp(sRemoteFileGz);
                //}

                //string sDirStatusInit = sDestDir + "\\STATUS\\";
                //if (!Directory.Exists(sDirStatusInit)) Directory.CreateDirectory(sDirStatusInit);
                //string sFileStatusTxt = sDirStatusInit + sTerminalID + ".TXT";
                //if (File.Exists(sFileStatusTxt)) File.Delete(sFileStatusTxt);
                //CommonLib.Write2File(sFileStatusTxt, "INIT NOT ALLOWED", true);

                //byte[] arrbContentStatusTxt;
                //using (FileStream fsRead = new FileStream(sFileStatusTxt, FileMode.Open))
                //{
                //    arrbContentStatusTxt = new byte[fsRead.Length];
                //    fsRead.Read(arrbContentStatusTxt, 0, Convert.ToInt32(fsRead.Length));
                //}
                //string sTxtStatusInit = @"\STATUS\" + Path.GetFileName(sFileStatusTxt);
                //UploadFileFtp(sTxtStatusInit, arrbContentStatusTxt, arrbContentStatusTxt.Length);

                #endregion

            }
            catch (Exception ex) { websvcLog.Error(ex); }
            return;
        }

        protected static bool IsNetCompressSuccess(string sFilename, string sGzFilename)
        {
            bool bReturn = false;
            if (File.Exists(sFilename))
            {
                try
                {
                    FileInfo fi = new FileInfo(sFilename);
                    using (FileStream inFile = fi.OpenRead())
                    {
                        if (File.Exists(sGzFilename))
                            File.Delete(sGzFilename);
                        using (FileStream outFile = File.Create(sGzFilename))
                        {
                            using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                            {
                                byte[] buffer = new byte[fi.Length];
                                inFile.Read(buffer, 0, Convert.ToInt32(fi.Length));
                                compress.Write(buffer, 0, Convert.ToInt32(fi.Length));
                                bReturn = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //CommonConsole.loggerConsoleSw.Error(ex);
                }
            }
            return bReturn;
        }

        protected DataTable dtGetProfileContent(string sTerminalID, SqlConnection oSqlConn)
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand("spProfileExportToText", oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.CommandTimeout = 0;
            oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = string.Format("WHERE TerminalID={0}", sTerminalID);
            oCmd.Parameters.Add("@iIsFullTable", SqlDbType.Bit).Value = 1;

            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            new SqlDataAdapter(oCmd).Fill(dtTemp);

            oCmd.Dispose();
            return dtTemp;
        }
        public double Add(double a, double b)
        {
            return a + b;
        }

        public double Substract(double a, double b)
        {
            return a - b;
        }

        public bool UploadFile(string sFilename, byte[] arrbContent, int iLength)
        {
            string sDestDir = WebConfigurationManager.AppSettings["Destination Folder"].ToString();
            sFilename = Path.Combine(sDestDir, sFilename);
            using (FileStream sw = new FileStream(sFilename, FileMode.Create))
            {
                sw.Write(arrbContent, 0, iLength);
            }
            return true;
        }

        protected static void Write2File(string sFileName, string sValue, bool bNewLine)
        {
            try
            {
                using (FileStream oFileStream = new FileStream(sFileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter oStreamWriter = new StreamWriter(oFileStream))
                    {
                        oStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        if (bNewLine) oStreamWriter.WriteLine(sValue);
                        else oStreamWriter.Write(sValue);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool UploadFileStatus(string sFilename)
        {
            string sDestDir = WebConfigurationManager.AppSettings["Destination Folder"].ToString();
            string sServerFilename = Path.Combine(sDestDir, sFilename);

            string sDirectory = Path.GetDirectoryName(sServerFilename);
            if (!Directory.Exists(sDirectory)) Directory.CreateDirectory(sDirectory);

            //websvcLog.Info(string.Format("Filename '{0}', Path '{1}', Size {2},", sFilename, sServerFilename, (new FileInfo(sServerFilename)).Length));

            string sFtpServer = WebConfigurationManager.AppSettings["FtpServer"].ToString();
            string sFtpUser = WebConfigurationManager.AppSettings["FtpUser"].ToString();
            string sFtpPwd = WebConfigurationManager.AppSettings["FtpPwd"].ToString();
            websvcLog.Info(string.Format("Server '{0}', user '{1}', sFtpPwd {2},", sFtpServer, sFtpUser, sFtpPwd));

            ftpClient ftp = new ftpClient(sFtpServer, sFtpUser, sFtpPwd);
            ftp.createDirectory(Path.GetDirectoryName(sFilename));
            /* Upload the File */
            ftp.uploadOverwrite(sFilename, sServerFilename);

            return true;
        }
        public bool UploadFileFtp(string sFilename, byte[] arrbContent, int iLength)
        {
            string sDestDir = WebConfigurationManager.AppSettings["Destination Folder"].ToString();
            string sServerFilename = Path.Combine(sDestDir, sFilename);

            string sDirectory = Path.GetDirectoryName(sServerFilename);
            if (!Directory.Exists(sDirectory)) Directory.CreateDirectory(sDirectory);

            using (FileStream sw = new FileStream(sServerFilename, FileMode.Create))
            {
                sw.Write(arrbContent, 0, iLength);
            }

            websvcLog.Info(string.Format("Filename '{0}', Path '{1}', Size {2},", sFilename, sServerFilename, new FileInfo(sServerFilename).Length));

            string sFtpServer = WebConfigurationManager.AppSettings["FtpServer"].ToString();
            string sFtpUser = WebConfigurationManager.AppSettings["FtpUser"].ToString();
            string sFtpPwd = WebConfigurationManager.AppSettings["FtpPwd"].ToString();
            websvcLog.Info(string.Format("Server '{0}', user '{1}', sFtpPwd {2},", sFtpServer, sFtpUser, sFtpPwd));

            ftpClient ftp = new ftpClient(sFtpServer, sFtpUser, sFtpPwd);
            ftp.createDirectory(Path.GetDirectoryName(sFilename));
            /* Upload the File */
            ftp.uploadOverwrite(sFilename, sServerFilename);

            return true;
        }

        public bool DeleteSubDirectoryFtp(string sMainDirectory)
        {

            string sFtpServer = WebConfigurationManager.AppSettings["FtpServer"].ToString();
            string sFtpUser = WebConfigurationManager.AppSettings["FtpUser"].ToString();
            string sFtpPwd = WebConfigurationManager.AppSettings["FtpPwd"].ToString();

            ftpClient ftp = new ftpClient(sFtpServer, sFtpUser, sFtpPwd);
            DeleteDirectoryFiles(ftp, sMainDirectory);
            return true;
        }

        void DeleteDirectoryFiles(ftpClient _ftp, string _sMainDirectory)
        {
            string[] arrsSubDirFiles = _ftp.directoryListSimple(_sMainDirectory);
            string[] arrsSubDirFilesDetail = _ftp.directoryListDetailed(_sMainDirectory);

            for (int i = 0; i < arrsSubDirFilesDetail.Length; i++)
            {
                string sSubDirFilesDetails = arrsSubDirFilesDetail[i];
                string sSubDirFiles = arrsSubDirFiles[i];

                if (!string.IsNullOrEmpty(sSubDirFiles))
                    if (sSubDirFilesDetails.Substring(0, 1) == "-")
                        _ftp.delete(_sMainDirectory + @"\" + Path.GetFileName(sSubDirFiles));
                    else if (sSubDirFilesDetails.Substring(0, 1) == "d")
                    {
                        DeleteDirectoryFiles(_ftp, _sMainDirectory + sSubDirFiles);
                        _ftp.deleteDirectory(_sMainDirectory + sSubDirFiles);
                    }
            }
        }

        public string[] DirectoryListFtp(string sDirectory)
        {
            string sFtpServer = WebConfigurationManager.AppSettings["FtpServer"].ToString();
            string sFtpUser = WebConfigurationManager.AppSettings["FtpUser"].ToString();
            string sFtpPwd = WebConfigurationManager.AppSettings["FtpPwd"].ToString();

            ftpClient ftp = new ftpClient(sFtpServer, sFtpUser, sFtpPwd);
            return ftp.directoryListSimple(sDirectory);
        }

        public void DownloadFtp(string sRemoteFile, ref byte[] arrbFileContent, ref int iLength)
        {
            string sDestDir = WebConfigurationManager.AppSettings["Destination Folder"].ToString();
            string sLocalFile = Path.Combine(sDestDir, Path.GetFileName(sRemoteFile));

            string sFtpServer = WebConfigurationManager.AppSettings["FtpServer"].ToString();
            string sFtpUser = WebConfigurationManager.AppSettings["FtpUser"].ToString();
            string sFtpPwd = WebConfigurationManager.AppSettings["FtpPwd"].ToString();

            ftpClient ftp = new ftpClient(sFtpServer, sFtpUser, sFtpPwd);
            ftp.download(sRemoteFile, sLocalFile);
            FileStream streamFileContent = new FileStream(sLocalFile, FileMode.Open, FileAccess.Read);
            streamFileContent.Read(arrbFileContent, 0, (int)streamFileContent.Length);
            iLength = (int)streamFileContent.Length;
        }

        public void DeleteFtp(string sFilename)
        {
            string sFtpServer = WebConfigurationManager.AppSettings["FtpServer"].ToString();
            string sFtpUser = WebConfigurationManager.AppSettings["FtpUser"].ToString();
            string sFtpPwd = WebConfigurationManager.AppSettings["FtpPwd"].ToString();

            ftpClient ftp = new ftpClient(sFtpServer, sFtpUser, sFtpPwd);
            ftp.delete(sFilename);
        }

        public bool LoginFtp()
        {
            string sFtpServer = WebConfigurationManager.AppSettings["FtpServer"].ToString();
            string sFtpUser = WebConfigurationManager.AppSettings["FtpUser"].ToString();
            string sFtpPwd = WebConfigurationManager.AppSettings["FtpPwd"].ToString();

            ftpClient ftpClient = new ftpClient(sFtpServer, sFtpUser, sFtpPwd);

            websvcLog.Info(string.Format("After ftpClient '{0}' / '{1}'", sFtpServer, sFtpPwd));
            string[] arrsDir = ftpClient.directoryListSimple("");
            if (arrsDir == null || arrsDir.Length == 0)
                return false;

            return true;
        }

        public void EmsAddUpdTms(EMS_XML emsXml, ref string sStatus)
        {
            EMS_XML emsResponse = new EMS_XML();
            int iError = 0;
            emsResponse = emsXml;
            string sResult = "XML Process Success";
            if (bDebug) sFileResult = string.Format(@"{1}\{0}.xml", emsResponse.Attribute.sSenderID, rootDir);

            EMSCommonClass.UserAccessDelete(new SqlConnection(sConnString), null);
            try
            {
                EMSCommonClass.IsXMLAddUpdate(sConnString, ref sResult, ref iError, ref emsResponse, null, sFileResult);
            }
            catch (Exception ex)
            {
                websvcLog.Error(ex);
                foreach (EMS_XML.DataClass.TerminalClass oTerminalClass in emsXml.Data.ltTerminal)
                {
                    EMS_XML.DataClass.TerminalClass oTempTerminalClass = new EMS_XML.DataClass.TerminalClass();

                    oTempTerminalClass.AddColumnValue("Terminal_Init", oTerminalClass.sGetColumnValue("Terminal_Init".ToUpper()));
                    oTempTerminalClass.AddColumnValue("Process_Type".ToUpper(), emsXml.Attribute.sTypes);

                    emsResponse.Data.Terminal.AddColumnValue("Response_Code", "001");
                    oTempTerminalClass.Result.sResultFieldName = "WEBSERVICE";
                    oTempTerminalClass.Result.sResultCode = "001";
                    oTempTerminalClass.Result.sResultDesc = sResult;
                    oTempTerminalClass.Result.sTimeProcess = DateTime.Now.ToString("ddMMyyyyhhmmss");

                    sStatus = string.Format("001. {0}", sResult);
                    emsResponse.Data.ltTerminal.Add(oTempTerminalClass);
                }
            }
        }

        public void EmsInqTms(EMS_XML emsXml, ref string sStatus)
        {
            EMS_XML emsResponse = new EMS_XML();
            int iError = 0;
            emsResponse = emsXml;
            string sResult = "XML Process Success";
            if (bDebug) sFileResult = string.Format(@"{1}\{0}.xml", emsResponse.Attribute.sSenderID, rootDir);

            EMSCommonClass.UserAccessDelete(new SqlConnection(sConnString), null);
            try
            {
                EMSCommonClass.IsXMLInquiry(sConnString, ref sResult, ref iError, ref emsResponse, null, sFileResult);
            }
            catch (Exception ex)
            {
                websvcLog.Error(ex);
                foreach (EMS_XML.DataClass.TerminalClass oTerminalClass in emsXml.Data.ltTerminal)
                {
                    EMS_XML.DataClass.TerminalClass oTempTerminalClass = new EMS_XML.DataClass.TerminalClass();

                    oTempTerminalClass.AddColumnValue("Terminal_Init", oTerminalClass.sGetColumnValue("Terminal_Init".ToUpper()));
                    oTempTerminalClass.AddColumnValue("Process_Type".ToUpper(), emsXml.Attribute.sTypes);

                    emsResponse.Data.Terminal.AddColumnValue("Response_Code", "001");
                    oTempTerminalClass.Result.sResultFieldName = "WEBSERVICE";
                    oTempTerminalClass.Result.sResultCode = "001";
                    oTempTerminalClass.Result.sResultDesc = sResult;
                    oTempTerminalClass.Result.sTimeProcess = DateTime.Now.ToString("ddMMyyyyhhmmss");

                    sStatus = string.Format("001. {0}", sResult);
                    emsResponse.Data.ltTerminal.Add(oTempTerminalClass);
                }
            }
        }

        public void EmsDelTms(EMS_XML emsXml, ref string sStatus)
        {
            EMS_XML emsResponse = new EMS_XML();
            int iError = 0;
            emsResponse = emsXml;
            string sResult = "XML Process Success";
            if (bDebug) sFileResult = string.Format(@"{1}\{0}.xml", emsResponse.Attribute.sSenderID, rootDir);

            EMSCommonClass.UserAccessDelete(new SqlConnection(sConnString), null);
            try
            {
                EMSCommonClass.IsXMLDelete(sConnString, ref sResult, ref iError, ref emsResponse, null, sFileResult);
            }
            catch (Exception ex)
            {
                websvcLog.Error(ex);
                foreach (EMS_XML.DataClass.TerminalClass oTerminalClass in emsXml.Data.ltTerminal)
                {
                    EMS_XML.DataClass.TerminalClass oTempTerminalClass = new EMS_XML.DataClass.TerminalClass();

                    oTempTerminalClass.AddColumnValue("Terminal_Init", oTerminalClass.sGetColumnValue("Terminal_Init".ToUpper()));
                    oTempTerminalClass.AddColumnValue("Process_Type".ToUpper(), emsXml.Attribute.sTypes);

                    emsResponse.Data.Terminal.AddColumnValue("Response_Code", "001");
                    oTempTerminalClass.Result.sResultFieldName = "WEBSERVICE";
                    oTempTerminalClass.Result.sResultCode = "001";
                    oTempTerminalClass.Result.sResultDesc = sResult;
                    oTempTerminalClass.Result.sTimeProcess = DateTime.Now.ToString("ddMMyyyyhhmmss");

                    sStatus = string.Format("001. {0}", sResult);
                    emsResponse.Data.ltTerminal.Add(oTempTerminalClass);
                }
            }
        }
    }
}