﻿using ipXML;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace WcfService_InSys
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IServiceInSys
    {
        //[OperationContract]
        //string GetData(int value);

        //[OperationContract]
        //CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
        [OperationContract]
        double Add(double a, double b);

        [OperationContract]
        double Substract(double a, double b);

        [OperationContract]
        bool UploadFile(string Filename, byte[] Content, int Length);

        [OperationContract]
        bool UploadFileFtp(string Filename, byte[] Content, int Length);

        [OperationContract]
        bool DeleteSubDirectoryFtp(string MainDirectory);

        [OperationContract]
        string[] DirectoryListFtp(string Directory);

        [OperationContract]
        void DownloadFtp(string RemoteFile, ref byte[] Content, ref int Length);

        [OperationContract]
        void DeleteFtp(string Filename);

        [OperationContract]
        bool LoginFtp();

        [OperationContract]
        void EmsAddUpdTms(EMS_XML emsXml, ref string sStatus);

        [OperationContract]
        void EmsInqTms(EMS_XML emsXml, ref string sStatus);

        [OperationContract]
        void EmsDelTms(EMS_XML emsXml, ref string sStatus);

        [OperationContract]
        void ExportToProfile(string sTerminalID);

        [OperationContract]
        void UploadFileNotAllow(string sTerminalID);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
    
}
