declare @sTotal varchar(5), @sQuery varchar(max)
set @sTotal = '[TOTAL]'

if OBJECT_ID('tempdb..#terminallist') is not null
	drop table #terminallist

select TerminalID into #terminallist
from tbProfileTerminalList with(nolock) where StatusMaster=0

if OBJECT_ID('tempdb..#terminalflazz') is not null
	drop table #terminalflazz

select TerminalID into #terminalflazz
from tbProfileTerminal with(nolock)  
where TerminalID in (select TerminalID from #terminallist)  
and TerminalTag='de43'
and TerminalTagValue=1

set @sQuery = 'select top ' + @sTotal + 'TerminalID, AcquirerTagValue from tbProfileAcquirer with(nolock)  
where TerminalID in (select TerminalID from #terminalflazz)
and AcquirerName = ''flazzbca''
and AcquirerTag = ''aa05''
and AcquirerTagValue like ''%xxxxxx'''

exec (@sQuery)