﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using InSysClass;

namespace ToolsUpdateFlazzFlag
{
    public partial class FrmUpdateFlazzFlag : Form
    {
        SqlConnection oSqlConn;
        int iTotalTerminal;
        DataTable dtResult;

        public FrmUpdateFlazzFlag()
        {
            InitializeComponent();
        }

        private void FrmUpdateFlazzFlag_Load(object sender, EventArgs e)
        {
            doInitConnection();
            doLogin();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            dgvResult.DataSource = null;
            if (IsTotalValid())
            {
                doSetDisplay(false);
                bwProcess.RunWorkerAsync();
            }
        }

        private void bwProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            /*
             * step-step :
             * 1. get list of terminalid which support bca flazz(table A) and list of terminalid with disable flazz acquirer(table B)
             * 2. check terminalid on table A is exist on table B, if false go to next terminalid, if true go to step-3
             * 3. disable flazz flag on terminal table
             */
            dtResult = new DataTable();
            DataColumn colTerminalId = new DataColumn("TerminalId");
            DataColumn colStatus = new DataColumn("Status");
            dtResult.Columns.Add(colTerminalId);
            dtResult.Columns.Add(colStatus);

            DataSet dsTables = dsGetTables();
            if (dsTables != null)
            {
                DataTable dtTerminalList = new DataTable();
                dtTerminalList = dsTables.Tables[0];
                if (dtTerminalList.Rows.Count > 0)
                {
                    foreach (DataRow rowProcessed in dtTerminalList.Rows)
                    {
                        string sError = null;
                        DataRow rowStatus = dtResult.NewRow();
                        rowStatus["TerminalId"] = rowProcessed["TerminalId"];
                        rowStatus["Status"] = IsUpdateFlagSuccess(rowProcessed["TerminalId"].ToString(), ref sError) ? "SUCCESS" : "FAILED. " + sError;
                        dtResult.Rows.Add(rowStatus);
                    }
                }
            }
        }

        private void bwProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            doSetDisplay(true);
            dgvResult.DataSource = dtResult;
        }

        #region "Function"
        protected void doInitConnection()
        {
            try
            {
                oSqlConn = new SqlConnection(new InitData(Application.StartupPath).sGetConnString());
                oSqlConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                oSqlConn.Dispose();
                this.Close();
            }
        }

        protected void doLogin()
        {
            if (isHaveConfiguration()) doStartLogin();
            else Application.ExitThread();
        }

        protected void doStartLogin()
        {
            FrmLogin fLogin = new FrmLogin(oSqlConn);
            try
            {
                fLogin.ShowDialog();
            }
            finally
            {
                UserData.sUserID = fLogin.sGetUserID();
                UserData.sUserName = fLogin.sGetUserName();
                UserData.sPassword = CommonClass.sGetEncrypt(fLogin.sGetPassword());
                UserData.isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
                UserData.sGroupID = fLogin.sGetGroupID();
                //iStatus = fLogin.iGetStatus();
            }

            if (fLogin.isCanLoginToSystem)
            {
                this.Visible = true;
                //doSetupDisplay(iStatus);
            }
            else
                Application.ExitThread();

            fLogin.Dispose();
        }

        /// <summary>
        /// Determines wheter have configuration or not.
        /// </summary>
        /// <returns>boolean : true if configuration found, else false</returns>
        protected bool isHaveConfiguration()
        {
            string sActiveDir = Application.StartupPath;
            string sFileName = sActiveDir + "\\Application.conf";

            if (File.Exists(sFileName)) return true;
            else return false;
        }

        protected bool IsTotalValid()
        {
            bool isValid = false;
            try
            {
                if (!string.IsNullOrEmpty(txtTotalTerminal.Text))
                {
                    iTotalTerminal = int.Parse(txtTotalTerminal.Text);
                    isValid = true;
                }
            }
            catch (Exception)
            {
            }
            return isValid;
        }

        protected DataSet dsGetTables()
        {
            //string sQuery = "select top iTotal TerminalID from tbProfileTerminal with(nolock) " +
            //    "where TerminalID in (select TerminalID from tbProfileTerminalList with(nolock) where StatusMaster=0) " +
            //    "and TerminalTag='de43' " +
            //    "and TerminalTagValue=1 " +
            //    "select TerminalID from tbProfileAcquirer with(nolock) " +
            //    "where TerminalID in (select TerminalID from tbProfileTerminalList with(nolock) where StatusMaster=0) " +
            //    "and AcquirerName = 'flazzbca' " +
            //    "and AcquirerTag = 'aa05' " +
            //    "and AcquirerTagValue like '%xxxxxx'";

            DataSet dsTemp = new DataSet();
            string sQuery = null;
            if (File.Exists(string.Format(@"{0}\Query\terminalList.sql", Environment.CurrentDirectory)))
            {
                using (StreamReader sr = new StreamReader(string.Format(@"{0}\Query\terminalList.sql", Environment.CurrentDirectory)))
                {
                    sQuery = sr.ReadToEnd();
                    sQuery = sQuery.Replace("[TOTAL]", iTotalTerminal.ToString());
                }
                if (!string.IsNullOrEmpty(sQuery))
                {
                    if (oSqlConn.State != ConnectionState.Open)
                        oSqlConn.Open();
                    using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
                    using (SqlDataAdapter oAdapter = new SqlDataAdapter(oCmd))
                        oAdapter.Fill(dsTemp);
                }
            }
            return dsTemp;
        }

        protected void doSetDisplay(bool isEnable)
        {
            gbSetting.Enabled = isEnable;
            gbResult.Enabled = isEnable;
            btnStart.Enabled = isEnable;
            pbProcessing.Style = !isEnable ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;
        }

        protected bool IsUpdateFlagSuccess(string sTerminalId, ref string sError)
        {
            bool isSuccess = false;
            string sQuery = string.Format("UPDATE tbProfileTerminal \n{0}\n{1}='{2}'\n{3}"
                , "SET TerminalTagValue='0' "
                , "WHERE TerminalId "
                , sTerminalId
                , "AND TerminalTag='DE43'");
            string sQueryLastUpdate = string.Format("EXEC spProfileTerminalUpdateLastUpdate '{0}'", sTerminalId);
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oCmd.ExecuteNonQuery();
                    (new SqlCommand(sQueryLastUpdate, oSqlConn)).ExecuteNonQuery();
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                sError = ex.Message;
            }
            return isSuccess;
        }
        #endregion
    }
}
