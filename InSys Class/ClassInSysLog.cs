﻿using System;
using System.Runtime.CompilerServices;
using Vi.Log4Vi;

namespace InSysClass
{
    public class InSysLogClass
    {
        static string rootFolder = Environment.CurrentDirectory + @"\LOGS\";
        public static string sAppName = "INSYSCLIENT";
        static Logger insysLog = new Logger(rootFolder, name: sAppName, useTraceAppender: false);

        public InSysLogClass(string LogFolder, string sName, bool TraceAppender)
        {
            insysLog = new Logger(LogFolder, sName, TraceAppender);
        }
        
        public void Info(string text, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysLog.Info(text, line, member, file);
        }

        public void Debug(string text, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysLog.Debug(text, line, member, file);
        }

        public void Warn(string text, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysLog.Warn(text, line, member, file);
        }

        public void Error(Exception se, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysLog.Error(se);

            // The logger in this class writes logs in two different files. 
            // One file is for Error and Fatal. The other file is for the other methods.
            // This line writes in the second file.
            insysLog.Warn("System.Exception: " + se.Message, line, member, file);
        }

        public void Fatal(string text, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysLog.Fatal(text, line, member, file);

            // The logger in this class writes logs in two different files. 
            // One file is for Error and Fatal. The other file is for the other methods.
            // This line writes in the second file.
            insysLog.Warn("System.Exception: " + text, line, member, file);
        }
    }
}
