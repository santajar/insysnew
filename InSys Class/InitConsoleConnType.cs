using System;
using System.IO;
using Ini;

namespace InSysClass
{
    public class InitConsoleConnType
    {
        protected bool IsSerial;
        protected bool IsTCPIP;
        protected bool IsModem;

        protected string sSerialPort;
        protected int iSerialBaudrate;
        protected float fSerialStopbit;
        protected int iSerialDataBit;
        protected string sSerialParity;

        protected string sModemPort1;
        protected string sModemPort2;
        protected string sModemPort3;
        protected string sModemPort4;
        protected string sModemPort5;

        protected string sDirectory;
        protected IniFile oObjIniFile;

        /// <summary>
        /// InitConsoleConnType class constructor
        /// </summary>
        /// <param name="_sDirectory">string : directory path of configuration file</param>
        public InitConsoleConnType(string _sDirectory)
        {
            sDirectory = _sDirectory;
            doGetInitData();
        }

        /// <summary>
        /// Get the data from configuration file
        /// </summary>
        protected void doGetInitData()
        {
            string sFileName = sDirectory + "\\Port.config";

            if (File.Exists(sFileName))
            {
                oObjIniFile = new IniFile(sFileName);
                try
                {
                    IsSerial = oObjIniFile.IniReadValue("CONNECTION", "Serial") == "1" ? true : false;
                    IsTCPIP = oObjIniFile.IniReadValue("CONNECTION", "TCPIP") == "1" ? true : false;
                    IsModem = oObjIniFile.IniReadValue("CONNECTION", "Modem") == "1" ? true : false;

                    sSerialPort = oObjIniFile.IniReadValue("SERIALPORT", "Port");
                    iSerialBaudrate = int.Parse(oObjIniFile.IniReadValue("SERIALPORT", "BaudRate"));
                    fSerialStopbit = int.Parse(oObjIniFile.IniReadValue("SERIALPORT", "StopBit"));
                    iSerialDataBit = int.Parse(oObjIniFile.IniReadValue("SERIALPORT", "DataBit"));
                    sSerialParity = oObjIniFile.IniReadValue("SERIALPORT", "Parity");

                    sModemPort1 = oObjIniFile.IniReadValue("MODEM", "Port-1");
                    sModemPort2 = oObjIniFile.IniReadValue("MODEM", "Port-2");
                    sModemPort3 = oObjIniFile.IniReadValue("MODEM", "Port-3");
                    sModemPort4 = oObjIniFile.IniReadValue("MODEM", "Port-4");
                    sModemPort5 = oObjIniFile.IniReadValue("MODEM", "Port-5");
                }
                catch (Exception ex)
                {
                    TraceLib.CreateMsgLog(sDirectory, ex.Message);
                }
            }
        }

        #region "SerialPort"
        /// <summary>
        /// To check the Console serialport is activated or not
        /// </summary>
        /// <returns>bool : TRUE if serialport is active, FALSE if not active</returns>
        public bool IsConnSerialActive()
        {
            return IsSerial;
        }

        /// <summary>
        /// Set the console serialport to active or not
        /// </summary>
        /// <param name="IsActive">bool : TRUE if serialport is active; FALSE if not active</param>
        public void SetSerialConn(bool IsActive)
        {
            oObjIniFile.IniWriteValue("CONNECTION", "Serial", IsActive ? "1" : "0");
        }

        /// <summary>
        /// Get or Set serialport name
        /// </summary>
        public string SerialPort { get { return sSerialPort; } set { sSerialPort = value; oObjIniFile.IniWriteValue("SERIALPORT", "Port", value); } }
        /// <summary>
        /// Get or Set serialport BaudRate
        /// </summary>
        public int SerialBaudRate { get { return iSerialBaudrate; } set { iSerialBaudrate = value; oObjIniFile.IniWriteValue("SERIALPORT", "BaudRate", value.ToString()); } }
        /// <summary>
        /// Get or Set serialport StopBits
        /// </summary>
        public float SerialStopBit { get { return fSerialStopbit; } set { fSerialStopbit = value; oObjIniFile.IniWriteValue("SERIALPORT", "StopBit", value.ToString()); } }
        /// <summary>
        /// Get or Set serialport DataBit
        /// </summary>
        public int SerialDataBit { get { return iSerialDataBit; } set { iSerialDataBit = value; oObjIniFile.IniWriteValue("SERIALPORT", "DataBit", value.ToString()); } }
        /// <summary>
        /// Get or Set serialport Parity
        /// </summary>
        public string SerialParity { get { return sSerialParity; } set { sSerialParity = value; oObjIniFile.IniWriteValue("SERIALPORT", "Parity", value); } }
        #endregion

        #region "TcpIp"
        /// <summary>
        /// To check the Console Tcp/Ip connection is active or not
        /// </summary>
        /// <returns>bool : TRUE if Tcp/Ip is active; FALSE if not active</returns>
        public bool IsConnTcpIpActive()
        {
            return IsTCPIP;
        }

        /// <summary>
        /// Set the Console Tcp/Ip connection to active or not
        /// </summary>
        /// <param name="IsActive">bool : TRUE if active; FALSE if not active</param>
        public void SetTcpIpConn(bool IsActive)
        {
            oObjIniFile.IniWriteValue("CONNECTION", "TCPIP", IsActive ? "1" : "0");
        }
        #endregion

        #region "Modem"
        /// <summary>
        /// To check Console serialport Modem activated or not.
        /// </summary>
        /// <returns>bool : TRUE if active; FALSE if not active</returns>
        public bool IsConnModemActive()
        {
            return IsModem;
        }

        /// <summary>
        /// Set the Console serialport Modem to active or not
        /// </summary>
        /// <param name="IsActive">bool : TRUE if active; FALSE if not active</param>
        public void SetModemConn(bool IsActive)
        {
            oObjIniFile.IniWriteValue("CONNECTION", "Modem", IsActive ? "1" : "0");
        }

        /// <summary>
        /// Get or Set the 1'st serialport Modem
        /// </summary>
        public string Modem1 { get { return sModemPort1; } set { sModemPort1 = value; oObjIniFile.IniWriteValue("MODEM", "Port-1", value); } }
        /// <summary>
        /// Get or Set the 2'nd serialport Modem
        /// </summary>
        public string Modem2 { get { return sModemPort2; } set { sModemPort2 = value; oObjIniFile.IniWriteValue("MODEM", "Port-2", value); } }
        /// <summary>
        /// Get or Set the 3'rd serialport Modem
        /// </summary>
        public string Modem3 { get { return sModemPort3; } set { sModemPort3 = value; oObjIniFile.IniWriteValue("MODEM", "Port-3", value); } }
        /// <summary>
        /// Get or Set the 4'th serialport Modem
        /// </summary>
        public string Modem4 { get { return sModemPort4; } set { sModemPort4 = value; oObjIniFile.IniWriteValue("MODEM", "Port-4", value); } }
        /// <summary>
        /// Get or Set the 5'th serialport Modem
        /// </summary>
        public string Modem5 { get { return sModemPort5; } set { sModemPort5 = value; oObjIniFile.IniWriteValue("MODEM", "Port-5", value); } }
        #endregion
    }
}
