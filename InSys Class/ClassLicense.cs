using System;
using System.IO;
using Ini;

namespace InSysClass
{
    public class LicenseIP
    {
        //  pattern filename : <application name>License.ini
        //  mis : application name -> insys, file -> insysLicense.ini
        //  ====================================================
        //  TLV Format
        //  ====================================================
        //  IP01 -> User
        //  IP02 -> Company Name
        //  IP03 -> License Date
        //  IP04 -> Registered application boolean; 0 : Trial Application, 1 : Registered Application, 2 : Renew Application
        //  IP05 -> SN HardDisk (Optional)
        //  IP06 -> Application Name
        //  ====================================================
        const string IP01 = "IP01";
        const string IP02 = "IP02";
        const string IP03 = "IP03";
        const string IP04 = "IP04";
        const string IP05 = "IP05";
        const string IP06 = "IP06";

        protected string sPath;
        protected string sAppName;
        protected string sLicense = "License.ini";

        string sUser;
        string sCompany;
        string sLicenseDate;
        string sHardDiskSN;
        string sLicenseKey;
        string sActivationKey;
        string sTrialApplication = "0";

        public LicenseIP(string _sAppName)
            : this(_sAppName, Environment.SystemDirectory + "\\")
        { 
        }

        public LicenseIP(string _sAppName, string _sPath)
        {
            sPath = _sPath;
            sAppName = _sAppName;
            if (File.Exists(sPath + sAppName + sLicense))
                InitLicense();
            else
            {
                sUser = null;
                sCompany = null;
                sLicenseDate = null;
                sHardDiskSN = null;
                sLicenseKey = null;
                sActivationKey = null;
            }
        }

        protected void InitLicense()
        {
            string sLicenseFileName = sPath + sAppName + sLicense;
            string sFilename = sPath + sAppName + ".txt";
            EncryptionLib.DecryptFile(sLicenseFileName, sFilename);
            InitAppLicense oObjLicense = new InitAppLicense(sFilename);
            sUser = oObjLicense.sGetUser();
            sCompany = oObjLicense.sGetCompany();
            sLicenseKey = oObjLicense.sGetLicenseKey();
            sHardDiskSN = HardDiskLib.sGetSerialNumber("C:\\");
            File.Delete(sFilename);
        }

        #region "Activation"
        /// <summary>
        /// Return TRUE, if decrypted User and Company from Activation Key registry 
        /// is match to User and Company registry
        /// </summary>
        /// <returns>TRUE, if match. False if not match</returns>
        public bool IsActivated()
        {
            sActivationKey = sGetActivation();
            sUser = sGetRegUser();
            sCompany = sGetRegCompany();

            string sDecryptedValue = EncryptionLib.Decrypt3DES(sActivationKey);
            string sUser2 = CommonLib.sGetTagValue(sDecryptedValue, IP01, 2);
            string sCompany2 = CommonLib.sGetTagValue(sDecryptedValue, IP02, 2);
            sTrialApplication = CommonLib.sGetTagValue(sDecryptedValue, IP04, 2);
            string sAppName2 = CommonLib.sGetTagValue(sDecryptedValue, IP06, 2);

            if (sUser == sUser2 && sCompany == sCompany2 && sAppName == sAppName2)
                return true;
            else
                return false;
        }

        public bool IsActivated(string _sUser, string _sCompany, string _LicenseKey)
        {
            string sDecryptedValue = EncryptionLib.Decrypt3DES(_LicenseKey);
            string sUser2 = CommonLib.sGetTagValue(sDecryptedValue, IP01, 2);
            string sCompany2 = CommonLib.sGetTagValue(sDecryptedValue, IP02, 2);
            sTrialApplication = CommonLib.sGetTagValue(sDecryptedValue, IP04, 2);
            string sAppName2 = CommonLib.sGetTagValue(sDecryptedValue, IP06, 2);

            if (_sUser == sUser2 && _sCompany == sCompany2 && sAppName == sAppName2)
            {
                CreateLicense(_sUser, _sCompany);
                return true;
            }
            else
                return false;
        }

        protected string sGetActivation()
        {
            return RegIP.sGetKeyValue("Activation Key", 1, sAppName);
        }

        protected string sGetRegUser()
        {
            return RegIP.sGetKeyValue("User", 1, sAppName);
        }

        protected string sGetRegCompany()
        {
            return RegIP.sGetKeyValue("Company", 1, sAppName);
        }

        protected string sGetRegLicenseDate()
        {
            return CommonLib.sGetTagValue(EncryptionLib.Decrypt3DES(sGetActivation()), IP03, 2);
        }
        #endregion

        #region "Create License"
        public void CreateLicense()
        {
            sUser = sGetRegUser();
            sCompany = sGetRegCompany();
            sLicenseDate = sGetRegLicenseDate();
            sHardDiskSN = HardDiskLib.sGetSerialNumber("C:\\");
            sLicenseKey = sGenLicenseKey();

            string sFilename = sPath + sAppName + ".txt";
            FillLicenseFile(sFilename);

            string sLicenseFileName = sPath + sAppName + sLicense;
            EncryptionLib.EncryptFile(sLicenseFileName, sFilename);
            File.Delete(sFilename);
        }

        public void CreateLicense(string _sUser, string _sCompany)
        {
            sUser = _sUser;
            sCompany = _sCompany;
            sLicenseDate = DateTime.Now.ToString("dd-MM-yyyy");
            sHardDiskSN = HardDiskLib.sGetSerialNumber("C:\\");

            sLicenseKey = sGenLicenseKey();

            string sFilename = sPath + "\\" + sAppName + ".txt";
            FillLicenseFile(sFilename);

            string sLicenseFileName = sPath + "\\" + sAppName + sLicense;
            EncryptionLib.EncryptFile(sLicenseFileName, sFilename);
            File.Delete(sFilename);
        }

        protected string sGenLicenseKey()
        {
            string sTempUser = IP01 + sUser.Length.ToString("00") + sUser;
            string sTempCompany = IP02 + sCompany.Length.ToString("00") + sCompany;
            string sTempLicenseDate = IP03 + sLicenseDate.Length.ToString("00") + sLicenseDate;
            string sTempTrialApps = IP04 + sTrialApplication.Length.ToString("00") + sTrialApplication;
            string sTempSN = IP05 + sHardDiskSN.Length.ToString("00") + sHardDiskSN;
            return EncryptionLib.Encrypt3DES(sTempUser + sTempCompany + sTempLicenseDate + sTempTrialApps + sTempSN);
        }

        public string sGenLicenseKey(string _sUser, 
            string _sCompany, 
            string _sLicenseDate,
            bool IsTrial)
        {
            sTrialApplication = IsTrial ? "0" : "1";

            string sTempUser = IP01 + _sUser.Length.ToString("00") + _sUser;
            string sTempCompany = IP02 + _sCompany.Length.ToString("00") + _sCompany;
            string sTempLicenseDate = IP03 + _sLicenseDate.Length.ToString("00") + _sLicenseDate;
            string sTempTrialApps = IP04 + sTrialApplication.Length.ToString("00") + sTrialApplication;
            string sTempSN = IP05 + "00";
            string sTempApp = IP06 + sAppName.Length.ToString("00") + sAppName;
            return EncryptionLib.Encrypt3DES(sTempUser 
                + sTempCompany + sTempLicenseDate + sTempTrialApps + sTempSN
                + sTempApp);
        }

        protected void FillLicenseFile(string _sFileName)
        {
            InitAppLicense oObjLicense = new InitAppLicense(_sFileName);
            oObjLicense.doChangeUser(sUser);
            oObjLicense.doChangeCompany(sCompany);
            oObjLicense.doChangeLicenseKey(sLicenseKey);
            //oObjLicense.doChangeLicenseDate(sLicenseDate);
        }
        #endregion
        
        public bool IsLicenseValid()
        {
            if (!string.IsNullOrEmpty(sLicenseKey))
            {
                string sDecryptedLicenseKey = EncryptionLib.Decrypt3DES(sLicenseKey);
                string sUser1 = CommonLib.sGetTagValue(sDecryptedLicenseKey, IP01, 2);
                string sCompany1 = CommonLib.sGetTagValue(sDecryptedLicenseKey, IP02, 2);
                string sHarddiskSN1 = CommonLib.sGetTagValue(sDecryptedLicenseKey, IP05, 2);
                if (sUser == sUser1 
                    && sCompany == sCompany1 
                    && sHardDiskSN == sHarddiskSN1)
                    return true;
                else return false;
            }
            else return false;
        }

        public bool IsLicenseTrial(ref int iExpiryCount)
        {
            string sDecryptedLicenseKey;
            string sTrial = "0";
            if (!string.IsNullOrEmpty(sLicenseKey))
            {
                sDecryptedLicenseKey = EncryptionLib.Decrypt3DES(sLicenseKey);
                string sTempLicenseDate = CommonLib.sGetTagValue(sDecryptedLicenseKey, IP03, 2);
                DateTime dtLicense = DateTime.Parse(sTempLicenseDate, CommonLib.ciIDFormat).AddMonths(1);
                TimeSpan span = dtLicense.Subtract(DateTime.Today);
                iExpiryCount = span.Days;                
                sTrial = CommonLib.sGetTagValue(sDecryptedLicenseKey, IP04, 2);
            }
            return sTrial == "0" ? true : false;
        }

        public bool IsLicenseWillExpiry(ref string sExpiryDate)
        {
            if (!string.IsNullOrEmpty(sLicenseKey))
            {
                string sDecryptedLicenseKey = EncryptionLib.Decrypt3DES(sLicenseKey);
                sLicenseDate = CommonLib.sGetTagValue(sDecryptedLicenseKey, IP03, 2);
                sLicenseDate = sLicenseDate.Substring(6, 4) + sLicenseDate.Substring(2, 4) + sLicenseDate.Substring(0, 2);
                DateTime dtLicense = DateTime.Parse(sLicenseDate, CommonLib.ciIDFormat).AddYears(1);
                sExpiryDate = dtLicense.ToString("dd-MM-yyyy", new System.Globalization.CultureInfo("en-US"));
                TimeSpan span = dtLicense.Subtract(DateTime.Today);
                //if (span.Days <= 30)
                //    return true;
                //else return false;
                return true;
            }
            else return false;
        }

        public bool IsLicenseTrial()
        {
            throw new NotImplementedException();
        }
    }

    public class LicenseKeepAliveCounter
    {
        protected int iCounterLive = 0;
        protected int iCounterRemain = 0;
        protected bool bRegistered; // FALSE, for trial application
        protected bool bUnlimited;
        protected DateTime dtLicenseCreated;
        protected string sFilename = Environment.SystemDirectory + @"\License.psiini";

        public int CounterLive
        {
            get { return iCounterLive; }
            set { iCounterLive = value; }
        }

        public int CounterRemain
        {
            get { return iCounterRemain; }
            set { iCounterRemain = value; }
        }

        public int CounterDaysRemain
        {
            get { return iCounterRemain / (24 * 60 * 60); }
        }

        protected bool Registered
        {
            get { return bRegistered; }
            set { bRegistered = value; }
        }

        public bool Unlimited
        {
            get { return bUnlimited; }
            set { bUnlimited = value; }
        }

        public LicenseKeepAliveCounter()
        {
            if (File.Exists(sFilename))
                InitLicense(sFilename);
            else
                CreateTrialLicense();
        }

        protected void CreateTrialLicense()
        {
            iCounterRemain = 30 * 24 * 60 * 60; //in seconds
            string sFilenamePlain = Environment.CurrentDirectory + @"\License.psiini.txt";
            try
            {
                //File.CreateText(sFilenamePlain);
                
                //IniFile oLicense = new IniFile(sFilenamePlain);
                //oLicense.IniWriteValue("LICENSE", "Remain", iCounterRemain.ToString());
                //oLicense.IniWriteValue("LICENSE", "Live", "0");
                //oLicense.IniWriteValue("LICENSE", "Trial", "1");
                //oLicense.IniWriteValue("LICENSE", "Unlimited", "0");
                //oLicense.IniWriteValue("LICENSE", "StartDate", DateTime.Now.ToString("MM/dd/yyyy"));

                using (StreamWriter sw = new StreamWriter(sFilenamePlain))
                {
                    string sLicenseContent = null;
                    sLicenseContent = string.Format("[LICENSE]");
                    sLicenseContent = string.Format("{0}\nRemain={1}", sLicenseContent, iCounterRemain);
                    sLicenseContent = string.Format("{0}\nLive=0", sLicenseContent);
                    sLicenseContent = string.Format("{0}\nTrial=1", sLicenseContent);
                    sLicenseContent = string.Format("{0}\nUnlimited=0", sLicenseContent);
                    sLicenseContent = string.Format("{0}\nStartDate={1:MM/dd/yyyy}", sLicenseContent, DateTime.Now);
                    sw.Write(sLicenseContent);
                }

                EncryptionLib.EncryptFile(sFilename, sFilenamePlain);
                File.Delete(sFilenamePlain);
            }
            catch (Exception)
            {
            }
        }

        protected void InitLicense(string sFilename)
        {
            string sFilenamePlain = Environment.CurrentDirectory + @"\License.psiini.txt";
            
            try
            {
                EncryptionLib.DecryptFile(sFilename, sFilenamePlain);
                IniFile oLicense = new IniFile(sFilenamePlain);
                iCounterRemain = int.Parse(oLicense.IniReadValue("LICENSE", "Remain"));
                iCounterLive = int.Parse(oLicense.IniReadValue("LICENSE", "Live"));
                bRegistered = int.Parse(oLicense.IniReadValue("LICENSE", "Trial")) == 0 ? true : false;
                bUnlimited = int.Parse(oLicense.IniReadValue("LICENSE", "Unlimited")) == 1 ? true : false;
                dtLicenseCreated = DateTime.Parse(oLicense.IniReadValue("LICENSE", "StartDate"));

                File.Delete(sFilenamePlain);
            }
            catch (Exception)
            {
            }
        }

        public bool isTrialLicense()
        {
            return !bRegistered;
        }

        public bool isValidLicense()
        {
            if (bUnlimited)
                return true;
            else
                if (iCounterRemain > 0)
                {
                    if (dtLicenseCreated.Date == DateTime.Now.AddDays(-(iCounterLive / (24 * 60 * 60))))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
        }

        public void UpdateLicense(int iAllReadyLive)
        {
            iCounterRemain -= iAllReadyLive;
            iCounterLive += iAllReadyLive;
            
            string sFilenamePlain = Environment.CurrentDirectory + @"\License.psiini.txt";
            try
            {
                File.CreateText(sFilenamePlain);
                IniFile oLicense = new IniFile(sFilenamePlain);
                oLicense.IniWriteValue("LICENSE", "Remain", iCounterRemain.ToString());
                oLicense.IniWriteValue("LICENSE", "Live", iCounterLive.ToString());
                oLicense.IniWriteValue("LICENSE", "Trial", bRegistered ? "0" : "1");
                oLicense.IniWriteValue("LICENSE", "Unlimited", bUnlimited ? "1" : "0");

                EncryptionLib.EncryptFile(sFilename, sFilenamePlain);
                File.Delete(sFilenamePlain);
            }
            catch (Exception)
            {
            }
        }

        public void RegisterLicense()
        {
            string sFilenamePlain = Environment.CurrentDirectory + @"\License.psiini.txt";
            try
            {
                EncryptionLib.DecryptFile(sFilename, sFilenamePlain);
                //File.CreateText(sFilenamePlain);
                IniFile oLicense = new IniFile(sFilenamePlain);
                oLicense.IniWriteValue("LICENSE", "Remain", iCounterRemain.ToString());
                oLicense.IniWriteValue("LICENSE", "Live", iCounterLive.ToString());
                oLicense.IniWriteValue("LICENSE", "Trial", "0");
                oLicense.IniWriteValue("LICENSE", "Unlimited", bUnlimited ? "1" : "0");
                oLicense.IniWriteValue("LICENSE", "StartDate", DateTime.Now.ToString("MM/dd/yyyy"));

                EncryptionLib.EncryptFile(sFilename, sFilenamePlain);
                File.Delete(sFilenamePlain);
            }
            catch (Exception)
            {
            }
        }
    }
}