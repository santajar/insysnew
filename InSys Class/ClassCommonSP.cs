﻿namespace InSysClass
{
    public class CommonSP
    {
        #region "Users"
        public static string sSPUserRoleInsert = "spUserRoleInsert";
        public static string sSPUserBrowse = "spUserLoginBrowse";
        public static string sSPUserInsert = "spUserLoginInsert";
        public static string sSPUserUpdate = "spUserLoginUpdate";
        public static string sSPUserDelete = "spUserLoginDelete";
        public static string sSPLockedUserID = "spLockedUserID";
        public static string sSPLockedUserIDbyDays = "spUserLoginLockedbyDays";
        public static string sSPCheckHistoryPassword = "spCheckHistoryPassword";
        #endregion

        #region "SuperUser"
        public static string sSPSuperUserUpdate = "spSuperUserLoginUpdate";
        public static string sSPSuperUserBrowse = "spUserSuperLoginBrowse";
        #endregion

        #region "Application"
        public static string sSPApplicationBrowse = "spBitMapBrowse";
        public static string sSPApplicationInsert = "spBitMapInsert";
        public static string sSPApplicationUpdate = "spBitMapUpdate";
        public static string sSPApplicationDelete = "spBitMapDelete";
        #endregion

        #region "Profile"
        public static string spUpdateConnAcquirer = "spUpdateConnAcq";
        public static string sSPTerminalDBBrowse = "spProfileTerminalDbBrowse";
        public static string sSPTerminalListBrowse = "spProfileTerminalListBrowse";
        public static string sSPTerminalListBrowseAdvance = "spProfileTerminalListBrowseAdvanceSearch";
        public static string sSPTerminalListBrowseAdvance1 = "spProfileTerminalListBrowseAdvanceSearch1";
        public static string sSPTerminalCompressBrowse = "spProfileTerminalInitCompressBrowse";
        public static string sSPRelationBrowse = "spProfileRelationBrowse";
        public static string sSPRelationDelete = "spProfileRelationDelete";
        public static string sSPRelationCopy = "spProfileRelationCopy";
        public static string sSPProfileBrowse = "spProfileTerminalContentBrowse";
        public static string sSPItemBrowse = "spItemListBrowse";
        public static string sSPItemChildBrowse = "spItemListChildBrowse";
        public static string sSPObjBrowse = "spItemObjectBrowse";
        public static string sSPCmbBrowse = "spItemComboBoxBrowse";
        public static string sSPViewTerminalListBrowse = "spViewTerminalListBrowse";
        public static string sSPViewSerialNumberListBrowse = "spViewSerialNumberListBrowse";
        public static string sSPInserOrUpdateSerialNumberAdd = "spInsertSerialNumberList";
        public static string sSPAutoInitLogInfoBrowse = "spAutoInitLogBrowse";
        public static string sSPInitLogInfoBrowse = "spInitLogBrowse";

        public static string sSPTerminalBrowse = "spProfileTerminalBrowse";
        public static string sSPAcqBrowse = "spProfileAcquirerBrowse";
        public static string sSPIssBrowse = "spProfileIssuerBrowse";
        public static string sSPCardBrowse = "spProfileCardListBrowse";
        public static string sSPCardListBrowse = "spCardListBrowse";
        public static string sSPTerminalListBrowseLast = "spProfileTerminalListBrowseTopTen";


        public static string sSPTerminalCompressUpdate = "spProfileTerminalListCompressUpdate";

        public static string sSPProfileCopy = "spProfileTerminalCopyProcess";

        public static string sSPProfileDelete = "spProfileTerminalDeleteProcess";
        public static string sSPAcqDeleteByTID = "spProfileAcquirerDeleteProcess";
        public static string sSPIssDeleteByTID = "spProfileIssuerDeleteProcess";
        public static string sSPCardDeleteByTID = "spProfileRelationDeleteByIssuerOrCard";
        public static string sSPIssuerCopy = "spProfileIssuerCopy";

        public static string sSPAcqRename = "spProfileAcquirerRenameProcess";
        public static string sSPIssRename = "spProfileIssuerRenameProcess";

        public static string sSPCardListInsert = "spProfileCardListInsert";
        public static string sSPCardListDelete = "spProfileCardListDelete";
        public static string sSPCardListUpdate = "spProfileCardListUpdate";

        //public static string sSPCardListCopy = "spProfileCardListCopy";
        public static string sSPCardNameCopy = "spCardNameCopy";
        public static string sSPTleNameCopy = "spProfileTLECopyList";
        public static string sSPAIDNameCopy = "spProfileAIDCopy";
        public static string sSPGprsCopy = "spProfileGPRSCopy";
        public static string sSPCAPKCopy = "spProfileCAPKCopy";

        public static string sSPTerminalInsert = "spProfileTerminalInsert";
        public static string sSPAcqInsert = "spProfileAcquirerInsert";
        public static string sSPIssInsert = "spProfileIssuerInsert";
        public static string sSPCardInsert = "spProfileRelationInsertProcess";


        public static string sSPTerminalUpdate = "spProfileTerminalUpdate";
        public static string sSPAcqUpdate = "spProfileAcquirerUpdate";
        public static string sSPIssUpdate = "spProfileIssuerUpdate";


        public static string sSPTerminalImport = "spProfileImport";
        public static string sSPRelationBrowseCondition = "spProfileRelationBrowseWithCondition";

        public static string sSPLocationBrowseWithTID = "spLocationBrowseWithTID";
        public static string sSPLocationUpdate = "spLocationUpdate";
        public static string sSPLocationBrowse = "spLocationBrowse";
        public static string sSPLocationInsert = "spLocationInsert";
        public static string sSPLocationDelete = "spLocationDelete";

        public static string sSPTerminalDBInsert = "spProfileTerminalDbInsert";
        public static string sSPVersionCopy = "spDatabaseVersionCopy";
        public static string sSPVersionDelete = "spDatabaseVersionDeleteProcess";

        public static string sSPCardListCopyByDbID = "spProfileCardListInsertByDbId";
        public static string sSPCardListCopyByID = "spProfileCardListInsertById";

        public static string sSPItemInsert = "spItemListInsert";
        public static string sSPItemUpdate = "spItemListUpdate";
        public static string sSPItemDelete = "spItemListDelete";
        public static string sSPItemDeleteAll = "spItemListDeleteByDbID";
        public static string sSPItemCmbValueInsert = "spItemComboBoxValueInsert";
        public static string sSPItemCmbValueDelete = "spItemComboBoxValueDelete";
        public static string sSPItemBrowseItemSeq = "spItemListBrowseValidItemSquence";

        public static string sSPMsItemFormBrowse = "spMsItemFormBrowse";

        public static string sSPBrowseProfileRelationAutoInitLogAppPackage = "spBrowseProfileRelationAutoInitLogAppPackage";
        public static string sSPTerminalDBEdit = "spProfileTerminalDbEdit";
        //public static string sSPTerminalListInsertScheduleID = "spProfileterminalListInsertScheduleID";
        //public static string sSPTerminalListUpdateScheduleID = "spProfileTerminalListUpdateScheduleID";
        #endregion

        #region "Audit"
        public static string sSPAuditTrailBrowse = "spAuditTrailBrowse";
        public static string sSPAuditTrailInsert = "spAuditTrailInsert";
        public static string sSPAuditTrailDelete = "spAuditTrailDelete";
        public static string sSPAuditTrailSUInsert = "spAuditTrailEncryptInsert";
        public static string sSPAuditTrailSUBrowse = "spAuditTrailEncryptBrowse";
        #endregion

        #region "InitTrail"
        public static string sSPInitTrailBrowse = "spAuditInitBrowse";
        public static string sSPInitTrailSummaryBrowse = "spAuditInitSummaryBrowse";

        public static string sSPInitTrailDelete = "spAuditInitDelete";
        public static string sSPInitTrailSummaryDelete = "spAuditInitSummaryDelete";

        public static string sSPInsertInitTrailTemp = "spAuditInitTempInsert";
        public static string sSPDeleteTempInitTrail = "spTempAuditTrailDelete";
        #endregion

        #region "Init Software Trail"
        public static string sSPInitSoftwareTrailBrowse = "spAuditInitSoftwareBrowse";
        public static string sSPInitSoftwareTrailDelete = "spAuditInitSoftwareDelete";
        #endregion

        #region "Software Progress"
        public static string sSPSoftwareProgressBrowse = "spSoftwareProgressBrowse";
        public static string sSPSoftwareProgressBrowseCPL = "spSoftwareProgressBrowseCPL";
        public static string sSPSoftwareProgressBrowseCPLDetail = "spSoftwareProgressBrowseCPLDetail";
        #endregion

        #region search sn
        public static string sSPSearchSerialNumber = "spSSearchSerialNumber";
        #endregion

        #region "Echo Ping"
        public static string sSPAuditTerminalSNEchoBrowse = "spAuditTerminalSNEchoBrowse";
        public static string sSPAuditTerminalSNEchoInsert { get { return "spAuditTerminalSNEchoInsert"; } }
        #endregion

        #region "Auto Init"
        public static string sSPAutoInitBrowse = "spControlFlagAutoInitBrowse";
        public static string sSPAutoInitUpdate = "spControlFlagAutoInitUpdate";
        #endregion

        #region "Init Connection"
        public static string sSPInitConnBrowse = "spControlFlagInitBrowse";
        public static string sSPInitConnUpdate = "spControlFlagInitUpdate";
        #endregion

        #region "Monitoring"
        public static string sSPInitLogConnBrowse = "spInitLogConnBrowse";
        public static string sSPInitLogConnDetailBrowse = "spInitLogConnDetailBrowse";
        #endregion

        #region
        public static string sSPProfileCopyDataTerminalID = "spProfileCopyDataTerminalID";
        #endregion

        #region "Data Upload"
        public static string sSPUploadTagDECount = "spUploadTagDECount";
        public static string sSPUploadTagAACount = "spUploadTagAACount";

        public static string sSPUploadTagDESourceCount = "spUploadTagDESourceCount";
        public static string sSPUploadTagAASourceCount = "spUploadTagAASourceCount";

        public static string sSPUploadTagDEInsert = "spUploadTagDEInsert";
        public static string sSPUploadTagAAInsert = "spUploadTagAAInsert";

        public static string sSPUploadTagDEDelete = "spUploadTagDEDelete";
        public static string sSPUploadTagAADelete = "spUploadTagAADelete";

        public static string sSPUploadTagDEAAUnion = "spUploadTagDEAAUnion";

        public static string sSPUploadTagTemplate = "spUploadTagTemplate";

        public static string sSPUploadTagDEUpdateSourceColumn = "spUploadTagDEUpdateSourceColumn";
        public static string sSPUploadTagAAUpdateSourceColumn = "spUploadTagAAUpdateSourceColumn";

        public static string sSPUploadControlSet = "spUploadControlSet";
        public static string sSPUploadControlGet = "spUploadControlGet";

        public static string sSPUploadGetColumnsDE = "spUploadGetColumnsDE";
        public static string sSPUploadGetColumnsAA = "spUploadGetColumnsAA";

        public static string sSPDataUploadStartProcessAdd = "spDataUploadStartProcessAdd";
        public static string sSPDataUploadCreateTable = "spDataUploadCreateTable";
        public static string sSPDataUploadDropTable = "spDataUploadDropTable";
        public static string sSPDataUploadDelete = "spDataUploadDelete";

        public static string sSPUploadTagDEBrowse = "spUploadTagDEBrowse";
        public static string sSPUploadTagAABrowse = "spUploadTagAABrowse";

        public static string sSPDataUploadStartProcessUpdate = "spDataUploadStartProcessUpdate";
        public static string sSPDataUploadStartProcessDelete = "spDataUploadStartProcessDelete";

        public static string sSPUploadAcquirerList = "spDataUploadAcquirerListBrowse";
        #endregion

        #region "Control Flag"
        public static string sSPControlFlagAllowInitUpdate = "spControlFlagAllowInitUpdate";
        public static string sSPControlFlagAllowInitBrowse = "spControlFlagAllowInitBrowse";
        public static string sSPControlFlagUpdate = "spControlFlagUpdatePaths";
        public static string sSPControlFlagBrowse = "spControlFlagBrowseItem";
        #endregion

        #region "Export Profile"
        // "Export"
        public static string sSPExportProfileToText = "spProfileExportToText";
        public static string sSPProfileText = "spProfileText";
        public static string sSPProfileTextFull = "spProfileTextFull";
        #endregion

        #region "EMV"
        public static string sSPDBListEMVBrowse = "spDBListEMVBrowse";

        #region "AID"
        public static string sSPProfileAIDBrowseList = "spProfileAIDBrowseList";
        public static string sSPProfileAIDBrowse = "spProfileAIDBrowse";
        public static string sSPProfileAIDInsert = "spProfileAIDInsert";
        public static string sSPProfileAIDDelete = "spProfileAIDDelete";
        public static string sSPProfileAIDUpdate = "spProfileAIDUpdate";
        public static string sSPProfileAIDCopyListBrowse = "spProfileAIDListBrowse";
        public static string sSPProfileAIDCopy = "spProfileAIDCopybyDBID";
        #endregion

        #region "CAPK"
        public static string sSPProfileCAPKBrowseList = "spProfileCAPKBrowseList";
        public static string sSPProfileCAPKBrowse = "spProfileCAPKBrowse";
        public static string sSPProfileCAPKInsert = "spProfileCAPKInsert";
        public static string sSPProfileCAPKDelete = "spProfileCAPKDelete";
        public static string sSPProfileCAPKUpdate = "spProfileCAPKUpdate";
        public static string sSPProfileCAPKCopyListBrowse = "spProfileCAPKListBrowse";
        public static string sSPProfileCAPKCopy = "spProfileCAPKCopybyDBID";
        #endregion
        #endregion

        #region "TLE"
        public static string sSPTLEListBrowse = "spProfileTLEListBrowse ";
        public static string sSPTLEBrowse = "spProfileTLEBrowse";
        public static string sSPTLEInsert = "spProfileTLEInsert";
        public static string sSPTLEUpdate = "spProfileTLEUpdate";
        public static string sSPTLEDelete = "spProfileTLEDelete";
        public static string sSPTLECopyListBrowse = "spTLEListBrowse";
        public static string sSPProfileTLECopy = "spProfileTLECopy";
        public static string sSPDBListTLEBrowse = "spDBListTLEBrowse";
        #endregion

        #region "Loyalty Pool"
        public static string sSPLoyPoolListBrowse = "spProfileLoyPoolListBrowse ";
        public static string sSPLoyPoolBrowse = "spProfileLoyPoolBrowse";
        public static string sSPLoyPoolInsert = "spProfileLoyPoolInsert";
        public static string sSPLoyPoolUpdate = "spProfileLoyPoolUpdate";
        public static string sSPLoyPoolDelete = "spProfileLoyPoolDelete";
        #endregion

        #region "Loyalty Product"
        public static string sSPLoyProdListBrowse = "spProfileLoyProdListBrowse ";
        public static string sSPLoyProdBrowse = "spProfileLoyProdBrowse";
        public static string sSPLoyProdInsert = "spProfileLoyProdInsert";
        public static string sSPLoyProdUpdate = "spProfileLoyProdUpdate";
        public static string sSPLoyProdDelete = "spProfileLoyProdDelete";
        #endregion

        #region "GPRS"
        public static string sSPGPRSListBrowse = "spProfileGPRSListBrowse ";
        public static string sSPGPRSBrowse = "spProfileGPRSBrowse";
        public static string sSPGPRSInsert = "spProfileGPRSInsert";
        public static string sSPGPRSUpdate = "spProfileGPRSUpdate";
        public static string sSPGPRSDelete = "spProfileGPRSDelete";
        public static string sSPDBListGPRSBrowse = "spDBListGPRSBrowse";
        public static string sSPGPRSCopyListBrowse = "spGPRSListBrowse";
        public static string sSPProfileGPRSInsertByIdAndName = "spProfileGPRSInsertByIdAndName";
        #endregion

        #region "Currency"
        public static string sSPCurrencyListBrowse = "spProfileCurrencyListBrowse";
        public static string sSPCurrencyInsert = "spProfileCurrencyInsert";
        public static string sSPCurrencyBrowse = "spProfileCurrencyBrowse";
        public static string sSPCurrencyUpdate = "spProfileCurrencyUpdate";
        public static string sSPCurrencyDelete = "spProfileCurrencyDelete";
        #endregion

        #region "Software Package"
        public static string sSPAppPackageBrowse = "spAppPackageBrowse";
        public static string sSPAppPackageBrowseDetail = "spAppPackageBrowseDetail";
        public static string sSPAppPackageBrowseTerminalID = "spAppPackageBrowseTerminalID";
        public static string sSPAppPackageTerminalUpdate = "spAppPackageTerminalUpdate";
        public static string sSPAppPackageAdd = "spAppPackageAdd";
        public static string sSPAppPackageAddContent = "spAppPackageAddContent";
        public static string sSPAppPackageEdit = "spAppPackageEdit";
        public static string sSPAppPackageDelete = "spAppPackageDelete";
        public static string sSPAppPackageTempDelete = "AppPackageEDCListTempDelete";
        public static string sSPAppPackageEdcListBrowse = "spAppPackageEdcListBrowse";
        public static string sSPTerminalSNUpdatebyUpdateAppPackage = "spTerminalSNUpdatebyAppPackageUpdate";
        public static string sSPRDListTIDUpdatebyAppPackageUpdate = "spRDListTerminalUpdatebyAppPackageUpdate";
        #endregion

        #region "Image Package"
        public static string sSPImagePackageAddContent = "spImagePackageAddContent";
        public static string sSPImagePackageBrowseContent = "spImagePackageBrowseContent";
        public static string sSPImagePackageDeleteContent = "spImagePackageDeleteContent";
        #endregion

        #region "Schedule/Remote Download"
        public static string sSPScheduleSoftwareInsert = "spScheduleSoftwareInsert";
        public static string sSPScheduleSoftwareBrowse = "spScheduleSoftwareBrowse";

        public static string sSPScheduleSoftwareUpdate = "spScheduleSoftwareUpdate";
        //public static string sSPInsertScheduleID = "spProfileTerminalListInsertScheduleID";

        public static string sSPBrowseRegion = "spRDBrowseRegion";
        public static string sSPBrowseGroup = "spRDBrowseGroup";
        public static string sSPBrowseCity = "spRDBrowseCity";

        public static string sSPListTerminalIDbyGroup = "spRDBrowseListTerminalIDbyGroup";
        public static string sSPListTerminalIDbyRegion = "spRDBrowseListTerminalIDbyRegion";
        public static string sSPListTerminalIDbyCity = "spRDBrowseListTerminalIDbyCity";

        public static string sSPGetInfoRD = "spRDGetInfobyTID";
        public static string sSPRDInsertUpdateListTid = "spRDInsertProcessListTid";
        public static string sSPRemoveTIDGroupRegionCity = "spRDDeleteTIDGroupRegionCity";

        public static string sSPInsertGroupRegionCity = "spRDInsertGroupRegionCity";
        public static string sSPUpdateGroupRegionCity = "spRDUpdateGroupRegionCity";
        public static string sSPRemoveGroupRegionCity = "spRDRemoveGroupRegionCity";
        public static string sSPBrowseAllColoumLinkListTID = "spRDBrowseAllColoumLinkListTID";
        #endregion

        #region "Edc Type"
        public static string sSPEdcTypeBrowse = "spEdcTypeBrowse";
        public static string sSPEdcTypeAdd = "spEdcTypeAdd";
        public static string sSPEdcTypeRemove = "spEdcTypeDelete";
        #endregion

        #region "Email Notification"
        public static string sSPSendNotification = "spSendNotification";
        #endregion

        #region "Software
        public static string sSPSoftwareAvailableBrowse = "spProfileSoftwareBrowseAvailable";
        public static string sSPSoftwareRegisteredBrowse = "spProfileSoftwareBrowseRegistered";
        public static string sSPSoftwareInsert = "spProfileSoftwareInsert";
        public static string sSPSoftwareDelete = "spProfileSoftwareDelete";

        #endregion
        
        #region "Init Terminal"
        //public static string sSPInitTerminalInsert = "spInitTerminalInsert";
        public static string sSPInitTempClear = "spInitTempClear";
        public static string sSPInitUpdate = "spInitUpdate";
        #endregion

        #region "User Access"
        public static string sSPUserAccessInsert = "spUserAccessInsert";
        public static string sSPUserAccessDelete = "spUserAccessDelete";
        public static string sSPUserAccessBrowse = "spUserAccessBrowse";
        #endregion

        #region "User Group"
        public static string sSPUserGroupBrowse = "spUserGroupBrowse";
        public static string sSPItemDisabledBrowse = "spUserGroupItemDisabledBrowse";
        public static string sSPItemEnableBrowse = "spUserGroupItemEnableBrowse";
        public static string sSPUserGroupDelete = "spUserGroupDelete";
        #endregion

        #region "Group Package"
        public static string sSPSoftwareGroupAvailablePackageBrowse = "spSoftwareGroupAvailablePackageBrowse";
        public static string sSPSoftwareGroupPackageBrowse = "spSoftwareGroupPackageBrowse";
        public static string sSPSoftwareGroupPackageDelete = "spSoftwareGroupPackageDelete";
        public static string sSPSoftwareGroupPackageInsert = "spSoftwareGroupPackageInsert";

        #endregion

        #region "TID Software Package"
        public static string sSPSoftwareTerminalIDPackageBrowse = "spSoftwareTerminalIDPackageBrowse";
        public static string sSPListTerminalIDPackageBrowse = "spListTerminalIDPackageBrowse";
        public static string sSPListTerminalIDPackageInsert = "spListTerminalIDPackageInsert";
        public static string sSPListTerminalIDPackageDelete = "spListTerminalIDPackageDelete";
        #endregion

        #region "Download Software BTPN"
        public static string sSPSoftwareListGetData = "spSoftwareListGetData";
        public static string sSPSoftwareGetData = "spSoftwareGetData";
        public static string sSPTempSoftwareListUpdateDelete = "spTempSoftwareListUpdateDelete";
        #endregion

        #region "Move Data"
        public static string sSPMoveData = "spMoveData";
        public static string sSPMoveDataTag = "spMoveDataTag";
        #endregion

        #region "Terminal Group"
        public static string sSPGroupBrowse = "spGroupBrowse";
        public static string sSPGroupInsert = "spGroupInsert";
        public static string sSPGroupDelete = "spGroupDelete";
        public static string sSPGroupUpdate = "spGroupUpdate";
        public static string sSPGroupTerminalScheduleUpdate = "spGroupTerminalScheduleUpdate";
        //public static string sSPGroupUpdateScheduleID = "spGroupUpdateScheduleID";
        #endregion

        #region "Terminal Region"
        public static string sSPRegionBrowse = "spRegionBrowse";
        public static string sSPRegionInsert = "spRegionInsert";
        public static string sSPRegionDelete = "spRegionDelete";
        public static string sSPRegionUpdate = "spRegionUpdate";
        public static string sSPRegionTerminalScheduleUpdate = "spRegionTerminalScheduleUpdate";
        #endregion

        #region "Terminal SN"
        public static string sSPTerminalSNBrowse = "spTerminalSNBrowse";
        public static string sSPTerminalSNInsert = "spTerminalSNInsert";
        public static string sSPTerminalSNDelete = "spTerminalSNDelete";
        public static string sSPTerminalSNUpdate = "spTerminalSNUpdate";
        public static string sSPRegionUpdateScheduleID = "spRegionUpdateScheduleID";
        public static string sSPTerminalSNUpdateAppPackID = "spTerminalSNUpdateAppPackIDbyGroup";
        #endregion

        #region "Terminal Brand"
        public static string sSPTerminalBrandBrowse = "spTerminalBrandBrowse";
        public static string sSPTerminalBrandInsert = "spTerminalBrandInsert";
        public static string sSPTerminalBrandDelete = "spTerminalBrandDelete";
        #endregion

        #region "Terminal Type"
        public static string sSPTerminalTypeBrowse = "spTerminalTypeBrowse";
        public static string sSPTerminalTypeInsert = "spTerminalTypeInsert";
        public static string sSPTerminalTypeDelete = "spTerminalTypeDelete";
        #endregion

        #region "PinPad"
        public static string sSPPinPadListBrowse = "spProfilePinPadListBrowse";
        public static string sSPPinPadBrowse = "spProfilePinPadBrowse";
        public static string sSPPinPadInsert = "spProfilePinPadInsert";
        public static string sSPPinPadDelete = "spProfilePinPadDelete";
        public static string sSPPinPadUpdate = "spProfilePinPadUpdate";
        #endregion

        #region "Remote Download"
        public static string sSPRemoteDownloadListBrowse = "spProfileRemoteDownloadListBrowse";
        public static string sSPRemoteDownloadInsert = "spProfileRemoteDownloadInsert";
        public static string sSPRemoteDownloadBrowse = "spProfileRemoteDownloadBrowse";
        public static string sSPRemoteDownloadDelete = "spProfileRemoteDownloadDelete";
        public static string sSPRemoteDownloadUpdate = "spProfileRemoteDownloadUpdate";
        public static string spDBListRDBrowse = "spDBListRDBrowse";
        public static string sSPProfileRDCopyListBrowse = "spProfileRDListBrowse";
        public static string sSPProfileRemoteDownloadCopy = "spProfileRemoteDownloadCopybyDBID";
        public static string sSPRemoteDownloadManagementCopy = "spProfileRemoteDownloadManagementCopy";
        public static string sSPIsValidTerminalID = "spIsValidTerminalID";
        public static string sSPIsRegisterRemoteDownload = "spIsRegisterRemoteDownload";
        public static string sSPIsRemoteDownloadAllowed = "spIsRemoteDownloadAllowed";
        public static string sSPAuditInitSoftwareInsert = "spAuditInitSoftwareInsert";
        public static string sSPsGetDownloadApplicationName = "spsGetDownloadApplicationName";
        public static string sSPCheckCrc = "spCheckCrc";
        public static string sSPSoftwareProgressInsert = "spSoftwareProgressInsert";
        public static string sSPSoftwareProgressUpdate = "spSoftwareProgressUpdate";
        #endregion

        #region "InSysConsole"
        public static string sSPFlagControlAllowInit { get { return "spControlFlagAllowInitBrowse"; } }
        public static string sSPProcessMessage { get { return "spOtherParseProcess"; } }
        public static string sSPPortInit { get { return "spOtherParsePortInit"; } }
        public static string sSPInitGetTableInit { get { return "spInitGetTableInit"; } }
        public static string sSPInitLogConnNewInit { get { return "spInitLogConnNewInit"; } }
        public static string sSPAuditInitInsert { get { return "spAuditInitInsert"; } }
        public static string sSPInitLogConnNewInitConsole { get { return "spInitLogConnNewInitConsole"; } }
        public static string sSPProfileTextFullTable { get { return "spProfileTextFullTable"; } }
        public static string sSPProfileTextFullTableDLProfile { get { return "spProfileTextFullTableDLProfile"; } }
        public static string sSPProfileTextFullTableFooter { get { return "spProfileTextFullTableFooter"; } }
        public static string sSPBitmapProfileSoftwareBrowse { get { return "spBitMapProfileSoftwareBrowse"; } }
        public static string sSPBitmapBrowseCustom48 { get { return "spBitmapBrowseCustom48"; } }
        public static string spBrowseInitBySerialNumber { get { return "spBrowseInitBySerialNumber"; } }
        public static string spGetTidBySerialNumber { get { return "spGetTidBySerialNumber"; } }
        public static string sSPAutoInitAllow { get { return "spAutoInitAllow"; } }
        public static string sSPConsoleLogInsert { get { return "spConsoleLogInsert"; } }
        #endregion

        #region "InSysConsoleSoftware"
        public static string sSPDownloadProcessMessage = "spOtherParseDownloadProcess";
        public static string sSPProfileRemoteDownloadData = "spProfileRemoteDownloadData";
        public static string sSPCheckDataValid = "spCheckRemoteDownloadContentValid";
        //NEW
        public static string sSPProfileListTerminalID = "spProfileListTerminalID";
        public static string sSPGetDataSoftware = "spGetDataSoftware";
        #endregion

        #region "Init Image"
        public static string sSPImageGetData = "spImageGetData";
        public static string sSPGetPrintFileName = "spImageGetPrintFileName";
        public static string sSPGetIdleFileName = "spImageGetIdleFileName";
        #endregion

        #region "BCA_QC"
        public static string sSPQueryReport { get { return "spQueryReport"; } }
        public static string sSPCompareData { get { return "spReportCompare"; } }
        public static string sSPReportReplaceTID { get { return "spReportReplaceTID"; } }
        public static string sSPCleanTableCompare { get { return "spTempXLSClean"; } }
        public static string sSPCleanTableReplace { get { return "spTempExcelClean"; } }
        public static string sSPXmlGetMapColumn { get { return "spXMLGetMapColumn"; } }
        #endregion

        #region "BankCode"
        public static string sSPProfileBankCodeListBrowse = "spProfileBankCodeListBrowse";
        public static string sSPProfileBankCodeBrowse = "spProfileBankCodeBrowse";
        public static string sSPProfileBankCodeInsert = "spProfileBankCodeInsert";
        public static string sSPProfileBankCodeDelete = "spProfileBankCodeDelete";
        public static string sSPProfileBankCodeUpdate = "spProfileBankCodeUpdate";
        #endregion

        #region "ProductCode"
        public static string sSPProfileProductCodeListBrowse = "spProfileProductCodeListBrowse";
        public static string sSPProfileProductCodeBrowse = "spProfileProductCodeBrowse";
        public static string sSPProfileProductCodeInsert = "spProfileProductCodeInsert";
        public static string sSPProfileProductCodeDelete = "spProfileProductCodeDelete";
        public static string sSPProfileProductCodeUpdate = "spProfileProductCodeUpdate";
        #endregion

        #region "EMV Management"
        public static string sSPProfileEMVManagementListBrowse = "spProfileEMVManagementListBrowse";
        public static string sSPProfileEMVManagementBrowse = "spProfileEMVManagementBrowse";
        public static string sSPProfileEMVManagementInsert = "spProfileEMVManagementInsert";
        public static string sSPProfileEMVManagementUpdate = "spProfileEMVManagementUpdate";
        public static string sSPProfileEMVManagementDelete = "spProfileEMVManagementDelete";
        public static string sSPpProfileEMVManagementCopy = "spProfileEMVManagementCopy";
        #endregion

        #region "Custom Menu Management"
        public static string sSPProfileCustomMenuInsert = "spProfileCustomMenuInsert";
        public static string sSPProfileCustomMenuDelete = "spProfileCustomMenuDelete";
        public static string sSPProfileCustomMenuBrowse = "spProfileCustomMenuBrowse";
        public static string sSPProfileCustomMenuUpdate = "spProfileCustomMenuUpdate";

        public static string sSPCustomMenuSchemaBrowse = "spCustomMenuSchemaBrowse";
        #endregion

        #region "Menu User Management"
        public static string sSPMenuNewInsysBrowse = "spMenuNewInsysBrowse";
        #endregion

        #region "Promo Management"
        public static string sSPPromoManagementBrowseList = "spProfilePromoManagementBrowseList";
        public static string sSPPromoManagementBrowse = "spProfilePromoManagementBrowse";
        public static string sSPPromoManagementInsert = "spProfilePromoManagementInsert";
        public static string sSPPromoManagementDelete = "spProfilePromoManagementDelete";
        public static string sSPPromoManagementUpdate = "spProfilePromoManagementUpdate";
        public static string sSPPromoManagementCopy = "spProfilePromoManagementCopy";
        public static string sSPPromoManagementCopyListBrowse = "spProfilePromoManagementListBrowse";
        public static string spDBListPromoManagementBrowse = "spDBListPromoManagementBrowse";
        public static string spDBListPromoManagementListBrowse = "spProfilePromoManagementListBrowse";
        public static string sSPPromoManagementCopyByDbId = "spProfilePromoManagementCopybyDBID";
        #endregion

        #region "Initial Flazz Management"
        public static string sSPInitialFlazzManagementBrowseList = "spProfileInitialFlazzManagementBrowseList";
        public static string sSPInitialFlazzManagementBrowse = "spProfileInitialFlazzManagementBrowse";
        public static string sSPInitialFlazzManagementInsert = "spProfileInitialFlazzInsert";
        public static string sSPInitialFlazzManagementDelete = "spProfileInitialFlazzDelete";
        public static string sSPInitialFlazzManagementUpdate = "spProfileInitialFlazzUpdate";
        public static string sSPInitialFlazzManagementCopy = "spProfileInitialFlazzCopy";
        public static string sSPInitialFlazzManagementCopyListBrowse = "spProfileInitialFlazzListBrowse";
        //public static string spDBListInitialFlazzManagementBrowse = "spDBListInitialFlazzManagementBrowse";
        public static string spDBListInitialFlazzManagementListBrowse = "spProfileInitialFlazzListBrowse";
        public static string sSPInitialFlazzManagementCopyByDbId = "spProfileInitialFlazzCopybyDBID";
        #endregion

        #region "Card Type Management"
        public static string sSPCardTypeBrowseList = "spProfileCardTypeBrowseList";
        public static string sSPCardTypeBrowse = "spProfileCardTypeBrowse";
        public static string sSPCardTypeInsert = "spProfileCardTypeInsert";
        public static string sSPCardTypeDelete = "spProfileCardTypeDelete";
        public static string sSPCardTypeUpdate = "spProfileCardTypeUpdate";
        #endregion

        #region "Edc Monitoring Management"
        public static string sSPProfileEdcMonitorBrowseList = "spProfileEdcMonitorListBrowse";
        public static string sSPProfileEdcMonitorBrowse = "spProfileEdcMonitorBrowse";
        public static string sSPProfileEdcMonitorInsert = "spProfileEdcMonitorInsert";
        public static string sSPProfileEdcMonitorDelete = "spProfileEdcMonitorDelete";
        public static string sSPProfileEdcMonitorUpdate = "spProfileEdcMonitorUpdate";
        #endregion

        #region "Edc Monitoring"
        public static string sSPEdcMonitorBrowse = "spEdcMonitorBrowse";
        public static string sSPHeartBeatrBrowse = "spHeartBeatBrowse";
        public static string sSPHeartBeatrBrowseByDate = "spHeartBeatBrowseByDate";
        public static string sSPEdcHeartBeatGraph1 = "spEdcHeartBeatGraph1";
        public static string sSPEdcMonitorBrowseDetailTrx = "spEdcMonitorBrowseDetailTrx";
        public static string sSPEdcMonitorBrowseSIM = "spEdcMonitorBrowseSIM";
        public static string sSPEdcMonitorGraph1 = "spEdcMonitorGraph1";
        public static string sSPEdcMonitorGraph2 = "spEdcMonitorGraph2";
        public static string sSPEdcMonitorGraph3 = "spEdcMonitorGraph3";
        #endregion

        #region "Request Paper Receipt"
        public static string sSPRequestPaperReceiptBrowseList = "spProfileRequestPaperReceiptListBrowse";
        public static string sSPRequestPaperReceiptInsert = "spProfileRequestPaperReceiptInsert";
        public static string sSPRequestPaperReceiptBrowse = "spProfileRequestPaperReceiptBrowse";
        public static string sSPRequestPaperReceiptDelete = "spProfileRequestPaperReceiptDelete";
        public static string sSPRequestPaperReceiptUpdate = "spProfileRequestPaperReceiptUpdate";
        public static string sSPRequestPaperReceiptVIew = "spRequestPaperReceiptView";

        #endregion

        #region "History"
        public static string sSPProfileTerminalHistoryBrowse = "spProfileTerminalHistoryBrowse";
        public static string sSPProfileAcquirerHistoryBrowse = "spProfileAcquirerHistoryBrowse";
        public static string sSPProfileIssuerHistoryBrowse = "spProfileIssuerHistoryBrowse";
        public static string sSPProfileRelationHistoryBrowse = "spProfileRelationHistoryBrowse";

        public static string sSPProfileTerminalHistoryRestore = "spProfileTerminalHistoryRestore";
        public static string sSPProfileAcquirerHistoryRestore = "spProfileAcquirerHistoryRestore";
        public static string sSPProfileIssuerHistoryRestore = "spProfileIssuerHistoryRestore";
        public static string sSPProfileRelationHistoryRestore = "spProfileRelationHistoryRestore";
        #endregion

        #region  "InsysConsoleRequestPaper
        public static string sSPIsValidStatusReceiptPaper = "IsValidStatusReceiptPaper";
        public static string sSPInsertOrUpdateRequestPaper = "spProfileRequestOrReceiptPaperInsert";
        public static string spProfileRequestOrReceiptPaperDelete = "spProfileRequestOrReceiptPaperDelete";
        #endregion

        #region "BNI Special purpose"
        public static string sSPGetTidAcquirer = "spGetTidAcquirer";
        #endregion

        #region  "InSys Tools"
        public static string sSPReportLogInsert = "spReplaceTIDLogInsert";
        public static string sSPReplaceTIDBCA = "spReplaceTIDBCA";
        public static string sSPReportLogBrowse = "spReplaceTIDLogBrowse";
        public static string sSPToolsAllowToggling = "spToolsAllowToggling";
        #endregion

        #region "SN Management"
        public static string sSPProfileSNBrowseList = "spProfileSNBrowseList";
        public static string sSPProfileSNBrowse = "spProfileSNBrowse";
        public static string sSPProfileSNInsert = "spProfileSNInsert";
        public static string sSPProfileSNDelete = "spProfileSNDelete";
        #endregion

        #region "EMS"
        public static string sSPEmsMsGetDefaultAttribute = "spEmsMsGetDefaultAttribute";
        #endregion

        #region "RD FTPS"
        public static string sSPRD_SwVersionBrowse = "spRD_SwVersionBrowse";
        public static string sSPRD_SwVersionInsert = "spRD_SwVersionInsert";
        public static string sSPRD_SwVersionListInsert = "spRD_SwVersionListInsert";
        public static string sSPRD_SwVersionEnableDisable = "spRD_SwVersionEnableDisable";
        public static string sSPRD_SwVersionDelete = "spRD_SwVersionDelete";
        public static string sSPRD_SwVersionUpdate = "spRD_SwVersionUpdate";
        public static string sSPRD_SwVersionListBrowse = "spRD_SwVersionListBrowse";
        public static string sSPRD_GetMID = "spRD_GetMID";
        public static string sSPRD_GroupBrowse = "spRD_GroupBrowse";
        public static string sSPRD_GroupInsert = "spRD_GroupInsert";
        public static string sSPRD_GroupEnableDisable = "spRD_GroupEnableDisable";
        public static string sSPRD_GroupDelete = "spRD_GroupDelete";
        public static string sSPRD_GroupDetailBrowse = "spRD_GroupDetailBrowse";
        public static string sSPRD_GroupDetailInsert = "spRD_GroupDetailInsert";
        public static string sSPRD_GroupDetailInsertTerminal = "spRD_GroupDetailInsertTerminal";
        public static string sSPRD_AuditUploadTerminalInsert = "spRD_AuditUploadTerminalInsert";
        public static string sSPRD_AuditUploadTerminalBrowse = "spRD_AuditUploadTerminalBrowse";
        public static string sSPRD_AuditDownloadInsert = "spRD_AuditDownloadInsert";
        public static string sSPRD_AuditDownloadBrowse = "spRD_AuditDownloadBrowse";
        #endregion

        #region "Deprecated Procedure"
        #region "Synchronize To EMS"
        public static string sSPXmlSyncEms = "spXmlSyncEms";
        #endregion
        #endregion

    }
}