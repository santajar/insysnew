using System.Data.OleDb;

namespace InSysClass
{
    public class OleDBConnLib
    {
        //Conn string : "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\mydatabase.mdb;Jet OLEDB:Database Password=MyDbPassword;"
        //Conn string : "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\mydatabase.mdb;User Id=admin;Password=;"

        public static string sConn(string sDatabase, string sPwd)
        {
            string sConn = null;
            sConn += "Provider=Microsoft.Jet.OLEDB.4.0;";
            sConn += "Data Source=" + sDatabase + ";";
            sConn += sDatabase.Contains(".mdb") ?
                "Jet OLEDB:Database Password=" + sPwd + ";" :
                "Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";
            return sConn;
        }

        public static string sConn(string sDatabase, string sUser, string sPwd)
        {
            string sConn = null;
            sConn += "Provider=Microsoft.Jet.OLEDB.4.0;";
            sConn += "Data Source=" + sDatabase + ";";
            sConn += "User Id=" + sUser + ";";
            sConn += "Password=" + sPwd + ";";
            return sConn;
        }

        public static string sAccesConn(string sDatabase)
        {
            string sConn = null;
            sConn += "Provider=Microsoft.ACE.OLEDB.12.0;";
            sConn += "Data Source=" + sDatabase + ";";
            return sConn;
        }

        public static string sConnACE12(string sDatabase, string sPwd)
        {
            string sConn = null;
            sConn += "Provider=Microsoft.ACE.OLEDB.12.0;";
            sConn += "Data Source=" + sDatabase + ";";
            sConn += sDatabase.Contains(".mdb") ?
                "Jet OLEDB:Database Password=" + sPwd + ";" :
                "Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\";";
            return sConn;
        }

        public static string sConnACE12(string sDatabase, string sUser, string sPwd)
        {
            string sConn = null;
            sConn += "Provider=Microsoft.ACE.OLEDB.12.0;";
            sConn += "Data Source=" + sDatabase + ";";
            sConn += "User Id=" + sUser + ";";
            sConn += "Password=" + sPwd + ";";
            return sConn;
        }

        public static string sConnACE14(string sDatabase, string sPwd)
        {
            string sConn = null;
            sConn += "Provider=Microsoft.ACE.OLEDB.14.0;";
            sConn += "Data Source=" + sDatabase + ";";
            sConn += sDatabase.Contains(".mdb") ?
                "Jet OLEDB:Database Password=" + sPwd + ";" :
                "Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\";";
            return sConn;
        }

        public static OleDbConnection EstablishConnection(string sConnString)
        {
            OleDbConnection oConn = new OleDbConnection(sConnString);
            oConn.Open();
            return oConn;
        }
    }
}