﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace InSysClass
{
    public class ClassSqlBulkUpSert
    {
        protected static SqlConnection oSqlConn;
        protected static DataTable dtSource;
        protected static DataTable dtUpsert;
        protected static string[] arrsColumnUpdate;
        protected static string[] arrsColumnCondition;
        protected static string sTempId;

        public static bool isSqlBulkUpsertSuccess(
            SqlConnection _oSqlConn,
            DataTable _dtSource, 
            DataTable _dtUpsert, 
            string[] _arrsColumnUpdate,
            string _sTransactionName,
            string _sTempId,
            ref string _sError)
        {
            InitParams(_oSqlConn, _dtSource, _dtUpsert, _arrsColumnUpdate, _sTempId);
            SqlTransaction oTrans = oSqlConn.BeginTransaction(_sTransactionName);
            try
            {
                CreateTempTable(oTrans);
                BulkInsertTempTable(oTrans);
                MergeTempTable(oTrans);
                DropTempTable(oTrans);
            }
            catch (Exception ex)
            {
                _sError = ex.Message;
                oTrans.Rollback();
                return false;
            }
            oTrans.Commit();
            return true;
        }

        protected static void InitParams(SqlConnection _oSqlConn,
            DataTable _dtSource,
            DataTable _dtUpsert,
            string[] _arrsColumnUpdate,
            string _sTempId)
        {
            oSqlConn = _oSqlConn;
            dtSource = new DataTable();
            dtSource = _dtSource;
            dtUpsert = new DataTable();
            dtUpsert = _dtUpsert;
            arrsColumnUpdate = new string[_arrsColumnUpdate.Length];
            arrsColumnUpdate = _arrsColumnUpdate;
            arrsColumnCondition = new string[dtSource.Columns.Count - arrsColumnUpdate.Length];
            int iCount = 0;
            foreach(DataColumn col in dtSource.Columns )
                if (Array.IndexOf(arrsColumnUpdate, col.ColumnName) == -1)
                {
                    arrsColumnCondition[iCount] = col.ColumnName;
                    iCount++;
                }
            sTempId = string.Format("{0}{1:hhmmssfff}", _sTempId, DateTime.Now);
        }

        protected static void CreateTempTable(SqlTransaction _oTrans)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            string sQuery = string.Format("SELECT * INTO {0}{1} FROM {0}\nDELETE FROM {0}{1}", 
                dtSource.TableName, sTempId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn, _oTrans))
                oCmd.ExecuteNonQuery();
        }

        protected static void BulkInsertTempTable(SqlTransaction _oTrans)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            SqlBulkCopy sqlBulk = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.Default, _oTrans);
            sqlBulk.DestinationTableName = string.Format("{0}{1}", dtSource.TableName, sTempId);
            foreach (DataColumn col in dtSource.Columns)
            {
                SqlBulkCopyColumnMapping colmapSource = new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName);
                sqlBulk.ColumnMappings.Add(colmapSource);
            }
            sqlBulk.WriteToServer(dtUpsert);
        }

        protected static void MergeTempTable(SqlTransaction _oTrans)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            string sCondition = null;
            foreach (string sCol in arrsColumnCondition)
                sCondition = string.IsNullOrEmpty(sCondition) ?
                    string.Format("Target.[{0}]=Source.[{0}] ", sCol) :
                    string.Format("{0}AND Target.[{1}] = Source.[{1}] ", sCondition, sCol);
            string sUpdate = null; 
            foreach (string sCol in arrsColumnUpdate)
                sUpdate = string.IsNullOrEmpty(sUpdate) ?
                    string.Format("Target.[{0}]=Source.[{0}]", sCol) :
                    string.Format("{0}, Target.[{1}] = Source.[{1}] ", sUpdate, sCol);
            string sColumns = null;
            foreach(DataColumn col in dtSource.Columns)
                sColumns = string.IsNullOrEmpty(sColumns) ?
                    string.Format("[{0}]", col.ColumnName) :
                    string.Format("{0}, [{1}]", sColumns, col.ColumnName);
            string sColumnsSource = null;
            foreach (DataColumn col in dtSource.Columns)
                sColumnsSource = string.IsNullOrEmpty(sColumnsSource) ?
                    string.Format("Source.[{0}]", col.ColumnName) :
                    string.Format("{0}, Source.[{1}]", sColumnsSource, col.ColumnName);
            string sMergeSql = string.Format(
                "MERGE INTO {0} AS Target " +
                "USING {0}{5} AS Source " +
                "ON {1}" +
                "WHEN MATCHED THEN " +
                "UPDATE SET {2} " +
                "WHEN NOT MATCHED THEN " +
                "INSERT ({3}) VALUES ({4});", 
                dtSource.TableName,
                sCondition,
                sUpdate,
                sColumns,
                sColumnsSource,
                sTempId);
            using (SqlCommand oCmd = new SqlCommand(sMergeSql, oSqlConn, _oTrans))
                oCmd.ExecuteNonQuery();
        }

        protected static void DropTempTable(SqlTransaction _oTrans)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            string sQuery = string.Format("DROP TABLE {0}{1}",
                   dtSource.TableName, sTempId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn, _oTrans))
                oCmd.ExecuteNonQuery();
        }
    }
}