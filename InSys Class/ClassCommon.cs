using System;
using System.Text;
using System.IO;
using System.Globalization;
using System.Net;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Data;

namespace InSysClass
{
    public class CommonLib
    {        
        public static CultureInfo ciUSFormat = new CultureInfo("en-US"); // English - United States
        public static CultureInfo ciIDFormat = new CultureInfo("id-ID"); // Indonesian - Indonesia

        #region "File Function"

        /// <summary>
        /// Write Value in string format to text file
        /// </summary>
        /// <param name="sFileName"> string : File name including file path which will be written</param>
        /// <param name="sValue">string : Value to be written to the file</param>
        /// <param name="bNewLine">bool : Value to be using new line on the file</param>
        public static void Write2File(string sFileName, string sValue, bool bNewLine)
        {
            //FileStream oFileStream;
            //StreamWriter oStreamWriter;
            try
            {
                using (FileStream oFileStream = new FileStream(sFileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter oStreamWriter = new StreamWriter(oFileStream))
                    {
                        oStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        if (bNewLine) oStreamWriter.WriteLine(sValue);
                        else oStreamWriter.Write(sValue);
                    }
                    //oStreamWriter.Close();
                    //oStreamWriter.Dispose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //oFileStream.Close();
            //oFileStream.Dispose();
        }

        /// <summary>
        /// Write Value in byte array value to text file
        /// </summary>
        /// <param name="sFileName">string : File name including file path which will be written</param>
        /// <param name="arrbValue">byte array : alue to be written to the file</param>
        public static void Write2File(string sFileName, byte[] arrbValue)
        {
            try
            {
                using (FileStream fs = new FileStream(sFileName, FileMode.Append, FileAccess.Write))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        bw.Seek(0, SeekOrigin.End);
                        bw.Write(arrbValue);
                    }
                    //bw.Close();
                }
                //fs.Close();
                //fs.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Determine if the file requested exist   
        /// </summary>
        /// <param name="sFileName">string : File name including file path which will be searched</param>
        /// <returns>bool : true if file is found, false if not</returns>
        public static bool isFileExist(string sFileName)
        {
            if (File.Exists(sFileName))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Read the file requested and return its content in string.
        /// If file not found, return null
        /// </summary>
        /// <param name="sFileName">string : File name including file path which will be read</param>
        /// <returns>string : content of the file</returns>
        public static string sReadTxtFile(string sFileName)
        {
            string sValue= null;
            try
            {
                if (isFileExist(sFileName))
                {
                    FileStream oFileStream = new FileStream(sFileName, FileMode.Open);
                    StreamReader oStreamReader = new StreamReader(oFileStream);
                    sValue = oStreamReader.ReadToEnd();

                    oStreamReader.Close();
                    oStreamReader.Dispose();
                    oFileStream.Close();
                    oFileStream.Dispose();
                }
                else
                    sValue = null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return sValue;
        }

        /// <summary>
        /// Read the file requested and return its content in array of byte.
        /// If file not found, return null
        /// </summary>
        /// <param name="sFilename">string : File name including file path which will be read</param>
        /// <returns>byte array : content of the file</returns>
        public static byte[] arrbReadTxtFile(string sFilename)
        {
            FileStream fs = new FileStream(sFilename, FileMode.Open);
            BinaryReader br = new BinaryReader(fs);
            byte[] arrbReturn = new byte[br.BaseStream.Length];
            try
            {
                for (int i = 0; i < (int)br.BaseStream.Length; i++) // Set the content of the file into array of byte
                {
                    //Console.Write("{0:X2}", br.ReadByte());
                    arrbReturn[i] = br.ReadByte();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            br.Close();
            return arrbReturn;
        }

        /// <summary>
        /// Create log file and write its content to log file.
        /// File name format : Log_yyyyMMdd.txt
        /// ContentFormat : datetime + LogContent
        /// </summary>
        /// <param name="sDirectory">string : the directory of which the file is stored</param>
        /// <param name="sText">string : the content to be written into the file</param>
        public static void Log(string sDirectory, string sText)
        {
            string sFileLog = sDirectory + "\\Log_" + sGetyyyyMMdd() + ".log"; // Set the directory
            string sTemp = sGetMMMddyyyyhhmmsstt() + " " + sText; // Set the content of the file
            Write2File(sFileLog, sTemp, true);
        }
        #endregion

        #region "Converter"

        /// <summary>
        /// Convert string type into integer type.
        /// If conversion failed, will return "0".
        /// </summary>
        /// <param name="sValue">string : value in string type that will be converted</param>
        /// <returns>int : value in integer type after converted</returns>
        public static int iStringToInt(string sValue)
        {
            /// Convert integer string to integer, ex : "1" to 1
            try
            {
                return int.Parse(sValue);
            }
            catch // if conversion failed
            {
                return 0;
            }
        }

        public static int iStringToInt(string sValue, int iDefValue)
        {
            /// Convert integer string to integer, ex : "1" to 1
            try
            {
                return int.Parse(sValue);
            }
            catch
            {
                return 1000;
            }
        }

        /// <summary>
        /// Convert hexnumber in string type into integer.
        /// </summary>
        /// <param name="sHex">string : HexNumber that will be converted</param>
        /// <returns>int : the result of the conversion</returns>
        public static int iHexStringToInt(string sHex)
        {
            sHex = sHex.Replace(" ", "");
            int iValue = int.Parse(sHex, NumberStyles.HexNumber);
            return iValue;
        }

        /// <summary>
        /// Convert boolean type into integer type.
        /// If true return "1", else return "0".
        /// </summary>
        /// <param name="bTemp">bool : bool value to be converted</param>
        /// <returns>int : the result of boolean conversion</returns>
        public static int iBoolToInt(bool bTemp)
        {
            int iTemp = 0;
            if (bTemp) iTemp = 1; // If true
            return iTemp;
        }

        /// <summary>
        /// Convert string value to float type.
        /// If failed, return the default value.
        /// </summary>
        /// <param name="sValue">string : value in string type that will be converted</param>
        /// <param name="fDefValue">fload : the default value returned if conversion failed</param>
        /// <returns>float : the conversion result from sValue</returns>
        public static float fStringToFloat(string sValue, float fDefValue)
        {
            try
            {
                return float.Parse(sValue);
            }
            catch // If failed
            {
                return fDefValue;
            }
        }

        /// <summary>
        /// Convert HexString to Decimal.
        /// </summary>
        /// <param name="sValue"> string :  HexNumber that will be converted </param>
        /// <returns>string : Conversion's result from HexNumber in Decimal</returns>
        public static string sConvertHextoDec(string sValue)
        {
            //string sResultValue = Convert.ToString(Int32.Parse(sValue, NumberStyles.HexNumber));
            string sResultValue = Convert.ToString(int.Parse(sValue, NumberStyles.HexNumber));
            return "0000".Substring(0, 4 - sResultValue.Length) + sResultValue;
        }

        /// <summary>
        /// Convert integer value into Hex Number in string format.
        /// The result conversion lengt must even, if not add "0" at the beginning.
        /// </summary>
        /// <param name="iValue">int : the value to be converted</param>
        /// <returns>string : the result of the conversion</returns>
        public static string sIntToHexString(int iValue)
        {
            string sHexValue = iValue.ToString("X");
            if (sHexValue.Length % 2 == 1) // If sHexValue.Lengt is odd
                sHexValue = "0" + sHexValue; 
            return sHexValue;
        }

        /// <summary>
        /// Convert an array of byte into a Hex String in uppercase.
        /// </summary>
        /// <param name="sarrbData">byte array : the array of byte that will be converted</param>
        /// <returns>string : HexString resulted from the conversion</returns>
        public static string sByteArrayToHexString(byte[] sarrbData)
        {
            StringBuilder sb = new StringBuilder(sarrbData.Length * 3);
            foreach (byte b in sarrbData) // Convert each byte in the array
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0').PadRight(3, ' ')); // Joining the result of conversion
            return sb.ToString().ToUpper();
        }

        public static string sByteArrayToHexString(byte[] sarrbData, int iLength)
        {
            StringBuilder sb = new StringBuilder(sarrbData.Length * 3);
            foreach (byte b in sarrbData)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0').PadRight(3, ' '));
            return sLeft(sb.ToString().ToUpper(), iLength * 3);
        }

        /// <summary>
        /// Convert a HexString into an array of byte.
        /// </summary>
        /// <param name="sValue">string : HexString to be converted</param>
        /// <returns>byte array : result of the conversion</returns>
        public static byte[] HexStringToByteArray(string sValue)
        {
            sValue = sValue.Replace(" ", ""); // Remove all white space
            byte[] arrbbuffer = new byte[sValue.Length / 2];
            for (int iCount = 0; iCount < sValue.Length; iCount += 2)
                arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sValue.Substring(iCount, 2), 16);
            return arrbbuffer;
        }

        /// <summary>
        /// Convert a byte array into string
        /// </summary>
        /// <param name="sarrbData">byte array : the value for conversion</param>
        /// <returns>string : result of the conversion</returns>
        public static string sByteArrayToString(byte[] sarrbData)
        {
            /// Convert Byte Array to String
            string sResult = null;
            for (int iCount = 0; iCount < sarrbData.Length; iCount++)
                sResult = sResult + sarrbData[iCount].ToString("X2");
            return sResult;
        }

        /// <summary>
        /// Convert a byte array into string for a specific length.
        /// </summary>
        /// <param name="sarrbData">byte array : the value for conversion</param>
        /// <param name="iLength">int : length of array that will be converted</param>
        /// <returns>string : result of the conversion</returns>
        public static string sByteArrayToString(byte[] sarrbData, int iLength)
        {
            /// Convert Byte Array to String
            string sResult = null;
            for (int iCount = 0; iCount < iLength; iCount++)
                sResult = sResult + sarrbData[iCount].ToString("X2");
            return sResult;
        }

        /// <summary>
        /// Convert byte array into Char String
        /// </summary>
        /// <param name="sarrbData">byte array : the value for conversion</param>
        /// <returns>string : result of the conversion</returns>
        public static string ByteArrayToCharString(byte[] sarrbData)
        {
            return Encoding.UTF8.GetString(sarrbData);
        }

        /// <summary>
        /// Convert string into a byte array
        /// </summary>
        /// <param name="sValue">string : the string value that will be converted</param>
        /// <returns>byte array : result of the conversion in a byte array</returns>
        public static byte[] arrbStringtoByteArray(string sValue)
        {
            /// Convert String to Byte Array
            int iLenString = sValue.Length;
            byte[] arrbBuffer = new byte[iLenString];

            for (int iCount = 0; iCount < iLenString; iCount++)
            {
                arrbBuffer[iCount] = Convert.ToByte(Convert.ToInt32(Convert.ToChar(sValue.Substring(iCount, 1))));
            }
            return arrbBuffer;
        }

        /// <summary>
        /// Convert decimal value into Hex value
        /// </summary>
        /// <param name="iDecValue">int : the decimal value int integer format</param>
        /// <returns>string : The Hex value in string format, result of the conversion</returns>
        public static string sConvertDecToHex(int iDecValue)
        {
            string sHexValue;
            return sHexValue = iDecValue.ToString("X");
        }

        /// <summary>
        /// Get the characters starting from left with specific length
        /// </summary>
        /// <param name="param">string : the value that will be used</param>
        /// <param name="length">int : length of characters that will be returned</param>
        /// <returns>string : the new value from the changes </returns>
        public static string sLeft(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Get the characters starting from right with specific length
        /// </summary>
        /// <param name="sParam">string : the value that will be used</param>
        /// <param name="iLength">int : length of characters that will be returned</param>
        /// <returns>string : the new value from the changes </returns>
        public static string sRight(string sParam, int iLength)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = sParam.Substring(sParam.Length - iLength, iLength);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Cut a string, from a specific start with a specific length
        /// </summary>
        /// <param name="sParam">string : the value that will be cut</param>
        /// <param name="iStartIndex">int : the starting index for the cut</param>
        /// <param name="iLength">int : length of characters that will be taken</param>
        /// <returns>string : result of the cut</returns>
        public static string sMid(string sParam, int iStartIndex, int iLength)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = sParam.Substring(iStartIndex, iLength);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Cut a string, from a specific start until the end of the string
        /// </summary>
        /// <param name="sParam">string : the value that will be cut</param>
        /// <param name="iStartIndex">int : the starting index for the cut</param>
        /// <returns></returns>
        public static string sMid(string sParam, int iStartIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = sParam.Substring(iStartIndex);
            //return the result of the operation
            return result;
        }

        /// <summary>
        /// Convert string value to Hex value
        /// </summary>
        /// <param name="asciiString">string : the ascii string that will be converted</param>
        /// <returns>string : hex value in string format, result of the conversion</returns>
        public static string sStringToHex(string asciiString)
        {
            string sHex = "";
            char[] arrcTemp = new char[asciiString.Length];
            arrcTemp = asciiString.ToCharArray();
            foreach (char c in asciiString) // Converting each char in assciiString
            //foreach (char c in arrcTemp) // Converting each char in assciiString
            {
                try
                {
                    int tmp = c;
                    sHex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
                    //sHex += string.Format("{0:x2}", tmp.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message.ToString());
                }
            }
            return sHex;
        }

        /// <summary>
        /// Converting string  into binary value in string format
        /// </summary>
        /// <param name="sValue">string : the value to be converted</param>
        /// <returns>string : result of the conversion, binary value in string format</returns>
        public static string sStringToBinStr(string sValue)
        {
            string sBinary = null;
            sBinary = sHexStrToBinStr(sStringToHex(sValue));
            return sBinary;
        }

        /// <summary>
        /// Convert a Hex value in string format into binary value in string format
        /// </summary>
        /// <param name="sValue">string : the hex value that will be converted</param>
        /// <returns>string : binary value resulted from the conversion in string format</returns>
        protected static string sHexStrToBinStr(string sValue)
        {
            string sBinary = null;
            sBinary = Convert.ToString(Convert.ToInt16(sValue, 16), 2);
            if (sBinary.Length < 8) sBinary = sBinary.PadLeft(8, '0');
            return sBinary;
        }

        /// <summary>
        /// Convert binary value to Hex value
        /// </summary>
        /// <param name="sBinString">string : the binary value for the conversion</param>
        /// <returns>string : Hex value from binary conversion in string format</returns>
        protected static string sBinStrToHexStr(string sBinString)
        {
            string sHex = String.Format("{0:X2}", Convert.ToInt32(sBinString, 2));
            return sHex;
        }

        /// <summary>
        /// Convert Hex value to string format
        /// </summary>
        /// <param name="sHex">string : hex value for conversion</param>
        /// <returns>string : result of the conversion</returns>
        public static string sHexToStringUTF8(string sHex)
        {
            byte[] arrbString = HexStringToByteArray(sHex);
            return Encoding.UTF8.GetString(arrbString);
        }

        public static string sHexToStringUTF7(string sHex)
        {
            byte[] arrbString = HexStringToByteArray(sHex);
            return Encoding.UTF7.GetString(arrbString, 0, arrbString.Length);
        }

        public static string sHexToStringUTF7Loop(string sHex)
        {
            byte[] arrbString = HexStringToByteArray(sHex);
            char[] arrcUTF7 = Encoding.UTF7.GetChars(arrbString, 0, arrbString.Length);

            return (new string(arrcUTF7));
        }

        /// <summary>
        /// Convert binary value into Hex value
        /// </summary>
        /// <param name="sBinary">string : binary value for the conversion</param>
        /// <returns>string : hex value from the conversion</returns>
        public static string sBinStringToStr(string sBinary)
        {
            return sHexToStringUTF8(sBinStrToHexStr(sBinary));
        }

        /// <summary>
        /// Convert bitmap into binary value
        /// </summary>
        /// <param name="sBitMapString">string : the bitmap that will be used in conversion</param>
        /// <returns>string : the result of bitmap conversion, in binary value</returns>
        public static string sBitMapToBinString(string sBitMapString)
        {
            string sBinary = null;
            int iLen = sBitMapString.Length;
            if (iLen == 16 || iLen == 32)
                for (int i = 0; i < iLen; i += 2)
                    sBinary += sHexStrToBinStr(sBitMapString.Substring(i, 2));
            return sBinary;
        }

        /// <summary>
        /// Conversion of Binary string into BitMap
        /// </summary>
        /// <param name="sBinString">string : binary value that will be converted</param>
        /// <returns>string : the conversion result</returns>
        public static string sBinStringToBitMap(string sBinString)
        {
            string sBitMap = null;
            if (sBinString.Length % 8 == 0 && (sBinString.Length / 8 == 8 || sBinString.Length / 8 == 16))
                for (int i = 0; i < sBinString.Length; i += 8)
                    sBitMap += sBinStrToHexStr(sBinString.Substring(i, 8));
            return sBitMap;
        }

        /// <summary>
        /// Convert integer into boolean
        /// </summary>
        /// <param name="iBool">int : the value to be converted</param>
        /// <returns>bool : result of the conversion</returns>
        public static bool bIntToBool(int iBool)
        {
            bool bReturn = false;
            if (iBool == 1) bReturn = true;  // 1--> true; 0--> false
            return bReturn;
        }

        // <summary>
        /// Convert integer into decimal
        /// </summary>
        /// <param name="iBool">int : the value to be converted</param>
        /// <returns>bool : result of the conversion</returns>
        public static decimal bIntToDec(int iInt)
        {
            decimal dReturn = (decimal)iInt / 100;
            return dReturn;
        }

        public static string sCalcHashCAPK(string sRID, string sIndex, string sModulus, string sExponent)
        {
            //string sTemp = "A00000000403C2490747FE17EB0584C88D47B1602704150ADC88C5B998BD59CE043EDEBF0FFEE3093AC7956AD3B6AD4554C6DE19A178D6DA295BE15D5220645E3C8131666FA4BE5B84FE131EA44B039307638B9E74A8C42564F892A64DF1CB15712B736E3374F1BBB6819371602D8970E97B900793C7C2A89A4A1649A59BE680574DD0B6014503";

            string sTemp = sRID + sIndex + sModulus + sExponent;

            byte[] arrbHashResult = new byte[20];
            //create new instance of md5
            SHA1 sha1Encrypt = SHA1.Create();

            //convert the input text to array of bytes
            byte[] arrbHashData = sha1Encrypt.ComputeHash(HexStringToByteArray(sTemp));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < arrbHashData.Length; i++)
            {
                returnValue.Append(arrbHashData[i].ToString());
            }

            // return hexadecimal string
            return sByteArrayToString(arrbHashData);
        }
        
        public static DataTable ConvertCSVtoDataTable(string strFilePath, char cSeparator)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                string[] headers = sr.ReadLine().Split(cSeparator);
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(cSeparator);
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }

            }
            return dt;
        }

        /// <summary>
        /// Leveling in terminal
        /// </summary>
        [Flags]
        public enum NodeLevel
        {
            Terminal,
            Acquirer,
            Issuer,
            Card,
            Category, //noe
            CustomMenu
        }

        /// <summary>
        /// Type of several forms
        /// </summary>
        [Flags]
        public enum FormType
        {
            DbTemplate,
            Register,
            CopyTerminal
        }
        #endregion

        #region "Date and Time"
        public static string sGetyyyyMMdd()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sGetMMMddyyyyhhmmsstt()
        {
            return DateTime.Now.ToString("MMM dd, yyyy, hh:mm:ss.fff tt ", ciUSFormat);
        }

        public static string sGetMMddyyyyhhmmsstt()
        {
            return DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt ", ciUSFormat);
        }

        public static string sGetMMddhhmmss()
        {
            return DateTime.Now.ToString("MMddhhmmss");
        }

        public static string sGetDisplayTimeFormat(string sTime)
        {
            return CommonLib.sMid(sTime, 0, 2) + ":" + CommonLib.sMid(sTime, 2);
        }

        public static string sGetTime(string sTime)
        {
            return sTime.Replace(":", "");
        }

        public static DateTime dtStringToDateTime(string sDate)
        {
            string[] arrsDateFormats = new string[] {
                   "MMddyy", "MMddyyhhmmss",
                   "M/d/yyyy", "MM/d/yyyy", "MM-d-yyyy", "MM-dd-yyyy",
                   "d/M/yyyy", "d/MM/yyyy", "dd/M/yyyy", "dd/MM/yyyy",
                   "d-M-yyyy", "d-MM-yyyy", "dd-M-yyyy", "dd-MM-yyyy",
                   "yyyy/MM/dd", "yyyy/MM/dd",
                   "MMM dd, yyyy", "MMM d, yyyy",
                   "dd MMMM yyyy"
                   };
            DateTime dtTemp;
            DateTime.TryParseExact(sDate, arrsDateFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out dtTemp);
            return dtTemp;
    }

        public static bool isValidDatetime(string sDate)
        {
            string[] arrsDateFormats = new string[] {"M/d/yyyy", "MM/d/yyyy", "MM-d-yyyy", "MM-dd-yyyy",
                   "d/M/yyyy", "d/MM/yyyy", "dd/M/yyyy", "dd/MM/yyyy",
                   "yyyy/MM/dd", "yyyy/MM/dd",
                   "MMM dd, yyyy", "MMM d, yyyy",
                   "dd MMMM yyyy","hhmmss", "HHmmss"};
            DateTime dtTemp;
            return DateTime.TryParseExact(sDate, arrsDateFormats, CultureInfo.InvariantCulture, DateTimeStyles.None, out dtTemp);
        }

        public static int iDateDiff(DateTime dtCurrDate, DateTime dtOldDate)
        {
            TimeSpan tsDiffResult = dtCurrDate.Subtract(dtOldDate);
            return tsDiffResult.Days;
        }

        public static int iDateDiff(string sCurrDate, string sOldDate)
        {
            DateTime dtCurrDate = dtStringToDateTime(sCurrDate);
            DateTime dtOldDate = dtStringToDateTime(sOldDate);

            TimeSpan tsDiffResult = dtCurrDate.Subtract(dtOldDate);
            return tsDiffResult.Days;
        }
        #endregion

        #region "Tag Value"
        /// <summary>
        /// Get the tag value from the given content
        /// </summary>
        /// <param name="sContent">string : the given content for searching the tag</param>
        /// <param name="sTag">string : tag title for searching</param>
        /// <param name="iLength">int : length of the tag value</param>
        /// <returns></returns>
        public static string sGetTagValue(string sContent, string sTag, int iLength)
        {
            
            if (sContent.Contains(sTag)) // If tag is not found
            {
                int iIndex = sContent.IndexOf(sTag);
                iIndex += 4;
                string sLength = sMid(sContent, iIndex, iLength);
                iIndex += iLength;
                return sMid(sContent, iIndex, int.Parse(sLength));
            }
            else
                return null;
        }

        public static string sGetTagValueNew(string sContent, string sTag, int iLength, int iLengthTag)
        {

            if (sContent.Contains(sTag)) // If tag is not found
            {
                int iIndex = sContent.IndexOf(sTag);
                iIndex += iLengthTag;
                string sLength = sMid(sContent, iIndex, iLength);
                iIndex += iLength;
                return sMid(sContent, iIndex, int.Parse(sLength));
            }
            else
                return null;
        }


        /// <summary>
        /// Get the tag value from the given content from specific start position
        /// </summary>
        /// <param name="sContent">string : the given content for searching the tag</param>
        /// <param name="sTag">string : tag title for searching</param>
        /// <param name="iLength">int : length of the tag value</param>
        /// <param name="iStartIndex">int : the search starting position</param>
        /// <returns></returns>
        public static string sGetTagValue(string sContent, string sTag, int iLength, int iStartIndex)
        {
            if (sContent.Contains(sTag)) // If tag title is found
            {
                int iIndex = sContent.IndexOf(sTag, iStartIndex);
                iIndex += 4;
                string sLength = sMid(sContent, iIndex, iLength);
                iIndex += iLength;
                return sMid(sContent, iIndex, int.Parse(sLength));
            }
            else
                return null;
        }

        /// <summary>
        /// Get index of the tag in the content from a specific start index
        /// </summary>
        /// <param name="sContent">string : value that contain the tag</param>
        /// <param name="sTag">string : tag to search</param>
        /// <param name="iStartIndex">int : the search starting position</param>
        /// <returns>int : index of the tag in sContent</returns>
        public static int iGetIndex(string sContent, string sTag, int iStartIndex)
        {
            return sContent.IndexOf(sTag, iStartIndex);
        }

        /// <summary>
        /// Get the length of the value in string format
        /// </summary>
        /// <param name="sValue">string : the value to be computed</param>
        /// <returns>string : length of the value</returns>
        public static string sGetLength(string sValue)
        {
            return String.Format("{0:D2}", sValue.Length);
        }

        /// <summary>
        /// Generate the TLV value for each tag.
        /// TLV format : Tag+Length+Value
        /// </summary>
        /// <param name="sTag">string : the tag title</param>
        /// <param name="sValue">string : the tag value</param>
        /// <param name="iLength">int : the length format</param>
        /// <returns>string : TLV format</returns>
        public static string sGenerateTLV(string sTag, string sValue, int iLength)
        {
            return sTag + ((string)sValue.Length.ToString()).PadLeft(iLength, '0') + sValue;
        }
        #endregion

        #region "Network"
        public static IPHostEntry GetHostEntry(string _sIP)
        {
            IPHostEntry ipheTemp = new IPHostEntry();
            try
            {
                if (_sIP == "127.0.0.1")
                {
                    ipheTemp = Dns.GetHostByName(Dns.GetHostName());
                }
                else
                    ipheTemp = Dns.GetHostByAddress(_sIP);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return ipheTemp;
        }
        #endregion

        #region "Validation"
        /// <summary>
        /// Validate if the value is numeric or not
        /// </summary>
        /// <param name="sNumber">string : value to be validated</param>
        /// <returns>bool : true if numeric, if not return false</returns>
        public static bool isNumeric(string sNumber)
        {
            try
            {
                Convert.ToDecimal(sNumber);
                return true;
            }
            catch // If not number
            {
                return false;
            }
        }

        /// <summary>
        /// Validate if the value is alphabetical
        /// </summary>
        /// <param name="sValue">string : value to be validated</param>
        /// <returns>bool : true if alphabetical, if not return false</returns>
        public static bool isAlphabet(string sValue)
        {
            foreach (char c in sValue.ToCharArray())
                if (((int)c < 65 || ((int)c > 90) && (int)c < 97) || (int)c > 122)
                    return false;
            return true;
        }

        /// <summary>
        /// Validate if the value is alphabetical
        /// </summary>
        /// <param name="sValue">char : value to be validated</param>
        /// <returns>bool : true if alphabetical, if not return false</returns>
        public static bool isAlphabet(char c)
        {
            int i = (int)c;
            return ((i >= 65 && i <= 90) || (i >= 97 && i <= 122));
        }

        /// <summary>
        /// Validate if the value is numeric or not
        /// </summary>
        /// <param name="sNumber">char : value to be validated</param>
        /// <returns>bool : true if numeric, if not return false</returns>
        public static bool isNumeric(char c)
        {
            int i = (int)c;
            return ((i >= 48 && i <= 57));
        }

        /// <summary>
        /// Get the index of specific key from one parameter
        /// </summary>
        /// <param name="param">string : the parameter for searching the key</param>
        /// <param name="sStr">string : the key to be found</param>
        /// <returns>int : index of the key in parameter</returns>
        public static int iIntStr(string param, string sStr)
        {
            return param.IndexOf(sStr);
        }

        /// <summary>
        /// Get the index of specific key from one parameter from a specific start
        /// </summary>
        /// <param name="param">string : the parameter for searching the key</param>
        /// <param name="sStr">string : the key to be found</param>
        /// <param name="iStartIndex">int : the search starting position</param>
        /// <returns>int : index of the key in parameter</returns>
        public static int iIntStr(string param, string sStr, int iStartIndex)
        {
            return param.IndexOf(sStr, iStartIndex);
        }

        public static int iIntStr(string param, string sStr, StringComparison scCompare)
        {
            return param.IndexOf(sStr, scCompare);
        }

        public static int iIntStr(string param, string sStr, int iStartIndex, StringComparison scCompare)
        {
            return param.IndexOf(sStr, iStartIndex, scCompare);
        }

        public bool isIpAddress(string sIpAddress)
        {
            return Regex.IsMatch(sIpAddress, @"^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$");
        }
        #endregion

        #region "Commented"

        //public static void CreateFile(string sFileName, string sValue)
        //{
        //    FileStream oFileStream;
        //    StreamWriter oStreamWriter;

        //    oFileStream = new FileStream(sFileName, FileMode.Append, FileAccess.ReadWrite);
        //    oStreamWriter = new StreamWriter(oFileStream);
        //    oStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
        //    oStreamWriter.Write(sValue);

        //    oStreamWriter.Close();
        //    oStreamWriter.Dispose();

        //    oFileStream.Close();
        //    oFileStream.Dispose();
        //}

        #endregion
        
        public static string sReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }
}