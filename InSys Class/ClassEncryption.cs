using System.Security.Cryptography;
using System.IO;
using System.Text;
using System;

namespace InSysClass
{
    public class EncryptionLib
    {
        static UTF8Encoding utf8 = new UTF8Encoding();

        static string MD5_KEY = "PT. INTEGRA PRATAMA";
        static byte[] TripleDES_KEY = utf8.GetBytes("PT. INTEGRA PRATAMA12345");

        // 24 byte
        static byte[] DES_KEY = utf8.GetBytes("PT. INTE");
        // 8 byte
        static byte[] TripleDES_IV = utf8.GetBytes("INTEGRA3");
        // 8 byte

        #region "Data Encryption"
        public static string GenerateMD5(string strSourceData)
        {
            byte[] tmpSource = null;
            byte[] tmpHash = null;

            strSourceData = strSourceData.Insert(5, MD5_KEY);

            tmpSource = ASCIIEncoding.ASCII.GetBytes(strSourceData);

            tmpHash = new MD5CryptoServiceProvider().ComputeHash(tmpSource);
            return (GenerateMD5ByteArrayToString(tmpHash));
        }

        protected static string GenerateMD5ByteArrayToString(byte[] arrInput)
        {
            int i = 0;
            StringBuilder sOutput = new StringBuilder(arrInput.Length);
            for (i = 0; i <= arrInput.Length - 1; i++)
            {
                sOutput.Append(arrInput[i].ToString("X2"));
            }
            return sOutput.ToString();
        }

        public static string Encrypt3DES(string _sText)
        {
            return Encrypt3DES(_sText, TripleDES_KEY, TripleDES_IV);
        }

        public static string Encrypt3DES(string _sText, byte[] arrbKey)
        {
            return Encrypt3DES(_sText, arrbKey, null);
        }

        public static string Encrypt3DES(string _sPlainText, byte[] arrbKey, byte[] arrbVector)
        {
            byte[] arrbPlain = UTF8Encoding.UTF8.GetBytes(_sPlainText);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = arrbKey;
            if (arrbVector != null) tdes.IV = arrbVector;
            tdes.Mode = CipherMode.ECB;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(arrbPlain, 0, arrbPlain.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        
        public static string Decrypt3DES(string _sText)
        {            
            return Decrypt3DES(_sText, TripleDES_KEY, TripleDES_IV);
        }

        public static string Decrypt3DES(string _sText, byte[] _arrbKey)
        {
            return Decrypt3DES(_sText, _arrbKey, null);
        }

        public static string Decrypt3DES(string _sCipherText, byte[] arrbKey, byte[] arrbVector)
        {
            byte[] arrbCipher = Convert.FromBase64String(_sCipherText);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = arrbKey;
            if (arrbVector != null) tdes.IV = arrbVector;
            tdes.Mode = CipherMode.ECB;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] arrbResult = cTransform.TransformFinalBlock(arrbCipher, 0, arrbCipher.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(arrbResult);
        }
        #endregion

        #region "File Encryption"
        private enum CryptoAction
        {
            actionEncrypt = 1,
            actionDecrypt = 2
        }

        public static void EncryptFile(string EncryptedFile, string UnEncryptedFile)
        {
            byte[] byteKey = DES_KEY;
            byte[] byteInitializationVector = TripleDES_IV;


            EncryptOrDecryptFile(UnEncryptedFile, EncryptedFile, byteKey, byteInitializationVector, CryptoAction.actionEncrypt);
        }

        public static void DecryptFile(string EncryptedFile, string UnEncryptedFile)
        {
            byte[] byteKey = DES_KEY;
            byte[] byteInitializationVector = TripleDES_IV;

            EncryptOrDecryptFile(EncryptedFile, UnEncryptedFile,
                byteKey, byteInitializationVector, CryptoAction.actionDecrypt);
        }

        //private byte[] GetKeyByteArray(string sPassword)
        //{
        //    byte[] byteTemp = new byte[8];
        //    sPassword = sPassword.PadRight(8);
        //    // make sure we have 8 chars

        //    int iCharIndex = 0;
        //    for (iCharIndex = 0; iCharIndex <= 7; iCharIndex++)
        //    {
        //        byteTemp[iCharIndex] = (byte) char.Parse(CommonLib.sMid(sPassword, iCharIndex + 1, 1));
        //    }

        //    return byteTemp;
        //}

        private static void EncryptOrDecryptFile(string sInputFile, string sOutputFile, byte[] byteDESKey, byte[] byteDESIV, CryptoAction Direction)
        {

            //Create the file streams to handle the input and output files.
            //Dim fsInput As New FileStream(sInputFile, _
            // FileMode.Open, FileAccess.Read)

            // 03/12/2008.
            FileStream fsInput = default(FileStream);
            if (File.Exists(sInputFile) == true)
            {
                fsInput = new FileStream(sInputFile, FileMode.Open, FileAccess.Read);
            }
            else
            {
                fsInput = new FileStream(sInputFile, FileMode.Create, FileAccess.Write);
            }
            // ***********
            FileStream fsOutput = new FileStream(sOutputFile, FileMode.OpenOrCreate, FileAccess.Write);
            fsOutput.SetLength(0);

            //Variables needed during encrypt/decrypt process
            byte[] byteBuffer = new byte[4097];
            //holds a block of bytes for processing
            long nBytesProcessed = 0;
            //running count of bytes encrypted
            long nFileLength = fsInput.Length;
            int iBytesInCurrentBlock = 0;
            DESCryptoServiceProvider desProvider = new DESCryptoServiceProvider();
            CryptoStream csMyCryptoStream = null;

            // Set up for encryption or decryption
            switch (Direction)
            {
                case CryptoAction.actionEncrypt:
                    csMyCryptoStream = new CryptoStream(fsOutput, desProvider.CreateEncryptor(byteDESKey, byteDESIV), CryptoStreamMode.Write);

                    break;
                case CryptoAction.actionDecrypt:
                    csMyCryptoStream = new CryptoStream(fsOutput, desProvider.CreateDecryptor(byteDESKey, byteDESIV), CryptoStreamMode.Write);

                    break;
            }

            //sbEncryptionStatus.Text = sDirection + " starting..."

            //Read from the input file, then encrypt or decrypt
            //and write to the output file.
            while (nBytesProcessed < nFileLength)
            {
                iBytesInCurrentBlock = fsInput.Read(byteBuffer, 0, 4096);
                csMyCryptoStream.Write(byteBuffer, 0, iBytesInCurrentBlock);
                nBytesProcessed = nBytesProcessed + (long)iBytesInCurrentBlock;
            }
            //sbEncryptionStatus.Text = sDirection + _
            // " in process - Bytes processed - " + _
            // nBytesProcessed.ToString

            //sbEncryptionStatus.Text = "Finished " + sDirection + _
            // ". Total bytes processed - " + nBytesProcessed.ToString
            csMyCryptoStream.Close();
            fsInput.Close();

            fsOutput.Close();
        }
        #endregion

        #region "SHA256"
        public static string EncryptSHA256(string sInputValue)
        {
            return EncryptSHA256(sInputValue, MD5_KEY);
        }

        public static string DecryptSHA256(string sInputValue)
        {
            return DecryptSHA256(sInputValue, MD5_KEY);
        }

        public static string EncryptSHA256(string sInputValue, string sInputSalt)
        {

            string sSalt = sInputSalt;

            byte[] bUtfData = UTF8Encoding.UTF8.GetBytes(sInputValue);

            byte[] bSaltBytes = Encoding.UTF8.GetBytes(sSalt);

            string sEncryptedString = string.Empty;

            using (AesManaged vAES = new AesManaged())
            {
                Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(sSalt, bSaltBytes);

                vAES.BlockSize = vAES.LegalBlockSizes[0].MaxSize;

                vAES.KeySize = vAES.LegalKeySizes[0].MaxSize;

                vAES.Key = rfc.GetBytes(vAES.KeySize / 8);

                vAES.IV = rfc.GetBytes(vAES.BlockSize / 8);

                using (ICryptoTransform ictEncryptTransform = vAES.CreateEncryptor())
                {

                    using (MemoryStream msEncryptedStream = new MemoryStream())
                    {

                        using (CryptoStream encryptor =

                        new CryptoStream(msEncryptedStream, ictEncryptTransform, CryptoStreamMode.Write))
                        {

                            encryptor.Write(bUtfData, 0, bUtfData.Length);

                            encryptor.Flush();

                            encryptor.Close();

                            byte[] encryptBytes = msEncryptedStream.ToArray();

                            //sEncryptedString = Convert.ToBase64String(encryptBytes);
                            sEncryptedString = CommonLib.sByteArrayToHexString(encryptBytes);
                        }
                    }
                }
            }
            return sEncryptedString.Replace(" ", "");
        }

        public static string DecryptSHA256(string sInputValue, string sInputSalt)
        {

            string sSalt = sInputSalt;

            //byte[] bEncryptedBytes = Convert.FromBase64String(sInputValue);
            byte[] bEncryptedBytes = CommonLib.HexStringToByteArray(sInputValue.Replace(" ", ""));

            byte[] bSaltBytes = Encoding.UTF8.GetBytes(sSalt);

            string sDdecryptedString = string.Empty;

            using (var vAES = new AesManaged())
            {

                Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(sSalt, bSaltBytes);

                vAES.BlockSize = vAES.LegalBlockSizes[0].MaxSize;

                vAES.KeySize = vAES.LegalKeySizes[0].MaxSize;

                vAES.Key = rfc.GetBytes(vAES.KeySize / 8);

                vAES.IV = rfc.GetBytes(vAES.BlockSize / 8);

                using (ICryptoTransform ictDecryptTransform = vAES.CreateDecryptor())
                {

                    using (MemoryStream msDecryptedStream = new MemoryStream())
                    {

                        CryptoStream decryptor = new CryptoStream(msDecryptedStream, ictDecryptTransform, CryptoStreamMode.Write);

                        decryptor.Write(bEncryptedBytes, 0, bEncryptedBytes.Length);

                        decryptor.Flush();

                        decryptor.Close();

                        byte[] decryptBytes = msDecryptedStream.ToArray();

                        sDdecryptedString = UTF8Encoding.UTF8.GetString(decryptBytes, 0, decryptBytes.Length);
                    }
                }
            }
            return sDdecryptedString;
        }
        #endregion
    }
}