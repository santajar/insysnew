using System;
using System.IO;

namespace InSysClass
{
    public class TraceLib
    {

        protected static void CreateDebugFolder(string sPath)
        {
            if (!Directory.Exists(sPath + "\\DEBUG"))
                Directory.CreateDirectory(sPath + "\\DEBUG");
        }

        public static void CreateAppLog(string sPath, string sLog)
        {
            CreateDebugFolder(sPath);

            string sAppLogFileName = DateTime.Now.ToString("yyyyMMdd") + ".AppLog";
            string sTime = DateTime.Now.ToString("MMM dd, yyyy, hh:mm:ss tt ");
            StreamWriter oSW = new StreamWriter(sPath + sAppLogFileName, true);
            oSW.WriteLine(sTime + sLog);
            oSW.Close();
        }

        public static void CreateCommLog(string sPath, string sLog)
        {
            CreateDebugFolder(sPath);

            string sCommLogFileName = DateTime.Now.ToString("yyyyMMdd") + ".CommLog";
            string sTime = DateTime.Now.ToString("MMM dd, yyyy, hh:mm:ss tt ");
            StreamWriter oSW = new StreamWriter(sPath + sCommLogFileName, true);
            oSW.WriteLine(sTime + sLog);
            oSW.Close();
        }

        public static void CreateMsgLog(string sPath, string sLog)
        {
            CreateDebugFolder(sPath);

            string sMsgLogFileName = DateTime.Now.ToString("yyyyMMdd") + ".MsgLog";
            string sTime = DateTime.Now.ToString("MMM dd, yyyy, hh:mm:ss tt ");
            StreamWriter oSW = new StreamWriter(sPath + sMsgLogFileName, true);
            oSW.WriteLine(sTime + sLog);
            oSW.Close();
        }
    }
}