using System.Net;
using System.Net.Mail;

namespace InSysClass
{
    public class EmailLib
    {
        protected string sFrom;
        protected string sTo;
        protected string sCc;
        protected string sBcc;
        protected string sSubject;
        protected string sBody;

        protected string sAttachment;

        protected string sSMTP;
        protected int iSMTPPort;
        protected string sUsername;
        protected string sPwd;

        public EmailLib(string _sSMTP, int _iSMTPPort, string _sUser, string _sPwd)
        {
            sSMTP = _sSMTP;
            iSMTPPort = _iSMTPPort;
            sUsername = _sUser;
            sPwd = _sPwd;
        }

        public void From(string _sFrom)
        {
            sFrom = _sFrom.Trim();
        }

        public void To(string _sTo)
        {
            sTo = _sTo.Trim();
        }

        public void Cc(string _sCc)
        {
            sCc = _sCc.Trim();
        }

        public void Bcc(string _Bcc)
        {
            sBcc = _Bcc.Trim();
        }

        public void Subject(string _sSubject)
        {
            sSubject = _sSubject;
        }

        public void Body(string _sBody)
        {
            sBody = _sBody;
        }

        public void Attach(string _sAttach)
        {
            sAttachment = _sAttach;
        }

        public void Send()
        {
            MailMessage oObjEmail = new MailMessage();
            oObjEmail.From = new MailAddress(sFrom);

            if (sTo != "")
            {
                char[] cDelimiter = new char[] { ';' };
                foreach (string sSubstr in sTo.Split(cDelimiter))
                {
                    oObjEmail.To.Add(sSubstr);
                }
            }

            if (sCc != "")
            {
                char[] cDelimiter = new char[] { ';' };
                foreach (string sSubstr in sCc.Split(cDelimiter))
                {
                    oObjEmail.CC.Add(sSubstr);
                }
            }

            if (sBcc != "")
            {
                char[] cDelimiter = new char[] { ';' };
                foreach (string sSubstr in sBcc.Split(cDelimiter))
                {
                    oObjEmail.Bcc.Add(sSubstr);
                }
            }
            oObjEmail.Subject = sSubject;
            oObjEmail.Body = sBody;
            oObjEmail.Priority = MailPriority.High;

            if (sAttachment != "")
            {
                char[] cDelimiter = new char[] { ';' };
                foreach (string sSubstr in sAttachment.Split(cDelimiter))
                {
                    Attachment MyAttachment = new Attachment(sSubstr);
                    oObjEmail.Attachments.Add(MyAttachment);
                }
            }

            SmtpClient oObjSMTPClient = new SmtpClient(sSMTP, iSMTPPort);
            NetworkCredential basicauthInfo = new NetworkCredential(sUsername, sPwd);
            oObjSMTPClient.UseDefaultCredentials = false;
            oObjSMTPClient.Credentials = basicauthInfo;
            //oObjSMTPClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            oObjSMTPClient.Send(oObjEmail);
        }
    }
}