using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;

public class ExcelLib
{
    public static void WriteExcel(SqlDataReader dr, string sFilename)
    {
        StringBuilder sbrHTML = new StringBuilder();
        //Making HTML
        sbrHTML.Append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><style>.text { mso-number-format:\\@; } </style><TABLE class=\"text\" Border=1 ID=\"Table1\">\n");
        //sbrHTML.Append("<TR bgcolor=\"#d7ffd7\"><TD ColSpan=4><Font Size=5> " + text_header + "</Font></TD><TR>");
        //sbrHTML.Append("<TR bgcolor=\"#bbbbbb\" >");
        for (int i = 0; i < dr.FieldCount; i++)
        {
            sbrHTML.Append("<TH>" + dr.GetName(i) + "</TH>\n");
        }
        sbrHTML.Append("</TR>");
        while (dr.Read())
        {
            sbrHTML.Append("<TR>");
            for (int i = 0; i < dr.FieldCount; i++)
            {
                //sbrHTML.Append("<TD>" + good_value(dr.GetValue(i).ToString()) + "</TD>");
                sbrHTML.Append("<TD>" + dr.GetValue(i).ToString() + "</TD>");
            }
            sbrHTML.Append("</TR>");
        }

        sbrHTML.Append("</TABLE>");
        //sbrHTML.Append("<BR><BR><B>Reported by " + Classes.clsPublicVariables.user_name + "</B>");
        //sbrHTML.Append("<BR><BR><B>Date: " + Classes.clsPublicVariables.strToday + "</B>");
        //ENDOF MAKING HTML
        StreamWriter swXLS = new StreamWriter(sFilename);
        swXLS.Write(sbrHTML.ToString());
        swXLS.Close();
        swXLS.Dispose();
    }

    public static void WriteExcel(DataTable dt, string sFilename)
    {
        StringBuilder sbrHTML = new StringBuilder();
        //Making HTML
        sbrHTML.Append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><style>.text { mso-number-format:\\@; } </style><TABLE class=\"text\" Border=1 ID=\"Table1\">\n");
        //sbrHTML.Append("<TR bgcolor=\"#d7ffd7\"><TD ColSpan=4><Font Size=5> " + text_header + "</Font></TD><TR>");
        //sbrHTML.Append("<TR bgcolor=\"#bbbbbb\" >");
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            sbrHTML.Append("<TH>" + dt.Columns[i].ColumnName.ToString() + "</TH>\n");
        }
        sbrHTML.Append("</TR>");

        foreach (DataRow drRow in dt.Rows)
        {
            sbrHTML.Append("<TR>");
            for (int iCol = 0; iCol < dt.Columns.Count; iCol ++)
            {
                //sbrHTML.Append("<TD>" + good_value(dr.GetValue(i).ToString()) + "</TD>");
                sbrHTML.Append("<TD>" + drRow[iCol].ToString() + "</TD>");
            }
            sbrHTML.Append("</TR>");
        }

        //while (dr.Read())
        //{
        //    sbrHTML.Append("<TR>");
        //    for (int i = 0; i < dr.FieldCount; i++)
        //    {
        //        //sbrHTML.Append("<TD>" + good_value(dr.GetValue(i).ToString()) + "</TD>");
        //        sbrHTML.Append("<TD>" + dr.GetValue(i).ToString() + "</TD>");
        //    }
        //    sbrHTML.Append("</TR>");
        //}

        sbrHTML.Append("</TABLE>");
        //sbrHTML.Append("<BR><BR><B>Reported by " + Classes.clsPublicVariables.user_name + "</B>");
        //sbrHTML.Append("<BR><BR><B>Date: " + Classes.clsPublicVariables.strToday + "</B>");
        //ENDOF MAKING HTML
        StreamWriter swXLS = new StreamWriter(sFilename);
        swXLS.Write(sbrHTML.ToString());
        swXLS.Close();
        swXLS.Dispose();
    }

    public static void WriteExcelQC (SqlDataReader dr, string sFilename)
    {
        StringBuilder sbrHTML = new StringBuilder();
        //Making HTML
        sbrHTML.Append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><style>.text { mso-number-format:\\@; } </style><TABLE class=\"text\" Border=1 ID=\"Table1\">\n");
        //sbrHTML.Append("<TR bgcolor=\"#d7ffd7\"><TD ColSpan=4><Font Size=5> " + text_header + "</Font></TD><TR>");
        //sbrHTML.Append("<TR bgcolor=\"#bbbbbb\" >");
        for (int i = 0; i < dr.FieldCount; i++)
        {
            sbrHTML.Append("<TH>" + dr.GetName(i) + "</TH>\n");
        }
        sbrHTML.Append("</TR>");
        while (dr.Read())
        {
            sbrHTML.Append("<TR>");
            for (int i = 0; i < dr.FieldCount; i++)
            {
                //sbrHTML.Append("<TD>" + good_value(dr.GetValue(i).ToString()) + "</TD>");
                if ((i == 0 || i == 1) && dr.GetValue(i).ToString().Trim() != "-")
                    sbrHTML.Append("<TD>" + "'" + dr.GetValue(i).ToString() + "</TD>");
                else
                    sbrHTML.Append("<TD>" + dr.GetValue(i).ToString() + "</TD>");
            }
            sbrHTML.Append("</TR>");
        }

        sbrHTML.Append("</TABLE>");
        //sbrHTML.Append("<BR><BR><B>Reported by " + Classes.clsPublicVariables.user_name + "</B>");
        //sbrHTML.Append("<BR><BR><B>Date: " + Classes.clsPublicVariables.strToday + "</B>");
        //ENDOF MAKING HTML
        StreamWriter swXLS = new StreamWriter(sFilename);
        swXLS.Write(sbrHTML.ToString());
        swXLS.Close();
        swXLS.Dispose();
    }

    public static void WriteExcel(OleDbDataReader dr, string sFilename)
    {
        StringBuilder sbrHTML = new StringBuilder();
        //Making HTML
        sbrHTML.Append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><style>.text { mso-number-format:\\@; } </style><TABLE class=\"text\" Border=1 ID=\"Table1\">");
        //sbrHTML.Append("<TR bgcolor=\"#d7ffd7\"><TD ColSpan=4><Font Size=5> " + text_header + "</Font></TD><TR>");
        //sbrHTML.Append("<TR bgcolor=\"#bbbbbb\" >");
        for (int i = 0; i < dr.FieldCount; i++)
        {
            sbrHTML.Append("<TH>" + dr.GetName(i) + "</TH>");
        }
        sbrHTML.Append("</TR>");
        while (dr.Read())
        {
            sbrHTML.Append("<TR>");
            for (int i = 0; i < dr.FieldCount; i++)
            {
                //sbrHTML.Append("<TD>" + good_value(dr.GetValue(i).ToString()) + "</TD>");
                sbrHTML.Append("<TD>" + dr.GetValue(i).ToString() + "</TD>");
            }
            sbrHTML.Append("</TR>");
        }

        sbrHTML.Append("</TABLE>");
        //sbrHTML.Append("<BR><BR><B>Reported by " + Classes.clsPublicVariables.user_name + "</B>");
        //sbrHTML.Append("<BR><BR><B>Date: " + Classes.clsPublicVariables.strToday + "</B>");
        //ENDOF MAKING HTML
        StreamWriter swXLS = new StreamWriter(sFilename);
        swXLS.Write(sbrHTML.ToString());
        swXLS.Close();
    }

    protected static string good_value(string input)
    {
        return ((input == "" || (input[0] >= '0' && input[0] <= '9')) ? "'" : "") + input;
    }

    public static void WriteXMLExcel(SqlDataReader dr, string sFilename)
    {
        int iCol = dr.FieldCount;
        string sNewLine = "\n";

        StringBuilder sExcelXML = new StringBuilder();

        //First Write the Excel Header
        sExcelXML.Append(ExcelXMLHeader());

        // Get all the Styles
        sExcelXML.Append(ExcelXMLStyles("styles.config"));

        // Worksheet options Required only one time 
        sExcelXML.Append(ExcelXMLWorkSheetOptions());

        // Create First Worksheet tag
        sExcelXML.Append("<Worksheet ss:Name=\"Sheet1\">");
        // Then Table Tag
        sExcelXML.Append("<Table>");

        // Table Header
        sExcelXML.Append("<Row ss:Index=\"1\">" + sNewLine);
        for (int i = 0; i < dr.FieldCount; i++)
        {
            // Cell Tags
            sExcelXML.Append("<Cell ss:StyleID=\"s24\"><Data ss:Type=\"String\">" + dr.GetName(i) + "</Data></Cell>" + sNewLine);
        }
        sExcelXML.Append("</Row>");

        while (dr.Read())
        {
            // Row Tag
            sExcelXML.Append("<tr>");
            for (int j = 0; j < iCol; j++)
            {
                // Cell Tags
                sExcelXML.Append("<td>");
                sExcelXML.Append(dr[j].ToString());
                sExcelXML.Append("</td>");
            }
            sExcelXML.Append("</tr>");
        }
        sExcelXML.Append("</Table>");
        sExcelXML.Append("</Worksheet>");

        // Close the Workbook tag (in Excel header you can see the Workbook tag)
        sExcelXML.Append("</Workbook>\n");

        SaveExcelXML(sFilename, sExcelXML.ToString());
    }

    public static void WriteXMLExcel(OleDbDataReader dr, string sFilename)
    {
        int iCol = dr.FieldCount;
        string sNewLine = "\n";

        StringBuilder sExcelXML = new StringBuilder();

        //First Write the Excel Header
        sExcelXML.Append(ExcelXMLHeader());

        // Get all the Styles
        sExcelXML.Append(ExcelXMLStyles("styles.config"));

        // Worksheet options Required only one time 
        sExcelXML.Append(ExcelXMLWorkSheetOptions());

        // Create First Worksheet tag
        sExcelXML.Append("<Worksheet ss:Name=\"Sheet1\">");
        // Then Table Tag
        sExcelXML.Append("<Table>");

        // Table Header
        sExcelXML.Append("<Row ss:Index=\"1\">" + sNewLine);
        for (int i = 0; i < dr.FieldCount; i++)
        {
            // Cell Tags
            sExcelXML.Append("<Cell ss:StyleID=\"s24\"><Data ss:Type=\"String\">" + dr.GetName(i) + "</Data></Cell>" + sNewLine);
        }
        sExcelXML.Append("</Row>");

        while (dr.Read())
        {
            // Row Tag
            sExcelXML.Append("<tr>");
            for (int j = 0; j < iCol; j++)
            {
                // Cell Tags
                sExcelXML.Append("<td>");
                sExcelXML.Append(dr[j].ToString());
                sExcelXML.Append("</td>");
            }
            sExcelXML.Append("</tr>");
        }
        sExcelXML.Append("</Table>");
        sExcelXML.Append("</Worksheet>");

        // Close the Workbook tag (in Excel header you can see the Workbook tag)
        sExcelXML.Append("</Workbook>\n");

        SaveExcelXML(sFilename, sExcelXML.ToString());
    }

    protected static string ExcelXMLHeader()
    {
        // Excel header
        StringBuilder sb = new StringBuilder();
        sb.Append("<?xml version=\"1.0\"?>\n");
        sb.Append("<?mso-application progid=\"Excel.Sheet\"?>\n");
        sb.Append("<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" ");
        sb.Append("xmlns:o=\"urn:schemas-microsoft-com:office:office\" ");
        sb.Append("xmlns:x=\"urn:schemas-microsoft-com:office:excel\" ");
        sb.Append("xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" ");
        sb.Append("xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n");
        sb.Append("<DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">");
        sb.Append("</DocumentProperties>");
        sb.Append("<ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">\n");
        sb.Append("<ProtectStructure>False</ProtectStructure>\n");
        sb.Append("<ProtectWindows>False</ProtectWindows>\n");
        sb.Append("</ExcelWorkbook>\n");

        return sb.ToString();
    }

    /// <summary>
    /// Read styles and copy it to the Excel string
    /// </summary>
    /// <param name="filename">Styles.config</param>
    /// <returns></returns>
    protected static string ExcelXMLStyles(string filename)
    {
        StreamReader sr;
        string s;
        StringBuilder sFileText = new StringBuilder();
        sr = File.OpenText(filename);
        s = sr.ReadLine();
        sFileText.Append(s);
        while (s != null)
        {
            s = sr.ReadLine();
            sFileText.Append(s + "\n");
        }
        sr.Close();
        return sFileText.ToString();
    }

    protected static string ExcelXMLWorkSheetOptions()
    {
        // This is Required Only Once ,	But this has to go after the First Worksheet's First Table		
        StringBuilder sb = new StringBuilder();
        sb.Append("<WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\n<Selected/>\n </WorksheetOptions>\n");
        return sb.ToString();
    }

    protected static void SaveExcelXML(string sFilename, string sExcelXML)
    {
        File.Delete(sFilename);
        //StreamWriter sw2 = new StreamWriter(sFilename + ".txt", true, System.Text.Encoding.Unicode);
        //sw2.Write(sExcelXML);
        //sw2.Close();

        StreamWriter sw = new StreamWriter(sFilename, true, System.Text.Encoding.Unicode);
        sw.Write(ConvertHTMLToExcelXML(sExcelXML));
        sw.Close();
    }

    protected static string ConvertHTMLToExcelXML(string strHtml)
    {

        // Just to replace TR with Row
        strHtml = strHtml.Replace("<tr>", "<Row ss:AutoFitHeight=\"1\" >\n");
        strHtml = strHtml.Replace("</tr>", "</Row>\n");

        //replace the cell tags
        strHtml = strHtml.Replace("<td>", "<Cell><Data ss:Type=\"String\">");
        strHtml = strHtml.Replace("</td>", "</Data></Cell>\n");

        return strHtml;
    }
}
