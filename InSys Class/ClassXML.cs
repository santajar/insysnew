using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;

namespace InSysClass
{
    public class XmlExport
    {
        /// <summary>
        /// Initialize XML export object.
        /// </summary>
        /// <param name="sWorksheetName">Worksheet name</param>
        public XmlExport(string sWorksheetName)
        {
            sWorksheetStart = "<Worksheet ss:Name='" + sWorksheetName + "'>\n";
            sWorksheetEnd = "</Worksheet>\n";
        }

        #region XML Header
        private string sXmlVersion = "<?xml version='1.0'?>\n";
        private string sMsoApplication = "<?mso-application progid='Excel.Sheet'?>\n";
        #endregion

        #region Workbook
        private string sWorkbookStart = "<Workbook\n" +
            "\txmlns='urn:schemas-microsoft-com:office:spreadsheet'\n" +
            "\txmlns:o='urn:schemas-microsoft-com:office:office'\n" +
            "\txmlns:x='urn:schemas-microsoft-com:office:excel'\n" +
            "\txmlns:ss='urn:schemas-microsoft-com:office:spreadsheet'>\n";
        private string sWorkbookEnd = "</Workbook>\n";
        #endregion

        #region Style
        private string sStyleStart = "<Styles>\n" +
            "\t<Style ss:ID='T'>\n" +
                "\t\t<Font ss:Bold='1'/>" + "<Interior ss:Color='#AAAAAA' ss:Pattern='Solid'/>\n" +
            "\t</Style>\n" +
            "\t<Style ss:ID='C'>\n" +
                "\t\t<Alignment ss:Horizontal='Left'/>\n" +
            "\t</Style>\n";
        private string sStyleEnd = "</Styles>\n";
        #endregion

        #region Document Properties
        private string sDocumentProperties = "";
        private string sAuthor = "";
        private string sLastAuthor = "";
        private string sCreationTime = "";
        private string sCompanyName = "";
        private string sProductVersion = "";


        /// <summary>
        /// Name of author.
        /// </summary>
        public string Author { set { sAuthor = sLastAuthor = value; } }

        /// <summary>
        /// Date of creation.
        /// </summary>
        public DateTime CreationTime
        {
            set
            {
                sCreationTime = value.ToString("yyyy-MM-dd") + "T" + value.ToString("hh:mm:ss") + "Z";
            }
        }

        /// <summary>
        /// Your company name.
        /// </summary>
        public string CompanyName { set { sCompanyName = value; } }

        /// <summary>
        /// Target Excel version.
        /// </summary>
        public string TargetVersion { set { sProductVersion = value; } }
        #endregion

        #region Worksheet
        private string sWorksheetStart = "";
        private string sWorksheetEnd = "";
        #endregion

        #region Body
        private string sTableStart = "<Table>\n";
        private string sTableEnd = "</Table>\n";
        private StringBuilder sTableColumns = new StringBuilder();
        private StringBuilder sTableRows = new StringBuilder();
        #endregion

        /// <summary>
        /// Generates Document Properties tag.
        /// </summary>
        /// <returns>Document Properties string</returns>
        private string sGenerateDocumentProperties()
        {
            return sDocumentProperties = "<DocumentProperties xmlns='urn:schemas-microsoft-com:office:office'>\n" +
                "\t<Author>" + sAuthor + "</Author>\n" +
                "\t<LastAuthor>" + sAuthor + "</LastAuthor>\n" +
                "\t<Created>" + sCreationTime + "</Created>\n" +
                "\t<Company>" + sCompanyName + "</Company>\n" +
                "\t<Version>" + sProductVersion + "</Version>\n" +
                "</DocumentProperties>\n";
        }

        /// <summary>
        /// Generates XML to be writen in a file.
        /// </summary>
        /// <returns>XML string</returns>
        private string sGenerateExcelXml()
        {
            return
                sXmlVersion +
                sMsoApplication +
                sWorkbookStart +
                    sGenerateDocumentProperties() +
                    sStyleStart +
                    sStyleEnd +
                    sWorksheetStart +
                        sTableStart +
                            sTableColumns +
                            sTableRows +
                        sTableEnd +
                    sWorksheetEnd +
                sWorkbookEnd;
        }

        /// <summary>
        /// Set columns for XML spreadsheet.
        /// </summary>
        /// <param name="oColumns">Column collection</param>
        private void SetColumn(XmlColumn[] oColumns)
        {
            foreach (XmlColumn oCol in oColumns)
                sTableColumns.Append("<Column ss:Width='" + oCol.Width.ToString() + "'/>\n");
        }

        /// <summary>
        /// Set titles by content columns name.
        /// </summary>
        /// <param name="oContent">SQL data adapter object</param>
        private void SetTitle(SqlDataReader oContent)
        {
            sTableRows.Append("<Row ss:Index='1'>\n");
            for (int iCell = 0; iCell < oContent.FieldCount; iCell++)
                sTableRows.Append(
                    "\t<Cell ss:StyleID='T' ss:Index='" + (iCell + 1).ToString() + "'>\n" +
                        "\t\t<Data ss:Type='String'>" + oContent.GetName(iCell) + "</Data>\n" +
                    "\t</Cell>\n");
            sTableRows.Append("</Row>\n");
        }

        /// <summary>
        /// Set rows for XML spreadsheet.
        /// </summary>
        /// <param name="oColumns">Column collection</param>
        /// <param name="oContent">SQL data adapter object</param>
        private void SetContent(XmlColumn[] oColumns, SqlDataReader oContent)
        {
            for (int iLine = 2; oContent.Read(); iLine++)
            {
                sTableRows.Append("<Row ss:Index='" + iLine.ToString() + "'>\n");
                for (int iCell = 0; iCell < oContent.FieldCount; iCell++)
                    sTableRows.Append(
                        "\t<Cell ss:StyleID='C' ss:Index='" + (iCell + 1).ToString() + "'>\n" +
                            "\t\t<Data ss:Type='" + oColumns[iCell].Type.ToString() + "'>" + oContent.GetValue(iCell).ToString() + "</Data>\n" +
                        "\t</Cell>\n");
                sTableRows.Append("</Row>\n");
            }
        }

        /// <summary>
        /// Write XML to a file.
        /// </summary>
        /// <param name="oColumns">Column collection</param>
        /// <param name="oContent">SQL data adapter object</param>
        /// <param name="sFilePath">File path</param>
        public void Write(XmlColumn[] oColumns, SqlDataReader oContent, string sFilePath)
        {
            SetColumn(oColumns);
            SetTitle(oContent);
            SetContent(oColumns, oContent);

            File.WriteAllText(sFilePath, sGenerateExcelXml());
        }
    }

    public class XmlImport
    {
        /// <summary>
        /// Initialize XML import object.
        /// </summary>
        /// <param name="sFilePath">File path to import</param>
        public XmlImport(string sFilePath)
        {
            this.sFilePath = sFilePath;
        }

        private string sFilePath;

        private void SetImportColumns(XmlType[] eType, XmlTextReader oReader, ref DataTable oTable)
        {
            int iType = 0;

            while (oReader.Read())
                if (oReader.NodeType == XmlNodeType.Element && oReader.Name == "Row")
                    break;

            while (oReader.Read())
                if (oReader.NodeType == XmlNodeType.EndElement && oReader.Name == "Row")
                    break;
                else if (oReader.NodeType == XmlNodeType.Text)
                    oTable.Columns.Add(oReader.Value, eType[iType++] == XmlType.Number ? (1).GetType() : "".GetType());
        }

        private void SetImportRows(XmlTextReader oReader, ref DataTable oTable, string sKey)
        {
            List<string> oString = new List<string>();

            bool isMatch = false;
            while (oReader.Read())
            {
                if (oReader.NodeType == XmlNodeType.Element && oReader.Name == "Data" && oReader.Read())
                {
                    string sCell = oReader.Value == null ? "" : oReader.Value;
                    if (sKey != null && !isMatch && sCell.ToUpper().Contains(sKey.ToUpper())) isMatch = true;
                    oString.Add(sCell);
                }
                else if (oReader.NodeType == XmlNodeType.EndElement && oReader.Name == "Row")
                {
                    if (sKey == null || isMatch)
                    {
                        oTable.Rows.Add(oString.ToArray());
                        isMatch = false;
                    }
                    oString.Clear();
                }
            }
        }

        private DataTable GetImportDataSource(XmlType[] eType, XmlTextReader oReader, string sKey)
        {
            DataTable oTable = new DataTable();

            SetImportColumns(eType, oReader, ref oTable);
            SetImportRows(oReader, ref oTable, sKey);

            return oTable;
        }

        private DataTable GetImportDataSource(XmlType[] eType, XmlTextReader oReader)
        {
            return GetImportDataSource(eType, oReader, null);
        }

        /// <summary>
        /// Read specified XML spreadsheet file and then fill the data to a table.
        /// </summary>
        /// <param name="eColumnsType">Columns type collection</param>
        /// <returns>DataTable as datasource.</returns>
        public DataTable Read(XmlType[] eColumnsType)
        {
            return GetImportDataSource(eColumnsType, new XmlTextReader(sFilePath));
        }

        public DataTable Read(XmlType[] eColumnsType, string sKey)
        {
            return GetImportDataSource(eColumnsType, new XmlTextReader(sFilePath), sKey);
        }
    }

    public class XmlColumn
    {
        /// <summary>
        /// Initialize XML column.
        /// </summary>
        /// <param name="iWidth">Column width</param>
        /// <param name="oType">Column type</param>
        public XmlColumn(int iWidth, XmlType oType)
        {
            this.iWidth = iWidth;
            this.oType = oType;
        }

        private int iWidth;
        public int Width
        {
            set { iWidth = value; }
            get { return iWidth; }
        }

        private XmlType oType;
        public XmlType Type
        {
            set { oType = value; }
            get { return oType; }
        }
    }

    /// <summary>
    /// Custom data types enum used in XML column.
    /// </summary>
    [Flags]
    public enum XmlType
    {
        Number,
        String
    }
}
