using System;
using Microsoft.Win32;

namespace InSysClass
{
    public class RegIP
    {
        public static void CreateAndSetKey(string sKeyName, string sKeyValue, int iRegType, string sAppName)
        {
            Microsoft.Win32.RegistryKey regVersion = null;
            switch (iRegType)
            {
                case 1:
                    regVersion = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("SOFTWARE\\\\Integra\\\\" + sAppName + "\\\\");
                    break;
                case 2:
                    regVersion = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\\\\Integra\\\\" + sAppName + "\\\\");
                    break;
            }

            if (((regVersion != null)))
            {
                regVersion.SetValue(sKeyName, sKeyValue);
                regVersion.Close();
            }
        }

        public static string sGetKeyValue(string sKeyName, int iRegType, string sAppName)
        {
            RegistryKey regVersion = null;
            string sRegVal = null;

            switch (iRegType)
            {
                case 1:
                    regVersion = Registry.LocalMachine.OpenSubKey("SOFTWARE\\\\Integra\\\\" + sAppName + "\\\\", true);
                    break;
                case 2:
                    regVersion = Registry.CurrentUser.OpenSubKey("SOFTWARE\\\\Integra\\\\" + sAppName + "\\\\", true);
                    break;
            }

            if (regVersion == null)
            {
                //MessageBox.Show("Missing or corrupt file.\n" + "Please repeat the installation.");
            }
            else if (regVersion.GetValue(sKeyName) != null)
            {
                sRegVal = regVersion.GetValue(sKeyName).ToString();
            }
            return sRegVal;
        }

        public static void DeleteKey(string sKeyName, int iRegType, string sAppName)
        {
            try
            {
                RegistryKey key = null;
                switch (iRegType)
                {
                    case 1:
                        key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\\\Integra\\\\" + sAppName + "\\\\", true);
                        break;
                    case 2:
                        key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\\\Integra\\\\" + sAppName + "\\\\", true);
                        break;
                }
                key.DeleteValue(sKeyName);
            }
            catch (Exception)
            {
            }
        }
    }
}