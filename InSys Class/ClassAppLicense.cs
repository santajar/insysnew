using System;
using System.IO;
using Ini;

namespace InSysClass
{
    public class InitAppLicense
    {
        protected string sUser;
        protected string sCompany;
        protected string sLicenseKey;
        //protected string sLicenseDate;
        //protected string sActivationKey;
        //protected string sHarddisk;

        protected string sFileName;
        IniFile oLicenseFile;

        public InitAppLicense(string _sFileName)
        {
            sFileName = _sFileName;
            oLicenseFile = new IniFile(sFileName);
            if (File.Exists(sFileName))
                doGetInitLicense();
            else
            {
                InitialFile();
            }
        }

        protected void InitialFile()
        {
            oLicenseFile.IniWriteValue("License", "User", "");
            oLicenseFile.IniWriteValue("License", "Company", "");
            oLicenseFile.IniWriteValue("License", "License Key", "");
            //sw.WriteLine("License Date=");
            //sw.WriteLine("Type=");
            //sw.WriteLine("Version=");
        }

        protected void doGetInitLicense()
        {
            try
            {
                sUser = oLicenseFile.IniReadValue("License", "User");
                sCompany = oLicenseFile.IniReadValue("License", "Company");
                sLicenseKey = oLicenseFile.IniReadValue("License", "License Key");

                //sLicenseType = oObjLicenseFile.IniReadValue("License", "Type");
                //sLicenseDate = oObjLicenseFile.IniReadValue("License", "License Date");
                //sHarddisk = oObjLicenseFile.IniReadValue("License", "SN Harddisk");
                //if (sLicenseType == "0")
                //    sLicenseKey = oObjLicenseFile.IniReadValue("License", "License Key");
                //else
                //    sActivationKey = oObjLicenseFile.IniReadValue("License", "Activation Key");
            }
            catch (Exception)
            {
                //throw new InstallException("Install Error doGetInitLicense : " + ex.Message);
            }
        }

        //public int iGetLicenseType()
        //{
        //    return Convert.ToInt16(sLicenseType);
        //}

        public string sGetUser()
        {
            return sUser;
        }

        public string sGetCompany()
        {
            return sCompany;
        }

        public string sGetLicenseKey()
        {
            return sLicenseKey;
        }

        //public DateTime dtInstallDate(CultureInfo ciTemp)
        //{
        //    return Convert.ToDateTime(sLicenseDate, ciTemp);
        //}

        public void doChangeUser(string _sUser)
        {
            oLicenseFile.IniWriteValue("License", "User", _sUser);
        }

        public void doChangeCompany(string _sCompany)
        {
            oLicenseFile.IniWriteValue("License", "Company", _sCompany);
        }

        public void doChangeLicenseKey(string _sLicense)
        {
            oLicenseFile.IniWriteValue("License", "License Key", _sLicense);
        }

        //public void doChangeLicenseDate(DateTime _dtLicense)
        //{
        //    oObjLicenseFile.IniWriteValue("License", "License Date", _dtLicense.ToString("dd/MM/yyyy"));
        //}

        //public void doChangeLicenseDate(string _sLicenseDate)
        //{
        //    oObjLicenseFile.IniWriteValue("License", "License Date", _sLicenseDate);
        //}

        //public void doChangeType(int _iType)
        //{
        //    oObjLicenseFile.IniWriteValue("License", "Type", _iType.ToString());
        //}

        //public void doChangeExpiryPeriod(int _iExpPeriod)
        //{
        //    oObjLicenseFile.IniWriteValue("License", "Expiry Period", _iExpPeriod.ToString());
        //}
    }
}