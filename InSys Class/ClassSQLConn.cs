using System.Data.SqlClient;
using System.Data;

namespace InSysClass
{
    public class SQLConnLib
    {
        public static string sConnString(string sServer, string sDatabaseName, string sUID, string sPwd)
        {
            string sConn = null;
            sConn += "Server=" + sServer + ";";
            sConn += "Database=" + sDatabaseName + ";";
            sConn += "UID=" + sUID + ";";
            sConn += "Password=" + sPwd + ";";
            return sConn;
        }

        public static string sConnStrSQLAuth(string sServer, string sDatabaseName, string sUID, string sPwd)
        {
            string sConn = null;
            sConn += "Data Source=" + sServer;
            sConn += ";Initial Catalog=" + sDatabaseName;
            sConn += ";UID=" + sUID;
            sConn += ";Password=" + sPwd + ";";
            return sConn;
        }

        public static string sConnStrWindowsAuth(string sDatabaseName, string sServer)
        {
            string sConn = null;
            sConn += "Persist Security Info=False;";
            sConn += "Integrated Security=SSPI;";
            sConn += "Database=" + sDatabaseName + ";";
            sConn += "Server=" + sServer + ";";
            return sConn;
        }

        public static SqlConnection EstablishConnection(string sConnString)
        {
            SqlConnection oConn = new SqlConnection(sConnString);
            if (oConn.State == ConnectionState.Closed) oConn.Open();
            return oConn;
        }

        public static string sConnStringAsyncMARS(string sServer, string sDatabaseName, string sUID, string sPwd)
        {
            string sConn = null;
            sConn += "Server=" + sServer + ";";
            sConn += "Database=" + sDatabaseName + ";";
            sConn += "UID=" + sUID + ";";
            sConn += "Password=" + sPwd + ";";
            sConn += "MultipleActiveResultSets=True;";
            sConn += "Asynchronous Processing=True;";
            return sConn;
        }

        public static string sConnStringAsyncMARSMaxPool(string sServer, string sDatabaseName, string sUID, string sPwd)
        {
            return sConnStringAsyncMARSMaxPool(sServer, sDatabaseName, sUID, sPwd, 1024);
        }

        public static string sConnStringAsyncMARSMaxPool(string sServer, string sDatabaseName, string sUID, string sPwd, int iMaxPool)
        {
            string sConn = null;
            sConn += "Server=" + sServer + ";";
            sConn += "Database=" + sDatabaseName + ";";
            sConn += "UID=" + sUID + ";";
            sConn += "Password=" + sPwd + ";";
            sConn += "MultipleActiveResultSets=True;";
            sConn += "Asynchronous Processing=True;";
            sConn += string.Format("Max Pool Size={0};Pooling=true;", iMaxPool);
            return sConn;
        }
        public static string sConnStringAsyncMARSMaxPool(string sConnString)
        {
            string sConn = null;
            sConn += "MultipleActiveResultSets=True;";
            sConn += "Asynchronous Processing=True;";
            sConn += string.Format("Max Pool Size={0};Pooling=true;", 1024);
            sConn += sConnString;
            return sConn;
        }
    }
}