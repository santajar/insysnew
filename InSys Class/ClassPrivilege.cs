﻿using System;

/* How to:
 * - set user privilege:
 *      LoginData.sPrivilege = "THE_STRING";
 * - validate user privilege:
 *      if(LoginData.IsAllowed(PrivilegeCode.ZZ)) { ... }                   //for ZZ00 format
 *      if(LoginData.IsAllowed(PrivilegeCode.YY, Privilege.Add)) { ... }    //for YY03111 format
 */
namespace InSysClass
{
    public static class UserPrivilege
    {
        private static string _sPrivilege;
        public static string sPrivilege { set { _sPrivilege = value; } }

        /// <summary>
        /// Returns true if privilege code is found in the user's privilege.
        /// </summary>
        /// <param name="ePrivilegeCode">Code of privilege</param>
        public static bool IsAllowed(PrivilegeCode ePrivilegeCode)
        {
            return string.IsNullOrEmpty(_sPrivilege) ? false : _sPrivilege.Contains(ePrivilegeCode.ToString());
        }

        /// <summary>
        /// Returns true if privilege code is found in the user's privilege.
        /// </summary>
        /// <param name="ePrivilegeCode">Code of privilege</param>
        /// <param name="ePrivilege">Type of privilege</param>
        public static bool IsAllowed(PrivilegeCode ePrivilegeCode, Privilege ePrivilege)
        {
            return
                IsAllowed(ePrivilegeCode) && _sPrivilege[_sPrivilege.IndexOf(ePrivilegeCode.ToString()) + ePrivilegeCode.ToString().Length + (int)ePrivilege] == '1';
        }
        public static bool IsAllowedWeb(PrivilegeCode ePrivilegeCode, PrivilegeWeb ePrivilege)
        {
            char value = _sPrivilege[ePrivilegeCode.ToString().Length + 2 + (int)ePrivilege];
            return
                IsAllowed(ePrivilegeCode) && value == '1';
        }
    }

    /// <summary>
    /// Used as a #define for privilege digit length. Example: XX03111 -> length of 03 = 2
    /// </summary>
    public enum PrivilegeDigit
    {
        Length = 2
    }

    /// <summary>
    /// Types of privilege.
    /// </summary>
    [Flags]
    public enum Privilege
    {
        Add = PrivilegeDigit.Length,
        Edit,
        Delete
    }

    /// <summary>
    /// Types of privilege.
    /// </summary>
    [Flags]
    public enum PrivilegeWeb
    {
        chkAccess,
        chkdtMonitoring,
        chkdtMonitoringExport,
        chkdtMonitoringDelete,
        chkHbMonitoring,
        chkHbMonitoringExport,
        chkHbMonitoringDelete,
        chkUserManagement,
        chkUploadData
    }
    public enum PrivilageCodeMAE
    //public int PrivilageCodeMAE
    {
    VN = 03, //Validasi Nasabah
    PG = 04, //Penarikan Gaji
    MS = 05,//Mini Satement
    LT = 06, //Log Trx
    CC = 07, //Check Cass
    RH = 08, //Raplenish
    IS = 09, //Inquiry Saldo
    LN = 10, //LKN
    RT= 11 , //Rekap Trx
    AN = 12, //Admin

    }

    /// <summary>
    /// The privilege code 
    /// </summary>
    [Flags]
    public enum PrivilegeCode
    {
        IA, //"Allow Init"
        IC, //"Compress Init"
        SN, //"SN Valid"
        AT, //"Audit Trail"
        IT, //"Initialize Trail"
        DU, //"Data Upload"
        UM, //"User Management"
        NP, //"CreateTerminal"
        RP, //"Register Old File"
        AI, //"AID Management"
        PK, //"Key Management"
        RC, //"Card Range"
        DE, //"Terminal"
        AA, //"Acquirer"
        AE, //"Issuer"
        AC, //"Card"
        CT, //Add Card On Terminal
        SU, //"Super User"        
        OI, //"Auto Init"
        TL, //"TLE"
        SW, //Software Package
        GP, //GPRS Ip Setting
        CR, //Currency DCC
        SS, //Schedule Software
        QR, //Query Report
        PP, //PinPad
        RD, //Remote Download
        WA, //Web Audit Trail
        WS, //Web Software Trail
        EM, //EMV Management
        IP, //Init Picture
        PM, //Promo Management
        IF, //Initial Flazz
        CM, //Card Type Management
        ME, //Edc Monitoring
        RQ, //Request Paper Receipt
        W1, //Edc Monitoring Web Admin
        W2, //Edc Monitoring Web Spv
        W3, //Edc Monitoring Web Op
        W4, //Edc Monitoring Web Guest

        //  unused previlege code
        LP, //Loyalty Product(Citibank)
        KB, //Kode Bank
        KP, //Kode Product
        PE, //"EDC_IP Tracking"
        PT, //"Populate TLV"
        DL, //"Download"
        LO //"Location"

        // */
    }
}