using System.Management;

namespace InSysClass
{
    public class HardDiskLib
    {
        public static string sGetSerialNumber(string DriveLetter)
        {
            ManagementObject wmi_ld = default(ManagementObject);

            string temp = null;
            string[] parts = null;
            string ans = null;

            ans = "";
            wmi_ld = new ManagementObject("Win32_LogicalDisk.DeviceID='" + DriveLetter.TrimEnd('\\') + "'");

            foreach (ManagementObject wmi_dp in wmi_ld.GetRelated("Win32_DiskPartition"))
            {
                // get the associated DiskDrive
                foreach (ManagementObject wmi_dd in wmi_dp.GetRelated("Win32_DiskDrive"))
                {
                    // the serial number is embedded in the PnPDeviceID
                    temp = wmi_dd["PnPDeviceID"].ToString();

                    // ini bagian untuk cek SN USB
                    //If Not temp.StartsWith("USBSTOR") Then
                    // Throw New ApplicationException(DriveLetter & " doesn't appear to be USB Device")
                    //End If
                    //parts = temp.Split("\&".ToCharArray)
                    //' The serial number should be the next to the last element
                    //ans = parts(parts.Length - 2)
                    // selesai

                    parts = temp.Split("\\&".ToCharArray());
                    // The serial number should be the next to the last element
                    ans = parts[parts.Length - 1];
                }
            }

            return ans.ToUpper();
        }
    }
}