using System;

namespace InSysClass
{
    public class ISO8583Lib
    {
        int MAX_BIT = 64;

        public void UnPackISO(byte[] _arrbContent, ref ISO8583_MSGLib isoMSG)
        {
            // receive sample : 
            // 60077782F4080020200100008100109300001000740777444E4133313652540026001044363038343635353536001242434147583533313642414900084243415831303030
            ISO8583_MSGLib isoMsgTemp = new ISO8583_MSGLib();

            // Unpack ISO8583 message dari edc
            GetTPDUAddress(_arrbContent, ref isoMsgTemp);
            string sContent = CommonLib.sByteArrayToHexString(CutLengthTpduAddress(_arrbContent, _arrbContent.Length)).Replace(" ", "");
            int iIndex = 0;
            int iBinIndex = 0;

            isoMsgTemp.MTI = CommonLib.sMid(sContent, iIndex, 4);
            iIndex += 4;

            //Bitmap
            isoMsgTemp.sBit[1] = (CommonLib.sMid(sContent, iIndex, 16));
            isoMsgTemp.sBinaryBitMap = CommonLib.sBitMapToBinString(isoMsgTemp.sBit[1]);
            if (isoMsgTemp.sBinaryBitMap.Substring(0, 1) == "1")
            {
                isoMsgTemp.sBit[1] = CommonLib.sMid(sContent, iIndex, 32);
                isoMsgTemp.sBinaryBitMap = CommonLib.sBitMapToBinString(isoMsgTemp.sBit[1]);
                iIndex += 32;
                MAX_BIT = 128;
            }
            else
                iIndex += 16;

            try
            {
                for (iBinIndex = 1; iBinIndex <= MAX_BIT; iBinIndex++)
                {
                    string s = isoMsgTemp.sBinaryBitMap.Substring(iBinIndex - 1, 1);
                    if (s == "1")
                    {
                        int iTempLen = 0;
                        string sTemp = null;

                        switch (iBinIndex)
                        {
                            #region "Bit 1-64"
                            case 1:
                                break;
                            case 2:
                            case 33:
                            case 44:
                            case 45:
                                iTempLen = CommonLib.iStringToInt(CommonLib.sMid(sContent, iIndex, 2));
                                if (iTempLen % 2 != 0)
                                {
                                    iTempLen += 1;
                                }
                                iIndex += 2;
                                sTemp = CommonLib.sMid(sContent, iIndex, iTempLen);
                                iIndex += iTempLen;
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (i)
                                //{
                                //    case 2:
                                //        isoMsgTemp.sBit[2] = sTemp;
                                //        break;
                                //    case 33:
                                //        isoMsgTemp.sBit[33] = sTemp;
                                //        break;
                                //    case 44:
                                //        isoMsgTemp.sBit[44] = sTemp;
                                //        break;
                                //    case 45:
                                //        isoMsgTemp.sBit[45] = sTemp;
                                //        break;
                                //}
                                break;
                            case 3:
                                // Processing Code
                                isoMsgTemp.sBit[3] = CommonLib.sMid(sContent, iIndex, 6);
                                iIndex += 6;
                                break;
                            case 4:
                            case 5:
                            case 6:
                            case 54:
                                sTemp = CommonLib.sMid(sContent, iIndex, 12);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 4:
                                //        isoMsgTemp.sBit[4] = sTemp;
                                //        break;
                                //    case 5:
                                //        isoMsgTemp.sBit[5] = sTemp;
                                //        break;
                                //    case 6:
                                //        isoMsgTemp.sBit[6] = sTemp;
                                //        break;
                                //    case 54:
                                //        isoMsgTemp.sBit[54] = sTemp;
                                //        break;
                                //}
                                iIndex += 12;
                                break;
                            case 7:
                                isoMsgTemp.sBit[7] = CommonLib.sMid(sContent, iIndex, 10);
                                iIndex += 10;
                                break;
                            case 8:
                            case 9:
                            case 10:
                                sTemp = CommonLib.sMid(sContent, iIndex, 8);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 8:
                                //        isoMsgTemp.sBit[8] = sTemp;
                                //        break;
                                //    case 9:
                                //        isoMsgTemp.sBit[9] = sTemp;
                                //        break;
                                //    case 10:
                                //        isoMsgTemp.sBit[10] = sTemp;
                                //        break;
                                //}
                                iIndex += 8;
                                break;
                            case 11:
                            case 12:
                                sTemp = CommonLib.sMid(sContent, iIndex, 6);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 11:
                                //        isoMsgTemp.sBit[11] = sTemp;
                                //        break;
                                //    case 12:
                                //        isoMsgTemp.sBit[12] = sTemp;
                                //        break;
                                //}
                                iIndex += 6;
                                break;
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                                sTemp = CommonLib.sMid(sContent, iIndex, 4);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 13:
                                //        isoMsgTemp.sBit[13] = sTemp;
                                //        break;
                                //    case 14:
                                //        isoMsgTemp.sBit[14] = sTemp;
                                //        break;
                                //    case 15:
                                //        isoMsgTemp.sBit[15] = sTemp;
                                //        break;
                                //    case 16:
                                //        isoMsgTemp.sBit[16] = sTemp;
                                //        break;
                                //    case 17:
                                //        isoMsgTemp.sBit[17] = sTemp;
                                //        break;
                                //    case 18:
                                //        isoMsgTemp.sBit[18] = sTemp;
                                //        break;
                                //}
                                iIndex += 4;
                                break;
                            case 19:
                            case 20:
                            case 21:
                            case 23:
                            case 24:
                                sTemp = CommonLib.sMid(sContent, iIndex, 4);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 19:
                                //        isoMsgTemp.sBit[19] = sTemp;
                                //        break;
                                //    case 20:
                                //        isoMsgTemp.sBit[20] = sTemp;
                                //        break;
                                //    case 21:
                                //        isoMsgTemp.sBit[21] = sTemp;
                                //        break;
                                //    case 23:
                                //        isoMsgTemp.sBit[23] = sTemp;
                                //        break;
                                //    case 24:
                                //        isoMsgTemp.sBit[24] = sTemp;
                                //        break;
                                //}
                                iIndex += 4;
                                break;

                            case 22:
                                sTemp = CommonLib.sMid(sContent, iIndex, 4);
                                isoMsgTemp.sBit[22] = sTemp;
                                iIndex += 4;
                                break;

                            case 25:
                            case 26:
                                sTemp = CommonLib.sMid(sContent, iIndex, 2);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                iIndex += 2;
                                break;
                            case 27:
                                isoMsgTemp.sBit[27] = CommonLib.sMid(sContent, iIndex, 2);
                                iIndex += 2;
                                break;
                            case 28:
                            case 29: //standartnya disini.
                            case 30:
                            case 31:
                                sTemp = CommonLib.sMid(sContent, iIndex, 8);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 28:
                                //        isoMsgTemp.sBit[28] = sTemp;
                                //        break;
                                //    case 29:
                                //        isoMsgTemp.sBit[29] = sTemp;
                                //        break;
                                //    case 30:
                                //        isoMsgTemp.sBit[30] = sTemp;
                                //        break;
                                //    case 31:
                                //        isoMsgTemp.sBit[31] = sTemp;
                                //        break;
                                //}
                                iIndex += 8;
                                break;
                            case 32:
                                iTempLen = CommonLib.iStringToInt(CommonLib.sMid(sContent, iIndex, 2));
                                if (iTempLen % 2 != 0)
                                {
                                    iTempLen += 1;
                                }
                                iIndex += 2;
                                sTemp = int.Parse(CommonLib.sMid(sContent, iIndex, iTempLen)).ToString();
                                iIndex += iTempLen;
                                isoMsgTemp.sBit[32] = sTemp;
                                break;
                            case 34:
                                iTempLen = CommonLib.iStringToInt(CommonLib.sMid(sContent, iIndex, 2));
                                if (iTempLen % 2 != 0)
                                {
                                    iTempLen += 1;
                                }
                                iIndex += 2;
                                //sTemp = CommonLib.sHexToString(CommonLib.sMid(sContent, iIndex, iTempLen));
                                sTemp = CommonLib.sMid(sContent, iIndex, iTempLen);
                                iIndex += iTempLen;
                                isoMsgTemp.sBit[34] = sTemp;
                                break;
                            case 35:
                                iTempLen = CommonLib.iStringToInt(CommonLib.sMid(sContent, iIndex, 2));
                                iIndex += 2;
                                sTemp = CommonLib.sMid(sContent, iIndex, iTempLen);
                                iIndex += iTempLen;
                                isoMsgTemp.sBit[35] = sTemp;
                                break;
                            case 37:
                                isoMsgTemp.sBit[37] = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, 24));
                                iIndex += 24;
                                break;
                            case 38:
                                isoMsgTemp.sBit[38] = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, 12));
                                iIndex += 12;
                                break;
                            case 39:
                                isoMsgTemp.sBit[39] = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, 4));
                                iIndex += 4;
                                break;
                            case 40:
                                break;
                            case 49:
                            case 50:
                            case 51:
                                sTemp = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, 6));
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 40:
                                //        isoMsgTemp.sBit[40] = sTemp;
                                //        break;
                                //    case 49:
                                //        isoMsgTemp.sBit[49] = sTemp;
                                //        break;
                                //    case 50:
                                //        isoMsgTemp.sBit[50] = sTemp;
                                //        break;
                                //    case 51:
                                //        isoMsgTemp.sBit[51] = sTemp;
                                //        break;
                                //}
                                iIndex += 6;
                                break;
                            case 41:
                                isoMsgTemp.sBit[41] = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, 16));
                                isoMsgTemp.sTerminalId = isoMsgTemp.sBit[41];
                                iIndex += 16;
                                break;

                            case 42:
                                isoMsgTemp.sBit[42] = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, 30));
                                iIndex += 30;
                                break;

                            case 43:
                                isoMsgTemp.sBit[43] = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, 80));
                                iIndex += 80;
                                break;
                            case 36:
                            case 46:
                            case 47:
                            //case 48:
                            case 55:
                            case 56:
                            case 58:
                            case 59:
                            case 60:
                            case 61:
                            case 62:
                            case 63:
                                iTempLen = CommonLib.iStringToInt(CommonLib.sMid(sContent, iIndex, 4));
                                iIndex += 4;
                                sTemp = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, iTempLen * 2));
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                iIndex += (iTempLen * 2);
                                break;
                            case 48:
                                iTempLen = CommonLib.iStringToInt(CommonLib.sMid(sContent, iIndex, 4));
                                iIndex += 4;
                                sTemp = CommonLib.sHexToStringUTF7(CommonLib.sMid(sContent, iIndex, iTempLen * 2));
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                iIndex += (iTempLen * 2);
                                break;
                            case 57: 
                                iTempLen = CommonLib.iStringToInt(CommonLib.sMid(sContent, iIndex, 4));
                                iIndex += 4;
                                //sTemp = CommonLib.sHexToStringUTF7(CommonLib.sMid(sContent, iIndex, iTempLen * 2));
                                //sTemp = CommonLib.sMid(sContent, iIndex, iTempLen * 2);
                                //sTemp = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, iTempLen * 2));
                                sTemp = CommonLib.sHexToStringUTF7Loop(CommonLib.sMid(sContent, iIndex, iTempLen * 2));
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                iIndex += (iTempLen * 2);
                                break;
                            case 52:
                                isoMsgTemp.sBit[52] = CommonLib.sMid(sContent, iIndex, 16);
                                iIndex += 16;
                                break;
                            case 53:
                                isoMsgTemp.sBit[53] = CommonLib.sMid(sContent, iIndex, 18);
                                iIndex += 18;
                                break;
                            case 64:
                                isoMsgTemp.sBit[64] = CommonLib.sMid(sContent, iIndex, 16);
                                iIndex += 16;
                                break;
                            #endregion
                            #region "Bit 65-128"
                            case 65:
                                break;
                            case 66:
                            case 67:
                            case 92:
                                sTemp = CommonLib.sMid(sContent, iIndex, 2);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 66:
                                //        isoMsgTemp.sBit[66] = sTemp;
                                //        break;
                                //    case 67:
                                //        isoMsgTemp.sBit[67] = sTemp;
                                //        break;
                                //    case 92:
                                //        isoMsgTemp.sBit[92] = sTemp;
                                //        break;
                                //}
                                iIndex += 2;
                                break;
                            case 68:
                            case 69:
                            case 70:
                            case 71:
                            case 72:
                                sTemp = CommonLib.sMid(sContent, iIndex, 4);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 68:
                                //        isoMsgTemp.sBit[68] = sTemp;
                                //        break;
                                //    case 69:
                                //        isoMsgTemp.sBit[69] = sTemp;
                                //        break;
                                //    case 70:
                                //        isoMsgTemp.sBit[70] = sTemp;
                                //        break;
                                //    case 71:
                                //        isoMsgTemp.sBit[71] = sTemp;
                                //        break;
                                //    case 72:
                                //        isoMsgTemp.sBit[72] = sTemp;
                                //        break;
                                //}
                                iIndex += 4;
                                break;
                            case 73:
                            case 93:
                                sTemp = CommonLib.sMid(sContent, iIndex, 6);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 73:
                                //        isoMsgTemp.sBit[72] = sTemp;
                                //        break;
                                //    case 93:
                                //        isoMsgTemp.sBit[93] = sTemp;
                                //        break;
                                //}
                                iIndex += 6;
                                break;
                            case 74:
                            case 75:
                            case 76:
                            case 77:
                            case 78:
                            case 79:
                            case 80:
                            case 81:
                                //TxtReceived.Text += i + " : " + Strings.Mid(arrbContent, iIndex, 10) + Constants.vbCrLf;
                                //iIndex += 10;
                                break;
                            case 82:
                            case 83:
                            case 84:
                            case 85:
                                //TxtReceived.Text += i + " : " + Strings.Mid(arrbContent, iIndex, 12) + Constants.vbCrLf;
                                //iIndex += 12;
                                break;
                            case 86:
                            case 87:
                            case 88:
                            case 89:
                            case 97:
                                //TxtReceived.Text += i + " : " + Strings.Mid(arrbContent, iIndex, 16) + Constants.vbCrLf;
                                //iIndex += 16;
                                break;
                            case 90:
                                //TxtReceived.Text += i + " : " + Strings.Mid(arrbContent, iIndex, 42) + Constants.vbCrLf;
                                //iIndex += 42;
                                break;
                            case 91:
                                //TxtReceived.Text += i + " : " + ConvertHexTostr(Strings.Mid(arrbContent, iIndex, 2)) + Constants.vbCrLf;
                                //iIndex += 2;
                                break;
                            case 94:
                                //TxtReceived.Text += i + " : " + ConvertHexTostr(Strings.Mid(arrbContent, iIndex, 14)) + Constants.vbCrLf;
                                //iIndex += 14;
                                break;
                            case 95:
                                //TxtReceived.Text += i + " : " + ConvertHexTostr(Strings.Mid(arrbContent, iIndex, 84)) + Constants.vbCrLf;
                                //iIndex += 84;
                                break;
                            case 96:
                                //TxtReceived.Text += i + " : " + ConvertHexTostr(Strings.Mid(arrbContent, iIndex, 16)) + Constants.vbCrLf;
                                //iIndex += 16;
                                break;
                            case 98:
                                //TxtReceived.Text += i + " : " + ConvertHexTostr(Strings.Mid(arrbContent, iIndex, 50)) + Constants.vbCrLf;
                                //iIndex += 50;
                                break;
                            case 99:
                                isoMsgTemp.sBit[99] = sTemp;
                                break;
                            case 100:
                            case 102:
                            case 103:
                            case 104:
                            case 113:
                                iTempLen = CommonLib.iStringToInt(CommonLib.sMid(sContent, iIndex, 2));
                                if (iTempLen % 2 != 0)
                                {
                                    iTempLen += 1;
                                }
                                iIndex += 2;
                                sTemp = CommonLib.sMid(sContent, iIndex, iTempLen * 2);
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                //switch (iBinIndex)
                                //{
                                //    case 100:
                                //        isoMsgTemp.sBit[100] = sTemp;
                                //        break;
                                //    case 102:
                                //        isoMsgTemp.sBit[102] = sTemp;
                                //        break;
                                //    case 103:
                                //        isoMsgTemp.sBit[103] = sTemp;
                                //        break;
                                //    case 104:
                                //        isoMsgTemp.sBit[104] = sTemp;
                                //        break;
                                //    case 113:
                                //        isoMsgTemp.sBit[113] = sTemp;
                                //        break;
                                //}
                                iIndex += (iTempLen * 2);
                                break;
                            case 105:
                            case 106:
                            case 107:
                            case 108:
                            case 109:
                            case 110:
                            case 111:
                            case 112:
                            case 114:
                            case 115:
                            case 116:
                            case 117:
                            case 118:
                            case 119:
                            case 120:
                            case 121:
                            case 122:
                            case 123:
                            case 124:
                            case 125:
                            case 126:
                            case 127:
                            case 128:
                                iTempLen = CommonLib.iStringToInt(CommonLib.sMid(sContent, iIndex, 4));
                                iIndex += 4;
                                sTemp = CommonLib.sHexToStringUTF8(CommonLib.sMid(sContent, iIndex, iTempLen * 2));
                                isoMsgTemp.sBit[iBinIndex] = sTemp;
                                switch (iBinIndex)
                                {
                                    case 105:
                                    case 106:
                                    case 107:
                                    case 108:
                                    case 109:
                                    case 110:
                                    case 111:
                                    case 112:
                                    case 114:
                                    case 115:
                                    case 116:
                                    case 117:
                                    case 118:
                                    case 119:
                                    case 120:
                                    case 121:
                                    case 122:
                                    case 123:
                                    case 124:
                                    case 125:
                                    case 126:
                                    case 127:
                                    case 128:
                                        break;
                                }
                                iIndex += 4 + (iTempLen * 2);
                                break;
                            #endregion
                        }
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(i + " : " + ex.Message);
            }
            isoMSG = isoMsgTemp;
        }

        public byte[] PackToISO(ISO8583_MSGLib isoMSG)
        {
            ISO8583_MSGLib isoMsgTemp = new ISO8583_MSGLib();
            isoMsgTemp = isoMSG;

            int i = 0;
            string sContent = null;
            sContent += isoMsgTemp.sTPDU;
            sContent += isoMsgTemp.sOriginAddr;
            sContent += isoMsgTemp.sDestNII;            

            sContent += isoMsgTemp.MTI;
            sContent += CommonLib.sBinStringToBitMap(isoMsgTemp.sBinaryBitMap);
            try
            {
                while (CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) >= 0 &&
                    CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) <= MAX_BIT)
                {
                    int iIndex = CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) + 1;
                    int iTempLen = 0;
                    string sTemp = null;
                    i = iIndex;

                    switch (iIndex)
                    {
                        #region "Bit 1-64"
                        case 1:
                            break;
                        case 2:
                        case 32:
                        case 33:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            iTempLen = sTemp.Length;
                            sContent += iTempLen.ToString("00") + sTemp;
                            break;
                        case 3:
                            sTemp = isoMsgTemp.sBit[3];
                            sContent += sTemp;
                            break;
                        case 4:
                        case 5:
                        case 6:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;
                        case 7:
                            sTemp = isoMsgTemp.sBit[7];
                            sContent += sTemp;
                            break;

                        case 8:
                        case 9:
                        case 10:
                        case 28:
                        case 29:
                        case 30:
                        case 31:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 11:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 12:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 13:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 14:
                        case 15:
                        case 16:
                        case 17:
                        case 18:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 19:
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 25:
                        case 26:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 27:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 34:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += iTempLen.ToString("00") + CommonLib.sStringToHex(sTemp);
                            break;

                        case 35:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += iTempLen.ToString("00") + CommonLib.sStringToHex(sTemp);
                            break;

                        case 37:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 38:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 39:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 40:
                            sTemp = "0" + isoMsgTemp.sBit[iIndex];
                            sContent += CommonLib.sStringToHex(sTemp);
                            break;

                        case 44:
                        case 45:
                            break;

                        case 49:
                        case 51:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;
                        case 50:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 41:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 42:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 43:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 36:
                        case 46:
                        case 47:
                        case 54:
                        case 55:
                        case 56:
                        case 58:
                        case 59:
                        case 61:
                        case 62:
                        case 63:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += CommonLib.sConvertDecToHex(iTempLen).PadLeft(4, '0') + sTemp;
                            break;
                        case 48:
                        case 57:
                        case 60: 
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += iTempLen.ToString().PadLeft(4, '0') + sTemp;
                            break;
                        case 52:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 53:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 64:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;
                        #endregion
                        #region "Bit 65-128"
                        case 65:
                            break;
                        case 66:
                        case 67:
                        case 92:
                            sTemp = CommonLib.sMid(sContent, iIndex, 2);
                            switch (i)
                            {
                                case 66:
                                    isoMsgTemp.sBit[66] = sTemp;
                                    break;
                                case 67:
                                    isoMsgTemp.sBit[67] = sTemp;
                                    break;
                                case 92:
                                    isoMsgTemp.sBit[92] = sTemp;
                                    break;
                            }
                            iIndex += 2;
                            break;
                        case 68:
                        case 69:
                        case 70:
                        case 71:
                        case 72:
                            sTemp = CommonLib.sMid(sContent, iIndex, 4);
                            switch (i)
                            {
                                case 68:
                                    isoMsgTemp.sBit[68] = sTemp;
                                    break;
                                case 69:
                                    isoMsgTemp.sBit[69] = sTemp;
                                    break;
                                case 70:
                                    isoMsgTemp.sBit[70] = sTemp;
                                    break;
                                case 71:
                                    isoMsgTemp.sBit[71] = sTemp;
                                    break;
                                case 72:
                                    isoMsgTemp.sBit[72] = sTemp;
                                    break;
                            }
                            iIndex += 4;
                            break;
                        case 73:
                        case 93:
                            switch (i)
                            {
                                case 73:
                                    isoMsgTemp.sBit[72] = CommonLib.sMid(sContent, iIndex, 6);
                                    break;
                                case 93:
                                    isoMsgTemp.sBit[93] = CommonLib.sMid(sContent, iIndex, 6);
                                    break;
                            }
                            iIndex += 6;
                            break;
                        case 74:
                        case 75:
                        case 76:
                        case 77:
                        case 78:
                        case 79:
                        case 80:
                        case 81:
                            break;
                        case 82:
                        case 83:
                        case 84:
                        case 85:
                            //TxtReceived.Text += i + " : " + Strings.Mid(arrbContent, iIndex, 12) + Constants.vbCrLf;
                            //iIndex += 12;
                            break;
                        case 86:
                        case 87:
                        case 88:
                        case 89:
                        case 97:
                            break;
                        case 90:
                            break;
                        case 91:
                            break;
                        case 94:
                            break;
                        case 95:
                            break;
                        case 96:
                            break;
                        case 98:
                            break;
                        case 99:
                            isoMsgTemp.sBit[99] = sTemp;
                            break;
                        case 100:
                        case 102:
                        case 103:
                        case 104:
                        case 113:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += iTempLen.ToString("00") + sTemp;
                            break;

                        case 105:
                        case 106:
                        case 107:
                        case 108:
                        case 109:
                        case 110:
                        case 111:
                        case 112:
                        case 114:
                        case 115:
                        case 116:
                        case 117:
                        case 118:
                        case 119:
                        case 120:
                        case 121:
                        case 122:
                        case 123:
                        case 124:
                        case 125:
                        case 126:
                        case 127:
                        case 128:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += iTempLen.ToString("0000") + sTemp;
                            switch (i)
                            {
                                case 105:
                                case 106:
                                case 107:
                                case 108:
                                case 109:
                                case 110:
                                case 111:
                                case 112:
                                case 114:
                                case 115:
                                case 116:
                                case 117:
                                case 118:
                                case 119:
                                case 120:
                                case 121:
                                case 122:
                                case 123:
                                case 124:
                                case 125:
                                case 126:
                                case 127:
                                case 128:
                                    break;
                            }
                            iIndex += 4 + (iTempLen * 2);
                            break;
                        #endregion
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(i + " : " + ex.Message);
            }
            //int iLenISO = sContent.Length / 2;
            //string sLenISO = CommonLib.sConvertDecToHex(iLenISO);
            //sContent = sLenISO.PadLeft(4,'0') + sContent;

            return CommonLib.HexStringToByteArray(sContent);
        }

        public string PackToISOHex(ISO8583_MSGLib isoMSG)
        {
            return PackToISOHex(isoMSG, 1);
        }

        public string PackToISOHex(ISO8583_MSGLib isoMSG, int iFunction)
        {
            //Function 0 -> Init
            //Function 1 -> download
            //Function 2 -> init Picture

            ISO8583_MSGLib isoMsgTemp = new ISO8583_MSGLib();
            isoMsgTemp = isoMSG;

            int i = 0;
            string sContent = null;
            sContent += isoMsgTemp.sTPDU;
            sContent += isoMsgTemp.sOriginAddr;
            sContent += isoMsgTemp.sDestNII;
            string s = isoMSG.sBinaryBitMap;
            sContent += isoMsgTemp.MTI;
           //isoMsgTemp.sBinaryBitMap = "0010000000111000000000010000000000000010000000010000000001010000";
            sContent += CommonLib.sBinStringToBitMap(isoMsgTemp.sBinaryBitMap);
            try
            {
                while (CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) >= 0 &&
                    CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) <= MAX_BIT)
                {
                    int iIndex = CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) + 1;
                    int iTempLen = 0;
                    string sTemp = null;
                    i = iIndex;

                    switch (iIndex)
                    {
                        #region "Bit 1-64"
                        case 1:
                            break;
                        case 2:
                        case 32:
                        case 33:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            iTempLen = sTemp.Length;
                            sContent += iTempLen.ToString("00") + sTemp;
                            break;
                        case 3:
                            sTemp = isoMsgTemp.sBit[3];
                            sContent += sTemp;
                            break;
                        case 4:
                        case 5:
                        case 6:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;
                        case 7:
                            sTemp = isoMsgTemp.sBit[7];
                            sContent += sTemp;
                            break;

                        case 8:
                        case 9:
                        case 10:
                        case 28:
                        case 29:
                        case 30:
                        case 31:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 11:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 12:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 13:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 14:
                        case 15:
                        case 16:
                        case 17:
                        case 18:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 19:
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 25:
                        case 26:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 27:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 34:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += iTempLen.ToString("00") + CommonLib.sStringToHex(sTemp);
                            break;

                        case 35:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += iTempLen.ToString("00") + CommonLib.sStringToHex(sTemp);
                            break;

                        case 37:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 38:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 39:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 40:
                            sTemp = "0" + isoMsgTemp.sBit[iIndex];
                            sContent += CommonLib.sStringToHex(sTemp);
                            break;

                        case 44:
                        case 45:
                            break;

                        case 49:
                        case 51:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;
                        case 50:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 41:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            //sTemp = "4252493130363031";
                            sContent += sTemp;
                            break;

                        case 42:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 43:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;
                        case 61:
                            //ini untuk crc u=di remote download
                            iTempLen = isoMsgTemp.sBit[iIndex].Length / 2;
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += iTempLen.ToString().PadLeft(4, '0') + sTemp;
                            //Console.WriteLine("CRC : {0} ", sTemp);
                            break;
                        case 36:
                        case 46:
                        case 47:
                        case 54:
                        case 56:
                        case 58:
                        case 59:
                        
                        case 62:
                        case 63:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += CommonLib.sConvertDecToHex(iTempLen).PadLeft(4, '0') + sTemp;
                            break;
                        case 57:
                            if (iFunction == 1)
                            {
                                iTempLen = 100;
                                int iIndexBit57;
                                if (isoMsgTemp.sBit[iIndex].Substring(0, 4) == "0000")
                                    iIndexBit57 = 12;
                                else
                                    iIndexBit57 = 8;

                                sTemp = "0100" + isoMsgTemp.sBit[iIndex].Substring(0, iIndexBit57);
                                sTemp += CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57, int.Parse(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57 - 4, 4))));
                                iIndexBit57 = iIndexBit57 + int.Parse(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57 - 4, 4));
                                sTemp += isoMsgTemp.sBit[iIndex].Substring(iIndexBit57, 8);
                                iIndexBit57 = iIndexBit57 + 8;
                                sTemp += CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57, int.Parse(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57 - 4, 4))));
                                iIndexBit57 = iIndexBit57 + int.Parse(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57 - 4, 4));

                                int iStart = 0, iTotalCount = 200 - sTemp.Length + 4;
                                string sFiller = "";
                                while (iStart < iTotalCount)
                                {
                                    sFiller = sFiller + "F";
                                    iStart += 1;
                                }
                                sTemp += sFiller;
                                sContent += sTemp;
                            }
                            else if (iFunction == 2)
                            {
                                iTempLen = isoMsgTemp.sBit[iIndex].Length / 2;
                                sTemp = isoMsgTemp.sBit[iIndex];
                                sContent += iTempLen.ToString().PadLeft(4, '0') + sTemp;
                            }
                            else if (iFunction == 3)
                            {
                                iTempLen = isoMsgTemp.sBit[iIndex].Length;
                                sTemp = isoMsgTemp.sBit[iIndex];
                                sContent += (iTempLen / 2).ToString().PadLeft(4, '0') + sTemp;
                            }
                            else
                            {
                                iTempLen = isoMsgTemp.sBit[iIndex].Length;
                                sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                                sContent += CommonLib.sConvertDecToHex(iTempLen).PadLeft(4, '0') + sTemp;
                            }
                            break;
                        case 48:
                        case 55:
                        case 60:
                            //the content is on HexString Format
                            iTempLen = isoMsgTemp.sBit[iIndex].Length / 2;
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += iTempLen .ToString().PadLeft(4, '0') + sTemp;
                            break;
                        case 52:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 53:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 64:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;
                        #endregion
                        #region "Bit 65-128"
                        case 65:
                            break;
                        case 66:
                        case 67:
                        case 92:
                            sTemp = CommonLib.sMid(sContent, iIndex, 2);
                            switch (i)
                            {
                                case 66:
                                    isoMsgTemp.sBit[66] = sTemp;
                                    break;
                                case 67:
                                    isoMsgTemp.sBit[67] = sTemp;
                                    break;
                                case 92:
                                    isoMsgTemp.sBit[92] = sTemp;
                                    break;
                            }
                            iIndex += 2;
                            break;
                        case 68:
                        case 69:
                        case 70:
                        case 71:
                        case 72:
                            sTemp = CommonLib.sMid(sContent, iIndex, 4);
                            switch (i)
                            {
                                case 68:
                                    isoMsgTemp.sBit[68] = sTemp;
                                    break;
                                case 69:
                                    isoMsgTemp.sBit[69] = sTemp;
                                    break;
                                case 70:
                                    isoMsgTemp.sBit[70] = sTemp;
                                    break;
                                case 71:
                                    isoMsgTemp.sBit[71] = sTemp;
                                    break;
                                case 72:
                                    isoMsgTemp.sBit[72] = sTemp;
                                    break;
                            }
                            iIndex += 4;
                            break;
                        case 73:
                        case 93:
                            switch (i)
                            {
                                case 73:
                                    isoMsgTemp.sBit[72] = CommonLib.sMid(sContent, iIndex, 6);
                                    break;
                                case 93:
                                    isoMsgTemp.sBit[93] = CommonLib.sMid(sContent, iIndex, 6);
                                    break;
                            }
                            iIndex += 6;
                            break;
                        case 74:
                        case 75:
                        case 76:
                        case 77:
                        case 78:
                        case 79:
                        case 80:
                        case 81:
                            break;
                        case 82:
                        case 83:
                        case 84:
                        case 85:
                            //TxtReceived.Text += i + " : " + Strings.Mid(arrbContent, iIndex, 12) + Constants.vbCrLf;
                            //iIndex += 12;
                            break;
                        case 86:
                        case 87:
                        case 88:
                        case 89:
                        case 97:
                            break;
                        case 90:
                            break;
                        case 91:
                            break;
                        case 94:
                            break;
                        case 95:
                            break;
                        case 96:
                            break;
                        case 98:
                            break;
                        case 99:
                            isoMsgTemp.sBit[99] = sTemp;
                            break;
                        case 100:
                        case 102:
                        case 103:
                        case 104:
                        case 113:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += iTempLen.ToString("00") + sTemp;
                            break;

                        case 105:
                        case 106:
                        case 107:
                        case 108:
                        case 109:
                        case 110:
                        case 111:
                        case 112:
                        case 114:
                        case 115:
                        case 116:
                        case 117:
                        case 118:
                        case 119:
                        case 120:
                        case 121:
                        case 122:
                        case 123:
                        case 124:
                        case 125:
                        case 126:
                        case 127:
                        case 128:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += iTempLen.ToString("0000") + sTemp;
                            switch (i)
                            {
                                case 105:
                                case 106:
                                case 107:
                                case 108:
                                case 109:
                                case 110:
                                case 111:
                                case 112:
                                case 114:
                                case 115:
                                case 116:
                                case 117:
                                case 118:
                                case 119:
                                case 120:
                                case 121:
                                case 122:
                                case 123:
                                case 124:
                                case 125:
                                case 126:
                                case 127:
                                case 128:
                                    break;
                            }
                            iIndex += 4 + (iTempLen * 2);
                            break;
                        #endregion
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(i + " : " + ex.Message);
            }

            return sContent;
        }

        //MAE ConsoleSoftware
        public string PackToISOHexSoftwareListDownload(ISO8583_MSGLib isoMSG)
        {
            //Perbedaan dgn PackToISOHex di BIT 60 (schedule type+ schedule time+ len app + app name) dimana schedule type, schedule time, len app, tidak di convert menjadi hex
            ISO8583_MSGLib isoMsgTemp = new ISO8583_MSGLib();
            isoMsgTemp = isoMSG;

            int i = 0;
            string sContent = null;
            sContent += isoMsgTemp.sTPDU;
            sContent += isoMsgTemp.sOriginAddr;
            sContent += isoMsgTemp.sDestNII;

            sContent += isoMsgTemp.MTI;
            sContent += CommonLib.sBinStringToBitMap(isoMsgTemp.sBinaryBitMap);
            try
            {
                while (CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) >= 0 &&
                    CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) <= MAX_BIT)
                {
                    int iIndex = CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) + 1;
                    int iTempLen = 0;
                    string sTemp = null;
                    i = iIndex;

                    switch (iIndex)
                    {
                        #region "Bit 1-64"
                        case 1:
                            break;
                        case 2:
                        case 32:
                        case 33:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            iTempLen = sTemp.Length;
                            sContent += iTempLen.ToString("00") + sTemp;
                            break;
                        case 3:
                            sTemp = isoMsgTemp.sBit[3];
                            sContent += sTemp;
                            break;
                        case 4:
                        case 5:
                        case 6:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;
                        case 7:
                            sTemp = isoMsgTemp.sBit[7];
                            sContent += sTemp;
                            break;

                        case 8:
                        case 9:
                        case 10:
                        case 28:
                        case 29:
                        case 30:
                        case 31:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 11:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 12:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 13:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 14:
                        case 15:
                        case 16:
                        case 17:
                        case 18:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 19:
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 25:
                        case 26:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 27:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 34:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += iTempLen.ToString("00") + CommonLib.sStringToHex(sTemp);
                            break;

                        case 35:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += iTempLen.ToString("00") + CommonLib.sStringToHex(sTemp);
                            break;

                        case 37:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 38:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 39:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 40:
                            sTemp = "0" + isoMsgTemp.sBit[iIndex];
                            sContent += CommonLib.sStringToHex(sTemp);
                            break;

                        case 44:
                        case 45:
                            break;

                        case 49:
                        case 51:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;
                        case 50:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 41:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 42:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 43:
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += sTemp;
                            break;

                        case 36:
                        case 46:
                        case 47:
                        case 54:
                        case 55:
                        case 56:
                        case 57:
                        //case 58:
                        case 59:
                        //case 60:
                        case 61:
                        case 62:
                        case 63:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += CommonLib.sConvertDecToHex(iTempLen).PadLeft(4, '0') + sTemp;
                            break;
                        case 60:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            int iIndexLength = 10;
                            int iLenMessage = 5;
                            string sContentTemp = isoMsgTemp.sBit[iIndex].Substring(0, iIndexLength);
                            while (iIndexLength < iTempLen)
                            {
                                int iTempLenApp = int.Parse(isoMsgTemp.sBit[iIndex].Substring(iIndexLength, 4));
                                sContentTemp = sContentTemp + isoMsgTemp.sBit[iIndex].Substring(iIndexLength, 4);
                                sContentTemp = sContentTemp + CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex].Substring(iIndexLength + 4, iTempLenApp));
                                iLenMessage = iLenMessage + 2 + iTempLenApp;
                                iIndexLength = iIndexLength + 4 + iTempLenApp;
                            }
                            sContent = sContent + iLenMessage.ToString().PadLeft(4, '0') + sContentTemp;
                            break;
                        case 48:
                        case 58:
                            //case 60:
                            //the content is on HexString Format
                            iTempLen = isoMsgTemp.sBit[iIndex].Length / 2;
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += iTempLen.ToString().PadLeft(4, '0') + sTemp;
                            break;
                        case 52:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 53:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;

                        case 64:
                            sTemp = isoMsgTemp.sBit[iIndex];
                            sContent += sTemp;
                            break;
                        #endregion
                        #region "Bit 65-128"
                        case 65:
                            break;
                        case 66:
                        case 67:
                        case 92:
                            sTemp = CommonLib.sMid(sContent, iIndex, 2);
                            switch (i)
                            {
                                case 66:
                                    isoMsgTemp.sBit[66] = sTemp;
                                    break;
                                case 67:
                                    isoMsgTemp.sBit[67] = sTemp;
                                    break;
                                case 92:
                                    isoMsgTemp.sBit[92] = sTemp;
                                    break;
                            }
                            iIndex += 2;
                            break;
                        case 68:
                        case 69:
                        case 70:
                        case 71:
                        case 72:
                            sTemp = CommonLib.sMid(sContent, iIndex, 4);
                            switch (i)
                            {
                                case 68:
                                    isoMsgTemp.sBit[68] = sTemp;
                                    break;
                                case 69:
                                    isoMsgTemp.sBit[69] = sTemp;
                                    break;
                                case 70:
                                    isoMsgTemp.sBit[70] = sTemp;
                                    break;
                                case 71:
                                    isoMsgTemp.sBit[71] = sTemp;
                                    break;
                                case 72:
                                    isoMsgTemp.sBit[72] = sTemp;
                                    break;
                            }
                            iIndex += 4;
                            break;
                        case 73:
                        case 93:
                            switch (i)
                            {
                                case 73:
                                    isoMsgTemp.sBit[72] = CommonLib.sMid(sContent, iIndex, 6);
                                    break;
                                case 93:
                                    isoMsgTemp.sBit[93] = CommonLib.sMid(sContent, iIndex, 6);
                                    break;
                            }
                            iIndex += 6;
                            break;
                        case 74:
                        case 75:
                        case 76:
                        case 77:
                        case 78:
                        case 79:
                        case 80:
                        case 81:
                            break;
                        case 82:
                        case 83:
                        case 84:
                        case 85:
                            //TxtReceived.Text += i + " : " + Strings.Mid(arrbContent, iIndex, 12) + Constants.vbCrLf;
                            //iIndex += 12;
                            break;
                        case 86:
                        case 87:
                        case 88:
                        case 89:
                        case 97:
                            break;
                        case 90:
                            break;
                        case 91:
                            break;
                        case 94:
                            break;
                        case 95:
                            break;
                        case 96:
                            break;
                        case 98:
                            break;
                        case 99:
                            isoMsgTemp.sBit[99] = sTemp;
                            break;
                        case 100:
                        case 102:
                        case 103:
                        case 104:
                        case 113:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += iTempLen.ToString("00") + sTemp;
                            break;

                        case 105:
                        case 106:
                        case 107:
                        case 108:
                        case 109:
                        case 110:
                        case 111:
                        case 112:
                        case 114:
                        case 115:
                        case 116:
                        case 117:
                        case 118:
                        case 119:
                        case 120:
                        case 121:
                        case 122:
                        case 123:
                        case 124:
                        case 125:
                        case 126:
                        case 127:
                        case 128:
                            iTempLen = isoMsgTemp.sBit[iIndex].Length;
                            sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
                            sContent += iTempLen.ToString("0000") + sTemp;
                            switch (i)
                            {
                                case 105:
                                case 106:
                                case 107:
                                case 108:
                                case 109:
                                case 110:
                                case 111:
                                case 112:
                                case 114:
                                case 115:
                                case 116:
                                case 117:
                                case 118:
                                case 119:
                                case 120:
                                case 121:
                                case 122:
                                case 123:
                                case 124:
                                case 125:
                                case 126:
                                case 127:
                                case 128:
                                    break;
                            }
                            iIndex += 4 + (iTempLen * 2);
                            break;
                            #endregion
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(i + " : " + ex.Message);
            }

            return sContent;
        }

        //public string PackToISOHexDownload(ISO8583_MSGLib isoMSG)
        //{
        //    ISO8583_MSGLib isoMsgTemp = new ISO8583_MSGLib();
        //    isoMsgTemp = isoMSG;

        //    int i = 0;
        //    string sContent = null;
        //    sContent += isoMsgTemp.sTPDU;
        //    sContent += isoMsgTemp.sOriginAddr;
        //    sContent += isoMsgTemp.sDestNII;

        //    sContent += isoMsgTemp.MTI;
        //    sContent += CommonLib.sBinStringToBitMap(isoMsgTemp.sBinaryBitMap);
        //    try
        //    {
        //        while (CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) >= 0 &&
        //            CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) <= MAX_BIT)
        //        {
        //            int iIndex = CommonLib.iIntStr(isoMsgTemp.sBinaryBitMap, "1", i) + 1;
        //            int iTempLen = 0;
        //            string sTemp = null;
        //            i = iIndex;

        //            switch (iIndex)
        //            {
        //                #region "Bit 1-64"
        //                case 1:
        //                    break;
        //                case 2:
        //                case 32:
        //                case 33:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    iTempLen = sTemp.Length;
        //                    sContent += iTempLen.ToString("00") + sTemp;
        //                    break;
        //                case 3:
        //                    sTemp = isoMsgTemp.sBit[3];
        //                    sContent += sTemp;
        //                    break;
        //                case 4:
        //                case 5:
        //                case 6:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;
        //                case 7:
        //                    sTemp = isoMsgTemp.sBit[7];
        //                    sContent += sTemp;
        //                    break;

        //                case 8:
        //                case 9:
        //                case 10:
        //                case 28:
        //                case 29:
        //                case 30:
        //                case 31:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 11:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 12:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 13:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 14:
        //                case 15:
        //                case 16:
        //                case 17:
        //                case 18:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 19:
        //                case 20:
        //                case 21:
        //                case 22:
        //                case 23:
        //                case 24:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 25:
        //                case 26:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 27:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 34:
        //                    iTempLen = isoMsgTemp.sBit[iIndex].Length;
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += iTempLen.ToString("00") + CommonLib.sStringToHex(sTemp);
        //                    break;

        //                case 35:
        //                    iTempLen = isoMsgTemp.sBit[iIndex].Length;
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += iTempLen.ToString("00") + CommonLib.sStringToHex(sTemp);
        //                    break;

        //                case 37:
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += sTemp;
        //                    break;

        //                case 38:
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += sTemp;
        //                    break;

        //                case 39:
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += sTemp;
        //                    break;

        //                case 40:
        //                    sTemp = "0" + isoMsgTemp.sBit[iIndex];
        //                    sContent += CommonLib.sStringToHex(sTemp);
        //                    break;

        //                case 44:
        //                case 45:
        //                    break;

        //                case 49:
        //                case 51:
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += sTemp;
        //                    break;
        //                case 50:
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += sTemp;
        //                    break;

        //                case 41:
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += sTemp;
        //                    break;

        //                case 42:
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += sTemp;
        //                    break;

        //                case 43:
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += sTemp;
        //                    break;
        //                case 57:
        //                    iTempLen = 100;
        //                    int iIndexBit57;
        //                    if (isoMsgTemp.sBit[iIndex].Substring(0, 4) == "0000")
        //                        iIndexBit57 = 12;
        //                    else
        //                        iIndexBit57 = 8;

        //                    sTemp = "0100" + isoMsgTemp.sBit[iIndex].Substring(0, iIndexBit57);
        //                    sTemp += CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57, int.Parse(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57-4, 4))));
        //                    iIndexBit57 = iIndexBit57 + int.Parse(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57-4, 4));
        //                    sTemp += isoMsgTemp.sBit[iIndex].Substring(iIndexBit57, 8);
        //                    iIndexBit57 = iIndexBit57 + 8;
        //                    sTemp += CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57, int.Parse(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57-4, 4))));
        //                    iIndexBit57 = iIndexBit57 + int.Parse(isoMsgTemp.sBit[iIndex].Substring(iIndexBit57 - 4, 4));

        //                    int iStart = 0, iTotalCount = 200 - sTemp.Length + 4;
        //                    string sFiller="";
        //                    while (iStart < iTotalCount)
        //                    {
        //                        sFiller = sFiller + "F";
        //                        iStart += 1;
        //                    }
        //                    sTemp += sFiller;
        //                    sContent += sTemp;
        //                    break;
        //                case 36:
        //                case 46:
        //                case 47:
        //                case 54:
        //                case 55:
        //                case 56:
        //                case 58:
        //                case 59:
        //                case 61:
        //                case 62:
        //                case 63:
        //                    iTempLen = isoMsgTemp.sBit[iIndex].Length;
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += CommonLib.sConvertDecToHex(iTempLen).PadLeft(4, '0') + sTemp;
        //                    break;
        //                case 48:
        //                case 60:
        //                    //the content is on HexString Format
        //                    iTempLen = isoMsgTemp.sBit[iIndex].Length / 2;
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += iTempLen.ToString().PadLeft(4, '0') + sTemp;
        //                    break;
        //                case 52:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 53:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;

        //                case 64:
        //                    sTemp = isoMsgTemp.sBit[iIndex];
        //                    sContent += sTemp;
        //                    break;
        //                #endregion
        //                #region "Bit 65-128"
        //                case 65:
        //                    break;
        //                case 66:
        //                case 67:
        //                case 92:
        //                    sTemp = CommonLib.sMid(sContent, iIndex, 2);
        //                    switch (i)
        //                    {
        //                        case 66:
        //                            isoMsgTemp.sBit[66] = sTemp;
        //                            break;
        //                        case 67:
        //                            isoMsgTemp.sBit[67] = sTemp;
        //                            break;
        //                        case 92:
        //                            isoMsgTemp.sBit[92] = sTemp;
        //                            break;
        //                    }
        //                    iIndex += 2;
        //                    break;
        //                case 68:
        //                case 69:
        //                case 70:
        //                case 71:
        //                case 72:
        //                    sTemp = CommonLib.sMid(sContent, iIndex, 4);
        //                    switch (i)
        //                    {
        //                        case 68:
        //                            isoMsgTemp.sBit[68] = sTemp;
        //                            break;
        //                        case 69:
        //                            isoMsgTemp.sBit[69] = sTemp;
        //                            break;
        //                        case 70:
        //                            isoMsgTemp.sBit[70] = sTemp;
        //                            break;
        //                        case 71:
        //                            isoMsgTemp.sBit[71] = sTemp;
        //                            break;
        //                        case 72:
        //                            isoMsgTemp.sBit[72] = sTemp;
        //                            break;
        //                    }
        //                    iIndex += 4;
        //                    break;
        //                case 73:
        //                case 93:
        //                    switch (i)
        //                    {
        //                        case 73:
        //                            isoMsgTemp.sBit[72] = CommonLib.sMid(sContent, iIndex, 6);
        //                            break;
        //                        case 93:
        //                            isoMsgTemp.sBit[93] = CommonLib.sMid(sContent, iIndex, 6);
        //                            break;
        //                    }
        //                    iIndex += 6;
        //                    break;
        //                case 74:
        //                case 75:
        //                case 76:
        //                case 77:
        //                case 78:
        //                case 79:
        //                case 80:
        //                case 81:
        //                    break;
        //                case 82:
        //                case 83:
        //                case 84:
        //                case 85:
        //                    //TxtReceived.Text += i + " : " + Strings.Mid(arrbContent, iIndex, 12) + Constants.vbCrLf;
        //                    //iIndex += 12;
        //                    break;
        //                case 86:
        //                case 87:
        //                case 88:
        //                case 89:
        //                case 97:
        //                    break;
        //                case 90:
        //                    break;
        //                case 91:
        //                    break;
        //                case 94:
        //                    break;
        //                case 95:
        //                    break;
        //                case 96:
        //                    break;
        //                case 98:
        //                    break;
        //                case 99:
        //                    isoMsgTemp.sBit[99] = sTemp;
        //                    break;
        //                case 100:
        //                case 102:
        //                case 103:
        //                case 104:
        //                case 113:
        //                    iTempLen = isoMsgTemp.sBit[iIndex].Length;
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += iTempLen.ToString("00") + sTemp;
        //                    break;

        //                case 105:
        //                case 106:
        //                case 107:
        //                case 108:
        //                case 109:
        //                case 110:
        //                case 111:
        //                case 112:
        //                case 114:
        //                case 115:
        //                case 116:
        //                case 117:
        //                case 118:
        //                case 119:
        //                case 120:
        //                case 121:
        //                case 122:
        //                case 123:
        //                case 124:
        //                case 125:
        //                case 126:
        //                case 127:
        //                case 128:
        //                    iTempLen = isoMsgTemp.sBit[iIndex].Length;
        //                    sTemp = CommonLib.sStringToHex(isoMsgTemp.sBit[iIndex]);
        //                    sContent += iTempLen.ToString("0000") + sTemp;
        //                    switch (i)
        //                    {
        //                        case 105:
        //                        case 106:
        //                        case 107:
        //                        case 108:
        //                        case 109:
        //                        case 110:
        //                        case 111:
        //                        case 112:
        //                        case 114:
        //                        case 115:
        //                        case 116:
        //                        case 117:
        //                        case 118:
        //                        case 119:
        //                        case 120:
        //                        case 121:
        //                        case 122:
        //                        case 123:
        //                        case 124:
        //                        case 125:
        //                        case 126:
        //                        case 127:
        //                        case 128:
        //                            break;
        //                    }
        //                    iIndex += 4 + (iTempLen * 2);
        //                    break;
        //                #endregion
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        //MessageBox.Show(i + " : " + ex.Message);
        //    }

        //    return sContent;
        //}

        public void SetField_Str(int iBitIndex, ref ISO8583_MSGLib isoMsg, string sBitContent)
        {
            isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, iBitIndex - 1, "1");
            isoMsg.sBit[iBitIndex] = sBitContent;
        }

        public void RemField(int iBitIndex, ref ISO8583_MSGLib isoMsg)
        {
            isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, iBitIndex - 1, "0");
            isoMsg.sBit[iBitIndex] = "";
        }

        protected string sPadding(string sOriginal, char cPaddingChar, int iTotalWidth)
        {
            return sOriginal.PadRight(iTotalWidth, cPaddingChar);
        }

        protected string sReplace(string sOldString, int iIndex, string sNewString)
        {
            string sReturn = sOldString.Remove(iIndex, 1);
            return sReturn.Insert(iIndex, sNewString);
        }

        protected byte[] CutLengthTpduAddress(byte[] arrbIso, int iIsoLength)
        {
            byte[] arrbReturn = new byte[iIsoLength - 5];
            for (int i = 0; i < iIsoLength - 5; i++)
                arrbReturn[i] = arrbIso[i + 5];
            return arrbReturn;
        }

        protected void GetTPDUAddress(byte[] arrbIso, ref ISO8583_MSGLib isoTemp)
        {
            byte[] arrbTPDU = new byte[1];
            arrbTPDU[0] = arrbIso[0];
            isoTemp.sTPDU = CommonLib.sByteArrayToHexString(arrbTPDU).Replace(" ", "");

            byte[] arrbAddr = new byte[2];
            arrbAddr[0] = arrbIso[1];
            arrbAddr[1] = arrbIso[2];
            isoTemp.sDestNII = CommonLib.sByteArrayToHexString(arrbAddr).Replace(" ", "");

            arrbAddr[0] = arrbIso[3];
            arrbAddr[1] = arrbIso[4];
            isoTemp.sOriginAddr = CommonLib.sByteArrayToHexString(arrbAddr).Replace(" ", "");
        }

        //protected void ConvertTo128()
        //{
        //    // Convert BitMap jadi 128bit.
        //    // note :
        //    // Bit 33 -> Bit 100
        //    // Bit 34 -> Bit 102
        //    // Bit 56 -> Bit 103
        //    // Bit 59 -> Bit 104
        //    // Bit 60 -> Bit 125
        //    // Bit 62 -> Bit 127
        //    isoMsg.sBit[100] = isoMsg.sBit[33];
        //    isoMsg.sBit[102] = isoMsg.sBit[34];
        //    isoMsg.sBit[103] = isoMsg.sBit[56];
        //    isoMsg.sBit[104] = isoMsg.sBit[59];
        //    isoMsg.sBit[126] = isoMsg.sBit[60];
        //    isoMsg.sBit[127] = isoMsg.sBit[62];
        //    isoMsg.sBinaryBitMap = sPadding(isoMsg.sBinaryBitMap, '0', 128);
        //    for (int i = 1; i <= MAX_BIT; i++)
        //    {
        //        string s = isoMsg.sBinaryBitMap.Substring(i - 1, 1);
        //        if (s == "1")
        //        {
        //            switch (i)
        //            {
        //                case 33:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, i - 1, "0");
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 99, "1");
        //                    break;
        //                case 34:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, i - 1, "0");
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 101, "1");
        //                    break;
        //                case 56:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, i - 1, "0");
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 102, "1");
        //                    break;
        //                case 59:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, i - 1, "0");
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 103, "1");
        //                    break;
        //                case 60:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, i - 1, "0");
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 124, "1");
        //                    break;
        //                case 62:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, i - 1, "0");
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 126, "1");
        //                    break;
        //            }
        //        }

        //    }
        //    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 0, "1");
        //    MAX_BIT = 128;
        //    isoMsg.sBit1 = CommonLib.sBinStringToBitMap(isoMsg.sBinaryBitMap);
        //}

        //protected void ConvertTo64()
        //{
        //    // Convert BitMap jadi 128bit.
        //    // note :
        //    // Bit 100 -> Bit 33
        //    // Bit 102 -> Bit 34
        //    // Bit 103 -> Bit 56
        //    // Bit 104 -> Bit 59
        //    // Bit 125 -> Bit 60
        //    // Bit 127 -> Bit 62
        //    isoMsg.sBit[33] = isoMsg.sBit[100];
        //    isoMsg.sBit[34] = isoMsg.sBit[102];
        //    isoMsg.sBit[56] = isoMsg.sBit[103];
        //    isoMsg.sBit[59] = isoMsg.sBit[104];
        //    isoMsg.sBit[60] = isoMsg.sBit[126];
        //    isoMsg.sBit[62] = isoMsg.sBit[127];        
        //    for (int i = 1; i <= MAX_BIT; i++)
        //    {
        //        string s = isoMsg.sBinaryBitMap.Substring(i - 1, 1);
        //        if (s == "1")
        //        {
        //            switch (i)
        //            {
        //                case 100:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 32, "1");
        //                    break;
        //                case 102:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 33, "1");
        //                    break;
        //                case 103:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 55, "1");
        //                    break;
        //                case 104:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 58, "1");
        //                    break;
        //                case 125:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 59, "1");
        //                    break;
        //                case 127:
        //                    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 61, "1");
        //                    break;
        //            }
        //        }
        //    }
        //    isoMsg.sBinaryBitMap = sReplace(isoMsg.sBinaryBitMap, 0, "0");
        //    isoMsg.sBinaryBitMap = CommonLib.sLeft(isoMsg.sBinaryBitMap, 64);
        //    isoMsg.sBit1 = CommonLib.sBinStringToBitMap(isoMsg.sBinaryBitMap);
        //}

        //public string sUnPackMSG_And_PackToASCII(byte[] arrbContent)
        //{
        //    UnPackISO(arrbContent);
        //    ConvertTo128();
        //    return PackToAsciiISO();
        //}

        //public byte[] sUnPackASCII_And_Pack(byte[] arrbContent)
        //{
        //    UnPackAsciiISO(arrbContent);
        //    ConvertTo64();
        //    return PackToISO();
        //}
    }
}