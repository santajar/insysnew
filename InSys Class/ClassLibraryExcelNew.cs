﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;

public class ExcelLib
{
    public static void WriteExcel(SqlDataReader dr, string sFilename)
    {
        StringBuilder sbrHTML = new StringBuilder();
        //Making HTML
        sbrHTML.Append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><style>.text { mso-number-format:\\@; } </style><TABLE class=\"text\" Border=1 ID=\"Table1\">");
        //sbrHTML.Append("<TR bgcolor=\"#d7ffd7\"><TD ColSpan=4><Font Size=5> " + text_header + "</Font></TD><TR>");
        //sbrHTML.Append("<TR bgcolor=\"#bbbbbb\" >");
        for (int i = 0; i < dr.FieldCount; i++)
        {
            sbrHTML.Append("<TH>" + dr.GetName(i) + "</TH>");
        }
        sbrHTML.Append("</TR>");
        while (dr.Read())
        {
            sbrHTML.Append("<TR>");
            for (int i = 0; i < dr.FieldCount; i++)
            {
                //sbrHTML.Append("<TD>" + good_value(dr.GetValue(i).ToString()) + "</TD>");
                sbrHTML.Append("<TD>" + dr.GetValue(i).ToString() + "</TD>");
            }
            sbrHTML.Append("</TR>");
        }

        sbrHTML.Append("</TABLE>");
        //sbrHTML.Append("<BR><BR><B>Reported by " + Classes.clsPublicVariables.user_name + "</B>");
        //sbrHTML.Append("<BR><BR><B>Date: " + Classes.clsPublicVariables.strToday + "</B>");
        //ENDOF MAKING HTML
        StreamWriter swXLS = new StreamWriter(sFilename);
        swXLS.Write(sbrHTML.ToString());
        swXLS.Close();
    }

    public static void WriteExcel(OleDbDataReader dr, string sFilename)
    {
        StringBuilder sbrHTML = new StringBuilder();
        //Making HTML
        sbrHTML.Append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><style>.text { mso-number-format:\\@; } </style><TABLE class=\"text\" Border=1 ID=\"Table1\">");
        //sbrHTML.Append("<TR bgcolor=\"#d7ffd7\"><TD ColSpan=4><Font Size=5> " + text_header + "</Font></TD><TR>");
        //sbrHTML.Append("<TR bgcolor=\"#bbbbbb\" >");
        for (int i = 0; i < dr.FieldCount; i++)
        {
            sbrHTML.Append("<TH>" + dr.GetName(i) + "</TH>");
        }
        sbrHTML.Append("</TR>");
        while (dr.Read())
        {
            sbrHTML.Append("<TR>");
            for (int i = 0; i < dr.FieldCount; i++)
            {
                //sbrHTML.Append("<TD>" + good_value(dr.GetValue(i).ToString()) + "</TD>");
                sbrHTML.Append("<TD>" + dr.GetValue(i).ToString() + "</TD>");
            }
            sbrHTML.Append("</TR>");
        }

        sbrHTML.Append("</TABLE>");
        //sbrHTML.Append("<BR><BR><B>Reported by " + Classes.clsPublicVariables.user_name + "</B>");
        //sbrHTML.Append("<BR><BR><B>Date: " + Classes.clsPublicVariables.strToday + "</B>");
        //ENDOF MAKING HTML
        StreamWriter swXLS = new StreamWriter(sFilename);
        swXLS.Write(sbrHTML.ToString());
        swXLS.Close();
    }

    public static void WriteExcel(DataTable dt, string sFilename)
    {
        StringBuilder sbrHTML = new StringBuilder();
        //Making HTML
        sbrHTML.Append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"><style>.text { mso-number-format:\\@; } </style><TABLE class=\"text\" Border=1 ID=\"Table1\">\n");
        //sbrHTML.Append("<TR bgcolor=\"#d7ffd7\"><TD ColSpan=4><Font Size=5> " + text_header + "</Font></TD><TR>");
        //sbrHTML.Append("<TR bgcolor=\"#bbbbbb\" >");
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            sbrHTML.Append("<TH>" + dt.Columns[i].ColumnName.ToString() + "</TH>\n");
        }
        sbrHTML.Append("</TR>");

        foreach (DataRow drRow in dt.Rows)
        {
            sbrHTML.Append("<TR>");
            for (int iCol = 0; iCol < dt.Columns.Count; iCol++)
            {
                //sbrHTML.Append("<TD>" + good_value(dr.GetValue(i).ToString()) + "</TD>");
                sbrHTML.Append("<TD> '" + drRow[iCol].ToString() + "</TD>");
            }
            sbrHTML.Append("</TR>");
        }

        //while (dr.Read())
        //{
        //    sbrHTML.Append("<TR>");
        //    for (int i = 0; i < dr.FieldCount; i++)
        //    {
        //        //sbrHTML.Append("<TD>" + good_value(dr.GetValue(i).ToString()) + "</TD>");
        //        sbrHTML.Append("<TD>" + dr.GetValue(i).ToString() + "</TD>");
        //    }
        //    sbrHTML.Append("</TR>");
        //}

        sbrHTML.Append("</TABLE>");
        //sbrHTML.Append("<BR><BR><B>Reported by " + Classes.clsPublicVariables.user_name + "</B>");
        //sbrHTML.Append("<BR><BR><B>Date: " + Classes.clsPublicVariables.strToday + "</B>");
        //ENDOF MAKING HTML
        StreamWriter swXLS = new StreamWriter(sFilename);
        swXLS.Write(sbrHTML.ToString());
        swXLS.Close();
        swXLS.Dispose();
    }

    protected static string good_value(string input)
    {
        return ((input == "" || (input[0] >= '0' && input[0] <= '9')) ? "'" : "") + input;
    }

    public static void WriteXMLExcel(SqlDataReader dr, string sFilename)
    {
        int iCol = dr.FieldCount;
        string sNewLine = "\n";

        StringBuilder sExcelXML = new StringBuilder();

        //First Write the Excel Header
        sExcelXML.Append(ExcelXMLHeader());

        // Get all the Styles
        sExcelXML.Append(ExcelXMLStyles("styles.config"));

        // Worksheet options Required only one time 
        sExcelXML.Append(ExcelXMLWorkSheetOptions());

        // Create First Worksheet tag
        sExcelXML.Append("<Worksheet ss:Name=\"Sheet1\">");
        // Then Table Tag
        sExcelXML.Append("<Table>");

        // Table Header
        sExcelXML.Append("<Row ss:Index=\"1\">" + sNewLine);
        for (int i = 0; i < dr.FieldCount; i++)
        {
            // Cell Tags
            sExcelXML.Append("<Cell ss:StyleID=\"s24\"><Data ss:Type=\"String\">" + dr.GetName(i) + "</Data></Cell>" + sNewLine);
        }
        sExcelXML.Append("</Row>");

        while (dr.Read())
        {
            // Row Tag
            sExcelXML.Append("<tr>");
            for (int j = 0; j < iCol; j++)
            {
                // Cell Tags
                sExcelXML.Append("<td>");
                sExcelXML.Append(dr[j].ToString());
                sExcelXML.Append("</td>");
            }
            sExcelXML.Append("</tr>");
        }
        sExcelXML.Append("</Table>");
        sExcelXML.Append("</Worksheet>");

        // Close the Workbook tag (in Excel header you can see the Workbook tag)
        sExcelXML.Append("</Workbook>\n");

        SaveExcelXML(sFilename, sExcelXML.ToString());
    }

    public static void WriteXMLExcel(OleDbDataReader dr, string sFilename)
    {
        int iCol = dr.FieldCount;
        string sNewLine = "\n";

        StringBuilder sExcelXML = new StringBuilder();

        //First Write the Excel Header
        sExcelXML.Append(ExcelXMLHeader());

        // Get all the Styles
        sExcelXML.Append(ExcelXMLStyles("styles.config"));

        // Worksheet options Required only one time 
        sExcelXML.Append(ExcelXMLWorkSheetOptions());

        // Create First Worksheet tag
        sExcelXML.Append("<Worksheet ss:Name=\"Sheet1\">");
        // Then Table Tag
        sExcelXML.Append("<Table>");

        // Table Header
        sExcelXML.Append("<Row ss:Index=\"1\">" + sNewLine);
        for (int i = 0; i < dr.FieldCount; i++)
        {
            // Cell Tags
            sExcelXML.Append("<Cell ss:StyleID=\"s24\"><Data ss:Type=\"String\">" + dr.GetName(i) + "</Data></Cell>" + sNewLine);
        }
        sExcelXML.Append("</Row>");

        while (dr.Read())
        {
            // Row Tag
            sExcelXML.Append("<tr>");
            for (int j = 0; j < iCol; j++)
            {
                // Cell Tags
                sExcelXML.Append("<td>");
                sExcelXML.Append(dr[j].ToString());
                sExcelXML.Append("</td>");
            }
            sExcelXML.Append("</tr>");
        }
        sExcelXML.Append("</Table>");
        sExcelXML.Append("</Worksheet>");

        // Close the Workbook tag (in Excel header you can see the Workbook tag)
        sExcelXML.Append("</Workbook>\n");

        SaveExcelXML(sFilename, sExcelXML.ToString());
    }

    protected static string ExcelXMLHeader()
    {
        // Excel header
        StringBuilder sb = new StringBuilder();
        sb.Append("<?xml version=\"1.0\"?>\n");
        sb.Append("<?mso-application progid=\"Excel.Sheet\"?>\n");
        sb.Append("<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" ");
        sb.Append("xmlns:o=\"urn:schemas-microsoft-com:office:office\" ");
        sb.Append("xmlns:x=\"urn:schemas-microsoft-com:office:excel\" ");
        sb.Append("xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" ");
        sb.Append("xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n");
        sb.Append("<DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">");
        sb.Append("</DocumentProperties>");
        sb.Append("<ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">\n");
        sb.Append("<ProtectStructure>False</ProtectStructure>\n");
        sb.Append("<ProtectWindows>False</ProtectWindows>\n");
        sb.Append("</ExcelWorkbook>\n");

        return sb.ToString();
    }

    /// <summary>
    /// Read styles and copy it to the Excel string
    /// </summary>
    /// <param name="filename">Styles.config</param>
    /// <returns></returns>
    protected static string ExcelXMLStyles(string filename)
    {
        StreamReader sr;
        string s;
        StringBuilder sFileText = new StringBuilder();
        sr = File.OpenText(filename);
        s = sr.ReadLine();
        sFileText.Append(s);
        while (s != null)
        {
            s = sr.ReadLine();
            sFileText.Append(s + "\n");
        }
        sr.Close();
        return sFileText.ToString();
    }

    protected static string ExcelXMLWorkSheetOptions()
    {
        // This is Required Only Once ,	But this has to go after the First Worksheet's First Table		
        StringBuilder sb = new StringBuilder();
        sb.Append("<WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">\n<Selected/>\n </WorksheetOptions>\n");
        return sb.ToString();
    }

    protected static void SaveExcelXML(string sFilename, string sExcelXML)
    {
        File.Delete(sFilename);
        //StreamWriter sw2 = new StreamWriter(sFilename + ".txt", true, System.Text.Encoding.Unicode);
        //sw2.Write(sExcelXML);
        //sw2.Close();

        StreamWriter sw = new StreamWriter(sFilename, true, System.Text.Encoding.Unicode);
        sw.Write(ConvertHTMLToExcelXML(sExcelXML));
        sw.Close();
    }

    protected static string ConvertHTMLToExcelXML(string strHtml)
    {

        // Just to replace TR with Row
        strHtml = strHtml.Replace("<tr>", "<Row ss:AutoFitHeight=\"1\" >\n");
        strHtml = strHtml.Replace("</tr>", "</Row>\n");

        //replace the cell tags
        strHtml = strHtml.Replace("<td>", "<Cell><Data ss:Type=\"String\">");
        strHtml = strHtml.Replace("</td>", "</Data></Cell>\n");

        return strHtml;
    }
}

public class ExcelOleDb : IDisposable
{
    protected OleDbConnection oOleConn = new OleDbConnection();

    public ExcelOleDb(string sExcelFilename)
    {
        oOleConn.ConnectionString = sConnectionString(sExcelFilename);
    }

    protected string sConnectionString(string sFilename)
    {
        string ext = Path.GetExtension(sFilename);

        // Support for Excel 2007 XML format
        if (ext.ToLower() == ".xlsx")
            return string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", sFilename);

        // Support for Excel 2007 binary format
        if (ext.ToLower() == ".xlsb")
            return string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR=YES\";", sFilename);

        // Support for Excel 2007 binary format
        if (ext.ToLower() == ".xlsm")
            return string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Macro;HDR=YES\";", sFilename);

        // Basic support for Excel 97 through 2003.
        return string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=YES\";", sFilename);
    }

    public void InsertRow(OleDbDataReader _drTemp) { InsertRow(_drTemp, "Sheet1"); }

    public void InsertRow(OleDbDataReader _drTemp, string sTableName)
    {
        if (oOleConn.State != ConnectionState.Open)
            oOleConn.Open();
        string sCmd = string.Format("INSERT INTO [{0}$] VALUES(", sTableName);
        OleDbCommand oCmd = new OleDbCommand();
        while (_drTemp.Read())
        {
            string sValues = null;
            for (int i = 0; i < _drTemp.FieldCount; i++)
            {
                sValues += "'" + _drTemp.GetValue(i).ToString() + "'";
            }
            string sQuery = sCmd + sValues + ")";
            oCmd = new OleDbCommand(sQuery, oOleConn);
            oCmd.ExecuteNonQuery();
        }
        oCmd.Dispose();
    }

    public void InsertRow(DataTable _dtTemp)
    {
        if (string.IsNullOrEmpty(_dtTemp.TableName))
            InsertRow(_dtTemp, "Sheet1");
        else
            InsertRow(_dtTemp, _dtTemp.TableName);
    }

    public void InsertRow(DataTable _dtTemp, string sTableName)
    {
        if (oOleConn.State != ConnectionState.Open)
            oOleConn.Open();
        string sCmd = string.Format("INSERT INTO [{0}$](", sTableName);
        foreach (DataColumn col in _dtTemp.Columns)
            sCmd = string.Format("{0}'{1}',", sCmd, col.ColumnName);

        sCmd = string.Format("{0}) VALUES", sCmd.Substring(0, sCmd.Length - 1));
        OleDbCommand oCmd = new OleDbCommand();
        foreach (DataRow row in _dtTemp.Rows)
        {
            string sValues = null;
            foreach (DataColumn col in _dtTemp.Columns)
            {
                //string sVal = row[col].ToString();
                string sVal = row[col].ToString().Replace("'", "''");
                //sVal = (new Regex("[^a-zA-Z0-9 -]")).Replace(sVal, "");
                sValues = string.Format("{0}'{1}',", sValues, sVal);
            }

            string sQuery = string.Format("{0}({1})", sCmd, sValues.Substring(0, sValues.Length - 1));
            oCmd = new OleDbCommand(sQuery, oOleConn);
            oCmd.ExecuteNonQuery();
        }
        oCmd.Dispose();
    }

    public void CreateWorkSheet(DataTable _dtTemp)
    {
        CreateWorkSheet("Sheet1", _dtTemp);
    }

    public void CreateWorkSheet(string sSheetName, DataTable _dtTemp)
    {
        if (oOleConn.State != ConnectionState.Open)
            oOleConn.Open();

        string sCmd = string.Format("CREATE TABLE [{0}$](", sSheetName);
        foreach (DataColumn col in _dtTemp.Columns)
        {
            //if (col.ColumnName.Contains("Time")||col.ColumnName.Contains("time"))
            //    sCmd = string.Format("{0}'{1}' Datetime,", sCmd, col.ColumnName);
            //else
            sCmd = string.Format("{0}'{1}' char(255),", sCmd, col.ColumnName);
        }
        sCmd = string.Format("{0})", sCmd.Substring(0, sCmd.Length - 1));

        OleDbCommand oCmd = new OleDbCommand();
        oCmd = new OleDbCommand(sCmd, oOleConn);
        oCmd.ExecuteNonQuery();
    }

    public DataTable ReadWorksheet(string sSheetName)
    {
        DataTable dtTemp = new DataTable();
        string sCommand = string.Format("SELECT * FROM [{0}$]", sSheetName);
        if (oOleConn.State != ConnectionState.Open) oOleConn.Open();
        using (OleDbCommand command = new OleDbCommand(sCommand, oOleConn))
        using (OleDbDataReader reader = command.ExecuteReader())
            if (reader.HasRows)
                dtTemp.Load(reader);
        return dtTemp;
    }

    public void DropWorkSheet(string sSheetName)
    {
        if (oOleConn.State != ConnectionState.Open)
            oOleConn.Open();

        string sCmd = string.Format("DROP TABLE [{0}$](", sSheetName);

        OleDbCommand oCmd = new OleDbCommand();
        oCmd = new OleDbCommand(sCmd, oOleConn);
        oCmd.ExecuteNonQuery();
    }

    //Implement IDisposable.
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            // Free other state (managed objects).
            if (oOleConn.State == ConnectionState.Open)
                oOleConn.Close();
            oOleConn.Dispose();
        }
        // Free your own state (unmanaged objects).
        // Set large fields to null.
    }

    // Use C# destructor syntax for finalization code.
    ~ExcelOleDb()
    {
        // Simply call Dispose(false).
        Dispose(false);
    }
}

public class NewExcelOleDb : IDisposable
{
    protected class ColumnName
    {
        string sColumnName = "";

        public ColumnName(string sColName)
        {
            sColumnName = sColName;
        }

        public string sColName
        {
            get { return sColumnName; }
        }
    }

    protected OleDbConnection oOleConn = new OleDbConnection();
    protected List<ColumnName> ltColName = new List<ColumnName>();

    public NewExcelOleDb(string sExcelFilename)
    {
        oOleConn.ConnectionString = sConnectionString(sExcelFilename);
    }

    protected string sConnectionString(string sFilename)
    {
        string ext = Path.GetExtension(sFilename);

        // Support for Excel 2007 XML format
        if (ext.ToLower() == ".xlsx")
            return string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", sFilename);

        // Support for Excel 2007 binary format
        if (ext.ToLower() == ".xlsb")
            return string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR=YES\";", sFilename);

        // Support for Excel 2007 binary format
        if (ext.ToLower() == ".xlsm")
            return string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Macro;HDR=YES\";", sFilename);

        // Basic support for Excel 97 through 2003.
        return string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=YES\";", sFilename);
    }

    protected void doOpenConnection()
    {
        if (oOleConn.State != ConnectionState.Open)
            oOleConn.Open();
    }

    public void AddColumnName(string sColName)
    {
        ColumnName oColName = new ColumnName(sColName);
        ltColName.Add(oColName);
    }

    protected string sGetColumNames()
    {
        string sColName = "";
        foreach (ColumnName oCol in ltColName)
        {
            if (!string.IsNullOrEmpty(sColName))
                sColName = sColName + ", ";
            sColName = sColName + oCol.sColName;
        }

        return string.IsNullOrEmpty(sColName) ? " * " : sColName;
    }

    protected string sGenerateCommand(string sWorksheet)
    {
        return string.Format("SELECT {0} FROM [{1}$] ", sGetColumNames(), sWorksheet);
    }

    public DataTable dtReadExcel(string sWorkSheet)
    {
        DataTable dtTable = new DataTable();

        doOpenConnection();
        //OleDbCommand oComm = new OleDbCommand(sGenerateCommand(sWorkSheet));
        OleDbDataAdapter oAdapt = new OleDbDataAdapter(sGenerateCommand(sWorkSheet), oOleConn);
        oAdapt.Fill(dtTable);

        oAdapt.Dispose();
        //oComm.Dispose();

        return dtTable;
    }

    public void InsertRow(OleDbDataReader _drTemp) { InsertRow(_drTemp, "Sheet1"); }
    //public void InsertRow(OleDbDataReader _drTemp) : this(_drTemp,"Sheet1"){}

    public void InsertRow(OleDbDataReader _drTemp, string sTableName)
    {
        if (oOleConn.State != ConnectionState.Open)
            oOleConn.Open();
        string sCmd = string.Format("INSERT INTO [{0}$] VALUES(", sTableName);
        OleDbCommand oCmd = new OleDbCommand();
        while (_drTemp.Read())
        {
            string sValues = null;
            for (int i = 0; i < _drTemp.FieldCount; i++)
            {
                sValues += "'" + _drTemp.GetValue(i).ToString() + "'";
            }
            string sQuery = sCmd + sValues + ")";
            oCmd = new OleDbCommand(sQuery, oOleConn);
            oCmd.ExecuteNonQuery();
        }
        oCmd.Dispose();
    }

    public void InsertRow(DataTable _dtTemp)
    {
        if (string.IsNullOrEmpty(_dtTemp.TableName))
            InsertRow(_dtTemp, "Sheet1");
        else
            InsertRow(_dtTemp, _dtTemp.TableName);
    }

    public void InsertRow(DataTable _dtTemp, string sTableName)
    {
        if (oOleConn.State != ConnectionState.Open)
            oOleConn.Open();
        string sCmd = string.Format("INSERT INTO [{0}$](", sTableName);
        foreach (DataColumn col in _dtTemp.Columns)
            sCmd = string.Format("{0}'{1}',", sCmd, col.ColumnName);

        sCmd = string.Format("{0}) VALUES", sCmd.Substring(0, sCmd.Length - 1));
        OleDbCommand oCmd = new OleDbCommand();
        foreach (DataRow row in _dtTemp.Rows)
        {
            string sValues = null;
            foreach (DataColumn col in _dtTemp.Columns)
                sValues = string.Format("{0}'{1}',", sValues, row[col].ToString().Replace("'", "''"));

            string sQuery = string.Format("{0}({1})", sCmd, sValues.Substring(0, sValues.Length - 1));
            oCmd = new OleDbCommand(sQuery, oOleConn);
            oCmd.ExecuteNonQuery();
        }
        oCmd.Dispose();
    }

    public void CreateWorkSheet(DataTable _dtTemp)
    {
        CreateWorkSheet(_dtTemp.TableName, _dtTemp);
    }

    public void CreateWorkSheet(string sSheetName, DataTable _dtTemp)
    {
        if (oOleConn.State != ConnectionState.Open)
            oOleConn.Open();

        string sCmd = string.Format("CREATE TABLE {0}(", sSheetName);
        foreach (DataColumn col in _dtTemp.Columns)
            //sCmd = string.Format("{0}'{1}' {2},", sCmd, col.ColumnName, col.MaxLength > 255 ? "memo" : "string");
            sCmd = string.Format("{0}'{1}' string,", sCmd, col.ColumnName);
        sCmd = string.Format("{0})", sCmd.Substring(0, sCmd.Length - 1));

        OleDbCommand oCmd = new OleDbCommand();
        oCmd = new OleDbCommand(sCmd, oOleConn);
        oCmd.ExecuteNonQuery();
    }

    //Implement IDisposable.
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            // Free other state (managed objects).
            if (oOleConn.State == ConnectionState.Open)
                oOleConn.Close();
            oOleConn.Dispose();
        }
        // Free your own state (unmanaged objects).
        // Set large fields to null.
    }

    // Use C# destructor syntax for finalization code.
    ~NewExcelOleDb()
    {
        // Simply call Dispose(false).
        Dispose(false);
    }
}
