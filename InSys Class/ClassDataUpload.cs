﻿using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Text.RegularExpressions;

namespace InSysClass
{
    public class ClassDataUpload
    {
        protected SqlConnection oSqlConn = new SqlConnection();
        protected DataTable dtExcel = new DataTable(); //data table for excel file
        protected DataTable dtTagCollDE = new DataTable(); //data table for terminal column name
        protected DataTable dtTagCollAA = new DataTable(); //data table for acquirer column name
        protected DataTable dtUploadDE = new DataTable(); //data table for upload terminal
        protected DataTable dtUploadAA = new DataTable(); //data table for upload acquirer
        protected DataTable dtReferenceDE = new DataTable(); //data table for terminal tag reference
        protected DataTable dtReferenceAA = new DataTable(); //data table for acquirer tag reference

        //data table for bulk upload
        protected DataTable dtTerminalList = new DataTable();
        protected DataTable dtTerminal = new DataTable();
        protected DataTable dtAcquirer = new DataTable();
        protected DataTable dtIssuer = new DataTable();
        protected DataTable dtRelation = new DataTable();
        protected DataTable dtCustomMenu = new DataTable();

        protected DataTable dtMasterList = new DataTable();
        protected DataTable dtMaster = new DataTable();
        protected bool bCustomMenu = false;
        public  bool bSerialNumber = false;
        protected string sLogMessageDE, sLogMessageAA, sLogDE, sLogAA, sDeleteLogDE, sDeleteLogAA,sLogDENew,sLogAANew,sLogDEOld,sLogAAOld,sLogDEComb,sLogAAComb, sItemDE;
        protected string sLogMessage = null;
        public string LogMessage { get { return sLogMessage; } set { sLogMessage = value; } }
        protected string sUserID = null;
        protected string sDBName = "";

        public bool bUploadRandomPwd = true;
        public int iTypeRandomPassword = 0;
        
        #region "Data Upload StoredProcedure"
        public static string sSPAuditTrailInsert = "spAuditTrailInsert";        
        protected static string sSPUploadTagDECount = "spUploadTagDECount";
        protected static string sSPUploadTagAACount = "spUploadTagAACount";

        protected static string sSPUploadTagDESourceCount = "spUploadTagDESourceCount";
        protected static string sSPUploadTagAASourceCount = "spUploadTagAASourceCount";

        protected static string sSPUploadTagDEInsert = "spUploadTagDEInsert";
        protected static string sSPUploadTagAAInsert = "spUploadTagAAInsert";

        protected static string sSPUploadTagDEDelete = "spUploadTagDEDelete";
        protected static string sSPUploadTagAADelete = "spUploadTagAADelete";

        protected static string sSPUploadTagDEAAUnion = "spUploadTagDEAAUnion";

        protected static string sSPUploadTagDEUpdateSourceColumn = "spUploadTagDEUpdateSourceColumn";
        protected static string sSPUploadTagAAUpdateSourceColumn = "spUploadTagAAUpdateSourceColumn";

        protected static string sSPUploadControlSet = "spUploadControlSet";
        protected static string sSPUploadControlGet = "spUploadControlGet";

        protected static string sSPUploadGetColumnsDE = "spUploadGetColumnsDE";
        protected static string sSPUploadGetColumnsAA = "spUploadGetColumnsAA";

        protected static string sSPDataUploadStartProcessAdd = "spDataUploadStartProcessAdd";
        protected static string sSPDataUploadCreateTable = "spDataUploadCreateTable";
        protected static string sSPDataUploadDropTable = "spDataUploadDropTable";
        protected static string sSPDataUploadDelete = "spDataUploadDelete";

        protected static string sSPUploadTagDEBrowse = "spUploadTagDEBrowse";
        protected static string sSPUploadTagAABrowse = "spUploadTagAABrowse";

        protected static string sSPDataUploadStartProcessUpdate = "spDataUploadStartProcessUpdate";
        protected static string sSPDataUploadStartProcessDelete = "spDataUploadStartProcessDelete";

        protected static string sSPProfileTerminalContentBrowse = "spProfileTerminalContentBrowse";
        protected static string sSPProfileTerminalDeleteProcess = "spProfileTerminalDeleteProcess";
        protected static string sSPTerminalListBrowse = "spProfileTerminalListBrowse";

        protected static string sSPGetTerminalValue = "spGetTerminalValueByTIDItemName";
        protected static string sSPGetAcquirerValue = "spGetAcquirerValueByTIDAcquirerNameItemName";

        protected static string sSPUserAccessInsert = "spUserAccessInsert";
        protected static string sSPUserAccessDelete = "spUserAccessDelete";
        protected static string sSPUserAccessBrowse = "spUserAccessBrowse";
        #endregion

        public enum UploadMode
        {
            Add = 1,
            Update,
            Delete,
        }

        protected UploadMode uploadMode;

        protected int iVersion = 1;

        public ClassDataUpload(SqlConnection _oSqlConn, DataTable _dtExcel) : this(_oSqlConn, _dtExcel, null) { }
        public ClassDataUpload(SqlConnection _oSqlConn, DataTable _dtExcel, string _sTemplateName)
        {
            oSqlConn = _oSqlConn;
            dtExcel = _dtExcel;
            Init(_sTemplateName);
           
           
        }

        #region "Initialize"
        protected void Init(string _sTemplateName)
        {
            InitTagCollection(_sTemplateName);

            InitTableReference(_sTemplateName);

            InitTableUpload();

            InitMasterProfile();
            InitTableProfile();
           

        }

        protected void InitTagCollection(string _sTemplateName)
        {
            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }

            using (SqlCommand cmdUploadGetColumnDE = new SqlCommand(sSPUploadGetColumnsDE, oSqlConn))
            {
                cmdUploadGetColumnDE.CommandType = CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(_sTemplateName))
                    cmdUploadGetColumnDE.Parameters.Add("@sFile", SqlDbType.VarChar, 25).Value = _sTemplateName;
                
                SqlDataAdapter adaptUploadGetColumnDE = new SqlDataAdapter(cmdUploadGetColumnDE);
                adaptUploadGetColumnDE.Fill(dtTagCollDE);
            }

            using (SqlCommand cmdUploadGetColumnAA = new SqlCommand(sSPUploadGetColumnsAA, oSqlConn))
            {
                cmdUploadGetColumnAA.CommandType = CommandType.StoredProcedure;
                
                if (!string.IsNullOrEmpty(_sTemplateName))
                    cmdUploadGetColumnAA.Parameters.Add("@sFile", SqlDbType.VarChar, 25).Value = _sTemplateName;

                (new SqlDataAdapter(cmdUploadGetColumnAA)).Fill(dtTagCollAA);
            }

            //new SqlDataAdapter(new SqlCommand(sSPUploadGetColumnsDE, oSqlConn)).Fill(dtTagCollDE);
            //new SqlDataAdapter(new SqlCommand(sSPUploadGetColumnsAA, oSqlConn)).Fill(dtTagCollAA);
        }

        protected void InitTableUpload()
        {
            foreach (DataRow oRow in dtTagCollDE.Rows)
            {
                dtUploadDE.Columns.Add(oRow[0].ToString());
            }
            dtUploadAA.Columns.Add("TerminalID");
            dtUploadAA.Columns.Add("AcquirerName");
            foreach (DataRow oRow in dtTagCollAA.Rows)
            {
                dtUploadAA.Columns.Add(oRow[0].ToString());
            }
        }

        protected void InitTableReference(string _sTemplate)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            using (SqlCommand cmdUploadTagDEBrowse = new SqlCommand(sSPUploadTagDEBrowse, oSqlConn))
            {
                cmdUploadTagDEBrowse.CommandType = CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(_sTemplate))
                    cmdUploadTagDEBrowse.Parameters.Add("@sFile", SqlDbType.VarChar, 25).Value = _sTemplate;
                
                //SqlDataAdapter adaptUploadTagDEBrowse = new SqlDataAdapter(cmdUploadTagDEBrowse);
                //adaptUploadTagDEBrowse.Fill(dtReferenceDE);
                new SqlDataAdapter(cmdUploadTagDEBrowse).Fill(dtReferenceDE);
            }

            using (SqlCommand cmdUploadTagAABrowse = new SqlCommand(sSPUploadTagAABrowse, oSqlConn))
            {
                cmdUploadTagAABrowse.CommandType = CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(_sTemplate))
                    cmdUploadTagAABrowse.Parameters.Add("@sFile", SqlDbType.VarChar, 25).Value = _sTemplate;

                //SqlDataAdapter adaptUploadTagAABrowse = new SqlDataAdapter(cmdUploadTagAABrowse);
                //adaptUploadTagAABrowse.Fill(dtReferenceAA);
                new SqlDataAdapter(cmdUploadTagAABrowse).Fill(dtReferenceAA);
            }

            //new SqlDataAdapter(new SqlCommand(sSPUploadTagDEBrowse, oSqlConn)).Fill(dtReferenceDE);
            //new SqlDataAdapter(new SqlCommand(sSPUploadTagAABrowse, oSqlConn)).Fill(dtReferenceAA);
        }

        protected void InitMasterProfile()
        {
            var masterProfile = (from excel in dtExcel.AsEnumerable()
                                select excel["Master"]).Distinct();
            string sMasterList = null;
            foreach (var rowMaster in masterProfile.AsEnumerable())
            {
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                using (SqlCommand oCmd = new SqlCommand(sSPProfileTerminalContentBrowse, oSqlConn))
                {
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = rowMaster.ToString();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 0;
                    DataTable dtMasterTemp = new DataTable();
                    new SqlDataAdapter(oCmd).Fill(dtMasterTemp);
                    if (dtMasterTemp.Rows.Count > 0)
                        CopyDataTable(dtMasterTemp, ref dtMaster, false);
                    sMasterList = string.IsNullOrEmpty(sMasterList) ? string.Format("'{0}'", rowMaster.ToString()) :
                        string.Format("{0},'{1}'", sMasterList, rowMaster.ToString());
                }
            }
            using (SqlCommand oCmd = new SqlCommand(sSPTerminalListBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format("WHERE TerminalID IN ({0})", sMasterList);

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                dtMasterList.Load(oCmd.ExecuteReader());
            }
        }

        

        protected void InitTableProfile()
        {
            dtTerminalList.Clear();
            dtTerminalList = dtMasterList.Clone();
            dtTerminal.Clear();
            dtTerminal = dtMaster.Clone();
            dtAcquirer.Clear();
            dtAcquirer = dtMaster.Clone();
            dtIssuer.Clear();
            dtIssuer = dtMaster.Clone();
            dtRelation.Clear();
            dtRelation = dtMaster.Clone();
        }

        public int iUploadMode
        {
            set
            {
                switch (value)
                {
                    case 1 :
                        uploadMode = UploadMode.Add;
                        break;
                    case 2 :
                        uploadMode = UploadMode.Update;
                        break;
                    case 3 :
                        uploadMode = UploadMode.Delete;
                        break;
                }
            }
        }

        public string UserID { set { sUserID = value; } }
        public string DBName { set { sDBName = value; } }
        #endregion

        #region "Validation"
        public bool IsValidExcel(ref string sErrorColumn, ref int iRowIndex, ref string sAcquirerName)
        {
            var refTable = (from refDE in dtReferenceDE.AsEnumerable()
                            orderby refDE["SourceColumn"]
                            select new
                            {
                                SourceColumn = refDE["SourceColumn"].ToString(),
                                Mandatory = refDE["Mandatory"].ToString(),
                                ColumnName = refDE["ColumnName"].ToString()
                            }).Union
                            (from refAA in dtReferenceAA.AsEnumerable()
                             orderby refAA["SourceColumn"]
                             select new
                             {
                                 SourceColumn = refAA["SourceColumn"].ToString(),
                                 Mandatory = refAA["Mandatory"].ToString(),
                                 ColumnName = refAA["ColumnName"].ToString()
                             });
            foreach (var rowRefTable in refTable.AsEnumerable())
            {
                if (Convert.ToBoolean(rowRefTable.Mandatory))
                {
                    foreach (DataRow row in dtExcel.Rows)
                        //if (string.IsNullOrEmpty(row[int.Parse(rowRefTable.SourceColumn.ToString())].ToString()))
                        if (string.IsNullOrEmpty(row[int.Parse(rowRefTable.SourceColumn.ToString()) -1].ToString()))
                        {
                            sErrorColumn = rowRefTable.ColumnName.ToString();
                            iRowIndex = dtExcel.Rows.IndexOf(row);

                            if (int.Parse(rowRefTable.SourceColumn) > dtReferenceDE.Rows.Count)
                            {
                                EnumerableRowCollection<DataRow> varAcqName = 
                                    from refAA in dtReferenceAA.AsEnumerable()
                                    where (refAA["SourceColumn"].ToString() == rowRefTable.SourceColumn.ToString()
                                    && refAA["ColumnName"].ToString() == rowRefTable.ColumnName.ToString())
                                    select refAA;
                                sAcquirerName = (varAcqName.AsDataView())[0]["AcquirerName"].ToString();
                            }
                            return false;
                        }
                }
            }
            return true;
        }

        static bool IsAllowUserAccess(SqlConnection _oSqlConn, string _sTerminalId, ref string sUserIdOnAccess)
        {
            bool isAllow = true;
            using (SqlCommand oCmd = new SqlCommand(sSPUserAccessBrowse, _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@sOutput", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();

                sUserIdOnAccess = oCmd.Parameters["@sUserId"].Value.ToString();
                isAllow = oCmd.Parameters["@sOutput"].Value.ToString() == "1" ? false : true;
            }
            return isAllow;
        }

        protected bool IsTerminalExist(string _sTerminalId)
        {
            bool isExist = true;
            DataTable odtTerminal = new DataTable();
            using (SqlCommand oSqlCmd = new SqlCommand(sSPTerminalListBrowse, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format("WHERE TerminalID='{0}'", _sTerminalId);

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                odtTerminal.Load(oSqlCmd.ExecuteReader());
                if (odtTerminal.Rows.Count == 0)
                    isExist = false;
            }
            return isExist;
        }
        #endregion

        public void StartUpload()
        {
            FillTableUpload();
      
            //if (uploadMode != UploadMode.Delete)
            //    foreach (DataRow row in dtUploadDE.Rows)
            //        CopyMasterProfile(row["Master"].ToString(), row["TerminalID"].ToString());
            switch (uploadMode)
            {
                case UploadMode.Add:
                    StartUploadAdd();
                    break;
                case UploadMode.Update:
                    StartUploadDelete();
                    Thread.Sleep(500);
                    StartUploadAdd();
                    break;
                case UploadMode.Delete:
                    StartUploadDelete();
                    break;
            }
        }

        protected void FillTableUpload()
        {
            FillTableUploadDE();
            if (uploadMode != UploadMode.Delete)
                FillTableUploadAA();
        }

        protected void FillTableUploadDE()
        {
            dtUploadDE.Clear();
            sLogMessageDE = null;
            foreach (DataRow rowExcel in dtExcel.Rows)
            {
                DataRow rowUpload = dtUploadDE.NewRow();
                foreach (DataRow rowRefDE in dtReferenceDE.Rows)
                {
                    int iColumn = int.Parse(rowRefDE["SourceColumn"].ToString()) - 1;
                    rowUpload[rowRefDE["ColumnName"].ToString()] = rowExcel[iColumn].ToString();
                    string sValueDE = null;
                    if (rowExcel[iColumn].ToString() == "0")
                        sValueDE = "False";
                    else if (rowExcel[iColumn].ToString() == "1")
                        sValueDE = "True";
                    else
                        sValueDE = rowExcel[iColumn].ToString();
                    sLogMessageDE = sLogMessageDE + rowRefDE["ColumnName"].ToString() + " : " + sValueDE + ", ";
                }
                dtUploadDE.Rows.Add(rowUpload);
            }
        }

        protected void FillTableUploadAA()
        {
            dtUploadAA.Clear();
            sLogMessageAA = null;
            foreach (DataRow rowExcel in dtExcel.Rows)
            {
                //string sTerminalId = rowExcel[1].ToString();
                string sTerminalId = rowExcel["TerminalID"].ToString();
                string sAcquirerName = null;
                string sAcquirerNameTemp = "";
                DataRow rowUpload = dtUploadAA.NewRow();
                foreach (DataRow rowRefAA in dtReferenceAA.Rows)
                {
                    int iColumn = int.Parse(rowRefAA["SourceColumn"].ToString()) - 1;
                    string sColumnName = rowRefAA["ColumnName"].ToString();
                    sAcquirerNameTemp = rowRefAA["AcquirerName"].ToString();
                    if (!string.IsNullOrEmpty(sAcquirerName) 
                        && sAcquirerName != sAcquirerNameTemp)
                    {
                        rowUpload["TerminalID"] = sTerminalId;
                        rowUpload["AcquirerName"] = sAcquirerName;
                        dtUploadAA.Rows.Add(rowUpload);
                        rowUpload = dtUploadAA.NewRow();
                    }
                    rowUpload[sColumnName] = rowExcel[iColumn].ToString();
                    sAcquirerName = sAcquirerNameTemp;
                    string sValueAA = null;
                    if (rowExcel[iColumn].ToString() == "0")
                        sValueAA = "False";
                    else if (rowExcel[iColumn].ToString() == "1")
                        sValueAA = "True";
                    else
                        sValueAA = rowExcel[iColumn].ToString();
                    sLogMessageAA = sLogMessageAA + sTerminalId + "-" + sAcquirerName + "-" + sColumnName + ":" + sValueAA + ", ";
                }
                rowUpload["TerminalID"] = sTerminalId;
                rowUpload["AcquirerName"] = sAcquirerName;
                dtUploadAA.Rows.Add(rowUpload);
            }
        }

        protected void CopyDataTable(DataTable dtSource, ref DataTable dtDest)
        {
            CopyDataTable(dtSource, ref dtDest, true);
        }

        protected void CopyDataTable(DataTable dtSource, ref DataTable dtDest, bool bClear)
        {
            if (dtDest.Rows.Count == 0)
                dtDest = dtSource;
            else
            {
                if(bClear) dtDest.Clear();
                foreach (DataRow row in dtSource.Rows)
                    dtDest.ImportRow(row);
            }
        }

        protected void CopyMasterProfile(string sMasterTerminal, string sTerminalID)
        {
            CopyMasterTerminal(sMasterTerminal, sTerminalID);
            CopyMasterAcquirer(sMasterTerminal, sTerminalID);
            CopyMasterIssuer(sMasterTerminal, sTerminalID);
            CopyMasterRelation(sMasterTerminal, sTerminalID);
        }

        protected void CopyMasterTerminal(string sMasterTerminal, string sTerminalID)
        {
            dtTerminalList.Clear();
            var masterProfile1 = from master in dtMaster.AsEnumerable()
                                 where (master.Field<string>("TerminalID").Equals(sMasterTerminal) &&
                                 master.Field<string>("TagId").StartsWith("DE"))
                                 orderby master.Field<string>("TagId")
                                 select master;

            var masterProfile2 = from master in dtMaster.AsEnumerable()
                                 where (master.Field<string>("TerminalID").Equals(sMasterTerminal) &&
                                 master.Field<string>("TagId").StartsWith("DC"))
                                 orderby master.Field<string>("TagId")
                                 select master;
            var masterTerminal = masterProfile1.Union(masterProfile2);

            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalID;
            DataTable dtTemp = new DataTable();
            dtTemp = dtMaster.Clone();
            masterTerminal.CopyToDataTable(dtTemp, LoadOption.OverwriteChanges);
            dtTemp.Columns.Remove(colTerminalId.ColumnName);
            dtTemp.Columns.Add(colTerminalId);
            DataColumn colName = new DataColumn("Name");
            colName.DefaultValue = colTerminalId.DefaultValue;
            dtTemp.Columns.Remove(colName.ColumnName);
            dtTemp.Columns.Add(colName);
            CopyDataTable(dtTemp, ref dtTerminal);

            DataRow[] rowsMaster = dtMasterList.Select(string.Format("TerminalID='{0}'", sMasterTerminal));
            dtTerminalList.ImportRow(rowsMaster[0]);
            dtTerminalList.Rows[dtTerminalList.Rows.Count - 1]["TerminalID"] = sTerminalID;
        }

        protected void CopyMasterAcquirer(string sMasterTerminal, string sTerminalID)
        {
            var masterAcquirer = from master in dtMaster.AsEnumerable()
                                 where master.Field<string>("TerminalID") == sMasterTerminal && master.Field<string>("TagID").StartsWith("AA")
                                 orderby master.Field<string>("Name"), master.Field<string>("TagId")
                                 select master;
            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalID;
            DataTable dtTemp = new DataTable();
            dtTemp = dtMaster.Clone();
            masterAcquirer.CopyToDataTable(dtTemp, LoadOption.OverwriteChanges);
            dtTemp.Columns.Remove(colTerminalId.ColumnName);
            dtTemp.Columns.Add(colTerminalId);
            CopyDataTable(dtTemp, ref dtAcquirer);
        }

        protected void CopyMasterIssuer(string sMasterTerminal, string sTerminalID)
        {
            var masterIssuer = from master in dtMaster.AsEnumerable()
                               where master.Field<string>("TerminalID") == sMasterTerminal && master.Field<string>("TagID").StartsWith("AE")
                               orderby master.Field<string>("Name"), master.Field<string>("TagId")
                               select master;
            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalID;
            DataTable dtTemp = new DataTable();
            dtTemp = dtMaster.Clone();
            masterIssuer.CopyToDataTable(dtTemp, LoadOption.OverwriteChanges);
            dtTemp.Columns.Remove(colTerminalId.ColumnName);
            dtTemp.Columns.Add(colTerminalId);
            CopyDataTable(dtTemp, ref dtIssuer);
        }

        protected void CopyMasterRelation(string sMasterTerminal, string sTerminalID)
        {
            
            var masterRelation = from master in dtMaster.AsEnumerable()
                                 where master.Field<string>("TerminalID") == sMasterTerminal && master.Field<string>("TagID").StartsWith("AD")
                                 select master;
            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalID;
            DataTable dtTemp = new DataTable();
            dtTemp = dtMaster.Clone();
            masterRelation.CopyToDataTable(dtTemp, LoadOption.OverwriteChanges);
            dtTemp.Columns.Remove(colTerminalId.ColumnName);
            dtTemp.Columns.Add(colTerminalId);
            CopyDataTable(dtTemp, ref dtRelation);
        }

        protected void StartUploadDelete()
        {
            if (oSqlConn.State != ConnectionState.Open)
                oSqlConn.Open();
            try
            {
                #region "TEDIE!!!"
                //// Record Data Terminal delete in Log
                //sDeleteLogDE = null;

                //foreach (DataRow Row in dtUploadDE.Rows)
                //{
                //    string sTerminalID = Row["TerminalID"].ToString();
                //    foreach (DataColumn columnDE in dtUploadDE.Columns)
                //    {
                //        string sItemList = columnDE.ToString();
                //        string sTerminalValue;
                //        if (columnDE.ToString() != "TerminalID")
                //        {
                //            using (SqlCommand oCmd = new SqlCommand(sSPGetTerminalValue, oSqlConn))
                //            {
                //                oCmd.CommandType = CommandType.StoredProcedure;
                //                oCmd.CommandTimeout = 0;
                //                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                //                oCmd.Parameters.Add("@sItemName", SqlDbType.VarChar).Value = sItemList;
                //                oCmd.Parameters.Add("@sTerminalValue", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                //                oCmd.ExecuteNonQuery();
                //                if (oCmd.Parameters["@sTerminalValue"].Value.ToString() == "1")
                //                    sTerminalValue = "True";
                //                else if (oCmd.Parameters["@sTerminalValue"].Value.ToString() == "0")
                //                    sTerminalValue = "False";
                //                else
                //                    sTerminalValue = oCmd.Parameters["@sTerminalValue"].Value.ToString();
                //                sDeleteLogDE = sDeleteLogDE + sItemList + " : " + sTerminalValue + ", ";
                //            }
                //        }
                //        else sDeleteLogDE = sDeleteLogDE + sItemList + " : " + sTerminalID + ", ";
                //    }

                //}

                //// Record Data Acquirer delete in Log
                //sDeleteLogAA = null;

                //foreach (DataRow Row in dtUploadAA.Rows)
                //{
                //    foreach (DataColumn ColumnAA in dtUploadAA.Columns)
                //    {
                //        if (ColumnAA.ToString() != "TerminalID" && ColumnAA.ToString() != "AcquirerName")
                //        {
                //            string sItemList = ColumnAA.ToString();
                //            string sTerminalID = Row["TerminalID"].ToString();
                //            string sAcquirerName = Row["AcquirerName"].ToString();

                //            using (SqlCommand oCmd = new SqlCommand(sSPGetAcquirerValue, oSqlConn))
                //            {
                //                oCmd.CommandType = CommandType.StoredProcedure;
                //                oCmd.CommandTimeout = 0;
                //                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                //                oCmd.Parameters.Add("@sAcquirerName", SqlDbType.VarChar).Value = sAcquirerName;
                //                oCmd.Parameters.Add("@sItemName", SqlDbType.VarChar).Value = sItemList;
                //                oCmd.Parameters.Add("@sAcquirerValue", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                //                oCmd.ExecuteNonQuery();
                //                sDeleteLogAA = sDeleteLogAA + sTerminalID + "-" + sAcquirerName + "-" + sItemList + " : " + oCmd.Parameters["@sAcquirerValue"].Value.ToString() + ", ";
                //            }
                //        }
                //    }
                //}
                #endregion

                foreach (DataRow row in dtUploadDE.Rows)
                {
                    string sTerminalID = row["TerminalID"].ToString();
                    string sUserIdOnAccess = null;
                    if ((IsAllowUserAccess(oSqlConn, sTerminalID, ref sUserIdOnAccess) && uploadMode == UploadMode.Delete)
                        || uploadMode == UploadMode.Update)
                    {
                        UserAccessInsert(sTerminalID);
                        using (SqlCommand oCmd = new SqlCommand(sSPProfileTerminalDeleteProcess, oSqlConn))
                        {
                            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                            oCmd.CommandType = CommandType.StoredProcedure;
                            oCmd.CommandTimeout = 0;
                            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                            oCmd.ExecuteNonQuery();
                            if (uploadMode == UploadMode.Delete)
                            {
                                WriteUploadLog(string.Format("Upload DELETE Profile {0}, DONE.", sTerminalID));
                                InputLog(oSqlConn, sTerminalID, sUserID, sDBName, "Upload -> Delete Terminal " + sTerminalID + " Success", "Delete " + sTerminalID + " " + sDeleteLogDE + sDeleteLogAA);
                            }
                        }
                        if (uploadMode == UploadMode.Delete) UserAccessDelete(sTerminalID);
                    }
                    else
                        WriteUploadLog(
                            string.Format("Upload DELETE Profile {0}, FAILED. Profile is been accessed by {1}", sTerminalID, sUserIdOnAccess));
                }
            }
            catch (Exception ex)
            {
                WriteUploadLog(ex.Message);
            }
        }

        protected void StartUploadAdd()
        {
            if (oSqlConn.State != ConnectionState.Open)
                oSqlConn.Open();
            try
            {   
                foreach (DataRow rowUploadDE in dtUploadDE.Rows)
                {
                    string sTerminalID = rowUploadDE["TerminalID"].ToString();

                    CopyMasterProfile(rowUploadDE["Master"].ToString(), sTerminalID);

                    if (dtTerminal.Rows[0]["TagId"].ToString().Length == 5) iVersion = 2;
                    else iVersion = 1;

                    if (!IsTerminalExist(sTerminalID))
                    {
                        string sUserIdOnAccess = null;
                        if ((IsAllowUserAccess(oSqlConn, sTerminalID, ref sUserIdOnAccess) && uploadMode == UploadMode.Add)
                            || uploadMode == UploadMode.Update)
                        {
                            if (uploadMode == UploadMode.Add) UserAccessInsert(sTerminalID);
                            #region "Start Process Terminal Table"
                            foreach (DataColumn colUploadDE in dtUploadDE.Columns)
                            {
                                if (colUploadDE.ColumnName != "TerminalID" && colUploadDE.ColumnName != "Master")
                                {
                                    var varTag = from refDE in dtReferenceDE.AsEnumerable()
                                               where refDE.Field<string>("ColumnName") == colUploadDE.ColumnName
                                               select refDE.Field<string>("Tag");
                                    string sTag = (varTag.ToArray())[0];
                                    if (iVersion == 2) sTag = sTag.Insert(2, "0");

                                    DataRow[] rowUpdate = dtTerminal.Select(
                                        string.Format("TerminalID='{0}' AND TagId='{1}'",
                                        sTerminalID, sTag)
                                        );
                                    string sTagValue = rowUploadDE[colUploadDE].ToString();
                                    
                                    //switch (varTag.ToString())
                                    switch (sTag)
                                    {
                                        case "DE02" :
                                        case "DE002":
                                        case "DE03" :
                                        case "DE003":
                                        case "DE04" :
                                        case "DE004":
                                            sTagValue = sTagValue.TrimEnd();
                                            sTagValue = sTagValue.Length > 23 ? sTagValue.Substring(0, 23) : sTagValue;
                                            break;
                                    }

                                    dtTerminal.Rows[dtTerminal.Rows.IndexOf(rowUpdate[0])]["TagLength"] = sTagValue.Length;
                                    dtTerminal.Rows[dtTerminal.Rows.IndexOf(rowUpdate[0])]["TagValue"] = sTagValue;
                                }
                            }
                            //dtTerminal.Rows[dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId='DE01' AND TerminalId='{0}'", sTerminalID)))[0])]["TagValue"]
                            //    = sTerminalID;
                            dtTerminal.Rows[dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE01','DE001') AND TerminalId='{0}'", sTerminalID)))[0])]["TagValue"]
                                = sTerminalID;
                            #endregion
                            #region "Start Process Acquirer Table"
                            //dtUploadAA.Select(string.Format("TerminalId='{0}'",sTerminalID))
                            foreach (DataRow rowUploadAA in dtUploadAA.Select(string.Format("TerminalID='{0}'", sTerminalID)))
                            {
                                if (rowUploadAA["TerminalID"].ToString() == sTerminalID)
                                {
                                    string sAcquirerName = rowUploadAA["AcquirerName"].ToString();
                                    string sAcquirerNameSearch = sAcquirerName;
                                    switch (sAcquirerNameSearch)
                                    {
                                        case "BCA":
                                            //if (dtAcquirer.Select(string.Format("TagId='AA01' AND TerminalId='{0}' AND TagValue='BCA1'", sTerminalID)).Count() > 0)
                                            if (dtAcquirer.Select(string.Format("TagId IN ('AA01','AA001') AND TerminalId='{0}' AND TagValue='BCA1'", sTerminalID)).Count() > 0)
                                                sAcquirerName = "BCA1";
                                            break;
                                        //case "PROMO F":
                                        //    sAcquirerName = "PROMO SQ A";
                                        //    break;
                                        //case "PROMO G":
                                        //    sAcquirerName = "PROMO SQ B";
                                        //    break;
                                        //case "PROMO H":
                                        //    sAcquirerName = "PROMO SQ C";
                                        //    break;
                                        //case "PROMO I":
                                        //    sAcquirerName = "PROMO SQ D";
                                        //    break;
                                        //case "PROMO J":
                                        //    sAcquirerName = "PROMO SQ E";
                                        //    break;
                                    }
                                    foreach (DataColumn colUploadAA in dtUploadAA.Columns)
                                    {
                                        if (colUploadAA.ColumnName != "TerminalID" && colUploadAA.ColumnName != "AcquirerName")
                                        {
                                            if (!string.IsNullOrEmpty(rowUploadAA[colUploadAA].ToString()))
                                            {
                                                var varTag = from refAA in dtReferenceAA.AsEnumerable()
                                                           where refAA.Field<string>("AcquirerName") == sAcquirerNameSearch &&
                                                           refAA.Field<string>("ColumnName") == colUploadAA.ColumnName
                                                           select refAA.Field<string>("Tag");
                                                string sTag = (varTag.ToArray())[0];
                                                if (iVersion == 2) sTag = sTag.Insert(2, "0");

                                                DataRow[] rowUpdate = dtAcquirer.Select(
                                                    string.Format("TerminalID='{0}' AND Name='{1}' AND TagId='{2}'",
                                                    sTerminalID, sAcquirerName, sTag));
                                                if (rowUpdate.Count() > 0)
                                                {
                                                    string sTagValue = rowUploadAA[colUploadAA].ToString();
                                                    dtAcquirer.Rows[dtAcquirer.Rows.IndexOf(rowUpdate[0])]["TagLength"] = sTagValue.Length;
                                                    dtAcquirer.Rows[dtAcquirer.Rows.IndexOf(rowUpdate[0])]["TagValue"] = sTagValue;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                    break;
                            }
                            #endregion
                           // StartUpdatePwd(sTerminalID);  --DI CIMB DIMATIKAN
                            dtTerminal.AcceptChanges();
                            dtAcquirer.AcceptChanges();

                            StartUploadAddBulk();
                            //SaveFullContentClass.SaveFullContent(oSqlConn, sTerminalID);
                            UserAccessDelete(sTerminalID);

                            #region "TEDIE !!!"
                            // //Split Data DE by TID
                            //if (sTerminalID != null)
                            //{
                            //    sLogDENew = sSplitDataDEEachTID(sLogMessageDE, sTerminalID);
                            //    sLogDEOld = sSplitDataDEEachTID(sDeleteLogDE, sTerminalID);
                            //    int iLastIndex = 0;
                            //    sLogDEComb = null;
                            //    if (sLogDENew.IndexOf("TerminalID : ") != -1)
                            //    {
                            //        int iVar = 0;
                            //        int iIndex = 0;

                            //        string sOldData, sNewData;
                            //        while ((iVar = sLogDENew.IndexOf(":", iVar)) != -1)
                            //        {
                            //            if (iIndex == 0)
                            //            {
                            //                sItemDE = sLogDENew.Substring(0, sLogDENew.IndexOf(":", iIndex) + 1);
                            //                sOldData = sLogDEOld.Substring(sLogDEOld.IndexOf(sItemDE), sLogDEOld.IndexOf(",", iIndex) - sLogDEOld.IndexOf(sItemDE));
                            //                sNewData = sLogDENew.Substring(sLogDENew.IndexOf(sItemDE), sLogDENew.IndexOf(",", iIndex) - sLogDENew.IndexOf(sItemDE));
                            //                sLogDEComb = sLogDEComb + " " + sItemDE + " " + sOldData.Substring(sOldData.IndexOf(":")+1) + " -> " + sNewData.Substring(sNewData.IndexOf(":")+1)+ ",";
                            //                iIndex = iIndex + 1;
                            //            }
                            //            else
                            //            {

                            //                sItemDE = sLogDENew.Substring((sLogDENew.IndexOf(",", iVar - 1) + 1), sLogDENew.IndexOf(",", iVar) + (sLogDENew.IndexOf(",", iVar - 1)));
                            //                sItemDE = sItemDE.Substring(0, sItemDE.IndexOf(":")+1);
                            //                sOldData = sLogDEOld.Substring(sLogDEOld.IndexOf(sItemDE), sLogDEOld.IndexOf(",", iIndex) + sLogDEOld.IndexOf(sItemDE)-3);
                            //                sNewData = sLogDENew.Substring(sLogDENew.IndexOf(sItemDE), sLogDENew.IndexOf(",", iIndex) + sLogDENew.IndexOf(sItemDE)-3);
                            //                sLogDEComb = sLogDEComb + " " + sItemDE + " " + sOldData.Substring(sOldData.IndexOf(":") + 1) + " -> " + sNewData.Substring(sNewData.IndexOf(":") + 1) + ",";
                            //                iIndex = iIndex + 1;
                            //            }
                            //        }
                            //    }

                            //}
                            // //END Split Data DE by TID

                            // //Split Data AA by TID
                            //if (sTerminalID != null)
                            //{
                            //    sLogAANew = sSplitDataAAEachTID(sLogMessageAA, sTerminalID);
                            //    sLogAAOld = sSplitDataAAEachTID(sDeleteLogAA,sTerminalID);  
                            //}
                            // //END Split Data AA by TID
                            #endregion

                            if (uploadMode == UploadMode.Add)
                            {
                                WriteUploadLog(string.Format("Upload {1} Profile {0}, DONE.", sTerminalID, uploadMode.ToString().ToUpper()));
                                InputLog(oSqlConn, sTerminalID, sUserID, sDBName,  "Upload "+ uploadMode.ToString().ToUpper() +" Terminal " + sTerminalID + " Success", uploadMode.ToString().ToUpper() + " " + sLogDENew + ", " + sLogAANew);
                            }
                            else if (uploadMode == UploadMode.Update)
                            {
                                WriteUploadLog(string.Format("Upload {1} Profile {0}, DONE.", sTerminalID, uploadMode.ToString().ToUpper()));
                                InputLog(oSqlConn, sTerminalID, sUserID, sDBName, "Upload " + uploadMode.ToString().ToUpper() + " Terminal " + sTerminalID + " Success", uploadMode.ToString().ToUpper() + " OldData=" + sLogDEOld + ", " + sLogAAOld + " NewData=" + sLogDENew + ", " + sLogAANew);
                            }

                        }
                        else
                            WriteUploadLog(
                                string.Format("Upload ADD Profile {0}, FAILED. Profile is been accessed by {1}", sTerminalID, sUserIdOnAccess));
                    }
                    else
                        WriteUploadLog(
                            string.Format("Upload ADD Profile {0}, FAILED. Profile allready exist", sTerminalID));
                }
                //StartUploadAddBulk();
                //foreach (string sTerminalId in ltTerminalId)
                //{
                //    SaveFullContentClass.SaveFullContent(oSqlConn, sTerminalId);
                //    UserAccessDelete(sTerminalId);
                //}
            }
            catch (Exception ex)
            {
                WriteUploadLog(ex.Message);
            }
        }

        private string sSplitDataAAEachTID(string sLogMessageAA, string sTerminalID)
        {
            sLogAA = null;
            int iIndexOflastData = 0;
            if (sLogMessageAA.IndexOf(sTerminalID + "-") != -1)
            {
                int i = 0;
                while ((i = sLogMessageAA.IndexOf(",", i)) != -1)
                {
                    if ((sLogMessageAA.LastIndexOf(sTerminalID + "-")) < i)
                    {
                        if (iIndexOflastData == 0)
                        {
                            iIndexOflastData = i;
                        }
                    }

                    i++;
                }
                if (iIndexOflastData == 0)
                {
                    iIndexOflastData = sLogMessageAA.Length;
                }

            }
            sLogAA = sLogMessageAA.Substring(sLogMessageAA.IndexOf(sTerminalID + "-", 0), iIndexOflastData - (sLogMessageAA.IndexOf(sTerminalID + "-", 0)));
            sLogAA = sLogAA.Replace(sTerminalID + "-", "");

            return sLogAA;
        }

        private string sSplitDataDEEachTID(string sLogMessageDE, string sTerminalID)
        {
            sLogDE = null;
            int iIndexOflastData = 0;
            if (sLogMessageDE.IndexOf("TerminalID : " + sTerminalID) != -1)
            {
                int i = 0;
                while ((i = sLogMessageDE.IndexOf(", TerminalID : ", i)) != -1)
                {
                    if ((sLogMessageDE.IndexOf("TerminalID : " + sTerminalID)) < i)
                    {
                        if (iIndexOflastData == 0)
                        {
                            iIndexOflastData = i;
                        }
                    }

                    i++;
                }
                if (iIndexOflastData == 0)
                {
                    iIndexOflastData = sLogMessageDE.Length - 2;
                }

            }
            sLogDE = sLogMessageDE.Substring(sLogMessageDE.IndexOf("TerminalID : " + sTerminalID), iIndexOflastData - (sLogMessageDE.IndexOf("TerminalID : " + sTerminalID)));

            return sLogDE;
        }

        protected void StartUploadAddBulk()
        {
            StartUploadAddBulkTerminalList();
            StartUploadAddBulkTerminal();
            StartUploadAddBulkAcquirer();
            StartUploadAddBulkIssuer();
            StartUploadAddBulkRelation();
            ResetDataTableBulk();
        }

        protected void StartUploadAddBulkTerminalList()
        {
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            oSqlBulk.BatchSize = 5000;
            oSqlBulk.BulkCopyTimeout = 0;
            oSqlBulk.DestinationTableName = "tbProfileTerminalList";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapDatabaseId = new SqlBulkCopyColumnMapping("DatabaseID", "DatabaseID");
            SqlBulkCopyColumnMapping mapAllowDownload = new SqlBulkCopyColumnMapping("AllowDownload", "AllowDownload");
            SqlBulkCopyColumnMapping mapStatusMaster = new SqlBulkCopyColumnMapping("StatusMaster", "StatusMaster");
            SqlBulkCopyColumnMapping mapLastView = new SqlBulkCopyColumnMapping("LastView", "LastView");
            SqlBulkCopyColumnMapping mapEnableIP = new SqlBulkCopyColumnMapping("EnableIP", "EnableIP");
            SqlBulkCopyColumnMapping mapIPAddress = new SqlBulkCopyColumnMapping("IPAddress", "IPAddress");
            SqlBulkCopyColumnMapping mapAutoInitTimeStamp = new SqlBulkCopyColumnMapping("AutoInitTimeStamp", "AutoInitTimeStamp");
            SqlBulkCopyColumnMapping mapLocationID = new SqlBulkCopyColumnMapping("LocationID", "LocationID");
            SqlBulkCopyColumnMapping mapInitCompress = new SqlBulkCopyColumnMapping("InitCompress", "InitCompress");
            SqlBulkCopyColumnMapping mapAppPackId = new SqlBulkCopyColumnMapping("AppPackID", "AppPackID");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapDatabaseId);
            oSqlBulk.ColumnMappings.Add(mapAllowDownload);
            oSqlBulk.ColumnMappings.Add(mapStatusMaster);
            oSqlBulk.ColumnMappings.Add(mapLastView);
            oSqlBulk.ColumnMappings.Add(mapEnableIP);
            oSqlBulk.ColumnMappings.Add(mapIPAddress);
            oSqlBulk.ColumnMappings.Add(mapAutoInitTimeStamp);
            oSqlBulk.ColumnMappings.Add(mapLocationID);
            oSqlBulk.ColumnMappings.Add(mapInitCompress);
            oSqlBulk.ColumnMappings.Add(mapAppPackId);
            try
            {
                oSqlBulk.WriteToServer(dtTerminalList);
            }
            catch (Exception ex)
            {
                WriteUploadLog(ex.Message);
            }
        }

        protected void StartUploadAddBulkTerminal()
        {
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            oSqlBulk.BatchSize = 5000;
            oSqlBulk.BulkCopyTimeout = 0;
            oSqlBulk.DestinationTableName = "tbProfileTerminal";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapTerminalTag = new SqlBulkCopyColumnMapping("TagId", "TerminalTag");
            SqlBulkCopyColumnMapping mapTerminalTagValue = new SqlBulkCopyColumnMapping("TagValue", "TerminalTagValue");
            SqlBulkCopyColumnMapping mapTerminalTagLength = new SqlBulkCopyColumnMapping("TagLength", "TerminalTagLength");
            SqlBulkCopyColumnMapping mapTerminalLengthOfTagLength = new SqlBulkCopyColumnMapping("LengthOfTagLength", "TerminalLengthOfTagLength");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapTerminalTag);
            oSqlBulk.ColumnMappings.Add(mapTerminalTagValue);
            oSqlBulk.ColumnMappings.Add(mapTerminalTagLength);
            oSqlBulk.ColumnMappings.Add(mapTerminalLengthOfTagLength);
            try
            {
                oSqlBulk.WriteToServer(dtTerminal);
            }
            catch (Exception ex)
            {
                WriteUploadLog(ex.Message);
            }
        }

        protected void StartUploadAddBulkAcquirer()
        {
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            oSqlBulk.BatchSize = 5000;
            oSqlBulk.BulkCopyTimeout = 0;
            oSqlBulk.DestinationTableName = "tbProfileAcquirer";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapAcquirerTag = new SqlBulkCopyColumnMapping("TagId", "AcquirerTag");
            SqlBulkCopyColumnMapping mapAcquirerName = new SqlBulkCopyColumnMapping("Name", "AcquirerName");
            SqlBulkCopyColumnMapping mapAcquirerTagValue = new SqlBulkCopyColumnMapping("TagValue", "AcquirerTagValue");
            SqlBulkCopyColumnMapping mapAcquirerTagLength = new SqlBulkCopyColumnMapping("TagLength", "AcquirerTagLength");
            SqlBulkCopyColumnMapping mapAcquirerLengthOfTagLength = new SqlBulkCopyColumnMapping("LengthOfTagLength", "AcquirerLengthOfTagLength");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapAcquirerName);
            oSqlBulk.ColumnMappings.Add(mapAcquirerTag);
            oSqlBulk.ColumnMappings.Add(mapAcquirerTagValue);
            oSqlBulk.ColumnMappings.Add(mapAcquirerTagLength);
            oSqlBulk.ColumnMappings.Add(mapAcquirerLengthOfTagLength);
            try
            {
                oSqlBulk.WriteToServer(dtAcquirer);
            }
            catch (Exception ex)
            {
                WriteUploadLog(ex.Message);
            }
        }

        protected void StartUploadAddBulkIssuer()
        {
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            oSqlBulk.BatchSize = 5000;
            oSqlBulk.BulkCopyTimeout = 0;
            oSqlBulk.DestinationTableName = "tbProfileIssuer";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapIssuerTag = new SqlBulkCopyColumnMapping("TagId", "IssuerTag");
            SqlBulkCopyColumnMapping mapIssuerName = new SqlBulkCopyColumnMapping("Name", "IssuerName");
            SqlBulkCopyColumnMapping mapIssuerTagValue = new SqlBulkCopyColumnMapping("TagValue", "IssuerTagValue");
            SqlBulkCopyColumnMapping mapIssuerTagLength = new SqlBulkCopyColumnMapping("TagLength", "IssuerTagLength");
            SqlBulkCopyColumnMapping mapIssuerLengthOfTagLength = new SqlBulkCopyColumnMapping("LengthOfTagLength", "IssuerLengthOfTagLength");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapIssuerName);
            oSqlBulk.ColumnMappings.Add(mapIssuerTag);
            oSqlBulk.ColumnMappings.Add(mapIssuerTagValue);
            oSqlBulk.ColumnMappings.Add(mapIssuerTagLength);
            oSqlBulk.ColumnMappings.Add(mapIssuerLengthOfTagLength);
            try
            {
                oSqlBulk.WriteToServer(dtIssuer);
            }
            catch (Exception ex)
            {
                WriteUploadLog(ex.Message);
            }
        }

        protected void StartUploadAddBulkRelation()
        {
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            oSqlBulk.BatchSize = 5000;
            oSqlBulk.BulkCopyTimeout = 0;
            oSqlBulk.DestinationTableName = "tbProfileRelation";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapRelationTag = new SqlBulkCopyColumnMapping("TagId", "RelationTag");
            SqlBulkCopyColumnMapping mapRelationTagValue = new SqlBulkCopyColumnMapping("TagValue", "RelationTagValue");
            SqlBulkCopyColumnMapping mapRelationTagLength = new SqlBulkCopyColumnMapping("TagLength", "RelationTagLength");
            SqlBulkCopyColumnMapping mapRelationLengthOfTagLength = new SqlBulkCopyColumnMapping("LengthOfTagLength", "RelationLengthOfTagLength");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapRelationTag);
            oSqlBulk.ColumnMappings.Add(mapRelationTagValue);
            oSqlBulk.ColumnMappings.Add(mapRelationTagLength);
            oSqlBulk.ColumnMappings.Add(mapRelationLengthOfTagLength);
            try
            {
                oSqlBulk.WriteToServer(dtRelation);
            }
            catch (Exception ex)
            {
                WriteUploadLog(ex.Message);
            }
        }

        protected void StartUpdatePwd(string sTerminalId)
        {
            if (int.Parse(sTerminalId.Substring(2)) >= 0)
            {
                int iLastDigit = iChekDigit(sTerminalId.Substring(5));

                string sPwdInit = string.Format("{0}{1}", sTerminalId.Substring(5), iLastDigit);
                string sPwdFunc2 = CommonLib.sReverseString(sPwdInit);
                string _tmp = (iLastDigit + 2).ToString();
                string tmp = _tmp.Substring(_tmp.Length - 1, 1);
                string sPwdAdmin99 = string.Format("99{0}{1}",
                    sTerminalId.Substring(5),
                    (int.Parse(sPwdInit) + 2).ToString().PadLeft(4, '0').Substring(3, 1));

                dtTerminal.Rows[
                    dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE24','DE024') AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                    = sPwdAdmin99;
                dtTerminal.Rows[
                    dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE28','DE028') AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                    = sPwdAdmin99;
                dtTerminal.Rows[
                    dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE25','DE025') AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                    = sPwdFunc2;
                dtTerminal.Rows[
                    dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE42','DE042') AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                    = sPwdInit;
            }
        }
        
        protected int iChekDigit(string sOrigVal)
        {
            int i_digit = 0;
            int calc_digit = 0;

            if (sOrigVal.Length % 2 == 1)
            {
                sOrigVal = sOrigVal + "0";
            }

            calc_digit = 0;
            int j = 1;
            int msg_len = sOrigVal.Length - 1;
            //int i_digit = 0;
            for (int i = msg_len; i >= 0; i += -1)
            {
                string digit = sOrigVal.Substring(i, 1);
                foreach (char c in digit)
                {
                    i_digit = (int)c;
                }
                // int i_digit = (int)digit;
                if (i_digit >= (int)'0' & i_digit <= (int)'9')
                {
                    i_digit = i_digit - 48;
                }
                else
                {
                    if (i_digit >= (int)'A' & i_digit <= (int)'Z')
                    {
                        i_digit = (i_digit - 58) % 10;
                    }
                    else
                    {
                        if (i_digit >= (int)'a' & i_digit <= (int)'z')
                        {
                            i_digit = (i_digit - 58 - 32) % 10;
                        }
                        else
                        {
                            i_digit = (i_digit - 32) % 10;
                        }
                    }
                }
                if (j % 2 == 1)
                {
                    i_digit = i_digit * 2;
                }
                if (i_digit >= 10)
                {
                    i_digit = ((i_digit - (i_digit % 10)) / 10) + (i_digit % 10);
                }
                calc_digit = calc_digit + i_digit;
                j = j + 1;
            }

            int temp_mod = calc_digit % 10;
            int temp_div = (calc_digit - (calc_digit % 10)) / 10;
            if (temp_mod != 0)
            {
                temp_div = temp_div + 1;
            }
            return (temp_div * 10) - calc_digit;
        }

        protected void ResetDataTableBulk()
        {
            //dtTerminalList = new DataTable();
            //dtTerminal = new DataTable();
            //dtAcquirer = new DataTable();
            //dtIssuer = new DataTable();
            //dtRelation = new DataTable();
            dtTerminalList.Clear();
            dtTerminal.Clear();
            dtAcquirer.Clear();
            dtIssuer.Clear();
            dtRelation.Clear();
            
        }

        #region "Misc"
        protected void WriteUploadLog(string sMessage)
        {
            sLogMessage = string.Format("{0}{1:yyyy-MM-dd hh:mm:ss.fff tt} : {2}\n", 
                sLogMessage, 
                DateTime.Now,
                sMessage);
        }

        protected void UserAccessInsert(string _sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand(sSPUserAccessInsert, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open)
                    oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = sUserID;
                oCmd.ExecuteNonQuery();
            }
        }

        protected void UserAccessDelete(string _sTerminalId)
        {
            if (!string.IsNullOrEmpty(sUserID))
                using (SqlCommand oCmd = new SqlCommand(sSPUserAccessDelete, oSqlConn))
                {
                    if (oSqlConn.State != ConnectionState.Open)
                        oSqlConn.Open();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = sUserID;
                    oCmd.ExecuteNonQuery();
                }
        }
        #endregion
        public static void InputLog(SqlConnection oSqlConn, string sTID, string sUsrID, string sDBName, string sActDesc, string sActDetl)
        {
            try
            {
                SqlParameter[] oSqlParam = new SqlParameter[5];
                oSqlParam[0] = new SqlParameter("@sTerminalID", System.Data.SqlDbType.VarChar, 8);
                oSqlParam[0].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[0].Value = sTID;
                oSqlParam[1] = new SqlParameter("@sUserId", System.Data.SqlDbType.VarChar, 10);
                oSqlParam[1].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[1].Value = sUsrID;
                oSqlParam[2] = new SqlParameter("@sDatabaseName", System.Data.SqlDbType.VarChar, 50);
                oSqlParam[2].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[2].Value = sDBName;
                oSqlParam[3] = new SqlParameter("@sActionDesc", System.Data.SqlDbType.VarChar, sActDesc.Length);
                oSqlParam[3].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[3].Value = sActDesc;
                oSqlParam[4] = new SqlParameter("@sActionDetail", System.Data.SqlDbType.VarChar, sActDetl.Length);
                oSqlParam[4].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[4].Value = (string.IsNullOrEmpty(sActDetl)) ? null : sActDetl;
                SqlCommand oSqlCmd = new SqlCommand(sSPAuditTrailInsert, oSqlConn);
                if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
                oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                oSqlCmd.Parameters.AddRange(oSqlParam);
                oSqlCmd.ExecuteNonQuery();

                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                string sError;
                sError = ex.Message;
            }
        }
    }

    public class ClassNewDataUpload
    {
        protected SqlConnection oSqlConn = new SqlConnection();
        protected DataTable dtExcel = new DataTable(); //data table for excel file
        protected DataTable dtTagCollDE = new DataTable(); //data table for terminal column name
        protected DataTable dtTagCollAA = new DataTable(); //data table for acquirer column name
        protected DataTable dtUploadDE = new DataTable(); //data table for upload terminal
        protected DataTable dtUploadAA = new DataTable(); //data table for upload acquirer
        protected DataTable dtReferenceDE = new DataTable(); //data table for terminal tag reference
        protected DataTable dtReferenceAA = new DataTable(); //data table for acquirer tag reference

        //data table for bulk upload
        protected DataTable dtTerminalList = new DataTable();
        protected DataTable dtTerminal = new DataTable();
        protected DataTable dtAcquirer = new DataTable();
        protected DataTable dtIssuer = new DataTable();
        protected DataTable dtRelation = new DataTable();
        protected DataTable dtCustomMenu = new DataTable();

        protected DataTable dtMasterList = new DataTable();
        protected DataTable dtMaster = new DataTable();
        protected bool bCustomMenu = false;
        protected string  sSerialNumber;
        protected bool bSerialNumber;
        protected string sLogMessageDE, sLogMessageAA, sLogDE, sLogAA, sDeleteLogDE, sDeleteLogAA,sLogDENew,sLogAANew,sLogDEOld,sLogAAOld,sLogDEComb,sLogAAComb, sItemDE;
        protected string sLogMessage = null;
        public string LogMessage { get { return sLogMessage; } set { sLogMessage = value; } }
        protected string sUserID = null;
        protected string sDBName = "";

        public bool bUploadRandomPwd = true;
        public int iTypeRandomPassword = 0;
        
        #region "Data Upload StoredProcedure"
        public static string sSPAuditTrailInsert = "spAuditTrailInsert";
        public static string sSPGetDbIDByTerminalID = "spGetDatabaseIDByTerminalID";
        protected static string sSPUploadTagDECount = "spUploadTagDECount";
        protected static string sSPUploadTagAACount = "spUploadTagAACount";

        protected static string sSPUploadTagDESourceCount = "spUploadTagDESourceCount";
        protected static string sSPUploadTagAASourceCount = "spUploadTagAASourceCount";

        protected static string sSPUploadTagDEInsert = "spUploadTagDEInsert";
        protected static string sSPUploadTagAAInsert = "spUploadTagAAInsert";

        protected static string sSPUploadTagDEDelete = "spUploadTagDEDelete";
        protected static string sSPUploadTagAADelete = "spUploadTagAADelete";

        protected static string sSPUploadTagDEAAUnion = "spUploadTagDEAAUnion";

        protected static string sSPUploadTagDEUpdateSourceColumn = "spUploadTagDEUpdateSourceColumn";
        protected static string sSPUploadTagAAUpdateSourceColumn = "spUploadTagAAUpdateSourceColumn";

        protected static string sSPUploadControlSet = "spUploadControlSet";
        protected static string sSPUploadControlGet = "spUploadControlGet";

        protected static string sSPUploadGetColumnsDE = "spUploadGetColumnsDE";
        protected static string sSPUploadGetColumnsAA = "spUploadGetColumnsAA";

        protected static string sSPDataUploadStartProcessAdd = "spDataUploadStartProcessAdd";
        protected static string sSPDataUploadCreateTable = "spDataUploadCreateTable";
        protected static string sSPDataUploadDropTable = "spDataUploadDropTable";
        protected static string sSPDataUploadDelete = "spDataUploadDelete";

        protected static string sSPUploadTagDEBrowse = "spUploadTagDEBrowse";
        protected static string sSPUploadTagAABrowse = "spUploadTagAABrowse";

        protected static string sSPDataUploadStartProcessUpdate = "spDataUploadStartProcessUpdate";
        protected static string sSPDataUploadStartProcessDelete = "spDataUploadStartProcessDelete";

        protected static string sSPProfileTerminalContentBrowse = "spProfileTerminalContentBrowse";
        protected static string sSPProfileTerminalDeleteProcess = "spProfileTerminalDeleteProcess";
        protected static string sSPProfileSerialNumberDeleteProcess = "sSPProfileSerialNumberDeleteProcess";
        protected static string sSPTerminalListBrowse = "spProfileTerminalListBrowse";
        protected static string sSPSerialNumberListBrowse = "spProfileSerialNumberList";
        protected static string sSPGetTerminalValue = "spGetTerminalValueByTIDItemName";
        protected static string sSPGetAcquirerValue = "spGetAcquirerValueByTIDAcquirerNameItemName";

        protected static string sSPUserAccessInsert = "spUserAccessInsert";
        protected static string sSPUserAccessDelete = "spUserAccessDelete";
        protected static string sSPUserAccessBrowse = "spUserAccessBrowse";
        #endregion

        public enum UploadMode
        {
            Add = 1,
            Update,
            Delete,
        }

        protected UploadMode uploadMode;

        protected int iVersion = 1;

        public ClassNewDataUpload(SqlConnection _oSqlConn, DataTable _dtExcel) : this(_oSqlConn, _dtExcel, null) { }
        public ClassNewDataUpload(SqlConnection _oSqlConn, DataTable _dtExcel, string _sTemplateName)
        {
            oSqlConn = _oSqlConn;
            dtExcel = _dtExcel;
            Init(_sTemplateName);
            //Init("Template");
        }

        #region "Initialize"
        protected void Init(string _sTemplateName)
        {
            InitTagCollection(_sTemplateName);

            InitTableReference(_sTemplateName);

            InitTableUpload();

            InitMasterProfile();
            InitTableProfile();
        }

        protected void InitTagCollection(string _sTemplateName)
        {
            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }

            using (SqlCommand cmdUploadGetColumnDE = new SqlCommand(sSPUploadGetColumnsDE, oSqlConn))
            {
                cmdUploadGetColumnDE.CommandType = CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(_sTemplateName))
                    cmdUploadGetColumnDE.Parameters.Add("@sTemplate", SqlDbType.VarChar, 25).Value = _sTemplateName;
                
                SqlDataAdapter adaptUploadGetColumnDE = new SqlDataAdapter(cmdUploadGetColumnDE);
                adaptUploadGetColumnDE.Fill(dtTagCollDE);
            }

            using (SqlCommand cmdUploadGetColumnAA = new SqlCommand(sSPUploadGetColumnsAA, oSqlConn))
            {
                cmdUploadGetColumnAA.CommandType = CommandType.StoredProcedure;
                
                if (!string.IsNullOrEmpty(_sTemplateName))
                    cmdUploadGetColumnAA.Parameters.Add("@sTemplate", SqlDbType.VarChar, 25).Value = _sTemplateName;

                (new SqlDataAdapter(cmdUploadGetColumnAA)).Fill(dtTagCollAA);
            }

            //new SqlDataAdapter(new SqlCommand(sSPUploadGetColumnsDE, oSqlConn)).Fill(dtTagCollDE);
            //new SqlDataAdapter(new SqlCommand(sSPUploadGetColumnsAA, oSqlConn)).Fill(dtTagCollAA);
        }

        protected void InitTableUpload()
        {
            foreach (DataRow oRow in dtTagCollDE.Rows)
            {
                dtUploadDE.Columns.Add(oRow[0].ToString());
            }
            dtUploadAA.Columns.Add("TerminalID");
            dtUploadAA.Columns.Add("AcquirerName");
            foreach (DataRow oRow in dtTagCollAA.Rows)
            {
                dtUploadAA.Columns.Add(oRow[0].ToString());
            }
        }

        protected void InitTableReference(string _sTemplate)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            using (SqlCommand cmdUploadTagDEBrowse = new SqlCommand(sSPUploadTagDEBrowse, oSqlConn))
            {
                cmdUploadTagDEBrowse.CommandType = CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(_sTemplate))
                    //cmdUploadTagDEBrowse.Parameters.Add("@sFile", SqlDbType.VarChar, 25).Value = _sTemplate;
                cmdUploadTagDEBrowse.Parameters.Add("@sTemplate", SqlDbType.VarChar, 25).Value = _sTemplate;
                
                //SqlDataAdapter adaptUploadTagDEBrowse = new SqlDataAdapter(cmdUploadTagDEBrowse);
                //adaptUploadTagDEBrowse.Fill(dtReferenceDE);
                new SqlDataAdapter(cmdUploadTagDEBrowse).Fill(dtReferenceDE);
            }

            using (SqlCommand cmdUploadTagAABrowse = new SqlCommand(sSPUploadTagAABrowse, oSqlConn))
            {
                cmdUploadTagAABrowse.CommandType = CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(_sTemplate))
                    //cmdUploadTagAABrowse.Parameters.Add("@sFile", SqlDbType.VarChar, 25).Value = _sTemplate;
                cmdUploadTagAABrowse.Parameters.Add("@sTemplate", SqlDbType.VarChar, 25).Value = _sTemplate;

                //SqlDataAdapter adaptUploadTagAABrowse = new SqlDataAdapter(cmdUploadTagAABrowse);
                //adaptUploadTagAABrowse.Fill(dtReferenceAA);
                new SqlDataAdapter(cmdUploadTagAABrowse).Fill(dtReferenceAA);
            }

            //new SqlDataAdapter(new SqlCommand(sSPUploadTagDEBrowse, oSqlConn)).Fill(dtReferenceDE);
            //new SqlDataAdapter(new SqlCommand(sSPUploadTagAABrowse, oSqlConn)).Fill(dtReferenceAA);
        }

        protected void InitMasterProfile()
        {
            var masterProfile = (from excel in dtExcel.AsEnumerable()
                                select excel["Master"]).Distinct();
            string sMasterList = null;
            foreach (var rowMaster in masterProfile.AsEnumerable())
            {
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                using (SqlCommand oCmd = new SqlCommand(sSPProfileTerminalContentBrowse, oSqlConn))
                {
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = rowMaster.ToString();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 0;
                    DataTable dtMasterTemp = new DataTable();
                    new SqlDataAdapter(oCmd).Fill(dtMasterTemp);
                    if (dtMasterTemp.Rows.Count > 0)
                        CopyDataTable(dtMasterTemp, ref dtMaster, false);
                    sMasterList = string.IsNullOrEmpty(sMasterList) ? string.Format("'{0}'", rowMaster.ToString()) :
                        string.Format("{0},'{1}'", sMasterList, rowMaster.ToString());
                }
            }
            using (SqlCommand oCmd = new SqlCommand(sSPTerminalListBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format("WHERE TerminalID IN ({0})", sMasterList);

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                dtMasterList.Load(oCmd.ExecuteReader());
            }
        }

        protected void InitTableProfile()
        {
            dtTerminalList.Clear();
            dtTerminalList = dtMasterList.Clone();
            dtTerminal.Clear();
            dtTerminal = dtMaster.Clone();
            dtAcquirer.Clear();
            dtAcquirer = dtMaster.Clone();
            dtIssuer.Clear();
            dtIssuer = dtMaster.Clone();
            dtRelation.Clear();
            dtRelation = dtMaster.Clone();
        }

        public int iUploadMode
        {
            set
            {
                switch (value)
                {
                    case 1 :
                        uploadMode = UploadMode.Add;
                        break;
                    case 2 :
                        uploadMode = UploadMode.Update;
                        break;
                    case 3 :
                        uploadMode = UploadMode.Delete;
                        break;
                }
            }
        }

        public string UserID { set { sUserID = value; } }
        public string DBName { set { sDBName = value; } }
        #endregion

        #region "Validation"
        public bool IsValidExcel(ref string sErrorColumn, ref int iRowIndex, ref string sAcquirerName)
        {
            var refTable = (from refDE in dtReferenceDE.AsEnumerable()
                            orderby refDE["SourceColumn"]
                            select new
                            {
                                SourceColumn = refDE["SourceColumn"].ToString(),
                                Mandatory = refDE["Mandatory"].ToString(),
                                ColumnName = refDE["ColumnName"].ToString()
                            }).Union
                            (from refAA in dtReferenceAA.AsEnumerable()
                             orderby refAA["SourceColumn"]
                             select new
                             {
                                 SourceColumn = refAA["SourceColumn"].ToString(),
                                 Mandatory = refAA["Mandatory"].ToString(),
                                 ColumnName = refAA["ColumnName"].ToString()
                             });
            foreach (var rowRefTable in refTable.AsEnumerable())
            {
                if (Convert.ToBoolean(rowRefTable.Mandatory))
                {
                    foreach (DataRow row in dtExcel.Rows)
                        //if (string.IsNullOrEmpty(row[int.Parse(rowRefTable.SourceColumn.ToString())].ToString()))
                            if (string.IsNullOrEmpty(row[int.Parse(rowRefTable.SourceColumn.ToString()) - 1].ToString()))
                        {
                            sErrorColumn = rowRefTable.ColumnName.ToString();
                            iRowIndex = dtExcel.Rows.IndexOf(row);

                            if (int.Parse(rowRefTable.SourceColumn) > dtReferenceDE.Rows.Count)
                            {
                                EnumerableRowCollection<DataRow> varAcqName = 
                                    from refAA in dtReferenceAA.AsEnumerable()
                                    where (refAA["SourceColumn"].ToString() == rowRefTable.SourceColumn.ToString()
                                    && refAA["ColumnName"].ToString() == rowRefTable.ColumnName.ToString())
                                    select refAA;
                                sAcquirerName = (varAcqName.AsDataView())[0]["AcquirerName"].ToString();
                            }
                            return false;
                        }
                }
            }
            return true;
        }

        static bool IsAllowUserAccess(SqlConnection _oSqlConn, string _sTerminalId, ref string sUserIdOnAccess)
        {
            bool isAllow = true;
            using (SqlCommand oCmd = new SqlCommand(sSPUserAccessBrowse, _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@sOutput", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();

                sUserIdOnAccess = oCmd.Parameters["@sUserId"].Value.ToString();
                isAllow = oCmd.Parameters["@sOutput"].Value.ToString() == "1" ? false : true;
            }
            return isAllow;
        }

        protected bool IsTerminalExist(string _sTerminalId)
        {
            bool isExist = true;
            DataTable odtTerminal = new DataTable();
            using (SqlCommand oSqlCmd = new SqlCommand(sSPTerminalListBrowse, oSqlConn))
            {

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format("WHERE TerminalID='{0}'", _sTerminalId);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                odtTerminal.Load(oSqlCmd.ExecuteReader());
               
                if (odtTerminal.Rows.Count == 0)
                    isExist = false;
                
            }
            return isExist;
        }

        protected int iDbId(string _sMaster)
        {
            int iValue = 0;
            DataTable odtTerminal = new DataTable();
            using (SqlCommand oSqlCmd = new SqlCommand(sSPGetDbIDByTerminalID, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sMaster;

                SqlDataReader dr = null;
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                dr = oSqlCmd.ExecuteReader();
                if (dr.Read())
                {
                    string svalue = dr.GetValue(0).ToString();
                    iValue = Convert.ToInt32(svalue);
                    dr.Close();
                }
                    //isExist = false;
            }
            return iValue;
        }


        protected bool IsSerialNumberExist(string _sSerialNumber)
        {
            bool isExist = true;
            DataTable odtTerminal = new DataTable();
            using (SqlCommand oSqlCmd = new SqlCommand(sSPSerialNumberListBrowse, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = _sSerialNumber;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                odtTerminal.Load(oSqlCmd.ExecuteReader());
                if (odtTerminal.Rows.Count == 0)
                    isExist = false;
            }
            return isExist;
        }
        #endregion

        public void StartUpload()
        {
            //FillTableUpload();
            
            switch (uploadMode)
            {
                case UploadMode.Update:
                    StartUploadDelete();
                    Thread.Sleep(500);
                    StartUploadAdd();
                    break;
                case UploadMode.Add:
                    StartUploadAdd();
                    break;
                case UploadMode.Delete:
                    StartUploadDelete();
                    break;
            }
        }

        public void StartUploadBCA()
        {
            //FillTableUpload();

            switch (uploadMode)
            {
                case UploadMode.Update:
                    StartUploadDelete();
                    Thread.Sleep(500);
                    StartUploadAddBCA();
                    break;
                case UploadMode.Add:
                    StartUploadAddBCA();
                    break;
                case UploadMode.Delete:
                    StartUploadDelete();
                    break;
            }
        }

        protected void FillTableUpload(DataRow rowExcel)
        {
            FillTableUploadDE(rowExcel);
            if (uploadMode != UploadMode.Delete)
                FillTableUploadAA(rowExcel);
        }

        protected void FillTableUploadDE(DataRow rowExcel)
        {
            dtUploadDE.Clear();
            sLogMessageDE = null;
            //foreach (DataRow rowExcel in dtExcel.Rows)
            //{
                DataRow rowUpload = dtUploadDE.NewRow();
                foreach (DataRow rowRefDE in dtReferenceDE.Rows)
                {
                    int iColumn = int.Parse(rowRefDE["SourceColumn"].ToString()) - 1;
                    rowUpload[rowRefDE["ColumnName"].ToString()] = rowExcel[iColumn].ToString();
                    string sValueDE = null;
                    if (rowExcel[iColumn].ToString() == "0")
                        sValueDE = "False";
                    else if (rowExcel[iColumn].ToString() == "1")
                        sValueDE = "True";
                    else
                        sValueDE = rowExcel[iColumn].ToString();
                    sLogMessageDE = sLogMessageDE + rowRefDE["ColumnName"].ToString() + " : " + sValueDE + ", ";
                }
                dtUploadDE.Rows.Add(rowUpload);
            //}
        }

        protected void FillTableUploadAA(DataRow rowExcel)
        {
            dtUploadAA.Clear();
            sLogMessageAA = null;
            //foreach (DataRow rowExcel in dtExcel.Rows)
            //{
                //string sTerminalId = rowExcel[1].ToString();
            string sTerminalId = rowExcel[0].ToString();
                
                string sAcquirerName = null;
                string sAcquirerNameTemp = "";
                DataRow rowUpload = dtUploadAA.NewRow();
                foreach (DataRow rowRefAA in dtReferenceAA.Rows)
                {
                    int iColumn = int.Parse(rowRefAA["SourceColumn"].ToString()) - 1;
                    string sColumnName = rowRefAA["ColumnName"].ToString();
                    sAcquirerNameTemp = rowRefAA["AcquirerName"].ToString();
                    if (!string.IsNullOrEmpty(sAcquirerName) 
                        && sAcquirerName != sAcquirerNameTemp)
                    {
                        rowUpload["TerminalID"] = sTerminalId;
                        rowUpload["AcquirerName"] = sAcquirerName;
                        dtUploadAA.Rows.Add(rowUpload);
                        rowUpload = dtUploadAA.NewRow();
                    }
                    rowUpload[sColumnName] = rowExcel[iColumn].ToString();
                    sAcquirerName = sAcquirerNameTemp;
                    string sValueAA = null;
                    if (rowExcel[iColumn].ToString() == "0")
                        sValueAA = "False";
                    else if (rowExcel[iColumn].ToString() == "1")
                        sValueAA = "True";
                    else
                        sValueAA = rowExcel[iColumn].ToString();
                    sLogMessageAA = sLogMessageAA + sTerminalId + "-" + sAcquirerName + "-" + sColumnName + ":" + sValueAA + ", ";
                }
                rowUpload["TerminalID"] = sTerminalId;
                rowUpload["AcquirerName"] = sAcquirerName;
                dtUploadAA.Rows.Add(rowUpload);
            //}
        }

        protected void CopyDataTable(DataTable dtSource, ref DataTable dtDest)
        {
            CopyDataTable(dtSource, ref dtDest, true);
        }

        protected void CopyDataTable(DataTable dtSource, ref DataTable dtDest, bool bClear)
        {
            if (dtDest.Rows.Count == 0)
                dtDest = dtSource;
            else
            {
                if(bClear) dtDest.Clear();
                foreach (DataRow row in dtSource.Rows)
                    dtDest.ImportRow(row);
            }
        }

        protected void CopyMasterProfile(string sMasterTerminal, string sTerminalID)
        {
            CopyMasterTerminal(sMasterTerminal, sTerminalID);
            CopyMasterAcquirer(sMasterTerminal, sTerminalID);
            CopyMasterIssuer(sMasterTerminal, sTerminalID);
            CopyMasterRelation(sMasterTerminal, sTerminalID);
            CopyMasterCustomMenu(sMasterTerminal, sTerminalID);
        }

        private void CopyMasterCustomMenu(string sMasterTerminal, string sTerminalID)
        {
            var masterCustomMenu = from master in dtMaster.AsEnumerable()
                                 where master.Field<string>("TerminalID") == sMasterTerminal && master.Field<string>("TagID").StartsWith("MN")
                                 select master;
            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalID;
            DataTable dtTemp = new DataTable();
            dtTemp = dtMaster.Clone();
            masterCustomMenu.CopyToDataTable(dtTemp, LoadOption.OverwriteChanges);
            dtTemp.Columns.Remove(colTerminalId.ColumnName);
            dtTemp.Columns.Add(colTerminalId);
            CopyDataTable(dtTemp, ref dtCustomMenu);
            if (dtCustomMenu.Rows.Count> 0)
                bCustomMenu = true;
        }

        protected void CopyMasterTerminal(string sMasterTerminal, string sTerminalID)
        {
            dtTerminalList.Clear();
            var masterProfile1 = from master in dtMaster.AsEnumerable()
                                 where (master.Field<string>("TerminalID").Equals(sMasterTerminal) &&
                                 master.Field<string>("TagId").StartsWith("DE"))
                                 orderby master.Field<string>("TagId")
                                 select master;

            var masterProfile2 = from master in dtMaster.AsEnumerable()
                                 where (master.Field<string>("TerminalID").Equals(sMasterTerminal) &&
                                 master.Field<string>("TagId").StartsWith("DC"))
                                 orderby master.Field<string>("TagId")
                                 select master;
            var masterTerminal = masterProfile1.Union(masterProfile2);

            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalID;
            DataTable dtTemp = new DataTable();
            dtTemp = dtMaster.Clone();
            masterTerminal.CopyToDataTable(dtTemp, LoadOption.OverwriteChanges);
            dtTemp.Columns.Remove(colTerminalId.ColumnName);
            dtTemp.Columns.Add(colTerminalId);
            DataColumn colName = new DataColumn("Name");
            colName.DefaultValue = colTerminalId.DefaultValue;
            dtTemp.Columns.Remove(colName.ColumnName);
            dtTemp.Columns.Add(colName);
            CopyDataTable(dtTemp, ref dtTerminal);

            DataRow[] rowsMaster = dtMasterList.Select(string.Format("TerminalID='{0}'", sMasterTerminal));
            dtTerminalList.ImportRow(rowsMaster[0]);
            dtTerminalList.Rows[dtTerminalList.Rows.Count - 1]["TerminalID"] = sTerminalID;
            
        }

        protected void CopyMasterAcquirer(string sMasterTerminal, string sTerminalID)
        {
            var masterAcquirer = from master in dtMaster.AsEnumerable()
                                 where master.Field<string>("TerminalID") == sMasterTerminal && master.Field<string>("TagID").StartsWith("AA")
                                 orderby master.Field<string>("Name"), master.Field<string>("TagId")
                                 select master;
            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalID;
            DataTable dtTemp = new DataTable();
            dtTemp = dtMaster.Clone();
            masterAcquirer.CopyToDataTable(dtTemp, LoadOption.OverwriteChanges);
            dtTemp.Columns.Remove(colTerminalId.ColumnName);
            dtTemp.Columns.Add(colTerminalId);
            CopyDataTable(dtTemp, ref dtAcquirer);
        }

        protected void CopyMasterIssuer(string sMasterTerminal, string sTerminalID)
        {
            var masterIssuer = from master in dtMaster.AsEnumerable()
                               where master.Field<string>("TerminalID") == sMasterTerminal && master.Field<string>("TagID").StartsWith("AE")
                               orderby master.Field<string>("Name"), master.Field<string>("TagId")
                               select master;
            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalID;
            DataTable dtTemp = new DataTable();
            dtTemp = dtMaster.Clone();
            masterIssuer.CopyToDataTable(dtTemp, LoadOption.OverwriteChanges);
            dtTemp.Columns.Remove(colTerminalId.ColumnName);
            dtTemp.Columns.Add(colTerminalId);
            CopyDataTable(dtTemp, ref dtIssuer);
        }

        protected void CopyMasterRelation(string sMasterTerminal, string sTerminalID)
        {
            
            var masterRelation = from master in dtMaster.AsEnumerable()
                                 where master.Field<string>("TerminalID") == sMasterTerminal && master.Field<string>("TagID").StartsWith("AD")
                                 select master;
            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalID;
            DataTable dtTemp = new DataTable();
            dtTemp = dtMaster.Clone();
            masterRelation.CopyToDataTable(dtTemp, LoadOption.OverwriteChanges);
            dtTemp.Columns.Remove(colTerminalId.ColumnName);
            dtTemp.Columns.Add(colTerminalId);
            CopyDataTable(dtTemp, ref dtRelation);
        }

        protected void StartUploadDelete()
        {
            if (oSqlConn.State != ConnectionState.Open)
                oSqlConn.Open();
          
            try
            {
                foreach (DataRow rowExcel in dtExcel.Rows)
                {
                    FillTableUpload(rowExcel);
                    //bSerialNumber = ContainColumn("Serial Number", dtExcel);
                    DataColumnCollection colm = dtExcel.Columns;
                    if (colm.Contains("Serial Number"))
                    {
                        bSerialNumber = true;
                    }
                    foreach (DataRow row in dtUploadDE.Rows)
                    {
                        string sTerminalID = row[0].ToString();
                        if (bSerialNumber == true)
                        {
                            sSerialNumber = row[0].ToString();
                        }
                        string sUserIdOnAccess = null;
                        if ((IsAllowUserAccess(oSqlConn, sTerminalID, ref sUserIdOnAccess) && uploadMode == UploadMode.Delete)
                            || uploadMode == UploadMode.Update)
                        {
                            UserAccessInsert(sTerminalID);
                            doDeleteTerminalID(sTerminalID);
                            if (bSerialNumber == true) doDeleteSerialNumber(sTerminalID, sSerialNumber);
                            if (uploadMode == UploadMode.Delete) UserAccessDelete(sTerminalID);
                        }
                        else
                            WriteUploadLog(
                                string.Format("Upload DELETE Profile {0}, FAILED. Profile is been accessed by {1}", sTerminalID, sUserIdOnAccess));
                    }
                }
            }
            catch (Exception ex)
            {
                WriteUploadLog(ex.Message);
            }
        }

        protected void doDeleteTerminalID(string sTerminalID)
        {
            using (SqlCommand oCmd = new SqlCommand(sSPProfileTerminalDeleteProcess, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.CommandTimeout = 0;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oCmd.ExecuteNonQuery();
                if (uploadMode == UploadMode.Delete)
                {
                    WriteUploadLog(string.Format("Upload DELETE Profile {0}, DONE.", sTerminalID));
                    InputLog(oSqlConn, sTerminalID, sUserID, sDBName, "Upload -> Delete Terminal " + sTerminalID + " Success", "Delete " + sTerminalID + " " + sDeleteLogDE + sDeleteLogAA);
                }
            }
        }

        protected void doDeleteSerialNumber(string sTerminalID, string sSerialNumber)
        {
            using (SqlCommand oCmd = new SqlCommand(sSPProfileSerialNumberDeleteProcess, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.CommandTimeout = 0;
                oCmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = sSerialNumber;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oCmd.ExecuteNonQuery();
                if (uploadMode == UploadMode.Delete)
                {
                    WriteUploadLog(string.Format("Upload DELETE TerminalID with  SerialNumber {0}, DONE.", sTerminalID +" --> " + sSerialNumber));
                    InputLog(oSqlConn, sSerialNumber, sUserID, sDBName, "Upload -> Delete TerminalID with SerialNumber " + sTerminalID + " Success", "Delete " + sSerialNumber + " " + "" + "");
                }
            }
        }

        protected void StartUploadAdd()
        {
            if (oSqlConn.State != ConnectionState.Open)
                oSqlConn.Open();
            //bSerialNumber = ContainColumn("Serial Number", dtExcel);
            DataColumnCollection colm = dtExcel.Columns;
            if (colm.Contains("Serial Number"))
            {
                bSerialNumber = true;
            }
            try
            {
                foreach (DataRow rowExcel in dtExcel.Rows)
                {
                    FillTableUpload(rowExcel);

                    foreach (DataRow rowUploadDE in dtUploadDE.Rows)
                    {
                        string sTerminalID = rowUploadDE[0].ToString();

                        if (bSerialNumber == true)
                        {
                            sSerialNumber = rowUploadDE[0].ToString();
                        }


                        CopyMasterProfile(rowUploadDE["Master"].ToString(), sTerminalID);

                        if (dtTerminal.Rows[0]["TagId"].ToString().Length == 5) iVersion = 2;
                        else iVersion = 1;

                        if (uploadMode == UploadMode.Update) doDeleteTerminalID(sTerminalID);
                        if (bSerialNumber == true)
                        {
                            if (IsSerialNumberExist(sSerialNumber))

                            {

                                WriteUploadLog(
                                    string.Format("Upload ADD Profile {0}, FAILED. SerialNumber allready exist", sSerialNumber));
                                break;
                            }

                        }
                        if (!IsTerminalExist(sTerminalID))
                        {
                            string sUserIdOnAccess = null;
                            if ((IsAllowUserAccess(oSqlConn, sTerminalID, ref sUserIdOnAccess) && uploadMode == UploadMode.Add)
                                || uploadMode == UploadMode.Update)
                            {
                                if (uploadMode == UploadMode.Add) UserAccessInsert(sTerminalID);
                                #region "Start Process Terminal Table"
                                string sContent = "";
                                foreach (DataColumn colUploadDE in dtUploadDE.Columns)
                                {
                                      if (colUploadDE.ColumnName != "TerminalID" && colUploadDE.ColumnName != "Master")
                                    {
                                        var varTag = from refDE in dtReferenceDE.AsEnumerable()
                                                     where refDE.Field<string>("ColumnName") == colUploadDE.ColumnName
                                                     select refDE.Field<string>("Tag");
                                        string sTag = (varTag.ToArray())[0];
                                       if (iVersion == 2) sTag = sTag.Insert(2, "0");

                                        string sTagValue = rowUploadDE[colUploadDE].ToString();
                                       
                                        if (sTag.Contains("DE"))
                                        {
                                            DataRow[] rowUpdate = dtTerminal.Select(
                                           string.Format("TerminalID='{0}' AND TagId='{1}'",
                                           sTerminalID, sTag)
                                           );
                                            //switch (varTag.ToString())
                                            switch (sTag)
                                            {
                                                case "DE02":
                                                case "DE002":
                                                case "DE03":
                                                case "DE003":
                                                case "DE04":
                                                case "DE004":
                                                case "DE08":
                                                case "DE008":
                                                    sTagValue = sTagValue.TrimEnd();
                                                    sTagValue = sTagValue.Length > 23 ? sTagValue.Substring(0, 23) : sTagValue;
                                                    //sContent += sTag + sTagValue.Length.ToString("00") + sTagValue;
                                                
                                                    break;
                                            }
                                            try
                                            {
                                                dtTerminal.Rows[dtTerminal.Rows.IndexOf(rowUpdate[0])]["TagLength"] = sTagValue.Length;
                                            }
                                            catch(Exception ex)
                                            {
                                                WriteUploadLog(sTag);
                                            }
                                            dtTerminal.Rows[dtTerminal.Rows.IndexOf(rowUpdate[0])]["TagValue"] = sTagValue;
                                          
                                        }
                                        else
                                        {
                                            try
                                            {
                                                DataRow[] rowUpdate = dtCustomMenu.Select(string.Format("TerminalID='{0}' AND TagId='{1}'", sTerminalID, sTag));

                                                dtCustomMenu.Rows[dtCustomMenu.Rows.IndexOf(rowUpdate[0])]["TagLength"] = sTagValue.Length;
                                                dtCustomMenu.Rows[dtCustomMenu.Rows.IndexOf(rowUpdate[0])]["TagValue"] = sTagValue;
                                            }
                                            catch(Exception ex)
                                            {
                                               // WriteUploadLog(ex.StackTrace);
                                            }
                                        }
                                    }
                                }
                                //dtTerminal.Rows[dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId='DE01' AND TerminalId='{0}'", sTerminalID)))[0])]["TagValue"]
                                //    = sTerminalID;
                                dtTerminal.Rows[dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE01','DE001') AND TerminalId='{0}'", sTerminalID)))[0])]["TagValue"]
                                    = sTerminalID;
                                #endregion
                                #region "Start Process Acquirer Table"
                                //dtUploadAA.Select(string.Format("TerminalId='{0}'",sTerminalID))
                                foreach (DataRow rowUploadAA in dtUploadAA.Select(string.Format("TerminalID='{0}'", sTerminalID)))
                                {
                                    if (rowUploadAA["TerminalID"].ToString() == sTerminalID)
                                    {
                                        string sAcquirerName = rowUploadAA["AcquirerName"].ToString();
                                        string sAcquirerNameSearch = sAcquirerName;
                                        switch (sAcquirerNameSearch)
                                        {
                                            case "BCA":
                                                //if (dtAcquirer.Select(string.Format("TagId='AA01' AND TerminalId='{0}' AND TagValue='BCA1'", sTerminalID)).Count() > 0)
                                                if (dtAcquirer.Select(string.Format("TagId IN ('AA01','AA001') AND TerminalId='{0}' AND TagValue='BCA1'", sTerminalID)).Count() > 0)
                                                    sAcquirerName = "BCA1";
                                                break;
                                            case "PROMO F":
                                                sAcquirerName = "PROMO SQ A";
                                                break;
                                            case "PROMO G":
                                                sAcquirerName = "PROMO SQ B";
                                                break;
                                            case "PROMO H":
                                                sAcquirerName = "PROMO SQ C";
                                                break;
                                            case "PROMO I":
                                                sAcquirerName = "PROMO SQ D";
                                                break;
                                            case "PROMO J":
                                                sAcquirerName = "PROMO SQ E";
                                                break;
                                        }
                                        foreach (DataColumn colUploadAA in dtUploadAA.Columns)
                                        {
                                            if (colUploadAA.ColumnName != "TerminalID" && colUploadAA.ColumnName != "AcquirerName")
                                            {
                                                if (!string.IsNullOrEmpty(rowUploadAA[colUploadAA].ToString()))
                                                {
                                                    var varTag = from refAA in dtReferenceAA.AsEnumerable()
                                                                 where refAA.Field<string>("AcquirerName") == sAcquirerNameSearch &&
                                                                 refAA.Field<string>("ColumnName") == colUploadAA.ColumnName
                                                                 select refAA.Field<string>("Tag");
                                                    string sTag = (varTag.ToArray())[0];
                                                    if (iVersion == 2) sTag = sTag.Insert(2, "0");

                                                    DataRow[] rowUpdate = dtAcquirer.Select(
                                                        string.Format("TerminalID='{0}' AND Name='{1}' AND TagId='{2}'",
                                                        sTerminalID, sAcquirerName, sTag));
                                                    if (rowUpdate.Count() > 0)
                                                    {
                                                        string sTagValue = rowUploadAA[colUploadAA].ToString();
                                                        dtAcquirer.Rows[dtAcquirer.Rows.IndexOf(rowUpdate[0])]["TagLength"] = sTagValue.Length;
                                                        dtAcquirer.Rows[dtAcquirer.Rows.IndexOf(rowUpdate[0])]["TagValue"] = sTagValue;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                        break;
                                }
                                #endregion
                             //   if (bUploadRandomPwd) StartUpdatePwd(sTerminalID); CIMB DIMATIKAN
                                dtTerminal.AcceptChanges();
                                dtAcquirer.AcceptChanges();

                                StartUploadAddBulk(sSerialNumber);
                                //SaveFullContentClass.SaveFullContent(oSqlConn, sTerminalID);
                                UserAccessDelete(sTerminalID);

                                #region "TEDIE !!!"
                                // //Split Data DE by TID
                                //if (sTerminalID != null)
                                //{
                                //    sLogDENew = sSplitDataDEEachTID(sLogMessageDE, sTerminalID);
                                //    sLogDEOld = sSplitDataDEEachTID(sDeleteLogDE, sTerminalID);
                                //    int iLastIndex = 0;
                                //    sLogDEComb = null;
                                //    if (sLogDENew.IndexOf("TerminalID : ") != -1)
                                //    {
                                //        int iVar = 0;
                                //        int iIndex = 0;

                                //        string sOldData, sNewData;
                                //        while ((iVar = sLogDENew.IndexOf(":", iVar)) != -1)
                                //        {
                                //            if (iIndex == 0)
                                //            {
                                //                sItemDE = sLogDENew.Substring(0, sLogDENew.IndexOf(":", iIndex) + 1);
                                //                sOldData = sLogDEOld.Substring(sLogDEOld.IndexOf(sItemDE), sLogDEOld.IndexOf(",", iIndex) - sLogDEOld.IndexOf(sItemDE));
                                //                sNewData = sLogDENew.Substring(sLogDENew.IndexOf(sItemDE), sLogDENew.IndexOf(",", iIndex) - sLogDENew.IndexOf(sItemDE));
                                //                sLogDEComb = sLogDEComb + " " + sItemDE + " " + sOldData.Substring(sOldData.IndexOf(":")+1) + " -> " + sNewData.Substring(sNewData.IndexOf(":")+1)+ ",";
                                //                iIndex = iIndex + 1;
                                //            }
                                //            else
                                //            {

                                //                sItemDE = sLogDENew.Substring((sLogDENew.IndexOf(",", iVar - 1) + 1), sLogDENew.IndexOf(",", iVar) + (sLogDENew.IndexOf(",", iVar - 1)));
                                //                sItemDE = sItemDE.Substring(0, sItemDE.IndexOf(":")+1);
                                //                sOldData = sLogDEOld.Substring(sLogDEOld.IndexOf(sItemDE), sLogDEOld.IndexOf(",", iIndex) + sLogDEOld.IndexOf(sItemDE)-3);
                                //                sNewData = sLogDENew.Substring(sLogDENew.IndexOf(sItemDE), sLogDENew.IndexOf(",", iIndex) + sLogDENew.IndexOf(sItemDE)-3);
                                //                sLogDEComb = sLogDEComb + " " + sItemDE + " " + sOldData.Substring(sOldData.IndexOf(":") + 1) + " -> " + sNewData.Substring(sNewData.IndexOf(":") + 1) + ",";
                                //                iIndex = iIndex + 1;
                                //            }
                                //        }
                                //    }

                                //}
                                // //END Split Data DE by TID

                                // //Split Data AA by TID
                                //if (sTerminalID != null)
                                //{
                                //    sLogAANew = sSplitDataAAEachTID(sLogMessageAA, sTerminalID);
                                //    sLogAAOld = sSplitDataAAEachTID(sDeleteLogAA,sTerminalID);  
                                //}
                                // //END Split Data AA by TID
                                #endregion

                                if (uploadMode == UploadMode.Add)
                                {
                                    WriteUploadLog(string.Format("Upload {1} Profile {0}, DONE.", sTerminalID, uploadMode.ToString().ToUpper()));
                                    InputLog(oSqlConn, sTerminalID, sUserID, sDBName, "Upload -> "+uploadMode.ToString().ToUpper() + " Terminal " + sTerminalID + " Success", uploadMode.ToString().ToUpper() + " " + sLogDENew + ", " + sLogAANew);
                                }
                                else if (uploadMode == UploadMode.Update)
                                {
                                    WriteUploadLog(string.Format("Upload {1} Profile {0}, DONE.", sTerminalID, uploadMode.ToString().ToUpper()));
                                    InputLog(oSqlConn, sTerminalID, sUserID, sDBName, "Upload -> " + uploadMode.ToString().ToUpper() + " Terminal " + sTerminalID + " Success", uploadMode.ToString().ToUpper() + " OldData=" + sLogDEOld + ", " + sLogAAOld + " NewData=" + sLogDENew + ", " + sLogAANew);
                                }

                            }
                            else
                                WriteUploadLog(
                                    string.Format("Upload ADD Profile {0}, FAILED. Profile is been accessed by {1}", sTerminalID, sUserIdOnAccess));
                        }
                        else
                            WriteUploadLog(
                                string.Format("Upload ADD Profile {0}, FAILED. Profile allready exist", sTerminalID));
                     
                    }
                }
            }
            catch (Exception ex)
            {
               
                WriteUploadLog(ex.StackTrace);
              
            }
        }

        protected void StartUploadAddBCA()
        {
            if (oSqlConn.State != ConnectionState.Open)
                oSqlConn.Open();
            DataColumnCollection colm = dtExcel.Columns;
            try
            {
                foreach (DataRow rowExcel in dtExcel.Rows)
                {
                    FillTableUpload(rowExcel);

                    foreach (DataRow rowUploadDE in dtUploadDE.Rows)
                    {
                        string sTerminalID = rowUploadDE[0].ToString();
                        string sMaster = rowUploadDE["Master"].ToString();
                        int iDbID = iDbId(sMaster);
                        CopyMasterProfile(rowUploadDE["Master"].ToString(), sTerminalID);

                        if (dtTerminal.Rows[0]["TagId"].ToString().Length == 5) iVersion = 2;
                        else iVersion = 1;
                        if (uploadMode == UploadMode.Update) doDeleteTerminalID(sTerminalID);
                        if (!IsTerminalExist(sTerminalID))
                        {
                            string sUserIdOnAccess = null;
                            if ((IsAllowUserAccess(oSqlConn, sTerminalID, ref sUserIdOnAccess) && uploadMode == UploadMode.Add)
                                || uploadMode == UploadMode.Update)
                            {
                                if (uploadMode == UploadMode.Add) UserAccessInsert(sTerminalID);
                                #region "Start Process Terminal Table"
                               
                                foreach (DataColumn colUploadDE in dtUploadDE.Columns)
                                {
                                    if (colUploadDE.ColumnName != "TerminalID" && colUploadDE.ColumnName != "Master")
                                    {
                                        var varTag = from refDE in dtReferenceDE.AsEnumerable()
                                                     where refDE.Field<string>("ColumnName") == colUploadDE.ColumnName
                                                     select refDE.Field<string>("Tag");
                                        string sTag = (varTag.ToArray())[0];
                                        if (iVersion == 2) sTag = sTag.Insert(2, "0");

                                        string sTagValue = rowUploadDE[colUploadDE].ToString();

                                        if (sTag.Contains("DE"))
                                        {
                                            DataRow[] rowUpdate = dtTerminal.Select(
                                           string.Format("TerminalID='{0}' AND TagId='{1}'",
                                           sTerminalID, sTag)
                                           );
                                            switch (sTag)
                                            {
                                                case "DE02":
                                                case "DE002":
                                                case "DE03":
                                                case "DE003":
                                                case "DE04":
                                                case "DE004":
                                                case "DE08":
                                                case "DE008":
                                                    sTagValue = sTagValue.TrimEnd();
                                                    sTagValue = sTagValue.Length > 23 ? sTagValue.Substring(0, 23) : sTagValue;
                                                    

                                                    break;
                                            }
                                            try
                                            {
                                                dtTerminal.Rows[dtTerminal.Rows.IndexOf(rowUpdate[0])]["TagLength"] = sTagValue.Length;
                                            }
                                            catch (Exception ex)
                                            {
                                                WriteUploadLog(sTag);
                                            }
                                            dtTerminal.Rows[dtTerminal.Rows.IndexOf(rowUpdate[0])]["TagValue"] = sTagValue;

                                        }
                                        else
                                        {
                                            try
                                            {
                                                DataRow[] rowUpdate = dtCustomMenu.Select(string.Format("TerminalID='{0}' AND TagId='{1}'", sTerminalID, sTag));

                                                dtCustomMenu.Rows[dtCustomMenu.Rows.IndexOf(rowUpdate[0])]["TagLength"] = sTagValue.Length;
                                                dtCustomMenu.Rows[dtCustomMenu.Rows.IndexOf(rowUpdate[0])]["TagValue"] = sTagValue;
                                            }
                                            catch (Exception ex)
                                            {
                                                // WriteUploadLog(ex.StackTrace);
                                            }
                                        }
                                    }
                                }
                                dtTerminal.Rows[dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE01','DE001') AND TerminalId='{0}'", sTerminalID)))[0])]["TagValue"] = sTerminalID;

                                #endregion

                                #region "Start Process Acquirer Table"
                                foreach (DataRow rowUploadAA in dtUploadAA.Select(string.Format("TerminalID='{0}'", sTerminalID)))
                                {
                                    if (rowUploadAA["TerminalID"].ToString() == sTerminalID)
                                    {
                                        string sAcquirerName = rowUploadAA["AcquirerName"].ToString();
                                        string sAcquirerNameSearch = sAcquirerName;
                                        switch (sAcquirerNameSearch)
                                        {
                                            case "BCA":
                                                if (dtAcquirer.Select(string.Format("TagId IN ('AA01','AA001') AND TerminalId='{0}' AND TagValue='BCA1'", sTerminalID)).Count() > 0)
                                                    sAcquirerName = "BCA1";
                                                break;
                                            case "PROMO F":
                                                sAcquirerName = "PROMO SQ A";
                                                break;
                                            case "PROMO G":
                                                sAcquirerName = "PROMO SQ B";
                                                break;
                                            case "PROMO H":
                                                sAcquirerName = "PROMO SQ C";
                                                break;
                                            case "PROMO I":
                                                sAcquirerName = "PROMO SQ D";
                                                break;
                                            case "PROMO J":
                                                sAcquirerName = "PROMO SQ E";
                                                break;
                                        }
                                        foreach (DataColumn colUploadAA in dtUploadAA.Columns)
                                        {
                                            if (colUploadAA.ColumnName != "TerminalID" && colUploadAA.ColumnName != "AcquirerName")
                                            {
                                                if (!string.IsNullOrEmpty(rowUploadAA[colUploadAA].ToString()))
                                                {
                                                    var varTag = from refAA in dtReferenceAA.AsEnumerable()
                                                                 where refAA.Field<string>("AcquirerName") == sAcquirerNameSearch &&
                                                                 refAA.Field<string>("ColumnName") == colUploadAA.ColumnName
                                                                 select refAA.Field<string>("Tag");
                                                    string sTag = (varTag.ToArray())[0];
                                                    if (iVersion == 2) sTag = sTag.Insert(2, "0");

                                                    DataRow[] rowUpdate = dtAcquirer.Select(
                                                        string.Format("TerminalID='{0}' AND Name='{1}' AND TagId='{2}'",
                                                        sTerminalID, sAcquirerName, sTag));
                                                    if (rowUpdate.Count() > 0)
                                                    {
                                                        string sTagValue = rowUploadAA[colUploadAA].ToString();
                                                        dtAcquirer.Rows[dtAcquirer.Rows.IndexOf(rowUpdate[0])]["TagLength"] = sTagValue.Length;
                                                        dtAcquirer.Rows[dtAcquirer.Rows.IndexOf(rowUpdate[0])]["TagValue"] = sTagValue;
                                                    }
                                                }
                                            }
                                        }

                                       




                                    }
                                    else
                                        break;
                                }
                                #endregion

                                if (isSaveTerminal(dtTerminal, sTerminalID, iDbID))
                                {
                                    if (isSaveAcquirer(dtAcquirer, sTerminalID))
                                    {
                                        if (isSaveIssuer(sMaster, sTerminalID))
                                        {
                                            if (isSaveRelation(sMaster, sTerminalID))
                                            {
                                                if (uploadMode == UploadMode.Add)
                                                {
                                                    WriteUploadLog(string.Format("Upload {1} Profile {0}, DONE.", sTerminalID, uploadMode.ToString().ToUpper()));
                                                    InputLog(oSqlConn, sTerminalID, sUserID, "", uploadMode.ToString().ToUpper() + " Terminal" + sTerminalID + " Success", uploadMode.ToString().ToUpper() + " " + sLogDENew + ", " + sLogAANew);
                                                }
                                                else if (uploadMode == UploadMode.Update)
                                                {
                                                    WriteUploadLog(string.Format("Upload {1} Profile {0}, DONE.", sTerminalID, uploadMode.ToString().ToUpper()));
                                                    InputLog(oSqlConn, sTerminalID, sUserID, "", uploadMode.ToString().ToUpper() + " Terminal" + sTerminalID + " Success", uploadMode.ToString().ToUpper() + " OldData=" + sLogDEOld + ", " + sLogAAOld + " NewData=" + sLogDENew + ", " + sLogAANew);
                                                }
                                            }
                                        }
                                    }
                                }
                                UserAccessDelete(sTerminalID);
                            }
                            else
                                WriteUploadLog(
                                    string.Format("Upload ADD Profile {0}, FAILED. Profile is been accessed by {1}", sTerminalID, sUserIdOnAccess));
                        }
                        else
                            WriteUploadLog(
                                string.Format("Upload ADD Profile {0}, FAILED. Profile allready exist", sTerminalID));

                    }
                }
            }
            catch (Exception ex)
            {

                WriteUploadLog(ex.StackTrace);

            }
        }
        private Boolean isSaveTerminal(DataTable dtTerminalInsert, string sTerminalID, int _iDbID)
        {
            Boolean bSuccess = false;
            string sContent = "";
            foreach (DataRow dr in dtTerminal.Rows)
            {
                string sTag = dr["TagId"].ToString();
                string sTagValue = dr["TagValue"].ToString();
                sContent += sTag + sTagValue.Length.ToString("00") + sTagValue;
            }
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPTerminalInsert, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                sqlcmd.Parameters.Add("@sDbID", SqlDbType.Int).Value = _iDbID;
                sqlcmd.Parameters.Add("@bAllowDownload", SqlDbType.Bit).Value = 1;
                sqlcmd.Parameters.Add("@bStatus", SqlDbType.Bit).Value = 1;
                sqlcmd.Parameters.Add("@bEnableIP", SqlDbType.Bit).Value = 1;
                sqlcmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                sqlcmd.Parameters.Add("@bInitCompress", SqlDbType.Bit).Value = 1;
                sqlcmd.ExecuteNonQuery();
                bSuccess = true;
            }
                return bSuccess;
        }

        private Boolean isSaveAcquirer(DataTable dtAcquirerInsert, string sTerminalID)
        {
            Boolean bSuccess = false;
            string sContentAcquirer = "";
            foreach (DataRow dr in dtAcquirerInsert.Rows)
            {
                string sTag = dr["TagId"].ToString();
                string sTagValue = dr["TagValue"].ToString();
                sContentAcquirer += sTag + sTagValue.Length.ToString("00") + sTagValue;
            }

            sContentAcquirer = sContentAcquirer.Replace("AA001", "#AA001");
            string[] sSpliteContentAcquirer = Regex.Split(sContentAcquirer, "#");

            for (int i = 1; i < sSpliteContentAcquirer.Length; i++)
            {

                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPAcqInsert, oSqlConn))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                    sqlcmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sSpliteContentAcquirer[i].ToString();
                    sqlcmd.Parameters.Add("@bByUpload", SqlDbType.Bit).Value = 1;
                    sqlcmd.ExecuteNonQuery();
                    bSuccess = true;
                }
                
            }
            return bSuccess;
        }

        private Boolean isSaveIssuer(string sOldTID, string sNewTID)
        {
            Boolean bSuccess = false;

            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPIssuerCopy, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sOldTID", SqlDbType.VarChar).Value = sOldTID;
                sqlcmd.Parameters.Add("@sNewTID", SqlDbType.VarChar).Value = sNewTID;
                sqlcmd.ExecuteNonQuery();
                bSuccess = true;
            }
            return bSuccess;
        }

        private Boolean isSaveRelation(string sOldTID, string sNewTID)
        {
            Boolean bSuccess = false;

            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRelationCopy, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sOldTID", SqlDbType.VarChar).Value = sOldTID;
                sqlcmd.Parameters.Add("@sNewTID", SqlDbType.VarChar).Value = sNewTID;
                sqlcmd.ExecuteNonQuery();
                bSuccess = true;
            }
            return bSuccess;
        }





        public bool ContainColumn(string columnname, DataTable dt)
        {
            bool bSerialNumber;
            DataColumnCollection columns = dt.Columns;
            if (columns.Contains(columnname))
            {
                bSerialNumber = true;
            }
            else
            {
                bSerialNumber = true;
            }
            return bSerialNumber;
        }

        private string sSplitDataAAEachTID(string sLogMessageAA, string sTerminalID)
        {
            sLogAA = null;
            int iIndexOflastData = 0;
            if (sLogMessageAA.IndexOf(sTerminalID + "-") != -1)
            {
                int i = 0;
                while ((i = sLogMessageAA.IndexOf(",", i)) != -1)
                {
                    if ((sLogMessageAA.LastIndexOf(sTerminalID + "-")) < i)
                    {
                        if (iIndexOflastData == 0)
                        {
                            iIndexOflastData = i;
                        }
                    }

                    i++;
                }
                if (iIndexOflastData == 0)
                {
                    iIndexOflastData = sLogMessageAA.Length;
                }

            }
            sLogAA = sLogMessageAA.Substring(sLogMessageAA.IndexOf(sTerminalID + "-", 0), iIndexOflastData - (sLogMessageAA.IndexOf(sTerminalID + "-", 0)));
            sLogAA = sLogAA.Replace(sTerminalID + "-", "");

            return sLogAA;
        }

        private string sSplitDataDEEachTID(string sLogMessageDE, string sTerminalID)
        {
            sLogDE = null;
            int iIndexOflastData = 0;
            if (sLogMessageDE.IndexOf("TerminalID : " + sTerminalID) != -1)
            {
                int i = 0;
                while ((i = sLogMessageDE.IndexOf(", TerminalID : ", i)) != -1)
                {
                    if ((sLogMessageDE.IndexOf("TerminalID : " + sTerminalID)) < i)
                    {
                        if (iIndexOflastData == 0)
                        {
                            iIndexOflastData = i;
                        }
                    }

                    i++;
                }
                if (iIndexOflastData == 0)
                {
                    iIndexOflastData = sLogMessageDE.Length - 2;
                }

            }
            sLogDE = sLogMessageDE.Substring(sLogMessageDE.IndexOf("TerminalID : " + sTerminalID), iIndexOflastData - (sLogMessageDE.IndexOf("TerminalID : " + sTerminalID)));

            return sLogDE;
        }

        protected void StartUploadAddBulk(string sSerialNumber)
        {
            bool bSuccess = true;

            //if (bSerialNumber == true)
            //{
            //    StartUploadAddBulkSerialNumber(ref bSuccess, sSerialNumber);
            //}
            StartUploadAddBulkTerminalList(ref bSuccess);
            
            if(bSuccess) 
            {
                StartUploadAddBulkTerminal(ref bSuccess);
                if (bSuccess)
                {
                    StartUploadAddBulkAcquirer(ref bSuccess);
                    if (bSuccess)
                    {
                        StartUploadAddBulkIssuer(ref bSuccess);
                        if (bSuccess) 
                        { 
                            StartUploadAddBulkRelation(ref bSuccess);
                            //if (bSuccess && bCustomMenu)
                            //{
                            //    StartUploadAddBulkCustomMenu(ref bSuccess);
                            //}
                        }
                        //else break;
                    }
                    //else break;
                }
                //else break;
            }
            //else break;
            ResetDataTableBulk();
        }
        protected void StartUploadAddBulkSerialNumber(ref bool bSuccess, string sSerialNumber)
        {
            try
            {
                string sQuery = string.Format("INSERT INTO TbProfileSerialNumberList (DataBaseID, TerminalID, SerialNumber) VALUES ('{0}','{1}','{2}')", dtTerminalList.Rows[0][1].ToString(), dtTerminalList.Rows[0][0].ToString(), sSerialNumber);
                SqlCommand cmd = new SqlCommand(sQuery, oSqlConn);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
 
            }


        }
        protected void StartUploadAddBulkTerminalList(ref bool bSuccess)
        {
            SqlTransaction oTrans = oSqlConn.BeginTransaction();
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.KeepIdentity, oTrans);
            oSqlBulk.BatchSize = 5000;
            oSqlBulk.BulkCopyTimeout = 0;
            oSqlBulk.DestinationTableName = "tbProfileTerminalList";
            SqlBulkCopyColumnMapping mapTerminalListId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapTerminalListDatabaseID = new SqlBulkCopyColumnMapping("DatabaseID", "DatabaseID");
            SqlBulkCopyColumnMapping mapTerminalListAllowDownload = new SqlBulkCopyColumnMapping("AllowDownload", "AllowDownload");
            SqlBulkCopyColumnMapping mapTerminalListStatusMaster = new SqlBulkCopyColumnMapping("StatusMaster", "StatusMaster");
            SqlBulkCopyColumnMapping mapTerminalListLastView = new SqlBulkCopyColumnMapping("LastView", "LastView");
            SqlBulkCopyColumnMapping mapTerminalListEnableIP = new SqlBulkCopyColumnMapping("EnableIP", "EnableIP");
            SqlBulkCopyColumnMapping mapTerminalListIPAddress = new SqlBulkCopyColumnMapping("IPAddress", "IPAddress");
            SqlBulkCopyColumnMapping mapTerminalListAutoInitTimeStamp = new SqlBulkCopyColumnMapping("AutoInitTimeStamp", "AutoInitTimeStamp");
            SqlBulkCopyColumnMapping mapTerminalListLocationID = new SqlBulkCopyColumnMapping("LocationID", "LocationID");
            SqlBulkCopyColumnMapping mapTerminalListInitCompress = new SqlBulkCopyColumnMapping("InitCompress", "InitCompress");
            SqlBulkCopyColumnMapping mapTerminalListAppPackID = new SqlBulkCopyColumnMapping("AppPackID", "AppPackID");
            SqlBulkCopyColumnMapping mapTerminalListRemoteDownload = new SqlBulkCopyColumnMapping("RemoteDownload", "RemoteDownload");
            oSqlBulk.ColumnMappings.Add(mapTerminalListId);
            oSqlBulk.ColumnMappings.Add(mapTerminalListDatabaseID);
            oSqlBulk.ColumnMappings.Add(mapTerminalListAllowDownload);
            oSqlBulk.ColumnMappings.Add(mapTerminalListStatusMaster);
            oSqlBulk.ColumnMappings.Add(mapTerminalListLastView);
            oSqlBulk.ColumnMappings.Add(mapTerminalListEnableIP);
            oSqlBulk.ColumnMappings.Add(mapTerminalListIPAddress);
            oSqlBulk.ColumnMappings.Add(mapTerminalListAutoInitTimeStamp);
            oSqlBulk.ColumnMappings.Add(mapTerminalListInitCompress);
            if (dtTerminalList.Columns.Contains("AppPackID"))
                oSqlBulk.ColumnMappings.Add(mapTerminalListAppPackID);
            if (dtTerminalList.Columns.Contains("RemoteDownload"))
                oSqlBulk.ColumnMappings.Add(mapTerminalListRemoteDownload);

            //foreach (DataColumn col in dtTerminalList.Columns)
            //{
            //    SqlBulkCopyColumnMapping mapColumn = new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName);
            //    oSqlBulk.ColumnMappings.Add(mapColumn);
            //}
            try
            {
                oSqlBulk.WriteToServer(dtTerminalList);
                oTrans.Commit();
            }
            catch (Exception ex)
            {
                oTrans.Rollback();
                sLogMessage = ex.Message;
                WriteUploadLog(sLogMessage);
                bSuccess = false;
            }
        }

        protected void StartUploadAddBulkTerminal(ref bool bSuccess)
        {
            SqlTransaction oTrans = oSqlConn.BeginTransaction();
            //SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            using (SqlBulkCopy oSqlBulk = new SqlBulkCopy(
                       oSqlConn, SqlBulkCopyOptions.KeepIdentity, oTrans))
            {
                oSqlBulk.BulkCopyTimeout = 0;
                oSqlBulk.DestinationTableName = "tbProfileTerminal";
                SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
                SqlBulkCopyColumnMapping mapTerminalTag = new SqlBulkCopyColumnMapping("TagId", "TerminalTag");
                SqlBulkCopyColumnMapping mapTerminalTagValue = new SqlBulkCopyColumnMapping("TagValue", "TerminalTagValue");
                SqlBulkCopyColumnMapping mapTerminalTagLength = new SqlBulkCopyColumnMapping("TagLength", "TerminalTagLength");
                SqlBulkCopyColumnMapping mapTerminalLengthOfTagLength = new SqlBulkCopyColumnMapping("LengthOfTagLength", "TerminalLengthOfTagLength");
                oSqlBulk.ColumnMappings.Add(mapTerminalId);
                oSqlBulk.ColumnMappings.Add(mapTerminalTag);
                oSqlBulk.ColumnMappings.Add(mapTerminalTagValue);
                oSqlBulk.ColumnMappings.Add(mapTerminalTagLength);
                oSqlBulk.ColumnMappings.Add(mapTerminalLengthOfTagLength);
                try
                {
                    oSqlBulk.WriteToServer(dtTerminal);
                    oTrans.Commit();
                }
                catch (Exception ex)
                {
                    oTrans.Rollback();
                    sLogMessage = ex.Message;
                    WriteUploadLog(sLogMessage);
                    bSuccess = false;
                }
            }
        }

        protected void StartUploadAddBulkAcquirer(ref bool bSuccess)
        {
            SqlTransaction oTrans = oSqlConn.BeginTransaction();
            //SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            using (SqlBulkCopy oSqlBulk = new SqlBulkCopy(
                       oSqlConn, SqlBulkCopyOptions.KeepIdentity, oTrans))
            {
                oSqlBulk.BatchSize = 5000;
                oSqlBulk.BulkCopyTimeout = 0;
                oSqlBulk.DestinationTableName = "tbProfileAcquirer";
                SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
                SqlBulkCopyColumnMapping mapAcquirerTag = new SqlBulkCopyColumnMapping("TagId", "AcquirerTag");
                SqlBulkCopyColumnMapping mapAcquirerName = new SqlBulkCopyColumnMapping("Name", "AcquirerName");
                SqlBulkCopyColumnMapping mapAcquirerTagValue = new SqlBulkCopyColumnMapping("TagValue", "AcquirerTagValue");
                SqlBulkCopyColumnMapping mapAcquirerTagLength = new SqlBulkCopyColumnMapping("TagLength", "AcquirerTagLength");
                SqlBulkCopyColumnMapping mapAcquirerLengthOfTagLength = new SqlBulkCopyColumnMapping("LengthOfTagLength", "AcquirerLengthOfTagLength");
                oSqlBulk.ColumnMappings.Add(mapTerminalId);
                oSqlBulk.ColumnMappings.Add(mapAcquirerName);
                oSqlBulk.ColumnMappings.Add(mapAcquirerTag);
                oSqlBulk.ColumnMappings.Add(mapAcquirerTagValue);
                oSqlBulk.ColumnMappings.Add(mapAcquirerTagLength);
                oSqlBulk.ColumnMappings.Add(mapAcquirerLengthOfTagLength);
                try
                {
                    oSqlBulk.WriteToServer(dtAcquirer);
                    oTrans.Commit();
                }
                catch (Exception ex)
                {
                    oTrans.Rollback();
                    sLogMessage = ex.Message;
                    WriteUploadLog(sLogMessage);
                    bSuccess = false;
                }
            }
        }

        protected void StartUploadAddBulkIssuer(ref bool bSuccess)
        {
            SqlTransaction oTrans = oSqlConn.BeginTransaction();
            //SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            using (SqlBulkCopy oSqlBulk = new SqlBulkCopy(
                       oSqlConn, SqlBulkCopyOptions.KeepIdentity, oTrans))
            {
                oSqlBulk.BatchSize = 5000;
                oSqlBulk.BulkCopyTimeout = 0;
                oSqlBulk.DestinationTableName = "tbProfileIssuer";
                SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
                SqlBulkCopyColumnMapping mapIssuerTag = new SqlBulkCopyColumnMapping("TagId", "IssuerTag");
                SqlBulkCopyColumnMapping mapIssuerName = new SqlBulkCopyColumnMapping("Name", "IssuerName");
                SqlBulkCopyColumnMapping mapIssuerTagValue = new SqlBulkCopyColumnMapping("TagValue", "IssuerTagValue");
                SqlBulkCopyColumnMapping mapIssuerTagLength = new SqlBulkCopyColumnMapping("TagLength", "IssuerTagLength");
                SqlBulkCopyColumnMapping mapIssuerLengthOfTagLength = new SqlBulkCopyColumnMapping("LengthOfTagLength", "IssuerLengthOfTagLength");
                oSqlBulk.ColumnMappings.Add(mapTerminalId);
                oSqlBulk.ColumnMappings.Add(mapIssuerName);
                oSqlBulk.ColumnMappings.Add(mapIssuerTag);
                oSqlBulk.ColumnMappings.Add(mapIssuerTagValue);
                oSqlBulk.ColumnMappings.Add(mapIssuerTagLength);
                oSqlBulk.ColumnMappings.Add(mapIssuerLengthOfTagLength);
                try
                {
                    oSqlBulk.WriteToServer(dtIssuer);
                    oTrans.Commit();
                }
                catch (Exception ex)
                {
                    oTrans.Rollback();
                    sLogMessage = ex.Message;
                    WriteUploadLog(sLogMessage);
                    bSuccess = false;
                }
            }
        }

        protected void StartUploadAddBulkRelation(ref bool bSuccess)
        {
            SqlTransaction oTrans = oSqlConn.BeginTransaction();
            //SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            using (SqlBulkCopy oSqlBulk = new SqlBulkCopy(
                       oSqlConn, SqlBulkCopyOptions.KeepIdentity, oTrans))
            {
                oSqlBulk.BatchSize = 5000;
                oSqlBulk.BulkCopyTimeout = 0;
                oSqlBulk.DestinationTableName = "tbProfileRelation";
                SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
                SqlBulkCopyColumnMapping mapRelationTag = new SqlBulkCopyColumnMapping("TagId", "RelationTag");
                SqlBulkCopyColumnMapping mapRelationTagValue = new SqlBulkCopyColumnMapping("TagValue", "RelationTagValue");
                SqlBulkCopyColumnMapping mapRelationTagLength = new SqlBulkCopyColumnMapping("TagLength", "RelationTagLength");
                SqlBulkCopyColumnMapping mapRelationLengthOfTagLength = new SqlBulkCopyColumnMapping("LengthOfTagLength", "RelationLengthOfTagLength");
                oSqlBulk.ColumnMappings.Add(mapTerminalId);
                oSqlBulk.ColumnMappings.Add(mapRelationTag);
                oSqlBulk.ColumnMappings.Add(mapRelationTagValue);
                oSqlBulk.ColumnMappings.Add(mapRelationTagLength);
                oSqlBulk.ColumnMappings.Add(mapRelationLengthOfTagLength);
                try
                {
                    oSqlBulk.WriteToServer(dtRelation);
                    oTrans.Commit();
                }
                catch (Exception ex)
                {
                    oTrans.Rollback();
                    sLogMessage = ex.Message;
                    WriteUploadLog(sLogMessage);
                    bSuccess = false;
                }
            }
        }

        protected void StartUploadAddBulkCustomMenu(ref bool bSuccess)
        {
            SqlTransaction oTrans = oSqlConn.BeginTransaction();
            //SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            using (SqlBulkCopy oSqlBulk = new SqlBulkCopy(
                       oSqlConn, SqlBulkCopyOptions.KeepIdentity, oTrans))
            {
                oSqlBulk.BulkCopyTimeout = 0;
                oSqlBulk.DestinationTableName = "tbProfileCustomMenu";
                SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
                SqlBulkCopyColumnMapping mapCustomMenuTag = new SqlBulkCopyColumnMapping("TagId", "CustomMenuTag");
                SqlBulkCopyColumnMapping mapCustomMenuTagValue = new SqlBulkCopyColumnMapping("TagValue", "CustomMenuTagValue");
                SqlBulkCopyColumnMapping mapCustomMenuTagLength = new SqlBulkCopyColumnMapping("TagLength", "CustomMenuTagLength");
                SqlBulkCopyColumnMapping mapCustomMenuLengthOfTagLength = new SqlBulkCopyColumnMapping("LengthOfTagLength", "CustomMenuLengthOfTagLength");
                oSqlBulk.ColumnMappings.Add(mapTerminalId);
                oSqlBulk.ColumnMappings.Add(mapCustomMenuTag);
                oSqlBulk.ColumnMappings.Add(mapCustomMenuTagValue);
                oSqlBulk.ColumnMappings.Add(mapCustomMenuTagLength);
                oSqlBulk.ColumnMappings.Add(mapCustomMenuLengthOfTagLength);
                try
                {
                    oSqlBulk.WriteToServer(dtCustomMenu);
                    oTrans.Commit();
                    bCustomMenu = false;
                }
                catch (Exception ex)
                {
                    oTrans.Rollback();
                    sLogMessage = ex.Message;
                    WriteUploadLog(sLogMessage);
                    bSuccess = false;
                }
            }
        }

        protected void StartUpdatePwd(string sTerminalId)
        {
            int x=0 ;
            bool y = int.TryParse((sTerminalId.Substring(2)), out  x);
            if (x >= 0)
            //if (int.Parse(sTerminalId.Substring(2)) >= 0)
            {
                int iLastDigit = iChekDigit(sTerminalId.Substring(5));

                string sPwdInit = string.Format("{0}{1}", sTerminalId.Substring(5), iLastDigit);
                string sPwdFunc2 = CommonLib.sReverseString(sPwdInit);
                string _tmp = (iLastDigit + 2).ToString();
                string tmp = _tmp.Substring(_tmp.Length - 1, 1);
                string sPwdAdmin99 = string.Format("99{0}{1}",
                    sTerminalId.Substring(5),
                    (int.Parse(sPwdInit) + 2).ToString().PadLeft(4, '0').Substring(3, 1));
                if (iTypeRandomPassword == 0) ///CIMB DIMATIKAN
                {
                    dtTerminal.Rows[
                        dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE24','DE024') AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                        = sPwdAdmin99;
                    dtTerminal.Rows[
                        dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE28','DE028') AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                        = sPwdAdmin99;
                    dtTerminal.Rows[
                        dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE25','DE025') AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                        = sPwdFunc2;
                    dtTerminal.Rows[
                        dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("TagId IN ('DE42','DE042') AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                        = sPwdInit;
                }
                else
                {
                    dtTerminal.Rows[
                        dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("ItemName = 'Function 99 Password' AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                        = sPwdAdmin99;
                    dtTerminal.Rows[
                        dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("ItemName = 'Admin Password' AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                        = sPwdAdmin99;
                    dtTerminal.Rows[
                        dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("ItemName = 'Function 2 Password' AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                        = sPwdFunc2;
                    dtTerminal.Rows[
                        dtTerminal.Rows.IndexOf((dtTerminal.Select(string.Format("ItemName = 'Initialise Password' AND TerminalId='{0}'", sTerminalId)))[0])]["TagValue"]
                        = sPwdInit;
                }
            }
        }
        
        protected int iChekDigit(string sOrigVal)
        {
            int i_digit = 0;
            int calc_digit = 0;

            if (sOrigVal.Length % 2 == 1)
            {
                sOrigVal = sOrigVal + "0";
            }

            calc_digit = 0;
            int j = 1;
            int msg_len = sOrigVal.Length - 1;
            //int i_digit = 0;
            for (int i = msg_len; i >= 0; i += -1)
            {
                string digit = sOrigVal.Substring(i, 1);
                foreach (char c in digit)
                {
                    i_digit = (int)c;
                }
                // int i_digit = (int)digit;
                if (i_digit >= (int)'0' & i_digit <= (int)'9')
                {
                    i_digit = i_digit - 48;
                }
                else
                {
                    if (i_digit >= (int)'A' & i_digit <= (int)'Z')
                    {
                        i_digit = (i_digit - 58) % 10;
                    }
                    else
                    {
                        if (i_digit >= (int)'a' & i_digit <= (int)'z')
                        {
                            i_digit = (i_digit - 58 - 32) % 10;
                        }
                        else
                        {
                            i_digit = (i_digit - 32) % 10;
                        }
                    }
                }
                if (j % 2 == 1)
                {
                    i_digit = i_digit * 2;
                }
                if (i_digit >= 10)
                {
                    i_digit = ((i_digit - (i_digit % 10)) / 10) + (i_digit % 10);
                }
                calc_digit = calc_digit + i_digit;
                j = j + 1;
            }

            int temp_mod = calc_digit % 10;
            int temp_div = (calc_digit - (calc_digit % 10)) / 10;
            if (temp_mod != 0)
            {
                temp_div = temp_div + 1;
            }
            return (temp_div * 10) - calc_digit;
        }

        protected void ResetDataTableBulk()
        {
            //dtTerminalList = new DataTable();
            //dtTerminal = new DataTable();
            //dtAcquirer = new DataTable();
            //dtIssuer = new DataTable();
            //dtRelation = new DataTable();
            dtTerminalList.Clear();
            dtTerminal.Clear();
            dtAcquirer.Clear();
            dtIssuer.Clear();
            dtRelation.Clear();
            
        }

        #region "Misc"
        protected void WriteUploadLog(string sMessage)
        {
            sLogMessage = string.Format("{0}{1:yyyy-MM-dd hh:mm:ss.fff tt} : {2}\n", 
                sLogMessage, 
                DateTime.Now,
                sMessage);
        }

        protected void UserAccessInsert(string _sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand(sSPUserAccessInsert, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open)
                    oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = sUserID;
                oCmd.ExecuteNonQuery();
            }
        }

        protected void UserAccessDelete(string _sTerminalId)
        {
            if (!string.IsNullOrEmpty(sUserID))
                using (SqlCommand oCmd = new SqlCommand(sSPUserAccessDelete, oSqlConn))
                {
                    if (oSqlConn.State != ConnectionState.Open)
                        oSqlConn.Open();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = sUserID;
                    oCmd.ExecuteNonQuery();
                }
        }
        #endregion
        public static void InputLog(SqlConnection oSqlConn, string sTID, string sUsrID, string sDBName, string sActDesc, string sActDetl)
        {
            try
            {
                SqlParameter[] oSqlParam = new SqlParameter[5];
                oSqlParam[0] = new SqlParameter("@sTerminalID", System.Data.SqlDbType.VarChar, 8);
                oSqlParam[0].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[0].Value = sTID;
                oSqlParam[1] = new SqlParameter("@sUserId", System.Data.SqlDbType.VarChar, 10);
                oSqlParam[1].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[1].Value = sUsrID;
                oSqlParam[2] = new SqlParameter("@sDatabaseName", System.Data.SqlDbType.VarChar, 50);
                oSqlParam[2].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[2].Value = sDBName;
                oSqlParam[3] = new SqlParameter("@sActionDesc", System.Data.SqlDbType.VarChar, sActDesc.Length);
                oSqlParam[3].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[3].Value = sActDesc;
                oSqlParam[4] = new SqlParameter("@sActionDetail", System.Data.SqlDbType.VarChar, sActDetl.Length);
                oSqlParam[4].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[4].Value = (string.IsNullOrEmpty(sActDetl)) ? null : sActDetl;
                SqlCommand oSqlCmd = new SqlCommand(sSPAuditTrailInsert, oSqlConn);
                if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
                oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                oSqlCmd.Parameters.AddRange(oSqlParam);
                oSqlCmd.ExecuteNonQuery();

                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                string sError;
                sError = ex.Message;
            }
        }    
    }
}