using System;
using System.IO;
using Ini;

namespace InSysClass
{
    public class ServiceLicenseIP
    {
        protected string sAppName;
        protected string sLicenseDate;
        protected bool bTrialLicense;
        protected static string sFileName = Environment.SystemDirectory + "\\License.dll";
        protected int iCounter;

        public ServiceLicenseIP(string _sAppName)
        {
            sAppName = _sAppName;
        }

        public ServiceLicenseIP(string _sAppName, string _sLicenseDate, bool _bTrialLicense)
        {
            sAppName = _sAppName;
            sLicenseDate = _sLicenseDate;
            bTrialLicense = _bTrialLicense;
            if (!File.Exists(sFileName))
                InitialFile();
        }

        protected void InitialFile()
        {
            StreamWriter sw = new StreamWriter(sFileName, true);
            sw.WriteLine("[App License]");
            sw.WriteLine("AppCount=0");
            sw.WriteLine("AppName-0=");
            sw.Close();
        }

        public void NewServiceLicense()
        {
            iCounter = iGetAppCounter();
            iCounter++;
            UpdateLicense(iCounter);
            CreateSection();
            WriteServiceLicense();
        }

        protected void CreateSection()
        {
            StreamWriter sw = new StreamWriter(sFileName);
            sw.WriteLine("");
            sw.WriteLine("[" + sAppName + "]");
            sw.Close();
        }

        protected void WriteServiceLicense()
        {
            IniFile oObjServiceLicense = new IniFile(sFileName);
            oObjServiceLicense.IniWriteValue(sAppName, "License Date", sLicenseDate);
            if (bTrialLicense)
                oObjServiceLicense.IniWriteValue(sAppName, "Counter", "90");
            else
                oObjServiceLicense.IniWriteValue(sAppName, "Counter", "365");
        }

        protected static int iGetAppCounter()
        {
            IniFile oObjIni = new IniFile(sFileName);
            return int.Parse(oObjIni.IniReadValue("App License", "AppCount"));
        }

        protected void UpdateLicense(int _iCount)
        {
            IniFile oObjIni = new IniFile(sFileName);
            oObjIni.IniWriteValue("App License", "AppCount", _iCount.ToString());
            oObjIni.IniWriteValue("App License", "AppName-" + (_iCount - 1).ToString(), sAppName);
        }

        protected int iGetExpiryCounter()
        {
            IniFile oObjIni = new IniFile(sFileName);
            return int.Parse(oObjIni.IniReadValue(sAppName, "Counter"));
        }

        protected string sGetOldDate()
        {
            IniFile oObjIni = new IniFile(sFileName);
            return oObjIni.IniReadValue("App License", "Date");
        }

        public bool isLicenseExpired()
        {
            int iCounter = iGetExpiryCounter();
            DateTime dtOldDate = CommonLib.dtStringToDateTime(sGetOldDate());
            if (iCounter == 0)
            {
                int iDateDiff = CommonLib.iDateDiff(DateTime.Now, dtOldDate);
                if (iDateDiff > 0)
                {
                    iCounter -= iDateDiff;
                }
                else
                    iCounter -= 1;
                return false;
            }
            return true;
        }

        public static string[] sGetAppName()
        {
            string[] arrsAppName = new string[iGetAppCounter()];
            IniFile oObjIni = new IniFile(sFileName);
            for (int i = 0; i < iGetAppCounter(); i++)
            {
                arrsAppName[i] = oObjIni.IniReadValue("App License", "AppName-" + i.ToString());
            }
            return arrsAppName;
        }

        public static void UpdateOldDate(string sNewDate)
        {
            IniFile oObjIni = new IniFile(sFileName);
            oObjIni.IniWriteValue("App License", "Date", sNewDate);
        }
    }
}