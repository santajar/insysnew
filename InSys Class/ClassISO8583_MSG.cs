namespace InSysClass
{
    public class ISO8583_MSGLib
    {
        public string sTPDU;
        public string sDestNII;
        public string sOriginAddr;

        protected int MAXBIT = 64;
        public string MTI;
        public string sBinaryBitMap;

        public string sTerminalId;

        public string[] sBit;
        //public string sBit2;
        //public string sBit3;
        //public string sBit4;
        //public string sBit5;
        //public string sBit6;
        //public string sBit[7];
        //public string sBit8;
        //public string sBit9;
        //public string sBit10;
        //public string sBit11;
        //public string sBit12;
        //public string sBit13;
        //public string sBit[14];
        //public string sBit[15];
        //public string sBit[16];
        //public string sBit[17];
        //public string sBit[18];
        //public string sBit[19];
        //public string sBit[20];
        //public string sBit[21];
        //public string sBit[22];
        //public string sBit[23];
        //public string sBit[24];
        //public string sBit[25];
        //public string sBit[26];
        //public string sBit[27];
        //public string sBit[28];
        //public string sBit[29];
        //public string sBit[30];
        //public string sBit[31];
        //public string sBit[32];
        //public string sBit[33];
        //public string sBit[34];
        //public string sBit[35];
        //public string sBit[36];
        //public string sBit[37];
        //public string sBit[38];
        //public string sBit[39];
        //public string sBit[40];
        //public string sBit[41];
        //public string sBit[42];
        //public string sBit[43];
        //public string sBit[44];
        //public string sBit[45];
        //public string sBit[46];
        //public string sBit[47];
        //public string sBit[48];
        //public string sBit[49];
        //public string sBit[50];
        //public string sBit[51];
        //public string sBit[52];
        //public string sBit[53];
        //public string sBit[54];
        //public string sBit[54];
        //public string sBit[56];
        //public string sBit[57];
        //public string sBit[58];
        //public string sBit[59];
        //public string sBit[60];
        //public string sBit[61];
        //public string sBit[62];
        //public string sBit[63];
        //public string sBit[64];

        //public string sBit[65];
        //public string sBit[66];
        //public string sBit[67];
        //public string sBit[68];
        //public string sBit[69];
        //public string sBit[70];
        //public string sBit[71];
        //public string sBit[72];
        //public string sBit[73];
        //public string sBit[74];
        //public string sBit[74];
        //public string sBit[76];
        //public string sBit[77];
        //public string sBit[78];
        //public string sBit[79];
        //public string sBit[80];
        //public string sBit[81];
        //public string sBit[82];
        //public string sBit[83];
        //public string sBit[84];
        //public string sBit[85];
        //public string sBit[86];
        //public string sBit[87];
        //public string sBit[88];
        //public string sBit[89];
        //public string sBit[90];
        //public string sBit[91];
        //public string sBit[92];
        //public string sBit[93];
        //public string sBit[94];
        //public string sBit[95];
        //public string sBit[96];
        //public string sBit[97];
        //public string sBit[98];
        //public string sBit[99];
        //public string sBit[100];
        //public string sBit[101];
        //public string sBit[102];
        //public string sBit[103];
        //public string sBit[104];
        //public string sBit[105];
        //public string sBit[106];
        //public string sBit[107];
        //public string sBit[108];
        //public string sBit[109];
        //public string sBit[110];
        //public string sBit[111];
        //public string sBit[112];
        //public string sBit[113];
        //public string sBit[114];
        //public string sBit[115];
        //public string sBit[116];
        //public string sBit[117];
        //public string sBit[118];
        //public string sBit[119];
        //public string sBit[120];
        //public string sBit[121];
        //public string sBit[122];
        //public string sBit[123];
        //public string sBit[124];
        //public string sBit[126];
        //public string sBit[126];
        //public string sBit[127];
        //public string sBit[128];

        public ISO8583_MSGLib()
        {
            sBit = new string[MAXBIT];
            for (int i = 0; i < MAXBIT; i++)
                sBinaryBitMap += "0";
        }
    }
}