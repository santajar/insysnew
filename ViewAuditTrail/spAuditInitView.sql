-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Jan 8, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spAuditInitView]
	@sDate DATETIME
	 
AS
BEGIN
	SET NOCOUNT ON;
	IF OBJECT_ID('TEMPDB..#TempAuditTrail') IS NOT NULL DROP TABLE #TempAuditTrail
	select LogID, AccessTime, UserId,  ActionDescription INTO #TempAuditTrail
	from tbAuditTrail where ActionDescription like   ('%User%') AND AccessTime BETWEEN CAST(@sDate AS DATE)
	AND CAST(@sDate AS DATE) 
	DELETE FROM #TempAuditTrail where ActionDescription like ('%user login%') or ActionDescription like ('%user logout%') or ActionDescription LiKE '%view%'
    select * from #TempAuditTrail
END
