﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using InSysClass;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace InSysConsolePicture
{
    // State object for reading client data asynchronously
    public class StateObject
    {
        // Client  socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 1024;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }

    class AsyncSocket
    {
        #region "TCP/IP"
        // Thread signal.
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        public static int iPort;
        public static SqlConnection oConn;
        public static SqlConnection oConnAuditTrail;
        public static string sConnString;
        public static string sConnStringAuditTrail;
        public static bool bCheckAllowInit;
        public static int iConnMax;
        protected static int iConnSum = 0;
        protected static Socket listener;
        protected static bool bListening;
        public static bool bDebug = true;
        public static int iMaxBytePerPacket = 0;

        /// <summary>
        /// initialize Connection
        /// </summary>
        public static void StartListening()
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.
            // The DNS name of the computer
            // running the listener is "host.contoso.com".
            //IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            //IPAddress ipAddress = ipHostInfo.AddressList[0];
            //IPEndPoint localEndPoint = new IPEndPoint(ipAddress, iPort);

            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, iPort);

            //IPEndPoint localEndPoint = new IPEndPoint((IPAddress)((IPHostEntry)(Dns.Resolve("192.168.1.218"))).AddressList[0], iPort);

            // Create a TCP/IP socket.
            listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                // maximum queue
                listener.Listen(iConnMax);

                bListening = true;

                while (bListening)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    //Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);
                    listener.NoDelay = false;
                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }
            }
            catch (Exception e)
            {
                //Logs.Write("[TCP] StartListen Error : " + e.ToString());
                WriteLogtoDatabase("Error", "[TCP] StartListen Error : " + e.ToString());
            }
        }

        /// <summary>
        /// Accept New Connection
        /// </summary>
        /// <param name="ar"></param>
        public static void AcceptCallback(IAsyncResult ar)
        {
            //CommonConsole.WriteToConsole("Waiting for incoming Initialize...");
            // Signal the main thread to continue.
            allDone.Set();

            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            CommonConsole.WriteToConsole(string.Format("Accepted connection from {0}:{1}\nReady to receive initialize...",
                ((IPEndPoint)handler.RemoteEndPoint).Address, ((IPEndPoint)handler.RemoteEndPoint).Port));

            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = handler;

            listener.NoDelay = false;

            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }
               


        public static void ReadCallback(IAsyncResult ar)
        {
            try
            {
                //String content = String.Empty;

                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket handler = state.workSocket;

                // Read data from the client socket. 
                //int bytesRead = handler.EndReceive(ar);

                // below updated on June 19, 2012.
                SocketError errorCode;
                int bytesRead = handler.EndReceive(ar, out errorCode);
                if (errorCode != SocketError.Success)
                    bytesRead = 0;

                if (bytesRead > 0)
                {
                    if (iConnSum <= iConnMax)
                    {
                        iConnSum++;
                        if (bDebug)
                            //Logs.Write(string.Format("|Receive|iConnSum : {0}|bytesRead {1}", iConnSum, bytesRead));
                            //WriteLogtoDatabase("Log", string.Format("|Receive|iConnSum : {0}|bytesRead {1}", iConnSum, bytesRead));
                        Logs.Write(string.Format(string.Format(((IPEndPoint)handler.RemoteEndPoint).Address.ToString(), ((IPEndPoint)handler.RemoteEndPoint).Port + " |Receive|iConnSum : {0}|bytesRead {1}", iConnSum, bytesRead)));
                        CommonConsole.WriteToConsole(string.Format(string.Format(((IPEndPoint)handler.RemoteEndPoint).Address.ToString(), ((IPEndPoint)handler.RemoteEndPoint).Port + " |Receive|iConnSum : {0}|bytesRead {1}", iConnSum, bytesRead)));
                       //WriteLogtoDatabase("Log", string.Format(((IPEndPoint)handler.RemoteEndPoint).Address.ToString(), ((IPEndPoint)handler.RemoteEndPoint).Port + " |Receive|iConnSum : {0}|bytesRead {1}", iConnSum, bytesRead));

                       // There  might be more data, so store the data received so far.
                       state.sb.Append(Encoding.ASCII.GetString(
                            state.buffer, 0, bytesRead));

                        // Check for end-of-file tag. If it is not there, read 
                        // more data.
                        //content = state.sb.ToString();

                        // Not all data received. Get more.
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                            new AsyncCallback(ReadCallback), state);

                        #region "Debug Receive Message"
                        // Start the Iso message processing
                        byte[] arrbDebug = new byte[1024];
                        Array.Copy(state.buffer, arrbDebug, 1024);
                        //if (bDebug) Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|",
                        //    iConnSum,
                        //    CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", "")));
                        WriteLogtoDatabase("Log", string.Format("|iConnSum : {0}|Recv : {1}|", iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", "")));
                        #endregion

                        byte[] arrbReceive = new byte[StateObject.BufferSize];
                        Array.Copy(state.buffer, arrbReceive, bytesRead);

                        //----------------------------------------------
                        Queue<Response> queueResponse = new Queue<Response>();

                        string sArrbReceive = CommonLib.sByteArrayToHexString(arrbReceive).Replace(" ", "").ToString();


                        int iStartLength = 0;

                        string sParameter = sArrbReceive.Substring(iStartLength, 4).ToString();

                        //PacketProtocol packet = new PacketProtocol(arrbReceive);
                        //arrbPacket = new byte[arrbReceive.Length];

                        List<byte[]> listPacket = new List<byte[]>();
                        int iTotalPacket = 0;
                        
                        // Process Packet
                        int iIndex = 1;

                        int iLengthOfRecvPacket = sArrbReceive.Length;

                        while ((iIndex = sArrbReceive.IndexOf(sParameter, iIndex)) > -1)
                        {
                            int iLengthOfPacket = CommonConsole.iISOLenReceive(sArrbReceive.Substring(iIndex - 4, 4));

                            string sTempPacket = sArrbReceive.Substring(iIndex - 4, 4 + iLengthOfPacket * 2);
                            if (!string.IsNullOrEmpty(sTempPacket))
                            {
                                listPacket.Add(CommonLib.HexStringToByteArray(sTempPacket));
                                iTotalPacket++;

                                iIndex += iLengthOfPacket * 2;
                                if (iIndex == iLengthOfRecvPacket || iIndex > iLengthOfRecvPacket) break;
                            }
                            else
                                break;
                        };

                        // end process packet



                        foreach (byte[] arrbTemp in listPacket)
                        {
                            int iLengthMessage = int.Parse(CommonLib.sConvertHextoDec(sParameter));
                            ////Console.WriteLine("StartLength" + iStartLength.ToString());
                            ////Console.WriteLine("length Message = " + iLengthMessage.ToString());
                            string sTemp = sArrbReceive.Substring(iStartLength, (iLengthMessage * 2) + 4);
                            iStartLength = iStartLength + (iLengthMessage * 2) + 4;

                            byte[] arrbSubReceive = new byte[StateObject.BufferSize];
                            arrbSubReceive = CommonLib.HexStringToByteArray(sTemp);

                            Response oNewResponse = new Response(arrbSubReceive, sConnString, sConnStringAuditTrail, bCheckAllowInit, ConnType.TcpIP, bDebug);
                            //Response oNewResponse = new Response(arrbTemp, sConnString, sConnStringAuditTrail, bCheckAllowInit, ConnType.TcpIP, bDebug);
                            queueResponse.Enqueue(oNewResponse);
                        }

                        //Response oNewResponse = new Response(arrbReceive, sConnString, sConnStringAuditTrail, bCheckAllowInit, ConnType.TcpIP, bDebug);
                        //collection.Enqueue(oNewResponse);

                        //    string sTemp = sArrbReceive.Substring(iStartLength, (iLengthMessage * 2) + 4);
                        //    iStartLength = iStartLength + (iLengthMessage * 2) + 4;

                        //    byte[] arrbSubReceive = new byte[StateObject.BufferSize];
                        //    arrbSubReceive = CommonLib.HexStringToByteArray(sTemp);


                        //    //Queue<Response> collection = new Queue<Response>();
                        //    Response oNewResponse = new Response(arrbSubReceive, sConnString, sConnStringAuditTrail, bCheckAllowInit, ConnType.TcpIP, bDebug);
                        //    //collection.Enqueue(oNewResponse);

                        //    //while (collection.Count > 0)
                        //    //{
                        //        byte[] arrbSend = new byte[StateObject.BufferSize];
                        //    //Response oTempResponse = collection.Dequeue();
                        //    // arrbSend = oTempResponse.arrbResponse();
                        //    arrbSend = oNewResponse.arrbResponse();


                        while (queueResponse.Count > 0)
                        {
                            byte[] arrbSend = new byte[StateObject.BufferSize];
                            Response oTempResponse = queueResponse.Dequeue();
                            arrbSend = oTempResponse.arrbResponse();

                            if (arrbSend != null && arrbSend[2] != 0)
                            {
                                #region "Debug Send Message"
                                //if (bDebug) Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2}", iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""),CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", ""))); //debug

                                if (bDebug)
                                    Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2}", iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""), CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", ""))); //debug
                                //Program.insyslog.Info(string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2}", iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""), CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", "")));

                                //WriteLogtoDatabase("Trace", string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2}",iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""), CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", "")));
                                #endregion

                                Thread.Sleep(50);

                                // Send the response
                                Send(handler, arrbSend);
                            }
                            else
                            {
                                //if (bDebug)
                                //    Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : NULL", iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""))); //debug
                                if (bDebug)
                                    //Program.insyslog.Info(string.Format("|iConnSum : {0}|Recv : {1}|Sent : NULL", iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""))); //debug
                                    Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : NULL", iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""))); //debug
                            }


                            //WriteLogtoDatabase("Trace", string.Format("|iConnSum : {0}|Recv : {1}|Sent : NULL",iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", "")));
                            iConnSum--;
                            if (bDebug) Logs.Write("|Send|iConnSum : " + iConnSum.ToString());
                            //if (bDebug) Program.insyslog.Info("|Send|iConnSum : " + iConnSum.ToString());

                            //if (oTempResponse.IsProcessInit)
                            //    SendLogInit(oTempResponse.TerminalId, oTempResponse.AppName, oTempResponse.SerialNum,
                            //        oTempResponse.IsStartInit, oTempResponse.IsCompleteInit, oTempResponse.Percentage, oTempResponse.ProcessCode);
                            //oTempResponse.Dispose();
                        }

                        // ======================================================  
                        //byte[] arrbReceive = new byte[bytesRead];
                        //arrbReceive = state.buffer;

                        //PacketProtocol oPacket = new PacketProtocol(iMaxBytePerPacket);
                        //byte[] arrbReceiveReal = new byte[iMaxBytePerPacket];
                        //oPacket.MessageArrived(arrbReceive);
                        //oPacket.DataReceived(arrbReceiveReal);

                        // each time receive create instance response                        
                        //byte[] arrbSend = (new Response2(oConn, bCheckAllowInit, ConnType.TcpIP)).arrbResponse(arrbReceive);

                        #region "OLD"
                        /*
                        byte[] arrbSend = new byte[StateObject.BufferSize];
                        Response3 oNewResponse = new Response3(sConnString, bCheckAllowInit, ConnType.TcpIP);
                        arrbSend = oNewResponse.arrbResponse(arrbReceive);

                        if (arrbSend != null && arrbSend[2] != 0)
                        {
                            #region "Debug Send Message"
                            if (bDebug) Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2} ",
                                iConnSum,
                                CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""),
                                CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", ""))); //debug
                            #endregion

                            Thread.Sleep(50);

                            // Send the response
                            Send(handler, arrbSend);
                            //Console.WriteLine(string.Format("|Sent : SUCCESS"));
                        }
                        else
                            if (bDebug) Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : NULL",
                                iConnSum,
                                CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""))); //debug
                        iConnSum--;
                        if (bDebug) Logs.Write("|Send|iConnSum : " + iConnSum.ToString());

                        if (oNewResponse.IsProcessInit)
                            SendLogInit(oNewResponse.TerminalId, oNewResponse.AppName, oNewResponse.SerialNum,
                                oNewResponse.IsStartInit, oNewResponse.IsCompleteInit, oNewResponse.Percentage, oNewResponse.ProcessCode);
                        oNewResponse.Dispose();
                        */
                        #endregion
//                        string sArrbReceive =
//"00BA600669300008002020010000810090910000000963066955415453414D53540145000000000000000000004243414932323131344B414E2020204F5356455253494F4E2035322E30304B45524E454C454D562030342E36363730333338363939202020202020202020202020202020202020202020202020202020202020202020202020202020202020373939353339322F31363737374B2020202020202020202020202020202020202020202020202020000300000000010000BA60066930000800202001000081009091000000096306695541544F4C533039540145000000000000000000004243414932323131344B414E2020204F5356455253494F4E2035322E30304B45524E454C454D562030342E36363730333338363939202020202020202020202020202020202020202020202020202020202020202020202020202020202020373939353339322F31363737374B2020202020202020202020202020202020202020202020202020000300000000010000BA60066930000800202001000081009091000000096306695541545354455354540145000000000000000000004243414932323131344B414E2020204F5356455253494F4E2035322E30304B45524E454C454D562030342E36363730333338363939202020202020202020202020202020202020202020202020202020202020202020202020202020202020373939353339322F31363737374B2020202020202020202020202020202020202020202020202020000300000000010000BA60066930000800202001000081009091000000096306695541545445533032540145000000000000000000004243414932323131344B414E2020204F5356455253494F4E2035322E30304B45524E454C454D562030342E36363730333338363939202020202020202020202020202020202020202020202020202020202020202020202020202020202020373939353339322F31363737374B202020202020202020202020202020202020202020202020202000030000000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
                        //CommonLib.sByteArrayToHexString(arrbReceive).Replace(" ", "").ToString();
                        //string sResult = Regex.Replace(sArrbReceive, @"\t|\n|\r", string.Empty);
                        //string sResult = Regex.Replace(sArrbReceive, @"\t|\n|\r| ", string.Empty);
                        //string sResult = sArrbReceive.Trim();
                        //string sResult = Regex.Replace(sArrbReceive, @"\r|\n", string.Empty);
                        //Console.WriteLine("Result");
                        //string sResult = sArrbReceive.Replace(Environment.NewLine, "");
                        //Console.WriteLine(sResult);
                        //Console.WriteLine("BEFORE");
                        //Console.WriteLine(sArrbReceive);
                        //sArrbReceive.Replace("/n", "");
                        //Console.WriteLine("AFTER");
                        //Console.WriteLine(sArrbReceive);
                        //int iStartLength = 0;
                        //Console.WriteLine("Jumlah sArrbreceive = " + sArrbReceive.Length.ToString());
                        //while (iStartLength < sArrbReceive.Length)
                        //{
                        //    string sParameter = sArrbReceive.Substring(iStartLength, 4).ToString();
                        //    if (sParameter == "0000")
                        //        break;
                        //    int iLengthMessage = int.Parse(CommonLib.sConvertHextoDec(sParameter));
                        //    Console.WriteLine("StartLength" + iStartLength.ToString());
                        //    Console.WriteLine("length Message = " + iLengthMessage.ToString());
                        //    string sTemp = sArrbReceive.Substring(iStartLength, (iLengthMessage * 2) + 4);
                        //    iStartLength = iStartLength + (iLengthMessage * 2) + 4;

                        //    byte[] arrbSubReceive = new byte[StateObject.BufferSize];
                        //    arrbSubReceive = CommonLib.HexStringToByteArray(sTemp);


                        //    //Queue<Response> collection = new Queue<Response>();
                        //    Response oNewResponse = new Response(arrbSubReceive, sConnString, sConnStringAuditTrail, bCheckAllowInit, ConnType.TcpIP, bDebug);
                        //    //collection.Enqueue(oNewResponse);

                        //    //while (collection.Count > 0)
                        //    //{
                        //        byte[] arrbSend = new byte[StateObject.BufferSize];
                        //    //Response oTempResponse = collection.Dequeue();
                        //    // arrbSend = oTempResponse.arrbResponse();
                        //    arrbSend = oNewResponse.arrbResponse();

                        //    if (arrbSend != null && arrbSend[2] != 0)
                        //        {
                        //            #region "Debug Send Message"
                        //            if (bDebug)
                        //                //Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2}",
                        //                //iConnSum,
                        //                //CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""),
                        //                //CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", ""))); //debug
                        //                WriteLogtoDatabase("Trace", string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2}", iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""), CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", "")));
                        //            #endregion

                        //            //Thread.Sleep(50);

                        //            // Send the response
                        //            Send(handler, arrbSend);
                        //        }
                        //        else
                        //            if (bDebug)
                        //            //Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : NULL",
                        //            //iConnSum,
                        //            //CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""))); //debug
                        //            WriteLogtoDatabase("Trace", string.Format("|iConnSum : {0}|Recv : {1}|Sent : NULL", iConnSum, CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", "")));
                        //        //iConnSum--;
                        //        //if (bDebug) Logs.Write("|Send|iConnSum : " + iConnSum.ToString());

                        //        ////if (oTempResponse.IsProcessInit)
                        //        ////    SendLogInit(oTempResponse.TerminalId, oTempResponse.AppName, oTempResponse.SerialNum,
                        //        ////        oTempResponse.IsStartInit, oTempResponse.IsCompleteInit, oTempResponse.Percentage, oTempResponse.ProcessCode);
                        //        ////oTempResponse.Dispose();
                        //    //}
                        //}
                        //iConnSum--;
                        ////if (bDebug) Logs.Write("|Send|iConnSum : " + iConnSum.ToString());
                        //WriteLogtoDatabase("Logs", string.Format("|Send|iConnSum : {0}", iConnSum.ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                //Logs.Write("[TCP] ReadCallBack Error : " + ex.ToString());
                WriteLogtoDatabase("Error", string.Format("ConSum {0}|[TCP] ReadCallBack Error :  {1}", iConnSum, ex.ToString()));
                //Console.WriteLine(ex.ToString());
                //handler.Close(30);
                iConnSum--;
            }
        }

        private static void WriteLogtoDatabase(string _sType, string _sDetailLog)
        {
            try
            {
                if (oConnAuditTrail.State != ConnectionState.Open) oConnAuditTrail.Open();
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPConsoleLogInsert, oConnAuditTrail))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sLogType", SqlDbType.VarChar).Value = _sType;
                    oCmd.Parameters.Add("@sDetailLog", SqlDbType.VarChar).Value = _sDetailLog;
                    oCmd.ExecuteNonQuery();
                    oConnAuditTrail.Close();
                }
            }
            catch (Exception ex)
            {
                //Trace.Write("|ERROR|SendLogInitAudit|" + ex.Message);
                WriteLogtoDatabase("Error", string.Format("ConSum {0}|ERROR|SendLogInitAudit| {1}", iConnSum, ex.ToString()));
            }
        }

        /// <summary>
        /// Convert message to Byte data and send data
        /// </summary>
        /// <param name="data">string : Send Message</param>
        private static void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        /// <summary>
        /// Send Data
        /// </summary>
        /// <param name="byteData">Byte : Send Byte</param>
        private static void Send(Socket handler, byte[] byteData)
        {
            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        /// <summary>
        /// Send Message
        /// </summary>
        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                //Console.WriteLine("Sent {0} bytes to client.", bytesSent);
                handler.ReceiveBufferSize = 0;
                handler.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer, 0);
            }
            catch (Exception)
            {
                //Logs.doWriteErrorFile("[TCP] SendCallback Error : " + e.ToString());
                //Console.WriteLine(e.ToString());
            }
        }

        /// <summary>
        /// Send Log Init to database
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <param name="sAppName">string : Application Name</param>
        /// <param name="sSerialNum">string : Serial Number</param>
        /// <param name="isStartInit">int: Start Init</param>
        /// <param name="isComplete">bool: true if Complete</param>
        /// <param name="iPercentage">int : Percentage</param>
        /// <param name="sProcCode">string : Proccode</param>
        private static void SendLogInit(string sTerminalId, string sAppName, string sSerialNum,
            bool isStartInit, bool isComplete, int iPercentage, string sProcCode)
        {
            string sStatus = null;
            if (isStartInit)
            {
                switch ((ProcCode)Enum.Parse(typeof(ProcCode), sProcCode))
                {
                    case ProcCode.ProfileStartEnd: sStatus = "Initialize Start"; break;
                    case ProcCode.AutoProfileStartEnd: sStatus = "Auto Initialize Start"; break;
                    case ProcCode.AutoPrompt: sStatus = "Auto Initialize Prompt"; break;
                    case ProcCode.Flazz: sStatus = "Flazz Initialize"; break;
                    case ProcCode.AutoCompletion: sStatus = "Auto Initialize Completion"; break;
                    case ProcCode.TerminalEcho: sStatus = "Terminal Echo"; break;
                }
                SendLogInitAudit(sTerminalId, sAppName, sSerialNum, sStatus);
                SendLogInitConn(sTerminalId, 1, iPercentage);
            }
            else
                SendLogInitConn(sTerminalId, 0, iPercentage);

            if (isComplete)
            {
                switch ((ProcCode)Enum.Parse(typeof(ProcCode), sProcCode))
                {
                    case ProcCode.ProfileCont: sStatus = "Initialize Complete"; break;
                    case ProcCode.AutoProfileCont: sStatus = "Auto Initialize Complete"; break;
                    case ProcCode.AutoPrompt: sStatus = "Auto Initialize Prompt"; break;
                    case ProcCode.Flazz: sStatus = "Flazz Initialize"; break;
                    case ProcCode.AutoCompletion: sStatus = "Auto Initialize Completion"; break;
                    case ProcCode.TerminalEcho: sStatus = "Terminal Echo"; break;
                    case ProcCode.DownloadProfileCont: sStatus = "Download Software Complete"; break;
                }
                SendLogInitAudit(sTerminalId, sAppName, sSerialNum, sStatus);
            }
        }

        /// <summary>
        /// Send Log Init to Database
        /// </summary>
        /// <param name="sTerminalId">string : TerminalID</param>
        /// <param name="sAppName">String : Application Name</param>
        /// <param name="sSerialNum">string : Serial Number</param>
        /// <param name="sStatus">String : Status</param>
        private static void SendLogInitAudit(string sTerminalId, string sAppName, string sSerialNum, string sStatus)
        {
            try
            {
                //using (SqlConnection oConnLocal = new SqlConnection(sConnString))
                //{
                //    oConnLocal.Open();
                //    using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAuditInitInsert, oConnLocal))
                //    {
                //        oCmd.CommandType = CommandType.StoredProcedure;
                //        oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                //        oCmd.Parameters.Add("@sSoftware", SqlDbType.VarChar).Value = string.IsNullOrEmpty(sAppName) ? "" : sAppName;
                //        oCmd.Parameters.Add("@sSerialNum", SqlDbType.VarChar).Value = string.IsNullOrEmpty(sSerialNum) ? "" : sSerialNum;
                //        oCmd.Parameters.Add("@sStatus", SqlDbType.VarChar).Value = sStatus;
                //        oCmd.ExecuteNonQuery();
                //    }
                //    oConnLocal.Close();
                //}

                if (oConn.State != ConnectionState.Open) oConn.Open();
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAuditInitInsert, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 60;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                    oCmd.Parameters.Add("@sSoftware", SqlDbType.VarChar).Value = string.IsNullOrEmpty(sAppName) ? "" : sAppName;
                    oCmd.Parameters.Add("@sSerialNum", SqlDbType.VarChar).Value = string.IsNullOrEmpty(sSerialNum) ? "" : sSerialNum;
                    oCmd.Parameters.Add("@sStatus", SqlDbType.VarChar).Value = sStatus;
                    oCmd.ExecuteNonQuery();
                    oConn.Close();
                }
            }
            catch (Exception ex)
            {
                //Trace.Write("|ERROR|SendLogInitAudit|" + ex.Message);
                WriteLogtoDatabase("Error", string.Format("|ERROR|SendLogInitAudit| {0}", ex.ToString()));
            }
        }

        /// <summary>
        /// Send Log init percentage
        /// </summary>
        /// <param name="sTerminalId">String : Terminal ID</param>
        /// <param name="iStartInit">int: Start Init</param>
        /// <param name="iPercentage">int : Percentage</param>
        private static void SendLogInitConn(string sTerminalId, int iStartInit, int iPercentage)
        {
            try
            {
                //using (SqlConnection oConnLocal = new SqlConnection(sConnString))
                //{
                //    oConnLocal.Open();
                //    using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitLogConnNewInitConsole, oConnLocal))
                //    {
                //        oCmd.CommandType = CommandType.StoredProcedure;
                //        oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                //        oCmd.Parameters.Add("@iStartInit", SqlDbType.Int).Value = iStartInit;
                //        oCmd.Parameters.Add("@iPercentage", SqlDbType.Int).Value = iPercentage;
                //        oCmd.ExecuteNonQuery();
                //    }
                //}

                if (oConn.State != ConnectionState.Open) oConn.Open();
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitLogConnNewInitConsole, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 60;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                    oCmd.Parameters.Add("@iStartInit", SqlDbType.Int).Value = iStartInit;
                    oCmd.Parameters.Add("@iPercentage", SqlDbType.Int).Value = iPercentage;
                    oCmd.ExecuteNonQuery();
                    oConn.Close();
                }
            }
            catch (Exception ex)
            {
                //Trace.Write(string.Format("|ERROR|SendLogInitConn| {0} | TerminalID : {1} | Start Init : {2} | Percentage : {3}", ex.Message, sTerminalId, iStartInit, iPercentage));
                WriteLogtoDatabase("Error", string.Format("|ERROR|SendLogInitConn| {0} | TerminalID : {1} | Start Init : {2} | Percentage : {3}", ex.Message, sTerminalId, iStartInit, iPercentage));
            }
        }

        /// <summary>
        /// Stop Listening
        /// </summary>
        public static void Stop()
        {
            bListening = false;
            listener.Close();
        }
        #endregion
    }
}
