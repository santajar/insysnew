﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc4InSys.Models;

namespace Mvc4InSys.Controllers
{
    [Authorize]
    public class AuditInitSoftwareController : Controller
    {
        private NewInSysEntities db = new NewInSysEntities();

        //
        // GET: /AuditInitSoftware/

        //public ActionResult Index()
        //{
        //    //return View(db.tbAuditInitSoftware.ToList());
        //    return View();
        //}

        public ActionResult Index(string sSearchTID, string sSearchSN, string sSearchSoftware)
        {
            if (!String.IsNullOrEmpty(sSearchTID) ||
                !String.IsNullOrEmpty(sSearchSN) ||
                !String.IsNullOrEmpty(sSearchSoftware))
            {
                var listTbInitSoftware = from tempInitSoftware in db.tbAuditInitSoftware
                                         select tempInitSoftware;

                if (!String.IsNullOrEmpty(sSearchTID))
                    listTbInitSoftware = listTbInitSoftware
                         .Where(s => s.TerminalId.Contains(sSearchTID))
                         .OrderByDescending(o => o.InitTime);

                if (!String.IsNullOrEmpty(sSearchSN))
                    listTbInitSoftware = listTbInitSoftware
                        .Where(s => s.SerialNumber.Contains(sSearchSN))
                        .OrderByDescending(o => o.InitTime);

                if (!String.IsNullOrEmpty(sSearchSoftware))
                    listTbInitSoftware = listTbInitSoftware
                        .Where(s => s.Software.Contains(sSearchSoftware))
                        .OrderByDescending(o => o.InitTime);

                //if (listTbInitSoftware != null)
                return View(listTbInitSoftware);
            }
            //else
            //{
            //    var listTbInitSoftware = from tempInitSoftware in db.tbAuditInitSoftware
            //                             select tempInitSoftware;
            //    listTbInitSoftware = listTbInitSoftware
            //        .OrderByDescending(o => o.InitTime);
            //    return View(listTbInitSoftware);
            //}
            return View(db.tbAuditInitSoftware.ToList());
        }

        //
        // GET: /AuditInitSoftware/Details/5

        public ActionResult Details(long id = 0)
        {
            tbAuditInitSoftware tbauditinitsoftware = db.tbAuditInitSoftware.Find(id);
            if (tbauditinitsoftware == null)
            {
                return HttpNotFound();
            }
            return View(tbauditinitsoftware);
        }

        //
        // GET: /AuditInitSoftware/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /AuditInitSoftware/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbAuditInitSoftware tbauditinitsoftware)
        {
            if (ModelState.IsValid)
            {
                db.tbAuditInitSoftware.Add(tbauditinitsoftware);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbauditinitsoftware);
        }

        //
        // GET: /AuditInitSoftware/Edit/5

        public ActionResult Edit(long id = 0)
        {
            tbAuditInitSoftware tbauditinitsoftware = db.tbAuditInitSoftware.Find(id);
            if (tbauditinitsoftware == null)
            {
                return HttpNotFound();
            }
            return View(tbauditinitsoftware);
        }

        //
        // POST: /AuditInitSoftware/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbAuditInitSoftware tbauditinitsoftware)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbauditinitsoftware).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbauditinitsoftware);
        }

        //
        // GET: /AuditInitSoftware/Delete/5

        public ActionResult Delete(long id = 0)
        {
            tbAuditInitSoftware tbauditinitsoftware = db.tbAuditInitSoftware.Find(id);
            if (tbauditinitsoftware == null)
            {
                return HttpNotFound();
            }
            return View(tbauditinitsoftware);
        }

        //
        // POST: /AuditInitSoftware/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            tbAuditInitSoftware tbauditinitsoftware = db.tbAuditInitSoftware.Find(id);
            db.tbAuditInitSoftware.Remove(tbauditinitsoftware);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}