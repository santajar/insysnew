﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc4InSys.Models;
using PagedList;

namespace Mvc4InSys.Controllers
{
    public class UserLogController : Controller
    {
        private NewInSysEntities db = new NewInSysEntities();

        //
        // GET: /UserLog/

        public ActionResult Index(string sSearchUserID, string currentFilter, string sListDatabaseName, int? Page)
        {
            ViewBag.sSearchUserID = sSearchUserID;

            ViewBag.sListDatabaseName = sListDatabaseName;

            var listTbAuditTrail = from tempAuditTrail in db.tbAuditTrail
                                   select tempAuditTrail;

            if (!String.IsNullOrEmpty(sSearchUserID))
            {
                listTbAuditTrail = listTbAuditTrail
                     .Where(s => s.UserId.Contains(sSearchUserID));
            }
            if (!String.IsNullOrEmpty(sListDatabaseName))
            {
                listTbAuditTrail = listTbAuditTrail
                     .Where(s => s.DatabaseName.Contains(sListDatabaseName));
            }


            listTbAuditTrail = listTbAuditTrail.OrderByDescending(o => o.LogID);

                int pageSize = 20;
                int pageNumber = (Page ?? 1);
                if (Request.IsAuthenticated)
                {
                        return View(listTbAuditTrail.ToPagedList(pageNumber, pageSize));
                }
                else
                { 
                    return RedirectToAction("Login","Login");
                }
        }

        //
        // GET: /UserLog/Details/5

        public ActionResult Details(long id = 0)
        {
            tbAuditTrail tbaudittrail = db.tbAuditTrail.Find(id);
            if (tbaudittrail == null)
            {
                return HttpNotFound();
            }
            return View(tbaudittrail);
        }

        //
        // GET: /UserLog/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /UserLog/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbAuditTrail tbaudittrail)
        {
            if (ModelState.IsValid)
            {
                db.tbAuditTrail.Add(tbaudittrail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbaudittrail);
        }

        //
        // GET: /UserLog/Edit/5

        public ActionResult Edit(long id = 0)
        {
            tbAuditTrail tbaudittrail = db.tbAuditTrail.Find(id);
            if (tbaudittrail == null)
            {
                return HttpNotFound();
            }
            return View(tbaudittrail);
        }

        //
        // POST: /UserLog/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbAuditTrail tbaudittrail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbaudittrail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbaudittrail);
        }

        //
        // GET: /UserLog/Delete/5

        public ActionResult Delete(long id = 0)
        {
            tbAuditTrail tbaudittrail = db.tbAuditTrail.Find(id);
            if (tbaudittrail == null)
            {
                return HttpNotFound();
            }
            return View(tbaudittrail);
        }

        //
        // POST: /UserLog/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            tbAuditTrail tbaudittrail = db.tbAuditTrail.Find(id);
            db.tbAuditTrail.Remove(tbaudittrail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}