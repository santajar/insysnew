﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc4InSys.Models;

namespace Mvc4InSys.Controllers
{
    public class Base4Controller : Controller
    {
        public Base4Controller()
        {
            ViewBag.Menu = BuildMenu();
        }

        private IList<ClassMenu> BuildMenu()
        {
            IList<ClassMenu> mmList = new List<ClassMenu>(){

                // Parent
                new ClassMenu(){ Id = 1, Name = "Home", ParentId = 0, SortOrder = 1},
                new ClassMenu(){ Id = 2, Name = "EDC Terminal", ParentId = 0, SortOrder = 1},
                new ClassMenu(){ Id = 3, Name = "Report", ParentId = 0, SortOrder = 1},
                new ClassMenu(){ Id = 4, Name = "About", ParentId = 0, SortOrder = 1},
                new ClassMenu(){ Id = 5, Name = "System", ParentId = 0, SortOrder = 1},

                // Children
                new ClassMenu() { Id=11, Name = "User", ParentId = 1, SortOrder=1 },
                new ClassMenu() { Id=12, Name = "Management", ParentId = 1, SortOrder=2 },

                new ClassMenu() { Id=21, Name = "Version", ParentId = 2, SortOrder=1},
                new ClassMenu() { Id=22, Name = "Setting", ParentId = 2, SortOrder=2},
                new ClassMenu() { Id=23, Name = "Upload", ParentId = 2, SortOrder=3},
                new ClassMenu() { Id=24, Name = "Software Package", ParentId = 2, SortOrder=4},

                new ClassMenu() { Id=31, Name = "Audit Trail", ParentId = 3, SortOrder=1},
                new ClassMenu() { Id=32, Name = "Init Trail", ParentId = 3, SortOrder=2},
                new ClassMenu() { Id=33, Name = "TLV", ParentId = 3, SortOrder=3},
                new ClassMenu() { Id=34, Name = "Customs", ParentId = 3, SortOrder=4},
                
                new ClassMenu() { Id=51, Name = "Version", ParentId = 5, SortOrder=1},
                new ClassMenu() { Id=52, Name = "Profile", ParentId = 5, SortOrder=2},
                new ClassMenu() { Id=53, Name = "Software", ParentId = 5, SortOrder=3},
                new ClassMenu() { Id=54, Name = "Communication", ParentId = 5, SortOrder=4},
                new ClassMenu() { Id=55, Name = "Audit Trail", ParentId = 5, SortOrder=5},
                
                new ClassMenu() { Id=121, Name = "User", ParentId = 12, SortOrder=1 },
                new ClassMenu() { Id=122, Name = "Groups", ParentId = 12, SortOrder=2 },
                new ClassMenu() { Id=123, Name = "User Groups", ParentId = 12, SortOrder=3 },

                new ClassMenu() { Id=231, Name = "Template Definition", ParentId = 23, SortOrder=1 },
                new ClassMenu() { Id=232, Name = "Upload Definition", ParentId = 23, SortOrder=2 },
                new ClassMenu() { Id=233, Name = "Upload", ParentId = 23, SortOrder=3 },
            };

            return mmList;
        }
    }
}
