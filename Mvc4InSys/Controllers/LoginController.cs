﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Mvc4InSys.Models;

namespace Mvc4InSys.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Models.tbUserLogin user)
        {
            if (ModelState.IsValid)
            {
                if (user.IsValid(user.UserID, user.Password))
                {
                    FormsAuthentication.SetAuthCookie(user.UserID, true);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "User ID or Password is incorrect!");
                }
            }
            return View(user);
        }
        
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Login");
        }
    }
}
