﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc4InSys.Models;
//using MvcPaging;
using PagedList;

namespace Mvc4InSys.Controllers
{
    [Authorize]
    public class AuditTrailController : Controller
    {
        private NewInSysEntities db = new NewInSysEntities();
        private int iDefaultPageSize = 15;

        //
        // GET: /AuditTrail/

        //public ActionResult Index()
        //{
        //    return View(db.tbAuditTrail.ToList());
        //}

        public ActionResult Index(int? iPage, string sSearchUserID, string sListDatabaseName, string sSearchAction)
        {
            int iCurrentPageIndex = iPage.HasValue ? iPage.Value - 1 : 1; 

            var listDatabaseName = from tempProfileTerminalDb in db.tbProfileTerminalDB
                                   select tempProfileTerminalDb.DatabaseName;
            ViewBag.sListDatabaseName = new SelectList(listDatabaseName);
            ViewBag.sSearchUserID = sSearchUserID;
            if (!String.IsNullOrEmpty(sSearchUserID) ||
                   !String.IsNullOrEmpty(sListDatabaseName) ||
                   !String.IsNullOrEmpty(sSearchAction))
            {
                var listTbAuditTrail = from tempAuditTrail in db.tbAuditTrail
                                       select tempAuditTrail;

                if (!String.IsNullOrEmpty(sSearchUserID))
                    listTbAuditTrail = listTbAuditTrail
                         .Where(s => s.UserId.Contains(sSearchUserID))
                         .OrderByDescending(o => o.AccessTime);

                if (!String.IsNullOrEmpty(sListDatabaseName))
                    listTbAuditTrail = listTbAuditTrail
                        .Where(s => s.DatabaseName == sListDatabaseName)
                        .OrderByDescending(o => o.AccessTime);

                if (!String.IsNullOrEmpty(sSearchAction))
                    listTbAuditTrail = listTbAuditTrail
                        .Where(s => s.ActionDescription.Contains(sSearchAction))
                        .OrderByDescending(o => o.AccessTime);

                //if (listTbInitSoftware != null)
                return View(listTbAuditTrail.ToPagedList(iCurrentPageIndex, iDefaultPageSize));
            }
            else
            {
                var listTbAuditTrail = (from tempAuditTrail in db.tbAuditTrail
                                        orderby tempAuditTrail.AccessTime descending
                                        select tempAuditTrail).Take(100);
                return View(listTbAuditTrail.ToPagedList(iCurrentPageIndex, iDefaultPageSize));
            }
        }

        //
        // GET: /AuditTrail/Details/5

        public ActionResult Details(long id = 0)
        {
            tbAuditTrail tbaudittrail = db.tbAuditTrail.Find(id);
            if (tbaudittrail == null)
            {
                return HttpNotFound();
            }
            return View(tbaudittrail);
        }

        //
        // GET: /AuditTrail/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /AuditTrail/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbAuditTrail tbaudittrail)
        {
            if (ModelState.IsValid)
            {
                db.tbAuditTrail.Add(tbaudittrail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbaudittrail);
        }

        //
        // GET: /AuditTrail/Edit/5

        public ActionResult Edit(long id = 0)
        {
            tbAuditTrail tbaudittrail = db.tbAuditTrail.Find(id);
            if (tbaudittrail == null)
            {
                return HttpNotFound();
            }
            return View(tbaudittrail);
        }

        //
        // POST: /AuditTrail/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbAuditTrail tbaudittrail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbaudittrail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbaudittrail);
        }

        //
        // GET: /AuditTrail/Delete/5

        public ActionResult Delete(long id = 0)
        {
            tbAuditTrail tbaudittrail = db.tbAuditTrail.Find(id);
            if (tbaudittrail == null)
            {
                return HttpNotFound();
            }
            return View(tbaudittrail);
        }

        //
        // POST: /AuditTrail/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            tbAuditTrail tbaudittrail = db.tbAuditTrail.Find(id);
            db.tbAuditTrail.Remove(tbaudittrail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}