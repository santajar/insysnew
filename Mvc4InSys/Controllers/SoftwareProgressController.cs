﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mvc4InSys.Models;
using PagedList;

namespace Mvc4InSys.Controllers
{
    public class SoftwareProgressController : Controller
    {
        private NewInSysEntities db = new NewInSysEntities();

        //
        // GET: /Default1/

        public ActionResult Index(string currentFilter, int? Page)
        {
            var listTempSoftwareProgress = from TempOsftwareProgress in db.tbSoftwareProgress
                                           select TempOsftwareProgress;

            listTempSoftwareProgress = listTempSoftwareProgress.OrderByDescending(o => o.ID);

            int pageSize = 20;
            int pageNumber = (Page ?? 1);
            if (Request.IsAuthenticated)
            {
                return View(listTempSoftwareProgress.ToPagedList(pageNumber, pageSize));
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }

        //
        // GET: /Default1/Details/5

        public ActionResult Details(int id = 0)
        {
            tbSoftwareProgress tbsoftwareprogress = db.tbSoftwareProgress.Find(id);
            if (tbsoftwareprogress == null)
            {
                return HttpNotFound();
            }
            return View(tbsoftwareprogress);
        }

        //
        // GET: /Default1/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Default1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbSoftwareProgress tbsoftwareprogress)
        {
            if (ModelState.IsValid)
            {
                db.tbSoftwareProgress.Add(tbsoftwareprogress);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbsoftwareprogress);
        }

        //
        // GET: /Default1/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tbSoftwareProgress tbsoftwareprogress = db.tbSoftwareProgress.Find(id);
            if (tbsoftwareprogress == null)
            {
                return HttpNotFound();
            }
            return View(tbsoftwareprogress);
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tbSoftwareProgress tbsoftwareprogress)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbsoftwareprogress).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbsoftwareprogress);
        }

        //
        // GET: /Default1/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tbSoftwareProgress tbsoftwareprogress = db.tbSoftwareProgress.Find(id);
            if (tbsoftwareprogress == null)
            {
                return HttpNotFound();
            }
            return View(tbsoftwareprogress);
        }

        //
        // POST: /Default1/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbSoftwareProgress tbsoftwareprogress = db.tbSoftwareProgress.Find(id);
            db.tbSoftwareProgress.Remove(tbsoftwareprogress);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}