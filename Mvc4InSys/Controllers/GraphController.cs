﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using Mvc4InSys.Models;
using System.Data.Entity;
using System.Data.SqlClient;

namespace Mvc4InSys.Controllers
{
    [Authorize]
    public class GraphController : Controller
    {
        public NewInSysEntities db = new NewInSysEntities();
        // GET: /Graph/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GroupChart()
        {
            return View();
        }
        public ActionResult CityChart()
        {
            return View();
        }

        public ActionResult RegionChart()
        {
            return View();
        }

        public ActionResult OnProgressChart()
        {
            return View();
        }


        public ActionResult DrawChartInitTrail()
        {
            //var bytes = new Chart(width: 400, height: 200, theme: ChartTheme.Yellow)
            //     .AddSeries(
            //        chartType: "bar",
            //        xValue: new[] { "Math", "English", "Computer", "Urdu" },
            //        yValues: new[] { "60", "70", "68", "88" })
            //    .GetBytes("png");
            
            var ListChart = (from temptListChart in db.tbSoftwareProgress
                             where temptListChart.InstallStatus == "Succes" || temptListChart.InstallStatus == "Success"
                             select new { temptListChart.TerminalID, temptListChart.Percentage }).Distinct().ToList();

            var myChart = new Chart(width: 600, height: 400)
                .AddTitle("Init trail Software")
                .AddSeries("Default",
                chartType: "bar",
                xValue: ListChart, xField: "TerminalID",
                yValues: ListChart, yFields: "Percentage")
            .GetBytes("png");
            return File(myChart, "image/png");
           
        }

        public ActionResult DrawChartSoftwareOnProgress()
        {
            var ListChart = (from temptListChart in db.tbSoftwareProgress
                             where temptListChart.Percentage == null || temptListChart.Percentage < 100
                             select new { temptListChart.TerminalID, temptListChart.Percentage }).Distinct().ToList();

            var myChart = new Chart(width: 600, height: 400)
                .AddTitle("Init trail Software On Progress")
                .AddSeries("Default",
                chartType: "bar",
                xValue: ListChart, xField: "TerminalID",
                yValues: ListChart, yFields: "Percentage")
            .GetBytes("png");
            return File(myChart, "image/png");
        }

        public ActionResult DrawChartGroup()
        {
            db.Database.Connection.Open();
            SqlConnection conn = new SqlConnection();
            var spCommand = db.Database.Connection.CreateCommand();
            spCommand.CommandText = "dbo.spChartGroup";
            spCommand.CommandType = System.Data.CommandType.StoredProcedure;
            var exec = (spCommand.ExecuteNonQuery());

            var ListChart = (from tempGroup in db.tbGroupChart
                             select new {  tempGroup.GroupName, tempGroup.GroupCount }).ToList();

            var myChart = new Chart(width: 600, height: 400)
                .AddTitle("EDC Group")
                .AddSeries("Default",
                chartType: "Bar",
                xValue: ListChart, xField: "GroupName",
                yValues: ListChart, yFields: "GroupCount")
            .GetBytes("png");
            return File(myChart, "image/png");

            db.Database.Connection.Close();
        }

        public ActionResult DrawChartCity()
        {
            db.Database.Connection.Open();
            SqlConnection conn = new SqlConnection();
            var spCommand = db.Database.Connection.CreateCommand();
            spCommand.CommandText = "dbo.spChartCity";
            spCommand.CommandType = System.Data.CommandType.StoredProcedure;
            var exec = (spCommand.ExecuteNonQuery());

            var ListChart = (from tempGroup in db.tbCityChart
                             select new { tempGroup.CityName, tempGroup.CityCount }).ToList();

            var myChart = new Chart(width: 600, height: 400)
                .AddTitle("EDC City")
                .AddSeries("Default",
                chartType: "Bar",
                xValue: ListChart, xField: "CityName",
                yValues: ListChart, yFields: "CityCount")
            .GetBytes("png");
            return File(myChart, "image/png");

            db.Database.Connection.Close();
        }


        public ActionResult DrawChartRegion()
        {
            db.Database.Connection.Open();
            SqlConnection conn = new SqlConnection();
            var spCommand = db.Database.Connection.CreateCommand();
            spCommand.CommandText = "dbo.spChartRegion";
            spCommand.CommandType = System.Data.CommandType.StoredProcedure;
            var exec = (spCommand.ExecuteNonQuery());

            var ListChart = (from tempRegion in db.tbRegionChart
                             select new { tempRegion.RegionName, tempRegion.RegionCount }).ToList();

            var myChart = new Chart(width: 600, height: 400)
                .AddTitle("EDC Region")
                .AddSeries("Default",
                chartType: "Bar",
                xValue: ListChart, xField: "RegionName",
                yValues: ListChart, yFields: "RegionCount")
            .GetBytes("png");
            return File(myChart, "image/png");
        }

    }
}
