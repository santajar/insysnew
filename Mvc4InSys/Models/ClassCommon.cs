﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mv4cInSys.Models
{
    public class ClassCommon
    {
        /// <summary>
        /// Used as a #define for privilege digit length. Example: XX03111 -> length of 03 = 2
        /// </summary>
        public enum PrivilegeDigit
        {
            Length = 2
        }

        /// <summary>
        /// Types of privilege.
        /// </summary>
        [Flags]
        public enum Privilege
        {
            Add = PrivilegeDigit.Length,
            Edit,
            Delete
        }

        /// <summary>
        /// The privilege code 
        /// </summary>
        [Flags]
        public enum PrivilegeCode
        {
            IA, //"Allow Init"
            IC, //"Compress Init"
            SN, //"SN Valid"
            AT, //"Audit Trail"
            IT, //"Initialize Trail"
            PT, //"Populate TLV"
            DU, //"Data Upload"
            UM, //"User Management"
            PE, //"EDC_IP Tracking"
            DL, //"Download"
            NP, //"CreateTerminal"
            RP, //"Register Old File"
            AI, //"AID Management"
            PK, //"Key Management"
            RC, //"Card Range"
            DE, //"Terminal"
            AA, //"Acquirer"
            AE, //"Issuer"
            AC, //"Card"
            LO, //"Location"
            SU, //"Super User"        
            OI, //"Auto Init"
            TL, //"TLE"
            SW, //Software Package
            LP, //Loyalty Product(Citibank)
            GP, //GPRS Ip Setting
            CR, //Currency DCC
        }
    }
}