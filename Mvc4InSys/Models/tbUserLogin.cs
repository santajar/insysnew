//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mvc4InSys.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Data;
    using InSysClass;
    using System.Text;
    using System.Security.Cryptography;

    public partial class tbUserLogin
    {
        [Required]
        [Display(Name = "User ID")]
        public string UserID { get; set; }
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public string UserRights { get; set; }
        public string GroupID { get; set; }


        /// <summary>
        /// Checks if user with given password exists in the database
        /// </summary>
        /// <param name="_userid">User name</param>
        /// <param name="_sPassword">User password</param>
        /// <returns>True if user exist and password is correct</returns>
        public bool IsValid(string _sUserId, string _sPassword)
        {
            using (NewInSysEntities db = new NewInSysEntities())
            {
                var colPassword = (from tempUserLogin in db.tbUserLogin
                                   where tempUserLogin.UserID == _sUserId
                                   select tempUserLogin.Password).Take(1);
                string sPwdMd5 = sGetMD5Result(_sPassword);
                if ((colPassword.ToArray()).Length == 1)
                {
                    if ((colPassword.ToArray())[0] == sPwdMd5)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        protected string sGetMD5Result(string _sToEncrypt)
        {
            byte[] bSource = ASCIIEncoding.ASCII.GetBytes(_sToEncrypt);

            MD5CryptoServiceProvider oMD5 = new MD5CryptoServiceProvider();

            byte[] bHash = oMD5.ComputeHash(bSource);
            return CommonLib.sByteArrayToString(bHash);
        }

        /// <summary>
        /// Returns true if privilege code is found in the user's privilege.
        /// </summary>
        /// <param name="ePrivilegeCode">Code of privilege</param>
        public static bool IsAllowed(string _sUserId, PrivilegeCode ePrivilegeCode)
        {
            NewInSysEntities db = new NewInSysEntities();
            var colPrivilege = (from tempUserLogin in db.tbUserLogin
                                where tempUserLogin.UserID == _sUserId
                                select tempUserLogin.UserRights).Take(1);
            string _sPrivilege = (colPrivilege.ToArray())[0];
            return string.IsNullOrEmpty(_sPrivilege) ? false : _sPrivilege.Contains(ePrivilegeCode.ToString());
        }

        /// <summary>
        /// Returns true if privilege code is found in the user's privilege.
        /// </summary>
        /// <param name="ePrivilegeCode">Code of privilege</param>
        /// <param name="ePrivilege">Type of privilege</param>
        public static bool IsAllowed(string _sUserId, PrivilegeCode ePrivilegeCode, Privilege ePrivilege)
        {
            NewInSysEntities db = new NewInSysEntities();
            var colPrivilege = (from tempUserLogin in db.tbUserLogin
                                where tempUserLogin.UserID == _sUserId
                                select tempUserLogin.UserRights).Take(1);
            string _sPrivilege = (colPrivilege.ToArray())[0];
            return
                IsAllowed(_sUserId, ePrivilegeCode) &&
                _sPrivilege[_sPrivilege.IndexOf(ePrivilegeCode.ToString()) + ePrivilegeCode.ToString().Length + (int)ePrivilege] == '1';
        }

        public static bool IsAllowed(string _sUserId, string _sParameter)
        {
            if (!string.IsNullOrEmpty(_sUserId) && !string.IsNullOrEmpty(_sParameter))
            {
                NewInSysEntities db = new NewInSysEntities();
                var colPrivilege = (from tempUserLogin in db.tbUserLogin
                                    where tempUserLogin.UserID == _sUserId
                                    select tempUserLogin.UserRights).Take(1);
                string _sPrivilege = (colPrivilege.ToArray())[0];
                if (_sPrivilege.Contains(_sParameter) == true)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
    }
}
