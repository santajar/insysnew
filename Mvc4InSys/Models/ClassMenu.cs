﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc4InSys.Models
{
    public class ClassMenu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public int SortOrder { get; set; }
    }
}