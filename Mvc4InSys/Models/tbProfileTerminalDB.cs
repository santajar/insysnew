//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mvc4InSys.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbProfileTerminalDB
    {
        public short DatabaseID { get; set; }
        public string DatabaseName { get; set; }
        public bool NewInit { get; set; }
        public Nullable<short> LengthOfTag { get; set; }
        public bool EMVInit { get; set; }
        public bool TLEInit { get; set; }
        public bool LoyaltyProd { get; set; }
        public bool GPRSInit { get; set; }
        public bool CurrencyInit { get; set; }
        public bool RemoteDownload { get; set; }
        public bool PinPad { get; set; }
        public bool BankCode { get; set; }
        public bool ProductCode { get; set; }
    }
}
