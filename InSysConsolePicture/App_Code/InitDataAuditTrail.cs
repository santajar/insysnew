﻿using System;
using System.IO;
using Ini;
using InSysClass;

namespace InSysConsolePicture
{
    class InitDataAuditTrail
    {
        protected string sDataSourceAuditTrail;
        protected string sDatabaseAuditTrail;
        protected string sUserIDAuditTrail;
        protected string sPasswordAuditTrail;
        protected string sDirectoryAuditTrail;

        public InitDataAuditTrail(string _sDirectory)
        {
            sDirectoryAuditTrail = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectoryAuditTrail + "\\Application.config";
            string sEncryptFileName = sDirectoryAuditTrail + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSourceAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "DataSource");
                    sDatabaseAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "Database");
                    sUserIDAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "UserID");
                    sPasswordAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "Password");
                }
                catch (Exception)
                {
                    //Logs.doWriteErrorFile(sDirectory, ex.Message);
                }
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSourceAuditTrail;
        }

        public string sGetDatabase()
        {
            return sDatabaseAuditTrail;
        }

        public string sGetUserID()
        {
            return sUserIDAuditTrail;
        }

        public string sGetPassword()
        {
            return sPasswordAuditTrail;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSourceAuditTrail, sDatabaseAuditTrail, sUserIDAuditTrail, sPasswordAuditTrail);
            //return SQLConnLib.sConnString(sDataSource, sDatabase, sUserID, sPassword);
        }

        public string sGetConnString(int iMaxPoolSize)
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSourceAuditTrail, sDatabaseAuditTrail, sUserIDAuditTrail, sPasswordAuditTrail, iMaxPoolSize);
        }

        //public void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword)
        //{
        //    string sFileName = sDirectory + "\\Application.config";
        //    string sEncryptFileName = sDirectory + "\\Application.conf";

        //    try
        //    {
        //        IniFile objIniFile = new IniFile(sFileName);
        //        objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
        //        objIniFile.IniWriteValue("Database", "Database", sDatabase);
        //        objIniFile.IniWriteValue("Database", "UserID", sUserID);
        //        objIniFile.IniWriteValue("Database", "Password", sPassword);
        //    }
        //    catch (Exception)
        //    {
        //        //Logs.doWriteErrorFile(string.Format("[Init Data] Error : {0}", ex.Message));
        //    };
        //    EncryptionLib.EncryptFile(sEncryptFileName, sFileName);
        //    File.Delete(sFileName);
        //}
    }
}
