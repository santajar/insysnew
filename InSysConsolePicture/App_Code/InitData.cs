﻿using System;
using System.IO;
using Ini;
using InSysClass;

namespace InSysConsolePicture
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;
        protected string sDirectory;

        public InitData(string _sDirectory)
        {
            sDirectory = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectory + "\\Application.config";
            string sEncryptFileName = sDirectory + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception)
                {
                    //Logs.doWriteErrorFile(sDirectory, ex.Message);
                }
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword);
            //return SQLConnLib.sConnString(sDataSource, sDatabase, sUserID, sPassword);
        }

        public string sGetConnString(int iMaxPoolSize)
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword, iMaxPoolSize);
        }

        //public void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword)
        //{
        //    string sFileName = sDirectory + "\\Application.config";
        //    string sEncryptFileName = sDirectory + "\\Application.conf";

        //    try
        //    {
        //        IniFile objIniFile = new IniFile(sFileName);
        //        objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
        //        objIniFile.IniWriteValue("Database", "Database", sDatabase);
        //        objIniFile.IniWriteValue("Database", "UserID", sUserID);
        //        objIniFile.IniWriteValue("Database", "Password", sPassword);
        //    }
        //    catch (Exception)
        //    {
        //        //Logs.doWriteErrorFile(string.Format("[Init Data] Error : {0}", ex.Message));
        //    };
        //    EncryptionLib.EncryptFile(sEncryptFileName, sFileName);
        //    File.Delete(sFileName);
        //}
    }
}
