﻿using System;
using System.Globalization;
using System.IO;
using InSysClass;

namespace InSysConsolePicture
{
    public enum ConnType
    {
        TcpIP = 1,
        Serial = 2,
        Modem = 3
    }

    public enum ProcCode
    {
        ProfileStartEnd = 930000,
        ProfileCont = 930001,
        AutoProfileStartEnd = 960000,
        AutoProfileCont = 960001,
        Flazz = 940000,
        AutoPrompt = 950000,
        AutoCompletion = 970000,
        TerminalEcho = 800000,
        DownloadProfileStartEnd = 980000,
        DownloadProfileCont = 980001,

        #region procode init logo/idle

        InitImagePrint1StartEnd =910000, //logo print 1
        InitImagePrint1Cont = 910001,   //logo print 1
        InitImagePrint2StartEnd = 910100, //logo merchant
        InitImagePrint2Cont = 910101, //logo merchant
        InitImagePrint3StartEnd = 910200, //logo receipt
        InitImagePrint3Cont = 910201, //logo receipt
        InitImagePrint4StartEnd = 910300,
        InitImagePrint4Cont = 910301, 
        InitImagePrint5StartEnd = 910400,
        InitImagePrint5Cont = 910401,
        InitImagePrint6StartEnd = 910500,
        InitImagePrint6Cont = 910501,
        InitImagePrint7StartEnd = 910600,
        InitImagePrint7Cont = 910601,
        InitImagePrint8StartEnd = 910700,
        InitImagePrint8Cont = 910701,
        InitImagePrint9StartEnd = 910800,
        InitImagePrint9Cont = 910801,
        InitImagePrint10StartEnd = 910900,
        InitImagePrint10Cont = 910901,
        
        //procode idle

        InitImageIdle1StartEnd = 920000, //init idle
        InitImageIdle1Cont = 920001, //init idle
        InitImageIdle2StartEnd = 920100,
        InitImageIdle2Cont = 920101,
        InitImageIdle3StartEnd = 920200,
        InitImageIdle3Cont = 920201,
        InitImageIdle4StartEnd = 920300,
        InitImageIdle4Cont = 920301,
        InitImageIdle5StartEnd = 920400,
        InitImageIdle5Cont = 920401,
        InitImageIdle6StartEnd = 920500,
        InitImageIdle6Cont = 920501,
        InitImageIdle7StartEnd = 920600,
        InitImageIdle7Cont = 920601,
        InitImageIdle8StartEnd = 920700,
        InitImageIdle8Cont = 920701,
        InitImageIdle9StartEnd = 920800,
        InitImageIdle9Cont = 920801,
        InitImageIdle10StartEnd = 920900,
        InitImageIdle10Cont = 920901,

        #endregion

        #region procode Download logo/idle

        DownloadImagePrint1StartEnd = 710000, //Download logo print 1
        DownloadImagePrint1Cont = 710001,   //Download logo print 1
        DownloadImagePrint2StartEnd = 710100, //Download logo merchant
        DownloadImagePrint2Cont = 710101, //Download logo merchant
        DownloadImagePrint3StartEnd = 710200, //Download logo receipt
        DownloadImagePrint3Cont = 710201, //Download logo receipt
        DownloadImagePrint4StartEnd = 710300,
        DownloadImagePrint4Cont = 710301,
        DownloadImagePrint5StartEnd = 710400,
        DownloadImagePrint5Cont = 710401,
        DownloadImagePrint6StartEnd = 710500,
        DownloadImagePrint6Cont = 710501,
        DownloadImagePrint7StartEnd = 710600,
        DownloadImagePrint7Cont = 710601,
        DownloadImagePrint8StartEnd = 710700,
        DownloadImagePrint8Cont = 710701,
        DownloadImagePrint9StartEnd = 710800,
        DownloadImagePrint9Cont = 710801,
        DownloadImagePrint10StartEnd = 710900,
        DownloadImagePrint10Cont = 710901,

        DownloadImageIdle1StartEnd = 720000, //Download idle
        DownloadImageIdle1Cont = 720001, //Download idle
        DownloadImageIdle2StartEnd = 720100,
        DownloadImageIdle2Cont = 720101,
        DownloadImageIdle3StartEnd = 720200,
        DownloadImageIdle3Cont = 720201,
        DownloadImageIdle4StartEnd = 720300,
        DownloadImageIdle4Cont = 720301,
        DownloadImageIdle5StartEnd = 720400,
        DownloadImageIdle5Cont = 720401,
        DownloadImageIdle6StartEnd = 720500,
        DownloadImageIdle6Cont = 720501,
        DownloadImageIdle7StartEnd = 720600,
        DownloadImageIdle7Cont = 720601,
        DownloadImageIdle8StartEnd = 720700,
        DownloadImageIdle8Cont = 720701,
        DownloadImageIdle9StartEnd = 720800,
        DownloadImageIdle9Cont = 720801,
        DownloadImageIdle10StartEnd = 720900,
        DownloadImageIdle10Cont = 720901,
        #endregion

    }

    class CommonConsole
    {
        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);
     static   int iTemp1 = 0;
     static int iTemp2 = 0;
     static int iTemp3 = 0;
        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sTimeHHMMSS()
        {
            return DateTime.Now.ToString("hhmmss", ciUSFormat);
        }

        public static string sDateMMddyy()
        {
            return DateTime.Now.ToString("MMddyy", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = CommonConsole.sFullTime();
            Console.BufferWidth = 1000; //untuk menangani case spasi kosong Init setelah remote 
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }

        public static string sGenerateISOSend(string sContent, ISO8583_MSGLib oIsoTemp, bool bComplete, bool isEnable48, int iCounterImage, int iTotalCounter, string sFileName, string sBit11, string sAppName)
        {
            string sIsoResponse;
            string sCounterImage, sTotalCounter, sLenFileName;
            ISO8583Lib oIso = new ISO8583Lib();
            oIsoTemp.MTI = "0810";
            oIsoTemp.sBinaryBitMap = "0010000000100000000000010000000000000010100000000000000000000000";
            ProcCode tempProcCode = (ProcCode)int.Parse(oIsoTemp.sBit[3].ToString());
            switch (tempProcCode)
            {
                case ProcCode.InitImagePrint1StartEnd:
                case ProcCode.InitImagePrint1Cont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        bComplete ? ProcCode.InitImagePrint1StartEnd.GetHashCode().ToString() : ProcCode.InitImagePrint1Cont.GetHashCode().ToString());
                    break;
                case ProcCode.DownloadImagePrint1StartEnd:
                case ProcCode.DownloadImagePrint1Cont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        bComplete ? ProcCode.DownloadImagePrint1StartEnd.GetHashCode().ToString() : ProcCode.DownloadImagePrint1Cont.GetHashCode().ToString());
                    break;
                case ProcCode.InitImageIdle1StartEnd:
                case ProcCode.InitImageIdle1Cont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        bComplete ? ProcCode.InitImageIdle1StartEnd.GetHashCode().ToString() : ProcCode.InitImageIdle1Cont.GetHashCode().ToString());
                    break;
                case ProcCode.DownloadImageIdle1StartEnd:
                case ProcCode.DownloadImageIdle1Cont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        bComplete ? ProcCode.DownloadImageIdle1StartEnd.GetHashCode().ToString() : ProcCode.DownloadImageIdle1Cont.GetHashCode().ToString());
                    break;
                case ProcCode.InitImagePrint2StartEnd:
                case ProcCode.InitImagePrint2Cont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        bComplete ? ProcCode.InitImagePrint2StartEnd.GetHashCode().ToString() : ProcCode.InitImagePrint2Cont.GetHashCode().ToString());
                    break;
                case ProcCode.DownloadImagePrint2StartEnd:
                case ProcCode.DownloadImagePrint2Cont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        bComplete ? ProcCode.DownloadImagePrint2StartEnd.GetHashCode().ToString() : ProcCode.DownloadImagePrint2Cont.GetHashCode().ToString());
                    break;
                case ProcCode.InitImagePrint3StartEnd:
                case ProcCode.InitImagePrint3Cont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        bComplete ? ProcCode.InitImagePrint3StartEnd.GetHashCode().ToString() : ProcCode.InitImagePrint3Cont.GetHashCode().ToString());
                    break;
                case ProcCode.DownloadImagePrint3StartEnd:
                case ProcCode.DownloadImagePrint3Cont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        bComplete ? ProcCode.DownloadImagePrint3StartEnd.GetHashCode().ToString() : ProcCode.DownloadImagePrint3Cont.GetHashCode().ToString());
                    break;
                //case ProcCode.DownloadImageIdle1StartEnd:
                //case ProcCode.DownloadImageIdle1Cont:
                //    oIso.SetField_Str(3, ref oIsoTemp,
                //        bComplete ? ProcCode.DownloadImageIdle1StartEnd.GetHashCode().ToString() : ProcCode.DownloadImageIdle1Cont.GetHashCode().ToString());
                //    break;
            }
           
            oIso.SetField_Str(11, ref oIsoTemp, sBit11);
            oIso.SetField_Str(12, ref oIsoTemp, string.Format("{0}", DateTime.Now.ToString("HHmmss", ciINAFormat)));
            // oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMddyy}", DateTime.Now));


            if (string.IsNullOrEmpty(sAppName) || Program.dtISO8583.Select(string.Format("SoftwareName='{0}' AND StandartISO8583=1", sAppName)).Length == 0)
                oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMddyy}", DateTime.Now));
            else
                oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));


            oIso.SetField_Str(39, ref oIsoTemp, "00");

            if (isEnable48)
            {
                oIso.SetField_Str(48, ref oIsoTemp, CommonLib.sStringToHex(oIsoTemp.sBit[48]));
            }

            string sHexConterImage = iCounterImage.ToString("X");
            sCounterImage = sHexConterImage.ToString().PadLeft(4, '0');
           // Console.WriteLine("Response {0} : {1} : ", sHexConterImage, sCounterImage);

            string sHexTotalCounter = iTotalCounter.ToString("X");
            sTotalCounter = sHexTotalCounter.ToString().PadLeft(4, '0');
            
            sLenFileName = sFileName.Length.ToString().PadLeft(2, '0');

            oIso.SetField_Str(57, ref oIsoTemp, string.Format("{0}{1}{2}{3}", sCounterImage, sLenFileName, CommonLib.sStringToHex(sFileName), sTotalCounter));

            if (!string.IsNullOrEmpty(sContent))
                oIso.SetField_Str(60, ref oIsoTemp, sContent);

            //sIsoResponse = oIso.PackToISOHexImage(oIsoTemp);
            sIsoResponse = oIso.PackToISOHex(oIsoTemp,2);
            return sIsoResponse;
        }

        public static string sGenerateISOError(string sContent, ISO8583_MSGLib oIsoTemp)
        {
            return string.Format("{0}{1}{2}{3}", oIsoTemp.sTPDU, oIsoTemp.sOriginAddr, oIsoTemp.sDestNII, sContent);
        }

        public static void GetAppNameSN(string sBit48, ref string _sAppName, ref string _sSN)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            //Console.WriteLine("HexBit48: {0}", sHexBit48);
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            //Console.WriteLine("Len 48: {0}", iLenSN.ToString());
            _sSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            //Console.WriteLine("SN: {0}", _sSN);
            int iLenAppName = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2), 4));
            //Console.WriteLine("LenAppName: {0}", iLenAppName);
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2), iLenAppName * 2));
            //Console.WriteLine("AppName: {0}", _sAppName);
        }

        public static void GetAutoInitMessage(string sBit48, ref string _sLastInitTime, ref string _sPABX, ref string _sAppName, ref string _sOSVers, ref string _sKernelEmvVers, ref string _sSerialNum, ref string _sReaderSN, ref string _sPSAMSN, ref string _sMCSN, ref string _sMemUsg, ref string _sICCID)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iIndex = 0;
            _sLastInitTime = sHexBit48.Substring(iIndex, 12);
            iIndex += 12;
            try
            {
                _sPABX = int.Parse(sHexBit48.Substring(iIndex, 8)).ToString();
            }
            catch (Exception)
            {
                _sPABX = "";
            }
            iIndex += 8;
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 30;
            _sOSVers = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 30;
            _sKernelEmvVers = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 30;
            _sSerialNum = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 30;
            _sReaderSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 30;
            _sPSAMSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 20));
            iIndex += 20;
            _sMCSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 20));
            iIndex += 20;
            _sMemUsg = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 30;
            _sICCID = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 40));
        }

        public static void GetSNAPPNameAutoInitMessage(string sBit48, ref string _sAppName, ref string _sSerialNum)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iIndex = 0;
            iIndex += 20;
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));
            iIndex += 90;
            _sSerialNum = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, 30));

        }

        public static string[] arrsGetBit48Values(string sBit48)
        {
            string[] arrsValues = new string[3];
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iIndex = 0;

            // SN <SN Length><SN>
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            iIndex += 4;
            arrsValues[0] = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            iIndex += (iLenSN * 2);

            // AppName <AppName Length><AppName>
            int iLenAppName = int.Parse(sHexBit48.Substring(iIndex, 4));
            iIndex += 4;
            arrsValues[1] = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, iLenAppName * 2));
            iIndex += (iLenAppName * 2);

            // Tipe Comms <CommsId>
            arrsValues[2] = sHexBit48.Substring(iIndex, 4);

            return arrsValues;
        }

        public static bool IsOpenFileAllowed(string sFilename)
        {
            bool bAllow = false;
            FileStream fs = null;
            try
            {
                fs = File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite);
                fs.Close();
                bAllow = true;
            }
            catch (IOException)
            {
                bAllow = false;
            }
            return bAllow;
        }

        public static int iISOLenReceive(string sTempLen)
        {
            int iReturn = 0;
            iReturn = CommonLib.iHexStringToInt(sTempLen);
            return iReturn;
        }

        /// <summary>
        /// convert Receive iso Message from hex to Int
        /// </summary>
        /// <param name="sTempLen">string : Temporary Len</param>
        /// <param name="cType">ConType : Conection Type</param>
        /// <returns>INT</returns>
        public static int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            //if (cType == ConnType.TcpIP)
            iReturn = CommonLib.iHexStringToInt(sTempLen);
            //else if (cType == ConnType.Serial)
            //    iReturn = int.Parse(sTempLen);
            return iReturn;
        }

        public static void GetCounterFileName(string sBit57, ref int _iCounterImage, ref string _sFileNameImage)
        {
            
            string sHexBit57 = CommonLib.sStringToHex(sBit57).Replace(" ", "");
           
            try
            {
                     string stemp = sHexBit57.Substring(0, 4);
                    _iCounterImage = CommonLib.iHexStringToInt(stemp);
                    if ((_iCounterImage == 19 || _iCounterImage == 40 || _iCounterImage == 41 || _iCounterImage == 42 || _iCounterImage == 32 || _iCounterImage == 43))
                    {
                        //Console.WriteLine("ini 42");
                    }
                   // Console.WriteLine("Request {0} : {1} : {2} :", sHexBit57, stemp,_iCounterImage);
            }
            catch (Exception ex) { Console.WriteLine("Catch 1 {0} : ", ex.Message.ToString()); }
        }

        public int iValidateCounter(int ivalid, int iValue)
        {
            int iTemp1 = 0;
            int iTemp2 = 0;
            int iTemp3 = 0;

            if (iValue == 1 && iTemp1 != 0)
            {
                iTemp1 = iValue;
                ivalid = iTemp1;
            }
            if (iValue == 1 && iTemp2 != 0)
            {
                iTemp1 = iValue;
                ivalid = iTemp1;
            }

            return ivalid;
        }

    }
}
