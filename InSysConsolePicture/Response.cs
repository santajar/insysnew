﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using InSysClass;

namespace InSysConsolePicture
{
    class Response : IDisposable
    {
        protected SqlConnection oConn;
        protected SqlConnection oConnAuditTrail;
        protected bool bCheckAllowInit;
        protected ConnType cType;
        protected int iTotalCounter = 0;
        protected string sTerminalId;
        protected string sAppName = null;
        protected string sSerialNum = null;
        protected bool bInit = false;
        protected bool bStart = false;
        protected bool bComplete = false;
        protected int iPercentage = 0;
        protected int iMaxByte = 400;
        protected bool bEnable48 = false;
        protected bool bEnableCompress = false;
        protected bool bEnableRemoteDownload = false;
        protected bool bCustom48_1;

        //tambahan untuk bit 48 message = autoinit
        protected string sLastInitTime = null;
        protected string sPABX = null;
        protected string sOSVers = null;
        protected string sKernelEmvVers = null;
        protected string sReaderSN = null;
        protected string sPSAMSN = null;
        protected string sMCSN = null;
        protected string sMemUsg = null;
        protected string sICCID = null;

        public string TerminalId { get { return sTerminalId; } }
        public string AppName { get { return sAppName; } }
        public string SerialNum { get { return sSerialNum; } }
        public bool IsProcessInit { get { return bInit; } }
        public bool IsStartInit { get { return bStart; } }
        public bool IsCompleteInit { get { return bComplete; } }
        public int Percentage { get { return iPercentage; } }

        protected string sProcessCode = "930000";
        public string ProcessCode { get { return sProcessCode; } }
        protected static bool bDebug = true;

        protected byte[] arrbKey = (new UTF8Encoding()).GetBytes("316E67336E316330");
        protected byte[] arrbReceive;
        protected static string sTypeMessage = "";

        public Response(string _sConnString, string _sConnStringAuditTrail, bool _IsCheckAllowInit, ConnType _cType) : this(_sConnString, _sConnStringAuditTrail, _IsCheckAllowInit, _cType, false) { }
        public Response(string _sConnString, string _sConnStringAuditTrail, bool _IsCheckAllowInit, ConnType _cType, bool _bDebug) : this(null, _sConnString, _sConnStringAuditTrail, _IsCheckAllowInit, _cType, false) { }
        public Response(byte[] _arrbReceive, string _sConnString, string _sConnStringAuditTrail, bool _IsCheckAllowInit, ConnType _cType) : this(_arrbReceive, _sConnString, _sConnStringAuditTrail, _IsCheckAllowInit, _cType, false) { }
        public Response(byte[] _arrbReceive, string _sConnString, string _sConnStringAuditTrail, bool _IsCheckAllowInit, ConnType _cType, bool _bDebug)
        {
            try
            {
                //Console.WriteLine("Create Object Response2");
                oConn = new SqlConnection();
                oConn = SQLConnLib.EstablishConnection(_sConnString);
                oConnAuditTrail = new SqlConnection();
                oConnAuditTrail = SQLConnLib.EstablishConnection(_sConnStringAuditTrail);
                bCheckAllowInit = _IsCheckAllowInit;
                cType = _cType;
                iMaxByte = iGetMaxByte();
                bDebug = _bDebug;
                arrbReceive = _arrbReceive;
            }
            catch (Exception ex)
            {
                //Trace.Write("Create Object Error: " + ex.Message);
                WriteLogtoDatabase("RESPONSE3", "Create Object Error: " + ex.Message);
                oConn.Close();
                oConn.Dispose();
                oConnAuditTrail.Close();
                oConnAuditTrail.Dispose();
            }
        }

        /// <summary>
        /// Close SQL Connection
        /// </summary>
        public void Dispose()
        {
            oConn.Close();
            oConn.Dispose();
            oConnAuditTrail.Close();
            oConnAuditTrail.Dispose();
        }

        public byte[] arrbResponse() { return arrbResponse(arrbReceive); }

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public byte[] arrbResponse(byte[] arrbReceive)
        {
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);
                //Console.WriteLine("Send Message: {0}", sSendMessage);

                if (!string.IsNullOrEmpty(sSendMessage))
                {
                    arrbTemp = new byte[iIsoLen + 2];
                    //arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);

                    sSendMessage = sSendMessage.Replace(" ", ""); // Remove all white space
                    byte[] arrbbuffer = new byte[sSendMessage.Length / 2];
                    for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                    {
                        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);
                    }
                    arrbTemp = arrbbuffer;
                }
            }
            catch (Exception ex)
            {
                //Trace.Write("|[RESPONSE]|Error arrbResponse : " + ex.Message);
                WriteLogtoDatabase("RESPONSE3", "|[RESPONSE]|Error arrbResponse : " + ex.Message);
                //Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTag = null;
            int iErrorCode = -1;
            
            //Console.WriteLine("Begin Get Response");

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            int iCounterImage = 0;
            string sFileNameImage = "";

            if (cType != ConnType.Modem)
            {
                int iLen = CommonConsole.iISOLenReceive(sTemp.Substring(0, 4));

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            try
            {
                // parse received ISO
                ISO8583Lib oIsoParse = new ISO8583Lib();
                ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
                //Console.WriteLine("ISO Length: {0}", oIsoMsg.sBit.Length.ToString());
                //Console.WriteLine("ArrbTemp");
                byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
                //Console.WriteLine("Unpack Iso");
                oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
                string sProcCode = oIsoMsg.sBit[3];
                sTerminalId = oIsoMsg.sBit[41];
                string sBit11 = oIsoMsg.sBit[11];
                //Console.WriteLine("sProcCode = {0}", sProcCode);

                // kirim ke sp,...
                try
                {
                    string sImageName = "";
                    string sType = "";
                    if (oIsoMsg.MTI == "0800")
                    {
                        ProcCode oProcCodeTemp = (ProcCode)int.Parse(sProcCode);
                        if (IsValidTerminalID(sTerminalId))
                        {
                            string sFileName = "";
                            bool bAllowInitLogo = false;
                            //Console.WriteLine("PROCODE : {0} ", oProcCodeTemp);
                            switch (oProcCodeTemp)
                            {

                                case ProcCode.InitImagePrint1StartEnd:
                                case ProcCode.InitImagePrint1Cont:
                                    bAllowInitLogo = isAllowInitPrintLogo(sTerminalId);
                                    sFileName = GetPrintFileName(sTerminalId, "Logo Print");
                                    sImageName = "Logo Print";
                                    sType = "Print";
                                    break;
                                case ProcCode.DownloadImagePrint1StartEnd: //download
                                case ProcCode.DownloadImagePrint1Cont: //download
                                    bAllowInitLogo = true;
                                    sFileName = GetPrintFileName(sTerminalId, "Logo Print");
                                    sImageName = "Logo Print";
                                    sType = "Print";
                                    break;
                                case ProcCode.InitImageIdle1StartEnd:
                                case ProcCode.InitImageIdle1Cont:
                                    bAllowInitLogo = isAllowInitIdleLogo(sTerminalId);
                                    sFileName = GetIdleFileName(sTerminalId);
                                    sImageName = "Logo Idle";
                                    sType = "Idle";
                                    break;
                                case ProcCode.DownloadImageIdle1StartEnd:
                                case ProcCode.DownloadImageIdle1Cont:
                                    bAllowInitLogo = true;
                                    sFileName = GetIdleFileName(sTerminalId);
                                    sImageName = "Logo Idle";
                                    sType = "Idle";
                                    break;

                                case ProcCode.InitImagePrint2StartEnd: //init merchant
                                case ProcCode.InitImagePrint2Cont: //init merchant
                                    bAllowInitLogo = isAllowInitPrintMerchant(sTerminalId);
                                    sFileName = GetPrintFileName(sTerminalId, "Logo Merchant");
                                    sImageName = "Logo Merchant";
                                    sType = "Merchant";
                                    break;
                                case ProcCode.DownloadImagePrint2StartEnd: //download merchant
                                case ProcCode.DownloadImagePrint2Cont: //download merchant
                                    bAllowInitLogo = true;
                                    sFileName = GetPrintFileName(sTerminalId, "Logo Merchant");
                                    sImageName = "Logo Merchant";
                                    sType = "Merchant";
                                    break;
                                case ProcCode.InitImagePrint3StartEnd: //logo receipt
                                case ProcCode.InitImagePrint3Cont: //logo receipt
                                    bAllowInitLogo = isAllowInitPrintRecipt(sTerminalId);
                                    sFileName = GetPrintFileName(sTerminalId, "Logo Receipt");
                                    sImageName = "Logo Receipt";
                                    sType = "Receipt";
                                    break;
                                case ProcCode.DownloadImagePrint3StartEnd:
                                case ProcCode.DownloadImagePrint3Cont:
                                    bAllowInitLogo = true;
                                    sFileName = GetPrintFileName(sTerminalId, "Logo Receipt");
                                    sImageName = "Logo Receipt";
                                    sType = "Receipt";
                                    break;
                            }
                            if (bAllowInitLogo == true)
                            {
                                if (!string.IsNullOrEmpty(sFileName))
                                {
                                    //switch (oProcCodeTemp)
                                    //{
                                    //    case ProcCode.DownloadImagePrint1StartEnd:
                                    #region "91__00"
                                    bool bComplete = false;
                                    string sImageData = "";
                                    //int iTotalCounter = 0;
                                    sProcessCode = sProcCode;
                                    //Console.WriteLine("Procode : {0} ", sProcessCode);

                                    DataTable dtDownloadTemp;
                                    int iImage = Convert.ToInt32(sProcCode.Substring(2, 2));
                                    if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                    {
                                        try
                                        {
                                            CommonConsole.GetAutoInitMessage(oIsoMsg.sBit[48], ref sLastInitTime, ref sPABX, ref sAppName, ref sOSVers, ref sKernelEmvVers, ref sSerialNum, ref sReaderSN, ref sPSAMSN, ref sMCSN, ref sMemUsg, ref sICCID);
                                            bCustom48_1 = true;
                                        }
                                        catch (Exception)
                                        {
                                            CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                        }

                                        bEnable48 = true;
                                    }

                                    if (!string.IsNullOrEmpty(oIsoMsg.sBit[57]))
                                    {
                                        CommonConsole.GetCounterFileName(oIsoMsg.sBit[57], ref iCounterImage, ref sFileNameImage);
                                    }

                                    if (!string.IsNullOrEmpty(oIsoMsg.sBit[60]))
                                    {
                                        string sHexBit60 = CommonLib.sStringToHex(oIsoMsg.sBit[60]).Replace(" ", "");
                                        if (sHexBit60 == "0x00")
                                        {
                                            sTypeMessage = "Initialize";
                                        }
                                        else
                                        {
                                            sTypeMessage = "AutoInit";
                                        }
                                        // Console.WriteLine("Type Message {0} : ", sTypeMessage);
                                    }

                                    if (Program.ltClassInitImage.FindIndex(classInitImageTemp => classInitImageTemp.Identity == sType + sFileName) == -1)
                                    {
                                        dtDownloadTemp = new DataTable();
                                        dtDownloadTemp = dtGetImageData(sType, sFileName);
                                        if (dtDownloadTemp.Rows.Count > 0)
                                        {
                                            ClassInitImage oClassImage = new ClassInitImage(sType + sFileName);
                                            oClassImage.ID = 0;
                                            oClassImage.dtResponses = dtDownloadTemp;

                                            if (Program.ltClassInitImage.FindIndex(classInitTempImage => classInitTempImage.Identity == sType + sFileName) > -1) Program.ltClassInitImage.RemoveAt(Program.ltClassInitImage.FindIndex(classInitImageTemp => classInitImageTemp.Identity == sType + sFileName));
                                            Program.ltClassInitImage.Add(oClassImage);
                                        }
                                        Program.ltClassInitImage[Program.ltClassInitImage.FindIndex(classInitTempImage => classInitTempImage.Identity == sType + sFileName)].ID = iCounterImage;
                                    }
                                    else
                                    {
                                        #region "Refresh Data Image and set Counter"
                                        if (!isTimeValid(sType, sFileName, Program.ltClassInitImage[Program.ltClassInitImage.FindIndex(classInitTempImage => classInitTempImage.Identity == sType + sFileName)].Time))
                                        {
                                            dtDownloadTemp = new DataTable();
                                            dtDownloadTemp = dtGetImageData(sType, sFileName);
                                            if (dtDownloadTemp.Rows.Count > 0)
                                            {
                                                ClassInitImage oClassImage = new ClassInitImage(sType + sFileName);
                                                oClassImage.ID = 0;
                                                oClassImage.dtResponses = dtDownloadTemp;

                                                if (Program.ltClassInitImage.FindIndex(classInitTempImage => classInitTempImage.Identity == sType + sFileName) > -1) Program.ltClassInitImage.RemoveAt(Program.ltClassInitImage.FindIndex(classInitImageTemp => classInitImageTemp.Identity == sType + sFileName));
                                                Program.ltClassInitImage.Add(oClassImage);
                                            }
                                            Program.ltClassInitImage[Program.ltClassInitImage.FindIndex(classInitTempImage => classInitTempImage.Identity == sType + sFileName)].ID = iCounterImage;
                                        }
                                        else
                                            Program.ltClassInitImage[Program.ltClassInitImage.FindIndex(classInitTempImage => classInitTempImage.Identity == sType + sFileName)].ID = iCounterImage;
                                        #endregion
                                    }

                                    sImageData = Program.ltClassInitImage[Program.ltClassInitImage.FindIndex(classInitImageTemp => classInitImageTemp.Identity == sType + sFileName)].ImageContent;
                                    if (Program.ltClassInitImage[Program.ltClassInitImage.FindIndex(classInitImageTemp => classInitImageTemp.Identity == sType + sFileName)].isLastCounter)
                                    {
                                        bComplete = true;
                                        iErrorCode = 1;
                                    }
                                    else
                                    {
                                        switch (oProcCodeTemp)
                                        {
                                            case ProcCode.InitImagePrint1StartEnd:
                                            case ProcCode.InitImagePrint1Cont:
                                            case ProcCode.InitImageIdle1StartEnd:
                                            case ProcCode.InitImageIdle1Cont:
                                            case ProcCode.InitImagePrint2StartEnd:
                                            case ProcCode.InitImagePrint2Cont:
                                            case ProcCode.InitImagePrint3StartEnd:
                                            case ProcCode.InitImagePrint3Cont:
                                                iErrorCode = 0;
                                                break;
                                            case ProcCode.DownloadImagePrint1StartEnd:
                                            case ProcCode.DownloadImagePrint1Cont:
                                            case ProcCode.DownloadImageIdle1StartEnd:
                                            case ProcCode.DownloadImageIdle1Cont:
                                            case ProcCode.DownloadImagePrint2StartEnd:
                                            case ProcCode.DownloadImagePrint2Cont:
                                            case ProcCode.DownloadImagePrint3StartEnd:
                                            case ProcCode.DownloadImagePrint3Cont:
                                                iErrorCode = 14;
                                                break;

                                        }
                                    }
                                    iTotalCounter = Program.ltClassInitImage[Program.ltClassInitImage.FindIndex(classInitImageTemp => classInitImageTemp.Identity == sType + sFileName)].TotalCounter;

                                    sTempReceive = CommonConsole.sGenerateISOSend(sImageData, oIsoMsg, bComplete, false, iCounterImage, iTotalCounter, sFileName, sBit11, sAppName);

                                    #endregion
                                }
                                else
                                {
                                    //Console.WriteLine("oProcodeTemp  1 {0} ", oProcCodeTemp);
                                    switch (oProcCodeTemp)
                                    {
                                        case ProcCode.InitImagePrint1StartEnd:
                                        case ProcCode.InitImagePrint1Cont:
                                        case ProcCode.DownloadImagePrint1StartEnd:
                                        case ProcCode.DownloadImagePrint1Cont:
                                            iErrorCode = 13;
                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("SET LOGO PRINT"), oIsoMsg);
                                            break;
                                        case ProcCode.InitImageIdle1StartEnd:
                                        case ProcCode.InitImageIdle1Cont:
                                        case ProcCode.DownloadImageIdle1StartEnd:
                                        case ProcCode.DownloadImageIdle1Cont:
                                            iErrorCode = 13;
                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("SET LOGO IDLE"), oIsoMsg);
                                            break;
                                        case ProcCode.InitImagePrint2StartEnd:
                                        case ProcCode.InitImagePrint2Cont:
                                        case ProcCode.DownloadImagePrint2StartEnd:
                                        case ProcCode.DownloadImagePrint2Cont:
                                            iErrorCode = 13;
                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("SET LOGO MERCH"), oIsoMsg);
                                            break;
                                        case ProcCode.InitImagePrint3StartEnd:
                                        case ProcCode.InitImagePrint3Cont:
                                        case ProcCode.DownloadImagePrint3StartEnd:
                                        case ProcCode.DownloadImagePrint3Cont:
                                            iErrorCode = 13;
                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("SET IMAGE RPT"), oIsoMsg);
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                //Console.WriteLine("oProcodeTemp 2 {0} ", oProcCodeTemp);
                                switch (oProcCodeTemp)
                                {
                                    case ProcCode.InitImagePrint1StartEnd:
                                    case ProcCode.InitImagePrint1Cont:
                                    case ProcCode.DownloadImagePrint1StartEnd:
                                    case ProcCode.DownloadImagePrint1Cont:
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("LOGO PRINT NOT ALLOWED"), oIsoMsg);
                                        break;
                                    case ProcCode.InitImageIdle1StartEnd:
                                    case ProcCode.InitImageIdle1Cont:
                                    case ProcCode.DownloadImageIdle1StartEnd:
                                    case ProcCode.DownloadImageIdle1Cont:
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("LOGO IDLE NOT ALLOWED"), oIsoMsg);
                                        break;
                                    case ProcCode.InitImagePrint2StartEnd:
                                    case ProcCode.InitImagePrint2Cont:
                                    case ProcCode.DownloadImagePrint2StartEnd:
                                    case ProcCode.DownloadImagePrint2Cont:
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("LOGO MERCH NOT ALLOWED"), oIsoMsg);
                                        break;
                                    case ProcCode.InitImagePrint3StartEnd:
                                    case ProcCode.InitImagePrint3Cont:
                                    case ProcCode.DownloadImagePrint3StartEnd:
                                    case ProcCode.DownloadImagePrint3Cont:
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("IMAGE RPT NOT ALLOWED"), oIsoMsg);
                                        break;
                                }
                            }
                        }
                        else
                        {
                            iErrorCode = 4;
                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALID TID"), oIsoMsg);
                            WriteLogtoDatabase("RESPONSE3", string.Format("INVALID TID: {0}", sTerminalId));
                        }
                    }

                    #region uncheck flag jika init sudah selesai
                    if (!string.IsNullOrEmpty(sTempReceive))
                    {
                        // tambahkan total length
                        _iIsoLenResponse = sTempReceive.Length / 2;
                        sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                        if (bDebug)
                            //Trace.Write(string.Format("|[RESPONSE]|{0}|ErrorCode : {2}|sTempReceive : {1}", sTerminalId, sTempReceive, iErrorCode));
                            WriteLogtoDatabase("RESPONSE3", string.Format("|[RESPONSE]|{0}|ErrorCode : {2}|sTempReceive : {1}", sTerminalId, sTempReceive, iErrorCode));

                        if (iErrorCode == 1 && (sProcCode == "910001" || sProcCode == "910201" || sProcCode == "910301" || sProcCode == "910401" || sProcCode == "910501" || sProcCode == "910601" || sProcCode == "910701" || sProcCode == "910801" || sProcCode == "910901" || sProcCode == "710001"))
                        {
                            SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET LogoPrint = 0 WHERE TerminalID = '{0}'", sTerminalId), oConn);
                            if (oConn.State != ConnectionState.Open) oConn.Open();
                            oCmd.ExecuteNonQuery();
                            oConn.Close();
                        }
                        if (iErrorCode == 1 && (sProcCode == "920001" || sProcCode == "920201" || sProcCode == "920301" || sProcCode == "910401" || sProcCode == "920501" || sProcCode == "920601" || sProcCode == "920701" || sProcCode == "920801" || sProcCode == "920901" || sProcCode == "720001"))
                        {
                            SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET LogoIdle = 0 WHERE TerminalID = '{0}'", sTerminalId), oConn);
                            if (oConn.State != ConnectionState.Open) oConn.Open();
                            oCmd.ExecuteNonQuery();
                            oConn.Close();
                        }

                        if (iErrorCode == 1 && (sProcCode == "910101" || sProcCode == "710101"))
                        {
                            SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET LogoMerchant = 0 WHERE TerminalID = '{0}'", sTerminalId), oConn);
                            if (oConn.State != ConnectionState.Open) oConn.Open();
                            oCmd.ExecuteNonQuery();
                            oConn.Close();
                        }
                        if (iErrorCode == 1 && (sProcCode == "910201" || sProcCode == "710201"))
                        {
                            SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET LogoReceipt = 0 WHERE TerminalID = '{0}'", sTerminalId), oConn);
                            if (oConn.State != ConnectionState.Open) oConn.Open();
                            oCmd.ExecuteNonQuery();
                            oConn.Close();
                        }

                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sImageName) + " " + iCounterImage.ToString() + "-->" + iTotalCounter);
                    }
                    #endregion uncheck flag jika init sudah selesai
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                    //    ex.ToString(), sTemp);
                    //Trace.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1} | sTerminalID : {2} | sProccode : {3}", ex.ToString(), sTemp, sTerminalId, sProcCode));
                    WriteLogtoDatabase("RESPONSE3", string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1} | sTerminalID : {2} | sProccode : {3}", ex.ToString(), sTemp, sTerminalId, sProcCode));
                }
            }
            catch (Exception ex)
            {
                //Trace.Write("Parsing Error: " + ex.Message);
                WriteLogtoDatabase("RESPONSE3", "Parsing Error: " + ex.Message);
            }
            Trace.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1} | sTerminalID : {2} | sTempReceive : {3}", "", sTemp, sTerminalId, sTempReceive.ToString()));

            return sTempReceive;
        }

        private bool isTimeValid(string sType, string sFileName, string sTime)
        {
            bool isValid = false;
            string sQuery = string.Format("SELECT DBO.isImageTimeValid('{0}','{1}','{2}')", sType, sFileName, sTime);
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                {
                    DataTable dtTemp = new DataTable();
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    dtTemp.Load(oCmd.ExecuteReader());
                    if (dtTemp.Rows.Count > 0)
                        isValid = bool.Parse(dtTemp.Rows[0][0].ToString());
                    oConn.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return isValid;
        }

        private string GetIdleFileName(string sTerminalId)
        {
            string sReturn="";
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPGetIdleFileName, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                if (oConn.State != ConnectionState.Open) oConn.Open();
                SqlDataReader oDataReader = oCmd.ExecuteReader();
                if (oDataReader.HasRows)
                {
                    while (oDataReader.Read())
                    {
                        sReturn = oDataReader[0].ToString();
                    }
                }
                oDataReader.Close();
            }
            return sReturn;
        }

        private string GetPrintFileName(string sTerminalId, string stypeLogo)
        {
            string sReturn="";
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPGetPrintFileName, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                //oCmd.Parameters.Add("@sLogo", SqlDbType.VarChar).Value = stypeLogo;
                if (oConn.State != ConnectionState.Open) oConn.Open();
                SqlDataReader oDataReader = oCmd.ExecuteReader();
                if (oDataReader.HasRows)
                {
                    while (oDataReader.Read())
                    {
                        sReturn = oDataReader[0].ToString();
                    }
                }
                oDataReader.Close();
            }
            return sReturn;
        }

        private DataTable dtGetImageData(string _sType,string _sFileName)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPImageGetData, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sFileName", SqlDbType.VarChar).Value = _sFileName;
                oCmd.Parameters.Add("@sType", SqlDbType.VarChar).Value = _sType;
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                return dtTemp;
            }
        }

        private void WriteLogtoDatabase(string _sType, string _sDetailLog)
        {
            try
            {
                if (oConnAuditTrail.State != ConnectionState.Open) oConnAuditTrail.Open();
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPConsoleLogInsert, oConnAuditTrail))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sLogType", SqlDbType.VarChar).Value = _sType;
                    oCmd.Parameters.Add("@sDetailLog", SqlDbType.VarChar).Value = _sDetailLog;
                    oCmd.ExecuteNonQuery();
                    oConnAuditTrail.Close();
                }
            }
            catch (Exception ex)
            {
                //Trace.Write("|ERROR|SendLogInitAudit|" + ex.Message);
                WriteLogtoDatabase("Error", string.Format("|ERROR|SendLogInitAudit| {1}", ex.ToString()));
            }
        }

        /// <summary>
        /// Get Auto Init Content from Database
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <param name="sContent"></param>
        private void sGetAutoInitContent(string sTerminalId, ref string sContent)
        {
            sContent = "";
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAutoInitAllow, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalID",SqlDbType.VarChar).Value = sTerminalId;
                    SqlParameter sOutputContent = oCmd.Parameters.Add("@sContent",SqlDbType.VarChar,65535);
                    sOutputContent.Direction = ParameterDirection.Output;
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    oCmd.ExecuteNonQuery();
                    sContent = (string)oCmd.Parameters["@sContent"].Value;
                    oConn.Close();
                }
            }
            catch (Exception ex)
            {
                //Trace.Write(string.Format("Error spAutoInitAllow : {0}", ex.Message));
                WriteLogtoDatabase("RESPONSE3", string.Format("Error spAutoInitAllow : {0}", ex.Message));
            }
        }

        /// <summary>
        /// Check bit 48 using ustom or not
        /// </summary>
        /// <param name="sTerminalId">string : TerminalID</param>
        /// <returns>Bool : true if bit 48 Custom</returns>
        protected bool IsCustom48(string sTerminalId)
        {
            bool isAllow = false;
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPBitmapBrowseCustom48, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@iCustom48", SqlDbType.Bit).Direction = ParameterDirection.Output;
                if (oConn.State != ConnectionState.Open) oConn.Open();
                oCmd.ExecuteNonQuery();
                isAllow = bool.Parse(oCmd.Parameters["@iCustom48"].Value.ToString());
                oConn.Close();
            }
            return isAllow;
        }

        /// <summary>
        /// Write Auto init Log to database
        /// </summary>
        private void WriteLogInit(string sTerminalId, string sLastInitTime, string sPABX, string sAppName, string sOSVers, string sKernelEmvVers, string sSerialNum, string sReaderSN, string sPSAMSN, string sMCSN, string sMemUsg, string sICCID)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitUpdate, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@sLastInitTime", SqlDbType.VarChar).Value = sLastInitTime;
                oCmd.Parameters.Add("@sPABX", SqlDbType.VarChar).Value = sPABX;
                oCmd.Parameters.Add("@sSoftwareVers", SqlDbType.VarChar).Value = sAppName;
                oCmd.Parameters.Add("@sOSVers", SqlDbType.VarChar).Value = sOSVers;
                oCmd.Parameters.Add("@sKernelEmvVers", SqlDbType.VarChar).Value = sKernelEmvVers;
                oCmd.Parameters.Add("@sEdcSN", SqlDbType.VarChar).Value = sSerialNum;
                oCmd.Parameters.Add("@sReaderSN", SqlDbType.VarChar).Value = sReaderSN;
                oCmd.Parameters.Add("@sPSAMSN", SqlDbType.VarChar).Value = sPSAMSN;
                oCmd.Parameters.Add("@sMCSN", SqlDbType.VarChar).Value = sMCSN;
                oCmd.Parameters.Add("@sMemUsg", SqlDbType.VarChar).Value = sMemUsg;
                oCmd.Parameters.Add("@sICCID", SqlDbType.VarChar).Value = sICCID;
                if (oConn.State != ConnectionState.Open) oConn.Open();
                oCmd.ExecuteNonQuery();
                oConn.Close();
            }
        }

        /// <summary>
        /// Write Lof Terminal SN Echo to database
        /// </summary>
        private void WriteLogTerminalSNEcho(ISO8583_MSGLib oIsoMsg)
        {
            string[] arrsBit48Values = CommonConsole.arrsGetBit48Values(oIsoMsg.sBit[48]);
            if (arrsBit48Values != null)
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAuditTerminalSNEchoInsert, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalSN", SqlDbType.VarChar).Value = arrsBit48Values[0];
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = oIsoMsg.sBit[41];
                    oCmd.Parameters.Add("@sAppName", SqlDbType.VarChar).Value = arrsBit48Values[1];
                    oCmd.Parameters.Add("@iCommsID", SqlDbType.Int).Value = int.Parse(arrsBit48Values[2]);
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    oCmd.ExecuteNonQuery();
                    oConn.Close();
                }
        }

        protected string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP || cType == ConnType.Modem)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            //sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected string sProcessing(int iCode, string sValues)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sValues;
                    break;
                case 1:
                    sTemp = "Initialize " + sValues + " Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = sValues+"Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                case 6:
                    sTemp = "Application Not Allow";
                    break;
                case 7:
                    sTemp = "Processing Echo";
                    break;
                case 8:
                    sTemp = "Processing Download " + sValues;
                    break;
                case 9:
                    sTemp = "Download Profile COMPLETE";
                    break;
                case 10:
                    sTemp = "Processing Autoinit Phase-1";
                    break;
                case 11:
                    sTemp = "Processing Autoinit Phase-3";
                    break;
                case 12:
                    sTemp = "Processing Init FLAZZ";
                    break;
                case 13:
                    sTemp = "Set " + sValues;
                    break;
                case 14:
                    sTemp = "Processing Download " + sValues;
                    break;
                default:
                    sTemp = "Unknown Error Code : " + iCode.ToString();
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
                case "AI":
                    sTemp = "AID";
                    break;
                case "PK":
                    sTemp = "CAPK";
                    break;
                case "PP":
                    sTemp = "PinPad";
                    break;
                case "GP":
                    sTemp = "GPRS";
                    break;
                case "MN":
                    sTemp = "Custom Menu";
                    break;
                default:
                    sTemp = "";
                    break;
            }
            return sTemp;
        }

        /// <summary>
        /// Get List Content Profile from database
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <param name="_sContent">string : Content</param>
        /// <returns>List Content</returns>
        protected List<ContentProfile> ltGetContentProfile(string _sTerminalId, ref string _sContent, ProcCode oProcCodeTemp)
        {
            List<ContentProfile> ltCPTemp = new List<ContentProfile>();
            string sQuery;
            if (oProcCodeTemp == ProcCode.DownloadProfileStartEnd)
                sQuery = CommonSP.sSPProfileTextFullTableDLProfile;
            else
                sQuery = CommonSP.sSPProfileTextFullTable;
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                

                string[] arrsMaskedTag = arrsGetMaskedTag(_sTerminalId);

                if (dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow rowTemp in dtTemp.Rows)
                    {
                        string sTag = rowTemp["Tag"].ToString();
                        string sValue = rowTemp["TagValue"].ToString();
                        try
                        {
                            if (arrsMaskedTag.Length > 0 && arrsMaskedTag.Contains(sTag))
                                sValue = EncryptionLib.Decrypt3DES(CommonLib.sHexToStringUTF8(sValue), arrbKey);

                        }
                        catch (Exception)
                        {
                            sValue = rowTemp["TagValue"].ToString();
                        }

                        string sContent;
                        if (sTag != "AE37" && sTag != "AE38" && sTag != "TL11" && sTag.Substring(0, 2) != "PK")
                            sContent = string.Format("{0}{1:00}{2}", sTag, sValue.Length, sValue);
                        else
                        {
                            if (sTag == "AE37" || sTag == "AE38")
                                sContent = string.Format("{0}{1:000}{2}", sTag, sValue.Length, sValue);
                            else if (sTag == "PK04" && sTag == "PK06")
                                sContent = string.Format("{0}{1}{2}",
                                    CommonLib.sStringToHex(sTag),
                                    CommonLib.sStringToHex(string.Format("{0:000}", sValue.Length)),
                                    CommonLib.sStringToHex(sValue));
                            else
                                sContent = string.Format("{0}{1}{2}",
                                    sTag,
                                    string.Format("{0:000}", rowTemp["TagLength"]),
                                    sValue);
                            //sContent = string.Format("{0}{1}{2}",
                            //        CommonLib.sStringToHex(sTag),
                            //        CommonLib.sStringToHex(string.Format("{0:000}", rowTemp["TagLength"])),
                            //        sValue);
                        }
                        _sContent += sContent;
                        ContentProfile oCPTemp = new ContentProfile(sTag.Substring(0, 2), sContent);
                        int iIndexSearch = -1;
                        if (ltCPTemp.Count == 0)
                            ltCPTemp.Add(oCPTemp);
                        else
                            if ((iIndexSearch = ltCPTemp.FindIndex(oCPSearch => oCPSearch.Tag == oCPTemp.Tag)) > -1)
                            {
                                ContentProfile oCPSearchResult = ltCPTemp[iIndexSearch];
                                ContentProfile oCPNew = new ContentProfile(oCPTemp.Tag, oCPSearchResult.Content + oCPTemp.Content);
                                ltCPTemp[iIndexSearch] = oCPNew;
                            }
                            else
                                ltCPTemp.Add(oCPTemp);
                    }
                }
                oConn.Close();
            }
            return ltCPTemp;
        }

        /// <summary>
        /// Check TerminalID is masking or not
        /// </summary>
        /// <param name="_sTerminalId"> string : Terminal ID</param>
        /// <returns>List Masking Tag</returns>
        protected string[] arrsGetMaskedTag(string _sTerminalId)
        {
            List<string> ltTag = new List<string>();
            string sQuery = string.Format("SELECT * FROM tbItemList WHERE DatabaseID IN (SELECT DatabaseID FROM tbProfileTerminalList WHERE TerminalID='{0}') AND vMasking=1", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtTemp);
                if (dtTemp != null && dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow row in dtTemp.Rows)
                        ltTag.Add(row["Tag"].ToString());
                }
                oConn.Close();
            }
            return ltTag.ToArray();
        }

        /// <summary>
        /// Create File Compress
        /// </summary>
        /// <param name="sFilename">string : File Name</param>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>Bool : true if Success</returns>
        protected bool IsNetCompressSuccess(string sFilename, string sTerminalId)
        {
            bool bReturn = false;
            if (File.Exists(sFilename))
            {
                try
                {
                    FileInfo fi = new FileInfo(sFilename);
                    using (FileStream inFile = fi.OpenRead())
                    {
                        string sGzipFile = sFilename + ".gz";
                        if (File.Exists(sGzipFile))
                            File.Delete(sGzipFile);
                        using (FileStream outFile = File.Create(sFilename + ".gz"))
                        {
                            using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                            {
                                byte[] buffer = new byte[fi.Length];
                                inFile.Read(buffer, 0, Convert.ToInt32(fi.Length));
                                compress.Write(buffer, 0, Convert.ToInt32(fi.Length));
                                bReturn = true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    //MessageBox.Show("NetCompress " + ex.Message);
                }
            }
            return bReturn;
        }

        /// <summary>
        /// Check Allow Compress
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : true if init Compress</returns>
        protected bool IsAllowCompress(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsCompressInit('{0}')", sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                oConn.Close();
            }
            return isAllow;
        }

        /// <summary>
        /// Check Allow Remote Download
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : True is allow remote Download</returns>
        protected bool IsAllowRemoteDownload(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsRemoteDownloadAllowed('{0}')", sTerminalId);
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                {
                    DataTable dtTemp = new DataTable();
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    dtTemp.Load(oCmd.ExecuteReader());
                    if (dtTemp.Rows.Count > 0)
                        isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                    oConn.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return isAllow;
        }

        /// <summary>
        /// Check Allow Init Idle Logo
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : True is allow Init Idle Logo</returns>
        private bool isAllowInitIdleLogo(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.isAllowInitIdleLogo('{0}')", sTerminalId);
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                {
                    DataTable dtTemp = new DataTable();
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    dtTemp.Load(oCmd.ExecuteReader());
                    if (dtTemp.Rows.Count > 0)
                        isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                    oConn.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return isAllow;
        }

        /// <summary>
        /// Check Allow Init Print Logo
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : True is allow Init Idle Logo</returns>
        private bool isAllowInitPrintLogo(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.isAllowInitPrintLogo('{0}')", sTerminalId);
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                {
                    DataTable dtTemp = new DataTable();
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    dtTemp.Load(oCmd.ExecuteReader());
                    if (dtTemp.Rows.Count > 0)
                        isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                    oConn.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return isAllow;
        }

        private bool isAllowInitPrintMerchant(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.isAllowInitPrintMerchant('{0}')", sTerminalId);
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                {
                    DataTable dtTemp = new DataTable();
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    dtTemp.Load(oCmd.ExecuteReader());
                    if (dtTemp.Rows.Count > 0)
                        isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                    oConn.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return isAllow;
        }

        private bool isAllowInitPrintRecipt(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.isAllowInitPrintRecipt('{0}')", sTerminalId);
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                {
                    DataTable dtTemp = new DataTable();
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    dtTemp.Load(oCmd.ExecuteReader());
                    if (dtTemp.Rows.Count > 0)
                        isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                    oConn.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return isAllow;
        }


        /// <summary>
        /// Get Max Byte from Database
        /// </summary>
        /// <returns>int : Max Byte</returns>
        protected int iGetMaxByte()
        {
            int iMaxLength = 400;
            string sQuery = string.Format("SELECT dbo.iPacketLength() [MaxByte]");
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                if (oConn.State != ConnectionState.Open) oConn.Open();
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        iMaxLength = int.Parse(oRead["MaxByte"].ToString());
                }
                oConn.Close();
            }
            return iMaxLength;
        }

        /// <summary>
        /// Create Temporary Table for Init Data
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <param name="isCompress">bool: True if Compress</param>
        /// <returns>Data Table </returns>
        protected DataTable dtGetInitTemp(string _sTerminalId, ref bool isCompress, ProcCode oProcCodeTemp)
        {
            DataTable dtInitTemp = new DataTable();
            dtInitTemp.Columns.Add("TerminalId", typeof(string));
            dtInitTemp.Columns.Add("Content", typeof(string));
            dtInitTemp.Columns.Add("Tag", typeof(string));
            dtInitTemp.Columns.Add("Flag", typeof(bool));

            string sContent = null;
            List<ContentProfile> _ltCPTemp = ltGetContentProfile(_sTerminalId, ref sContent, oProcCodeTemp);
            if (_ltCPTemp != null && _ltCPTemp.Count > 0)
            {
                isCompress = bEnableCompress;
                string sTag;
                int iOffset = 0;
                int iFlag = 0;
                if (bEnableCompress)
                {
                    #region "GZ part"
                    sTag = "GZ";
                    iOffset = 0;
                    string sContentCompressed = null;
                    // write to file locally and compress
                    if (!Directory.Exists(Environment.CurrentDirectory + @"\ZIP"))
                        Directory.CreateDirectory(Environment.CurrentDirectory + @"\ZIP");
                    string sFilename = Environment.CurrentDirectory + @"\ZIP\" + _sTerminalId;

                    if (File.Exists(sFilename))
                        File.Delete(sFilename);
                    CommonLib.Write2File(sFilename, sContent, false);

                    if (IsNetCompressSuccess(sFilename, _sTerminalId))
                    {
                        FileStream fsRead = new FileStream(sFilename + ".gz", FileMode.Open);
                        byte[] arrbContent = new byte[fsRead.Length];
                        fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
                        fsRead.Close();
                        sContentCompressed = CommonLib.sByteArrayToHexString(arrbContent).Replace(" ", "");
                        while (!string.IsNullOrEmpty(sContentCompressed) && iOffset < sContentCompressed.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            rows["Content"] = sContentCompressed.Substring(iOffset,
                                iOffset + (iMaxByte * 2) <= sContentCompressed.Length ?
                                (iMaxByte * 2) :
                                sContentCompressed.Length - iOffset);
                            dtInitTemp.Rows.Add(rows);
                            iOffset += (iMaxByte * 2);
                        }
                    }
                    #endregion
                }
                else
                {
                    iFlag = 0;
                    iOffset = 0;
                    #region "non-GZ part"
                    foreach (ContentProfile oCPTemp in _ltCPTemp)
                    {
                        sTag = oCPTemp.Tag;
                        sContent = oCPTemp.Content;
                        iOffset = 0;
                        //while (iOffset <= sContent.Length), error, row menjadi null ketika value nya kosong.
                        while (iOffset < sContent.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            int iOffsetHeader = sContent.IndexOf(sTag + "01", iOffset + 1);
                            int iLength = 0;

                            if (sTag != "AI" && sTag != "PK")
                            {
                                if (iOffset + iMaxByte <= sContent.Length)
                                    if (iOffsetHeader > 0)
                                        if (iOffset + iMaxByte <= iOffsetHeader)
                                            iLength = iMaxByte;
                                        else
                                            iLength = iOffsetHeader - iOffset;
                                    else
                                        iLength = iMaxByte;
                                else
                                    iLength = sContent.Length - iOffset;
                            }
                            else
                            {
                                if (iOffset + iMaxByte <= sContent.Length || iOffsetHeader > 0)
                                    if (iOffsetHeader > 0)
                                        if (iOffset + iMaxByte <= iOffsetHeader)
                                            iLength = iMaxByte;
                                        else
                                            iLength = iOffsetHeader - iOffset;
                                    else
                                        iLength = iMaxByte;
                                else
                                    iLength = sContent.Length - iOffset;
                            }

                            rows["Content"] = sContent.Substring(iOffset, iLength);

                            iOffset += iLength;
                            dtInitTemp.Rows.Add(rows);
                        }
                    }
                    #endregion
                }
            }
            return dtInitTemp;
        }

        /// <summary>
        /// Determine Application Allow Init
        /// </summary>
        /// <param name="_sTerminalId"> string : Terminal ID</param>
        /// <param name="_sAppName">string : Application Name</param>
        /// <returns>bool: true if Allow Init</returns>
        protected bool IsApplicationAllowInit(string _sTerminalId, string _sAppName)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsApplicationAllowInit('{0}','{1}')", _sTerminalId, _sAppName);
            
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                oConn.Close();
            }
            return isAllow;
        }

        /// <summary>
        /// Determine Init Allow
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <returns>bool: true is allow</returns>        
        protected bool IsInitAllowed(string _sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsInitAllowed('{0}')", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                oConn.Close();
            }
            return isAllow;
        }

        //BEGIN
        //string sTIDRemoteDownload;
        private Boolean CheckTIDRemoteDownload(string sTIDRemoteDownload)
        {
            sTIDRemoteDownload = sTIDRemoteDownload.ToString();
            oConn.Open();
            //string sql = "select TerminalID from tbListTIDRemoteDownload WHere TerminalID";
            SqlCommand command = new SqlCommand("spTIDRemoteDownload", oConn);
            command.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
            //SqlCommand command = new SqlCommand(sql, oConn);
            if (oConn.State != ConnectionState.Open) oConn.Open();
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sTIDRemoteDownload)
                {
                    reader.Close();
                    return true;
                }
            }
            oConn.Close();
            reader.Close();
            return false;
        }

        //LAST
        /// <summary>
        /// Determine Valid Terminal ID
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <returns>Bool : true if valid</returns>
        protected bool IsValidTerminalID(string _sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsValidTerminalID('{0}')", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                oConn.Close();
            }
            return isAllow;
        }

    }
}
