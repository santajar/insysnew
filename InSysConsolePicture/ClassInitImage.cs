﻿using System.Data;
using System;

namespace InSysConsolePicture
{
    class ClassInitImage
    {
        protected DataTable dtImageData = new DataTable();
        protected int iImageID;
        protected string sFileName;
        protected int iID;
        protected string sImageContent;
        protected string sIdentity;

        public ClassInitImage() { }
        public ClassInitImage(string _sIdentity)
        {
            sIdentity = _sIdentity;
        }

        public DataTable dtResponses
        {
            set { dtImageData = value; }
        }

        public string Identity { get { return sIdentity; } set { sIdentity = value; } }
        public int ID {set { iID = value; } }

        public string FileName { get { return sGetFileName(); } }
        public string Type { get { return sGetType(); } }
        public string Time { get { return sGetTime(); } }
        public int ImageID { get { return iGetImageID(iID); } }
        public string ImageContent {get{return sGetImageContent(iID);}}
        public bool isLastCounter { get { return bIsLastPacket(iID); } }
        public int TotalCounter { get { return iGetTotalCounter(); } }

        private string sGetTime()
        {
            string sTempTime = "";
            try
            {
                sTempTime = dtImageData.Select(string.Format("Id = {0}", iID))[0]["UploadTime"].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sTempTime;
        }

        private string sGetFileName()
        {
            string sTempFileName = "";
            try
            {
                sTempFileName = dtImageData.Select(string.Format("Id = {0}", iID))[0]["FileName"].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sTempFileName;
        }

        private string sGetType()
        {
            string sTempType = "";
            try
            {
                sTempType = dtImageData.Select(string.Format("Id = {0}", iID))[0]["LogoType"].ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sTempType;
        }

        private string sGetImageContent(int iID)
        {
 	        string sTempImageContent="";
            try
            {
                sTempImageContent = dtImageData.Select(string.Format("Id = {0}", iID))[0]["ImageContent"].ToString();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sTempImageContent;
        }

        private int iGetImageID(int iID)
        {
            int iTempImageID = 0;
            try
            {
                iTempImageID = int.Parse(dtImageData.Select(string.Format("[FileName] = '{0}'", sFileName))[0]["ImageID"].ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return iTempImageID;
        }

        protected bool bIsLastPacket(int iID)
        {
            bool bReturn = false;

            if (iID == dtImageData.Rows.Count - 1)
                bReturn = true;

            return bReturn;
        }

        private int iGetTotalCounter()
        {
            int iTotalCounter = 0;
            try
            {
                iTotalCounter = int.Parse(dtImageData.Rows.Count.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return iTotalCounter;
        }
    }
}
