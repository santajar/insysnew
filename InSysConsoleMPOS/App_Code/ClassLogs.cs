using System;
using System.IO;
using System.Threading;
using InSysClass;

namespace InSysConsoleMPOS
{
    class Logs
    {
        static string sPath = Environment.CurrentDirectory;

        static string sDirName = @"\LOGS\";
        static string sFilename = "Log_";
        static string sFileType = ".log";

        protected static bool isLogFolderExist(string _sDirectory)
        {
            return Directory.Exists(_sDirectory);
        }

        public static void Write(string sError)
        {
            Write(sPath + sDirName, sError);
        }

        public static void Write(string sDirectory, string sError)
        {
            if (!isLogFolderExist(sDirectory))
                Directory.CreateDirectory(sDirectory);
            string sFile = null;
            sFile = sPath + sDirName + sFilename + CommonConsole.sDateYYYYMMDD() + sFileType;
            if (!File.Exists(sFile))
                using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                    fs.Close();

            while (true)
            {
                if (CommonConsole.IsOpenFileAllowed(sFile))
                    break;
                else
                    Thread.Sleep(100);
            }
            CommonLib.Write2File(sFile, CommonConsole.sFullTime() + " " + sError, true);
        }
    
    }
}
