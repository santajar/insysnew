﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using InSysClass;

namespace ConsoleRepairRelation
{
    class Program
    {
        static SqlConnection oSqlConn = new SqlConnection();
        static DataTable dtTerminalListError;
        static DataTable dtMasterRelation = new DataTable();
        static DataTable dtMasterAcquirer = new DataTable();
        static DataTable dtMasterIssuer = new DataTable();

        static List<TerminalRelation> ltMasterRelation = new List<TerminalRelation>();

        static void Main(string[] args)
        {
            try
            {
                Init();
                int iCount = 1;
                List<string> ltTerminalId = new List<string>();
                ltTerminalId = GetListTerminalId();
                foreach (string sTerminalId in ltTerminalId)
                {
                    try
                    {
                        DataSet dsTemp = new DataSet();
                        DataTable dtRelation = new DataTable();
                        DataTable dtIssuer = new DataTable();
                        DataTable dtAcquirer = new DataTable();
                        DataTable dtTerminal = new DataTable();

                        InitTerminalTable(ref dsTemp, sTerminalId);
                        dtRelation = dsTemp.Tables[0];
                        dtIssuer = dsTemp.Tables[1];
                        dtAcquirer = dsTemp.Tables[2];
                        dtTerminal = dsTemp.Tables[3];
                        DataTable dtAcquirerList = (new DataView(dtAcquirer)).ToTable(true, "AcquirerName");
                        DataTable dtIssuerList = (new DataView(dtIssuer)).ToTable(true, "IssuerName");
                        
                        string sMaster = sTerminalMaster(dtAcquirerList,
                            ((dtTerminal.Select("TerminalTag='DE05'"))[0])["TerminalTagValue"].ToString());
                        if (!string.IsNullOrEmpty(sMaster))
                        {
                            ShowWriteLog(string.Format("{2:00}.TerminalId : {0}, Master : {1}", sTerminalId, sMaster, iCount));
                            string sUserId = null;
                            if (IsBroken(dtAcquirerList, dtIssuerList, dtRelation, sMaster))
                                if (IsAllowUserAccess(oSqlConn, sTerminalId, ref sUserId))
                                {
                                    UserAccessInsert(oSqlConn, sTerminalId);
                                    if (isDeleteRelationSuccess(sTerminalId))
                                    {
                                        RestoreRelation(sTerminalId, sMaster);
                                        RestoreIssuer(sTerminalId, sMaster, dtIssuer);
                                        RestoreAcquirerPromoSQ(sTerminalId, sMaster, dtAcquirer);
                                        SaveFullContentClass.SaveFullContent(oSqlConn, sTerminalId);
                                    }
                                    else
                                        ShowWriteLog(string.Format("{0} FAILED, Delete old Relation failed", sTerminalId));
                                    UserAccessDelete(oSqlConn, sTerminalId);
                                }
                                else
                                    ShowWriteLog(string.Format("{0} FAILED, Profile is being accessed by {1}", sTerminalId, sUserId));
                            else
                                ShowWriteLog(string.Format("{0} is not broken", sTerminalId));
                        }
                        else
                            ShowWriteLog(string.Format("{0} FAILED, Master not found", sTerminalId));

                    }
                    catch (Exception ex)
                    {
                        ShowWriteLog(ex.Message);
                    }
                    iCount++;
                }
            }
            catch (Exception ex)
            {
                ShowWriteLog(ex.Message);
            }
            Console.Read();
        }

        class TerminalRelation
        {
            public string TerminalId { get { return sTerminalId; } set { sTerminalId = value; } }
            public string Category { get { return sCategory; } set { sCategory = value; } }
            public List<Relation> TerminalIdRelation = new List<Relation>();

            public int TotalAcquirer { get{return iTotalAcquirer();}}
            public int TotalIssuer { get { return iTotalIssuer(); } }
            public string[] arrAcquirer { get { return ltAcquirer.ToArray(); } }
            public string[] arrIssuer { get { return ltIssuer.ToArray(); } }

            protected string sTerminalId;
            protected string sCategory;
            protected List<string> ltAcquirer = new List<string>();
            protected List<string> ltIssuer = new List<string>();
            protected int iTotalAcquirer()
            {
                foreach (Relation _oRelation in TerminalIdRelation)
                    if (ltAcquirer == null)
                        ltAcquirer.Add(_oRelation.sAcquirer);
                    else
                        if (string.IsNullOrEmpty(ltAcquirer.Find(o => o == _oRelation.sAcquirer)))
                            ltAcquirer.Add(_oRelation.sAcquirer);
                ltAcquirer.Sort();
                return ltAcquirer.Count;
            }

            protected int iTotalIssuer()
            {
                foreach (Relation _oRelation in TerminalIdRelation)
                    if (ltIssuer == null)
                        ltIssuer.Add(_oRelation.sIssuer);
                    else
                        if (string.IsNullOrEmpty(ltIssuer.Find(o => o == _oRelation.sIssuer)))
                            ltIssuer.Add(_oRelation.sIssuer);
                ltIssuer.Sort();
                return ltAcquirer.Count;
            }
        }

        class Relation
        {
            public string sAcquirer;
            public string sIssuer;
            public string sCard;
        }

        #region "Init"
        static void Init()
        {
            InitConn();
            InitMaster();
            InitTerminalList();
        }

        static void InitConn()
        {
            oSqlConn = new SqlConnection((new InitData()).sGetConnString());
            oSqlConn.Open();
        }

        static void InitMaster()
        {
            InitMasterTable();

            for (int iCount = 0; iCount < dtMasterRelation.Rows.Count / 3; iCount++)
            {
                TerminalRelation oTempTerminalRelation = new TerminalRelation();
                Relation oTempRelation = new Relation();
                string sTerminalId = dtMasterRelation.Rows[3 * iCount]["TerminalId"].ToString();
                string sNameAcq = null;
                string sNameIss = null;
                string sNameCard = null;
                for (int iIndexArr = 0; iIndexArr < 3; iIndexArr++)
                {
                    string sTag = dtMasterRelation.Rows[(3 * iCount) + iIndexArr]["RelationTag"].ToString();
                    string sTagValue = dtMasterRelation.Rows[(3 * iCount) + iIndexArr]["RelationTagValue"].ToString();
                    switch (sTag)
                    {
                        case "AD01":
                            sNameCard = sTagValue;
                            break;
                        case "AD02":
                            sNameIss = sTagValue;
                            break;
                        case "AD03":
                            sNameAcq = sTagValue;
                            break;
                    }
                }
                oTempTerminalRelation.TerminalId = sTerminalId;
                oTempRelation.sAcquirer = sNameAcq;
                oTempRelation.sIssuer = sNameIss;
                oTempRelation.sCard = sNameCard;
                TerminalRelation oSearchResult = ltMasterRelation.Find(o => o.TerminalId == oTempTerminalRelation.TerminalId);
                if (oSearchResult != null)
                    oSearchResult.TerminalIdRelation.Add(oTempRelation);
                else
                {
                    oTempTerminalRelation.Category = dtGetTerminal(sTerminalId, " AND TerminalTag='DE05'").Rows[0]["TerminalTagValue"].ToString();
                    oTempTerminalRelation.TerminalIdRelation.Add(oTempRelation);
                    ltMasterRelation.Add(oTempTerminalRelation);
                }
            }
        }

        static void InitTerminalList()
        {
            //dtTerminalListError = new DataTable();
            //dtTerminalListError = dtGetTerminalListError();
        }

        static void InitMasterTable()
        {
            DataSet dsTemp = new DataSet();
            string sQuery = string.Format(
                "SELECT TerminalId, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue " +
                "FROM tbProfileRelation WHERE TerminalID LIKE 'SIQ%' ORDER BY TerminalID "+
                "SELECT TerminalID, IssuerName, IssuerTag, IssuerLengthOfTagLength, IssuerTagLength, IssuerTagValue " +
                "FROM tbProfileIssuer WHERE TerminalID LIKE 'SIQ%' "+
                "SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerLengthOfTagLength, AcquirerTagLength, AcquirerTagValue " +
                "FROM tbProfileAcquirer WHERE TerminalID LIKE 'SIQ%' ");
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oCmd.CommandTimeout = 0;
                (new SqlDataAdapter(oCmd)).Fill(dsTemp);
                dtMasterRelation = dsTemp.Tables[0];
                dtMasterIssuer = dsTemp.Tables[1];
                dtMasterAcquirer = dsTemp.Tables[2];
            }
        }

        static void InitTerminalTable(ref DataSet _dsTemp, string sTerminalID)
        {
            string sQuery = string.Format(
                "SELECT TerminalId, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue " +
                "FROM tbProfileRelation WHERE TerminalID = '{0}' " +
                "SELECT TerminalID, IssuerName, IssuerTag, IssuerLengthOfTagLength, IssuerTagLength, IssuerTagValue " +
                "FROM tbProfileIssuer WHERE TerminalID = '{0}' ORDER BY IssuerName, IssuerTag " +
                "SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerLengthOfTagLength, AcquirerTagLength, AcquirerTagValue " +
                "FROM tbProfileAcquirer WHERE TerminalID = '{0}' ORDER BY AcquirerName, AcquirerTag " +
                "SELECT TerminalID, TerminalTag, TerminalLengthOfTagLength, TerminalTagLength, TerminalTagValue " +
                "FROM tbProfileTerminal WHERE TerminalID = '{0}' ORDER BY TerminalTag ", sTerminalID);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oCmd.CommandTimeout = 0;
                (new SqlDataAdapter(oCmd)).Fill(_dsTemp);
            }
        }
        #endregion

        #region "Function"
        //static DataTable dtGetIssuer(string sTerminalId, string sCondition)
        //{
        //    DataTable dtTemp = new DataTable();
        //    string sQuery = string.Format(
        //        "SELECT TerminalID, IssuerName, IssuerTag, IssuerLengthOfTagLength, IssuerTagLength, IssuerTagValue " +
        //        "FROM tbProfileIssuer " +
        //        "WHERE TerminalID = '{0}' {1}", sTerminalId, sCondition);
        //    using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
        //    {
        //        oCmd.CommandTimeout = 0;
        //        (new SqlDataAdapter(oCmd)).Fill(dtTemp);
        //    }
        //    return dtTemp;
        //}

        //static DataTable dtGetAcquirer(string sTerminalId, string sCondition)
        //{
        //    DataTable dtTemp = new DataTable();
        //    string sQuery = string.Format(
        //        "SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerLengthOfTagLength, AcquirerTagLength, AcquirerTagValue " +
        //        "FROM tbProfileAcquirer " +
        //        "WHERE TerminalID = '{0}' {1}", sTerminalId, sCondition);
        //    using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
        //    {
        //        oCmd.CommandTimeout = 0;
        //        (new SqlDataAdapter(oCmd)).Fill(dtTemp);
        //    }
        //    return dtTemp;
        //}

        static DataTable dtGetTerminal(string sTerminalId, string sCondition)
        {
            DataTable dtTemp = new DataTable();
            string sQuery = string.Format(
                "SELECT TerminalID, TerminalTag, TerminalTagValue " +
                "FROM tbProfileTerminal " +
                "WHERE TerminalID = '{0}' {1}", sTerminalId, sCondition);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oCmd.CommandTimeout = 0;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }

        static string sTerminalMaster(DataTable _dtAcquirerList, string _sCategory)
        {
            string sTemp = null;
            var exceptAcq = new List<string> { "PROMO F", "PROMO G", "PROMO H", "PROMO I", "PROMO J" };
            string[] arrAcq = (
                from acqList in _dtAcquirerList.AsEnumerable()
                select acqList.Field<string>("AcquirerName")
                ).Except(exceptAcq).ToArray();

            List<TerminalRelation> MasterFound = new List<TerminalRelation>();
            MasterFound = ltMasterRelation.FindAll(o => o.Category == _sCategory);

            TerminalRelation Master = new TerminalRelation();
            Master = MasterFound.Find(o => o.TotalAcquirer == arrAcq.Length && CompareArray(o.arrAcquirer, arrAcq));
            if (Master != null)
                sTemp = Master.TerminalId;
            return sTemp;
        }

        static bool CompareArray(string[] arrExpression, string[] arrSearchExpression)
        {
            bool isFound = false;
            foreach (string sTemp in arrSearchExpression)
            {
                if (!string.IsNullOrEmpty(sTemp) 
                    && sTemp != "PROMO F" && sTemp != "PROMO G" && sTemp != "PROMO H" && sTemp != "PROMO I" && sTemp != "PROMO J")
                {
                    if (Array.IndexOf(arrExpression, sTemp) < 0)
                    {
                        isFound = false;
                        break;
                    }
                    else
                        isFound = true;
                }
            }
            return isFound;
        }

        static void ShowWriteLog(string _sMessage)
        {
            string sMessage = string.Format("[{0:ddd, dd-MM-yyyy hh:mm:ss.fff tt]} {1}", DateTime.Now, _sMessage);
            Console.WriteLine(sMessage);
            string sDir = Environment.CurrentDirectory + @"\LOG";
            if(!Directory.Exists(sDir))
                Directory.CreateDirectory(sDir);
            using (StreamWriter sw = new StreamWriter(string.Format(@"{0}\Log_{1:yyyyMMdd}.log", sDir, DateTime.Now), true))
                sw.WriteLine(sMessage);
        }

        static bool isDeleteRelationSuccess(string _sTerminalId)
        {
            int iRowAffected = 0;
            using (SqlCommand oCmd = new SqlCommand("spProfileRelationDelete", oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalId;
                iRowAffected = oCmd.ExecuteNonQuery();
            }
            return iRowAffected > 0 ? true : false;
        }

        static void UserAccessInsert(SqlConnection _oSqlConn, string _sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand("spUserAccessInsert", _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = "GEMALTO";
                oCmd.ExecuteNonQuery();
            }
        }

        static void UserAccessDelete(SqlConnection _oSqlConn, string _sTerminalId)
        {
            if (!string.IsNullOrEmpty("GEMALTO"))
                using (SqlCommand oCmd = new SqlCommand("spUserAccessDelete", _oSqlConn))
                {
                    if (_oSqlConn.State != ConnectionState.Open)
                        _oSqlConn.Open();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = "GEMALTO";
                    oCmd.ExecuteNonQuery();
                }
        }

        static bool IsAllowUserAccess(SqlConnection _oSqlConn, string _sTerminalId, ref string sUserIdOnAccess)
        {
            bool isAllow = true;
            using (SqlCommand oCmd = new SqlCommand("spUserAccessBrowse", _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@sOutput", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();

                sUserIdOnAccess = oCmd.Parameters["@sUserId"].Value.ToString();
                isAllow = oCmd.Parameters["@sOutput"].Value.ToString() == "1" ? false : true;
            }
            return isAllow;
        }

        static void RestoreRelation(string sTerminalId, string sMaster)
        {
            DataTable dtRelation = new DataTable();
            dtRelation = dtMasterRelation.Clone();
            var relationProfile = dtMasterRelation.Select(
                string.Format("TerminalID = '{0}'", sMaster));
            relationProfile.CopyToDataTable(dtRelation, LoadOption.OverwriteChanges);
            DataColumn colTerminalId = new DataColumn("TerminalID");
            colTerminalId.DefaultValue = sTerminalId;
            dtRelation.Columns.Remove(colTerminalId.ColumnName);
            dtRelation.Columns.Add(colTerminalId);

            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            oSqlBulk.DestinationTableName = "tbProfileRelation";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapRelationTag = new SqlBulkCopyColumnMapping("RelationTag", "RelationTag");
            SqlBulkCopyColumnMapping mapRelationTagValue = new SqlBulkCopyColumnMapping("RelationTagValue", "RelationTagValue");
            SqlBulkCopyColumnMapping mapRelationTagLength = new SqlBulkCopyColumnMapping("RelationTagLength", "RelationTagLength");
            SqlBulkCopyColumnMapping mapRelationLengthOfTagLength = new SqlBulkCopyColumnMapping("RelationLengthOfTagLength", "RelationLengthOfTagLength");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapRelationTag);
            oSqlBulk.ColumnMappings.Add(mapRelationTagValue);
            oSqlBulk.ColumnMappings.Add(mapRelationTagLength);
            oSqlBulk.ColumnMappings.Add(mapRelationLengthOfTagLength);
            oSqlBulk.WriteToServer(dtRelation);
        }

        static void RestoreIssuer(string sTerminalId, string sMaster, DataTable dtIssuerProfile)
        {
            var issuerProfileDistinct = (
                from issuer in dtIssuerProfile.AsEnumerable()
                select issuer.Field<string>("IssuerName")).Distinct();
            var issuerMasterDistinct = (
                from relationMaster in dtMasterRelation.AsEnumerable().OrderBy(o => o.Field<string>("RelationTagValue"))
                where relationMaster.Field<string>("TerminalID") == sMaster
                && relationMaster.Field<string>("RelationTag") == "AD02"
                select relationMaster.Field<string>("RelationTagValue")).Distinct();

            string[] arrIssuerName = issuerProfileDistinct.ToArray();
            string[] arrIssuerNameMaster = issuerMasterDistinct.ToArray();
            if (!CompareArray(arrIssuerName, arrIssuerNameMaster))
            {
                List<string> ltIssuer = new List<string>();
                foreach (string sTemp in arrIssuerNameMaster)
                    if (!arrIssuerName.Contains(sTemp) && !string.IsNullOrEmpty(sTemp))
                        ltIssuer.Add(sTemp);
                if (ltIssuer.Count > 0)
                {
                    DataTable dtIssuerNew = (
                        from issMaster in dtMasterIssuer.AsEnumerable()
                        where issMaster.Field<string>("TerminalID") == sMaster && ltIssuer.Contains(issMaster.Field<string>("IssuerName"))
                        select issMaster
                        ).CopyToDataTable();

                    DataColumn colTerminalId = new DataColumn("TerminalID");
                    colTerminalId.DefaultValue = sTerminalId;
                    dtIssuerNew.Columns.Remove(colTerminalId.ColumnName);
                    dtIssuerNew.Columns.Add(colTerminalId);

                    SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
                    oSqlBulk.DestinationTableName = "tbProfileIssuer";
                    SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
                    SqlBulkCopyColumnMapping mapIssuerTag = new SqlBulkCopyColumnMapping("IssuerTag", "IssuerTag");
                    SqlBulkCopyColumnMapping mapIssuerName = new SqlBulkCopyColumnMapping("IssuerName", "IssuerName");
                    SqlBulkCopyColumnMapping mapIssuerTagValue = new SqlBulkCopyColumnMapping("IssuerTagValue", "IssuerTagValue");
                    SqlBulkCopyColumnMapping mapIssuerTagLength = new SqlBulkCopyColumnMapping("IssuerTagLength", "IssuerTagLength");
                    SqlBulkCopyColumnMapping mapIssuerLengthOfTagLength = new SqlBulkCopyColumnMapping("IssuerLengthOfTagLength", "IssuerLengthOfTagLength");
                    oSqlBulk.ColumnMappings.Add(mapTerminalId);
                    oSqlBulk.ColumnMappings.Add(mapIssuerName);
                    oSqlBulk.ColumnMappings.Add(mapIssuerTag);
                    oSqlBulk.ColumnMappings.Add(mapIssuerTagValue);
                    oSqlBulk.ColumnMappings.Add(mapIssuerTagLength);
                    oSqlBulk.ColumnMappings.Add(mapIssuerLengthOfTagLength);
                    oSqlBulk.WriteToServer(dtIssuerNew);
                }
            }
        }

        static List<string> GetListTerminalId()
        {
            List<string> _arrsTemp = new List<string>();
            string sFilename = Environment.CurrentDirectory + @"\Terminal.config";
            StreamReader sr = new StreamReader(sFilename);
            while (!sr.EndOfStream)
                _arrsTemp.Add(sr.ReadLine().ToUpper());
            sr.Close();
            return _arrsTemp;
        }

        static void RestoreAcquirerPromoSQ(string sTerminalId, string sMaster, DataTable _dtAcquirer)
        {
            var acqPromoSQ = new List<string> { "PROMO SQ A", "PROMO SQ B", "PROMO SQ C", "PROMO SQ D", "PROMO SQ E" };
            var masterPromoSQ = 
                from acqMaster in dtMasterAcquirer.AsEnumerable()
                where acqMaster.Field<string>("TerminalID") == sMaster && acqPromoSQ.Contains(acqMaster.Field<string>("AcquirerName"))
                select acqMaster;

            var acqPromo = new List<string> { "PROMO A", "PROMO B", "PROMO C", "PROMO D", "PROMO E" };
            var profilePromo =
                from acq in _dtAcquirer.AsEnumerable()
                where acq.Field<string>("TerminalID") == sTerminalId && acqPromo.Contains(acq.Field<string>("AcquirerName"))
                select acq;

            if (masterPromoSQ.Count() != 0)
            {
                DataTable dtPromoSQ = masterPromoSQ.CopyToDataTable();
                DataTable dtPromo = profilePromo.CopyToDataTable();

                string sQueryDeletePromoFJ = string.Format("DELETE FROM tbProfileAcquirer " +
                    "WHERE TerminalID='{0}' AND AcquirerName IN ('PROMO F', 'PROMO G', 'PROMO H', 'PROMO I', 'PROMO J')", sTerminalId);
                using (SqlCommand oCmd = new SqlCommand(sQueryDeletePromoFJ, oSqlConn))
                    oCmd.ExecuteNonQuery();
                DataColumn colTerminalId = new DataColumn("TerminalID");
                colTerminalId.DefaultValue = sTerminalId;
                dtPromoSQ.Columns.Remove(colTerminalId.ColumnName);
                dtPromoSQ.Columns.Add(colTerminalId);

                UpdateAcquirer(sTerminalId, sMaster, ref dtPromoSQ, dtPromo);
                string sQueryCheckSQ = string.Format("SELECT 1 FROM tbProfileAcquirer " +
                    "WHERE TerminalID='{0}' AND AcquirerName IN ('PROMO SQ A', 'PROMO SQ B', 'PROMO SQ C', 'PROMO SQ D', 'PROMO SQ E')", sTerminalId);
                using (SqlCommand oCmd = new SqlCommand(sQueryCheckSQ, oSqlConn))
                {
                    if (oCmd.ExecuteNonQuery() == 0)
                    {
                        SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
                        oSqlBulk.DestinationTableName = "tbProfileAcquirer";
                        SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
                        SqlBulkCopyColumnMapping mapAcquirerTag = new SqlBulkCopyColumnMapping("AcquirerTag", "AcquirerTag");
                        SqlBulkCopyColumnMapping mapAcquirerName = new SqlBulkCopyColumnMapping("AcquirerName", "AcquirerName");
                        SqlBulkCopyColumnMapping mapAcquirerTagValue = new SqlBulkCopyColumnMapping("AcquirerTagValue", "AcquirerTagValue");
                        SqlBulkCopyColumnMapping mapAcquirerTagLength = new SqlBulkCopyColumnMapping("AcquirerTagLength", "AcquirerTagLength");
                        SqlBulkCopyColumnMapping mapAcquirerLengthOfTagLength = new SqlBulkCopyColumnMapping("AcquirerLengthOfTagLength", "AcquirerLengthOfTagLength");
                        oSqlBulk.ColumnMappings.Add(mapTerminalId);
                        oSqlBulk.ColumnMappings.Add(mapAcquirerName);
                        oSqlBulk.ColumnMappings.Add(mapAcquirerTag);
                        oSqlBulk.ColumnMappings.Add(mapAcquirerTagValue);
                        oSqlBulk.ColumnMappings.Add(mapAcquirerTagLength);
                        oSqlBulk.ColumnMappings.Add(mapAcquirerLengthOfTagLength);
                        oSqlBulk.WriteToServer(dtPromoSQ);
                    }
                }
            }
        }

        static void UpdateAcquirer(string sTerminalId, string sMaster, ref DataTable _dtPromoSQ, DataTable _dtPromo)
        {
            string[] arrsAcqName = { "PROMO A", "PROMO B", "PROMO C", "PROMO D", "PROMO E" };
            foreach (string sAcqName in arrsAcqName)
            {
                string sAcqNameSQ = string.Format("PROMO SQ {0}", sAcqName.Substring(6));
                string sTag = "AA04";
                string sSelectPromoSQ = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqNameSQ, sTag);
                string sSelectPromo = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqName, sTag);
                DataRow rowUpdate = (_dtPromoSQ.Select(sSelectPromoSQ))[0];                
                _dtPromoSQ.Rows[_dtPromoSQ.Rows.IndexOf(rowUpdate)]["AcquirerTagValue"] =
                    (_dtPromo.Select(sSelectPromo))[0]["AcquirerTagValue"];

                sTag = "AA05";
                sSelectPromoSQ = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqNameSQ, sTag);
                sSelectPromo = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqName, sTag);
                rowUpdate = (_dtPromoSQ.Select(sSelectPromoSQ))[0];
                _dtPromoSQ.Rows[_dtPromoSQ.Rows.IndexOf(rowUpdate)]["AcquirerTagValue"] =
                    string.Format("Q{0}", (_dtPromo.Select(sSelectPromo))[0]["AcquirerTagValue"].ToString().Substring(1));

                sTag = "AA06";
                sSelectPromoSQ = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqNameSQ, sTag);
                sSelectPromo = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqName, sTag);
                rowUpdate = (_dtPromoSQ.Select(sSelectPromoSQ))[0];
                _dtPromoSQ.Rows[_dtPromoSQ.Rows.IndexOf(rowUpdate)]["AcquirerTagValue"] =
                    (_dtPromo.Select(sSelectPromo))[0]["AcquirerTagValue"];

                sTag = "AA07";
                sSelectPromoSQ = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqNameSQ, sTag);
                sSelectPromo = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqName, sTag);
                rowUpdate = (_dtPromoSQ.Select(sSelectPromoSQ))[0];
                _dtPromoSQ.Rows[_dtPromoSQ.Rows.IndexOf(rowUpdate)]["AcquirerTagValue"] =
                    (_dtPromo.Select(sSelectPromo))[0]["AcquirerTagValue"];

                sTag = "AA10";
                sSelectPromoSQ = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqNameSQ, sTag);
                sSelectPromo = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqName, sTag);
                rowUpdate = (_dtPromoSQ.Select(sSelectPromoSQ))[0];
                _dtPromoSQ.Rows[_dtPromoSQ.Rows.IndexOf(rowUpdate)]["AcquirerTagValue"] =
                    (_dtPromo.Select(sSelectPromo))[0]["AcquirerTagValue"];

                sTag = "AA11";
                sSelectPromoSQ = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqNameSQ, sTag);
                sSelectPromo = string.Format("AcquirerName='{0}' AND AcquirerTag='{1}'", sAcqName, sTag);
                rowUpdate = (_dtPromoSQ.Select(sSelectPromoSQ))[0];
                _dtPromoSQ.Rows[_dtPromoSQ.Rows.IndexOf(rowUpdate)]["AcquirerTagValue"] =
                    (_dtPromo.Select(sSelectPromo))[0]["AcquirerTagValue"];
            }
        }

        static bool IsBroken(DataTable _dtAcqList, DataTable _dtIssList, DataTable _dtRelation, string _sMaster)
        {
            if (_dtAcqList.Rows.Count > 0
                && _dtAcqList.Rows.Count == dtMasterAcquirer.Select(string.Format("TerminalID='{0}' AND AcquirerTag='AA01'", _sMaster)).Length)
                if (_dtIssList.Rows.Count > 0
                    && _dtIssList.Rows.Count == dtMasterIssuer.Select(string.Format("TerminalID='{0}' AND IssuerTag='AE01'", _sMaster)).Length)
                    if (IsRelationEquals((new DataView(_dtRelation.Select("RelationTag='AD03'").CopyToDataTable())).ToTable(true, "RelationTagValue"),
                        (new DataView(dtMasterRelation.Select("RelationTag='AD03'").CopyToDataTable())).ToTable(true, "RelationTagValue")))
                        if (IsRelationEquals((new DataView(_dtRelation.Select("RelationTag='AD02'").CopyToDataTable())).ToTable(true, "RelationTagValue"),
                        (new DataView(dtMasterRelation.Select("RelationTag='AD02'").CopyToDataTable())).ToTable(true, "RelationTagValue")))
                            return false;
            return true;
        }

        static bool IsRelationEquals(DataTable dtTable1, DataTable dtTable2)
        {
            if (dtTable1.Rows.Count == dtTable2.Rows.Count)
                if (dtTable1.Columns.Count == dtTable2.Columns.Count)
                {
                    foreach (DataRow row in dtTable1.Rows)
                        if (!string.IsNullOrEmpty(row[0].ToString()))
                            if (dtTable2.Select(string.Format("RelationTagValue='{0}'", row[0])).Length == 0)
                                return false;
                    return true;
                }
            return false;
        }
        #endregion
    }
}