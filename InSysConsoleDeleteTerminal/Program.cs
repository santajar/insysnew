﻿using Ini;
using InSysClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InSysConsoleDeleteTerminal
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;
        protected string sDirectory;

        public InitData(string _sDirectory)
        {
            sDirectory = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectory + "\\Application.config";
            string sEncryptFileName = sDirectory + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception)
                {
                    //Logs.doWriteErrorFile(sDirectory, ex.Message);
                }
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword);
        }

        public string sGetConnString(int iMaxPoolSize)
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword, iMaxPoolSize);
        }
        
    }

    class Program
    {
        protected static SqlConnection oSqlConn;
        protected static List<string> ltTerminalId = new List<string>();
        static string sUserID, sPwd;
        static string sMessage = null;
        static string sConnString;
        static string sDbTerminal = "";
        static string sAppDirectory = Directory.GetCurrentDirectory();
        static CultureInfo ciUSFormat = new CultureInfo("en-US"); // English - United States
        static string sTemp = "";
        static string sStatus = "0";
        static string sStatusNotOpen = "0";

        static void Main(string[] args)
        {
            #region
            sUserID = ConfigurationManager.AppSettings["User"].ToString();
            sPwd = ConfigurationManager.AppSettings["Pwd"].ToString();
            UserData.sUserID = sUserID;

            string sFileStatus = "status.txt";
            string sFile = Application.StartupPath + "\\" + sFileStatus;
            try
            {
                if (sFile != null)
                { File.Delete(sFile); Write2File(sFile, "0", true); }
                else Write2File(sFile, "0", true);
            }
            catch (Exception ex)
            {
                sTemp = sGetMMMddyyyyhhmmsstt() + " " + string.Format("{0} Delete File Failed, {1}", UserData.sUserID, sMessage + " " + sFileStatus + " " + ex.Message); // Set the content of the file
                Console.WriteLine(sTemp);
                CommonClass.doWritelogFile(string.Format("{0} Delete File Failed, {1}", UserData.sUserID, sMessage + " " + sFileStatus + " " + ex.Message));
            }

            string sTerminalBackup = "TerminalBackup";

            string path = ConfigurationManager.AppSettings["Path"].ToString();
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            string PathTerminalBackup = path + @"\\" + sTerminalBackup + "\\";
            if (!Directory.Exists(PathTerminalBackup))
                Directory.CreateDirectory(PathTerminalBackup);

            string sFileNotOpen = "StatusNotOpen.txt";
            string sFilePathNotOpen = Application.StartupPath + "\\" + sFileNotOpen;
            try
            {
                if (sFilePathNotOpen != null)
                { File.Delete(sFilePathNotOpen); Write2File(sFilePathNotOpen, "0", true); }
                else Write2File(sFilePathNotOpen, "0", true);
            }
            catch (Exception ex)
            {
                sTemp = sGetMMMddyyyyhhmmsstt() + " " + string.Format("{0} Delete File Failed, {1}", UserData.sUserID, sMessage + " " + sFilePathNotOpen + " " + ex.Message); // Set the content of the file
                Console.WriteLine(sTemp);
                CommonClass.doWritelogFile(string.Format("{0} Delete File Failed, {1}", UserData.sUserID, sMessage + " " + sFilePathNotOpen + " " + ex.Message));
            }
            #endregion

            InitStatusNotOpen();
            while (sStatusNotOpen == "0")
            {
                try
                {
                    oSqlConn = InitConnection();
                    if (oSqlConn.State == ConnectionState.Open)
                    {
                        File.Delete(sFileNotOpen);
                        Write2File(sFileNotOpen, "1", true);

                        #region
                        if (isValid())
                            if (!isLocked())
                            {
                                if (isCanLogin())
                                {
                                    InitStatus();
                                    while (sStatus == "0")
                                    {
                                        #region
                                        string[] asFilename;
                                        asFilename = null;
                                        asFilename = Directory.GetFiles(path, @"*.txt", SearchOption.TopDirectoryOnly);

                                        ltTerminalId = new List<string>();
                                        bool isEmpty = asFilename.Any();
                                        if (isEmpty)
                                        {

                                            File.Delete(sFile);
                                            Write2File(sFile, "1", true);
                                            foreach (string sFilename in asFilename)
                                            {
                                                if (!string.IsNullOrEmpty(sFilename))
                                                {
                                                    ltTerminalId.Clear();
                                                    InitTerminalID(sFilename);
                                                    string Filename = Path.GetFileName(sFilename);
                                                    string sUserIdOnAccess = null;

                                                    foreach (string sTerminalID in ltTerminalId)
                                                    {
                                                        try
                                                        {
                                                            if (IsValidTerminal(sTerminalID))
                                                            {
                                                                CommonClass.UserAccessDelete(oSqlConn, sTerminalID);
                                                                if (CommonClass.IsAllowUserAccess(oSqlConn, sTerminalID, ref sUserIdOnAccess))
                                                                {
                                                                    CommonClass.UserAccessInsert(oSqlConn, sTerminalID);

                                                                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                                                                    SqlTransaction oSqlTrans = oSqlConn.BeginTransaction();

                                                                    SqlCommand oSqlCommand = new SqlCommand();
                                                                    oSqlCommand = new SqlCommand(CommonSP.sSPProfileDelete, oSqlConn, oSqlTrans);
                                                                    sMessage = "Delete Terminal : |" + sTerminalID;
                                                                    oSqlCommand.CommandType = CommandType.StoredProcedure;
                                                                    oSqlCommand.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                                                                    oSqlCommand.Connection = oSqlConn;
                                                                    //oSqlCommand.CommandTimeout = 60;

                                                                    bool bSuccess = false;
                                                                    try
                                                                    {
                                                                        oSqlCommand.ExecuteNonQuery();
                                                                        oSqlTrans.Commit();
                                                                        sMessage = sMessage + "|SUCCESS|FROM CONSOLE|" + Filename;
                                                                        bSuccess = true;
                                                                        CommonClass.InputLog(oSqlConn, sTerminalID, UserData.sUserID, sDbTerminal, sMessage.Replace("\\", " \\ "), "");
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        sMessage = string.Format("Delete Terminal : |" + sTerminalID + "|Failed|" + Filename + ex.Message);
                                                                        oSqlConn = InitConnection();
                                                                        File.Delete(sFile);
                                                                        Write2File(sFile, "0", true);
                                                                        oSqlTrans.Rollback();

                                                                    }
                                                                    if (bSuccess == true)
                                                                    {
                                                                        try
                                                                        {
                                                                            SqlCommand oSqlCmd = new SqlCommand();
                                                                            oSqlCmd = new SqlCommand(CommonSP.sSPRemoveTIDGroupRegionCity, oSqlConn);
                                                                            oSqlCmd.CommandType = CommandType.StoredProcedure;
                                                                            oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                                                                            oSqlCmd.Parameters.Add("@sCategory", SqlDbType.VarChar).Value = (string)sTerminalID;

                                                                            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                                                                            oSqlCmd.ExecuteNonQuery();

                                                                            SqlCommand oSqlCmd2 = new SqlCommand();
                                                                            oSqlCmd2 = new SqlCommand(string.Format("DELETE FROM tbListTIDRemoteDownload WHERE TerminalID = '{0}'", sTerminalID), oSqlConn);
                                                                            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                                                                            oSqlCmd2.ExecuteNonQuery();
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            sMessage = "|" + sTerminalID + "|" + Filename + "|" + ex.Message;

                                                                            File.Delete(sFile);
                                                                            Write2File(sFile, "0", true);

                                                                            oSqlTrans.Rollback();
                                                                        }
                                                                    }
                                                                    CommonClass.UserAccessDelete(oSqlConn, sTerminalID);

                                                                }
                                                                else
                                                                {
                                                                    sMessage = string.Format("Delete Terminal : |" + sTerminalID + "|Failed|profile is being accessed by {0}", UserData.sUserID + "|" + Filename);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                sMessage = string.Format("Delete Terminal : |" + sTerminalID + "|Failed|Not Terminal|" + Filename);
                                                            }
                                                        }
                                                        catch (SqlException ex)
                                                        {
                                                            sMessage = string.Format("Delete Terminal : |" + sTerminalID + "|Failed|From File|" + Filename + " " + ex.Message);
                                                            oSqlConn = InitConnection();
                                                            CommonClass.UserAccessDelete(oSqlConn, sTerminalID);
                                                        }
                                                        sTemp = sGetMMMddyyyyhhmmsstt() + " " + sMessage; // Set the content of the file

                                                        Console.WriteLine(sTemp);
                                                        CommonClass.doWritelogFile(string.Format("{0}", sMessage));
                                                        
                                                    }


                                                    try
                                                    {
                                                        string pathfull;
                                                        pathfull = PathTerminalBackup + System.IO.Path.GetFileName(sFilename);
                                                        if (File.Exists(pathfull))
                                                        {
                                                            File.Delete(pathfull);
                                                        }
                                                        File.Copy(sFilename, PathTerminalBackup + System.IO.Path.GetFileName(sFilename));
                                                        File.Delete(sFilename);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        oSqlConn = InitConnection();
                                                        sTemp = sGetMMMddyyyyhhmmsstt() + " " + string.Format("Delete Terminal : |{0} txt Failed|, {1}", UserData.sUserID, " |From File|" + Filename + " " + ex.Message); // Set the content of the file
                                                        Console.WriteLine(sTemp);
                                                        CommonClass.doWritelogFile(string.Format("Delete Terminal : |{0} txt Failed|, {1}", UserData.sUserID, " |From File|" + Filename + " " + ex.Message));
                                                        File.Delete(sFile);
                                                        Write2File(sFile, "0", true);
                                                    }

                                                }// terminalid
                                            } // file name

                                            File.Delete(sFile);
                                            Write2File(sFile, "0", true);
                                        }
                                        Thread.Sleep(5000);
                                        InitStatus();
                                        #endregion
                                    } // end while

                                } // can login
                                else
                                {
                                    sMessage = string.Format("Delete Terminal : Cannot login {0} ",UserData.sUserID);
                                    sTemp = sGetMMMddyyyyhhmmsstt() + " " + sMessage; // Set the content of the file
                                    Console.WriteLine(sTemp);
                                    CommonClass.doWritelogFile(sMessage);
                                    Console.ReadKey();
                                }

                            }
                            else
                            {
                                sMessage = string.Format("Delete Terminal : {0} User not found" , UserData.sUserID);
                                sTemp = sGetMMMddyyyyhhmmsstt() + " " + sMessage; // Set the content of the file
                                Console.WriteLine(sTemp);
                                CommonClass.doWritelogFile(sMessage);
                                Console.ReadKey();
                            }

                        #endregion

                    }
                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn = InitConnection();
                        oSqlConn.Open();
                    }
                    File.Delete(sFileNotOpen);
                    Write2File(sFileNotOpen, "0", true);
                }
                catch (Exception ex)
                {
                    oSqlConn = InitConnection();
                    sTemp = sGetMMMddyyyyhhmmsstt() + " " + string.Format("{0} Delete Failed, {1}", UserData.sUserID, sMessage + " " + ex.Message); // Set the content of the file
                    Console.WriteLine(sTemp);
                    File.Delete(sFileNotOpen);
                    Write2File(sFileNotOpen, "0", true);

                    File.Delete(sFile);
                    Write2File(sFile, "0", true);
                }
                
                System.Threading.Thread.Sleep(50000);
                InitStatusNotOpen();
            } // end while status not open
        }

        protected static void InitStatus()
        {
            //string Contents = File.ReadAllText("Status.txt");
            string[] lineOfContents = File.ReadAllLines("Status.txt");
            foreach (var line in lineOfContents)
            {
                string[] tokens = line.Split(',');
                sStatus=(tokens[0]);
            }
        }
        protected static void InitStatusNotOpen()
        {
            //string Contents = File.ReadAllText("Status.txt");
            string[] lineOfContents = File.ReadAllLines("StatusNotOpen.txt");
            foreach (var line in lineOfContents)
            {
                string[] tokens = line.Split(',');
                sStatus = (tokens[0]);
            }
        }
        protected static void Write2File(string sFileName, string sValue, bool bNewLine)
        {
            try
            {
                using (FileStream oFileStream = new FileStream(sFileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter oStreamWriter = new StreamWriter(oFileStream))
                    {
                        oStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        if (bNewLine) oStreamWriter.WriteLine(sValue);
                        else oStreamWriter.Write(sValue);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected static string sGetMMMddyyyyhhmmsstt()
        {
            return DateTime.Now.ToString("MMM dd, yyyy, hh:mm:ss.fff tt ", ciUSFormat);
        }

        static bool isValid()
        {
            if (sUserID.Length == 0)
            {
                return false;
            }
            else
            if (sPwd.Length == 0)
            {
                //CommonClass.showErrorMessageTxt(txtPassword, CommonMessage.sErrPassword);
                return false;
            }
            return true;
        }
        static string sGetEncryptPassword()
        {
            return CommonClass.sGetEncrypt(sPwd);
        }


        /// <summary>
        /// Condition to limit UserID search based on UserID and Encrypted Password
        /// </summary>
        /// <returns>string : the filter string</returns>
        static string sCondition()
        {
            return " WHERE UserID='" + sUserID + "' AND Password='" + sGetEncryptPassword() + "'";
        }

        /// <summary>
        /// Browse the database if UserID requested if Valid
        /// </summary>
        /// <returns>boolean : true if UserID is found, else false</returns>
        static bool isUserIDFound()
        {
            bool isPass = false;
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        using (DataTable dtUser = new DataTable())
                        {
                            dtUser.Load(oRead);
                            if (dtUser.Rows.Count > 0) // UserID Found
                            {
                                isPass = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return isPass;
        }

        static bool isCanLogin()
        {
            if (isUserIDFound()) // If is User
            {
                return true;
            }
            else return false;
        }

        
        /// <summary>
        /// Check UserID locked or not
        /// </summary>
        /// <returns>True if locked</returns>
        static bool isLocked()
        {
            bool iLen = true;
            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.isUserIDLocked('{0}')", sUserID), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    iLen = bool.Parse(oRead[0].ToString());
                oRead.Close();
            }
            return iLen;
        }


        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                sConnString = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
            }

            return oSqlTempConn;
        }

        protected static bool IsValidTerminal(string sTerminalID)
        {
            string sDbid = "";
            using (SqlCommand cmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE TerminalID='{0}'", sTerminalID);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        sDbid = reader["DatabaseID"].ToString();
                        reader.Close();
                        sDbTerminal = GetDBTerminal(sDbid);
                        return true;
                    }
                    else
                    {
                        reader.Close();
                        sDbTerminal = "";
                        return false;
                    }
                }
            }
        }

        protected static string GetDBTerminal(string sDBID)
        {
            SqlCommand oSqlCmd4 = new SqlCommand();
            oSqlCmd4 = new SqlCommand(string.Format("select DatabaseName FROM tbProfileterminalDB WHERE DatabaseID = {0}", sDBID), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            using (SqlDataReader rdr = oSqlCmd4.ExecuteReader())
            {
                if (rdr.HasRows)
                { rdr.Read(); sDbTerminal = rdr["DatabaseName"].ToString(); rdr.Close(); }
                else { sDbTerminal = ""; rdr.Close(); }
            }
            return sDbTerminal;
        }

        protected static void InitTerminalID(string _sFilename)
        {
            using (StreamReader sr = new StreamReader(_sFilename))
            {
                string sTerminalID;
                while ((sTerminalID = sr.ReadLine()) != null)
                    ltTerminalId.Add(sTerminalID);
            }
        }

    }
}
