﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using InSysClass;
using System.Diagnostics;

namespace InSysConsoleNewZipBRI
{
    class Response : IDisposable
    {
        protected SqlConnection oConn;
        protected SqlConnection oConnAuditTrail;
        protected bool bCheckAllowInit;
        protected ConnType cType;

        protected string sTerminalId;
        protected string sAppName = null;
        protected string sSerialNum = null;
        protected string sLastInit = null;

        protected bool bInit = false;
        protected bool bStart = false;
        protected bool bComplete = false;
        protected int iPercentage = 0;
        protected int iMaxByte = 400;
        protected bool bEnable48 = false;
        protected bool bEnableCompress = false;
        protected bool bEnableRemoteDownload = false;
        protected bool bCustom48_1;

        //tambahan untuk bit 48 message = autoinit
        protected string sLastInitTime = null;
        protected string sPABX = null;
        protected string sOSVers = null;
        protected string sKernelEmvVers = null;
        protected string sReaderSN = null;
        protected string sPSAMSN = null;
        protected string sMCSN = null;
        protected string sMemUsg = null;
        protected string sICCID = null;

        public string TerminalId { get { return sTerminalId; } }
        public string AppName { get { return sAppName; } }
        public string SerialNum { get { return sSerialNum; } }
        public bool IsProcessInit { get { return bInit; } }
        public bool IsStartInit { get { return bStart; } }
        public bool IsCompleteInit { get { return bComplete; } }
        public int Percentage { get { return iPercentage; } }

        protected string sProcessCode = "930000";
        public string ProcessCode { get { return sProcessCode; } }
        protected static bool bDebug = true;

        protected byte[] arrbKey = (new UTF8Encoding()).GetBytes("316E67336E316330");
        protected byte[] arrbReceive;

        public Response(string _sConnString, string _sConnStringAuditTrail, bool _IsCheckAllowInit, ConnType _cType) : this(_sConnString, _sConnStringAuditTrail, _IsCheckAllowInit, _cType, false) { }
        public Response(string _sConnString, string _sConnStringAuditTrail, bool _IsCheckAllowInit, ConnType _cType, bool _bDebug) : this(null, _sConnString, _sConnStringAuditTrail, _IsCheckAllowInit, _cType, false) { }
        public Response(byte[] _arrbReceive, string _sConnString, string _sConnStringAuditTrail, bool _IsCheckAllowInit, ConnType _cType) : this(_arrbReceive, _sConnString, _sConnStringAuditTrail, _IsCheckAllowInit, _cType, false) { }
        public Response(byte[] _arrbReceive, string _sConnString, string _sConnStringAuditTrail, bool _IsCheckAllowInit, ConnType _cType, bool _bDebug)
        {
            try
            {
                //Console.WriteLine("Create Object Response2");
                oConn = new SqlConnection();
                oConn = SQLConnLib.EstablishConnection(_sConnString);
                oConnAuditTrail = new SqlConnection();
                oConnAuditTrail = SQLConnLib.EstablishConnection(_sConnStringAuditTrail);
                bCheckAllowInit = _IsCheckAllowInit;
                cType = _cType;
                iMaxByte = iGetMaxByte();
                bDebug = _bDebug;
                arrbReceive = _arrbReceive;
            }
            catch (Exception ex)
            {
                Trace.Write("Create Object Error: " + ex.StackTrace);
                //WriteLogtoDatabase("RESPONSE3", "Create Object Error: " + ex.Message);
                oConn.Close();
                oConn.Dispose();
                oConnAuditTrail.Close();
                oConnAuditTrail.Dispose();

            }
        }

        /// <summary>
        /// Close SQL Connection
        /// </summary>
        public void Dispose()
        {
            oConn.Close();
            oConn.Dispose();
            oConnAuditTrail.Close();
            oConnAuditTrail.Dispose();
        }

        public byte[] arrbResponse() { return arrbResponse(arrbReceive); }

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public byte[] arrbResponse(byte[] arrbReceive)
        {
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);
                //Console.WriteLine("Send Message: {0}", sSendMessage);

                if (!string.IsNullOrEmpty(sSendMessage))
                {
                    arrbTemp = new byte[iIsoLen + 2];
                    //arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);

                    sSendMessage = sSendMessage.Replace(" ", ""); // Remove all white space
                    byte[] arrbbuffer = new byte[sSendMessage.Length / 2];
                    for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                    {
                        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);
                    }
                    arrbTemp = arrbbuffer;
                }
            }
            catch (Exception ex)
            {
                Trace.Write("|[RESPONSE]|Error arrbResponse : " + ex.StackTrace);
                //WriteLogtoDatabase("RESPONSE3", "|[RESPONSE]|Error arrbResponse : " + ex.Message);
                //Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTag = null;
            int iErrorCode = -1;
            //Console.WriteLine("Begin Get Response");

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = CommonConsole.iISOLenReceive(sTemp.Substring(0, 4));

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            try
            {
                // parse received ISO
                ISO8583Lib oIsoParse = new ISO8583Lib();
                ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
                byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
                oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
                string sProcCode = oIsoMsg.sBit[3];
                sTerminalId = oIsoMsg.sBit[41];
                //penambahan untuk init by sn
                if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                    try
                    {
                        {
                            CommonConsole.GetAppNameSNLstInit(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum, ref sLastInit);
                            //CommonConsole.GetAutoInitMessage(oIsoMsg.sBit[48], ref sLastInitTime, ref sPABX, ref sAppName, ref sOSVers, ref sKernelEmvVers, ref sSerialNum, ref sReaderSN, ref sPSAMSN, ref sMCSN, ref sMemUsg, ref sICCID);
                            if (IsInitBySerialNumber(sAppName))
                            {
                                sGetTidBySN(sSerialNum, ref sTerminalId);
                            }
                            //else
                            if (sTerminalId.Length != 8)
                            {
                                sTerminalId = oIsoMsg.sBit[41];
                            }
                        }
                    }
                    catch
                    {
                        sTerminalId = oIsoMsg.sBit[41];
                    }

                //akhir penambahan


                // kirim ke sp,...
                try
                {
                    if (oIsoMsg.MTI == "0800")
                    {
                        ProcCode oProcCodeTemp = (ProcCode)int.Parse(sProcCode);
                        switch (oProcCodeTemp)
                        {
                            case ProcCode.TerminalEcho:
                                if (bDebug) Trace.Write("|[RESPONSE]|TerminalEcho " + sProcCode + " : " + sTemp);
                                #region "800000"
                                WriteLogTerminalSNEcho(oIsoMsg);
                                sTempReceive = CommonConsole.sGenerateISOSend(null, oIsoMsg, bComplete, false, true, false, false, null);
                                iErrorCode = 7;
                                #endregion
                                break;
                            case ProcCode.AutoCompletion:
                            case ProcCode.Flazz:
                                if (bDebug) Trace.Write("|[RESPONSE]|AutoCompletion-Flazz " + sProcCode + " : " + sTemp);
                                #region "NOT 930000 AND NOT 960000"
                                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                                {
                                    oCmd.CommandType = CommandType.StoredProcedure;
                                    oCmd.CommandTimeout = 25;
                                    oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                                    oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;
                                    if (oConn.State != ConnectionState.Open) oConn.Open();
                                    #region "Using DataReader"
                                    using (SqlDataReader reader = oCmd.ExecuteReader())
                                    {
                                        while (reader.Read())
                                        {
                                            iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                            sTerminalId = reader["TerminalId"].ToString();
                                            sTempReceive = reader["ISO"].ToString();
                                            sTag = reader["Tag"].ToString();
                                            string scont = reader["Content"].ToString();
                                        }
                                    }
                                    #endregion
                                }
                                //if (oProcCodeTemp == ProcCode.AutoCompletion) iErrorCode = 11; // init process init phase 3
                                if (oProcCodeTemp == ProcCode.AutoCompletion)  // init process init phase 3
                                {
                                    iErrorCode = 11;
                                    SendLogInitAudit(sTerminalId, sAppName, sSerialNum, "Auto Init Phase-3");
                                }
                                else iErrorCode = 12;
                                bInit = true;
                                #endregion
                                break;
                            case ProcCode.DownloadProfileStartEnd:
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    if (IsAllowRemoteDownload(sTerminalId))
                                    {
                                        int iIndex = -1;
                                        #region "980000"
                                        sProcessCode = sProcCode;
                                        if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                        {
                                            try
                                            {
                                                CommonConsole.GetAutoInitMessage(oIsoMsg.sBit[48], ref sLastInitTime, ref sPABX, ref sAppName, ref sOSVers, ref sKernelEmvVers, ref sSerialNum, ref sReaderSN, ref sPSAMSN, ref sMCSN, ref sMemUsg, ref sICCID);
                                                //WriteLogInit(sTerminalId, sLastInitTime, sPABX, sAppName, sOSVers, sKernelEmvVers, sSerialNum, sReaderSN, sPSAMSN, sMCSN, sMemUsg, sICCID);
                                                //WriteLogInitBRI(sTerminalId, sLastInit, "", sAppName, "", "", sSerialNum, "", "", "", "", "");
                                                bCustom48_1 = true;
                                            }
                                            catch (Exception)
                                            {
                                                try
                                                {
                                                    CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                                    WriteLogInitBRI(sTerminalId, sLastInit, "", sAppName, "", "", sSerialNum, "", "", "", "", "");
                                                }
                                                catch (Exception)
                                                {
                                                    sAppName = "";
                                                    sSerialNum = "";
                                                }
                                            }

                                            bEnable48 = true;
                                            bEnableCompress = IsAllowCompress(sTerminalId);
                                        }
                                        bEnableRemoteDownload = IsAllowRemoteDownload(sTerminalId);

                                        if (string.IsNullOrEmpty(sAppName) ||
                                            (!string.IsNullOrEmpty(sAppName) && IsApplicationAllowInit(sTerminalId, sAppName)))
                                        {
                                            iErrorCode = 8; //processing download profile after upgrade software
                                            if (bDebug) Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);
                                            bInit = true;
                                            string sContent = null;
                                            bool bHexContent = false;
                                            DataTable dtInitTemp = dtGetInitTemp(sTerminalId, ref bHexContent, oProcCodeTemp);
                                            if (dtInitTemp.Rows.Count > 0)
                                            {
                                                ClassInit oClass = new ClassInit(sTerminalId);
                                                oClass.dtResponses = dtInitTemp;
                                                oClass.SerialNumber = sSerialNum;
                                                oClass.AppName = sAppName;
                                                oClass.HexContent = bHexContent;
                                                oClass.Enable48 = bEnable48;
                                                oClass.EnableCompress = bEnableCompress;
                                                oClass.EnableRemoteDownload = bEnableRemoteDownload;
                                                oClass.Custom48_1 = bCustom48_1;

                                                iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                                                if (iIndex > -1) Program.ltClassInit.RemoveAt(iIndex);

                                                Program.ltClassInit.Add(oClass);
                                                sTag = oClass.Tag;
                                                sContent = oClass.Content;
                                                bComplete = oClass.IsLastPacket;
                                                oClass.UpdateFlag();
                                                iPercentage = oClass.Percentage;

                                                iErrorCode = bComplete ? 9 : 8;
                                                //if (sProcCode == "98000000") //baru ditambahkan 6 Juni 2015
                                                //    Trace.Write(string.Format("TerminalID '{0}' : {1}", sTerminalId, bComplete ? "COMPLETED Download Profile" : "STARTED Download Profile")); //baru diuncoment 6 Juni 2015

                                                if (bDebug) Trace.Write(string.Format("|[sContent]|{0}|sTempReceive {1}", sContent, sProcCode));
                                                //WriteLogtoDatabase("RESPONSE3", string.Format("|[sContent]|{0}|sTempReceive {1}", sContent, sProcCode));
                                                sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, bHexContent, bEnable48, bEnableCompress, bEnableRemoteDownload, sAppName);
                                                bStart = true;
                                            }
                                        }
                                        else
                                        {
                                            iErrorCode = 6;
                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("APPLICATION NOT ALLOWED"), oIsoMsg);
                                            Trace.Write(string.Format("Application Not Allow | TID: {0} | AppName : {1} | Bit41 : {2} Bit48 : {3}", sTerminalId, sAppName, oIsoMsg.sBit[41], oIsoMsg.sBit[48]));
                                            //WriteLogtoDatabase("RESPONSE3", string.Format("Application Not Allow | TID: {0} | AppName : {1} | Bit41 : {2} Bit48 : {3}", sTerminalId, sAppName, oIsoMsg.sBit[41], oIsoMsg.sBit[48]));
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("NOTALLOWED"), oIsoMsg);
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    Trace.Write(string.Format("INVALID TID: {0}", sTerminalId));
                                }
                                break;
                            case ProcCode.DownloadProfileCont:
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    if (IsAllowRemoteDownload(sTerminalId))
                                    {
                                        int iIndex = -1;
                                        #region "980001"
                                        sProcessCode = sProcCode;
                                        bInit = true;
                                        if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                        {
                                            try
                                            {
                                                CommonConsole.GetAutoInitMessage(oIsoMsg.sBit[48], ref sLastInitTime, ref sPABX, ref sAppName, ref sOSVers, ref sKernelEmvVers, ref sSerialNum, ref sReaderSN, ref sPSAMSN, ref sMCSN, ref sMemUsg, ref sICCID);
                                            }
                                            catch (Exception)
                                            {
                                                try
                                                {
                                                    CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                                }
                                                catch (Exception)
                                                {
                                                    sAppName = "";
                                                    sSerialNum = "";
                                                }
                                            }
                                        }

                                        iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);

                                        if (iIndex > -1)
                                        {
                                            bComplete = Program.ltClassInit[iIndex].IsLastPacket;
                                            iErrorCode = bComplete ? 9 : 8;
                                            sTag = Program.ltClassInit[iIndex].Tag;
                                            string sContent = Program.ltClassInit[iIndex].Content;
                                            bool bHexContent = Program.ltClassInit[iIndex].HexContent;
                                            //bool bHexContent = sTag == "PK" ? true : Program.ltClassInit[iIndex].HexContent;
                                            bEnable48 = Program.ltClassInit[iIndex].Enable48;
                                            bEnableCompress = Program.ltClassInit[iIndex].EnableCompress;

                                            if (!string.IsNullOrEmpty(Program.ltClassInit[iIndex].EnableRemoteDownload.ToString()))
                                                bEnableRemoteDownload = Program.ltClassInit[iIndex].EnableRemoteDownload;
                                            else
                                                bEnableRemoteDownload = false;

                                            sAppName = Program.ltClassInit[iIndex].AppName;
                                            sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, bHexContent, bEnable48, bEnableCompress, bEnableRemoteDownload, sAppName);
                                            Program.ltClassInit[iIndex].UpdateFlag();
                                            iPercentage = Program.ltClassInit[iIndex].Percentage;

                                            if (bComplete)
                                            {
                                                iErrorCode = 9;
                                                Program.ltClassInit.RemoveAt(iIndex);

                                                //CommonConsole.WriteToConsole(string.Format("{0} Download Profile COMPLETE", sTerminalId));
                                                if (bDebug) Trace.Write(string.Format("Total List : {0}", Program.ltClassInit.Count));
                                                //WriteLogtoDatabase("RESPONSE3", string.Format("Total List : {0}", Program.ltClassInit.Count));
                                            }
                                        }
                                        if (bComplete) Trace.Write(string.Format("TerminalID '{0}' : COMPLETED", sTerminalId));
                                        //WriteLogtoDatabase("RESPONSE3", string.Format("TerminalID '{0}' : COMPLETED", sTerminalId));
                                        #endregion
                                    }
                                    else
                                    {
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("NOTALLOWED"), oIsoMsg);
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    Trace.Write(string.Format("INVALID TID: {0}", sTerminalId));
                                }
                                break;
                                //Auto Init
                            case ProcCode.AutoPrompt:
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    try
                                    {
                                      //  CommonConsole.GetAutoInitMessage(oIsoMsg.sBit[48], ref sLastInitTime, ref sPABX, ref sAppName, ref sOSVers, ref sKernelEmvVers, ref sSerialNum, ref sReaderSN, ref sPSAMSN, ref sMCSN, ref sMemUsg, ref sICCID);
                                        //WriteLogInit(sTerminalId, sLastInitTime, sPABX, sAppName, sOSVers, sKernelEmvVers, sSerialNum, sReaderSN, sPSAMSN, sMCSN, sMemUsg, sICCID);
                                        WriteLogInitBRI(sTerminalId, sLastInit, "", sAppName, "", "", sSerialNum, "", "", "", "", "");
                                    }
                                    catch (Exception)
                                    {
                                        try
                                        {
                                            CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                        }
                                        catch (Exception)
                                        {
                                            sAppName = "";
                                            sSerialNum = "";
                                        }
                                    }
                                    if (bCheckAllowInit == false || (bCheckAllowInit && IsInitAllowed(sTerminalId)))
                                    {
                                        string sContent = null;
                                        #region "950000"
                                        sProcessCode = sProcCode;
                                        if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                        {
                                            try
                                            {
                                                //CommonConsole.GetAutoInitMessage(oIsoMsg.sBit[48], ref sLastInitTime, ref sPABX, ref sAppName, ref sOSVers, ref sKernelEmvVers, ref sSerialNum, ref sReaderSN, ref sPSAMSN, ref sMCSN, ref sMemUsg, ref sICCID);
                                                //WriteLogInit(sTerminalId, sLastInitTime, sPABX, sAppName, sOSVers, sKernelEmvVers, sSerialNum, sReaderSN, sPSAMSN, sMCSN, sMemUsg, sICCID);
                                                //WriteLogInitBRI(sTerminalId, sLastInit, "", sAppName, "", "", sSerialNum, "", "", "", "", "");
                                                bEnable48 = true;
                                                bEnableCompress = IsAllowCompress(sTerminalId);

                                                if (string.IsNullOrEmpty(sAppName) ||
                                                (!string.IsNullOrEmpty(sAppName) && IsApplicationAllowInit(sTerminalId, sAppName)))
                                                {
                                                    //panggil k SP dan Compress
                                                    sGetAutoInitContent(sTerminalId, ref sContent);
                                                    if (bEnableCompress == true)
                                                        sContent = CommonLib.sStringToHex(sContent);

                                                    iErrorCode = 10; //processing auto init phase ke 1
                                                    bEnableRemoteDownload = IsAllowRemoteDownload(sTerminalId);
                                                    SendLogInitAudit(sTerminalId, sAppName, sSerialNum, "Auto Init Phase-1");
                                                    bInit = true;
                                                }
                                                else
                                                {
                                                    iErrorCode = 6;
                                                    oIsoMsg.sBit[11] = "";
                                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("APPLICATION NOT ALLOWED"), oIsoMsg);
                                                }
                                            }
                                            catch (Exception)
                                            {
                                                iErrorCode = 6;
                                                oIsoMsg.sBit[11] = "";
                                                sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("APPLICATION NOT ALLOWED"), oIsoMsg);
                                            }
                                        }
                                        if (bDebug) Trace.Write(string.Format("|[sContent]|{0}|sTempReceive {1}", sContent, sProcCode));

                                        sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, bEnableCompress, false, bEnableCompress, bEnableRemoteDownload, sAppName);
                                        #endregion
                                    }
                                    else
                                    {
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("NOTALLOWED"), oIsoMsg);
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    Trace.Write(string.Format("INVALID TID: {0}", sTerminalId));
                                }
                                break;
                            case ProcCode.AutoProfileStartEnd:
                            case ProcCode.ProfileStartEnd:
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    try
                                    {
                                        CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                        //CommonConsole.GetAutoInitMessage(oIsoMsg.sBit[48], ref sLastInitTime, ref sPABX, ref sAppName, ref sOSVers, ref sKernelEmvVers, ref sSerialNum, ref sReaderSN, ref sPSAMSN, ref sMCSN, ref sMemUsg, ref sICCID);
                                        //WriteLogInit(sTerminalId, sLastInitTime, sPABX, sAppName, sOSVers, sKernelEmvVers, sSerialNum, sReaderSN, sPSAMSN, sMCSN, sMemUsg, sICCID);
                                        WriteLogInitBRI(sTerminalId, sLastInit, "", sAppName, "", "", sSerialNum, "", "", "", "", "");
                                        if (oProcCodeTemp == ProcCode.AutoProfileStartEnd)
                                        {
                                            SendLogInitAudit(sTerminalId, sAppName, sSerialNum, "Auto Init Phase-2");//
                                        }
                                        bCustom48_1 = true;
                                    }
                                    catch (Exception)
                                    {
                                        try
                                        {
                                            CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                        }
                                        catch (Exception)
                                        {
                                            sAppName = "error";
                                            sSerialNum = "error";
                                        }

                                    }

                                    if (bCheckAllowInit == false || (bCheckAllowInit && IsInitAllowed(sTerminalId)))
                                    {
                                        int iIndex = -1;
                                        string sContent = null;
                                        #region "930000 AND 960000"
                                        sProcessCode = sProcCode;
                                        if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                        {
                                            bEnable48 = true;
                                            bEnableCompress = IsAllowCompress(sTerminalId);
                                        }
                                        bEnableRemoteDownload = IsAllowRemoteDownload(sTerminalId);

                                        if (string.IsNullOrEmpty(sAppName) ||
                                            (!string.IsNullOrEmpty(sAppName) && IsApplicationAllowInit(sTerminalId, sAppName)))
                                        {
                                            iErrorCode = 0;
                                            if (bDebug) Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);
                                            //WriteLogtoDatabase("RESPONSE", "|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);
                                           //WriteLogInitBRI(sTerminalId, sLastInit, "", sAppName, "", "", sSerialNum, "", "", "", "", "");
                                            bInit = true;
                                            sContent = null;
                                            bool bHexContent = false;
                                            DataTable dtInitTemp = dtGetInitTemp(sTerminalId, ref bHexContent, oProcCodeTemp);
                                            if (dtInitTemp.Rows.Count > 0)
                                            {
                                                ClassInit oClass = new ClassInit(sTerminalId);
                                                oClass.dtResponses = dtInitTemp;
                                                oClass.SerialNumber = sSerialNum;
                                                oClass.AppName = sAppName;
                                                oClass.HexContent = bHexContent;
                                                oClass.Enable48 = bEnable48;
                                                oClass.EnableCompress = bEnableCompress;
                                                oClass.EnableRemoteDownload = bEnableRemoteDownload;
                                                oClass.Custom48_1 = bCustom48_1;

                                                iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                                                if (iIndex > -1) Program.ltClassInit.RemoveAt(iIndex);

                                                Program.ltClassInit.Add(oClass);
                                                sTag = oClass.Tag;
                                                sContent = oClass.Content;
                                                bComplete = oClass.IsLastPacket;
                                                oClass.UpdateFlag();
                                                iPercentage = oClass.Percentage;

                                                iErrorCode = bComplete ? 1 : 0;
                                                if (sProcCode == "960000")
                                                    Trace.Write(string.Format("|TerminalID '{0}' : {1}", sTerminalId, bComplete ? "COMPLETED Auto Init" : "STARTED Auto Init"));
                                                SendLogInitAudit( sTerminalId, sAppName, sSerialNum, "Auto Init Phase-2");//
                                                if (bDebug) Trace.Write(string.Format("|[sContent]|{0}|sTempReceive {1}", sContent, sProcCode));
                                                sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, bHexContent, bEnable48, bEnableCompress, bEnableRemoteDownload, sAppName);
                                                //Console.WriteLine(sTempReceive.ToString());
                                                bStart = true;
                                            }
                                        }
                                        else
                                        {
                                            iErrorCode = 6;
                                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("APPLICATION NOT ALLOWED"), oIsoMsg);
                                            Trace.Write(string.Format("Application Not Allow | TID: {0} | AppName : {1} | Bit41 : {2} Bit48 : {3}", sTerminalId, sAppName, oIsoMsg.sBit[41], oIsoMsg.sBit[48]));
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("NOTALLOWED"), oIsoMsg);
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    Trace.Write(string.Format("INVALID TID: {0}", sTerminalId));
                                }
                                break;
                            case ProcCode.ProfileCont:
                            case ProcCode.AutoProfileCont:
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    try
                                    {
                                        CommonConsole.GetAutoInitMessage(oIsoMsg.sBit[48], ref sLastInitTime, ref sPABX, ref sAppName, ref sOSVers, ref sKernelEmvVers, ref sSerialNum, ref sReaderSN, ref sPSAMSN, ref sMCSN, ref sMemUsg, ref sICCID);
                                    }
                                    catch (Exception)
                                    {
                                        try
                                        {
                                            CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                        }
                                        catch (Exception)
                                        {
                                            sAppName = "";
                                            sSerialNum = "";
                                        }
                                    }

                                    if (bCheckAllowInit == false || (bCheckAllowInit && IsInitAllowed(sTerminalId)))
                                    {
                                        int iIndex = -1;
                                        string sContent = null;
                                        #region "930001 AND 960001"
                                        sProcessCode = sProcCode;
                                        bInit = true;

                                        iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);

                                        if (iIndex > -1)
                                        {
                                            bComplete = Program.ltClassInit[iIndex].IsLastPacket;
                                            iErrorCode = bComplete ? 1 : 0;
                                            sTag = Program.ltClassInit[iIndex].Tag;
                                            sContent = Program.ltClassInit[iIndex].Content;
                                            bool bHexContent = Program.ltClassInit[iIndex].HexContent;
                                            //bool bHexContent = sTag == "PK" ? true : Program.ltClassInit[iIndex].HexContent;
                                            bEnable48 = Program.ltClassInit[iIndex].Enable48;
                                            bEnableCompress = Program.ltClassInit[iIndex].EnableCompress;

                                            if (!string.IsNullOrEmpty(Program.ltClassInit[iIndex].EnableRemoteDownload.ToString()))
                                                bEnableRemoteDownload = Program.ltClassInit[iIndex].EnableRemoteDownload;
                                            else
                                                bEnableRemoteDownload = false;

                                            sAppName = Program.ltClassInit[iIndex].AppName;
                                            sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, bHexContent, bEnable48, bEnableCompress, bEnableRemoteDownload, sAppName);
                                            Program.ltClassInit[iIndex].UpdateFlag();
                                            iPercentage = Program.ltClassInit[iIndex].Percentage;

                                            if (bComplete)
                                            {
                                                iErrorCode = 1;
                                                Program.ltClassInit.RemoveAt(iIndex);

                                                if (bDebug) Trace.Write(string.Format("Total List : {0}", Program.ltClassInit.Count));
                                            }
                                        }
                                        if (bComplete) Trace.Write(string.Format("TerminalID '{0}' : COMPLETED", sTerminalId));
                                        #endregion
                                    }
                                    else
                                    {
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("NOTALLOWED"), oIsoMsg);
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    Trace.Write(string.Format("INVALID TID: {0}", sTerminalId));
                                }
                                break;
                        }
                    }
                    else
                    {
                        iErrorCode = 5;
                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDMTI"), oIsoMsg);
                    }
                    if (!string.IsNullOrEmpty(sTempReceive))
                    {
                        // tambahkan total length
                        _iIsoLenResponse = sTempReceive.Length / 2;
                        sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                        if (bDebug) Trace.Write(string.Format("|[RESPONSE]|{0}|ErrorCode : {2}|sTempReceive : {1}", sTerminalId, sTempReceive, iErrorCode));

                        if (iErrorCode == 1 && sProcCode == "930001")
                        {
                            SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET AllowDownload = 0 WHERE TerminalID = '{0}'", sTerminalId), oConn);
                            if (oConn.State != ConnectionState.Open) oConn.Open();
                            oCmd.ExecuteNonQuery();
                        }
                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                    }
                }
                catch (Exception ex)
                {
                    Trace.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1} | sTerminalID : {2} | sProccode : {3}", ex.StackTrace, sTemp, sTerminalId, sProcCode));
                }
            }
            catch (Exception ex)
            {
                Trace.Write("Parsing Error: " + ex.StackTrace);
            }
            return sTempReceive;
        }

        /// <summary>
        /// Get Auto Init Content from Database
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <param name="sContent"></param>
        private void sGetAutoInitContent(string sTerminalId, ref string sContent)
        {
            sContent = "";
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAutoInitAllow, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                    SqlParameter sOutputContent = oCmd.Parameters.Add("@sContent", SqlDbType.VarChar, 65535);
                    sOutputContent.Direction = ParameterDirection.Output;
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    oCmd.ExecuteNonQuery();
                    sContent = (string)oCmd.Parameters["@sContent"].Value;
                }
            }
            catch (Exception ex)
            {
                Trace.Write(string.Format("Error spAutoInitAllow : {0}", ex.StackTrace));
            }
        }

        /// <summary>
        /// Check bit 48 using ustom or not
        /// </summary>
        /// <param name="sTerminalId">string : TerminalID</param>
        /// <returns>Bool : true if bit 48 Custom</returns>
        protected bool IsCustom48(string sTerminalId)
        {
            bool isAllow = false;
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPBitmapBrowseCustom48, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@iCustom48", SqlDbType.Bit).Direction = ParameterDirection.Output;
                if (oConn.State != ConnectionState.Open) oConn.Open();
                oCmd.ExecuteNonQuery();
                isAllow = bool.Parse(oCmd.Parameters["@iCustom48"].Value.ToString());
            }
            return isAllow;
        }

        /// <summary>
        /// Get AppName init by serial number
        /// </summary>
        /// <param name="sAppName"></param>
        /// <returns></returns>
         protected bool IsInitBySerialNumber(string sAppName)
        {
            bool isAllow = false;
            using (SqlCommand oCmd = new SqlCommand(CommonSP.spBrowseInitBySerialNumber, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sAppName", SqlDbType.VarChar).Value = sAppName;
                oCmd.Parameters.Add("@iInitBySerialNumber", SqlDbType.Bit).Direction = ParameterDirection.Output;
                if (oConn.State != ConnectionState.Open) oConn.Open();
                oCmd.ExecuteNonQuery();
                isAllow = bool.Parse(oCmd.Parameters["@iInitBySerialNumber"].Value.ToString());
            }
            return isAllow;
        }

        /// <summary>
        /// get terminalID by Serial number
        /// </summary>
        /// <param name="sSn"></param>
        /// <param name="sTid"></param>
         protected void sGetTidBySN(string sSn, ref string sTid)
         {
             using (SqlCommand oCmd = new SqlCommand(CommonSP.spGetTidBySerialNumber, oConn))
             {
                 oCmd.CommandType = CommandType.StoredProcedure;
                 oCmd.Parameters.Add("@sSN", SqlDbType.VarChar).Value = sSn;
                 oCmd.Parameters.Add("@sTerminalID", SqlDbType.NVarChar, 8).Direction = ParameterDirection.Output;
                 if (oConn.State != ConnectionState.Open) oConn.Open();
                 oCmd.ExecuteNonQuery();
                 sTid = oCmd.Parameters["@sTerminalID"].Value.ToString();
                
             }
         }

        /// <summary>
        /// Write Auto init Log to database
        /// </summary>
        private void WriteLogInit(string sTerminalId, string sLastInitTime, string sPABX, string sAppName, string sOSVers, string sKernelEmvVers, string sSerialNum, string sReaderSN, string sPSAMSN, string sMCSN, string sMemUsg, string sICCID)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitUpdate, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@sLastInitTime", SqlDbType.VarChar).Value = sLastInitTime;
                oCmd.Parameters.Add("@sPABX", SqlDbType.VarChar).Value = sPABX;
                oCmd.Parameters.Add("@sSoftwareVers", SqlDbType.VarChar).Value = sAppName;
                oCmd.Parameters.Add("@sOSVers", SqlDbType.VarChar).Value = sOSVers;
                oCmd.Parameters.Add("@sKernelEmvVers", SqlDbType.VarChar).Value = "";
                oCmd.Parameters.Add("@sEdcSN", SqlDbType.VarChar).Value = sSerialNum;
                oCmd.Parameters.Add("@sReaderSN", SqlDbType.VarChar).Value = "";
                oCmd.Parameters.Add("@sPSAMSN", SqlDbType.VarChar).Value = "";
                oCmd.Parameters.Add("@sMCSN", SqlDbType.VarChar).Value = "";
                oCmd.Parameters.Add("@sMemUsg", SqlDbType.VarChar).Value = "";
                oCmd.Parameters.Add("@sICCID", SqlDbType.VarChar).Value = "";
                if (oConn.State != ConnectionState.Open) oConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }
        private void WriteLogInitBRI(string sTerminalId, string sLastInitTime, string sPABX, string sAppName, string sOSVers, string sKernelEmvVers, string sSerialNum, string sReaderSN, string sPSAMSN, string sMCSN, string sMemUsg, string sICCID)
        {
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitUpdate, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                    oCmd.Parameters.Add("@sLastInitTime", SqlDbType.VarChar).Value = sLastInitTime;
                    oCmd.Parameters.Add("@sPABX", SqlDbType.VarChar).Value = "";
                    oCmd.Parameters.Add("@sSoftwareVers", SqlDbType.VarChar).Value = sAppName;
                    oCmd.Parameters.Add("@sOSVers", SqlDbType.VarChar).Value = "";
                    oCmd.Parameters.Add("@sKernelEmvVers", SqlDbType.VarChar).Value = "";
                    oCmd.Parameters.Add("@sEdcSN", SqlDbType.VarChar).Value = sSerialNum;
                    oCmd.Parameters.Add("@sReaderSN", SqlDbType.VarChar).Value = "";
                    oCmd.Parameters.Add("@sPSAMSN", SqlDbType.VarChar).Value = "";
                    oCmd.Parameters.Add("@sMCSN", SqlDbType.VarChar).Value = "";
                    oCmd.Parameters.Add("@sMemUsg", SqlDbType.VarChar).Value = "";
                    oCmd.Parameters.Add("@sICCID", SqlDbType.VarChar).Value = "";
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    oCmd.ExecuteNonQuery();
                }
            }
            catch
            {
 
            }
        }
        /// <summary>
        /// Write Lof Terminal SN Echo to database
        /// </summary>
        private void WriteLogTerminalSNEcho(ISO8583_MSGLib oIsoMsg)
        {
            string[] arrsBit48Values = CommonConsole.arrsGetBit48Values(oIsoMsg.sBit[48]);
            if (arrsBit48Values != null)
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAuditTerminalSNEchoInsert, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalSN", SqlDbType.VarChar).Value = arrsBit48Values[0];
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = oIsoMsg.sBit[41];
                    oCmd.Parameters.Add("@sAppName", SqlDbType.VarChar).Value = arrsBit48Values[1];
                    oCmd.Parameters.Add("@iCommsID", SqlDbType.Int).Value = int.Parse(arrsBit48Values[2]);
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    oCmd.ExecuteNonQuery();
                }
        }

        protected string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP || cType == ConnType.Modem)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            //sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Initialize Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Initialize Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                case 6:
                    sTemp = "Application Not Allow";
                    break;
                case 7:
                    sTemp = "Processing Echo";
                    break;
                case 8:
                    sTemp = "Processing Download " + sDetailProcess(sTag);
                    break;
                case 9:
                    sTemp = "Download Profile COMPLETE";
                    break;
                case 10:
                    sTemp = "Processing Autoinit Phase-1";
                    break;
                case 11:
                    sTemp = "Processing Autoinit Phase-3";
                    break;
                case 12:
                    sTemp = "Processing Init FLAZZ";
                    break;
                default:
                    sTemp = "Unknown Error Code : " + iCode.ToString();
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
                case "AI":
                    sTemp = "AID";
                    break;
                case "PK":
                    sTemp = "CAPK";
                    break;
                case "PP":
                    sTemp = "PinPad";
                    break;
                case "GP":
                    sTemp = "GPRS";
                    break;
                case "MN":
                    sTemp = "Custom Menu";
                    break;
                default:
                    sTemp = "";
                    break;
            }
            return sTemp;
        }

        /// <summary>
        /// Get List Content Profile from database
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <param name="_sContent">string : Content</param>
        /// <returns>List Content</returns>
        protected List<ContentProfile> ltGetContentProfile(string _sTerminalId, ref string _sContent, ProcCode oProcCodeTemp)
        {
            List<ContentProfile> ltCPTemp = new List<ContentProfile>();
            string sQuery;
            if (oProcCodeTemp == ProcCode.DownloadProfileStartEnd)
                sQuery = CommonSP.sSPProfileTextFullTableDLProfile;
            else
                sQuery = CommonSP.sSPProfileTextFullTable;
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);

                string[] arrsMaskedTag = arrsGetMaskedTag(_sTerminalId);

                if (dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow rowTemp in dtTemp.Rows)
                    {
                        string sTag = rowTemp["Tag"].ToString();
                        string sValue = rowTemp["TagValue"].ToString();
                        try
                        {
                            if (arrsMaskedTag.Length > 0 && arrsMaskedTag.Contains(sTag))
                                sValue = EncryptionLib.Decrypt3DES(CommonLib.sHexToStringUTF8(sValue), arrbKey);

                        }
                        catch (Exception)
                        {
                            sValue = rowTemp["TagValue"].ToString();
                        }

                        string sContent;
                        if (sTag != "AE37" && sTag != "AE38" && sTag != "TL11" && sTag.Substring(0, 2) != "PK")
                            sContent = string.Format("{0}{1:00}{2}", sTag, sValue.Length, sValue);
                        else
                        {
                            if (sTag == "AE37" || sTag == "AE38")
                                sContent = string.Format("{0}{1:000}{2}", sTag, sValue.Length, sValue);
                            else if (sTag == "PK04" && sTag == "PK06")
                                sContent = string.Format("{0}{1}{2}",
                                    CommonLib.sStringToHex(sTag),
                                    CommonLib.sStringToHex(string.Format("{0:000}", sValue.Length)),
                                    CommonLib.sStringToHex(sValue));
                            else
                                sContent = string.Format("{0}{1}{2}",
                                    sTag,
                                    string.Format("{0:000}", rowTemp["TagLength"]),
                                    sValue);
                            //sContent = string.Format("{0}{1}{2}",
                            //        CommonLib.sStringToHex(sTag),
                            //        CommonLib.sStringToHex(string.Format("{0:000}", rowTemp["TagLength"])),
                            //        sValue);
                        }
                        _sContent += sContent;
                        ContentProfile oCPTemp = new ContentProfile(sTag.Substring(0, 2), sContent);
                        int iIndexSearch = -1;
                        if (ltCPTemp.Count == 0)
                            ltCPTemp.Add(oCPTemp);
                        else
                            if ((iIndexSearch = ltCPTemp.FindIndex(oCPSearch => oCPSearch.Tag == oCPTemp.Tag)) > -1)
                            {
                                ContentProfile oCPSearchResult = ltCPTemp[iIndexSearch];
                                ContentProfile oCPNew = new ContentProfile(oCPTemp.Tag, oCPSearchResult.Content + oCPTemp.Content);
                                ltCPTemp[iIndexSearch] = oCPNew;
                            }
                            else
                                ltCPTemp.Add(oCPTemp);
                    }
                }
            }
            return ltCPTemp;
        }


        protected List<ContentProfile>ltGetContentFooter(ref string _sContent, ProcCode oProcCodeTemp)
        {
            List<ContentProfile> ltCPTemp = new List<ContentProfile>();
            string sQuery = CommonSP.sSPProfileTextFullTableFooter;
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                //oCmd.Parameters.Add("@DatabaseID", SqlDbType.VarChar).Value = Convert.ToInt32(sDbId);
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);

                if (dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow rowTemp in dtTemp.Rows)
                    {
                        string sTag = rowTemp["FooterTag"].ToString();
                        string sValue = rowTemp["FooterTagValue"].ToString();
                        string sContent = string.Format("{0}{1:00}{2}", sTag, sValue.Length, sValue);
                        _sContent += sContent;
                        ContentProfile oCPTemp = new ContentProfile(sTag.Substring(0, 2), sContent);
                        int iIndexSearch = -1;

                        if (ltCPTemp.Count == 0)
                            ltCPTemp.Add(oCPTemp);
                        else
                            if ((iIndexSearch = ltCPTemp.FindIndex(oCPSearch => oCPSearch.Tag == oCPTemp.Tag)) > -1)
                            {
                                ContentProfile oCPSearchResult = ltCPTemp[iIndexSearch];
                                ContentProfile oCPNew = new ContentProfile(oCPTemp.Tag, oCPSearchResult.Content + oCPTemp.Content);
                                ltCPTemp[iIndexSearch] = oCPNew;
                            }
                            else
                                ltCPTemp.Add(oCPTemp);
                    }
                }
            }
            return ltCPTemp;
        }


        /// <summary>
        /// Check TerminalID is masking or not
        /// </summary>
        /// <param name="_sTerminalId"> string : Terminal ID</param>
        /// <returns>List Masking Tag</returns>
        protected string[] arrsGetMaskedTag(string _sTerminalId)
        {
            List<string> ltTag = new List<string>();
            string sQuery = string.Format("SELECT * FROM tbItemList WHERE DatabaseID IN (SELECT DatabaseID FROM tbProfileTerminalList WHERE TerminalID='{0}') AND vMasking=1", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtTemp);
                if (dtTemp != null && dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow row in dtTemp.Rows)
                        ltTag.Add(row["Tag"].ToString());
                }
            }
            return ltTag.ToArray();
        }

        /// <summary>
        /// Create File Compress
        /// </summary>
        /// <param name="sFilename">string : File Name</param>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>Bool : true if Success</returns>
        protected bool IsNetCompressSuccess(string sFilename, string sTerminalId)
        {
            bool bReturn = false;
            if (File.Exists(sFilename))
            {
                try
                {
                    FileInfo fi = new FileInfo(sFilename);
                    using (FileStream inFile = fi.OpenRead())
                    {
                        string sGzipFile = sFilename + ".gz";
                        if (File.Exists(sGzipFile))
                            File.Delete(sGzipFile);
                        using (FileStream outFile = File.Create(sFilename + ".gz"))
                        {
                            using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                            {
                                byte[] buffer = new byte[fi.Length];
                                inFile.Read(buffer, 0, Convert.ToInt32(fi.Length));
                                compress.Write(buffer, 0, Convert.ToInt32(fi.Length));
                                bReturn = true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    //MessageBox.Show("NetCompress " + ex.Message);
                }
            }
            return bReturn;
        }

        /// <summary>
        /// Check Allow Compress
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : true if init Compress</returns>
        protected bool IsAllowCompress(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsCompressInit('{0}')", sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        /// <summary>
        /// Check Allow Remote Download
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : True is allow remote Download</returns>
        protected bool IsAllowRemoteDownload(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsRemoteDownloadAllowed('{0}')", sTerminalId);
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                {
                    DataTable dtTemp = new DataTable();
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    dtTemp.Load(oCmd.ExecuteReader());
                    if (dtTemp.Rows.Count > 0)
                        isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                }
            }
            catch (Exception)
            {
            }
            return isAllow;
        }

        /// <summary>
        /// Get Max Byte from Database
        /// </summary>
        /// <returns>int : Max Byte</returns>
        protected int iGetMaxByte()
        {
            int iMaxLength = 400;
            string sQuery = string.Format("SELECT dbo.iPacketLength() [MaxByte]");
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                if (oConn.State != ConnectionState.Open) oConn.Open();
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        iMaxLength = int.Parse(oRead["MaxByte"].ToString());
                }
            }
            return iMaxLength;
        }

        /// <summary>
        /// Create Temporary Table for Init Data
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <param name="isCompress">bool: True if Compress</param>
        /// <returns>Data Table </returns>
        protected DataTable dtGetInitTemp(string _sTerminalId, ref bool isCompress, ProcCode oProcCodeTemp)
        {
            DataTable dtInitTemp = new DataTable();
            dtInitTemp.Columns.Add("TerminalId", typeof(string));
            dtInitTemp.Columns.Add("Content", typeof(string));
            dtInitTemp.Columns.Add("Tag", typeof(string));
            dtInitTemp.Columns.Add("Flag", typeof(bool));

            string sContent = null;
            List<ContentProfile> _ltCPTemp = ltGetContentProfile(_sTerminalId, ref sContent, oProcCodeTemp);
            if (_ltCPTemp != null && _ltCPTemp.Count > 0)
            {
                isCompress = bEnableCompress;
                string sTag;
                int iOffset = 0;
                int iFlag = 0;
                if (bEnableCompress)
                {
                    #region "GZ part"
                    sTag = "GZ";
                    iOffset = 0;
                    string sContentCompressed = null;
                    // write to file locally and compress
                    if (!Directory.Exists(Environment.CurrentDirectory + @"\ZIP"))
                        Directory.CreateDirectory(Environment.CurrentDirectory + @"\ZIP");
                    string sFilename = Environment.CurrentDirectory + @"\ZIP\" + _sTerminalId;

                    if (File.Exists(sFilename))
                        File.Delete(sFilename);
                    CommonLib.Write2File(sFilename, sContent, false);

                    if (IsNetCompressSuccess(sFilename, _sTerminalId))
                    {
                        FileStream fsRead = new FileStream(sFilename + ".gz", FileMode.Open);
                        byte[] arrbContent = new byte[fsRead.Length];
                        fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
                        fsRead.Close();
                        sContentCompressed = CommonLib.sByteArrayToHexString(arrbContent).Replace(" ", "");
                        while (!string.IsNullOrEmpty(sContentCompressed) && iOffset < sContentCompressed.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            rows["Content"] = sContentCompressed.Substring(iOffset,
                                iOffset + (iMaxByte * 2) <= sContentCompressed.Length ?
                                (iMaxByte * 2) :
                                sContentCompressed.Length - iOffset);
                            dtInitTemp.Rows.Add(rows);
                            iOffset += (iMaxByte * 2);
                        }
                    }
                    #endregion
                }
                else
                {
                    iFlag = 0;
                    iOffset = 0;
                    #region "non-GZ part"
                    foreach (ContentProfile oCPTemp in _ltCPTemp)
                    {
                        sTag = oCPTemp.Tag;
                        sContent = oCPTemp.Content;
                        iOffset = 0;
                        //while (iOffset <= sContent.Length), error, row menjadi null ketika value nya kosong.
                        while (iOffset < sContent.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            int iOffsetHeader = sContent.IndexOf(sTag + "01", iOffset + 1);
                            int iLength = 0;

                            if (sTag != "AI" && sTag != "PK")
                            {
                                if (iOffset + iMaxByte <= sContent.Length)
                                    if (iOffsetHeader > 0)
                                        if (iOffset + iMaxByte <= iOffsetHeader)
                                            iLength = iMaxByte;
                                        else
                                            iLength = iOffsetHeader - iOffset;
                                    else
                                        iLength = iMaxByte;
                                else
                                    iLength = sContent.Length - iOffset;
                            }
                            else
                            {
                                if (iOffset + iMaxByte <= sContent.Length || iOffsetHeader > 0)
                                    if (iOffsetHeader > 0)
                                        if (iOffset + iMaxByte <= iOffsetHeader)
                                            iLength = iMaxByte;
                                        else
                                            iLength = iOffsetHeader - iOffset;
                                    else
                                        iLength = iMaxByte;
                                else
                                    iLength = sContent.Length - iOffset;
                            }

                            rows["Content"] = sContent.Substring(iOffset, iLength);

                            iOffset += iLength;
                            dtInitTemp.Rows.Add(rows);
                        }
                    }
                    #endregion
                }
            }
            return dtInitTemp;
        }

        /// <summary>
        /// Determine Application Allow Init
        /// </summary>
        /// <param name="_sTerminalId"> string : Terminal ID</param>
        /// <param name="_sAppName">string : Application Name</param>
        /// <returns>bool: true if Allow Init</returns>
        protected bool IsApplicationAllowInit(string _sTerminalId, string _sAppName)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsApplicationAllowInit('{0}','{1}')", _sTerminalId, _sAppName);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        /// <summary>
        /// Determine Init Allow
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <returns>bool: true is allow</returns>        
        protected bool IsInitAllowed(string _sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsInitAllowed('{0}')", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        //BEGIN
        //string sTIDRemoteDownload;
        private Boolean CheckTIDRemoteDownload(string sTIDRemoteDownload)
        {
            sTIDRemoteDownload = sTIDRemoteDownload.ToString();
            oConn.Open();
            //string sql = "select TerminalID from tbListTIDRemoteDownload WHere TerminalID";
            SqlCommand command = new SqlCommand("spTIDRemoteDownload", oConn);
            command.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
            //SqlCommand command = new SqlCommand(sql, oConn);
            if (oConn.State != ConnectionState.Open) oConn.Open();
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sTIDRemoteDownload)
                {
                    reader.Close();
                    return true;
                }
            }
            oConn.Close();
            reader.Close();
            return false;
        }

        //LAST
        /// <summary>
        /// Determine Valid Terminal ID
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <returns>Bool : true if valid</returns>
        protected bool IsValidTerminalID(string _sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsValidTerminalID('{0}')", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                if (oConn.State != ConnectionState.Open) oConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }
        
        public  void SendLogInitAudit(string sTerminalId, string sAppName, string sSerialNum, string sStatus)
        {
            try
            {
                if (oConn.State != ConnectionState.Open) oConn.Open();
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAuditInitInsert, oConn))
                {
                    //if (oConn.State != ConnectionState.Open) oConn.Open();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 60;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                    oCmd.Parameters.Add("@sSoftware", SqlDbType.VarChar).Value = string.IsNullOrEmpty(sAppName) ? "" : sAppName;
                    oCmd.Parameters.Add("@sSerialNum", SqlDbType.VarChar).Value = string.IsNullOrEmpty(sSerialNum) ? "" : sSerialNum;
                    oCmd.Parameters.Add("@sStatus", SqlDbType.VarChar).Value = sStatus;
                    oCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Trace.Write("|ERROR|SendLogInitAudit|" + ex.StackTrace);
                //WriteLogtoDatabase("Error", string.Format("|ERROR|SendLogInitAudit| {0}", ex.ToString()));
            }

        }
    }
}