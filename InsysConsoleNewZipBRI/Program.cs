﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using InSysClass;

namespace InSysConsoleNewZipBRI
{
    class Program
    {
        /*
         * The changes on this program will affect only on profile initialization
         * as soon as the console receive message, it will be parse(on console app side, not sql side)
         * and then check the proccode, if init proccode 930000 request inittable
         * if proccode 930001 search existing
         */
        static bool bCheckAllowInit;
        static SqlConnection oConn = new SqlConnection();
        static SqlConnection oConnAuditTrail = new SqlConnection();
        static string sConnString;
        static string sConnStringAuditTrail;
        static string sAppDirectory = Directory.GetCurrentDirectory();

        public static List<ClassInit> ltClassInit = new List<ClassInit>();
        public static DataTable dtISO8583 = new DataTable();
        static bool bDebug = true;

        #region "TCP/IP Connection"
        static bool IsConnTcpIpActive;
        static Thread threadIP;
        #endregion

        #region "Dial-Up Modem Connection"
        static bool IsConnModemActive;

        static string sModemPort1;
        static string sModemPort2;
        static string sModemPort3;
        static string sModemPort4;
        static string sModemPort5;

        //static SerialModem oSerModem1;
        //static SerialModem oSerModem2;
        //static SerialModem oSerModem3;
        //static SerialModem oSerModem4;
        //static SerialModem oSerModem5;
        #endregion

        /// <summary>
        /// Initialize Component
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                bDebug = bool.Parse(ConfigurationManager.AppSettings["Debug"].ToString());
                AppDomain.CurrentDomain.ProcessExit += new EventHandler(ProcessExit);

                oConn = InitConnection();
                oConnAuditTrail = InitConnectionAuditTrail();

                if (oConn != null && oConn.State == ConnectionState.Open && oConnAuditTrail != null && oConnAuditTrail.State == ConnectionState.Open)
                {
                    InitTableSoftwareISO();
                    InitControlAllowInit();
                    AsyncSocket.oConn = oConn;
                    AsyncSocket.oConnAuditTrail = oConnAuditTrail;
                    AsyncSocket.bCheckAllowInit = bCheckAllowInit;
                    AsyncSocket.sConnString = sConnString;
                    AsyncSocket.sConnStringAuditTrail = sConnStringAuditTrail;
                    AsyncSocket.bDebug = bDebug;
                    AsyncSocket.iMaxBytePerPacket = int.Parse(ConfigurationManager.AppSettings["MaxBytePerPacket"].ToString());

                    //SerialModem.oConn = oConn;
                    //SerialModem.oConnAuditTrail = oConnAuditTrail;
                    //SerialModem.bCheckAllowInit = bCheckAllowInit;

                    InitConnType();

                    // Start the TcpIp thread
                    if (IsConnTcpIpActive)
                    {
                        InitThreadTCPIP();

                        threadIP = new Thread(new ThreadStart(AsyncSocket.StartListening));
                        threadIP.Start();

                        CommonConsole.WriteToConsole("Ready...");
                    }

                    // Start the Modem thread
                    //if (IsConnModemActive)
                    //{
                    //    InitSerialModem();
                    //    //OpenSerialModem();
                    //    CommonConsole.WriteToConsole("Ready...");
                    //}
                }
                else
                {
                    CommonConsole.WriteToConsole("Database Connection Failed!...");
                    Console.WriteLine("Press ENTER to Terminate or R to Restart the program...");
                    string sKey = Console.ReadLine();
                    if (sKey.ToUpper() == "R")
                    {
                        Process oInSys = new Process();
                        oInSys.StartInfo.FileName = sAppDirectory + "\\InSysConsoleNewZipBRI.exe";
                        oInSys.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.Write("[Main] Error : " + ex.StackTrace);
                //CommonConsole.WriteToConsole(ex.Message);
                //Console.Read();
                Process oInSys = new Process();
                oInSys.StartInfo.FileName = sAppDirectory + "\\InSysConsoleNewZipBRI.exe";
                oInSys.Start();
            }
        }

        /// <summary>
        /// Stop Aplication
        /// </summary>
        static void ProcessExit(object sender, EventArgs e)
        {
            AsyncSocket.Stop();
        }

        /// <summary>
        /// Get data Bitmap in Database
        /// </summary>
        static void InitTableSoftwareISO()
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPBitmapProfileSoftwareBrowse, oConn))
            {
                if (oConn.State != ConnectionState.Open) oConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtISO8583);
            }
        }

        #region "Function"
        /// <summary>
        /// Initialize all the needed variable
        /// </summary>
        static void InitSocketClass()
        {
            int iPort = 1800;
            //int iMaxConn = 1000;
            int iMaxConn = int.Parse(ConfigurationManager.AppSettings["MaxConn"].ToString());
            InitSocketControl(ref iMaxConn, ref iPort);
            AsyncSocket.iPort = int.Parse(ConfigurationManager.AppSettings["Port1"].ToString());
            AsyncSocket.iConnMax = iMaxConn;
        }

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                sConnString = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnectionAuditTrail()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitDataAuditTrail oObjInit = new InitDataAuditTrail(sAppDirectory);
                sConnStringAuditTrail = oObjInit.sGetConnString();
                sConnStringAuditTrail = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnStringAuditTrail);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }

        /// <summary>
        /// Get the Port number, Max simultan connections configuration
        /// </summary>
        static void InitSocketControl(ref int _iConnMax, ref int _iPortInit)
        {
            int _iPortDownload;
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPInitConnBrowse, oConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sMaxConn", SqlDbType.VarChar).Value = "InitMaxConn";
                oSqlCmd.Parameters.Add("@sInitPort", SqlDbType.VarChar).Value = "PortInit";
                oSqlCmd.Parameters.Add("@sSoftwarePort", SqlDbType.VarChar).Value = "PortDownload";

                if (oConn.State != ConnectionState.Open)
                {
                    oConn.Close();
                    oConn.Open();
                }

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                    {
                        _iConnMax = int.Parse(oRead["InitMaxConn"].ToString());
                        _iPortInit = int.Parse(oRead["PortInit"].ToString());
                        _iPortDownload = int.Parse(oRead["PortDownload"].ToString());
                    }
                    oRead.Close();
                }
            }
        }

        /// <summary>
        /// Set the Allow Initialization configuration
        /// </summary>
        static void InitControlAllowInit()
        {
            bCheckAllowInit = bGetControlAllowInit();
        }

        /// <summary>
        /// Get the Allow Init configuration from the database
        /// </summary>
        /// <returns>bool : boolean allow to initialize or not</returns>
        static bool bGetControlAllowInit()
        {
            bool bTemp = false;
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();

                SqlCommand oCmd = new SqlCommand(CommonSP.sSPFlagControlAllowInit, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;

                if (oConn.State != ConnectionState.Open) oConn.Open();

                SqlDataAdapter oda = new SqlDataAdapter(oCmd);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                if (dt.Rows.Count > 0)
                    bTemp = Convert.ToInt16(dt.Rows[0][0].ToString()) == 1 ? true : false;

                oCmd.Dispose();
                oda.Dispose();
                dt.Dispose();
            }
            catch (Exception)
            {
                //Logs.doWriteErrorFile("[MAIN] InitControlAllowInit : " + ex.Message);
            }
            return bTemp;
        }

        static void InitThreadTCPIP()
        {
            InitSocketClass();
        }

        /// <summary>
        /// Set Connection
        /// </summary>
        static void InitConnType()
        {
            InitConsoleConnType oConnType = new InitConsoleConnType(sAppDirectory);
            IsConnTcpIpActive = oConnType.IsConnTcpIpActive();
            IsConnModemActive = oConnType.IsConnModemActive();

            sModemPort1 = oConnType.Modem1;
            sModemPort2 = oConnType.Modem2;
            sModemPort3 = oConnType.Modem3;
            sModemPort4 = oConnType.Modem4;
            sModemPort5 = oConnType.Modem5;
        }

        /// <summary>
        /// Initialize Serial Modem
        /// </summary>
        static void InitSerialModem()
        {
            //if (!string.IsNullOrEmpty(sModemPort1))
            //    oSerModem1 = new SerialModem(sModemPort1);
            //Thread.Sleep(100);
            //if (!string.IsNullOrEmpty(sModemPort2))
            //    oSerModem2 = new SerialModem(sModemPort2);
            //Thread.Sleep(100);
            //if (!string.IsNullOrEmpty(sModemPort3))
            //    oSerModem3 = new SerialModem(sModemPort3);
            //Thread.Sleep(100);
            //if (!string.IsNullOrEmpty(sModemPort4))
            //    oSerModem4 = new SerialModem(sModemPort4);
            //Thread.Sleep(100);
            //if (!string.IsNullOrEmpty(sModemPort5))
            //    oSerModem5 = new SerialModem(sModemPort5);
            //Thread.Sleep(100);
        }

        /// <summary>
        /// Open Serial Modem
        /// </summary>
        static void OpenSerialModem()
        {
            //if (!string.IsNullOrEmpty(sModemPort1))
            //    oSerModem1.Open();
            //Thread.Sleep(100);
            //if (!string.IsNullOrEmpty(sModemPort2))
            //    oSerModem2.Open();
            //Thread.Sleep(100);
            //if (!string.IsNullOrEmpty(sModemPort3))
            //    oSerModem3.Open();
            //Thread.Sleep(100);
            //if (!string.IsNullOrEmpty(sModemPort4))
            //    oSerModem4.Open();
            //Thread.Sleep(100);
            //if (!string.IsNullOrEmpty(sModemPort5))
            //    oSerModem5.Open();
            //Thread.Sleep(100);
        }
        #endregion
    }
}
