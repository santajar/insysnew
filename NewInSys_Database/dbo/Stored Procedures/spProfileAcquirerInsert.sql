﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 23, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Insert new terminal into database (tbProfileTerminal and tbProfileTerminalList)
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerInsert]
	@sTerminalID VARCHAR(8),
	@sContent VARCHAR(MAX)
AS
DECLARE @sTag VARCHAR(8)


CREATE TABLE #TempAcq
(
	TerminalID VARCHAR(8),
	[Name] VARCHAR(15),
	Tag VARCHAR(5),
	LenghtOfTagLength INT,
	TagLength INT,
	TagValue VARCHAR(150)
)

Declare @iDBID INT
	SELECT @iDBID=DatabaseID 
	FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
IF (dbo.isTagLength4(@iDBID)=4)
BEGIN

	SET @sTag = 'AD03'
	INSERT INTO #TempAcq(TerminalID,[Name],Tag,LenghtOfTagLength,TagLength,TagValue)
		SELECT TerminalID,
		[Name],
		Tag,
		LEN(TagLength) AS LenghtOfTagLength,
		TagLength,
		TagValue
	FROM dbo.fn_TbProfileTLV(@sTerminalID,@sContent)

END
ELSE
BEGIN

	SET @sTag = 'AD003'
	INSERT INTO #TempAcq(TerminalID,[Name],Tag,LenghtOfTagLength,TagLength,TagValue)
		SELECT TerminalID,
		[Name],
		Tag,
		LEN(TagLength) AS LenghtOfTagLength,
		TagLength,
		TagValue
	FROM dbo.fn_TbProfileTLV5(@sTerminalID,@sContent)

END
DECLARE @sAcqName VARCHAR(50)

SELECT @iDBID=DatabaseID 
FROM tbProfileTerminalList WITH (NOLOCK)
WHERE TerminalID = @sTerminalID

SELECT DISTINCT @sAcqName = [Name] FROM #TempAcq

INSERT INTO tbProfileAcquirer(TerminalID,
	AcquirerName,
	AcquirerTag, 
	AcquirerLengthOfTagLength, 
	AcquirerTagLength, 
	AcquirerTagValue)
SELECT TerminalID,
	[Name],
	Tag,
	LenghtOfTagLength,
	TagLength,
	TagValue
FROM #TempAcq
--IF Object_ID ('TempDB..#TempAcq') IS NOT NULL
	DROP TABLE #TempAcq
--Add Relation
EXEC spProfileRelationInsertProcess @sTerminalID,@sAcqName, NULL, NULL, @sTag

--DROP TABLE  #TempAcq

EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

DECLARE @sDbID VARCHAR(5)
SET @sDbID = dbo.iDatabaseIdByTerminalId(@sTerminalID)

EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'AA', @sAcqName, @sContent, 0

--IF Object_ID ('TempDB..#TempAcq') IS NOT NULL
--	DROP TABLE #TempAcq