﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <19 Maret 2015>
-- Description:	<Insert EMV Management>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileEMVManagementInsert]
	@iDatabaseID INT,
	@sEMVManagementName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfileEMVManagement WITH (NOLOCK)
WHERE EMVManagementTag IN ('EM01','EM001') AND
		EMVManagementTagValue = @sEMVManagementName AND
		DatabaseID = @iDatabaseID

IF (DBO.isTagLength4(@iDatabaseID) = 4)
BEGIN
print'1'
	INSERT INTO tbProfileEMVManagement(
		DatabaseId,
		EMVManagementName,
		EMVManagementTag,
		EMVManagementLengthofTagLength,
		EMVManagementTagLength,
		EMVManagementTagValue
		)
	SELECT 
		@iDatabaseID [DatabaseId],
		[Name] [EMVManagementName],
		Tag [EMVManagementTag],
		LEN(TagLength) [EMVManagementLengthofTagLength],
		TagLength [EMVManagementTagLength],
		TagValue [EMVManagementTagValue]
	FROM dbo.fn_tbProfileTLV(@sEMVManagementName,@sContent)
END
ELSE
BEGIN
print '2'
	INSERT INTO tbProfileEMVManagement(
		DatabaseId,
		EMVManagementName,
		EMVManagementTag,
		EMVManagementLengthofTagLength,
		EMVManagementTagLength,
		EMVManagementTagValue
		)
	SELECT 
		@iDatabaseID [DatabaseId],
		[Name] [EMVManagementName],
		Tag [EMVManagementTag],
		LEN(TagLength) [EMVManagementLengthofTagLength],
		TagLength [EMVManagementTagLength],
		TagValue [EMVManagementTagValue]
	FROM dbo.fn_tbProfileTLV5(@sEMVManagementName,@sContent)
END