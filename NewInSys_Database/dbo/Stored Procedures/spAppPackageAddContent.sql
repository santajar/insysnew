﻿CREATE PROCEDURE [dbo].[spAppPackageAddContent]
	@iBuildNumber INT,
	@sAppPackageName VARCHAR(150),
	@sAppPackageFilename VARCHAR(50),
	@sAppPackageDesc VARCHAR(250),
	@bAppPackageContent VARBINARY(MAX) = NULL,
	--@iDatabaseID INT,
	@sEdcTypeId VARCHAR(150) = NULL,
	@iSuccess BIT OUTPUT,
	@iAppPackId INT OUTPUT
AS
	SET NOCOUNT ON
DECLARE @iCount INT

SELECT @iCount = COUNT(AppPackageName) 
FROM tbAppPackageEDCList WITH (NOLOCK)
WHERE AppPackageName = @sAppPackageName

IF @iCount = 0
BEGIN
	DECLARE @table table (id int)
	INSERT INTO tbAppPackageEDCList(
		BuildNumber,
		AppPackageName,
		AppPackageFilename,
		AppPackageContent,
		AppPackageDesc,
		EdcTypeId,
		UploadTime
		--DatabaseId
	)
	OUTPUT inserted.AppPackId into @table
	VALUES(
		@iBuildNumber,
		@sAppPackageName,
		@sAppPackageFilename,
		@bAppPackageContent,
		@sAppPackageDesc,
		@sEdcTypeId,
		GETDATE()
		--@iDatabaseID
	)
	SELECT @iAppPackId=id FROM @table
	SET @iSuccess = 1
END
ELSE
	SET @iSuccess = 0