﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 20, 2016
-- Modify date: 
-- Description:	
--				1. view The History Terminal table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalHistoryBrowse]
	@sTerminalID VARCHAR(8),
	@sDatetime VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT ItemName,A.TerminalTagValue AS [Value]
	FROM tbProfileTerminalHistory A
	LEFT JOIN tbProfileTerminalList B
	ON A.TerminalID = B.TerminalID
	LEFT JOIN tbItemList C
	ON B.DatabaseID = C.DatabaseID AND A.TerminalTag = C.Tag
	WHERE A.TerminalID = @sTerminalID
		AND A.LastUpdate = @sDatetime
		AND FormID = 1
	ORDER BY ItemSequence
END