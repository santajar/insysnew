﻿

-- ============================================================
-- Description:	 Insert new Remote Download Name into tbProfileEMVManagement with different name
-- Ibnu Saefullah
-- Create : 23 Mei 2017, Copy Emv Management Name
-- ============================================================

CREATE PROCEDURE [dbo].[spProfileEMVManagementCopy]
	@iDatabaseID VARCHAR(3),
	@sOldEmvName VARCHAR(50),
	@sNewEmvName VARCHAR(50)

AS
Declare
	@Content VARCHAR(MAX)=NULL,
	@iCount INT,
	@iIndex INT

SELECT @iCount= COUNT(*)
FROM tbProfileEMVManagement WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID
	AND EMVManagementName = @sNewEmvName


IF @iCount = 0
BEGIN
	INSERT INTO tbProfileEMVManagement(EMVManagementName, DatabaseID,EMVManagementTag, EMVManagementLengthofTagLength, EMVManagementTagLength, EMVManagementTagValue)
	SELECT @sNewEmvName, DatabaseID,EMVManagementTag, EMVManagementLengthofTagLength, EMVManagementTagLength, EMVManagementTagValue
	FROM tbProfileEMVManagement WITH (NOLOCK)
	WHERE EMVManagementName = @sOldEmvName AND DatabaseId=@iDatabaseID
	--EXEC spItemComboBoxValueInsertRemoteDownload @sNewEmvName, @iDatabaseID

	SET @iIndex=CHARINDEX('-',@sOldEmvName)
	 
	IF (@iIndex = 0)
	BEGIN
		 UPDATE tbProfileEMVManagement
		 SET EMVManagementTagValue = @sNewEmvName, EMVManagementTagLength = LEN(@sNewEmvName)
		 WHERE EMVManagementTag IN ('EM001','EM01') AND EMVManagementName = @sNewEmvName
	END
	ELSE
	BEGIN
		 UPDATE tbProfileEMVManagement
		 SET EMVManagementTagValue = SUBSTRING(@sNewEmvName,0,@iIndex-1), EMVManagementTagLength = LEN(SUBSTRING(@sNewEmvName,0,@iIndex-1))
		 WHERE EMVManagementTag IN ('EM001','EM01') AND EMVManagementName = @sNewEmvName
	END
	 
END
ELSE
	SELECT 'Emv management Index Already Exist.'

