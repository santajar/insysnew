﻿
-- =========================================================
-- Description: View data from tbAutoInitLog for current TID
-- =========================================================

CREATE PROCEDURE [dbo].[spAutoInitLogBrowse] 
	@sTerminalID CHAR(8)
AS
	SET NOCOUNT ON
	SELECT TerminalID, 
			LastInitTime,
			PABX,
			SoftwareVer,
			OSVer,
			KernelVer,
			EDCSN,
			ReaderSN,
			PSAMSN,
			MCSN,
			MemUsage,
			ICCID
	FROM tbAutoInitLog WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID