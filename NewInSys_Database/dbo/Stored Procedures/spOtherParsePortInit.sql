﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 23, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. get the control flag of port number
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParsePortInit]
AS
SELECT Flag
FROM tbCOntrolFlag WITH (NOLOCK)
WHERE ItemName='PortInit'
