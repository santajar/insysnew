﻿CREATE PROCEDURE [dbo].[sptbItemComboBoxValueCopy]
	@sDbSourceID SMALLINT,
	@sDbDestinationID SMALLINT
AS
	INSERT INTO tbItemComboBoxValue (DatabaseID,
									 FormID,
									 ItemSequence,
									 DisplayValue,
									 RealValue
									)
	SELECT @sDbDestinationID,
		   FormID,
		   ItemSequence,
		   DisplayValue,
		   RealValue
	FROM tbItemComboBoxValue WITH (NOLOCK)
	WHERE DatabaseID = @sDbSourceID
