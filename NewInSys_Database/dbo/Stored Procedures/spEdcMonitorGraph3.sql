﻿CREATE PROCEDURE [dbo].[spEdcMonitorGraph3]
AS
SELECT b.CardName,SUM(b.CardTotalAmount) TotalAmount
FROM tbEdcMonitor a LEFT JOIN tbEdcMonitorDetail b ON a.ID=b.ID_Header
WHERE DATEDIFF(day,MessageTimestamp,GETDATE())<=14
GROUP BY b.CardName
