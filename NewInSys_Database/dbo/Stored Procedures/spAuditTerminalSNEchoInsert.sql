﻿-- Author		: Tobias SETYO
-- Created Date : Nov 29, 2013
-- Modify Date	:
--					1. <MMM dd, yyyy>, <modified by>
-- Description	: insert echo log
CREATE PROCEDURE [dbo].[spAuditTerminalSNEchoInsert]
	@sTerminalSN VARCHAR(25),
	@sTerminalID VARCHAR(10)=NULL,
	@sAppName VARCHAR(50)=NULL,
	@iCommsID INT
AS
	SET NOCOUNT ON
INSERT INTO tbAuditTerminalSNEcho(
	TerminalSN,
	TerminalID,
	AppName,
	CommsID,
	DateCreated)
VALUES (
	@sTerminalSN,
	@sTerminalID,
	@sAppName,
	@iCommsID,
	GETDATE())