﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Oct 18, 2013
-- Modify date:  
--				1. Oct 22, 2013, change table name to tbScheduleSoftware and variable names
--				2. Oct 27, 2013, remove the update part to different store procedures, set TerminalID to allow null, 
--					EnableDownload default is true
-- Description:	
--				Insert new schedule into database (tbScheduleUpdate)
-- =============================================

CREATE PROCEDURE [dbo].[spScheduleSoftwareInsert]
	@sTerminalID VARCHAR(8)=NULL,
	@sScheduleType VARCHAR(10)=null,
	@sScheduleCheck VARCHAR(10)=null,
	@dtScheduleTime DATETIME=null,
	@iEnableDownload BIT = 1,
	@iScheduleID INT OUTPUT
AS
DECLARE @INSERT TABLE(SCHEDID INT)
INSERT INTO tbScheduleSoftware (TerminalID,ScheduleType,ScheduleCheck,ScheduleTime,EnableDownload)
OUTPUT inserted.ScheduleID INTO @INSERT
VALUES (@sTerminalID,@sScheduleType,@sScheduleCheck,@dtScheduleTime,@iEnableDownload)

SELECT @iScheduleID=SCHEDID FROM @INSERT