﻿CREATE PROCEDURE [dbo].[spProfileTerminalDbDelete]
	@sDatabaseID SMALLINT
AS
	DELETE FROM tbProfileTerminalDb
	WHERE DatabaseID = @sDatabaseID
