﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Agt 19, 2013
-- Modify date:
--				1. 
-- Description:	Insert into tbSoftwareProgress
-- =============================================

CREATE procedure [dbo].[spSoftwareProgressInsert]
@sTerminalID varchar(8),
@sSoftware varchar(50),
@sSoftwareDownload varchar(50),
@sSerialNumber varchar(25),
@fIndex float,
@iBuildNumber int

AS
DECLARE
	@iTotalIndex int,
	@fPercentage float,
	@sEdcGroup varchar(25),
	@sEdcRegion varchar(25),
	@sEdcCity Varchar(25),
	@iAppPackId INT,
	@iCount INT
--select @sEdcGroup=b.GroupName,@sEdcRegion=c.RegionName from tbTerminalSN a 
--left join tbGroup b on a.GroupId=b.GroupId 
--left join tbRegion c on a.RegionId=c.RegionId where a.SerialNumber = @sSerialNumber

SELECT @sEdcGroup=B.GroupRDName,@sEdcRegion=C.RegionRDName,@sEdcCity=D.CityRDName, @iAppPackId=E.AppPackId
FROM (
	(SELECT *
	FROM tbListTIDRemoteDownload WITH (NOLOCK)) A
	LEFT JOIN
	(SELECT *
	FROM tbGroupRemoteDownload WITH (NOLOCK)) B
	ON A.IdGroupRD= B.IdGroupRD
	LEFT JOIN
	(SELECT * 
	FROM tbRegionRemoteDownload WITH (NOLOCK)) C
	ON A.IdRegionRD = C.IdRegionRD
	LEFT JOIN
	(SELECT *
	FROM tbCityRemoteDownload WITH (NOLOCK)) D
	ON A.IdCityRD = D.IdCityRD
	LEFT JOIN
	(SELECT *
	FROM tbAppPackageEDCList WITH (NOLOCK)) E
	ON A.AppPackID = E.AppPackId
)
WHERE A.TerminalID = @sTerminalID

--SELECT @iAppPackId = AppPackId
--FROM tbAppPackageEDCList
--WHERE AppPackageName = @sSoftwareDownload
--	AND BuildNumber = @iBuildNumber

SELECT @iTotalIndex=COUNT(*) 
FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackId = @iAppPackId

PRINT 'MASUK SOFTWARE PROGRESS INSERT'
PRINT 'SOFTWARENAME = '+@sSoftware
PRINT 'SOFTWAREDOWNLOAD = '+@sSoftwareDownload
PRINT @fIndex
PRINT @iTotalIndex
PRINT '----'

SET @fPercentage = @fIndex / @iTotalIndex * 100

SELECT @iCount=COUNT(*)
FROM tbSoftwareProgress WITH (NOLOCK)
WHERE InstallStatus = 'On Progress'
	AND TerminalID = @sTerminalID
	AND Software = @sSoftware
	AND SoftwareDownload = @sSoftwareDownload
	AND SerialNumber= @sSerialNumber
	AND BuildNumber = @iBuildNumber

IF (@iCount>0)
BEGIN
	EXEC spSoftwareProgressUpdate
		@sTerminalID,
		@sSoftware,
		@sSoftwareDownload,
		@sSerialNumber,
		@fPercentage,
		@iBuildNumber,
		'On Progress'
END
ELSE
BEGIN
	INSERT INTO tbSoftwareprogress 
				(EdcGroup,
				EdcRegion,
				EdcCity,
				TerminalID,
				Software,
				BuildNumber,
				SoftwareDownload,
				SerialNumber,
				StartTime,
				Percentage,
				InstallStatus)
	VALUES(@sEdcGroup,
		   @sEdcRegion,
		   @sEdcCity,
		   @sTerminalID,
		   @sSoftware,
		   @iBuildNumber,
		   @sSoftwareDownload,
		   @sSerialNumber,
		   CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
		   @fPercentage,
		   'On Progress')
END