﻿
CREATE PROCEDURE [dbo].[spProfileGPRSDeleteByDBID]
	@sDatabaseID SMALLINT
AS
	DELETE FROM tbProfileGPRS
	WHERE DatabaseID = @sDatabaseID

