﻿CREATE PROCEDURE [dbo].[spRD_GroupDetailInsert]
	@iGroupID INT,
	@sMID VARCHAR(15)
AS
SELECT *
INTO #rd_groupdetail
FROM tbRD_GroupDetail WITH(NOLOCK)
WHERE GROUP_ID=@iGroupID AND [Enabled]=1


INSERT INTO tbRD_GroupDetail(TERMINALID,MID,GROUP_ID)
SELECT DISTINCT TerminalID, @sMID MID, @iGroupID GROUPID
FROM dbo.tbProfileAcquirer WITH(NOLOCK)
WHERE TerminalID NOT IN (SELECT TerminalID FROM #rd_groupdetail)
	AND AcquirerTag IN ('AA04','AA004') AND AcquirerTagValue=@sMID
