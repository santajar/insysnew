﻿CREATE PROCEDURE [dbo].[spRD_SwVersionListBrowse]
	@sCondition VARCHAR(500)=NULL
AS
DECLARE @sQuery VARCHAR(MAX)
SET @sQuery = '
SELECT [SV_ID]
	,[SVList_Name] [Filename]
	,[SVList_Content]
	,[CreateDate]
FROM tbRD_SoftwareVersionList
WHERE [Enabled]=1
'
EXEC (@sQuery)
