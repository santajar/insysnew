﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Aug 19, 2010
-- Modify	  :
--				1. 
-- Description:	Copy The Acquirer table content of Terminal
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerCopy]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
BEGIN
	-- select all value
	INSERT INTO tbProfileAcquirer(
		TerminalId,
		AcquirerName,
		AcquirerTag,
		AcquirerLengthOfTagLength,
		AcquirerTagLength,
		AcquirerTagValue,
		[Description]
		)
	SELECT @sNewTID,
		AcquirerName,
		AcquirerTag,
		AcquirerLengthOfTagLength,
		AcquirerTagLength,
		AcquirerTagValue,
		[Description] 
	FROM tbProfileAcquirer WITH (NOLOCK)
	WHERE TerminalId=@sOldTID
END
