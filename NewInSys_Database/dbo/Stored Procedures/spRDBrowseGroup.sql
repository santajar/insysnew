﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,,16 April 2014>
-- Description:	<Description,,SP Browse Region Remote Download>
-- =============================================
CREATE PROCEDURE [dbo].[spRDBrowseGroup]
	@sCondition VARCHAR(2000)= NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @sQuery VARCHAR(2000)
    -- Insert statements for procedure here
	SET @sQuery = 'SELECT IdGroupRD,GroupRDName as [Group]
				   FROM [tbGroupRemoteDownload] WITH (NOLOCK) '

	EXEC (@sQuery + @sCondition)
	
END