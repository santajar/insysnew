﻿
CREATE PROCEDURE [dbo].[spProfileTLEInsertTagToAll]
	@sDatabaseID SMALLINT,
	@sTag CHAR(4),
	@iLengthofTagValueLength INT,
	@sTagValue VARCHAR(500),
	@iTagValueLength INT
AS
	INSERT INTO tbProfileTLE (TLEName, TLETag, TLELengthOfTagLength, TLETagLength, TLETagValue)
	SELECT TLEName, @sTag, @iLengthOfTagValueLength, @iTagValueLength, @sTagValue
	FROM (SELECT DISTINCT TLEName
		  FROM tbProfileTLE WITH (NOLOCK)
		  WHERE DatabaseID = @sDatabaseID
	  	 ) AS Temp



