﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: SEPT 3, 2014
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Add BANK CODE
-- =============================================
CREATE PROCEDURE [dbo].[spProfileBankCodeInsert]
	@iDatabaseID INT,
	@sBankCode VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfileBankCode WITH (NOLOCK)
WHERE BankCodeTag IN ('KB01','KB001') AND
		BankCodeTagValue = @sBankCode AND
		DatabaseID = @iDatabaseID

IF @iCountID =0
BEGIN
	IF (dbo.isTagLength4(@iDatabaseID)= 4)
	BEGIN
		INSERT INTO tbProfileBankCode(
			[DatabaseId],
			[BankCode],
			[BankCodeTag],
			[BankCodeLengthOfTagLength],
			[BankCodeTagLength],
			[BankCodeTagValue]
			)
		SELECT 
			@iDatabaseID [DatabaseId],
			[Name] [BankCode],
			Tag [BankCodeTag],
			LEN(TagLength) [BankCodeLengthOfTagLength],
			TagLength [BankCodeTagLength],
			TagValue [BankCodeTagValue]
		FROM dbo.fn_tbProfileTLV(@sBankCode,@sContent)
	END
	ELSE
	BEGIN
		INSERT INTO tbProfileBankCode(
			[DatabaseId],
			[BankCode],
			[BankCodeTag],
			[BankCodeLengthOfTagLength],
			[BankCodeTagLength],
			[BankCodeTagValue]
			)
		SELECT 
			@iDatabaseID [DatabaseId],
			[Name] [BankCode],
			Tag [BankCodeTag],
			LEN(TagLength) [BankCodeLengthOfTagLength],
			TagLength [BankCodeTagLength],
			TagValue [BankCodeTagValue]
		FROM dbo.fn_tbProfileTLV5(@sBankCode,@sContent)
	END
END
