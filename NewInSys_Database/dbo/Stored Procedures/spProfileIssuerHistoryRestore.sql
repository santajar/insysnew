﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <26 January 2016>
-- Description:	<Restore Profile data Issuer>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileIssuerHistoryRestore]
	@sTerminalID VARCHAR(8),
	@sDate VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue
	INTO #TempTerminal
	FROM tbProfileIssuer WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID AND 
			IssuerName IN (SELECT IssuerName FROM tbProfileIssuerHistory WHERE TerminalID = @sTerminalID AND  LastUpdate = @sDate)

	DELETE FROM tbProfileIssuer
	WHERE TerminalID = @sTerminalID AND IssuerName IN (SELECT IssuerName FROM tbProfileIssuerHistory WHERE TerminalID = @sTerminalID AND  LastUpdate = @sDate)

	INSERT INTO tbProfileIssuer (TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue)
	SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue
	FROM tbProfileIssuerHistory
	WHERE TerminalID = @sTerminalID AND LastUpdate = @sDate

	INSERT INTO tbProfileIssuerHistory(TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue,LastUpdate)
	SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue,CONVERT(VARCHAR(20),GETDATE(),113)
	FROM #TempTerminal
	WHERE TerminalID = @sTerminalID

	EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
	EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

	DROP TABLE #TempTerminal

    
END