﻿-- =============================================
-- Author:		<Author,,TEDIE SCORFIA>
-- Create date: <Create Date,,24 APRIL 2014>
-- Description:	<Description,,INSERT INTO tbListTIDRemoteDownload>
-- =============================================
CREATE PROCEDURE [dbo].[spRDInsertListTid]
	@sTerminalID VARCHAR(8), @sCategory VARCHAR(10), @iID INT,@iScheduleID int, @iAppPackID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @iCount INT, @iDefaultValue INT

	SET @iDefaultValue = '0'

	SELECT @iCount = COUNT(*)
	FROM tbListTIDRemoteDownload WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID

	IF(@iCount >0)
	BEGIN
		IF (@sCategory = 'Group')
		BEGIN
			UPDATE tbListTIDRemoteDownload
			SET IdGroupRD = @iID,ScheduleID=@iScheduleID,AppPackID = @iAppPackID
			WHERE TerminalID = @sTerminalID
		END
		ELSE IF ( @sCategory = 'Region')
		BEGIN
			UPDATE tbListTIDRemoteDownload
			SET IdRegionRD = @iID,ScheduleID=@iScheduleID,AppPackID = @iAppPackID
			WHERE TerminalID = @sTerminalID
		END
		ELSE
		BEGIN
			UPDATE tbListTIDRemoteDownload
			SET IdCityRD = @iID,ScheduleID=@iScheduleID,AppPackID = @iAppPackID
			WHERE TerminalID = @sTerminalID
		END
	END
	ELSE
	BEGIN
		IF (@sCategory = 'Group')
		BEGIN
			INSERT INTO tbListTIDRemoteDownload(TerminalID,IdRegionRD,IdGroupRD,IdCityRD,ScheduleID,AppPackID)
			VALUES (@sTerminalID,@iDefaultValue,@iID,@iDefaultValue,@iScheduleID,@iAppPackID)
		END
		ELSE IF ( @sCategory = 'Region')
		BEGIN
			INSERT INTO tbListTIDRemoteDownload(TerminalID,IdRegionRD,IdGroupRD,IdCityRD,ScheduleID,AppPackID)
			VALUES (@sTerminalID,@iID,@iDefaultValue,@iDefaultValue,@iScheduleID,@iAppPackID)
		END
		ELSE
		BEGIN
			INSERT INTO tbListTIDRemoteDownload(TerminalID,IdRegionRD,IdGroupRD,IdCityRD,ScheduleID,AppPackID)
			VALUES (@sTerminalID,@iDefaultValue,@iDefaultValue,@iID,@iScheduleID,@iAppPackID)
		END
	END

   
END