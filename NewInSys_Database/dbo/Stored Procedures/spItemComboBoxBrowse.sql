﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 7, 2010
-- Description:	Get Form Item Combo Database
-- =============================================
CREATE PROCEDURE [dbo].[spItemComboBoxBrowse]
	@sCond VARCHAR(MAX)=NULL
AS
	DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery='SELECT 
			ItemId,
			DatabaseID,
			FormID,
			ItemSequence,
			DisplayValue,
			RealValue
		FROM tbItemComboBoxValue WITH (NOLOCK) '
	IF @sCond IS NULL
		EXEC (@sQuery)
	ELSE
		EXEC (@sQuery + @sCond)
