﻿
-- =============================================
-- Author:		Tobias Setyo
-- Create date: Feb 10, 2016
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Update the CARDTYPE Management on table tbProfileInitialFlazz
--				2. Modify for Form Flexsible
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCardTypeUpdate]
	@iDatabaseID INT,
	@sCardTypeName VARCHAR(20),
	@sContent VARCHAR(MAX)
AS
	EXEC spProfileCardTypeDelete @iDatabaseID, @sCardTypeName
	EXEC spProfileCardTypeInsert @iDatabaseID, @sCardTypeName, @sContent