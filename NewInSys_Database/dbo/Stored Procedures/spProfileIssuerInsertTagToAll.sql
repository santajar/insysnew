﻿
CREATE PROCEDURE [dbo].[spProfileIssuerInsertTagToAll]
	@sDatabaseID SMALLINT,
	@sTag CHAR(5),
	@iLengthofTagValueLength INT,
	@sTagValue VARCHAR(500),
	@iTagValueLength INT
AS
	INSERT INTO tbProfileIssuer (TerminalID, IssuerName, IssuerTag, IssuerLengthOfTagLength, IssuerTagLength, IssuerTagValue)
	SELECT TerminalID, IssuerName, @sTag, @iLengthOfTagValueLength, @iTagValueLength, @sTagValue
	FROM (	SELECT DISTINCT TerminalID, IssuerName
			FROM tbProfileIssuer WITH (NOLOCK)
			WHERE TerminalID IN ( SELECT TerminalID
								  FROM tbProfileTerminalList WITH (NOLOCK)
								  WHERE DatabaseID = @sDatabaseID
								)
		 ) AS Temp