﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 9, 2010
-- Modify date:
--				1. 
-- Description:	Get The last 1000 records of Audit Trail
-- =============================================
CREATE PROCEDURE [dbo].[spAuditTrailBrowse]
	@sCond VARCHAR(MAX)=NULL
AS
	SET NOCOUNT ON
DECLARE @sStmt VARCHAR(MAX)
SET @sStmt = '
	SELECT TOP (10000000) dbo.tbAuditTrail.LogID, dbo.tbAuditTrail.AccessTime, dbo.tbAuditTrail.UserId, dbo.tbAuditTrail.DatabaseName, dbo.tbAuditTrail.ActionDescription, 
                      dbo.tbAuditTrailDetail.Remarks
	FROM dbo.tbAuditTrail WITH (NOLOCK) LEFT OUTER JOIN
                      dbo.tbAuditTrailDetail WITH (NOLOCK) ON dbo.tbAuditTrail.LogID = dbo.tbAuditTrailDetail.LogID '
 
EXEC (@sStmt + @sCond + 'ORDER BY dbo.tbAuditTrail.LogID DESC')
