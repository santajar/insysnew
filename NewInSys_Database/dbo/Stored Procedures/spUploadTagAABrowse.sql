﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Browse the defined template tag and item name
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagAABrowse]
@sTemplate VARCHAR(50)=NULL
AS
SET NOCOUNT ON;
DECLARE @sQuery VARCHAR(500)
SET @sQuery = '
SELECT
	ID,
	AcquirerName,
	Tag,
	ColumnName,
	Mandatory,
	SourceColumn
FROM tbUploadTagAA WITH (NOLOCK) '
IF ISNULL(@sTemplate, '')<>''
	SET @sQuery = @sQuery + '	WHERE Template = ' + '''' + @sTemplate + ''''
EXEC (@sQuery)