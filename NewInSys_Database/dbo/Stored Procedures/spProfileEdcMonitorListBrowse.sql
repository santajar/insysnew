﻿-- =============================================
-- Author		: Tobias SETYO
-- Create date	: March 8, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spProfileEdcMonitorListBrowse]
	@sDatabaseId VARCHAR(3)
AS
	SELECT DISTINCT Name,
			DatabaseID			
	FROM tbProfileEdcMonitor WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId