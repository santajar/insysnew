﻿CREATE PROCEDURE [dbo].[spProfileCurrencyDelete]
	@iDatabaseId BIGINT,
	@sCurrencyName VARCHAR(5),
	@bDeleteData BIT = 0
AS
	IF (@bDeleteData = 1)
	BEGIN
		DELETE FROM tbProfileCurrency
		WHERE DatabaseId = @iDatabaseId AND
				CurrencyName = @sCurrencyName
			
		DECLARE @sDatabaseID VARCHAR(3)
		SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
		EXEC spProfileTerminalUpdateLastUpdateByDbID @sDatabaseID
	END
	
