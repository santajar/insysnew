﻿CREATE PROCEDURE [dbo].[spUSER_DOMAIN_INSERT]
	@UserDomain    VARCHAR (30),
	@UserName     VARCHAR (25),
	@Email        VARCHAR (50)
AS
	INSERT INTO dbo.tbUserLoginDomain (UserDomain,UserName, Email)
	VALUES (@UserDomain,@UserName, @Email)