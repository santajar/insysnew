﻿
CREATE PROCEDURE [dbo].[spBrowseProfileRelationAutoInitLogAppPackage]
	@sTerminalId VARCHAR(8)
AS
	SET NOCOUNT ON
exec spProfileRelationBrowse @sTerminalId
exec spAutoInitLogBrowse @sTerminalID
exec spAppPackageBrowseTerminalID @sTerminalID
exec spAuditInitGetNewSNbyTID @sTerminalID
