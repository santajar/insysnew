﻿CREATE PROCEDURE [dbo].[spProfileProductCodeBrowse]
	@sCondition VARCHAR(100) = NULL
AS
DECLARE @sStmt VARCHAR(MAX)
SET @sStmt = 'SELECT ProductCodeID,
					ProductCode, 
					DatabaseID,
					ProductCodeTag as Tag,
					ProductCodeLengthOfTagLength,
					ProductCodeTagLength,
					ProductCodeTagValue as Value
			FROM tbProfileProductCode WITH (NOLOCK) '
SET @sStmt = @sStmt + ISNULL(@sCondition,'') + ' ORDER BY DatabaseID, ProductCode, Tag'
EXEC (@sStmt)