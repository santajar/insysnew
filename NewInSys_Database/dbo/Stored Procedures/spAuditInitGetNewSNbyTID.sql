﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,,7 april 2014>
-- Description:	<Description,,Get new SN by TID>
-- =============================================
CREATE PROCEDURE [dbo].[spAuditInitGetNewSNbyTID]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @dtInitTime DATETIME

	SELECT TOP 1 @dtInitTime= InitTime
	FROM tbAuditInit WITH (NOLOCK)
	WHERE TERMINALID = @sTerminalID AND StatusDesc = 'Initialize Complete'
	ORDER BY INITID DESC

	SELECT TOP 1 TerminalId,Software,SerialNumber 
	FROM tbAuditInit WITH (NOLOCK)
	WHERE TerminalId = @sTerminalID AND StatusDesc = 'Initialize Start' AND InitTime >=@dtInitTime
	ORDER BY INITID DESC


END