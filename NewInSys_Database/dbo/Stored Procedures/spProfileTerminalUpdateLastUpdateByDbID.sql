﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 8, 2010
-- Modify date:
--				1. 
-- Description:	Update LastUpdate TerminalId profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalUpdateLastUpdateByDbID]
	@sDatabaseID VARCHAR(3)
AS
DECLARE @sLastUpdate VARCHAR(12)
SET @sLastUpdate = dbo.sGetDateMMDDYY()
SET @sLastUpdate = @sLastUpdate + dbo.sGetTimeHHMMSS()

UPDATE tbProfileTerminal
SET 
	TerminalTagValue=@sLastUpdate,
	TerminalTagLength=LEN(@sLastUpdate)
	WHERE TerminalId IN (SELECT TerminalID
						 FROM tbProfileTerminalList WITH (NOLOCK)
						 WHERE DatabaseID = @sDatabaseID
					)
		AND TerminalTag IN ('DC03','DC003')
