﻿CREATE PROCEDURE [dbo].[spInitGetTableInit]
 @sTerminalID VARCHAR(8)
AS
IF dbo.IsCompressInit(@sTerminalID) = 0
	EXEC spInitStart @sTerminalID
ELSE
	EXEC spInitCompressStart @sTerminalID

SELECT * FROM tbInit WITH (NOLOCK)
WHERE TerminalId = @sTerminalID
ORDER BY InitID