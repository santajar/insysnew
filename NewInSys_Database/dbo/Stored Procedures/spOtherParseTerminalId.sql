﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. get the received message terminal id
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseTerminalId] 
	@sSessionTime VARCHAR(MAX),
	@sTerminalId VARCHAR(8) OUTPUT
AS
SET NOCOUNT ON
DECLARE @sTemp VARCHAR(16)
DECLARE @sSqlStmt NVARCHAR(MAX)

SET @sSqlStmt = N'SELECT @tid=BitValue FROM ##tempBitMap' + @sSessionTime + 
	' WHERE BitId=41'
EXEC sp_executesql @sSqlStmt, N'@tid VARCHAR(16) OUTPUT', @tid=@sTemp OUTPUT

EXEC spOtherParseHexStringToString @sTemp, @sTerminalId OUTPUT
