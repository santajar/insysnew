﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 29, 2010
-- Modify date: 
--				1. Jan 12, 2011, update column IpAddress
-- Description:	
--				1. Updating the AllowDownload, StatusMaster, and EnableIp profile parameter
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalListUpdate] 
	@sTerminalID VARCHAR(8),
	@sDbID INT,
	@bAllowDownload BIT,
	@bStatus BIT,
	@bEnableIP BIT,
	@sIpAddress VARCHAR(15) = NULL,
--	@bAutoInit BIT = '1',
	@bInitCompress BIT = '0',
	@bRemoteDownload BIT ='0',
	@bLogoPrint BIT ='0',
	@bLogoIdle BIT ='0',
	@bLogoMerchant BIT ='0',
	@bLogoReceipt BIT ='0'

AS
SET NOCOUNT ON

DECLARE @dDate DATETIME

print '[spProfileTerminalListUpdate]'
print @bAllowDownload

IF @bAllowDownload = 1 
	SET @dDate = GETDATE()
BEGIN
UPDATE tbProfileTerminalList 
SET AllowDownload = @bAllowDownload,
	StatusMaster = @bStatus,
	EnableIP = @bEnableIP,
	IpAddress = @sIpAddress
WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID
END

IF @bAllowDownload = 0 
	SET @dDate = GETDATE()

UPDATE tbProfileTerminalList 
SET AllowDownload = @bAllowDownload,
	StatusMaster = @bStatus,
	EnableIP = @bEnableIP,
	IpAddress = @sIpAddress
WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID

IF EXISTS ( 
	SELECT * FROM SYS.COLUMNS
		WHERE Name = N'AutoInitTimeStamp' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
	--AND @bAllowDownload = '1' 
BEGIN
	UPDATE tbProfileTerminalList 
		SET AutoInitTimeStamp = @dDate
	WHERE TerminalID = @sTerminalID AND 
		  DatabaseID = @sDbID
END


IF EXISTS (  
	SELECT * FROM SYS.COLUMNS
	WHERE Name = N'InitCompress' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
BEGIN
	UPDATE tbProfileTerminalList 
		SET InitCompress = @bInitCompress
	WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID
END

IF EXISTS (  
	SELECT * FROM SYS.COLUMNS
	WHERE Name = N'RemoteDownload' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
BEGIN
	UPDATE tbProfileTerminalList 
		SET RemoteDownload = @bRemoteDownload
	WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID
END

IF EXISTS (  
	SELECT * FROM SYS.COLUMNS
	WHERE Name = N'LogoPrint' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
BEGIN
	UPDATE tbProfileTerminalList 
		SET LogoPrint = @bLogoPrint
	WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID
END

IF EXISTS (  
	SELECT * FROM SYS.COLUMNS
	WHERE Name = N'LogoIdle' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
BEGIN
	UPDATE tbProfileTerminalList 
		SET LogoIdle = @bLogoIdle
	WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID
END

IF EXISTS (  
	SELECT * FROM SYS.COLUMNS
	WHERE Name = N'LogoMerchant' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
BEGIN
	UPDATE tbProfileTerminalList 
		SET LogoIdle = @bLogoMerchant
	WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID
END

IF EXISTS (  
	SELECT * FROM SYS.COLUMNS
	WHERE Name = N'LogoReceipt' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	) 
BEGIN
	UPDATE tbProfileTerminalList 
		SET LogoIdle = @bLogoReceipt
	WHERE TerminalID = @sTerminalID AND DatabaseID = @sDbID
END
