﻿

CREATE PROCEDURE [dbo].[spItemComboBoxValueDeletePromoManagementName]
	@sPromoManagementName VARCHAR(50),
	@sDatabaseId INT
AS
DELETE FROM tbItemComboBoxValue
WHERE DatabaseId=@sDatabaseId
	AND RealValue=@sPromoManagementName