﻿


CREATE PROCEDURE [dbo].[spAutoInitCreateJobCheckTimeOut]
AS
	SET NOCOUNT ON
	DECLARE @jobId BINARY (16)

	DECLARE @sJobName NVARCHAR(50)
	SET @sJobName = N'AutoInitCheckTimeOut'

	SELECT @jobId = job_id 
		FROM msdb.dbo.sysjobs WHERE ([Name] = @sJobName)
	IF (@jobId IS NOT NULL)
		EXEC msdb.dbo.SP_DELETE_JOB @jobId

	SET @jobId = NULL

	-- Create Job
	EXEC msdb.dbo.SP_ADD_JOB 
			@Job_Name = @sJobName,
			@enabled  = 1,
			@Notify_Level_Eventlog = 0,
			@Job_Id = @jobId OUTPUT

	PRINT @jobId

	DECLARE @sAutoInitReportPath VARCHAR(100)
	SELECT @sAutoInitReportPath = dbo.sGetFlagValue('Report AutoInit Path')
	SET @sAutoInitReportPath = @sAutoInitReportPath  + '\AutoInitReport.txt'

	-- Create Job Step
	EXEC msdb.dbo.SP_ADD_JOBSTEP 
			@Job_Id = @jobId,
			@Step_ID = 1,
			@Step_Name = 'CheckTimeOut',
			@Command = 'EXEC spAutoInitCheckTimeOut',
			@Server = @@ServerName,
			@Database_Name = 'NewInsys',
			@Output_File_Name = @sAutoInitReportPath, 
			@Flags = 0

	DECLARE @schedID INT
	DECLARE @sScheduleName NVARCHAR(50)
	SET @sScheduleName = N'ScheduleAutoInitTimeOut'

	SELECT @schedID = Schedule_Id 
		FROM msdb.dbo.SysSchedules WHERE ([Name] = @sScheduleName)
	IF (@schedID IS NOT NULL)
		EXEC msdb.dbo.sp_delete_schedule @schedID 

	--Create Schedule
	EXEC msdb.dbo.SP_ADD_SCHEDULE
			@Schedule_Name = @sScheduleName,
			@Enabled = 1,
			@Freq_Type = 4,
			@Freq_Interval = 1,
			@Active_Start_Date = 20110719,
			@Active_Start_Time = 000000,
			@Active_End_Time = 010000,
			@Schedule_ID = @schedID OUTPUT

	Print @schedID

	--Attach Schedule to Job
	EXEC msdb.dbo.SP_ATTACH_SCHEDULE 
			@Job_Id = @jobId,
			@Schedule_Id = @schedID

	-- Create Job Server
	EXEC msdb.dbo.SP_ADD_JOBSERVER
		@Job_Id = @jobId,
		@Server_Name = @@ServerName
		



-- SELECT * FROM msdb.dbo.sysjobs WHERE (name = N'AutoInitCheckTimeOut')

	



