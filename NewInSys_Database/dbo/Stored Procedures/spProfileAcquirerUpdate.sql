﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 24, 2010
-- Modify	  :
--				1. Jul 04, 2012, add spInitTerminalInsert
-- Description:	
--				1. Updating an acquirer on a profile
--				2. Add Tag Lenght validation
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerUpdate]
@sTerminalID VARCHAR(8),
@sContent VARCHAR(MAX)
AS
	CREATE TABLE #TempAcq
	(
		TerminalID VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		LengthOfTagLength INT,
		TagLength INT,
		TagValue VARCHAR(150)
	)
	DECLARE @sDbID VARCHAR(5),
			@sAcqName VARCHAR(50)

	SET @sDbID = dbo.iDatabaseIdByTerminalId(@sTerminalID)
	IF (dbo.isTagLength4(CAST(@sDbID AS INT)) = 4 )
	BEGIN
		INSERT INTO #TempAcq (TerminalID,[Name],Tag,[LengthOfTagLength],TagLength,TagValue)
		SELECT TerminalID,
				[Name],
				Tag,
				LEN(TagLength) [LengthOfTagLength],
				TagLength,
				TagValue
		FROM fn_tbprofiletlv(@sTerminalID, @sContent)
	END
	ELSE
	BEGIN
		INSERT INTO #TempAcq (TerminalID,[Name],Tag,[LengthOfTagLength],TagLength,TagValue)
		SELECT TerminalID,
				[Name],
				Tag,
				LEN(TagLength) [LengthOfTagLength],
				TagLength,
				TagValue
		FROM fn_tbprofiletlv5(@sTerminalID, @sContent)
	END
	
	SELECT DISTINCT @sAcqName = [Name] FROM #TempAcq

	EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'AA', @sAcqName, @sContent, 1
	EXEC spProfileAcquirerHistoryInsert @sTerminalID,@sAcqName

	INSERT INTO [tbProfileAcquirer]
		   ([TerminalID]
		   ,[AcquirerName]
		   ,[AcquirerTag]
		   ,[AcquirerLengthOfTagLength]
		   ,[AcquirerTagLength]
		   ,[AcquirerTagValue])
	SELECT TerminalID, 
			[Name],
			Tag,
			LengthOfTagLength,
			TagLength,
			TagValue
	FROM #TempAcq
	WHERE Tag COLLATE DATABASE_DEFAULT NOT IN (
						SELECT AcquirerTag
						FROM tbProfileAcquirer WITH (NOLOCK)
						WHERE TerminalID = @sTerminalID AND 
							  AcquirerName COLLATE DATABASE_DEFAULT = [Name]
					 )


	UPDATE Acquirer SET Acquirer.AcquirerLengthOfTagLength = Temp.LengthOfTagLength,
					    Acquirer.AcquirerTagLength = Temp.TagLength,
						Acquirer.AcquirerTagValue =  Temp.TagValue
	FROM tbProfileAcquirer Acquirer, #TempAcq Temp
	WHERE Acquirer.TerminalID = @sTerminalID AND 
		  Acquirer.AcquirerName COLLATE DATABASE_DEFAULT = Temp.[Name] COLLATE DATABASE_DEFAULT AND 
		  Acquirer.AcquirerTag COLLATE DATABASE_DEFAULT = Temp.Tag COLLATE DATABASE_DEFAULT

EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

--IF (OBJECT_ID('TEMPDB..#TempAcq') IS NOT NULL)
		DROP TABLE #TempAcq