﻿CREATE PROCEDURE [dbo].[spProfileRelationCopy]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
BEGIN
	INSERT INTO tbProfileRelation(
		TerminalId,
		RelationTag,
		RelationLengthOfTagLength,
		RelationTagLength,
		RelationTagValue,
		[Description]
		)
	SELECT @sNewTID,
		RelationTag,
		RelationLengthOfTagLength,
		RelationTagLength,
		RelationTagValue,
		[Description] 
	FROM tbProfileRelation WITH (NOLOCK)
	WHERE TerminalId=@sOldTID
	ORDER BY RelationTagID
END
