﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. ????
-- =============================================
CREATE PROCEDURE [dbo].[spDataUploadInsert]
	@sTableName VARCHAR(MAX),
	@sDestinations VARCHAR(MAX),
	@sSources VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt =
		'INSERT INTO [dbo].[' + @sTableName + ']('
			+ @sDestinations +
		')VALUES('
			+ @sSources +
		')'
	EXEC (@sStmt)
END
