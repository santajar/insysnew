﻿



-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. Aug 12, 2013, ICCID message
-- Description:	
--				1. parse the ISO message value to get the bi-48 value, EDC serial number and application name
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseBit48AutoInit] 
	@sSessionTime VARCHAR(MAX),
	@sSoftwareVers VARCHAR(50) OUTPUT
AS
	SET NOCOUNT ON
	DECLARE @sSqlStmt NVARCHAR(MAX)
	DECLARE @sBit48 VARCHAR(MAX)
	
	SET @sSqlStmt = N'SELECT @Bit48=BitValue FROM ##tempBitMap' + @sSessionTime + 
		' WHERE BitId=48'
	EXEC sp_executesql @sSqlStmt,N'@Bit48 VARCHAR(MAX) OUTPUT',@Bit48=@sBit48 OUTPUT

	SET @sBit48 = SUBSTRING(@sBit48, 5, LEN(@sBit48)-4)

	DECLARE @sLastInitTime VARCHAR(50)
	DECLARE @sPABX VARCHAR(50)
	--DECLARE @sSoftwareVers VARCHAR(50)
	DECLARE @sOSVers VARCHAR(50)
	DECLARE @sKernelEmvVers VARCHAR(50)
	DECLARE @sEdcSN VARCHAR(50)
	DECLARE @sReaderSN VARCHAR(50)
	DECLARE @sPSAMSN VARCHAR(50)
	DECLARE @sMCSN VARCHAR(50)
	DECLARE @sMemUsg VARCHAR(50)

	-- modified for ICCID
	DECLARE @sICCID VARCHAR(50)

	DECLARE @iLenValue INT
	DECLARE @iStartIndex INT
	SET @iStartIndex = 1
	DECLARE @iTemp INT
	SET @iTemp = 1

	DECLARE @sTempValue VARCHAR(50)

	WHILE (@iStartIndex < LEN(@sBit48))
	BEGIN
--		SET @iLenValue = (dbo.iBitLen(@sBit48, @iStartIndex)) * 2
		SET @iLenValue = dbo.iBitLenAutoInit(@iTemp)

		SET @sTempValue = dbo.sBitValue(@iLenValue, @iStartIndex, @sBit48)

		IF @iTemp = 1
			SET @sLastInitTime = @sTempValue
 		ELSE IF @iTemp = 2
			SET @sPABX = CONVERT(VARCHAR, CONVERT(INT,@sTempValue))
		ELSE IF @iTemp = 3
			EXEC spOtherParseHexStringToString @sTempValue, @sSoftwareVers OUTPUT
		ELSE IF @iTemp = 4
			EXEC spOtherParseHexStringToString @sTempValue, @sOSVers OUTPUT
		ELSE IF @iTemp = 5
			EXEC spOtherParseHexStringToString @sTempValue, @sKernelEmvVers OUTPUT 
		ELSE IF @iTemp = 6
			EXEC spOtherParseHexStringToString @sTempValue, @sEdcSN OUTPUT		
		ELSE IF @iTemp = 7
			EXEC spOtherParseHexStringToString @sTempValue, @sReaderSN OUTPUT 
		ELSE IF @iTemp = 8
			EXEC spOtherParseHexStringToString @sTempValue, @sPSAMSN OUTPUT 
		ELSE IF @iTemp = 9
			EXEC spOtherParseHexStringToString @sTempValue, @sMCSN OUTPUT 
		ELSE IF @iTemp = 10
			EXEC spOtherParseHexStringToString @sTempValue, @sMemUsg OUTPUT
		ELSE IF @iTemp = 11
			EXEC spOtherParseHexStringToString @sTempValue, @sICCID OUTPUT

		SET @iStartIndex = @iStartIndex + @iLenValue
		SET @iTemp = @iTemp + 1
	END
	
	EXEC spAutoInitUpdate @sSessionTime, @sLastInitTime, @sPABX, @sSoftwareVers, @sOSVers, 
						  @sKernelEmvVers, @sEdcSN, @sReaderSN, @sPSAMSN, @sMCSN, @sMemUsg