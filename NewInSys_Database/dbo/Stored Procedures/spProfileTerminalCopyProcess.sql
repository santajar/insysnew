﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 19, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	Copying Profile process
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalCopyProcess]
	@iDatabaseId INT,
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
SET NOCOUNT ON
INSERT INTO tbProfileTerminalList( TerminalID, DatabaseID, AllowDownload, StatusMaster, LocationID,RemoteDownload, LogoIdle, LogoPrint, LogoMerchant, LogoReceipt) 
SELECT @sNewTID, @iDatabaseId, 1, 0, LocationID, RemoteDownload, LogoIdle, LogoPrint, LogoMerchant, LogoReceipt
FROM tbProfileTerminalList WITH (NOLOCK)
WHERE TerminalID = @sOldTID AND DatabaseID = @iDatabaseId

IF EXISTS ( SELECT * FROM SYS.COLUMNS
		WHERE Name = N'AutoInitTimeStamp' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	  )
BEGIN
	UPDATE tbProfileTerminalList
	SET AutoInitTimeStamp =	NULL
	FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE TerminalID = @sNewTID AND 
		  DatabaseID = @iDatabaseId 
END

IF EXISTS ( SELECT * FROM SYS.COLUMNS
		WHERE Name = N'InitCompress' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	  )
BEGIN
	UPDATE new 
	SET new.InitCompress = old.InitCompress
	FROM tbProfileTerminalList new WITH (NOLOCK), tbProfileTerminalList old WITH (NOLOCK)
	WHERE new.TerminalID = @sNewTID AND 
		  new.DatabaseID = @iDatabaseId AND
		  old.TerminalID = @sOldTID AND 
		  old.DatabaseID = @iDatabaseId
END

--IF EXISTS ( SELECT * FROM SYS.COLUMNS
--		WHERE Name = N'AppPackID' AND
--		Object_ID = Object_ID (N'tbProfileTerminalList')
--	  )
--BEGIN
--	UPDATE new
--	SET new.AppPackID = old.AppPackID
--	FROM tbProfileTerminalList new, tbProfileTerminalList old
--	WHERE new.TerminalID = @sNewTID AND 
--		  new.DatabaseID = @iDatabaseId AND
--	 	  old.TerminalID = @sOldTID AND 
--		  old.DatabaseID = @iDatabaseId
--END

EXEC spProfileTerminalCopy @sOldTID, @sNewTID
EXEC spProfileAcquirerCopy @sOldTID, @sNewTID
EXEC spProfileIssuerCopy @sOldTID, @sNewTID
EXEC spProfileRelationCopy @sOldTID, @sNewTID
EXEC spProfileTerminalUpdatePwd @sNewTID

IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'Custom_Menu' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		  exec spProfileCustomMenuCopy @sOldTID, @sNewTID