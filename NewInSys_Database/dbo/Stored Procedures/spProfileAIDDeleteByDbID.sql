﻿
-- ===============================================
-- Description:	Delete All AID in given DatabaseID
-- ===============================================

CREATE PROCEDURE [dbo].[spProfileAIDDeleteByDbID]
	@sDatabaseID SMALLINT
AS
	DELETE FROM tbProfileAID
	WHERE DatabaseID = @sDatabaseID

