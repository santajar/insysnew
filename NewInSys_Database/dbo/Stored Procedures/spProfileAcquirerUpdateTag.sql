﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 7, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating an acquirer tag value on a profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerUpdateTag]
	@sTerminalID VARCHAR(8),
	@sAcqName VARCHAR(30),
	@sTag VARCHAR(5),
	@sValue VARCHAR(MAX)
AS
DECLARE @iTagId BIGINT,
	@iLengthOfTagLength INT,
	@iTagLength INT
SET @iLengthOfTagLength=LEN(LEN(@sValue))
SET @iTagLength=LEN(@sValue)

SELECT @iTagId=AcquirerTagID
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE TerminalID = @sTerminalID AND 
	AcquirerName = @sAcqName AND
	AcquirerTag = @sTag
	
UPDATE tbProfileAcquirer SET AcquirerLengthOfTagLength = @iLengthOfTagLength,
							 AcquirerTagLength = @iTagLength,
							 AcquirerTagValue =  @sValue
WHERE AcquirerTagID=@iTagId