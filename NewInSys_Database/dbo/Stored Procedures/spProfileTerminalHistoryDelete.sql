﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Modify date: 
-- Description:	
--				1. DELETE The History Terminal table content of profile
-- =============================================
Create PROCEDURE [dbo].[spProfileTerminalHistoryDelete]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM tbProfileTerminalHistory
	WHERE TerminalID = @sTerminalID
END