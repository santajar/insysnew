﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. parse the ISO message value to get the EDC serial number and application name
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseAppEdcSn] 
	@sSessionTime VARCHAR(MAX),
	@sEdcSn VARCHAR(MAX) OUTPUT,
	@sAppName VARCHAR(MAX) OUTPUT
AS
DECLARE @sTemp VARCHAR(MAX)
DECLARE @sSqlStmt VARCHAR(MAX),
		@sSqmStmtDestroy VARCHAR(MAX)
SET @sSqlStmt = 'SELECT BitValue AS VALUE FROM ##tempBitMap' + @sSessionTime + 
	' WHERE BitId=48'

SET @sSqmStmtDestroy = 'DROP TABLE ##tempBitMap'+ @sSessionTime

DECLARE @Bit48 TABLE(BitValue VARCHAR(MAX))
INSERT INTO @Bit48(BitValue)
EXEC (@sSqlStmt)

SELECT @sTemp=BitValue FROM @Bit48
DECLARE @iLenSn INT
DECLARE @iLenApp INT
DECLARE @sTempSn VARCHAR(50)
DECLARE @sTempApp VARCHAR(50)
SET @iLenSn = CONVERT(INT,SUBSTRING(@sTemp,5,4))
SET @sTempSn = SUBSTRING(@sTemp,9,@iLenSn*2)
EXEC spOtherParseHexStringToString @sTempSn, @sEdcSn OUTPUT

SET @iLenApp = CONVERT(INT,SUBSTRING(@sTemp,9+(@iLenSn*2),4))
SET @sTempApp = SUBSTRING(@sTemp,13+(@iLenSn*2),@iLenApp*2)
EXEC spOtherParseHexStringToString @sTempApp, @sAppName OUTPUT

EXEC (@sSqmStmtDestroy)
--RETURN (@sEdcSn)
