﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Update the tagvalue of terminal table
--				2. changes length @stag from 4 -> 5
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalUpdateTag]
	@sTerminalID VARCHAR(8),
	@sTag VARCHAR(5),
	@sValue VARCHAR(MAX)
AS
UPDATE tbProfileTerminal SET TerminalLengthOfTagLength = LEN(LEN(@sValue)),
							 TerminalTagLength = LEN(@sValue),
							 TerminalTagValue =  @sValue
WHERE TerminalID = @sTerminalID AND TerminalTag = @sTag