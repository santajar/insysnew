﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 11, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. add log of Data Upload 
-- =============================================
CREATE PROCEDURE [dbo].[spUploadLogInsert]
	@sTerminalId VARCHAR(8),
	@sAction VARCHAR(50),
	@sActionDetail VARCHAR(50)
AS
INSERT INTO [tbUploadLog]
           ([Time]
           ,[TerminalId]
           ,[Description]
           ,[Remarks])
     VALUES(
           CURRENT_TIMESTAMP
           ,@sTerminalId
           ,@sAction
           ,@sActionDetail)
