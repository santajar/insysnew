﻿
CREATE PROCEDURE [dbo].[spProfileIssuerUpdateTag]
	@sTerminalID VARCHAR(8),
	@sIssName VARCHAR(30),
	@sTag VARCHAR(5),
	@sValue VARCHAR(MAX)
AS
IF ISNULL(@sIssName,'') <> ''
BEGIN
	UPDATE tbProfileIssuer SET IssuerLengthOfTagLength = LEN(LEN(@sValue)),
								 IssuerTagLength = LEN(@sValue),
								 IssuerTagValue =  @sValue
	WHERE TerminalID = @sTerminalID AND 
		IssuerName = @sIssName AND
		IssuerTag = @sTag
END
ELSE
BEGIN
	UPDATE tbProfileIssuer SET IssuerLengthOfTagLength = LEN(LEN(@sValue)),
								 IssuerTagLength = LEN(@sValue),
								 IssuerTagValue =  @sValue
	WHERE TerminalID = @sTerminalID AND
		IssuerTag = @sTag
END