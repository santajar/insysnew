﻿



CREATE PROCEDURE [dbo].[spProfileLoyProdListBrowse] 
	@sDatabaseId VARCHAR(2)
AS
	SELECT DISTINCT LoyProdName [Name],
			DatabaseID			
	FROM tbProfileLoyProd WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId 


