﻿
-- =============================================  
-- Author:  Christianto  
-- Create date: Apr 15, 2012  
-- Description: view TLE list  
-- =============================================  
CREATE PROCEDURE [dbo].[spTLEListBrowse]  
	@sDatabaseId VARCHAR(3) = ''  
AS  
SELECT TLETagID,  
	--DatabaseId,  
	TLEName  
FROM dbo.fn_viewTLE(@sDatabaseId)  
--WHERE DatabaseId = @sDatabaseId--case when @sDatabaseId = '' then DatabaseId else @sDatabaseId end
  

--SELECT TLETagID,  
--	DatabaseId,  
--	TLEName  
--FROM dbo.fn_viewTLE()