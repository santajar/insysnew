﻿CREATE PROCEDURE [dbo].[spProfileIssuerRenameProcess]
	@sTerminalID VARCHAR(8),
	@sOldKey VARCHAR(15),
	@sNewKey VARCHAR(15)
AS
BEGIN
	IF (SELECT COUNT(*) FROM TbProfileRelation WITH (NOLOCK) WHERE TerminalID = @sTerminalID AND RelationTag IN ('AD02','AD002') AND RelationTagValue = @sNewKey) = 0
	BEGIN
		EXEC spProfileRelationRenameIssuer @sTerminalID, @sOldKey, @sNewKey
		EXEC spProfileIssuerRename @sTerminalID, @sOldKey, @sNewKey
	END
END
