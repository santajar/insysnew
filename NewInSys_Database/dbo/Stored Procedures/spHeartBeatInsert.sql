﻿
CREATE PROCEDURE [dbo].[spHeartBeatInsert]
	@sRawMsg VARCHAR(MAX)='',
	@sProcCode VARCHAR(6)='',
	@sTerminalID VARCHAR(8)='',
	@sSerialNumber VARCHAR(50)='',
	@sSoftwareVersion VARCHAR(50)='',
	@iLifetimeCounter INT=0,
	@iTotalAllTransaction INT=0,
	@iTotalSwipe INT=0,
	@iTotalDip INT=0,
	@sSettlementDateTime VARCHAR(12)='',
	@sICCID VARCHAR(50)='',
	--@TowerNumber  varchar(50)=0,
	--@Mcc_Mnc_Data  varchar(50)=null,
	--@Lac_Data  varchar(50)=null,
	--@Cell_Id_Data  varchar(50)=null,
	@iID_Header INT = 0 OUTPUT
AS
DECLARE @iDatabaseID INT
SELECT @iDatabaseID=DatabaseID 
FROM tbProfileTerminalList 
WHERE TerminalID=@sTerminalID

DECLARE @tempID TABLE (ID INT)
--DECLARE @SYSDATETIMEOFFSET DATETIME

--SELECT @SYSDATETIMEOFFSET=MessageTimestamp AS  FROM tbEdcMonitor
--SELECT CAST(MessageTimestamp AS DATETIMEOFFSET) FROM tbEdcMonitor
--CONVERT(VARCHAR,SYSDATETIMEOFFSET(),127)


--CONVERT(varchar(50), DateTimeExample, 120)+ ' -5:00'


INSERT INTO tbHeartBeat(TerminalID
	,DatabaseID
	,MessageTimestamp
	,MessageRaw
	,ProcCode
	,SerialNumber
	,SoftwareVersion
	,LifetimeCounter
	,TotalAllTransaction
	,TotalSwipe
	,TotalDip
	,SettlementDateTime
	,ICCID
	--,TowerNumber
	--,MCC_MNC_Data
	--,LAC_Data
	--,Cell_ID_Data
	)
OUTPUT inserted.ID INTO @tempID(ID)
VALUES (@sTerminalID
	,@iDatabaseID
	,getdate()
	,@sRawMsg
	,@sProcCode
	,@sSerialNumber
	,@sSoftwareVersion
	,@iLifetimeCounter
	,@iTotalAllTransaction
	,@iTotalSwipe
	,@iTotalDip
	,@sSettlementDateTime
	,@sICCID
	--,@TowerNumber
	--,@Mcc_Mnc_Data
	--,@Lac_Data
	--,@Cell_Id_Data
	)

SELECT @iID_Header=ID FROM @tempID