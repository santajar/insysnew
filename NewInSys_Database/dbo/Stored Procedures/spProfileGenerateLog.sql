﻿
CREATE PROCEDURE [dbo].[spProfileGenerateLog]
		@iDbID INT,
		@sTID VARCHAR(8),
		@sTag VARCHAR(3),
		@sAcqIssName VARCHAR(50),
		@sContent VARCHAR(MAX),
		@isEdit BIT
AS
DECLARE @sStmt VARCHAR(MAX),
		@sTableName VARCHAR(20)

--SET @sTID = 'DNA317RT'
--SET @iDbID = 83
--SET @sTag = 'DE'
--SET @sTableName = 'Terminal'
--SET @sAcqIssName = 'BCA'
--SET @isEdit = 1
--SET @sContent = 'DE0108DNA317NLDE02011DE030205'

IF @sTag = 'DE'
	SET @sTableName = 'Terminal'
ELSE IF @sTag = 'AA'
	SET @sTableName = 'Acquirer'
ELSE IF @sTag = 'AE'
	SET @sTableName = 'Issuer'
ELSE 
	SET @sTableName = ''

IF (@sTag <> '')
BEGIN
	IF @isEdit = 0
	BEGIN
		SET @sStmt = '
		SELECT ' + @sTableName + 'Tag, ItemName, Isnull('+@sTableName+'TagValue,'''') TagValue
		FROM (
		SELECT '+@sTableName +'Tag, dbo.Bit2Str( '+ CONVERT(VARCHAR,@iDbID) +', ' + @sTableName +'Tag, '+
									@sTableName+'TagValue) '+ @sTableName+'TagValue 
		FROM tbProfile'+@sTableName +' WITH (NOLOCK)  
		WHERE TerminalID = ''' + @sTID + ''''

		IF (@sTag <> 'DE')	
			SET  @sStmt = @sStmt + ' AND '+@sTableName+'Name = '''+@sAcqIssName+''' '
		SET  @sStmt = @sStmt + '
		) a 
			FULL OUTER JOIN (
		SELECT ItemName, Tag
		FROM tbItemList WITH (NOLOCK)
		WHERE DatabaseID = ' + convert(varchar,@iDbID) + ' and tag like ''' + @sTag + '%''
		) b 
			ON a.'+@sTableName +'Tag = b.Tag'

		EXEC (@sStmt) 
	END
	ELSE
	BEGIN
		SET @sStmt = 'SELECT c.Tag, ItemName, dbo.Bit2Str(' + CONVERT(VARCHAR,@iDbID) + ',  c.Tag, '
											  + @sTableName + 'TagValue) OldValue, '+
											  ' dbo.Bit2Str(' + CONVERT(VARCHAR,@iDbID) + ',  c.Tag, ' 
											  + 'TagValue) NewValue
					  FROM ( SELECT TerminalID, '+ @sTableName + 'Tag, '+ @sTableName + 'TagValue
							 FROM tbProfile'+ @sTableName + ' WITH (NOLOCK)
							 WHERE TerminalID = ''' + @sTID + ''''
		IF (@sTag <> 'DE')
			SET @sStmt = @sStmt + ' AND ' + @sTableName + 'Name = '''+@sAcqIssName+''' '
						   
		IF (dbo.isTagLength4(@iDbID)=4)
		BEGIN
		SET @sStmt = @sStmt + '
							) A
						FULL OUTER JOIN 
							( SELECT Tag, TagValue
							  FROM dbo.fn_tbProfileTLV(''' + @sTID + ''', '''+ @sContent + ''')
							) B
						 ON A.' + @sTableName + 'Tag = B.Tag
						FULL OUTER JOIN 
							( SELECT ItemName, Tag
							  FROM tbItemList WITH (NOLOCK)
							  WHERE DatabaseID = ' + CONVERT(VARCHAR,@iDbID) + ' AND Tag LIKE ''' + @sTag + '%''
							) C
						 ON A.' + @sTableName + 'Tag = C.Tag
						WHERE ' + @sTableName + 'TagValue <> TagValue
					'
		END
		ELSE
		BEGIN
			SET @sStmt = @sStmt + '
					) A
				FULL OUTER JOIN 
					( SELECT Tag, TagValue
						FROM dbo.fn_tbProfileTLV5(''' + @sTID + ''', '''+ @sContent + ''')
					) B
					ON A.' + @sTableName + 'Tag = B.Tag
				FULL OUTER JOIN 
					( SELECT ItemName, Tag
						FROM tbItemList WITH (NOLOCK)
						WHERE DatabaseID = ' + CONVERT(VARCHAR,@iDbID) + ' AND Tag LIKE ''' + @sTag + '%''
					) C
					ON A.' + @sTableName + 'Tag = C.Tag
				WHERE ' + @sTableName + 'TagValue <> TagValue
			'
		END
		EXEC (@sStmt)
	END
END