﻿CREATE PROCEDURE [dbo].[spAppPackageAdd]
	@sAppPackageName VARCHAR(50),
	@sAppPackagePath VARCHAR(150),
	@sAppPackageFilename VARCHAR(50),
	@sAppPackageDesc VARCHAR(MAX),
	@iDatabaseID INT,
	@sEdcTypeId VARCHAR(MAX) = NULL,
	@iSuccess BIT OUTPUT
AS
	SET NOCOUNT ON
DECLARE @iCount INT

SELECT @iCount = COUNT(AppPackageName) 
FROM tbAppPackageEDCList WITH (NOLOCK)
WHERE AppPackageName = @sAppPackageName

IF @iCount = 0
BEGIN
	DECLARE @bAppPackageContent VARBINARY(MAX),
		@sQuery NVARCHAR(MAX),
		@sFilename NVARCHAR(MAX)		
	SET @sFilename = @sAppPackagePath + @sAppPackageFilename	
	SET @sQuery = '(SELECT @bAppPackage=BulkColumn 
				   FROM OPENROWSET(BULK ''' + @sFilename + ''', SINGLE_BLOB) AS a)'

	--SELECT @bAppPackageContent=BulkColumn 
	--FROM OPENROWSET(BULK @sAppPackagePath, SINGLE_BLOB) AS A

	EXEC sp_executesql @sQuery, 
		N'@bAppPackage VARBINARY(MAX) OUTPUT', 
		@bAppPackage = @bAppPackageContent OUTPUT

	INSERT INTO tbAppPackageEDCList(
		AppPackageName,
		AppPackageFilename,
		AppPackageContent,
		AppPackageDesc,
		EdcTypeId,
		UploadTime
	)
	VALUES(
		@sAppPackageName,
		@sAppPackageFilename,
		@bAppPackageContent,
		@sAppPackageDesc,
		@sEdcTypeId,
		GETDATE()
	)
	SET @iSuccess = 1
END
ELSE
	SET @iSuccess = 0