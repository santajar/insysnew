﻿CREATE PROCEDURE [dbo].[spProfileRequestPaperReceiptInsert]
	@iDatabaseID INT,
	@sRequestPaperName VARCHAR(15),
	@sContent VARCHAR(MAX)=NULL
AS
	DECLARE @iCountName INT,
			@iCountID INT

	SELECT @iCountName = COUNT(*)
	FROM tbProfileReqPaperReceiptManagement WITH (NOLOCK)
	WHERE ReqName = @sRequestPaperName AND
		  DatabaseID = @iDatabaseID

	SELECT @iCountID = COUNT(*)
	FROM tbProfileReqPaperReceiptManagement WITH (NOLOCK)
	WHERE ReqTag IN ('RQ01','RQ001') AND
		  ReqTagValue = @sRequestPaperName AND
		  DatabaseID = @iDatabaseID

	IF @iCountName = 0
	BEGIN
		IF @iCountID = 0
		BEGIN
			IF @sContent IS NOT NULL
			BEGIN
				DECLARE @sRequestPaperReceipt VARCHAR(2)

				IF (DBO.isTagLength4(@iDatabaseID)=5)
				BEGIN
					INSERT INTO tbProfileReqPaperReceiptManagement(
						ReqName,
						DatabaseID,
						ReqTag,
						ReqLengthOfTagLength,
						ReqTagLength,
						ReqTagValue)
					SELECT 
						@sRequestPaperName,
						@iDatabaseID,
						Tag,
						LEN(TagLength),
						TagLength,
						TagValue
					FROM dbo.fn_tbprofiletlv5(@sRequestPaperName,@sContent)

					SELECT @sRequestPaperReceipt = ReqTagValue	
					FROM tbProfileReqPaperReceiptManagement WITH (NOLOCK)
					WHERE ReqName = @sRequestPaperName AND 
						  DatabaseID = @iDatabaseID AND 
						  ReqTag LIKE 'RQ001'
				END
				ELSE
				BEGIN
					INSERT INTO tbProfileReqPaperReceiptManagement(
						ReqName,
						DatabaseID,
						ReqTag,
						ReqLengthOfTagLength,
						ReqTagLength,
						ReqTagValue)
					SELECT 
						@sRequestPaperName,
						@iDatabaseID,
						Tag,
						LEN(TagLength),
						TagLength,
						TagValue
					FROM dbo.fn_tbprofiletlv(@sRequestPaperName,@sContent)

					SELECT @sRequestPaperReceipt = ReqTagValue	
					FROM tbProfileReqPaperReceiptManagement WITH (NOLOCK)
					WHERE ReqName = @sRequestPaperName AND 
						  DatabaseID = @iDatabaseID AND 
						  ReqTag LIKE 'RD01'
				END
				EXEC spItemComboBoxValueInsertRequestPaperReceipt @sRequestPaperName, @iDatabaseID

				DECLARE @sDatabaseID VARCHAR(3)
				SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
				EXEC spProfileTerminalUpdateLastUpdateByTagAndDbID @sDatabaseID,'Request Paper Receipt',@sRequestPaperName

			END
		END
		ELSE
			SELECT 'Request Paper Receipt ID has already used. Please choose other Request Paper Receipt ID.' 
	END
	ELSE
		SELECT 'IP Primary has already used. Please choose other IP Primary.'