﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Agt 27, 2015
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete CustomMenu table content
-- =============================================
Create PROCEDURE [dbo].[spProfileCustomMenuDelete]
	@sTerminalID VARCHAR(8)
AS
	DELETE FROM tbProfileCustomMenu 
		WHERE TerminalID=@sTerminalID