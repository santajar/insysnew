﻿CREATE PROCEDURE [dbo].[spRequestPaperReceiptView]
	-- Add the parameters for the stored procedure here
	@sTerminalID VARCHAR(8)
AS
BEGIN
	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @sTerminalID=''
	BEGIN
	SELECT  TOP 1000000 RequestID,TerminalID,Software,SerialNumber,RequestTime,ReceiptTime,NumberOfRequest,Status  FROM  tbProfileRequestPaper ORDER BY RequestTime DESC
	END
	ELSE
	BEGIN
	SELECT  TOP 1000000 RequestID,TerminalID,Software,SerialNumber,RequestTime,ReceiptTime,NumberOfRequest,Status  FROM  tbProfileRequestPaper WHERE TerminalID = @sTerminalID
	END
END