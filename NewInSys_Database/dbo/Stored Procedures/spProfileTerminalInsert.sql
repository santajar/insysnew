﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 16, 2010
-- Modify date: 
--				1. August 12, 2010, change variable name from @sLastView to @bLastView
-- Description:	
--				1. Insert new terminal into database (tbProfileTerminal and tbProfileTerminalList)
--				2. Add Validation of Tag length 
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalInsert]
	@sTerminalID VARCHAR(8),
	@sDbID INT,
	@bAllowDownload BIT,
	@bStatus BIT,
	@bEnableIP BIT,
	@sContent VARCHAR(MAX),
--	@bAutoInit BIT = '1',
	@bInitCompress BIT = '0',
	@bRemoteDownload BIT ='0',
	@bEMVInit BIT = '1'

AS
DECLARE @dDate DATETIME
	IF @bAllowDownload = '1' 
		SET @dDate = GETDATE()
	ELSE
		SET @dDate = NULL

	INSERT INTO tbProfileTerminalList
		(TerminalID, DatabaseID, AllowDownload, 
		 StatusMaster, EnableIP , InitCompress,RemoteDownload,EMVInit) 
	VALUES 
		(@sTerminalID, @sDbID,@bAllowDownload,
		 @bStatus, @bEnableIP ,@bInitCompress,@bRemoteDownload,@bEMVInit)

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
		WHERE Name = N'AutoInitTimeStamp' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	  ) 
	BEGIN
		UPDATE tbProfileTerminalList
		SET AutoInitTimeStamp = @dDate
		WHERE TerminalID = @sTerminalID
	END
	
	IF (dbo.isTagLength4(@sDbID)=4)
	BEGIN
	INSERT INTO tbProfileTerminal 
		(TerminalID, TerminalTag, TerminalLengthOfTagLength, TerminalTagLength, TerminalTagValue)
	SELECT TerminalID,
			Tag,
			LEN(TagLength),
			TagLength,
			TagValue
	FROM dbo.fn_TbProfileTLV(@sTerminalID,@sContent)
	END
	ELSE
	BEGIN
	INSERT INTO tbProfileTerminal 
		(TerminalID, TerminalTag, TerminalLengthOfTagLength, TerminalTagLength, TerminalTagValue)
	SELECT TerminalID,
			Tag,
			LEN(TagLength),
			TagLength,
			TagValue
	FROM dbo.fn_TbProfileTLV5(@sTerminalID,@sContent)
	END
	print 'ok'
	EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'DE', '', @sContent, 0
