﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AppPackageEDCListTempDelete]
	@sAppPackageName VARCHAR(150),
	@iAppPackId INT

AS
BEGIN
	SET NOCOUNT ON
	DECLARE @iCount int

	SELECT @iCount = COUNT(*)
	FROM tbAppPackageEDCListTemp WITH (NOLOCK)
	WHERE AppPackageName=@sAppPackageName 
		AND AppPackId = @iAppPackId

	IF (@iCount>0)
	BEGIN
		DELETE FROM tbAppPackageEDCListTemp
		WHERE AppPackageName=@sAppPackageName 
			AND AppPackId = @iAppPackId
	END


END