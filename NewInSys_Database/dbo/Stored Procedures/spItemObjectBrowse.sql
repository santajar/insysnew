﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 7, 2010
-- Description:	Get Form Item Object Database
-- =============================================
CREATE PROCEDURE [dbo].[spItemObjectBrowse]
AS
SELECT ObjectId,
	ObjectName
FROM tbItemObject
