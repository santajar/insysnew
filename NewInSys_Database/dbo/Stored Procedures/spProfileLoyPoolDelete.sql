﻿

CREATE PROCEDURE [dbo].[spProfileLoyPoolDelete]
	@iDatabaseId INT,
	@sLoyPoolName VARCHAR(50),
	@bDeleteData BIT = 1
AS
DELETE FROM tbProfileLoyPool
		WHERE DatabaseID = @iDatabaseId AND
			  LoyPoolName = @sLoyPoolName

