﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 7, 2010
-- Description:	Get Form Item Database
-- =============================================
CREATE PROCEDURE [dbo].[spItemListBrowse]
	@sCond VARCHAR(MAX)=NULL,
	@iDatabaseId INT = -1
AS

SELECT TOP (100) PERCENT dbo.tbItemList.ItemID, dbo.tbItemList.DatabaseID, dbo.tbItemList.FormID, dbo.tbItemList.ItemSequence, dbo.tbItemList.ItemName, 
                         dbo.tbItemObject.ObjectName, dbo.tbItemList.DefaultValue, dbo.tbItemList.vAllowNull, dbo.tbItemList.vUpperCase, dbo.tbItemList.vType, dbo.tbItemList.vMinLength, 
                         dbo.tbItemList.vMaxLength, dbo.tbItemList.vMinValue, dbo.tbItemList.vMaxValue, dbo.tbItemList.ValidationMsg, dbo.tbItemList.Tag, 
                         dbo.tbItemList.LengthofTagValueLength AS LengthOfTagLength, dbo.tbItemList.vMasking
                         INTO #TempViewItemList
FROM dbo.tbItemList WITH (NOLOCK) INNER JOIN
                         dbo.tbItemObject WITH (NOLOCK) ON dbo.tbItemList.ObjectID = dbo.tbItemObject.ObjectID ORDER BY dbo.tbItemList.DatabaseID, dbo.tbItemList.FormID, dbo.tbItemList.ItemSequence



DECLARE @sQuery VARCHAR(MAX)
SET @sQuery='SELECT ItemID,
					DatabaseID,
					FormID,
					ItemSequence,
					ItemName,	
					ObjectName,
					DefaultValue,
					LengthOfTagLength,
					vAllowNull,
					vUpperCase,
					vType,
					vMinLength,
					vMaxLength,
					vMinValue,
					vMaxValue,
					vMasking,
					ValidationMsg,
					Tag
			FROM #TempViewItemList '

IF @sCond IS NULL
	EXEC (@sQuery)
ELSE
	EXEC (@sQuery + @sCond)

DROP TABLE #TempViewItemList

