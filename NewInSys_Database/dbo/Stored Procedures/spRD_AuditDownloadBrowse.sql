﻿CREATE PROCEDURE [dbo].[spRD_AuditDownloadBrowse]
	@sCondition VARCHAR(5000)=''
AS
DECLARE @sQuery VARCHAR(500)
SET @sQuery = '
SELECT ROW_NUMBER() OVER(ORDER BY Id) [ID]
	,TerminalID
	,SerialNumber [Serial Number]
	,InstallDate [Install Date]
	,SV_NAME [Software Version]
	,FreeRAM [Free RAM]
	,TotalRAM[Total RAM]
	,[Filename]	
FROM tbRD_AuditDownload
'
EXEC (@sQuery + @sCondition)
