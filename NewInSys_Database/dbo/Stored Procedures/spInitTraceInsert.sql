--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInitTraceInsert]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [dbo].[spInitTraceInsert]
--GO


CREATE PROCEDURE spInitTraceInsert
	@iERRORCODE INT,
	@sTID VARCHAR(8),
	@sISO VARCHAR(MAX),
	@sTag VARCHAR(4),
	@sContent VARCHAR(MAX)
WITH ENCRYPTION
AS
	IF OBJECT_ID ('master.dbo.tbInitTrace') IS NULL
	BEGIN
		CREATE TABLE master.[dbo].[tbInitTrace](
			[Id] [bigint] IDENTITY(1,1) NOT NULL,
			[Date] [datetime] NULL,
			[ErrorCode] [smallint] NULL,
			[TerminalId] [varchar](8) NULL,
			[ISO] [varchar](max) NULL,
			[Tag] [varchar](4) NULL,
			[TerminalContent] [varchar](max) NULL,
		 CONSTRAINT [PK_tbInitTrace] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END

	INSERT INTO master.dbo.tbInitTrace
			   (Date
			   ,[ErrorCode]
			   ,[TerminalId]
			   ,[ISO]
			   ,[Tag]
			   ,[TerminalContent])
	VALUES ( GETDATE(),
			 @iERRORCODE,
			 @sTID,
			 @sISO,
			 @sTag,
			 @sContent )
