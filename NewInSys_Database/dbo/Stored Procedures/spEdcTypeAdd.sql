﻿
CREATE PROCEDURE [dbo].[spEdcTypeAdd]
	@sEdcTypeName VARCHAR(50),
	@sEdcTypeDesc VARCHAR(300) = NULL,
	@iSuccess BIT OUTPUT
AS
DECLARE @iCount INT
SELECT @iCount = COUNT(EdcTypeName) FROM tbEdcType
WHERE EdcTypeName = @sEdcTypeName
IF @iCount = 0
BEGIN
	INSERT INTO tbEdcType(
		EdcTypeName,
		EdcTypeDesc
	)
	VALUES(
		@sEdcTypeName,
		@sEdcTypeDesc
	)
	SET @iSuccess = 1
END
ELSE
	SET @iSuccess = 0