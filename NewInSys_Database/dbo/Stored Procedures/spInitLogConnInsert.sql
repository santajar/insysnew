﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 30, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating the initialization log percentage progress
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnInsert]
@sTerminalID CHAR(8)
AS

DECLARE @iPercentage INT
SET @iPercentage = dbo.iInitPercentage (dbo.iTotalLoop (@sTerminalID), dbo.iProcessRows(@sTerminalID))

INSERT INTO tbInitLogConn ([Time], TerminalID, [Description], Percentage)
VALUES (GETDATE(), @sTerminalID, dbo.sInitLogConnErrorMessage(@iPercentage),@iPercentage)