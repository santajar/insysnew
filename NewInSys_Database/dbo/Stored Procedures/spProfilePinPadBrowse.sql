﻿/*
-- Author		: Tobias SETYO
-- Created Date	: Oct 8, 2013
-- Modify Date	: 1. <mmm dd, yyyy>, <description>
-- Description	: browse tbProfilePinPad based on spesific condition
*/
CREATE PROCEDURE [dbo].[spProfilePinPadBrowse]
	@sCondition VARCHAR(500)=NULL
AS
DECLARE @sQuery VARCHAR(1000)
SET @sQuery = '
SELECT PinPadId,
	DatabaseID,
	PinPadName,
	PinPadTag [Tag],
	PinPadLengthOfTagLength,
	PinPadTagLength,
	PinPadTagValue [Value]
FROM tbProfilePinPad WITH (NOLOCK) '
IF ISNULL(@sCondition,'')=''
	EXEC (@sQuery)
ELSE
	EXEC (@sQuery + @sCondition)