﻿CREATE PROCEDURE [dbo].[spProfileSoftwareDelete] 
	@iDatabaseID INT
AS
	DELETE FROM tbProfileSoftware
	WHERE DatabaseID = @iDatabaseID 
