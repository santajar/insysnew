﻿-- =============================================
-- Author:		Kiki Ariady
-- Create date: Oct 7, 2010
-- Modify	  :
--				1. Mar 09, 2011, return log table to show the failed/success upload.
-- Description:	
--				1. Process the profile Data Upload
-- =============================================
CREATE PROCEDURE [dbo].[spDataUploadStartProcessAdd]
	@TerminalTable VARCHAR(150),
	@AcquirerTable VARCHAR(150),
	@sUser VARCHAR(30)
AS
SET DEADLOCK_PRIORITY NORMAL
SET NOCOUNT ON

DECLARE @sCurrTime DATETIME,
	@SqlStmt NVARCHAR(MAX)
SET @sCurrTime=CURRENT_TIMESTAMP

--IF OBJECT_ID('tbUploadTerminal') IS NOT NULL
--	DROP TABLE tbUploadTerminal
--SET @SqlStmt = 'SELECT * INTO tbUploadTerminal FROM ' + @TerminalTable

--EXEC sp_executesql @SqlStmt

--IF OBJECT_ID('tbUploadAcquirer') IS NOT NULL
--	DROP TABLE tbUploadAcquirer
--SET @SqlStmt = 'SELECT * INTO tbUploadAcquirer FROM ' + @AcquirerTable

--EXEC sp_executesql @SqlStmt

-- process upload terminal
EXEC spDataUploadProcessTerminalAdd @TerminalTable

--select * from tbUploadAcquirer
EXEC spDataUploadProcessAcquirer @AcquirerTable, @sUser, 1

--DROP TABLE tbUploadTerminal
--DROP TABLE tbUploadAcquirer

SELECT Id, [Time], TerminalId, Description, Remarks
FROM tbUploadLog WITH (NOLOCK)
WHERE [Time]>@sCurrTime