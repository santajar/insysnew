﻿
-- ============================================================
-- Description:	 Insert new CAPK into TbProfileCAPK with different name
-- Ibnu Saefullah
-- Modify : 12 Mei 2015, Copy CAPK with default and Custom CAPK Name
-- ============================================================

CREATE PROCEDURE [dbo].[spProfilePromoManagementCopy]
	@iDatabaseID VARCHAR(3),
	@sOldPromoManagementName VARCHAR(50),
	@sNewPromoManagementName VARCHAR(50)

AS
Declare
	@Content VARCHAR(MAX)=NULL,
	@iCount INT,
	@iIndex INT

SELECT @iCount= COUNT(*)
FROM tbProfilePromoManagement WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID
	AND PromoManagementName = @sNewPromoManagementName


IF @iCount = 0
BEGIN
	INSERT INTO tbProfilePromoManagement(PromoManagementName, DatabaseID, PromoManagementTag ,PromoManagementLength, PromoManagementTagLength, PromoManagementTagValue, Description)
	SELECT @sNewPromoManagementName, DatabaseID, PromoManagementTag ,PromoManagementLength, PromoManagementTagLength, PromoManagementTagValue, Description
	FROM tbProfilePromoManagement WITH (NOLOCK)
	WHERE PromoManagementName = @sOldPromoManagementName AND DatabaseId=@iDatabaseID
	
	SET @iIndex=CHARINDEX('-',@sOldPromoManagementName)
	 
	IF (@iIndex = 0)
	BEGIN
		 UPDATE tbProfilePromoManagement
		 SET PromoManagementTagValue = @sNewPromoManagementName, PromoManagementTagLength = LEN(@sNewPromoManagementName)
		 WHERE PromoManagementTag IN ('PR001','PR01') AND PromoManagementName = @sNewPromoManagementName
	END
	ELSE
	BEGIN
		 UPDATE tbProfilePromoManagement
		 SET PromoManagementTagValue = SUBSTRING(@sNewPromoManagementName,0,@iIndex-1), PromoManagementTagLength = LEN(SUBSTRING(@sNewPromoManagementName,0,@iIndex-1))
		 WHERE PromoManagementTag IN ('PR001','PR01') AND PromoManagementName = @sNewPromoManagementName
	END
	 
END
ELSE
	SELECT 'Promo management Index Already Exist.'