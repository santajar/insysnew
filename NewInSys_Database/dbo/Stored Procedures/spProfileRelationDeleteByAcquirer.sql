﻿CREATE PROCEDURE [dbo].[spProfileRelationDeleteByAcquirer]
	@sTerminalID VARCHAR(8),
	@sMessage VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	EXEC spProfileRelationHistoryInsert @sTerminalID

	DELETE FROM TbProfileRelation
	WHERE TerminalID = @sTerminalID
IF(dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalID))=5)
BEGIN
	INSERT INTO TbProfileRelation
	(
		[TerminalID],
		[RelationTag],
		[RelationLengthOfTagLength],
		[RelationTagLength],
		[RelationtagValue]
	)
	SELECT
		terminalid,
		tag,
		len(taglength),
		taglength,
		tagvalue
	FROM dbo.Fn_TbProfileTLV5(@sTerminalID,@sMessage)
END
ELSE
BEGIN
	INSERT INTO TbProfileRelation
	(
		[TerminalID],
		[RelationTag],
		[RelationLengthOfTagLength],
		[RelationTagLength],
		[RelationtagValue]
	)
	SELECT
		terminalid,
		tag,
		len(taglength),
		taglength,
		tagvalue
	FROM dbo.Fn_TbProfileTLV(@sTerminalID,@sMessage)
END
END
