﻿
CREATE PROCEDURE [dbo].[spQueryReport] 
--	@sDateStart CHAR(20),
--	@sDateEnd CHAR(20)
AS	
--DECLARE @dtDateStart DATETIME,
--	@dtDateEnd DATETIME
--
--SET @dtDateStart = CONVERT(DATETIME,@sDateStart,120)
--SET @dtDateEnd = CONVERT(DATETIME,@sDateEnd,120)



IF (OBJECT_ID('TempXLS') IS NOT NULL)
BEGIN
	SELECT TerminalID
	INTO #terminallist
	FROM tbProfileTerminal 
	WHERE 
		TerminalTag='DC03' 
		AND ISNULL( TerminalTagValue,'') <> '' 
	--	AND dbo.dtGetLastUpdateDate(TerminalTagValue) BETWEEN @dtDateStart AND @dtDateEnd
		AND TerminalID IN (SELECT TerminalID FROM TempXLS)
	--	AND TerminalID IN (SELECT TerminalID From tbProfileTerminalList WHERE DatabaseID IN (2,17, 22, 75, 83, 69))

	SELECT *
	INTO #terminal
	FROM tbProfileTerminal
	WHERE TerminalID IN (SELECT TerminalID FROM #terminallist)

	
	SELECT * 
	INTO #acquirer
	FROM tbProfileAcquirer
	WHERE TerminalID IN (SELECT TerminalID FROM #terminallist)

	
	SELECT 
		a1.TerminalID,
		(CASE WHEN b1.TerminalTagValue=1 AND SUBSTRING(c1.AcquirerTagValue,3,6) = SUBSTRING(a1.TerminalID,3,6) THEN 1
		ELSE 0 END) Flazz
	INTO #flazz	
	FROM 
		#terminallist a1
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE43') b1 ON a1.TerminalID=b1.TerminalID
		LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='flazzbca' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID

	
	SELECT 
		a1.TerminalID,
		(CASE WHEN SUBSTRING(c1.AcquirerTagValue,3,6) = SUBSTRING(a1.TerminalID,3,6) THEN 1
		ELSE 0 END) BCA
	INTO #bca	
	FROM 
		#terminallist a1
		LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='BCA' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID

	
	SELECT 
		a1.TerminalID,
		(CASE WHEN b1.TerminalTagValue=1 AND SUBSTRING(c1.AcquirerTagValue,3,6) = SUBSTRING(a1.TerminalID,3,6) THEN 1
		ELSE 0 END) Loyalty
	INTO #loyalty	
	FROM 
		#terminallist a1
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE20') b1 ON a1.TerminalID=b1.TerminalID
		LEFT JOIN (SELECT * FROM #acquirer WHERE AcquirerName='LOYALTY' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID

	
	SELECT 
		a1.TerminalID,
		(CASE WHEN b1.TerminalTagValue=1 AND SUBSTRING(c1.AcquirerTagValue,3,6) = SUBSTRING(a1.TerminalID,3,6) THEN 1
		ELSE 0 END) PromoA
	INTO #promoA
	FROM 
		#terminallist a1
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE21') b1 ON a1.TerminalID=b1.TerminalID
		left JOIN (SELECT * FROM #acquirer WHERE AcquirerName in ('Promo A', 'Promo SQ A') AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID

	
	SELECT 
		a1.TerminalID,
		(CASE WHEN b1.TerminalTagValue=1 AND SUBSTRING(c1.AcquirerTagValue,3,6) = SUBSTRING(a1.TerminalID,3,6) THEN 1
		ELSE 0 END) PromoOther
	INTO #promoOther
	FROM 
		#terminallist a1
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE21') b1 ON a1.TerminalID=b1.TerminalID
		LEFT JOIN (
			SELECT * FROM #acquirer 
			WHERE AcquirerName in ('Promo B', 'Promo SQ B', 'Promo C', 'Promo SQ C','Promo D', 'Promo SQ D','Promo E', 'Promo SQ E') 
			AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID

	
	SELECT 
		a.TerminalID,
		(CASE WHEN a.Flazz=1 AND b.TerminalTagValue=1 AND c.TerminalTagValue=1 THEN 1
		ELSE 0 END) TopUp
	INTO #topup
	FROM #flazz a 
		left JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE46') b ON a.TerminalID=b.TerminalID
		left JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE87') c ON a.TerminalID=c.TerminalID

	
	SELECT 
		a.TerminalID,
		(CASE WHEN a.Flazz=1 AND b.TerminalTagValue=1 THEN 1
		ELSE 0 END) Payment
	INTO #payment
	FROM #flazz a 
		LEFT JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE45') b ON a.TerminalID=b.TerminalID


	SELECT 
		a1.TerminalID,
		(CASE WHEN SUBSTRING(c1.AcquirerTagValue,3,6) = SUBSTRING(a1.TerminalID,3,6) THEN 1
		ELSE 0 END) AMEX
	INTO #amex	
	FROM 
		#terminallist a1
		LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='AMEX' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID

	SELECT 
		a1.TerminalID,
		(CASE WHEN SUBSTRING(c1.AcquirerTagValue,3,6) = SUBSTRING(a1.TerminalID,3,6) THEN 1
		ELSE 0 END) DINERS
	INTO #diners	
	FROM 
		#terminallist a1
		LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='DINERS' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID

	
	SELECT 
		a1.TerminalID,
		(CASE WHEN SUBSTRING(c1.AcquirerTagValue,3,6) = SUBSTRING(a1.TerminalID,3,6) THEN 1
		ELSE 0 END) DEBIT
	INTO #debit	
	FROM 
		#terminallist a1
		LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='DEBIT' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID

	
	SELECT 
		a.TerminalID,
		b.TerminalTagValue MerchantName,
		c.TerminalTagValue Alamat1,
		d.TerminalTagValue Alamat2,
		e.Flazz,
		j.TopUp,
		k.Payment,
		f.BCA BCACard,
		f.BCA Visa,
		f.BCA [Master],
		l.AMEX,
		m.DINERS,
		g.Loyalty Reward,
		h.PromoA Cicilan,
		i.PromoOther CicilanPr,
		n.DEBIT,
		o.TUNAI,
		p.TerminalTagValue [F2.Pwd],
		q.TerminalTagValue AdminPwd,
		r.TerminalTagValue InitPwd
	INTO #result
		FROM 
		#terminallist a 
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE02') b ON a.TerminalID=b.TerminalID
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE03') c ON a.TerminalID=c.TerminalID
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE04') d ON a.TerminalID=d.TerminalID
		JOIN #flazz e ON a.TerminalID=e.TerminalID
		JOIN #topup j ON a.TerminalID=j.TerminalID
		JOIN #payment k ON a.TerminalID=k.TerminalID
		JOIN #bca f ON a.TerminalID=f.TerminalID
		JOIN #amex l ON a.TerminalID=l.TerminalID
		JOIN #diners m ON a.TerminalID=m.TerminalID
		JOIN #loyalty g ON a.TerminalID=g.TerminalID
		JOIN (SELECT distinct TerminalID, PromoA FROM #promoA) h ON a.TerminalID=h.TerminalID
		JOIN (SELECT distinct TerminalID, PromoOther FROM #promoOther) i ON a.TerminalID=i.TerminalID
		JOIN #debit n ON a.TerminalID=n.TerminalID
		JOIN (SELECT TerminalID, (CASE WHEN TerminalTagValue='SLCS' THEN 1 ELSE 0 END) TUNAI FROM #terminal WHERE TerminalTag='DE05') o ON a.TerminalID=o.TerminalID
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE25') p ON a.TerminalID=p.TerminalID
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE28') q ON a.TerminalID=q.TerminalID
		JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE42') r ON a.TerminalID=r.TerminalID
	--
	
	SELECT 
		a.TerminalID,
		(CASE WHEN a.BCACard=1 THEN d.AcquirerTagValue ELSE null END) T1_BCA,
		(CASE WHEN a.BCACard=1 THEN e.AcquirerTagValue ELSE null END) T2_BCA,
		(CASE WHEN a.Reward=1 THEN f.AcquirerTagValue ELSE null END) T1_Reward,
		(CASE WHEN a.Reward=1 THEN g.AcquirerTagValue ELSE null END) T2_Reward,
		(CASE WHEN a.DEBIT=1 THEN h.AcquirerTagValue ELSE null END) T1_Debit,
		(CASE WHEN a.DEBIT=1 THEN i.AcquirerTagValue ELSE null END) T2_Debit,
		(CASE WHEN a.Cicilan=1 THEN j.AcquirerTagValue ELSE null END) T1_Cicilan,
		(CASE WHEN a.Cicilan=1 THEN k.AcquirerTagValue ELSE null END) T2_Cicilan,
		(CASE WHEN a.CicilanPr=1 THEN l.AcquirerTagValue ELSE null END) T1_CicilanPrB,
		(CASE WHEN a.CicilanPr=1 THEN m.AcquirerTagValue ELSE null END) T2_CicilanPrB,
		(CASE WHEN a.CicilanPr=1 THEN n.AcquirerTagValue ELSE null END) T1_CicilanPrC,
		(CASE WHEN a.CicilanPr=1 THEN o.AcquirerTagValue ELSE null END) T2_CicilanPrC,
		(CASE WHEN a.CicilanPr=1 THEN p.AcquirerTagValue ELSE null END) T1_CicilanPrD,
		(CASE WHEN a.CicilanPr=1 THEN q.AcquirerTagValue ELSE null END) T2_CicilanPrD,
		(CASE WHEN a.CicilanPr=1 THEN r.AcquirerTagValue ELSE null END) T1_CicilanPrE,
		(CASE WHEN a.CicilanPr=1 THEN s.AcquirerTagValue ELSE null END) T2_CicilanPrE,
		(CASE WHEN a.Cicilan=1 THEN t.AcquirerTagValue ELSE null END) T1_CicilanSQ,
		(CASE WHEN a.Cicilan=1 THEN u.AcquirerTagValue ELSE null END) T2_CicilanSQ,
		(CASE WHEN a.CicilanPr=1 THEN v.AcquirerTagValue ELSE null END) T1_CicilanSQPrB,
		(CASE WHEN a.CicilanPr=1 THEN w.AcquirerTagValue ELSE null END) T2_CicilanSQPrB,
		(CASE WHEN a.CicilanPr=1 THEN x.AcquirerTagValue ELSE null END) T1_CicilanSQPrC,
		(CASE WHEN a.CicilanPr=1 THEN y.AcquirerTagValue ELSE null END) T2_CicilanSQPrC,
		(CASE WHEN a.CicilanPr=1 THEN z.AcquirerTagValue ELSE null END) T1_CicilanSQPrD,
		(CASE WHEN a.CicilanPr=1 THEN aa.AcquirerTagValue ELSE null END) T2_CicilanSQPrD,
		(CASE WHEN a.CicilanPr=1 THEN ab.AcquirerTagValue ELSE null END) T1_CicilanSQPrE,
		(CASE WHEN a.CicilanPr=1 THEN ac.AcquirerTagValue ELSE null END) T2_CicilanSQPrE
	INTO #resultPhone
	FROM 
		#result a
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='BCA' AND AcquirerTag='AA06') d ON a.TerminalID=d.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='BCA' AND AcquirerTag='AA07') e ON a.TerminalID=e.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='LOYALTY' AND AcquirerTag='AA06') f ON a.TerminalID=f.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='LOYALTY' AND AcquirerTag='AA07') g ON a.TerminalID=g.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='DEBIT' AND AcquirerTag='AA06') h ON a.TerminalID=h.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='DEBIT' AND AcquirerTag='AA07') i ON a.TerminalID=i.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO A' AND AcquirerTag='AA06') j ON a.TerminalID=j.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO A' AND AcquirerTag='AA07') k ON a.TerminalID=k.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO B' AND AcquirerTag='AA06') l ON a.TerminalID=l.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO B' AND AcquirerTag='AA07') m ON a.TerminalID=m.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO C' AND AcquirerTag='AA06') n ON a.TerminalID=n.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO C' AND AcquirerTag='AA07') o ON a.TerminalID=o.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO D' AND AcquirerTag='AA06') p ON a.TerminalID=p.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO D' AND AcquirerTag='AA07') q ON a.TerminalID=q.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO E' AND AcquirerTag='AA06') r ON a.TerminalID=r.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO E' AND AcquirerTag='AA07') s ON a.TerminalID=s.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ A' AND AcquirerTag='AA06') t ON a.TerminalID=t.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ A' AND AcquirerTag='AA07') u ON a.TerminalID=u.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ B' AND AcquirerTag='AA06') v ON a.TerminalID=v.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ B' AND AcquirerTag='AA07') w ON a.TerminalID=w.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ C' AND AcquirerTag='AA06') x ON a.TerminalID=x.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ C' AND AcquirerTag='AA07') y ON a.TerminalID=y.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ D' AND AcquirerTag='AA06') z ON a.TerminalID=z.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ D' AND AcquirerTag='AA07') aa ON a.TerminalID=aa.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ E' AND AcquirerTag='AA06') ab ON a.TerminalID=ab.TerminalID
		left JOIN (SELECT TerminalID, AcquirerTagValue FROM #acquirer WHERE AcquirerName='PROMO SQ E' AND AcquirerTag='AA07') ac ON a.TerminalID=ac.TerminalID
	--
	SELECT distinct
		a.TerminalID,
		(CASE WHEN b.MID IS NULL THEN '-' ELSE b.MID END)MID,
		a.MerchantName,
		a.Alamat1,
		a.Alamat2,
		a.Flazz,
		a.TopUp,
		a.Payment,
		a.BCACard,
		a.Visa,
		a.[Master],
		a.AMEX,
		a.DINERS,
		a.Reward,
		a.Cicilan,
		a.CicilanPr,
		a.DEBIT,
		a.TUNAI,
		c.AcquirerTagValue,
		(CASE WHEN c.AcquirerTagValue IS NULL THEN '0' ELSE c.AcquirerTagValue END) MasterDial,
		a.[F2.Pwd],
		a.AdminPwd,
		a.InitPwd,
		d.T1_BCA,
		d.T2_BCA,
		d.T1_Reward,
		d.T2_Reward,
		d.T1_Debit,
		d.T2_Debit,
		d.T1_Cicilan,
		d.T2_Cicilan,
		d.T1_CicilanPrB,
		d.T2_CicilanPrB,
		d.T1_CicilanPrC,
		d.T2_CicilanPrC,
		d.T1_CicilanPrD,
		d.T2_CicilanPrD,
		d.T1_CicilanPrE,
		d.T2_CicilanPrE,
		d.T1_CicilanSQ,
		d.T2_CicilanSQ,
		d.T1_CicilanSQPrB,
		d.T2_CicilanSQPrB,
		d.T1_CicilanSQPrC,
		d.T2_CicilanSQPrC,
		d.T1_CicilanSQPrD,
		d.T2_CicilanSQPrD,
		d.T1_CicilanSQPrE,
		d.T2_CicilanSQPrE
	FROM 
		#result a
		LEFT OUTER JOIN (
			SELECT mid.TerminalID, mid.MID, mid.AcquirerName
			FROM (
				SELECT a.TerminalID, RIGHT(a.AcquirerTagValue,9) MID, a.AcquirerName
				FROM #acquirer a JOIN #bca b ON a.TerminalID=b.TerminalID AND a.AcquirerName='BCA'
				WHERE a.AcquirerTag='AA04'
				union all
				SELECT a.TerminalID, RIGHT(a.AcquirerTagValue,9) MID, a.AcquirerName
				FROM #acquirer a JOIN #loyalty b ON a.TerminalID=b.TerminalID AND a.AcquirerName='LOYALTY'
				WHERE a.AcquirerTag='AA04'
				union all
				SELECT a.TerminalID, RIGHT(a.AcquirerTagValue,9) MID, a.AcquirerName
				FROM #acquirer a JOIN #debit b ON a.TerminalID=b.TerminalID AND a.AcquirerName='DEBIT'
				WHERE a.AcquirerTag='AA04'
				) mid 
				JOIN #bca bca ON mid.TerminalID=bca.TerminalID
				JOIN #loyalty loyalty ON mid.TerminalID=loyalty.TerminalID
				JOIN #debit debit ON mid.TerminalID=debit.TerminalID
			WHERE 
				mid.AcquirerName=COALESCE(
					CASE WHEN bca.BCA=1 THEN 'BCA' END,
					CASE WHEN loyalty.LOYALTY=1 THEN 'LOYALTY'
					ELSE 'DEBIT' END
				)
			) b ON a.TerminalID=b.TerminalID
		LEFT OUTER JOIN (SELECT TerminalID, AcquirerTagValue, AcquirerName FROM #acquirer WHERE AcquirerTag='AA35') c 
			ON a.TerminalID=c.TerminalID AND c.AcquirerName=b.AcquirerName
		JOIN #resultPhone d ON a.TerminalID=d.TerminalID
END
--IF OBJECT_ID('tempdb..#terminal') IS NOT NULL
	DROP TABLE #terminal
--IF OBJECT_ID('tempdb..#terminallist') IS NOT NULL
	DROP TABLE #terminallist
--IF OBJECT_ID('tempdb..#acquirer') IS NOT NULL
		DROP TABLE #acquirer
--IF OBJECT_ID('tempdb..#flazz') IS NOT NULL
		DROP TABLE #flazz
--IF OBJECT_ID('tempdb..#bca') IS NOT NULL
		DROP TABLE #bca
--IF OBJECT_ID('tempdb..#loyalty') IS NOT NULL
		DROP TABLE #loyalty
--IF OBJECT_ID('tempdb..#promoA') IS NOT NULL
		DROP TABLE #promoA
--IF OBJECT_ID('tempdb..#promoOther') IS NOT NULL
		DROP TABLE #promoOther
--IF OBJECT_ID('tempdb..#topup') IS NOT NULL
		DROP TABLE #topup
--IF OBJECT_ID('tempdb..#payment') IS NOT NULL
		DROP TABLE #payment
--IF OBJECT_ID('tempdb..#amex') IS NOT NULL
		DROP TABLE #amex
--IF OBJECT_ID('tempdb..#diners') IS NOT NULL
		DROP TABLE #diners
--IF OBJECT_ID('tempdb..#debit') IS NOT NULL
		DROP TABLE #debit
--IF OBJECT_ID('tempdb..#result') IS NOT NULL
		DROP TABLE #result
--IF OBJECT_ID('tempdb..#resultPhone') IS NOT NULL
		DROP TABLE #resultPhone