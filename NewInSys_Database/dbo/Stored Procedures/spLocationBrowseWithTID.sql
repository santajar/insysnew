﻿
-- =============================================================
-- Description: Get Location ID for each TID in given DatabaseID
-- =============================================================

CREATE PROCEDURE [dbo].[spLocationBrowseWithTID] 
	@iDbID INT, @iLocationID INT
AS
IF OBJECT_ID ('TempDb..#terminal', 'U') IS NOT NULL
	DROP TABLE #terminal

SELECT * INTO #terminal
FROM dbo.fn_tbProfilingTerminal(@iDbID, @iLocationID)

IF OBJECT_ID ('TempDb..#acquirerbca', 'U') IS NOT NULL
	DROP TABLE #acquirerbca
SELECT * INTO #acquirerbca
FROM dbo.fn_tbProfilingAcquirer('BCA', @iDbID, @iLocationID)

IF OBJECT_ID ('TempDb..#acquirerbca2', 'U') IS NOT NULL
	DROP TABLE #acquirerbca2
SELECT * INTO #acquirerbca2
FROM dbo.fn_tbProfilingAcquirer('BCA2', @iDbID, @iLocationID)

IF OBJECT_ID ('TempDb..#acquirerloyalty', 'U') IS NOT NULL
	DROP TABLE #acquirerloyalty
SELECT * INTO #acquirerloyalty
FROM dbo.fn_tbProfilingAcquirer('LOYALTY', @iDbID, @iLocationID)

IF OBJECT_ID ('TempDb..#acquirerdebit', 'U') IS NOT NULL
	DROP TABLE #acquirerdebit
SELECT * INTO #acquirerdebit
FROM dbo.fn_tbProfilingAcquirer('DEBIT', @iDbID, @iLocationID)

IF OBJECT_ID ('TempDb..#acquirerflazzbca', 'U') IS NOT NULL
	DROP TABLE #acquirerflazzbca
SELECT * INTO #acquirerflazzbca
FROM dbo.fn_tbProfilingAcquirer('FLAZZBCA', @iDbID, @iLocationID)

SELECT a.*,
b.TID [TID BCA],b.MID [MID BCA],b.TrxPrimary [TrxPrimary BCA],b.TrxSecondary [TrxSecondary BCA],b.StlPrimary [StlPrimary BCA],b.StlSecondary [StlSecondary BCA],
c.TID [TID BCA2],c.MID [MID BCA2],c.TrxPrimary [TrxPrimary BCA2],c.TrxSecondary [TrxSecondary BCA2],c.StlPrimary [StlPrimary BCA2],c.StlSecondary [StlSecondary BCA2],
d.TID [TID LOYALTY],d.MID[MID LOYALTY],d.TrxPrimary [TrxPrimary LOYALTY],d.TrxSecondary [TrxSecondary LOYALTY],d.StlPrimary [StlPrimary LOYALTY],d.StlSecondary [StlSecondary LOYALTY],
e.TID [TID DEBIT],e.MID [MID DEBIT],b.TrxPrimary [TrxPrimary DEBIT],e.TrxSecondary [TrxSecondary DEBIT],e.StlPrimary [StlPrimary DEBIT],e.StlSecondary [StlSecondary DEBIT],
f.TID [TID FLAZZBCA],f.MID [MID FLAZZBCA],f.TrxPrimary [TrxPrimary FLAZZBCA],f.TrxSecondary [TrxSecondary FLAZZBCA],f.StlPrimary [StlPrimary FLAZZBCA],f.StlSecondary [StlSecondary FLAZZBCA]
FROM #terminal a LEFT JOIN #acquirerbca b ON a.TerminalId=b.TerminalId
LEFT JOIN #acquirerbca2 c ON a.TerminalId=c.TerminalId
LEFT JOIN #acquirerloyalty d ON a.TerminalId=d.TerminalId
LEFT JOIN #acquirerdebit e ON a.TerminalId=e.TerminalId
LEFT JOIN #acquirerflazzbca f ON a.TerminalId=f.TerminalId

