﻿
-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Return rows number on tbUploadTagDE which is not null
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagDESourceCount]
@sTemplate varchar(50)=NULL
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT COUNT(*) FROM tbUploadTagDE WITH (NOLOCK) WHERE SourceColumn IS NOT NULL and Template = @sTemplate

END