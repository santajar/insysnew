﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 30, 2010
-- Modify date:
--				1. 
-- Description:	Get The AllowInit Flag Control
-- =============================================
CREATE PROCEDURE [dbo].[spControlFlagAllowInitBrowse]
AS
SELECT Flag FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName='AllowInit'
