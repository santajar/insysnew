﻿


CREATE PROCEDURE [dbo].[spProfileLoyProdDelete]
	@iDatabaseId INT,
	@sLoyProdName VARCHAR(50),
	@bDeleteData BIT = 1
AS
DELETE FROM tbProfileLoyProd
		WHERE DatabaseID = @iDatabaseId AND
			  LoyProdName = @sLoyProdName


