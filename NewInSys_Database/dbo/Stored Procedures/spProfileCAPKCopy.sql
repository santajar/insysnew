﻿
-- ============================================================
-- Description:	 Insert new CAPK into TbProfileCAPK with different name
-- Ibnu Saefullah
-- Modify : 12 Mei 2015, Copy CAPK with default and Custom CAPK Name
-- ============================================================

CREATE PROCEDURE [dbo].[spProfileCAPKCopy]
	@iDatabaseID VARCHAR(3),
	@sOldCAPKName VARCHAR(50),
	@sNewCAPKName VARCHAR(50)

AS
Declare
	@Content VARCHAR(MAX)=NULL,
	@iCount INT,
	@iIndex INT

SELECT @iCount= COUNT(*)
FROM tbProfileCAPK WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID
	AND CAPKIndex = @sNewCAPKName


IF @iCount = 0
BEGIN
	INSERT INTO tbProfileCAPK(DatabaseId, CAPKIndex, CAPKTag ,CAPKLengthOfTagLength, CAPKTagLength, CAPKTagValue)
	SELECT DatabaseId, @sNewCAPKName, CAPKTag, CAPKLengthOfTagLength, CAPKTagLength, CAPKTagValue
	FROM tbProfileCAPK WITH (NOLOCK)
	WHERE CAPKIndex = @sOldCAPKName AND DatabaseId=@iDatabaseID
	
	SET @iIndex=CHARINDEX('-',@sNewCAPKName)
	 
	IF (@iIndex = 0)
	BEGIN
		 UPDATE tbProfileCAPK
		 SET CAPKTagValue = @sNewCAPKName, CAPKTagLength = LEN(@sNewCAPKName)
		 WHERE CAPKTag IN ('PK001','PK01') AND CAPKIndex = @sNewCAPKName
	END
	ELSE
	BEGIN
		 UPDATE tbProfileCAPK
		 SET CAPKTagValue = SUBSTRING(@sNewCAPKName,0,@iIndex-1), CAPKTagLength = LEN(SUBSTRING(@sNewCAPKName,0,@iIndex-1))
		 WHERE CAPKTag IN ('PK001','PK01') AND CAPKIndex = @sNewCAPKName
	END
	 
END
ELSE
	SELECT 'CAPK Index Already Exist.'