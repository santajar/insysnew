﻿
CREATE PROCEDURE [dbo].[spDataUploadAcquirerListBrowse]
	@iDatabaseId INT
AS
SELECT AcquirerName
FROM tbUploadAcquirerList WITH (NOLOCK)
WHERE DatabaseId=@iDatabaseId
ORDER BY tbUploadAcquirerList.Id

