﻿
-- =================================================================
-- Description:	Delete given DatabaseID (version) and all it's data
-- =================================================================

CREATE PROCEDURE [dbo].[spDatabaseVersionDeleteProcess]
	@sDatabaseID SMALLINT
AS
	EXEC spDatabaseVersionDeleteByTID @sDatabaseID
	EXEC spDatabaseVersionDeleteByDbID @sDatabaseID
	EXEC spProfileTerminalDbDelete @sDatabaseID

