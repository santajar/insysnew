﻿
CREATE PROCEDURE [dbo].[spProfileIssuerDeleteProcess]
	@sTerminalID VARCHAR(8),
	@sIssuer VARCHAR(15),
	@sRelation VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileRelationDeleteByIssuerOrCard @sTerminalID, @sRelation
	EXEC spProfileIssuerDelete @sTerminalID, @sIssuer

	EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
END
