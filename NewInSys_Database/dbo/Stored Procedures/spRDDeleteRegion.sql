﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,,16 April 2014>
-- Description:	<Description,,SP Insert Region Remote Download>
-- =============================================
CREATE PROCEDURE [dbo].[spRDDeleteRegion]
	@sName VARCHAR(150)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM tbRegionRemoteDownload
	WHERE RegionRDName = @sName
	
END