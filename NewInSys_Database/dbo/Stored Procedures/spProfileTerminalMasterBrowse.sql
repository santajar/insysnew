﻿
CREATE PROCEDURE [dbo].[spProfileTerminalMasterBrowse]
	@sTerminalId VARCHAR(8),
	@sMstTerminalId VARCHAR(8)=NULL OUTPUT
AS
SELECT @sMstTerminalId=MsTerminalId
FROM tbProfileTerminalListMaster WITH (NOLOCK)
WHERE TerminalId=@sTerminalId

