﻿
-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, Tedie, add template name
--				2. Sept 08, 2014, template default value
-- Description:	
--				1. Browse the acquirer itemname of the defined template
-- =============================================
CREATE PROCEDURE [dbo].[spUploadGetColumnsAA]
	@sTemplate VARCHAR(25)=NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ItemName
	FROM tbItemList WITH (NOLOCK)
	WHERE ISNULL(@sTemplate,'')<>'' AND EXISTS
	(
		SELECT ColumnName
		FROM tbUploadTagAA WITH (NOLOCK)
		WHERE ItemName = ColumnName
			AND Template = @sTemplate
	)
	GROUP BY ItemName
	ORDER BY MIN(ItemSequence)
END
