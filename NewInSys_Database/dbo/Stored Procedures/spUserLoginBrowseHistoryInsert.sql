﻿CREATE PROCEDURE [dbo].[spUserLoginBrowseHistoryInsert]
	@sUserRights VARCHAR(2000)
AS
	
	DECLARE @TempTable TABLE (UserTag VARCHAR(10),
							TagName VARCHAR(30),
							UserRight VARCHAR(200))
	
	DECLARE @iIndex INT
	SET @iIndex = 1
	DECLARE @sUserTag VARCHAR(5),
			@sUserRight VARCHAR(5),
			@sUserTagName VARCHAR(30),
			@sUserAction VARCHAR(200)
	DECLARE @iLen INT
	
	WHILE (@iIndex < LEN(@sUserRights))
	BEGIN
		SET @sUserTag = SUBSTRING(@sUserRights, @iIndex,2)
		SET @iIndex = @iIndex + 4

		SELECT @sUserTagName = TagName,
		@iLen = TagLen
		FROM tbUserLoginMap
		WHERE Tag = @sUserTag

		SET @sUserRight = SUBSTRING(@sUserRights, @iIndex, @iLen)
		
		EXEC spUserLoginGetActionDetail @sUserTag, @sUserRight, @sUserAction OUTPUT
	
		SET @iIndex = @iIndex + @iLen
		
		INSERT INTO @TempTable VALUES (@sUserTag, @sUserTagName, @sUserAction)		
	END
	
	SELECT UserTag, TagName, UserRight
	FROM @TempTable




