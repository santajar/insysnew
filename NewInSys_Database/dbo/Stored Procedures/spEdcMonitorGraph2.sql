﻿CREATE PROCEDURE [dbo].[spEdcMonitorGraph2]
AS
SELECT TerminalID, SUM(TotalAllTransaction) TotalTransaction 
FROM tbEdcMonitor 
WHERE DATEDIFF(day, MessageTimestamp, GETDATE()) <= 14 
GROUP BY TerminalID