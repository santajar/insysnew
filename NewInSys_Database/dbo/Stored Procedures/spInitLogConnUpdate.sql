﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 30, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating the initialization log percentage progress
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnUpdate] 
@sTerminalID VARCHAR(8)
AS
DECLARE @iPercentage INT
SET @iPercentage = dbo.iInitPercentage (dbo.iTotalLoop (@sTerminalID), dbo.iProcessRows(@sTerminalID))
DECLARE @sDate VARCHAR(30)
IF (@iPercentage = 0 OR @iPercentage = 100) 
	SELECT @sDate  = [Time] 
	FROM tbInitLogConn WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
ELSE
	SET @sDate = CONVERT(VARCHAR,GETDATE(),21)

UPDATE tbInitLogConn -- WITH (ROWLOCK)
	SET [Time] = @sDate,
		[Description] = dbo.sInitLogConnErrorMessage(@iPercentage),
		Percentage = @iPercentage
WHERE TerminalID = @sTerminalID
