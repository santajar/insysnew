﻿CREATE PROCEDURE [dbo].[spProfileRequestOrReceiptPaperInsert] 
	-- Add the parameters for the stored procedure here
	@iType INT,
	@sTerminalID varchar(8),
	@sSoftware varchar(15),
	@sSerialNumber varchar(25),
	@iNumberOfRequest INT = 0
	--@dRequest DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF @iType = 0
	BEGIN
    -- Insert statements for procedure here
	INSERT INTO tbProfileRequestPaper (TerminalID, Software, SerialNumber, RequestTime, NumberOfRequest,  [Status])
	VALUES (@sTerminalID, @sSoftware, @sSerialNumber, GETDATE(), @iNumberOfRequest,'WAITING')
	END
	ELSE 
	BEGIN
	UPDATE tbProfileRequestPaper
	SET [Status] = 'DONE',
	ReceiptTime = GETDATE()
	WHERE TerminalID = @sTerminalID AND SerialNumber = @sSerialNumber AND [Status] = 'WAITING'
	END
END

GO


