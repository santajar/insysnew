﻿CREATE PROCEDURE [dbo].[spBitmapBrowseCustom48]
	@sTerminalID VARCHAR(8),
	@iCustom48 BIT OUTPUT
AS
	SET NOCOUNT ON
SELECT @iCustom48=Custom48_1
FROM tbBitMap WITH(NOLOCK)
WHERE ApplicationName IN (
	SELECT SoftwareName 
	FROM tbProfileSoftware WITH(NOLOCK)
	WHERE DatabaseId IN (
		SELECT DatabaseId
		FROM tbProfileTerminalList WITH(NOLOCK)
		WHERE TerminalID=@sTerminalID
	)
)