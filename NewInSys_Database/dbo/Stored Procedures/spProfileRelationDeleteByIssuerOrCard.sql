﻿
-- =============================================
-- Author:		Kiky Ariady
-- Create date: Sept 17, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete relation of profile content
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationDeleteByIssuerOrCard]
	@sTerminalID VARCHAR(8),
	@sRelation VARCHAR(MAX)
AS
SET NOCOUNT ON;
	
	EXEC spProfileRelationHistoryInsert @sTerminalID

CREATE TABLE #Temp
(
	RelationtagID BIGINT,
	TerminalID VARCHAR(8),
	RelationTag VARCHAR(5),
	RelationLengthOfTagLength INT,
	RelationtagLength INT,
	TagValue VARCHAR(50),
	[Description] VARCHAR(50)
)
IF (dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalID))=5)
BEGIN
	INSERT INTO #Temp(RelationtagID,TerminalID,RelationTag,RelationLengthOfTagLength,RelationtagLength,TagValue,[Description])
	SELECT DISTINCT
		T.RelationtagID,
		T.TerminalID,
		T.RelationTag,
		T.RelationLengthOfTagLength,
		T.RelationtagLength,
		F.TagValue,
		T.Description
	FROM tbProfileRelation T WITH (NOLOCK)
		FULL OUTER JOIN dbo.fn_tbProfileTLV5(@sTerminalID, @sRelation) F
			ON
				T.TerminalID = F.TerminalID AND
				T.RelationTag = F.Tag AND
				T.RelationTagValue = F.TagValue
	WHERE T.TerminalID = @sTerminalID
END
ELSE
BEGIN
	INSERT INTO #Temp(RelationtagID,TerminalID,RelationTag,RelationLengthOfTagLength,RelationtagLength,TagValue,[Description])
	SELECT DISTINCT
		T.RelationtagID,
		T.TerminalID,
		T.RelationTag,
		T.RelationLengthOfTagLength,
		T.RelationtagLength,
		F.TagValue,
		T.Description
	FROM tbProfileRelation T WITH (NOLOCK)
		FULL OUTER JOIN dbo.fn_tbProfileTLV(@sTerminalID, @sRelation) F
			ON
				T.TerminalID = F.TerminalID AND
				T.RelationTag = F.Tag AND
				T.RelationTagValue = F.TagValue
	WHERE T.TerminalID = @sTerminalID
END

DELETE FROM tbProfileRelation
WHERE TerminalID = @sTerminalID

INSERT INTO tbProfileRelation
(
	[TerminalID],
	[RelationTag],
	[RelationLengthOfTagLength],
	[RelationTagLength],
	[RelationtagValue],
	[Description]
)
SELECT
	[TerminalID], 
	[RelationTag], 
	dbo.iConvertNull([TagValue], 1, [RelationLengthOfTagLength]),
	dbo.iConvertNull([TagValue], 0, [RelationtagLength]),
	[TagValue], 
	[Description]
FROM #Temp
ORDER BY RelationTagID

--DROP TABLE #Temp

EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
--IF OBJECT_ID('tempdb..#Temp') IS NOT NULL
	DROP TABLE #Temp