﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: SEPT 20, 2014
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Delete PRODUCT CODE
-- =============================================
CREATE PROCEDURE [dbo].[spProfileProductCodeDelete]
	@iDatabaseId INT,
	@sProductCode VARCHAR(50),
	@bDeleteData BIT = 1
AS
DELETE FROM tbProfileProductCode
WHERE DatabaseID = @iDatabaseId AND ProductCode = @sProductCode
