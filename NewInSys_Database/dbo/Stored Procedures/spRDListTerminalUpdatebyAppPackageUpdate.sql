﻿

CREATE PROCEDURE [dbo].[spRDListTerminalUpdatebyAppPackageUpdate]
	@iAppPackageDelete INT,
	@iAppPackageUpdate INT
AS
UPDATE tbListTIDRemoteDownload
SET AppPackID = @iAppPackageUpdate
WHERE AppPackID = @iAppPackageDelete