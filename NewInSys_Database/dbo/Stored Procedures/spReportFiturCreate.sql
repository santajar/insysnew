﻿CREATE PROCEDURE [dbo].[spReportFiturCreate]
AS
DECLARE TermIdCursor CURSOR FOR
	SELECT TerminalId FROM tbProfileTerminalList WITH (NOLOCK)
OPEN TermIdCursor

DECLARE @sTerminalId VARCHAR(8)

IF OBJECT_ID('NewInSys..tbReportFitur') IS NOT NULL
	DROP TABLE tbReportFitur

CREATE TABLE tbReportFitur(
	TerminalID VARCHAR(8),
	Merch1 VARCHAR(23),
	Merch2 VARCHAR(23),
	Merch3 VARCHAR(23),
	TID VARCHAR(8),
	MID VARCHAR(15),
	T1 VARCHAR(15),
	T2 VARCHAR(15)
)

FETCH NEXT FROM TermIdCursor INTO @sTerminalId
WHILE @@FETCH_STATUS=0
BEGIN	
	INSERT INTO tbReportFitur(
		TerminalId,
		TID,
		MID,
		Merch1,
		Merch2,
		Merch3,
		T1,
		T2)
	EXEC spFiturTerminal @sTerminalId

	FETCH NEXT FROM TermIdCursor INTO @sTerminalId
END

CLOSE TermIdCursor
DEALLOCATE TermIdCursor

--SELECT TerminalID TerminalInit, TID, MID, Merch1 MerchName1, Merch2 MerchName2, Merch3 MercName3, T1 TrxPhone1 , T2 TrxPhone2
--FROM tbReportFitur
