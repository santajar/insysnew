--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAuditTrailEncryptInsert]') AND type in (N'P', N'PC'))
--DROP PROCEDURE [dbo].[spAuditTrailEncryptInsert]
--GO

CREATE PROCEDURE [dbo].[spAuditTrailEncryptInsert]
	@sTerminalID VARCHAR(8),
	@sUserId VARCHAR(10),
	@sDatabaseName VARCHAR(50),
	@sActionDesc VARCHAR(MAX),
	@sActionDetail VARCHAR(MAX)=NULL
WITH ENCRYPTION
AS

IF OBJECT_ID ('master.dbo.tbAuditTrail') IS NULL
BEGIN
		CREATE TABLE master.[dbo].[tbAuditTrail](
			[LogID] [bigint] IDENTITY(1,1) NOT NULL,
			[AccessTime] [varchar](255) NOT NULL,
			[UserId] [varchar](10) NULL,
			[DatabaseName] [varchar](50) NULL,
			[ActionDescription] [varchar](255) NULL,
		 CONSTRAINT [PK_tbAuditTrail] PRIMARY KEY CLUSTERED 
		(
			[LogID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

END

IF OBJECT_ID ('master.dbo.tbAuditTrailDetail') IS NULL
BEGIN
	CREATE TABLE [master].[dbo].[tbAuditTrailDetail](
		[LogID] [bigint] NOT NULL,
		[Remarks] [varchar](3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	) ON [PRIMARY]
END

DECLARE @sAccessTime VARCHAR(MAX)
SET @sAccessTime = CONVERT(VARCHAR, CURRENT_TIMESTAMP, 9)

DECLARE @tablevar table (ID BIGINT);
INSERT INTO master.dbo.tbAuditTrail(
	AccessTime,
	UserId,
	DatabaseName,
	ActionDescription)
	OUTPUT INSERTED.logid INTO @tablevar	
VALUES(
	@sAccessTime,
	@sUserId,
	@sDatabaseName,
	@sActionDesc)

IF @sActionDetail IS NOT NULL
BEGIN
	DECLARE @iLogId BIGINT
	--SELECT  @iLogId = LogId FROM master.dbo.tbAuditTrail
	--	WHERE UserId=@sUserId AND DatabaseName=@sDatabaseName
	--		AND ActionDescription=@sActionDesc
	SELECT @iLogId=id from @tablevar			
	
	INSERT INTO master.dbo.tbAuditTrailDetail(
		LogId,
		Remarks)
	VALUES(
		@iLogId,
		@sActionDetail)
END

