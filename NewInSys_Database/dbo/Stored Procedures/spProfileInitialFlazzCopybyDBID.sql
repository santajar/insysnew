﻿-- =============================================
-- Author:		Tobias Setyo
-- Create date: Nov 14 2016
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileInitialFlazzCopybyDBID] 
	@NewDatabaseID VARCHAR(3),
	@OldDatabaseID VARCHAR(3),
	@sInitialFlazzName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @iCount INT,
			@iOldTagLength INT,
			@iNewTagLength INT
	
	SELECT @iCount = COUNT(*)
	FROM tbProfileInitialFlazz WITH (NOLOCK)
	WHERE DatabaseId = @NewDatabaseID AND InitialFlazzName = @sInitialFlazzName
	
	SET @iOldTagLength = dbo.isTagLength4(@OldDatabaseID)
	SET @iNewTagLength = dbo.isTagLength4(@NewDatabaseID)
	
	IF @iCount = 0
	BEGIN
		IF @iOldTagLength = @iNewTagLength
		BEGIN
			INSERT INTO tbProfileInitialFlazz(DatabaseId,InitialFlazzName, InitialFlazzTag,InitialFalzzLengthOfTagLength, InitialFlazzTagLength, InitialFlazzTagValue)
			SELECT @NewDatabaseID,InitialFlazzName, InitialFlazzTag,InitialFalzzLengthOfTagLength, InitialFlazzTagLength, InitialFlazzTagValue
			FROM tbProfileInitialFlazz WITH (NOLOCK)
			WHERE DatabaseId = @OldDatabaseID AND InitialFlazzName = @sInitialFlazzName
		END
		ELSE
		BEGIN
			IF @iOldTagLength > @iNewTagLength
			BEGIN
				INSERT INTO tbProfileInitialFlazz(DatabaseId,InitialFlazzName, InitialFlazzTag,InitialFalzzLengthOfTagLength, InitialFlazzTagLength, InitialFlazzTagValue)
				SELECT @NewDatabaseID,InitialFlazzName,SUBSTRING(InitialFlazzTag,1,2)+ SUBSTRING(InitialFlazzTag,4,2),InitialFalzzLengthOfTagLength,InitialFlazzTagLength,InitialFlazzTagValue
				FROM tbProfileInitialFlazz WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND InitialFlazzName = @sInitialFlazzName
			END
			ELSE
			BEGIN
				INSERT INTO tbProfileInitialFlazz(DatabaseId,InitialFlazzName, InitialFlazzTag,InitialFalzzLengthOfTagLength, InitialFlazzTagLength, InitialFlazzTagValue)
				SELECT @NewDatabaseID,InitialFlazzName,SUBSTRING(InitialFlazzTag,1,2) +'0'+SUBSTRING(InitialFlazzTag,3,2),InitialFalzzLengthOfTagLength,InitialFlazzTagLength,InitialFlazzTagValue
				FROM tbProfileInitialFlazz WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND InitialFlazzName = @sInitialFlazzName
			END
		END
	END
	ELSE
		SELECT 'InitialFlazzName  has already used. Please choose other InitialFlazzName Index.'
	
	
END