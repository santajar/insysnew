﻿



-- ===========================================================
-- Description: Update tbControlFlag for MaxConn, InitPort, 
--				and MaxConnFlag value
-- ===========================================================

CREATE PROCEDURE [dbo].[spControlFlagInitUpdate]
	@sMaxConn VARCHAR(50),
	@sInitPort VARCHAR(50),
	@sSoftwarePort VARCHAR(50),
	@sMaxConnFlag VARCHAR(64) = '',
	@sInitPortFlag VARCHAR(64) = '',
	@sSoftwarePortFlag VARCHAR(64) = ''
AS
	SET NOCOUNT ON
	SELECT * FROM dbo.fn_tbInitFlag (@sMaxConn, @sInitPort, @sSoftwarePort, 
									 @sMaxConnFlag, @sInitPortFlag, @sSoftwarePortFlag)

	EXEC spControlFlagUpdate @sMaxConn, @sMaxConnFlag
	EXEC spControlFlagUpdate @sInitPort, @sInitPortFlag
	EXEC spControlFlagUpdate @sSoftwarePort, @sSoftwarePortFlag





