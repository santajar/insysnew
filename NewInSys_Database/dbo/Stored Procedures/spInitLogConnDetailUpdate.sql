﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Sept 30, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating the initialization log percentage progress and time
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnDetailUpdate]
@sTerminalID CHAR(8),
@sTag VARCHAR(5)
AS
DECLARE @iPercentage INT
SET @iPercentage = dbo.iInitPercentage(dbo.iTotalRowsDetails(@sTerminalID, @sTag), dbo.iProcessRowsDetails(@sTerminalID, @sTag))
DECLARE @sDate VARCHAR(30)
IF (@iPercentage = 0 OR @iPercentage = 100) 
	SELECT @sDate  = [Time] FROM tbInitLogConnDetail WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID AND Tag = @sTag
ELSE
	SET @sDate = CONVERT(VARCHAR,GETDATE(),21)
UPDATE tbInitLogConnDetail
	SET [Time] = @sDate,	
	[Description] = dbo.sInitLogConnErrorMessage(@iPercentage),
	Percentage = @iPercentage  
WHERE TerminalID = @sTerminalID AND Tag = @sTag