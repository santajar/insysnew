﻿CREATE PROCEDURE [dbo].[spTIDRemoteDownload]
	@sTerminalID NVARCHAR(8),
	@bReturn BIT OUTPUT
AS
BEGIN
	DECLARE @sQuery NVARCHAR(MAX),
			@ParmDefinition NVARCHAR(500),
			@bRemoteDownload BIT,
			@sLinkDatabase NVARCHAR(50)

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

	set @sQuery = N'SELECT @sTerminalID = TerminalID
				  FROM '+@sLinkDatabase+'tbListTIDRemoteDownload
				  WITH(NOLOCK)
				  WHERE TerminalId=@sTerminalID'
exec @sQuery
	
	
END