﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. parse the received message and save it to global temp table
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseSaveTempResponse]
	@sSessionTime VARCHAR(MAX),
	@sEdcMessage VARCHAR(MAX),
	@sBitId VARCHAR(2),
	@iBitLength INT,
	@iOffset INT OUTPUT
AS
DECLARE @sSqlStmt VARCHAR(MAX)
DECLARE @sBitValue VARCHAR(MAX)
SET @sBitValue = dbo.sBitValue(@iBitLength,@iOffset,@sEdcMessage)
SET @sSqlStmt='INSERT INTO ##tempBitMap'+ @sSessionTime +'
	(BitId,BitValue) VALUES ('+ @sBitId +','''+ @sBitValue +''')'
EXEC (@sSqlStmt)
SET @iOffset = @iOffset + @iBitLength
