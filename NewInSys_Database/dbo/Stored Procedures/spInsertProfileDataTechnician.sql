﻿

-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: January 20, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spInsertProfileDataTechnician]
	@sIdKtp NVarchar(15),
	@sNama VARCHAR (50),
	@sHP NVARCHAR(15),
	@sServiePoint VARCHAR(30),
	@sVendorName VARCHAR(30),
	@sJenisKelamin VARCHAR(10)
	

AS
BEGIN

INSERT INTO tbProfileDataTechnician 
(IdKtp, Nama, HP, ServicePoint, VendorName, JenisKelamin)
VALUES
(@sIdKtp, @sNama, @sHP, @sServiePoint, @sVendorName, @sJenisKelamin)
END