﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. get the received processing code 
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseProcCode] 
	@sSessionTime VARCHAR(MAX),
	@sProcCode VARCHAR(6) OUTPUT
AS
SET NOCOUNT ON
DECLARE @sSqlStmt NVARCHAR(MAX)
DECLARE @sParamDef NVARCHAR(MAX)
SET @sParamDef = N'@sProcCode1 VARCHAR(6) OUTPUT'

SET @sSqlStmt = N'SELECT @sProcCode1=BitValue FROM ##tempBitMap' + @sSessionTime + 
	' WHERE BitId=3'
EXEC sp_executesql @sSqlStmt, @sParamDef, @sProcCode1=@sProcCode OUTPUT
RETURN @sProcCode
