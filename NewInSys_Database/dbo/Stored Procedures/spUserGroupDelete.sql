﻿
CREATE PROCEDURE [dbo].[spUserGroupDelete]
	@sGroupID VARCHAR(30)
AS
	DELETE FROM tbUserGroup
	WHERE GroupID = @sGroupID

