﻿
-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Okt 12, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spGetReportHeartBeat]
AS
	SET NOCOUNT ON

	--select * INTO #TempEdcMonitorActive from tbEdcMonitorTemp WHERE MessageTimestamp like '%' + SUBSTRING(CAST(GETDATE() as varchar), 0,12)+'%'
--select * INTO #TempEdcMonitorNotActive from tbEdcMonitorTemp WHERE TerminalID NOT IN (select TerminalID From #TempEdcMonitorActive)

IF OBJECT_ID('tempdb..#TempEdcHeartBeatActive') IS NOT NULL  DROP TABLE #TempEdcHeartBeatActive 
select * INTO #TempEdcHeartBeatActive from tbHeartBeat WHERE MessageTimestamp like '%' + SUBSTRING(CAST(GETDATE() as varchar), 0,12)+'%'

IF OBJECT_ID('tempdb..#TempHeartBeatCIMBN') IS NOT NULL  DROP TABLE #TempHeartBeatCIMBN 
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempHeartBeatCIMBN From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From #TempEdcHeartBeatActive) AND AcquirerName='CIMBN'
IF OBJECT_ID('tempdb..#TempHeartBeatCREDIT') IS NOT NULL  DROP TABLE #TempHeartBeatCREDIT 
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempHeartBeatCREDIT From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From #TempEdcHeartBeatActive) AND AcquirerName='CREDIT'
IF OBJECT_ID('tempdb..#TempHeartBeatDEBIT') IS NOT NULL  DROP TABLE #TempHeartBeatDEBIT 
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempHeartBeatDEBIT From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From #TempEdcHeartBeatActive) AND AcquirerName='DEBIT'
IF OBJECT_ID('tempdb..#TempHeartBeatCILTAP') IS NOT NULL  DROP TABLE #TempHeartBeatCILTAP 
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempHeartBeatCILTAP From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From #TempEdcHeartBeatActive) AND AcquirerName LIKE '%CILTAP%'

IF OBJECT_ID('tempdb..#TempResultCIMBN') IS NOT NULL  DROP TABLE #TempResultCIMBN 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultCIMBN FROM #TempHeartBeatCIMBN
Group By TerminalID

IF OBJECT_ID('tempdb..#TempResultCREDIT') IS NOT NULL  DROP TABLE #TempResultCREDIT 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultCREDIT FROM #TempHeartBeatCREDIT
Group By TerminalID

IF OBJECT_ID('tempdb..#TempResultDEBIT') IS NOT NULL  DROP TABLE #TempResultDEBIT 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultDEBIT FROM #TempHeartBeatDEBIT
Group By TerminalID

IF OBJECT_ID('tempdb..#TempResultCILTAP') IS NOT NULL  DROP TABLE #TempResultCILTAP 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultCILTAP FROM #TempHeartBeatCILTAP
Group By TerminalID




DELETE FROM #TempResultCREDIT WHERE TerminalID IN (SELECT TerminalID From #TempResultCIMBN)
DELETE FROM #TempResultDEBIT WHERE TerminalID IN (SELECT TerminalID From #TempResultCIMBN)
DELETE FROM #TempResultCILTAP WHERE TerminalID IN (SELECT TerminalID From #TempResultCIMBN)
DELETE FROM #TempResultDEBIT WHERE TerminalID IN (SELECT TerminalID From #TempResultCREDIT)
DELETE FROM #TempResultCILTAP WHERE TerminalID IN (SELECT TerminalID From #TempResultCREDIT)
DELETE FROM #TempResultCILTAP WHERE TerminalID IN (SELECT TerminalID From #TempResultDEBIT)

IF OBJECT_ID('tempdb..#tbTempTidMid') IS NOT NULL  
DROP TABLE #tbTempTidMid  
CREATE TABLE #tbTempTidMid
(TerminalID VARCHAR(8), TID VARCHAR(8), MID VARCHAR(20))

INSERT INTO #tbTempTidMid(TerminalID, TID, MID)
SELECT TerminalID, TID, MID FROM #TempResultCIMBN
INSERT INTO #tbTempTidMid(TerminalID, TID, MID)
SELECT TerminalID, TID, MID FROM #TempResultCREDIT
INSERT INTO #tbTempTidMid(TerminalID, TID, MID)
SELECT TerminalID, TID, MID FROM #TempResultDEBIT
INSERT INTO #tbTempTidMid(TerminalID, TID, MID)
SELECT TerminalID, TID, MID FROM #TempResultCILTAP

IF OBJECT_ID('tempdb..#tbTempResult') IS NOT NULL  DROP TABLE #tbTempResult 
CREATE TABLE #tbTempResult
(Tgl VARCHAR(20), CSI VARCHAR(8), TerminalID VARCHAR(8), MerchantID VARCHAR(20), SerialNumber VARCHAR(20), SoftwareVersion varchar(20), ICCID VARCHAR(25), Status VARCHAR(10))

INSERT INTO #tbTempResult (Tgl, CSI,TerminalID, MerchantID, SerialNumber,  SoftwareVersion, ICCID, Status)
select a.MessageTimestamp, a.TerminalID, b.TID, b.MID, a.SerialNumber, a.SoftwareVersion, a.ICCID, 'ACTIVE' 
FROM #TempEdcHeartBeatActive a left join #tbTempTidMid b  on a.TerminalID = b.TerminalID



---------------------------------GET DATA UNACTIVE------------------------------
IF OBJECT_ID('tempdb..#TempEdcHeartBeatNonActive') IS NOT NULL  DROP TABLE #TempEdcHeartBeatNonActive 
select * INTO #TempEdcHeartBeatNonActive from tbHeartBeat WHERE TerminalID NOT IN (select TerminalID From #TempEdcHeartBeatActive) AND MessageTimestamp not like '%' + SUBSTRING(CAST(GETDATE() as varchar), 0,12)+'%'

IF OBJECT_ID('tempdb..#TempEdcHeartBeatNonHeartBeat') IS NOT NULL  DROP TABLE #TempEdcHeartBeatNonHeartBeat 
select * INTO #TempEdcHeartBeatNonHeartBeat from tbProfileTerminalList WHERE TerminalID NOT IN (select TerminalID From #TempEdcHeartBeatActive) AND TerminalID NOT IN (select TerminalID From #TempEdcHeartBeatActive) AND DatabaseID=90

INSERT INTO #TempEdcHeartBeatNonActive(MessageTimestamp,TerminalID, DatabaseID) SELECT GETDATE(), TerminalID, DatabaseID From #TempEdcHeartBeatNonHeartBeat

IF OBJECT_ID('tempdb..#TempHeartBeatCIMBN_UNACTIVE') IS NOT NULL  DROP TABLE #TempHeartBeatCIMBN_UNACTIVE 
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempHeartBeatCIMBN_UNACTIVE From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From #TempEdcHeartBeatNonActive) AND AcquirerName='CIMBN'
IF OBJECT_ID('tempdb..#TempHeartBeatCREDIT_UNACTIVE') IS NOT NULL  DROP TABLE #TempHeartBeatCREDIT_UNACTIVE
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempHeartBeatCREDIT_UNACTIVE From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From #TempEdcHeartBeatNonActive) AND AcquirerName='CREDIT'
IF OBJECT_ID('tempdb..#TempHeartBeatDEBIT_UNACTIVE') IS NOT NULL  DROP TABLE #TempHeartBeatDEBIT_UNACTIVE
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempHeartBeatDEBIT_UNACTIVE From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From #TempEdcHeartBeatNonActive) AND AcquirerName='DEBIT'
IF OBJECT_ID('tempdb..#TempHeartBeatCILTAP_UNACTIVE') IS NOT NULL  DROP TABLE #TempHeartBeatCILTAP_UNACTIVE
SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue INTO #TempHeartBeatCILTAP_UNACTIVE From tbProfileAcquirer WHERE AcquirerTag in ('AA04','AA05') AND TerminalID IN (Select TerminalID From #TempEdcHeartBeatNonActive) AND AcquirerName LIKE '%CILTAP%'

IF OBJECT_ID('tempdb..#TempResultCIMBN_UNACTIVE') IS NOT NULL  DROP TABLE #TempResultCIMBN_UNACTIVE 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultCIMBN_UNACTIVE FROM #TempHeartBeatCIMBN_UNACTIVE
Group By TerminalID



IF OBJECT_ID('tempdb..#TempResultCREDIT_UNACTIVE') IS NOT NULL  DROP TABLE #TempResultCREDIT_UNACTIVE 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultCREDIT_UNACTIVE FROM #TempHeartBeatCREDIT_UNACTIVE
Group By TerminalID

IF OBJECT_ID('tempdb..#TempResultDEBIT_UNACTIVE') IS NOT NULL  DROP TABLE #TempResultDEBIT_UNACTIVE 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultDEBIT_UNACTIVE FROM #TempHeartBeatDEBIT_UNACTIVE
Group By TerminalID

IF OBJECT_ID('tempdb..#TempResultCILTAP_UNACTIVE') IS NOT NULL  DROP TABLE #TempResultCILTAP_UNACTIVE 
Select TerminalID,
MIN(Case AcquirerTag WHEN 'AA05' then AcquirerTagValue END) TID,
MIN(Case AcquirerTag WHEN 'AA04' then AcquirerTagValue END) MID
INTO #TempResultCILTAP_UNACTIVE FROM #TempHeartBeatCILTAP_UNACTIVE
Group By TerminalID


IF OBJECT_ID('tempdb..#tbTempTidMid_UNACTIVE') IS NOT NULL DROP TABLE #tbTempTidMid_UNACTIVE  
CREATE TABLE #tbTempTidMid_UNACTIVE
(TerminalID VARCHAR(8), TID VARCHAR(8), MID VARCHAR(20))
INSERT INTO #tbTempTidMid_UNACTIVE(TerminalID, TID, MID) SELECT TerminalID, TID, MID FROM #TempResultCIMBN_UNACTIVE
INSERT INTO #tbTempTidMid_UNACTIVE (TerminalID, TID, MID) SELECT TerminalID, TID, MID FROM #TempResultCREDIT_UNACTIVE
INSERT INTO #tbTempTidMid_UNACTIVE(TerminalID, TID, MID) SELECT TerminalID, TID, MID FROM #TempResultDEBIT_UNACTIVE
INSERT INTO #tbTempTidMid_UNACTIVE(TerminalID, TID, MID)SELECT TerminalID, TID, MID FROM #TempResultCILTAP_UNACTIVE


INSERT INTO #tbTempResult (Tgl, CSI,TerminalID, MerchantID, SerialNumber,  SoftwareVersion, ICCID, Status)
select a.MessageTimestamp as varchar, a.TerminalID, b.TID, b.MID, a.SerialNumber, a.SoftwareVersion, a.ICCID, 'UNACTIVE' 
FROM #TempEdcHeartBeatNonActive a left join #tbTempTidMid_UNACTIVE b  on a.TerminalID = b.TerminalID

select Tgl, CSI,TerminalID, MerchantID, SerialNumber,  SoftwareVersion, ICCID, Status from #tbTempResult