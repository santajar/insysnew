﻿

-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: may 30, 2017
-- Modify date:
--				1. 
-- Description:	Insert into tbSoftwareProgressCPL
-- =============================================

CREATE procedure [dbo].[spSoftwareProgressUpdateCPL]
@sTerminalID varchar(8),
@sSoftware varchar(50),
@sSoftwareDownload varchar(50),
@sSerialNumber varchar(25),
@fIndex float,
--@iBuildNumber INT,
@sStatus varchar(50) =null

AS
DECLARE
	@iTotalIndex int,
	@fPercentage float,
	@sGroupName varchar(25),
	@iApppackID INT,
	@iCount INT


SELECT @sGroupName =  GroupName From tbListTerminalIDPackage WHERE TerminalID = @sTerminalID
PRINT 'sGroupName : ' + @sGroupName
SELECT @iTotalIndex=COUNT(*) 
FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackId = @iApppackID

PRINT 'AAppPackId : ' + @iApppackID

SET @fPercentage = @fIndex/(@iTotalIndex-1)*100

PRINT '-masuk progress update'
PRINT @sSerialNumber
PRINT @sSoftwareDownload
PRINT @sSoftware
PRINT @fPercentage
PRINT @fIndex
PRINT @iTotalIndex
PRINT '--'

SELECT @iCount=COUNT(*)
FROM tbSoftwareProgressCPL WITH (NOLOCK)
WHERE InstallStatus = 'On Progress'
	AND TerminalID = @sTerminalID
	AND Software = @sSoftware
	AND SoftwareDownload = @sSoftwareDownload
	AND SerialNumber= @sSerialNumber
	--AND BuildNumber = @iBuildNumber

IF @fIndex IS NOT NULL
BEGIN
	IF @iCount = 1
	BEGIN
		UPDATE tbSoftwareprogressCPL 
		SET Percentage = @fPercentage, EndTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP)
		WHERE TerminalID =@sTerminalID 
			AND SerialNumber = @sSerialNumber
			AND Software = @sSoftware  
			AND SoftwareDownload = @sSoftwareDownload 
			--AND BuildNumber = @iBuildNumber
			AND InstallStatus = 'On Progress'
	END
	ELSE IF @iCount = 0
	BEGIN
		IF @fIndex=0
		BEGIN
		INSERT INTO tbSoftwareprogressCPL 
				(GroupName,
				TerminalID,
				Software,
				--BuildNumber,
				SoftwareDownload,
				SerialNumber,
				StartTime,
				Percentage,
				InstallStatus)
		VALUES(@sGroupName,
			   @sTerminalID,
			   @sSoftware,
			   --@iBuildNumber,
			   @sSoftwareDownload,
			   @sSerialNumber,
			   CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
			   @fPercentage,
			   'On Progress')
		END
	END
END

IF @sStatus IS NOT NULL
BEGIN
	
	PRINT '-masuk status'
	PRINT @sStatus
	PRINT @sSerialNumber
	PRINT @sSoftwareDownload
	PRINT @sSoftware
	PRINT '---'

	UPDATE tbSoftwareProgressCPL
	SET InstallStatus = @sStatus, FinishInstallTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP)
	WHERE TerminalID = @sTerminalID 
		AND SerialNumber = @sSerialNumber 
		AND SoftwareDownload=@sSoftwareDownload 
		AND Software=@sSoftware 
		AND InstallStatus ='On Progress'
		--AND BuildNumber = @iBuildNumber 
		AND Percentage= 100 
END