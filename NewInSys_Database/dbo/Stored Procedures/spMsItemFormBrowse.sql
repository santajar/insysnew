﻿
CREATE PROCEDURE [dbo].[spMsItemFormBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
SELECT FormId,
	FormName
FROM 
	tbMsItemForm WITH (NOLOCK)