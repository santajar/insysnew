﻿
CREATE PROCEDURE [dbo].[spProfileTLEGenerateLog]
	@iDatabaseID INT,
	@sTLEName VARCHAR(50),
	@sContent VARCHAR(MAX)=NULL,
	@isEdit BIT
AS



	SELECT  ItemName, TLETagValue, TLETag
	 INTO #TLETempLog
	FROM (SELECT TLEName, DatabaseID, TLETag, TLETagValue
		  FROM tbProfileTLE WHERE databaseid = @iDatabaseID) A
		FULL OUTER JOIN
		(SELECT ItemName, Tag
		 FROM tbItemList
		 WHERE DatabaseID = @iDatabaseID AND Tag LIKE 'TL%') B
		ON A.TLETag = B.Tag
	WHERE TLEName = @sTLEName

	IF @isEdit = 0
		SELECT ItemName, TLETagValue
		FROM #TLETempLog
	ELSE
	BEGIN
		IF (dbo.isTagLength4(@iDatabaseID)=4)
		BEGIN
			SELECT ItemName, TLETagValue OldValue, TagValue NewValue
			FROM #TLETempLog OLD FULL OUTER JOIN 
			  ( SELECT TagValue, Tag
				FROM dbo.fn_tbProfileTLV(@sTLEName,@sContent)
			  ) NEW
				ON OLD.TLETag = New.Tag
			WHERE TLETagValue != TagValue
		END
		ELSE
		BEGIN
			SELECT ItemName, TLETagValue OldValue, TagValue NewValue
			FROM #TLETempLog OLD FULL OUTER JOIN 
			  ( SELECT TagValue, Tag
				FROM dbo.fn_tbProfileTLV5(@sTLEName,@sContent)
			  ) NEW
				ON OLD.TLETag = New.Tag
			WHERE TLETagValue != TagValue
		END
	END

	DROP TABLE #TLETempLog
	--IF OBJECT_ID('tempdb..##TLETempLog') IS NOT NULL
	DROP TABLE ##TLETemp
