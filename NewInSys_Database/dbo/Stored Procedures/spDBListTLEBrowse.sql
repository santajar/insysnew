﻿

-- =============================================  
-- Author:  Christianto  
-- Create date: Apr 15, 2012  
-- Description: view DB list with TLEInit
-- =============================================  
CREATE PROCEDURE [dbo].[spDBListTLEBrowse]  
AS
SELECT DatabaseID, DatabaseName FROM tbProfileTerminalDB WITH (NOLOCK)
	WHERE TLEInit = 1
	ORDER BY DatabaseName