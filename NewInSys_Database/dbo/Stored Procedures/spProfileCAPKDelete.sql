﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 3, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete the CAPK on table tbProfileAID
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCAPKDelete]
	@iDatabaseId INT,
	@sCAPKIndex VARCHAR(20),
	@bDeleteData BIT = 1
AS
DELETE FROM tbProfileCAPK
WHERE DatabaseId=@iDatabaseId AND CAPKIndex=@sCAPKIndex
