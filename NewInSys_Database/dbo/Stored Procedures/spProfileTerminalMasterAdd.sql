﻿
CREATE PROCEDURE [dbo].[spProfileTerminalMasterAdd]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
DECLARE @iExist INT

SELECT @iExist=COUNT(TerminalId)
FROM tbProfileTerminalListMaster WITH(NOLOCK)
WHERE TerminalId=@sNewTID

IF @iExist=0
BEGIN
	INSERT INTO tbProfileTerminalListMaster(TerminalId, MsTerminalId)
	VALUES (@sNewTID, @sOldTID)
END
ELSE
BEGIN
	UPDATE tbProfileTerminalListMaster
	SET MsTerminalId=@sOldTID
	WHERE TerminalId=@sNewTID
END

