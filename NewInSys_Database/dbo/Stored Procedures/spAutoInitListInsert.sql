﻿-- =============================================
-- Author		:	Tobias Supriyadi
-- Create date	:	Mar 18, 2014
-- Modify date	:	
-- Description	:	Insert into tbAutoInitList
-- =============================================
CREATE PROCEDURE [dbo].[spAutoInitListInsert]
	@sTerminalID VARCHAR(8)
AS
	SET NOCOUNT ON
INSERT INTO tbAutoInitList(TerminalID, CreatedDate, [Enabled])
VALUES (@sTerminalID, GETDATE(), 1)

