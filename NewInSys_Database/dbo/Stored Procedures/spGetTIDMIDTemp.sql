IF OBJECT_ID ('dbo.spGetTIDMIDTemp') IS NOT NULL
	DROP PROCEDURE dbo.spGetTIDMIDTemp
GO

CREATE PROCEDURE [dbo].[spGetTIDMIDTemp]
@sFlag VARCHAR(8)
AS 

SET NOCOUNT ON
	
DECLARE @Count INT 

IF @sFlag = 'TID'
BEGIN

SELECT @Count=count(*) FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'CIMBN'
IF (@Count>0) BEGIN SELECT TID FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'CIMBN' END ELSE
BEGIN
	SELECT @Count=count(*)  FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'CREDIT'
	IF @Count>0 BEGIN SELECT TID FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'CREDIT' END ELSE 
	BEGIN 
		SELECT @Count=count(*) FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'DEBIT' 
		IF @Count>0 BEGIN SELECT TID FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'DEBIT' END ELSE 
		BEGIN 
			SELECT @Count=count(*) FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName LIKE '%CILTAP%'
			IF @Count>0 SELECT TID FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName LIKE '%CILTAP%'
		END
	END
END

END

ELSE

IF @sFlag = 'MID'
BEGIN

SELECT @Count=count(*) FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'CIMBN'
IF (@Count>0) BEGIN SELECT MID FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'CIMBN' END ELSE
BEGIN
	SELECT @Count=count(*)  FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'CREDIT'
	IF @Count>0 BEGIN SELECT MID FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'CREDIT' END ELSE 
	BEGIN 
		SELECT @Count=count(*) FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'DEBIT' 
		IF @Count>0 BEGIN SELECT MID FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName = 'DEBIT' END ELSE 
		BEGIN 
			SELECT @Count=count(*) FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName LIKE '%CILTAP%'
			IF @Count>0 SELECT MID FROM tbEdcMonitorTempAcqSeq WHERE AcquirerName LIKE '%CILTAP%'
		END
	END
	
END


END






GO


