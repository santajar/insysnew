﻿

CREATE PROCEDURE [dbo].[spProfileTLEUpdate]
	@iDatabaseID INT,
	@sTLEName VARCHAR(50),
	@sTLID VARCHAR(3),
	@sContent VARCHAR(8000)
AS
BEGIN
	EXEC spProfileTLEDelete @iDatabaseID, @sTLEName, 0
	EXEC spProfileTLEInsert @iDatabaseID, @sTLEName, @sTLID, @sContent
END






