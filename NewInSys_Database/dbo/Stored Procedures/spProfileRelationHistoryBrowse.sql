﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 27, 2016
-- Modify date: 
-- Description:	
--				1. view The History Relation table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationHistoryBrowse]
	@sTerminalID VARCHAR(8),
	@sDatetime VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TerminalId,
		RelationTag,
		RelationLengthOfTagLength,
		RelationTagLength,
		RelationTagValue
	FROM tbProfileRelationHistory WITH (NOLOCK)
	WHERE TerminalId=@sTerminalId AND LastUpdate = @sDatetime
	ORDER BY RelationTagID


END