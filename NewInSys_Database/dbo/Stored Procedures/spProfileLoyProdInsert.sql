﻿

CREATE PROCEDURE [dbo].[spProfileLoyProdInsert]
	@iDatabaseID INT,
	@sLoyProdName VARCHAR(50),
	@sContent VARCHAR(MAX)=NULL
AS
IF (dbo.isTagLength4(@iDatabaseID)=5)
BEGIN
	INSERT INTO tbProfileLoyProd(
		LoyProdName,
		DatabaseID,
		LoyProdTag,
		LoyProdLengthOfTagLength,
		LoyProdTagLength,
		LoyProdTagValue)
	SELECT 
		@sLoyProdName,
		@iDatabaseID,
		Tag,
		LEN(TagLength),
		TagLength,
		TagValue
	FROM dbo.fn_tbprofiletlv5(@sLoyProdName,@sContent)
END
ELSE
BEGIN
	INSERT INTO tbProfileLoyProd(
		LoyProdName,
		DatabaseID,
		LoyProdTag,
		LoyProdLengthOfTagLength,
		LoyProdTagLength,
		LoyProdTagValue)
	SELECT 
		@sLoyProdName,
		@iDatabaseID,
		Tag,
		LEN(TagLength),
		TagLength,
		TagValue
	FROM dbo.fn_tbprofiletlv(@sLoyProdName,@sContent)
END



