﻿CREATE PROCEDURE [dbo].[spProfileIssuerCopy]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
BEGIN
	-- select all value
	INSERT INTO tbprofileIssuer(
		TerminalId,
		IssuerName,
		IssuerTag,
		IssuerLengthOfTagLength,
		IssuerTagLength,
		IssuerTagValue,
		[Description]
		)
	SELECT @sNewTID,
		IssuerName,
		IssuerTag,
		IssuerLengthOfTagLength,
		IssuerTagLength,
		IssuerTagValue,
		[Description] 
	FROM tbprofileIssuer WITH (NOLOCK)
	WHERE TerminalId=@sOldTID
END
