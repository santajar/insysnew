﻿
CREATE PROCEDURE [dbo].[spTerminalIdMigrate] 
    @sTerminalId VARCHAR(8)
AS
SET NOCOUNT ON
PRINT 'Start Migrate Terminal ' + @sTerminalId

DECLARE @sContent VARCHAR(MAX)
DECLARE @sDatabaseId VARCHAR(3)
DECLARE @bStatMaster BIT

--DECLARE @sDatabaseName VARCHAR(30)
--DECLARE @iNewInit BIT

SELECT 
    @sContent=[Content],
    @sDatabaseId=DatabaseId,
    @bStatMaster=StatusMaster
FROM dbtms..TerminalList
WHERE TerminalName=@sTerminalId

--SELECT @sDatabaseName=DatabaseName,
--    @iNewInit=NewInit
--FROM dbtms..BrDatabase
--WHERE DatabaseId=@sDatabaseId

SET @sContent=SUBSTRING(@scontent,2,LEN(@sContent)-2)

--IF OBJECT_ID('tempdb..#profile') IS NOT NULL
--    DROP TABLE #profile
--SELECT * INTO #profile FROM dbo.fn_tbProfileTLV(@sTerminalId,@sContent)

------DatabaseId dipindah ke spItemMigrate
--IF dbo.IsMigrateDbIdExist(@sDatabaseId)=0
--BEGIN
--    INSERT INTO tbProfileTerminalDB(
--        DatabaseID,
--        DatabaseName,
--        NewInit)
--    VALUES(CAST(@sDatabaseId AS INT),
--        @sDatabaseName,
--        @iNewInit)
--END

----Terminal
INSERT INTO tbProfileTerminalList(TerminalID, DatabaseID, AllowDownload, StatusMaster) 
    VALUES (@sTerminalId, @sDatabaseId,1,@bStatMaster)

IF EXISTS ( SELECT * FROM SYS.COLUMNS
        WHERE Name = N'AutoInitTimeStamp' AND
        Object_ID = Object_ID (N'tbProfileTerminalList')
      )
BEGIN
    UPDATE tbProfileTerminalList
    SET AutoInitTimeStamp =    GETDATE()
    FROM tbProfileTerminalList
    WHERE TerminalID = @sTerminalId AND 
          DatabaseID = @sDatabaseId 
END

INSERT INTO tbProfileTerminal(
    TerminalId,
    TerminalTag,
    TerminalLengthOfTagLength,
    TerminalTagLength,
    TerminalTagValue)
SELECT TerminalId,
    Tag,
    LEN(CAST(TagLength AS INT)), 
    TagLength,
    TagValue 
FROM ##TempContent
WHERE Tag LIKE 'DE%' OR Tag LIKE 'DC%'
ORDER BY RowId
