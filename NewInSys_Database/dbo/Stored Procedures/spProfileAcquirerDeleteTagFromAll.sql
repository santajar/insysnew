﻿
CREATE PROCEDURE [dbo].[spProfileAcquirerDeleteTagFromAll]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5)
AS
	DELETE FROM tbProfileAcquirer
	WHERE TerminalID IN (SELECT TerminalID 
						 FROM tbProfileTerminalList WITH (NOLOCK)
						 WHERE DatabaseID = @sDatabaseID
						)
		 AND AcquirerTag = @sTag 




