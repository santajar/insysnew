﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 26, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. import the acquirer content
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerImport] @sDatabaseId VARCHAR(3),
	@sTerminalId VARCHAR(8),
	@sContent VARCHAR(MAX)
AS

SELECT * INTO #profile FROM dbo.fn_tbProfileTLV(@sTerminalId,@sContent)

DECLARE @iCountRow INT
SELECT @iCountRow=COUNT(*) FROM #profile
	WHERE Tag LIKE 'AA%'

----Acquirer
IF @iCountRow > 0
BEGIN
	INSERT INTO tbProfileAcquirer(
		TerminalId,
		AcquirerName,
		AcquirerTag,
		AcquirerLengthOfTagLength,
		AcquirerTagLength,
		AcquirerTagValue)
	SELECT TerminalId,
		[Name],
		Tag,
		LEN(CAST(TagLength AS INT)), 
		TagLength,
		TagValue 
	FROM #profile
	WHERE Tag LIKE 'AA%'
	AND Tag IN (SELECT Tag 
	FROM tbItemList WITH (NOLOCK)
	WHERE DatabaseID=@sDatabaseId AND FormID=2)
	ORDER BY RowId
END
--IF OBJECT_ID('tempdb..#profile') IS NOT NULL
	DROP TABLE #profile