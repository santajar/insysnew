﻿
CREATE PROCEDURE [dbo].[spProfileInsertProcess]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5),
	@iLengthofTagValueLength INT,
	@sTagValue VARCHAR(500) = ''
AS
	DECLARE @iTagValueLength INT
	SET @iTagValueLength = DATALENGTH(@sTagValue)
	
	IF SUBSTRING(@sTag,1,2) = 'DE'
		EXEC spProfileTerminalInsertTagToAll @sDatabaseID, @sTag, @iLengthofTagValueLength, @sTagValue, @iTagValueLength
	ELSE IF SUBSTRING(@sTag,1,2) = 'AA'
		EXEC spProfileAcquirerInsertTagToAll @sDatabaseID, @sTag, @iLengthofTagValueLength, @sTagValue, @iTagValueLength
	ELSE IF SUBSTRING(@sTag,1,2) = 'AE'
		EXEC spProfileIssuerInsertTagToAll @sDatabaseID, @sTag, @iLengthofTagValueLength, @sTagValue, @iTagValueLength
	ELSE IF SUBSTRING(@sTag,1,2) = 'AC'
		EXEC spProfileCardInsertTagToAll @sDatabaseID, @sTag, @iLengthofTagValueLength, @sTagValue, @iTagValueLength
	ELSE IF SUBSTRING(@sTag,1,2) = 'TL'
		EXEC spProfileTLEInsertTagToAll @sDatabaseID, @sTag, @iLengthofTagValueLength, @sTagValue, @iTagValueLength