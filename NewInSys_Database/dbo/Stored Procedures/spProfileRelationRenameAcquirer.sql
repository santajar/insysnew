﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 20, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating relation table on Acquirer name
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationRenameAcquirer]
	@sTerminalID VARCHAR(8),
	@sOldAcq VARCHAR(15),
	@sNewAcq VARCHAR(15)
AS
BEGIN
SET NOCOUNT ON;
	EXEC spProfileRelationHistoryInsert @sTerminalID
	EXEC spProfileAcquirerHistoryInsert @sTerminalID,@sOldAcq
	--IF (dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalID))=4)
	--BEGIN
		UPDATE TbProfileRelation
		SET RelationTagValue = @sNewAcq
		WHERE TerminalID = @sTerminalID AND
			RelationTag IN  ('AD03','AD003') AND
			RelationTagValue = @sOldAcq
	--END
	--ELSE
	--BEGIN
	--	UPDATE TbProfileRelation
	--	SET RelationTagValue = @sNewAcq
	--	WHERE TerminalID = @sTerminalID AND
	--		RelationTag = 'AD003' AND
	--		RelationTagValue = @sOldAcq
	--END
END
