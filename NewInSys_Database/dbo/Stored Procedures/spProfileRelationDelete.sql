﻿CREATE PROCEDURE [dbo].[spProfileRelationDelete]
	@sTerminalID VARCHAR(8)
AS
	DELETE FROM tbProfileRelation 
		WHERE TerminalID=@sTerminalID
