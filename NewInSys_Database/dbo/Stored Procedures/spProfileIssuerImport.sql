﻿CREATE PROCEDURE [dbo].[spProfileIssuerImport] @sDatabaseId VARCHAR(3),
	@sTerminalId VARCHAR(8),
	@sContent VARCHAR(MAX)
AS


CREATE TABLE #profile
(
	RowId		INT,
	TerminalID	VARCHAR(8),
	[Name]		VARCHAR(15),
	Tag			VARCHAR(4),
	TagLength	VARCHAR(3),
	TagValue	VARCHAR(500)
	
)
IF (dbo.isTagLength4(CAST(@sDatabaseId AS INT))=5)
BEGIN
	INSERT INTO #profile (RowId,TerminalID,[Name],Tag,TagLength,TagValue)
	SELECT RowId,TerminalID,[Name],Tag,TagLength,TagValue FROM dbo.fn_tbProfileTLV5(@sTerminalId,@sContent)
END
ELSE
BEGIN
	INSERT INTO #profile (RowId,TerminalID,[Name],Tag,TagLength,TagValue)
	SELECT RowId,TerminalID,[Name],Tag,TagLength,TagValue FROM dbo.fn_tbProfileTLV(@sTerminalId,@sContent)
END
----Issuer
DECLARE @iCountRow INT
SELECT @iCountRow=COUNT(*) FROM #profile
	WHERE Tag LIKE 'AA%'
IF @iCountRow > 0
BEGIN
	INSERT INTO tbProfileIssuer(
		TerminalId,
		IssuerName,
		IssuerTag,
		IssuerLengthOfTagLength,
		IssuerTagLength,
		IssuerTagValue)
	SELECT TerminalId,
		[Name],
		Tag,
		LEN(CAST(TagLength AS INT)), 
		TagLength,
		TagValue
	FROM #profile
	WHERE Tag LIKE 'AE%'
	AND Tag IN 
		(SELECT Tag 
		FROM tbItemList WITH (NOLOCK)
		WHERE DatabaseID=@sDatabaseId 
		AND FormID=3)
	ORDER BY RowId
END
--IF OBJECT_ID('tempdb..#profile') IS NOT NULL
	DROP TABLE #profile
