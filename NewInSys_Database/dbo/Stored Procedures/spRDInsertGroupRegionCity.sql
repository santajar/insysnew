﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spRDInsertGroupRegionCity]
	@sCategory VARCHAR(10), @sCategoryName VARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON;

	IF (@sCategory = 'Group')
	BEGIN
		EXEC spRDInsertGroup @sCategoryName
	END
	ELSE IF ( @sCategory = 'Region')
	BEGIN
		EXEC spRDInsertRegion @sCategoryName
	END
	ELSE
	BEGIN
		EXEC spRDInsertCity @sCategoryName
	END

END