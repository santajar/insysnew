﻿CREATE PROCEDURE [dbo].[spEmsMsGetValue] 
	@sTerminalID CHAR(8),
	@sTag VARCHAR(5),
	@sFilter VARCHAR(50),
	@sValue VARCHAR(500) OUTPUT 
AS
DECLARE @sTbName VARCHAR(10)
SELECT @sTbName = CASE SUBSTRING(@sTag,1,2)
					WHEN 'AA' THEN 'Acquirer'
					WHEN 'AE' THEN 'Issuer'
					ELSE 'Terminal'
				  END

DECLARE @sStmt nVARCHAR(4000)

SET @sStmt = 'SELECT @sValue1 = TagValue FROM [' + @sTerminalID + '] WITH (NOLOCK) WHERE Tag = '''+@sTag+''''
IF @sFilter<>'' AND @sTbName <> 'Terminal'
	SET @sStmt = @sStmt + ' AND Name = ''' + @sFilter+''''

EXEC sp_executesql  @sStmt, N'@sValue1 VARCHAR(150) OUTPUT', @sValue1 = @sValue OUTPUT
IF ISNULL(@sValue,'')=''
	SET @sValue='0'