﻿
CREATE PROCEDURE [dbo].[spItemComboBoxValueInsertInitialFlazz]
	@sInitialFlazzName VARCHAR(50),
	@sDatabaseId INT
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @iItemSeq INT, @iCountPax INT

SELECT @iCountPax = COUNT(*)
FROM tbItemList
WHERE ItemName LIKE 'Initial Card Setting' AND 
	  FormID = 1  AND
	  DatabaseID=@sDatabaseId

IF @iCountPax>0
BEGIN
SELECT @iItemSeq=ItemSequence
FROM tbItemList 
WHERE ItemName LIKE 'Initial Card Setting' AND 
	  FormId=1 AND 
	  DatabaseId=@sDatabaseId

INSERT INTO tbItemComboBoxValue(DatabaseId,
			FormId,
			ItemSequence,
			DisplayValue,
			RealValue)
SELECT @sDatabaseId [DatabaseId], 
	1 [FormID],
	@iItemSeq [ItemSequence],
	@sInitialFlazzName [DisplayValue],
	@sInitialFlazzName [RealValue]
END

END