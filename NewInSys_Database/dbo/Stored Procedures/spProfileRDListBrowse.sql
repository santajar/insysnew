﻿
-- =============================================  
-- Author:  Tedie Scorfia  
-- Create date: march 16, 2015  
-- Description: view Remote Donwload list  
-- =============================================  
CREATE PROCEDURE [dbo].[spProfileRDListBrowse]  
 @sDatabaseId VARCHAR(3) = ''  
AS  
SELECT RemoteDownloadTagID,  
	--DatabaseId,  
	RemoteDownloadName  
FROM dbo.fn_viewRemoteDownload(@sDatabaseId)  
--WHERE DatabaseId = @sDatabaseId--case when @sDatabaseId = '' then DatabaseId else @sDatabaseId end