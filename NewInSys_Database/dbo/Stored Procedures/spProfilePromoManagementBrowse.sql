﻿-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Feb 10, 2015
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the content of Promo Management table
-- =============================================
CREATE PROCEDURE [dbo].[spProfilePromoManagementBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery='
	SELECT 
		PromoManagementName,
		DatabaseID,
		PromoManagementTag as Tag,
		PromoManagementTagLength,
		PromoManagementTagValue as Value
	FROM tbProfilePromoManagement WITH (NOLOCK) '
	SET @sQuery = @sQuery + ISNULL(@sCondition,'') + ' ORDER BY PromoManagementName, PromoManagementTag'
EXEC (@sQuery)