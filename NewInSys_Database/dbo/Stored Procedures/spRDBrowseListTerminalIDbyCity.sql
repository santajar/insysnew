﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,,21 april 2014>
-- Description:	<Description,,Browse List TerminalID>
-- =============================================
CREATE PROCEDURE [dbo].[spRDBrowseListTerminalIDbyCity]
	@sCondition VARCHAR(3000) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @sQuery VARCHAR(1000)
    -- Insert statements for procedure here
	set @sQuery=
		'SELECT 
			A.TerminalID
			, CityRDName 
			, B.RemoteDownload
		FROM (SELECT TerminalID,IdRegionRD,IdGroupRD,IdCityRD
			FROM tbListTIDRemoteDownload WITH (NOLOCK)) A 
		JOIN (SELECT IdCityRD,CityRDName
			FROM tbCityRemoteDownload WITH (NOLOCK)) D ON A.IdCityRD= D.IdCityRD 
		LEFT JOIN tbProfileTerminalList B WITH (NOLOCK) ON A.TerminalID=B.TerminalID '	
	EXEC ( @sQuery + @sCondition)
END