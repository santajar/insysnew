﻿-- =============================================
-- Author:		<Tedie>
-- Create date: <18 January 2016>
-- Description:	<Update LastUpdate TerminalId profile>
-- =============================================
CREATE PROCEDURE spProfileTerminalUpdateLastUpdateByTagAndDbID
	@sDatabaseID VARCHAR(3),
	@sItemName VARCHAR(50),
	@sValue VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @sLastUpdate VARCHAR(12)
	SET @sLastUpdate = dbo.sGetDateMMDDYY()
	SET @sLastUpdate = @sLastUpdate + dbo.sGetTimeHHMMSS()

	UPDATE tbProfileTerminal
	SET 
		TerminalTagValue=@sLastUpdate,
		TerminalTagLength=LEN(@sLastUpdate)
		WHERE TerminalId IN 
				(SELECT TerminalID
				FROM tbProfileTerminal WITH (NOLOCK)
				WHERE TerminalId IN 
						(SELECT TerminalID
						FROM tbProfileTerminalList WITH (NOLOCK)
						WHERE DatabaseID = @sDatabaseID)
					AND TerminalTag IN 
						(SELECT Tag 
						FROM tbItemList WITH (NOLOCK)
						WHERE DatabaseID = @sDatabaseID
							AND ItemName = @sItemName)
					AND TerminalTagValue = @sValue
						)
			AND TerminalTag IN ('DC03','DC003')
END