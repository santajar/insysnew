﻿CREATE PROCEDURE [dbo].[spProfileCardDelete] 
	@sDatabaseID SMALLINT
AS
	DELETE FROM tbProfileCard
	WHERE CardID IN ( 
						SELECT CardID
						FROM tbProfileCardList WITH (NOLOCK)
						WHERE DatabaseID = @sDatabaseID
					)