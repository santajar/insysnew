﻿CREATE PROCEDURE [dbo].[spProfileRequestPaperReceiptBrowse]
	@sCondition VARCHAR(100) = NULL
AS
	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt = 'SELECT ReqTagId,
						ReqName, 
						DatabaseID,
						ReqTag as Tag,
						ReqLengthOfTagLength,
						ReqTagLength,
						ReqTagValue as Value,
						[Description]
				FROM tbProfileReqPaperReceiptManagement  WITH (NOLOCK) '
	SET @sStmt = @sStmt + ISNULL(@sCondition,'') + ' ORDER BY ReqTagId, ReqTag'
	EXEC (@sStmt)