﻿

CREATE PROCEDURE [dbo].[spEmsMsGetValueComplicated]
	@sTerminalID VARCHAR(8),
	@sTag VARCHAR(4),
	@sFilter VARCHAR(50),
	@sValue VARCHAR(50) OUTPUT 
AS
DECLARE @sQuery NVARCHAR(MAX)

IF @sTag = ''
BEGIN
	IF @sFilter = 'true' OR @sFilter = 'offus'
		SET @sValue = '1'
	ELSE IF @sFilter = 'lastinit'
		--SELECT @sValue = (
		--	CONVERT(VARCHAR, DAY(CAST(InitTime as datetime))) +
		--	CONVERT(VARCHAR, MONTH(CAST(InitTime as datetime))) +
		--	CONVERT(VARCHAR, YEAR(CAST(InitTime as datetime))) +
		--	CONVERT(VARCHAR, DATEPART(HOUR,CAST(InitTime as datetime))) +
		--	CONVERT(VARCHAR, DATEPART(MINUTE,CAST(InitTime as datetime))) +
		--	CONVERT(VARCHAR, DATEPART(SECOND,CAST(InitTime as datetime)))
		--)
		SELECT @sValue = (
			RIGHT('0'+CONVERT(VARCHAR, DAY(CAST(InitTime as datetime))),2) +
			RIGHT('0'+CONVERT(VARCHAR, MONTH(CAST(InitTime as datetime))),2) +
			RIGHT(CONVERT(VARCHAR, YEAR(CAST(InitTime as datetime))),2)+
			RIGHT('0'+CONVERT(VARCHAR, DATEPART(HOUR,CAST(InitTime as datetime))),2) +
			RIGHT('0'+CONVERT(VARCHAR, DATEPART(MINUTE,CAST(InitTime as datetime))),2) +
			'00')
		FROM tbAuditInit
		WHERE TerminalID = @sTerminalID AND StatusDesc LIKE '%complete%' 
		ORDER BY inittime DESC
	ELSE IF @sFilter = 'MASTER'
		EXEC spProfileTerminalMasterBrowse @sTerminalID, @sValue OUTPUT
	ELSE IF @sFilter = 'FALSE'
		SET @sValue = '0'
	ELSE IF @sFilter = 'PROMO'
		SET @sValue = dbo.IsCicilanPromo(@sTerminalID)
END
ELSE IF @stag = 'DE05'
BEGIN
	DECLARE @sCategory VARCHAR(10)
	SET @sQuery =
		'SELECT @sCategory = TagValue
		 FROM ['+ @sTerminalID +'] WHERE Tag = ''' + @sTag + ''''
	EXEC sp_executesql @sQuery, N'@sCategory VARCHAR(10) OUTPUT', @sCategory = @sCategory OUTPUT

	IF @sFilter = @sCategory
		SET @sValue = '1'
	ELSE
		SET @sValue = '0'
END
ELSE IF @stag = 'AA04'
BEGIN
	SET @sQuery = 
	'SELECT @sValue = SUBSTRING(TagValue, LEN(TagValue)-9+1,9)
		 FROM ['+ @sTerminalID +'] WHERE Tag = ''' + @sTag + ''' AND Name = ''BCA''
	
	IF ISNUMERIC(@sValue) = 0
	BEGIN		
		SELECT @sValue = SUBSTRING(TagValue, LEN(TagValue)-9+1,9) 
			FROM ['+ @sTerminalID +'] WHERE Tag = ''' + @sTag + ''' AND Name = ''DEBIT''
		IF ISNUMERIC(@sValue) = 0
			SET @sValue = ''''
	END'
	EXEC sp_executesql @sQuery, N'@sValue VARCHAR(10) OUTPUT', @sValue = @sValue OUTPUT
END
ELSE IF @stag = 'AA05'
BEGIN	
	DECLARE @iTidLen int
	IF @sFilter = 'AMEX' OR @sFilter = 'DINERS'
		SET @iTidLen = 8
	ELSE IF CHARINDEX('PROMO', @sFilter ) > 0
		SET @iTidLen = 5
	ELSE 
		SET @iTidLen = 6
	SET @sQuery = '
	SELECT @sValue = ISNUMERIC(SUBSTRING(TagValue, LEN(TagValue) - '+ CAST(@iTidLen AS VARCHAR) +' + 1, '+ CAST(@iTidLen AS VARCHAR) +'))
	FROM [' + @sTerminalID + ']
	WHERE Tag = '''+ @sTag + ''' AND Name = ''' + @sFilter + ''''
	
	EXEC sp_executesql @sQuery, N'@sValue VARCHAR(10) OUTPUT', @sValue = @sValue OUTPUT
	IF ISNULL(@sValue,'')=''
		SET @sValue='0'
END
ELSE IF @stag LIKE 'AE%' OR @stag LIKE 'AA%'
BEGIN
	DECLARE @sDefValue bit
	IF @sFilter = 'TRUE'
		--SET @sDefValue = 1
		SET @sQuery = '
			SELECT @sValue = CASE
							  WHEN	COUNT(TagValue) > 0 THEN  1
							  ELSE 0
							END
			FROM [' + @sTerminalID + '] 
			WHERE Tag LIKE ''' + @sTag + '''
				AND Name NOT LIKE ''AMEX'' AND Name NOT LIKE ''DINERS''
				AND TagValue NOT LIKE 1'
	ELSE 
		--SET @sDefValue = 0
		SET @sQuery = '
			SELECT @sValue = CASE
							  WHEN	COUNT(TagValue) = 0 THEN  0
							  ELSE 1
							END
			FROM [' + @sTerminalID + '] 
			WHERE Tag LIKE ''' + @sTag + ''' 
				AND Name NOT LIKE ''AMEX'' AND Name NOT LIKE ''DINERS''
				AND TagValue NOT LIKE 0'
	EXEC sp_executesql @sQuery, N'@sValue VARCHAR(10) OUTPUT', @sValue = @sValue OUTPUT
END

