﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 3, 2010
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the list of CAPK
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCAPKBrowseList]
	@sDatabaseId VARCHAR(3)
AS
SELECT DISTINCT
	CAPKIndex as Name
FROM tbProfileCAPK WITH (NOLOCK)
WHERE DatabaseId=@sDatabaseId
