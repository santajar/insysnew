﻿
-- ==================================================
-- Description:	Delete the CAPK on table tbProfileAID
-- ==================================================

CREATE PROCEDURE [dbo].[spProfileCAPKDeleteByDbID]
	@sDatabaseID SMALLINT
AS
	DELETE FROM tbProfileCAPK
	WHERE DatabaseID = @sDatabaseID

