﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Sept 30, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. View the detail profile initialization log
-- =============================================
CREATE PROCEDURE [dbo].[spInitLogConnDetailBrowse]
	@sTerminalID CHAR(8)
AS
	SELECT [Time], 
			TerminalID, 
			dbo.sTagName(Tag)as [Name],
			[Description],  
			Percentage
	FROM tbInitLogConnDetail WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID