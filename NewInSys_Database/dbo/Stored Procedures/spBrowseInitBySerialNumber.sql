﻿create PROCEDURE [dbo].[spBrowseInitBySerialNumber]
	@sAppName VARCHAR(20),
	@iInitBySerialNumber BIT OUTPUT
AS
	SET NOCOUNT ON
SELECT @iInitBySerialNumber=InitBySerialNumber
FROM tbBitMap WITH(NOLOCK)
WHERE ApplicationName = @sAppName