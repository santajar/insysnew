﻿
-- =========================================
-- Description:	 Viewing the Bit Map table
-- =========================================

CREATE PROCEDURE [dbo].[spBitMapBrowse]
AS
	SET NOCOUNT ON
	SELECT ApplicationName,
			AllowCompress,
			BitMap,
			HexBitMap,
			StandartISO8583
	FROM tbBitMap WITH (NOLOCK)
	ORDER BY ApplicationName

