﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 17, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. delete acquire table on specific terminalid and acquirer name
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerDelete]
	@sTerminalID VARCHAR(8),
	@sAcquirer VARCHAR(15)
AS
EXEC spProfileAcquirerHistoryinsert @sTerminalID,@sAcquirer

	DELETE FROM tbProfileAcquirer 
		WHERE TerminalID=@sTerminalID AND
			  AcquirerName=@sAcquirer

EXEC spProfileTerminalUpdateLastUpdate @sTerminalID
