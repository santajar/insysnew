﻿
CREATE PROCEDURE [dbo].[spProfileDeleteTagProcess]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5)
AS
	IF SUBSTRING(@sTag,1,2) = 'DE'
		EXEC spProfileTerminalDeleteTagFromAll @sDatabaseID, @sTag		
	ELSE IF SUBSTRING(@sTag,1,2) = 'AA'
		EXEC spProfileAcquirerDeleteTagFromAll @sDatabaseID, @sTag
	ELSE IF SUBSTRING(@sTag,1,2) = 'AE'
		EXEC spProfileIssuerDeleteTagFromAll @sDatabaseID, @sTag
	ELSE IF SUBSTRING(@sTag,1,2) = 'AC'
		EXEC spProfileCardDeleteTagFromAll @sDatabaseID, @sTag
	ELSE IF SUBSTRING(@sTag,1,2) = 'TL'
		EXEC spProfileTLEDeleteTagFromAll @sDatabaseID, @sTag


