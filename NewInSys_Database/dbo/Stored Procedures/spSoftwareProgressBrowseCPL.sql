﻿-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: may 30, 2017
-- Modify date:
--				1. 
-- Description:	Browse into tbSoftwareProgressCPL
-- =============================================

CREATE procedure [dbo].[spSoftwareProgressBrowseCPL]
@sCondition VARCHAR(MAX) =  null
AS
DECLARE @sQuery VARCHAR(MAX) = 
'SELECT
	ID,
	A.GroupName [Group],
	TerminalID,
	Software,
	SoftwareDownload,
	SerialNumber,
	StartTime,
	EndTime,
	--B.DownloadedPercent,
	B.TotalFiles,
	[Percentage] TotalPercentage,
	FinishInstallTime,
	InstallStatus
FROM tbSoftwareProgressCPL A WITH (NOLOCK) JOIN
	(SELECT SUM([Percentage]) DownloadedPercent, COUNT(GroupName) TotalFiles, GroupName 
	FROM tbSoftwareProgressCPLDetail 
	GROUP BY GroupName) B
	ON A.GroupName=B.GroupName
 '
IF @sCondition IS NOT NULL
EXEC (@sQuery + @sCondition+ ' ORDER BY ID DESC')
ELSE
EXEC (@sQuery + ' ORDER BY ID DESC')