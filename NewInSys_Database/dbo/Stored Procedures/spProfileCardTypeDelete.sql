﻿-- =============================================
-- Author:		Tobias Setyo
-- Create date: Nov 14, 2016
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete the tbProfileCardType management on table tbProfileCardType
--				2. Modify InitialFlazz Management for Form Flexsible
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCardTypeDelete]
	@iDatabaseId INT,
	@sCardTypeName VARCHAR(50),
	@bDeleteData BIT = 1
AS
DELETE FROM tbProfileCardType
WHERE DatabaseId=@iDatabaseId AND CardTypeName=@sCardTypeName