﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: SEPT 20, 2014
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the list of PRODUCT CODE
-- =============================================
CREATE PROCEDURE [dbo].[spProfileProductCodeListBrowse]
	@sDatabaseID VARCHAR(3)
AS
	DECLARE @sQuery VARCHAR(MAX)
SET @sQuery = 
'SELECT DISTINCT ProductCode AS [Name]
FROM tbProfileProductCode WITH (NOLOCK)
WHERE DatabaseID IN (''0'', ''' + @sDatabaseID + ''') AND ProductCodeTag IN (''KP01'',''KP001'')'

EXEC (@sQuery)