﻿CREATE PROCEDURE [dbo].[spBitMapUpdateGetLogDetail]
	@sLogDetail VARCHAR(200) OUTPUT,
	@sApplicationName VARCHAR(MAX)
AS
	SET NOCOUNT ON
	DECLARE @iIndex INT,
			@iCount INT

	SET @iIndex = 1
	SET @sLogDetail = ''

	CREATE TABLE #TemporaryTable (ID INT IDENTITY,
									ColumnName VARCHAR(50),
									OldValue VARCHAR(100),
									NewValue VARCHAR(100)
									)
	--CREATE TABLE #tbBitMap
	--(
	--	ColumnName VARCHAR(50),
	--	ColumnValue VARCHAR(100)
	--)
	
	--INSERT INTO #tbBitMap
	--EXEC sp_tbBitMap @sApplicationName								
					
	--CREATE TABLE #OldTable
	--(
	--	ColumnName VARCHAR(50),
	--	ColumnValue VARCHAR(100)
	--)	
	--INSERT INTO #OldTable (ColumnName, ColumnValue)
	--EXEC sp_tbBitMap @sApplicationName
	
	SELECT ColumnName, ColumnValue
	INTO #NewTable
	FROM DBO.fn_tbBitMap (@sApplicationName)

	SELECT ColumnName,ColumnValue
	INTO #OldTable
	FROM DBO.fn_tbBitMap (@sApplicationName)
	
	INSERT INTO #TemporaryTable (ColumnName, OldValue, NewValue)
	SELECT a.ColumnName, a.ColumnValue, b.ColumnValue
	FROM #OldTable a FULL OUTER JOIN #NewTable b
		ON a.ColumnName = b.ColumnName
	WHERE ISNULL(a.ColumnValue,'') <> ISNULL(b.ColumnValue,'')

	SET @iCount = @@ROWCOUNT

	WHILE (@iIndex <= @iCount)
	BEGIN
		DECLARE @sColumnName VARCHAR(50),
				@sOldValue VARCHAR(100),
				@sNewValue VARCHAR(100)

		SELECT @sColumnName = ColumnName,
				@sOldValue = OldValue,
				@sNewValue = NewValue
		FROM #TemporaryTable
		WHERE ID = @iIndex

		SET @sLogDetail = @sLogDetail + @sColumnName + ' : ' + @sOldValue + ' --> ' + @sNewValue + ' \n '
		SET @iIndex = @iIndex + 1
	END
	--IF (OBJECT_ID ('tempdb..#TemporaryTable') IS NOT NULL)
		DROP TABLE #TemporaryTable