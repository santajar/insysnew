﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Updating table tbUploadTagDE
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagDEUpdateSourceColumn]
	@iID SMALLINT,
	@iCol SMALLINT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tbUploadTagDE
	SET SourceColumn = @iCol
	WHERE ID = @iID
END
