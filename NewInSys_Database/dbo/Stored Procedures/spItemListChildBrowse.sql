﻿
CREATE PROCEDURE [dbo].[spItemListChildBrowse]
	@iDatabaseId INT,
	@iFormID INT
AS
SELECT 
	ROW_NUMBER() over (order by a.ItemId) ItemSequence, 
	a.DatabaseID,
	TagParent,
	TagChild,
	b.ItemName + ' ' + a.ItemName [ItemName],
	ObjectName,
	DefaultValue,
	LengthOfTagLength,
	vAllowNull,
	vUpperCase,
	vType,
	vMinLength,
	vMaxLength,
	vMinValue,
	vMaxValue,
	ValidationMsg
FROM tbItemListChild a WITH (NOLOCK) join 
(
	SELECT        TOP (100) PERCENT dbo.tbItemList.ItemID, dbo.tbItemList.DatabaseID, dbo.tbItemList.FormID, dbo.tbItemList.ItemSequence, dbo.tbItemList.ItemName, 
							 dbo.tbItemObject.ObjectName, dbo.tbItemList.DefaultValue, dbo.tbItemList.vAllowNull, dbo.tbItemList.vUpperCase, dbo.tbItemList.vType, dbo.tbItemList.vMinLength, 
							 dbo.tbItemList.vMaxLength, dbo.tbItemList.vMinValue, dbo.tbItemList.vMaxValue, dbo.tbItemList.ValidationMsg, dbo.tbItemList.Tag, 
							 dbo.tbItemList.LengthofTagValueLength AS LengthOfTagLength, dbo.tbItemList.vMasking
	FROM            dbo.tbItemList WITH (NOLOCK) INNER JOIN
							 dbo.tbItemObject WITH (NOLOCK) ON dbo.tbItemList.ObjectID = dbo.tbItemObject.ObjectID
	ORDER BY dbo.tbItemList.DatabaseID, dbo.tbItemList.FormID, dbo.tbItemList.ItemSequence
) b on a.DatabaseID=b.DatabaseID and a.TagParent=b.Tag
WHERE a.DatabaseID=@iDatabaseId and b.FormID=@iFormID



