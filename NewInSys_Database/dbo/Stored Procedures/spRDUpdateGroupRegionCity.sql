﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,,24 April 2014>
-- Description:	<Description,,Update Group,Region or City Name>
-- =============================================
CREATE PROCEDURE [dbo].[spRDUpdateGroupRegionCity]
	@sCategory VARCHAR(10), @sNewCategoryName VARCHAR(150),@sOldCategoryName VARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON;

	IF (@sCategory ='Group')
	BEGIN
		EXEC spRDUpdateGroup @sOldCategoryName,@sNewCategoryName
	END
    ELSE IF (@sCategory ='Region')
	BEGIN
		EXEC spRDUpdateRegion @sOldCategoryName,@sNewCategoryName
	END
	ELSE
	BEGIN
		EXEC spRDUpdateCity @sOldCategoryName,@sNewCategoryName
	END
END