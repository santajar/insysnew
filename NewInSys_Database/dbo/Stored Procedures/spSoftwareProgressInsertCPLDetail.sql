﻿-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: may 30, 2017
-- Modify date:
--				1. 
-- Description:	Insert into tbSoftwareProgress
-- =============================================

CREATE PROCEDURE [dbo].[spSoftwareProgressInsertCPLDetail]
	@sId INT,
	@sTerminalID VARCHAR(8),
	@sSoftware VARCHAR(50),
	@sSoftwareDownload VARCHAR(50),
	@sSoftwareDownloadGlobal VARCHAR(50),
	@sSerialNumber VARCHAR(25),
	@fIndex FLOAT,
	@iTotalIndexGlobalTotal FLOAT
AS
DECLARE
	@iTotalIndex INT,
	@fPercentage FLOAT,
	@sGroupName VARCHAR(25),
	@iAppPackId INT,
	@iCount INT,
	@fPercentaseGlobal FLOAT,
	@fIndexGlobal INT
SELECT @sGroupName =  GroupName FROM tbListTerminalIDPackage WHERE TerminalID = @sTerminalID


SELECT @iTotalIndex=COUNT(*) 
FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackageName =  @sSoftwareDownload 

----PRINT 'MASUK SOFTWARE PROGRESS INSERT'
----PRINT 'SOFTWARENAME = '+@sSoftware
----PRINT 'SOFTWAREDOWNLOAD = '+@sSoftwareDownload
----PRINT @fIndex
----PRINT CAST(@iTotalIndex  as VARCHAR(20))
--PRINT '----'

IF @iTotalIndex > 0 
BEGIN
	PRINT @iTotalIndex
	SET @fPercentage = (@fIndex / @iTotalIndex) * 100
	PRINT @fPercentage

	SELECT @iCount=COUNT(*)
	FROM tbSoftwareProgressCPLDetail WITH (NOLOCK)
	WHERE 
		--InstallStatus = 'On Progress'	AND 
		TerminalID = @sTerminalID
		AND Software = @sSoftware
		AND AppPackageName = @sSoftwareDownload
		AND SerialNumber= @sSerialNumber
	

	IF (@iCount>0)
	BEGIN
		--EXEC spSoftwareProgressUpdateCPLDetail
		--	@sId,
		--	@sTerminalID,
		--	@sSoftware,
		--	@sSoftwareDownload,
		--	@sSerialNumber,
		--	@fPercentage,
		--	'On Progress'
		UPDATE tbSoftwareProgressCPLDetail SET Percentage = @fPercentage WHERE TerminalID=@sTerminalID AND AppPackageName = @sSoftwareDownload
		--AND ID = @sId
	END
	ELSE
	BEGIN
		INSERT INTO tbSoftwareProgressCPLDetail 
					(--ID, 
					GroupName,
					TerminalID,
					Software,
					AppPackageName,
					SerialNumber,
					StartTime,
					Percentage,
					InstallStatus)
		VALUES(--@sId, 
			@sGroupName,
			@sTerminalID,
			@sSoftware,
			@sSoftwareDownload,
			@sSerialNumber,
			CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
			@fPercentage,
			'On Progress')
	END

	IF @fPercentage = 100 or  @fPercentage > 99.00
	BEGIN
		UPDATE tbSoftwareProgressCPLDetail
		SET InstallStatus = 'DONE', [Percentage]=100, FinishInstallTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP) 
		WHERE TerminalID = @sTerminalID 
			AND SerialNumber = @sSerialNumber 
			AND AppPackageName=@sSoftwareDownload 
			AND Software=@sSoftware 
			--AND InstallStatus ='On Progress'
			--AND BuildNumber = @iBuildNumber 
			--AND Percentage= 100 

	END

	--SELECT @fIndexGlobal = SUM(Percentage) FROM tbSoftwareProgressCPLDetail WHERE ID = @sId
	--SET @fPercentaseGlobal = @fIndexGlobal / @iTotalIndexGlobalTotal * 100
	DECLARE @iTotalDownload INT
	SELECT --@iTotalDownload=COUNT(*),
		@fIndexGlobal=SUM(Percentage) 
	FROM tbSoftwareProgressCPLDetail 
	WHERE GroupName = @sGroupName

	SELECT @iTotalDownload=COUNT(*)
		--a.GroupName,SoftwareName,ScheduleType,ScheduleTime,TerminalID 
	FROM tbSoftwareGroupPackage a JOIN tbListTerminalIDPackage b ON a.GroupName = b.GroupName
		--JOIN tbAppPackageEDCListTemp c ON a.SoftwareName=c.AppPackageName
	WHERE a.GroupName = @sGroupName

	SET @fPercentaseGlobal = @fIndexGlobal / @iTotalDownload

	PRINT '@fIndexGlobal'
	PRINT @fIndexGlobal
	PRINT '@iTotalDownload'
	PRINT @iTotalDownload

	IF @fPercentaseGlobal = 100 or @fPercentaseGlobal > 99.00
	BEGIN
		UPDATE tbSoftwareProgressCPL
			SET InstallStatus = 'SUCCESS', Percentage=100, FinishInstallTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP) 
			WHERE TerminalID = @sTerminalID 
				AND SerialNumber = @sSerialNumber 
				AND Software=@sSoftware 
				--AND SoftwareDownload=@sSoftwareDownloadGlobal 
				--AND InstallStatus ='On Progress'
				--AND BuildNumber = @iBuildNumber 
				--AND Percentage= 100 
		UPDATE tbProfileTerminalList SET RemoteDownload=0
			WHERE TerminalID=@sTerminalID
	END
	ELSE
		UPDATE tbSoftwareProgressCPL SET [Percentage] = @fPercentaseGlobal
		WHERE TerminalID=@sTerminalID 
			AND Software = @sSoftware

END