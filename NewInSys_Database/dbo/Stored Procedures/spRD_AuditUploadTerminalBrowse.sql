﻿CREATE PROCEDURE [dbo].[spRD_AuditUploadTerminalBrowse]
	@sCondition VARCHAR(500)=''
AS
DECLARE @sQuery VARCHAR(100)
SET @sQuery = '
SELECT *
FROM tbRD_AuditUploadTerminal
'
EXEC (@sQuery + @sCondition + ' ORDER BY LastUpload DESC')
