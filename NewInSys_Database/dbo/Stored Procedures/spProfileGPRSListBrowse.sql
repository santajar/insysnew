﻿
CREATE PROCEDURE [dbo].[spProfileGPRSListBrowse] 
	@sDatabaseId VARCHAR(3)
AS
	SELECT DISTINCT GPRSName [Name],
			DatabaseID			
	FROM tbProfileGPRS WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId

