﻿CREATE PROC [dbo].[spUSER_DOMAIN_BROWSE] 
	@sCondition VARCHAR(200)=NULL
AS
BEGIN
	DECLARE @sSqlstmt VARCHAR(500)
	SET @sSqlstmt =
			'SELECT *
			FROM tbUserLoginDomain WITH (NOLOCK) ' 
	IF ( @sCondition IS NULL) EXEC(@sSqlstmt)
	ELSE EXEC(@sSqlstmt + @sCondition)
	
 
END