﻿
-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <21 okt 2015>
-- Description:	<Browse List TerminalID>
-- =============================================
CREATE PROCEDURE [dbo].[spListTerminalIDPackageInsert]
	@sGroupName VARCHAR(50),
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @iCount INT
    SELECT @iCount = COUNT (*)
	FROM tbListTerminalIDPackage WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID

	IF @iCount = 0
	BEGIN
		INSERT INTO tbListTerminalIDPackage (GroupName,TerminalID) VALUES (@sGroupName,@sTerminalID)
	END
	ELSE
	BEGIN
		SELECT 'TerminalID Already Exist.'
	END

END