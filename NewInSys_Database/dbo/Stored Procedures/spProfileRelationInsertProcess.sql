﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 28, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Add new relation on profile content
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationInsertProcess]
	@sTerminalID VARCHAR(8),
	@sAcquirerValue VARCHAR(50),
	@sIssuerValue VARCHAR(50),
	@sCardValue VARCHAR(50),
	@sTag VARCHAR(5)
AS
IF (SELECT COUNT(*) FROM tbProfileRelation WITH (NOLOCK) WHERE TerminalID = @sTerminalID AND RelationTag IN ('AD01','AD001') AND RelationTagValue = @sCardValue) <> 0
BEGIN
	SELECT 'Card name ' + @sCardValue + ' already exists in ' + @sTerminalID + ' \ ' +
		dbo.sGetAcquirerOrIssuerByCard(@sTerminalID, @sCardValue, 'AA') + ' \ ' +
		dbo.sGetAcquirerOrIssuerByCard(@sTerminalID, @sCardValue, 'AE')
END
ELSE
BEGIN
	DECLARE @sSqlStmt VARCHAR(MAX)

	IF (@sTag = 'AD03' OR @sTag = 'AD003')
		EXEC spProfileRelationInsert @sTerminalID, @sAcquirerValue, @sIssuerValue, @sCardValue
	ELSE
	BEGIN
		

		CREATE TABLE #TempRelation 
		(
			ID INT IDENTITY(1,1),
			TagID INT
		)

		SET  @sSqlStmt = 'INSERT INTO #TempRelation (TagID)
							SELECT RelationTagID FROM tbProfileRelation ' + dbo.sRelationInsertCondition(@sTerminalID,@sAcquirerValue,@sIssuerValue,@sTag)
		EXEC(@sSqlStmt)
		DECLARE @iNoRecord INT
		DECLARE @iID INT
		DECLARE @iCount INT
		DECLARE @iRelationTagID INT
		DECLARE @sValue VARCHAR(50)

		SELECT @iNoRecord = COUNT(*) FROM #TempRelation
		SET @iID = 1
		SET @iCount = 0
		WHILE (@iID <= @iNoRecord)
		BEGIN
			SELECT @iRelationTagID = TagID - 1 FROM #TempRelation WHERE ID = @iID
			IF (LEN(ISNULL((SELECT RelationTagValue FROM tbProfileRelation WHERE RelationTagID = @iRelationTagID),'')) = 0 AND @iCount = 0)
			BEGIN
				SET @sValue = dbo.sRelationInsertValue (@sAcquirerValue,@sIssuerValue,@sCardValue,@sTag)
				SET @sSqlStmt ='UPDATE tbProfileRelation SET RelationTagValue = '''+@sValue+''', 
								RelationTagLength = '+CONVERT(VARCHAR,LEN(ISNULL(@sValue, '')))+', 
								RelationLengthOfTagLength = '+
						 							CONVERT(VARCHAR,LEN(CONVERT(VARCHAR,LEN(ISNULL(@sValue,''))))) + '
								WHERE RelationTagID = '+ CONVERT(NVARCHAR,@iRelationTagID)
				EXEC (@sSqlStmt)
				SET @iCount = @iCount + 1
				BREAK
			END
			SET @iID = @iID+1
		END
		
		IF (@iCount = 0)
			EXEC spProfileRelationInsert @sTerminalID, @sAcquirerValue, @sIssuerValue, @sCardValue
		DROP TABLE #TempRelation
	END

	EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
	EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

END
