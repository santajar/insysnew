﻿
-- =============================================  
-- Author: Tobias Setyo
-- Create Date: Nov 14, 2016  
-- Description: view InitialFlazz Management List 
-- =============================================  
CREATE PROCEDURE [dbo].[spProfileInitialFlazzListBrowse]  
 @sDatabaseId VARCHAR(3) = ''  
AS  
SELECT InitialFlazzID, InitialFlazzName  
FROM dbo.fn_viewInitialFlazzManagementId()
WHERE
DatabaseID = @sDatabaseId;