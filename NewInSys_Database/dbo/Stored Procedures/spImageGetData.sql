﻿-- =============================================
-- Author:		Tedie Scorfia	
-- Create date: 18 Sept 2015
-- Description:	Get Image Data
-- =============================================
CREATE PROCEDURE [dbo].[spImageGetData]
	@sFileName VARCHAR(50),
	@sType VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT a.ImageID,LogoType,[FileName],ID, ImageContent,UploadTime
	FROM tbProfileImage A
	JOIN tbProfileImageTemp B
	ON A.ImageID=B.ImageID
	WHERE A.[FileName] = @sFileName
		AND A.LogoType = @sType
	ORDER BY A.ImageID,ID
    
END