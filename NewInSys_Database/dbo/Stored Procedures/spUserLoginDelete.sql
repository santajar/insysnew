﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Agt 27, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete user login
-- =============================================
CREATE PROCEDURE [dbo].[spUserLoginDelete]
@sConditions VARCHAR(MAX)
AS
	DECLARE	@sSqlStmt VARCHAR(MAX)
	
	SET @sSqlStmt = 'DELETE tbUserLogin '
	SET @sSqlStmt = @sSqlStmt + @sConditions
	SET @sSqlStmt = @sSqlStmt + 'AND USERID != ''ADMIN'''

	EXEC(@sSqlStmt)
