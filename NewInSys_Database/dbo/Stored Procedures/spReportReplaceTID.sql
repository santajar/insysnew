﻿CREATE PROCEDURE [dbo].[spReportReplaceTID]
--	@sPath VARCHAR(200) = NULL,
	@iFlag SMALLINT = 0
AS
DECLARE @sSql VARCHAR(2000)
DECLARE @listTID TABLE(TID CHAR(8))

--SET @sSql = 'SELECT * INTO TempExcel 
--			FROM OPENROWSET(''Microsoft.Jet.OLEDB.4.0'',''Excel 8.0;Database=' + 
--			@sPath + ';HDR=YES'', ''SELECT * FROM [Sheet1$]'')'
--
----PRINT @sSql
--IF OBJECT_ID('tempdb..TempExcel') IS NOT NULL
--	DROP TABLE TempExcel
--
--EXEC (@sSql)

IF @iFlag = 0
BEGIN
	INSERT INTO @listTID (TID) 
	SELECT TIDLama FROM TempExcel WITH (NOLOCK)
END
ELSE IF @iFlag = 1
BEGIN
	INSERT INTO @listTID (TID) 
	SELECT TIDBaru FROM TempExcel WITH (NOLOCK)
END

SELECT TerminalID
	INTO #terminallist
	FROM tbProfileTerminal WITH (NOLOCK)
WHERE 
	TerminalTag='dc03' 
	AND ISNULL( TerminalTagValue,'') <> '' 
	AND TerminalID IN (SELECT TID FROM @listTID)
	--AND dbo.dtGetLastUpdateDate(TerminalTagValue) BETWEEN @dtDateStart AND @dtDateEnd

SELECT TerminalID, TerminalTag, TerminalTagValue
	INTO #terminal
	FROM tbProfileTerminal WITH (NOLOCK)
WHERE TerminalID IN (SELECT TID FROM @listTID)


SELECT TerminalID, AcquirerName, AcquirerTag, AcquirerTagValue 
	INTO #acquirer
	FROM tbProfileAcquirer WITH (NOLOCK)
WHERE TerminalID IN (SELECT TID FROM @listTID)--= '300BCA01'--IN (SELECT TerminalID FROM #terminallist)


SELECT 
	a1.TerminalID,
	(CASE WHEN b1.TerminalTagValue=1 AND c1.AcquirerTagValue NOT LIKE '%xx' THEN 1
	ELSE 0 END) Flazz
INTO #flazz	
FROM 
	#terminallist a1
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE43') b1 ON a1.TerminalID=b1.TerminalID
	LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='flazzbca' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID


SELECT 
	a1.TerminalID,
	(CASE WHEN c1.AcquirerTagValue NOT LIKE '%xx' THEN 1
	ELSE 0 END) BCA
INTO #bca	
FROM 
	#terminallist a1
	LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='BCA' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID


SELECT 
	a1.TerminalID,
	(CASE WHEN b1.TerminalTagValue=1 AND c1.AcquirerTagValue NOT LIKE '%xx' THEN 1
	ELSE 0 END) Loyalty
INTO #loyalty	
FROM 
	#terminallist a1
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE20') b1 ON a1.TerminalID=b1.TerminalID
	LEFT JOIN (SELECT * FROM #acquirer WHERE AcquirerName='LOYALTY' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID


SELECT 
	a1.TerminalID,
	(CASE WHEN b1.TerminalTagValue=1 AND c1.AcquirerTagValue NOT LIKE '%xx' THEN 1
	ELSE 0 END) PromoA
INTO #promoA
FROM 
	#terminallist a1
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE21') b1 ON a1.TerminalID=b1.TerminalID
	left JOIN (SELECT * FROM #acquirer WHERE AcquirerName in ('Promo A', 'Promo SQ A') AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID


SELECT 
	a1.TerminalID,
	(CASE WHEN b1.TerminalTagValue=1 AND c1.AcquirerTagValue NOT LIKE '%xx' THEN 1
	ELSE 0 END) PromoOther
INTO #promoOther
FROM 
	#terminallist a1
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE21') b1 ON a1.TerminalID=b1.TerminalID
	LEFT JOIN (
		SELECT * FROM #acquirer 
		WHERE AcquirerName in ('Promo B', 'Promo SQ B', 'Promo C', 'Promo SQ C','Promo D', 'Promo SQ D','Promo E', 'Promo SQ E') 
		AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID


SELECT 
	a.TerminalID,
	(CASE WHEN a.Flazz=1 AND b.TerminalTagValue=1 AND c.TerminalTagValue=1 THEN 1
	ELSE 0 END) TopUp
INTO #topup
FROM #flazz a 
	left JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE46') b ON a.TerminalID=b.TerminalID
	left JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE87') c ON a.TerminalID=c.TerminalID


SELECT 
	a.TerminalID,
	(CASE WHEN a.Flazz=1 AND b.TerminalTagValue=1 THEN 1
	ELSE 0 END) Payment
INTO #payment
FROM #flazz a 
	LEFT JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE45') b ON a.TerminalID=b.TerminalID


SELECT 
	a1.TerminalID,
	(CASE WHEN c1.AcquirerTagValue NOT LIKE '%xx' THEN 1
	ELSE 0 END) AMEX
INTO #amex	
FROM 
	#terminallist a1
	LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='AMEX' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID


SELECT 
	a1.TerminalID,
	(CASE WHEN c1.AcquirerTagValue NOT LIKE '%xx' THEN 1
	ELSE 0 END) DINERS
INTO #diners	
FROM 
	#terminallist a1
	LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='DINERS' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID


SELECT 
	a1.TerminalID,
	(CASE WHEN c1.AcquirerTagValue NOT LIKE '%xx' THEN 1
	ELSE 0 END) DEBIT
INTO #debit	
FROM 
	#terminallist a1
	LEFT OUTER JOIN (SELECT * FROM #acquirer WHERE AcquirerName='DEBIT' AND AcquirerTag='AA05') c1 ON a1.TerminalID=c1.TerminalID

/*Tambahan CST*/

SELECT TerminalID, IssuerName, IssuerTag, IssuerTagValue 
	INTO #issuer
	FROM tbProfileIssuer
WHERE TerminalID IN (SELECT TID FROM @listTID)--= '300BCA01'--IN (SELECT TerminalID FROM #terminallist)


SELECT 
	a1.TerminalID,
	i1.issuerName,
	i1.issuerTagValue AS MANUALTRX
INTO #manual	
FROM 
	#terminallist a1
	LEFT OUTER JOIN (SELECT * FROM #issuer WHERE IssuerTag='AE07') i1 ON a1.TerminalID=i1.TerminalID


SELECT 
	a1.TerminalID,
	i1.issuerName,
	i1.issuerTagValue AS OFFLINETRX
INTO #offline	
FROM 
	#terminallist a1
	LEFT OUTER JOIN (SELECT * FROM #issuer WHERE IssuerTag='AE11') i1 ON a1.TerminalID=i1.TerminalID


SELECT 
	a1.TerminalID,
	i1.issuerName,
	i1.issuerTagValue AS ADJUST
INTO #adjust	
FROM 
	#terminallist a1
	LEFT OUTER JOIN (SELECT * FROM #issuer WHERE IssuerTag='AE12') i1 ON a1.TerminalID=i1.TerminalID


SELECT 
	a1.TerminalID,
	i1.issuerName,
	i1.issuerTagValue AS REFUND
INTO #refund	
FROM 
	#terminallist a1
	LEFT OUTER JOIN (SELECT * FROM #issuer WHERE IssuerTag='AE13') i1 ON a1.TerminalID=i1.TerminalID


SELECT 
	a1.TerminalID,
	i1.issuerName,
	i1.issuerTagValue AS CARDVER
INTO #cardVer	
FROM 
	#terminallist a1
	LEFT OUTER JOIN (SELECT * FROM #issuer WHERE IssuerTag='AE14') i1 ON a1.TerminalID=i1.TerminalID
/**/


SELECT 
	a.TerminalID,
	b.TerminalTagValue MerchantName,
	c.TerminalTagValue Alamat1,
	d.TerminalTagValue Alamat2,
	e.Flazz,
	j.TopUp,
	k.Payment,
	f.BCA BCACard,
	f.BCA Visa,
	f.BCA [Master],
	l.AMEX,
	m.DINERS,
	g.Loyalty Reward,
	h.PromoA Cicilan,
	i.PromoOther CicilanPr,
	n.DEBIT,
	o.TUNAI,
	p.TerminalTagValue [F2.Pwd],
	q.TerminalTagValue AdminPwd,
	r.TerminalTagValue InitPwd,
	s.IssuerName,
	s.MANUALTRX,
	t.OFFLINETRX,
	u.ADJUST,
	v.REFUND,
	w.CARDVER
INTO #result
	FROM 
	#terminallist a 
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE02') b ON a.TerminalID=b.TerminalID
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE03') c ON a.TerminalID=c.TerminalID
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE04') d ON a.TerminalID=d.TerminalID
	JOIN #flazz e ON a.TerminalID=e.TerminalID
	JOIN #topup j ON a.TerminalID=j.TerminalID
	JOIN #payment k ON a.TerminalID=k.TerminalID
	JOIN #bca f ON a.TerminalID=f.TerminalID
	JOIN #amex l ON a.TerminalID=l.TerminalID
	JOIN #diners m ON a.TerminalID=m.TerminalID
	JOIN #loyalty g ON a.TerminalID=g.TerminalID
	JOIN (SELECT distinct TerminalID, PromoA FROM #promoA) h ON a.TerminalID=h.TerminalID
	JOIN (SELECT distinct TerminalID, PromoOther FROM #promoOther) i ON a.TerminalID=i.TerminalID
	JOIN #debit n ON a.TerminalID=n.TerminalID
	JOIN (SELECT TerminalID, (CASE WHEN TerminalTagValue='SLCS' THEN 1 ELSE 0 END) TUNAI FROM #terminal WHERE TerminalTag='DE05') o ON a.TerminalID=o.TerminalID
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE25') p ON a.TerminalID=p.TerminalID
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE28') q ON a.TerminalID=q.TerminalID
	JOIN (SELECT TerminalID, TerminalTagValue FROM #terminal WHERE TerminalTag='DE42') r ON a.TerminalID=r.TerminalID
	JOIN #manual s ON a.TerminalID=s.TerminalID
	JOIN #offline t ON a.TerminalID=t.TerminalID AND s.IssuerName = t.IssuerName
	JOIN #adjust u ON a.TerminalID=u.TerminalID AND s.IssuerName = u.IssuerName
	JOIN #refund v ON a.TerminalID=v.TerminalID AND s.IssuerName = v.IssuerName
	JOIN #cardVer w ON a.TerminalID=w.TerminalID AND s.IssuerName = w.IssuerName

SELECT distinct
	a.TerminalID,
	(CASE WHEN b.MID IS NULL THEN '-' ELSE b.MID END)MID,
	a.MerchantName,
	a.Alamat1,
	a.Alamat2,
	a.Flazz,
	a.TopUp,
	a.Payment,
	a.BCACard,
	a.Visa,
	a.[Master],
	a.AMEX,
	a.DINERS,
	a.Reward,
	a.Cicilan,
	a.CicilanPr,
	a.DEBIT,
	a.TUNAI,
	c.AcquirerTagValue,
	(CASE WHEN c.AcquirerTagValue IS NULL THEN '0' ELSE c.AcquirerTagValue END) MasterDial,
	a.[F2.Pwd],
	a.AdminPwd,
	a.InitPwd,
	a.IssuerName,
	a.MANUALTRX,
	a.OFFLINETRX,
	a.ADJUST,
	a.REFUND,
	a.CARDVER
FROM 
	#result a
	LEFT OUTER JOIN (
		SELECT mid.TerminalID, mid.MID, mid.AcquirerName
		FROM (
			SELECT a.TerminalID, RIGHT(a.AcquirerTagValue,9) MID, a.AcquirerName
			FROM #acquirer a JOIN #bca b ON a.TerminalID=b.TerminalID AND a.AcquirerName='BCA'
			WHERE a.AcquirerTag='AA04'
			union all
			SELECT a.TerminalID, RIGHT(a.AcquirerTagValue,9) MID, a.AcquirerName
			FROM #acquirer a JOIN #loyalty b ON a.TerminalID=b.TerminalID AND a.AcquirerName='LOYALTY'
			WHERE a.AcquirerTag='AA04'
			union all
			SELECT a.TerminalID, RIGHT(a.AcquirerTagValue,9) MID, a.AcquirerName
			FROM #acquirer a JOIN #debit b ON a.TerminalID=b.TerminalID AND a.AcquirerName='DEBIT'
			WHERE a.AcquirerTag='AA04'
			) mid 
			JOIN #bca bca ON mid.TerminalID=bca.TerminalID
			JOIN #loyalty loyalty ON mid.TerminalID=loyalty.TerminalID
			JOIN #debit debit ON mid.TerminalID=debit.TerminalID
		WHERE 
			mid.AcquirerName=COALESCE(
				CASE WHEN bca.BCA=1 THEN 'BCA' END,
				CASE WHEN loyalty.LOYALTY=1 THEN 'LOYALTY'
				ELSE 'DEBIT' END
			)
		) b ON a.TerminalID=b.TerminalID
	LEFT OUTER JOIN (SELECT TerminalID, AcquirerTagValue, AcquirerName FROM #acquirer WHERE AcquirerTag='AA35') c 
		ON a.TerminalID=c.TerminalID AND c.AcquirerName=b.AcquirerName


DROP TABLE #terminal

DROP TABLE #terminallist
--IF OBJECT_ID('tempdb..#acquirer') IS NOT NULL
	DROP TABLE #acquirer
--IF OBJECT_ID('tempdb..#flazz') IS NOT NULL
	DROP TABLE #flazz
--IF OBJECT_ID('tempdb..#bca') IS NOT NULL
	DROP TABLE #bca
--IF OBJECT_ID('tempdb..#loyalty') IS NOT NULL
	DROP TABLE #loyalty
--IF OBJECT_ID('tempdb..#promoA') IS NOT NULL
	DROP TABLE #promoA
--IF OBJECT_ID('tempdb..#promoOther') IS NOT NULL
	DROP TABLE #promoOther
--IF OBJECT_ID('tempdb..#topup') IS NOT NULL
	DROP TABLE #topup
--IF OBJECT_ID('tempdb..#payment') IS NOT NULL
	DROP TABLE #payment
--IF OBJECT_ID('tempdb..#amex') IS NOT NULL
	DROP TABLE #amex
--IF OBJECT_ID('tempdb..#diners') IS NOT NULL
	DROP TABLE #diners
--IF OBJECT_ID('tempdb..#debit') IS NOT NULL
	DROP TABLE #debit
--IF OBJECT_ID('tempdb..#issuer') IS NOT NULL
	DROP TABLE #issuer
--IF OBJECT_ID('tempdb..#manual') IS NOT NULL
	DROP TABLE #manual
--IF OBJECT_ID('tempdb..#offline') IS NOT NULL
	DROP TABLE #offline
--IF OBJECT_ID('tempdb..#adjust') IS NOT NULL
	DROP TABLE #adjust
--IF OBJECT_ID('tempdb..#refund') IS NOT NULL
	DROP TABLE #refund
--IF OBJECT_ID('tempdb..#cardVer') IS NOT NULL
	DROP TABLE #cardVer
--IF OBJECT_ID('tempdb..#result') IS NOT NULL
	DROP TABLE #result
