﻿
CREATE PROCEDURE [dbo].[spUserGroupItemDisabledBrowse]
	@sGroupID VARCHAR(30) = ''
AS
	SELECT DISTINCT Tag, ItemName, FormID
	FROM tbItemList WITH (NOLOCK)
	WHERE Tag NOT LIKE '%DC%' AND Tag NOT IN (SELECT Tag FROM tbUserGroup WITH (NOLOCK) WHERE GroupID = @sGroupID)
	ORDER BY FormID

