﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 24, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Copying profile content of terminal table
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalCopy]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
BEGIN
	-- select all value
	INSERT INTO tbProfileTerminal(
		TerminalId,
		TerminalTag,
		TerminalLengthOfTagLength,
		TerminalTagLength,
		TerminalTagValue,
		[Description]
		)
	SELECT @sNewTID,
		TerminalTag,
		TerminalLengthOfTagLength,
		TerminalTagLength,
		TerminalTagValue,
		[Description] 
	FROM tbProfileTerminal (NOLOCK)
	WHERE TerminalId=@sOldTID
	
	-- TerminalName
	--IF (dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sNewTID))=5)
	--BEGIN
	UPDATE tbProfileTerminal
	SET TerminalTagValue=@sNewTID
	WHERE TerminalId=@sNewTID
		AND TerminalTag IN ('DE001','DE01')
	--END
	--ELSE
	--BEGIN
	--UPDATE tbProfileTerminal
	--SET TerminalTagValue=@sNewTID
	--WHERE TerminalId=@sNewTID
	--	AND TerminalTag='DE01'
	--END
END
