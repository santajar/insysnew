﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 14, 2010
-- Description:	view Card Range list
-- =============================================
CREATE PROCEDURE [dbo].[spCardListBrowse]
	@sDatabaseId VARCHAR(5)
AS
	SET NOCOUNT ON
SELECT CardId,
	DatabaseId,
	CardName [Name]
FROM tbProfileCardList WITH (NOLOCK)
WHERE DatabaseId=@sDatabaseId
ORDER BY CardName