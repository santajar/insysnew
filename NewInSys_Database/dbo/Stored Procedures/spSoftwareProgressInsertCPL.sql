﻿-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: may 30, 2017
-- Modify date:
--				1. 
-- Description:	Insert INTo tbSoftwareProgressCPL
-- =============================================

CREATE PROCEDURE [dbo].[spSoftwareProgressInsertCPL]
	@sTerminalID VARCHAR(8)=null,
	@sSoftware VARCHAR(50)=null,
	@sSoftwareDownload VARCHAR(50)=null,
	@sSerialNumber VARCHAR(25)=null,
	@fIndex FLOAT=null
AS
DECLARE
	@iTotalIndex INT,
	@fPercentage INT,
	@sGroupName VARCHAR(25),
	@sSoftwareVer NVARCHAR(100),
	@sSoftwareVerName VARCHAR(25),
	@sChar CHAR(5),
	@iAppPackId INT,
	@sId INT,
	@iCount INT
SELECT @sGroupName = GroupName FROM tbListTerminalIDPackage WHERE TerminalID = @sTerminalID

SELECT @sSoftwareVer = AppPackageContent FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackageName IN (SELECT SoftwareName FROM tbSoftwareGroupPackage WHERE GroupName =@sGroupName AND SoftwareName like '%.PAR')

SELECT @iTotalIndex=COUNT(*) 
FROM tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackageName IN (SELECT SoftwareName FROM tbSoftwareGroupPackage WHERE GroupName =@sGroupName)

SET @sSoftwareVer = '0x' + @sSoftwareVer;
SET @sSoftwareVerName =   [dbo].[HexToAsci] (@sSoftwareVer)

PRINT @fIndex
PRINT @iTotalIndex
PRINT '@fPercentage'
SET @fPercentage = (@fIndex / @iTotalIndex) * 100
PRINT @fPercentage

SELECT @iCount=COUNT(*)
FROM tbSoftwareProgressCPL WITH (NOLOCK)
WHERE --InstallStatus = 'On Progress' AND 
	TerminalID = @sTerminalID
	AND Software = @sSoftware
	--AND SoftwareDownload = @sSoftwareVerName
	--AND SerialNumber= @sSerialNumber
	
PRINT '@iCount'
PRINT @iCount

IF (@iCount>0)
BEGIN
	PRINT '@iCount>0'
	PRINT @fPercentage
	--UPDATE tbSoftwareProgressCPL SET [Percentage] = @fPercentage
	--WHERE TerminalID=@sTerminalID 
	--	AND Software = @sSoftware
		--AND SoftwareDownload = @sSoftwareVerName
END
ELSE
BEGIN
	INSERT INTO tbSoftwareprogressCPL (GroupName,TerminalID,Software,SoftwareDownload,SerialNumber,StartTime,InstallStatus)
	VALUES(	@sGroupName,
			@sTerminalID,
			@sSoftware,
		    --@sSoftwareDownload,
			@sSoftwareVerName,
			@sSerialNumber,
			CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
			'On Progress')
END

SELECT @sId = ID FROM tbSoftwareProgressCPL WITH (NOLOCK)
WHERE InstallStatus = 'On Progress'
	AND TerminalID = @sTerminalID
	AND Software = @sSoftware
	--AND SoftwareDownload = @sSoftwareVerName
	--AND SerialNumber= @sSerialNumber 



EXEC spSoftwareProgressInsertCPLDetail
	@sId, @sTerminalID,@sSoftware,@sSoftwareDownload,@sSoftwareVerName, @sSerialNumber,@fIndex,@iTotalIndex