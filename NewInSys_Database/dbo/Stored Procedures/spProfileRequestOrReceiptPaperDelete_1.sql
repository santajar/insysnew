﻿CREATE PROCEDURE [dbo].[spProfileRequestOrReceiptPaperDelete]
	@dDateFrom DATETIME,
	@dDateTo DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		RequestID,
		TerminalID,
		Software,
		SerialNumber,
		RequestTime,
		ReceiptTime,
		Status,
		NumberOfRequest
	FROM tbProfileRequestPaper WITH (NOLOCK)
	WHERE
		CAST(FLOOR(CAST(CAST(ReceiptTime AS DATETIME) AS FLOAT)) AS DATETIME) >= CAST(FLOOR(CAST(@dDateFrom AS FLOAT)) AS DATETIME) AND
		CAST(FLOOR(CAST(CAST(ReceiptTime AS DATETIME) AS FLOAT)) AS DATETIME) <= CAST(FLOOR(CAST(@dDateTo AS FLOAT)) AS DATETIME)
		--ReceiptTime >= @dDateFrom  AND ReceiptTime <= @dDateFrom  

	DELETE FROM tbProfileRequestPaper
	WHERE
		 --	ReceiptTime >= @dDateFrom  AND ReceiptTime <= @dDateFrom  

		CAST(FLOOR(CAST(CAST(ReceiptTime AS DATETIME) AS FLOAT)) AS DATETIME) >= CAST(FLOOR(CAST(@dDateFrom AS FLOAT)) AS DATETIME) AND
		CAST(FLOOR(CAST(CAST(ReceiptTime AS DATETIME) AS FLOAT)) AS DATETIME) <= CAST(FLOOR(CAST(@dDateTo AS FLOAT)) AS DATETIME)
END