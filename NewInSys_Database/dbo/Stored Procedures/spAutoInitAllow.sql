﻿CREATE PROCEDURE [dbo].[spAutoInitAllow]
	@sTerminalID CHAR(8),
	@sContent VARCHAR(5) OUTPUT
AS
SET NOCOUNT ON
DECLARE @sLastTimeInit VARCHAR(20)

SELECT @sLastTimeInit = LastInitTime
FROM tbAutoInitLog WITH (NOLOCK)
WHERE TerminalID = @sTerminalID

DECLARE @dLastInitTime DATETIME
print '@sLastTimeInit 1 ' print cast(@sLastTimeInit as varchar)


IF (ISNULL(@sLastTimeInit,'') = '')
BEGIN
	--SET @dLastInitTime =  CONVERT(DATETIME, '')
	SELECT @sLastTimeInit=InitTime
	FROM (
		SELECT TOP 1 * FROM tbAuditInit WITH(NOLOCK)
		WHERE TerminalID = 'test4042' AND StatusDesc LIKE '%complete'
		ORDER BY InitID DESC
	) A
	SET @dLastInitTime = CONVERT (DATETIME, @sLastTimeInit, 106)
END
ELSE
	BEGIN
	SET @dLastInitTime = CONVERT (DATETIME, SUBSTRING(@sLastTimeInit, 1, 6), 112)
	SET @sLastTimeInit = SUBSTRING(@sLastTimeInit, 7, LEN(@sLastTimeInit) - 6)
	SET @sLastTimeInit = SUBSTRING(@sLastTimeInit, 1, 2) + ':' + SUBSTRING(@sLastTimeInit, 3,2) 
							+ ':' + SUBSTRING (@sLastTimeInit, 5,2)
	SET @dLastInitTime = @dLastInitTime + CONVERT (DATETIME, @sLastTimeInit, 106)
END
print '@dLastInitTime ' + cast(@dLastInitTime as varchar)
print '@sLastTimeInit 2 ' print cast(@sLastTimeInit as varchar)


DECLARE @dAutoInitTimeStamp DATETIME
SELECT @dAutoInitTimeStamp = AutoInitTimeStamp
FROM tbProfileTerminalList WITH (NOLOCK)
WHERE TerminalID = @sTerminalID
	
print '@dAutoInitTimeStamp ' + cast(@dAutoInitTimeStamp as varchar)
print '@dLastInitTime ' + cast(@dLastInitTime as varchar)
print DATEDIFF (MINUTE, @dLastInitTime, @dAutoInitTimeStamp)

IF (DATEDIFF (MINUTE, @dLastInitTime, @dAutoInitTimeStamp) >= 0)
BEGIN 
print 'debug DATEDIFF (MINUTE, @dLastInitTime, @dAutoInitTimeStamp) >= 0'
	DECLARE @sInitPermission VARCHAR(64)

	SELECT @sInitPermission = Flag
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName = 'AllowInit'
		
	
	IF (@sInitPermission = '0')
	BEGIN
		SET @sContent = '1'
	END
	ELSE	
	BEGIN			
		SELECT @sContent = CONVERT (VARCHAR, ISNULL(AllowDownload,'0'))
		FROM tbProfileTerminalList WITH (NOLOCK)
		WHERE TerminalID = @sTerminalID

		DECLARE @sLastUpdate VARCHAR(50)
		SELECT @sLastUpdate = TerminalTagValue
		FROM tbProfileTerminal WITH(NOLOCK)
		WHERE TerminalID = @sTerminalID AND TerminalTag LIKE 'DC%03'

		print '@sLastUpdate ' + @sLastUpdate
		print DATEDIFF(MINUTE, @dAutoInitTimeStamp, dbo.dtGetLastUpdateDate(@sLastUpdate))
		print dbo.dtGetLastUpdateDate(@sLastUpdate)

		IF @sContent = '1'
		BEGIN
		print 'masuk'
		PRINT @sLastUpdate
		PRINT dbo.dtGetLastUpdateDate(@sLastUpdate)
		PRINT @dAutoInitTimeStamp
			IF DATEDIFF(MINUTE, @dAutoInitTimeStamp, dbo.dtGetLastUpdateDate(@sLastUpdate))>= 0
			BEGIN 
			PRINT 'NILAI 1'
			SET @sContent = '1'
			END
			ELSE
			BEGIN 
			PRINT 'NILAI 0'
			SET @sContent = '0'
			END
		END
		END
END
ELSE 
BEGIN
	SET @sContent = 0
	UPDATE tbProfileTerminalList
	SET AllowDownload = '0' 
	WHERE TerminalID = @sTerminalID
END
