﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <30 Maret 2016>
-- Description:	<dELETE Card Relation to all TID in Database>
-- =============================================
CREATE PROCEDURE spProfileCardRelationDeletebyIssuerAndDatabaseID
	@sDatabaseID VARCHAR(3),
	@sIssuerName VARCHAR(50),
	@sCardName VARCHAR(50),
	@sAcquirerName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	IF OBJECT_ID('Tempdb..#TempTID') IS NOT NULL
	DROP TABLE #TempTID
	SELECT TerminalID
	INTO #TempTID
	FROM tbProfileTerminalList
	WHERE DatabaseID = @sDatabaseID
		AND TerminalID IN 
			(SELECT TerminalID
			FROM tbProfileIssuer WITH (NOLOCK)
			WHERE IssuerName = @sIssuerName
				AND TerminalID IN 
					(SELECT TerminalID
					FROM tbProfileTerminalList WITH (NOLOCK)
					WHERE DatabaseID = @sDatabaseID)
			)
		AND TerminalID IN 
			(SELECT TerminalID
			FROM tbProfileAcquirer WITH (NOLOCK)
			WHERE AcquirerName = @sAcquirerName
				AND TerminalID IN 
					(SELECT TerminalID
					FROM tbProfileTerminalList WITH (NOLOCK)
					WHERE DatabaseID = @sDatabaseID)
			)

	IF OBJECT_ID('Tempdb..#TempDelRelation1') IS NOT NULL
		DROP TABLE #TempDelRelation1		
	SELECT RelationTagID
	INTO #TempDelRelation1
	FROM tbProfileRelation WITH (NOLOCK)
	WHERE TerminalID IN 
		(SELECT TerminalID 
		FROM #TempTID)
		AND RelationTagValue = @sCardName

	IF OBJECT_ID('Tempdb..#TempDelRelation2') IS NOT NULL
		DROP TABLE #TempDelRelation2
	SELECT RelationTagID 
	INTO #TempDelRelation2
	FROM tbProfileRelation WITH (NOLOCK)
	WHERE RelationTagID IN 
			(SELECT RelationTagID
			FROM #TempDelRelation1)
		OR RelationTagID IN 
			(SELECT RelationTagID+1
			FROM #TempDelRelation1)
		OR RelationTagID IN 
			(SELECT RelationTagID+2
			FROM #TempDelRelation1)

	DELETE FROM tbProfileRelation
	WHERE RelationTagID IN (SELECT RelationTagID FROM #TempDelRelation2)

END