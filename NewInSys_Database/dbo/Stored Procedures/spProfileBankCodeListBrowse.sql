﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: SEPT 3, 2014
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the list of BANK CODE
-- =============================================
CREATE PROCEDURE [dbo].[spProfileBankCodeListBrowse]
	@sDatabaseID VARCHAR(3)
AS
DECLARE @sQuery VARCHAR(MAX)
SET @sQuery = 
'SELECT DISTINCT BankCode AS [Name]
FROM tbProfileBankCode WITH (NOLOCK)
WHERE DatabaseID IN (''' + @sDatabaseID + ''') AND BankCodeTag IN (''KB01'',''KB001'')'

EXEC (@sQuery)