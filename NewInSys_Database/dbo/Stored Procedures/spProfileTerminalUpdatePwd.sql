﻿CREATE PROCEDURE [dbo].[spProfileTerminalUpdatePwd] @sTerminalId VARCHAR(8)
AS
SET NOCOUNT ON
DECLARE	@sSerNumber VARCHAR(3),
	@iDigit6 INT,
	@iDigit7 INT,
	@iDigit8 INT,
	@sPwdInit VARCHAR(4),
	@sPwdAdmin99 VARCHAR(6),
	@sPwdFunc2 VARCHAR(4)

SELECT @sSerNumber = SUBSTRING(@sTerminalId,6,3)
IF ISNUMERIC(@sSerNumber)=1
BEGIN
	SELECT @iDigit6 = CAST(SUBSTRING(@sSerNumber,1,1) AS INT)
	SELECT @iDigit7 = CAST(SUBSTRING(@sSerNumber,2,1) AS INT)
	SELECT @iDigit8 = CAST(SUBSTRING(@sSerNumber,3,1) AS INT)

	SELECT @sPwdInit = @sSerNumber + cast(((@iDigit6 + (@iDigit7*2) + @iDigit8)%10) as VARCHAR)

	--SELECT @sPwdAdmin99 = '99' + SUBSTRING(@sPwdInit,1,3) + SUBSTRING(CAST(( CAST(@sPwdInit AS int) + 2)AS VARCHAR),4,1)
	DECLARE @iPwdInit INT 
	SET @iPwdInit=CAST(@sPwdInit AS INT) + 2
	SELECT @sPwdAdmin99 = '99' + SUBSTRING(@sPwdInit,1,3) + SUBSTRING(CAST(@iPwdInit AS VARCHAR),LEN(@iPwdInit),1)

	SELECT @sPwdFunc2 = REVERSE(@sPwdInit)

	-- Function 99 Password DE24
	-- Function 2 Password	DE25
	-- Initialise Password	DE42
	
	IF (dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalId))=4)
	BEGIN
		EXEC spProfileTerminalUpdateTag @sTerminalId, 'DE24', @sPwdAdmin99
		EXEC spProfileTerminalUpdateTag @sTerminalId, 'DE25', @sPwdFunc2
		EXEC spProfileTerminalUpdateTag @sTerminalId, 'DE28', @sPwdAdmin99
		EXEC spProfileTerminalUpdateTag @sTerminalId, 'DE42', @sPwdInit
	END
	ELSE
	BEGIN
		EXEC spProfileTerminalUpdateTag @sTerminalId, 'DE024', @sPwdAdmin99
		EXEC spProfileTerminalUpdateTag @sTerminalId, 'DE025', @sPwdFunc2
		EXEC spProfileTerminalUpdateTag @sTerminalId, 'DE028', @sPwdAdmin99
		EXEC spProfileTerminalUpdateTag @sTerminalId, 'DE042', @sPwdInit
	END
END