﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Browse the defined template terminal and acquirer tag and item name
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagDEAAUnion]
@sTemplate varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ColumnName,
		Tag,
		ID
	FROM tbUploadTagDE WITH (NOLOCK) where Template  = @sTemplate

	UNION ALL

	SELECT
		ColumnName + ' ' + AcquirerName,
		Tag,
		ID
	FROM tbUploadTagAA WITH (NOLOCK) where Template  = @sTemplate
END