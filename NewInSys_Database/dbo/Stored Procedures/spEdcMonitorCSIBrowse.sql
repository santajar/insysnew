
CREATE PROCEDURE [dbo].[spEdcMonitorCSIBrowse]
@sDate DATETIME,
@sDateEnd DATETIME,
@sCondition VARCHAR(MAX) = NULL

AS



--DECLARE @sDate DATETIME,@sDateEnd DATETIME,@sCondition VARCHAR(MAX) = NULL
--SET @sDate = '2017/10/12' SET @sDateEnd = '2017/10/12' SET @sCondition = ''

DECLARE @sQuery VARCHAR(MAX), @sQueryDateNow VARCHAR(MAX), @sQueryDateParam VARCHAR(MAX),@sQueryOrderBy VARCHAR(MAX),@sQueryGab VARCHAR(MAX)
	
SET @sQueryDateNow = 'CONVERT (date, CreateDate)  >= CONVERT (date, GETDATE())  AND CONVERT (date, CreateDate) <= CONVERT (date, GETDATE()) '
SET @sQueryDateParam = 'CONVERT (date, CreateDate) >= '''+CONVERT (VARCHAR(10),CAST (@sDate AS DATE),101)+''' AND CONVERT (date, CreateDate) <= '''+CONVERT (VARCHAR(10),CAST (@sDateEnd AS DATE),101)+''''
SET @sQueryOrderBy = ' ORDER BY TerminalID ASC '
SET @sQuery = '	SELECT No= ROW_NUMBER() OVER ( order by TerminalID ASC ) 
	,TerminalID as CSI
	,TID as TerminalID
	,MID as MerchantID
	,ID
	,CreateDate
	,ModifyDate
	FROM tbEdcMonitorCSI WITH(NOLOCK)	
	'

IF @sCondition=''
BEGIN 

	IF (@sDate = '' AND @sDateEnd = '')
		BEGIN
			SELECT No= ROW_NUMBER() OVER ( order by TerminalID ASC ) 
				,TerminalID as CSI
				,TID as TerminalID
				,MID as MerchantID
				,ID
				,CreateDate
				,ModifyDate
				FROM tbEdcMonitorCSI WITH(NOLOCK)		
				WHERE CONVERT (date, CreateDate) >= CONVERT (date, GETDATE())  AND CONVERT (date, CreateDate) <= CONVERT (date, GETDATE())
				ORDER BY TerminalID 
		END
	IF (@sDate <> '' AND @sDateEnd <> '')
		--SET @sQueryGab = @sQuery + 'WHERE ' + @sQueryDateParam + @sQueryOrderBy
		BEGIN
			SELECT No= ROW_NUMBER() OVER ( order by TerminalID ASC ) 
				,TerminalID as CSI
				,TID as TerminalID
				,MID as MerchantID
				,ID
				,CreateDate
				,ModifyDate
				FROM tbEdcMonitorCSI WITH(NOLOCK)			
				WHERE CONVERT (date, CreateDate) >= CONVERT (date, @sDate)  AND CONVERT (date, CreateDate) <= CONVERT (date, @sDateEnd)
				ORDER BY TerminalID
		END

END 
ELSE IF @sCondition <>''
BEGIN
	IF (@sDate = '' AND @sDateEnd = '')
		SET @sQueryGab = @sQuery + @sCondition + ' AND ' +@sQueryDateNow + @sQueryOrderBy
	IF (@sDate <> '' AND @sDateEnd <> '')
		SET @sQueryGab = @sQuery + @sCondition + ' AND ' + @sQueryDateParam + @sQueryOrderBy
END
EXEC (@sQueryGab)
PRINT @sQueryGab

GO

