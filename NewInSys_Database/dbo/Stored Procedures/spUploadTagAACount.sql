﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 6, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Counting how many rows tag template allready defined
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagAACount]
@sCond VARCHAR(MAX)
AS
BEGIN
DECLARE @sQuery VARCHAR(MAX)
	SET NOCOUNT ON;
	SET @sQuery = 'SELECT COUNT(*) FROM tbUploadTagAA WITH (NOLOCK) '
	IF @sCond is not null
	BEGIN
		SET @sQuery = @sQuery + @sCond
		EXEC (@sQuery)
	END
	ELSE
		EXEC (@sQuery)
	
END