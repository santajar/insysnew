﻿
CREATE PROCEDURE [dbo].[spAuditInitTempInsert]
	@sTerminalID CHAR(8),
	@sSoftware VARCHAR(25),
	@sSerialNum VARCHAR(20),
	@sInitTime DATETIME,
	@sStatus VARCHAR(50)
AS
	SET NOCOUNT ON
INSERT INTO	tbAuditInitTemp(
	TerminalID,
	Software,
	SerialNumber,
	InitTime,
	StatusDesc)
VALUES(
	@sTerminalID,
	@sSoftware,
	@sSerialNum,
	@sInitTime,
	@sStatus)

