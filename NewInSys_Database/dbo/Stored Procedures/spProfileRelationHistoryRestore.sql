﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: February 26, 2016
-- Modify date: 
-- Description:	
--				1. INSERT The History Relation table content of profile Acquirer
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationHistoryRestore]
	@sTerminalID VARCHAR(8),
	@sDate VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sDateNow VARCHAR(20)= CONVERT(VARCHAR(20),GETDATE(),113)

	INSERT INTO tbProfileRelationHistory(TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue,LastUpdate)
	SELECT TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue,@sDateNow
	FROM tbProfileRelation WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
	ORDER BY RelationTagID

	DELETE FROM tbProfileRelation
	WHERE TerminalID = @sTerminalID

	INSERT INTO tbProfileRelation(TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue)
	SELECT TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue
	FROM tbProfileRelationHistory
	WHERE LastUpdate = @sDate
	ORDER BY RelationTagID
	
	-- SYNCRONIZE ACQUIRER AND ISSUER
	SELECT * 
	INTO #TempRelation
	FROM tbProfileRelation WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
	ORDER BY RelationTagID

	UPDATE #TempRelation
	SET RelationTagValue = ''
	WHERE RelationTagValue IS NULL

	SELECT IssuerName
	INTO #TempListIssuer
	FROM tbProfileIssuer
	WHERE TerminalID = @sTerminalID
		AND IssuerName NOT IN 
			(SELECT RelationTagValue
			FROM #TempRelation
			WHERE RelationTag IN ('AD002','AD02'))
		AND IssuerTag IN ('AE001','AE01')

	INSERT INTO tbProfileIssuerHistory(TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue,LastUpdate)
	SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue,@sDateNow
	FROM tbProfileIssuer WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID 
		AND IssuerName IN 
			(SELECT IssuerName
			FROM #TempListIssuer)

	DELETE FROM tbProfileIssuer
	WHERE TerminalID = @sTerminalID
		AND IssuerName IN 
			(SELECT IssuerName
			FROM #TempListIssuer)

	SELECT AcquirerName
	INTO #TempListAcq
	FROM tbProfileAcquirer
	WHERE TerminalID = @sTerminalID
		AND AcquirerName NOT IN 
			(SELECT RelationTagValue
			FROM #TempRelation
			WHERE RelationTag IN ('AD001','AD01'))
		AND AcquirerTag IN ('AA001','AA01')
	
	INSERT INTO tbProfileAcquirerHistory(TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue,LastUpdate)
	SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue,@sDateNow
	FROM tbProfileAcquirer WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID 
		AND AcquirerName IN 
			(SELECT AcquirerName
			FROM #TempListAcq)
	
	DELETE FROM tbProfileAcquirer
	WHERE TerminalID = @sTerminalID
		AND AcquirerName IN 
			(SELECT AcquirerName
			FROM #TempListAcq)

	DROP TABLE #TempRelation
	DROP TABLE #TempListIssuer
	DROP TABLE #TempListAcq
END