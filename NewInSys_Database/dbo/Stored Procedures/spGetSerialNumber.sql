﻿
-- =============================================
-- Author:		Ibnu Saefulah
-- Create date: 20 maret 2017
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[spGetSerialNumber] 
	-- Add the parameters for the stored procedure here
	
	@sTerminalid varchar(8)
	

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT SerialNumber FROM TbProfileSerialNumberList WHERE TerminalID = @sTerminalid
END