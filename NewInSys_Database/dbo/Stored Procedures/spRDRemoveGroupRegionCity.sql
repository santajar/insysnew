﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date,,24 April 2014>
-- Description:	<Description,,Remove Group,Region or City >
-- =============================================
CREATE PROCEDURE [dbo].[spRDRemoveGroupRegionCity]
	@sCategory VARCHAR(10), @sCategoryName VARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @iID INT
	IF (@sCategory = 'Group')
	BEGIN
		SELECT @iID = IdGroupRD
		FROM tbGroupRemoteDownload WITH (NOLOCK)
		WHERE GroupRDName = @sCategoryName

		UPDATE tbProfileTerminalList 
		SET RemoteDownload = 0 
		WHERE TerminalID IN (
			SELECT TerminalID
			FROM tbListTIDRemoteDownload WITH (NOLOCK)	
			WHERE IdGroupRD = @iID)

		UPDATE tbListTIDRemoteDownload
		SET IdGroupRD = 0
		WHERE IdGroupRD = @iID

		EXEC spRDDeleteGroup @sCategoryName
	END
	ELSE IF ( @sCategory = 'Region')
	BEGIN
		SELECT @iID = IdRegionRD
		FROM tbRegionRemoteDownload WITH (NOLOCK)
		WHERE RegionRDName = @sCategoryName

		UPDATE tbProfileTerminalList 
		SET RemoteDownload = 0 
		WHERE TerminalID IN (
			SELECT TerminalID
			FROM tbListTIDRemoteDownload WITH (NOLOCK)
			WHERE IdRegionRD = @iID)

		UPDATE tbListTIDRemoteDownload
		SET IdRegionRD = 0
		WHERE IdRegionRD = @iID

		EXEC spRDDeleteRegion @sCategoryName
	END
	ELSE
	BEGIN
		SELECT @iID = IdCityRD
		FROM tbCityRemoteDownload WITH (NOLOCK)
		WHERE CityRDName = @sCategoryName

		UPDATE tbProfileTerminalList 
		SET RemoteDownload = 0 
		WHERE TerminalID IN (
			SELECT TerminalID
			FROM tbListTIDRemoteDownload WITH (NOLOCK)	
			WHERE IdCityRD = @iID)

		UPDATE tbListTIDRemoteDownload
		SET IdCityRD = 0
		WHERE IdCityRD = @iID
		
		EXEC spRDDeleteCity @sCategoryName
	END
END