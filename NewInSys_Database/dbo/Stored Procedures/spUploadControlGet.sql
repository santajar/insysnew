﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Get the starting row process of the defined template
-- =============================================
CREATE PROCEDURE [dbo].[spUploadControlGet]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT CAST(Flag AS SMALLINT)
	FROM tbControlFlag
	WHERE ItemName='StartingRow'
END
