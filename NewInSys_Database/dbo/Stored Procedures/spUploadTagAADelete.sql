﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 6, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Clear the tbUploadTagAA table, when define new template
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagAADelete]
@sCond VARCHAR(MAX)
AS
BEGIN
	DECLARE @sQuery VARCHAR(MAX)
	SET NOCOUNT ON;
	SET @sQuery = 'DELETE FROM tbUploadTagAA '
	IF @sCond is not null
	BEGIN
		SET @sQuery = @sQuery + @sCond
		EXEC(@sQuery)
	END
	ELSE 
	EXEC (@sQuery)
END