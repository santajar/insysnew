﻿-- =============================================
-- Author:		Kiki Ariady
-- Create date: Oct 7, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Process the terminal table of profile Data Upload
-- =============================================
CREATE PROCEDURE [dbo].[spDataUploadProcessTerminalAdd]
	@TerminalTable VARCHAR(150)
AS
DECLARE @sTID CHAR(8),
	@sTidMaster CHAR(8),
	@iOffsetStart INT, 
	@iOffsetEnd INT,
	@sColumnName VARCHAR(50),
	@sTag VARCHAR(5),
	@sNewTagValue VARCHAR(MAX),
	@SqlStmt NVARCHAR(MAX),
	@iIdStart INT,
	@iIdMax INT,
	@sQuery VARCHAR(MAX)

IF OBJECT_ID('tempdb..#tbUpload') IS NOT NULL
	DROP TABLE #tbUpload

CREATE TABLE #tbUpload (
	Id INT IDENTITY(1,1),
	TerminalId CHAR(8),
	[Master] CHAR(8)
)

SET NOCOUNT ON
SET @sQuery ='
	INSERT INTO #tbUpload(TerminalId,[Master])
	SELECT TerminalId, [Master] FROM ' + @TerminalTable + ' WITH (NOLOCK)'

EXEC (@sQuery)

SELECT @iIdStart = MIN(Id), @iIdMax = MAX(Id)
FROM #tbUpload

WHILE @iIdStart <= @iIdMax
BEGIN
	SELECT @sTID = TerminalId, @sTidMaster = [Master]
	FROM #tbUpload
	WHERE Id = @iIdStart
	IF dbo.IsTerminalIdExist(@sTID)=0
	BEGIN
		-- run sp untuk copy terminal
		DECLARE @iDbId INT
		SET @iDbId = dbo.iDatabaseIdByTerminalId(@sTidMaster)
		EXEC spProfileTerminalCopyProcess @iDbId, @sTidMaster, @sTID

		-- mulai process update per tag
		SELECT @iOffsetStart=MIN(id) + 1 FROM tbUploadTagDE
		SELECT @iOffsetEnd=MAX(id) FROM tbUploadTagDE

		WHILE @iOffsetStart <= @iOffsetEnd
		BEGIN
			SELECT @sColumnName=ColumnName, @sTag=Tag
			FROM tbUploadTagDE
			WHERE id = @iOffsetStart
			
--			print '@sColumnName='+ @sColumnName + ', @sTag=' + @sTag

			IF @sColumnName != 'Master'
			BEGIN
				SET @SqlStmt = 'SELECT @sNewTagValue=['+ @sColumnName + ']
					FROM ' + @TerminalTable + ' WITH (NOLOCK)
					WHERE TerminalId=''' + @sTID + ''''
--				print @SqlStmt
				EXEC sp_executesql @SqlStmt, N'@sNewTagValue VARCHAR(MAX) OUTPUT', @sNewTagValue=@sNewTagValue OUTPUT

				EXEC spProfileTerminalUpdateTag @sTID, @sTag, @sNewTagValue
			END
			SET @iOffsetStart = @iOffsetStart + 1
		END
	END
	SET @iIdStart = @iIdStart + 1
END