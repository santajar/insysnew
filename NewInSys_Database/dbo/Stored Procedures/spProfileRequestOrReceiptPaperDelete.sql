IF OBJECT_ID ('dbo.spProfileRequestOrReceiptPaperDelete') IS NOT NULL
	DROP PROCEDURE dbo.spProfileRequestOrReceiptPaperDelete
GO

-- =============================================
-- Author:		Tatang Sumarta
-- Create date: Okt 31, 2017
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Clear the Request Receipt Paper
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRequestOrReceiptPaperDelete]
	@dDateFrom DATETIME,
	@dDateTo DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		RequestID,
		TerminalID,
		Software,
		SerialNumber,
		RequestTime,
		ReceiptTime,
		Status,
		NumberOfRequest
	FROM tbProfileRequestPaper WITH (NOLOCK)
	WHERE
		CAST(FLOOR(CAST(CAST(ReceiptTime AS DATETIME) AS FLOAT)) AS DATETIME) >= CAST(FLOOR(CAST(@dDateFrom AS FLOAT)) AS DATETIME) AND
		CAST(FLOOR(CAST(CAST(ReceiptTime AS DATETIME) AS FLOAT)) AS DATETIME) <= CAST(FLOOR(CAST(@dDateTo AS FLOAT)) AS DATETIME)
		--ReceiptTime >= @dDateFrom  AND ReceiptTime <= @dDateFrom  

	DELETE FROM tbProfileRequestPaper
	WHERE
		 --	ReceiptTime >= @dDateFrom  AND ReceiptTime <= @dDateFrom  

		CAST(FLOOR(CAST(CAST(ReceiptTime AS DATETIME) AS FLOAT)) AS DATETIME) >= CAST(FLOOR(CAST(@dDateFrom AS FLOAT)) AS DATETIME) AND
		CAST(FLOOR(CAST(CAST(ReceiptTime AS DATETIME) AS FLOAT)) AS DATETIME) <= CAST(FLOOR(CAST(@dDateTo AS FLOAT)) AS DATETIME)
END


GO

