﻿
-- =============================================
-- Author:		<Author,Tedie Scorfia>
-- Create date: <Create Date,18 February 2014>
-- Description:	<Description,Make New TerminalID FROM another TID with TID Master As Default value>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCopyDataTerminalID]
	@sNewTerminalID VARCHAR(8)=null,
	@sRefTerminalID VARCHAR(8),
	@sMasterTerminalID VARCHAR(8),
	@iTypeFunction int
AS
BEGIN
	IF OBJECT_ID('tempDB..#TempRelationAdd') IS NOT NULL
		DROP TABLE #TempRelationAdd
	CREATE TABLE #TempRelationAdd
	(
		ID INT IDENTITY(1,1),
		RelationTagID BIGINT
	)
	IF OBJECT_ID('tempDB..#TempRelationDel') IS NOT NULL
		DROP TABLE #TempRelationDel
	CREATE TABLE #TempRelationDel
	(
		ID INT IDENTITY(1,1),
		RelationTagID BIGINT
	)

	IF OBJECT_ID('tempDB..#TempProfileADD') IS NOT NULL
		DROP TABLE #TempProfileADD
	CREATE TABLE #TempProfileADD
	(
		ID INT IDENTITY(1,1),
		TagID BIGINT,
		TerminalID VARCHAR(8),
		Name VARCHAR(50),
		Tag VARCHAR(5),
		LengthOfTagLength INT,
		TagLength INT,
		TagValue VARCHAR(150)
	)

	IF OBJECT_ID('tempDB..#TempProfile') IS NOT NULL
		DROP TABLE #TempProfile
	CREATE TABLE #TempProfile
	(
		TagID int,
		TerminalID VARCHAR(8),
		Name VARCHAR(50),
		Tag VARCHAR(5),
		LengthOfTagLength int,
		TagLength int,
		TagValue VARCHAR(50)
	)

	IF OBJECT_ID('tempDB..#TempProfileRelation') IS NOT NULL
		DROP TABLE #TempProfileRelation
	CREATE TABLE #TempProfileRelation(
		RelationTagID bigint,
		TerminalID varchar(8),
		RelationTag varchar(5),
		RelationLengthOfTagLength int,
		RelationTagLength int,
		RelationTagValue varchar(50))

	DECLARE @iDestinationDB INT,
			@iBaseDB INT,
			@isCountNewTerminalID INT,
			@iIndexTag INT,
			@iTotalTag INT,
			@sTag VARCHAR(5),
			@sItemNameBase VARCHAR(50),
			@sItemNameDest VARCHAR(50),
			@iTotalDifferentTag int,
			@sNewTag VARCHAR(5),
			@iTagID BIGINT
	SET NOCOUNT ON;

	SELECT @iDestinationDB= DatabaseID FROM tbProfileTerminalList WHERE TerminalID = @sMasterTerminalID
	SELECT @iBaseDB= DatabaseID FROM tbProfileTerminalList WHERE TerminalID = @sRefTerminalID
	SELECT @isCountNewTerminalID=COUNT(*) FROM tbProfileTerminalList WHERE TerminalID= @sNewTerminalID

	IF (@iTypeFunction=0)
	BEGIN
		IF (@isCountNewTerminalID=0)
		BEGIN
			INSERT INTO tbProfileTerminalList(TerminalID,DatabaseID,AllowDownload,StatusMaster,LastView,EnableIP,IPAddress,AutoInitTimeStamp,LocationID,InitCompress,AppPackID)
			SELECT @sNewTerminalID,@iDestinationDB,AllowDownload,StatusMaster,LastView,EnableIP,IPAddress,AutoInitTimeStamp,LocationID,InitCompress,AppPackID 
			FROM tbProfileTerminalList WITH (NOLOCK)
			WHERE TerminalID=@sRefTerminalID
			EXEC spProfileTerminalCopy @sRefTerminalID, @sNewTerminalID
			EXEC spProfileAcquirerCopy @sRefTerminalID, @sNewTerminalID
			EXEC spProfileIssuerCopy @sRefTerminalID, @sNewTerminalID
			EXEC spProfileRelationCopy @sRefTerminalID, @sNewTerminalID
		END
	END
	ELSE
	BEGIN
		SET @sNewTerminalID = @sRefTerminalID
		UPDATE tbProfileTerminalList
		SET DatabaseID = @iDestinationDB
		WHERE TerminalID = @sNewTerminalID
	END
	
		INSERT INTO #TempProfile(TagID,TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue)
		SELECT A1.AcquirerTagID,A1.TerminalID,A1.AcquirerName,B1.Tag,A1.AcquirerLengthOfTagLength,A1.AcquirerTagLength,A1.AcquirerTagValue FROM (
			SELECT A.AcquirerTagID,A.TerminalID,A.AcquirerName,B.Tag,A.AcquirerLengthOfTagLength,A.AcquirerTagLength,A.AcquirerTagValue,B.ItemName FROM tbProfileAcquirer A WITH (NOLOCK) 
			JOIN tbItemList B WITH (NOLOCK) ON A.AcquirerTag=B.Tag
			WHERE AcquirerTag IN ('AA36','AA37','AA38','AA39','AA40') AND
				TerminalID=@sNewTerminalID AND
				B.DatabaseID=@iBaseDB
			)A1 JOIN
			(SELECT Tag,ItemName FROM tbItemList WHERE DatabaseID=@iDestinationDB)B1
			ON A1.ItemName=B1.ItemName
		DELETE FROM tbProfileAcquirer WHERE AcquirerTagID in (
		SELECT TagID FROM #TempProfile)
		SET IDENTITY_INSERT tbProfileAcquirer ON
		INSERT INTO tbProfileAcquirer(AcquirerTagID,TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue)
		SELECT TagID,TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue FROM #TempProfile
		SET IDENTITY_INSERT tbProfileAcquirer OFF
	
	--DELETE tag
	DELETE FROM tbProfileTerminal WHERE TerminalTag NOT IN ( SELECT Tag FROM tbItemList WITH (NOLOCK) WHERE DatabaseID= @iDestinationDB) AND TerminalID= @sNewTerminalID
	DELETE FROM tbProfileAcquirer WHERE AcquirerTag NOT IN ( SELECT Tag FROM tbItemList WITH (NOLOCK) WHERE DatabaseID= @iDestinationDB) AND TerminalID= @sNewTerminalID
	DELETE FROM tbProfileIssuer WHERE IssuerTag NOT IN ( SELECT Tag FROM tbItemList WITH (NOLOCK) WHERE DatabaseID= @iDestinationDB) AND TerminalID= @sNewTerminalID
	
	Declare @iCountPromoTID int, @iCountPromoMaster int
---- PROMO F dan PROMO SQ A
--	Select @iCountPromoTID=COUNT(*) from tbProfileAcquirer where TerminalID = @sNewTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO F'
--	Select @iCountPromoMaster=Count(*) from tbProfileAcquirer where TerminalID = @sMasterTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO SQ A'
--	if @iCountPromoTID>0 and @iCountPromoMaster>0
--	Begin
--		Insert into #TempProfileADD (TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue)
--		SELECT @sNewTerminalID,'PROMO F',AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer Where TerminalID=@sMasterTerminalID 
--		AND AcquirerName='PROMO SQ A' 
--		AND AcquirerTag not in ( select AcquirerTag from tbProfileAcquirer where TerminalID =@sNewTerminalID and AcquirerName ='PROMO F')
--		SET @iCountPromoTID=0
--		SET @iCountPromoMaster= 0
--	END
---- Promo G dan Promo SQ B
--	Select @iCountPromoTID=COUNT(*) from tbProfileAcquirer where TerminalID = @sNewTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO G'
--	Select @iCountPromoMaster=Count(*) from tbProfileAcquirer where TerminalID = @sMasterTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO SQ B'
--	if @iCountPromoTID>0 and @iCountPromoMaster>0
--	Begin
--		Insert into #TempProfileADD (TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue)
--		SELECT @sNewTerminalID,'PROMO G',AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer Where TerminalID=@sMasterTerminalID 
--		AND AcquirerName='PROMO SQ B' 
--		AND AcquirerTag not in ( select AcquirerTag from tbProfileAcquirer where TerminalID =@sNewTerminalID and AcquirerName ='PROMO G')
--		SET @iCountPromoTID=0
--		SET @iCountPromoMaster= 0
--	END
---- Promo H dan Promo SQ C
--	Select @iCountPromoTID=COUNT(*) from tbProfileAcquirer where TerminalID = @sNewTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO H'
--	Select @iCountPromoMaster=Count(*) from tbProfileAcquirer where TerminalID = @sMasterTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO SQ C'
--	if @iCountPromoTID>0 and @iCountPromoMaster>0
--	Begin
--		Insert into #TempProfileADD (TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue)
--		SELECT @sNewTerminalID,'PROMO H',AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer Where TerminalID=@sMasterTerminalID 
--		AND AcquirerName='PROMO SQ C' 
--		AND AcquirerTag not in ( select AcquirerTag from tbProfileAcquirer where TerminalID =@sNewTerminalID and AcquirerName ='PROMO H')
--		SET @iCountPromoTID=0
--		SET @iCountPromoMaster= 0
--	END
---- Promo I dan Promo SQ D
--	Select @iCountPromoTID=COUNT(*) from tbProfileAcquirer where TerminalID = @sNewTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO I'
--	Select @iCountPromoMaster=Count(*) from tbProfileAcquirer where TerminalID = @sMasterTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO SQ D'
--	if @iCountPromoTID>0 and @iCountPromoMaster>0
--	Begin
--		Insert into #TempProfileADD (TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue)
--		SELECT @sNewTerminalID,'PROMO I',AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer Where TerminalID=@sMasterTerminalID 
--		AND AcquirerName='PROMO SQ D' 
--		AND AcquirerTag not in ( select AcquirerTag from tbProfileAcquirer where TerminalID =@sNewTerminalID and AcquirerName ='PROMO I')
--		SET @iCountPromoTID=0
--		SET @iCountPromoMaster= 0
--	END
---- Promo J dan Promo SQ E
--	Select @iCountPromoTID=COUNT(*) from tbProfileAcquirer where TerminalID = @sNewTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO J'
--	Select @iCountPromoMaster=Count(*) from tbProfileAcquirer where TerminalID = @sMasterTerminalID AND AcquirerTag = 'AA01' And AcquirerName= 'PROMO SQ E'
--	if @iCountPromoTID>0 and @iCountPromoMaster>0
--	Begin
--		Insert into #TempProfileADD (TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue)
--		SELECT @sNewTerminalID,'PROMO J',AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer Where TerminalID=@sMasterTerminalID 
--		AND AcquirerName='PROMO SQ E' 
--		AND AcquirerTag not in ( select AcquirerTag from tbProfileAcquirer where TerminalID =@sNewTerminalID and AcquirerName ='PROMO J')
--		SET @iCountPromoTID=0
--		SET @iCountPromoMaster= 0
--	END

-- BCA CASH dan CASH BCA
	Select @iCountPromoTID=COUNT(*) from tbProfileIssuer where TerminalID = @sNewTerminalID AND IssuerTag = 'AE01' And IssuerName= 'BCA CASH'
	Select @iCountPromoMaster=Count(*) from tbProfileIssuer where TerminalID = @sMasterTerminalID AND IssuerTag = 'AE01' And IssuerName= 'CASH BCA'
	if @iCountPromoTID>0 and @iCountPromoMaster>0
	Begin
		Insert into #TempProfileADD (TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue)
		SELECT @sNewTerminalID,'BCA CASH',IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue 
		FROM tbProfileIssuer WITH (NOLOCK) 
		Where TerminalID=@sMasterTerminalID 
		AND IssuerName='CASH BCA' 
		AND IssuerTag not in ( 
			select IssuerTag 
			from tbProfileIssuer WITH (NOLOCK) 
			where TerminalID =@sNewTerminalID and IssuerName ='BCA CASH')
		SET @iCountPromoTID=0
		SET @iCountPromoMaster= 0
	END
-- DELETE issuer Credit dan ganti menjadi Union Pay
	DECLARE @icountCredit INT, @icountUnionPay INT,@iIndex INT, @iTotal INT
	SELECT @icountCredit=count(*) FROM tbProfileIssuer WHERE TerminalID = @sNewTerminalID AND IssuerName like 'CREDIT' AND IssuerTag = 'AE01'
	SELECT @icountUnionPay=count(*) FROM tbProfileIssuer WHERE TerminalID = @sMasterTerminalID AND IssuerName like 'UNIONPAY%' AND IssuerTag = 'AE01'
	IF (@icountCredit>0 AND @icountUnionPay>0)
	BEGIN
		INSERT INTO #TempRelationDel(RelationTagID)
		SELECT RelationTagID 
		FROM tbprofilerelation WITH (NOLOCK) 
		WHERE TerminalID = @sNewTerminalID AND RelationTagValue='CREDIT' AND RelationTag = 'AD02'
		
		INSERT INTO #TempProfileADD (TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue)
		SELECT @sNewTerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue 
		FROM tbProfileIssuer WITH (NOLOCK) 
		WHERE terminalid= @sMasterTerminalID AND IssuerName like 'UNIONPAY%'

		INSERT INTO #TempRelationAdd(RelationTagID)
		SELECT RelationTagID 
		FROM tbProfileRelation WITH (NOLOCK) 
		WHERE TerminalID =@sMasterTerminalID AND RelationTag='AD02' AND RelationTagValue like 'UNIONPAY%'	
	END
	--BCA SYARIAH
	DECLARE @iCountCardSyariah INT, @iCountIssuerSyariah INT
	SELECT @iCountCardSyariah=COUNT(*) FROM tbProfileRelation WHERE TerminalID= @sNewTerminalID AND RelationTag = 'AD01' AND RelationTagValue = 'BCA SYARIAH'
	SELECT @iCountIssuerSyariah=COUNT(*) FROM tbProfileRelation WHERE TerminalID = @sMasterTerminalID AND RelationTag = 'AD02' AND RelationTagValue = 'BCA SYARIAH'
	IF (@iCountCardSyariah>0 AND @iCountIssuerSyariah>0)
	BEGIN
		-- RELATION YG MAU DI DEL
		INSERT INTO #TempRelationDel (RelationTagID)
		SELECT RelationTagID+1 
		FROM tbProfileRelation WITH (NOLOCK) 
		WHERE TerminalID= @sNewTerminalID AND RelationTag = 'AD01' AND RelationTagValue = 'BCA SYARIAH'

		-- DATA YANG MAU DI TAMBAH
		INSERT INTO #TempProfileADD (TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue)
		SELECT @sNewTerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue 
		FROM tbProfileIssuer WITH (NOLOCK) 
		WHERE TerminalID = @sMasterTerminalID AND IssuerName = 'BCA SYARIAH'

		INSERT INTO #TempRelationAdd (RelationTagID)
		SELECT RelationTagID 
		FROM tbProfileRelation WITH (NOLOCK) 
		WHERE TerminalID= @sMasterTerminalID AND RelationTag =  'AD02'  AND RelationTagValue = 'BCA SYARIAH'
	END
	--AddCard Maestro
	DECLARE @iCountMaestroMaster INT, @iCountMaestroTerminal INT
	SELECT @iCountMaestroTerminal=COUNT(*) FROM tbProfileIssuer WHERE IssuerTag = 'AE01' AND IssuerName ='MAESTRO' AND TerminalID	= @sNewTerminalID
	SELECT @iCountMaestroMaster=COUNT(*) FROM tbProfileIssuer WHERE IssuerTag = 'AE01' and IssuerName ='MAESTRO' and TerminalID	= @sMasterTerminalID
	IF (@iCountMaestroMaster>0 AND @iCountMaestroTerminal>0)
	BEGIN
		INSERT INTO #TempRelationAdd (RelationTagID)
		SELECT RelationTagID+1 
		FROM tbprofilerelation WITH (NOLOCK) 
		WHERE RelationTag = 'AD01' and RelationTagValue = 'MAESTRO' AND TerminalID = @sMasterTerminalID
	END

	--ADD VALUE
	Insert into tbProfileAcquirer (TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue)
	SELECT TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue 
	FROM #TempProfileADD 
	WHERE TAG LIKE 'AA__'
	Insert Into tbProfileIssuer(TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue)
	SELECT TerminalID,Name,Tag,LengthOfTagLength,TagLength,TagValue 
	FROM #TempProfileADD 
	WHERE TAG LIKE 'AE__'
	Insert Into tbProfileTerminal(TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue)
	SELECT TerminalID,Tag,LengthOfTagLength,TagLength,TagValue 
	FROM #TempProfileADD 
	WHERE TAG LIKE 'DE__'

	--DEL Relation
	DELETE FROM tbProfileRelation 
	WHERE RelationTagID in (
		SELECT RelationTagID-1 FROM #TempRelationDel
		UNION ALL
		SELECT RelationTagID FROM #TempRelationDel
		UNION ALL
		SELECT RelationTagID+1 FROM #TempRelationDel)
		
	--ADD Relation
		INSERT INTO #TempProfileRelation (RelationTagID,TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue)
		SELECT RelationTagID,TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue 
		FROM tbProfileRelation WITH (NOLOCK) 
		WHERE RelationTagID in (
			SELECT RelationTagID-1 
			FROM #TempRelationAdd)
		UNION ALL
		SELECT RelationTagID,TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue 
		FROM tbProfileRelation WITH (NOLOCK) 
		WHERE RelationTagID in (
			SELECT RelationTagID 
			FROM #TempRelationAdd)
		UNION ALL
		SELECT RelationTagID,TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue 
		FROM tbProfileRelation WITH (NOLOCK) 
		WHERE RelationTagID in (
			SELECT RelationTagID+1 
			FROM #TempRelationAdd)
		Order by RelationTagID
		
		INSERT INTO tbProfileRelation (TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue)
		SELECT @sNewTerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue 
		From #TempProfileRelation

	--insert Tag yg null
	Insert into tbProfileAcquirer (TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue)
	select @sNewTerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue 
	from (
		Select a.AcquirerTag as TagTID, b.TerminalID,b.AcquirerName,b.AcquirerTag,b.AcquirerLengthOfTagLength,b.AcquirerTagLength,b.AcquirerTagValue 
		From (
			Select * from tbProfileAcquirer WITH (NOLOCK) WHERE TerminalID =@sNewTerminalID 
			) A
		Right Join (
		Select * from tbProfileAcquirer WITH (NOLOCK) 
		WHERE TerminalID =@sMasterTerminalID AND 
		AcquirerName in (
			select distinct RelationTagValue 
			from tbProfileRelation 
			where TerminalID = @sNewTerminalID and RelationTag= 'AD03')
			) B
		ON A.AcquirerTag=B.AcquirerTag AND A.AcquirerName=B.AcquirerName
		) C 
		WHERE TagTID is null And AcquirerTag like 'AA%'
	Insert Into tbProfileIssuer(TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue)
	select @sNewTerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue 
	from (
		Select a.IssuerTag as TagTID, b.TerminalID,b.IssuerName,b.IssuerTag,b.IssuerLengthOfTagLength,b.IssuerTagLength,b.IssuerTagValue 
		From (
			Select * from tbProfileIssuer WITH (NOLOCK) WHERE TerminalID =@sNewTerminalID 
			) A
		Right Join(
		Select * from tbProfileIssuer WITH (NOLOCK) 
		WHERE TerminalID =@sMasterTerminalID AND 
		IssuerName in (
			select distinct RelationTagValue 
			from tbProfileRelation 
			where TerminalID = @sNewTerminalID and RelationTag= 'AD02')
			) B
		ON A.IssuerTag=B.IssuerTag AND A.IssuerName=B.IssuerName
		) C 
		where TagTID is null And IssuerTag like 'AE%'
	Insert Into tbProfileTerminal(TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue)
	select @sNewTerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue 
	from (
		Select a.TerminalTag as TagTID, b.TerminalID,b.TerminalTag,b.TerminalLengthOfTagLength,b.TerminalTagLength,b.TerminalTagValue 
		From (
			Select * from tbProfileTerminal WITH (NOLOCK) 
			WHERE TerminalID =@sNewTerminalID 
			) A
		Right Join(
			Select * from tbProfileTerminal WITH (NOLOCK) WHERE TerminalID =@sMasterTerminalID
			) B
		ON a.TerminalTag=b.TerminalTag 
		) C 
		where TagTID is null And TerminalTag like 'DE%'
END