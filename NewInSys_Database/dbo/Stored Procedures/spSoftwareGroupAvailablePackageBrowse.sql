﻿
CREATE PROCEDURE [dbo].[spSoftwareGroupAvailablePackageBrowse]
@sGroupName varchar(max) = null
AS
--declare @sQuery varchar(max) ='SELECT	AppPackID, 
--	AppPackageName [Software Name],
--	BuildNumber, 
--	AppPackageFilename [Package Filename],
--	AppPackageDesc [Description]	
--FROM tbAppPackageEDCList WITH (NOLOCK)
--WHERE AppPackageName NOT IN 
--		(SELECT SoftwareName 
--		from tbSoftwareGroupPackage
--		WHERE GroupName = ) '

--if @sCondition is null
--	exec (@sQuery)
--else
--begin
--	set @sQuery = @sQuery+ @sCondition
--	exec(@sQuery)
--end
SELECT	AppPackID, 
	AppPackageName [Software Name],
	AppPackageFilename [Package Filename],
	AppPackageDesc [Description]	
FROM tbAppPackageEDCList WITH (NOLOCK)
WHERE AppPackageName NOT IN 
		(SELECT SoftwareName 
		from tbSoftwareGroupPackage
		WHERE GroupName = @sGroupName)