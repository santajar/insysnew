﻿

-- ============================================================
-- Description:	Insert new TLE-EFTSec into tbItemComboBoxValue 
-- ============================================================

CREATE PROCEDURE [dbo].[spItemComboBoxValueInsertTLE] 
	@sTLEName VARCHAR(15),
	@sTLEID VARCHAR(2),
	@sDatabaseId INT
AS
DECLARE @iItemSeq INT, @iCount INT  

SELECT @iCount=count(ItemSequence)
FROM tbItemList WITH (NOLOCK)
WHERE ItemName LIKE 'TLE-EFTSec' AND 
	  FormId=2 AND 
	  DatabaseId=@sDatabaseId
	  
IF @iCount > 0
BEGIN
	SELECT @iItemSeq=ItemSequence
	FROM tbItemList WITH (NOLOCK)
	WHERE ItemName LIKE 'TLE-EFTSec' AND 
			FormId=2 AND 
			DatabaseId=@sDatabaseId

	INSERT INTO tbItemComboBoxValue(DatabaseId,
				FormId,
				ItemSequence,
				DisplayValue,
				RealValue)
	SELECT @sDatabaseId [DatabaseId], 
		2 [FormID],
		@iItemSeq [ItemSequence],
		@sTLEName [DisplayValue],
		@sTLEID [RealValue]
		
END
ELSE
	SELECT 'TLE Tag transaction not define.'