﻿
CREATE PROCEDURE [dbo].[spInitTerminalInsert]
	@sTerminalId VARCHAR(8),
	@sContent VARCHAR(MAX)=NULL
AS

DELETE FROM tbInitTerminal
WHERE TerminalId=@sTerminalId

IF ISNULL(@sContent, '') = ''
	EXEC spProfileText @sTerminalId, @sContent OUTPUT

INSERT INTO tbInitTerminal(TerminalId, TerminalContent)
VALUES (@sTerminalId, @sContent)