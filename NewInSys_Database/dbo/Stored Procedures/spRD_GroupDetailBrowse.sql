﻿CREATE PROCEDURE [dbo].[spRD_GroupDetailBrowse]
	@sCondition VARCHAR(500)=''
AS
DECLARE @sQuery VARCHAR(500)
SET @sQuery='
SELECT a.MID,a.TERMINALID
	,a.[ENABLED] [ENABLED], b.SV_NAME
FROM tbRD_GroupDetail a WITH(NOLOCK) 
	LEFT JOIN tbRD_SoftwareVersion b WITH(NOLOCK) ON a.GROUP_ID=b.GROUP_ID 
'
IF ISNULL(@sCondition,'')<>''
	SET @sQuery = @sQuery + @sCondition

EXEC (@sQuery)
