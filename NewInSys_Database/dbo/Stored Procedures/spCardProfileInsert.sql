﻿
-- ============================================================
-- Description:	 Insert new Cards into CardList and ProfileCard
-- ============================================================

CREATE PROCEDURE [dbo].[spCardProfileInsert]
	@sNewDatabaseID SMALLINT,
	@sExistingDatabaseID SMALLINT
AS
	SET NOCOUNT ON
	IF OBJECT_ID ('TempDb..#TempCard', 'U') IS NOT NULL
		DROP TABLE #TempCard

	CREATE TABLE #TempCard ( ID INT IDENTITY,
							 OldCardID INT,
							 NewCardID INT)
	DECLARE @iIndex INT,
			@iRowCount INT,
			@iOldID INT,
			@iNewID INT

	INSERT INTO #TempCard(OldCardID, NewCardID)
	SELECT A.CardID as 'OldCardID', 
		  B.CardID as 'NewCardID' 
	FROM tbProfileCardLIst A WITH (NOLOCK)
		LEFT JOIN tbProfileCardList B WITH (NOLOCK) ON A.CardName = B.CardName
	WHERE A.DatabaseID = @sExistingDatabaseID AND B.DatabaseID = @sNewDatabaseID
	
	SET @iRowCount = @@ROWCOUNT
	SET @iIndex = 1
	WHILE (@iIndex <= @iRowCount)
	BEGIN
		SELECT @iOldID = OldCardID,
			   @iNewID = NewCardID
		FROM #TempCard
		WHERE ID = @iIndex
		
		INSERT INTO tbProfileCard (CardID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue, [Description])
		SELECT @iNewID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue, [Description]
		FROM tbProfileCard WITH (NOLOCK)
		WHERE CardID = @iOldID
		SET @iIndex = @iIndex + 1
	END

	DROP TABLE #TempCard
