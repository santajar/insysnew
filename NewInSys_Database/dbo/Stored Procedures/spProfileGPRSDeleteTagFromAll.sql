﻿
CREATE PROCEDURE [dbo].[spProfileGPRSDeleteTagFromAll]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5)
AS
	DELETE FROM tbProfileGPRS
	WHERE DatabaseID = @sDatabaseID
		 AND GPRSTag = @sTag

