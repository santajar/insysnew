﻿

Create PROCEDURE [dbo].[spItemComboBoxValueDeleteRemoteDownload]
	@sRemoteDownloadName VARCHAR(15),
	@sDatabaseId INT
AS
DELETE FROM tbItemComboBoxValue
WHERE DatabaseId=@sDatabaseId
	AND RealValue=@sRemoteDownloadName