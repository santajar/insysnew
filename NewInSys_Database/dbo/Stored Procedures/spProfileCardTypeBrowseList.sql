﻿
-- =============================================  
-- Author: Tobias Setyo
-- Create Date: Nov 14, 2016  
-- Description: view CardType Management List 
-- =============================================  
CREATE PROCEDURE [dbo].[spProfileCardTypeBrowseList]  
 @sDatabaseId VARCHAR(3) = ''  
AS  
SELECT DISTINCT CardTypeName [Name]
FROM tbProfileCardType WITH(NOLOCK)
WHERE DatabaseID = @sDatabaseId;