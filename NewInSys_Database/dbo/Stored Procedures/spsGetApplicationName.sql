﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spsGetApplicationName]
	@sAppName NVARCHAR(25),
	@sReturn NVARCHAR(50) OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @iCount INT
	
	DECLARE @sLinkDatabase NVARCHAR(50),
		@sQuery NVARCHAR(MAX),
		@ParmDefinitionGetData NVARCHAR(500)

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

	IF @sAppName IS NULL
		SET @sAppName = 'HexBitMap'
	ELSE	
	BEGIN
		SET @sQuery = N'
		SELECT @iParamCount = COUNT(ApplicationName)
		FROM '+@sLinkDatabase+'tbBitMap 
		WITH (NOLOCK)
		WHERE ApplicationName=@sParamAppName'

		SET @ParmDefinitionGetData = N'@iParamCount INT OUTPUT,@sParamAppName VARCHAR(50)  ';

		EXECUTE sp_executesql @sQuery,@ParmDefinitionGetData, @iParamCount = @iCount OUTPUT, @sParamAppName = @sAppName 
		PRINT 'TEST'
		IF @iCount != 1
			SET @sAppName = 'HexBitMapStandard'

		SET @sReturn = @sAppName
	END
END