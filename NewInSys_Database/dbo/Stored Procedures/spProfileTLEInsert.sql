﻿CREATE PROCEDURE [dbo].[spProfileTLEInsert]
	@iDatabaseID INT,
	@sTLEName VARCHAR(50),
	@sTLID VARCHAR(3),
	@sContent VARCHAR(8000)=NULL
AS
	DECLARE @iCountName INT,
			@iCountID INT

	SELECT @iCountName = COUNT(*)
	FROM tbProfileTLE
	WHERE TLEName = @sTLEName AND
		  DatabaseID = @iDatabaseID
	
	IF (dbo.isTagLength4(@iDatabaseID)=4)
	BEGIN
		SELECT @iCountID = COUNT(*)
		FROM tbProfileTLE WITH (NOLOCK)
		WHERE TLETag = 'TL02' AND
			  TLETagValue = @sTLID AND
			  DatabaseID = @iDatabaseID
	END
	ELSE
	BEGIN
		SELECT @iCountID = COUNT(*)
		FROM tbProfileTLE WITH (NOLOCK)
		WHERE TLETag = 'TL002' AND
			  TLETagValue = @sTLID AND
			  DatabaseID = @iDatabaseID
	END
	IF @iCountName = 0
	BEGIN
		IF @iCountID = 0
		BEGIN
			IF @sContent IS NOT NULL
			BEGIN
				DECLARE @sTLEID VARCHAR(2)
				IF (dbo.isTagLength4(@iDatabaseID)=4)
				BEGIN
					INSERT INTO tbProfileTLE(
						TLEName,
						DatabaseID,
						TLETag,
						TLELengthOfTagLength,
						TLETagLength,
						TLETagValue)
					SELECT 
						@sTLEName,
						@iDatabaseID,
						Tag,
						LEN(TagLength),
						TagLength,
						TagValue
					FROM dbo.fn_tbprofiletlv(@sTLEName,@sContent)

					SELECT @sTLEID = TLETagValue	
				FROM tbProfileTLE WITH (NOLOCK)
				WHERE TLEName = @sTLEName AND 
					  DatabaseID = @iDatabaseID AND 
					  TLETag LIKE 'TL02'
				END
				ELSE
				BEGIN
					INSERT INTO tbProfileTLE(
						TLEName,
						DatabaseID,
						TLETag,
						TLELengthOfTagLength,
						TLETagLength,
						TLETagValue)
					SELECT 
						@sTLEName,
						@iDatabaseID,
						Tag,
						LEN(TagLength),
						TagLength,
						TagValue
					FROM dbo.fn_tbProfileTLV5(@sTLEName,@sContent)

					SELECT @sTLEID = TLETagValue	
				FROM tbProfileTLE WITH (NOLOCK)
				WHERE TLEName = @sTLEName AND 
					  DatabaseID = @iDatabaseID AND 
					  TLETag LIKE 'TL002'
				END

				EXEC spItemComboBoxValueInsertTLE @sTLEName, @sTLEID, @iDatabaseID
				DECLARE @sDatabaseID VARCHAR(3)
				SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
				EXEC spProfileTerminalUpdateLastUpdateByDbID @sDatabaseID
			END
		END
		ELSE
			SELECT 'TLE ID has already used. Please choose other TLE ID.' 
	END
	ELSE
		SELECT 'TLE-EFTSec has already used. Please choose other TLE-EFTSec.'









