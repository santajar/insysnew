﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Browse the defined template tag and item name
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagDEBrowse]
@sTemplate varchar(50)=NULL
AS
SET NOCOUNT ON;
DECLARE @sQuery VARCHAR(500)
SET @sQuery = '
	SELECT
		ID,
		Tag,
		ColumnName,
		Mandatory,
		SourceColumn
	FROM tbUploadTagDE WITH (NOLOCK) '
IF ISNULL(@sTemplate, '')<>''
SET @sQuery = @sQuery + '	WHERE Template = ' + '''' + @sTemplate + '''' 
EXEC (@sQuery)