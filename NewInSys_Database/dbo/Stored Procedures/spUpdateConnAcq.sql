﻿CREATE PROCEDURE [dbo].[spUpdateConnAcq]
	@RetVal NVARCHAR(100) OUTPUT,
	@sFlazzJakarta Varchar(3),
	@sTerminalID VARCHAR(8),
	@sTag VARCHAR(5),
	@sTagValue VARCHAR(MAX)
AS
SET NOCOUNT ON
IF @sFlazzJakarta = 'NO'
UPDATE tbProfileAcquirer SET AcquirerTagValue = @sTagValue WHERE  AcquirerTag = @sTag AND TerminalID = @sTerminalID  
IF (@@ROWCOUNT > 0 AND @sFlazzJakarta = 'NO')
UPDATE tbProfileTerminalList SET AllowDownload = 1
SET @RetVal = 'Success'

IF @sFlazzJakarta = 'YES'
UPDATE tbProfileAcquirer SET AcquirerTagValue = @sTagValue WHERE   AcquirerTagValue NOT IN ('44444444444')  AND AcquirerTag=@sTag  AND  TerminalID = @sTerminalID
IF (@@ROWCOUNT > 0 AND @sFlazzJakarta = 'YES')
UPDATE tbProfileTerminalList SET AllowDownload = 1
SET @RetVal = 'Success'
