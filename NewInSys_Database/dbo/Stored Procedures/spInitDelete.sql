﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 25, 2010
-- Modify	  :
--				1. 
-- Description:	Delete the table Init based on TerminalId
-- =============================================
CREATE PROCEDURE [dbo].[spInitDelete]
	@sTerminalId CHAR(8),
	@sCond VARCHAR(3000)=NULL
AS

DECLARE @sQuery VARCHAR(MAX)
DECLARE @sMainCond VARCHAR(MAX)
SET @sQuery='DELETE FROM tbInit'
SET @sMainCond=' WHERE TerminalId=''' + @sTerminalId + ''' '

IF @sCond IS NULL
	EXEC (@sQuery+@sMainCond)
ELSE
	EXEC (@sQuery+@sMainCond+@sCond)