﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 26, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Import profile into NewInSys database
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalImport]  @sDatabaseId VARCHAR(3),
	@sTerminalId VARCHAR(8),
	@sContent VARCHAR(MAX)
AS

CREATE TABLE #profile
(
	RowId		INT,
	TerminalID	VARCHAR(8),
	[Name]		VARCHAR(15),
	Tag			VARCHAR(4),
	TagLength		VARCHAR(3),
	TagValue		VARCHAR(500)
)
IF (dbo.isTagLength4(@sDatabaseId)=5)
BEGIN
	INSERT INTO #profile (RowId,TerminalID,Name,Tag,TagLength,TagValue)
	SELECT *  FROM dbo.fn_tbProfileTLV5(@sTerminalId,@sContent)
END
ELSE 
BEGIN
	INSERT INTO #profile (RowId,TerminalID,Name,Tag,TagLength,TagValue)
	SELECT * FROM dbo.fn_tbProfileTLV(@sTerminalId,@sContent)
END
----Terminal
INSERT INTO tbProfileTerminalList(TerminalID, DatabaseID, AllowDownload, StatusMaster) 
	VALUES (@sTerminalId, @sDatabaseId,1,0)

INSERT INTO tbProfileTerminal(
	TerminalId,
	TerminalTag,
	TerminalLengthOfTagLength,
	TerminalTagLength,
	TerminalTagValue)
SELECT TerminalId,
	Tag,
	LEN(CAST(TagLength AS INT)), 
	TagLength,
	TagValue 
FROM #profile
WHERE (Tag LIKE 'DE%' OR Tag LIKE 'DC%')
AND Tag IN (SELECT Tag FROM tbItemList WITH (NOLOCK) WHERE DatabaseID=@sDatabaseId AND FormID=1)
ORDER BY RowId
--IF OBJECT_ID('tempdb..#profile') IS NOT NULL WITH (NOLOCK)
	DROP TABLE #profile