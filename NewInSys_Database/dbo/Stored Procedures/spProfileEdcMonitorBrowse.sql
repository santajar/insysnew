﻿-- =============================================
-- Author		: Tobias SETYO
-- Create date	: March 8, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spProfileEdcMonitorBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery='
	SELECT DatabaseId,
		Tag,
		TagLength,
		TagValue as [Value]
	FROM tbProfileEdcMonitor WITH (NOLOCK) 
	'
	SET @sQuery = @sQuery + ISNULL(@sCondition,'') + ' ORDER BY Name, Tag'
EXEC (@sQuery)