﻿


-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Feb 10, 2016
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Update the Promo Management on table tbProfilepromoManagement
--				2. Modify for Form Flexsible
-- =============================================
CREATE PROCEDURE [dbo].[spProfilePromoManagementUpdate]
	@iDatabaseID INT,
	@sPromoManagementName VARCHAR(20),
	@sContent VARCHAR(MAX)
AS
	EXEC spProfilePromoManagementDelete @iDatabaseID, @sPromoManagementName
	EXEC spProfilePromoManagementInsert @iDatabaseID,@sPromoManagementName, @sContent