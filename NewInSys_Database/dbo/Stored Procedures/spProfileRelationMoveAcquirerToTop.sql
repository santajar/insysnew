﻿
CREATE PROCEDURE [dbo].[spProfileRelationMoveAcquirerToTop] @sTerminalId VARCHAR(8),
	@sAcquirerName VARCHAR(25)
AS

SET NOCOUNT ON;

EXEC spProfileRelationHistoryInsert @sTerminalId

DECLARE @iTagId INT
--IF (dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalID))=4)
--BEGIN
	SET @iTagId=(SELECT TOP 1 RelationTagId FROM tbProfileRelation WITH (NOLOCK)
	WHERE TerminalId=@sTerminalId AND RelationTag IN ('AD03','AD003') AND RelationTagValue=@sAcquirerName)
--END
--ELSE
--BEGIN
--	SET @iTagId=(SELECT TOP 1 RelationTagId FROM tbProfileRelation 
--	WHERE TerminalId=@sTerminalId AND RelationTag='AD003' AND RelationTagValue=@sAcquirerName)
--END
print @iTagId

CREATE TABLE #relation(
	ID INT IDENTITY(1,1),
	TerminalId VARCHAR(8), 
	RelationTag VARCHAR(5), 
	RelationLengthOfTagLength INT, 
	RelationTagLength INT, 
	RelationTagValue VARCHAR(50), 
	[Description] VARCHAR(50)
)

INSERT INTO #relation (TerminalId, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue, [Description])
SELECT TerminalId, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue, [Description] FROM tbProfileRelation 
WHERE TerminalId=@sTerminalId AND RelationTagId > @iTagId-3 AND RelationTagId <= @iTagId UNION ALL
SELECT TerminalId, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue, [Description] FROM tbProfileRelation 
WHERE TerminalId=@sTerminalId AND RelationTagId <= @iTagId-3 UNION ALL
SELECT TerminalId, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue, [Description] FROM tbProfileRelation 
WHERE TerminalId=@sTerminalId AND RelationTagId > @iTagId


EXEC spProfileRelationDelete @sTerminalId

INSERT INTO tbProfileRelation(TerminalId, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue, [Description])
SELECT TerminalId, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue, [Description]
FROM #relation
ORDER BY ID