﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 3, 2010
-- Modify date:
--				1. August 12, 2010, adding column IssuerTagLength
--				2. September 8, 2010, Var @sTerminalID dan @sIssuerName diubah jadi @sCondition 
--				   sebagai pengganti statement 'WHERE'
-- Description:	Get The Issuer table content of Terminal
-- =============================================
CREATE PROCEDURE [dbo].[spProfileIssuerBrowse]
	@sCondition VARCHAR(MAX)=NULL,
	@sTerminalID VARCHAR(8)
AS
	DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery = 
		'SELECT TerminalID, 
				IssuerTag AS Tag, 
				IssuerTagLength, 
				IssuerTagValue AS Value
		FROM tbProfileIssuer WITH (NOLOCK) '

	IF @sCondition IS NULL
		EXEC (@sQuery)
	ELSE
		EXEC (@sQuery + @sCondition + ' ORDER BY TerminalID, IssuerName, IssuerTag')

	EXEC spProfileTerminalUpdateLastView @sTerminalID

