﻿-- =============================================
-- Author:		SUPRIYADI SETYO
-- Create date: Agt 19, 2013
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Clear the Initialization Trail
-- =============================================
CREATE PROCEDURE [dbo].[spAuditInitSoftwareDelete]
	@dDateFrom DATETIME,
	@dDateTo DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		TerminalId,
		Software,
		SerialNumber,
		InitTime
		--,StatusDesc
	FROM tbAuditInitSoftware WITH (NOLOCK)
	WHERE
		CAST(FLOOR(CAST(CAST(InitTime AS DATETIME) AS FLOAT)) AS DATETIME) >= CAST(FLOOR(CAST(@dDateFrom AS FLOAT)) AS DATETIME) AND
		CAST(FLOOR(CAST(CAST(InitTime AS DATETIME) AS FLOAT)) AS DATETIME) <= CAST(FLOOR(CAST(@dDateTo AS FLOAT)) AS DATETIME)

	DELETE FROM tbAuditInit
	WHERE
		CAST(FLOOR(CAST(CAST(InitTime AS DATETIME) AS FLOAT)) AS DATETIME) >= CAST(FLOOR(CAST(@dDateFrom AS FLOAT)) AS DATETIME) AND
		CAST(FLOOR(CAST(CAST(InitTime AS DATETIME) AS FLOAT)) AS DATETIME) <= CAST(FLOOR(CAST(@dDateTo AS FLOAT)) AS DATETIME)
END

