﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 8, 2010
-- Modify date: 27 jul 2012
--				1. @sTerminalID CHAR(8)
-- Description:	Insert into tbAuditInit
-- =============================================
CREATE PROCEDURE [dbo].[spAuditInitInsert]
	@sTerminalID CHAR(8),
	@sSoftware VARCHAR(25),
	@sSerialNum VARCHAR(20),
	@sStatus VARCHAR(50)
AS
	SET NOCOUNT ON
INSERT INTO	tbAuditInit(
	TerminalId,
	Software,
	SerialNumber,
	InitTime,
	StatusDesc)
VALUES(
	@sTerminalID,
	@sSoftware,
	@sSerialNum,
	CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
	@sStatus)