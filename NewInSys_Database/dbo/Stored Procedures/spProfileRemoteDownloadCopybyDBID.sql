﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <March 2 2015>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRemoteDownloadCopybyDBID] 
	@NewDatabaseID VARCHAR(3),
	@OldDatabaseID VARCHAR(3),
	@sRemoteDownloadName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @iCount INT,
			@iOldTagLength INT,
			@iNewTagLength INT
	
	SELECT @iCount = COUNT(*)
	FROM tbProfileRemoteDownload WITH (NOLOCK)
	WHERE DatabaseId = @NewDatabaseID AND RemoteDownloadName = @sRemoteDownloadName
	
	SET @iOldTagLength = dbo.isTagLength4(@OldDatabaseID)
	SET @iNewTagLength = dbo.isTagLength4(@NewDatabaseID)
	
	IF @iCount = 0
	BEGIN
		IF @iOldTagLength = @iNewTagLength
		BEGIN
			INSERT INTO tbProfileRemoteDownload(DatabaseId,RemoteDownloadName,RemoteDownloadTag,RemoteDownloadLengthOfTagLength,RemoteDownloadTagLength,RemoteDownloadTagValue)
			SELECT @NewDatabaseID,RemoteDownloadName,RemoteDownloadTag,RemoteDownloadLengthOfTagLength,RemoteDownloadTagLength,RemoteDownloadTagValue
			FROM tbProfileRemoteDownload WITH (NOLOCK)
			WHERE DatabaseId = @OldDatabaseID AND RemoteDownloadName = @sRemoteDownloadName
		END
		ELSE
		BEGIN
			IF @iOldTagLength > @iNewTagLength
			BEGIN
				INSERT INTO tbProfileRemoteDownload(DatabaseId,RemoteDownloadName,RemoteDownloadTag,RemoteDownloadLengthOfTagLength,RemoteDownloadTagLength,RemoteDownloadTagValue)
				SELECT @NewDatabaseID,RemoteDownloadName,SUBSTRING(RemoteDownloadTag,1,2)+ SUBSTRING(RemoteDownloadTag,4,2),RemoteDownloadLengthOfTagLength,RemoteDownloadTagLength,RemoteDownloadTagValue
				FROM tbProfileRemoteDownload WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND RemoteDownloadName = @sRemoteDownloadName
			END
			ELSE
			BEGIN
				INSERT INTO tbProfileRemoteDownload(DatabaseId,RemoteDownloadName,RemoteDownloadTag,RemoteDownloadLengthOfTagLength,RemoteDownloadTagLength,RemoteDownloadTagValue)
				SELECT @NewDatabaseID,RemoteDownloadName,SUBSTRING(RemoteDownloadTag,1,2) +'0'+SUBSTRING(RemoteDownloadTag,3,2),RemoteDownloadLengthOfTagLength,RemoteDownloadTagLength,RemoteDownloadTagValue
				FROM tbProfileRemoteDownload WITH (NOLOCK)
				WHERE DatabaseId = @OldDatabaseID AND RemoteDownloadName = @sRemoteDownloadName
			END
		END
		
		DECLARE  @sDatabaseId INT 
		SET @sDatabaseId = CAST(@NewDatabaseID as INT)
		EXEC spItemComboBoxValueInsertRemoteDownload @sRemoteDownloadName, @sDatabaseId  
		
	END
	ELSE
		SELECT 'CAPK Index has already used. Please choose other Remote Download Name.'
	
	
END