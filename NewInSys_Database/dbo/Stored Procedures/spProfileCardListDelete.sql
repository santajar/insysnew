﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 23, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete Card range list
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCardListDelete]
	@iDatabaseId INT,
	@sCardName VARCHAR(50),
	@bIsDeleteRelation BIT = 1
AS
DELETE FROM tbProfileCard
		WHERE CardId=dbo.iCardId(@iDatabaseId,@sCardName)
	DELETE FROM tbProfileCardList
		WHERE DatabaseId=@iDatabaseId AND CardName=@sCardName

	IF (@bIsDeleteRelation = 1)
	BEGIN
		IF (dbo.isTagLength4(@iDatabaseId)=4)
		BEGIN
			UPDATE tbProfileRelation 
				SET RelationTagValue = '',
					RelationTagLength = 0
			FROM tbProfileRelation A, tbProfileTerminalList B
			WHERE B.DatabaseID = @iDatabaseId AND
				  A.TerminalID = B.TerminalID AND
				  A.RelationTag = 'AD01' AND
				  A.RelationTagValue = @sCardName		
		END
		ELSE
		BEGIN
			UPDATE tbProfileRelation 
				SET RelationTagValue = '',
					RelationTagLength = 0
			FROM tbProfileRelation A, tbProfileTerminalList B
			WHERE B.DatabaseID = @iDatabaseId AND
				  A.TerminalID = B.TerminalID AND
				  A.RelationTag = 'AD001' AND
				  A.RelationTagValue = @sCardName
		END

		DECLARE @sDatabaseID VARCHAR(3)
		SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseId) 
		EXEC spProfileTerminalUpdateLastUpdateByDbID @sDatabaseID	   
	END

	EXEC spProfileTerminalListUpdateAllowDownload @iDatabaseId, NULL