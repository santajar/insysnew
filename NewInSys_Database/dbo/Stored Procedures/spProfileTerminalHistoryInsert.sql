﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Modify date: 
-- Description:	
--				1. INSERT The History Terminal table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalHistoryInsert]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO tbProfileTerminalHistory(TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue,LastUpdate)
	SELECT TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue,CONVERT(VARCHAR(20),GETDATE(),113)
	FROM tbProfileTerminal WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
END