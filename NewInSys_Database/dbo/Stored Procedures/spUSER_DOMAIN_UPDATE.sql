﻿CREATE PROCEDURE [dbo].[spUSER_DOMAIN_UPDATE]
	@UserDomain   VARCHAR (30),
	@UserIDAccess VARCHAR (20),
	@Actived      BIT,
	@Approved     BIT
AS
	SET NOCOUNT ON
UPDATE tbUserLoginDomain
SET 
	UserIDAccess = @UserIDAccess, 
	Actived = @Actived,
	Approved = @Approved,
	ApprovedDate = getdate()
WHERE UserDomain = @UserDomain