﻿CREATE PROCEDURE [dbo].[spEdcMonitorDetailInsert]
	@iID_Header INT,
	@sTerminalID VARCHAR(8),
	@sSoftwareVersion VARCHAR(50),
	@sSettlementDateTime VARCHAR(12),
	@sCardName VARCHAR(50),
	@iCardTotalAmount INT,
	@iCardTotalTrx INT,
	@sAcquirerName VARCHAR(10)=null,
	@sTID varchar(8)=null,
	@sMID varchar(20)=null

AS
INSERT INTO tbEdcMonitorDetail(ID_Header
	,TerminalID
	,SoftwareVersion
	,SettlementDateTime
	,CardName
	,CardTotalAmount
	,CardTotalTransaction
	,AcquirerName
	,TID
	,MID)
VALUES(@iID_Header
	,@sTerminalID
	,@sSoftwareVersion
	,@sSettlementDateTime
	,@sCardName
	,@iCardTotalAmount
	,@iCardTotalTrx
	,@sAcquirerName
	,@sTID
	,@sMID)