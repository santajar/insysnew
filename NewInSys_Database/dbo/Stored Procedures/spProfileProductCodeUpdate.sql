﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: SEPT 20, 2014
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Update PRODUCT CODE
-- =============================================
CREATE PROCEDURE [dbo].[spProfileProductCodeUpdate]
	@iDatabaseID INT,
	@sProductCode VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
EXEC spProfileProductCodeDelete @iDatabaseID, @sProductCode
EXEC spProfileProductCodeInsert @iDatabaseID, @sProductCode, @sContent
