﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Agt 31, 2015
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Copying profile content of Custom Menu table
-- =============================================
Create PROCEDURE [dbo].[spProfileCustomMenuCopy]
	@sOldTID VARCHAR(8),
	@sNewTID VARCHAR(8)
AS
BEGIN
	-- select all value
	INSERT INTO tbProfileCustomMenu(
		TerminalId,
		CustomMenuTag,
		CustomMenuLengthOfTagLength,
		CustomMenuTagLength,
		CustomMenuTagValue,
		[Description]
		)
	SELECT @sNewTID,
		CustomMenuTag,
		CustomMenuLengthOfTagLength,
		CustomMenuTagLength,
		CustomMenuTagValue,
		[Description]
	FROM tbProfileCustomMenu (NOLOCK)
	WHERE TerminalId=@sOldTID

END