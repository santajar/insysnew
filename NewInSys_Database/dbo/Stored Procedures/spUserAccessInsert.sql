﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 8, 2010
-- Modify date:
--				1. 
-- Description:	Insert into tbUserAccess to prevent 
-- 2 user accessing same TerminalId
-- =============================================
CREATE PROCEDURE [dbo].[spUserAccessInsert]
	@sTerminalID VARCHAR(8),
	@sUserId VARCHAR(10)
AS
INSERT INTO	tbUserAccess(
	TerminalId,
	UserId)
VALUES(
	@sTerminalID,
	@sUserId)
