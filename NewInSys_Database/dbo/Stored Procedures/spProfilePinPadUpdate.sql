﻿/*
-- Author		: Tobias SETYO
-- Created Date	: Oct 8, 2013
-- Modify Date	: 1. <mmm dd, yyyy>, <description>
-- Description	: UPDATE tbProfilePinPad
*/
CREATE PROCEDURE [dbo].[spProfilePinPadUpdate]
--	@iDatabaseID INT,
--	@sPinPadName VARCHAR(23),
--	@sPinPadTag VARCHAR (5),
--    @iPinPadLengthOfTagLength INT,
--    @iPinPadTagLength INT,
--    @sPinPadTagValue VARCHAR (150)
--AS
--EXEC spProfilePinPadDelete @iDatabaseID, @sPinPadName
--EXEC spProfilePinPadInsert @iDatabaseID,
--	@sPinPadName,
--	@sPinPadTag,
--    @iPinPadLengthOfTagLength,
--    @iPinPadTagLength,
--    @sPinPadTagValue
	@iDatabaseID INT,
	@sPinPadName VARCHAR(25),
	@sContent VARCHAR(MAX)=NULL
AS
	EXEC spProfilePinPadDelete @iDatabaseID, @sPinPadName
	EXEC spProfilePinPadInsert @iDatabaseID, @sPinPadName,@sContent
