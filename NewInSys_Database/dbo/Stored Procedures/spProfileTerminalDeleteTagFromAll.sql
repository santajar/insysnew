﻿
CREATE PROCEDURE [dbo].[spProfileTerminalDeleteTagFromAll]
	@sDatabaseID SMALLINT,
	@sTag VARCHAR(5)
AS
	DELETE FROM tbProfileTerminal
	WHERE TerminalID IN (SELECT TerminalID 
						 FROM tbProfileTerminalList WITH (NOLOCK)
						 WHERE DatabaseID = @sDatabaseID
						)
		 AND TerminalTag = @sTag 




