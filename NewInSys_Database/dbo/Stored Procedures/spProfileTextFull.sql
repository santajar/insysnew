﻿
CREATE PROCEDURE [dbo].[spProfileTextFull] 
	@sTerminalID VARCHAR(8),
	@sContent VARCHAR(MAX) OUTPUT
AS
SET NOCOUNT ON

DECLARE @tbInitTerminal TABLE(
	Id INT IDENTITY(1,1) PRIMARY KEY,
	TerminalId VARCHAR(8),
	Name VARCHAR(50),
	Tag VARCHAR(5),
	ItemName VARCHAR(100),
	TagLength SMALLINT,
	TagValue VARCHAR(1000)
)

CREATE TABLE #TempGprs
(
	TerminalId VARCHAR(8),
	Name VARCHAR(50),
	Tag VARCHAR(5),
	ItemName VARCHAR(100),
	TagLength SMALLINT,
	TagValue VARCHAR(1000)
)


INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
FROM dbo.fn_tbProfileContentSortNew(@sTerminalId,null)
ORDER BY ID

INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
FROM dbo.fn_tbProfileContentTLE(@sTerminalId,null)

IF dbo.IsGPRSInit(@sTerminalId)=1
BEGIN
	INSERT INTO #TempGprs(TerminalId, Name, Tag, ItemName, TagLength, TagValue )
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
	FROM dbo.fn_tbProfileContentGPRS(@sTerminalID,NULL)

	SELECT AcquirerTagValue 
	INTO #TempAcq
	FROM tbProfileAcquirer
	WHERE TerminalID = @sTerminalID

	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue
	FROM #TempGprs
	WHERE NAME IN
		(SELECT AcquirerTagValue
		FROM #TempAcq)

	DROP TABLE #TempGprs
	DROP TABLE #TempAcq
END

IF dbo.IsPinPadInit(@sTerminalId)=1
BEGIN
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
	FROM dbo.fn_tbProfileContentPinPad(@sTerminalID,NULL)
END

--IF dbo.IsBankCodeInit(@sTerminalID)=1
--BEGIN
-- INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
-- SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
-- FROM dbo.fn_tbProfileContentBankCode(@sTerminalID,NULL)  
--END
 
--IF dbo.IsProductCodeInit(@sTerminalID)=1
--BEGIN
-- INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
-- SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
-- FROM dbo.fn_tbProfileContentProductCode(@sTerminalID,NULL)  
--END
  
IF DBO.IsRemoteDownloadInit(@sTerminalID)=1
BEGIN
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentRemoteDownload(@sTerminalID,NULL) 
END

IF DBO.IsEMVManagementInit(@sTerminalID)=1
BEGIN
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentEMVManagement(@sTerminalID,NULL)
END

IF dbo.IsEdcMonitoringInit(@sTerminalID)=1
BEGIN
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
	FROM dbo.fn_tbProfileContentEdcMonitoring(@sTerminalID,NULL)
END

IF DBO.IsCustomMenu(@sTerminalID)=1
BEGIN
 INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)  
 SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue   
 FROM dbo.fn_tbProfileContentCustomMenu(@sTerminalID,NULL)  
END

DECLARE @iEMVInit BIT
SET @iEMVInit=dbo.IsEMVInit(@sTerminalID)
IF @iEMVInit=1
BEGIN
	--DELETE @tbInitTerminal
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
	FROM dbo.fn_tbProfileContentAID(@sTerminalID,NULL)

	--SELECT @iIdStart = MIN(Id) FROM @tbInitTerminal
	--SELECT @iIdMax = MAX(Id) FROM @tbInitTerminal
	--WHILE @iIdStart<=@iIdMax
	--BEGIN
	--	SELECT @sTag = Tag, @sValue=TagValue
	--	FROM @tbInitTerminal
	--	WHERE Id=@iIdStart
	--	SET @sTempContent = @sTempContent + dbo.sGetTLVString(@sTag, @sValue)
	--	SET @iIdStart= @iIdStart + 1
	--END

	--DELETE @tbInitTerminal
	INSERT INTO @tbInitTerminal(TerminalId, Name, Tag, ItemName, TagLength, TagValue)
	SELECT TerminalId, Name, Tag, ItemName, TagLength, TagValue 
	FROM dbo.fn_tbProfileContentCAPK(@sTerminalId,null)

	--SELECT @iIdStart = MIN(Id) FROM @tbInitTerminal
	--SELECT @iIdMax = MAX(Id) FROM @tbInitTerminal
	--WHILE @iIdStart<=@iIdMax
	--BEGIN
	--	SELECT @sTag = Tag, @sValue=TagValue
	--	FROM @tbInitTerminal
	--	WHERE Id=@iIdStart
	--	SET @sTempContent = @sTempContent + dbo.sGetTLVStringLEN3(@sTag, @sValue)
	--	SET @iIdStart= @iIdStart + 1
END

DECLARE @sTempContent VARCHAR(MAX),
	@sTag VARCHAR(5),
	@sValue VARCHAR(500),
	@iIdStart INT,
	@iIdMax INT
SELECT @iIdStart = MIN(Id) FROM @tbInitTerminal
SELECT @iIdMax = MAX(Id) FROM @tbInitTerminal
SET @sTempContent = ''
WHILE @iIdStart<=@iIdMax
BEGIN
	SELECT @sTag = Tag, @sValue=TagValue
	FROM @tbInitTerminal
	WHERE Id=@iIdStart
	SET @sTempContent = @sTempContent + dbo.sGetTLVString(@sTag, @sValue)
	SET @iIdStart= @iIdStart + 1
END
SET @sContent = @sTempContent