﻿CREATE PROCEDURE [dbo].[spEdcTypeDelete]
	@sEdcTypeName VARCHAR(50),
	@iSuccess BIT OUTPUT
AS
DECLARE @iCount INT
SELECT @iCount = COUNT(EdcTypeName) 
FROM tbEdcType WITH (NOLOCK)

IF @iCount = 1
BEGIN
	DELETE FROM tbEdcType
	WHERE
		EdcTypeName=@sEdcTypeName
	SET @iSuccess = 1
END
ELSE
	SET @iSuccess = 0