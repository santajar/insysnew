﻿



-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 23, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. adding new Card range list
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCardListInsert]
	@sDatabaseID VARCHAR(3),
	@sCardName VARCHAR(50),
	@sContent VARCHAR(MAX)=NULL
AS
	CREATE TABLE #TempTable
	(
		ItemName VARCHAR(MAX),
		TagValue VARCHAR(MAX)
	)

INSERT INTO tbProfileCardList(
	DatabaseId,
	CardName)
VALUES(
	CAST(@sDatabaseId AS SMALLINT),
	@sCardName)
	
IF @sContent IS NOT NULL
BEGIN
	DECLARE @iCardId INT
	SET @iCardId=dbo.iCardId(@sDatabaseId,@sCardName)
	
	--INSERT INTO #TempTable(ItemName,TagValue)
	--EXEC sp_tbHistoryInsertCard @sDatabaseID, @sCardName, @iCardID
	
	IF(dbo.isTagLength4(@sDatabaseId)=5)
	BEGIN
	INSERT INTO tbProfileCard(
		CardId,
		CardTag,
		CardLengthOfTagLength,
		CardTagLength,
		CardTagValue)
	SELECT 
		@iCardId,
		Tag,
		LEN(TagLength),
		TagLength,
		TagValue
	FROM dbo.fn_tbprofiletlv5(@sCardName,@sContent)
	END
	ELSE
	BEGIN
	INSERT INTO tbProfileCard(
		CardId,
		CardTag,
		CardLengthOfTagLength,
		CardTagLength,
		CardTagValue)
	SELECT 
		@iCardId,
		Tag,
		LEN(TagLength),
		TagLength,
		TagValue
	FROM dbo.fn_tbprofiletlv(@sCardName,@sContent)
	END
	EXEC spProfileTerminalUpdateLastUpdateByDbID @sDatabaseID

	SELECT * FROM #TempTable
END




