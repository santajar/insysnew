﻿CREATE PROCEDURE [dbo].[spXMLMsProfileBrowseByXmlProfile]
	@sVersion VARCHAR(7),
	@sXmlProfile VARCHAR(8),
	@iDatabaseId INT,
	@sMaster VARCHAR(8)=NULL OUTPUT
AS
SELECT @sMaster=MasterProfile FROM tbEmsMsProfile WITH (NOLOCK)
WHERE
	EmsXmlVersion=@sVersion AND
	EmsXmlProfile=@sXmlProfile AND
	DatabaseId=@iDatabaseId



