﻿CREATE PROCEDURE [dbo].[spInitPictureLogConnInsert]
	@sTerminalID VARCHAR(8),
	@sInitType VARCHAR(100),
	@iPercentage INT,
	@iRow VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sInitTypeLike VARCHAR(30)
	SET @sInitTypeLike = @sInitType+'%'
	PRINT @sInitTypeLike

	DECLARE @sEnableLogoPrint INT

	DECLARE @iPercentageSum INT
	DECLARE @datetime DATETIME
	DECLARE @iCount INT
		
	SELECT @sEnableLogoPrint = LogoPrint FROM tbProfileTerminalList 
	WHERE TerminalID = @sTerminalID 
	
	IF @sEnableLogoPrint = 1
	BEGIN
		
	INSERT INTO tbInitLogConnDetail ([Time], TerminalID, Tag, Description, Percentage)
	VALUES (GETDATE(), @sTerminalID, @iRow, @sInitType ,@iPercentage)
	
	IF @iPercentage = 100
	BEGIN
		SELECT @datetime = [TIME] FROM tbInitLogConn
		WHERE TerminalID = @sTerminalID AND Description NOT LIKE @sInitTypeLike 
		AND Convert(DATETIME,[TIME])=Convert(DATETIME,getdate())		

		SELECT @iCount=count(*) FROM tbAuditInit
		WHERE TerminalID = @sTerminalID AND StatusDesc = 'Initialize Complete'	
		AND Convert(DATETIME,InitTime)=Convert(DATETIME,@datetime)		
		
		IF @iCount = 1
		BEGIN 
			UPDATE tbInitLogConn
			SET Description = 'Initialize Complete', Percentage = 100
			WHERE TerminalID = @sTerminalID
		END else
		IF @iCount <> 0
		BEGIN 
			UPDATE tbInitLogConn
			SET Description = 'Initialize Progress', Percentage = @iPercentage
			WHERE TerminalID = @sTerminalID
		END
	END
	ELSE
	BEGIN
		SELECT @datetime = [TIME] FROM tbInitLogConn
		WHERE TerminalID = @sTerminalID AND Description NOT LIKE @sInitTypeLike 
		AND Convert(DATETIME,[TIME])=Convert(DATETIME,getdate())		

		SELECT @iCount=count(*) FROM tbAuditInit
		WHERE TerminalID = @sTerminalID AND StatusDesc = 'Initialize Complete' 
		AND Convert(DATETIME,InitTime)=Convert(DATETIME,@datetime)			
		
		IF @iCount = 1
		BEGIN 
			UPDATE tbInitLogConn
			SET Description = 'Initialize Progress Logo', Percentage = @iPercentage
			WHERE TerminalID = @sTerminalID
		END
		IF @iCount <> 0
		BEGIN 
			UPDATE tbInitLogConn
			SET Description = 'Initialize Progress', Percentage = @iPercentage
			WHERE TerminalID = @sTerminalID
		END		
	END


	END 

    
    --==================== LogoIdle
    
    DECLARE @sEnableLogoIddle INT
	
	SELECT @sEnableLogoIddle = LogoIdle FROM tbProfileTerminalList 
	WHERE TerminalID = @sTerminalID 
	IF @sEnableLogoIddle = 1
	BEGIN
	
	
		INSERT INTO tbInitLogConnDetail ([Time], TerminalID, Tag, Description, Percentage)
		VALUES (GETDATE(), @sTerminalID, @iRow, @sInitType ,@iPercentage)
		
	 	IF @iPercentage = 100
		BEGIN
			SELECT @datetime = [TIME] FROM tbInitLogConn
			WHERE TerminalID = @sTerminalID AND Description NOT LIKE @sInitTypeLike 
			AND Convert(DATETIME,[TIME])=Convert(DATETIME,getdate())		
	
	
			SELECT @iCount=count(*) FROM tbAuditInit
			WHERE TerminalID = @sTerminalID AND StatusDesc = 'Initialize Complete'	
			AND Convert(DATETIME,InitTime)=Convert(DATETIME,@datetime)		
			
			IF @iPercentageSum = 1
			BEGIN 
				UPDATE tbInitLogConn
				SET Description = 'Initialize Complete', Percentage = 100
				WHERE TerminalID = @sTerminalID
			END
			IF @iPercentageSum <> 0
			BEGIN 
				UPDATE tbInitLogConn
				SET Description = 'Initialize Progress', Percentage = @iPercentage
				WHERE TerminalID = @sTerminalID
			END
		END
		ELSE
		BEGIN
			SELECT @datetime = [TIME] FROM tbInitLogConn
			WHERE TerminalID = @sTerminalID AND Description NOT LIKE @sInitTypeLike 
			AND Convert(DATETIME,[TIME])=Convert(DATETIME,getdate())		
	
	
			SELECT @iCount=count(*) FROM tbAuditInit
			WHERE TerminalID = @sTerminalID AND StatusDesc = 'Initialize Complete'	
			AND Convert(DATETIME,InitTime)=Convert(DATETIME,@datetime)		
			
			IF @iCount = 1
			BEGIN 
				UPDATE tbInitLogConn
				SET Description = 'Initialize Progress Logo', Percentage = @iPercentage
				WHERE TerminalID = @sTerminalID
			END
			IF @iCount <> 0
			BEGIN 
				UPDATE tbInitLogConn
				SET Description = 'Initialize Progress', Percentage = @iPercentage
				WHERE TerminalID = @sTerminalID
			END		
		END
	
	END

    
END