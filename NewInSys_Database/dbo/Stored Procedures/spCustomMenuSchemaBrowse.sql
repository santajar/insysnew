﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <11 Jan 2015>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCustomMenuSchemaBrowse]
	@sCondition VARCHAR(600) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sQuery VARCHAR(300)

	SET @sQuery = 
	'SELECT DatabaseID,Tag,ItemName,ParentTag
	FROM tbSchemaCustomMenu '

	IF @sCondition = NULL
		EXEC (@sQuery)
	ELSE
		EXEC (@sQuery+@sCondition)
	

END