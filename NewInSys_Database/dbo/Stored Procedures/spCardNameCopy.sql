﻿
-- ============================================================
-- Description:	 Insert new Cards into CardList and ProfileCard
-- Ibnu Saefullah
-- ============================================================

CREATE PROCEDURE [dbo].[spCardNameCopy]
	@iDatabaseID VARCHAR(3),
	@sOldCardName VARCHAR(50),
	@sNewCardName VARCHAR(50)

AS
	SET NOCOUNT ON
Declare
	@iNewCardID INT,
	@Content VARCHAR(MAX)=NULL,
	@iOldCardID INT,
	@iCount INT

SELECT @iCount= COUNT(*)
FROM tbProfileCardList WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID
	AND CardName = @sNewCardName

IF @iCount = 0
BEGIN
	INSERT INTO tbProfileCardList(DatabaseID,CardName)
	VALUES(@iDatabaseID,@sNewCardName)

	SELECT  @iOldCardID = CardID
	FROM tbProfileCardList WITH (NOLOCK)
	WHERE DatabaseID = @iDatabaseID 
		AND CardName = @sOldCardName

	SELECT  @iNewCardID = CardID
	FROM tbProfileCardList WITH (NOLOCK)
	WHERE DatabaseID = @iDatabaseID 
		AND CardName = @sNewCardName

	INSERT INTO tbProfileCard(CardID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue)
	SELECT @iNewCardID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue
	FROM tbProfileCard WITH (NOLOCK)
	WHERE CardID=@iOldCardID 
	
	UPDATE tbProfileCard
	 SET CardTagValue = @sNewCardName, CardTagLength = LEN(@sNewCardName)
	 WHERE CardTag IN ('AC001','AC01') AND CardID = @iNewCardID

END