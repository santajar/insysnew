﻿CREATE PROCEDURE [dbo].[spProfileTLECopyList]
	@iDatabaseID VARCHAR(3),
	@sOldTleName VARCHAR(50),
	@sNewTleName VARCHAR(50),
	@sNewTleID varchar(4)

AS
Declare
	@iNewTleTag Varchar(5),
	@iOldTleTag Varchar(5),
	@iCount INT,
	@iOldTleLengthOf varchar(2),
	@iOldTleLength varchar(3),
	@ioldTagValue Varchar(20),
	@Content VARCHAR(MAX)=NULL

SELECT @iCount= COUNT(*)
FROM tbProfileTLE WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID
	AND TLEName = @sNewTleName

IF @iCount = 0
BEGIN
	
	INSERT INTO tbProfileTLE(TLETag, DatabaseID, TLEName, TLELengthOfTagLength, TLETagLength, TLETagValue)
	SELECT TLETag, @iDatabaseID, @sNewTleName, TLELengthOfTagLength, TLETagLength, TLETagValue
	FROM tbProfileTLE WITH (NOLOCK)
	WHERE DatabaseID=@iDatabaseID AND TLEName = @sOldTleName

	
	UPDATE tbProfileTLE
	 SET TLETagValue = @sNewTleName, TLETagLength = LEN(@sNewTleName)
	 WHERE TLETag = 'TL001' AND TLEName = @sNewTleName
END

SELECT @iCount= COUNT(*)
FROM tbProfileTLE WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID 
AND TLETag = @iNewTleTag


IF @iCount = 0
	 begin
	 UPDATE tbProfileTLE
	 SET TLETagValue = @sNewTleID, TLETagLength = LEN(@sNewTleName)
	 WHERE TLETag = 'TL002' AND TLEName = @sNewTleName
	 END