﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. ????
-- =============================================
CREATE PROCEDURE [dbo].[spDataUploadDelete]
	@sTableName VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt =
		'DELETE FROM ' + @sTableName
	EXEC (@sStmt)
END
