﻿-- =============================================
-- Author		: Tobias Setyo
-- Create date	: March 16, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spEdcMonitorBrowse]
@sCondition VARCHAR(MAX) = NULL
AS
DECLARE @sQuery VARCHAR(MAX), @sQueryJoin VARCHAR(MAX)

SET @sQueryJoin = ' ORDER BY MessageTimestamp DESC '
SET @sQuery = 'SELECT a.MessageTimestamp [Timestamp]
	,a.SerialNumber [Serial Number]
	,a.SoftwareVersion [Software Version]
	,a.TerminalID
	,a.ProcCode [Proccessing Code]
	,a.TotalDip [Sum of Dip Trx]
	,a.TotalSwipe [Sum of Swipe Trx]
	,a.TotalAllTransaction [Sum of All Transaction]
	,a.TID
	,a.MID
	,a.ICCID
	--,b.SettlementDateTime [Settlement Date Time(ddMMyyhhmmss)]
	--,b.CardName [Card Label]
	--,b.CardTotalAmount [Sum of Amount per Card]
	--,b.CardTotalTransaction [Sum of Transaction per Card]
	--,LEFT(c.MCC_MNC_Data,3) [MCC]
	--,RIGHT(c.MCC_MNC_Data,2) [MNC]
	--,c.LAC_Data [LAC]
	--,c.Cell_ID_Data [CID]
FROM tbEdcMonitor a WITH(NOLOCK) 
	--LEFT JOIN tbEdcMonitorDetail b WITH(NOLOCK) ON a.ID=b.ID_Header
	--LEFT JOIN tbEdcMonitorSIM c ON a.ID=c.ID_Header
 '

IF @sCondition IS NULL
	EXEC (@sQuery + @sQueryJoin)
ELSE
	EXEC (@sQuery + @sCondition + @sQueryJoin)
