﻿
CREATE PROCEDURE [dbo].[spProfileIssuerUpdate]
@sTerminalID VARCHAR(8),
@sContent VARCHAR(MAX)
AS
DECLARE @sTID VARCHAR(8)
	DECLARE @sName VARCHAR(50)
	DECLARE @sTag VARCHAR(5)
	DECLARE @iLengthOfTagLength INT
	DECLARE @iTagLength INT
	DECLARE @sValue VARCHAR(150)

	
	CREATE TABLE #TempIss 
	(
		TerminalID VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		[LengthOfTagLength] INT,
		TagLength INT,
		TagValue VARCHAR(150)
	)
	DECLARE @sDbID VARCHAR(5),
			@sIssName VARCHAR(50)

	SET @sDbID = dbo.iDatabaseIdByTerminalId(@sTerminalID)
	IF (DBO.isTagLength4(CAST(@sDbID AS INT))=5)
	BEGIN
		INSERT INTO #TempIss(TerminalID,[Name],	Tag,[LengthOfTagLength],TagLength,TagValue)
		SELECT TerminalID,
				[Name],
				Tag,
				LEN(TagLength) [LengthOfTagLength],
				TagLength,
				TagValue
		FROM fn_tbprofiletlv5(@sTerminalID, @sContent)
	END
	ELSE
	BEGIN
		INSERT INTO #TempIss(TerminalID,[Name],	Tag,[LengthOfTagLength],TagLength,TagValue)
		SELECT TerminalID,
				[Name],
				Tag,
				LEN(TagLength) [LengthOfTagLength],
				TagLength,
				TagValue
		FROM fn_tbprofiletlv(@sTerminalID, @sContent)
	END
	
	SELECT DISTINCT @sIssName = [Name] FROM #TempIss
	EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'AE', @sIssName, @sContent, 1
	EXEC spProfileIssuerHistoryInsert @sTerminalID,@sIssName

	INSERT INTO [tbProfileIssuer]
	   ([TerminalID]
	   ,[IssuerName]
	   ,[IssuerTag]
	   ,[IssuerLengthOfTagLength]
	   ,[IssuerTagLength]
	   ,[IssuerTagValue])
	SELECT TerminalID, 
			[Name],
			Tag,
			LengthOfTagLength,
			TagLength,
			TagValue
	FROM #TempIss
	WHERE Tag COLLATE DATABASE_DEFAULT NOT IN (
						SELECT IssuerTag
						FROM tbProfileIssuer WITH (NOLOCK)
						WHERE TerminalID = @sTerminalID AND 
							  IssuerName COLLATE DATABASE_DEFAULT = [Name]
					 )

	UPDATE  Issuer
		SET Issuer.IssuerLengthOfTagLength = Temp.LengthOfTagLength,
			Issuer.IssuerTagLength = Temp.TagLength,
			Issuer.IssuerTagValue =  Temp.TagValue
	FROM tbProfileIssuer Issuer WITH (NOLOCK), #TempIss Temp
	WHERE Issuer.TerminalID = @sTerminalID AND 
		  Issuer.IssuerName COLLATE DATABASE_DEFAULT = Temp.[Name] COLLATE DATABASE_DEFAULT AND 
		  Issuer.IssuerTag COLLATE DATABASE_DEFAULT = Temp.Tag COLLATE DATABASE_DEFAULT

EXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID
EXEC spProfileTerminalUpdateLastUpdate @sTerminalID

--IF (OBJECT_ID('TEMPDB..#TempIss') IS NOT NULL)
		DROP TABLE #TempIss