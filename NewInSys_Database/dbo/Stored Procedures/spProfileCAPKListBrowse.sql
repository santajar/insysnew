﻿-- =============================================  
-- Author:  Tedie Scorfia  
-- Create date: march 2, 2015   
-- Description: view CAPK list  
-- =============================================  
Create PROCEDURE [dbo].[spProfileCAPKListBrowse]  
 @sDatabaseId VARCHAR(3) = ''  
AS  
SELECT CAPKTagID,  
	--DatabaseId,  
	CAPKIndex  
FROM dbo.fn_viewCAPK()  
WHERE DatabaseId = @sDatabaseId--case when @sDatabaseId = '' then DatabaseId else @sDatabaseId end