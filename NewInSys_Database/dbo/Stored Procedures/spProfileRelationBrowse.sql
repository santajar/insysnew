﻿CREATE PROCEDURE [dbo].[spProfileRelationBrowse]
	@sTerminalId VARCHAR(8)
AS
	SELECT TerminalId,
		RelationTag,
		RelationLengthOfTagLength,
		RelationTagLength,
		RelationTagValue
	FROM tbProfileRelation WITH (NOLOCK)
	WHERE TerminalId=@sTerminalId
	ORDER BY RelationTagID
	OPTION (RECOMPILE)