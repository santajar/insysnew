﻿CREATE PROCEDURE [dbo].[spProfileRemoteDownloadInsert]
	@iDatabaseID INT,
	@sRemoteDownloadName VARCHAR(15),
	@sContent VARCHAR(MAX)=NULL
AS
	DECLARE @iCountName INT,
			@iCountID INT

	SELECT @iCountName = COUNT(*)
	FROM tbProfileRemoteDownload WITH (NOLOCK)
	WHERE RemoteDownloadName = @sRemoteDownloadName AND
		  DatabaseID = @iDatabaseID

	SELECT @iCountID = COUNT(*)
	FROM tbProfileRemoteDownload WITH (NOLOCK)
	WHERE RemoteDownloadTag IN ('RD01','RD001') AND
		  RemoteDownloadTagValue = @sRemoteDownloadName AND
		  DatabaseID = @iDatabaseID

	IF @iCountName = 0
	BEGIN
		IF @iCountID = 0
		BEGIN
			IF @sContent IS NOT NULL
			BEGIN
				DECLARE @sRemoteDownloadID VARCHAR(2)

				IF (DBO.isTagLength4(@iDatabaseID)=5)
				BEGIN
					INSERT INTO tbProfileRemoteDownload(
						RemoteDownloadName,
						DatabaseID,
						RemoteDownloadTag,
						RemoteDownloadLengthOfTagLength,
						RemoteDownloadTagLength,
						RemoteDownloadTagValue)
					SELECT 
						@sRemoteDownloadName,
						@iDatabaseID,
						Tag,
						LEN(TagLength),
						TagLength,
						TagValue
					FROM dbo.fn_tbprofiletlv5(@sRemoteDownloadName,@sContent)

					SELECT @sRemoteDownloadID = RemoteDownloadTagValue	
					FROM tbProfileRemoteDownload WITH (NOLOCK)
					WHERE RemoteDownloadName = @sRemoteDownloadName AND 
						  DatabaseID = @iDatabaseID AND 
						  RemoteDownloadTag LIKE 'RD001'
				END
				ELSE
				BEGIN
					INSERT INTO tbProfileRemoteDownload(
						RemoteDownloadName,
						DatabaseID,
						RemoteDownloadTag,
						RemoteDownloadLengthOfTagLength,
						RemoteDownloadTagLength,
						RemoteDownloadTagValue)
					SELECT 
						@sRemoteDownloadName,
						@iDatabaseID,
						Tag,
						LEN(TagLength),
						TagLength,
						TagValue
					FROM dbo.fn_tbprofiletlv(@sRemoteDownloadName,@sContent)

					SELECT @sRemoteDownloadID = RemoteDownloadTagValue	
					FROM tbProfileRemoteDownload WITH (NOLOCK)
					WHERE RemoteDownloadName = @sRemoteDownloadName AND 
						  DatabaseID = @iDatabaseID AND 
						  RemoteDownloadTag LIKE 'RD01'
				END
				EXEC spItemComboBoxValueInsertRemoteDownload @sRemoteDownloadName, @iDatabaseID

				DECLARE @sDatabaseID VARCHAR(3)
				SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
				EXEC spProfileTerminalUpdateLastUpdateByTagAndDbID @sDatabaseID,'Remote Download Name',@sRemoteDownloadName

			END
		END
		ELSE
			SELECT 'Remote Download ID has already used. Please choose other Remote Download ID.' 
	END
	ELSE
		SELECT 'IP Primary has already used. Please choose other IP Primary.'