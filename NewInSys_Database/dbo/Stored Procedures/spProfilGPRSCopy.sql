﻿
-- ============================================================
-- Description:	 Insert new GPRS into TbProfileGPRS with different name
-- Ibnu Saefullah
-- ============================================================

CREATE PROCEDURE [dbo].[spProfilGPRSCopy]
	@iDatabaseID VARCHAR(3),
	@sOldGprsName VARCHAR(50),
	@sNewGprsName VARCHAR(50)

AS
Declare

	@iOldAIdTag Varchar(5),
	@iOldAIdLengthOf varchar(2),
	@iOldAidTagLenghth varchar(3),
	@iOldAidTagValue Varchar(30),
	@Content VARCHAR(MAX)=NULL,
	
	@iCount INT

SELECT @iCount= COUNT(*)
FROM tbProfileGPRS WITH (NOLOCK)
WHERE DatabaseID = @iDatabaseID
	AND GPRSName = @sNewGprsName


IF @iCount = 0
BEGIN
	INSERT INTO tbProfileGPRS(DatabaseId, GPRSName,GPRSTag ,GPRSLengthOfTagLength, GPRSTagLength, GPRSTagValue)
	SELECT DatabaseId, @sNewGprsName, GPRSTag, GPRSLengthOfTagLength, GPRSTagLength, GPRSTagValue
	FROM tbProfileGPRS WITH (NOLOCK)
	WHERE GPRSName = @sOldGprsName AND DatabaseId=@iDatabaseID
	 
	 UPDATE tbProfileGPRS
	 SET GPRSTagValue = @sNewGprsName, GPRSTagLength = LEN(@sNewGprsName)
	 WHERE GPRSTag = 'GP001' AND GPRSName = @sNewGprsName
	 END
	ELSE
	BEGIN
	UPDATE tbProfileGPRS
	 SET GPRSTagValue = @sNewGprsName, GPRSTagLength = LEN(@sNewGprsName)
	 WHERE GPRSTag = 'GP01' AND GPRSName = @sNewGprsName
	 END