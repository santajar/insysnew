﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Sept 23, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Clear the Audit Trail
-- =============================================
CREATE PROCEDURE [dbo].[spAuditTrailDelete]
	@sCond VARCHAR(MAX)=NULL
AS
	SET NOCOUNT ON
DECLARE @sStmt VARCHAR(MAX)
SET @sStmt = '
DELETE FROM tbAuditTrail
WHERE LogID IN
(
	SELECT LogID FROM dbo.viewAuditTrail
	' + @sCond + '
)

DELETE FROM tbAuditTrailDetail
WHERE LogID IN
(
	SELECT LogID FROM dbo.viewAuditTrail
	' + @sCond + '
)'

EXEC (@sStmt)
