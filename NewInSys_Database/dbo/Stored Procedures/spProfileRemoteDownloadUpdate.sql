﻿CREATE PROCEDURE [dbo].[spProfileRemoteDownloadUpdate]
	@iDatabaseID INT,
	@sRemoteDownloadName VARCHAR(15),
	@sContent VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileRemoteDownloadDelete @iDatabaseID, @sRemoteDownloadName, 0
	EXEC spProfileRemoteDownloadInsert @iDatabaseID, @sRemoteDownloadName, @sContent
END