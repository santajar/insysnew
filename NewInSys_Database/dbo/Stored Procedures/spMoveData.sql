﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: 31 July 2013
-- Description:	Move Terminal beserta tag dari database yg satu ke database yang lain
-- =============================================
CREATE PROCEDURE [dbo].[spMoveData]
	@sTerminalID varchar (8),
	@iDBSourceID int,
	@iDBDestID int
AS
declare @iCount int,
		@sTag varchar(5),
		@i int,
		@sNameTag Varchar(15)

BEGIN
	update tbProfileTerminalList 
set DatabaseID = @iDBDestID
where TerminalID = @sTerminalID

--cek tag dr db source yg berlebih
set @iCount=0
select @iCount= COUNT(Tag) from tbItemList WITH (NOLOCK) 
where DatabaseID= @iDBSourceID 
	and Tag not in (
		select Tag 
		from tbItemList WITH (NOLOCK) 
		where DatabaseID= @iDBDestID)


if @iCount != 0
begin 
	DECLARE tag_cursor CURSOR FOR  
	select Tag from tbItemList WITH (NOLOCK) 
	Where DatabaseID= @iDBSourceID 
		and Tag not in (
			select Tag 
			from tbItemList WITH (NOLOCK) 
			where DatabaseID= @iDBDestID) order by tag	

	OPEN tag_cursor  
	FETCH NEXT FROM tag_cursor INTO @sTag
	WHILE @@FETCH_STATUS = 0  
	begin
		--Terminal Tag
		if @sTag like 'DE%'
		begin
			delete from tbProfileTerminal 
			where TerminalTag = @sTag 
				and TerminalID in 
					(select TerminalID 
					from tbProfileTerminalList WITH (NOLOCK) 
					where DatabaseID = @iDBDestID) 
					and TerminalID = @sTerminalID
			
		end
		--Acquirer Tag
		if @sTag like 'AA%'
		begin
			delete from tbProfileAcquirer 
			where AcquirerTag = @sTag 
				and TerminalID in 
				(select TerminalID 
				from tbProfileTerminalList 
				where DatabaseID = @iDBDestID) 
				and TerminalID = @sTerminalID
	
		end
		--Issuer Tag
		if @sTag like 'AE%'
		begin
			delete from tbProfileIssuer 
			where IssuerTag = @sTag 
			and TerminalID in 
				(select TerminalID 
				from tbProfileTerminalList WITH (NOLOCK) 
				where DatabaseID = @iDBDestID) 
				and TerminalID = @sTerminalID
		end
	
		FETCH NEXT FROM tag_cursor INTO @sTag
	end
	
	close tag_cursor
	Deallocate tag_cursor
end

--cek tag dr db 
select @iCount= COUNT(Tag) 
from tbItemList WITH (NOLOCK)
where DatabaseID=@iDBDestID 
and Tag not in 
	(select Tag 
	from tbItemList WITH (NOLOCK) 
	where DatabaseID=@iDBSourceID )


if @iCount != 0
begin 

	DECLARE tag_cursor CURSOR FOR  
	select Tag 
	from tbItemList WITH (NOLOCK) 
	where DatabaseID= @iDBDestID 
	and Tag not in (
		select Tag 
		from tbItemList WITH (NOLOCK)
		where DatabaseID= @iDBSourceID ) order by tag	

	OPEN tag_cursor  
	FETCH NEXT FROM tag_cursor INTO @sTag
	WHILE @@FETCH_STATUS = 0  
	begin
		
		declare @DefaultValue varchar(max), @lenghtOfValue int
		--Terminal Tag
		if @sTag like 'DE%'
		begin
			
			select @DefaultValue=DefaultValue 
			from tbItemList WITH (NOLOCK) 
			where DatabaseID =@iDBDestID and Tag = @sTag
			set @lenghtOfValue = LEN(@DefaultValue)
			
			insert into tbProfileTerminal (TerminalID,TerminalTag, TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue,Description)
			values(@sTerminalID,@sTag,LEN(@lenghtOfValue),@lenghtOfValue,@DefaultValue,'')		
		
		end
		--Acquirer Tag
		if @sTag like 'AA%'
		begin
			if OBJECT_ID('tempdb..#TempNameAcquirerTag') is not null
				drop table #TempNameAcquirerTag
			create table #TempNameAcquirerTag
			(
				Row int identity(1,1),
				NameTag varchar(15)
			)
			
			select @DefaultValue=DefaultValue 
			from tbItemList WITH (NOLOCK)
			where DatabaseID =@iDBDestID 
				and Tag = @sTag

			set @lenghtOfValue = LEN(@DefaultValue)
			
			insert into #TempNameAcquirerTag(NameTag)
			select distinct AcquirerName 
			from tbProfileAcquirer WITH (NOLOCK) 
			where TerminalID = @sTerminalID
			
			select @iCount=COUNT(distinct AcquirerName) 
			from tbProfileAcquirer WITH (NOLOCK) 
			where TerminalID = @sTerminalID
			
			set @i = 1
			while @i<=@iCount
			begin
				select @sNameTag = NameTag 
				from #TempNameAcquirerTag 
				where Row= @i

				insert into tbProfileAcquirer (TerminalID,AcquirerName,AcquirerTag, AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue,Description)
				values(@sTerminalID,@sNameTag,@sTag,LEN(@lenghtOfValue),@lenghtOfValue,@DefaultValue,'')		
				set @i= @i+1
			end
		end
		--Issuer Tag
		if @sTag like 'AE%'
		begin
			if OBJECT_ID('tempdb..#TempNameIssuerTag') is not null
				drop table #TempNameIssuerTag
			create table #TempNameIssuerTag
			(
				Row int identity(1,1),
				NameTag varchar(15)
			)
			
			select @DefaultValue=DefaultValue 
			from tbItemList WITH (NOLOCK) 
			where DatabaseID =@iDBDestID and Tag = @sTag
			
			set @lenghtOfValue = LEN(@DefaultValue)
			
			insert into #TempNameIssuerTag(NameTag)
			select distinct IssuerName 
			from tbProfileIssuer WITH (NOLOCK) 
			where TerminalID = @sTerminalID
			
			select @iCount=COUNT(distinct IssuerName) 
			from tbProfileIssuer WITH (NOLOCK) 
			where TerminalID = @sTerminalID

			set @i = 1
			while @i<=@iCount
			begin
				select @sNameTag = NameTag 
				from #TempNameIssuerTag 
				where Row= @i
				
				insert into tbProfileIssuer(TerminalID,IssuerName,IssuerTag, IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue,Description)
				values(@sTerminalID,@sNameTag,@sTag,LEN(@lenghtOfValue),@lenghtOfValue,@DefaultValue,'')	
				set @i= @i+1	
			end
			
		end
	
		FETCH NEXT FROM tag_cursor INTO @sTag
	end
	
	close tag_cursor
	Deallocate tag_cursor
end

 
END