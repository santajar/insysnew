﻿create PROCEDURE [dbo].[spGetTidBySerialNumber]
	@sSN VARCHAR(30),
	@sTerminalID NVARCHAR(8) OUTPUT
AS
	SET NOCOUNT ON
SELECT @sTerminalID=TerminalID
FROM TbProfileSerialNumberList WITH(NOLOCK)
WHERE SerialNumber = @sSN