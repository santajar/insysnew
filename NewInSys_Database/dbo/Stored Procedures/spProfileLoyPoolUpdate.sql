﻿

CREATE PROCEDURE [dbo].[spProfileLoyPoolUpdate]
	@iDatabaseID INT,
	@sLoyPoolName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS
BEGIN
	EXEC spProfileLoyPoolDelete @iDatabaseID, @sLoyPoolName, 0
	EXEC spProfileLoyPoolInsert @iDatabaseID, @sLoyPoolName, @sContent
END



