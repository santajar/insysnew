﻿-- =============================================
-- Author:		SUPRIYADI SETYO
-- Create date: Agt 19, 2013
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Viewing the Software Initialization Trail
-- =============================================
CREATE PROCEDURE [dbo].[spAuditInitSoftwareBrowse]
	@sKey VARCHAR(MAX) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		TerminalId,
		Software,
		SoftwareDownload [Software Download],
		SerialNumber,
		InitTime,
		IndexSoftware
	FROM tbAuditInitSoftware WITH (NOLOCK)
	WHERE
		TerminalId LIKE '%' + @sKey + '%' OR
		Software LIKE '%' + @sKey + '%' OR
		SerialNumber LIKE '%' + @sKey + '%' OR
		InitTime LIKE '%' + @sKey + '%' OR
		SoftwareDownload like '%' + @sKey + '%'
	ORDER BY InitID DESC
END