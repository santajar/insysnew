﻿
-- ============================================================
-- Description:	Update CompressedContentProfile for current TID
-- ============================================================

CREATE PROCEDURE [dbo].[spProfileGZipUpdate] 
	@sTerminalID VARCHAR(8),
	@sPath VARCHAR(100)
AS
DECLARE @sql NVARCHAR(500)
SET @sql = N'UPDATE tbInitCompress
SET CompressedContent = (SELECT * 
               FROM OPENROWSET(BULK ''' + @sPath + '\' + @sTerminalID + '.gz'', 
                               SINGLE_BLOB) AS a) WHERE TerminalID=''' + @sTerminalID + ''''
PRINT @sql
EXEC sp_executesql @sql



