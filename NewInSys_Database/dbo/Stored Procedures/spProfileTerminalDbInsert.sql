﻿
CREATE PROCEDURE [dbo].[spProfileTerminalDbInsert]
	@sDbID SMALLINT,
	@sDatabaseName VARCHAR(15),
	@sNewInit BIT,
	@sEMVInit BIT,
	@sTLEInit BIT,
	@sLoyaltyProd BIT =0,
	@sGPRSInit BIT=0,
	@sBankCode BIT=0,
	@sProductCode BIT=0,
	@sRemoteDownload BIT=0,
	@sCustomMenu BIT=0
AS
	INSERT INTO tbProfileTerminalDb (DatabaseID, 
									 DatabaseName,
			 						 NewInit,
									 EMVInit,
									 TLEInit,
									 LoyaltyProd,
									 GPRSInit,
									 BankCode,
									 ProductCode,
									 RemoteDownload,
									 Custom_Menu)
	VALUES (@sDbID,
			@sDatabaseName,
			@sNewInit,
			@sEMVInit,
			@sTLEInit,
			@sLoyaltyProd,
			@sGPRSInit,
			@sBankCode,
			@sProductCode,
			@sRemoteDownload,
			@sCustomMenu)





