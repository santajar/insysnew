﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 3, 2010
-- Modify date: 
--				1. August 12, 2010, adding column TerminalTagLength
--				2. September 8, 2010, Var @sTerminalID diubah jadi @sCondition sebagai pengganti statement 'WHERE'
-- Description:	
--				1. view The Terminal table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalBrowse]
	@sCondition VARCHAR(MAX)=NULL,
	@sTerminalID VARCHAR(8)
AS
BEGIN
	DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery = '
	SELECT TerminalID, 
		TerminalTag AS Tag, 
		TerminalTagLength, 
		TerminalTagValue AS Value 
	FROM tbProfileTerminal WITH (NOLOCK) '
	EXEC (@sQuery + @sCondition+ ' ORDER BY TerminalID, TerminalTag')
	EXEC spProfileTerminalUpdateLastView @sTerminalID
END

