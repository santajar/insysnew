﻿
-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <20 Okt 2015>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSoftwareGroupPackageBrowse]
	@sCondition VARCHAR(100)= NULL
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sQuery VARCHAR(2000)
    SET @sQuery = 'SELECT [ID]
				  ,[GroupName]
				  ,[SoftwareName]
				  ,[ScheduleType]
				  ,[ScheduleTime]
				FROM tbSoftwareGroupPackage'
	
	EXEC (@sQuery + @sCondition)
END