﻿CREATE procedure [dbo].[spChartGroup]
as
	SET NOCOUNT ON
truncate Table tbGroupChart
if OBJECT_ID('tempdb..#GroupList') is not null
	drop table #GroupList

create table #GroupList
(
	Name varchar(25)
)

truncate table tbGroupChart

insert into #GroupList (Name)
select distinct GroupRDName from tbGroupRemoteDownload WITH (NOLOCK)
where GroupRDName != ''

declare @iCount int, @sName varchar(25)
declare ChartCursor cursor FOR
select Name from #GroupList

open ChartCursor

fetch next from ChartCursor into @sName
set @iCount=0
while @@FETCH_STATUS=0
begin
	select @iCount=Count(GroupRDName) 
	from tbGroupRemoteDownload A WITH (NOLOCK)
	join tbListTIDRemoteDownload B WITH (NOLOCK)
	on A.IdGroupRD=B.IdGroupRD
	 where A.GroupRDName= @sName

	insert into tbGroupChart (GroupName,GroupCount)
	Values ( @sName,@iCount)

	fetch next from ChartCursor into @sName
end

close ChartCursor
Deallocate ChartCursor
select * from tbGroupChart WITH (NOLOCK)