﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. parse the received ISO message to get the information
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseISOUnpack]
	@sSessionTime VARCHAR(12),
	@sBinBitMap VARCHAR(64), 
	@sEdcMessage VARCHAR(MAX)
AS
BEGIN
	DECLARE @sSqlStmt VARCHAR(MAX)
	SET @sSqlStmt='
	IF (OBJECT_ID(''TEMPDB..##tempBitMap'+ @sSessionTime +''') IS NOT NULL)
		DROP TABLE ##tempBitMap'+ @sSessionTime +'
	
	CREATE TABLE ##tempBitMap'+ @sSessionTime +'(
		BitId SMALLINT,
		BitValue VARCHAR(MAX)
	)'
	EXEC (@sSqlStmt)
	
	DECLARE @sBitValue VARCHAR(MAX)
	DECLARE @iOffset INT
	DECLARE @iTemp INT

	SET @iOffset=1
	DECLARE @iIndex INT
	SET @iIndex=1
	WHILE @iIndex<=64
	BEGIN
		IF SUBSTRING(@sBinBitMap,@iIndex,1)=1
		BEGIN
			IF @iIndex = 3 
				BEGIN
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,3,6,@iOffset = @iOffset OUTPUT
				END
			ELSE IF @iIndex = 11
				BEGIN
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,11,6,
						@iOffset = @iOffset OUTPUT
				END
			ELSE IF @iIndex = 12
				BEGIN
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,12,6,
						@iOffset = @iOffset OUTPUT
				END
			ELSE IF @iIndex = 13
				BEGIN
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,13,6,
						@iOffset = @iOffset OUTPUT
				END
			ELSE IF @iIndex = 24
				BEGIN
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,24,4,
						@iOffset = @iOffset OUTPUT
				END
			ELSE IF @iIndex = 39
				BEGIN
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,39,4,
						@iOffset = @iOffset OUTPUT
				END
			ELSE IF @iIndex = 41
				BEGIN
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,41,16,
						@iOffset = @iOffset OUTPUT
				END
			ELSE IF @iIndex = 48
				BEGIN
					--SET @iTemp = (CAST(dbo.HexStrToVarBin(SUBSTRING(@sEdcMessage,@iOffset,4)) AS INT) * 2) + 4
					SET @iTemp = (CAST(SUBSTRING(@sEdcMessage,@iOffset,4) AS INT) * 2) + 4
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,48,@iTemp,
						@iOffset = @iOffset OUTPUT
				END
			ELSE IF @iIndex = 57
				BEGIN
					--SET @iTemp = (CAST(dbo.HexStrToVarBin(SUBSTRING(@sEdcMessage,@iOffset,4)) AS INT) * 2) + 4
					SET @iTemp = (CAST(SUBSTRING(@sEdcMessage,@iOffset,4) AS INT) * 2) + 4
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,57,@iTemp,
						@iOffset = @iOffset OUTPUT
				END
--			ELSE IF @iIndex = 58
--				BEGIN
--					SET @iTemp = (CAST(SUBSTRING(@sEdcMessage, @iOffset, 4) AS INT)) + 4
--					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,58,@iTemp,
--						@iOffset = @iOffset OUTPUT				
--				END
			ELSE IF @iIndex = 60
				BEGIN
					EXEC spOtherParseSaveTempResponse @sSessionTime,@sEdcMessage,60,20,
						@iOffset = @iOffset OUTPUT
				END
		END

		SET @iIndex = @iIndex + 1
	END
END