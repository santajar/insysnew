﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Aug 19, 2010
-- Modify	  :
--				1. 
-- Description:	Delete The Acquirer table content of Terminal
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerDeleteByTID]
	@sTerminalID VARCHAR(8)
AS
	DELETE FROM tbProfileAcquirer 
		WHERE TerminalID=@sTerminalID
