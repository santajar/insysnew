﻿
-- =======================================================================
-- Description: Insert all CardList from Existing Database to New Database
--				Insert all Profile Card from CardList into new Database
-- =======================================================================

CREATE PROCEDURE [dbo].[spProfileCardListInsertByDbId]
	@sNewDatabaseID SMALLINT,
	@sExistingDatabaseID SMALLINT
AS
	-- Insert CardList
	INSERT INTO tbProfileCardList (DatabaseID, CardName)
	SELECT @sNewDatabaseID, CardName
	FROM tbProfileCardList WITH (NOLOCK)
	WHERE DatabaseID = @sExistingDatabaseID

	-- Insert Card's Profile
	EXEC spCardProfileInsert @sNewDatabaseID, @sExistingDatabaseID