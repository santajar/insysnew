﻿CREATE Procedure [dbo].[spCheckRemoteDownloadContentValid]
(
	@sTerminalID NVARCHAR(8),
	@sAppDateTime NVARCHAR(20),
	@sAppNameDownload NVARCHAR(20)
)
AS
BEGIN
	DECLARE @dtUploadTimeOut DATETIME,
		@sUploadTime NVARCHAR(20),
		@sUploadTimeDDMMYYHHMM NVARCHAR(20),
		@sBit BIT,
		@sAppNameDownloadDBOut NVARCHAR(20),
		@sLinkDatabase Nvarchar(50),
		@sQuery Nvarchar(max),
		@iAppPackID BIGINT,
		@sQuery2 Nvarchar(max)

CREATE TABLE #TempAppPackID
(
	AppPackID BIGINT		
)

CREATE TABLE #TempValidTime
(
	AppPackageName VARCHAR(150),
	UploadTime DATETIME
)

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName = 'LinkedRemoteDownloadServer'

	SET @sQuery = N'SELECT AppPackID
					FROM '+@sLinkDatabase+'tbListTIDRemoteDownload WITH (NOLOCK)
					WHERE TerminalID = '''+@sTerminalID+''''

	INSERT INTO #TempAppPackID (AppPackID)
	EXEC (@sQuery)
	
	SELECT @iAppPackID = AppPackID FROM #TempAppPackID
	DROP TABLE #TempAppPackID

	SET @sQuery2 = N'SELECT AppPackageName,UploadTime
					FROM '+@sLinkDatabase+'tbAppPackageEDCList WITH (NOLOCK)
					WHERE AppPackID ='''+ CONVERT(NVARCHAR,@iAppPackID)+''''
	INSERT INTO #TempValidTime(AppPackageName,UploadTime)
	EXEC (@sQuery2)

	SELECT @sAppNameDownloadDBOut=AppPackageName,@dtUploadTimeOut=UploadTime 
	FROM #TempValidTime
	
	DROP TABLE #TempValidTime

--	SET @sQuery = N'SELECT @sAppNameDownloadDB=AppPackageName, @dtUploadTime = UploadTime
--	FROM '+@sLinkDatabase+'tbListTIDRemoteDownload A
--	JOIN '+@sLinkDatabase+'tbAppPackageEDCList B
--	ON A.AppPackID= B.AppPackId
--	WHERE TerminalID = @sParmTerminalID';

--SET @ParmDefinition = N'@sParmTerminalID VARCHAR(8), @sAppNameDownloadDB VARCHAR(20) OUTPUT, @dtUploadTime DATETIME OUTPUT';

--EXECUTE sp_executesql @sQuery,@ParmDefinition,@sParmTerminalID = @sTerminalID,@sAppNameDownloadDB = @sAppNameDownloadDBOut OUTPUT,  @dtUploadTime = @dtUploadTimeOut OUTPUT;

	SELECT @sUploadTime=CONVERT(VARCHAR(8), @dtUploadTimeOut, 112) + LEFT(REPLACE(CONVERT(varchar, @dtUploadTimeOut, 108), ':',''),4)
	SET @sUploadTimeDDMMYYHHMM = SUBSTRING(@sUploadTime,7,2) + SUBSTRING(@sUploadTime,5,2) + SUBSTRING(@sUploadTime,3,2) + SUBSTRING(@sUploadTime,9,2) + SUBSTRING(@sUploadTime,11,2)

	IF @sAppDateTime = @sUploadTimeDDMMYYHHMM AND @sAppNameDownload = @sAppNameDownloadDBOut
		SET @sBit= 1
	ELSE
		SET @sBit= 0

	SELECT @sBit
END