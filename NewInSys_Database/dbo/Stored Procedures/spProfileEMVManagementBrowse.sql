﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <19 Maret 2015>
-- Description:	<Browse EMV Management>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileEMVManagementBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery='
	SELECT DatabaseId,
		EMVManagementName,
		EMVManagementTag as Tag,
		EMVManagementTagLength,
		EMVManagementTagValue as Value
	FROM tbProfileEMVManagement WITH (NOLOCK) '
	SET @sQuery = @sQuery + ISNULL(@sCondition,'') + ' ORDER BY EMVManagementName, EMVManagementTag'
EXEC (@sQuery)