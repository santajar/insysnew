﻿




-- ======================================================
-- Description:	Delete Version based on given DatabaseID
-- ======================================================

CREATE PROCEDURE [dbo].[spDatabaseVersionDeleteByDbID]
	@sDatabaseID SMALLINT
AS
	--Data Deleted : AID, CAPK, CardList, TLE, ItemList, ItemComboBoxValue
	EXEC spProfileAIDDeleteByDbID @sDatabaseID
	EXEC spProfileCAPKDeleteByDbID @sDatabaseID
	EXEC spProfileCardListDeleteByDbID @sDatabaseID
	EXEC spProfileTLEDeleteByDBID @sDatabaseID
	EXEC spProfileSoftwareDeleteByDbID @sDatabaseID
	EXEC spItemListDeleteByDbID @sDatabaseID
--	EXEC spItemComboBoxValueDeleteByDbID @sDatabaseID






