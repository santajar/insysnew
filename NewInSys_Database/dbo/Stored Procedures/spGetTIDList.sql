﻿CREATE PROCEDURE [dbo].[spGetTIDList] 
@sFilterValue varchar(50), 
@XMLTag varchar(5),
@XMLFilter VARCHAR(50) = '',
@isComplicated bit
as
if @iscomplicated = 0
	begin
		declare @stbname varchar(10)
		select @stbname = case substring(@XMLTag,1,2)
						when 'aa' then 'acquirer'
						when 'ae' then 'issuer'
						else 'terminal'
					  end

		declare @sStmt varchar(max)
		set  @sStmt = 'select distinct terminalid from tbProfile' + @stbname +  
					' WITH (NOLOCK) where ' + @stbname + 'Tag = ''' + @xmltag + 
					''' and ' + @stbname + 'tagvalue = ''' + @sfiltervalue+''''

		IF @XMLFilter <> ''
			if @stbname <> 'terminal'
				set @sStmt = @sStmt + ' and '+@stbname+'name = ''' + @XMLFilter + ''''
	
		exec (@sStmt)
	end