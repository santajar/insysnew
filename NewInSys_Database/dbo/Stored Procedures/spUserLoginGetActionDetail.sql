﻿CREATE PROCEDURE [dbo].[spUserLoginGetActionDetail]
	@sUserTag VARCHAR(5),
	@sUserRight VARCHAR(5),
	@sUserAction VARCHAR(100) OUTPUT
AS
DECLARE @sTagName VARCHAR(20)
DECLARE @sTagLen INT

SELECT @sTagName = TagName,
		@sTagLen = TagLen
FROM tbUserLoginMap
WHERE Tag = @sUserTag

SET @sUserAction = 'View'

DECLARE @iIndex INT
SET @iIndex = 1

WHILE (@iIndex <= @sTagLen)
BEGIN
	IF (SUBSTRING(@sUserRight, @iIndex,1) = '1')
	BEGIN
		DECLARE @sStmt NVARCHAR(200)
		DECLARE @sAction VARCHAR(20)
		SET @sStmt = N'SELECT @sAction = Index'+ CONVERT(VARCHAR,@iIndex) + ' FROM tbUserLoginMap WITH (NOLOCK)
						WHERE Tag = ''' + @sUserTag + ''''
		EXEC sp_executesql @sStmt, N'@sAction VARCHAR(20) OUTPUT', @sAction = @sAction OUTPUT
		SET @sUserAction = @sUserAction + ', ' + @sAction
	END

	SET @iIndex = @iIndex + 1
END
