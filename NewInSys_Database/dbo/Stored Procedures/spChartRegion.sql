﻿CREATE procedure [dbo].[spChartRegion]
as
	SET NOCOUNT ON
if OBJECT_ID('tempdb..#regionList') is not null
	drop table #regionList

create table #regionList
(
	Name varchar(25)
)

truncate table tbRegionChart

insert into #regionList (Name)
select distinct RegionRDName from tbRegionRemoteDownload WITH (NOLOCK)
where RegionRDName != ''

declare @iCount int, @sName varchar(25)
declare ChartCursor cursor FOR
select Name from #regionList

open ChartCursor

fetch next from ChartCursor into @sName
set @iCount=0
while @@FETCH_STATUS=0
begin
	
	select @iCount=Count(RegionRDName) 
	from tbRegionRemoteDownload A WITH (NOLOCK)
	join tbListTIDRemoteDownload B WITH (NOLOCK)
	on A.IdRegionRD=B.IdRegionRD
	where A.RegionRDName= @sName

	insert into tbRegionChart (RegionName,RegionCount)
	Values ( @sName,@iCount)

	fetch next from ChartCursor into @sName
end

close ChartCursor
Deallocate ChartCursor
SELECT * FROM tbRegionChart