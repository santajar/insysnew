﻿
-- ======================================================
-- Description:	Get FlagValue for InitTimeOut and MaxConn
-- ======================================================

CREATE PROCEDURE [dbo].[spControlFlagAutoInitBrowse] 
	@sInitTimeOut VARCHAR(50),
	@sMaxConn VARCHAR(50)
AS
	SET NOCOUNT ON
	DECLARE @sInitTOValue VARCHAR(10)
	DECLARE @sMaxConnValue VARCHAR(10)

	EXEC spControlFlagBrowse @sInitTimeOut, @sInitTOValue OUTPUT
	EXEC spControlFlagBrowse @sMaxConn, @sMaxConnValue OUTPUT

	SELECT @sInitTOValue AS 'InitTimeOut', @sMaxConnValue AS 'MaxConn'



