﻿

-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Description:	delete The History Issuer table content of Terminal
-- =============================================
Create PROCEDURE [dbo].[spProfileIssuerHistoryDeleteAll]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM tbProfileIssuerHistory 
	WHERE TerminalID = @sTerminalID
END