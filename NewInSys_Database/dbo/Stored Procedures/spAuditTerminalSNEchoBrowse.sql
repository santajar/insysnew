﻿
-- Author		: Tobias SETYO
-- Created Date : Nov 29, 2013
-- Modify Date	:
--					1. <MMM dd, yyyy>, <modified by>
-- Description	: browse echo log
CREATE PROCEDURE [dbo].[spAuditTerminalSNEchoBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
	SET NOCOUNT ON
DECLARE @sQuery VARCHAR(MAX)
SET @sQuery=' SELECT Echo.Id,
					 Echo.TerminalSN,
					 Echo.TerminalID,
					 Echo.AppName [Application Name],
					 Com.CommsTypeName [Type],
					 Echo.DateCreated [Created Date]
			  FROM tbAuditTerminalSNEcho Echo WITH (NOLOCK) 
			  LEFT JOIN tbMSCommsType Com WITH (NOLOCK)
			  ON Echo.CommsID=Com.CommsID '

IF ISNULL(@sCondition,'')=''
	EXEC (@sQuery + ' Order by Id Desc')
ELSE
	EXEC (@sQuery + @sCondition + ' Order by Id Desc')