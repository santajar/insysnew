﻿-- =============================================
-- Author:		Shelvy Sutrisno
-- Create date: Sept 21, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Insert new Card relation into profile content
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationInsert]
	@sTerminalID VARCHAR(8),
	@sAcquirerValue VARCHAR(50),
	@sIssuerValue VARCHAR(50),
	@sCardValue VARCHAR(50)
AS
EXEC spProfileRelationHistoryInsert @sTerminalID
EXEC spProfileIssuerHistoryInsert @sTerminalID,@sIssuerValue

IF (DBO.isTagLength4(DBO.iDatabaseIdByTerminalId(@sTerminalID))=5)
BEGIN
	INSERT INTO tbProfileRelation (TerminalID, 
		RelationTag, 
		RelationLengthOfTagLength, 
		RelationTagLength, 
		RelationTagValue)
	SELECT @sTerminalID, 
		'AD001',
		LEN(CONVERT(VARCHAR,LEN(ISNULL(@sCardValue,'')))),
		LEN(ISNULL(@sCardValue,'')), 
		@sCardValue
	UNION ALL
	SELECT @sTerminalID, 
		'AD002',
		LEN(CONVERT(VARCHAR,LEN(ISNULL(@sIssuerValue,'')))),
		LEN(ISNULL(@sIssuerValue,'')), 
		@sIssuerValue
	UNION ALL
	SELECT @sTerminalID, 
		'AD003',
		LEN(CONVERT(VARCHAR,LEN(ISNULL(@sAcquirerValue,'')))),
		LEN(ISNULL(@sAcquirerValue,'')), 
		@sAcquirerValue	
END
ELSE
BEGIN
	INSERT INTO tbProfileRelation (TerminalID, 
		RelationTag, 
		RelationLengthOfTagLength, 
		RelationTagLength, 
		RelationTagValue)
	SELECT @sTerminalID, 
		'AD01',
		LEN(CONVERT(VARCHAR,LEN(ISNULL(@sCardValue,'')))),
		LEN(ISNULL(@sCardValue,'')), 
		@sCardValue
	UNION ALL
	SELECT @sTerminalID, 
		'AD02',
		LEN(CONVERT(VARCHAR,LEN(ISNULL(@sIssuerValue,'')))),
		LEN(ISNULL(@sIssuerValue,'')), 
		@sIssuerValue
	UNION ALL
	SELECT @sTerminalID, 
		'AD03',
		LEN(CONVERT(VARCHAR,LEN(ISNULL(@sAcquirerValue,'')))),
		LEN(ISNULL(@sAcquirerValue,'')), 
		@sAcquirerValue
END
