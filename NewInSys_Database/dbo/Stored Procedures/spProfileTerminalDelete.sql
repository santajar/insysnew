﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Agt 24, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Delete terminal table content
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalDelete]
	@sTerminalID VARCHAR(8)
AS
	DELETE FROM tbProfileTerminal 
		WHERE TerminalID=@sTerminalID
