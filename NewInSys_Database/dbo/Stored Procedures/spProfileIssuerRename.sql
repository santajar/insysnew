﻿CREATE PROCEDURE [dbo].[spProfileIssuerRename]
	@sTerminalID VARCHAR(8),
	@sOldIss VARCHAR(15),
	@sNewIss VARCHAR(15)
AS
BEGIN
	UPDATE TbProfileIssuer
	SET IssuerName = @sNewIss
	WHERE TerminalID = @sTerminalID AND
		IssuerName = @sOldIss
IF(dbo.isTagLength4(dbo.iDatabaseIdByTerminalId(@sTerminalID))=5)
BEGIN
	UPDATE TbProfileIssuer
	SET IssuerTagValue = @sNewIss,
		IssuerTagLength = LEN(@sNewIss),
		IssuerLengthOfTagLength = LEN(LEN(@sNewIss))
	WHERE TerminalID = @sTerminalID AND
		IssuerName = @sNewIss AND
		IssuerTag = 'AE001'
END
ELSE
BEGIN
	UPDATE TbProfileIssuer
	SET IssuerTagValue = @sNewIss,
		IssuerTagLength = LEN(@sNewIss),
		IssuerLengthOfTagLength = LEN(LEN(@sNewIss))
	WHERE TerminalID = @sTerminalID AND
		IssuerName = @sNewIss AND
		IssuerTag = 'AE01'
END
END
