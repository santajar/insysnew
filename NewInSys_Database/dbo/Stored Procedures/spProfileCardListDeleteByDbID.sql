﻿

-- ======================================================
-- Description:	Delete all Card List in given DatabaseID
-- ======================================================

CREATE PROCEDURE [dbo].[spProfileCardListDeleteByDbID]
	@sDatabaseID SMALLINT
AS
	EXEC spProfileCardDelete @sDatabaseID

	DELETE FROM tbProfileCardList
	WHERE DatabaseID = @sDatabaseID


