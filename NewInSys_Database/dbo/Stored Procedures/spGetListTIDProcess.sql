﻿CREATE PROCEDURE [dbo].[spGetListTIDProcess] 
@sTableName VARCHAR(255),
@sVersion VARCHAR(5)
as
DECLARE @sQuery nVARCHAR(MAX)

CREATE TABLE #TempFilter (FilterName varchar(50),
FilterValue varchar(50),
XMLTag VARCHAR(5),
XMLFilter VARCHAR(50),
IsComplicated BIT
)
declare @index int, @total int

SET @sQuery = 'insert into #TempFilter (FilterName, FilterValue, XMLTag, XMLFilter, IsComplicated)
				SELECT sFilterFieldName, sValues, EMSXMLTag, EMSXMLFilter, EMSXMLIsComplicated
				FROM tbTempxml' + @sTableName + ' Temp WITH (NOLOCK)
					LEFT JOIN tbEmsMsXMLMap Map WITH (NOLOCK)
					ON Temp.sFilterFieldName = Map.EMSXMLColumn 
				WHERE Map.EMSXMLVersion = ' + @sVersion


EXEC (@sQuery)

--select * from ##TempFilter

declare temp cursor for 
	select FilterValue, XMLTag, XMLFilter, IsComplicated
	from #TempFilter
	
declare @sFilterValue VARCHAR(50),
@sXMLTag varchar(5),
@sXMLFilter VARCHAR(50),
@isXMLComplicated BIT

create table #TempTIDList (TerminalID VARCHAR(8))

open temp
fetch next from temp into @sfiltervalue, @sxmltag, @sxmlfilter, @isxmlcomplicated
while @@fetch_status = 0
begin
	insert into #TempTIDList (TerminalID)
	exec spGetTIDList @sfiltervalue, @sxmltag, @sxmlfilter, @isxmlcomplicated

	fetch next from temp into @sfiltervalue, @sxmltag, @sxmlfilter, @isxmlcomplicated
end
close temp
deallocate temp

--insert into #TIDList (TerminalID)
--select distinct TerminalId
--From #TempTIDList




IF OBJECT_ID ('Tempdb..#TempFilter') IS NOT NULL
	DROP TABLE #TempFilter

IF OBJECT_ID ('Tempdb..#TempIDList') IS NOT NULL
	DROP TABLE #TempTIDList