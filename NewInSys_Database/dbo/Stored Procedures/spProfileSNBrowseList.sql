﻿CREATE PROCEDURE [dbo].[spProfileSNBrowseList]
	@sDatabaseID VARCHAR(3)
AS
SELECT SN_Name [Name]
FROM tbProfileSN
WHERE DatabaseID=@sDatabaseID AND SN_Tag LIKE '%001'
