﻿CREATE PROCEDURE [dbo].[spScheduleSoftwareSetDaily]
	@sScheduleType VARCHAR(10),
	@dtScheduleTime DATETIME,
	@sTYPE VARCHAR(2) OUTPUT,
	@sCek VARCHAR(2) OUTPUT,
	@sTime VARCHAR(4) OUTPUT
AS
DECLARE @sHour VARCHAR(2),
		@sMinute VARCHAR(2)
		
SET @sType = 'EV'
SET @sCek = '00'
IF (@dtScheduleTime is not null)
BEGIN
	SET @sHour = RIGHT('0'+CONVERT(VARCHAR, DATEPART(HOUR,@dtScheduleTime)),2)
	SET @sMinute = RIGHT('0'+CONVERT(VARCHAR, DATEPART(MINUTE,@dtScheduleTime)),2)
	SET @sTime = @sHour+@sMinute
END
ELSE
BEGIN
	SET @sTime = '0000'
END