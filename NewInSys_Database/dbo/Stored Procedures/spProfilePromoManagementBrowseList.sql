﻿-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: Feb 10, 2016
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the list of Promo Management
-- =============================================
CREATE PROCEDURE [dbo].[spProfilePromoManagementBrowseList]
	@sDatabaseId VARCHAR(3)
AS
SELECT DISTINCT
	PromoManagementName as Name
FROM tbProfilePromoManagement WITH (NOLOCK)
WHERE DatabaseID=@sDatabaseId