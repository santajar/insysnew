﻿


CREATE PROCEDURE [dbo].[spProfileLoyProdBrowse]
	@sCondition VARCHAR(100) = NULL
AS
	DECLARE @sStmt VARCHAR(MAX)
	SET @sStmt = 'SELECT LoyProdTagID,
						LoyProdName, 
						DatabaseID,
						LoyProdTag as Tag,
						LoyProdLengthOfTagLength,
						LoyProdTagLength,
						LoyProdTagValue as Value,
						[Description]
				FROM tbProfileLoyProd WITH (NOLOCK) '
	SET @sStmt = @sStmt + ISNULL(@sCondition,'') + ' ORDER BY LoyProdName, LoyProdTag'
	EXEC (@sStmt)


