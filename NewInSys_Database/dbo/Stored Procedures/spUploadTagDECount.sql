﻿
-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 7, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Return rows number on tbUploadTagDE which is not null
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagDECount]
@sCond VARCHAR(MAX)=NULL
AS
BEGIN
DECLARE @sQuery VARCHAR(MAX)
	SET NOCOUNT ON;
	SET @sQuery = 'SELECT COUNT(*) FROM tbUploadTagDE WITH (NOLOCK) '
	IF @sCond IS NOT NULL
	BEGIN
		SET @sQuery = @sQuery + @sCond
		EXEC (@sQuery)
	END
	ELSE
		EXEC (@sQuery)	
END