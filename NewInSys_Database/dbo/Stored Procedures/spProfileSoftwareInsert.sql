﻿
CREATE PROCEDURE [dbo].[spProfileSoftwareInsert]
	@sSoftwareName VARCHAR(25),
	@iDatabaseID INT
AS
	INSERT INTO tbProfileSoftware (DatabaseID, SoftwareName)
	VALUES (@iDatabaseID, @sSoftwareName)


