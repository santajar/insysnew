﻿
CREATE PROCEDURE [dbo].[spUserAccessBrowse]
	@sTerminalID VARCHAR(8),
	@sUserId VARCHAR(10) = NULL OUTPUT,
	@sOutput CHAR(1) = 0 OUTPUT
AS
SELECT 
	@sOutput = CASE WHEN COUNT(UserID) > 0 THEN 1 END
FROM tbUserAccess
WHERE
	TerminalId = @sTerminalID
IF @sOutput > 0
	SELECT 
		@sUserId = UserId
	FROM tbUserAccess
	WHERE
		TerminalId = @sTerminalID