﻿-- =============================================
-- Author:		Kiky Ariady
-- Create date: Oct 6, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Inserting Tag ID, and Column name to table tbUploadTagAA
--				2. Run on process create Data Upload Template
-- =============================================
CREATE PROCEDURE [dbo].[spUploadTagAAInsert]
	@sAcquirerName VARCHAR(50),
	@sTag VARCHAR(5),
	@sColumnName VARCHAR(50),
	@bMandatory BIT,
	@iSourceColumn SMALLINT = NULL,
	@sTemplate varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO tbUploadTagAA
	(
		AcquirerName,
		Tag,
		ColumnName,
		Mandatory,
		SourceColumn,
		Template
	)
	VALUES
	(
		@sAcquirerName,
		@sTag,
		@sColumnName,
		@bMandatory,
		@iSourceColumn,
		@sTemplate
	)
END