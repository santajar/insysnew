
IF OBJECT_ID ('dbo.spInsertAcqSequenceTemp') IS NOT NULL
	DROP PROCEDURE dbo.spInsertAcqSequenceTemp
GO

CREATE PROCEDURE [dbo].[spInsertAcqSequenceTemp]
@sTerminalID VARCHAR(8)
AS 

SET NOCOUNT ON

--DECLARE @sTerminalID VARCHAR(8) SET @sTerminalID = 'TMTATANK'

DELETE tbEdcMonitorTempAcqSeq
WHERE TerminalID = @sTerminalID

INSERT INTO tbEdcMonitorTempAcqSeq
SELECT a.TerminalID,a.AcquirerName,b.TID,c.MID
FROM 
(
SELECT DISTINCT (AcquirerTagValue) AS AcquirerName,TerminalID
FROM NewInsys_CIMB.dbo.tbProfileAcquirer WITH (NOLOCK)
WHERE TerminalID = @sTerminalID AND AcquirerTag='AA01' 
) a LEFT JOIN 
(SELECT DISTINCT(AcquirerName),AcquirerTagValue AS TID,TerminalID
FROM NewInsys_CIMB.dbo.tbProfileAcquirer WITH (NOLOCK) WHERE TerminalID = @sTerminalID AND AcquirerTag='AA05') b 
	ON a.TerminalID = b.TerminalID AND a.AcquirerName = b.AcquirerName
LEFT JOIN 
(SELECT DISTINCT(AcquirerName),AcquirerTagValue AS MID,TerminalID
FROM NewInsys_CIMB.dbo.tbProfileAcquirer WITH (NOLOCK) WHERE TerminalID = @sTerminalID AND AcquirerTag='AA04') c
	ON a.TerminalID = c.TerminalID AND a.AcquirerName = c.AcquirerName

GO