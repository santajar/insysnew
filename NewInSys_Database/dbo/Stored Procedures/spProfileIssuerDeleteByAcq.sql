﻿CREATE PROCEDURE [dbo].[spProfileIssuerDeleteByAcq]
	@sTerminalID VARCHAR(8),
	@sMessage VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
    DELETE FROM TbProfileIssuer
	WHERE
		TerminalID = @sTerminalID AND
		IssuerName IN
		(
			SELECT String
			FROM DBO.fn_tbAnyString (@sMessage)
		)
END
