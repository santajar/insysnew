﻿CREATE PROCEDURE [dbo].[spRD_SwVersionEnableDisable]
	@sSV_Name VARCHAR(50),
	@iEnable INT
AS
DECLARE @IDS TABLE(ID INT)

UPDATE tbRD_SoftwareVersion
SET [Enabled]=@iEnable, LastModify = GETDATE()
OUTPUT inserted.SV_ID INTO @IDS
WHERE SV_NAME LIKE @sSV_Name

UPDATE tbRD_SoftwareVersionList
SET [Enabled]=@iEnable
WHERE SV_ID IN (SELECT * FROM @IDS)
