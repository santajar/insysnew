﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUserLoginLockedbyDays]
	@sUserID VARCHAR(50), @iLocked INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @iCount INT,
			@dtLastLogin DATETIME,
			@bRevoke BIT
	
	SELECT @dtLastLogin =LastLogin,@bRevoke = [Revoke]
	FROM tbUserLogin
	WHERE UserID = @sUserID
	
	IF (@bRevoke=0)
	BEGIN
		IF(DATEDIFF(DAY,@dtLastLogin,GETDATE())>= DBO.iGetTotalDaysLocked())
		BEGIN
			UPDATE tbUserLogin
			SET Locked = 1
			WHERE UserID = @sUserID
		END
	END
	
	SELECT @iLocked = Locked
	FROM tbUserLogin
	WHERE UserID = @sUserID
	
	RETURN @iLocked    
END