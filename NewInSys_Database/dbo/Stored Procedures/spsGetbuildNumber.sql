﻿-- =============================================
-- Author:		<Author,,TEDIE SCORFIA>
-- Create date: <Create Date, ,15 DEC 2014>
-- Description:	<Description, ,GET BUILD NUMBER BY TID>
-- =============================================

CREATE PROCEDURE [dbo].[spsGetbuildNumber]
	@sTerminalID VARCHAR(8),
	@iBuildNumber INT OUTPUT

AS

	DECLARE @sQuery NVARCHAR(MAX),
			@ParmDefinition NVARCHAR(500),
			@iCount INT,
			@sLinkDatabase NVARCHAR(50)

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

	SET @sQuery = N'
	SELECT @iParamBuildNumber = B.BuildNumber
	FROM 
	(
		(SELECT * FROM '+@sLinkDatabase+'tbListTIDRemoteDownload) A
		LEFT JOIN
		(SELECT * FROM '+@sLinkDatabase+'tbAppPackageEDCList) B
		ON A.AppPackID=B.AppPackId
	)
	WHERE A.TerminalID = @sParamTerminalID'
	
	SET @ParmDefinition = N'@iParamBuildNumber INT OUTPUT, @sParamTerminalID VARCHAR(8)';

	EXECUTE sp_executesql @sQuery,@ParmDefinition, @sParamTerminalID = @sTerminalID, @iParamBuildNumber = @iBuildNumber OUTPUT