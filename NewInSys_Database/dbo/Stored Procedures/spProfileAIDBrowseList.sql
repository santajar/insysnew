﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 1, 2010
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the list of AID
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAIDBrowseList]
	@sDatabaseId VARCHAR(3)
AS
SELECT DISTINCT
	AIDName as Name
FROM tbProfileAID WITH (NOLOCK)
WHERE DatabaseId=@sDatabaseId
