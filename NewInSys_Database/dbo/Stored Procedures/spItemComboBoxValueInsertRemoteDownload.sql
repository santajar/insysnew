﻿-- =============================================
-- Author:		Tedie
-- Create date: 1 Okt 2014
-- Description:	Input ComboBox Value of Remote Download
-- =============================================
CREATE PROCEDURE [dbo].[spItemComboBoxValueInsertRemoteDownload]
	@sRDName VARCHAR(15),
	@sDatabaseId INT
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @iItemSeq INT, @iCountPax INT

SELECT @iCountPax = COUNT(*)
FROM tbItemList WITH (NOLOCK)
WHERE ItemName LIKE 'Remote Download Name' AND 
	  FormID = 1  AND
	  DatabaseID=@sDatabaseId

IF @iCountPax>0
BEGIN
SELECT @iItemSeq=ItemSequence
FROM tbItemList WITH (NOLOCK)
WHERE ItemName LIKE 'Remote Download Name' AND 
	  FormId=1 AND 
	  DatabaseId=@sDatabaseId

INSERT INTO tbItemComboBoxValue(DatabaseId,
			FormId,
			ItemSequence,
			DisplayValue,
			RealValue)
SELECT @sDatabaseId [DatabaseId], 
	1 [FormID],
	@iItemSeq [ItemSequence],
	@sRDName [DisplayValue],
	@sRDName [RealValue]
END

END