﻿
-- ====================================================
-- Description:	Get Form Item Database
--				Item browsed where Item Sequence Valid.
-- ====================================================

CREATE PROCEDURE [dbo].[spItemListBrowseValidItemSquence] 
	@sDatabaseID SMALLINT
AS
	SELECT ItemSequence, 
			COUNT(ItemSequence) as [Count], 
			CASE FormID
			WHEN '1' THEN 'Terminal'
			WHEN '2' THEN 'Acquirer'
			WHEN '3' THEN 'Issuer'
			WHEN '4' THEN 'Card'
			END Form
	FROM tbItemList WITH (NOLOCK)
	WHERE DATABASEID = @sDatabaseID
	GROUP BY ItemSequence, FormID
	HAVING COUNT(ItemSequence) > 1

