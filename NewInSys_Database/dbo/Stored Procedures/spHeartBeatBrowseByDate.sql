IF OBJECT_ID ('dbo.spHeartBeatBrowseByDate') IS NOT NULL
	DROP PROCEDURE dbo.spHeartBeatBrowseByDate
GO

CREATE PROCEDURE [dbo].[spHeartBeatBrowseByDate]
@sDate DATETIME,
@sCondition VARCHAR(MAX),
@sStatus INT
AS


DECLARE @sQuery VARCHAR(MAX),@Sql VARCHAR(MAX),@sQueryOrderBy VARCHAR(MAX),@sQueryGab VARCHAR(MAX)
SET @sQueryOrderBy = ' ORDER BY  Status DESC, Tgl DESC '

--DECLARE @sDate DATETIME,@sCondition VARCHAR(MAX),@sStatus INT
--SET @sDate = '2017-11-14'

IF OBJECT_ID('tempdb..##tbEdcMonitorHeartBeatCSI') IS NOT NULL  DROP TABLE ##tbEdcMonitorHeartBeatCSI 
SET @Sql = 'CREATE TABLE ##tbEdcMonitorHeartBeatCSI (TerminalID VARCHAR(8),MessageTimestamp DATETIME)'
EXEC (@Sql)
 
IF @sDate = ''
BEGIN	 
	INSERT INTO ##tbEdcMonitorHeartBeatCSI (TerminalID,MessageTimestamp)
	(SELECT TerminalID,max(MessageTimestamp) AS MessageTimestamp
	FROM tbEdcMonitorHeartBeat
	WHERE CONVERT (date, MessageTimestamp) <= CONVERT (date, GETDATE())  
	GROUP BY TerminalID)
END 
IF @sDate <> ''
BEGIN 
	INSERT INTO ##tbEdcMonitorHeartBeatCSI (TerminalID,MessageTimestamp)
	(SELECT TerminalID,max(MessageTimestamp) AS MessageTimestamp
	FROM tbEdcMonitorHeartBeat
	WHERE CONVERT (date, MessageTimestamp) <= CONVERT (date, @sDate)  
	GROUP BY TerminalID)
END

IF @sCondition =''
BEGIN
	IF @sStatus = 0
	BEGIN
		
 		IF @sDate = ''
		BEGIN	    
			IF OBJECT_ID('tempdb..#Temp11') IS NOT NULL  DROP TABLE #Temp11 
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'ACTIVE' AS Status 
			INTO #Temp11
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK) INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) = CONVERT (date, GETDATE())  
			UNION ALL
		    SELECT a.ID,c.MessageTimestamp,a.TerminalID,a.TID,a.MID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'INACTIVE' AS Status 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK) INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) < CONVERT (date, GETDATE())  
			UNION ALL
		    SELECT a.ID,c.MessageTimestamp,a.TerminalID,a.TID,a.MID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'NOT LIST' AS Status 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK) INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp			
			WHERE NOT EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) <= CONVERT (date, GETDATE())  
			
			SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp11'
	    END
 		IF @sDate <> ''
		BEGIN	    
			IF OBJECT_ID('tempdb..#Temp12') IS NOT NULL  DROP TABLE #Temp12 
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'ACTIVE' AS Status 
			INTO #Temp12
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) = CONVERT (date, @sDate)  
			UNION ALL
		    SELECT a.ID,c.MessageTimestamp,a.TerminalID,a.TID,a.MID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'INACTIVE' AS Status 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) < CONVERT (date, @sDate)  
			UNION ALL
		    SELECT a.ID,c.MessageTimestamp,a.TerminalID,a.TID,a.MID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'NOT LIST' AS Status 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE NOT EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) <= CONVERT (date, @sDate)  

		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp12'
	    END	    
		SET @sQueryGab =@sQuery + @sCondition + @sQueryOrderBy
		EXEC (@sQueryGab)
	END
	IF @sStatus = 1
	BEGIN
   		IF @sDate = ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp21') IS NOT NULL  DROP TABLE #Temp21
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'ACTIVE' AS Status 
			INTO #Temp21
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) = CONVERT (date, GETDATE())  	  
				
		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp21'
		END
   		IF @sDate <> ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp22') IS NOT NULL  DROP TABLE #Temp22
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'ACTIVE' AS Status 
			INTO #Temp22 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) = CONVERT (date, @sDate)  
			
		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp22'
		END
		
		
		SET @sQueryGab =@sQuery + @sCondition + @sQueryOrderBy
		EXEC (@sQueryGab)
	END
	IF @sStatus = 2
	BEGIN
   		IF @sDate = ''
		BEGIN	
			IF OBJECT_ID('tempdb..#Temp31') IS NOT NULL  DROP TABLE #Temp31
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID  AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'INACTIVE' AS Status 
			INTO #Temp31 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE 
			EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
   			AND CONVERT (date, c.MessageTimestamp) < CONVERT (date, GETDATE())  
		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp31'
		END
   		IF @sDate <> ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp32') IS NOT NULL  DROP TABLE #Temp32
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID  AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'INACTIVE' AS Status 
			INTO #Temp32 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE 
			EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
 			AND CONVERT (date, c.MessageTimestamp) < CONVERT (date, @sDate)  		  
		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp32'
		END  
		
		SET @sQueryGab =@sQuery + @sCondition + @sQueryOrderBy
		EXEC (@sQueryGab)
	END	
	IF @sStatus = 3
	BEGIN
   		IF @sDate = ''
		BEGIN	
			IF OBJECT_ID('tempdb..#Temp41') IS NOT NULL  DROP TABLE #Temp41
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID  AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'NOT LIST' AS Status 			
			INTO #Temp41
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE 
			NOT EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
   			AND CONVERT (date, c.MessageTimestamp) <= CONVERT (date, GETDATE()) 

		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp41'
		END
   		IF @sDate <> ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp42') IS NOT NULL  DROP TABLE #Temp42
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID  AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'NOT LIST' AS Status 			
			INTO #Temp42
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE 
			NOT EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
   			AND CONVERT (date, c.MessageTimestamp) <= CONVERT (date, @sDate) 
  
		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp42'
		END  
		SET @sQueryGab =@sQuery + @sCondition + @sQueryOrderBy
		EXEC (@sQueryGab)
	END
END

IF @sCondition <> ''
BEGIN
	IF @sStatus = 0
	BEGIN
		IF @sDate = ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp211') IS NOT NULL  DROP TABLE #Temp211
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'ACTIVE' AS Status 
			INTO #Temp211
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) = CONVERT (date, GETDATE())  
			UNION ALL
		    SELECT a.ID,c.MessageTimestamp,a.TerminalID,a.TID,a.MID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'INACTIVE' AS Status 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) < CONVERT (date, GETDATE())  
			UNION ALL
		    SELECT a.ID,c.MessageTimestamp,a.TerminalID,a.TID,a.MID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'NOT LIST' AS Status 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE NOT EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) <= CONVERT (date, GETDATE())  
			
 		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp211'
					 
	    END ELSE	      		
		IF @sDate <> ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp212') IS NOT NULL  DROP TABLE #Temp212
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'ACTIVE' AS Status 
			INTO #Temp212
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) = CONVERT (date, @sDate)  
			UNION ALL
		    SELECT a.ID,c.MessageTimestamp,a.TerminalID,a.TID,a.MID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'INACTIVE' AS Status 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) < CONVERT (date, @sDate)  
			UNION ALL
		    SELECT a.ID,c.MessageTimestamp,a.TerminalID,a.TID,a.MID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'NOT LIST' AS Status 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE NOT EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) <= CONVERT (date, @sDate)  

 		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp212'
		END 	
		SET @sQueryGab =@sQuery + @sCondition + @sQueryOrderBy
		EXEC (@sQueryGab)
	END
	IF @sStatus = 1
	BEGIN
		IF @sDate = ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp221') IS NOT NULL  DROP TABLE #Temp221 
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'ACTIVE' AS Status 
			INTO #Temp221
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) = CONVERT (date, GETDATE())  		
 		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp221'
	    END ELSE	      		
		IF @sDate <> ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp222') IS NOT NULL  DROP TABLE #Temp222 
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'ACTIVE' AS Status 
			INTO #Temp222 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
			AND CONVERT (date, c.MessageTimestamp) = CONVERT (date, @sDate)  
 		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp222'
		END 
		SET @sQueryGab =@sQuery + @sCondition + @sQueryOrderBy
		EXEC (@sQueryGab)
	END
	IF @sStatus = 2
	BEGIN	
		IF @sDate = ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp231') IS NOT NULL  DROP TABLE #Temp231 
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID  AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'INACTIVE' AS Status 
			INTO #Temp231 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE 
			EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
   			AND CONVERT (date, c.MessageTimestamp) <= CONVERT (date, GETDATE())  
			
 		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp231'
	    END ELSE	      		
		IF @sDate <> ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp232') IS NOT NULL  DROP TABLE #Temp232 
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID  AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'INACTIVE' AS Status 
			INTO #Temp232 
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE 
			EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
 			AND CONVERT (date, c.MessageTimestamp) < CONVERT (date, @sDate)  		  
			
 		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp232'
		END 
		SET @sQueryGab =@sQuery + @sCondition + @sQueryOrderBy
		EXEC (@sQueryGab)
	END
	IF @sStatus = 3
	BEGIN	
		IF @sDate = ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp331') IS NOT NULL  DROP TABLE #Temp331 
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID  AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'NOT LIST' AS Status 			
			INTO #Temp331
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE 
			NOT EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
   			AND CONVERT (date, c.MessageTimestamp) <= CONVERT (date, GETDATE()) 

			
 		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp331'
	    END ELSE	      		
		IF @sDate <> ''
		BEGIN
			IF OBJECT_ID('tempdb..#Temp332') IS NOT NULL  DROP TABLE #Temp332 
		    SELECT a.ID,c.MessageTimestamp AS Tgl,a.TerminalID  AS CSI,a.TID AS TerminalID,a.MID AS MerchantID,a.SerialNumber,a.SoftwareVersion,a.ICCID,'NOT LIST' AS Status 			
			INTO #Temp332
			FROM tbEdcMonitorHeartBeat a WITH (NOLOCK)
 			INNER JOIN ##tbEdcMonitorHeartBeatCSI c
			ON c.TerminalID = a.TerminalID AND c.MessageTimestamp = a.MessageTimestamp
			WHERE 
			NOT EXISTS(SELECT b.TerminalID FROM tbEdcMonitorCSI b WHERE b.TerminalID = a.TerminalID) 
			AND c.MessageTimestamp IS NOT NULL AND a.TID IS NOT NULL AND a.MID IS NOT NULL AND a.MID<>''AND a.TID <>''
   			AND CONVERT (date, c.MessageTimestamp) <= CONVERT (date, @sDate) 
			
 		    SET @sQuery = 'SELECT ID,Tgl,CSI,TerminalID,MerchantID,SerialNumber,SoftwareVersion,ICCID,Status FROM #Temp332'
		END 
		SET @sQueryGab =@sQuery + @sCondition + @sQueryOrderBy
		EXEC (@sQueryGab)
	END
	
END

GO

