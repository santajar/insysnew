﻿
CREATE PROCEDURE [dbo].[spProfileTLEListBrowse] 
	@sDatabaseId VARCHAR(3)
AS
	SELECT DISTINCT TLEName [Name],
			DatabaseID			
	FROM tbProfileTLE WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId 



