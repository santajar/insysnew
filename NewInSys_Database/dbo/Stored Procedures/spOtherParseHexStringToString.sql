﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. parse hexstring value to string
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseHexStringToString]
	@sValue VARCHAR(100),
	@sString VARCHAR(MAX) OUTPUT
AS
SET NOCOUNT ON
DECLARE @sql NVARCHAR(MAX)
SET @sql=N'SELECT @sTemp=CONVERT(VARCHAR, 0x'+@sValue+')'

--DECLARE @TEMP TABLE(RESULT VARCHAR(MAX))
--INSERT INTO @TEMP(RESULT)
EXEC sp_executesql @sql,N'@sTemp VARCHAR(MAX) OUTPUT',@sTemp=@sString OUTPUT

--SELECT @sString=RESULT FROM @TEMP
