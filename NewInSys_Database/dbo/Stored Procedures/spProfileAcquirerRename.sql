﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 20, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. renaming the acquirer name of a profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerRename]
	@sTerminalID VARCHAR(8),
	@sOldAcq VARCHAR(15),
	@sNewAcq VARCHAR(15)
AS
BEGIN
	UPDATE tbProfileAcquirer
	SET AcquirerName = @sNewAcq
	WHERE TerminalID = @sTerminalID AND
		AcquirerName = @sOldAcq

	UPDATE tbProfileAcquirer
	SET AcquirerTagValue = @sNewAcq,
		AcquirerTagLength = LEN(@sNewAcq),
		AcquirerLengthOfTagLength = LEN(LEN(@sNewAcq))
	WHERE TerminalID = @sTerminalID AND
		AcquirerName = @sNewAcq AND
		AcquirerTag IN ('AA01','AA001')
END
