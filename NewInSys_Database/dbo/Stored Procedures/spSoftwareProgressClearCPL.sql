﻿CREATE PROCEDURE spSoftwareProgressClearCPL
	@sTerminalID VARCHAR(8)
AS
DELETE FROM tbSoftwareProgressCPL WHERE TerminalID=@sTerminalID
DELETE FROM tbSoftwareProgressCPLDetail WHERE TerminalID=@sTerminalID