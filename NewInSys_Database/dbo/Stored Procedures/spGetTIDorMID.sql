﻿CREATE PROCEDURE [dbo].[spGetTIDorMID]
	@sTerminalID varchar(8)=null,
	@sAcquirerName varchar(20)=null,
	@sFLAG varchar(20)=null

AS

IF @sFLAG = 'TID'
BEGIN
SELECT AcquirerTagValue From tbProfileAcquirer WHERE TerminalID=@sTerminalID AND AcquirerName=@sAcquirerName AND AcquirerTag='AA05'
END
IF @sFLAG = 'MID'
BEGIN
SELECT AcquirerTagValue From tbProfileAcquirer WHERE TerminalID=@sTerminalID AND AcquirerName=@sAcquirerName AND AcquirerTag='AA04'
END



GO

