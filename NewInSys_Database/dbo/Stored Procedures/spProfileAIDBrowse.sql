﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 1, 2010
-- Modify date:
--				1. <MMM dd, yyyy>, <desc>
-- Description:	
--				1. Get the content of AID table
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAIDBrowse]
	@sCondition VARCHAR(MAX)=NULL
AS
DECLARE @sQuery VARCHAR(MAX)
	SET @sQuery='
	SELECT DatabaseId,
		AIDName,
		AIDTag as Tag,
		AIDTagLength,
		AIDTagValue as Value
	FROM tbProfileAID WITH (NOLOCK) '
	SET @sQuery = @sQuery + ISNULL(@sCondition,'') + ' ORDER BY AIDName, AIDTag'
EXEC (@sQuery)
