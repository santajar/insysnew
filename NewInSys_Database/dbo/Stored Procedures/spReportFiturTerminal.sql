﻿CREATE PROCEDURE [dbo].[spReportFiturTerminal]
	@sTerminalID VARCHAR(8)
AS
DECLARE @sMerch1 VARCHAR(23),
	@sMerch2 VARCHAR(23),
	@sMerch3 VARCHAR(23),
	@sCategory VARCHAR(50)

print '[spFiturTerminal] ambil DE02'

SET @sMerch1 = dbo.sReportFiturTerminalTagValue(@sTerminalID,'DE02')
SET @sMerch2 = dbo.sReportFiturTerminalTagValue(@sTerminalID,'DE03')
SET @sMerch3 = dbo.sReportFiturTerminalTagValue(@sTerminalID,'DE04')
SET @sCategory = dbo.sReportFiturTerminalTagValue(@sTerminalID,'DE05')

print '[spFiturTerminal] after ambil DE02'

--CREATE TABLE #tbAcqIssCard
--(
--	TerminalID VARCHAR(8),
--	AcqName VARCHAR(25),
--	Issname VARCHAR(25),
--	CrdName VARCHAR(25)
--)

--INSERT INTO #tbAcqIssCard (TerminalID,AcqName,Issname,CrdName)
--EXEC sp_tbAcqIssCard @sTerminalID

DECLARE @tempAcq TABLE(
	ID INT IDENTITY(1,1),
	AcqName VARCHAR(25),
	TID VARCHAR(8),
	MID VARCHAR(15),
	T1 VARCHAR(15),
	T2 VARCHAR(15),
	ManualEntry BIT,
	iOffline BIT,
	Adjust BIT,
	Refund BIT,
	CardVer BIT
)

INSERT INTO @tempAcq(AcqName)
SELECT DISTINCT(AcqName) FROM dbo.fn_tbAcqIssCard (@sTerminalID)

DECLARE @iLoop INT,
	@iRowMax INT
SET @iLoop = 1
SELECT @iRowMax = MAX(ID) FROM @tempAcq

DECLARE @sAcqName VARCHAR(25),
	@sTID VARCHAR(8),
	@sMID VARCHAR(15)

WHILE @iLoop <= @iRowMax
BEGIN
	SELECT @sAcqName = AcqName FROM @tempAcq WHERE ID = @iLoop

	DECLARE 
		@sT1 VARCHAR(15),
		@sT2 VARCHAR(15),
		@sManualEntry BIT,
		@sOffline BIT,
		@sAdjust BIT,
		@sRefund BIT,
		@sCardVer BIT
 
	SET @sTID = dbo.sReportFiturAcquirerTagValue(@sTerminalID,@sAcqName,'AA05')
	SET @sMID = dbo.sReportFiturAcquirerTagValue(@sTerminalID,@sAcqName,'AA04')
	SET @sT1 = dbo.sReportFiturAcquirerTagValue(@sTerminalID,@sAcqName,'AA06')
	SET @sT2 = dbo.sReportFiturAcquirerTagValue(@sTerminalID,@sAcqName,'AA07')
	EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE07',0,@sManualEntry OUTPUT
	EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE11',0,@sOffline OUTPUT
	EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE12',0,@sAdjust OUTPUT
	EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE13',0,@sRefund OUTPUT
	EXEC spReportFiturIssuerTagValue @sTerminalID,@sAcqName,'AE14',0,@sCardVer OUTPUT

	SELECT @sTerminalID TerminalID, 
		@sAcqName AcqName, 
		@sTID TID, 
		@sMID MID, 
		@sT1 T1, 
		@sT2 T2,
		@sManualEntry ManualEntry,
		@sOffline iOffline,
		@sAdjust Adjust,
		@sRefund Refund,
		@sCardVer CardVer
	INTO #tbFiturAcquirer

	
	print '[spFiturTerminal] EXEC spFiturAcquirer ' + @sAcqName

	UPDATE @tempAcq
	SET TID = b.TID, 
		MID = b.MID, 
		T1 = b.T1, 
		T2 = b.T2, 
		ManualEntry = b.ManualEntry,
		iOffline = b.iOffline,
		Adjust = b.Adjust,
		Refund = b.Refund,
		CardVer = b.CardVer
	FROM @tempAcq a, #tbFiturAcquirer b
	WHERE a.ID = @iLoop AND a.AcqName=b.AcqName

	SELECT  @iLoop = @iLoop + 1
	DROP TABLE #tbFiturAcquirer

END

SELECT
	@sTerminalID TerminalID_Init,
	@sCategory Category,
	AcqName,
	TID,
	MID,
	@sMerch1 MerchName1,
	@sMerch2 MerchName2,
	@sMerch3 MerchName3,
	T1,
	T2,
	ManualEntry,
	iOffline,
	Adjust,
	Refund,
	CardVer
FROM @tempAcq