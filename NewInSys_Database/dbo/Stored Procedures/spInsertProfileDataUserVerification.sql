﻿

-- =============================================
-- Author:		Ibnu Saefullah
-- Create date: January 20, 2017
-- =============================================
CREATE PROCEDURE [dbo].[spInsertProfileDataUserVerification]
	@sUserID NVarchar(15),
	@sNama NVARCHAR (50),
	@sPassword NVARCHAR(15),
	@sUserRole NVARCHAR(30)
	

AS
BEGIN

INSERT INTO tbUserLoginVerification 
(UserID, Nama, [Password], UserRole)
VALUES
(@sUserID, @sNama, @sPassword, @sUserRole)
END