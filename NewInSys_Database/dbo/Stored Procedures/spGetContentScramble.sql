﻿CREATE PROCEDURE [dbo].[spGetContentScramble]
	@sTerminalID VARCHAR(8)
AS
IF OBJECT_ID('TEMPDB..#TempContent') IS NOT NULL
DROP TABLE #TempContent
CREATE TABLE #TempContent
(
CSI Varchar(8), TID Varchar(8), MID varchar(40), MERCHANTNAME Varchar(100),
[ADDRESS] Varchar(100), CITY Varchar(100), PROFILECREATED varchar (30), Password VARCHAR(6)
)
DECLARE @sCSI VARCHAR(8), @sTID VARCHAR(8), @sMID VARCHAR(40), @sMerchantName VARCHAR(100), @sAddress VARCHAR(100), @sCity VARCHAR(100), @sCreateData VARCHAR(30), @sPassword VARCHAR(6)

SELECT @sMerchantName = TerminalTagValue FROM tbProfileTerminal WHERE TerminalID = @sTerminalID AND TerminalTag='DE02'
SELECT @sAddress = TerminalTagValue FROM tbProfileTerminal WHERE TerminalID = @sTerminalID AND TerminalTag='DE03'
SELECT @sCity = TerminalTagValue FROM tbProfileTerminal WHERE TerminalID = @sTerminalID AND TerminalTag='DE04'
SELECT @sCreateData = CreateDate FROM tbProfileTerminalList WHERE TerminalID = @sTerminalID 
SELECT @sPassword = TerminalTagValue FROM tbProfileTerminal WHERE TerminalID = @sTerminalID AND TerminalTag='DC04'

DECLARE @iCountID INT
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CIMBN' AND TerminalID=@sTerminalID
print 'icount' + COnvert(varchar(10),@iCountID)
IF @iCountID > 1
BEGIN
	SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
	SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CREDIT' AND TerminalID=@sTerminalID
IF @iCountID > 1
BEGIN
SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END

SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'DEBIT' AND TerminalID=@sTerminalID
print 'icount' + COnvert(varchar(10),@iCountID)
IF @iCountID > 1
BEGIN
	SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
	SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP03' AND TerminalID=@sTerminalID
IF @iCountID > 1
BEGIN
SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP06'AND TerminalID=@sTerminalID
IF @iCountID > 1
BEGIN
SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END

ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP09'AND TerminalID=@sTerminalID
IF @iCountID > 1
BEGIN
SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
	SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END

ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP12' AND TerminalID=@sTerminalID
IF @iCountID > 1
BEGIN
SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP18' AND TerminalID=@sTerminalID
IF @iCountID > 1
BEGIN
SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
	SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP24'AND TerminalID=@sTerminalID
IF @iCountID > 1
BEGIN
SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END
ELSE IF @iCountID = 0
SELECT @iCountID = COUNT(*)
FROM tbProfileAcquirer WITH (NOLOCK)
WHERE AcquirerName = 'CILTAP36'AND TerminalID=@sTerminalID
IF @iCountID > 1
BEGIN
SELECT @sTID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa05'
SELECT @sMID = AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = @sTerminalID AND AcquirerTag='aa04'
END
PRINT 'MULAI'
INSERT INTO #TempContent (CSI, TID, MID, MERCHANTNAME,[ADDRESS], CITY, PROFILECREATED, [Password])
VALUES(@sTerminalID, @sTID, @sMID, @sMerchantName, @sAddress, @sCity, @sCreateData, @sPassword)

PRINT @sTerminalID
PRINT @sTID
PRINT @sMID
PRINT @sMerchantName
PRINT @sAddress
PRINT @sCity
PRINT @sCreateData
PRINT @sPassword

PRINT 'AKHIR'


select * from #TempContent

--BEGIN
--	select TerminalTagValue as CONTENT 
--	from tbProfileTerminal where TerminalID=@sTerminalID 
--	AND TerminalTag In ('de01','de02','de03','de04','de09')
--END