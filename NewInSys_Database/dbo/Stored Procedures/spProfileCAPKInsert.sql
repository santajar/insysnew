﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 3, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Insert new CAPK into tbProfileCAPK
--				2. Modify to use form flexible`
-- =============================================
CREATE PROCEDURE [dbo].[spProfileCAPKInsert]
	@iDatabaseID INT,
	@sCAPKName VARCHAR(50),
	@sContent VARCHAR(MAX)
AS

IF (DBO.isTagLength4(@iDatabaseID) = 4)
BEGIN
	INSERT INTO tbProfileCAPK(
		DatabaseId,
		[CAPKIndex],
		[CAPKTag],
		[CAPKLengthOfTagLength],
		[CAPKTagLength],
		[CAPKTagValue]
		)
	SELECT 
		@iDatabaseID [DatabaseId],
		@sCAPKName [CAPKIndex],
		Tag [CAPKTag],
		LEN(TagLength) [CAPKLengthOfTagLength],
		TagLength [CAPKTagLength],
		TagValue [CAPKTagValue]
	FROM fn_tbCAPKTLV(@sCAPKName,@sContent)
END
ELSE
BEGIN
	INSERT INTO tbProfileCAPK(
		DatabaseId,
		[CAPKIndex],
		[CAPKTag],
		[CAPKLengthOfTagLength],
		[CAPKTagLength],
		[CAPKTagValue]
		)
	SELECT 
		@iDatabaseID [DatabaseId],
		@sCAPKName [CAPKIndex],
		Tag [CAPKTag],
		LEN(TagLength) [CAPKLengthOfTagLength],
		TagLength [CAPKTagLength],
		TagValue [CAPKTagValue]
	FROM fn_tbCAPKTLV5(@sCAPKName,@sContent)
END

UPDATE tbProfileTerminalList
SET EMVInit = '1'
WHERE DatabaseID = @iDatabaseID
