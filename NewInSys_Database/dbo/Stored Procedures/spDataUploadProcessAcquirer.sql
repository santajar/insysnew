﻿
-- =============================================
-- Author:		Kiki Ariady
-- Create date: Oct 7, 2010
-- Modify	  :
--				1. Mar 09, 2011, update log string
-- Description:	
--				1. Process the acquirer table of profile Data Upload
-- =============================================
CREATE PROCEDURE [dbo].[spDataUploadProcessAcquirer]
	@AcquirerTable VARCHAR(150),
	@sUser VARCHAR(30),
	@iProcessType BIT
AS
DECLARE @sTid CHAR(8),
	@sAcq VARCHAR(8),
	@iOffsetStart INT, 
	@iOffsetEnd INT,
	@sColumnName VARCHAR(50),
	@sTag VARCHAR(5),
	@sNewTagValue VARCHAR(MAX),
	@sSqlstmt NVARCHAR(MAX),
	@iIdStart INT,
	@iIdMax INT,
	@sQuery VARCHAR(MAX)

IF OBJECT_ID('tempdb..#tbUpload') IS NOT NULL
	DROP TABLE #tbUpload

CREATE TABLE #tbUpload (
	Id INT IDENTITY(1,1),
	TerminalId CHAR(8),
	AcquirerName VARCHAR(50)
)

SET NOCOUNT ON
SET @sQuery ='
	INSERT INTO #tbUpload(TerminalId,AcquirerName)
	SELECT TerminalId, AcquirerName FROM ' + @AcquirerTable + ' WITH (NOLOCK)'

EXEC (@sQuery)

SELECT @iIdStart = MIN(Id), @iIdMax = MAX(Id)
FROM #tbUpload

WHILE @iIdStart <= @iIdMax
BEGIN
	SELECT @sTid = TerminalId, @sAcq = AcquirerName
	FROM #tbUpload
	WHERE Id = @iIdStart
	
	DECLARE @sDbName VARCHAR(30)
	DECLARE @sAction VARCHAR(MAX), @sActionDetail VARCHAR(MAX)
	IF @iProcessType=1
		SET @sAction = 'Upload, Add ' + @sTid
	ELSE IF @iProcessType=2
		SET @sAction = 'Upload, Update ' + @sTid
	ELSE IF @iProcessType=3
		SET @sAction = 'Upload, Delete ' + @sTid
	 
	SET @sDbName=dbo.sDatabaseNameByTerminalId(@sTID)

	IF dbo.IsTerminalIdExist(@sTID)=1
	BEGIN
		SELECT @iOffsetStart=MIN(id)
			FROM tbUploadTagAA WITH (NOLOCK)
			WHERE AcquirerName=@sAcq
			
		SELECT @iOffsetEnd=MAX(id)
			FROM tbUploadTagAA WITH (NOLOCK)
			WHERE AcquirerName=@sAcq
		
		WHILE @iOffsetStart <= @iOffsetEnd
		BEGIN
			
			SELECT @sColumnName=ColumnName, @sTag=Tag
			FROM tbUploadTagAA WITH (NOLOCK)
			WHERE id = @iOffsetStart
					
			SET @sSqlstmt = 'SELECT @sNewTagValue=['+ @sColumnName + ']
				FROM '+ @AcquirerTable +' WITH (NOLOCK)5
				WHERE TerminalId=''' + @sTid + '''
				AND AcquirerName=''' + @sAcq + ''''

			--PRINT '@sqlstmt  = ' + @sqlstmt

			EXEC sp_executesql @sSqlstmt, N'@sNewTagValue VARCHAR(MAX) OUTPUT', @sNewTagValue=@sNewTagValue OUTPUT

			DECLARE @sNewAcq VARCHAR(25)
			SET @sNewAcq = NULL
			IF dbo.IsAcquirerExist(@sTid, @sAcq) = 0
			BEGIN
				print 'tidak ada acquirer ' + @sAcq
				SELECT @sNewAcq = COALESCE( 
					CASE WHEN @sAcq = 'BCA' THEN 'BCA1' END,
					CASE WHEN @sAcq = 'PROMO F' THEN 'PROMO SQ A' END,
					CASE WHEN @sAcq = 'PROMO G' THEN 'PROMO SQ B' END,
					CASE WHEN @sAcq = 'PROMO H' THEN 'PROMO SQ C' END,
					CASE WHEN @sAcq = 'PROMO I' THEN 'PROMO SQ D' END,
					CASE WHEN @sAcq = 'PROMO J' THEN 'PROMO SQ E' END
					)
			END
			ELSE
				SET @sNewAcq = @sAcq

			print '@sTid '+ @sTid + ' @sNewAcq ' + @sNewAcq + ' @sTag ' + @sTag + ' @sNewTagValue '+ @sNewTagValue
			
			IF dbo.IsAcquirerExist(@sTid, @sNewAcq) != 0
				EXEC spProfileAcquirerUpdateTag @sTid, @sNewAcq, @sTag, @sNewTagValue

			SET @iOffsetStart = @iOffsetStart + 1
		END
		SET @sActionDetail = 'Upload success.'
		
	END
	DECLARE @sOldTid VARCHAR(8)
	SET @sOldTid = @sTid
	SET @iIdStart = @iIdStart + 1
	IF @sOldTid != @sTid
	BEGIN
		EXEC spAuditTrailInsert @sOldTid, 
				@sUser,
				@sDbName,
				@sAction,
				@sActionDetail
		EXEC spUploadLogInsert @sOldTid, 
				@sAction,
				@sActionDetail
	END
END