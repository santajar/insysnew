﻿-- =============================================  
-- Author:  Tedie Scorfia  
-- Create date: march 2, 2015  
-- Description: view AID list  
-- =============================================  
CREATE PROCEDURE [dbo].[spProfileAIDListBrowse]  
 @sDatabaseId VARCHAR(3) = ''  
AS  
SELECT AIDTagID,  
	--DatabaseId,  
	AIDName  
FROM dbo.fn_viewAID()  
WHERE DatabaseId = @sDatabaseId--case when @sDatabaseId = '' then DatabaseId else @sDatabaseId end