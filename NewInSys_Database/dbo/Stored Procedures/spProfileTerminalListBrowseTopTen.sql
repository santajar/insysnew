﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 29, 2010
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Get the list of last 10 accessed profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalListBrowseTopTen] 
@iDatabaseID INT
AS
DECLARE @sQuery varchar(MAX)
	SET @sQuery = 'SELECT TOP 10 TerminalID,
					DatabaseID,
					AllowDownload,
					StatusMaster,
					LastView,
					EnableIP,
					IpAddress,
					LocationID'

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
		WHERE Name = N'AutoInitTimeStamp' AND
		Object_ID = Object_ID (N'tbProfileTerminalList')
	  )
		SET @sQuery = @sQuery + ', AutoInitTimeStamp'

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'InitCompress' AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
		SET @sQuery = @sQuery + ', InitCompress'
	ELSE 
		SET @sQuery = @sQuery + ', 0 InitCompress,'

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'AppPackID' AND
			Object_ID = Object_ID (N'tbProfileTerminalList')
		  )
		SET @sQuery= @sQuery + ', AppPackID '
	ELSE 
		SET @sQuery = @sQuery + ''''', AppPackID '
 
		
	SET @sQuery = @sQuery + 
				' FROM tbProfileTerminalList WITH (NOLOCK)
				  WHERE DatabaseID = ' + CONVERT(VARCHAR,@iDatabaseID)
	SET  @sQuery = @sQuery + ' ORDER BY LastView DESC'

	EXEC (@sQuery)
