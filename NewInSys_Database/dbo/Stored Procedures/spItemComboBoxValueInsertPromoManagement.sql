﻿-- =============================================
-- Author:		Ibnu
-- Create date: 9 Feb 2016
-- Description:	Input ComboBox Value of Remote Download
-- =============================================
CREATE PROCEDURE [dbo].[spItemComboBoxValueInsertPromoManagement]
	@sPromoManagementName VARCHAR(15),
	@sDatabaseId INT
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @iItemSeq INT, @iCountPax INT

SELECT @iCountPax = COUNT(*)
FROM tbItemList WITH (NOLOCK)
WHERE ItemName LIKE 'Promo Name' AND 
	  FormID = 1  AND
	  DatabaseID=@sDatabaseId

IF @iCountPax>0
BEGIN
SELECT @iItemSeq=ItemSequence
FROM tbItemList WITH (NOLOCK)
WHERE ItemName LIKE 'Promo Name' AND 
	  FormId=1 AND 
	  DatabaseId=@sDatabaseId

INSERT INTO tbItemComboBoxValue(DatabaseId,
			FormId,
			ItemSequence,
			DisplayValue,
			RealValue)
SELECT @sDatabaseId [DatabaseId], 
	1 [FormID],
	@iItemSeq [ItemSequence],
	@sPromoManagementName [DisplayValue],
	@sPromoManagementName [RealValue]
END

END