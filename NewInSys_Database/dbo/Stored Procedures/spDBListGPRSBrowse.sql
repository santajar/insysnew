﻿
-- =============================================  
-- Author:  Christianto  
-- Create date: Apr 15, 2012  
-- Description: view DB list with GPRSInit
-- =============================================  
CREATE PROCEDURE [dbo].[spDBListGPRSBrowse]  
AS
SELECT DatabaseID, DatabaseName from tbProfileTerminalDB WITH (NOLOCK)
	WHERE GPRSInit = 1
	ORDER BY DatabaseName