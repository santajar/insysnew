﻿CREATE PROCEDURE [dbo].[spScheduleSoftwareSetMonthly]
	@sScheduleType VARCHAR(10),
	@sScheduleCek VARCHAR(10),
	@dtScheduleTime DATETIME,
	@sTYPE VARCHAR(2) OUTPUT,
	@sCek VARCHAR(2) OUTPUT,
	@sTime VARCHAR(4) OUTPUT
AS
DECLARE @sHour VARCHAR(2),
		@sMinute VARCHAR(2)

SET @sType = 'MO'
IF (@dtScheduleTime is not null)
BEGIN
	SET @sHour = RIGHT('0'+CONVERT(VARCHAR, DATEPART(HOUR,@dtScheduleTime)),2)
	SET @sMinute = RIGHT('0'+CONVERT(VARCHAR, DATEPART(MINUTE,@dtScheduleTime)),2)
	SET @sTime = @sHour+@sMinute
	PRINT @sTime
END
ELSE
BEGIN
	SET @sTime = '0000'
END
--set days of month
IF (@sScheduleCek= '1')
BEGIN
	SET @sCek = '01'
END
ELSE IF (@sScheduleCek= '2')
BEGIN
	SET @sCek = '02'
END
ELSE IF (@sScheduleCek= '3')
BEGIN
	SET @sCek = '03'
END
ELSE IF (@sScheduleCek= '4')
BEGIN
	SET @sCek = '04'
END
ELSE IF (@sScheduleCek= '5')
BEGIN
	SET @sCek = '05'
END
ELSE IF (@sScheduleCek= '6')
BEGIN
	SET @sCek = '06'
END
ELSE IF (@sScheduleCek= '7')
BEGIN
	SET @sCek = '07'
END
ELSE IF (@sScheduleCek= '8')
BEGIN
	SET @sCek = '08'
END
ELSE IF (@sScheduleCek= '9')
BEGIN
	SET @sCek = '09'
END
ELSE IF (@sScheduleCek= '10')
BEGIN
	SET @sCek = '10'
END
ELSE IF (@sScheduleCek= '11')
BEGIN
	SET @sCek = '11'
END
ELSE IF (@sScheduleCek= '12')
BEGIN
	SET @sCek = '12'
END
ELSE IF (@sScheduleCek= '13')
BEGIN
	SET @sCek = '13'
END
ELSE IF (@sScheduleCek= '14')
BEGIN
	SET @sCek = '14'
END
ELSE IF (@sScheduleCek= '15')
BEGIN
	SET @sCek = '15'
END
ELSE IF (@sScheduleCek= '16')
BEGIN
	SET @sCek = '16'
END
ELSE IF (@sScheduleCek= '17')
BEGIN
	SET @sCek = '17'
END
ELSE IF (@sScheduleCek= '18')
BEGIN
	SET @sCek = '18'
END
ELSE IF (@sScheduleCek= '19')
BEGIN
	SET @sCek = '19'
END
ELSE IF (@sScheduleCek= '20')
BEGIN
	SET @sCek = '20'
END
ELSE IF (@sScheduleCek= '21')
BEGIN
	SET @sCek = '21'
END
ELSE IF (@sScheduleCek= '22')
BEGIN
	SET @sCek = '22'
END
ELSE IF (@sScheduleCek= '23')
BEGIN
	SET @sCek = '23'
END
ELSE IF (@sScheduleCek= '24')
BEGIN
	SET @sCek = '24'
END
ELSE IF (@sScheduleCek= '25')
BEGIN
	SET @sCek = '25'
END
ELSE IF (@sScheduleCek= '26')
BEGIN
	SET @sCek = '26'
END
ELSE IF (@sScheduleCek= '27')
BEGIN
	SET @sCek = '27'
END
ELSE IF (@sScheduleCek= '28')
BEGIN
	SET @sCek = '28'
END