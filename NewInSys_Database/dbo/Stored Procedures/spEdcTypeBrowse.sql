﻿CREATE PROCEDURE [dbo].[spEdcTypeBrowse]
AS
SELECT EdcTypeId, EdcTypeName
FROM tbEdcType WITH (NOLOCK)