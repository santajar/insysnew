﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 8, 2010
-- Modify date:
--				1. 
-- Description:	Update LastUpdate TerminalId profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalUpdateLastUpdate]
	@sTerminalID VARCHAR(8)
AS
DECLARE @sLastUpdate VARCHAR(12)
SET @sLastUpdate = dbo.sGetDateMMDDYY()
SET @sLastUpdate = @sLastUpdate + dbo.sGetTimeHHMMSS()

UPDATE tbProfileTerminal
SET 
	TerminalTagValue=@sLastUpdate,
	TerminalTagLength=LEN(@sLastUpdate)
WHERE TerminalId = @sTerminalID
	AND TerminalTag in ('DC03','DC003')
