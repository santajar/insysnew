﻿
CREATE PROCEDURE [dbo].[spProfileTLEDelete]
	@iDatabaseId INT,
	@sTLEName VARCHAR(50),
	@bDeleteData BIT = 1
AS
	DECLARE @sTLEID VARCHAR(2)
	DECLARE @iCount INT
	
	IF (dbo.isTagLength4(@iDatabaseId)=4)
	BEGIN
		SELECT @sTLEID = TLETagValue	
		FROM tbProfileTLE WITH (NOLOCK)
		WHERE TLEName = @sTLEName AND 
			  DatabaseID = @iDatabaseID AND 
			  TLETag LIKE 'TL02'

		SELECT @iCount = COUNT(*)
		FROM tbProfileAcquirer WITH (NOLOCK)
		WHERE AcquirerTag = 'AA34' AND 
			  AcquirerTagValue = @sTLEID AND
			  TerminalId IN (SELECT TerminalId
							FROM tbProfileTerminalList
							WHERE DatabaseId = @iDatabaseId)
	END
	ELSE
	BEGIN
		SELECT @sTLEID = TLETagValue	
		FROM tbProfileTLE WITH (NOLOCK)
		WHERE TLEName = @sTLEName AND 
			  DatabaseID = @iDatabaseID AND 
			  TLETag LIKE 'TL002'

		SELECT @iCount = COUNT(*)
		FROM tbProfileAcquirer WITH (NOLOCK)
		WHERE AcquirerTag = 'AA034' AND 
			  AcquirerTagValue = @sTLEID AND
			  TerminalId IN (SELECT TerminalId
							FROM tbProfileTerminalList
							WHERE DatabaseId = @iDatabaseId)
	END

	IF @bDeleteData = 1 AND @iCount > 0
		SELECT 'TLE-EFTSec still used in one or more terminals.'
	ELSE
	BEGIN
		DELETE FROM tbProfileTLE
		WHERE DatabaseID = @iDatabaseId AND
			  TLEName = @sTLEName

		EXEC spItemComboBoxValueDeleteTLE @sTLEName, @sTLEID, @iDatabaseId
		DECLARE @sDatabaseID VARCHAR(3)
		SET @sDatabaseID = CONVERT(VARCHAR, @iDatabaseID)
		EXEC spProfileTerminalUpdateLastUpdateByDbID @sDatabaseID
	END







