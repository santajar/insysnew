﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetDataSoftware] 
	@iAppPackID VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sQuery NVARCHAR(MAX),
			@sLinkDatabase NVARCHAR(50)

	SET @sLinkDatabase = dbo.[sGetFlagValue]('LinkedRemoteDownloadServer')

	SET @sQuery = N'SELECT LIST.AppPackId,BuildNumber,LIST.AppPackageName,AppPackageFilename,UploadTime,TEMP.AppPackageContent,TEMP.Id
					FROM '+@sLinkDatabase+'tbAppPackageEDCList LIST
					JOIN
					'+@sLinkDatabase+'tbAppPackageEDCListTemp TEMP
					ON LIST.AppPackId = TEMP.AppPackId
					WHERE LIST.AppPackId = '''+ @iAppPackID + ''''
	EXEC (@sQuery)

END