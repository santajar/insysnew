﻿
-- ============================================================
-- Description:	 Insert new Card into CardList and ProfileCard
-- ============================================================

CREATE PROCEDURE [dbo].[spProfileCardListInsertById]
	@sNewDatabaseID SMALLINT,
	@sExistingDatabaseID SMALLINT,
	@sCardName VARCHAR(50)

AS
	DECLARE @iCount INT
	SELECT @iCount = COUNT(CardName)
	FROM tbProfileCardList WITH (NOLOCK)
	WHERE DatabaseID = @sNewDatabaseID AND CardName = @sCardName

	IF @iCount = 0
	BEGIN
		--Insert into CardList
		INSERT INTO tbProfileCardList (DatabaseID, CardName)
		SELECT @sNewDatabaseID, CardName
		FROM tbProfileCardList WITH (NOLOCK)
		WHERE DatabaseID = @sExistingDatabaseID AND CardName = @sCardName

		--Get existing CardID
		DECLARE @iOldCardID INT, 
			    @iNewCardID INT

		-- Get new inserted CardID
		SELECT @iOldCardID = CardID 
		FROM tbProfileCardList WITH (NOLOCK)
		WHERE DatabaseID = @sExistingDatabaseID AND CardName = @sCardName

		SELECT @iNewCardID = CardID 
		FROM tbProfileCardList WITH (NOLOCK)
		WHERE DatabaseID = @sNewDatabaseID AND CardName = @sCardName

		--Insert to ProfileCard, where new Card's Profile = existing Card's Profile
	IF(dbo.isTagLength4(@sExistingDatabaseID)=4)
	BEGIN	
		IF(dbo.isTagLength4(@sNewDatabaseID)=5)
		BEGIN
			INSERT INTO tbProfileCard (CardID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue, [Description])
			SELECT @iNewCardID, SUBSTRING(CardTag,1,2)+'0'+SUBSTRING(CardTag,3,2), CardLengthOfTagLength, CardTagLength, CardTagValue, [Description]
			FROM tbProfileCard WITH (NOLOCK)
			WHERE CardID = @iOldCardID
		END
		ELSE
		BEGIN
			INSERT INTO tbProfileCard (CardID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue, [Description])
			SELECT @iNewCardID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue, [Description]
			FROM tbProfileCard WITH (NOLOCK)
			WHERE CardID = @iOldCardID
		END
	END
	ELSE
	BEGIN	
		IF(dbo.isTagLength4(@sNewDatabaseID)=4)
		BEGIN
			INSERT INTO tbProfileCard (CardID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue, [Description])
			SELECT @iNewCardID, SUBSTRING(CardTag,1,2)+SUBSTRING(CardTag,4,2), CardLengthOfTagLength, CardTagLength, CardTagValue, [Description]
			FROM tbProfileCard WITH (NOLOCK)
			WHERE CardID = @iOldCardID
		END
		ELSE
		BEGIN
			INSERT INTO tbProfileCard (CardID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue, [Description])
			SELECT @iNewCardID, CardTag, CardLengthOfTagLength, CardTagLength, CardTagValue, [Description]
			FROM tbProfileCard WITH (NOLOCK)
			WHERE CardID = @iOldCardID
		END
	END
	END
	ELSE
		SELECT 'Card has already exist in Version. Please choose other Card'
