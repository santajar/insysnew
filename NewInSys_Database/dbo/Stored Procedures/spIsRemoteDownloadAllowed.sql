﻿CREATE PROCEDURE [dbo].[spIsRemoteDownloadAllowed]
	@sTerminalID NVARCHAR(8),
	@bReturn BIT OUTPUT
AS
BEGIN
	DECLARE @sQuery NVARCHAR(MAX),
			@ParmDefinition NVARCHAR(500),
			@bRemoteDownload BIT,
			@sLinkDatabase NVARCHAR(50)

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

	set @sQuery = N'SELECT @bParamRemoteDownload = RemoteDownload
				  FROM '+@sLinkDatabase+'tbProfileTerminalList 
				  WITH(NOLOCK)
				  WHERE TerminalId=@sParamTerminalID'
	
	SET @ParmDefinition = N'@sParamTerminalID VARCHAR(8), @bParamRemoteDownload BIT OUTPUT';

	EXECUTE sp_executesql @sQuery,@ParmDefinition, @sParamTerminalID = @sTerminalID, @bParamRemoteDownload = @bRemoteDownload OUTPUT;
	
	SET @bReturn = @bRemoteDownload
	
END