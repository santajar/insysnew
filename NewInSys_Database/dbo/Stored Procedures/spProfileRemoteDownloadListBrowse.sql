﻿/****** Object:  StoredProcedure [dbo].[spProfileRemoteDownloadListBrowse]    Script Date: 11/20/2013 2:14:38 PM ******/
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Nov 20, 2013
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Browse content tbRemoteDownload
-- =============================================
CREATE Procedure [dbo].[spProfileRemoteDownloadListBrowse]
	@sDatabaseId VARCHAR(3)
AS
	SELECT DISTINCT RemoteDownloadName as Name,
			DatabaseID			
	FROM tbProfileRemoteDownload WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId