﻿CREATE PROCEDURE [dbo].[spProfileTerminalDbCopy]
	@sDbSourceID SMALLINT,
	@sDbDestinationID SMALLINT,
	@sDatabaseName VARCHAR(15)
AS
INSERT INTO tbProfileTerminalDb ( DatabaseID,
									DatabaseName,
									NewInit,
									EMVInit,
									TLEInit,
									LoyaltyProd,
									GPRSInit,
									Pinpad
								)
SELECT @sDbDestinationID,
		@sDatabaseName,
		NewInit,
		EMVInit,
		TLEInit,
		LoyaltyProd,
		GPRSInit,
		Pinpad
FROM  tbProfileTerminalDb WITH (NOLOCK)
WHERE DatabaseID = @sDbSourceID

IF EXISTS(SELECT * FROM sys.columns
WHERE Name = N'BankCode' AND OBJECT_ID = OBJECT_ID(N'tbProfileTerminalDb'))
BEGIN
	UPDATE tbProfileTerminalDB SET BankCode=A.BankCode
	FROM tbProfileTerminalDB A WITH (NOLOCK)
	WHERE DatabaseID = @sDbDestinationID AND A.DatabaseID=@sDbSourceID
END 

IF EXISTS(SELECT * FROM sys.columns
WHERE Name = N'ProductCode' AND OBJECT_ID = OBJECT_ID(N'tbProfileTerminalDb'))
BEGIN
	UPDATE tbProfileTerminalDB SET ProductCode=A.ProductCode
	FROM tbProfileTerminalDB A WITH (NOLOCK)
	WHERE DatabaseID = @sDbDestinationID AND A.DatabaseID=@sDbSourceID
END 