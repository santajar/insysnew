﻿

-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Description:	Get The History Issuer table content of Terminal
-- =============================================
CREATE PROCEDURE [dbo].[spProfileIssuerHistoryBrowse]
	@sTerminalID VARCHAR(8),
	@sDatetime VARCHAR(20)
AS
SET NOCOUNT ON;

SELECT ItemName,A.IssuerTagValue AS [Value]
FROM tbProfileIssuerHistory A
LEFT JOIN tbProfileTerminalList B
ON A.TerminalID = B.TerminalID
LEFT JOIN tbItemList C
ON B.DatabaseID = C.DatabaseID AND A.IssuerTag = C.Tag
WHERE A.TerminalID = @sTerminalID
	AND LastUpdate = @sDatetime
	AND FormID = 3
ORDER BY A.TerminalID,A.IssuerName,ItemSequence