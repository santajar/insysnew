﻿

-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Description:	Get The History Acquirer table content of Terminal
-- =============================================
CREATE PROCEDURE [dbo].[spProfileAcquirerHistoryBrowse]
	@sTerminalID VARCHAR(8),
	@sDatetime VARCHAR(20)
AS
SET NOCOUNT ON;

SELECT ItemName,A.AcquirerTagValue AS [Value]
FROM tbProfileAcquirerHistory A
LEFT JOIN tbProfileTerminalList B
ON A.TerminalID = B.TerminalID
LEFT JOIN tbItemList C
ON B.DatabaseID = C.DatabaseID AND A.AcquirerTag = C.Tag
WHERE A.TerminalID = @sTerminalID
	AND LastUpdate = @sDatetime
	AND FormID = 2
ORDER BY A.TerminalID,A.AcquirerName,ItemSequence