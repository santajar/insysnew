﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 8, 2010
-- Modify date:
--				1. 
-- Description:	Insert into tbAuditTrail and tbAuditTrailDetail if there is any
--		detail log.
-- =============================================
CREATE PROCEDURE [dbo].[spAuditTrailInsert]
	@sTerminalID VARCHAR(8),
	@sUserId VARCHAR(10),
	@sDatabaseName VARCHAR(50),
	@sActionDesc VARCHAR(MAX),
	@sActionDetail VARCHAR(MAX)=NULL
AS
	SET NOCOUNT ON
DECLARE @sAccessTime VARCHAR(MAX)
SET @sAccessTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP, 9)

DECLARE @tablevar table (ID BIGINT);
INSERT INTO tbAuditTrail(
	AccessTime,
	UserId,
	DatabaseName,
	ActionDescription)
	OUTPUT INSERTED.logid INTO @tablevar	
VALUES(
	@sAccessTime,
	@sUserId,
	@sDatabaseName,
	@sActionDesc)

--Insert AuditTrailDetail
IF @sActionDetail IS NOT NULL
BEGIN
	DECLARE @iLogId BIGINT
	--SELECT  @iLogId = LogId FROM tbAuditTrail WITH(NOLOCK)
	--	WHERE AccessTime = @sAccessTime AND UserId=@sUserId AND DatabaseName=@sDatabaseName
	--		AND ActionDescription=@sActionDesc
	SELECT @iLogId=id from @tablevar
	INSERT INTO tbAuditTrailDetail(
		LogId,
		Remarks)
	VALUES(
		@iLogId,
		@sActionDetail)
END

--Insert Log
EXEC spAuditTrailEncryptInsert @sTerminalID, @sUserId, @sDatabaseName, @sActionDesc, @sActionDetail



