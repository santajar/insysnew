﻿
-- =============================================
-- Author:		Tobias Setyo
-- Create date: Feb 10, 2016
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Update the Promo Management on table tbProfileInitialFlazz
--				2. Modify for Form Flexsible
-- =============================================
CREATE PROCEDURE [dbo].[spProfileInitialFlazzUpdate]
	@iDatabaseID INT,
	@sInitialFlazzName VARCHAR(20),
	@sContent VARCHAR(MAX)
AS
	EXEC spProfileInitialFlazzDelete @iDatabaseID, @sInitialFlazzName
	EXEC spProfileInitialFlazzInsert @iDatabaseID,@sInitialFlazzName, @sContent