﻿

CREATE PROCEDURE [dbo].[spSendNotification] @sBody VARCHAR(200),
	@sNotifType VARCHAR(1)
AS
DECLARE @sSubject VARCHAR(50),
	@sRecipients NVARCHAR(150),
	@sRecipients1 NVARCHAR(50),
	@sRecipients2 NVARCHAR(50),
	@sRecipients3 NVARCHAR(50),
	@sCopyRecipients VARCHAR(150),
	@sCopyRecipients1 VARCHAR(50),
	@sCopyRecipients2 VARCHAR(50),
	@sCopyRecipients3 VARCHAR(50)

SELECT @sRecipients1=Recipients1, @sRecipients2=Recipients2, @sRecipients3=Recipients3,
	@sCopyRecipients1=CopyRecipients1, @sCopyRecipients2=CopyRecipients2, @sCopyRecipients3=CopyRecipients3
FROM tbEmail
WHERE NotifType=@sNotifType

SET @sRecipients = ''

IF ISNULL(@sRecipients1, '') <> ''
	SET @sRecipients = @sRecipients + @sRecipients1 + ','

IF ISNULL(@sRecipients2, '') <> '' AND ISNULL(@sRecipients3, '') <> ''
	SET @sRecipients = @sRecipients + @sRecipients2 + ',' + @sRecipients3
ELSE
	SET @sRecipients = @sRecipients + @sRecipients2
	
IF ISNULL(@sCopyRecipients1, '') <> ''
	SET @sCopyRecipients = @sCopyRecipients + @sCopyRecipients1 + ','

IF ISNULL(@sCopyRecipients2, '') <> '' AND ISNULL(@sCopyRecipients3, '') <> ''
	SET @sCopyRecipients = @sCopyRecipients + @sCopyRecipients2 + ',' + @sCopyRecipients3
ELSE
	SET @sCopyRecipients = @sCopyRecipients + @sCopyRecipients2

EXEC msdb.dbo.sp_send_dbmail
	@recipients=@sRecipients,
	@copy_recipients =@sCopyRecipients,
	@body=@sBody, 
	@subject=@sSubject,
	@profile_name ='SQL 2008 Mail',
	@file_attachments ='';


