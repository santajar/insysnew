﻿CREATE PROCEDURE [dbo].[spProfileIssuerDeleteByTID]
	@sTerminalID VARCHAR(8)
AS
	DELETE FROM tbProfileIssuer 
		WHERE TerminalID=@sTerminalID
