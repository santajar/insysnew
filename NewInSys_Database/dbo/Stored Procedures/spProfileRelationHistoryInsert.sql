﻿
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: January 25, 2016
-- Modify date: 
-- Description:	
--				1. INSERT The History Relation table content of profile
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationHistoryInsert]
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO tbProfileRelationHistory(TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue,LastUpdate)
	SELECT TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue,CONVERT(VARCHAR(20),GETDATE(),113)
	FROM tbProfileRelation WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
	ORDER BY RelationTagID
END