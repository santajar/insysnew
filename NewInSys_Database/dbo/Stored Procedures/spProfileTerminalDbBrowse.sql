﻿
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: June 7, 2010
-- Description:	Get Profile Database List
-- Table: TerminalDB
-- Modify @sQuery parameter
-- =============================================
CREATE PROCEDURE [dbo].[spProfileTerminalDbBrowse]
	 @sCondition VARCHAR(150) = ''
AS
BEGIN
	DECLARE @sQuery VARCHAR(8000)
	SET @sQuery = 'SELECT DatabaseID, 
		DatabaseName, 
		NewInit,
		EMVInit,
		EMVInitManagement,
		TLEInit,
		LoyaltyProd,
		GPRSInit,
		CurrencyInit,
		RemoteDownload,
		PinPad,
		BankCode,
		ProductCode,
		PromoManagement,
		InitialFlazzManagement,
		CardType,
		EnableMonitor'
	
	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'Custom_CAPK_Name' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		SET @sQuery= @sQuery + ', Custom_CAPK_Name '

	IF EXISTS ( SELECT * FROM SYS.COLUMNS
			WHERE Name = N'Custom_Menu' AND
			Object_ID = Object_ID (N'tbProfileTerminalDB')
		  )
		SET @sQuery= @sQuery + ', Custom_Menu '
	
	SET @sQuery = @sQuery +' FROM tbProfileTerminalDB WITH (NOLOCK) '+ @sCondition
	EXEC (@sQuery + ' ORDER BY DatabaseName')
END