﻿
CREATE PROCEDURE [dbo].[spProfileGPRSInsertTagToAll]
	@sDatabaseID SMALLINT,
	@sTag CHAR(4),
	@iLengthofTagValueLength INT,
	@sTagValue VARCHAR(500),
	@iTagValueLength INT
AS
	INSERT INTO tbProfileGPRS (GPRSName, GPRSTag, GPRSLengthOfTagLength, GPRSTagLength, GPRSTagValue)
	SELECT GPRSName, @sTag, @iLengthOfTagValueLength, @iTagValueLength, @sTagValue
	FROM (SELECT DISTINCT GPRSName
		  FROM tbProfileGPRS WITH (NOLOCK)
		  WHERE DatabaseID = @sDatabaseID
	  	 ) AS Temp

