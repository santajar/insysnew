﻿CREATE PROCEDURE [dbo].[spProfileBankCodeBrowse]
	@sCondition VARCHAR(100) = NULL
AS
DECLARE @sStmt VARCHAR(MAX)
SET @sStmt = 'SELECT BankCodeID,
					BankCode, 
					DatabaseID,
					BankCodeTag as Tag,
					BankCodeLengthOfTagLength,
					BankCodeTagLength,
					BankCodeTagValue as Value
			FROM tbProfileBankCode WITH (NOLOCK) '
SET @sStmt = @sStmt + ISNULL(@sCondition,'') + ' ORDER BY DatabaseID, BankCode, Tag'
EXEC (@sStmt)