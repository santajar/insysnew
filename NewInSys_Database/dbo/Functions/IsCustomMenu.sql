﻿CREATE FUNCTION [dbo].[IsCustomMenu](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iCustomMenu BIT
	SELECT @iCustomMenu=b.Custom_Menu
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @iCustomMenu
END