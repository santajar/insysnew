﻿-- =============================================
-- Author:		<Author,,Tedie>
-- Create date: <Create Date, 27 August 2015>
-- Description:	<Description, Check Custom Menu have data or not>
-- =============================================
create FUNCTION [dbo].[isCustomMenuHaveData]
(
	@sTerminalID VARCHAR(8)
)
RETURNS BIT
AS
BEGIN
	DECLARE @iCount INT,
			@bReturn BIT

	SELECT @iCount = COUNT(*)
	FROM tbProfileCustomMenu WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID

	IF (@iCount > 0)
		SET @bReturn = 1
	ELSE
		SET @bReturn = 0

	RETURN @bReturn
END