﻿
CREATE FUNCTION [dbo].[iLastDownloadIdbySN](@sSN VARCHAR(25))
	RETURNS INT
BEGIN
	RETURN (
		SELECT MAX(appList.Id) 
		FROM tbAppPackageEDCList app JOIN tbAppPackageEDCListTemp appList ON app.AppPackageName = appList.AppPackageName
			JOIN tbTerminalSN terminalsn ON app.AppPackId=terminalsn.AppPackId
		WHERE SerialNumber=@sSN)
END