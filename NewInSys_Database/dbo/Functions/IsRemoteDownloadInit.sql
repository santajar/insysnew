﻿CREATE FUNCTION [dbo].[IsRemoteDownloadInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iRemoteDownloadInit BIT
	SELECT @iRemoteDownloadInit=b.RemoteDownload
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @iRemoteDownloadInit
END