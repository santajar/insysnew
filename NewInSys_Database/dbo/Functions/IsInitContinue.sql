﻿CREATE FUNCTION [dbo].[IsInitContinue](@sProcCode VARCHAR(6))
RETURNS BIT
BEGIN
	DECLARE @bReturn BIT
	IF @sProcCode = '930001' OR @sProcCode = '960001'
		SELECT @bReturn = 1
	ELSE
		SELECT @bReturn = 0
	RETURN (@bReturn)
END
