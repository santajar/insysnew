﻿
CREATE FUNCTION [dbo].[sGetFormName]( @iFormID SMALLINT)
	RETURNS VARCHAR(50)
BEGIN
	DECLARE @sFormName VARCHAR(100)
	SELECT @sFormName  = FormName
	FROM tbItemForm
	WHERE FormID = @iFormID  
	
	RETURN @sFormName  
END



