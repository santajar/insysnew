﻿-- ====================================================================== --
-- Function Name : iIsValidTerminalId
-- Parameter	 : 
--		1. @sTerminalId, initialized terminalid
-- Return		 :
--		ex.: iIsValidTerminalId('12345678') --> 0
-- Description	 :
--		return boolean, 1 : valid TerminalId, 0 : invalid TerminalId
-- Modify		 :
--		1. Jul 04, 2012, directing init to table tbInitTerminal
-- ====================================================================== --
CREATE FUNCTION [dbo].[IsValidTerminalId](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	--RETURN (SELECT COUNT(*) FROM tbProfileTerminalList 
	--	WHERE TerminalId=@sTerminalId)
	RETURN (
		--SELECT (CASE WHEN COUNT(*)>0 THEN 1	ELSE 0 END) 
		--FROM tbInitTemp
		SELECT (CASE WHEN COUNT(*)>0 THEN 1	ELSE 0 END) 
		FROM tbProfileTerminalList 
		WITH(NOLOCK)
		WHERE TerminalId=@sTerminalId)
END
