﻿CREATE FUNCTION [dbo].[iCurrentDownloadLoop](@sTerminalId VARCHAR(8))
RETURNS INT
BEGIN
	RETURN (SELECT COUNT(*) FROM tbDownloadApp 
		WHERE TerminalId=@sTerminalId AND Flag=1)
END

