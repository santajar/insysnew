﻿

CREATE FUNCTION [dbo].[fn_tbProfileContentRemoteDownload](  
 @sTerminalID VARCHAR(8),  
 @sConditions VARCHAR(MAX)  
 )  
RETURNS @tempTerminal TABLE(  
  TerminalId VARCHAR(8),  
  [Name] VARCHAR(15),  
  Tag VARCHAR(5),  
  ItemName VARCHAR(50),  
  TagLength VARCHAR(3),  
  TagValue VARCHAR(MAX)  
 )  
BEGIN  
INSERT INTO @tempTerminal (TerminalId,  
  [Name],  
  Tag,  
  ItemName,  
  TagLength,  
  TagValue )  
SELECT b.TerminalId,   
 a.RemoteDownloadName [Name],   
 a.RemoteDownloadTag Tag,  
 '' ItemName,   
 a.RemoteDownloadTagLength TagLength,  
 a.RemoteDownloadTagValue TagValue  
FROM tbProfileRemoteDownload a WITH (NOLOCK) JOIN tbProfileTerminalList b WITH (NOLOCK)
ON a.DatabaseId=b.DatabaseId  
WHERE b.TerminalId=@sTerminalID  
	AND RemoteDownloadName IN 
		(SELECT C.TerminalTagValue
		FROM tbProfileTerminal C WITH (NOLOCK)
		JOIN tbProfileTerminalList D
		ON C.TerminalID=D.TerminalID
		JOIN tbItemList E WITH (NOLOCK)
		ON D.DatabaseID= E.DatabaseID and c.terminaltag=e.Tag
		WHERE E.ItemName = 'Remote Download Name'
			AND C.TerminalID =@sTerminalID )
RETURN  
END