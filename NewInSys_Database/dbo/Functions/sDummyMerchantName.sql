﻿
CREATE FUNCTION [dbo].[sDummyMerchantName] (@sMidDummy VARCHAR(15))
	 RETURNS VARCHAR(50)
BEGIN
	RETURN (SELECT DummyName 
			FROM tbMIDDummy
			WHERE MID = @sMidDummy)
END

