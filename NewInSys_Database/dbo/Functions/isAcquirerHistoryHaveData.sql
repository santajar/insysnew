﻿-- =============================================
-- Author:		<Author,,Tedie>
-- Create date: <Create Date, 20 January 2016>
-- Description:	<Description, Check History Acquirer have data or not>
-- =============================================
Create FUNCTION [dbo].[isAcquirerHistoryHaveData]
(
	@sTerminalID VARCHAR(8),
	@sAcquirerName VARCHAR(25)
)
RETURNS BIT
AS
BEGIN
	DECLARE @iCount INT,
			@bReturn BIT

	SELECT @iCount = COUNT(*)
	FROM tbProfileAcquirerHistory WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID
		AND AcquirerName = @sAcquirerName

	IF (@iCount > 0)
		SET @bReturn = 1
	ELSE
		SET @bReturn = 0

	RETURN @bReturn
END