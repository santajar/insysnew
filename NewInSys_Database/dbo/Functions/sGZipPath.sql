﻿CREATE FUNCTION [dbo].[sGZipPath]()
	RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sZipPath VARCHAR(MAX)
	SELECT @sZipPath = Flag
	From tbControlFlag
	WHERE ItemName = 'GZIP Path'

	RETURN @sZipPath
END


