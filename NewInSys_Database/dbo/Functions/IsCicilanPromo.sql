﻿

CREATE FUNCTION [dbo].[IsCicilanPromo](@sTerminalID varchar(8))
	RETURNS BIT
BEGIN
DECLARE @iTotalAcquirer SMALLINT, @iReturn BIT
SELECT @iTotalAcquirer=COUNT(*) FROM tbProfileAcquirer WITH(NOLOCK)
WHERE TerminalID = @sTerminalID
	AND AcquirerName != 'PROMO A' 
	AND AcquirerName like 'PROMO%' 
	AND AcquirerTag='AA04' 
	AND ISNULL(AcquirerTagValue,'') <> ''
	AND LEN(AcquirerTagValue)=15
	AND AcquirerTagValue NOT LIKE '%yyy' 
	AND AcquirerTagValue NOT LIKE '%xxx'
IF @iTotalAcquirer > 0 
	SET @iReturn = 1
ELSE
	SET @iReturn = 0
RETURN @iReturn
END


