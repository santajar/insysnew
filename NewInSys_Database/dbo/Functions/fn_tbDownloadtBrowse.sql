﻿CREATE FUNCTION [dbo].[fn_tbDownloadtBrowse](@sTerminalId VARCHAR(8))
RETURNS @temp TABLE(
	ID INT,
	TerminalId VARCHAR(8),
	Cont VARCHAR(MAX),
	Flag BIT)
AS
BEGIN
	INSERT INTO @temp(ID,
		TerminalId,
		Cont,
		Flag)
	SELECT TOP(1) ID, 
		TerminalId,
		[Content],
		Flag
	FROM tbDownloadApp WITH (NOLOCK)
	WHERE TerminalId = @sTerminalId 
		AND Flag = 0

	RETURN
END
