﻿
CREATE FUNCTION [dbo].[sGenerateDownloadFiller](
	@sTerminalId VARCHAR(8),
	@iCounterApp INT
	)
RETURNS VARCHAR(200)
BEGIN
DECLARE @sExtraData VARCHAR(200),
	@sDownloadedAppName VARCHAR(150),
	@sDownloadedAppFilename VARCHAR(150),
	@sHexDownloadedAppName VARCHAR(300),
	@sHexDownloadedAppFilename VARCHAR(300),
	@iLenDownloadedAppName INT,
	@iLenDownloadedAppFilename INT,
	@sLenDownloadedAppName VARCHAR(4),
	@sLenDownloadedAppFilename VARCHAR(4),
	@sCounterApp VARCHAR(4),
	@iBuildNumber INT,
	@sBuildNumber VARCHAR(4),
	@iTotalIndex INT,
	@sTotalIndex VARCHAR(4)

	
SELECT @sDownloadedAppName = AppPackageName, @sDownloadedAppFilename = AppPackageFilename, @iBuildNumber=BuildNumber
FROM tbAppPackageEDCList app JOIN tbListTIDRemoteDownload terminal
ON app.AppPackId = terminal.AppPackId
WHERE TerminalID = @sTerminalId

SET @iLenDownloadedAppName = LEN(@sDownloadedAppName)
SET @iLenDownloadedAppFilename = LEN(@sDownloadedAppFilename)

SET @sLenDownloadedAppName = REPLICATE('0', 4-LEN(@iLenDownloadedAppName)) 
	+ CONVERT(VARCHAR,@iLenDownloadedAppName)
SET @sLenDownloadedAppFilename = REPLICATE('0', 4-LEN(@iLenDownloadedAppFilename)) 
	+ CONVERT(VARCHAR,@iLenDownloadedAppFilename)
SET @sCounterApp = REPLICATE('0', 4-LEN(@iCounterApp)) 
	+ CONVERT(VARCHAR,@iCounterApp)
SET @sBuildNumber = REPLICATE('0', 4-LEN(@iBuildNumber)) 
	+ CONVERT(VARCHAR,@iBuildNumber)

SET @sHexDownloadedAppName = dbo.sStringToHexString(@sDownloadedAppName)
SET @sHexDownloadedAppFilename = dbo.sStringToHexString(@sDownloadedAppFilename)

IF @iCounterApp = 0
BEGIN
	SELECT @iTotalIndex=COUNT(*)
	FROM tbAppPackageEDCListTemp app JOIN tbListTIDRemoteDownload terminal
	ON app.AppPackId = terminal.AppPackId
	WHERE TerminalID = @sTerminalId

	SET @sTotalIndex = REPLICATE('0', 4-LEN(@iTotalIndex)) 
	+ CONVERT(VARCHAR,@iTotalIndex)

	SET @sExtraData = @sCounterApp + @sTotalIndex
	+ @sLenDownloadedAppName + @sHexDownloadedAppName
	+ @sBuildNumber
	+ @sLenDownloadedAppFilename + @sHexDownloadedAppFilename
	+ REPLICATE('F',200 - 4 - 4 - LEN(@sHexDownloadedAppName) - 4 - LEN(@sHexDownloadedAppFilename))
END
ELSE
BEGIN
SET @sExtraData = @sCounterApp 
	+ @sLenDownloadedAppName + @sHexDownloadedAppName
	+ @sBuildNumber
	+ @sLenDownloadedAppFilename + @sHexDownloadedAppFilename
	+ REPLICATE('F',200 - 4 - 4 - LEN(@sHexDownloadedAppName) - 4 - LEN(@sHexDownloadedAppFilename))
END
RETURN (@sExtraData)
END	

