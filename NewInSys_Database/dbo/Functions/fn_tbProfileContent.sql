﻿/****** Object:  StoredProcedure [dbo].[spProfileContent]    Script Date: 09/15/2010 14:18:23 ******/
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 15, 2010
-- Modify date:
--				1. August 12, 2010, adding column TagLength to result table
-- Description:	Get The Terminal Content
-- =============================================
CREATE FUNCTION [dbo].[fn_tbProfileContent](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(100)
	)
BEGIN
	DECLARE @sSqlStmt VARCHAR(MAX)

	INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		TerminalId,
		Tag,
		ItemName,
		TagLength,
		TagValue
	FROM dbo.fn_viewTerminalDetail(@sTerminalID)

	INSERT INTO @tempTerminal(TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue 
		FROM dbo.fn_viewCardDetail(@sTerminalID)
		ORDER BY [Name], Tag

	INSERT INTO @tempTerminal(TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue 
	FROM dbo.fn_viewIssuerDetail(@sTerminalID)

	INSERT INTO @tempTerminal(TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue 
	FROM dbo.fn_viewAcquirerDetail(@sTerminalID)
	ORDER BY [Name], Tag

	INSERT INTO @tempTerminal(TerminalId,
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		Tag,
		'',
		TagLength,
		TagValue 
	FROM dbo.fn_viewRelationTLV(@sTerminalID)

	RETURN
END