﻿
-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <29 okt 2015>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[isInitMigrasi]
(
	@sTerminalID VARCHAR(8)
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @bReturn BIT

	-- Add the T-SQL statements to compute the return value here
	SELECT @bReturn = InitMigrasi
	FROM tbProfileTerminalList
	WHERE TerminalID = @sTerminalID

	-- Return the result of the function
	RETURN @bReturn

END