﻿
CREATE FUNCTION [dbo].[fn_tbProfileContentGPRS](  
 @sTerminalID VARCHAR(8),  
 @sConditions VARCHAR(MAX)  
 )  
RETURNS @tempTerminal TABLE(  
  TerminalId VARCHAR(8),  
  [Name] VARCHAR(15),  
  Tag VARCHAR(5),  
  ItemName VARCHAR(50),  
  TagLength VARCHAR(3),  
  TagValue VARCHAR(MAX)  
 )  
BEGIN  
INSERT INTO @tempTerminal (TerminalId,  
  [Name],  
  Tag,  
  ItemName,  
  TagLength,  
  TagValue )  
SELECT b.TerminalId,   
 a.GPRSName [Name],   
 a.GPRSTag Tag,  
 '' ItemName,   
 a.GPRSTagLength TagLength,  
 a.GPRSTagValue TagValue  
FROM tbProfileGPRS a WITH (NOLOCK) JOIN tbProfileTerminalList b WITH (NOLOCK)
ON a.DatabaseId=b.DatabaseId  
WHERE b.TerminalId=@sTerminalID  
RETURN  
END
