﻿CREATE FUNCTION	[dbo].[sRelationInsertValue] (
@sAcquirerValue VARCHAR(50),
@sIssuerValue VARCHAR(50),
@sCardValue VARCHAR(50),
@sTag VARCHAR(4)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @sValue VARCHAR(50)

	IF (@sTag LIKE 'AD03')
		SET @sValue = @sAcquirerValue
	IF (@sTag LIKE 'AD02')
		SET @sValue = @sIssuerValue
	ELSE IF (@sTag LIKE 'AD01')
		SET @sValue = @sCardValue
	RETURN @sValue
END
