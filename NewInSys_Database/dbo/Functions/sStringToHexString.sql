﻿CREATE FUNCTION [dbo].[sStringToHexString](@sValue VARCHAR(MAX))
RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sResult VARCHAR(MAX)
	SET @sResult=''
	DECLARE @Result TABLE(
		LoopId INT IDENTITY(1,1),
		ResultValue VARCHAR(MAX)
		)
	DECLARE @sTempResult VARCHAR(MAX)
	DECLARE @sTempValue VARCHAR(1000)
	DECLARE @iLoop INT,
		@iCount INT
	SET @iLoop = CEILING(CONVERT(DECIMAL(10,2),DATALENGTH(@sValue))/30)
	SET @iCount = 0
	WHILE @iCount<@iLoop
	BEGIN
		SET @sTempValue = SUBSTRING(@sValue,1+(@iCount*30),30)
	--PRINT '@sTempValue '+@sTempValue
		SET @sTempResult = UPPER(SUBSTRING(master.sys.fn_varbintohexstr(CONVERT(varbinary, @sTempValue)),
			3, 2*(DATALENGTH(@sTempValue)+1)))
	--PRINT '@sTempResult '+@sTempResult
		SET @sResult = @sResult + @sTempResult
	--PRINT '@sResult '+@sResult
		SET @iCount = @iCount + 1
		INSERT INTO @Result(ResultValue) VALUES(@sResult)
	END
	SELECT TOP 1 @sResult=ResultValue FROM @Result ORDER BY LoopId DESC
	
	RETURN (@sResult)
END