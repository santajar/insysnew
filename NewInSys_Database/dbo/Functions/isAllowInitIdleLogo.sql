﻿CREATE FUNCTION [dbo].[isAllowInitIdleLogo](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	RETURN(SELECT LogoIdle FROM tbProfileTerminalList WITH (NOLOCK)
		WHERE TerminalId=@sTerminalId)
END