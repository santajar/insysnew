﻿
CREATE FUNCTION [dbo].[fn_viewTerminalList]()
RETURNS @viewTerminalList TABLE(
	[TerminalID] VARCHAR(8) NOT NULL,
	[DatabaseID] SMALLINT NOT NULL,
	[AllowDownload] BIT NULL,
	[StatusMaster] BIT NULL,
	[LastView] DATETIME NULL,
	[EnableIP] BIT NULL,
	[IPAddress] VARCHAR(15) NULL,
	[AutoInitTimeStamp] DATETIME NULL,
	[LocationID] [bigint] NULL,
	[InitCompress] BIT NULL,
	[AppPackID] VARCHAR(150) NULL,
	[ScheduleID] INT NULL,
	[RemoteDownload] BIT NOT NULL,
	[DatabaseName] VARCHAR(25) NOT NULL,
	[NewInit] BIT NOT NULL,
	[EMVInit] BIT NOT NULL,
	[TLEInit] BIT NOT NULL,
	[LoyaltyProd] BIT NOT NULL,
	[GPRSInit] BIT NOT NULL
)
AS
BEGIN
	INSERT INTO @viewTerminalList
	SELECT A.TerminalID,
		A.DatabaseID,
		A.AllowDownload,
		A.StatusMaster,
		A.LastView,
		A.EnableIP,
		A.IPAddress,
		A.AutoInitTimeStamp,
		A.LocationID,
		A.InitCompress,
		A.AppPackID,
		A.ScheduleID,
		A.RemoteDownload,		
		DatabaseName,
		NewInit,
		b.EMVInit,
		TLEInit,
		LoyaltyProd,
		GPRSInit
	FROM tbProfileTerminalList A  WITH (NOLOCK) LEFT JOIN tbProfileTerminalDB B  WITH (NOLOCK)
	ON A.DatabaseID = B.DatabaseID
	RETURN
END
