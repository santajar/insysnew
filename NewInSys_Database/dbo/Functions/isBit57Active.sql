﻿CREATE FUNCTION [dbo].[isBit57Active] (@sHexBitMap VARCHAR(MAX))
	RETURNS BIT
BEGIN
	DECLARE @sBitMap varchar(64)
	--SET @sAppName = dbo.sGetApplicationName(@sAppName)

	SELECT top 1 @sBitMap = BitMapRD
	FROM tbBitMap
		WITH (NOLOCK)
	WHERE HexBitMapRD = @sHexBitMap

	RETURN SUBSTRING(@sBitMap, 57,1)
END