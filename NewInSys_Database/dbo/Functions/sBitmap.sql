﻿-- ====================================================================== --
-- Function Name : sBitmap
-- Parameter	 : 
--		1. 
-- Return		 :
--		ex.: sBitmap() --> '0810'
-- Description	 :
--		return string Flag from tbControlFlag where ItemName is HexBitMap
-- ====================================================================== --
CREATE FUNCTION [dbo].[sBitmap](@sAppName VARCHAR(MAX))
	RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sValue VARCHAR(MAX)
	
	SET @sAppName = dbo.sGetApplicationName(@sAppName)

	SELECT @sValue=HexBitMap 
	FROM tbBitMap
	WHERE ApplicationName=@sAppName 

	RETURN @sValue
END



