﻿
CREATE FUNCTION [dbo].[fn_tbProfileContentCardTypeManagement](  
	@sTerminalID VARCHAR(8),  
	@sConditions VARCHAR(MAX)  
)
RETURNS @tempTerminal TABLE(  
  TerminalId VARCHAR(8),  
  [Name] VARCHAR(15),  
  Tag VARCHAR(5),  
  ItemName VARCHAR(50),  
  TagLength VARCHAR(3),  
  TagValue VARCHAR(MAX)  
 )  
BEGIN  
INSERT INTO @tempTerminal (TerminalId,  
  [Name],  
  Tag,  
  ItemName,  
  TagLength,  
  TagValue )  
SELECT b.TerminalId,   
 a.CardTypeName [Name],   
 a.CardTypeTag Tag,  
 '' ItemName,   
 a.CardTypeTagLength TagLength,  
 a.CardTypeTagValue TagValue  
FROM tbProfileCardType a JOIN tbProfileTerminalList b  
ON a.DatabaseId=b.DatabaseId  
WHERE b.TerminalId=@sTerminalID 
RETURN  
END