﻿-- ====================================================================== --
-- Function Name : iLastInitId
-- Parameter	 : 
--		1. @sTerminalId, initialized terminalid
-- Return		 :
--		ex.: iLastInitId('12345678') --> 151
-- Description	 :
--		return int InitId which currently do initialize process
-- ====================================================================== --
CREATE FUNCTION [dbo].[iCurrentLoop](@sTerminalId VARCHAR(8))
RETURNS INT
BEGIN
	RETURN (SELECT COUNT(*) FROM tbInit 
		WHERE TerminalId=@sTerminalId AND Flag=1)
END
