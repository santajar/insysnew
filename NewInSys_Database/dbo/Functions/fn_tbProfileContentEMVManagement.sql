﻿CREATE FUNCTION [dbo].[fn_tbProfileContentEMVManagement](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(20),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(MAX)
	)
BEGIN
INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
SELECT b.TerminalId, 
	a.EMVManagementName [Name],
	a.EMVManagementTag Tag,
	'' ItemName, 
	a.EMVManagementTagLength TagLength,
	a.EMVManagementTagValue TagValue
FROM tbProfileEMVManagement a WITH (NOLOCK) JOIN tbProfileTerminalList b WITH (NOLOCK)
ON a.DatabaseId=b.DatabaseId
WHERE b.TerminalId=@sTerminalID
RETURN
END