﻿CREATE FUNCTION [dbo].[IsCustomGPRSInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iCustomGPRSInit BIT
	SELECT @iCustomGPRSInit=b.CustomGPRSInit
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @iCustomGPRSInit
END