﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 27, 2010
-- Description:	Create a temporary table holding Content of specified TerminalID stored in new row for each tag
-- =============================================
CREATE FUNCTION [dbo].[fn_tbTLV](
		@sContent VARCHAR(MAX))
	RETURNS @TempTable TABLE(
		RowId		INT IDENTITY(1,1),
		[Name]		VARCHAR(15),
		Tag			VARCHAR(5),
		TagLength		VARCHAR(3),
		TagValue		VARCHAR(500)
	)
AS
BEGIN
	DECLARE @sTag			VARCHAR(5)
	DECLARE @sLenValue		VARCHAR(3)
	DECLARE @sValue			VARCHAR(500)
	DECLARE @iIndex			INT
	DECLARE @iLenContent	INT
	DECLARE @iStartPos		INT

	DECLARE @sName			VARCHAR(15)

	--Create temporary table holding Content of specified TerminalID
	SET @iLenContent = DATALENGTH(@sContent)
	SET @iStartPos=1

	SET @iIndex=@iStartPos

	--Get all tags in Content, break each tag into one row and insert to TempTable
	WHILE @iIndex<=@iLenContent
		BEGIN
			SELECT @sTag=SUBSTRING(@sContent,@iIndex,4)
			SET @iIndex=@iIndex+4
			IF @sTag='AE37' OR @sTag='AE38'
				BEGIN
					SELECT @sLenValue=SUBSTRING(@sContent,@iIndex,3)
					SET @iIndex=@iIndex+3
				END
			ELSE
				BEGIN
					SELECT @sLenValue=SUBSTRING(@sContent,@iIndex,2)
					SET @iIndex=@iIndex+2
				END

			DECLARE @iLenValue INT
			SET @iLenValue=@sLenValue

			SELECT @sValue=SUBSTRING(@sContent,@iIndex,@iLenValue)
			SET @iIndex=@iIndex+@sLenValue

			DECLARE @sTempTag VARCHAR(2)
			
			IF( SUBSTRING(@sTag,3,2) = '01'AND @sTag <> 'DC01' )
				SET @sName = @sValue			
			IF (@sTag <>'"')
				INSERT INTO @TempTable([Name], Tag, TagLength, TagValue) VALUES(@sName,@sTag,@sLenValue,@sValue)
		END
	RETURN
END