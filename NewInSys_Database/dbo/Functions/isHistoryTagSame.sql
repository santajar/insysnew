﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[isHistoryTagSame]
(
	@sTerminalID VARCHAR(8),
	@sDate VARCHAR(20)
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @bReturn BIT = 0,
			@iCountTerminalHistoryTag INT,
			@iCountAcquirerHistoryTag INT,
			@iCountIssuerHistoryTag INT,
			@iCountRelationHistoryTag INT
	
	SELECT @iCountTerminalHistoryTag = COUNT (*)
	FROM tbProfileTerminalHistory WITH (NOLOCK)
	WHERE TerminalID = @sterminalID
		AND LastUpdate = @sDate
		AND TerminalTag NOT IN
			(SELECT Tag 
			FROM tbItemList WITH (NOLOCK)
			WHERE DatabaseID IN 
				(SELECT DatabaseID 
				FROM tbProfileTerminalList WITH (NOLOCK)
				WHERE TerminalID = @sTerminalID)
			AND FormID = 1)
		AND TerminalTag NOT LIKE 'DC%'


	SELECT @iCountAcquirerHistoryTag = COUNT(*)
	FROM tbProfileAcquirerHistory WITH (NOLOCK)
	WHERE TerminalID = @sterminalID
		AND LastUpdate = @sDate
		AND AcquirerTag NOT IN
			(SELECT Tag 
			FROM tbItemList WITH (NOLOCK)
			WHERE DatabaseID IN 
				(SELECT DatabaseID 
				FROM tbProfileTerminalList WITH (NOLOCK)
				WHERE TerminalID = @sTerminalID)
			AND FormID = 2)
	
	SELECT @iCountIssuerHistoryTag = COUNT(*)
	FROM tbProfileIssuerHistory WITH (NOLOCK)
	WHERE TerminalID = @sterminalID
		AND LastUpdate = @sDate
		AND IssuerTag NOT IN
			(SELECT Tag 
			FROM tbItemList WITH (NOLOCK)
			WHERE DatabaseID IN 
				(SELECT DatabaseID 
				FROM tbProfileTerminalList WITH (NOLOCK)
				WHERE TerminalID = @sTerminalID)
			AND FormID = 3)

	SELECT @iCountRelationHistoryTag = COUNT(*)
	FROM tbProfileRelationHistory WITH (NOLOCK)
	WHERE TerminalID = @sterminalID
		AND LastUpdate = @sDate
		AND RelationTag NOT IN
			(SELECT Tag 
			FROM tbItemList WITH (NOLOCK)
			WHERE DatabaseID IN 
				(SELECT DatabaseID 
				FROM tbProfileTerminalList WITH (NOLOCK)
				WHERE TerminalID = @sTerminalID)
			AND FormID = 5)

	IF (@iCountTerminalHistoryTag = 0 AND @iCountAcquirerHistoryTag = 0 AND @iCountIssuerHistoryTag = 0 AND @iCountRelationHistoryTag = 0)
		SET @bReturn = 1
	ELSE
		SET @bReturn = 0

	-- Return the result of the function
	RETURN @bReturn

END