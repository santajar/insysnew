﻿
CREATE FUNCTION [dbo].[IsInitAllowedbySN](@sSN VARCHAR(25))
RETURNS BIT
BEGIN
	RETURN(SELECT EnableDownload FROM tbTerminalSN
		WHERE SerialNumber=@sSN)
END