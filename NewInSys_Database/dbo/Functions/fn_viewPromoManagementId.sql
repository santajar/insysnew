﻿

CREATE FUNCTION [dbo].[fn_viewPromoManagementId]()  
RETURNS @viewPromoManagement TABLE(  
 PromoManagementID VARCHAR(8),  
 DatabaseId SMALLINT,  
 PromoManagementName VARCHAR(50)  
)  
AS  
BEGIN  
INSERT INTO @viewPromoManagement  
	SELECT  
		PromoManagementID = MIN(PromoManagementID), DatabaseID, PromoManagementName
	FROM  
		tbProfilePromoManagement WITH (NOLOCK)  
	GROUP BY DatabaseID, PromoManagementName
	ORDER BY PromoManagementID
RETURN  
END