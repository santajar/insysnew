﻿-- =============================================
-- Author:		<Author,,Tedie>
-- Create date: <Create Date, 20 January 2016>
-- Description:	<Description, Check History Terminal have data or not>
-- =============================================
Create FUNCTION [dbo].[isTerminalHistoryHaveData]
(
	@sTerminalID VARCHAR(8)
)
RETURNS BIT
AS
BEGIN
	DECLARE @iCount INT,
			@bReturn BIT

	SELECT @iCount = COUNT(*)
	FROM tbProfileTerminalHistory WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID

	IF (@iCount > 0)
		SET @bReturn = 1
	ELSE
		SET @bReturn = 0

	RETURN @bReturn
END