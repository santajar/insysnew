﻿CREATE FUNCTION [dbo].[isProcodeAllowed]( @sProcode VARCHAR(6))
	RETURNS BIT
BEGIN
	DECLARE @bProcodeAllowed BIT
	DECLARE @sProcodeFlag VARCHAR(6)

	SELECT @sProcodeFlag = Flag
	FROM tbControlFlag
	WITH (NOLOCK)
	WHERE ItemName = 'InitFlazzRequest'

	IF @sProcodeFlag = @sProcode
		SET @bProcodeAllowed = 1
	ELSE
	BEGIN
		SELECT @sProcodeFlag = Flag
		FROM tbControlFlag
		WITH (NOLOCK)
		WHERE ItemName = 'AutoInitStart'
		
		IF @sProcodeFlag = @sProcode
			SET @bProcodeAllowed = 1
		ELSE
			SET @bProcodeAllowed = 0
	END

	RETURN @bProcodeAllowed 
END