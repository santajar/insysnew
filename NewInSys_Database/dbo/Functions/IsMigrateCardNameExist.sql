﻿CREATE FUNCTION [dbo].[IsMigrateCardNameExist](@sCardName VARCHAR(30),
	@sDatabaseId VARCHAR(3))
RETURNS BIT
BEGIN
	RETURN(SELECT COUNT(*) 
		FROM tbProfileCardList 
		WHERE CardName=@sCardName AND DatabaseID=CAST(@sDatabaseId AS INT)
		)
END
