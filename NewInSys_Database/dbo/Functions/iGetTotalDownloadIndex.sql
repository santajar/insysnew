﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <3 Nov 2014>
-- Description:	<Get Total Index Download>
-- =============================================
CREATE FUNCTION [dbo].[iGetTotalDownloadIndex]
(
	@sTerminalID VARCHAR(8)
)
RETURNS INT
AS
BEGIN
	DECLARE @iTotalIndex INT

	-- Add the T-SQL statements to compute the return value here
	SELECT @iTotalIndex = COUNT(*)
	FROM tbAppPackageEDCListTemp app JOIN tbListTIDRemoteDownload terminal
	ON app.AppPackId = terminal.AppPackId
	WHERE TerminalID = @sTerminalId

	-- Return the result of the function
	RETURN @iTotalIndex

END