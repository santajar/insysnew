﻿CREATE FUNCTION [dbo].[sDatabaseNameByTerminalId](@sTerminalId VARCHAR(8))
RETURNS VARCHAR(30)
BEGIN
RETURN (SELECT DatabaseName FROM dbo.fn_viewTerminalList()
	WHERE TerminalId=@sTerminalId)
END