﻿CREATE FUNCTION [dbo].[IsDownloadStart](@sProcCode VARCHAR(6))
	RETURNS BIT
BEGIN
	DECLARE @bReturn BIT
	IF @sProcCode = '990000'
		SELECT @bReturn = 1
	ELSE
		SELECT @bReturn = 0
	RETURN (@bReturn)
END
