﻿-- ====================================================================== --
-- Function Name : sPadLeft
-- Parameter	 : 
--		1. @sValue --> Add String
--		2. @iAddCount --> how many characters to add
--		3. @sAddValue --> characters to add
-- Return		 :
--		ex.: sPadLeft('8',1,'0') --> '08'
-- Description	 :
--		return string with add some characters
-- ====================================================================== --
CREATE FUNCTION [dbo].[sPadLeft](@sValue VARCHAR(100), @iAddCount INT, @sAddValue VARCHAR(10))
	RETURNS VARCHAR(150)
AS
BEGIN
	RETURN (REPLICATE(@sAddValue,@iAddCount)+@sValue)
END
