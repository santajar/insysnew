﻿-- ============================================= 
-- Author:              <Ibnu Saefullah> 
-- Create date: <23 okt 2015> 
-- Description: <Check Existing Software Name> 
-- ============================================= 
CREATE FUNCTION [dbo].[isSoftwareNameAlreadyExist] 
(       
        @sSoftwareName VARCHAR(50) 
) 
RETURNS BIT 
AS 
BEGIN 
        DECLARE @bReturn BIT, 
                        @iCount INT 
        
        SELECT @iCount = COUNT(*) 
        FROM tbAppPackageEDCList WITH (NOLOCK) 
        WHERE AppPackageName = @sSoftwareName 
        IF @iCount>0 
                SET @bReturn = 1 
        ELSE 
                SET @bReturn = 0 
        
        RETURN @bReturn 
END 