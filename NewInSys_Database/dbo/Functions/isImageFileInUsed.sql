﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
Create FUNCTION [dbo].[isImageFileInUsed]
(
	@sFileName VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @iCount INT,
			@bReturn BIT

SELECT @iCount = COUNT(TerminalTagValue)
FROM tbProfileTerminal WITH (NOLOCK)
WHERE TerminalTag IN (
		SELECT Tag
		FROM tbItemList WITH (NOLOCK)
		WHERE ItemName IN ('Logo Print','Logo Idle'))
	AND TerminalTagValue = @sFileName

	IF @iCount > 0
		SET @bReturn = 1
	ELSE 
		SET @bReturn = 0
	
	RETURN @bReturn

END