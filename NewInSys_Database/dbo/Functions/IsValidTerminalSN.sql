﻿-- ====================================================================== --
-- Function Name : iIsValidTerminalSN
-- Created Date	 : Oct 31, 2013
-- Parameter	 : 
--		1. @sTerminalId, initialized terminalid
-- Return		 :
--		ex.: iIsValidTerminalId('12345678') --> 0
-- Description	 :
--		return boolean, 1 : valid TerminalId, 0 : invalid TerminalId
-- Modify		 :
--		1. 
-- ====================================================================== --
CREATE FUNCTION [dbo].[IsValidTerminalSN](@sTerminalSN VARCHAR(25))
RETURNS BIT
BEGIN
	RETURN (
		SELECT (CASE WHEN COUNT(*)>0 THEN 1	ELSE 0 END) 
		FROM tbTerminalSN
		WITH(NOLOCK)
		WHERE SerialNumber=@sTerminalSN)
END
