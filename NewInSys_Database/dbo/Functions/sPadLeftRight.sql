﻿
CREATE FUNCTION [dbo].[sPadLeftRight] (
					@sBaseChar VARCHAR(5),
					@iLen INT,
					@sValue VARCHAR(30),
					@isLeft BIT
					)
	RETURNS VARCHAR(1000)
BEGIN
	DECLARE @sTemp VARCHAR(1000)

	SET @sTemp = REPLICATE(@sBaseChar, @iLen)
	
	IF (@isLeft = 1) 		
		SET @sTemp = SUBSTRING (@sTemp, 1, @iLen - DATALENGTH(@sValue)) + @sValue
	ELSE
		SET @sTemp = @sValue + SUBSTRING (@sTemp, 1, @iLen - DATALENGTH(@sValue))
		
	RETURN @sTemp
	
END

