﻿CREATE FUNCTION [dbo].[IsCardTypeManagementInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iCardTypeInit BIT
	SELECT @iCardTypeInit=b.CardType
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDB b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @iCardTypeInit
END