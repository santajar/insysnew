﻿
CREATE FUNCTION [dbo].[iConvertNull]
(
	@ConditionColumn VARCHAR(MAX),
	@ResultNull INT,
	@ResultNotNull INT
)
RETURNS INT
AS
BEGIN
	DECLARE @ReturnValue INT

	IF (@ConditionColumn IS NULL)
	BEGIN
		SET @ReturnValue = @ResultNull
	END
	ELSE
		SET @ReturnValue = @ResultNotNull

	RETURN @ReturnValue

END

