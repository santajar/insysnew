﻿CREATE FUNCTION [dbo].[fn_viewCardId](@iCardID INT)
RETURNS @viewCardId TABLE(
	CardID VARCHAR(8),
	DatabaseId SMALLINT,
	CardName VARCHAR(50)
)
AS
BEGIN
	INSERT INTO @viewCardId
	SELECT
		CardID, 
		DatabaseID, 
		CardName
	FROM
		tbProfileCardList WITH (NOLOCK)
	WHERE CardID = @iCardID
RETURN
END