﻿CREATE FUNCTION [dbo].[fn_viewTerminalDetail](@sTerminalId VARCHAR(8))
RETURNS @viewTerminalDetail TABLE(
	[TerminalID] VARCHAR(8) NOT NULL,
	[Tag] VARCHAR(5) NOT NULL,
	[ItemName] VARCHAR(24) NOT NULL,
	[TagLength] INT NOT NULL,
	[LengthOfTagLength] INT NOT NULL,
	[TagValue] VARCHAR(150) NULL
)
AS
BEGIN
	INSERT INTO @viewTerminalDetail
	SELECT T.TerminalID, 
		T.TerminalTag AS Tag, 
		I.ItemName, 
		T.TerminalTagLength AS TagLength, 
		I.LengthofTagValueLength AS LengthOfTagLength, 
		--T.TerminalTagValue AS TagValue
		TagValue = 
			CASE
				WHEN I.vMasking = 1 THEN '****************'
				ELSE T.TerminalTagValue
			END
	FROM dbo.tbProfileTerminal AS T WITH (NOLOCK) INNER JOIN
		dbo.tbItemList AS I WITH (NOLOCK) ON T.TerminalTag = I.Tag INNER JOIN
        dbo.tbProfileTerminalList WITH (NOLOCK) ON T.TerminalID = dbo.tbProfileTerminalList.TerminalID 
        AND I.DatabaseID = dbo.tbProfileTerminalList.DatabaseID
    WHERE T.TerminalID=@sTerminalId
	RETURN
END