﻿
CREATE FUNCTION [dbo].[iInitPercentage] (
			@iTotalRows INT,
			@iProcessRows INT
								)
RETURNS INT
BEGIN
	DECLARE @iReturn INT
	IF @iTotalRows = 0
		SET @iReturn = 0
	ELSE
		SELECT @iReturn = (@iProcessRows * 100) /@iTotalRows
		
	RETURN @iReturn
	--RETURN (
	--		SELECT (@iProcessRows * 100) /@iTotalRows
	--	   )
END