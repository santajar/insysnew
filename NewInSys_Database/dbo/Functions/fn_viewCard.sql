﻿CREATE FUNCTION [dbo].[fn_viewCard](@iCardID INT)
RETURNS @viewCardDetail TABLE(
	CardID INT,
	DatabaseID SMALLINT,
	CardName VARCHAR(50),
	ItemName VARCHAR(24),
	CardTag VARCHAR(5),
	LengthOfTagValueLength INT,
	CardTagValue VARCHAR(150)
)
AS
BEGIN
INSERT INTO @viewCardDetail
SELECT 
	viewCardTLV.CardID, 
	viewCardId.DatabaseID, 
	viewCardId.CardName, 
	tbItemList.ItemName, 
	viewCardTLV.Tag AS CardTag, 
	tbItemList.LengthofTagValueLength, 
	viewCardTLV.TagValue AS CardTagValue
FROM
	(SELECT * FROM dbo.fn_viewCardTLV(@iCardID)) viewCardTLV INNER JOIN
	(SELECT * FROM dbo.fn_viewCardId(@iCardID)) viewCardId ON viewCardTLV.CardID = viewCardId.CardID INNER JOIN
	tbItemList ON viewCardId.DatabaseID = tbItemList.DatabaseID AND viewCardTLV.Tag = tbItemList.Tag
RETURN
END