﻿CREATE FUNCTION [dbo].[fn_tbAutoInitFlag] (@sInitTimeOut VARCHAR(50),
										@sMaxConn VARCHAR(50),
										@sInitTimeOutFlag VARCHAR(64),
										@sMaxConnFlag VARCHAR(64)
									   )
RETURNS @tbAutoInit TABLE (ItemName VARCHAR(50),
						   OldValue VARCHAR(64),
						   NewValue VARCHAR(64)
						  )
AS
BEGIN
	INSERT INTO @tbAutoInit (ItemName, OldValue, NewValue)
	SELECT ItemName, Flag, CASE ItemName
							WHEN @sInitTimeOut THEN @sInitTimeOutFlag
							WHEN @sMaxConn THEN @sMaxConnFlag
						   END
	FROM tbControlFlag WITH (NOLOCK)
	WHERE (ItemName = @sInitTimeOut AND Flag <> @sInitTimeOutFlag) OR
		  (ItemName = @sMaxConn AND Flag <> @sMaxConnFlag)
	RETURN
END 
