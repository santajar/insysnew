﻿CREATE FUNCTION [dbo].[isAllowInitPrintLogo](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	RETURN(SELECT LogoPrint FROM tbProfileTerminalList
		WHERE TerminalId=@sTerminalId)
END