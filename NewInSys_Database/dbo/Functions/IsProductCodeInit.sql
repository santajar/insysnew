﻿CREATE FUNCTION [dbo].[IsProductCodeInit]
(
	@sTerminalID VARCHAR(8)
)
RETURNS INT
AS
BEGIN
	DECLARE @ProductCodeInit BIT
	SELECT @ProductCodeInit=b.ProductCode
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDB b
	ON a.DatabaseID=b.DatabaseID
	WHERE a.TerminalId=@sTerminalId

	RETURN @ProductCodeInit
END
