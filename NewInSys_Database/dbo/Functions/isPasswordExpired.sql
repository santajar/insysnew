﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date, ,8 June 2015>
-- Description:	<Description, ,Validate Password Expired>
-- =============================================
CREATE FUNCTION [dbo].[isPasswordExpired]
(
	@sUserID VARCHAR(200)
)
RETURNS BIT
AS
BEGIN
	DECLARE @dtLastModifyPassword DATETIME,
			@iMaxExpiredDays INT,
			@bReturn BIT,
			@bRevoke BIT
	
	SELECT @dtLastModifyPassword = LastModifyPassword, @bRevoke = [Revoke]
	FROM tbUserLogin WITH (NOLOCK)
	WHERE UserID = @sUserID
	

	IF (@sUserID != 'INGENICO' AND @sUserID !='GEMALTO' AND @bRevoke !=1)
	BEGIN
		SET @iMaxExpiredDays = DBO.iGetPasswordLockedDays()
	
		IF DATEDIFF(DAY,@dtLastModifyPassword,GETDATE())>= @iMaxExpiredDays
			SET @bReturn = 1
		ELSE
			SET @bReturn = 0
	END
	ELSE
	BEGIN
		SET @bReturn = 0
	END
	
	RETURN @bReturn

END