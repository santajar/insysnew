﻿CREATE FUNCTION [dbo].[IsInitStart](@sProcCode VARCHAR(6))
	RETURNS BIT
BEGIN
	DECLARE @bReturn BIT
	IF @sProcCode = '930000' OR @sProcCode = '960000'
		SELECT @bReturn = 1
	ELSE
		SELECT @bReturn = 0
	RETURN (@bReturn)
END
