﻿
CREATE FUNCTION [dbo].[fn_tbProfileContentAID](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(20),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(MAX)
	)
BEGIN
INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
SELECT b.TerminalId, 
	a.AIDName [Name],
	a.AIDTag Tag,
	'' ItemName, 
	a.AIDTaglength TagLength,
	a.AIDTagValue TagValue
FROM tbProfileAID a WITH (NOLOCK) JOIN tbProfileTerminalList b WITH (NOLOCK)
ON a.DatabaseId=b.DatabaseId
WHERE b.TerminalId=@sTerminalID
--ORDER BY a.AIDTag
RETURN
END
