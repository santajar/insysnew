﻿-- =============================================
-- Author:		<Author,,Tedie Scorfia>
-- Create date: <Create Date, 13 March 2014,>
-- Description:	<Description, Get Tag Length from tbItemList,>
-- =============================================
Create FUNCTION [dbo].[isTagLength4](@iDBID INT)

RETURNS	INT
AS
BEGIN
	DECLARE @iCount INT, @isLenTag4 INT

	SELECT @iCount=COUNT(Tag) FROM tbItemList WHERE DatabaseID = @iDBID AND Tag='DE01'
	
	IF (@iCount=1)
		SET @isLenTag4 = 4
	else
		SET @isLenTag4 = 5

	RETURN @isLenTag4

END