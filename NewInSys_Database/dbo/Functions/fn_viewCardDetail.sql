﻿CREATE FUNCTION [dbo].[fn_viewCardDetail](@sTerminalID VARCHAR(8))
RETURNS @viewCardDetail TABLE(
	TerminalID VARCHAR(8),
	Name VARCHAR(50),
	Tag VARCHAR(5),
	ItemName VARCHAR(24),
	TagLength INT,
	LengthOfTagLength INT,
	TagValue VARCHAR(150)
)
AS
BEGIN
DECLARE @iDatabaseID INT
SELECT @iDatabaseID=DatabaseID FROM tbProfileTerminalList 
WHERE TerminalID=@sTerminalID

INSERT INTO @viewCardDetail
SELECT 
	@sTerminalID, 
	vwCardId.CardName AS Name, 
	vwCardTLV.Tag, 
	ItemList.ItemName, 
	vwCardTLV.TagLength, 
	ItemList.LengthofTagValueLength AS LengthOfTagLength, 
	--vwCardTLV.TagValue
	TagValue = 
		CASE
			WHEN ItemList.vMasking = 1 THEN '****************'
			ELSE vwCardTLV.TagValue
		END
FROM
	(SELECT * FROM tbProfileCardList WITH (NOLOCK) WHERE DatabaseID=@iDatabaseID) vwCardId INNER JOIN
	(
		SELECT
			CardID, 
			CardTag AS Tag, 
			CardTagLength AS TagLength, 
			CardTagValue AS TagValue
		FROM
			tbProfileCard WITH (NOLOCK)
		WHERE CardID IN 
			(
				SELECT CardID
				FROM tbProfileCardList WITH (NOLOCK) 
				WHERE DatabaseID=@iDatabaseID
			)
	) vwCardTLV ON vwCardId.CardID = vwCardTLV.CardID INNER JOIN
	(SELECT * FROM tbItemList WITH (NOLOCK) WHERE DatabaseID=@iDatabaseID) ItemList ON vwCardTLV.Tag = ItemList.Tag
ORDER BY Name,vwCardTLV.Tag
RETURN
END