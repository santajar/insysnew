﻿CREATE FUNCTION [dbo].[IsInitialFlazzManagementInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iInitialFlazzInit BIT
	SELECT @iInitialFlazzInit=b.InitialFlazzManagement
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDB b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @iInitialFlazzInit
END