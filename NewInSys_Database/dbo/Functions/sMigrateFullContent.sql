﻿CREATE FUNCTION [dbo].[sMigrateFullContent](@sTerminalId VARCHAR(8))
RETURNS VARCHAR(MAX)
BEGIN
	RETURN (
		SELECT [Content]
		FROM dbtms..TerminalList
		WHERE TerminalName=@sTerminalId
		)
END