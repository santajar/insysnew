﻿-- =============================================
-- Author:		<Author: Tedie Scorfia>
-- Create date: <Create Date: 2 juli 2015>
-- Description:	<Description: Get Max Length Password>
-- =============================================
CREATE FUNCTION [dbo].[iGetTotalPasswordHistory]()
RETURNS INT
AS
BEGIN
	RETURN (SELECT CONVERT(INT,Flag)
			FROM tbControlFlag 
			WHERE ItemName='TotalPasswordHistory')

END