﻿CREATE FUNCTION [dbo].[sTPDU]()
	RETURNS varchar(max)
BEGIN
	RETURN (SELECT Flag 
		FROM tbControlflag 
		WITH (NOLOCK)
		WHERE ItemName='TPDU')
END