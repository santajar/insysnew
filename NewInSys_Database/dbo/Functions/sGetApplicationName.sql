﻿
CREATE FUNCTION [dbo].[sGetApplicationName] (@sAppName VARCHAR(25))
	RETURNS VARCHAR(50)
BEGIN
	DECLARE @iCount INT
	
	IF @sAppName IS NULL
		SET @sAppName = 'HexBitMap'
	ELSE	
	BEGIN
		SELECT @iCount = COUNT(ApplicationName)
		FROM tbBitMap
		WITH (NOLOCK)
		WHERE ApplicationName=@sAppName

		IF @iCount != 1
			SET @sAppName = 'HexBitMapStandard'
	END

	RETURN @sAppName
END


