﻿

CREATE FUNCTION [dbo].[IsGPRSInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @GPRSInit BIT
	SELECT @GPRSInit=b.GPRSInit
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @GPRSInit
END


