﻿CREATE FUNCTION [dbo].[fn_tbProfileRelationInsert]
(	
	@sTerminalID VARCHAR(8),
	@sAcquirerValue VARCHAR(50),
	@sIssuerValue VARCHAR(50),
	@sCardValue VARCHAR(50),
	@sTag VARCHAR(5)
)
RETURNS @tempTable TABLE 
(
	ID INT IDENTITY(1,1),
	TagID INT
)
AS
BEGIN
	DECLARE @sSqlStmt VARCHAR(MAX)
	DECLARE @sSqlCond VARCHAR(MAX)
	
	SET @sSqlCond = dbo.sRelationInsertCondition(@sTerminalID,@sAcquirerValue,@sIssuerValue,@sTag)
	SET  @sSqlStmt = 'INSERT INTO #TempRelation (TagID)
		SELECT RelationTagID FROM tbProfileRelation WITH (NOLOCK) '+@sSqlCond
	
--	EXEC(@sSqlStmt)
	RETURN
END