﻿CREATE FUNCTION [dbo].[IsTerminalIdExist](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	RETURN (SELECT COUNT(*) 
		FROM tbProfileTerminalList
		WHERE TerminalId=@sTerminalId)
END