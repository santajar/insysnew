﻿CREATE FUNCTION [dbo].[sAllowNull](@vIsNothing VARCHAR(1))
RETURNS VARCHAR(1)
BEGIN
	DECLARE @vAllowNull VARCHAR(1)
	IF @vIsNothing=1
		SET @vAllowNull='0'
	ELSE
		SET @vAllowNull='1'
	RETURN @vAllowNull
END