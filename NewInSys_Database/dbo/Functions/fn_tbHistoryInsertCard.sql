﻿
CREATE FUNCTION [dbo].[fn_tbHistoryInsertCard]
(
		@sDatabaseID VARCHAR(3),
		@sCardName VARCHAR(50),
		@iCardID INT
)
	RETURNS @TempTable TABLE
(
		ItemName VARCHAR(MAX),
		TagValue VARCHAR(MAX)
)
AS
BEGIN
	INSERT INTO @TempTable
	SELECT
		T.ItemName,
		dbo.Bit2Str(@sDatabaseID, T.CardTag, T.CardTagValue)
	FROM dbo.fn_viewCard(@iCardID) T
	WHERE T.DatabaseId = @sDatabaseID AND T.CardId = @iCardID 
	RETURN
END


