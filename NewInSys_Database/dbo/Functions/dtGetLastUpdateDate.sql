﻿CREATE FUNCTION [dbo].[dtGetLastUpdateDate](@sDate VARCHAR(50))
RETURNS DATETIME
AS
BEGIN
DECLARE @sDateTemp VARCHAR(20),
       @dtDateTemp DATETIME,
       @sTemp VARCHAR(2)
       
IF ISNULL(@sDate,'') <> ''
       IF LEN(@sDate) <= 12
       BEGIN
              SET @sTemp = SUBSTRING(@sDate,1,2)
              SET @sDateTemp = SUBSTRING(@sDate,1,2) + '/'  --month
                     + SUBSTRING(@sDate,3,2) + '/' --date
                     + SUBSTRING(@sDate,5,2) + ' ' --year
                     + SUBSTRING(@sDate,7,2) + ':'
                     + SUBSTRING(@sDate,9,2) + ':'
                     + SUBSTRING(@sDate,11,2)
              
              IF CONVERT(INT, @sTemp) < 13 SET @dtDateTemp = CONVERT(DATETIME,@sDateTemp,1)
              ELSE SET @dtDateTemp = CONVERT(DATETIME,@sDateTemp,3)
       END
       ELSE
              SET @dtDateTemp = CONVERT(DATETIME,@sDate,103)
ELSE
       SET @sDateTemp = ''

RETURN @dtDateTemp
END

