﻿

CREATE FUNCTION [dbo].[IsPromoManagementInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iPromoManagementInit BIT
	SELECT @iPromoManagementInit= PromoManagement
	FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE TerminalId=@sTerminalId

	RETURN @iPromoManagementInit
END


	--DECLARE @isRemoteDownload BIT

	----SET @sAppName = dbo.sGetApplicationName(@sAppName)

	--SELECT @isRemoteDownload = RemoteDownload
	--FROM tbProfileTerminalList
	--	WITH (NOLOCK)
	--WHERE TerminalID = @sTerminalID

	--RETURN @isRemoteDownload