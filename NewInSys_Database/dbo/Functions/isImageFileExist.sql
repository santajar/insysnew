﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[isImageFileExist]
(
	@sFileName VARCHAR(50)
	
)
RETURNS BIT
AS
BEGIN
	DECLARE @iCount INT,
			@bReturn BIT

	SELECT @iCount = COUNT(*)
	FROM tbProfileImage WITH (NOLOCK)
	WHERE [FileName] = @sFileName

	IF @iCount > 0
		SET @bReturn = 1
	ELSE 
		SET @bReturn = 0
	
	RETURN @bReturn

END