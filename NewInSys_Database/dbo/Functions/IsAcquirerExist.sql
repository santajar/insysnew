﻿

CREATE FUNCTION [dbo].[IsAcquirerExist](@sTerminalId VARCHAR(8), @sAcquirerName VARCHAR(25))
RETURNS BIT
BEGIN
	RETURN (SELECT COUNT(*) 
		FROM tbProfileAcquirer
		WHERE TerminalId=@sTerminalId 
		AND AcquirerName=@sAcquirerName
		AND AcquirerTag='AA01')
END
