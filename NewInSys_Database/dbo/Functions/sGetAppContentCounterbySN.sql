﻿
CREATE FUNCTION [dbo].[sGetAppContentCounterbySN] (@sSN VARCHAR(25), @iCounter INT)
	RETURNS VARCHAR(MAX)
BEGIN
DECLARE @sContent VARCHAR(MAX)
SELECT @sContent = applist.AppPackageContent
FROM tbAppPackageEdcList app JOIN 
	tbAppPackageEDCListTemp applist ON app.AppPackageName = applist.AppPackageName JOIN 
	tbTerminalSN list ON app.AppPackId = list.AppPackId
WHERE list.SerialNumber = @sSN AND applist.Id = @iCounter

RETURN @sContent
END