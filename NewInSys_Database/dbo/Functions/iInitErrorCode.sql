﻿CREATE FUNCTION [dbo].[iInitErrorCode](@sTerminalId VARCHAR(MAX),
	@iCurrInitId INT)
	RETURNS INT
BEGIN
	DECLARE @iErrorCode INT
	SET @iErrorCode=0

	--DECLARE @iLastInitId INT
	--SET @iLastInitId = dbo.iLastInitId(@sTerminalId)

	IF @iCurrInitId=dbo.iLastInitId(@sTerminalId)
		SET @iErrorCode=1

	RETURN (@iErrorCode)
END