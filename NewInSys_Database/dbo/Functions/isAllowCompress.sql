﻿CREATE FUNCTION [dbo].[isAllowCompress] (@sAppName VARCHAR(25))
	RETURNS BIT
BEGIN
	DECLARE @isAllowCompress BIT

	--SET @sAppName = dbo.sGetApplicationName(@sAppName)

	SELECT @isAllowCompress = AllowCompress
	FROM tbBitMap
		WITH (NOLOCK)
	WHERE ApplicationName = dbo.sGetApplicationName(@sAppName)

	RETURN @isAllowCompress
END