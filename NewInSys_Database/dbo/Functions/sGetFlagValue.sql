﻿CREATE FUNCTION [dbo].[sGetFlagValue] (@sItemName VARCHAR(50))
	RETURNS VARCHAR(100)
BEGIN
	DECLARE @sFlagValue VARCHAR(100)
	SELECT @sFlagValue = Flag 
	FROM tbControlFlag
	WITH (NOLOCK)
	WHERE ItemName = @sItemName 
	
	RETURN @sFlagValue 
END