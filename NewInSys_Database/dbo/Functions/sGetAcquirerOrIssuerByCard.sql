﻿
CREATE FUNCTION [dbo].[sGetAcquirerOrIssuerByCard]
(
	@sTerminalID VARCHAR(8),
	@sCardName VARCHAR(50),
	@sTag VARCHAR(2)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @iDelta SMALLINT,
			@sFullTag VARCHAR(5)
	IF (DBO.isTagLength4(DBO.iDatabaseIdByTerminalId(@sTerminalID))=5)
		SET @sFullTag = 'AD001'
	ELSE
		SET @sFullTag = 'AD01'
	
	IF (@sTag = 'AA')
	BEGIN
		SET @iDelta = 2
	END
	ELSE IF (@sTag = 'AE')
	BEGIN
		SET @iDelta = 1
	END
	ELSE
	BEGIN
		-- Return the result of the function
		RETURN ''
	END

	-- Return the result of the function
	RETURN
		(SELECT RelationTagValue FROM tbProfileRelation WHERE RelationTagID =
			(SELECT RelationTagID + @iDelta FROM tbProfileRelation
			WHERE TerminalID = @sTerminalID AND RelationTag = @sFullTag AND RelationTagValue = @sCardName))
END
