﻿CREATE FUNCTION [dbo].[iAllowUpdateIntLogConnDetail] (
			@sTerminalID VARCHAR(8),
			@sTag VARCHAR(5))
RETURNS INT
BEGIN
DECLARE @iUpdate INT
DECLARE @iPercentage INT
DECLARE @iPercentageDetail INT

SELECT @iPercentage = Percentage 
FROM tbInitLogConnDetail WITH (NOLOCK)
WHERE TerminalID = @sTerminalID

SELECT @iPercentageDetail = Percentage 
FROM tbInitLogConnDetail WITH (NOLOCK)
WHERE TerminalID = @sTerminalID AND Tag = @sTag
	IF (@iPercentage < 100)
		IF (@iPercentageDetail < 100)
			SET @iUpdate = 1
		ELSE 
			SET @iUpdate = 0
	ELSE 
		SET @iUpdate = 1
RETURN @iUpdate
END