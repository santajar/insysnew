﻿
-- ====================================================================== --
-- Function Name : sProcCode
-- Parameter	 : 
--		1. 
-- Return		 :
--		ex.: sBitmap() --> '0810'
-- Description	 :
--		return string Flag from tbControlFlag where ItemName is HexBitMap
-- ====================================================================== --
CREATE FUNCTION [dbo].[sProcCode](@sTerminalID VARCHAR(8),
	@sProcCodeRecv VARCHAR(6),
	@iInitIdCurr INT = NULL
	)
	RETURNS VARCHAR(6)
BEGIN
	DECLARE @sTemp VARCHAR(6)
	DECLARE @sItemName VARCHAR(50)

	-- get the last InitId
	DECLARE @iInitIdLast INT

	IF @sProcCodeRecv = dbo.sGetFlagValue('ProcCodeStart') --'930000'
	BEGIN
		SET @sTemp = dbo.sGetFlagValue('ProcCodeContinue')
	END
	ELSE IF @sProcCodeRecv = '940000'
--dbo.sGetFlagValue('InitFlazzStartEnd') --
	BEGIN
		SET @sTemp = '940000'
		--SELECT @sTemp = dbo.sGetFlagValue('InitFlazzStartEnd')
	END
	ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('AutoInitStart') -- '950000'
	BEGIN
		SET @sTemp = dbo.sGetFlagValue('AutoInitRequest')
	END
	ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('AutoInitPhase2StartEnd') --'960000'
	BEGIN
		SET @sTemp = dbo.sGetFlagValue('AutoInitPhase2Continue')
	END
	ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('AutoInitEnd')  --'970000'
	BEGIN
		SET @sTemp = dbo.sGetFlagValue('AutoInitComplete')
	END
	ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('DownloadScheduleStartEnd')  --'990010'
	BEGIN
		SET @sTemp = dbo.sGetFlagValue('DownloadScheduleStartEnd')
	END
	ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('InstallmessageStartEnd')  --'990010'
	BEGIN
		SET @sTemp = dbo.sGetFlagValue('InstallmessageStartEnd')
	END
	ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('DownloadAppStartEnd')  --'990000'
	BEGIN
		SET @sTemp = dbo.sGetFlagValue('DownloadAppContinue')
	END
	ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('DownloadAppContinue') -- '990001'
	BEGIN
		--SELECT @iInitIdCurr = dbo.iCurrentDownloadLoop(@sTerminalID)
		SELECT @iInitIdLast = dbo.iTotalDownloadLoop(@sTerminalID)
		
		IF @iInitIdCurr < @iInitIdLast
			SET @sItemName = 'DownloadAppContinue'
		ELSE IF @iInitIdCurr=@iInitIdLast
			SET @sItemName = 'DownloadAppStartEnd'

		SELECT @sTemp=Flag FROM tbControlflag 
			WHERE ItemName=@sItemName
	END
	ELSE
	BEGIN
		SELECT @iInitIdCurr = dbo.iCurrentLoop(@sTerminalID)

		SELECT @iInitIdLast = dbo.iTotalLoop(@sTerminalID)
		-- if not the last, send the continue proccode
		IF @iInitIdCurr<@iInitIdLast
		BEGIN
			IF @sProcCodeRecv = dbo.sGetFlagValue('ProcCodeContinue') --'930001' 
				SET @sItemName = 'ProcCodeContinue'
			ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('AutoInitPhase2Continue') --'960001'
				SET @sItemName = 'AutoInitPhase2Continue'
--			ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('DownloadAppContinue') --'990001'
--				SET @sItemName = 'DownloadAppContinue'
--			SET @sTemp = @sItemName
			SELECT @sTemp=Flag FROM tbControlflag 
				WHERE ItemName=@sItemName
		END
		-- if the last, send the end init proccode
		ELSE IF @iInitIdCurr=@iInitIdLast
		BEGIN
			IF @sProcCodeRecv = dbo.sGetFlagValue('ProcCodeContinue') --'930001' 
				SET @sItemName = 'ProcCodeEnd'
			ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('AutoInitPhase2Continue') --'960001'
				SET @sItemName = 'AutoInitPhase2StartEnd'
----			ELSE IF @sProcCodeRecv = dbo.sGetFlagValue('DownloadAppContinue') --'990001'
----				SET @sItemName = 'DownloadAppStartEnd'

			SELECT @sTemp=Flag FROM tbControlflag 
				WHERE ItemName=@sItemName
		END
	END

	RETURN @sTemp
END
