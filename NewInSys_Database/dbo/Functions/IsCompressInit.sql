﻿

CREATE FUNCTION [dbo].[IsCompressInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iCompressInit BIT
	IF NOT EXISTS ( SELECT * FROM SYS.COLUMNS
				WHERE Name = N'InitCompress' AND
				Object_ID = Object_ID (N'tbProfileTerminalList')
			  )
		SET @iCompressInit = 0
	ELSE
	BEGIN
		SELECT @iCompressInit = ISNULL(InitCompress,0) 
		FROM tbProfileTerminalList
		WITH (NOLOCK)
		WHERE TerminalID = @sTerminalId
	END
	RETURN @iCompressInit
END






