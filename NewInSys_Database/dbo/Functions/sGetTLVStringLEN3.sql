﻿



-- ====================================================================== --
-- Function Name : sGetTLVString
-- Parameter	 : 
--		1. @sTag --> Tag String, ex.: DE01, AA01, etc.
--		2. @@sValue --> Tag value string, ex.: XRETFLZ1
-- Return		 :
--		ex.: sGetTLVString('de01','xretflz1') --> 'DE0108XRETFLZ1'
-- Modify		 :
--		1. Sept 20, 2010. modify the length format will be added 0 if the tag value
--		   length is less than 10
-- Description	 :
--		return tag-length-value string
-- ====================================================================== --
CREATE FUNCTION [dbo].[sGetTLVStringLEN3](@sTag VARCHAR(5), @sValue VARCHAR(500))
	RETURNS VARCHAR(800)
AS
BEGIN
	DECLARE @sTemp VARCHAR(800)
	DECLARE @sLen VARCHAR(3)
	IF @sValue IS NOT NULL
	BEGIN
		SET @sLen=DATALENGTH(@sValue)
		IF (LEN(@sLen)=1 OR LEN(@sLen)=2)
			SELECT @sTemp=RIGHT(dbo.sPadLeft(@sLen,5,'0'),3)
		ELSE
			SET @sTemp = @sLen

		SET @sTemp = UPPER(@sTag) + @sTemp + (@sValue)
	END
	ELSE
		SET @sTemp = UPPER(@sTag) + '000'
	RETURN @sTemp
END