﻿CREATE FUNCTION [dbo].[isLdapEnable]()
RETURNS BIT
BEGIN
	DECLARE @isStatus BIT
	SELECT @isStatus=Flag
	FROM tbControlFlag 
	WHERE ItemName='isLdapEnable'

	RETURN @isStatus
END