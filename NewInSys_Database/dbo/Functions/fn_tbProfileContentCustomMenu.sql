﻿/****** Object:  StoredProcedure [dbo].[spProfileContent]    Script Date: 09/15/2010 14:18:23 ******/
-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Sept 3, 2015
-- Modify date:
--				
-- Description:	Get The Custom Menu Content
-- =============================================
CREATE FUNCTION [dbo].[fn_tbProfileContentCustomMenu](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(100)
	)
BEGIN
	INSERT INTO @tempTerminal (TerminalId,
		--[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT CM.TerminalID
		,CM.CustomMenuTag
		,'' ItemName
		,CM.CustomMenuTagLength
		,CM.CustomMenuTagValue 
	FROM tbProfileCustomMenu CM WITH(NOLOCK) 
		JOIN tbProfileTerminalList TL  WITH(NOLOCK) ON CM.TerminalID = TL.TerminalID
	WHERE TL.TerminalID = @sTerminalID

	RETURN
END