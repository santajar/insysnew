﻿CREATE FUNCTION [dbo].[fn_tbBitMap] (@sApplicationName VARCHAR(25))
RETURNS @tbBitMap TABLE ( ColumnName VARCHAR(50),
						  ColumnValue VARCHAR(100)
						)
AS
BEGIN
	INSERT INTO @tbBitMap(ColumnName, ColumnValue)
	SELECT [Name], CASE [Name]
				WHEN 'ApplicationName' THEN ApplicationName
				WHEN 'AllowCompress' THEN dbo.sBit2Str(AllowCompress) 
				WHEN 'BitMap' THEN BitMap 
				WHEN 'HexBitMap' THEN HexBitMap END ColumnValue
	FROM (SELECT [Name] 
		  FROM SYSCOLUMNS 
		  WHERE ID In (SELECT ID 
					   FROM SYSOBJECTS 
					   WHERE [Name] = 'tbBitMap'
					  )
		 ) a, tbBitMap WITH (NOLOCK)
	WHERE ApplicationName = @sApplicationName
	RETURN
END

