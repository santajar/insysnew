﻿CREATE FUNCTION [dbo].[sGetAppFilename] (@sTerminalID VARCHAR(8))
	RETURNS VARCHAR(MAX)
BEGIN
DECLARE @sAppFilename VARCHAR(MAX)
SELECT @sAppFilename = app.AppPackageFilename
FROM tbAppPackageEdcList app LEFT JOIN 
	tbListTIDRemoteDownload list ON app.AppPackId = list.AppPackID
WHERE list.TerminalID = @sTerminalID

RETURN @sAppFilename
END

