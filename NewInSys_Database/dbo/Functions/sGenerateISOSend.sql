﻿
CREATE FUNCTION [dbo].[sGenerateISOSend](
	@sTerminalId VARCHAR(8),
	@sProcCodeRecv VARCHAR(6),
	@sNII VARCHAR(4),
	@sAdress VARCHAR(4),
	@sBit11 VARCHAR(6)=NULL,
	@sBit48 VARCHAR(MAX)=NULL,
	@sBit58 VARCHAR(10) = NULL,
	@sTag VARCHAR(5) = NULL,
	@sBit60 VARCHAR(MAX),
	@sAppName VARCHAR(MAX))
RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sISO VARCHAR(MAX), 
		@sBit12 VARCHAR(6), 
		@sBit13 VARCHAR(6)

	SET @sISO = dbo.sTPDU()
			+ @sAdress
			+ @sNII

	IF @sBit11 IS NOT NULL
	BEGIN
		SET @sISO = @sISO + dbo.sMTIResponse()
		-- BitMap
		IF @sBit48 IS NULL
			IF @sAppName = 'BCAInitFlazz'
				SET @sISO = @sISO 
					+ dbo.sBitMap('BCAInitFlazz')
			ELSE
				IF (dbo.isRemoteDownload(@sTerminalId) =1)
					SET @sISO = @sISO 
						+ dbo.sBitmapRD(NULL)
				ELSE
					SET @sISO = @sISO 
						+ dbo.sBitMap(NULL)
		ELSE
		BEGIN
			SET @sISO = @sISO 
				+ dbo.sBitMap(@sAppName)
			SELECT @sBit12 = dbo.sGetTimeHHMMSS()
			if @sProcCodeRecv= '990000' or @sProcCodeRecv = '990001' --proccode untuk download software
			begin
			SELECT @sBit13 = dbo.sGetDateMMDD()
			end
			else
			begin
			SELECT @sBit13 = dbo.sGetDateMMDDYY()
			end
		END

		-- Bit3
		SET @sISO = @sISO 
				+ dbo.sProcCode(@sTerminalId, @sProcCodeRecv, default)

		-- Bit11,12,13,24,39,41,48
		IF @sBit48 IS NOT NULL
		BEGIN
			SET @sISO = @sISO 
					+ @sBit11
					+ @sBit12
					+ @sBit13
					+ @sNII
					+ dbo.sStringToHexString('00')
					+ dbo.sStringToHexString(@sTerminalId)
					+ @sBit48
		END
		ELSE
		BEGIN
			SET @sISO = @sISO 
					+ @sBit11
					+ @sNII
					+ dbo.sStringToHexString('00')
					+ dbo.sStringToHexString(@sTerminalId)
		END

		--BIT 57
		IF dbo.isBit57Active(SUBSTRING(@sISO,15,16)) = 1
		BEGIN
			IF dbo.isRemoteDownload(@sTerminalId)=1
				SET @sISO = @sISO+'0001'+ dbo.sStringToHexString('1')
			ELSE
				SET @sISO = @sISO+'0001' +dbo.sStringToHexString('0')
		END

		-- BIT 58 
		IF dbo.isAllowCompress(@sAppName) = 1
			SET @sISO = @sISO + ISNULL(@sBit58,'')

		-- Bit60
		IF (SUBSTRING(@sBit60,1,4) = dbo.sStringToHexString('PK') OR @sTag = 'PK')
		BEGIN			
			SET @sISO = @sISO
				+ dbo.sPadLeft(LEN(@sBit60) / 2,4-LEN(LEN(@sBit60) / 2),'0')
			SET @sISO = @sISO 
				+ @sBit60
		END
		ELSE
		BEGIN
			IF ISNULL(@sBit60,'') <> ''
			BEGIN
				IF dbo.isAllowCompress (@sAppName) = 1 AND 
				   dbo.IsCompressInit(@sTerminalID)=1 
				BEGIN
					SET @sISO = @sISO + dbo.sPadLeft(LEN(@sBit60) / 2,4-LEN(LEN(@sBit60) / 2),'0')
					SET @sISO = @sISO + @sBit60
				END
				ELSE
				BEGIN	
					SET @sISO = @sISO + dbo.sPadLeft(LEN(@sBit60),4-LEN(LEN(@sBit60)),'0')
					SET @sISO = @sISO + dbo.sStringToHexString(@sBit60)
				END
			END
		END
	END
	ELSE
	BEGIN
		IF ISNULL(@sBit60,'') <> ''
			SET @sISO = @sISO + dbo.sStringToHexString(@sBit60)
	END
	
	RETURN (@sISO)
END