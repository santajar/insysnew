﻿CREATE FUNCTION [dbo].[sBrDatabaseNameById](@sDatabaseId VARCHAR(3))
RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sDatabaseName VARCHAR(MAX)
	SELECT @sDatabaseName=DatabaseName 
	FROM dbtms..BrDatabase 
	WHERE DatabaseId=@sDatabaseId
	RETURN @sDatabaseName
END