﻿-- ====================================================================== --
-- Function Name : sBitmap
-- Parameter	 : 
--		1. 
-- Return		 :
--		ex.: sBitmap() --> '0810'
-- Description	 :
--		return string Flag from tbControlFlag where ItemName is HexBitMapRD
-- ====================================================================== --
CREATE FUNCTION [dbo].[sBitmapRD](@sAppName VARCHAR(MAX))
	RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sValue VARCHAR(MAX)
	
	SET @sAppName = dbo.sGetApplicationName(@sAppName)

	SELECT @sValue=HexBitMapRD
	FROM tbBitMap
	WHERE ApplicationName=@sAppName 

	RETURN @sValue
END