﻿CREATE FUNCTION [dbo].[sOthProcCodeMessage](@sSessionTime VARCHAR(MAX))
RETURNS VARCHAR(6)
BEGIN
DECLARE @sProcCode VARCHAR(6)
DECLARE @sSqlStmt VARCHAR(MAX)
SET @sSqlStmt = 'SELECT @sProcCode=BitValue AS VALUE FROM ##tempBitMap' + @sSessionTime + 
	' WHERE BitId=3 '
exec sp_executesql @sSqlStmt
RETURN (@sProcCode)
END
