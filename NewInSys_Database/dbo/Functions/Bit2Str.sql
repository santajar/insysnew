﻿
CREATE FUNCTION [dbo].[Bit2Str]
(
	@iDatabaseID INT,
	@sTag VARCHAR(5),
	@sBit VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	IF (SELECT DISTINCT ObjectID FROM tbItemList WHERE Tag = @sTag AND DatabaseID = @iDatabaseID) = 3
	BEGIN
		IF(@sBit = '0')
		BEGIN
			RETURN 'FALSE'
		END
		IF(@sBit = '1')
		BEGIN
			RETURN 'TRUE'
		END
	END

	RETURN @sBIT
END