﻿
Create FUNCTION [dbo].[IsDownloadTimeValidbySN](
	@sLastDownload VARCHAR(15),
	@sAppName VARCHAR(25),
	@sSN VARCHAR(25))
	RETURNS BIT
BEGIN
DECLARE	@dtUploadTime DATETIME,
	@iReturn BIT,
	@sAppNameTemp VARCHAR(25)
--SET @slastDownload='2802130501'
--SET @dtUploadTime = GETDATE()

SELECT @sAppNameTemp=AppPackageName
FROM tbAppPackageEDCList JOIN tbTerminalSN 
	ON tbAppPackageEDCList.AppPackId=tbTerminalSN.AppPackId
WHERE tbTerminalSN.SerialNumber=@sSN

IF @sAppNameTemp != @sAppName
	SET @iReturn = 0
ELSE
BEGIN
	SELECT @dtUploadTime = UploadTime
	FROM tbAppPackageEDCList
	WHERE AppPackageName = @sAppName

	SET @sLastDownload = SUBSTRING(@sLastDownload,1,2) + '-'
		+ SUBSTRING(@sLastDownload,3,2) + '-'
		+ SUBSTRING(@sLastDownload,5,2) + ' '
		+ SUBSTRING(@sLastDownload,7,2) + ':'
		+ SUBSTRING(@sLastDownload,9,2)

	IF DATEDIFF(MINUTE,@dtUploadTime,CONVERT(DATETIME,@sLastDownload,5)) > 0
		SET @iReturn = 1
	ELSE
		SET @iReturn = 0
END	
RETURN @iReturn

END