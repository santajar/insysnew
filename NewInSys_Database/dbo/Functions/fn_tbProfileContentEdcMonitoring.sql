﻿-- =============================================
-- Author		: Tobias Setyo
-- Create date	: March 17, 2017
-- =============================================
CREATE FUNCTION [dbo].[fn_tbProfileContentEdcMonitoring](  
	@sTerminalID VARCHAR(8),  
	@sConditions VARCHAR(MAX)  
)
RETURNS @tempTerminal TABLE(  
  TerminalId VARCHAR(8),  
  [Name] VARCHAR(15),  
  Tag VARCHAR(5),  
  ItemName VARCHAR(50),  
  TagLength VARCHAR(3),  
  TagValue VARCHAR(MAX)  
 )  
BEGIN  
INSERT INTO @tempTerminal (TerminalId,  
  [Name],  
  Tag,  
  ItemName,  
  TagLength,  
  TagValue )  
SELECT b.TerminalId,   
 a.[Name],   
 a.Tag,  
 '' ItemName,   
 a.TagLength,  
 a.TagValue  
FROM tbProfileEdcMonitor a JOIN tbProfileTerminalList b  
ON a.DatabaseId=b.DatabaseId  
WHERE b.TerminalId=@sTerminalID 
AND a.Name IN (SELECT c.TerminalTagValue
FROM tbProfileTerminal c WITH (NOLOCK)
JOIN tbProfileTerminalList d on c.TerminalID=d.TerminalID
JOIN tbItemList e WITH (NOLOCK)
ON d.DatabaseID = e.DatabaseID
WHERE e.ItemName = 'TMS Monitoring Name'
AND c.TerminalID = @sTerminalID)
RETURN  
END