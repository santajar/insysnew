﻿
CREATE FUNCTION [dbo].[sGetAppFilenamebySN] (@sSN VARCHAR(25))
	RETURNS VARCHAR(MAX)
BEGIN
DECLARE @sAppFilename VARCHAR(MAX)
SELECT @sAppFilename = app.AppPackageFilename
FROM tbAppPackageEdcList app LEFT JOIN 
	tbTerminalSN list ON app.AppPackId = list.AppPackId
WHERE list.SerialNumber = @sSN

RETURN @sAppFilename
END