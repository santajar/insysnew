﻿CREATE FUNCTION [dbo].[sLengthFormat](@sTag VARCHAR(5))
RETURNS VARCHAR(1)
BEGIN
	DECLARE @sLengthFormat VARCHAR(1)
	IF @sTag='AE37' OR @sTag='AE38'
		SET @sLengthFormat='3'
	ELSE
		SET @sLengthFormat='2'
	RETURN @sLengthFormat
END