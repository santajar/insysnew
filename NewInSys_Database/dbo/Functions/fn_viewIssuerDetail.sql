﻿CREATE FUNCTION [dbo].[fn_viewIssuerDetail](@sTerminalId VARCHAR(8))
RETURNS @viewIssuerDetail TABLE(
	TerminalId VARCHAR(8),
	Name VARCHAR(25),
	Tag VARCHAR(5),
	ItemName VARCHAR(25),
	TagLength INT,
	LengthOfTagLength INT,
	TagValue VARCHAR(150)
)
AS
BEGIN
INSERT INTO @viewIssuerDetail
SELECT 
	tbProfileIssuer.TerminalID, 
	tbProfileIssuer.IssuerName AS Name, 
	tbProfileIssuer.IssuerTag AS Tag, 
	tbItemList.ItemName, 
	tbProfileIssuer.IssuerTagLength AS TagLength, 
	tbItemList.LengthofTagValueLength AS LengthOfTagLength, 
	tbProfileIssuer.IssuerTagValue AS TagValue
FROM 
	tbProfileIssuer WITH (NOLOCK) INNER JOIN tbProfileTerminalList WITH (NOLOCK)
	ON tbProfileIssuer.TerminalID = tbProfileTerminalList.TerminalID 
	INNER JOIN tbItemList WITH (NOLOCK) ON tbItemList.DatabaseID = tbProfileTerminalList.DatabaseID 
	AND tbItemList.Tag = tbProfileIssuer.IssuerTag
WHERE 
	tbProfileIssuer.TerminalID=@sTerminalId
RETURN
END