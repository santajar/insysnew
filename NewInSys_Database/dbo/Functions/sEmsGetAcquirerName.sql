﻿CREATE FUNCTION [dbo].[sEmsGetAcquirerName](@sEmsAcqName VARCHAR(50), @sVersion VARCHAR(50))
RETURNS VARCHAR(50)
BEGIN
	DECLARE @sValue VARCHAR(50)
	SELECT @sValue=EMSXMLAcquirerName FROM tbEmsMsAcquirer WITH(NOLOCK)
	WHERE EMSXMLColumnName=@sEmsAcqName AND EMSXMLVersion=@sVersion
	RETURN @sValue
END