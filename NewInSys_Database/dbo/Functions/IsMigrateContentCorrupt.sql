﻿CREATE FUNCTION [dbo].[IsMigrateContentCorrupt](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iReturn INT
	DECLARE @iOffset INT
	DECLARE @sContent VARCHAR(MAX)
	SET @sContent=dbo.sMigrateFullContent(@sTerminalId)
	
	--SET @iOffset=CHARINDEX('"',@sContent,3)
	SET @iOffset=PATINDEX('%"',@sContent)
	
	IF @iOffset>0
		SET @iReturn=0
	ELSE
		SET @iReturn=1
	
	RETURN (@iReturn)
END
