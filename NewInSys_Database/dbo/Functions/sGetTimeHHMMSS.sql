﻿CREATE FUNCTION [dbo].[sGetTimeHHMMSS]()
RETURNS VARCHAR(6)
BEGIN
	DECLARE @sTemp VARCHAR(6)
	SET @sTemp = (REPLACE(CONVERT(VARCHAR(10),GETDATE(),8),':',''))
	DECLARE @iNow INT
	SET @iNow = SUBSTRING(@sTemp,1,2)
--	IF @iNow > 12
--		SET @iNow = @iNow - 12
	RETURN (dbo.sPadLeft(@iNow,2-LEN(@iNow),'0') + RIGHT(@sTemp,4))
END


