﻿
CREATE FUNCTION [dbo].[fn_viewInitialFlazzManagementId]()  
RETURNS @viewPromoManagement TABLE(  
 InitialFlazzID VARCHAR(8),  
 DatabaseId SMALLINT,  
 InitialFlazzName VARCHAR(50)  
)  
AS  
BEGIN  
INSERT INTO @viewPromoManagement  
	SELECT  
		InitialFlazzID = MIN(ID), DatabaseID, InitialFlazzName
	FROM  
		tbProfileInitialFlazz WITH (NOLOCK)  
	GROUP BY DatabaseID, InitialFlazzName
	ORDER BY InitialFlazzID
RETURN  
END