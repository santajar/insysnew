﻿

CREATE FUNCTION [dbo].[iDatabaseIdBySoftName](@sSoftwareName VARCHAR(50))
RETURNS INT
BEGIN

DECLARE @iDbId INT
SELECT @iDbId=DatabaseId
FROM tbProfileSoftware
WHERE SoftwareName=@sSoftwareName

IF ISNULL(@iDbId,0)=0
	SET @iDbId=0

RETURN @iDbId
END


