﻿CREATE FUNCTION [dbo].[sBitValue](@iLength INT, 
		@iIndeksMsg INT, 
		@sEdcMessage VARCHAR(MAX))
	RETURNS VARCHAR(MAX)
BEGIN
	RETURN SUBSTRING(@sEdcMessage,@iIndeksMsg,@iLength)
END
