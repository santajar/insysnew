﻿CREATE FUNCTION [dbo].[iProcessRowsDetails] 
			(
				@sTerminalID VARCHAR(8),
				@sTag VARCHAR(5)
			)
RETURNS INT
BEGIN
	RETURN ( SELECT COUNT(InitId) 
			FROM tbInit WITH (NOLOCK)
			 WHERE TerminalID = @sTerminalId AND Tag =@sTag AND Flag = 1)
END