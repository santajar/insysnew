﻿CREATE FUNCTION [dbo].[sGetAppName] (@sTerminalID VARCHAR(8))
	RETURNS VARCHAR(MAX)
BEGIN
DECLARE @sAppName VARCHAR(MAX)
SELECT @sAppName = app.AppPackageName
FROM tbAppPackageEdcList app LEFT JOIN 
	tbListTIDRemoteDownload list ON app.AppPackId = list.AppPackID
WHERE list.TerminalID = @sTerminalID

RETURN @sAppName
END

