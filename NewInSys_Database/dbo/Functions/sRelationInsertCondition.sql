﻿CREATE FUNCTION [dbo].[sRelationInsertCondition]
(
@sTerminalID VARCHAR(8),
@sAcquirerValue VARCHAR(50),
@sIssuerValue VARCHAR(50),
@sTag VARCHAR(4)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @sConditions VARCHAR(MAX)

	IF (@sTag LIKE 'AD03')
		SET @sConditions = ''
	IF (@sTag LIKE 'AD02')
		SET @sConditions = N'WHERE TerminalID = '''+@sTerminalID+''' AND RelationTag = ''AD03''
								AND RelationTagValue = '''+@sAcquirerValue+''''
	ELSE IF (@sTag LIKE 'AD01')
		SET @sConditions = N'WHERE RelationTagID IN (SELECT RelationTagID - 1 FROM tbProfileRelation 
								WHERE RelationTagValue = '''+@sAcquirerValue+''' AND RelationTag = ''AD03'' AND TerminalID = '''+@sTerminalID+'''
								) AND RelationTagValue = '''+@sIssuerValue+''''
	RETURN @sConditions
END
