﻿
CREATE FUNCTION [dbo].[fn_tbProfileContentInitialFlazzManagement](  
	@sTerminalID VARCHAR(8),  
	@sConditions VARCHAR(MAX)  
)
RETURNS @tempTerminal TABLE(  
  TerminalId VARCHAR(8),  
  [Name] VARCHAR(15),  
  Tag VARCHAR(5),  
  ItemName VARCHAR(50),  
  TagLength VARCHAR(3),  
  TagValue VARCHAR(MAX)  
 )  
BEGIN  
INSERT INTO @tempTerminal (TerminalId,  
  [Name],  
  Tag,  
  ItemName,  
  TagLength,  
  TagValue )  
SELECT b.TerminalId,   
 a.InitialFlazzName [Name],   
 a.InitialFlazzTag Tag,  
 '' ItemName,   
 a.InitialFlazzTagLength TagLength,  
 a.InitialFlazzTagValue TagValue  
FROM tbProfileInitialFlazz a JOIN tbProfileTerminalList b  
ON a.DatabaseId=b.DatabaseId  
WHERE b.TerminalId=@sTerminalID  
	AND InitialFlazzName IN 
		(SELECT C.TerminalTagValue
		FROM tbProfileTerminal C WITH(NOLOCK) 
		JOIN tbProfileTerminalList D WITH(NOLOCK) 
		ON C.TerminalID=D.TerminalID
		JOIN tbItemList E WITH(NOLOCK) 
		ON D.DatabaseID= E.DatabaseID
		WHERE E.ItemName = 'Initial Card Setting'
			AND C.TerminalID=@sTerminalID )
RETURN  
END