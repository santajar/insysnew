﻿-- ====================================================================== --
-- Function Name : iTotalLoop
-- Parameter	 : 
--		1. @sTerminalId, initialized terminalid
-- Return		 :
--		ex.: iTotalLoop('12345678') --> 151
-- Description	 :
--		return initialize process total looping in integer
-- ====================================================================== --
CREATE FUNCTION [dbo].[iTotalLoop](@sTerminalId VARCHAR(8))
RETURNS INT
BEGIN
	RETURN (SELECT COUNT(*) FROM tbInit WITH (NOLOCK)
		WHERE TerminalId=@sTerminalId)
END
