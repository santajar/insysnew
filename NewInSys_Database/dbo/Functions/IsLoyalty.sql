﻿

CREATE FUNCTION [dbo].[IsLoyalty](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @LoyaltyInit BIT
	SELECT @LoyaltyInit=b.LoyaltyProd
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @LoyaltyInit
END


