﻿CREATE FUNCTION [dbo].[fn_tbProfileContentPinPad]( @sTerminalID VARCHAR(8),
	@sCondition VARCHAR(500)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(25),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(MAX)
	)
BEGIN
INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
SELECT 
	b.TerminalID,
	a.PinPadName,
	a.PinPadTag,
	'' ItemName,
	a.PinPadTagLength,
	a.PinPadTagValue
FROM tbProfilePinPad a WITH (NOLOCK) JOIN tbProfileTerminalList b WITH (NOLOCK) ON a.DatabaseID=b.DatabaseID
WHERE b.TerminalID=@sTerminalID
RETURN
END