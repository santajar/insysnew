﻿Create FUNCTION [dbo].[IsRemoteDownloadAllowed](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	RETURN(SELECT RemoteDownload FROM tbProfileTerminalList
		WHERE TerminalId=@sTerminalId)
END