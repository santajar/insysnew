﻿



-- ====================================================================== --
-- Function Name : sGetTLVString
-- Parameter	 : 
--		1. @sTag --> Tag String, ex.: DE01, AA01, etc.
--		2. @@sValue --> Tag value string, ex.: XRETFLZ1
-- Return		 :
--		ex.: sGetTLVString('de01','xretflz1') --> 'DE0108XRETFLZ1'
-- Modify		 :
--		1. Sept 20, 2010. modify the length format will be added 0 if the tag value
--		   length is less than 10
-- Description	 :
--		return tag-length-value string
-- ====================================================================== --
CREATE FUNCTION [dbo].[sGetTLVString](@sTag VARCHAR(5), @sValue VARCHAR(600))
	RETURNS VARCHAR(600)
AS
BEGIN
	DECLARE @sTemp VARCHAR(600)
	DECLARE @sLen VARCHAR(3)
	IF @sValue IS NOT NULL
	BEGIN
		SET @sLen=DATALENGTH(@sValue)
		IF LEN(@sLen)=1
		BEGIN
			IF @sTag <> 'AE37' AND @sTag <> 'AE38'
				SELECT @sTemp=RIGHT(dbo.sPadLeft(@sLen,5,'0'),2)
			ELSE
				SELECT @sTemp=RIGHT(dbo.sPadLeft(@sLen,5,'0'),3)
		END
		ELSE
			SET @sTemp = @sLen

		SET @sTemp = UPPER(@sTag) + @sTemp + (@sValue)
	END
	ELSE
		SET @sTemp = UPPER(@sTag) + '00'
	RETURN @sTemp
END




