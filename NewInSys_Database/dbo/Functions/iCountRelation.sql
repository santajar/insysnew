﻿CREATE FUNCTION [dbo].[iCountRelation](@sConditionStmt VARCHAR(MAX))
RETURNS INT
BEGIN
DECLARE @iJml INT
DECLARE @sSqlStmt NVARCHAR(MAX)	
SET @sSqlStmt = N'SELECT @iJml = COUNT(*) 
					FROM tbProfileRelation ' + ISNULL(@sConditionStmt,'')

EXECUTE SP_EXECUTESQL @sSqlStmt, N'@iJml INT OUTPUT', @iJml=@iJml OUTPUT

RETURN (@iJml)
END