﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <30 Sept 2015>
-- Description:	<Check Image Time Valid>
-- =============================================
CREATE FUNCTION [dbo].[isImageTimeValid]
(
	@sType VARCHAR(10),
	@sFileName VARCHAR(50),
	@sTime VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @bReturn BIT,
			@sDBTime VARCHAR(50),
			@sRemakeTime VARCHAR(50) 

	SELECT @sDBTime=UploadTime
	FROM tbProfileImage
	WHERE LogoType = @sType AND [FileName] = @sFileName

	--MM/dd/yyyy HH:mm:ss
	SET @sRemakeTime = SUBSTRING(@sTime,4,2) +SUBSTRING(@sTime,3,1)+ SUBSTRING(@sTime,1,2) + SUBSTRING(@sTime,6,LEN(@sTime)-5)
	set @sRemakeTime = convert(datetime, @sRemakeTime)


	IF @sRemakeTime = @sDBTime
		SET @bReturn = 1
	ELSE
		SET @bReturn = 0
	
	RETURN @bReturn

END