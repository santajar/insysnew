﻿

CREATE FUNCTION [dbo].[fn_viewRemoteDownload](@sDatabaseId VARCHAR(3) = '')  
RETURNS @viewRD TABLE(  
 RemoteDownloadTagID VARCHAR(8),  
 DatabaseId SMALLINT,  
 RemoteDownloadName VARCHAR(50)  
)  
AS  
BEGIN  
INSERT INTO @viewRD 
	SELECT AIDTagID = MIN(RemoteDownloadTagID), DatabaseID, RemoteDownloadName 
	FROM tbProfileRemoteDownload WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId
	--WHERE DatabaseID = CASE WHEN @DatabaseID = '' THEN DatabaseID ELSE @DatabaseID END
		GROUP BY DatabaseID, RemoteDownloadName
		ORDER BY RemoteDownloadName
RETURN  
END