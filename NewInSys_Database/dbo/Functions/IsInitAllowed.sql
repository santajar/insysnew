﻿-- ====================================================================== --
-- Function Name : iIsInitAllowed
-- Parameter	 : 
--		1. @sTerminalId, initialized terminalid
-- Return		 :
--		ex.: iIsInitAllowed('12345678') --> 0
-- Description	 :
--		return boolean, 1 : allowed TerminalId, 0 : not allowed TerminalId
-- ====================================================================== --
CREATE FUNCTION [dbo].[IsInitAllowed](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
DECLARE @bAllowInit BIT
DECLARE @iMaxRetry INT

SELECT @bAllowInit=AllowDownload, @iMaxRetry=MaxInitRetry 
FROM tbProfileTerminalList WITH (NOLOCK)
WHERE TerminalId=@sTerminalId

IF @bAllowInit<>1
BEGIN
	IF @iMaxRetry>0
	BEGIN
		SET @bAllowInit=1
	END
	ELSE
	BEGIN
		SET @bAllowInit=0
	END
END
RETURN @bAllowInit
END