﻿
CREATE FUNCTION [dbo].[sGetAppNamebySN] (@sSN VARCHAR(25))
	RETURNS VARCHAR(MAX)
BEGIN
DECLARE @sAppName VARCHAR(MAX)
SELECT @sAppName = app.AppPackageName
FROM tbAppPackageEdcList app LEFT JOIN 
	tbTerminalSN list ON app.AppPackId = list.AppPackId
WHERE list.SerialNumber = @sSN

RETURN @sAppName
END