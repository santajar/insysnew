﻿CREATE FUNCTION [dbo].[iTotalDownloadLoopbySN](@sSN VARCHAR(25))
RETURNS INT
BEGIN
	RETURN (
		SELECT MAX(appList.Id)
		FROM tbAppPackageEDCListTemp appList WITH (NOLOCK) JOIN tbAppPackageEDCList app WITH (NOLOCK) ON app.AppPackageName=appList.AppPackageName
			JOIN tbTerminalSN terminalSN ON app.AppPackId=terminalSN.AppPackID
		WHERE terminalSN.SerialNumber=@sSN)
END