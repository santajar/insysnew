﻿CREATE FUNCTION [dbo].[iDatabaseIdByTerminalId](@sTerminalId VARCHAR(8))
RETURNS INT
BEGIN

DECLARE @iDbId INT
SELECT @iDbId=DatabaseId
FROM dbo.fn_viewTerminalList()
WHERE TerminalId=@sTerminalId

RETURN @iDbId
END
