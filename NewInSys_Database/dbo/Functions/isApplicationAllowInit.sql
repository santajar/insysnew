﻿CREATE FUNCTION [dbo].[isApplicationAllowInit]( @sTerminalID VARCHAR(8),
											@sApplicationName VARCHAR(25))
	RETURNS BIT
BEGIN
	DECLARE @isAllow BIT
	SET @isAllow = 0

	DECLARE @iDatabaseID SMALLINT

	SELECT @iDatabaseID = DatabaseID
	FROM tbProfileTerminalList 
	WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID

	IF ISNULL(@sApplicationName,'') = ''
		SELECT @sApplicationName = dbo.sGetApplicationName(@sApplicationName)

	DECLARE @iCount INT
	
	SELECT @iCount = COUNT(SoftwareName)
	FROM tbProfileSoftware
	WITH (NOLOCK)
	WHERE DatabaseID = @iDatabaseID AND
		  SoftwareName = @sApplicationName

	IF (@iCount = 1)
		SET @isAllow = 1

	RETURN @isAllow
END
