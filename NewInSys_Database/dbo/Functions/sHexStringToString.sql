﻿CREATE FUNCTION [dbo].[sHexStringToString](@sValue VARCHAR(100))
RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sTemp VARCHAR(MAX)
	DECLARE @SqlStmt AS NVARCHAR(MAX)
	DECLARE @sParamDef AS NVARCHAR(MAX)
	SET @sParamDef = N'@sResult VARCHAR(MAX) OUTPUT'
	SET @SqlStmt=N'SELECT CONVERT(VARCHAR, 0x'+@sValue+')'

--	EXEC sp_executesql @SqlStmt, @sParamDef, @sResult=@sTemp OUTPUT
	EXECUTE sp_executesql @SqlStmt, N'DECLARE @sResult VARCHAR(MAX)', @sResult=@sTemp OUTPUT

	RETURN (@sTemp)
END
