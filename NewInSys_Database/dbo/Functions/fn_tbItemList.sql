﻿CREATE FUNCTION [dbo].[fn_tbItemList] (@iDatabaseID SMALLINT,
									  @sTag VARCHAR(5))
RETURNS @tbItemList TABLE ( ColumnName VARCHAR(50),
						  ColumnValue VARCHAR(500)
						)
AS
BEGIN
	INSERT INTO @tbItemList(ColumnName, ColumnValue)
	SELECT ColumnName, ColumnValue
	FROM ( SELECT CONVERT(VARCHAR(500), dbo.sGetDatabaseName(DatabaseID)) VersionName, 
					CONVERT(VARCHAR(500), Tag) Tag, 
					CONVERT(VARCHAR(500), ItemName) ItemName, 
					CONVERT(VARCHAR(500), dbo.sGetFormName(FormId)) FormID, 
					CONVERT(VARCHAR(500), dbo.sGetObjectName(ObjectID)) ObjectID,
					CONVERT(VARCHAR(500), ItemSequence) ItemSequence, 
					CONVERT(VARCHAR(500), ISNULL(DefaultValue,'')) DefaultValue, 
					CONVERT(VARCHAR(500), LengthOfTagValueLength) LengthOfTagValueLength,
					CONVERT(VARCHAR(500), dbo.sBit2Str(vAllowNull)) AllowNull, 
					CONVERT(VARCHAR(500), dbo.sBit2Str(vUpperCase)) UpperCase, 
					CONVERT(VARCHAR(500), vType) [Type], 
					CONVERT(VARCHAR(500), vMinLength) MinimumLength, 
					CONVERT(VARCHAR(500), vMaxLength) MaximumLength,
					CONVERT(VARCHAR(500), vMinValue) MinimumValue, 
					CONVERT(VARCHAR(500), vMaxValue) MaximumValue, 
					CONVERT(VARCHAR(500), dbo.sBit2Str(vMasking)) Masking, 
					CONVERT(VARCHAR(500), validationmsg) ValidationMessage
			FROM tbItemList WITH (NOLOCK)
			WHERE DatabaseID = @iDatabaseID and Tag = @sTag
		 ) tbPivot
	UNPIVOT
		(ColumnValue FOR ColumnName IN (VersionName, 
										Tag, 
										FormID, 
										ObjectID, 
										ItemName, 
										ItemSequence, 
										DefaultValue, 
										LengthOfTagValueLength,
										AllowNull,
										UpperCase, 
										[Type], 
										MinimumLength,
										MaximumLength, 
										MinimumValue, 
										MaximumValue, 
										Masking,
										ValidationMessage
										)
		) AS tbUnpivot
	RETURN
END


