﻿-- =============================================
-- Author		: Tobias Setyo
-- Create date	: March 17, 2017
-- =============================================
CREATE FUNCTION [dbo].[IsEdcMonitoringInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iEnable BIT
	SELECT @iEnable=b.EnableMonitor
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDB b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @iEnable
END