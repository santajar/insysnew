﻿CREATE FUNCTION [dbo].[sGetObjectName]( @iObjectID SMALLINT)
	RETURNS VARCHAR(50)
BEGIN
	DECLARE @sObjectName VARCHAR(100)
	SELECT @sObjectName  = ObjectName
	FROM tbItemObject
	WHERE ObjectID = @iObjectID  
	
	RETURN @sObjectName  
END




