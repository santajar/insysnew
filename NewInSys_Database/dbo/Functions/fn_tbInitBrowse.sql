﻿CREATE FUNCTION [dbo].[fn_tbInitBrowse](@sTerminalId VARCHAR(8))
RETURNS @temp TABLE(
	InitId INT,
	TerminalId VARCHAR(8),
	Cont VARCHAR(MAX),
	Tag VARCHAR(5),
	Flag BIT)
AS
BEGIN
	INSERT INTO @temp(InitId,
		TerminalId,
		Cont,
		Tag,
		Flag)
	SELECT TOP(1) InitId, 
		TerminalId,
		[Content],
		Tag,
		Flag
	FROM tbInit 
	WITH (NOLOCK)
	WHERE TerminalId = @sTerminalId 
		AND Flag = 0
	ORDER BY InitID

	RETURN
END