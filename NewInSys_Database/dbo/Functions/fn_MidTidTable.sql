﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Oct 6, 2016
-- Description:	return table, which contains of MID and TerminalID
-- =============================================
--ALTER FUNCTION [dbo].[fn_MidTidTable]()
--RETURNS @MidTidTable TABLE(MID VARCHAR(9), TerminalID VARCHAR(8))
--AS
--BEGIN
--INSERT INTO @MidTidTable(MID, TerminalID)
--SELECT DISTINCT RIGHT(AcquirerTagValue,9) MID, TerminalID
--FROM tbProfileAcquirer WITH(NOLOCK)
--WHERE AcquirerTag IN ('AA04','AA004') 
--	AND ISNUMERIC(AcquirerTagValue)>0
--ORDER BY MID
--RETURN
--END

CREATE FUNCTION [dbo].[fn_MidTidTable]
(	
)
RETURNS TABLE 
AS

RETURN 

(
	SELECT DISTINCT RIGHT(AcquirerTagValue,9) MID, TerminalID
	FROM tbProfileAcquirer WITH(NOLOCK)
	WHERE AcquirerTag IN ('AA04','AA004') and AcquirerName in ('BCA','LOYALTY','DEBIT','FLAZZBCA','BCA3','BCA4','BCA5','BCA6','BCA7','BCA8','BCA9','BCA10')
	AND ISNUMERIC(AcquirerTagValue)>0
)