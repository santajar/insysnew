﻿CREATE FUNCTION [dbo].[sInitLogConnErrorMessage] 
			(
				@iPercentage INT
			)
RETURNS VARCHAR(50)		
BEGIN
	DECLARE @iErrID INT
	DECLARE @sErrMsg VARCHAR(50)
	SET @iErrID = -1

	IF (@iPercentage = 0)
		SET @sErrMsg = 'Waiting'
	ELSE IF (@iPercentage < 100)
		SET @iErrID = 0
	ELSE IF (@iPercentage = 100)
		SET @iErrID = 1

	IF @iErrID >= 0
	BEGIN
		SELECT @sErrMsg = ErrorMessage FROM tbMSErrorInit WITH (NOLOCK)
		WHERE ErrorCode = @iErrID
	END
	RETURN
	(
		SELECT @sErrMsg
	)
END	
