﻿CREATE FUNCTION [dbo].[iTotalRowsDetails] 
			(
				@sTerminalID VARCHAR(8),
				@sTag VARCHAR(5)
			)
RETURNS INT
BEGIN
	DECLARE @iCount INT
	SELECT @iCount = COUNT(InitId) 
	FROM tbInit
		WITH (NOLOCK)
	 WHERE TerminalID = @sTerminalId AND Tag =@sTag

	IF (@iCount = 0) 
		SET @iCount = 1
	RETURN ( SELECT @iCount )
END