﻿--function ini tdk bisa dijalankan.
--error "Only functions and extended stored procedures can be executed from within a function.
--"
CREATE FUNCTION [dbo].[IsCreateMessage](@sSessionTime AS VARCHAR(10))
RETURNS TINYINT
--RETURNS VARCHAR(6)
AS
BEGIN
	DECLARE @Status INT
	DECLARE @sSqlstmt NVARCHAR(MAX)
	DECLARE @sParamDef NVARCHAR(MAX)
	DECLARE @sProcode VARCHAR(6)
	SET @sParamDef = N'@sProcode VARCHAR(6) OUTPUT'
	SET @sSqlstmt = N'
	SELECT @sProcode = bitValue FROM ##tempBitMap'+@sSessionTime+' WHERE bitid = 3'
	EXECUTE sp_executesql @sSqlstmt, @sParamDef, @sProcode = @sProcode output

	IF (@sProcode = '930000') SET @Status = 1--Create Message
	ELSE IF (@sProcode = '930001') SET @Status = 2 -- Continue Message
	ELSE SET @Status = 0 -- Unknown Procode

	RETURN @Status
--	RETURN @sProcode
END  
