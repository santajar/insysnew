﻿-- =============================================
-- Author:		<Author,,TEDIE SCORFIA>
-- Create date: <Create Date, ,6 MEI 2014>
-- Description:	<Description, ,GET BUILD NUMBER BY TID>
-- =============================================
CREATE FUNCTION [dbo].[sGetbuildNumber]
(
	@sTerminalID VARCHAR(8)
)
RETURNS INT
AS
BEGIN
	RETURN (
	SELECT B.BuildNumber
	FROM 
	(
		(SELECT * FROM tbListTIDRemoteDownload) A
		LEFT JOIN
		(SELECT * FROM tbAppPackageEDCList) B
		ON A.AppPackID=B.AppPackId
	)
	WHERE A.TerminalID = @sTerminalID
	)
END