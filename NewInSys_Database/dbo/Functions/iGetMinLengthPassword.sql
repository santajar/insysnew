﻿-- =============================================
-- Author:		<Author: Tedie Scorfia>
-- Create date: <Create Date: 2 juli 2015>
-- Description:	<Description: Get Min Length Password>
-- =============================================
CREATE FUNCTION [dbo].[iGetMinLengthPassword]()
RETURNS INT
AS
BEGIN
	--DECLARE @sFlag VARCHAR(10)
	--SELECT @sFlag = Flag 
	--FROM tbControlFlag 
	--WHERE ItemName='MinLengthPassword'
	
	RETURN (SELECT CONVERT(INT,Flag)
			FROM tbControlFlag 
			WHERE ItemName='MinLengthPassword')

END