﻿create FUNCTION [dbo].[sGenerateDownloadFillerbySN](
	@sSN VARCHAR(25),
	@iCounterApp INT
	)
RETURNS VARCHAR(200)
BEGIN
DECLARE @sExtraData VARCHAR(200),
	@sDownloadedAppName VARCHAR(150),
	@sDownloadedAppFilename VARCHAR(150),
	@sHexDownloadedAppName VARCHAR(300),
	@sHexDownloadedAppFilename VARCHAR(300),
	@iLenDownloadedAppName INT,
	@iLenDownloadedAppFilename INT,
	@sLenDownloadedAppName VARCHAR(4),
	@sLenDownloadedAppFilename VARCHAR(4),
	@sCounterApp VARCHAR(4)
	
SELECT @sDownloadedAppName = AppPackageName, @sDownloadedAppFilename = AppPackageFilename
FROM tbAppPackageEDCList app JOIN tbTerminalSN terminal
ON app.AppPackId = terminal.AppPackID
WHERE SerialNumber = @sSN

SET @iLenDownloadedAppName = LEN(@sDownloadedAppName)
SET @iLenDownloadedAppFilename = LEN(@sDownloadedAppFilename)

SET @sLenDownloadedAppName = REPLICATE('0', 4-LEN(@iLenDownloadedAppName)) 
	+ CONVERT(VARCHAR,@iLenDownloadedAppName)
SET @sLenDownloadedAppFilename = REPLICATE('0', 4-LEN(@iLenDownloadedAppFilename)) 
	+ CONVERT(VARCHAR,@iLenDownloadedAppFilename)
SET @sCounterApp = REPLICATE('0', 4-LEN(@iCounterApp)) 
	+ CONVERT(VARCHAR,@iCounterApp)
	
SET @sHexDownloadedAppName = dbo.sStringToHexString(@sDownloadedAppName)
SET @sHexDownloadedAppFilename = dbo.sStringToHexString(@sDownloadedAppFilename)
	
SET @sExtraData = @sCounterApp 
	+ @sLenDownloadedAppName + @sHexDownloadedAppName
	+ @sLenDownloadedAppFilename + @sHexDownloadedAppFilename
	+ REPLICATE('F',200 - 4 - 4 - LEN(@sHexDownloadedAppName) - 4 - LEN(@sHexDownloadedAppFilename))

RETURN (@sExtraData)
END