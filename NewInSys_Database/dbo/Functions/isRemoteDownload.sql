﻿Create FUNCTION [dbo].[isRemoteDownload] (@sTerminalID VARCHAR(8))
	RETURNS BIT
BEGIN
	DECLARE @isRemoteDownload BIT

	--SET @sAppName = dbo.sGetApplicationName(@sAppName)

	SELECT @isRemoteDownload = RemoteDownload
	FROM tbProfileTerminalList
		WITH (NOLOCK)
	WHERE TerminalID = @sTerminalID

	RETURN @isRemoteDownload
END