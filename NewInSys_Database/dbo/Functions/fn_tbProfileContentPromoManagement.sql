﻿
CREATE FUNCTION [dbo].[fn_tbProfileContentPromoManagement](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(20),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(MAX)
	)
BEGIN
INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
SELECT b.TerminalId, 
	a.PromoManagementName [Name],
	a.PromoManagementTag Tag,
	'' ItemName, 
	a.PromoManagementTaglength TagLength,
	a.PromoManagementTagValue TagValue
FROM tbProfilePromoManagement a WITH (NOLOCK) JOIN tbProfileTerminalList b WITH (NOLOCK)
ON a.DatabaseId=b.DatabaseId
WHERE b.TerminalId=@sTerminalID
AND PromoManagementName IN (SELECT c.TerminalTagValue
FROM tbProfileTerminal c WITH (NOLOCK)
JOIN tbProfileTerminalList d on c.TerminalID=d.TerminalID
JOIN tbItemList e WITH (NOLOCK)
ON d.DatabaseID = e.DatabaseID
WHERE e.ItemName = 'Promo Name'
AND c.TerminalID = @sTerminalID)
RETURN
END