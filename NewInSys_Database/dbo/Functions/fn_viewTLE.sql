﻿

CREATE FUNCTION [dbo].[fn_viewTLE](@sDatabaseId VARCHAR(3) = '')  
RETURNS @viewTLE TABLE(  
 TLETagID VARCHAR(8),  
 DatabaseId SMALLINT,  
 TLEName VARCHAR(50)  
)  
AS  
BEGIN  
INSERT INTO @viewTLE  
	SELECT TLETagID = MIN(TLETagID), DatabaseID, TLEName 
	FROM tbProfileTLE WITH (NOLOCK)
	WHERE DatabaseID = @sDatabaseId
	--WHERE DatabaseID = CASE WHEN @DatabaseID = '' THEN DatabaseID ELSE @DatabaseID END
		GROUP BY DatabaseID, TLEName
		ORDER BY TLEName
RETURN  
END

