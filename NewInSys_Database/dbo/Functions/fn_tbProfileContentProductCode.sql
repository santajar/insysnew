﻿CREATE FUNCTION [dbo].[fn_tbProfileContentProductCode]
(
	@sTerminalID VARCHAR(8), 
	@sConditions VARCHAR(MAX)
)
RETURNS @tempTerminal TABLE(  
  TerminalId VARCHAR(8),  
  [Name] VARCHAR(15),  
  Tag VARCHAR(5),  
  ItemName VARCHAR(50),  
  TagLength VARCHAR(3),  
  TagValue VARCHAR(MAX)  
)
AS
BEGIN
	INSERT INTO @tempTerminal (TerminalId,
			[Name],
			Tag,
			ItemName,
			TagLength,
			TagValue )
	SELECT b.TerminalId, 
		a.ProductCode [Name],
		a.ProductCodeTag Tag,
		'' ItemName, 
		a.ProductCodeTagLength TagLength,
		a.ProductCodeTagValue TagValue
	FROM tbProfileProductCode a WITH (NOLOCK) JOIN tbProfileTerminalList b WITH (NOLOCK)
	ON a.DatabaseId=b.DatabaseId
	WHERE b.TerminalId=@sTerminalID
	RETURN
END
