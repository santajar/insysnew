﻿CREATE FUNCTION [dbo].[IsCurrencyInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iCurrencyInit BIT
	SELECT @iCurrencyInit=b.CurrencyInit
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @iCurrencyInit
END

