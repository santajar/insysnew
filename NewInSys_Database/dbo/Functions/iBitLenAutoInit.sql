﻿CREATE FUNCTION [dbo].[iBitLenAutoInit] (@iTemp INT)
	RETURNS INT
BEGIN
	DECLARE @iLen INT
		IF @iTemp = 1
			SET @iLen = 12
 		ELSE IF @iTemp = 2
			SET @iLen = 8
		ELSE IF @iTemp = 3
			SET @iLen = 30
		ELSE IF @iTemp = 4
			SET @iLen = 30
		ELSE IF @iTemp = 5
			SET @iLen = 30
		ELSE IF @iTemp = 6
			SET @iLen = 30
		ELSE IF @iTemp = 7
			SET @iLen = 30
		ELSE IF @iTemp = 8
			SET @iLen = 20
		ELSE IF @iTemp = 9
			SET @iLen = 20
		ELSE IF @iTemp = 10
			SET @iLen = 30
		/* modified for ICCID message on bit48 autoinit*/
		ELSE IF @iTemp = 11
			SET @iLen = 40
	RETURN @iLen
END
