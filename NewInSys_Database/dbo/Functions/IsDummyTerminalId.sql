﻿CREATE FUNCTION [dbo].[IsDummyTerminalId](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
DECLARE @iBit BIT
SET @iBit = 1

IF dbo.IsTerminalIdExist(@sTerminalId) = 1
BEGIN
	DECLARE @sDE02 VARCHAR(50)
	DECLARE @sDE03 VARCHAR(50)
	DECLARE @sDE04 VARCHAR(50)
	SELECT @sDE02=TerminalTagValue FROM tbProfileTerminal
		WHERE TerminalTag='DE02'
	SELECT @sDE03=TerminalTagValue FROM tbProfileTerminal
		WHERE TerminalTag='DE03'
	SELECT @sDE04=TerminalTagValue FROM tbProfileTerminal
		WHERE TerminalTag='DE04'

	IF (PATINDEX('%DUMMY%',@sDE02) = 0) AND 
	(PATINDEX('%DUMMY%',@sDE03) = 0) AND
	(PATINDEX('%DUMMY%',@sDE04) = 0)
		SET @iBit=0
END	
RETURN @iBit
END