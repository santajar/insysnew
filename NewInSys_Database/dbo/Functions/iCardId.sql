﻿
CREATE FUNCTION [dbo].[iCardId](@iDatabaseId INT,
	@sCardName VARCHAR(50))
RETURNS INT
BEGIN
	RETURN (SELECT CardId 
		FROM tbProfileCardList WITH (NOLOCK)
		WHERE DatabaseId=@iDatabaseId AND CardName=@sCardName)
END
