﻿CREATE FUNCTION [dbo].[sReportFiturTerminalTagValue]
(
	@sTerminalID VARCHAR(8),
	@sTag VARCHAR(4)
)
RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sValue VARCHAR(MAX)
	SELECT @sValue=TerminalTagValue FROM tbProfileTerminal
	WHERE TerminalID=@sTerminalID AND TerminalTag=@sTag
	RETURN @sValue
END