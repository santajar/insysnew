﻿CREATE FUNCTION [dbo].[iRelationTagID](@sConditionStmt VARCHAR(MAX))
RETURNS INT
BEGIN
DECLARE @iRelationTagID INT
DECLARE @sSqlStmt NVARCHAR(MAX)
SET @sSqlStmt = N'SELECT @iRelationTagID = RelationTagID - 1 FROM tbProfileRelation ' 
		+ ISNULL(@sConditionStmt,'')
EXECUTE SP_EXECUTESQL @sSqlStmt, N'@iRelationTagID INT OUT', @iRelationTagID OUT

RETURN @iRelationTagID
END