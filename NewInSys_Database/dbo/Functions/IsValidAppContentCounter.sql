﻿
CREATE FUNCTION [dbo].[IsValidAppContentCounter](@iCounter INT, @sAppNameDownload VARCHAR(MAX))
RETURNS BIT
BEGIN
	RETURN (
		SELECT (CASE WHEN COUNT(*)>0 THEN 1	ELSE 0 END) 
		FROM tbAppPackageEDCListTemp
		WITH(NOLOCK)
		WHERE AppPackageName=@sAppNameDownload AND Id=@iCounter)
END


