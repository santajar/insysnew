﻿CREATE FUNCTION [dbo].[sMTIResponse]()
	RETURNS VARCHAR(MAX)
BEGIN
	RETURN (SELECT Flag 
			FROM tbControlflag 
			WITH (NOLOCK)
			WHERE ItemName='MTIResponse')
END