﻿

CREATE FUNCTION [dbo].[iDatabaseIdByName](@sDatabaseName VARCHAR(50))
RETURNS INT
BEGIN

DECLARE @iDbId INT
SELECT @iDbId=DatabaseId
FROM tbProfileTerminalDb
WHERE DatabaseName=@sDatabaseName

IF ISNULL(@iDbId,0)=0
	SET @iDbId=0

RETURN @iDbId
END


