﻿CREATE FUNCTION [dbo].[fn_viewInit](@sTerminalId VARCHAR(8))
RETURNS @viewInitAll TABLE(
	TerminalId VARCHAR(8),
	Tag VARCHAR(5),
	TotalRowSent INT,
	TotalRow INT
)
AS
BEGIN
INSERT INTO @viewInitAll
SELECT 
	TerminalId, 
	Tag, 
	dbo.iProcessRowsDetails(TerminalId, Tag) AS TotalRowSent, 
	COUNT(Tag) AS TotalRow
FROM         
	tbInit WITH (NOLOCK)
GROUP BY 
	TerminalId, 
	Tag
RETURN
END