﻿CREATE FUNCTION [dbo].[iProcessRows] (@sTerminalID VARCHAR(8))
RETURNS INT
BEGIN
	RETURN ( SELECT COUNT(InitId) 
			FROM tbInit WITH (NOLOCK)
			WHERE TerminalID = @sTerminalId AND Flag = 1)
END