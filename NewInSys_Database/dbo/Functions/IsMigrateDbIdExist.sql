﻿CREATE FUNCTION [dbo].[IsMigrateDbIdExist](@sDatabaseId VARCHAR(3))
RETURNS BIT
BEGIN
	RETURN (SELECT COUNT(*) 
		FROM tbProfileTerminalDB
		WHERE DatabaseID=CAST(@sDatabaseId AS INT))
END
