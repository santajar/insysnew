﻿CREATE FUNCTION [dbo].[fn_viewRelationTLV](@sTerminalId VARCHAR(8))
RETURNS @viewRelationTLV TABLE(
	[TerminalID] VARCHAR(8) NOT NULL,
	[Tag] VARCHAR(5) NOT NULL,
	[TagLength] INT NOT NULL,
	[LengthOfTagLength] INT NOT NULL,
	[TagValue] VARCHAR(150) NULL,
	[RelationTagID] BIGINT NULL,
	[DatabaseID] SMALLINT NULL
)
AS
BEGIN
	INSERT INTO @viewRelationTLV
	SELECT dbo.tbProfileRelation.TerminalID, 
		dbo.tbProfileRelation.RelationTag AS Tag, 
		dbo.tbProfileRelation.RelationTagLength AS TagLength, 
		dbo.tbItemList.LengthofTagValueLength AS LengthOfTagLength, 
		dbo.tbProfileRelation.RelationTagValue AS TagValue,
		dbo.tbProfileRelation.RelationTagID, 
        dbo.tbItemList.DatabaseID
	FROM 
		dbo.tbProfileRelation WITH (NOLOCK) JOIN dbo.tbProfileTerminalList WITH (NOLOCK) 
		ON dbo.tbProfileRelation.TerminalID = dbo.tbProfileTerminalList.TerminalID
		LEFT OUTER JOIN tbItemList WITH (NOLOCK) ON tbProfileTerminalList.DatabaseID=tbItemList.DatabaseID 
		AND tbProfileRelation.RelationTag=tbItemList.Tag
    WHERE tbProfileRelation.TerminalID=@sTerminalId
    ORDER BY RelationTagID
	RETURN
END
