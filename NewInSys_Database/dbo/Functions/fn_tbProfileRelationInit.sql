﻿CREATE FUNCTION [dbo].[fn_tbProfileRelationInit](@sTerminalId VARCHAR(8))
RETURNS @RelationInit TABLE(
	TerminalID VARCHAR(8), 
	Tag VARCHAR(4), 
	TagLength INT, 
	LengthOfTagLength INT, 
	TagValue VARCHAR(30))
BEGIN

INSERT INTO @RelationInit(
	TerminalID,
	Tag,
	TagLength,
	LengthOfTagLength,
	TagValue)
SELECT    TerminalID, 
	Tag, 
	TagLength, 
	LengthOfTagLength, 
	TagValue
FROM dbo.fn_viewRelationTLV(@sTerminalId)
WHERE RelationTagID NOT IN
        (
        SELECT RelationTagID
        FROM    dbo.fn_viewRelationTagValueIsNull(@sTerminalId)
		)
ORDER BY RelationTagID
RETURN
END