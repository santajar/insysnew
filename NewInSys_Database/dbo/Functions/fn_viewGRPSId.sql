﻿

CREATE FUNCTION [dbo].[fn_viewGRPSId]()  
RETURNS @viewGPRS TABLE(  
 GPRSTagID VARCHAR(8),  
 DatabaseId SMALLINT,  
 GPRSName VARCHAR(50)  
)  
AS  
BEGIN  
INSERT INTO @viewGPRS  
	SELECT  
		GprsTagId = MIN(GprsTagId), DatabaseID, GPRSName
	FROM  
		tbProfileGPRS WITH (NOLOCK)  
	GROUP BY DatabaseID, GPRSName
	ORDER BY GprsTagId
RETURN  
END

