﻿CREATE FUNCTION [dbo].[IsDownloadTimeValid](
	@sLastDownload VARCHAR(15),
	@sAppName VARCHAR(25),
	@sTerminalID VARCHAR(8))
	RETURNS BIT
BEGIN
DECLARE	@dtUploadTime DATETIME,
	@iReturn BIT,
	@sAppNameTemp VARCHAR(25)
--SET @slastDownload='2802130501'
--SET @dtUploadTime = GETDATE()

SELECT @sAppNameTemp=AppPackageName
FROM tbAppPackageEDCList JOIN tbListTIDRemoteDownload 
	ON tbAppPackageEDCList.AppPackId=tbListTIDRemoteDownload.AppPackId
WHERE TerminalID=@sTerminalID

IF @sAppNameTemp != @sAppName
	SET @iReturn = 0
ELSE
BEGIN
	SELECT @dtUploadTime = UploadTime
	FROM tbAppPackageEDCList
	WHERE AppPackageName = @sAppName

	SET @sLastDownload = SUBSTRING(@sLastDownload,1,2) + '-'
		+ SUBSTRING(@sLastDownload,3,2) + '-'
		+ SUBSTRING(@sLastDownload,5,2) + ' '
		+ SUBSTRING(@sLastDownload,7,2) + ':'
		+ SUBSTRING(@sLastDownload,9,2)

	IF DATEDIFF(YEAR,@dtUploadTime,CONVERT(DATETIME,@sLastDownload,5)) > 0
		SET @iReturn = 1
	ELSE
	BEGIN
		IF DATEDIFF(MONTH,@dtUploadTime,CONVERT(DATETIME,@sLastDownload,5)) > 0
			SET @iReturn = 1
		ELSE
		BEGIN
			IF DATEDIFF(DAY,@dtUploadTime,CONVERT(DATETIME,@sLastDownload,5)) > 0
			SET @iReturn = 1
			ELSE
			BEGIN
				IF DATEDIFF(HOUR,@dtUploadTime,CONVERT(DATETIME,@sLastDownload,5)) > 0
					SET @iReturn = 1
				ELSE
				BEGIN
					IF DATEDIFF(MINUTE,@dtUploadTime,CONVERT(DATETIME,@sLastDownload,5)) > 0
						SET @iReturn = 1
					ELSE
						SET @iReturn = 0
				END
			END
			
		END
	END
END	
RETURN @iReturn

END
