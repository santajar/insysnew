﻿
cREATE FUNCTION isRemoteDownloadContentValid
(
	@sTerminalID VARCHAR(8),
	@sAppDateTime VARCHAR(20),
	@sAppNameDownload VARCHAR(20)
)
RETURNS BIT
AS
BEGIN
	DECLARE @dtUploadTime DATETIME,
		@sUploadTime VARCHAR(20),
		@sUploadTimeDDMMYYHHMM VARCHAR(20),
		@sBit BIT,
		@sAppNameDownloadDB VARCHAR(20)

	SELECT @dtUploadTime= UploadTime, @sAppNameDownloadDB=AppPackageName FROM tbListTIDRemoteDownload A
	JOIN tbAppPackageEDCList B
	ON A.AppPackID= B.AppPackId
	WHERE TerminalID = @sTerminalID

	SELECT @sUploadTime=CONVERT(VARCHAR(8), @dtUploadTime, 112) + LEFT(REPLACE(CONVERT(varchar, @dtUploadTime, 108), ':',''),4)
	SET @sUploadTimeDDMMYYHHMM = SUBSTRING(@sUploadTime,7,2) + SUBSTRING(@sUploadTime,5,2) + SUBSTRING(@sUploadTime,3,2) + SUBSTRING(@sUploadTime,9,2) + SUBSTRING(@sUploadTime,11,2)

	IF @sAppDateTime = @sUploadTimeDDMMYYHHMM AND @sAppNameDownload = @sAppNameDownloadDB
		SET @sBit= 1
	ELSE
		SET @sBit= 0

	RETURN @sBit
END