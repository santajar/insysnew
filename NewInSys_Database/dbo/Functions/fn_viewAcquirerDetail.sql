﻿CREATE FUNCTION [dbo].[fn_viewAcquirerDetail](@sTerminalId VARCHAR(8))
RETURNS @viewAcquirerDetail TABLE(
	TerminalId VARCHAR(8),
	Name VARCHAR(15),
	Tag VARCHAR(5),
	ItemName VARCHAR(24),
	TagLength INT,
	LengthOfTagLength INT,
	TagValue VARCHAR(150)
)
AS
BEGIN
INSERT INTO @viewAcquirerDetail
SELECT
	tbProfileAcquirer.TerminalID, 
	tbProfileAcquirer.AcquirerName AS Name, 
	tbProfileAcquirer.AcquirerTag AS Tag, 
	tbItemList.ItemName, 
	tbProfileAcquirer.AcquirerTagLength AS TagLength, 
	tbItemList.LengthofTagValueLength AS LengthOfTagLength, 
	--tbProfileAcquirer.AcquirerTagValue AS TagValue
	TagValue = 
		CASE
			WHEN tbItemList.vMasking = 1 THEN '****************'
			ELSE tbProfileAcquirer.AcquirerTagValue
		END
FROM
	tbProfileAcquirer WITH (NOLOCK) INNER JOIN tbProfileTerminalList WITH (NOLOCK)
	ON tbProfileAcquirer.TerminalID = tbProfileTerminalList.TerminalID
	INNER JOIN tbItemList WITH (NOLOCK) ON tbItemList.Tag = tbProfileAcquirer.AcquirerTag
	AND tbItemList.DatabaseID = tbProfileTerminalList.DatabaseID
WHERE tbProfileAcquirer.TerminalID=@sTerminalId
ORDER BY Name, Tag
RETURN
END