﻿
CREATE FUNCTION [dbo].[IsEMVInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iEMVInit BIT,
			@iEMVInitTerminal BIT,
			@bReturn BIT

	SELECT @iEMVInit=b.EMVInit, @iEMVInitTerminal = a.EMVInit
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	IF @iEMVInit = 1
	BEGIN
		--IF @iEMVInit = @iEMVInitTerminal
			SET @bReturn = '1'
		--ELSE
		--	SET @bReturn = '0'
	END
	ELSE
		SET @bReturn = '0'

	RETURN @bReturn
END
