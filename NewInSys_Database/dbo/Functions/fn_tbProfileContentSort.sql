﻿
/****** Object:  StoredProcedure [dbo].[spProfileContent]    Script Date: 09/15/2010 14:18:23 ******/
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 20, 2010
-- Modify date:
--				1. 
-- Description:	Get The Terminal Content and Sorted by Tag
-- =============================================
CREATE FUNCTION [dbo].[fn_tbProfileContentSort](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		ID INT IDENTITY,
		TerminalId VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(100)
	)
BEGIN
	DECLARE @sSqlStmt VARCHAR(MAX)

	INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		TerminalId,
		Tag,
		ItemName,
		TagLength,
		TagValue
	FROM dbo.fn_viewTerminalDetail(@sTerminalID)
	WHERE Tag LIKE 'DE%'
	ORDER BY Tag
	
	INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		TerminalId,
		Tag,
		ItemName,
		TagLength,
		TagValue
	FROM dbo.fn_viewTerminalDetail(@sTerminalID)
	WHERE Tag LIKE 'DC%'
	ORDER BY Tag

	INSERT INTO @tempTerminal(TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue 
	FROM dbo.fn_viewCardDetail(@sTerminalID)
	WHERE [Name] IN 
		(
		SELECT CardName 
		FROM dbo.fn_viewProfileTerminalIdCard(@sTerminalID)
		)
	ORDER BY [Name], Tag

	INSERT INTO @tempTerminal(TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue 
	FROM dbo.fn_viewIssuerDetail(@sTerminalID)

	INSERT INTO @tempTerminal(TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue 
	FROM dbo.fn_viewAcquirerDetail(@sTerminalID)

	INSERT INTO @tempTerminal(TerminalId,
		Tag,
		ItemName,
		TagLength,
		TagValue )
	SELECT TerminalId,
		Tag,
		'',
		TagLength,
		TagValue 
	FROM dbo.fn_tbProfileRelationInit(@sTerminalID)
	RETURN
END