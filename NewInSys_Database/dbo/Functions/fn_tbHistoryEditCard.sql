﻿
CREATE FUNCTION [dbo].[fn_tbHistoryEditCard]
(
		@sDatabaseID VARCHAR(3),
		@sCardName VARCHAR(50),
		@sContent VARCHAR(MAX),
		@iCardID INT
)
	RETURNS @TempTable TABLE
(
		ItemName VARCHAR(MAX),
		OldValue VARCHAR(MAX),
		NewValue VARCHAR(MAX)
)
AS
BEGIN
	IF (dbo.isTagLength4(@sDatabaseID)=4)
	BEGIN
		INSERT INTO @TempTable(ItemName,OldValue,NewValue)
		SELECT
			T.ItemName,-- FROM tbItemList WHERE Tag = F.Tag),
			dbo.Bit2Str(@sDatabaseID, F.Tag, T.CardTagValue),
			dbo.Bit2Str(@sDatabaseID, F.Tag, F.TagValue)
		FROM dbo.fn_tbProfileTLV(@sCardName, @sContent) F
			JOIN dbo.fn_viewCard(@iCardID) T
				ON F.Tag = T.CardTag
		WHERE T.DatabaseId = @sDatabaseID AND T.CardId = @iCardID AND F.TagValue <>
		(
			SELECT CardTagValue
			FROM tbProfileCard
			WHERE
				CardID = @iCardID AND
				CardTag = F.Tag
		)
	END
	ELSE
	BEGIN
		INSERT INTO @TempTable(ItemName,OldValue,NewValue)
		SELECT
			T.ItemName,-- FROM tbItemList WHERE Tag = F.Tag),
			dbo.Bit2Str(@sDatabaseID, F.Tag, T.CardTagValue),
			dbo.Bit2Str(@sDatabaseID, F.Tag, F.TagValue)
		FROM dbo.fn_tbProfileTLV5(@sCardName, @sContent) F
			JOIN dbo.fn_viewCard(@iCardID) T
				ON F.Tag = T.CardTag
		WHERE T.DatabaseId = @sDatabaseID AND T.CardId = @iCardID AND F.TagValue <>
		(
			SELECT CardTagValue
			FROM tbProfileCard
			WHERE
				CardID = @iCardID AND
				CardTag = F.Tag
		)
	END
	RETURN
END
