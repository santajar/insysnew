﻿

CREATE FUNCTION [dbo].[sStringYYMMDDToDateTime](@sDate VARCHAR(12))
RETURNS DATETIME
BEGIN
	DECLARE @sDate1 VARCHAR(20)
	DECLARE @dtDateTime DATETIME
	IF @sDate <> '000000000000'
	BEGIN
	SET @sDate1 = '20' + SUBSTRING(@sDate,1,2) + '/' +
		SUBSTRING(@sDate,3,2) + '/' +
		SUBSTRING(@sDate,5,2) + ' ' +
		SUBSTRING(@sDate,7,2) + ':' +
		SUBSTRING(@sDate,9,2) + ':' +
		SUBSTRING(@sDate,11,2)
	SET @dtDateTime = CAST(@sDate1 AS DATETIME)
	END
RETURN @dtDateTime
END

