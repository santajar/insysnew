﻿CREATE FUNCTION [dbo].[iLastDownloadId](@sTerminalId VARCHAR(MAX))
	RETURNS INT
BEGIN
	RETURN (
		SELECT MAX(appList.Id) 
		FROM tbAppPackageEDCList app JOIN tbAppPackageEDCListTemp appList ON app.AppPackageName = appList.AppPackageName
			JOIN tbListTIDRemoteDownload terminalList ON app.AppPackId=terminalList.AppPackId
		WHERE TerminalId=@sTerminalId)
END