﻿

CREATE FUNCTION [dbo].[fn_tbProfileContentLoyaltyPool](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(4),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(MAX)
	)
BEGIN
INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
SELECT b.TerminalId, 
	a.LoyPoolName [Name], 
	a.LoyPoolTag Tag,
	'' ItemName, 
	a.LoyPoolTagLength TagLength,
	a.LoyPoolTagValue TagValue
FROM tbProfileLoyPool a WITH (NOLOCK) JOIN tbProfileTerminalList b WITH (NOLOCK)
ON a.DatabaseId=b.DatabaseId
WHERE b.TerminalId=@sTerminalID
RETURN
END

