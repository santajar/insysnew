﻿CREATE FUNCTION [dbo].[sBit2Str]
(
	@isBit BIT
)
RETURNS VARCHAR(10)
AS
BEGIN
	DECLARE @sBit VARCHAR(10)
	IF(@isBit = '0')
		SET @sBit = 'FALSE'
	IF(@isBit = '1')
		SET @sBit = 'TRUE'

	RETURN @sBit
END

