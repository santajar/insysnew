﻿CREATE FUNCTION [dbo].[IsPinPadInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iPinPadInit BIT
	SELECT @iPinPadInit=b.PinPad
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @iPinPadInit
END