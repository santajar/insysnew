﻿


CREATE FUNCTION [dbo].[sGetCAPKTLVStringLEN3](@sTag VARCHAR(5), @sValue VARCHAR(1000))
	RETURNS VARCHAR(1500)
AS
BEGIN
	DECLARE @sTemp VARCHAR(1500),
		@sLen VARCHAR(3),
		@sHexTag VARCHAR(8),
		@sHexLen VARCHAR(6)
	SET @sHexTag = dbo.sStringToHexString(@sTag)

	IF @sValue IS NOT NULL
	BEGIN
		IF @sTag = 'PK002' OR @sTag = 'PK003' OR @sTag = 'PK005'
			SET @sLen=LEN(@sValue) / 2
		ELSE
			SET @sLen=LEN(@sValue)

		IF LEN(@sLen)<3
			SELECT @sHexLen=dbo.sStringToHexString(RIGHT(dbo.sPadLeft(@sLen,5,'0'),3))
		ELSE
			SET @sHexLen = dbo.sStringToHexString(@sLen)

		IF @sTag = 'PK002' OR @sTag = 'PK003' OR @sTag = 'PK005'
			SET @sTemp = UPPER(@sHexTag) + @sHexLen + UPPER(@sValue)
		ELSE IF @sTag = 'PK001'
			SET @sTemp = UPPER(dbo.sStringToHexString('PK')) + 
				UPPER(@sHexTag) + @sHexLen + UPPER(dbo.sStringToHexString(@sValue))
		ELSE
			SET @sTemp = UPPER(@sHexTag) + @sHexLen + UPPER(dbo.sStringToHexString(@sValue))
	END
	ELSE
		SET @sTemp = UPPER(@sHexTag) + dbo.sStringToHexString('000')
	RETURN @sTemp
END