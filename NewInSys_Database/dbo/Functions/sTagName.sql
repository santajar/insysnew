﻿CREATE FUNCTION [dbo].[sTagName] (
			@sTag VARCHAR(5)
						)
RETURNS VARCHAR(20)
BEGIN
	DECLARE @sTagName VARCHAR(30)
	IF (@sTag = 'DC')
		SET @sTagName = ' Count'
	ELSE IF (@sTag = 'DE')
		SET @sTagName = ' Terminal'
	ELSE IF (@sTag = 'AA')
		SET @sTagName = ' Acquirer'
	ELSE IF (@sTag = 'AE')
		SET @sTagName = ' Issuer'
	ELSE IF (@sTag = 'AC')
		SET @sTagName = ' Card'
	ELSE IF (@sTag = 'AD')
		SET @sTagName = ' Relation'
	RETURN (SELECT @sTagName)
END