﻿CREATE FUNCTION [dbo].[iDownloadErrorCode](@sTerminalId VARCHAR(MAX),
	@iCurrDownloadId INT)
	RETURNS INT
BEGIN
	DECLARE @iErrorCode INT
	SET @iErrorCode=0

	DECLARE @iLastDownloadId INT
	SET @iLastDownloadId = dbo.iLastDownloadId(@sTerminalId)

	IF @iCurrDownloadId=@iLastDownloadId
		SET @iErrorCode=1

	RETURN (@iErrorCode)
END

