﻿CREATE FUNCTION [dbo].[fn_tbProfileContentCurrency](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(4),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(MAX)
	)
BEGIN

INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
SELECT  @sTerminalID,
		CurrencyName,
		CurrencyTag,
		ItemName,
		CurrencyTagLength, 
		CurrencyTagValue
FROM  (SELECT DatabaseId, ItemName, Tag
		  FROM tbItemList WITH (NOLOCK)
		  WHERE Tag LIKE 'CR%' ) a 
	INNER JOIN tbProfileCurrency b (NOLOCK) ON b.CurrencyTag = a.Tag AND a.DatabaseId = b.DatabaseID
	INNER JOIN tbProfileTerminalList c (NOLOCK) ON b.DatabaseID = c.DatabaseID
WHERE c.TerminalID = @sTerminalID
ORDER BY [CurrencyName], CurrencyTag
RETURN
END

