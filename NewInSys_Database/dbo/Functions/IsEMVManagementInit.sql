﻿

CREATE FUNCTION [dbo].[IsEMVManagementInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @EMVInitManagement BIT
	SELECT @EMVInitManagement=b.EMVInitManagement
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @EMVInitManagement
END