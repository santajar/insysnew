﻿CREATE FUNCTION [dbo].[sTerminalIdMessage](@sSessionTime VARCHAR(MAX))
RETURNS VARCHAR(8)
BEGIN
	DECLARE @sTemp VARCHAR(8)
	DECLARE @sTerminalId VARCHAR(16)
	DECLARE @sSqlStmt NVARCHAR(MAX)
	DECLARE @sParamDef NVARCHAR(MAX)
	SET @sParamDef = N'DECLARE @sTerminalId VARCHAR(16)'
	SET @sSqlStmt = N'SELECT BitValue AS VALUE FROM ##tempBitMap' + @sSessionTime + 
		' WHERE BitId=41'

	EXEC sp_executesql @sSqlStmt, @sParamDef, @sTerminalId = @sTerminalId OUTPUT

	EXEC spOthHexStringToString @sTemp, @sTerminalId OUTPUT
	RETURN (@sTerminalId)
END
