﻿CREATE FUNCTION [dbo].[sGenerateDownloadISOSendbySN](
	@sSN Varchar(25),
	@sTerminalId VARCHAR(8),
	@sProcCodeRecv VARCHAR(6),
	@sNII VARCHAR(4),
	@sAdress VARCHAR(4),
	@sBit11 VARCHAR(6)=NULL,
	@sBit48 VARCHAR(MAX)=NULL,
	@sBit60 VARCHAR(MAX),
	@sAppName VARCHAR(MAX),
	@iCounterApp INT,
	@iDownloadApp BIT = 0
	)
RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sISO VARCHAR(MAX), 
		@sBit12 VARCHAR(6), 
		@sBit13 VARCHAR(6),
		@iCountAppName int

	SET @sISO = dbo.sTPDU()
			+ @sAdress
			+ @sNII
			

	IF @sBit11 IS NOT NULL
	BEGIN
		SET @sISO = @sISO 
					+ dbo.sMTIResponse()
		-- BitMap
		IF @sBit48 IS NULL
			begin
			if @sAppName is not null 
			begin
				select @iCountAppName=count(ApplicationName) from tbBitMap where ApplicationName = @sAppName
				if @iCountAppName=1
					SET @sISO = @sISO 
					+ dbo.sBitMap(@sAppName)
				else
					SET @sISO = @sISO 
					+ dbo.sBitMap(NULL)
			end
			else
			SET @sISO = @sISO 
				+ dbo.sBitMap(NULL)
			end
		ELSE
		BEGIN
			IF @iDownloadApp=0
				SET @sISO = @sISO + dbo.sBitMap(@sAppName)
			ELSE
				SET @sISO = @sISO + dbo.sBitMap('DownloadApp')
			SELECT @sBit12 = dbo.sGetTimeHHMMSS()
			if @sProcCodeRecv= '990000' or @sProcCodeRecv = '990001' or @sProcCodeRecv='990010' or @sProcCodeRecv = '990011' OR @sProcCodeRecv = '990020' --proccode untuk download software
			begin
			SELECT @sBit13 = dbo.sGetDateMMDD()
			end
			else
			begin
			SELECT @sBit13 = dbo.sGetDateMMDDYY()
			end
		END

		-- Bit3
		SET @sISO = @sISO 
				+ dbo.sProcCodebySN(@sSN, @sProcCodeRecv, @iCounterApp)

		-- Bit11,12,13,24,39,41,48
		IF @sBit48 IS NOT NULL
		BEGIN
			SET @sISO = @sISO 
					+ @sBit11
					+ @sBit12
					+ @sBit13
					+ @sNII
					+ dbo.sStringToHexString('00')
					+ dbo.sStringToHexString(@sTerminalId)
					+ @sBit48
			IF @iDownloadApp=1
			BEGIN
				SET @sISO = @sISO 
					+ '0100' +dbo.sGenerateDownloadFillerbySN(@sSN, @iCounterApp)
			END
		END
		ELSE
		BEGIN
			SET @sISO = @sISO 
					+ @sBit11
					+ @sNII
					+ dbo.sStringToHexString('00')
					+ dbo.sStringToHexString(@sTerminalId)
		END

		-- Bit60
		IF SUBSTRING(@sBit60,1,4) = dbo.sStringToHexString('PK')
		BEGIN			
			SET @sISO = @sISO
				+ dbo.sPadLeft(LEN(@sBit60) / 2,4-LEN(LEN(@sBit60) / 2),'0')
			SET @sISO = @sISO 
				+ @sBit60
		END
		ELSE
		BEGIN
			IF ISNULL(@sBit60,'') <> ''
			BEGIN
				SET @sISO = @sISO + dbo.sPadLeft(LEN(@sBit60) / 2,4-LEN(LEN(@sBit60) / 2),'0')
				SET @sISO = @sISO + @sBit60
			END
		END
	END
	ELSE
	BEGIN
		IF ISNULL(@sBit60,'') <> ''
			SET @sISO = @sISO + dbo.sStringToHexString(@sBit60)
	END
	
	RETURN (@sISO)
END