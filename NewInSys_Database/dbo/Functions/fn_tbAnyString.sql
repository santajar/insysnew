﻿
CREATE FUNCTION [dbo].[fn_tbAnyString]
	(	@sMessage VARCHAR(MAX)	)
	RETURNS @TempTable TABLE
	(	String VARCHAR(MAX)	)
AS
BEGIN
	DECLARE @iIndex INT
	DECLARE @iMsgLen INT
	DECLARE @iSubLen INT

	SET @iIndex = 1
	SET @iMsgLen = DATALENGTH(@sMessage)

	WHILE @iIndex <= @iMsgLen
	BEGIN
		SET @iSubLen = CAST(SUBSTRING(@sMessage, @iIndex, 2) AS INT)
		INSERT INTO @TempTable (String) VALUES (SUBSTRING(@sMessage, @iIndex + 2, @iSubLen))
		SET @iIndex = @iIndex + @iSubLen + 2
	END

	RETURN
END