﻿CREATE FUNCTION [dbo].[fn_tbProfileContentSN](  
	@sTerminalID VARCHAR(8),  
	@sConditions VARCHAR(MAX)  
)  
RETURNS @tempTerminal TABLE(  
  TerminalId VARCHAR(8),  
  [Name] VARCHAR(50),  
  Tag VARCHAR(5),  
  ItemName VARCHAR(50),  
  TagLength VARCHAR(3),  
  TagValue VARCHAR(MAX)  
 )  
BEGIN  

DECLARE @sRDValue VARCHAR(50)
--SET @sTerminalId='114COBRN'

SELECT @sRDValue=C.TerminalTagValue
FROM 
	tbProfileTerminal C WITH (NOLOCK)
	JOIN tbProfileTerminalList D ON C.TerminalID=D.TerminalID
	JOIN tbItemList E WITH (NOLOCK) ON D.DatabaseID= E.DatabaseID
WHERE E.ItemName = 'Remote Download Name'
	AND C.TerminalID=@sTerminalId AND C.TerminalTag=E.Tag

IF ISNULL(@sRDValue,'')<>''
	INSERT INTO @tempTerminal (TerminalId,  
	  [Name],  
	  Tag,  
	  ItemName,  
	  TagLength,  
	  TagValue )  
	SELECT @sTerminalID TerminalId, SN_Name [Name], SN_Tag Tag, '' ItemName, LEN(SN_TagValue) TagLength, SN_TagValue TagValue   
	FROM tbProfileSN a WITH(NOLOCK) JOIN tbProfileTerminalList b WITH(NOLOCK) ON a.DatabaseID=b.DatabaseID
	WHERE b.TerminalID=@sTerminalID 


RETURN  
END
