﻿CREATE FUNCTION [dbo].[fn_tbInitFlag] (	@sMaxConn VARCHAR(50),
									@sInitPort VARCHAR(50),
									@sSoftwarePort VARCHAR(50),
									@sMaxConnFlag VARCHAR(64),
									@sInitPortFlag VARCHAR(64),
									@sSoftwarePortFlag VARCHAR(64)
								   )
RETURNS @tbInit TABLE (ItemName VARCHAR(50),
					   OldValue VARCHAR(64),
					   NewValue VARCHAR(64)
					  )
AS
BEGIN
	INSERT INTO @tbInit (ItemName, OldValue, NewValue)
	SELECT ItemName, Flag, CASE ItemName
							WHEN @sMaxConn THEN @sMaxConnFlag
							WHEN @sInitPort THEN @sInitPortFlag
							WHEN @sSoftwarePort THEN @sSoftwarePortFlag
						   END
	FROM tbControlFlag WITH (NOLOCK)
	WHERE (ItemName = @sMaxConn AND Flag <> @sMaxConnFlag) OR
		  (ItemName = @sInitPort AND Flag <> @sInitPortFlag) OR
		  (ItemName = @sSoftwarePort AND Flag <> @sSoftwarePortFlag)
	RETURN
END 
