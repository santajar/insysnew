﻿CREATE FUNCTION [dbo].[fn_tbUploadTagAAColumnName]()
RETURNS @AcqColumn TABLE(
	ColumnName VARCHAR(MAX))
BEGIN
	INSERT INTO @AcqColumn(ColumnName)
	SELECT ColumnName
	FROM tbUploadTagAA WITH (NOLOCK)
	GROUP BY ColumnName
	ORDER BY MIN(Id)
RETURN
END