﻿CREATE FUNCTION [dbo].[IsBankCodeInit]
(
	@sTerminalID VARCHAR(8)
)
RETURNS BIT
AS
BEGIN
	DECLARE @BankCodeInit BIT
	SELECT @BankCodeInit=b.BankCode
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDB b
	ON a.DatabaseID=b.DatabaseID
	WHERE a.TerminalId=@sTerminalId

	RETURN @BankCodeInit
END
