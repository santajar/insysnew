﻿CREATE FUNCTION [dbo].[fn_viewProfileTerminalIdCard](@sTerminalId VARCHAR(8))
RETURNS @viewProfileTerminalIdCard TABLE(
	TerminalId VARCHAR(8),
	CardName VARCHAR(50)
)
AS
BEGIN
INSERT INTO @viewProfileTerminalIdCard
SELECT TerminalID, RelationTagValue AS CardName
FROM tbProfileRelation WITH (NOLOCK)
WHERE TerminalID=@sTerminalId AND RelationTag = 'AD01' AND ISNULL(RelationTagValue,'')<>''
RETURN
END