﻿CREATE FUNCTION [dbo].[sReportFiturAcquirerTagValue]
(
	@sTerminalID VARCHAR(8),
	@sAcquirerName VARCHAR(25),
	@sTag VARCHAR(5)
)
RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sValue VARCHAR(MAX)
	SELECT @sValue=AcquirerTagValue FROM tbProfileAcquirer
	WHERE TerminalID=@sTerminalID AND AcquirerTag=@sTag AND AcquirerName=@sAcquirerName
	RETURN @sValue
END