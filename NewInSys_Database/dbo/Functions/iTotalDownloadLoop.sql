﻿CREATE FUNCTION [dbo].[iTotalDownloadLoop](@sTerminalId VARCHAR(8))
RETURNS INT
BEGIN
	RETURN (
		SELECT MAX(appList.Id)
		FROM tbAppPackageEDCListTemp appList WITH (NOLOCK) JOIN tbAppPackageEDCList app WITH (NOLOCK) ON app.AppPackageName=appList.AppPackageName
			JOIN tbListTIDRemoteDownload terminalList ON app.AppPackId=terminalList.AppPackId
		WHERE TerminalId=@sTerminalId)
END