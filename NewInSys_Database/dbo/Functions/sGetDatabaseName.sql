﻿CREATE FUNCTION [dbo].[sGetDatabaseName]( @iDatabaseID SMALLINT)
	RETURNS VARCHAR(100)
BEGIN
	DECLARE @sDbName VARCHAR(100)
	SELECT @sDbName = DatabaseName
	FROM tbProfileTerminalDb
	WHERE DatabaseID = @iDatabaseID 
	
	RETURN @sDbName 
END