﻿
CREATE FUNCTION [dbo].[sType](@sTag VARCHAR(5),
	@vObjectId INT, 
	@vIsNumeric INT, 
	@vIsAlNum INT)
RETURNS VARCHAR(5)
BEGIN
	DECLARE @vType VARCHAR(5)
	IF @vObjectId = 1
	BEGIN
		SELECT @vType=dbo.sTypeDefault(@sTag)
		IF @vType IS NULL OR @vType LIKE ''
		BEGIN
			IF (@vIsNumeric=1 AND @vIsAlNum=0) OR (@vIsNumeric=0 AND @vIsAlNum=0)
				SET @vType = 'S'
			ELSE IF @vIsNumeric=0 AND @vIsAlNum=1
				SET @vType = 'A'
		END
	END
	ELSE IF @vObjectId = 4
		SET @vType = 'S'
	ELSE
		SET @vType = 'N'
	RETURN @vType
END