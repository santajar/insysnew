﻿

CREATE FUNCTION [dbo].[fn_tbProfileRemoteDownload](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(4),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(MAX)
	)
BEGIN

INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
SELECT  @sTerminalID,
		RemoteDownloadName,
		b.RemoteDownloadTag,
		ItemName,
		RemoteDownloadTagLength, 
		RemoteDownloadTagValue
FROM  (SELECT DatabaseId, ItemName, Tag
		  FROM tbItemList 
		  WHERE Tag LIKE 'RD%' ) a 
	INNER JOIN tbProfileRemoteDownload b (NOLOCK) ON b.RemoteDownloadTag = a.Tag AND a.DatabaseId = b.DatabaseID
	INNER JOIN tbProfileTerminalList c (NOLOCK) ON b.DatabaseID = c.DatabaseID
WHERE c.TerminalID = @sTerminalID
ORDER BY RemoteDownloadName, b.RemoteDownloadTag
RETURN
END