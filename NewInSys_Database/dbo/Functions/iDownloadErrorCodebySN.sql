﻿
CREATE FUNCTION [dbo].[iDownloadErrorCodebySN] (@sSN VARCHAR(25),
	@iCurrDownloadId INT)
	RETURNS INT
BEGIN
	DECLARE @iErrorCode INT
	SET @iErrorCode=0

	DECLARE @iLastDownloadId INT
	SET @iLastDownloadId = dbo.iLastDownloadIdbySN(@sSN)

	IF @iCurrDownloadId=@iLastDownloadId
		SET @iErrorCode=1

	RETURN (@iErrorCode)
END