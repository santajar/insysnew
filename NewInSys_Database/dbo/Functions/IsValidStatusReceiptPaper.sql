﻿
-- ====================================================================== --
-- Function Name : [IsValidStatusRequestPaper]
-- Parameter	 : 
--		1. @sTerminalId, initialized terminalid
-- Return		 :
-- Description	 :
--		return boolean, 1 : valid TerminalId, 0 : invalid TerminalId
-- ====================================================================== --
CREATE FUNCTION [dbo].[IsValidStatusReceiptPaper](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	--RETURN (SELECT COUNT(*) FROM tbProfileTerminalList 
	--	WHERE TerminalId=@sTerminalId)
	RETURN (
		--SELECT (CASE WHEN COUNT(*)>0 THEN 1	ELSE 0 END) 
		--FROM tbInitTemp
		SELECT (CASE WHEN COUNT(*)>0 THEN 1	ELSE 0 END) 
		FROM tbProfileRequestPaper
		WITH(NOLOCK)
		WHERE TerminalId=@sTerminalId AND Status = 'WAITING')
END


GO


