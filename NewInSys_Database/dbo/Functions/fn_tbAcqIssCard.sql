﻿CREATE FUNCTION [dbo].[fn_tbAcqIssCard](@sTerminalID VARCHAR(8))
RETURNS @tbAcqIssCard TABLE(
	TerminalID VARCHAR(8),
	AcqName VARCHAR(25),
	Issname VARCHAR(25),
	CrdName VARCHAR(25)
)
AS
BEGIN
	DECLARE @maxtagid INT, @mINTagid INT, @step INT
	DECLARE @ad01 VARCHAR(25),@ad02 VARCHAR(25),@ad03 VARCHAR(25), @tag VARCHAR(5),@terminalid VARCHAR(8)

	DECLARE @relation TABLE(
		RelationTagID BIGINT,
		TerminalID VARCHAR(8),
		RelationTag VARCHAR(5),
		RelationTagValue VARCHAR(50)
	)
	INSERT INTO @relation
	SELECT RelationTagID,TerminalID,RelationTag,RelationTagValue  
	FROM tbProfileRelation WITH (NOLOCK)
	WHERE terminalid=@sTerminalID
	order by relationtagid desc
	SELECT @maxtagid=MAX(relationtagid) FROM @relation 
	SELECT @mINTagid=MIN(relationtagid) FROM @relation
	SET @step=1

	WHILE @maxtagid>@mINTagid
	BEGIN
		SELECT @tag=relationtag FROM @relation WHERE relationtagid=@maxtagid
		SELECT @terminalid=terminalid FROM @relation WHERE relationtagid=@maxtagid
		IF @tag IN ('AD03','AD003')
			SELECT @ad03=relationtagvalue FROM @relation WHERE relationtagid=@maxtagid
		ELSE IF @tag IN ('AD02','AD002')
			SELECT @ad02=relationtagvalue FROM @relation WHERE relationtagid=@maxtagid
		ELSE IF @tag IN ('AD01','AD001')
			SELECT @ad01=relationtagvalue FROM @relation WHERE relationtagid=@maxtagid

		SET @step = @step + 1
		SET @maxtagid = @maxtagid-1
		IF @step=3
		BEGIN
			INSERT INTO @tbAcqIssCard(terminalid,AcqName,IssName,CrdName)
			VALUES (@terminalid,@ad03,@ad02,@ad01)
			SET @step=1
		END
	END
	RETURN
END