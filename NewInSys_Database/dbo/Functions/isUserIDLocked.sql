﻿-- =============================================
-- Author:		<Author: Tedie Scorfia>
-- Create date: <Create Date: 2 juli 2015>
-- Description:	<Description: Get Max Length Password>
-- =============================================
CREATE FUNCTION [dbo].[isUserIDLocked]( @sUserID VARCHAR(50))
RETURNS BIT
AS
BEGIN
	RETURN (SELECT Locked
			FROM tbUserLogin 
			WHERE UserID=@sUserID)

END