﻿CREATE FUNCTION [dbo].[fn_viewRelationTagValueIsNull](@sTerminalId VARCHAR(8))
RETURNS @viewRelationTLVIsNull TABLE(
	TerminalId VARCHAR(8),
	RelationTagID BIGINT
)
AS
BEGIN
INSERT INTO @viewRelationTLVIsNull
SELECT TerminalId, RelationTagID
FROM dbo.fn_viewRelationTLV(@sTerminalId)
WHERE TagValue IS NULL AND Tag = 'AD01'
UNION ALL
SELECT TerminalId, RelationTagID + 1
FROM dbo.fn_viewRelationTLV(@sTerminalId)
WHERE TagValue IS NULL AND Tag = 'AD01'
UNION ALL
SELECT TerminalId, RelationTagID + 2
FROM dbo.fn_viewRelationTLV(@sTerminalId)
WHERE TagValue IS NULL AND Tag = 'AD01'
ORDER BY RelationTagID
RETURN
END