﻿CREATE FUNCTION [dbo].[sMigrateContent](@sTerminalId VARCHAR(8))
RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @sContent VARCHAR(MAX)
	SELECT @sContent=[Content]
	FROM dbtms..TerminalList
	WHERE TerminalName=@sTerminalId
	RETURN (SUBSTRING(@scontent,2,LEN(@sContent)-2))
END