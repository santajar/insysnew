﻿CREATE FUNCTION [dbo].[fn_viewInitAll](@sTerminalId VARCHAR(8))
RETURNS @viewInitAll TABLE(
	TerminalId VARCHAR(8),
	Tag VARCHAR(5),
	TotalRowSent INT,
	Total INT
)
AS
BEGIN
INSERT INTO @viewInitAll
SELECT 
	A.TerminalId, 
	A.Tag, 
	B.TotalRowSent, 
	COUNT(A.Tag) AS Total
FROM 
	tbInit AS A WITH (NOLOCK) INNER JOIN 
	dbo.fn_viewInit(@sTerminalId) AS B ON A.TerminalId = B.TerminalId AND A.Tag = B.Tag
GROUP BY A.Tag, A.TerminalId, B.TotalRowSent
RETURN
END