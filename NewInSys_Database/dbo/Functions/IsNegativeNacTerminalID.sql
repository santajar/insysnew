﻿
CREATE FUNCTION dbo.IsNegativeNacTerminalID(@sTerminalid varchar(8))
RETURNS BIT
BEGIN 
	DECLARE @bReturn BIT
	SELECT @bReturn = (CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END)
	FROM tbProfileAcquirer WITH(NOLOCK)
	WHERE TerminalID = @sTerminalid
		AND AcquirerTag IN ('AA06','AA07')
		AND AcquirerName IN (
			SELECT AcquirerName 
			FROM tbProfileAcquirer WITH(NOLOCK)
			WHERE TerminalID = @sTerminalid
				AND AcquirerName IN ('BCA','DEBIT','FLAZZBCA','LOYALTY')
				AND AcquirerTag IN ('AA05')
				AND (AcquirerTagValue NOT LIKE 'D%X%'
					OR AcquirerTagValue NOT LIKE 'C%X%'
					OR AcquirerTagValue NOT LIKE 'E%X%'
					OR AcquirerTagValue NOT LIKE 'L%X%')
		)
		AND AcquirerTagValue IN (SELECT NacNumber FROM tbNACNegativeList)
	RETURN @bReturn
END
