﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Dec 3, 2010
-- Description:	Create a temporary table holding Content of specified 
--			TerminalID stored in new row for each tag, with 3 digit length
-- =============================================
CREATE FUNCTION [dbo].[fn_tbCAPKTLV5](
		@sTerminalID VARCHAR(8),
		@sContent VARCHAR(MAX))
	RETURNS @TempTable TABLE(
		RowId		INT IDENTITY(1,1),
		TerminalID	VARCHAR(8),
		[Name]		VARCHAR(50),
		Tag			VARCHAR(5),
		TagLength		VARCHAR(3),
		TagValue		VARCHAR(MAX)
	)
AS
BEGIN
	DECLARE @sTag			VARCHAR(5)
	DECLARE @sLenValue		VARCHAR(3)
	DECLARE @sValue			VARCHAR(MAX)
	DECLARE @iIndex			INT
	DECLARE @iLenContent	INT
	DECLARE @iStartPos		INT

	DECLARE @sName			VARCHAR(50)

	--Create temporary table holding Content of specified TerminalID
	SET @iLenContent = DATALENGTH(@sContent)
	SET @iStartPos=1

	SET @iIndex=@iStartPos

	--Get all tags in Content, break each tag into one row and insert to TempTable
	DECLARE @iLenValue INT
	DECLARE @sTempTag VARCHAR(2)
	
	WHILE @iIndex<=@iLenContent
		BEGIN
			SELECT @sTag=SUBSTRING(@sContent,@iIndex,5)
			SET @iIndex=@iIndex+5
			
			IF @sTag = 'PK003'
			BEGIN
				SELECT @sLenValue=SUBSTRING(@sContent,@iIndex,3)
				SET @iIndex=@iIndex+3
			END
			ELSE
			BEGIN
				SELECT @sLenValue=SUBSTRING(@sContent,@iIndex,2)
				SET @iIndex=@iIndex+2
			END

			SET @iLenValue=@sLenValue

			SELECT @sValue=SUBSTRING(@sContent,@iIndex,@iLenValue)
			SET @iIndex=@iIndex+@iLenValue

			IF( SUBSTRING(@sTag,3,3) = '001'AND @sTag <> 'DC01' )
				SET @sName = @sValue			
			IF (@sTag <>'"')
				INSERT INTO @TempTable(TerminalID, [Name], Tag, TagLength, TagValue) VALUES(@sTerminalID,@sName,@sTag,@sLenValue,@sValue)
		END
	RETURN
END