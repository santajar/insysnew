﻿
CREATE FUNCTION [dbo].[IsInitPermission]()
	RETURNS BIT
AS
BEGIN
	DECLARE @isPermissionInit BIT

	SELECT @isPermissionInit = CASE WHEN Flag = 0 THEN 0
			ELSE 1
			END
	FROM tbControlFlag
	WHERE ItemName = 'AllowInit'
	
	RETURN @isPermissionInit
END

