﻿CREATE FUNCTION [dbo].[fn_viewCardTLV](@iCardID INT)
RETURNS @viewCardId TABLE(
	CardID VARCHAR(8),
	Tag VARCHAR(5),
	TagLength INT,
	TagValue VARCHAR(150)
)
AS
BEGIN
INSERT INTO @viewCardId
SELECT
	CardID, 
	CardTag AS Tag, 
	CardTagLength AS TagLength, 
	CardTagValue AS TagValue
FROM
	tbProfileCard WITH (NOLOCK)
WHERE CardID = @iCardID
RETURN
END