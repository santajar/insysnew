﻿CREATE FUNCTION [dbo].[fn_viewAID]()  
RETURNS @viewAID TABLE(  
 AIDTagID VARCHAR(8),  
 DatabaseId SMALLINT,  
 AIDName VARCHAR(50)  
)  
AS  
BEGIN  
INSERT INTO @viewAID 
	SELECT AIDTagID = MIN(AIDId), DatabaseID, AIDName 
	FROM tbProfileAID WITH (NOLOCK)
	--WHERE DatabaseID = CASE WHEN @DatabaseID = '' THEN DatabaseID ELSE @DatabaseID END
		GROUP BY DatabaseID, AIDName
		ORDER BY AIDName
RETURN  
END