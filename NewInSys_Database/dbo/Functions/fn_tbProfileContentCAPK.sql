﻿
CREATE FUNCTION [dbo].[fn_tbProfileContentCAPK](
	@sTerminalID VARCHAR(8),
	@sConditions VARCHAR(MAX)
	)
RETURNS @tempTerminal TABLE(
		TerminalId VARCHAR(8),
		[Name] VARCHAR(15),
		Tag VARCHAR(5),
		ItemName VARCHAR(50),
		TagLength VARCHAR(3),
		TagValue VARCHAR(MAX)
	)
BEGIN
INSERT INTO @tempTerminal (TerminalId,
		[Name],
		Tag,
		ItemName,
		TagLength,
		TagValue )
SELECT b.TerminalId, 
	a.CAPKIndex [Name], 
	a.CAPKTag Tag,
	'' ItemName, 
	a.CAPKTaglength TagLength,
	a.CAPKTagValue TagValue
FROM tbProfileCAPK a WITH (NOLOCK) JOIN tbProfileTerminalList b WITH (NOLOCK)
ON a.DatabaseId=b.DatabaseId
WHERE b.TerminalId=@sTerminalID
--ORDER BY a.CAPKTag
RETURN
END
