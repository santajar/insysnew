﻿CREATE FUNCTION [dbo].[fn_tbProfileTLV5](
		@sTerminalID VARCHAR(8),
		@sContent VARCHAR(MAX))
	RETURNS @TempTable TABLE(
		RowId		INT IDENTITY(1,1),
		TerminalID	VARCHAR(8),
		[Name]		VARCHAR(50),
		Tag			VARCHAR(5),
		TagLength		VARCHAR(3),
		TagValue		VARCHAR(8000)
	)
AS
BEGIN
	DECLARE @sTag			VARCHAR(5)
	DECLARE @sLenValue		VARCHAR(3)
	DECLARE @sValue			VARCHAR(8000)
	DECLARE @iIndex			INT
	DECLARE @iLenContent	INT
	DECLARE @iStartPos		INT

	DECLARE @sName			VARCHAR(50)

	--Create temporary table holding Content of specified TerminalID
	SET @iLenContent = DATALENGTH(@sContent)
	SET @iStartPos=1

	SET @iIndex=@iStartPos

	--Get all tags in Content, break each tag into one row and insert to TempTable
	WHILE @iIndex<=@iLenContent
		BEGIN
			SELECT @sTag=SUBSTRING(@sContent,@iIndex,5)
			SET @iIndex=@iIndex+5
			IF @sTag='AE037' OR @sTag='AE038' OR @sTag = 'TL011' OR @sTag = 'PK003'
				BEGIN
					SELECT @sLenValue=SUBSTRING(@sContent,@iIndex,3)
					SET @iIndex=@iIndex+3
				END
			ELSE
			IF @sTag='AE037'
			BEGIN
				SELECT @sLenValue=SUBSTRING(@sContent,@iIndex,3)
				SET @iIndex=@iIndex+3
			END
			ELSE
			BEGIN
					SELECT @sLenValue=SUBSTRING(@sContent,@iIndex,2)
					SET @iIndex=@iIndex+2
				END

			DECLARE @iLenValue INT
			SET @iLenValue=@sLenValue

			SELECT @sValue=SUBSTRING(@sContent,@iIndex,@iLenValue)
			SET @iIndex=@iIndex+@sLenValue

			DECLARE @sTempTag VARCHAR(2)
			
			IF( SUBSTRING(@sTag,3,3) = '001'AND @sTag <> 'DC001' )
				SET @sName = @sValue			
			IF (@sTag <>'"')
				INSERT INTO @TempTable(TerminalID, [Name], Tag, TagLength, TagValue) VALUES(@sTerminalID,@sName,@sTag,@sLenValue,@sValue)
		END
	RETURN
END