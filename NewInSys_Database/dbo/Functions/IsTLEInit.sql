﻿CREATE FUNCTION [dbo].[IsTLEInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @TLEInit BIT
	SELECT @TLEInit=b.TLEInit
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @TLEInit
END


