﻿CREATE FUNCTION [dbo].[IsNewInit](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	DECLARE @iNewInit BIT
	SELECT @iNewInit=b.NewInit
	FROM tbProfileTerminalList a JOIN tbProfileTerminalDb b
	ON a.DatabaseId=b.DatabaseId
	WHERE a.TerminalId=@sTerminalId

	RETURN @iNewInit
END