﻿CREATE FUNCTION [dbo].[sGetAppContentCounter] (@sTerminalID VARCHAR(8), @iCounter INT)
	RETURNS VARCHAR(MAX)
BEGIN
DECLARE @sContent VARCHAR(MAX)
SELECT @sContent = applist.AppPackageContent
FROM tbAppPackageEdcList app JOIN 
	tbAppPackageEDCListTemp applist ON app.AppPackageName = applist.AppPackageName JOIN 
	tbListTIDRemoteDownload list ON app.AppPackId = list.AppPackID
WHERE list.TerminalID = @sTerminalID AND applist.Id = @iCounter

RETURN @sContent
END

