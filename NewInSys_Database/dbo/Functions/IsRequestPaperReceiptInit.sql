﻿CREATE FUNCTION [dbo].[IsRequestPaperReceiptInit]
(
@sTerminalID VARCHAR(8)
)
RETURNS INT
AS
BEGIN
DECLARE @RequestPaperReceiptInit BIT
SELECT @RequestPaperReceiptInit=b.RequestPaperReceipt
FROM tbProfileTerminalList a JOIN tbProfileTerminalDB b
ON a.DatabaseID= b.DatabaseID
WHERE a.TerminalID=@sTerminalID
RETURN @RequestPaperReceiptInit
END