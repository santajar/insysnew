﻿CREATE TABLE [dbo].[tbProfileProductCode] (
    [ProductCodeID]                INT          IDENTITY (1, 1) NOT NULL,
    [DatabaseID]                   INT          CONSTRAINT [DF_tbProfileProductCode_DatabaseID] DEFAULT ((0)) NULL,
	[ProductCode]				   VARCHAR(50)  NOT NULL,
    [ProductCodeTag]               VARCHAR (5)  NOT NULL,
    [ProductCodeLengthOfTagLength] INT          NOT NULL,
    [ProductCodeTagLength]         INT          NOT NULL,
    [ProductCodeTagValue]          VARCHAR (50) NULL,
    CONSTRAINT [PK_tbProfileProductCode_1] PRIMARY KEY CLUSTERED ([ProductCodeID] ASC)
);

