﻿CREATE TABLE [dbo].[tbReportFitur] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [TerminalID]  VARCHAR (8)  NULL,
    [Category]    VARCHAR (50) NULL,
    [Merch1]      VARCHAR (23) NULL,
    [Merch2]      VARCHAR (23) NULL,
    [Merch3]      VARCHAR (23) NULL,
    [Acquirer]    VARCHAR (15) NULL,
    [TID]         VARCHAR (8)  NULL,
    [MID]         VARCHAR (15) NULL,
    [T1]          VARCHAR (15) NULL,
    [T2]          VARCHAR (15) NULL,
    [ManualEntry] BIT          NULL,
    [Offline]     BIT          NULL,
    [Adjust]      BIT          NULL,
    [Refund]      BIT          NULL,
    [CardVer]     BIT          NULL,
    CONSTRAINT [PK_tbReportFitur] PRIMARY KEY CLUSTERED ([Id] ASC)
);



