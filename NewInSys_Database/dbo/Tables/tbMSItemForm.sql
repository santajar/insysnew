﻿CREATE TABLE [dbo].[tbMSItemForm] (
    [FormID]   INT          NOT NULL,
    [FormName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tbMSItemForm] PRIMARY KEY CLUSTERED ([FormID] ASC)
);

