﻿CREATE TABLE [dbo].[tbStockRegion] (
    [RegionID]   INT           NOT NULL,
    [RegionName] VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([RegionID] ASC) WITH (FILLFACTOR = 70),
    UNIQUE NONCLUSTERED ([RegionName] ASC) WITH (FILLFACTOR = 70)
);

