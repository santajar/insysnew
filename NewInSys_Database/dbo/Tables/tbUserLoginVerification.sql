﻿CREATE TABLE [dbo].[tbUserLoginVerification] (
    [UserID]   NVARCHAR (10) NOT NULL,
    [Nama]     NVARCHAR (40) NULL,
    [Password] NVARCHAR (10) NOT NULL,
    [UserRole] NVARCHAR (20) NOT NULL,
    CONSTRAINT [PK_tbUserLoginVerification] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

