CREATE TABLE [dbo].[tbAuditTerminalSNEcho] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [TerminalSN]  VARCHAR (25) NOT NULL,
    [TerminalID]  VARCHAR (10) NULL,
    [AppName]     VARCHAR (50) NULL,
    [CommsID]     INT          CONSTRAINT [DF__tmp_ms_xx__Comms__03808CD4] DEFAULT ((0)) NOT NULL,
    [DateCreated] DATETIME     CONSTRAINT [DF__tmp_ms_xx__DateC__0474B10D] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK__tbAuditT__6A72B00B7B7E4DA1] PRIMARY KEY CLUSTERED ([Id] ASC) ON [Data_Audit]
);










GO
CREATE NONCLUSTERED INDEX [Index-AuditTerminalSNEcho]
    ON [dbo].[tbAuditTerminalSNEcho]([Id] ASC, [TerminalSN] ASC, [TerminalID] ASC, [AppName] ASC, [CommsID] ASC, [DateCreated] ASC)
    ON [Data_Audit];

