﻿CREATE TABLE [dbo].[tbProfileGPRS] (
    [GPRSTagId]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [DatabaseID]            INT           NOT NULL,
    [GPRSName]              VARCHAR (15)  NOT NULL,
    [GPRSTag]               VARCHAR (5)   NOT NULL,
    [GPRSLengthOfTagLength] INT           NOT NULL,
    [GPRSTagLength]         INT           NOT NULL,
    [GPRSTagValue]          VARCHAR (150) NOT NULL,
    [Description]           VARCHAR (150) NULL,
    CONSTRAINT [PK_tbProfileGPRS] PRIMARY KEY CLUSTERED ([GPRSTagId] ASC, [DatabaseID] ASC, [GPRSName] ASC, [GPRSTag] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON) ON [Data]
);








GO
CREATE NONCLUSTERED INDEX [Id_DatabaseId_Name]
    ON [dbo].[tbProfileGPRS]([GPRSTagId] ASC, [DatabaseID] ASC, [GPRSName] ASC, [GPRSTag] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

