﻿CREATE TABLE [dbo].[tbStockPLCPart] (
    [ID]             INT           IDENTITY (7000, 1) NOT NULL,
    [SerialNumber]   VARCHAR (255) NULL,
    [TerminalID]     INT           NULL,
    [PartTemplateID] INT           NULL,
    [UserCreationID] VARCHAR (10)  NULL,
    [DateCreation]   DATETIME      NULL,
    [Timezone]       VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_PLCPartPartTemplate] FOREIGN KEY ([PartTemplateID]) REFERENCES [dbo].[tbStockPLCPartTemplate] ([ID]),
    CONSTRAINT [FK_PLCPartTerminal] FOREIGN KEY ([TerminalID]) REFERENCES [dbo].[tbStockPLCTerminal] ([ID]),
    CONSTRAINT [FK_PLCPartUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

