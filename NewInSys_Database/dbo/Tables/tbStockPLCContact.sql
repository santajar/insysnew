﻿CREATE TABLE [dbo].[tbStockPLCContact] (
    [ID]             INT           IDENTITY (6000, 1) NOT NULL,
    [Name]           VARCHAR (255) NOT NULL,
    [Email]          VARCHAR (255) NULL,
    [PhoneNumber]    VARCHAR (255) NULL,
    [MobilePhone]    VARCHAR (255) NULL,
    [SiteID]         INT           NULL,
    [UserCreationID] VARCHAR (10)  NULL,
    [DateCreation]   DATETIME      NULL,
    [Timezone]       VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_PLCContactPLCSitie] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[tbStockPLCSite] ([ID]),
    CONSTRAINT [FK_PLCContactUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

