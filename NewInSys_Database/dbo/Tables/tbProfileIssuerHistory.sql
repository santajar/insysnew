﻿CREATE TABLE [dbo].[tbProfileIssuerHistory] (
    [IssuerTagID]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [TerminalID]              VARCHAR (8)   NOT NULL,
    [IssuerName]              VARCHAR (15)  NOT NULL,
    [IssuerTag]               VARCHAR (5)   NOT NULL,
    [IssuerLengthOfTagLength] INT           NOT NULL,
    [IssuerTagLength]         INT           NOT NULL,
    [IssuerTagValue]          VARCHAR (150) NULL,
    [Description]             VARCHAR (255) NULL,
    [LastUpdate]              VARCHAR (20)  NULL
);



