﻿CREATE TABLE [dbo].[tbUserPasswordHistory] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [CreateDate] DATETIME      NULL,
    [UserID]     VARCHAR (25)  NULL,
    [Password]   VARCHAR (100) NULL
);

