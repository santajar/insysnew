﻿CREATE TABLE [dbo].[tbProfileCAPK] (
    [CAPKId]                SMALLINT       IDENTITY (1, 1) NOT NULL,
    [DatabaseId]            SMALLINT       NOT NULL,
    [CAPKIndex]             VARCHAR (200)  NULL,
    [CAPKTag]               VARCHAR (5)    NOT NULL,
    [CAPKLengthOfTagLength] SMALLINT       NOT NULL,
    [CAPKTagLength]         SMALLINT       NOT NULL,
    [CAPKTagValue]          VARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_tbProfileCAPK] PRIMARY KEY CLUSTERED ([CAPKId] ASC) WITH (FILLFACTOR = 90) ON [Data]
) ON [Data];










GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Auto increment Id', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbProfileCAPK', @level2type = N'COLUMN', @level2name = N'CAPKId';


GO
CREATE NONCLUSTERED INDEX [Id_DatabaseId_Name_Tag]
    ON [dbo].[tbProfileCAPK]([CAPKId] ASC, [DatabaseId] ASC, [CAPKIndex] ASC, [CAPKTag] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

