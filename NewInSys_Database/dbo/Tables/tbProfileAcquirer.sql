﻿CREATE TABLE [dbo].[tbProfileAcquirer] (
    [AcquirerTagID]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [TerminalID]                VARCHAR (8)   NOT NULL,
    [AcquirerName]              VARCHAR (15)  NOT NULL,
    [AcquirerTag]               VARCHAR (5)   NOT NULL,
    [AcquirerLengthOfTagLength] INT           CONSTRAINT [DF_tbProfileAcquirer_AcquirerLengthOfTagLength] DEFAULT ((0)) NOT NULL,
    [AcquirerTagLength]         INT           CONSTRAINT [DF_tbProfileAcquirer_AcquirerTagLength] DEFAULT ((0)) NOT NULL,
    [AcquirerTagValue]          VARCHAR (150) NULL,
    [Description]               VARCHAR (255) NULL,
    CONSTRAINT [PK_Acquirer] PRIMARY KEY CLUSTERED ([AcquirerTagID] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON) ON [Data],
    CONSTRAINT [FK_tbProfileAcquirer_tbProfileTerminalList] FOREIGN KEY ([TerminalID]) REFERENCES [dbo].[tbProfileTerminalList] ([TerminalID]) ON DELETE CASCADE
);










GO



GO
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Nov 13, 2012
-- Description:	Repair the incorrect value on TerminalTagLength field to fix the DataUpload module
-- =============================================
CREATE TRIGGER [dbo].[AfterInsertRepairAcquirerTagLength] 
   ON  dbo.tbProfileAcquirer
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
    DECLARE @sTerminalID VARCHAR(8),
		@sAcquirerName VARCHAR(15),
		@sAcquirerTag VARCHAR(5),
		@iAcquirerLengthOfTagLength INT,
		@iAcquirerTagLength INT,
		@sAcquirerTagValue VARCHAR(150)
		
    SELECT @sTerminalID = TerminalID,
		@sAcquirerName = AcquirerName,
		@sAcquirerTag = AcquirerTag,
		@iAcquirerLengthOfTagLength = AcquirerLengthOfTagLength,
		@iAcquirerTagLength = AcquirerTagLength,
		@sAcquirerTagValue = AcquirerTagValue
	FROM inserted
    
	IF @iAcquirerTagLength != DATALENGTH(@sAcquirerTagValue)
		UPDATE tbProfileAcquirer
		SET AcquirerTagLength = DATALENGTH(@sAcquirerTagValue), 
			AcquirerLengthOfTagLength = LEN(DATALENGTH(@sAcquirerTagValue))
		WHERE TerminalID = @sTerminalID
			AND AcquirerName = @sAcquirerName
			AND AcquirerTag = @sAcquirerTag
			AND AcquirerTagValue = @sAcquirerTagValue
	
END
GO
CREATE NONCLUSTERED INDEX [TerminalAcquirer]
    ON [dbo].[tbProfileAcquirer]([TerminalID] ASC, [AcquirerName] ASC, [AcquirerTag] ASC, [AcquirerTagValue] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

