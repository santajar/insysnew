﻿CREATE TABLE [dbo].[tbProfileLoyPool] (
    [LoyPoolTagID]             INT           IDENTITY (1, 1) NOT NULL,
    [LoyPoolName]              VARCHAR (50)  NOT NULL,
    [DatabaseID]               SMALLINT      NOT NULL,
    [LoyPoolTag]               VARCHAR (5)   NOT NULL,
    [LoyPoolLengthOfTagLength] INT           NOT NULL,
    [LoyPoolTagLength]         INT           NOT NULL,
    [LoyPoolTagValue]          VARCHAR (256) NOT NULL,
    [Description]              VARCHAR (255) NULL,
    CONSTRAINT [PK_tbProfileLoyPool] PRIMARY KEY CLUSTERED ([LoyPoolTagID] ASC)
);



