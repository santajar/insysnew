﻿CREATE TABLE [dbo].[tbProfileSoftware] (
    [Id]           INT          IDENTITY (1, 1) NOT NULL,
    [DatabaseId]   SMALLINT     NOT NULL,
    [SoftwareName] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_tbProfileSoftware] PRIMARY KEY CLUSTERED ([Id] ASC) ON [Data],
    CONSTRAINT [FK_tbProfileSoftware_tbBitMap] FOREIGN KEY ([SoftwareName]) REFERENCES [dbo].[tbBitMap] ([ApplicationName]),
    CONSTRAINT [FK_tbProfileSoftware_tbProfileTerminalDB] FOREIGN KEY ([DatabaseId]) REFERENCES [dbo].[tbProfileTerminalDB] ([DatabaseID])
);








GO
CREATE NONCLUSTERED INDEX [DatabaseId_SoftwareId]
    ON [dbo].[tbProfileSoftware]([Id] ASC, [DatabaseId] ASC, [SoftwareName] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

