﻿CREATE TABLE [dbo].[tbMenuNewInsys] (
    [ID]       INT          IDENTITY (1, 1) NOT NULL,
    [MenuName] VARCHAR (50) NOT NULL,
    [Status]   BIT          NOT NULL
);

