﻿CREATE TABLE [dbo].[tbEmsMsAcquirer] (
    [EMSAcqId]           INT          IDENTITY (1, 1) NOT NULL,
    [EMSXMLVersion]      VARCHAR (50) NOT NULL,
    [EMSXMLColumnName]   VARCHAR (50) NOT NULL,
    [EMSXMLAcquirerName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tbMSAcquirerEMS] PRIMARY KEY CLUSTERED ([EMSAcqId] ASC)
);



