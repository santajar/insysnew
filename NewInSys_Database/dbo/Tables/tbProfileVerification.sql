﻿CREATE TABLE [dbo].[tbProfileVerification] (
    [EdcType]        NVARCHAR (10) NULL,
    [DbVersion]      NVARCHAR (10) NULL,
    [TerminalID]     NVARCHAR (8)  NOT NULL,
    [TicketNumber]   NVARCHAR (15) NOT NULL,
    [TID]            NVARCHAR (8)  NOT NULL,
    [MerchantName]   NVARCHAR (30) NULL,
    [Address]        NVARCHAR (30) NULL,
    [City]           NVARCHAR (30) NULL,
    [ProfileCreated] NVARCHAR (50) NULL,
    [SerialNumber]   NVARCHAR (15) NULL,
    [TechnicianName] NVARCHAR (50) NULL,
    [Remarks]        TEXT          NULL,
    [SPK]            NVARCHAR (10) NULL,
    [WO]             NVARCHAR (10) NULL,
    [Operator]       NCHAR (10)    NULL,
    [Status]         NCHAR (10)    NULL,
    [LogDate]        DATETIME      NULL,
    CONSTRAINT [PK_tbProfileVerification] PRIMARY KEY CLUSTERED ([TerminalID] ASC, [TicketNumber] ASC)
);

