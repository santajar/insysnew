﻿CREATE TABLE [dbo].[tbItemForm] (
    [DatabaseID] SMALLINT     NOT NULL,
    [FormID]     SMALLINT     NOT NULL,
    [FormName]   VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_tbItemForm] PRIMARY KEY CLUSTERED ([DatabaseID] ASC, [FormID] ASC)
);

