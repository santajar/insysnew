﻿CREATE TABLE [dbo].[tbUploadLog] (
    [Id]          BIGINT       IDENTITY (1, 1) NOT NULL,
    [Time]        DATETIME     NULL,
    [TerminalId]  VARCHAR (8)  NULL,
    [Description] VARCHAR (50) NULL,
    [Remarks]     VARCHAR (50) NULL,
    CONSTRAINT [PK_tbUploadLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);



