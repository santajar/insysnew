﻿CREATE TABLE [dbo].[tbUserLoginDomain] (
    [UserDomain]   VARCHAR (30) NOT NULL,
    [UserName]     VARCHAR (25) NULL,
    [Email]        VARCHAR (50) NULL,
    [UserIDAccess] VARCHAR (20) NULL,
    [Actived]      BIT          DEFAULT ((0)) NOT NULL,
    [Approved]     BIT          DEFAULT ((0)) NOT NULL,
    [ApprovedDate] DATETIME     NULL,
    [CreateDate]   DATETIME     DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tbUserLoginDomain] PRIMARY KEY CLUSTERED ([UserDomain] ASC) WITH (FILLFACTOR = 90)
);

