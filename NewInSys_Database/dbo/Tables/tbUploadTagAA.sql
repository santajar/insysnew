﻿CREATE TABLE [dbo].[tbUploadTagAA] (
    [Id]           SMALLINT     IDENTITY (1, 1) NOT NULL,
    [AcquirerName] VARCHAR (50) NOT NULL,
    [Tag]          VARCHAR (5)  NOT NULL,
    [ColumnName]   VARCHAR (50) NOT NULL,
    [Mandatory]    BIT          CONSTRAINT [DF_tbUploadTagDE_Mandatory] DEFAULT ((1)) NOT NULL,
    [SourceColumn] SMALLINT     NULL,
    [Template]     VARCHAR (50) NULL,
    CONSTRAINT [PK_tbUploadTagAA] PRIMARY KEY CLUSTERED ([Id] ASC)
);



