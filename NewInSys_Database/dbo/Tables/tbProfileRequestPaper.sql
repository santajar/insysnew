﻿CREATE TABLE [dbo].[tbProfileRequestPaper] (
    [RequestID]       INT          IDENTITY (1, 1) NOT NULL,
    [TerminalID]      VARCHAR (8)  NULL,
    [Software]        VARCHAR (15) NULL,
    [SerialNumber]    VARCHAR (25) NULL,
    [RequestTime]     DATETIME     NULL,
    [ReceiptTime]     DATETIME     NULL,
    [NumberOfRequest] INT          NULL,
    [Status]          VARCHAR (25) NULL,
    CONSTRAINT [PK_tbProfileRequestPaper] PRIMARY KEY CLUSTERED ([RequestID] ASC) WITH (FILLFACTOR = 90)
);



