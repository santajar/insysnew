﻿CREATE TABLE [dbo].[temptable] (
    [TagID]      BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (MAX) NULL,
    [TerminalID] VARCHAR (8)   NOT NULL,
    [Tag]        VARCHAR (5)   NOT NULL,
    [TagLength]  INT           DEFAULT ((0)) NOT NULL,
    [TagValue]   VARCHAR (MAX) NULL,
    CONSTRAINT [PK_temptable] PRIMARY KEY CLUSTERED ([TagID] ASC) WITH (FILLFACTOR = 90)
);

