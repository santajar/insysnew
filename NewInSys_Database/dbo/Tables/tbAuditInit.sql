CREATE TABLE [dbo].[tbAuditInit] (
    [InitID]       BIGINT    IDENTITY (1, 1) NOT NULL,
    [TerminalId]   CHAR (8)  NULL,
    [Software]     CHAR (25) NULL,
    [SerialNumber] CHAR (20) NULL,
    [InitTime]     CHAR (25) NULL,
    [StatusDesc]   CHAR (50) NULL,
    CONSTRAINT [PK_tbAuditInit] PRIMARY KEY CLUSTERED ([InitID] ASC) ON [Data_Audit]
);










GO
CREATE NONCLUSTERED INDEX [Index-AuditInit]
    ON [dbo].[tbAuditInit]([InitID] ASC, [TerminalId] ASC, [Software] ASC, [SerialNumber] ASC, [InitTime] ASC)
    ON [Data_Audit];

