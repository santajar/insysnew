﻿CREATE TABLE [dbo].[tbLocation] (
    [LocationID]   VARCHAR (15) NOT NULL,
    [LocationDesc] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tbLocation] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);

