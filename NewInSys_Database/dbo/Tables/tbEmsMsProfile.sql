﻿CREATE TABLE [dbo].[tbEmsMsProfile] (
    [Id]            INT         IDENTITY (1, 1) NOT NULL,
    [EmsXmlVersion] VARCHAR (7) NOT NULL,
    [EmsXmlProfile] VARCHAR (8) NOT NULL,
    [DatabaseId]    SMALLINT    NOT NULL,
    [MasterProfile] VARCHAR (8) NOT NULL,
    CONSTRAINT [PK_tbEmsMsProfile] PRIMARY KEY CLUSTERED ([Id] ASC)
);



