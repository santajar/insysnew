﻿CREATE TABLE [dbo].[tbSoftwareGroupPackage] (
    [ID]           INT          IDENTITY (1, 1) NOT NULL,
    [GroupName]    VARCHAR (50) NOT NULL,
    [SoftwareName] VARCHAR (50) NOT NULL,
    [ScheduleType] VARCHAR (50) NOT NULL,
    [ScheduleTime] VARCHAR (6)  NOT NULL,
    CONSTRAINT [PK_tbSoftwareGroupPackage] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70)
);

