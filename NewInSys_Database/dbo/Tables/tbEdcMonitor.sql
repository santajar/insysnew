﻿CREATE TABLE [dbo].[tbEdcMonitor] (
    [ID]                  INT           IDENTITY (1, 1) NOT NULL,
    [MessageTimestamp]    DATETIME      NOT NULL,
    [MessageRaw]          VARCHAR (MAX) NOT NULL,
    [ProcCode]            VARCHAR (6)   NOT NULL,
    [DatabaseID]          INT           NOT NULL,
    [TerminalID]          VARCHAR (8)   NOT NULL,
    [SerialNumber]        VARCHAR (50)  NOT NULL,
    [SoftwareVersion]     VARCHAR (50)  NOT NULL,
    [LifetimeCounter]     INT           DEFAULT ((0)) NOT NULL,
    [TotalAllTransaction] INT           DEFAULT ((0)) NOT NULL,
    [TotalSwipe]          INT           DEFAULT ((0)) NOT NULL,
    [TotalDip]            INT           DEFAULT ((0)) NOT NULL,
    [SettlementDateTime]  VARCHAR (12)  NULL,
    [ICCID]               VARCHAR (50)  NULL,
    [TID]                 VARCHAR (8)   NULL,
    [MID]                 VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70)
);


