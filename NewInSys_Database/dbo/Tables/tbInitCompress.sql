﻿CREATE TABLE [dbo].[tbInitCompress] (
    [InitId]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [TerminalId]        VARCHAR (8)     NOT NULL,
    [TermContent]       VARCHAR (MAX)   NOT NULL,
    [CompressedContent] VARBINARY (MAX) NULL,
    CONSTRAINT [PK_tbInitCompress] PRIMARY KEY CLUSTERED ([InitId] ASC)
);



