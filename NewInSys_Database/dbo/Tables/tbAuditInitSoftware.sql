CREATE TABLE [dbo].[tbAuditInitSoftware] (
    [InitID]           BIGINT       IDENTITY (1, 1) NOT NULL,
    [TerminalId]       CHAR (8)     NULL,
    [Software]         CHAR (25)    NULL,
    [SoftwareDownload] VARCHAR (25) NULL,
    [SerialNumber]     CHAR (20)    NULL,
    [InitTime]         CHAR (25)    NULL,
    [IndexSoftware]    INT          NULL,
    CONSTRAINT [PK_tbAuditInitSoftware] PRIMARY KEY CLUSTERED ([InitID] ASC) ON [Data_Audit]
);










GO
CREATE NONCLUSTERED INDEX [Index-AuditInitSoftware]
    ON [dbo].[tbAuditInitSoftware]([InitID] ASC, [TerminalId] ASC, [Software] ASC, [SoftwareDownload] ASC, [SerialNumber] ASC, [InitTime] ASC, [IndexSoftware] ASC)
    ON [Data_Audit];

