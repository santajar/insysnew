﻿CREATE TABLE [dbo].[tbProfileCardType] (
    [ID]                INT          IDENTITY (1, 1) NOT NULL,
    [DatabaseID]        INT          NOT NULL,
    [CardTypeName]      VARCHAR (50) NOT NULL,
    [CardTypeTag]       VARCHAR (5)  NOT NULL,
    [CardTypeTagLength] INT          NOT NULL,
    [CardTypeTagValue]  VARCHAR (50) NULL,
    CONSTRAINT [PK_tbProfileCardType] PRIMARY KEY CLUSTERED ([ID] ASC, [DatabaseID] ASC, [CardTypeName] ASC)
);

