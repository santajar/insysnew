﻿CREATE TABLE [dbo].[tbProfileImage] (
    [ImageID]    INT          IDENTITY (1, 1) NOT NULL,
    [LogoType]   VARCHAR (10) NOT NULL,
    [FileName]   VARCHAR (50) NOT NULL,
    [UploadTime] DATETIME     NOT NULL,
    CONSTRAINT [PK_tbProfileImage] PRIMARY KEY CLUSTERED ([ImageID] ASC) WITH (FILLFACTOR = 70)
);

