﻿CREATE TABLE [dbo].[tbProfileEdcMonitor]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DatabaseID] INT NOT NULL, 
    [Name] VARCHAR(15) NOT NULL, 
    [Tag] VARCHAR(5) NOT NULL, 
    [TagLength] INT NOT NULL, 
    [TagValue] VARCHAR(50) NULL
)
