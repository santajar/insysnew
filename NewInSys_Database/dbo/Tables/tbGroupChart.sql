﻿CREATE TABLE [dbo].[tbGroupChart] (
    [IDChartGroup] INT          IDENTITY (1, 1) NOT NULL,
    [GroupName]    VARCHAR (25) NULL,
    [GroupCount]   INT          NULL,
    CONSTRAINT [PK_tbGroupChart] PRIMARY KEY CLUSTERED ([IDChartGroup] ASC)
);





