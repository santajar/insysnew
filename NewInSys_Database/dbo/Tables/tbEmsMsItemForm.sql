﻿CREATE TABLE [dbo].[tbEmsMsItemForm] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [FormId]   INT          NOT NULL,
    [FormName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tbEmsMsItemForm] PRIMARY KEY CLUSTERED ([Id] ASC)
);



