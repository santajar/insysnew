IF OBJECT_ID ('dbo.tbEdcMonitorTempAcqSeq') IS NOT NULL
	DROP TABLE dbo.tbEdcMonitorTempAcqSeq
GO

CREATE TABLE dbo.tbEdcMonitorTempAcqSeq
	(
	ID           INT IDENTITY NOT NULL,
	TerminalID   VARCHAR (8) NULL,
	AcquirerName VARCHAR (20) NULL,
	TID          VARCHAR (20) NULL,
	MID          VARCHAR (20) NULL,
	CONSTRAINT PK_tbEdcMonitorTempAcqSeq PRIMARY KEY (ID)
	)
GO

