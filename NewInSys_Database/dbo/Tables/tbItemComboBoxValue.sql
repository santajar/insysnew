﻿CREATE TABLE [dbo].[tbItemComboBoxValue] (
    [ItemID]       INT          IDENTITY (1, 1) NOT NULL,
    [DatabaseID]   SMALLINT     NOT NULL,
    [FormID]       SMALLINT     NOT NULL,
    [ItemSequence] SMALLINT     NOT NULL,
    [DisplayValue] VARCHAR (40) NOT NULL,
    [RealValue]    VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_ComboBoxValue] PRIMARY KEY CLUSTERED ([ItemID] ASC) ON [Data]
);






GO
CREATE NONCLUSTERED INDEX [index-ItemComboBoxValue]
    ON [dbo].[tbItemComboBoxValue]([ItemID] ASC, [DatabaseID] ASC, [FormID] ASC, [ItemSequence] ASC, [DisplayValue] ASC, [RealValue] ASC)
    ON [Data];

