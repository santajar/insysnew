﻿CREATE TABLE [dbo].[tbStockPLCTerminalHistory] (
    [ID]             INT           IDENTITY (8000, 1) NOT NULL,
    [TerminalID]     INT           NULL,
    [Type]           VARCHAR (50)  NULL,
    [Description]    VARCHAR (255) NULL,
    [UserCreationID] VARCHAR (10)  NULL,
    [DateCreation]   DATETIME      NULL,
    [Timezone]       VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_PLCTerminalHistory] FOREIGN KEY ([TerminalID]) REFERENCES [dbo].[tbStockPLCTerminal] ([ID]),
    CONSTRAINT [FK_PLCTerminalHistoryUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

