﻿CREATE TABLE [dbo].[tbNACNegativeList] (
    [NacId]     SMALLINT     IDENTITY (1, 1) NOT NULL,
    [NacNumber] VARCHAR (15) NULL,
    CONSTRAINT [PK_tbNACNegativeList] PRIMARY KEY CLUSTERED ([NacId] ASC)
);



