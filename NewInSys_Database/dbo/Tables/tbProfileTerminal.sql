﻿CREATE TABLE [dbo].[tbProfileTerminal] (
    [TerminalTagID]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [TerminalID]                VARCHAR (8)   NOT NULL,
    [TerminalTag]               VARCHAR (5)   NOT NULL,
    [TerminalLengthOfTagLength] INT           CONSTRAINT [DF_tbProfileTerminal_TerminalLengthOfTagLength] DEFAULT ((0)) NOT NULL,
    [TerminalTagLength]         INT           CONSTRAINT [DF_tbProfileTerminal_TerminalTagLength] DEFAULT ((0)) NOT NULL,
    [TerminalTagValue]          VARCHAR (150) NULL,
    [Description]               VARCHAR (255) NULL,
    CONSTRAINT [PK_TerminalTag] PRIMARY KEY CLUSTERED ([TerminalTagID] ASC) ON [Data],
    CONSTRAINT [FK_tbProfileTerminal_tbProfileTerminalList] FOREIGN KEY ([TerminalID]) REFERENCES [dbo].[tbProfileTerminalList] ([TerminalID]) ON DELETE CASCADE
);










GO



GO
-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Nov 13, 2012
-- Description:	Repair the incorrect value on TerminalTagLength field to fix the DataUpload module
-- =============================================
CREATE TRIGGER [dbo].[AfterInsertRepairTerminalTagLength] 
   ON  [dbo].[tbProfileTerminal]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
    DECLARE @sTerminalID VARCHAR(8),
		@sTerminalTag VARCHAR(5),
		@iTerminalLengthOfTagLength INT,
		@iTerminalTagLength INT,
		@sTerminalTagValue VARCHAR(150)
		
    SELECT @sTerminalID = TerminalID,
		@sTerminalTag = TerminalTag,
		@iTerminalLengthOfTagLength = TerminalLengthOfTagLength,
		@iTerminalTagLength = TerminalTagLength,
		@sTerminalTagValue = TerminalTagValue
	FROM inserted
    
	IF @iTerminalTagLength != DATALENGTH(@sTerminalTagValue)
		UPDATE tbProfileTerminal 
		SET TerminalTagLength = DATALENGTH(@sTerminalTagValue), 
			TerminalLengthOfTagLength = LEN(DATALENGTH(@sTerminalTagValue))
		WHERE TerminalID = @sTerminalID
			AND TerminalTag = @sTerminalTag
			AND TerminalTagValue = @sTerminalTagValue
	
END
GO
CREATE NONCLUSTERED INDEX [Id_TerminalId_Tag]
    ON [dbo].[tbProfileTerminal]([TerminalID] ASC, [TerminalTag] ASC, [TerminalTagValue] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

