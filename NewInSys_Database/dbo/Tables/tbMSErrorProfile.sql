﻿CREATE TABLE [dbo].[tbMSErrorProfile] (
    [ErrorCode]        SMALLINT      NOT NULL,
    [ErrorDescription] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_tbMSErrorProfile] PRIMARY KEY CLUSTERED ([ErrorCode] ASC)
);

