CREATE TABLE [dbo].[tbAppPackageEDCListTemp] (
    [Id]                BIGINT        NOT NULL,
    [AppPackId]         BIGINT        NOT NULL,
    [AppPackageName]    VARCHAR (150) NOT NULL,
    [AppPackageContent] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tbAppPackageEDCListTemp_1] PRIMARY KEY CLUSTERED ([Id] ASC, [AppPackId] ASC, [AppPackageName] ASC) WITH (FILLFACTOR = 90)
);










GO
CREATE NONCLUSTERED INDEX [IdAppNameListTemp]
    ON [dbo].[tbAppPackageEDCListTemp]([Id] ASC, [AppPackId] ASC) WITH (FILLFACTOR = 90)
    ON [Data];

