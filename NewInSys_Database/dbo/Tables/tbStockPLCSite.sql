﻿CREATE TABLE [dbo].[tbStockPLCSite] (
    [ID]              INT           IDENTITY (3000, 1) NOT NULL,
    [Name]            VARCHAR (255) NOT NULL,
    [StreetAddress1]  VARCHAR (255) NULL,
    [StreetAddress2]  VARCHAR (255) NULL,
    [StreetAddress3]  VARCHAR (255) NULL,
    [City]            VARCHAR (255) NULL,
    [County]          VARCHAR (255) NULL,
    [PostCode]        VARCHAR (255) NULL,
    [Country]         VARCHAR (255) NULL,
    [PrimarySite]     BIT           NULL,
    [OpeningTimeFrom] VARCHAR (255) NULL,
    [OpeningTimeTo]   VARCHAR (255) NULL,
    [SiteType]        VARCHAR (255) NULL,
    [PartyID]         INT           NULL,
    [UserCreationID]  VARCHAR (10)  NULL,
    [DateCreation]    DATETIME      NULL,
    [Timezone]        VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_PLCSitePLCParty] FOREIGN KEY ([PartyID]) REFERENCES [dbo].[tbStockPLCParty] ([ID]),
    CONSTRAINT [FK_PLCSiteUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

