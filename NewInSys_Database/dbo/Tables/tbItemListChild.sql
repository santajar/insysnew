﻿CREATE TABLE [dbo].[tbItemListChild] (
    [ItemID]     INT          IDENTITY (1, 1) NOT NULL,
    [DatabaseID] SMALLINT     NOT NULL,
    [TagParent]  VARCHAR (5)  NOT NULL,
    [TagChild]   VARCHAR (5)  NOT NULL,
    [ItemName]   VARCHAR (24) NOT NULL,
    CONSTRAINT [PK_tbItemListChild] PRIMARY KEY CLUSTERED ([ItemID] ASC)
);



