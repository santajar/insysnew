﻿CREATE TABLE [dbo].[tbProfileRelation] (
    [RelationTagID]             BIGINT       IDENTITY (1, 1) NOT NULL,
    [TerminalID]                VARCHAR (8)  NOT NULL,
    [DatabaseID]                INT          DEFAULT ((0)) NOT NULL,
    [RelationTag]               VARCHAR (5)  NOT NULL,
    [RelationLengthOfTagLength] INT          CONSTRAINT [DF_tbProfileRelation_RelationLengthOfTagLength] DEFAULT ((0)) NOT NULL,
    [RelationTagLength]         INT          CONSTRAINT [DF_tbProfileRelation_RelationTagLength] DEFAULT ((0)) NOT NULL,
    [RelationTagValue]          VARCHAR (50) NULL,
    [Description]               VARCHAR (50) NULL,
    CONSTRAINT [PK_Relation] PRIMARY KEY CLUSTERED ([RelationTagID] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON) ON [Data],
    CONSTRAINT [FK_tbProfileRelation_tbProfileTerminalList] FOREIGN KEY ([TerminalID]) REFERENCES [dbo].[tbProfileTerminalList] ([TerminalID]) ON DELETE CASCADE
);








GO
CREATE NONCLUSTERED INDEX [Id_TerminalId_Tag]
    ON [dbo].[tbProfileRelation]([RelationTagID] ASC, [TerminalID] ASC, [RelationTag] ASC, [RelationTagValue] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

