﻿CREATE TABLE [dbo].[tbProfileCustomMenu] (
    [CustomMenuID]                BIGINT        IDENTITY (1, 1) NOT NULL,
    [TerminalID]                  VARCHAR (8)   NOT NULL,
    [CustomMenuTag]               VARCHAR (5)   NOT NULL,
    [CustomMenuLengthOfTagLength] INT           NOT NULL,
    [CustomMenuTagLength]         INT           NOT NULL,
    [CustomMenuTagValue]          VARCHAR (150) NULL,
    [Description]                 VARCHAR (255) NULL,
    CONSTRAINT [PK_tbProfileCustomMenu] PRIMARY KEY CLUSTERED ([CustomMenuID] ASC) WITH (FILLFACTOR = 70)
);




GO
CREATE NONCLUSTERED INDEX [ID_TerminalID_Tag]
    ON [dbo].[tbProfileCustomMenu]([CustomMenuID] ASC, [TerminalID] ASC, [CustomMenuTag] ASC, [CustomMenuTagValue] ASC) WITH (FILLFACTOR = 70);

