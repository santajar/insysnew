﻿CREATE TABLE [dbo].[tbSoftwareProgressCPLDetail] (
    [ID]                INT             IDENTITY (1, 1) NOT NULL,
    [GroupName]         VARCHAR (50)    NULL,
    [TerminalID]        VARCHAR (8)     NULL,
    [Software]          VARCHAR (50)    NOT NULL,
    [AppPackageName]    VARCHAR (50)    NULL,
    [SerialNumber]      VARCHAR (25)    NOT NULL,
    [StartTime]         DATETIME        NOT NULL,
    [EndTime]           DATETIME        NULL,
    [Percentage]        DECIMAL (18, 2) NULL,
    [FinishInstallTime] DATETIME        NULL,
    [InstallStatus]     VARCHAR (50)    NULL
);



