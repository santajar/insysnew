﻿CREATE TABLE [dbo].[tbProfileCardList] (
    [CardID]     INT          IDENTITY (1, 1) NOT NULL,
    [DatabaseID] SMALLINT     NOT NULL,
    [CardName]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Card] PRIMARY KEY CLUSTERED ([CardID] ASC) ON [Data],
    CONSTRAINT [FK_tbProfileCardList_tbProfileTerminalDB] FOREIGN KEY ([DatabaseID]) REFERENCES [dbo].[tbProfileTerminalDB] ([DatabaseID])
);








GO
CREATE NONCLUSTERED INDEX [CardNameDatabaseId]
    ON [dbo].[tbProfileCardList]([DatabaseID] ASC, [CardName] ASC, [CardID] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

