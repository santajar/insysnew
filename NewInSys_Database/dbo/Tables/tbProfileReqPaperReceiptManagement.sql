﻿CREATE TABLE [dbo].[tbProfileReqPaperReceiptManagement] (
    [ReqTagId]             BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [DatabaseID]           INT           NOT NULL,
    [ReqName]              VARCHAR (15)  NOT NULL,
    [ReqTag]               VARCHAR (5)   NOT NULL,
    [ReqLengthOfTagLength] INT           NOT NULL,
    [ReqTagLength]         INT           NOT NULL,
    [ReqTagValue]          VARCHAR (150) NOT NULL,
    [Description]          VARCHAR (150) NULL,
    CONSTRAINT [PK_tbProfileReqPaperReceiptManagement] PRIMARY KEY CLUSTERED ([ReqTagId] ASC, [DatabaseID] ASC, [ReqName] ASC, [ReqTag] ASC) WITH (FILLFACTOR = 90)
);

