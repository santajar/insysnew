﻿CREATE TABLE [dbo].[tbStockPLCBOM] (
    [ID]                    INT           IDENTITY (2000, 1) NOT NULL,
    [Description]           VARCHAR (255) NOT NULL,
    [PartNumber]            VARCHAR (255) NULL,
    [BOMType]               VARCHAR (255) NULL,
    [Model]                 VARCHAR (255) NULL,
    [Reference]             VARCHAR (255) NULL,
    [DisplayColor]          VARCHAR (255) NULL,
    [Color]                 BIT           NULL,
    [Ethernet]              BIT           NULL,
    [Contactless]           BIT           NULL,
    [ConnectivityGPRS]      VARCHAR (1)   NULL,
    [Connectivity3G]        VARCHAR (1)   NULL,
    [ConnectivityWifi]      VARCHAR (1)   NULL,
    [ConnectivityBluetooth] VARCHAR (1)   NULL,
    [DateCreation]          DATETIME      NULL,
    [UserCreationID]        VARCHAR (10)  NULL,
    [Timezone]              VARCHAR (255) NULL,
    [EstateOwnerID]         INT           NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_PLCBOMSponsor] FOREIGN KEY ([EstateOwnerID]) REFERENCES [dbo].[tbStockSponsor] ([SponsorID]),
    CONSTRAINT [FK_PLCBOMUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

