﻿CREATE TABLE [dbo].[tbProfileRelationHistory] (
    [RelationTagID]             BIGINT       IDENTITY (1, 1) NOT NULL,
    [TerminalID]                VARCHAR (8)  NOT NULL,
    [RelationTag]               VARCHAR (5)  NOT NULL,
    [RelationLengthOfTagLength] INT          NOT NULL,
    [RelationTagLength]         INT          NULL,
    [RelationTagValue]          VARCHAR (50) NULL,
    [Description]               VARCHAR (50) NULL,
    [LastUpdate]                VARCHAR (20) NULL
);



