﻿CREATE TABLE [dbo].[tbStockEMBatch] (
    [ID]             INT             IDENTITY (10000, 1) NOT NULL,
    [EstateOwnerID]  INT             NULL,
    [ImportType]     VARCHAR (50)    NOT NULL,
    [DATA]           VARBINARY (MAX) NULL,
    [UserCreationID] VARCHAR (10)    NULL,
    [DateCreation]   DATETIME        NULL,
    [Timezone]       VARCHAR (255)   NULL,
    [FileName]       VARCHAR (255)   NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_EMBatchUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

