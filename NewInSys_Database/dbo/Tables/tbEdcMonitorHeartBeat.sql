IF OBJECT_ID ('dbo.tbEdcMonitorHeartBeat') IS NOT NULL
	DROP TABLE dbo.tbEdcMonitorHeartBeat
GO

CREATE TABLE dbo.tbEdcMonitorHeartBeat
	(
	ID                  INT IDENTITY NOT NULL,
	MessageTimestamp    DATETIME NULL,
	MessageRaw          VARCHAR (max) NULL,
	ProcCode            VARCHAR (6) NULL,
	DatabaseID          INT NOT NULL,
	TerminalID          VARCHAR (8) NOT NULL,
	SerialNumber        VARCHAR (50) NULL,
	SoftwareVersion     VARCHAR (50) NULL,
	LifetimeCounter     INT CONSTRAINT DF__tbHeartBe__Lifet__49FDC4EB DEFAULT ((0)) NULL,
	TotalAllTransaction INT CONSTRAINT DF__tbHeartBe__Total__4AF1E924 DEFAULT ((0)) NULL,
	TotalSwipe          INT CONSTRAINT DF__tbHeartBe__Total__4BE60D5D DEFAULT ((0)) NULL,
	TotalDip            INT CONSTRAINT DF__tbHeartBe__Total__4CDA3196 DEFAULT ((0)) NULL,
	SettlementDateTime  VARCHAR (12) NULL,
	ICCID               VARCHAR (50) NULL,
	TowerNumber         INT NULL,
	MCC_MNC_Data        VARCHAR (50) NULL,
	LAC_Data            VARCHAR (50) NULL,
	Cell_ID_Data        VARCHAR (50) NULL,
	TID                 VARCHAR (8) NULL,
	MID                 VARCHAR (15) NULL,
	CONSTRAINT PK__tbHeartB__3214EC27A31EFAE5 PRIMARY KEY (ID)
	)
GO

