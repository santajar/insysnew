﻿CREATE TABLE [dbo].[tbProfileDataTechnician] (
    [IdKtp]        NVARCHAR (15) NOT NULL,
    [Nama]         NVARCHAR (50) NOT NULL,
    [HP]           NVARCHAR (15) NOT NULL,
    [ServicePoint] NVARCHAR (50) NULL,
    [VendorName]   NVARCHAR (50) NULL,
    [JenisKelamin] NVARCHAR (10) NULL,
    CONSTRAINT [PK_tbProfileDataTechnician] PRIMARY KEY CLUSTERED ([IdKtp] ASC)
);

