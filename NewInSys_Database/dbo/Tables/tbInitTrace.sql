﻿CREATE TABLE [dbo].[tbInitTrace] (
    [Id]              BIGINT        IDENTITY (1, 1) NOT NULL,
    [ErrorCode]       SMALLINT      NULL,
    [TerminalId]      VARCHAR (8)   NULL,
    [ISO]             VARCHAR (MAX) NULL,
    [Tag]             VARCHAR (5)   NULL,
    [TerminalContent] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_tbInitTrace] PRIMARY KEY CLUSTERED ([Id] ASC)
);



