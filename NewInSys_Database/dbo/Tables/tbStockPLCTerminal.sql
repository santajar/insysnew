﻿CREATE TABLE [dbo].[tbStockPLCTerminal] (
    [ID]             INT           IDENTITY (5000, 1) NOT NULL,
    [BOMID]          INT           NULL,
    [Status]         VARCHAR (255) NULL,
    [SiteID]         INT           NULL,
    [UserCreationID] VARCHAR (10)  NULL,
    [DateCreation]   DATETIME      NULL,
    [Timezone]       VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [FK_PLCTerminalBOM] FOREIGN KEY ([BOMID]) REFERENCES [dbo].[tbStockPLCBOM] ([ID]),
    CONSTRAINT [FK_PLCTerminalSite] FOREIGN KEY ([SiteID]) REFERENCES [dbo].[tbStockPLCSite] ([ID]),
    CONSTRAINT [FK_PLCTerminalUserDetails] FOREIGN KEY ([UserCreationID]) REFERENCES [dbo].[tbUserLogin] ([UserID])
);

