﻿CREATE TABLE [dbo].[tbProfileBankCode] (
	[BankCodeID]                INT          IDENTITY (1, 1) NOT NULL,
	[DatabaseID]                INT          CONSTRAINT [DF_tbProfileBankCode_DatabaseID] DEFAULT ((0)) NULL,
	[BankCode]					VARCHAR(50) NOT NULL, 
	[BankCodeTag]               VARCHAR (5)  NOT NULL,
	[BankCodeLengthOfTagLength] INT          CONSTRAINT [DF_tbProfileBankCode_BankCodeLengthOfTagLength] DEFAULT ((0)) NOT NULL,
	[BankCodeTagLength]         INT          NOT NULL,
	[BankCodeTagValue]          VARCHAR (50) NULL,    
	CONSTRAINT [PK_tbProfileBankCode_1] PRIMARY KEY CLUSTERED ([BankCodeID] ASC)
);

