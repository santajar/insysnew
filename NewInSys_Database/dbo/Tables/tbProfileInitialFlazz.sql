﻿CREATE TABLE [dbo].[tbProfileInitialFlazz] (
    [ID]                            INT          IDENTITY (1, 1) NOT NULL,
    [InitialFlazzName]              VARCHAR (50) NOT NULL,
    [DatabaseID]                    INT          NOT NULL,
    [InitialFlazzTag]               VARCHAR (5)  NOT NULL,
    [InitialFalzzLengthOfTagLength] INT          CONSTRAINT [DF_tbProfileInitialFlazz_InitialFalzzLengthOfTagLength] DEFAULT ((2)) NOT NULL,
    [InitialFlazzTagLength]         INT          NOT NULL,
    [InitialFlazzTagValue]          VARCHAR (50) NULL,
    CONSTRAINT [PK_tbProfileInitialFlazz] PRIMARY KEY CLUSTERED ([ID] ASC)
);

