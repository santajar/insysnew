﻿CREATE TABLE [dbo].[tbItemList] (
    [ItemID]                 INT          IDENTITY (1, 1) NOT NULL,
    [DatabaseID]             SMALLINT     NOT NULL,
    [FormID]                 SMALLINT     NOT NULL,
    [ItemSequence]           SMALLINT     NOT NULL,
    [ItemName]               VARCHAR (24) NOT NULL,
    [ObjectID]               SMALLINT     NOT NULL,
    [DefaultValue]           TEXT         NULL,
    [LengthofTagValueLength] INT          NOT NULL,
    [vAllowNull]             BIT          NOT NULL,
    [vUpperCase]             BIT          DEFAULT ((1)) NOT NULL,
    [vType]                  VARCHAR (5)  DEFAULT ('N') NULL,
    [vMinLength]             SMALLINT     NOT NULL,
    [vMaxLength]             SMALLINT     NOT NULL,
    [vMinValue]              INT          NULL,
    [vMaxValue]              INT          NULL,
    [vMasking]               BIT          DEFAULT ((0)) NOT NULL,
    [ValidationMsg]          VARCHAR (50) NULL,
    [Tag]                    VARCHAR (5)  NULL,
    CONSTRAINT [PK_tbItemList] PRIMARY KEY CLUSTERED ([ItemID] ASC, [FormID] ASC, [ItemSequence] ASC) WITH (STATISTICS_NORECOMPUTE = ON) ON [Data],
    CONSTRAINT [FK_tbItemList_tbItemObject] FOREIGN KEY ([ObjectID]) REFERENCES [dbo].[tbItemObject] ([ObjectID]),
    CONSTRAINT [FK_tbItemList_tbProfileTerminalDB] FOREIGN KEY ([DatabaseID]) REFERENCES [dbo].[tbProfileTerminalDB] ([DatabaseID])
);




GO
CREATE NONCLUSTERED INDEX [DatabaseIDItemName]
    ON [dbo].[tbItemList]([DatabaseID] ASC, [FormID] ASC, [ItemSequence] ASC, [ItemName] ASC, [ObjectID] ASC)
    ON [Data];

