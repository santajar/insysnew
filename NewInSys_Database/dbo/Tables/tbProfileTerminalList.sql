﻿CREATE TABLE [dbo].[tbProfileTerminalList] (
    [TerminalID]        VARCHAR (8)   NOT NULL,
    [DatabaseID]        SMALLINT      NOT NULL,
    [AllowDownload]     BIT           CONSTRAINT [DF_Terminal_AllowDownload] DEFAULT ((1)) NULL,
    [StatusMaster]      BIT           CONSTRAINT [DF_Terminal_StatusMaster] DEFAULT ((0)) NULL,
    [LastView]          DATETIME      NULL,
    [EnableIP]          BIT           CONSTRAINT [DF_Terminal_EnableIP] DEFAULT ((0)) NULL,
    [IPAddress]         VARCHAR (15)  NULL,
    [AutoInitTimeStamp] DATETIME      NULL,
    [LocationID]        BIGINT        NULL,
    [InitCompress]      BIT           CONSTRAINT [DF_tbProfileTerminalList_InitCompress] DEFAULT ((0)) NULL,
    [AppPackID]         VARCHAR (150) NULL,
    [ScheduleID]        INT           NULL,
    [RemoteDownload]    BIT           CONSTRAINT [DF_tbProfileTerminalList_RemoteDownload] DEFAULT ((0)) NULL,
    [EMVInit]           BIT           CONSTRAINT [DF_tbProfileTerminalList_EMVInit] DEFAULT ((1)) NULL,
    [LogoPrint]         BIT           CONSTRAINT [DF_tbProfileTerminalList_LogoPrint] DEFAULT ((1)) NULL,
    [LogoIdle]          BIT           CONSTRAINT [DF_tbProfileTerminalList_LogoIdle] DEFAULT ((1)) NULL,
    [PromoManagement]   BIT           NULL,
    [InitMigrasi]       BIT           CONSTRAINT [DF_tbProfileTerminalList_InitMigrasi] DEFAULT ((0)) NULL,
    [LogoMerchant]      BIT           NULL,
    [LogoReceipt]       BIT           NULL,
    [MaxInitRetry]      INT           NULL,
    [CreateDate]        DATETIME      NULL,
    [EMVInitManagement] BIT NULL DEFAULT ((0)), 
    CONSTRAINT [PK_Terminal] PRIMARY KEY CLUSTERED ([TerminalID] ASC) WITH (FILLFACTOR = 70) ON [Data],
    CONSTRAINT [FK_tbProfileTerminalList_tbProfileTerminalDB] FOREIGN KEY ([DatabaseID]) REFERENCES [dbo].[tbProfileTerminalDB] ([DatabaseID])
);















GO


GO


GO
CREATE UNIQUE NONCLUSTERED INDEX [TerminalVersion]
    ON [dbo].[tbProfileTerminalList]([TerminalID] ASC, [DatabaseID] ASC, [AllowDownload] ASC, [StatusMaster] ASC, [LastView] ASC, [AutoInitTimeStamp] ASC, [InitCompress] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];


GO
CREATE NONCLUSTERED INDEX [FK_TerminalID]
    ON [dbo].[tbProfileTerminalList]([TerminalID] ASC, [DatabaseID] ASC, [AllowDownload] ASC, [StatusMaster] ASC, [LastView] ASC, [AutoInitTimeStamp] ASC, [InitCompress] ASC) WITH (FILLFACTOR = 50, PAD_INDEX = ON)
    ON [Data];


GO
CREATE NONCLUSTERED INDEX [Database]
    ON [dbo].[tbProfileTerminalList]([DatabaseID] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

