﻿CREATE TABLE [dbo].[tbStockTerminal] (
    [Signature]        VARCHAR (50)  NOT NULL,
    [TerminalID]       VARCHAR (8)   NOT NULL,
    [Name]             VARCHAR (100) NOT NULL,
    [ParentID]         INT           NULL,
    [Description]      VARCHAR (200) NOT NULL,
    [RegistrationDate] DATETIME      NOT NULL,
    [Type]             VARCHAR (20)  NOT NULL,
    [Status]           INT           NOT NULL,
    [Category]         INT           NOT NULL,
    [Comms]            VARCHAR (200) NULL,
    [NextCallDate]     DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([Signature] ASC) WITH (FILLFACTOR = 70)
);

