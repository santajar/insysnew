﻿CREATE TABLE [dbo].[tbInitLogConnDetail] (
    [ID]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [Time]        DATETIME      NULL,
    [TerminalID]  VARCHAR (8)   NOT NULL,
    [Tag]         VARCHAR (5)   NOT NULL,
    [Description] VARCHAR (150) NULL,
    [Percentage]  INT           NULL,
    CONSTRAINT [PK_tbInitLogConnDetail] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 70)
);








GO
CREATE NONCLUSTERED INDEX [Id_TerminalId_Tag]
    ON [dbo].[tbInitLogConnDetail]([ID] ASC, [TerminalID] ASC, [Tag] ASC, [Time] ASC) WITH (FILLFACTOR = 70);

