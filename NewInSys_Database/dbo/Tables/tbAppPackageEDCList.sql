CREATE TABLE [dbo].[tbAppPackageEDCList] (
    [AppPackId]          BIGINT          IDENTITY (1, 1) NOT NULL,
    [BuildNumber]        INT             NULL,
    [AppPackageName]     VARCHAR (150)   NOT NULL,
    [AppPackageFilename] VARCHAR (150)   NOT NULL,
    [AppPackageContent]  VARBINARY (MAX) NOT NULL,
    [AppPackageDesc]     VARCHAR (250)   NULL,
    [EdcTypeID]          VARCHAR (150)   NULL,
    [UploadTime]         DATETIME        NOT NULL,
    CONSTRAINT [PK_tbAppList] PRIMARY KEY CLUSTERED ([AppPackId] ASC) WITH (FILLFACTOR = 90)
);














GO
CREATE NONCLUSTERED INDEX [IdAppName]
    ON [dbo].[tbAppPackageEDCList]([AppPackId] ASC, [AppPackageName] ASC, [AppPackageFilename] ASC, [UploadTime] ASC) WITH (FILLFACTOR = 90)
    ON [Data];

