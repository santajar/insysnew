﻿CREATE TABLE [dbo].[tbProfileIssuer] (
    [IssuerTagID]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [TerminalID]              VARCHAR (8)   NOT NULL,
    [DatabaseID]              INT           DEFAULT ((0)) NOT NULL,
    [IssuerName]              VARCHAR (15)  NOT NULL,
    [IssuerTag]               VARCHAR (5)   NOT NULL,
    [IssuerLengthOfTagLength] INT           CONSTRAINT [DF_tbProfileIssuer_IssuerLengthOfTagLength] DEFAULT ((0)) NOT NULL,
    [IssuerTagLength]         INT           CONSTRAINT [DF_tbProfileIssuer_IssuerTagLength] DEFAULT ((0)) NOT NULL,
    [IssuerTagValue]          VARCHAR (150) NULL,
    [Description]             VARCHAR (255) NULL,
    CONSTRAINT [PK_Issuer] PRIMARY KEY CLUSTERED ([IssuerTagID] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON) ON [Data],
    CONSTRAINT [FK_tbProfileIssuer_tbProfileTerminalList] FOREIGN KEY ([TerminalID]) REFERENCES [dbo].[tbProfileTerminalList] ([TerminalID]) ON DELETE CASCADE
);








GO
CREATE NONCLUSTERED INDEX [TerminalIssuer]
    ON [dbo].[tbProfileIssuer]([TerminalID] ASC, [IssuerName] ASC, [IssuerTag] ASC) WITH (FILLFACTOR = 90, PAD_INDEX = ON)
    ON [Data];

