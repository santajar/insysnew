﻿using System;
using System.IO;
using System.Threading;
using InSysClass;
using System.Globalization;


namespace ConsoleTLV
{
    class Trace
    {
        static string sPath = Environment.CurrentDirectory;
        //Path.GetDirectoryName(            Assembly.GetExecutingAssembly().GetName().CodeBase);

        static string sDirName = "\\TRACE\\";
        static string sFilename = "Trace_";
        static string sFileType = ".log";

        public static void Write(string sMessage)
        {
            string sFile = null;
            if (!isTraceFolderExist())
                Directory.CreateDirectory(sPath + sDirName);
            sFile = sPath + sDirName + sFilename + CommonConsole.sDateYYYYMMDD() + sFileType;
            if (!File.Exists(sFile))
                using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                    fs.Close();
            while (true)
            {
                if (CommonConsole.IsOpenFileAllowed(sFile))
                    break;
                else
                    Thread.Sleep(100);
            }
            //CommonLib.Write2File(sFile, CommonConsole.sFullTime() + " " +
            //    (sMessage.Length > 500 ? sMessage.Substring(0, 500) : sMessage),
            //    true);
            CommonLib.Write2File(sFile, CommonConsole.sFullTime() + " " +
                sMessage,
                true);
        }

        public static void Write(byte[] arrbMessage)
        {
            string sFile = null;
            if (!isTraceFolderExist())
                Directory.CreateDirectory(sPath + sDirName);
            sFile = sPath + sDirName + sFilename + CommonConsole.sDateYYYYMMDD() + sFileType;
            if (!File.Exists(sFile))
                using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                    fs.Close();
            byte[] arrbValue = new byte[1024];

            arrbValue = CommonLib.arrbStringtoByteArray(CommonConsole.sFullTime());

            Array.Copy(arrbMessage, 0, arrbValue, arrbValue.Length + 1, arrbMessage.Length);
            //Array.Copy(arrbMessage, 1, arrbValue, arrbValue.Length + 1, arrbMessage.Length);
            while (true)
            {
                if (CommonConsole.IsOpenFileAllowed(sFile))
                    break;
                else
                    Thread.Sleep(100);
            }
            CommonLib.Write2File(sFile, arrbValue);
        }

        public static void Write(string sFileName, string sMessage)
        {
            string sFile = null;
            if (!isTraceFolderExist())
                Directory.CreateDirectory(sPath + sDirName);
            sFile = sPath + sDirName + sFileName;
            if (!File.Exists(sFile))
                using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                    fs.Close();
            while (true)
            {
                if (CommonConsole.IsOpenFileAllowed(sFile))
                    break;
                else
                    Thread.Sleep(100);
            }
            CommonLib.Write2File(sFile, sMessage, true);
        }

        private static bool isTraceFolderExist()
        {
            return Directory.Exists(sPath + sDirName);
        }
    }
    class CommonConsole
    {
        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sTimeHHMMSS()
        {
            return DateTime.Now.ToString("hhmmss", ciUSFormat);
        }

        public static string sDateMMddyy()
        {
            return DateTime.Now.ToString("MMddyy", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = CommonConsole.sFullTime();
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }



        public static bool IsOpenFileAllowed(string sFilename)
        {
            bool bAllow = false;
            FileStream fs = null;
            try
            {
                fs = File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite);
                fs.Close();
                bAllow = true;
            }
            catch (IOException)
            {
                bAllow = false;
            }
            return bAllow;
        }


    }
}
