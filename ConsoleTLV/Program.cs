﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using InSysClass;
using System.IO;

namespace ConsoleTLV
{
    class Program
    {
        static bool bDebug = false;
        static void Main(string[] args)
        {
            try
            {
                SqlConnection oSqlSource = new SqlConnection();
                SqlConnection oSqlDest = new SqlConnection();
                InitSqlConnection(ref oSqlSource, ref oSqlDest);

                bool bUpdate = bool.Parse(sGetAppSettingsValue("Update"));
                string sFilename = sGetAppSettingsValue("TerminalID File");
                string sDateStart = sGetAppSettingsValue("LastRunDate");
                int iRunType = int.Parse(sGetAppSettingsValue("Run Type"));

                ClearNewinsys_Summary(oSqlDest);

                DataTable dtTerminalDatabase = dtGetTerminalDatabase(oSqlSource);
                if (dtTerminalDatabase != null && dtTerminalDatabase.Rows.Count > 0)
                {
                    DataTable dtSummaryTerminal = new DataTable();
                    DataTable dtSummaryAcquirer = new DataTable();
                    DataTable dtSummaryIssuer = new DataTable();
                    DataTable dtSummaryCard = new DataTable();
                    DataTable dtSummaryRelation = new DataTable();

                    InitTableSummary(oSqlDest, ref dtSummaryTerminal, ref dtSummaryAcquirer, ref dtSummaryIssuer, ref dtSummaryCard, ref dtSummaryRelation);
                    int iCount = 0;
                    foreach (DataRow rowDatabaseID in dtTerminalDatabase.Rows)
                    {
                        string sError = null;
                        string sDbID = rowDatabaseID["DatabaseID"].ToString();
                        string sDbName = rowDatabaseID["DatabaseName"].ToString();

                        DataTable dtTerminalList = new DataTable();
                        if (iRunType == 1)
                        {
                            DateTime dateStart;
                            DateTime dateRun = DateTime.Now;
                            if (DateTime.TryParseExact(sDateStart, "d/M/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out dateStart))
                            {
                                dtTerminalList = dtGetTerminalList(sDbID, oSqlSource, dateStart, dateRun);
                            }
                        }
                        else
                            if (sDbID != "64")
                                dtTerminalList = dtGetTerminalList(sDbID, oSqlSource, sFilename);

                        DataTable dtItemList = dtGetItemList(sDbID, oSqlSource);
                        CheckAndAlterSummaryTable(dtItemList, dtSummaryTerminal, dtSummaryAcquirer, dtSummaryIssuer, dtSummaryCard, oSqlDest);

                        try
                        {
                            if (dtTerminalList != null && dtTerminalList.Rows.Count > 0)
                            {

                                foreach (DataRow rowTerminalID in dtTerminalList.Rows)
                                {
                                    iCount++;
                                    string sTerminalID = rowTerminalID["TerminalID"].ToString();
                                    DataTable dtTerminal = dtGetTerminalFullTable(sTerminalID, oSqlSource);
                                    //CreateLogProfileTID(sTerminalID, oSqlDest);

                                    //DataTable dtItemList = dtGetItemList(sDbID, oSqlSource);
                                    //CheckAndAlterSummaryTable(dtItemList, dtSummaryTerminal, dtSummaryAcquirer, dtSummaryIssuer, dtSummaryCard, oSqlDest);

                                    List<string> listAcquirerName = new List<string>();
                                    List<string> listIssuerName = new List<string>();
                                    List<string> listCardName = new List<string>();

                                    Console.WriteLine(string.Format("Processing {0}", sTerminalID));

                                    if (dtTerminal != null && dtTerminal.Rows.Count > 0)
                                    {
                                        foreach (DataRow row in dtTerminal.Rows)
                                        {
                                            string sName = row["Name"].ToString();
                                            string sTag = row["Tag"].ToString();
                                            string sValue = row["TagValue"].ToString();

                                            try
                                            {
                                                string sFormID = (dtItemList.Select(string.Format("Tag='{0}'", sTag)))[0]["FormID"].ToString();

                                                switch (sFormID)
                                                {
                                                    case "1": CreateUpdateSummary(dtSummaryTerminal, sTerminalID, sName, sTag, sValue, sFormID); break;
                                                    case "2": CreateUpdateSummary(dtSummaryAcquirer, sTerminalID, sName, sTag, sValue, sFormID); break;
                                                    case "3": CreateUpdateSummary(dtSummaryIssuer, sTerminalID, sName, sTag, sValue, sFormID); break;
                                                    //case "5": AddRelation(ref listAcquirerName, ref listIssuerName, ref listCardName, sTag, sValue); break;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Trace.Write(string.Format("{0} : {1},{2}", sTerminalID, sTag, ex.Message));
                                                //Console.WriteLine(string.Format("{0} : {1},{2}", sTerminalID, sTag, ex.Message));
                                            }

                                        }
                                        //CreateRelationSummary(ref dtSummaryRelation, sTerminalID, listAcquirerName, listIssuerName, listCardName);
                                    }
                                    
                                    if (bUpdate) DeleteTerminalID(sTerminalID, oSqlDest, 1);
                                    UploadBulkSummary(oSqlDest, dtSummaryTerminal, "tbTerminal");
                                    EmptyTable(ref dtSummaryTerminal);
                                    if (bUpdate) DeleteTerminalID(sTerminalID, oSqlDest, 2);
                                    UploadBulkSummary(oSqlDest, dtSummaryAcquirer, "tbAcquirer");
                                    EmptyTable(ref dtSummaryAcquirer);
                                    if (bUpdate) DeleteTerminalID(sTerminalID, oSqlDest, 3);
                                    UploadBulkSummary(oSqlDest, dtSummaryIssuer, "tbIssuer");
                                    EmptyTable(ref dtSummaryIssuer);
                                    //if (bUpdate) DeleteTerminalID(sTerminalID, oSqlDest, 4);
                                    //UploadBulkSummary(oSqlDest, dtSummaryRelation, "tbRelation");
                                    //EmptyTable(ref dtSummaryRelation);
                                    Console.WriteLine("[{0:hh:mm:ss.fff tt}] {1}. {2} DONE", DateTime.Now, iCount, sTerminalID);
                                }
                            }
                            //CreateLogCard(sDbName, oSqlDest);
                            //CreateUpdateCardSummary(ref dtSummaryCard, sDbID, sDbName, oSqlSource);

                            //UploadBulkSummary(oSqlDest, dtSummaryCard, "tbCard");
                            //EmptyTable(ref dtSummaryCard);

                            
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        
                    }
                }
                //DataTable dtTempTerminal = new DataTable();
                //DataTable dtTempAcquirer = new DataTable();
                //DataTable dtTempIssuer = new DataTable();
                //DataTable dtTempCard = new DataTable();
                //DataTable dtTempRelation = new DataTable();
                //InitTableSummaryResult(oSqlDest, ref dtTempTerminal, ref dtTempAcquirer, ref dtTempIssuer);
                ExportFile("Terminal", oSqlDest);
                ExportFile("Acquirer", oSqlDest);
                ExportFile("Issuer", oSqlDest);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("PROCESS DONE");
            Console.Read();
        }

        /// <summary>
        /// Clear Data in database Destination
        /// </summary>
        /// <param name="oSqlDest">SQL Connection : SQL Destination</param>
        private static void ClearNewinsys_Summary( SqlConnection oSqlDest)
        {
            string sQuery = string.Format("TRUNCATE TABLE tbTerminal;");
            sQuery = string.Format("{0}\nTRUNCATE TABLE tbAcquirer;", sQuery);
            sQuery = string.Format("{0}\nTRUNCATE TABLE tbIssuer;", sQuery);

            SqlCommand oCmd = new SqlCommand(sQuery, oSqlDest);
            if (oSqlDest.State == ConnectionState.Closed) oSqlDest.Open(); 
            oCmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Export Data Summary to Excel File
        /// </summary>
        /// <param name="sType">String: Terminal, Acqurer or Issuer</param>
        /// <param name="oSqlConn"> Sql Connection : Database Destination</param>
        private static void ExportFile(string sType, SqlConnection oSqlConn)
        {
            string sFilePath = Directory.GetCurrentDirectory();
            SqlCommand oSqlCmd = new SqlCommand(string.Format("EXEC sp{0}Browse", sType), oSqlConn);

            try
            {
                using (DataTable dtTemp = new DataTable())
                {
                    (new SqlDataAdapter(oSqlCmd)).Fill(dtTemp);
                    File.Delete(sFilePath + string.Format("\\Newinsys-Summary\\{0}-{1}-Newinsys_summary.xls",DateTime.Now.ToString("ddMMyy"), sType));

                    File.Copy(string.Format(@"{0}\TEMPLATE\template.xls", sFilePath), sFilePath + string.Format("\\Newinsys-Summary\\{0}-{1}-Newinsys_summary.xls", DateTime.Now.ToString("ddMMyy"), sType));
                    ExcelOleDb oExcel = new ExcelOleDb(sFilePath + string.Format("\\Newinsys-Summary\\{0}-{1}-Newinsys_summary.xls", DateTime.Now.ToString("ddMMyy"), sType));
                    oExcel.CreateWorkSheet(dtTemp);
                    oExcel.InsertRow(dtTemp);
                    oExcel.Dispose();
                }
                //File.Copy(string.Format(@"{0}\TEMPLATE\template.xls", sFilePath), sFilePath + string.Format("\\Newinsys-Summary\\{0}-Newinsys_summary.xls", sType));
                //ExcelOleDb oExcel = new ExcelOleDb(sFilePath);
                //oExcel.CreateWorkSheet(dtTempTable);
                //oExcel.InsertRow(dtTempTable);
                //oExcel.Dispose();
            }
            catch (Exception ex)
            {
                using (DataTable dtTemp = new DataTable())
                {
                    (new SqlDataAdapter(oSqlCmd)).Fill(dtTemp);
                    File.Delete(sFilePath + string.Format("\\Newinsys-Summary\\{0}-{1}-Newinsys_summary.xls", DateTime.Now.ToString("ddMMyy"), sType));
                    File.Delete(sFilePath + string.Format("\\Newinsys-Summary\\{0}-{1}-Newinsys_summary.xlsx", DateTime.Now.ToString("ddMMyy"), sType));

                    File.Copy(string.Format(@"{0}\TEMPLATE\template.xlsx", sFilePath), sFilePath + string.Format("\\Newinsys-Summary\\{0}-{1}-Newinsys_summary.xlsx", DateTime.Now.ToString("ddMMyy"), sType));
                    ExcelOleDb oExcel = new ExcelOleDb(sFilePath + string.Format("\\Newinsys-Summary\\{0}-{1}-Newinsys_summary.xlsx", DateTime.Now.ToString("ddMMyy"), sType));
                    oExcel.CreateWorkSheet(dtTemp);
                    oExcel.InsertRow(dtTemp);
                    oExcel.Dispose();
                }
            }
        }

        /// <summary>
        /// Create Log Card/Database
        /// </summary>
        /// <param name="sDbName">string : Database Name</param>
        /// <param name="oSqlDest">sql Connection : Sql Connection Database</param>
        private static void CreateLogCard(string sDbName, SqlConnection oSqlDest)
        {
            try
            {
                using (SqlCommand oCmd = new SqlCommand("spInputCardLog", oSqlDest))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sDatabaseName", SqlDbType.VarChar).Value = sDbName;
                    if (oSqlDest.State != ConnectionState.Open) oSqlDest.Open();
                    oCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            { Console.WriteLine(ex.Message); }
        }

        /// <summary>
        /// Create Lof Profile Terminal ID
        /// </summary>
        /// <param name="sDbName">string : Database Name</param>
        /// <param name="oSqlDest">sql Connection : Sql Connection Database</param>
        private static void CreateLogProfileTID(string sTerminalID, SqlConnection oSqlDest)
        {
            try
            {
                using (SqlCommand oCmd = new SqlCommand("spInputProfileLog", oSqlDest))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                    if (oSqlDest.State != ConnectionState.Open) oSqlDest.Open();
                    oCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            { Console.WriteLine(ex.Message); }
        }

        /// <summary>
        /// Get List Terminal ID from txt file
        /// </summary>
        /// <param name="sDbID">string : Database ID</param>
        /// <param name="oSqlConn">Sql Connection : Destination Connection</param>
        /// <param name="sFilename">String : File Name</param>
        /// <returns>Data Table : List Terminal ID</returns>
        private static DataTable dtGetTerminalList(string sDbID, SqlConnection oSqlConn, string sFilename)
        {
            DataTable dtTemp = new DataTable();

            string sTerminalIDList = null;
            string[] arrLines = File.ReadAllLines(sFilename);

            foreach (string sLine in arrLines)
                if (string.IsNullOrEmpty(sTerminalIDList))
                    sTerminalIDList = string.Format("'{0}'", sLine);
                else
                    sTerminalIDList = string.Format("{0},'{1}'", sTerminalIDList, sLine);

            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value =
                string.Format(" WHERE DatabaseID='{0}' AND TerminalID IN ({1})", sDbID, sTerminalIDList);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            else { oSqlConn.Close(); oSqlConn.Open(); }

            (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// Delete Terminal ID 
        /// </summary>
        /// <param name="sTerminalID">string : Terminal ID</param>
        /// <param name="oSqlDest">Sql Connection: Destination Connection</param>
        /// <param name="iTableID">int: Table ID</param>
        private static void DeleteTerminalID(string sTerminalID, SqlConnection oSqlDest, int iTableID)
        {
            string sTableName = null;
            switch (iTableID)
            {
                case 1: sTableName = "tbTerminal"; break;
                case 2: sTableName = "tbAcquirer"; break;
                case 3: sTableName = "tbIssuer"; break;
                case 4: sTableName = "tbRelation"; break;
            }
            string sQuery = string.Format("DELETE FROM {1} WHERE TerminalID='{0}'", sTerminalID, sTableName);
            SqlCommand oCmd = new SqlCommand(sQuery, oSqlDest);
            oCmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Clear Temporary Summary
        /// </summary>
        /// <param name="dtSummaryTemp">Data Table : Summary</param>
        private static void EmptyTable(ref DataTable dtSummaryTemp)
        {
            dtSummaryTemp.Clear();
            dtSummaryTemp.AcceptChanges();
        }

        /// <summary>
        /// Insert Bulk to database
        /// </summary>
        /// <param name="oSqlDest">sql Connection : Destination Database</param>
        /// <param name="dtSummaryTemp">Data Table : S</param>
        /// <param name="sTableName">string : Destination Table</param>
        private static void UploadBulkSummary(SqlConnection oSqlDest, DataTable dtSummaryTemp, string sTableName)
        {
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlDest);
            oSqlBulk.BulkCopyTimeout = 0;
            oSqlBulk.DestinationTableName = sTableName;
            
            foreach (DataColumn col in dtSummaryTemp.Columns)
            {
                SqlBulkCopyColumnMapping mapColumn = new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName);
                oSqlBulk.ColumnMappings.Add(mapColumn);
            }
            try
            {
                oSqlBulk.WriteToServer(dtSummaryTemp);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Update Tabel Summary Card
        /// </summary>
        /// <param name="dtSummaryCard"></param>
        /// <param name="sDbID">string : Database ID</param>
        /// <param name="sDbName">string : Database Name</param>
        /// <param name="oSqlSource">SQL Connection</param>
        private static void CreateUpdateCardSummary(ref DataTable dtSummaryCard, string sDbID, string sDbName, SqlConnection oSqlSource)
        {
            DataTable dtCardList = dtGetCardList(sDbID, oSqlSource);
            foreach (DataRow rowCard in dtCardList.Rows)
            {
                string sCardName = rowCard["CardName"].ToString();
                string sCardTag = rowCard["Tag"].ToString();
                string sCardValue = rowCard["Value"].ToString();
                DataRow[] arrrowSearch = dtSummaryCard.Select(string.Format("DatabaseName='{0}' AND CardName='{1}'", sDbName, sCardName));
                if (arrrowSearch.Length == 0)
                {
                    DataRow rowNewSummaryCard = dtSummaryCard.NewRow();
                    rowNewSummaryCard["DatabaseName"] = sDbName;
                    rowNewSummaryCard["CardName"] = sCardName;
                    rowNewSummaryCard[sCardTag] = sCardValue;
                    dtSummaryCard.Rows.Add(rowNewSummaryCard);
                }
                else
                    arrrowSearch[0][sCardTag] = sCardValue;
                dtSummaryCard.AcceptChanges();
            }
        }

        /// <summary>
        /// Get Card List by DatabaseID
        /// </summary>
        /// <param name="sDbID">string : Database ID</param>
        /// <param name="oSqlConn">Sql Connection : Destination Connection</param>
        /// <returns>Data Table : list Card</returns>
        private static DataTable dtGetCardList(string sDbID, SqlConnection oSqlConn)
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPCardBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format("WHERE DatabaseID='{0}'", sDbID);

            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// Create Relation 
        /// </summary>
        /// <param name="_dtSummaryRelation"></param>
        /// <param name="sTerminalID"></param>
        /// <param name="listAcquirerName"></param>
        /// <param name="listIssuerName"></param>
        /// <param name="listCardName"></param>
        private static void CreateRelationSummary(ref DataTable _dtSummaryRelation
            , string sTerminalID, List<string> listAcquirerName, List<string> listIssuerName, List<string> listCardName)
        {
            for (int i = 0; i < listAcquirerName.Count; i++)
            {
                DataRow rowNew = _dtSummaryRelation.NewRow();
                rowNew["TerminalID"] = sTerminalID;
                rowNew["AcquirerName"] = listAcquirerName[i];
                rowNew["IssuerName"] = listIssuerName[i];
                rowNew["CardName"] = listCardName[i];
                _dtSummaryRelation.Rows.Add(rowNew);
                _dtSummaryRelation.AcceptChanges();
            }
        }

        private static void AddRelation(ref List<string> _listAcquirerName, ref List<string> _listIssuerName, ref List<string> _listCardName
            , string sTag, string sValue)
        {
            switch (sTag)
            {
                case "AD01":
                case "AD001":
                    _listCardName.Add(sValue);
                    break;
                case "AD02":
                case "AD002":
                    _listIssuerName.Add(sValue);
                    break;
                case "AD03":
                case "AD003":
                    _listAcquirerName.Add(sValue);
                    break;
            }
        }

        /// <summary>
        /// Update Summary Profile
        /// </summary>
        /// <param name="dtSummaryTemp"></param>
        /// <param name="sTerminalID"></param>
        /// <param name="sName"></param>
        /// <param name="sTag"></param>
        /// <param name="sValue"></param>
        /// <param name="sFormID"></param>
        private static void CreateUpdateSummary(DataTable dtSummaryTemp, string sTerminalID, string sName, string sTag, string sValue, string sFormID)
        {
            try
            {
                string sCondition = null;
                switch (sFormID)
                {
                    case "1": sCondition = string.Format("TerminalID='{0}'", sTerminalID); break;
                    case "2": sCondition = string.Format("TerminalID='{0}' AND AcquirerName='{1}'", sTerminalID, sName); break;
                    case "3": sCondition = string.Format("TerminalID='{0}' AND IssuerName='{1}'", sTerminalID, sName); break;
                }
                DataRow[] arrrowSearch = dtSummaryTemp.Select(sCondition);
                if (arrrowSearch.Length == 0)
                {
                    DataRow rowNew = dtSummaryTemp.NewRow();
                    rowNew["TerminalID"] = sTerminalID;
                    rowNew[sTag] = sValue;
                    if (sFormID == "2") rowNew["AcquirerName"] = sName;
                    else if (sFormID == "3") rowNew["IssuerName"] = sName;
                    dtSummaryTemp.Rows.Add(rowNew);
                }
                else arrrowSearch[0][sTag] = sValue;
                dtSummaryTemp.AcceptChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(sTag);
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Get Data Profile from Database
        /// </summary>
        /// <param name="sTerminalID">string : Terminal ID</param>
        /// <param name="oSqlConn">SQL Connection : </param>
        /// <returns></returns>
        private static DataTable dtGetTerminalFullTable(string sTerminalID, SqlConnection oSqlConn)
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileTextFullTable, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;

            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// Make sure Column tables are same
        /// </summary>
        /// <param name="dtItemList">Data Table : Item List Value</param>
        /// <param name="dtSummaryTerminal">Data Table : Terminal Value</param>
        /// <param name="dtSummaryAcquirer">Data Table : Acquirer Value</param>
        /// <param name="dtSummaryIssuer">Data Table : Issuer Value</param>
        /// <param name="dtSummaryCard">Data Table : Card Value</param>
        /// <param name="oSqlConn">Sql Connection</param>
        private static void CheckAndAlterSummaryTable(DataTable dtItemList
            , DataTable dtSummaryTerminal, DataTable dtSummaryAcquirer, DataTable dtSummaryIssuer, DataTable dtSummaryCard
            , SqlConnection oSqlConn)
        {
            foreach (DataRow rowItem in dtItemList.Rows)
            {
                string sFormID = rowItem["FormID"].ToString();
                string sTag = rowItem["Tag"].ToString().TrimEnd();
                switch (sFormID)
                {
                    case "1":
                        if (!dtSummaryTerminal.Columns.Contains(sTag)) 
                            AlterTable(ref dtSummaryTerminal, sTag, sFormID, oSqlConn);
                        break;
                    case "2":
                        if (!dtSummaryAcquirer.Columns.Contains(sTag)) 
                            AlterTable(ref dtSummaryAcquirer, sTag, sFormID, oSqlConn);
                        break;
                    case "3":
                        if (!dtSummaryIssuer.Columns.Contains(sTag)) 
                            AlterTable(ref dtSummaryIssuer, sTag, sFormID, oSqlConn);
                        break;
                    case "4":
                        if (!dtSummaryCard.Columns.Contains(sTag)) 
                            AlterTable(ref dtSummaryCard, sTag, sFormID, oSqlConn);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Make sure Column tables are same
        /// </summary>
        /// <param name="dtTemp">Data Table :</param>
        /// <param name="sTag">string : tag</param>
        /// <param name="sFormID">string: form ID</param>
        /// <param name="oSqlConn">Sql Connection </param>
        private static void AlterTable(ref DataTable dtTemp, string sTag, string sFormID, SqlConnection oSqlConn)
        {
            try
            {
                string sTableName = null;
                string sTableNameLog = null;
                switch (sFormID)
                {
                    case "1": sTableName = "tbTerminal"; sTableNameLog = "tbTerminal"; break;
                    case "2": sTableName = "tbAcquirer"; sTableNameLog = "tbAcquirer"; break;
                    case "3": sTableName = "tbIssuer"; sTableNameLog = "tbIssuer"; break;
                    case "4": sTableName = "tbCard"; sTableNameLog = "tbCard"; break;
                }
                SqlCommand oCmd = new SqlCommand(string.Format("ALTER TABLE {0} ADD {1} VARCHAR(500) NULL;", sTableName, sTag), oSqlConn);
                SqlCommand oCmdLog = new SqlCommand(string.Format("ALTER TABLE {0} ADD {1} VARCHAR(500) NULL;", sTableNameLog, sTag), oSqlConn);
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                oCmd.ExecuteNonQuery(); oCmdLog.ExecuteNonQuery();
                dtTemp.Columns.Add(sTag);
                dtTemp.AcceptChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine("AlterTable : {0}", ex.Message);
            }
        }

        /// <summary>
        /// Initialize Table in database Destination
        /// </summary>
        /// <param name="oSqlDest">Sql Connection</param>
        /// <param name="dtSummaryTerminal">Data Table : Temporary Terminal Table</param>
        /// <param name="dtSummaryAcquirer">Data Table :Temporary Acquirer Table</param>
        /// <param name="dtSummaryIssuer">Data Table : Temporary Issuer Table</param>
        /// <param name="dtSummaryCard">Data Table : Temporary Card Table</param>
        /// <param name="dtSummaryRelation">Data Table : Temporary Relation Table</param>
        private static void InitTableSummary(SqlConnection oSqlDest, ref DataTable dtSummaryTerminal, ref DataTable dtSummaryAcquirer, ref DataTable dtSummaryIssuer, ref DataTable dtSummaryCard, ref DataTable dtSummaryRelation)
        {
            string sQuery = string.Format("SELECT * FROM tbTerminal;");
            sQuery = string.Format("{0}\nSELECT * FROM tbAcquirer;", sQuery);
            sQuery = string.Format("{0}\nSELECT * FROM tbIssuer;", sQuery);
            sQuery = string.Format("{0}\nSELECT * FROM tbCard;", sQuery);
            sQuery = string.Format("{0}\nSELECT * FROM tbRelation;", sQuery);
            SqlCommand oCmd = new SqlCommand(sQuery, oSqlDest);
            DataSet dsSummary = new DataSet();
            (new SqlDataAdapter(oCmd)).Fill(dsSummary);
            if (dsSummary != null)
            {
                dtSummaryTerminal = dsSummary.Tables[0];
                dtSummaryAcquirer = dsSummary.Tables[1];
                dtSummaryIssuer = dsSummary.Tables[2];
                dtSummaryCard = dsSummary.Tables[3];
                dtSummaryRelation = dsSummary.Tables[4];
            }
        }

        /// <summary>
        /// Get Item List Value by Database ID
        /// </summary>
        /// <param name="sDbID">string : Database ID</param>
        /// <param name="oSqlConn">SQL Connection</param>
        /// <returns></returns>
        private static DataTable dtGetItemList(string sDbID, SqlConnection oSqlConn)
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = string.Format(" WHERE DatabaseID='{0}' ORDER BY Tag", sDbID);

            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// Get Terminal List Value
        /// </summary>
        /// <param name="sDbID">String : Database ID</param>
        /// <param name="oSqlConn">SQL Connection</param>
        /// <param name="dateStart">date Time : time start</param>
        /// <param name="dateRun">Date Time : Time END </param>
        /// <returns></returns>
        private static DataTable dtGetTerminalList(string sDbID, SqlConnection oSqlConn, DateTime dateStart, DateTime dateRun)
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse , oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value =
                string.Format(" WHERE DatabaseID='{0}' AND DATEDIFF(day,'{1:M/d/yyyy}',LastView)>=0 AND DATEDIFF(day,LastView,'{2:M/d/yyyy}')>=0", sDbID, dateStart, dateRun);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();

            (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// Get Database Value
        /// </summary>
        /// <param name="oSqlConn"></param>
        /// <returns></returns>
        private static DataTable dtGetTerminalDatabase(SqlConnection oSqlConn)
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            //oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "WHERE DatabaseID = 101";
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        /// <summary>
        /// Get Sql Configuration from file
        /// </summary>
        /// <param name="sKeyName"></param>
        /// <returns></returns>
        private static string sGetAppSettingsValue(string sKeyName)
        {
            return ConfigurationManager.AppSettings[sKeyName].ToString();
        }

        /// <summary>
        /// initialize Sql Connection
        /// </summary>
        /// <param name="_oSqlSource">sql Connection Source</param>
        /// <param name="_oSqlDest">sql Connection Destination</param>
        private static void InitSqlConnection(ref SqlConnection _oSqlSource, ref SqlConnection _oSqlDest)
        {
            string sConnSource = ConfigurationManager.ConnectionStrings["Source"].ConnectionString;
            _oSqlSource = new SqlConnection(sConnSource);
            _oSqlSource.Open();

            string sConnDestination = ConfigurationManager.ConnectionStrings["Destination"].ConnectionString;
            _oSqlDest = new SqlConnection(sConnDestination);
            _oSqlDest.Open();
        }

        public static SqlConnection oSqlDest { get; set; }
    }
}
