using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmBrDatabaseConnection : Form
    {
        public FrmBrDatabaseConnection()
        {
            InitializeComponent();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            if (txtDataSource.Text == "")
                MessageBox.Show("Please Fill Data Source");
        }
    }
}