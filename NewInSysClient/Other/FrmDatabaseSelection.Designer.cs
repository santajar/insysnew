namespace InSys
{
    partial class FrmDatabaseSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDatabaseSelection));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ListDb = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            // 
            // ListDb
            // 
            this.ListDb.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.ListDb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListDb.LabelWrap = false;
            this.ListDb.LargeImageList = this.ImageList1;
            this.ListDb.Location = new System.Drawing.Point(0, 0);
            this.ListDb.MultiSelect = false;
            this.ListDb.Name = "ListDb";
            this.ListDb.Size = new System.Drawing.Size(443, 325);
            this.ListDb.SmallImageList = this.ImageList1;
            this.ListDb.StateImageList = this.ImageList1;
            this.ListDb.TabIndex = 2;
            this.ListDb.UseCompatibleStateImageBehavior = false;
            this.ListDb.View = System.Windows.Forms.View.SmallIcon;
            // 
            // FrmDatabaseSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 325);
            this.Controls.Add(this.ListDb);
            this.Name = "FrmDatabaseSelection";
            this.Text = "Database Selection";
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ImageList ImageList1;
        internal System.Windows.Forms.ListView ListDb;
    }
}