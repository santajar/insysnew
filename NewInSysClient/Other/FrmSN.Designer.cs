namespace InSys
{
    partial class FrmSN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGridTerminalSN = new System.Windows.Forms.DataGrid();
            this.TabPageRegister = new System.Windows.Forms.TabPage();
            this.CheckBoxColHeader = new System.Windows.Forms.CheckBox();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.RadioVertical = new System.Windows.Forms.RadioButton();
            this.RadioComma = new System.Windows.Forms.RadioButton();
            this.btnExit = new System.Windows.Forms.Button();
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.btnStartAll = new System.Windows.Forms.Button();
            this.btnBrowseText = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.RadioSpace = new System.Windows.Forms.RadioButton();
            this.btnReset = new System.Windows.Forms.Button();
            this.ListSN = new System.Windows.Forms.ListView();
            this.TabPageView = new System.Windows.Forms.TabPage();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.btnViewSN = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.TabRegisterSN = new System.Windows.Forms.TabControl();
            this.OpenFileListSN = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.DGridTerminalSN)).BeginInit();
            this.TabPageRegister.SuspendLayout();
            this.TabPageView.SuspendLayout();
            this.TabRegisterSN.SuspendLayout();
            this.SuspendLayout();
            // 
            // DGridTerminalSN
            // 
            this.DGridTerminalSN.DataMember = "";
            this.DGridTerminalSN.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.DGridTerminalSN.Location = new System.Drawing.Point(8, 40);
            this.DGridTerminalSN.Name = "DGridTerminalSN";
            this.DGridTerminalSN.Size = new System.Drawing.Size(776, 344);
            this.DGridTerminalSN.TabIndex = 0;
            // 
            // TabPageRegister
            // 
            this.TabPageRegister.Controls.Add(this.CheckBoxColHeader);
            this.TabPageRegister.Controls.Add(this.tbFileName);
            this.TabPageRegister.Controls.Add(this.RadioVertical);
            this.TabPageRegister.Controls.Add(this.RadioComma);
            this.TabPageRegister.Controls.Add(this.btnExit);
            this.TabPageRegister.Controls.Add(this.Progress);
            this.TabPageRegister.Controls.Add(this.btnStartAll);
            this.TabPageRegister.Controls.Add(this.btnBrowseText);
            this.TabPageRegister.Controls.Add(this.Label1);
            this.TabPageRegister.Controls.Add(this.Label2);
            this.TabPageRegister.Controls.Add(this.RadioSpace);
            this.TabPageRegister.Controls.Add(this.btnReset);
            this.TabPageRegister.Controls.Add(this.ListSN);
            this.TabPageRegister.Location = new System.Drawing.Point(4, 22);
            this.TabPageRegister.Name = "TabPageRegister";
            this.TabPageRegister.Size = new System.Drawing.Size(792, 398);
            this.TabPageRegister.TabIndex = 0;
            this.TabPageRegister.Text = "Register SN";
            // 
            // CheckBoxColHeader
            // 
            this.CheckBoxColHeader.Checked = true;
            this.CheckBoxColHeader.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxColHeader.Location = new System.Drawing.Point(8, 72);
            this.CheckBoxColHeader.Name = "CheckBoxColHeader";
            this.CheckBoxColHeader.Size = new System.Drawing.Size(208, 24);
            this.CheckBoxColHeader.TabIndex = 51;
            this.CheckBoxColHeader.Text = "The Table has column Header";
            // 
            // tbFileName
            // 
            this.tbFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFileName.Location = new System.Drawing.Point(120, 8);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(232, 21);
            this.tbFileName.TabIndex = 40;
            // 
            // RadioVertical
            // 
            this.RadioVertical.Checked = true;
            this.RadioVertical.Location = new System.Drawing.Point(344, 40);
            this.RadioVertical.Name = "RadioVertical";
            this.RadioVertical.Size = new System.Drawing.Size(104, 24);
            this.RadioVertical.TabIndex = 50;
            this.RadioVertical.TabStop = true;
            this.RadioVertical.Text = "Vertical Bar";
            // 
            // RadioComma
            // 
            this.RadioComma.Location = new System.Drawing.Point(120, 40);
            this.RadioComma.Name = "RadioComma";
            this.RadioComma.Size = new System.Drawing.Size(104, 24);
            this.RadioComma.TabIndex = 49;
            this.RadioComma.Text = "Comma";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(216, 136);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 24);
            this.btnExit.TabIndex = 46;
            this.btnExit.Text = "&Close";
            // 
            // Progress
            // 
            this.Progress.Location = new System.Drawing.Point(8, 104);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(776, 23);
            this.Progress.Step = 1;
            this.Progress.TabIndex = 47;
            // 
            // btnStartAll
            // 
            this.btnStartAll.Location = new System.Drawing.Point(8, 136);
            this.btnStartAll.Name = "btnStartAll";
            this.btnStartAll.Size = new System.Drawing.Size(88, 24);
            this.btnStartAll.TabIndex = 43;
            this.btnStartAll.Text = "&Start";
            // 
            // btnBrowseText
            // 
            this.btnBrowseText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseText.Location = new System.Drawing.Point(368, 8);
            this.btnBrowseText.Name = "btnBrowseText";
            this.btnBrowseText.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseText.TabIndex = 42;
            this.btnBrowseText.Text = "Browse";
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(8, 8);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 23);
            this.Label1.TabIndex = 39;
            this.Label1.Text = "File Name (*.txt)";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label2
            // 
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(8, 40);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(100, 23);
            this.Label2.TabIndex = 38;
            this.Label2.Text = "Separator Value :";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RadioSpace
            // 
            this.RadioSpace.Location = new System.Drawing.Point(232, 40);
            this.RadioSpace.Name = "RadioSpace";
            this.RadioSpace.Size = new System.Drawing.Size(104, 24);
            this.RadioSpace.TabIndex = 48;
            this.RadioSpace.Text = "Space";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(112, 136);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(88, 24);
            this.btnReset.TabIndex = 44;
            this.btnReset.Text = "&Reset";
            // 
            // ListSN
            // 
            this.ListSN.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListSN.LabelEdit = true;
            this.ListSN.Location = new System.Drawing.Point(8, 168);
            this.ListSN.Name = "ListSN";
            this.ListSN.Size = new System.Drawing.Size(776, 216);
            this.ListSN.TabIndex = 35;
            this.ListSN.UseCompatibleStateImageBehavior = false;
            this.ListSN.View = System.Windows.Forms.View.Details;
            // 
            // TabPageView
            // 
            this.TabPageView.Controls.Add(this.DGridTerminalSN);
            this.TabPageView.Controls.Add(this.tbSearch);
            this.TabPageView.Controls.Add(this.btnViewSN);
            this.TabPageView.Controls.Add(this.btnSearch);
            this.TabPageView.Location = new System.Drawing.Point(4, 22);
            this.TabPageView.Name = "TabPageView";
            this.TabPageView.Size = new System.Drawing.Size(792, 398);
            this.TabPageView.TabIndex = 1;
            this.TabPageView.Text = "View Terminal-SN";
            // 
            // tbSearch
            // 
            this.tbSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSearch.Location = new System.Drawing.Point(520, 8);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(184, 21);
            this.tbSearch.TabIndex = 41;
            this.tbSearch.Text = "Search...";
            // 
            // btnViewSN
            // 
            this.btnViewSN.Location = new System.Drawing.Point(8, 8);
            this.btnViewSN.Name = "btnViewSN";
            this.btnViewSN.Size = new System.Drawing.Size(128, 21);
            this.btnViewSN.TabIndex = 52;
            this.btnViewSN.Text = "Refresh Table";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(712, 8);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(72, 21);
            this.btnSearch.TabIndex = 45;
            this.btnSearch.Text = "Go";
            // 
            // TabRegisterSN
            // 
            this.TabRegisterSN.Controls.Add(this.TabPageView);
            this.TabRegisterSN.Controls.Add(this.TabPageRegister);
            this.TabRegisterSN.Location = new System.Drawing.Point(3, 4);
            this.TabRegisterSN.Name = "TabRegisterSN";
            this.TabRegisterSN.SelectedIndex = 0;
            this.TabRegisterSN.Size = new System.Drawing.Size(800, 424);
            this.TabRegisterSN.TabIndex = 38;
            // 
            // OpenFileListSN
            // 
            this.OpenFileListSN.Filter = "Text Files|*.txt";
            // 
            // FrmSN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 431);
            this.Controls.Add(this.TabRegisterSN);
            this.Name = "FrmSN";
            this.Text = "Terminal Serial Number";
            ((System.ComponentModel.ISupportInitialize)(this.DGridTerminalSN)).EndInit();
            this.TabPageRegister.ResumeLayout(false);
            this.TabPageRegister.PerformLayout();
            this.TabPageView.ResumeLayout(false);
            this.TabPageView.PerformLayout();
            this.TabRegisterSN.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DataGrid DGridTerminalSN;
        internal System.Windows.Forms.TabPage TabPageRegister;
        internal System.Windows.Forms.CheckBox CheckBoxColHeader;
        internal System.Windows.Forms.TextBox tbFileName;
        internal System.Windows.Forms.RadioButton RadioVertical;
        internal System.Windows.Forms.RadioButton RadioComma;
        internal System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.ProgressBar Progress;
        internal System.Windows.Forms.Button btnStartAll;
        internal System.Windows.Forms.Button btnBrowseText;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.RadioButton RadioSpace;
        internal System.Windows.Forms.Button btnReset;
        internal System.Windows.Forms.ListView ListSN;
        internal System.Windows.Forms.TabPage TabPageView;
        internal System.Windows.Forms.TextBox tbSearch;
        internal System.Windows.Forms.Button btnViewSN;
        internal System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.TabControl TabRegisterSN;
        internal System.Windows.Forms.OpenFileDialog OpenFileListSN;
    }
}