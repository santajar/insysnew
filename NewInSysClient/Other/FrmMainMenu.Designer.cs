namespace InSys
{
    partial class FrmMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMainMenu));
            this.MainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.MenuItem1 = new System.Windows.Forms.MenuItem();
            this.mnuUser = new System.Windows.Forms.MenuItem();
            this.mnuLogin = new System.Windows.Forms.MenuItem();
            this.mnuLogOff = new System.Windows.Forms.MenuItem();
            this.MenuItem15 = new System.Windows.Forms.MenuItem();
            this.mnuPassword = new System.Windows.Forms.MenuItem();
            this.MenuItem5 = new System.Windows.Forms.MenuItem();
            this.mnuSetting = new System.Windows.Forms.MenuItem();
            this.mnuDBConn = new System.Windows.Forms.MenuItem();
            this.mnuAllDownload = new System.Windows.Forms.MenuItem();
            this.mnuAllSN = new System.Windows.Forms.MenuItem();
            this.MenuItem7 = new System.Windows.Forms.MenuItem();
            this.mnuUserManagement = new System.Windows.Forms.MenuItem();
            this.mnuAdmin = new System.Windows.Forms.MenuItem();
            this.MenuItem9 = new System.Windows.Forms.MenuItem();
            this.mnuAudit = new System.Windows.Forms.MenuItem();
            this.mnuTLV = new System.Windows.Forms.MenuItem();
            this.MenuItem2 = new System.Windows.Forms.MenuItem();
            this.mnuExit = new System.Windows.Forms.MenuItem();
            this.mnuBrowse = new System.Windows.Forms.MenuItem();
            this.mnuSelectDB = new System.Windows.Forms.MenuItem();
            this.mnuEDC = new System.Windows.Forms.MenuItem();
            this.mnuServer = new System.Windows.Forms.MenuItem();
            this.mnuStartListen = new System.Windows.Forms.MenuItem();
            this.mnudownload = new System.Windows.Forms.MenuItem();
            this.mnuSerial = new System.Windows.Forms.MenuItem();
            this.MenuItem4 = new System.Windows.Forms.MenuItem();
            this.mnuHelp = new System.Windows.Forms.MenuItem();
            this.MenuItem11 = new System.Windows.Forms.MenuItem();
            this.mnuAbout = new System.Windows.Forms.MenuItem();
            this.mnuAdministrator = new System.Windows.Forms.MenuItem();
            this.mnuAppSetting = new System.Windows.Forms.MenuItem();
            this.mnuRegister = new System.Windows.Forms.MenuItem();
            this.mnuSuperUser = new System.Windows.Forms.MenuItem();
            this.StatusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
            this.StatusBarPanel2 = new System.Windows.Forms.StatusBarPanel();
            this.StatusBarPanel3 = new System.Windows.Forms.StatusBarPanel();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.ToolBarMenu = new System.Windows.Forms.ToolBar();
            this.ToolBarLogin = new System.Windows.Forms.ToolBarButton();
            this.ToolBarLogOff = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton5 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarExit = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton1 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarPrimeDB = new System.Windows.Forms.ToolBarButton();
            this.ToolBarUserMan = new System.Windows.Forms.ToolBarButton();
            this.ToolBarAudit = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton2 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarSelectDB = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton3 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarUnited = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton4 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarDownload = new System.Windows.Forms.ToolBarButton();
            this.ImageIcon = new System.Windows.Forms.ImageList(this.components);
            this.StatusBar1 = new System.Windows.Forms.StatusBar();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel3)).BeginInit();
            this.SuspendLayout();
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.MenuItem1,
            this.mnuBrowse,
            this.mnuServer,
            this.mnudownload,
            this.MenuItem4,
            this.mnuAdministrator});
            // 
            // MenuItem1
            // 
            this.MenuItem1.Index = 0;
            this.MenuItem1.MdiList = true;
            this.MenuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuUser,
            this.MenuItem5,
            this.mnuSetting,
            this.MenuItem7,
            this.mnuUserManagement,
            this.mnuAdmin,
            this.MenuItem9,
            this.mnuAudit,
            this.mnuTLV,
            this.MenuItem2,
            this.mnuExit});
            this.MenuItem1.Text = "&Application";
            // 
            // mnuUser
            // 
            this.mnuUser.Index = 0;
            this.mnuUser.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuLogin,
            this.mnuLogOff,
            this.MenuItem15,
            this.mnuPassword});
            this.mnuUser.Text = "User";
            // 
            // mnuLogin
            // 
            this.mnuLogin.Index = 0;
            this.mnuLogin.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
            this.mnuLogin.Text = "Login";
            this.mnuLogin.Click += new System.EventHandler(this.mnuLogin_Click);
            // 
            // mnuLogOff
            // 
            this.mnuLogOff.Enabled = false;
            this.mnuLogOff.Index = 1;
            this.mnuLogOff.Shortcut = System.Windows.Forms.Shortcut.CtrlF;
            this.mnuLogOff.Text = "LogOff";
            // 
            // MenuItem15
            // 
            this.MenuItem15.Index = 2;
            this.MenuItem15.Text = "-";
            // 
            // mnuPassword
            // 
            this.mnuPassword.Index = 3;
            this.mnuPassword.Text = "Change Password";
            this.mnuPassword.Click += new System.EventHandler(this.mnuPassword_Click);
            // 
            // MenuItem5
            // 
            this.MenuItem5.Index = 1;
            this.MenuItem5.Text = "-";
            // 
            // mnuSetting
            // 
            this.mnuSetting.Index = 2;
            this.mnuSetting.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuDBConn,
            this.mnuAllDownload,
            this.mnuAllSN});
            this.mnuSetting.Text = "Setting";
            // 
            // mnuDBConn
            // 
            this.mnuDBConn.Index = 0;
            this.mnuDBConn.Shortcut = System.Windows.Forms.Shortcut.CtrlC;
            this.mnuDBConn.Text = "Primary Database Connection";
            this.mnuDBConn.Click += new System.EventHandler(this.mnuDBConn_Click);
            // 
            // mnuAllDownload
            // 
            this.mnuAllDownload.Index = 1;
            this.mnuAllDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.mnuAllDownload.Text = "Allow Download";
            // 
            // mnuAllSN
            // 
            this.mnuAllSN.Index = 2;
            this.mnuAllSN.Text = "SN Validation";
            // 
            // MenuItem7
            // 
            this.MenuItem7.Index = 3;
            this.MenuItem7.Text = "-";
            // 
            // mnuUserManagement
            // 
            this.mnuUserManagement.Enabled = false;
            this.mnuUserManagement.Index = 4;
            this.mnuUserManagement.Shortcut = System.Windows.Forms.Shortcut.CtrlU;
            this.mnuUserManagement.Text = "User Management";
            this.mnuUserManagement.Click += new System.EventHandler(this.mnuUserManagement_Click);
            // 
            // mnuAdmin
            // 
            this.mnuAdmin.Enabled = false;
            this.mnuAdmin.Index = 5;
            this.mnuAdmin.Text = "Administrator";
            this.mnuAdmin.Click += new System.EventHandler(this.mnuAdmin_Click);
            // 
            // MenuItem9
            // 
            this.MenuItem9.Index = 6;
            this.MenuItem9.Text = "-";
            // 
            // mnuAudit
            // 
            this.mnuAudit.Enabled = false;
            this.mnuAudit.Index = 7;
            this.mnuAudit.Shortcut = System.Windows.Forms.Shortcut.CtrlA;
            this.mnuAudit.Text = "Audit Trail";
            this.mnuAudit.Click += new System.EventHandler(this.mnuAudit_Click);
            // 
            // mnuTLV
            // 
            this.mnuTLV.Index = 8;
            this.mnuTLV.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.mnuTLV.Text = "Populate TLV Database...";
            this.mnuTLV.Click += new System.EventHandler(this.mnuTLV_Click);
            // 
            // MenuItem2
            // 
            this.MenuItem2.Index = 9;
            this.MenuItem2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 10;
            this.mnuExit.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuBrowse
            // 
            this.mnuBrowse.Enabled = false;
            this.mnuBrowse.Index = 1;
            this.mnuBrowse.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSelectDB,
            this.mnuEDC});
            this.mnuBrowse.Text = "&Browse";
            // 
            // mnuSelectDB
            // 
            this.mnuSelectDB.Index = 0;
            this.mnuSelectDB.Shortcut = System.Windows.Forms.Shortcut.CtrlB;
            this.mnuSelectDB.Text = "Database Selection";
            this.mnuSelectDB.Click += new System.EventHandler(this.mnuSelectDB_Click);
            // 
            // mnuEDC
            // 
            this.mnuEDC.Index = 1;
            this.mnuEDC.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.mnuEDC.Text = "EDC IP-Tracking";
            this.mnuEDC.Click += new System.EventHandler(this.mnuEDC_Click);
            // 
            // mnuServer
            // 
            this.mnuServer.Enabled = false;
            this.mnuServer.Index = 2;
            this.mnuServer.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuStartListen});
            this.mnuServer.Text = "&Server";
            // 
            // mnuStartListen
            // 
            this.mnuStartListen.Index = 0;
            this.mnuStartListen.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftS;
            this.mnuStartListen.Text = "Start Listening";
            // 
            // mnudownload
            // 
            this.mnudownload.Enabled = false;
            this.mnudownload.Index = 3;
            this.mnudownload.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSerial});
            this.mnudownload.Text = "&Download";
            // 
            // mnuSerial
            // 
            this.mnuSerial.Index = 0;
            this.mnuSerial.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.mnuSerial.Text = "Serial";
            this.mnuSerial.Click += new System.EventHandler(this.mnuSerial_Click);
            // 
            // MenuItem4
            // 
            this.MenuItem4.Index = 4;
            this.MenuItem4.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuHelp,
            this.MenuItem11,
            this.mnuAbout});
            this.MenuItem4.Text = "&Help";
            // 
            // mnuHelp
            // 
            this.mnuHelp.Index = 0;
            this.mnuHelp.Shortcut = System.Windows.Forms.Shortcut.F1;
            this.mnuHelp.Text = "Help";
            // 
            // MenuItem11
            // 
            this.MenuItem11.Index = 1;
            this.MenuItem11.Text = "-";
            // 
            // mnuAbout
            // 
            this.mnuAbout.Index = 2;
            this.mnuAbout.Text = "About";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // mnuAdministrator
            // 
            this.mnuAdministrator.Index = 5;
            this.mnuAdministrator.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuAppSetting,
            this.mnuRegister,
            this.mnuSuperUser});
            this.mnuAdministrator.Text = "Administrator";
            this.mnuAdministrator.Visible = false;
            // 
            // mnuAppSetting
            // 
            this.mnuAppSetting.Index = 0;
            this.mnuAppSetting.Text = "Application";
            this.mnuAppSetting.Click += new System.EventHandler(this.mnuAppSetting_Click);
            // 
            // mnuRegister
            // 
            this.mnuRegister.Index = 1;
            this.mnuRegister.Text = "Register";
            this.mnuRegister.Click += new System.EventHandler(this.mnuRegister_Click);
            // 
            // mnuSuperUser
            // 
            this.mnuSuperUser.Index = 2;
            this.mnuSuperUser.Text = "Super User";
            this.mnuSuperUser.Visible = false;
            // 
            // StatusBarPanel1
            // 
            this.StatusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.StatusBarPanel1.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusBarPanel1.Icon")));
            this.StatusBarPanel1.Name = "StatusBarPanel1";
            this.StatusBarPanel1.Width = 554;
            // 
            // StatusBarPanel2
            // 
            this.StatusBarPanel2.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.StatusBarPanel2.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusBarPanel2.Icon")));
            this.StatusBarPanel2.Name = "StatusBarPanel2";
            this.StatusBarPanel2.Text = "waktu";
            this.StatusBarPanel2.Width = 71;
            // 
            // StatusBarPanel3
            // 
            this.StatusBarPanel3.Alignment = System.Windows.Forms.HorizontalAlignment.Right;
            this.StatusBarPanel3.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.StatusBarPanel3.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusBarPanel3.Icon")));
            this.StatusBarPanel3.Name = "StatusBarPanel3";
            this.StatusBarPanel3.Text = "tanggal";
            this.StatusBarPanel3.Width = 79;
            // 
            // Timer1
            // 
            this.Timer1.Enabled = true;
            this.Timer1.Interval = 1000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // ToolBarMenu
            // 
            this.ToolBarMenu.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.ToolBarMenu.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.ToolBarLogin,
            this.ToolBarLogOff,
            this.ToolBarButton5,
            this.ToolBarExit,
            this.ToolBarButton1,
            this.ToolBarPrimeDB,
            this.ToolBarUserMan,
            this.ToolBarAudit,
            this.ToolBarButton2,
            this.ToolBarSelectDB,
            this.ToolBarButton3,
            this.ToolBarUnited,
            this.ToolBarButton4,
            this.ToolBarDownload});
            this.ToolBarMenu.DropDownArrows = true;
            this.ToolBarMenu.ImageList = this.ImageIcon;
            this.ToolBarMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolBarMenu.Name = "ToolBarMenu";
            this.ToolBarMenu.ShowToolTips = true;
            this.ToolBarMenu.Size = new System.Drawing.Size(720, 42);
            this.ToolBarMenu.TabIndex = 5;
            this.ToolBarMenu.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.ToolBarMenu_ButtonClick);
            // 
            // ToolBarLogin
            // 
            this.ToolBarLogin.ImageIndex = 0;
            this.ToolBarLogin.Name = "ToolBarLogin";
            this.ToolBarLogin.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarLogin.ToolTipText = "Login";
            // 
            // ToolBarLogOff
            // 
            this.ToolBarLogOff.ImageIndex = 1;
            this.ToolBarLogOff.Name = "ToolBarLogOff";
            this.ToolBarLogOff.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarLogOff.ToolTipText = "Log Off";
            // 
            // ToolBarButton5
            // 
            this.ToolBarButton5.Name = "ToolBarButton5";
            this.ToolBarButton5.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarExit
            // 
            this.ToolBarExit.ImageIndex = 5;
            this.ToolBarExit.Name = "ToolBarExit";
            this.ToolBarExit.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarExit.ToolTipText = "Exit";
            // 
            // ToolBarButton1
            // 
            this.ToolBarButton1.Name = "ToolBarButton1";
            this.ToolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarPrimeDB
            // 
            this.ToolBarPrimeDB.ImageIndex = 8;
            this.ToolBarPrimeDB.Name = "ToolBarPrimeDB";
            this.ToolBarPrimeDB.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarPrimeDB.ToolTipText = "Set Primary Database";
            // 
            // ToolBarUserMan
            // 
            this.ToolBarUserMan.ImageIndex = 7;
            this.ToolBarUserMan.Name = "ToolBarUserMan";
            this.ToolBarUserMan.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarUserMan.ToolTipText = "User Management";
            // 
            // ToolBarAudit
            // 
            this.ToolBarAudit.ImageIndex = 2;
            this.ToolBarAudit.Name = "ToolBarAudit";
            this.ToolBarAudit.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarAudit.ToolTipText = "Audit Trail";
            // 
            // ToolBarButton2
            // 
            this.ToolBarButton2.Name = "ToolBarButton2";
            this.ToolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarSelectDB
            // 
            this.ToolBarSelectDB.ImageIndex = 3;
            this.ToolBarSelectDB.Name = "ToolBarSelectDB";
            this.ToolBarSelectDB.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarSelectDB.ToolTipText = "Database Selection";
            // 
            // ToolBarButton3
            // 
            this.ToolBarButton3.Name = "ToolBarButton3";
            this.ToolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarUnited
            // 
            this.ToolBarUnited.ImageIndex = 6;
            this.ToolBarUnited.Name = "ToolBarUnited";
            this.ToolBarUnited.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarUnited.ToolTipText = "Start United";
            // 
            // ToolBarButton4
            // 
            this.ToolBarButton4.Name = "ToolBarButton4";
            this.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarDownload
            // 
            this.ToolBarDownload.ImageIndex = 4;
            this.ToolBarDownload.Name = "ToolBarDownload";
            this.ToolBarDownload.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarDownload.ToolTipText = "Download";
            // 
            // ImageIcon
            // 
            this.ImageIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageIcon.ImageStream")));
            this.ImageIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageIcon.Images.SetKeyName(0, "");
            this.ImageIcon.Images.SetKeyName(1, "");
            this.ImageIcon.Images.SetKeyName(2, "");
            this.ImageIcon.Images.SetKeyName(3, "");
            this.ImageIcon.Images.SetKeyName(4, "");
            this.ImageIcon.Images.SetKeyName(5, "");
            this.ImageIcon.Images.SetKeyName(6, "");
            this.ImageIcon.Images.SetKeyName(7, "");
            this.ImageIcon.Images.SetKeyName(8, "");
            // 
            // StatusBar1
            // 
            this.StatusBar1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBar1.Location = new System.Drawing.Point(0, 356);
            this.StatusBar1.Name = "StatusBar1";
            this.StatusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.StatusBarPanel1,
            this.StatusBarPanel2,
            this.StatusBarPanel3});
            this.StatusBar1.ShowPanels = true;
            this.StatusBar1.Size = new System.Drawing.Size(720, 24);
            this.StatusBar1.TabIndex = 4;
            this.StatusBar1.Text = "StatusBar1";
            // 
            // FrmMainMenu
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(720, 380);
            this.Controls.Add(this.ToolBarMenu);
            this.Controls.Add(this.StatusBar1);
            this.IsMdiContainer = true;
            this.Menu = this.MainMenu1;
            this.Name = "FrmMainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMainMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarPanel3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.MainMenu MainMenu1;
        internal System.Windows.Forms.MenuItem MenuItem1;
        internal System.Windows.Forms.MenuItem mnuUser;
        internal System.Windows.Forms.MenuItem mnuLogin;
        internal System.Windows.Forms.MenuItem mnuLogOff;
        internal System.Windows.Forms.MenuItem MenuItem15;
        internal System.Windows.Forms.MenuItem mnuPassword;
        internal System.Windows.Forms.MenuItem MenuItem5;
        internal System.Windows.Forms.MenuItem mnuSetting;
        internal System.Windows.Forms.MenuItem mnuDBConn;
        internal System.Windows.Forms.MenuItem mnuAllDownload;
        internal System.Windows.Forms.MenuItem mnuAllSN;
        internal System.Windows.Forms.MenuItem MenuItem7;
        internal System.Windows.Forms.MenuItem mnuUserManagement;
        internal System.Windows.Forms.MenuItem mnuAdmin;
        internal System.Windows.Forms.MenuItem MenuItem9;
        internal System.Windows.Forms.MenuItem mnuAudit;
        internal System.Windows.Forms.MenuItem mnuTLV;
        internal System.Windows.Forms.MenuItem MenuItem2;
        internal System.Windows.Forms.MenuItem mnuExit;
        internal System.Windows.Forms.MenuItem mnuBrowse;
        internal System.Windows.Forms.MenuItem mnuSelectDB;
        internal System.Windows.Forms.MenuItem mnuEDC;
        internal System.Windows.Forms.MenuItem mnuServer;
        internal System.Windows.Forms.MenuItem mnuStartListen;
        internal System.Windows.Forms.MenuItem mnudownload;
        internal System.Windows.Forms.MenuItem mnuSerial;
        internal System.Windows.Forms.MenuItem MenuItem4;
        internal System.Windows.Forms.MenuItem mnuHelp;
        internal System.Windows.Forms.MenuItem MenuItem11;
        internal System.Windows.Forms.MenuItem mnuAbout;
        internal System.Windows.Forms.MenuItem mnuAdministrator;
        internal System.Windows.Forms.MenuItem mnuAppSetting;
        internal System.Windows.Forms.MenuItem mnuRegister;
        internal System.Windows.Forms.MenuItem mnuSuperUser;
        internal System.Windows.Forms.StatusBarPanel StatusBarPanel1;
        internal System.Windows.Forms.StatusBarPanel StatusBarPanel2;
        internal System.Windows.Forms.StatusBarPanel StatusBarPanel3;
        internal System.Windows.Forms.Timer Timer1;
        internal System.Windows.Forms.ToolBar ToolBarMenu;
        internal System.Windows.Forms.ToolBarButton ToolBarLogin;
        internal System.Windows.Forms.ToolBarButton ToolBarLogOff;
        internal System.Windows.Forms.ToolBarButton ToolBarButton5;
        internal System.Windows.Forms.ToolBarButton ToolBarExit;
        internal System.Windows.Forms.ToolBarButton ToolBarButton1;
        internal System.Windows.Forms.ToolBarButton ToolBarPrimeDB;
        internal System.Windows.Forms.ToolBarButton ToolBarUserMan;
        internal System.Windows.Forms.ToolBarButton ToolBarAudit;
        internal System.Windows.Forms.ToolBarButton ToolBarButton2;
        internal System.Windows.Forms.ToolBarButton ToolBarSelectDB;
        internal System.Windows.Forms.ToolBarButton ToolBarButton3;
        internal System.Windows.Forms.ToolBarButton ToolBarUnited;
        internal System.Windows.Forms.ToolBarButton ToolBarButton4;
        internal System.Windows.Forms.ToolBarButton ToolBarDownload;
        internal System.Windows.Forms.ImageList ImageIcon;
        internal System.Windows.Forms.StatusBar StatusBar1;
    }
}