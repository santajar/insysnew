namespace InSys
{
    partial class FrmDebug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDiff = new System.Windows.Forms.Button();
            this.ColumnItemName = new System.Windows.Forms.ColumnHeader();
            this.ListDiff = new System.Windows.Forms.ListView();
            this.ColumnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.ColumnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.ColumnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.ListTag2 = new System.Windows.Forms.ListView();
            this.ColumnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.ColumnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.ColumnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.ColumnValue = new System.Windows.Forms.ColumnHeader();
            this.ListTag1 = new System.Windows.Forms.ListView();
            this.ColumnTag = new System.Windows.Forms.ColumnHeader();
            this.btnLoad1 = new System.Windows.Forms.Button();
            this.cboDb1 = new System.Windows.Forms.ComboBox();
            this.btnLoad2 = new System.Windows.Forms.Button();
            this.cboDb2 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnDiff
            // 
            this.btnDiff.Location = new System.Drawing.Point(500, 338);
            this.btnDiff.Name = "btnDiff";
            this.btnDiff.Size = new System.Drawing.Size(75, 23);
            this.btnDiff.TabIndex = 27;
            this.btnDiff.Text = "Load Difference";
            this.btnDiff.Visible = false;
            // 
            // ColumnItemName
            // 
            this.ColumnItemName.Text = "ItemName";
            this.ColumnItemName.Width = 200;
            // 
            // ListDiff
            // 
            this.ListDiff.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader4,
            this.ColumnHeader5,
            this.ColumnHeader6});
            this.ListDiff.FullRowSelect = true;
            this.ListDiff.Location = new System.Drawing.Point(12, 338);
            this.ListDiff.Name = "ListDiff";
            this.ListDiff.Size = new System.Drawing.Size(472, 104);
            this.ListDiff.TabIndex = 26;
            this.ListDiff.UseCompatibleStateImageBehavior = false;
            this.ListDiff.View = System.Windows.Forms.View.Details;
            this.ListDiff.Visible = false;
            // 
            // ColumnHeader4
            // 
            this.ColumnHeader4.Text = "Tag";
            // 
            // ColumnHeader5
            // 
            this.ColumnHeader5.Text = "ItemName";
            this.ColumnHeader5.Width = 200;
            // 
            // ColumnHeader6
            // 
            this.ColumnHeader6.Text = "Default Value";
            this.ColumnHeader6.Width = 200;
            // 
            // ListTag2
            // 
            this.ListTag2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader1,
            this.ColumnHeader2,
            this.ColumnHeader3});
            this.ListTag2.FullRowSelect = true;
            this.ListTag2.Location = new System.Drawing.Point(500, 42);
            this.ListTag2.Name = "ListTag2";
            this.ListTag2.Size = new System.Drawing.Size(472, 288);
            this.ListTag2.TabIndex = 25;
            this.ListTag2.UseCompatibleStateImageBehavior = false;
            this.ListTag2.View = System.Windows.Forms.View.Details;
            // 
            // ColumnHeader1
            // 
            this.ColumnHeader1.Text = "Tag";
            // 
            // ColumnHeader2
            // 
            this.ColumnHeader2.Text = "ItemName";
            this.ColumnHeader2.Width = 200;
            // 
            // ColumnHeader3
            // 
            this.ColumnHeader3.Text = "Default Value";
            this.ColumnHeader3.Width = 200;
            // 
            // ColumnValue
            // 
            this.ColumnValue.Text = "Value";
            this.ColumnValue.Width = 200;
            // 
            // ListTag1
            // 
            this.ListTag1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnTag,
            this.ColumnItemName,
            this.ColumnValue});
            this.ListTag1.FullRowSelect = true;
            this.ListTag1.Location = new System.Drawing.Point(12, 42);
            this.ListTag1.Name = "ListTag1";
            this.ListTag1.Size = new System.Drawing.Size(472, 288);
            this.ListTag1.TabIndex = 24;
            this.ListTag1.UseCompatibleStateImageBehavior = false;
            this.ListTag1.View = System.Windows.Forms.View.Details;
            // 
            // ColumnTag
            // 
            this.ColumnTag.Text = "Tag";
            // 
            // btnLoad1
            // 
            this.btnLoad1.Location = new System.Drawing.Point(180, 10);
            this.btnLoad1.Name = "btnLoad1";
            this.btnLoad1.Size = new System.Drawing.Size(75, 23);
            this.btnLoad1.TabIndex = 21;
            this.btnLoad1.Text = "Load Item";
            this.btnLoad1.Visible = false;
            // 
            // cboDb1
            // 
            this.cboDb1.Location = new System.Drawing.Point(12, 10);
            this.cboDb1.Name = "cboDb1";
            this.cboDb1.Size = new System.Drawing.Size(152, 21);
            this.cboDb1.TabIndex = 22;
            this.cboDb1.Visible = false;
            // 
            // btnLoad2
            // 
            this.btnLoad2.Location = new System.Drawing.Point(668, 10);
            this.btnLoad2.Name = "btnLoad2";
            this.btnLoad2.Size = new System.Drawing.Size(75, 23);
            this.btnLoad2.TabIndex = 20;
            this.btnLoad2.Text = "Load Item";
            // 
            // cboDb2
            // 
            this.cboDb2.Location = new System.Drawing.Point(500, 10);
            this.cboDb2.Name = "cboDb2";
            this.cboDb2.Size = new System.Drawing.Size(152, 21);
            this.cboDb2.TabIndex = 23;
            // 
            // FrmDebug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 451);
            this.Controls.Add(this.btnDiff);
            this.Controls.Add(this.ListDiff);
            this.Controls.Add(this.ListTag2);
            this.Controls.Add(this.ListTag1);
            this.Controls.Add(this.btnLoad1);
            this.Controls.Add(this.cboDb1);
            this.Controls.Add(this.btnLoad2);
            this.Controls.Add(this.cboDb2);
            this.Name = "FrmDebug";
            this.Text = "Debug & Testing";
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnDiff;
        internal System.Windows.Forms.ColumnHeader ColumnItemName;
        internal System.Windows.Forms.ListView ListDiff;
        internal System.Windows.Forms.ColumnHeader ColumnHeader4;
        internal System.Windows.Forms.ColumnHeader ColumnHeader5;
        internal System.Windows.Forms.ColumnHeader ColumnHeader6;
        internal System.Windows.Forms.ListView ListTag2;
        internal System.Windows.Forms.ColumnHeader ColumnHeader1;
        internal System.Windows.Forms.ColumnHeader ColumnHeader2;
        internal System.Windows.Forms.ColumnHeader ColumnHeader3;
        internal System.Windows.Forms.ColumnHeader ColumnValue;
        internal System.Windows.Forms.ListView ListTag1;
        internal System.Windows.Forms.ColumnHeader ColumnTag;
        internal System.Windows.Forms.Button btnLoad1;
        internal System.Windows.Forms.ComboBox cboDb1;
        internal System.Windows.Forms.Button btnLoad2;
        internal System.Windows.Forms.ComboBox cboDb2;
    }
}