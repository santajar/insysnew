﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Security.AccessControl;
//using System.Windows.Forms;
using InSysClass;

namespace InSys
{
    class SaveFullContentClass
    {
        protected static SqlConnection oSqlConn = new SqlConnection();
        protected static string sSPProfileTextFull { get { return "spProfileTextFull"; } }
        protected static string sSPInitTempClear = "spInitTempClear";

        public static void SaveFullContent(SqlConnection _oSqlConn, string _sTerminalId)
        {
            oSqlConn = _oSqlConn;
            string sContent = sFullContent(_sTerminalId);
            string sContentCompressed = null;
            int iMaxBytePacket = iMaxByte();

            // write to file locally and compress
            if (!Directory.Exists(Environment.CurrentDirectory + @"\ZIP"))
                Directory.CreateDirectory(Environment.CurrentDirectory + @"\ZIP");
            string sFilename = Environment.CurrentDirectory + @"\ZIP\" + _sTerminalId;
            //File.Delete(sFilename);

            CommonLib.Write2File(sFilename, sContent, false);
            if (IsNetCompressSuccess(sFilename, _sTerminalId))
            {
                FileStream fsRead = new FileStream(sFilename + ".gz", FileMode.Open);
                byte[] arrbContent = new byte[fsRead.Length];
                fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
                fsRead.Close();
                sContentCompressed = CommonLib.sByteArrayToHexString(arrbContent).Replace(" ", "");

                SaveContentInit(_sTerminalId, sContent, sContentCompressed, iMaxBytePacket);
            }
            ClearFolderZip(sFilename);
        }

        protected static string sFullContent(string _sTerminalId)
        {
            string sContent = null;
            using (SqlCommand oCmd = new SqlCommand(sSPProfileTextFull, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sContent", SqlDbType.VarChar, 204800).Direction = ParameterDirection.Output;

                oCmd.ExecuteNonQuery();
                sContent = oCmd.Parameters["@sContent"].Value.ToString();
            }
            return sContent;
        }

        protected static bool IsCompressInit(string _sTerminalId)
        {
            bool isCompress = false;
            string sQuery = string.Format("SELECT dbo.IsCompressInit({0}) [CompressInit]", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        isCompress = oRead["CompressInit"].ToString() == "1" ? true : false;
                }
            }
            return isCompress;
        }

        protected static int iMaxByte()
        {
            int iMaxLength = 400;
            string sQuery = string.Format("SELECT dbo.iPacketLength() [MaxByte]");
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        iMaxLength = int.Parse(oRead["MaxByte"].ToString());
                }
            }
            return iMaxLength;
        }

        protected static bool IsNetCompressSuccess(string sFilename, string sTerminalId)
        {
            bool bReturn = false;
            if (File.Exists(sFilename))
            {
                try
                {
                    FileInfo fi = new FileInfo(sFilename);
                    using (FileStream inFile = fi.OpenRead())
                    {
                        string sGzipFile = sFilename + ".gz";
                        if (File.Exists(sGzipFile))
                            File.Delete(sGzipFile);
                        using (FileStream outFile = File.Create(sFilename + ".gz"))
                        {
                            using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                            {
                                byte[] buffer = new byte[fi.Length];
                                inFile.Read(buffer, 0, Convert.ToInt32(fi.Length));
                                compress.Write(buffer, 0, Convert.ToInt32(fi.Length));
                                bReturn = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("NetCompress " + ex.Message);
                }
            }
            return bReturn;
        }

        protected static void SaveContentInit(string _sTerminalId, string _sContent, string _sContentCompressed,
            int _iMaxByte)
        {
            ClearInitTemp(_sTerminalId);

            // jika compress, tag = GZ, langsung bagi per _iMaxByte
            // jika tidak, part per tabel, part per _iMaxByte
            // simpan ke tbInitTemp
            DataTable dtInitTemp = new DataTable();
            dtInitTemp = dtGetInitTemp(_sTerminalId, _sContent, _sContentCompressed, _iMaxByte);
            SqlBulkCopy sqlBulk = new SqlBulkCopy(oSqlConn);
            sqlBulk.DestinationTableName = "tbInitTemp";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalId", "TerminalId");
            SqlBulkCopyColumnMapping mapContent = new SqlBulkCopyColumnMapping("Content", "Content");
            SqlBulkCopyColumnMapping mapTag = new SqlBulkCopyColumnMapping("Tag", "Tag");
            SqlBulkCopyColumnMapping mapFlag = new SqlBulkCopyColumnMapping("Flag", "Flag");
            sqlBulk.ColumnMappings.Add(mapTerminalId);
            sqlBulk.ColumnMappings.Add(mapContent);
            sqlBulk.ColumnMappings.Add(mapTag);
            sqlBulk.ColumnMappings.Add(mapFlag);
            try
            {
                sqlBulk.WriteToServer(dtInitTemp);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public static void ClearInitTemp(SqlConnection _oSqlConn, string _sTerminalId)
        {
            oSqlConn = _oSqlConn;
            ClearInitTemp(_sTerminalId);
        }

        protected static void ClearInitTemp(string _sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand(sSPInitTempClear, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.ExecuteNonQuery();
            }
        }

        protected class ContentTable
        {
            string sTag = null;
            string sContent = null;

            public string Tag
            {
                get { return sTag; }
                set { sTag = value; }
            }
            public string Content
            {
                get { return sContent; }
                set { sContent = value; }
            }
        }

        protected static DataTable dtGetInitTemp(string _sTerminalId, string _sContent, string _sContentCompressed,
            int _iMaxByte)
        {
            DataTable dtInitTemp = new DataTable();
            dtInitTemp.Columns.Add("TerminalId", typeof(string));
            dtInitTemp.Columns.Add("Content", typeof(string));
            dtInitTemp.Columns.Add("Tag", typeof(string));
            dtInitTemp.Columns.Add("Flag", typeof(int));

            int iFlag = 0;
            string sTag = "GZ";
            int iOffset = 0;
            while (!string.IsNullOrEmpty(_sContentCompressed) && iOffset <= _sContentCompressed.Length)
            {
                DataRow rows = dtInitTemp.NewRow();
                rows["TerminalId"] = _sTerminalId;
                rows["Tag"] = sTag;
                rows["Flag"] = iFlag;
                rows["Content"] = _sContentCompressed.Substring(iOffset,
                    iOffset + _iMaxByte <= _sContentCompressed.Length ? _iMaxByte : _sContentCompressed.Length - iOffset);
                dtInitTemp.Rows.Add(rows);
                iOffset += _iMaxByte;
            }

            List<ContentTable> ltTemp = new List<ContentTable>();
            ltTemp = ltContentTable(_sContent);
            foreach (ContentTable ctTemp in ltTemp)
            {
                sTag = ctTemp.Tag;
                string sContent = ctTemp.Content;
                iOffset = 0;
                //while (iOffset <= sContent.Length), error, row menjadi null ketika value nya kosong.
                while (iOffset < sContent.Length)
                {
                    DataRow rows = dtInitTemp.NewRow();
                    rows["TerminalId"] = _sTerminalId;
                    rows["Tag"] = sTag;
                    rows["Flag"] = iFlag;
                    int iOffsetHeader = sContent.IndexOf(sTag + "01", iOffset + 1);
                    int iLength = 0;
                    if (iOffset + _iMaxByte <= sContent.Length)
                        if (iOffsetHeader > 0)
                            if (iOffset + _iMaxByte <= iOffsetHeader)
                                iLength = _iMaxByte;
                            else
                                iLength = iOffsetHeader - iOffset;
                        else
                            iLength = _iMaxByte;
                    else
                        iLength = sContent.Length - iOffset;
                    rows["Content"] = sContent.Substring(iOffset, iLength);
                    iOffset += iLength;

                    dtInitTemp.Rows.Add(rows);
                }
            }
            return dtInitTemp;
        }

        protected static List<ContentTable> ltContentTable(string _sContent)
        {
            List<ContentTable> ltTempContent = new List<ContentTable>();

            Array arrValues = Enum.GetValues(typeof(TagType));
            foreach (TagType oTagType in arrValues)
            {
                ContentTable oContTblTemp = new ContentTable();
                oContTblTemp.Tag = oTagType.ToString().ToUpper();
                if (oTagType.ToString() == "pk")
                    oContTblTemp.Content = sContentTable(_sContent, "504B");
                else
                    oContTblTemp.Content = sContentTable(_sContent, oTagType);
                if (!string.IsNullOrEmpty(oContTblTemp.Content))
                    ltTempContent.Add(oContTblTemp);
            }

            return ltTempContent;
        }

        protected static string sContentTable(string _sContent, TagType oTagType)
        {
            string sTemp = null;
            string sTagHeader = oTagType.ToString().ToUpper();
            string sTag = sTagHeader + "01";
            int iOffset = _sContent.IndexOf(sTag);
            int iTagLength = 0;
            string sTagValue = null;

            while (iOffset >= 0 && iOffset < _sContent.Length && _sContent.Substring(iOffset, 2) == sTagHeader)
            {
                sTag = _sContent.Substring(iOffset, 4);
                if (sTag != "AE37" && sTag != "AE38" && sTag != "TL11")
                {
                    iTagLength = int.Parse(_sContent.Substring(iOffset + 4, 2));
                    sTagValue = _sContent.Substring(iOffset + 6, iTagLength);
                    sTemp += _sContent.Substring(iOffset, 6 + iTagLength);
                    iOffset += 6 + iTagLength;
                }
                else
                {
                    iTagLength = int.Parse(_sContent.Substring(iOffset + 4, 3));
                    sTagValue = _sContent.Substring(iOffset + 7, iTagLength);
                    sTemp += _sContent.Substring(iOffset, 7 + iTagLength);
                    iOffset += 7 + iTagLength;
                }
            }
            return oTagType == TagType.ai && !string.IsNullOrEmpty(sTemp) ? "AI" + sTemp : sTemp;
        }

        protected static string sContentTable(string _sContent, string sTagType)
        {
            string sTemp = null;
            string sTagHeader = sTagType.ToString().ToUpper();
            string sTag = sTagHeader + "3031";
            int iOffset = _sContent.IndexOf(sTag);
            int iTagLength = 0;
            string sTagValue = null;

            while (iOffset >= 0 && iOffset < _sContent.Length && _sContent.Substring(iOffset, 4) == sTagHeader)
            {
                sTag = _sContent.Substring(iOffset, 8);
                iTagLength = int.Parse(CommonLib.sHexToString(_sContent.Substring(iOffset + 8, 6)));
                sTagValue = _sContent.Substring(iOffset + 14, iTagLength * 2);
                sTemp += _sContent.Substring(iOffset, 14 + (iTagLength * 2));
                iOffset += 14 + (iTagLength * 2);
            }
            return sTemp;
        }

        protected static void ClearFolderZip(string sFilename)
        {
            File.Delete(sFilename);
            File.Delete(sFilename + ".gz");
        }
    }
}
