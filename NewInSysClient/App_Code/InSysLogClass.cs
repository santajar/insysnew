﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vi.Log4Vi;
using System.Runtime.CompilerServices;

namespace InSys
{
    public static class InSysLogClass
    {
        static string rootFolder = Environment.CurrentDirectory + @"\LOGS\";
        static Logger insysClientLog = new Logger(rootFolder, name: "INSYSCLIENT", useTraceAppender: false);

        public static void Info(string text, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysClientLog.Info(text, line, member, file);
        }

        public static void Debug(string text, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysClientLog.Debug(text, line, member, file);
        }

        public static void Warn(string text, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysClientLog.Warn(text, line, member, file);
        }

        public static void Error(Exception se, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysClientLog.Error(se);

            // The logger in this class writes logs in two different files. 
            // One file is for Error and Fatal. The other file is for the other methods.
            // This line writes in the second file.
            insysClientLog.Warn("System.Exception: " + se.Message, line, member, file);
        }

        public static void Fatal(string text, [CallerLineNumber] int line = 0, [CallerMemberName] string member = "?", [CallerFilePath] string file = "?")
        {
            insysClientLog.Fatal(text, line, member, file);

            // The logger in this class writes logs in two different files. 
            // One file is for Error and Fatal. The other file is for the other methods.
            // This line writes in the second file.
            insysClientLog.Warn("System.Exception: " + text, line, member, file);
        }
    }
}