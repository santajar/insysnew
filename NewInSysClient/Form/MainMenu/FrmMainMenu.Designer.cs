namespace InSys
{
    partial class FrmMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMainMenu));
            this.mnuMain = new System.Windows.Forms.MainMenu(this.components);
            this.mnuApplication = new System.Windows.Forms.MenuItem();
            this.mnuUser = new System.Windows.Forms.MenuItem();
            this.mnuLogin = new System.Windows.Forms.MenuItem();
            this.mnuLogOff = new System.Windows.Forms.MenuItem();
            this.MenuItem15 = new System.Windows.Forms.MenuItem();
            this.mnuPassword = new System.Windows.Forms.MenuItem();
            this.MenuItemApplicationUser = new System.Windows.Forms.MenuItem();
            this.mnuSettingApp = new System.Windows.Forms.MenuItem();
            this.mnuDBConn = new System.Windows.Forms.MenuItem();
            this.MenuItemApplicationSetting = new System.Windows.Forms.MenuItem();
            this.mnuUserManagement = new System.Windows.Forms.MenuItem();
            this.mnuItemUserRight = new System.Windows.Forms.MenuItem();
            this.mnuUserGroup = new System.Windows.Forms.MenuItem();
            this.MenuItemApplicationUserManagement = new System.Windows.Forms.MenuItem();
            this.mnuExit = new System.Windows.Forms.MenuItem();
            this.mnuEdcTerminal = new System.Windows.Forms.MenuItem();
            this.mnuBrowse = new System.Windows.Forms.MenuItem();
            this.mnuSelectVs = new System.Windows.Forms.MenuItem();
            this.menuItemApplicationBrowse = new System.Windows.Forms.MenuItem();
            this.mnuSettingEdc = new System.Windows.Forms.MenuItem();
            this.mnuAllSN = new System.Windows.Forms.MenuItem();
            this.mnuAllowInit = new System.Windows.Forms.MenuItem();
            this.mnuCompressInit = new System.Windows.Forms.MenuItem();
            this.mnuAutoInit = new System.Windows.Forms.MenuItem();
            this.mnuLocationProfiling = new System.Windows.Forms.MenuItem();
            this.mnuAddLocation = new System.Windows.Forms.MenuItem();
            this.mnuLocationView = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.mnuDataUpload = new System.Windows.Forms.MenuItem();
            this.mnuTemplateDefinition = new System.Windows.Forms.MenuItem();
            this.mnuUploadDefinition = new System.Windows.Forms.MenuItem();
            this.mnuUpload = new System.Windows.Forms.MenuItem();
            this.menuItemApplicationDataUpload = new System.Windows.Forms.MenuItem();
            this.mnLogoEDC = new System.Windows.Forms.MenuItem();
            this.mnLogoEDCPrint = new System.Windows.Forms.MenuItem();
            this.mnLogoEDCIdle = new System.Windows.Forms.MenuItem();
            this.mnLogoEDCMerchant = new System.Windows.Forms.MenuItem();
            this.mnLogoEDCReceipt = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.mnuRD_RmtDownloadFtps = new System.Windows.Forms.MenuItem();
            this.mnuRD_TerminalGroupFtps = new System.Windows.Forms.MenuItem();
            this.mnuRD_UploadSoftwareFtps = new System.Windows.Forms.MenuItem();
            this.mnuRmtDownloadISO = new System.Windows.Forms.MenuItem();
            this.mnuAppPackage = new System.Windows.Forms.MenuItem();
            this.mnuUpdateRD = new System.Windows.Forms.MenuItem();
            this.menuItem21 = new System.Windows.Forms.MenuItem();
            this.mnuAppPackageCpl = new System.Windows.Forms.MenuItem();
            this.mnuSettingSoftwarePackage = new System.Windows.Forms.MenuItem();
            this.mnuSettingTerminalIDPackage = new System.Windows.Forms.MenuItem();
            this.mnuRemoteDownload = new System.Windows.Forms.MenuItem();
            this.mnuReport = new System.Windows.Forms.MenuItem();
            this.mnuAuditTrail = new System.Windows.Forms.MenuItem();
            this.mnuInitializeTrail = new System.Windows.Forms.MenuItem();
            this.mnuViewAuditTrailUser = new System.Windows.Forms.MenuItem();
            this.mnuSoftwareInitTrail = new System.Windows.Forms.MenuItem();
            this.mnuTLV = new System.Windows.Forms.MenuItem();
            this.mnuQueryReport = new System.Windows.Forms.MenuItem();
            this.mnuRD_AuditDownload = new System.Windows.Forms.MenuItem();
            this.mnuMonitor = new System.Windows.Forms.MenuItem();
            this.mnuStartListen = new System.Windows.Forms.MenuItem();
            this.mnuEDCTracking = new System.Windows.Forms.MenuItem();
            this.mnuSerialDownload = new System.Windows.Forms.MenuItem();
            this.mnSoftwareProgress = new System.Windows.Forms.MenuItem();
            this.mnEchoPing = new System.Windows.Forms.MenuItem();
            this.mnSoftwareProgressCPL = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.mnuTools = new System.Windows.Forms.MenuItem();
            this.mnUpdateConnection = new System.Windows.Forms.MenuItem();
            this.mnUpdateParameter = new System.Windows.Forms.MenuItem();
            this.mnReplaceTID = new System.Windows.Forms.MenuItem();
            this.mnOpenTogglingAllowInit = new System.Windows.Forms.MenuItem();
            this.mnQcEms = new System.Windows.Forms.MenuItem();
            this.mnMoveTid = new System.Windows.Forms.MenuItem();
            this.mnUploadTidRD = new System.Windows.Forms.MenuItem();
            this.mnUploadEms = new System.Windows.Forms.MenuItem();
            this.mnGenerateUploadPTD = new System.Windows.Forms.MenuItem();
            this.mnuQueryLoader = new System.Windows.Forms.MenuItem();
            this.MenuItem4 = new System.Windows.Forms.MenuItem();
            this.mnuHelp = new System.Windows.Forms.MenuItem();
            this.mnuRegister = new System.Windows.Forms.MenuItem();
            this.MenuItemHelp = new System.Windows.Forms.MenuItem();
            this.mnuAbout = new System.Windows.Forms.MenuItem();
            this.mnuSuperAdmin = new System.Windows.Forms.MenuItem();
            this.menuSUDatabase = new System.Windows.Forms.MenuItem();
            this.menuSUVersion = new System.Windows.Forms.MenuItem();
            this.menuSUVersion_Add = new System.Windows.Forms.MenuItem();
            this.menuSUVersion_Edit = new System.Windows.Forms.MenuItem();
            this.menuSUVersion_Delete = new System.Windows.Forms.MenuItem();
            this.menuSUTagItem = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItem_AddUpdate = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItem_Delete = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItemExport = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItemImport = new System.Windows.Forms.MenuItem();
            this.mnuSUTagItem_AddUpdateNew = new System.Windows.Forms.MenuItem();
            this.menuSUCopyCardRange = new System.Windows.Forms.MenuItem();
            this.menuSUCopyGPRS = new System.Windows.Forms.MenuItem();
            this.menuSUCopyTLE = new System.Windows.Forms.MenuItem();
            this.menuSUCopyAID = new System.Windows.Forms.MenuItem();
            this.menuSUCopyCAPK = new System.Windows.Forms.MenuItem();
            this.menuSUCopyRD = new System.Windows.Forms.MenuItem();
            this.menuSUCopyPromo = new System.Windows.Forms.MenuItem();
            this.menuSUCard = new System.Windows.Forms.MenuItem();
            this.menuSUImportCard = new System.Windows.Forms.MenuItem();
            this.menuSUExportCard = new System.Windows.Forms.MenuItem();
            this.menuSUModify = new System.Windows.Forms.MenuItem();
            this.mnuSUData = new System.Windows.Forms.MenuItem();
            this.mnuSUImportData = new System.Windows.Forms.MenuItem();
            this.mnuSUExportData = new System.Windows.Forms.MenuItem();
            this.mnuSUExportToTxtFile = new System.Windows.Forms.MenuItem();
            this.mnuSUExportToDb = new System.Windows.Forms.MenuItem();
            this.mnuSUMoveData = new System.Windows.Forms.MenuItem();
            this.mnuSUCopyData = new System.Windows.Forms.MenuItem();
            this.mnuSUTable = new System.Windows.Forms.MenuItem();
            this.mnuSUAddTable = new System.Windows.Forms.MenuItem();
            this.mnuSUDeleteTable = new System.Windows.Forms.MenuItem();
            this.mnuSUFields = new System.Windows.Forms.MenuItem();
            this.mnuSUAddFields = new System.Windows.Forms.MenuItem();
            this.mnuSUUpdateFields = new System.Windows.Forms.MenuItem();
            this.mnuSUDeleteFields = new System.Windows.Forms.MenuItem();
            this.menuSUProfile = new System.Windows.Forms.MenuItem();
            this.menuAddCard = new System.Windows.Forms.MenuItem();
            this.menuDeleteCard = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.menuSUApplicationMgmt = new System.Windows.Forms.MenuItem();
            this.menuSUSwProfile = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuSUInitConn = new System.Windows.Forms.MenuItem();
            this.menuSUSetFolder = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuSUAudit = new System.Windows.Forms.MenuItem();
            this.menuSUTag5 = new System.Windows.Forms.MenuItem();
            this.menuSUQueryReport = new System.Windows.Forms.MenuItem();
            this.menuItem22 = new System.Windows.Forms.MenuItem();
            this.menuSUEdcMonitoring = new System.Windows.Forms.MenuItem();
            this.StatusBarApplication = new System.Windows.Forms.StatusBarPanel();
            this.StatusBarTime = new System.Windows.Forms.StatusBarPanel();
            this.StatusBarDate = new System.Windows.Forms.StatusBarPanel();
            this.TimerClock = new System.Windows.Forms.Timer(this.components);
            this.ToolBarMenu = new System.Windows.Forms.ToolBar();
            this.ToolBarLogin = new System.Windows.Forms.ToolBarButton();
            this.ToolBarLogOff = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton5 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarExit = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton1 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarAudit = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton2 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarSelectVs = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton3 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarUnited = new System.Windows.Forms.ToolBarButton();
            this.ToolBarButton4 = new System.Windows.Forms.ToolBarButton();
            this.ToolBarDownload = new System.Windows.Forms.ToolBarButton();
            this.ImageIcon = new System.Windows.Forms.ImageList(this.components);
            this.StatusBar1 = new System.Windows.Forms.StatusBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarApplication)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarDate)).BeginInit();
            this.SuspendLayout();
            // 
            // mnuMain
            // 
            this.mnuMain.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuApplication,
            this.mnuEdcTerminal,
            this.mnuReport,
            this.mnuMonitor,
            this.MenuItem4,
            this.mnuSuperAdmin});
            // 
            // mnuApplication
            // 
            this.mnuApplication.Index = 0;
            this.mnuApplication.MdiList = true;
            this.mnuApplication.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuUser,
            this.MenuItemApplicationUser,
            this.mnuSettingApp,
            this.MenuItemApplicationSetting,
            this.mnuUserManagement,
            this.mnuItemUserRight,
            this.mnuUserGroup,
            this.MenuItemApplicationUserManagement,
            this.mnuExit});
            this.mnuApplication.Text = "&Application";
            // 
            // mnuUser
            // 
            this.mnuUser.Index = 0;
            this.mnuUser.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuLogin,
            this.mnuLogOff,
            this.MenuItem15,
            this.mnuPassword});
            this.mnuUser.Text = "&User";
            // 
            // mnuLogin
            // 
            this.mnuLogin.Index = 0;
            this.mnuLogin.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftI;
            this.mnuLogin.Text = "Log&in";
            this.mnuLogin.Click += new System.EventHandler(this.mnuLogin_Click);
            // 
            // mnuLogOff
            // 
            this.mnuLogOff.Index = 1;
            this.mnuLogOff.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftO;
            this.mnuLogOff.Text = "Log&out";
            this.mnuLogOff.Click += new System.EventHandler(this.mnuLogOff_Click);
            // 
            // MenuItem15
            // 
            this.MenuItem15.Index = 2;
            this.MenuItem15.Text = "-";
            // 
            // mnuPassword
            // 
            this.mnuPassword.Index = 3;
            this.mnuPassword.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftC;
            this.mnuPassword.Text = "&Change Password";
            this.mnuPassword.Click += new System.EventHandler(this.mnuPassword_Click);
            // 
            // MenuItemApplicationUser
            // 
            this.MenuItemApplicationUser.Index = 1;
            this.MenuItemApplicationUser.Text = "-";
            // 
            // mnuSettingApp
            // 
            this.mnuSettingApp.Index = 2;
            this.mnuSettingApp.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuDBConn});
            this.mnuSettingApp.Text = "&Setting";
            // 
            // mnuDBConn
            // 
            this.mnuDBConn.Index = 0;
            this.mnuDBConn.Shortcut = System.Windows.Forms.Shortcut.F8;
            this.mnuDBConn.Text = "&Primary Database Connection";
            this.mnuDBConn.Click += new System.EventHandler(this.mnuDBConn_Click);
            // 
            // MenuItemApplicationSetting
            // 
            this.MenuItemApplicationSetting.Index = 3;
            this.MenuItemApplicationSetting.Text = "-";
            // 
            // mnuUserManagement
            // 
            this.mnuUserManagement.Index = 4;
            this.mnuUserManagement.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftM;
            this.mnuUserManagement.Text = "User &Management";
            this.mnuUserManagement.Click += new System.EventHandler(this.mnuUserManagement_Click);
            // 
            // mnuItemUserRight
            // 
            this.mnuItemUserRight.Index = 5;
            this.mnuItemUserRight.Text = "Item &Group Management";
            this.mnuItemUserRight.Click += new System.EventHandler(this.mnuItemUserRight_Click);
            // 
            // mnuUserGroup
            // 
            this.mnuUserGroup.Index = 6;
            this.mnuUserGroup.Text = "U&ser Group Management";
            this.mnuUserGroup.Click += new System.EventHandler(this.mnuUserGroup_Click);
            // 
            // MenuItemApplicationUserManagement
            // 
            this.MenuItemApplicationUserManagement.Index = 7;
            this.MenuItemApplicationUserManagement.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 8;
            this.mnuExit.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftX;
            this.mnuExit.Text = "E&xit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuEdcTerminal
            // 
            this.mnuEdcTerminal.Index = 1;
            this.mnuEdcTerminal.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuBrowse,
            this.menuItemApplicationBrowse,
            this.mnuSettingEdc,
            this.menuItem3,
            this.mnuDataUpload,
            this.menuItemApplicationDataUpload,
            this.mnLogoEDC,
            this.menuItem7,
            this.mnuRD_RmtDownloadFtps,
            this.mnuRmtDownloadISO});
            this.mnuEdcTerminal.Text = "&EDC Terminal";
            // 
            // mnuBrowse
            // 
            this.mnuBrowse.Index = 0;
            this.mnuBrowse.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSelectVs});
            this.mnuBrowse.Text = "&Browse";
            // 
            // mnuSelectVs
            // 
            this.mnuSelectVs.Index = 0;
            this.mnuSelectVs.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftB;
            this.mnuSelectVs.Text = "&Version Selection";
            this.mnuSelectVs.Click += new System.EventHandler(this.mnuSelectVs_Click);
            // 
            // menuItemApplicationBrowse
            // 
            this.menuItemApplicationBrowse.Index = 1;
            this.menuItemApplicationBrowse.Text = "-";
            // 
            // mnuSettingEdc
            // 
            this.mnuSettingEdc.Index = 2;
            this.mnuSettingEdc.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuAllSN,
            this.mnuAllowInit,
            this.mnuCompressInit,
            this.mnuAutoInit,
            this.mnuLocationProfiling});
            this.mnuSettingEdc.Text = "&Setting";
            // 
            // mnuAllSN
            // 
            this.mnuAllSN.Index = 0;
            this.mnuAllSN.Shortcut = System.Windows.Forms.Shortcut.F9;
            this.mnuAllSN.Text = "&SN Validation";
            this.mnuAllSN.Visible = false;
            this.mnuAllSN.Click += new System.EventHandler(this.mnuAllSN_Click);
            // 
            // mnuAllowInit
            // 
            this.mnuAllowInit.Index = 1;
            this.mnuAllowInit.Shortcut = System.Windows.Forms.Shortcut.F10;
            this.mnuAllowInit.Text = "&Allow Initialize";
            this.mnuAllowInit.Click += new System.EventHandler(this.mnuAllowInit_Click);
            // 
            // mnuCompressInit
            // 
            this.mnuCompressInit.Index = 2;
            this.mnuCompressInit.Shortcut = System.Windows.Forms.Shortcut.F12;
            this.mnuCompressInit.Text = "&Compress Initialize";
            this.mnuCompressInit.Click += new System.EventHandler(this.mnuCompressInit_Click);
            // 
            // mnuAutoInit
            // 
            this.mnuAutoInit.Index = 3;
            this.mnuAutoInit.Shortcut = System.Windows.Forms.Shortcut.F11;
            this.mnuAutoInit.Text = "Auto &Init";
            this.mnuAutoInit.Click += new System.EventHandler(this.mnuAutoInit_Click);
            // 
            // mnuLocationProfiling
            // 
            this.mnuLocationProfiling.Index = 4;
            this.mnuLocationProfiling.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuAddLocation,
            this.mnuLocationView});
            this.mnuLocationProfiling.Text = "&Location Profiling";
            // 
            // mnuAddLocation
            // 
            this.mnuAddLocation.Index = 0;
            this.mnuAddLocation.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftA;
            this.mnuAddLocation.Text = "&Add Location";
            this.mnuAddLocation.Click += new System.EventHandler(this.mnuAddLocation_Click);
            // 
            // mnuLocationView
            // 
            this.mnuLocationView.Index = 1;
            this.mnuLocationView.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftV;
            this.mnuLocationView.Text = "&View Profile Location";
            this.mnuLocationView.Click += new System.EventHandler(this.mnuLocationView_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 3;
            this.menuItem3.Text = "-";
            // 
            // mnuDataUpload
            // 
            this.mnuDataUpload.Index = 4;
            this.mnuDataUpload.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuTemplateDefinition,
            this.mnuUploadDefinition,
            this.mnuUpload});
            this.mnuDataUpload.Text = "&Data Upload";
            // 
            // mnuTemplateDefinition
            // 
            this.mnuTemplateDefinition.Index = 0;
            this.mnuTemplateDefinition.Shortcut = System.Windows.Forms.Shortcut.Ctrl1;
            this.mnuTemplateDefinition.Text = "&Template Definition";
            this.mnuTemplateDefinition.Click += new System.EventHandler(this.mnuTemplateDefinition_Click);
            // 
            // mnuUploadDefinition
            // 
            this.mnuUploadDefinition.Index = 1;
            this.mnuUploadDefinition.Shortcut = System.Windows.Forms.Shortcut.Ctrl2;
            this.mnuUploadDefinition.Text = "&Upload Definition";
            this.mnuUploadDefinition.Click += new System.EventHandler(this.mnuUploadDefinition_Click);
            // 
            // mnuUpload
            // 
            this.mnuUpload.Index = 2;
            this.mnuUpload.Shortcut = System.Windows.Forms.Shortcut.Ctrl3;
            this.mnuUpload.Text = "U&pload Data";
            this.mnuUpload.Click += new System.EventHandler(this.mnuUpload_Click);
            // 
            // menuItemApplicationDataUpload
            // 
            this.menuItemApplicationDataUpload.Index = 5;
            this.menuItemApplicationDataUpload.Text = "-";
            // 
            // mnLogoEDC
            // 
            this.mnLogoEDC.Index = 6;
            this.mnLogoEDC.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnLogoEDCPrint,
            this.mnLogoEDCIdle,
            this.mnLogoEDCMerchant,
            this.mnLogoEDCReceipt});
            this.mnLogoEDC.Text = "Logo EDC";
            // 
            // mnLogoEDCPrint
            // 
            this.mnLogoEDCPrint.Index = 0;
            this.mnLogoEDCPrint.Text = "&Print";
            this.mnLogoEDCPrint.Click += new System.EventHandler(this.mnLogoEDCPrint_Click);
            // 
            // mnLogoEDCIdle
            // 
            this.mnLogoEDCIdle.Index = 1;
            this.mnLogoEDCIdle.Text = "&Idle";
            this.mnLogoEDCIdle.Click += new System.EventHandler(this.mnLogoEDCIdle_Click);
            // 
            // mnLogoEDCMerchant
            // 
            this.mnLogoEDCMerchant.Index = 2;
            this.mnLogoEDCMerchant.Text = "Merchant";
            this.mnLogoEDCMerchant.Click += new System.EventHandler(this.mnLogoEDCMerchant_Click);
            // 
            // mnLogoEDCReceipt
            // 
            this.mnLogoEDCReceipt.Index = 3;
            this.mnLogoEDCReceipt.Text = "Receipt";
            this.mnLogoEDCReceipt.Click += new System.EventHandler(this.mnLogoEDCReceipt_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 7;
            this.menuItem7.Text = "-";
            // 
            // mnuRD_RmtDownloadFtps
            // 
            this.mnuRD_RmtDownloadFtps.Index = 8;
            this.mnuRD_RmtDownloadFtps.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuRD_TerminalGroupFtps,
            this.mnuRD_UploadSoftwareFtps});
            this.mnuRD_RmtDownloadFtps.Text = "Remote Download FTPS";
            // 
            // mnuRD_TerminalGroupFtps
            // 
            this.mnuRD_TerminalGroupFtps.Index = 0;
            this.mnuRD_TerminalGroupFtps.Text = "Terminal Group";
            this.mnuRD_TerminalGroupFtps.Click += new System.EventHandler(this.mnuRD_TerminalGroupFtps_Click);
            // 
            // mnuRD_UploadSoftwareFtps
            // 
            this.mnuRD_UploadSoftwareFtps.Index = 1;
            this.mnuRD_UploadSoftwareFtps.Text = "Upload Software";
            this.mnuRD_UploadSoftwareFtps.Click += new System.EventHandler(this.mnuRD_UploadSoftwareFtps_Click);
            // 
            // mnuRmtDownloadISO
            // 
            this.mnuRmtDownloadISO.Index = 9;
            this.mnuRmtDownloadISO.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuAppPackage,
            this.mnuUpdateRD,
            this.menuItem21,
            this.mnuAppPackageCpl,
            this.mnuSettingSoftwarePackage,
            this.mnuSettingTerminalIDPackage,
            this.mnuRemoteDownload});
            this.mnuRmtDownloadISO.Text = "Remote Download ISO";
            // 
            // mnuAppPackage
            // 
            this.mnuAppPackage.Index = 0;
            this.mnuAppPackage.Text = "Software &Package";
            this.mnuAppPackage.Click += new System.EventHandler(this.mnuAppPackage_Click);
            // 
            // mnuUpdateRD
            // 
            this.mnuUpdateRD.Index = 1;
            this.mnuUpdateRD.Text = "Update Remote Download";
            this.mnuUpdateRD.Click += new System.EventHandler(this.mnUpdateRD_Click);
            // 
            // menuItem21
            // 
            this.menuItem21.Index = 2;
            this.menuItem21.Text = "-";
            // 
            // mnuAppPackageCpl
            // 
            this.mnuAppPackageCpl.Index = 3;
            this.mnuAppPackageCpl.Text = "Software Package CPL";
            this.mnuAppPackageCpl.Click += new System.EventHandler(this.menuItem8_Click);
            // 
            // mnuSettingSoftwarePackage
            // 
            this.mnuSettingSoftwarePackage.Index = 4;
            this.mnuSettingSoftwarePackage.Text = "Setting Software Package";
            this.mnuSettingSoftwarePackage.Click += new System.EventHandler(this.mnuSettingSoftwarePackage_Click);
            // 
            // mnuSettingTerminalIDPackage
            // 
            this.mnuSettingTerminalIDPackage.Index = 5;
            this.mnuSettingTerminalIDPackage.Text = "Setting TerminalID Package";
            this.mnuSettingTerminalIDPackage.Click += new System.EventHandler(this.mnuSettingTerminalIDPackage_Click);
            // 
            // mnuRemoteDownload
            // 
            this.mnuRemoteDownload.Index = 6;
            this.mnuRemoteDownload.Text = "Remote Download";
            this.mnuRemoteDownload.Click += new System.EventHandler(this.mnuRemoteDownload_Click);
            // 
            // mnuReport
            // 
            this.mnuReport.Index = 2;
            this.mnuReport.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuAuditTrail,
            this.mnuInitializeTrail,
            this.mnuViewAuditTrailUser,
            this.mnuSoftwareInitTrail,
            this.mnuTLV,
            this.mnuQueryReport,
            this.mnuRD_AuditDownload});
            this.mnuReport.Text = "&Report";
            this.mnuReport.Click += new System.EventHandler(this.mnuReport_Click);
            this.mnuReport.Popup += new System.EventHandler(this.mnuReport_Popup);
            // 
            // mnuAuditTrail
            // 
            this.mnuAuditTrail.Index = 0;
            this.mnuAuditTrail.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftM;
            this.mnuAuditTrail.Text = "&Audit Trail";
            this.mnuAuditTrail.Click += new System.EventHandler(this.mnuAuditTrail_Click);
            // 
            // mnuInitializeTrail
            // 
            this.mnuInitializeTrail.Index = 1;
            this.mnuInitializeTrail.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftN;
            this.mnuInitializeTrail.Text = "&Initialize Trail";
            this.mnuInitializeTrail.Click += new System.EventHandler(this.mnuInitializeTrail_Click);
            // 
            // mnuViewAuditTrailUser
            // 
            this.mnuViewAuditTrailUser.Index = 2;
            this.mnuViewAuditTrailUser.Text = "View Audit Trail User";
            this.mnuViewAuditTrailUser.Click += new System.EventHandler(this.mnuViewAuditTrailUser_Click);
            // 
            // mnuSoftwareInitTrail
            // 
            this.mnuSoftwareInitTrail.Index = 3;
            this.mnuSoftwareInitTrail.Text = "Software Init Trail";
            this.mnuSoftwareInitTrail.Visible = false;
            this.mnuSoftwareInitTrail.Click += new System.EventHandler(this.mnuSoftwareInitTrail_Click);
            // 
            // mnuTLV
            // 
            this.mnuTLV.Index = 4;
            this.mnuTLV.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftP;
            this.mnuTLV.Text = "&Populate TLV Database";
            this.mnuTLV.Click += new System.EventHandler(this.mnuTLV_Click);
            // 
            // mnuQueryReport
            // 
            this.mnuQueryReport.Index = 5;
            this.mnuQueryReport.Text = "&Query Report";
            this.mnuQueryReport.Click += new System.EventHandler(this.mnuQueryReport_Click);
            // 
            // mnuRD_AuditDownload
            // 
            this.mnuRD_AuditDownload.Index = 6;
            this.mnuRD_AuditDownload.Text = "FTPS Download";
            this.mnuRD_AuditDownload.Click += new System.EventHandler(this.mnuRD_AuditDownload_Click);
            // 
            // mnuMonitor
            // 
            this.mnuMonitor.Index = 3;
            this.mnuMonitor.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuStartListen,
            this.mnuEDCTracking,
            this.mnuSerialDownload,
            this.mnSoftwareProgress,
            this.mnEchoPing,
            this.mnSoftwareProgressCPL,
            this.menuItem10,
            this.mnuTools});
            this.mnuMonitor.Text = "&Utilities";
            // 
            // mnuStartListen
            // 
            this.mnuStartListen.Index = 0;
            this.mnuStartListen.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftU;
            this.mnuStartListen.Text = "&Start United";
            this.mnuStartListen.Click += new System.EventHandler(this.mnuStartListen_Click);
            // 
            // mnuEDCTracking
            // 
            this.mnuEDCTracking.Enabled = false;
            this.mnuEDCTracking.Index = 1;
            this.mnuEDCTracking.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftT;
            this.mnuEDCTracking.Text = "&EDC IP-Tracking";
            this.mnuEDCTracking.Visible = false;
            this.mnuEDCTracking.Click += new System.EventHandler(this.mnuEDC_Click);
            // 
            // mnuSerialDownload
            // 
            this.mnuSerialDownload.Enabled = false;
            this.mnuSerialDownload.Index = 2;
            this.mnuSerialDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftD;
            this.mnuSerialDownload.Text = "&M5100/X1000";
            this.mnuSerialDownload.Visible = false;
            this.mnuSerialDownload.Click += new System.EventHandler(this.mnuSerialDownload_Click);
            // 
            // mnSoftwareProgress
            // 
            this.mnSoftwareProgress.Index = 3;
            this.mnSoftwareProgress.Text = "Init Software Progress";
            this.mnSoftwareProgress.Click += new System.EventHandler(this.mnuSoftwareProgress_Click);
            // 
            // mnEchoPing
            // 
            this.mnEchoPing.Enabled = false;
            this.mnEchoPing.Index = 4;
            this.mnEchoPing.Text = "Echo Ping";
            this.mnEchoPing.Visible = false;
            this.mnEchoPing.Click += new System.EventHandler(this.mnuEchoPing_Click);
            // 
            // mnSoftwareProgressCPL
            // 
            this.mnSoftwareProgressCPL.Index = 5;
            this.mnSoftwareProgressCPL.Text = "Init Software Progress CPL";
            this.mnSoftwareProgressCPL.Click += new System.EventHandler(this.mnSoftwareProgressCPL_Click);
            // 
            // menuItem10
            // 
            this.menuItem10.Index = 6;
            this.menuItem10.Text = "-";
            // 
            // mnuTools
            // 
            this.mnuTools.Index = 7;
            this.mnuTools.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnUpdateConnection,
            this.mnUpdateParameter,
            this.mnReplaceTID,
            this.mnOpenTogglingAllowInit,
            this.mnQcEms,
            this.mnMoveTid,
            this.mnUploadTidRD,
            this.mnUploadEms,
            this.mnGenerateUploadPTD,
            this.mnuQueryLoader});
            this.mnuTools.Text = "Tools";
            // 
            // mnUpdateConnection
            // 
            this.mnUpdateConnection.Index = 0;
            this.mnUpdateConnection.Text = "Update Connection";
            this.mnUpdateConnection.Click += new System.EventHandler(this.mnUpdateConnection_Click);
            // 
            // mnUpdateParameter
            // 
            this.mnUpdateParameter.Index = 1;
            this.mnUpdateParameter.Text = "Update Parameter";
            this.mnUpdateParameter.Click += new System.EventHandler(this.mnUpdateParameter_Click);
            // 
            // mnReplaceTID
            // 
            this.mnReplaceTID.Index = 2;
            this.mnReplaceTID.Text = "Replace TID";
            this.mnReplaceTID.Click += new System.EventHandler(this.mnReplaceTID_Click);
            // 
            // mnOpenTogglingAllowInit
            // 
            this.mnOpenTogglingAllowInit.Index = 3;
            this.mnOpenTogglingAllowInit.Text = "Open Toggling Allow Init";
            this.mnOpenTogglingAllowInit.Click += new System.EventHandler(this.mnOpenTogglingAllowInit_Click);
            // 
            // mnQcEms
            // 
            this.mnQcEms.Index = 4;
            this.mnQcEms.Text = "QC EMS";
            this.mnQcEms.Click += new System.EventHandler(this.mnQcEms_Click);
            // 
            // mnMoveTid
            // 
            this.mnMoveTid.Index = 5;
            this.mnMoveTid.Text = "Move TID";
            this.mnMoveTid.Click += new System.EventHandler(this.mnMoveTid_Click);
            // 
            // mnUploadTidRD
            // 
            this.mnUploadTidRD.Index = 6;
            this.mnUploadTidRD.Text = "Upload TID RD";
            this.mnUploadTidRD.Click += new System.EventHandler(this.mnUploadTidRD_Click);
            // 
            // mnUploadEms
            // 
            this.mnUploadEms.Index = 7;
            this.mnUploadEms.Text = "Upload EMS";
            this.mnUploadEms.Click += new System.EventHandler(this.mnUploadEms_Click);
            // 
            // mnGenerateUploadPTD
            // 
            this.mnGenerateUploadPTD.Index = 8;
            this.mnGenerateUploadPTD.Text = "Generate Upload PTD(CRP)";
            this.mnGenerateUploadPTD.Click += new System.EventHandler(this.mnGenerateUploadPTD_Click);
            // 
            // mnuQueryLoader
            // 
            this.mnuQueryLoader.Index = 9;
            this.mnuQueryLoader.Text = "Query Loader";
            this.mnuQueryLoader.Visible = false;
            // 
            // MenuItem4
            // 
            this.MenuItem4.Index = 4;
            this.MenuItem4.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuHelp,
            this.mnuRegister,
            this.MenuItemHelp,
            this.mnuAbout});
            this.MenuItem4.Text = "&Help";
            // 
            // mnuHelp
            // 
            this.mnuHelp.Index = 0;
            this.mnuHelp.Shortcut = System.Windows.Forms.Shortcut.F1;
            this.mnuHelp.Text = "&Help";
            this.mnuHelp.Click += new System.EventHandler(this.mnuHelp_Click);
            // 
            // mnuRegister
            // 
            this.mnuRegister.Enabled = false;
            this.mnuRegister.Index = 1;
            this.mnuRegister.Text = "Register";
            this.mnuRegister.Click += new System.EventHandler(this.mnuRegister_Click);
            // 
            // MenuItemHelp
            // 
            this.MenuItemHelp.Index = 2;
            this.MenuItemHelp.Text = "-";
            // 
            // mnuAbout
            // 
            this.mnuAbout.Index = 3;
            this.mnuAbout.Text = "&About";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // mnuSuperAdmin
            // 
            this.mnuSuperAdmin.Index = 5;
            this.mnuSuperAdmin.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuSUDatabase,
            this.menuSUModify,
            this.menuSUProfile,
            this.menuItem6,
            this.menuSUApplicationMgmt,
            this.menuSUSwProfile,
            this.menuItem2,
            this.menuSUInitConn,
            this.menuSUSetFolder,
            this.menuItem1,
            this.menuSUAudit,
            this.menuSUTag5,
            this.menuSUQueryReport,
            this.menuItem22,
            this.menuSUEdcMonitoring});
            this.mnuSuperAdmin.Text = "&Super User";
            // 
            // menuSUDatabase
            // 
            this.menuSUDatabase.Index = 0;
            this.menuSUDatabase.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuSUVersion,
            this.menuSUTagItem,
            this.menuSUCopyCardRange,
            this.menuSUCopyGPRS,
            this.menuSUCopyTLE,
            this.menuSUCopyAID,
            this.menuSUCopyCAPK,
            this.menuSUCopyRD,
            this.menuSUCopyPromo,
            this.menuSUCard});
            this.menuSUDatabase.Text = "&Database";
            // 
            // menuSUVersion
            // 
            this.menuSUVersion.Index = 0;
            this.menuSUVersion.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuSUVersion_Add,
            this.menuSUVersion_Edit,
            this.menuSUVersion_Delete});
            this.menuSUVersion.Text = "&Version";
            // 
            // menuSUVersion_Add
            // 
            this.menuSUVersion_Add.Index = 0;
            this.menuSUVersion_Add.Text = "&Add";
            this.menuSUVersion_Add.Click += new System.EventHandler(this.menuSUVersion_Add_Click);
            // 
            // menuSUVersion_Edit
            // 
            this.menuSUVersion_Edit.Index = 1;
            this.menuSUVersion_Edit.Text = "Edit";
            this.menuSUVersion_Edit.Click += new System.EventHandler(this.menuSUVersion_Edit_Click);
            // 
            // menuSUVersion_Delete
            // 
            this.menuSUVersion_Delete.Index = 2;
            this.menuSUVersion_Delete.Text = "&Delete";
            this.menuSUVersion_Delete.Click += new System.EventHandler(this.menuSUVersion_Delete_Click);
            // 
            // menuSUTagItem
            // 
            this.menuSUTagItem.Index = 1;
            this.menuSUTagItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUTagItem_AddUpdate,
            this.mnuSUTagItem_Delete,
            this.menuItem5,
            this.mnuSUTagItemExport,
            this.mnuSUTagItemImport,
            this.mnuSUTagItem_AddUpdateNew});
            this.menuSUTagItem.Text = "Tag / &Item";
            // 
            // mnuSUTagItem_AddUpdate
            // 
            this.mnuSUTagItem_AddUpdate.Index = 0;
            this.mnuSUTagItem_AddUpdate.Text = "&Add / Update";
            this.mnuSUTagItem_AddUpdate.Click += new System.EventHandler(this.menuSUTagItem_AddUpdate_Click);
            // 
            // mnuSUTagItem_Delete
            // 
            this.mnuSUTagItem_Delete.Index = 1;
            this.mnuSUTagItem_Delete.Text = "&Delete";
            this.mnuSUTagItem_Delete.Click += new System.EventHandler(this.menuSUTagItem_Delete_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 2;
            this.menuItem5.Text = "-";
            // 
            // mnuSUTagItemExport
            // 
            this.mnuSUTagItemExport.Index = 3;
            this.mnuSUTagItemExport.Text = "&Export";
            this.mnuSUTagItemExport.Click += new System.EventHandler(this.menuSUTagItemExport_Click);
            // 
            // mnuSUTagItemImport
            // 
            this.mnuSUTagItemImport.Index = 4;
            this.mnuSUTagItemImport.Text = "&Import";
            this.mnuSUTagItemImport.Click += new System.EventHandler(this.menuSUTagItemImport_Click);
            // 
            // mnuSUTagItem_AddUpdateNew
            // 
            this.mnuSUTagItem_AddUpdateNew.Index = 5;
            this.mnuSUTagItem_AddUpdateNew.Text = "Add-Update";
            this.mnuSUTagItem_AddUpdateNew.Click += new System.EventHandler(this.menuSUTagItem_AddUpdateNew_Click);
            // 
            // menuSUCopyCardRange
            // 
            this.menuSUCopyCardRange.Index = 2;
            this.menuSUCopyCardRange.Text = "Copy &Card Range";
            this.menuSUCopyCardRange.Click += new System.EventHandler(this.menuSUCopyCardRange_Click);
            // 
            // menuSUCopyGPRS
            // 
            this.menuSUCopyGPRS.Index = 3;
            this.menuSUCopyGPRS.Text = "Copy &GPRS";
            this.menuSUCopyGPRS.Click += new System.EventHandler(this.menuSUCopyGPRS_Click);
            // 
            // menuSUCopyTLE
            // 
            this.menuSUCopyTLE.Index = 4;
            this.menuSUCopyTLE.Text = "Copy &TLE";
            this.menuSUCopyTLE.Click += new System.EventHandler(this.menuSUCopyTLE_Click);
            // 
            // menuSUCopyAID
            // 
            this.menuSUCopyAID.Index = 5;
            this.menuSUCopyAID.Text = "Copy AID";
            this.menuSUCopyAID.Click += new System.EventHandler(this.mnSUCopyAID_Click);
            // 
            // menuSUCopyCAPK
            // 
            this.menuSUCopyCAPK.Index = 6;
            this.menuSUCopyCAPK.Text = "Copy CAPK";
            this.menuSUCopyCAPK.Click += new System.EventHandler(this.mnSUCopyCAPK_Click);
            // 
            // menuSUCopyRD
            // 
            this.menuSUCopyRD.Index = 7;
            this.menuSUCopyRD.Text = "Copy Remote Download";
            this.menuSUCopyRD.Click += new System.EventHandler(this.mnCopyRD_Click);
            // 
            // menuSUCopyPromo
            // 
            this.menuSUCopyPromo.Index = 8;
            this.menuSUCopyPromo.Text = "Copy Promo Management";
            this.menuSUCopyPromo.Click += new System.EventHandler(this.menuSUCopyPromo_Click);
            // 
            // menuSUCard
            // 
            this.menuSUCard.Index = 9;
            this.menuSUCard.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuSUImportCard,
            this.menuSUExportCard});
            this.menuSUCard.Text = "C&ard";
            // 
            // menuSUImportCard
            // 
            this.menuSUImportCard.Index = 0;
            this.menuSUImportCard.Text = "&Import Card";
            this.menuSUImportCard.Click += new System.EventHandler(this.menuSUImportCard_Click);
            // 
            // menuSUExportCard
            // 
            this.menuSUExportCard.Index = 1;
            this.menuSUExportCard.Text = "&Export Card";
            this.menuSUExportCard.Click += new System.EventHandler(this.menuSUExportCard_Click);
            // 
            // menuSUModify
            // 
            this.menuSUModify.Index = 1;
            this.menuSUModify.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUData,
            this.mnuSUTable,
            this.mnuSUFields});
            this.menuSUModify.Text = "&Modify";
            // 
            // mnuSUData
            // 
            this.mnuSUData.Index = 0;
            this.mnuSUData.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUImportData,
            this.mnuSUExportData,
            this.mnuSUMoveData,
            this.mnuSUCopyData});
            this.mnuSUData.Text = "&Data...";
            // 
            // mnuSUImportData
            // 
            this.mnuSUImportData.Index = 0;
            this.mnuSUImportData.Text = "&Import Text File to Profile";
            this.mnuSUImportData.Click += new System.EventHandler(this.menuSUImportData_Click);
            // 
            // mnuSUExportData
            // 
            this.mnuSUExportData.Index = 1;
            this.mnuSUExportData.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUExportToTxtFile,
            this.mnuSUExportToDb});
            this.mnuSUExportData.Text = "&Export Profile to...";
            // 
            // mnuSUExportToTxtFile
            // 
            this.mnuSUExportToTxtFile.Index = 0;
            this.mnuSUExportToTxtFile.Text = "&Text File";
            this.mnuSUExportToTxtFile.Click += new System.EventHandler(this.menuSUExportToTxtFile_Click);
            // 
            // mnuSUExportToDb
            // 
            this.mnuSUExportToDb.Index = 1;
            this.mnuSUExportToDb.Text = "&Database";
            this.mnuSUExportToDb.Click += new System.EventHandler(this.menuSUExportToDb_Click);
            // 
            // mnuSUMoveData
            // 
            this.mnuSUMoveData.Index = 2;
            this.mnuSUMoveData.Text = "&Move Data";
            this.mnuSUMoveData.Visible = false;
            this.mnuSUMoveData.Click += new System.EventHandler(this.mnuMoveData_Click);
            // 
            // mnuSUCopyData
            // 
            this.mnuSUCopyData.Index = 3;
            this.mnuSUCopyData.Text = "&Copy Data";
            this.mnuSUCopyData.Click += new System.EventHandler(this.menuSUCopyData_Click);
            // 
            // mnuSUTable
            // 
            this.mnuSUTable.Enabled = false;
            this.mnuSUTable.Index = 1;
            this.mnuSUTable.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUAddTable,
            this.mnuSUDeleteTable});
            this.mnuSUTable.Text = "Table...";
            this.mnuSUTable.Visible = false;
            // 
            // mnuSUAddTable
            // 
            this.mnuSUAddTable.Index = 0;
            this.mnuSUAddTable.Text = "Add Table";
            this.mnuSUAddTable.Click += new System.EventHandler(this.menuSUAddTable_Click);
            // 
            // mnuSUDeleteTable
            // 
            this.mnuSUDeleteTable.Index = 1;
            this.mnuSUDeleteTable.Text = "Delete Table";
            this.mnuSUDeleteTable.Click += new System.EventHandler(this.menuSUDeleteTable_Click);
            // 
            // mnuSUFields
            // 
            this.mnuSUFields.Enabled = false;
            this.mnuSUFields.Index = 2;
            this.mnuSUFields.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuSUAddFields,
            this.mnuSUUpdateFields,
            this.mnuSUDeleteFields});
            this.mnuSUFields.Text = "Fields...";
            this.mnuSUFields.Visible = false;
            // 
            // mnuSUAddFields
            // 
            this.mnuSUAddFields.Index = 0;
            this.mnuSUAddFields.Text = "Add Fields";
            this.mnuSUAddFields.Click += new System.EventHandler(this.menuSUAddFields_Click);
            // 
            // mnuSUUpdateFields
            // 
            this.mnuSUUpdateFields.Index = 1;
            this.mnuSUUpdateFields.Text = "Update Fields";
            this.mnuSUUpdateFields.Click += new System.EventHandler(this.menuSUUpdateFields_Click);
            // 
            // mnuSUDeleteFields
            // 
            this.mnuSUDeleteFields.Index = 2;
            this.mnuSUDeleteFields.Text = "Delete Fields";
            this.mnuSUDeleteFields.Click += new System.EventHandler(this.menuSUDeleteFields_Click);
            // 
            // menuSUProfile
            // 
            this.menuSUProfile.Index = 2;
            this.menuSUProfile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuAddCard,
            this.menuDeleteCard});
            this.menuSUProfile.Text = "&Profile";
            // 
            // menuAddCard
            // 
            this.menuAddCard.Index = 0;
            this.menuAddCard.Text = "&Add Card";
            this.menuAddCard.Click += new System.EventHandler(this.menuAddCard_Click);
            // 
            // menuDeleteCard
            // 
            this.menuDeleteCard.Index = 1;
            this.menuDeleteCard.Text = "&Delete Card";
            this.menuDeleteCard.Click += new System.EventHandler(this.menuDeleteCard_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 3;
            this.menuItem6.Text = "-";
            // 
            // menuSUApplicationMgmt
            // 
            this.menuSUApplicationMgmt.Index = 4;
            this.menuSUApplicationMgmt.Text = "&Application";
            this.menuSUApplicationMgmt.Click += new System.EventHandler(this.menuSUApplicationMgmt_Click);
            // 
            // menuSUSwProfile
            // 
            this.menuSUSwProfile.Index = 5;
            this.menuSUSwProfile.Text = "&Software Profile";
            this.menuSUSwProfile.Click += new System.EventHandler(this.menuSUSwProfile_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 6;
            this.menuItem2.Text = "-";
            // 
            // menuSUInitConn
            // 
            this.menuSUInitConn.Index = 7;
            this.menuSUInitConn.Text = "&Init Connection";
            this.menuSUInitConn.Click += new System.EventHandler(this.menuSUInitConn_Click);
            // 
            // menuSUSetFolder
            // 
            this.menuSUSetFolder.Index = 8;
            this.menuSUSetFolder.Text = "&Folder Path";
            this.menuSUSetFolder.Click += new System.EventHandler(this.menuSUSetFolder_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 9;
            this.menuItem1.Text = "-";
            // 
            // menuSUAudit
            // 
            this.menuSUAudit.Index = 10;
            this.menuSUAudit.Text = "Audit &Trail";
            this.menuSUAudit.Click += new System.EventHandler(this.menuSUAudit_Click);
            // 
            // menuSUTag5
            // 
            this.menuSUTag5.Index = 11;
            this.menuSUTag5.Text = "Tag Fi&ve";
            this.menuSUTag5.Click += new System.EventHandler(this.menuSUTag5_Click);
            // 
            // menuSUQueryReport
            // 
            this.menuSUQueryReport.Index = 12;
            this.menuSUQueryReport.Text = "Query Report";
            this.menuSUQueryReport.Click += new System.EventHandler(this.mnuQueryReport_Click);
            // 
            // menuItem22
            // 
            this.menuItem22.Index = 13;
            this.menuItem22.Text = "-";
            // 
            // menuSUEdcMonitoring
            // 
            this.menuSUEdcMonitoring.Index = 14;
            this.menuSUEdcMonitoring.Text = "Edc Monitoring";
            // 
            // StatusBarApplication
            // 
            this.StatusBarApplication.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.StatusBarApplication.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusBarApplication.Icon")));
            this.StatusBarApplication.Name = "StatusBarApplication";
            this.StatusBarApplication.Width = 1027;
            // 
            // StatusBarTime
            // 
            this.StatusBarTime.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.StatusBarTime.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusBarTime.Icon")));
            this.StatusBarTime.Name = "StatusBarTime";
            this.StatusBarTime.Text = "waktu";
            this.StatusBarTime.Width = 71;
            // 
            // StatusBarDate
            // 
            this.StatusBarDate.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.StatusBarDate.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusBarDate.Icon")));
            this.StatusBarDate.Name = "StatusBarDate";
            this.StatusBarDate.Text = "tanggal";
            this.StatusBarDate.Width = 79;
            // 
            // TimerClock
            // 
            this.TimerClock.Enabled = true;
            this.TimerClock.Tick += new System.EventHandler(this.TimerClock_Tick);
            // 
            // ToolBarMenu
            // 
            this.ToolBarMenu.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.ToolBarMenu.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.ToolBarLogin,
            this.ToolBarLogOff,
            this.ToolBarButton5,
            this.ToolBarExit,
            this.ToolBarButton1,
            this.ToolBarAudit,
            this.ToolBarButton2,
            this.ToolBarSelectVs,
            this.ToolBarButton3,
            this.ToolBarUnited,
            this.ToolBarButton4,
            this.ToolBarDownload});
            this.ToolBarMenu.DropDownArrows = true;
            this.ToolBarMenu.ImageList = this.ImageIcon;
            this.ToolBarMenu.Location = new System.Drawing.Point(0, 0);
            this.ToolBarMenu.Name = "ToolBarMenu";
            this.ToolBarMenu.ShowToolTips = true;
            this.ToolBarMenu.Size = new System.Drawing.Size(1177, 42);
            this.ToolBarMenu.TabIndex = 5;
            this.ToolBarMenu.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.ToolBarMenu_ButtonClick);
            // 
            // ToolBarLogin
            // 
            this.ToolBarLogin.ImageIndex = 0;
            this.ToolBarLogin.Name = "ToolBarLogin";
            this.ToolBarLogin.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarLogin.ToolTipText = "Login";
            // 
            // ToolBarLogOff
            // 
            this.ToolBarLogOff.ImageIndex = 1;
            this.ToolBarLogOff.Name = "ToolBarLogOff";
            this.ToolBarLogOff.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarLogOff.ToolTipText = "Logout";
            // 
            // ToolBarButton5
            // 
            this.ToolBarButton5.Name = "ToolBarButton5";
            this.ToolBarButton5.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarExit
            // 
            this.ToolBarExit.ImageIndex = 13;
            this.ToolBarExit.Name = "ToolBarExit";
            this.ToolBarExit.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarExit.ToolTipText = "Exit";
            // 
            // ToolBarButton1
            // 
            this.ToolBarButton1.Name = "ToolBarButton1";
            this.ToolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarAudit
            // 
            this.ToolBarAudit.ImageIndex = 14;
            this.ToolBarAudit.Name = "ToolBarAudit";
            this.ToolBarAudit.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarAudit.ToolTipText = "Audit Trail";
            // 
            // ToolBarButton2
            // 
            this.ToolBarButton2.Name = "ToolBarButton2";
            this.ToolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarSelectVs
            // 
            this.ToolBarSelectVs.ImageIndex = 10;
            this.ToolBarSelectVs.Name = "ToolBarSelectVs";
            this.ToolBarSelectVs.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarSelectVs.ToolTipText = "Version Selection";
            // 
            // ToolBarButton3
            // 
            this.ToolBarButton3.Name = "ToolBarButton3";
            this.ToolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // ToolBarUnited
            // 
            this.ToolBarUnited.ImageIndex = 12;
            this.ToolBarUnited.Name = "ToolBarUnited";
            this.ToolBarUnited.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarUnited.ToolTipText = "Start United";
            // 
            // ToolBarButton4
            // 
            this.ToolBarButton4.Name = "ToolBarButton4";
            this.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            this.ToolBarButton4.Visible = false;
            // 
            // ToolBarDownload
            // 
            this.ToolBarDownload.ImageIndex = 4;
            this.ToolBarDownload.Name = "ToolBarDownload";
            this.ToolBarDownload.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.ToolBarDownload.ToolTipText = "Download";
            this.ToolBarDownload.Visible = false;
            // 
            // ImageIcon
            // 
            this.ImageIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageIcon.ImageStream")));
            this.ImageIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageIcon.Images.SetKeyName(0, "");
            this.ImageIcon.Images.SetKeyName(1, "");
            this.ImageIcon.Images.SetKeyName(2, "");
            this.ImageIcon.Images.SetKeyName(3, "");
            this.ImageIcon.Images.SetKeyName(4, "");
            this.ImageIcon.Images.SetKeyName(5, "");
            this.ImageIcon.Images.SetKeyName(6, "");
            this.ImageIcon.Images.SetKeyName(7, "");
            this.ImageIcon.Images.SetKeyName(8, "");
            this.ImageIcon.Images.SetKeyName(9, "exit(1).ico");
            this.ImageIcon.Images.SetKeyName(10, "database(1).ico");
            this.ImageIcon.Images.SetKeyName(11, "clipboard_audit.ico");
            this.ImageIcon.Images.SetKeyName(12, "Monitor.ico");
            this.ImageIcon.Images.SetKeyName(13, "stock_quit.ico");
            this.ImageIcon.Images.SetKeyName(14, "Document.ico");
            // 
            // StatusBar1
            // 
            this.StatusBar1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusBar1.Location = new System.Drawing.Point(0, 524);
            this.StatusBar1.Name = "StatusBar1";
            this.StatusBar1.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.StatusBarApplication,
            this.StatusBarTime,
            this.StatusBarDate});
            this.StatusBar1.ShowPanels = true;
            this.StatusBar1.Size = new System.Drawing.Size(1177, 26);
            this.StatusBar1.SizingGrip = false;
            this.StatusBar1.TabIndex = 4;
            this.StatusBar1.Text = "StatusBar1";
            // 
            // FrmMainMenu
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1177, 550);
            this.Controls.Add(this.ToolBarMenu);
            this.Controls.Add(this.StatusBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Menu = this.mnuMain;
            this.Name = "FrmMainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InSys";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMainMenu_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMainMenu_Closed);
            this.Load += new System.EventHandler(this.FrmMainMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarApplication)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusBarDate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.MenuItem mnuApplication;
        internal System.Windows.Forms.MenuItem mnuLogin;
        internal System.Windows.Forms.MenuItem mnuLogOff;
        internal System.Windows.Forms.MenuItem MenuItem15;
        internal System.Windows.Forms.MenuItem mnuPassword;
        internal System.Windows.Forms.MenuItem MenuItemApplicationUser;
        internal System.Windows.Forms.MenuItem mnuDBConn;
        internal System.Windows.Forms.MenuItem mnuAllowInit;
        internal System.Windows.Forms.MenuItem mnuAllSN;
        internal System.Windows.Forms.MenuItem MenuItemApplicationSetting;
        internal System.Windows.Forms.MenuItem mnuUserManagement;
        internal System.Windows.Forms.MenuItem mnuTLV;
        internal System.Windows.Forms.MenuItem mnuExit;
        internal System.Windows.Forms.MenuItem mnuEDCTracking;
        internal System.Windows.Forms.MenuItem mnuMonitor;
        internal System.Windows.Forms.MenuItem mnuStartListen;
        internal System.Windows.Forms.MenuItem MenuItem4;
        internal System.Windows.Forms.MenuItem mnuHelp;
        internal System.Windows.Forms.MenuItem MenuItemHelp;
        internal System.Windows.Forms.MenuItem mnuAbout;
        internal System.Windows.Forms.MenuItem mnuSuperAdmin;
        internal System.Windows.Forms.MenuItem mnuRegister;
        internal System.Windows.Forms.StatusBarPanel StatusBarApplication;
        internal System.Windows.Forms.StatusBarPanel StatusBarTime;
        internal System.Windows.Forms.StatusBarPanel StatusBarDate;
        internal System.Windows.Forms.Timer TimerClock;
        internal System.Windows.Forms.ToolBar ToolBarMenu;
        internal System.Windows.Forms.ToolBarButton ToolBarLogin;
        internal System.Windows.Forms.ToolBarButton ToolBarLogOff;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator1;
        internal System.Windows.Forms.ToolBarButton ToolBarExit;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator2;
        internal System.Windows.Forms.ToolBarButton ToolBarAudit;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator3;
        internal System.Windows.Forms.ToolBarButton ToolBarSelectVs;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator4;
        internal System.Windows.Forms.ToolBarButton ToolBarUnited;
        internal System.Windows.Forms.ToolBarButton ToolBarSeparator5;
        internal System.Windows.Forms.ToolBarButton ToolBarDownload;
        internal System.Windows.Forms.ImageList ImageIcon;
        internal System.Windows.Forms.StatusBar StatusBar1;
        private System.Windows.Forms.MenuItem mnuDataUpload;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem menuSUModify;
        private System.Windows.Forms.MenuItem mnuSUTable;
        private System.Windows.Forms.MenuItem mnuSUAddTable;
        private System.Windows.Forms.MenuItem mnuSUDeleteTable;
        private System.Windows.Forms.MenuItem mnuSUFields;
        private System.Windows.Forms.MenuItem mnuSUAddFields;
        private System.Windows.Forms.MenuItem mnuSUUpdateFields;
        private System.Windows.Forms.MenuItem mnuSUDeleteFields;
        private System.Windows.Forms.MenuItem mnuSUData;
        private System.Windows.Forms.MenuItem mnuSUImportData;
        private System.Windows.Forms.MenuItem mnuSUExportData;
        private System.Windows.Forms.MenuItem mnuSUExportToTxtFile;
        private System.Windows.Forms.MenuItem mnuSUExportToDb;
        private System.Windows.Forms.MenuItem mnuBrowse;
        private System.Windows.Forms.MenuItem mnuSelectVs;
        private System.Windows.Forms.MenuItem MenuItemApplicationUserManagement;
        internal System.Windows.Forms.MenuItem mnuSerialDownload;
        internal System.Windows.Forms.ToolBarButton ToolBarButton5;
        internal System.Windows.Forms.ToolBarButton ToolBarButton1;
        internal System.Windows.Forms.ToolBarButton ToolBarButton2;
        internal System.Windows.Forms.ToolBarButton ToolBarButton3;
        internal System.Windows.Forms.ToolBarButton ToolBarButton4;
        private System.Windows.Forms.MenuItem mnuUser;
        private System.Windows.Forms.MenuItem mnuSettingApp;
        internal System.Windows.Forms.MenuItem mnuAuditTrail;
        internal System.Windows.Forms.MenuItem mnuInitializeTrail;
        internal System.Windows.Forms.MenuItem mnuTemplateDefinition;
        internal System.Windows.Forms.MenuItem mnuUploadDefinition;
        internal System.Windows.Forms.MenuItem mnuUpload;
        internal System.Windows.Forms.MainMenu mnuMain;
        private System.Windows.Forms.MenuItem mnuLocationProfiling;
        private System.Windows.Forms.MenuItem mnuLocationView;
        private System.Windows.Forms.MenuItem mnuAddLocation;
        private System.Windows.Forms.MenuItem menuSUDatabase;
        private System.Windows.Forms.MenuItem menuSUVersion;
        private System.Windows.Forms.MenuItem menuSUCopyCardRange;
        private System.Windows.Forms.MenuItem menuSUTagItem;
        private System.Windows.Forms.MenuItem mnuSUTagItem_AddUpdate;
        private System.Windows.Forms.MenuItem mnuSUTagItem_Delete;
        private System.Windows.Forms.MenuItem menuSUVersion_Add;
        private System.Windows.Forms.MenuItem menuSUVersion_Delete;
        private System.Windows.Forms.MenuItem mnuCompressInit;
        private System.Windows.Forms.MenuItem mnuAutoInit;
        private System.Windows.Forms.MenuItem menuSUSetFolder;
        private System.Windows.Forms.MenuItem menuSUApplicationMgmt;
        private System.Windows.Forms.MenuItem mnuEdcTerminal;
        private System.Windows.Forms.MenuItem menuItemApplicationBrowse;
        private System.Windows.Forms.MenuItem mnuReport;
        private System.Windows.Forms.MenuItem mnuSettingEdc;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem menuItemApplicationDataUpload;
        private System.Windows.Forms.MenuItem menuSUInitConn;
        private System.Windows.Forms.MenuItem menuSUSwProfile;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuSUAudit;
        internal System.Windows.Forms.MenuItem mnuUserGroup;
        internal System.Windows.Forms.MenuItem mnuItemUserRight;
        private System.Windows.Forms.MenuItem menuItem5;
        private System.Windows.Forms.MenuItem mnuSUTagItemExport;
        private System.Windows.Forms.MenuItem mnuSUTagItemImport;
        private System.Windows.Forms.MenuItem menuSUCopyGPRS;
        private System.Windows.Forms.MenuItem menuSUCopyTLE;
        private System.Windows.Forms.MenuItem menuSUTag5;
        private System.Windows.Forms.MenuItem menuSUVersion_Edit;
        private System.Windows.Forms.MenuItem mnuSoftwareInitTrail;
        private System.Windows.Forms.MenuItem menuItem7;
        private System.Windows.Forms.MenuItem menuSUQueryReport;
        private System.Windows.Forms.MenuItem mnuSUMoveData;
        private System.Windows.Forms.MenuItem mnuQueryReport;
        private System.Windows.Forms.MenuItem mnSoftwareProgress;
        private System.Windows.Forms.MenuItem mnEchoPing;
        private System.Windows.Forms.MenuItem menuSUCard;
        private System.Windows.Forms.MenuItem menuSUImportCard;
        private System.Windows.Forms.MenuItem menuSUExportCard;
        private System.Windows.Forms.MenuItem mnuSUCopyData;
        private System.Windows.Forms.MenuItem mnuUpdateRD;
        private System.Windows.Forms.MenuItem mnuAppPackage;
        private System.Windows.Forms.MenuItem mnuRemoteDownload;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.MenuItem menuSUCopyAID;
        private System.Windows.Forms.MenuItem menuSUCopyCAPK;
        private System.Windows.Forms.MenuItem menuSUCopyRD;
        private System.Windows.Forms.MenuItem mnLogoEDC;
        private System.Windows.Forms.MenuItem mnLogoEDCPrint;
        private System.Windows.Forms.MenuItem mnLogoEDCIdle;
        private System.Windows.Forms.MenuItem menuItem21;
        private System.Windows.Forms.MenuItem menuSUCopyPromo;
        private System.Windows.Forms.MenuItem menuSUProfile;
        private System.Windows.Forms.MenuItem menuAddCard;
        private System.Windows.Forms.MenuItem menuDeleteCard;
        private System.Windows.Forms.MenuItem mnLogoEDCMerchant;
        private System.Windows.Forms.MenuItem mnLogoEDCReceipt;
        private System.Windows.Forms.MenuItem mnuSUTagItem_AddUpdateNew;
        private System.Windows.Forms.MenuItem mnuViewAuditTrailUser;
        private System.Windows.Forms.MenuItem menuItem22;
        private System.Windows.Forms.MenuItem menuSUEdcMonitoring;
        private System.Windows.Forms.MenuItem mnuAppPackageCpl;
        private System.Windows.Forms.MenuItem mnuSettingSoftwarePackage;
        private System.Windows.Forms.MenuItem mnuSettingTerminalIDPackage;
        private System.Windows.Forms.MenuItem mnSoftwareProgressCPL;
        private System.Windows.Forms.MenuItem menuItem10;
        private System.Windows.Forms.MenuItem mnUpdateConnection;
        private System.Windows.Forms.MenuItem mnReplaceTID;
        private System.Windows.Forms.MenuItem mnOpenTogglingAllowInit;
        private System.Windows.Forms.MenuItem mnQcEms;
        private System.Windows.Forms.MenuItem mnMoveTid;
        private System.Windows.Forms.MenuItem mnUploadTidRD;
        private System.Windows.Forms.MenuItem mnUploadEms;
        private System.Windows.Forms.MenuItem mnUpdateParameter;
        private System.Windows.Forms.MenuItem mnGenerateUploadPTD;
        private System.Windows.Forms.MenuItem mnuTools;
        private System.Windows.Forms.MenuItem mnuQueryLoader;
        private System.Windows.Forms.MenuItem mnuRD_RmtDownloadFtps;
        private System.Windows.Forms.MenuItem mnuRD_UploadSoftwareFtps;
        private System.Windows.Forms.MenuItem mnuRD_TerminalGroupFtps;
        private System.Windows.Forms.MenuItem mnuRmtDownloadISO;
        private System.Windows.Forms.MenuItem mnuRD_AuditDownload;
    }
}