using System;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmFilterInitTrail : Form
    {
        public FrmFilterInitTrail()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Runs when from is loading, set time.
        /// </summary>
        private void frmFilterInitTrail_Load(object sender, EventArgs e)
        {
            dateTimePickerFrom.Value = DateTime.Now;
            dateTimePickerTo.Value = DateTime.Now;
        }
    }
}