﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace InSys
{
    public partial class FrmReport : Form
    {
        private SqlConnection oSqlConn;
        private SqlConnection oSqlConnAuditTrail;
        string sQuery;
        int cols;
        
        /// <summary>
        /// Form to make report form Query
        /// </summary>
        /// <param name="_oSqlConn">sql Connection</param>
        /// <param name="_oSqlConnAuditTrail">sql connection</param>
        public FrmReport(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            ofdReport.Filter = "SQL files (*.sql)|*.txt|All files (*.*)|*.*";
            ofdReport.InitialDirectory = @"C:\";
            ofdReport.Title = "Choose Report text.";
            if (ofdReport.ShowDialog() == DialogResult.OK)
            {
                
                StreamReader sr = new StreamReader(ofdReport.FileName);
                sQuery = sr.ReadToEnd();
                sr.Close();

                if (sQuery.Substring(0, 8) == "--Report")
                {
                    SqlDataAdapter adap = new SqlDataAdapter(sQuery, oSqlConn);
                    DataTable DT = new DataTable();
                    adap.Fill(DT);
                    dgvReport.DataSource = DT;
                    
                }
                else
                {
                    MessageBox.Show("Choose correct data report");
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = "Report";
            saveFileDialog1.Filter = "Comma Separated Values(*.csv)|*.csv";
           

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //open file 
                StreamWriter wr = new StreamWriter(saveFileDialog1.FileName);

                //determine the number of columns and write columns to file 
                cols = dgvReport.Columns.Count;
                for (int i = 0; i <= dgvReport.Columns.Count-1; i++)
                {
                    wr.Write(dgvReport.Columns[i].Name.ToString().ToUpper() + ";");
                }
                wr.WriteLine();

                //write rows to excel file
                for (int i = 0; i < (dgvReport.Rows.Count - 1); i++)
                {
                    for (int j = 0; j < cols; j++)
                    {
                        if (dgvReport.Rows[i].Cells[j].Value != null)
                        {
                            wr.Write(dgvReport.Rows[i].Cells[j].Value + ";");
                        }
                        else
                        {
                            wr.Write(";");
                        }
                    }

                    wr.WriteLine();
                }

                //close file
                wr.Close();

                //CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, "", "Execute SQL FIle" + ofdReport.FileName + " from Form Report", sQuery);
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", "Execute SQL FIle" + ofdReport.FileName + " from Form Report", sQuery);
                }
            }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
