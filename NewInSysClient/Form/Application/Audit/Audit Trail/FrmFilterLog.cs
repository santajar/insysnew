using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmFilterLog : Form
    {
        
        protected SqlConnection oSqlConn;
        public FrmFilterLog(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        /// <summary>
        /// Event function, runs when the form is loading.
        /// Call InitData function.
        /// </summary>
        private void FrmFilterLog_Load(object sender, EventArgs e)
        {
            InitData();
        }

        /// <summary>
        /// Event function, runs when BtnOK is clicked.
        /// Set dialog result = OK
        /// </summary>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// Create filter log string from user input.
        /// </summary>
        /// <returns>string : Condition string for filter log</returns>
        public string sGetConditionString()
        {
            return string.Format(@"WHERE datediff ( day, '{0}', accesstime) >= 0 AND 
                datediff(day,accesstime, '{1}') >= 0 AND UserId LIKE '{2}' AND 
                DatabaseName LIKE '{3}' AND ActionDescription LIKE '%{4}%'", sGetFromDate(), sGetEndDate(),
                  sGetUserID(), sGetDbName(), sGetDescription());
        }

        /// <summary>
        /// Get User ID
        /// </summary>
        /// <returns>string : User ID</returns>
        protected string sGetUserID()
        {
            if (cmbUser.SelectedIndex >= 0)
                return cmbUser.SelectedValue.ToString();
            else return "%";
        }

        /// <summary>
        /// Get database name 
        /// </summary>
        /// <returns>string : database name</returns>
        protected string sGetDbName()
        {
            if (cmbDatabase.SelectedIndex >= 0)
                return cmbDatabase.SelectedValue.ToString();
            else return "%";
        }

        /// <summary>
        /// Get start date.
        /// Format Date : MM/dd/yyyy.
        /// </summary>
        /// <returns>string : start date.</returns>
        protected string sGetFromDate()
        {
            return dtpDateFrom.Value.ToString("MM/dd/yyyy");                 
        }

        /// <summary>
        /// Get End Date
        /// Format Date : MM/dd/yyyy.
        /// </summary>
        /// <returns>string : End Date</returns>
        protected string sGetEndDate()
        {
            return dtpDateTo.Value.ToString("MM/dd/yyyy");
        }

        /// <summary>
        /// Get Action Description
        /// </summary>
        /// <returns>string : Description</returns>
        protected string sGetDescription()
        {
            return txtActionDesc.Text.Trim().Replace("'", "''");
        }

        /// <summary>
        /// Event funcion, runs when btnCancel is clicked.
        /// Set Dialog Result = Cancel and close the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
            this.Dispose();
            //return value to Main Menu
            //End return value to Main Menu
        }

        /// <summary>
        /// Initiate ComboBoxes in the form
        /// </summary>
        protected void InitCombo()
        {
            CommonClass.FillComboBox (oSqlConn, CommonSP.sSPUserBrowse, "", "UserID", "UserID", ref cmbUser); //LoadUser
            CommonClass.FillComboBox (oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseName", "DatabaseName", ref cmbDatabase); //LoadUser
        }

        /// <summary>
        /// Initiate Data in Form
        /// </summary>
        protected void InitData()
        {
            InitCombo();
        }
    }
}