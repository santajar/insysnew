namespace InSys
{
    partial class FrmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TabAuditInitDetail = new System.Windows.Forms.TabPage();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnXportDetail = new System.Windows.Forms.Button();
            this.btnCloseDetail = new System.Windows.Forms.Button();
            this.btnFreshLIDetail = new System.Windows.Forms.Button();
            this.txtTIDdetail = new System.Windows.Forms.TextBox();
            this.btnSearchDetail = new System.Windows.Forms.Button();
            this.dgInitDetail = new System.Windows.Forms.DataGrid();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.TabAuditInit = new System.Windows.Forms.TabControl();
            this.TabColExcel = new System.Windows.Forms.TabPage();
            this.btnClose = new System.Windows.Forms.Button();
            this.gbXLS = new System.Windows.Forms.GroupBox();
            this.btnExecute = new System.Windows.Forms.Button();
            this.rdUndownloadedData = new System.Windows.Forms.RadioButton();
            this.rdDownloadedData = new System.Windows.Forms.RadioButton();
            this.rdAllData = new System.Windows.Forms.RadioButton();
            this.cmbDb = new System.Windows.Forms.ComboBox();
            this.TabAuditInitSum = new System.Windows.Forms.TabPage();
            this.gbButtonSum = new System.Windows.Forms.GroupBox();
            this.btnXportSum = new System.Windows.Forms.Button();
            this.btnCloseSum = new System.Windows.Forms.Button();
            this.btnFreshLISum = new System.Windows.Forms.Button();
            this.txtTIDSum = new System.Windows.Forms.TextBox();
            this.btnSearchSum = new System.Windows.Forms.Button();
            this.dgInitSum = new System.Windows.Forms.DataGrid();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.sfdExport = new System.Windows.Forms.SaveFileDialog();
            this.TabAuditInitDetail.SuspendLayout();
            this.gbButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitDetail)).BeginInit();
            this.TabAuditInit.SuspendLayout();
            this.TabColExcel.SuspendLayout();
            this.gbXLS.SuspendLayout();
            this.TabAuditInitSum.SuspendLayout();
            this.gbButtonSum.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitSum)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // TabAuditInitDetail
            // 
            this.TabAuditInitDetail.Controls.Add(this.gbButton);
            this.TabAuditInitDetail.Controls.Add(this.dgInitDetail);
            this.TabAuditInitDetail.Location = new System.Drawing.Point(4, 24);
            this.TabAuditInitDetail.Name = "TabAuditInitDetail";
            this.TabAuditInitDetail.Size = new System.Drawing.Size(610, 466);
            this.TabAuditInitDetail.TabIndex = 1;
            this.TabAuditInitDetail.Text = "Initialisation Log Detail";
            this.TabAuditInitDetail.UseVisualStyleBackColor = true;
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbButton.Controls.Add(this.btnXportDetail);
            this.gbButton.Controls.Add(this.btnCloseDetail);
            this.gbButton.Controls.Add(this.btnFreshLIDetail);
            this.gbButton.Controls.Add(this.txtTIDdetail);
            this.gbButton.Controls.Add(this.btnSearchDetail);
            this.gbButton.Location = new System.Drawing.Point(16, 419);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(576, 44);
            this.gbButton.TabIndex = 96;
            this.gbButton.TabStop = false;
            // 
            // btnXportDetail
            // 
            this.btnXportDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXportDetail.Location = new System.Drawing.Point(281, 14);
            this.btnXportDetail.Name = "btnXportDetail";
            this.btnXportDetail.Size = new System.Drawing.Size(120, 23);
            this.btnXportDetail.TabIndex = 2;
            this.btnXportDetail.Text = "Export";
            this.ToolTip1.SetToolTip(this.btnXportDetail, "Export to Excel(*.xls) Files");
            this.btnXportDetail.Click += new System.EventHandler(this.btnXportDetail_Click);
            // 
            // btnCloseDetail
            // 
            this.btnCloseDetail.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCloseDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseDetail.Location = new System.Drawing.Point(450, 13);
            this.btnCloseDetail.Name = "btnCloseDetail";
            this.btnCloseDetail.Size = new System.Drawing.Size(120, 24);
            this.btnCloseDetail.TabIndex = 94;
            this.btnCloseDetail.Text = "&Close";
            this.btnCloseDetail.Click += new System.EventHandler(this.btnCloseDetail_Click);
            // 
            // btnFreshLIDetail
            // 
            this.btnFreshLIDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFreshLIDetail.Location = new System.Drawing.Point(155, 14);
            this.btnFreshLIDetail.Name = "btnFreshLIDetail";
            this.btnFreshLIDetail.Size = new System.Drawing.Size(120, 23);
            this.btnFreshLIDetail.TabIndex = 1;
            this.btnFreshLIDetail.Text = "&Refresh";
            // 
            // txtTIDdetail
            // 
            this.txtTIDdetail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTIDdetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTIDdetail.Location = new System.Drawing.Point(6, 14);
            this.txtTIDdetail.MaxLength = 8;
            this.txtTIDdetail.Name = "txtTIDdetail";
            this.txtTIDdetail.Size = new System.Drawing.Size(72, 21);
            this.txtTIDdetail.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtTIDdetail, "Enter the TID and Press Go button to search");
            // 
            // btnSearchDetail
            // 
            this.btnSearchDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchDetail.Location = new System.Drawing.Point(84, 14);
            this.btnSearchDetail.Name = "btnSearchDetail";
            this.btnSearchDetail.Size = new System.Drawing.Size(40, 23);
            this.btnSearchDetail.TabIndex = 1;
            this.btnSearchDetail.Text = "&Go";
            // 
            // dgInitDetail
            // 
            this.dgInitDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgInitDetail.DataMember = "";
            this.dgInitDetail.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgInitDetail.Location = new System.Drawing.Point(16, 16);
            this.dgInitDetail.Name = "dgInitDetail";
            this.dgInitDetail.PreferredColumnWidth = 100;
            this.dgInitDetail.PreferredRowHeight = 15;
            this.dgInitDetail.ReadOnly = true;
            this.dgInitDetail.RowHeaderWidth = 15;
            this.dgInitDetail.Size = new System.Drawing.Size(576, 397);
            this.dgInitDetail.TabIndex = 0;
            // 
            // lblDatabase
            // 
            this.lblDatabase.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatabase.Location = new System.Drawing.Point(16, 24);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(100, 23);
            this.lblDatabase.TabIndex = 95;
            this.lblDatabase.Text = "Database";
            this.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Progress
            // 
            this.Progress.Location = new System.Drawing.Point(6, 112);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(509, 16);
            this.Progress.Step = 1;
            this.Progress.TabIndex = 94;
            // 
            // TabAuditInit
            // 
            this.TabAuditInit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.TabAuditInit.Controls.Add(this.TabColExcel);
            this.TabAuditInit.Controls.Add(this.TabAuditInitDetail);
            this.TabAuditInit.Controls.Add(this.TabAuditInitSum);
            this.TabAuditInit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabAuditInit.Location = new System.Drawing.Point(12, 12);
            this.TabAuditInit.Name = "TabAuditInit";
            this.TabAuditInit.SelectedIndex = 0;
            this.TabAuditInit.Size = new System.Drawing.Size(618, 494);
            this.TabAuditInit.TabIndex = 2;
            // 
            // TabColExcel
            // 
            this.TabColExcel.Controls.Add(this.btnClose);
            this.TabColExcel.Controls.Add(this.gbXLS);
            this.TabColExcel.Location = new System.Drawing.Point(4, 24);
            this.TabColExcel.Name = "TabColExcel";
            this.TabColExcel.Size = new System.Drawing.Size(610, 466);
            this.TabColExcel.TabIndex = 0;
            this.TabColExcel.Text = "Excel";
            this.TabColExcel.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(425, 198);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(112, 24);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gbXLS
            // 
            this.gbXLS.Controls.Add(this.btnExecute);
            this.gbXLS.Controls.Add(this.lblDatabase);
            this.gbXLS.Controls.Add(this.Progress);
            this.gbXLS.Controls.Add(this.rdUndownloadedData);
            this.gbXLS.Controls.Add(this.rdDownloadedData);
            this.gbXLS.Controls.Add(this.rdAllData);
            this.gbXLS.Controls.Add(this.cmbDb);
            this.gbXLS.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbXLS.Location = new System.Drawing.Point(16, 16);
            this.gbXLS.Name = "gbXLS";
            this.gbXLS.Size = new System.Drawing.Size(521, 165);
            this.gbXLS.TabIndex = 0;
            this.gbXLS.TabStop = false;
            this.gbXLS.Text = "Export to Excel Files";
            // 
            // btnExecute
            // 
            this.btnExecute.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExecute.Location = new System.Drawing.Point(204, 134);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(112, 24);
            this.btnExecute.TabIndex = 5;
            this.btnExecute.Text = "E&xecute";
            // 
            // rdUndownloadedData
            // 
            this.rdUndownloadedData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdUndownloadedData.Location = new System.Drawing.Point(264, 64);
            this.rdUndownloadedData.Name = "rdUndownloadedData";
            this.rdUndownloadedData.Size = new System.Drawing.Size(148, 24);
            this.rdUndownloadedData.TabIndex = 4;
            this.rdUndownloadedData.Text = "Undownloaded Data";
            // 
            // rdDownloadedData
            // 
            this.rdDownloadedData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdDownloadedData.Location = new System.Drawing.Point(104, 64);
            this.rdDownloadedData.Name = "rdDownloadedData";
            this.rdDownloadedData.Size = new System.Drawing.Size(152, 24);
            this.rdDownloadedData.TabIndex = 3;
            this.rdDownloadedData.Text = "Downloaded Data";
            // 
            // rdAllData
            // 
            this.rdAllData.Checked = true;
            this.rdAllData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdAllData.Location = new System.Drawing.Point(16, 64);
            this.rdAllData.Name = "rdAllData";
            this.rdAllData.Size = new System.Drawing.Size(80, 24);
            this.rdAllData.TabIndex = 2;
            this.rdAllData.TabStop = true;
            this.rdAllData.Text = "All Data";
            // 
            // cmbDb
            // 
            this.cmbDb.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDb.Location = new System.Drawing.Point(184, 24);
            this.cmbDb.Name = "cmbDb";
            this.cmbDb.Size = new System.Drawing.Size(160, 23);
            this.cmbDb.TabIndex = 1;
            // 
            // TabAuditInitSum
            // 
            this.TabAuditInitSum.Controls.Add(this.gbButtonSum);
            this.TabAuditInitSum.Controls.Add(this.dgInitSum);
            this.TabAuditInitSum.Location = new System.Drawing.Point(4, 24);
            this.TabAuditInitSum.Name = "TabAuditInitSum";
            this.TabAuditInitSum.Size = new System.Drawing.Size(610, 466);
            this.TabAuditInitSum.TabIndex = 2;
            this.TabAuditInitSum.Text = "Initialisation Log Summary";
            this.TabAuditInitSum.UseVisualStyleBackColor = true;
            // 
            // gbButtonSum
            // 
            this.gbButtonSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbButtonSum.Controls.Add(this.btnXportSum);
            this.gbButtonSum.Controls.Add(this.btnCloseSum);
            this.gbButtonSum.Controls.Add(this.btnFreshLISum);
            this.gbButtonSum.Controls.Add(this.txtTIDSum);
            this.gbButtonSum.Controls.Add(this.btnSearchSum);
            this.gbButtonSum.Location = new System.Drawing.Point(16, 419);
            this.gbButtonSum.Name = "gbButtonSum";
            this.gbButtonSum.Size = new System.Drawing.Size(576, 44);
            this.gbButtonSum.TabIndex = 96;
            this.gbButtonSum.TabStop = false;
            // 
            // btnXportSum
            // 
            this.btnXportSum.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnXportSum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXportSum.Location = new System.Drawing.Point(281, 14);
            this.btnXportSum.Name = "btnXportSum";
            this.btnXportSum.Size = new System.Drawing.Size(120, 23);
            this.btnXportSum.TabIndex = 2;
            this.btnXportSum.Text = "Export";
            this.ToolTip1.SetToolTip(this.btnXportSum, "Export to Excel(*.xls) Files");
            this.btnXportSum.Click += new System.EventHandler(this.btnXportSum_Click);
            // 
            // btnCloseSum
            // 
            this.btnCloseSum.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCloseSum.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCloseSum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseSum.Location = new System.Drawing.Point(450, 13);
            this.btnCloseSum.Name = "btnCloseSum";
            this.btnCloseSum.Size = new System.Drawing.Size(120, 24);
            this.btnCloseSum.TabIndex = 94;
            this.btnCloseSum.Text = "&Close";
            this.btnCloseSum.Click += new System.EventHandler(this.btnCloseSum_Click);
            // 
            // btnFreshLISum
            // 
            this.btnFreshLISum.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnFreshLISum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFreshLISum.Location = new System.Drawing.Point(155, 14);
            this.btnFreshLISum.Name = "btnFreshLISum";
            this.btnFreshLISum.Size = new System.Drawing.Size(120, 23);
            this.btnFreshLISum.TabIndex = 1;
            this.btnFreshLISum.Text = "&Refresh";
            // 
            // txtTIDSum
            // 
            this.txtTIDSum.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTIDSum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTIDSum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTIDSum.Location = new System.Drawing.Point(6, 14);
            this.txtTIDSum.MaxLength = 8;
            this.txtTIDSum.Name = "txtTIDSum";
            this.txtTIDSum.Size = new System.Drawing.Size(72, 21);
            this.txtTIDSum.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtTIDSum, "Enter the TID and Press Go button to search");
            // 
            // btnSearchSum
            // 
            this.btnSearchSum.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnSearchSum.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchSum.Location = new System.Drawing.Point(84, 14);
            this.btnSearchSum.Name = "btnSearchSum";
            this.btnSearchSum.Size = new System.Drawing.Size(40, 23);
            this.btnSearchSum.TabIndex = 1;
            this.btnSearchSum.Text = "&Go";
            // 
            // dgInitSum
            // 
            this.dgInitSum.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgInitSum.DataMember = "";
            this.dgInitSum.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgInitSum.Location = new System.Drawing.Point(16, 16);
            this.dgInitSum.Name = "dgInitSum";
            this.dgInitSum.PreferredColumnWidth = 100;
            this.dgInitSum.PreferredRowHeight = 15;
            this.dgInitSum.ReadOnly = true;
            this.dgInitSum.RowHeaderWidth = 15;
            this.dgInitSum.Size = new System.Drawing.Size(576, 397);
            this.dgInitSum.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(324, 14);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Export";
            this.ToolTip1.SetToolTip(this.button3, "Export to Excel(*.xls) Files");
            // 
            // textBox1
            // 
            this.textBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(6, 14);
            this.textBox1.MaxLength = 8;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(72, 21);
            this.textBox1.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.textBox1, "Enter the TID and Press Go button to search");
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(610, 466);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Excel";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(425, 198);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 24);
            this.button1.TabIndex = 93;
            this.button1.Text = "&Close";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton5);
            this.groupBox1.Controls.Add(this.radioButton6);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(521, 165);
            this.groupBox1.TabIndex = 88;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Export to Excel Files";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(204, 134);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 24);
            this.button2.TabIndex = 89;
            this.button2.Text = "E&xecute";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 95;
            this.label2.Text = "Database";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(16, 112);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(496, 16);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 94;
            // 
            // radioButton4
            // 
            this.radioButton4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton4.Location = new System.Drawing.Point(264, 64);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(148, 24);
            this.radioButton4.TabIndex = 92;
            this.radioButton4.Text = "Undownloaded Data";
            // 
            // radioButton5
            // 
            this.radioButton5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton5.Location = new System.Drawing.Point(104, 64);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(152, 24);
            this.radioButton5.TabIndex = 91;
            this.radioButton5.Text = "Downloaded Data";
            // 
            // radioButton6
            // 
            this.radioButton6.Checked = true;
            this.radioButton6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton6.Location = new System.Drawing.Point(16, 64);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(80, 24);
            this.radioButton6.TabIndex = 90;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "All Data";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.Location = new System.Drawing.Point(184, 24);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(160, 23);
            this.comboBox1.TabIndex = 88;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.button7);
            this.tabPage2.Controls.Add(this.dataGrid1);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(610, 466);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Audit Init Detail";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Location = new System.Drawing.Point(16, 382);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(576, 44);
            this.groupBox3.TabIndex = 96;
            this.groupBox3.TabStop = false;
            // 
            // button4
            // 
            this.button4.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(450, 14);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(120, 24);
            this.button4.TabIndex = 95;
            this.button4.Text = "&Summary";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(155, 14);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(120, 23);
            this.button5.TabIndex = 1;
            this.button5.Text = "&Refresh";
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(84, 15);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(40, 23);
            this.button6.TabIndex = 1;
            this.button6.Text = "&Go";
            // 
            // button7
            // 
            this.button7.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(238, 432);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(120, 24);
            this.button7.TabIndex = 94;
            this.button7.Text = "&Close";
            // 
            // dataGrid1
            // 
            this.dataGrid1.DataMember = "";
            this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGrid1.Location = new System.Drawing.Point(16, 16);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.PreferredColumnWidth = 100;
            this.dataGrid1.PreferredRowHeight = 15;
            this.dataGrid1.ReadOnly = true;
            this.dataGrid1.RowHeaderWidth = 15;
            this.dataGrid1.Size = new System.Drawing.Size(576, 360);
            this.dataGrid1.TabIndex = 0;
            // 
            // FrmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 511);
            this.Controls.Add(this.TabAuditInit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmAdmin";
            this.Text = "Audit Init";
            this.Load += new System.EventHandler(this.FrmAdmin_Load);
            this.TabAuditInitDetail.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.gbButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitDetail)).EndInit();
            this.TabAuditInit.ResumeLayout(false);
            this.TabColExcel.ResumeLayout(false);
            this.gbXLS.ResumeLayout(false);
            this.TabAuditInitSum.ResumeLayout(false);
            this.gbButtonSum.ResumeLayout(false);
            this.gbButtonSum.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgInitSum)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TabPage TabAuditInitDetail;
        internal System.Windows.Forms.Button btnCloseDetail;
        internal System.Windows.Forms.Button btnXportDetail;
        internal System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.Button btnFreshLIDetail;
        internal System.Windows.Forms.DataGrid dgInitDetail;
        internal System.Windows.Forms.TextBox txtTIDdetail;
        internal System.Windows.Forms.Button btnSearchDetail;
        internal System.Windows.Forms.Label lblDatabase;
        internal System.Windows.Forms.ProgressBar Progress;
        internal System.Windows.Forms.TabControl TabAuditInit;
        internal System.Windows.Forms.TabPage TabColExcel;
        internal System.Windows.Forms.GroupBox gbXLS;
        internal System.Windows.Forms.RadioButton rdUndownloadedData;
        internal System.Windows.Forms.RadioButton rdDownloadedData;
        internal System.Windows.Forms.RadioButton rdAllData;
        internal System.Windows.Forms.Button btnExecute;
        internal System.Windows.Forms.ComboBox cmbDb;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.TabPage TabAuditInitSum;
        private System.Windows.Forms.GroupBox gbButtonSum;
        internal System.Windows.Forms.Button btnXportSum;
        internal System.Windows.Forms.Button btnFreshLISum;
        internal System.Windows.Forms.TextBox txtTIDSum;
        internal System.Windows.Forms.Button btnSearchSum;
        internal System.Windows.Forms.Button btnCloseSum;
        internal System.Windows.Forms.DataGrid dgInitSum;
        internal System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.Button button1;
        internal System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Button button2;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.ProgressBar progressBar1;
        internal System.Windows.Forms.RadioButton radioButton4;
        internal System.Windows.Forms.RadioButton radioButton5;
        internal System.Windows.Forms.RadioButton radioButton6;
        internal System.Windows.Forms.ComboBox comboBox1;
        internal System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Button button3;
        internal System.Windows.Forms.Button button4;
        internal System.Windows.Forms.Button button5;
        internal System.Windows.Forms.TextBox textBox1;
        internal System.Windows.Forms.Button button6;
        internal System.Windows.Forms.Button button7;
        internal System.Windows.Forms.DataGrid dataGrid1;
        private System.Windows.Forms.SaveFileDialog sfdExport;
    }
}