﻿namespace InSys
{
    partial class FrmUserGroupMngmt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbUserID = new System.Windows.Forms.GroupBox();
            this.btnUnReg = new System.Windows.Forms.Button();
            this.btnReg = new System.Windows.Forms.Button();
            this.lblRegisteredUID = new System.Windows.Forms.Label();
            this.lbRegisteredUser = new System.Windows.Forms.ListBox();
            this.lblAvailable = new System.Windows.Forms.Label();
            this.lbAvailableUser = new System.Windows.Forms.ListBox();
            this.lblGroupID = new System.Windows.Forms.Label();
            this.cmbUserGroup = new System.Windows.Forms.ComboBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbUserID.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbUserID
            // 
            this.gbUserID.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbUserID.Controls.Add(this.btnUnReg);
            this.gbUserID.Controls.Add(this.btnReg);
            this.gbUserID.Controls.Add(this.lblRegisteredUID);
            this.gbUserID.Controls.Add(this.lbRegisteredUser);
            this.gbUserID.Controls.Add(this.lblAvailable);
            this.gbUserID.Controls.Add(this.lbAvailableUser);
            this.gbUserID.Controls.Add(this.lblGroupID);
            this.gbUserID.Controls.Add(this.cmbUserGroup);
            this.gbUserID.Location = new System.Drawing.Point(6, 1);
            this.gbUserID.Name = "gbUserID";
            this.gbUserID.Size = new System.Drawing.Size(464, 315);
            this.gbUserID.TabIndex = 0;
            this.gbUserID.TabStop = false;
            // 
            // btnUnReg
            // 
            this.btnUnReg.Location = new System.Drawing.Point(211, 177);
            this.btnUnReg.Name = "btnUnReg";
            this.btnUnReg.Size = new System.Drawing.Size(35, 30);
            this.btnUnReg.TabIndex = 7;
            this.btnUnReg.Text = "<";
            this.btnUnReg.UseVisualStyleBackColor = true;
            this.btnUnReg.Click += new System.EventHandler(this.btnUnReg_Click);
            // 
            // btnReg
            // 
            this.btnReg.Location = new System.Drawing.Point(211, 141);
            this.btnReg.Name = "btnReg";
            this.btnReg.Size = new System.Drawing.Size(35, 30);
            this.btnReg.TabIndex = 6;
            this.btnReg.Text = ">";
            this.btnReg.UseVisualStyleBackColor = true;
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            // 
            // lblRegisteredUID
            // 
            this.lblRegisteredUID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblRegisteredUID.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblRegisteredUID.Location = new System.Drawing.Point(261, 55);
            this.lblRegisteredUID.Name = "lblRegisteredUID";
            this.lblRegisteredUID.Size = new System.Drawing.Size(180, 22);
            this.lblRegisteredUID.TabIndex = 5;
            this.lblRegisteredUID.Text = "Registered User ID";
            this.lblRegisteredUID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbRegisteredUser
            // 
            this.lbRegisteredUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbRegisteredUser.FormattingEnabled = true;
            this.lbRegisteredUser.Location = new System.Drawing.Point(261, 78);
            this.lbRegisteredUser.Name = "lbRegisteredUser";
            this.lbRegisteredUser.ScrollAlwaysVisible = true;
            this.lbRegisteredUser.Size = new System.Drawing.Size(180, 212);
            this.lbRegisteredUser.TabIndex = 4;
            // 
            // lblAvailable
            // 
            this.lblAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblAvailable.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblAvailable.Location = new System.Drawing.Point(19, 55);
            this.lblAvailable.Name = "lblAvailable";
            this.lblAvailable.Size = new System.Drawing.Size(180, 22);
            this.lblAvailable.TabIndex = 3;
            this.lblAvailable.Text = "Available User ID";
            this.lblAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAvailableUser
            // 
            this.lbAvailableUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbAvailableUser.FormattingEnabled = true;
            this.lbAvailableUser.Location = new System.Drawing.Point(19, 78);
            this.lbAvailableUser.Name = "lbAvailableUser";
            this.lbAvailableUser.ScrollAlwaysVisible = true;
            this.lbAvailableUser.Size = new System.Drawing.Size(180, 212);
            this.lbAvailableUser.TabIndex = 2;
            // 
            // lblGroupID
            // 
            this.lblGroupID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblGroupID.AutoSize = true;
            this.lblGroupID.Location = new System.Drawing.Point(97, 22);
            this.lblGroupID.Name = "lblGroupID";
            this.lblGroupID.Size = new System.Drawing.Size(50, 13);
            this.lblGroupID.TabIndex = 1;
            this.lblGroupID.Text = "Group ID";
            // 
            // cmbUserGroup
            // 
            this.cmbUserGroup.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbUserGroup.FormattingEnabled = true;
            this.cmbUserGroup.Location = new System.Drawing.Point(153, 19);
            this.cmbUserGroup.Name = "cmbUserGroup";
            this.cmbUserGroup.Size = new System.Drawing.Size(210, 21);
            this.cmbUserGroup.TabIndex = 0;
            this.cmbUserGroup.SelectedIndexChanged += new System.EventHandler(this.cmbUserGroup_SelectedIndexChanged);
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(6, 317);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(464, 47);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(370, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(85, 30);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(279, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 30);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmUserGroupMngmt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(475, 373);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbUserID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(485, 405);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(485, 405);
            this.Name = "FrmUserGroupMngmt";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Group Management";
            this.Load += new System.EventHandler(this.FrmUserGroupMngmt_Load);
            this.gbUserID.ResumeLayout(false);
            this.gbUserID.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbUserID;
        private System.Windows.Forms.Label lblGroupID;
        private System.Windows.Forms.ComboBox cmbUserGroup;
        private System.Windows.Forms.ListBox lbAvailableUser;
        private System.Windows.Forms.Label lblAvailable;
        private System.Windows.Forms.Label lblRegisteredUID;
        private System.Windows.Forms.ListBox lbRegisteredUser;
        private System.Windows.Forms.Button btnReg;
        private System.Windows.Forms.Button btnUnReg;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
    }
}