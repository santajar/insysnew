using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmUserMngmt : Form
    {
        #region "Variables"

        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected DataTable oDataTable;

        protected string sSPLoginChangePassword = "spUserLoginChangePassword";

        protected string sSPSuperLogin = "spUserSuperLoginBrowse";
        protected string sSPSuperLoginChangePassword = "spUserSuperLoginChangePassword";
        protected string sSPSuperLoginEditUser = "spUserSuperLoginEditUser";

        protected string sUserID;
        protected string[] sArrLevelUser = new string[5];
        protected int iColHeader = 0;
        protected string sSortOrder = "ASC";

        protected string sTitleResetMessage = "Reset Password";
        protected string sResetMessage = "Are you sure want to reset the Password?";

        protected string sErrSuperStatus = "Cannot edit or add new Super User.";
        protected string sErrActiveUser = "User currently is active, can not be edited.";

        protected bool isEdit = true;
        protected bool isSuperUser;
        #endregion
 
        /// <summary>
        /// Form where we can view list of users
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to connect to database</param>
        /// <param name="_sUserID">String : Current UserID</param>
        public FrmUserMngmt(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sUserID)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUserID = _sUserID;
            isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
            InitializeComponent();
        }

        /// <summary>
        /// Event Function, runs when form is loading.
        /// Call InitForm function
        /// </summary>
        private void FrmUserMngmt_Load(object sender, EventArgs e)
        {
            InitForm();
            CommonClass.InputLog(oSqlConnAuditTrail, "", sUserID, "", CommonMessage.sFormOpened + "User Management", "");
        }

        /// <summary>
        /// Initializing form, such as buttons and data to be shown.
        /// </summary>
        protected void InitForm()
        {
            InitButton();
            InitData();
        }

        /// <summary>
        /// Initialize buttons in the form.
        /// </summary>
        protected void InitButton()
        {
            btnAdd.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.UM, Privilege.Add);
            btnEdit.Text = (UserPrivilege.IsAllowed(PrivilegeCode.UM, Privilege.Edit)) ? "Edit" : "View";
        }
        
        /// <summary>
        /// Initialize data to be shown in the form.
        /// Get data from database and show it at datagridview
        /// </summary>
        public void InitData()
        {
            LoadDataUser();
            InitDgUserView();
        }

        /// <summary>
        /// Get User data from database
        /// </summary>
        public void LoadDataUser()
        {
            oDataTable = new DataTable();
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditions();
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                SqlDataReader oRead = oSqlCmd.ExecuteReader();
                oDataTable.Clear();
                oDataTable.Load(oRead);

                oSqlCmd.Dispose();
                oRead.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Get the conditions string for "NOT LIKE SuperUser"
        /// </summary>
        /// <returns>string : sConditions</returns>
        protected string sConditions()
        {
            return isSuperUser ? "" : "WHERE UserRights NOT LIKE '%SU%'";
        }

        /// <summary>
        /// Initialize datagridview
        /// </summary>
        public void InitDgUserView()
        {
            dgUserView.DataSource = oDataTable;
            dgUserView.Columns["Password"].Visible = false;
            dgUserView.Columns["UserRights"].Visible = false;
        }

        /// <summary>
        /// Get the selected UserID from datagridview
        /// </summary>
        /// <returns>string : UserID</returns>
        public string sGetSelectedUID()
        {
            return dgUserView["UserID", dgUserView.CurrentRow.Index].Value.ToString();
        }

        /// <summary>
        /// Get the selected UserName from datagridview
        /// </summary>
        /// <returns>string : UserName</returns>
        public string sGetSelectedUserName()
        {
            return dgUserView["UserName", dgUserView.CurrentRow.Index].Value.ToString();
        }

        /// <summary>
        /// Determines if any User is selected.
        /// </summary>
        /// <returns>boolean : true if selected, else false</returns>
        public bool isUserSelected()
        {
            return dgUserView.SelectedRows.Count > 0;
        }

        /// <summary>
        /// Event function, runs when btnAdd is clicked.
        /// Call UserMngmt_Edit form to add new user data
        /// </summary>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            FrmUserMngmt_Edit fAdd = new FrmUserMngmt_Edit(!isEdit, oSqlConn, oSqlConnAuditTrail, isSuperUser);
            try
            {
                fAdd.ShowDialog();
            }
            finally
            {
                InitData();
                fAdd.Dispose();
            }
        }

        /// <summary>
        /// Event Function, runs when btnEdit is clicked.
        /// Call UserMngmt_Edit to edit selected user data
        /// </summary>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (isUserSelected())
            {
                FrmUserMngmt_Edit fEdit = new FrmUserMngmt_Edit(isEdit, oSqlConn, oSqlConnAuditTrail, isSuperUser);
                try
                {
                    fEdit.sGetUID = sGetSelectedUID();
                    fEdit.ShowDialog();
                }
                finally
                {
                    InitData();
                    fEdit.Dispose();
                }
            }
        }

        /// <summary>
        /// Event function, runs when btnCancel is clicked.
        /// Close the form
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}