using System;
using System.Data;
using System.Windows.Forms;
using InSysClass;
using System.Threading;
//using MsExcel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
//using Excel;

namespace InSys
{
    /// <summary>
    /// Form to map the Data Upload setting
    /// </summary>
    public partial class FrmUploadDefinition : Form
    {
        private readonly CultureInfo oCultInfo = Thread.CurrentThread.CurrentCulture;
        //Needed to store original culture, and then restore it later.

        private SqlConnection oSqlConn;
        private SqlConnection oSqlConnAuditTrail;
        //private MsExcel._Application oXLapp;   //Excel object to be used throughout process.
        //private MsExcel._Workbook oXLbook;     //Excel object to be used throughout process.
        //private MsExcel._Worksheet oXLsheet;   //Excel object to be used throughout process.
        //private MsExcel.Range oXLrange;       //Excel object to be used throughout process.
        private string sTemplate;

        /// <summary>
        /// Upload Definition Form constructor
        /// </summary>
        /// <param name="_oSqlConn">SQLConnection : SQLCOnnection parameter</param>
        public FrmUploadDefinition(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            this.oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        /// <summary>
        /// Commands the computer what to do on this form load event.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void FrmUploadDefinition_Load(object sender, EventArgs e)
        {
            try
            {
                /*if (InitData())*/ LoadData();
                /*else this.Close();*/
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        /// <summary>
        /// Commands the computer what to do on this form closed event.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void FrmUploadDefinition_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                DisposeData();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Dispose();
            }
        }

        /// <summary>
        /// Sets displays if the file typed in this text box exists.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void txtSourceFile_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SetDisplay(File.Exists(txtSourceFile.Text)))
                {
                    ExcelEndSession();//Reset Excel Session if there's any.
                    ExcelStartSession(txtSourceFile.Text);
                    SetDataFromExcel();
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Shows the full path of the typed file in this text box.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void txtSourceFile_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                tipUploadDefinition.Show(txtSourceFile.Text, txtSourceFile);
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Hides shown tool tip on mouse leave.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void txtSourceFile_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                tipUploadDefinition.Hide(txtSourceFile);
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Shows a dialog box to choose a source file.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                if (ofdDefinition.ShowDialog() == DialogResult.OK)
                    txtSourceFile.Text = ofdDefinition.FileName;

                sTemplate = ofdDefinition.SafeFileName.Substring(19, ofdDefinition.SafeFileName.Length - 23);
                
                if (InitData())
                    FillDataGridView(sTemplate);
                else this.Close();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Sends a new Upload Definition. If it already exists it'll ask the user if they still want to save it or not.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            #region x
            /*string sMap;
            if (!File.Exists(Application.StartupPath + "\\Map.config") || MessageBox.Show("Map Definition file already exists. Replace existing file?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if ((sMap = sGenerateMap()) != null)
                {
                    File.WriteAllText(Application.StartupPath + "\\Map.config", sMap);
                    CommonClass.inputLog(oSqlConn, "", UserData.sUserID, "", "Create Upload Definition", "Saved at " + Application.StartupPath + "\\Map.config");
                    this.Close();
                }
            }*/
            #endregion
            try
            {
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                if (!CommonClass.IsUploadDefinitionExists(oSqlConn, sTemplate) || MessageBox.Show("Upload Definition file already exists. Update Upload Definition?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (CreateUploadDefinition())
                    {
                        CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", "Create Upload Defintion", "");
                        this.Close();
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Custom Function(s)
        /// <summary>
        /// Initializes the environment, such as Culture Info, and checks wether an Upload Definition already exists or not.
        /// </summary>
        /// <returns></returns>
        private bool InitData()
        {
            SetCultureInfo("en-US");
            //SetColumnSource();
            return
                !CommonClass.IsUploadDefinitionExists(oSqlConn, sTemplate) ||
                MessageBox.Show("Upload Definition file already exists. Update Upload Definition?", Application.ProductName,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        }

        /// <summary>
        /// Releases all used data, such as Culture Info, and ends the session for Microsoft Excel service if it's not been terminated yet.
        /// </summary>
        private void DisposeData()
        {
            ExcelEndSession();
            ResetCultureInfo();
        }

        /// <summary>
        /// Populates the data grid view dan sets it's display.
        /// </summary>
        private void LoadData()
        {
            //FillComboBoxData();
            //FillDataGridView();
            SetDisplay(false);
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + "Upload Definition", "");
        }

        //private void FillComboBoxData()
        //{
        //    if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
        //    SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
        //    oSqlComm.CommandType = CommandType.StoredProcedure;

        //    DataTable oTable = new DataTable();
        //    new SqlDataAdapter(oSqlComm).Fill(oTable);
        //    cmbDatabaseDestination.DataSource = oTable;
        //}

        /// <summary>
        /// Populates the data grid view.
        /// </summary>
        private void FillDataGridView(string sGridTemplate)
        {
            dgvUploadDefinition.Rows.Clear();
            dgvUploadDefinition.Refresh();
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand cmdLoad = new SqlCommand(CommonSP.sSPUploadTagDEAAUnion, oSqlConn);
            cmdLoad.CommandType = CommandType.StoredProcedure;
            cmdLoad.Parameters.Add("@sTemplate", SqlDbType.VarChar, 15).Value = sGridTemplate;

            SqlDataReader oReader = cmdLoad.ExecuteReader();

            while (oReader.Read())
            {
                dgvUploadDefinition.Rows.Add();
                dgvUploadDefinition[0, dgvUploadDefinition.RowCount - 1].Value = oReader.GetValue(0).ToString(); //Column Name
                dgvUploadDefinition[2, dgvUploadDefinition.RowCount - 1].Value = oReader.GetValue(1).ToString(); //Tag
                dgvUploadDefinition[3, dgvUploadDefinition.RowCount - 1].Value = oReader.GetValue(2).ToString(); //ID
            }
            oReader.Close();
            dgvUploadDefinition.Refresh();
            //SqlDataReader oReader = new SqlCommand(CommonSP.sSPUploadTagDEAAUnion, oSqlConn).ExecuteReader();
            //while (oReader.Read())
            //{
            //    dgvUploadDefinition.Rows.Add();
            //    dgvUploadDefinition[0, dgvUploadDefinition.RowCount - 1].Value = oReader.GetValue(0).ToString(); //Column Name
            //    dgvUploadDefinition[2, dgvUploadDefinition.RowCount - 1].Value = oReader.GetValue(1).ToString(); //Tag
            //    dgvUploadDefinition[3, dgvUploadDefinition.RowCount - 1].Value = oReader.GetValue(2).ToString(); //ID
            //}
            //oReader.Close();
            #region x
            /*if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;
            oSqlComm.Parameters.Add("@sCond", SqlDbType.VarChar).Value =
                @"WHERE
                    Tag LIKE 'DE__' OR
                    Tag LIKE 'AA__'";
            SqlDataReader oReader = oSqlComm.ExecuteReader();
            while (oReader.Read())
            {
                #region Exception
                if (dgvUploadDefinition.RowCount == 4)
                {
                    dgvUploadDefinition.Rows.Add();
                    dgvUploadDefinition[0, 4].Value = "Master";
                }
                #endregion
                dgvUploadDefinition.Rows.Add();
                dgvUploadDefinition[0, dgvUploadDefinition.RowCount - 1].Value = oReader.GetValue(4).ToString();
            }
            oReader.Close();*/

            /*int iLine = -1;
            string[] sLines = File.ReadAllLines(Application.StartupPath + "\\Template.config");

            List<string> listTerminalTag = new List<string>();
            List<bool> listTerminalMandatory = new List<bool>();

            List<string> listAcquirerTag = new List<string>();
            List<bool> listAcquirerMandatory = new List<bool>();

            List<string> listAcquirer = new List<string>();
            List<string> listAcquirerCondition = new List<string>();

            List<string> listAcquirerAll = new List<string>();

            dgvUploadDefinition = (DataGridView)
                ProcessTag(sLines, ref iLine, "[Terminal]", dgvUploadDefinition);

            listTerminalTag = (List<string>)
                ProcessTag(sLines, ref iLine, "[Terminal Tag]", listTerminalTag);

            listTerminalMandatory = (List<bool>)
                ProcessTag(sLines, ref iLine, "[Terminal Mandatory]", listTerminalMandatory);

            listAcquirer = (List<string>)
                ProcessTag(sLines, ref iLine, "[Acquirer]", listAcquirer);

            listAcquirerTag = (List<string>)
                ProcessTag(sLines, ref iLine, "[Acquirer Tag]", listAcquirerTag);

            listAcquirerCondition = (List<string>)
                ProcessTag(sLines, ref iLine, "[Acquirer Condition]", ref listAcquirerAll, listAcquirerCondition);

            listAcquirerMandatory = (List<bool>)
                ProcessTag(sLines, ref iLine, "[Acquirer Mandatory]", listAcquirerMandatory);

            ProcessAcquirersToDGV(listAcquirerAll, listAcquirer, listAcquirerCondition);*/
            #endregion
        }

        //private void ProcessAcquirersToDGV(List<string> listAcquirerAll, List<string> listAcquirerColumn, List<string> listAcquirerCondition)
        //{
        //    string[] sAcq = listAcquirerAll.ToArray();
        //    string[] sCol = listAcquirerColumn.ToArray();
        //    string[] sCon = listAcquirerCondition.ToArray();

        //    dgvUploadDefinition.Rows.Add();
        //    dgvUploadDefinition[0, dgvUploadDefinition.RowCount - 1].Value = "Master";

        //    for (int i = 0; i < sAcq.Length; i++)
        //    {
        //        for (int n = 0; n < sCol.Length; n++)
        //        {
        //            if (new List<string>(sCon[n].Split(',')).Contains(sAcq[i]))
        //            {
        //                dgvUploadDefinition.Rows.Add();
        //                dgvUploadDefinition[0, dgvUploadDefinition.RowCount - 1].Value = sCol[n] + " " + sAcq[i];
        //            }
        //        }
        //    }
        //}

        //private object ProcessTag(string[] sLines, ref int iLine, string sTag, ref List<string> listAcquirer, object obj)
        //{
        //    while (sLines[++iLine] != sTag) ;
        //    int iCounter = Convert.ToInt32(sLines[++iLine].Split('=')[1]);

        //    listAcquirer.AddRange(sLines[++iLine].Split('=')[1].Split(','));
        //    for (int iRow = 0; iRow < iCounter; iRow++) ((List<string>)obj).Add(sLines[++iLine].Split('=')[1]);

        //    return obj;
        //}

        //private object ProcessTag(string[] sLines, ref int iLine, string sTag, object obj)
        //{
        //    while (sLines[++iLine] != sTag) ;
        //    int iCounter = Convert.ToInt32(sLines[++iLine].Split('=')[1]);

        //    for (int iRow = 0; iRow < iCounter; iRow++)
        //    {
        //        string sValue = sLines[++iLine].Split('=')[1];
        //        if (obj is DataGridView)
        //        {
        //            ((DataGridView)obj).Rows.Add();
        //            ((DataGridView)obj)[0, ((DataGridView)obj).RowCount - 1].Value = sValue;
        //        }
        //        else if (obj is List<string>)
        //        {
        //            ((List<string>)obj).Add(sValue);
        //        }
        //        else if (obj is List<bool>)
        //        {
        //            ((List<bool>)obj).Add(Convert.ToBoolean(sValue));
        //        }
        //    }

        //    return obj;
        //}

        /// <summary>
        /// Sets display after the data grid view has been populated.
        /// </summary>
        /// <param name="isEnabled"></param>
        /// <returns></returns>
        private bool SetDisplay(bool isEnabled)
        {
            this.Opacity = 1;

            numStartingRow.Value = numStartingRow.Maximum > 2 ?
                2 : numStartingRow.Maximum;
            numStartingRow.Enabled = isEnabled;

            cmbDatabaseDestination.SelectedIndex = -1;
            cmbDatabaseDestination.Enabled = isEnabled;

            //gbBody.Enabled = isEnabled;
            dgvUploadDefinition.ClearSelection();
            dgvUploadDefinition.Select();
            btnSave.Enabled = isEnabled;

            return isEnabled;
        }

        /// <summary>
        /// Sets this thread's Culture Info to the desired Culture Info.
        /// </summary>
        /// <param name="sCultName">The desired Culture Info</param>
        /// <returns>The new Calture Info</returns>
        private CultureInfo SetCultureInfo(string sCultName)
        {
            return Thread.CurrentThread.CurrentCulture = new CultureInfo(sCultName);
        }

        /// <summary>
        /// Reverts to this thread's original Culture Info before it's changed.
        /// </summary>
        /// <returns>The original Culture Info</returns>
        private CultureInfo ResetCultureInfo()
        {
            return Thread.CurrentThread.CurrentCulture = oCultInfo;
        }

        /// <summary>
        /// Starts session for the Microsoft Excel service.
        /// </summary>
        /// <param name="sFileName">Filename (.xls)</param>
        private void ExcelStartSession(string sFileName)
        {
            //oXLapp = new MsExcel.Application();
            //oXLbook = oXLapp.Workbooks.Open(
            //    sFileName, 0, true, 5, "",
            //    "", true, MsExcel.XlPlatform.xlWindows, "\t", false,
            //    false, 0, true, 1, 0);
            //oXLsheet = (MsExcel._Worksheet)oXLbook.Worksheets.get_Item(1);
            //oXLrange = oXLsheet.UsedRange;
        }

        /// <summary>
        /// Ends session for the Microsoft Excel service.
        /// </summary>
        private void ExcelEndSession()
        {
            //if (oXLapp != null && oXLbook != null && oXLrange != null && oXLsheet != null)
            //{
            //    try
            //    {
            //        oXLbook.Close(false, null, null);
            //        oXLapp.Quit();

            //        DisposeExcelObj(oXLsheet);
            //        DisposeExcelObj(oXLbook);
            //        DisposeExcelObj(oXLapp);
            //    }
            //    catch
            //    {
            //        DisposeExcelObj(oXLsheet);
            //        DisposeExcelObj(oXLbook);
            //        DisposeExcelObj(oXLapp);
            //    }
            //}
        }

        /// <summary>
        /// Releases all Microsoft Excel objects.
        /// </summary>
        /// <param name="oExcelObj"></param>
        private void DisposeExcelObj(object oExcelObj)
        {
            try
            {
                Marshal.ReleaseComObject(oExcelObj);
                oExcelObj = null;
            }
            catch (Exception oException)
            {
                CommonClass.doWriteErrorFile(oException.ToString());
                oExcelObj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        /// <summary>
        /// Sets data according to the retrieved data from the selected Excel file.
        /// </summary>
        private void SetDataFromExcel()
        {
            SetNumericUpDown();
            SetDataGridView();
            SetColumnSource();
        }

        /// <summary>
        /// Sets maximum value for the numeric up down according to the selected Excel file.
        /// </summary>
        private void SetNumericUpDown()
        {
            //numStartingRow.Maximum = oXLrange.Rows.Count;
        }

        /// <summary>
        /// Sets the data grid view.
        /// </summary>
        private void SetDataGridView()
        {
            for (int iRow = 0; iRow < dgvUploadDefinition.RowCount; iRow++)
                dgvUploadDefinition.Rows[iRow].Cells[1].Value = null;
        }

        /// <summary>
        /// Sets columns selection based on the Excel file.
        /// </summary>
        private void SetColumnSource()
        {
            DataTable oTable = new DataTable();
            oTable.Columns.Add("Index");
            oTable.Columns.Add("Source");
            //for (int iCol = 1; iCol <= oXLrange.Columns.Count; iCol++)
            //{
            //    object[] oCells = new object[2];
            //    oCells[0] = iCol;
            //    if ((oCells[1] = (oXLrange.Cells[1, iCol] as MsExcel.Range).Value2) == null)
            //    {
            //        SetDisplay(false);
            //        MessageBox.Show("File is empty.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //        return;
            //    }
            //    oCells[1] = oCells[1].ToString();
            //    oTable.Rows.Add(oCells);
            //}
            clmSource.DisplayMember = "Source";
            clmSource.ValueMember = "Index";
            clmSource.DataSource = oTable;

            #region x
            /*DataTable oTable = new DataTable();
            oTable.Columns.Add("AZ");
            for (int l = 'A' - 1; l <= 'Z'; l++)
                for (int r = 'A'; r <= 'Z'; r++)
                    oTable.Rows.Add((l >= 'A' ? ((char)l).ToString() : "") + ((char)r).ToString());
            clmSource.ValueMember = "AZ";
            clmSource.DisplayMember = "AZ";
            clmSource.DataSource = oTable;*/
            #endregion
        }

        #region x
        //private string sGetMapping()
        //{
        //    StringBuilder sHeader = new StringBuilder();
        //    StringBuilder sFooter = new StringBuilder();

        //    for (int iCol = 1; iCol <= dgvUploadDefinition.RowCount; iCol++)
        //    {
        //        if (dgvUploadDefinition.Rows[iCol - 1].Cells[1].Value == null)
        //        {
        //            dgvUploadDefinition.Rows[iCol - 1].Cells[1].Selected = true;
        //            MessageBox.Show("Please select Source Column for " + dgvUploadDefinition.Rows[iCol - 1].Cells[0].Value.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        //            return null;
        //        }

        //        sHeader.Append("Col-");
        //        sHeader.Append(iCol.ToString());
        //        sHeader.Append("=");
        //        sHeader.Append(dgvUploadDefinition.Rows[iCol - 1].Cells[0].Value.ToString());
        //        sHeader.Append("\n");

        //        sFooter.Append(dgvUploadDefinition.Rows[iCol - 1].Cells[0].Value.ToString());
        //        sFooter.Append("=");
        //        sFooter.Append(dgvUploadDefinition.Rows[iCol - 1].Cells[1].Value.ToString());
        //        sFooter.Append("\n");
        //    }

        //    return sHeader.ToString() + "\n" + sFooter.ToString();
        //}

        //private string sGenerateMap()
        //{
        //    string sMap;
        //    StringBuilder sContent = new StringBuilder();

        //    sContent.Append("[Upload File]\n");
        //    sContent.Append("Filename="); sContent.Append(txtSourceFile.Text); sContent.Append("\n");
        //    sContent.Append("\n");

        //    sContent.Append("[Mapping]\n");
        //    sContent.Append("Counter="); sContent.Append(dgvUploadDefinition.RowCount.ToString()); sContent.Append("\n");
        //    sContent.Append("Start Row="); sContent.Append(numStartingRow.Value.ToString()); sContent.Append("\n");

        //    if ((sMap = sGetMapping()) == null) return null;
        //    sContent.Append(sMap);

        //    return sContent.ToString();
        //}
        #endregion

        /// <summary>
        /// Generates DE (Terminal) data table from the data grid view.
        /// </summary>
        /// <param name="iStartRow">Starting row</param>
        /// <param name="sColumnCollection">Columns format</param>
        /// <returns>The table</returns>
        private DataTable GetTableDE(ref int iStartRow, string[] sColumnCollection)
        {
            DataTable oTable = new DataTable();
            object[] oCells = new object[2];

            foreach (string sCol in sColumnCollection)
                oTable.Columns.Add(sCol);

            do
            {
                if (dgvUploadDefinition["clmSource", iStartRow].Value == null)
                {
                    dgvUploadDefinition["clmSource", iStartRow].Selected = true;
                    MessageBox.Show("Please select Source Column for " + dgvUploadDefinition["clmDestination", iStartRow].Value.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return null;
                }
                oCells[0] = Convert.ToInt16(dgvUploadDefinition["clmID", iStartRow].Value);
                oCells[1] = Convert.ToInt16(dgvUploadDefinition["clmSource", iStartRow].Value);
                oTable.Rows.Add(oCells);
                oTable.Rows[oTable.Rows.Count - 1].AcceptChanges();
                oTable.Rows[oTable.Rows.Count - 1].SetModified();
            }
            while (dgvUploadDefinition["clmTag", iStartRow++].Value.ToString() != "");

            return oTable;
        }

        /// <summary>
        /// Generates AA (Acquirer) data table from the data grid view.
        /// </summary>
        /// <param name="iStartRow">Starting row</param>
        /// <param name="sColumnCollection">Columns format</param>
        /// <returns>The table</returns>
        private DataTable GetTableAA(ref int iStartRow, string[] sColumnCollection)
        {
            DataTable oTable = new DataTable();
            object[] oCells = new object[2];

            foreach (string sCol in sColumnCollection)
                oTable.Columns.Add(sCol);

            do
            {
                try
                {
                    if (dgvUploadDefinition["clmSource", iStartRow].Value == null)
                    {
                        dgvUploadDefinition["clmSource", iStartRow].Selected = true;
                        MessageBox.Show("Please select Source Column.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return null;
                    }
                    oCells[0] = Convert.ToInt16(dgvUploadDefinition["clmID", iStartRow].Value);
                    oCells[1] = Convert.ToInt16(dgvUploadDefinition["clmSource", iStartRow].Value);
                    oTable.Rows.Add(oCells);
                    oTable.Rows[oTable.Rows.Count - 1].AcceptChanges();
                    oTable.Rows[oTable.Rows.Count - 1].SetModified();
                }
                catch
                {
                    MessageBox.Show("No Acquirer setting. Go to Template Definition to update Template", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
            }
            while (++iStartRow < dgvUploadDefinition.RowCount);

            return oTable;
        }

        /// <summary>
        /// Prepares DE (Terminal) table to be sent to database.
        /// </summary>
        /// <param name="oAdapt">A reference SQL Data Adapter variable</param>
        /// <param name="oTable">A reference DE Table</param>
        /// <param name="iStartRow">A reference starting row</param>
        /// <returns>Returns true if all datas valid, otherwise false.</returns>
        private bool IsSuccessPreparingUpdateDE(ref SqlDataAdapter oAdapt, ref DataTable oTable, ref int iStartRow)
        {
            oAdapt = new SqlDataAdapter();
            string[] sColumns =
                {
                    "ID",
                    "Col"
                };

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            oAdapt.UpdateCommand = new SqlCommand(CommonSP.sSPUploadTagDEUpdateSourceColumn, oSqlConn);
            oAdapt.UpdateCommand.CommandType = CommandType.StoredProcedure;

            oAdapt.UpdateCommand.Parameters.Add("@iID", SqlDbType.SmallInt).SourceColumn = sColumns[0];
            oAdapt.UpdateCommand.Parameters.Add("@iCol", SqlDbType.SmallInt).SourceColumn = sColumns[1];

            if ((oTable = GetTableDE(ref iStartRow, sColumns)) == null) return false;
            else return true;
        }

        /// <summary>
        /// Prepares AA (Acquirer) table to be sent to database.
        /// </summary>
        /// <param name="oAdapt">A reference SQL Data Adapter variable</param>
        /// <param name="oTable">A reference DE Table</param>
        /// <param name="iStartRow">A reference starting row</param>
        /// <returns>Returns true if all datas valid, otherwise false.</returns>
        private bool IsSuccessPreparingUpdateAA(ref SqlDataAdapter oAdapt, ref DataTable oTable, ref int iStartRow)
        {
            oAdapt = new SqlDataAdapter();
            string[] sColumns =
                {
                    "ID",
                    "Col"
                };

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            oAdapt.UpdateCommand = new SqlCommand(CommonSP.sSPUploadTagAAUpdateSourceColumn, oSqlConn);
            oAdapt.UpdateCommand.CommandType = CommandType.StoredProcedure;

            oAdapt.UpdateCommand.Parameters.Add("@iID", SqlDbType.SmallInt).SourceColumn = sColumns[0];
            oAdapt.UpdateCommand.Parameters.Add("@iCol", SqlDbType.SmallInt).SourceColumn = sColumns[1];

            if ((oTable = GetTableAA(ref iStartRow, sColumns)) == null) return false;
            else return true;
        }

        /// <summary>
        /// Tells the database what's the starting row.
        /// </summary>
        /// <param name="iStartRow">Starting row</param>
        private void SetStartingRow(int iStartRow)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPUploadControlSet, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;

            oSqlComm.Parameters.Add("@iRow", SqlDbType.SmallInt).Value = iStartRow;

            oSqlComm.ExecuteNonQuery();
        }

        /// <summary>
        /// All the process of creating an Upload Definition.
        /// </summary>
        /// <returns>Returns true if successfull, otherwise false.</returns>
        private bool CreateUploadDefinition()
        {
            int iStartRow = 0;

            DataTable oTableDE = new DataTable();
            DataTable oTableAA = new DataTable();

            SqlDataAdapter oAdaptDE = new SqlDataAdapter();
            SqlDataAdapter oAdaptAA = new SqlDataAdapter();

            if (IsSuccessPreparingUpdateDE(ref oAdaptDE, ref oTableDE, ref iStartRow) &&
                IsSuccessPreparingUpdateAA(ref oAdaptAA, ref oTableAA, ref iStartRow))
            {
                SetStartingRow(Convert.ToInt16(numStartingRow.Value));
                oAdaptDE.Update(oTableDE);
                oAdaptAA.Update(oTableAA);
                return true;
            }
            else return false;
        }
        #endregion
    }
}