﻿namespace InSys
{
    partial class FrmLogin2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin2));
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblUserID = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtUserID = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.gbLogin = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtServerPort = new System.Windows.Forms.TextBox();
            this.btnCheckMainConn = new System.Windows.Forms.Button();
            this.txtPasswordSql = new System.Windows.Forms.TextBox();
            this.txtUserSql = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.wrkLogin = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnHide = new System.Windows.Forms.Button();
            this.btnDetail = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtServerPortAuditTrail = new System.Windows.Forms.TextBox();
            this.btnCheckAuditTrailConn = new System.Windows.Forms.Button();
            this.txtPasswordAuditTrail = new System.Windows.Forms.TextBox();
            this.txtUserAuditTrail = new System.Windows.Forms.TextBox();
            this.lblPasswordDBAuditTrail = new System.Windows.Forms.Label();
            this.lblUserDBAuditTrail = new System.Windows.Forms.Label();
            this.lblDBAuditTrail = new System.Windows.Forms.Label();
            this.lblServerAuditTrail = new System.Windows.Forms.Label();
            this.txtDatabaseAuditTrail = new System.Windows.Forms.TextBox();
            this.txtServerAuditTrail = new System.Windows.Forms.TextBox();
            this.gbDatabaseConnection = new System.Windows.Forms.GroupBox();
            this.gbLogin.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbDatabaseConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnOK.Location = new System.Drawing.Point(36, 111);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(131, 37);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(183, 111);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(129, 37);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblUserID
            // 
            this.lblUserID.AutoSize = true;
            this.lblUserID.Location = new System.Drawing.Point(36, 32);
            this.lblUserID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(55, 17);
            this.lblUserID.TabIndex = 5;
            this.lblUserID.Text = "User ID";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(36, 71);
            this.lblPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(69, 17);
            this.lblPassword.TabIndex = 6;
            this.lblPassword.Text = "Password";
            // 
            // txtUserID
            // 
            this.txtUserID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUserID.Location = new System.Drawing.Point(143, 28);
            this.txtUserID.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserID.MaxLength = 10;
            this.txtUserID.Name = "txtUserID";
            this.txtUserID.Size = new System.Drawing.Size(178, 22);
            this.txtUserID.TabIndex = 0;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(143, 68);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(178, 22);
            this.txtPassword.TabIndex = 1;
            this.txtPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPassword_KeyDown);
            // 
            // gbLogin
            // 
            this.gbLogin.Controls.Add(this.label5);
            this.gbLogin.Controls.Add(this.txtServerPort);
            this.gbLogin.Controls.Add(this.btnCheckMainConn);
            this.gbLogin.Controls.Add(this.txtPasswordSql);
            this.gbLogin.Controls.Add(this.txtUserSql);
            this.gbLogin.Controls.Add(this.label4);
            this.gbLogin.Controls.Add(this.label3);
            this.gbLogin.Controls.Add(this.label2);
            this.gbLogin.Controls.Add(this.label1);
            this.gbLogin.Controls.Add(this.txtDatabase);
            this.gbLogin.Controls.Add(this.txtServer);
            this.gbLogin.Location = new System.Drawing.Point(8, 23);
            this.gbLogin.Margin = new System.Windows.Forms.Padding(4);
            this.gbLogin.Name = "gbLogin";
            this.gbLogin.Padding = new System.Windows.Forms.Padding(4);
            this.gbLogin.Size = new System.Drawing.Size(495, 167);
            this.gbLogin.TabIndex = 0;
            this.gbLogin.TabStop = false;
            this.gbLogin.Text = "Newinsys Connection";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 52);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Port";
            // 
            // txtServerPort
            // 
            this.txtServerPort.Enabled = false;
            this.txtServerPort.Location = new System.Drawing.Point(129, 49);
            this.txtServerPort.Margin = new System.Windows.Forms.Padding(4);
            this.txtServerPort.Name = "txtServerPort";
            this.txtServerPort.Size = new System.Drawing.Size(154, 22);
            this.txtServerPort.TabIndex = 1;
            this.txtServerPort.Text = "1433";
            // 
            // btnCheckMainConn
            // 
            this.btnCheckMainConn.Enabled = false;
            this.btnCheckMainConn.Location = new System.Drawing.Point(364, 23);
            this.btnCheckMainConn.Margin = new System.Windows.Forms.Padding(4);
            this.btnCheckMainConn.Name = "btnCheckMainConn";
            this.btnCheckMainConn.Size = new System.Drawing.Size(127, 28);
            this.btnCheckMainConn.TabIndex = 5;
            this.btnCheckMainConn.Text = "Test Connection";
            this.btnCheckMainConn.UseVisualStyleBackColor = true;
            this.btnCheckMainConn.Click += new System.EventHandler(this.btnCheckMainConn_Click);
            // 
            // txtPasswordSql
            // 
            this.txtPasswordSql.Enabled = false;
            this.txtPasswordSql.Location = new System.Drawing.Point(129, 130);
            this.txtPasswordSql.Margin = new System.Windows.Forms.Padding(4);
            this.txtPasswordSql.Name = "txtPasswordSql";
            this.txtPasswordSql.PasswordChar = '*';
            this.txtPasswordSql.ShortcutsEnabled = false;
            this.txtPasswordSql.Size = new System.Drawing.Size(225, 22);
            this.txtPasswordSql.TabIndex = 4;
            // 
            // txtUserSql
            // 
            this.txtUserSql.Enabled = false;
            this.txtUserSql.Location = new System.Drawing.Point(129, 103);
            this.txtUserSql.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserSql.Name = "txtUserSql";
            this.txtUserSql.Size = new System.Drawing.Size(225, 22);
            this.txtUserSql.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 133);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Password DB";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 106);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "User DB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 79);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Database";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Server";
            // 
            // txtDatabase
            // 
            this.txtDatabase.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDatabase.Enabled = false;
            this.txtDatabase.Location = new System.Drawing.Point(129, 76);
            this.txtDatabase.Margin = new System.Windows.Forms.Padding(4);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(225, 22);
            this.txtDatabase.TabIndex = 2;
            // 
            // txtServer
            // 
            this.txtServer.Enabled = false;
            this.txtServer.Location = new System.Drawing.Point(129, 22);
            this.txtServer.Margin = new System.Windows.Forms.Padding(4);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(154, 22);
            this.txtServer.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(352, 373);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(147, 31);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnChange
            // 
            this.btnChange.Location = new System.Drawing.Point(352, 373);
            this.btnChange.Margin = new System.Windows.Forms.Padding(4);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(147, 31);
            this.btnChange.TabIndex = 6;
            this.btnChange.Text = "Change Connection";
            this.btnChange.UseVisualStyleBackColor = true;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // wrkLogin
            // 
            this.wrkLogin.WorkerReportsProgress = true;
            this.wrkLogin.WorkerSupportsCancellation = true;
            this.wrkLogin.DoWork += new System.ComponentModel.DoWorkEventHandler(this.wrkLogin_DoWork);
            this.wrkLogin.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.wrkLogin_ProgressChanged);
            this.wrkLogin.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.wrkLogin_RunWorkerCompleted);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnHide);
            this.groupBox1.Controls.Add(this.btnOK);
            this.groupBox1.Controls.Add(this.btnDetail);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.txtUserID);
            this.groupBox1.Controls.Add(this.lblUserID);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.lblPassword);
            this.groupBox1.Location = new System.Drawing.Point(12, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(524, 167);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnHide
            // 
            this.btnHide.Location = new System.Drawing.Point(328, 112);
            this.btnHide.Margin = new System.Windows.Forms.Padding(4);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(129, 37);
            this.btnHide.TabIndex = 4;
            this.btnHide.Text = "Hide";
            this.btnHide.UseVisualStyleBackColor = true;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // btnDetail
            // 
            this.btnDetail.Location = new System.Drawing.Point(329, 111);
            this.btnDetail.Margin = new System.Windows.Forms.Padding(4);
            this.btnDetail.Name = "btnDetail";
            this.btnDetail.Size = new System.Drawing.Size(129, 37);
            this.btnDetail.TabIndex = 5;
            this.btnDetail.Text = "Detail";
            this.btnDetail.UseVisualStyleBackColor = true;
            this.btnDetail.Click += new System.EventHandler(this.btnDetail_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtServerPortAuditTrail);
            this.groupBox2.Controls.Add(this.btnCheckAuditTrailConn);
            this.groupBox2.Controls.Add(this.txtPasswordAuditTrail);
            this.groupBox2.Controls.Add(this.txtUserAuditTrail);
            this.groupBox2.Controls.Add(this.lblPasswordDBAuditTrail);
            this.groupBox2.Controls.Add(this.lblUserDBAuditTrail);
            this.groupBox2.Controls.Add(this.lblDBAuditTrail);
            this.groupBox2.Controls.Add(this.lblServerAuditTrail);
            this.groupBox2.Controls.Add(this.txtDatabaseAuditTrail);
            this.groupBox2.Controls.Add(this.txtServerAuditTrail);
            this.groupBox2.Location = new System.Drawing.Point(8, 198);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(495, 167);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Audit Trail Connection";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 53);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Port";
            // 
            // txtServerPortAuditTrail
            // 
            this.txtServerPortAuditTrail.Enabled = false;
            this.txtServerPortAuditTrail.Location = new System.Drawing.Point(129, 50);
            this.txtServerPortAuditTrail.Margin = new System.Windows.Forms.Padding(4);
            this.txtServerPortAuditTrail.Name = "txtServerPortAuditTrail";
            this.txtServerPortAuditTrail.Size = new System.Drawing.Size(154, 22);
            this.txtServerPortAuditTrail.TabIndex = 1;
            this.txtServerPortAuditTrail.Text = "1433";
            // 
            // btnCheckAuditTrailConn
            // 
            this.btnCheckAuditTrailConn.Enabled = false;
            this.btnCheckAuditTrailConn.Location = new System.Drawing.Point(364, 23);
            this.btnCheckAuditTrailConn.Margin = new System.Windows.Forms.Padding(4);
            this.btnCheckAuditTrailConn.Name = "btnCheckAuditTrailConn";
            this.btnCheckAuditTrailConn.Size = new System.Drawing.Size(127, 28);
            this.btnCheckAuditTrailConn.TabIndex = 5;
            this.btnCheckAuditTrailConn.Text = "Test Connection";
            this.btnCheckAuditTrailConn.UseVisualStyleBackColor = true;
            this.btnCheckAuditTrailConn.Click += new System.EventHandler(this.btnCheckAuditTrailConn_Click);
            // 
            // txtPasswordAuditTrail
            // 
            this.txtPasswordAuditTrail.Enabled = false;
            this.txtPasswordAuditTrail.Location = new System.Drawing.Point(129, 128);
            this.txtPasswordAuditTrail.Margin = new System.Windows.Forms.Padding(4);
            this.txtPasswordAuditTrail.Name = "txtPasswordAuditTrail";
            this.txtPasswordAuditTrail.PasswordChar = '*';
            this.txtPasswordAuditTrail.ShortcutsEnabled = false;
            this.txtPasswordAuditTrail.Size = new System.Drawing.Size(225, 22);
            this.txtPasswordAuditTrail.TabIndex = 4;
            // 
            // txtUserAuditTrail
            // 
            this.txtUserAuditTrail.Enabled = false;
            this.txtUserAuditTrail.Location = new System.Drawing.Point(129, 102);
            this.txtUserAuditTrail.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserAuditTrail.Name = "txtUserAuditTrail";
            this.txtUserAuditTrail.Size = new System.Drawing.Size(225, 22);
            this.txtUserAuditTrail.TabIndex = 3;
            // 
            // lblPasswordDBAuditTrail
            // 
            this.lblPasswordDBAuditTrail.AutoSize = true;
            this.lblPasswordDBAuditTrail.Location = new System.Drawing.Point(23, 131);
            this.lblPasswordDBAuditTrail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPasswordDBAuditTrail.Name = "lblPasswordDBAuditTrail";
            this.lblPasswordDBAuditTrail.Size = new System.Drawing.Size(92, 17);
            this.lblPasswordDBAuditTrail.TabIndex = 10;
            this.lblPasswordDBAuditTrail.Text = "Password DB";
            // 
            // lblUserDBAuditTrail
            // 
            this.lblUserDBAuditTrail.AutoSize = true;
            this.lblUserDBAuditTrail.Location = new System.Drawing.Point(23, 105);
            this.lblUserDBAuditTrail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserDBAuditTrail.Name = "lblUserDBAuditTrail";
            this.lblUserDBAuditTrail.Size = new System.Drawing.Size(61, 17);
            this.lblUserDBAuditTrail.TabIndex = 9;
            this.lblUserDBAuditTrail.Text = "User DB";
            // 
            // lblDBAuditTrail
            // 
            this.lblDBAuditTrail.AutoSize = true;
            this.lblDBAuditTrail.Location = new System.Drawing.Point(23, 79);
            this.lblDBAuditTrail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDBAuditTrail.Name = "lblDBAuditTrail";
            this.lblDBAuditTrail.Size = new System.Drawing.Size(69, 17);
            this.lblDBAuditTrail.TabIndex = 8;
            this.lblDBAuditTrail.Text = "Database";
            // 
            // lblServerAuditTrail
            // 
            this.lblServerAuditTrail.AutoSize = true;
            this.lblServerAuditTrail.Location = new System.Drawing.Point(23, 27);
            this.lblServerAuditTrail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblServerAuditTrail.Name = "lblServerAuditTrail";
            this.lblServerAuditTrail.Size = new System.Drawing.Size(50, 17);
            this.lblServerAuditTrail.TabIndex = 6;
            this.lblServerAuditTrail.Text = "Server";
            // 
            // txtDatabaseAuditTrail
            // 
            this.txtDatabaseAuditTrail.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDatabaseAuditTrail.Enabled = false;
            this.txtDatabaseAuditTrail.Location = new System.Drawing.Point(129, 76);
            this.txtDatabaseAuditTrail.Margin = new System.Windows.Forms.Padding(4);
            this.txtDatabaseAuditTrail.Name = "txtDatabaseAuditTrail";
            this.txtDatabaseAuditTrail.Size = new System.Drawing.Size(225, 22);
            this.txtDatabaseAuditTrail.TabIndex = 2;
            // 
            // txtServerAuditTrail
            // 
            this.txtServerAuditTrail.Enabled = false;
            this.txtServerAuditTrail.Location = new System.Drawing.Point(129, 24);
            this.txtServerAuditTrail.Margin = new System.Windows.Forms.Padding(4);
            this.txtServerAuditTrail.Name = "txtServerAuditTrail";
            this.txtServerAuditTrail.Size = new System.Drawing.Size(225, 22);
            this.txtServerAuditTrail.TabIndex = 0;
            // 
            // gbDatabaseConnection
            // 
            this.gbDatabaseConnection.Controls.Add(this.btnSave);
            this.gbDatabaseConnection.Controls.Add(this.btnChange);
            this.gbDatabaseConnection.Controls.Add(this.gbLogin);
            this.gbDatabaseConnection.Controls.Add(this.groupBox2);
            this.gbDatabaseConnection.Location = new System.Drawing.Point(16, 198);
            this.gbDatabaseConnection.Margin = new System.Windows.Forms.Padding(4);
            this.gbDatabaseConnection.Name = "gbDatabaseConnection";
            this.gbDatabaseConnection.Padding = new System.Windows.Forms.Padding(4);
            this.gbDatabaseConnection.Size = new System.Drawing.Size(520, 417);
            this.gbDatabaseConnection.TabIndex = 1;
            this.gbDatabaseConnection.TabStop = false;
            this.gbDatabaseConnection.Text = "Database Connection";
            // 
            // FrmLogin2
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 628);
            this.Controls.Add(this.gbDatabaseConnection);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmLogin2";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLogin2_Closing);
            this.Load += new System.EventHandler(this.FrmLogin2_Load);
            this.gbLogin.ResumeLayout(false);
            this.gbLogin.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbDatabaseConnection.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Label lblUserID;
        internal System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.GroupBox gbLogin;
        private System.ComponentModel.BackgroundWorker wrkLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.Button btnDetail;
        private System.Windows.Forms.TextBox txtPasswordSql;
        private System.Windows.Forms.TextBox txtUserSql;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCheckAuditTrailConn;
        private System.Windows.Forms.TextBox txtPasswordAuditTrail;
        private System.Windows.Forms.TextBox txtUserAuditTrail;
        private System.Windows.Forms.Label lblPasswordDBAuditTrail;
        private System.Windows.Forms.Label lblUserDBAuditTrail;
        private System.Windows.Forms.Label lblDBAuditTrail;
        private System.Windows.Forms.Label lblServerAuditTrail;
        private System.Windows.Forms.TextBox txtDatabaseAuditTrail;
        private System.Windows.Forms.TextBox txtServerAuditTrail;
        private System.Windows.Forms.GroupBox gbDatabaseConnection;
        private System.Windows.Forms.Button btnCheckMainConn;
        public System.Windows.Forms.TextBox txtUserID;
        public System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtServerPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtServerPortAuditTrail;
    }
}