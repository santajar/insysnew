using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace InSys
{
    public partial class FrmDatabasePrimaryConnection : Form
    {
        public SqlConnection oSqlConn;
        
        public FrmDatabasePrimaryConnection()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Runs when form is loading, initiate data for Form.
        /// </summary>
        private void FrmDatabasePrimaryConnection_Load(object sender, EventArgs e)
        {
            InitData();
        }

        /// <summary>
        /// Set database connection based on Applicaiton configuration.
        /// </summary>
        protected void InitData()
        {
            InitData oInitData = new InitData();
            txtDataSource.Text = oInitData.sGetDataSource();
            txtDatabase.Text = oInitData.sGetDatabase();
            txtUserId.Text = oInitData.sGetUserID();
            txtPassword.Text = oInitData.sGetPassword();
            if (UserData.isSuperUser)
                txtPassword.UseSystemPasswordChar = false;
        }

        /// <summary>
        /// Get Database name.
        /// </summary>
        /// <returns>string : DatabaseName</returns>
        protected string sGetDatabase()
        {
            return txtDatabase.Text.Trim();
        }

        /// <summary>
        /// Get DataSource for database connection.
        /// </summary>
        /// <returns>string : DataSource</returns>
        protected string sGetDataSource()
        {
            return txtDataSource.Text.Trim();
        }

        /// <summary>
        /// Get UserID to connect to database.
        /// </summary>
        /// <returns>string : User ID</returns>
        protected string sGetUserID()
        {
            return txtUserId.Text.Trim();
        }

        /// <summary>
        /// Get Password to connect to database.
        /// </summary>
        /// <returns>string : Password</returns>
        protected string sGetPassword()
        {
            return txtPassword.Text.Trim();
        }

        /// <summary>
        /// Test if connection established successfully with current configuration.
        /// </summary>
        private void btnTest_Click(object sender, EventArgs e)
        {
            if (isValid())
                isCanConnect();
        }

        /// <summary>
        /// Determine if all user's input is valid or not.
        /// </summary>
        /// <returns>boolean : true if valid, else false</returns>
        protected bool isValid()
        {
            if (sGetDataSource().Length == 0) // Not null
            {
                CommonClass.doShowErrorMessage(CommonMessage.sErrPrimeDbDatabaseSource, txtDataSource, ObjectType.TEXTBOX, ErrorType.ERROR);
                return false;
            }
            else
                if (sGetUserID().Length == 0) // Not null
                {
                    CommonClass.doShowErrorMessage(CommonMessage.sErrPrimeDbUserIDDB, txtUserId, ObjectType.TEXTBOX, ErrorType.ERROR);
                    return false;
                }
                else
                    if (sGetPassword().Length == 0) // Not null
                    {
                        CommonClass.doShowErrorMessage(CommonMessage.sErrPrimeDbPasswordDB, txtPassword, ObjectType.TEXTBOX, ErrorType.ERROR);
                        return false;
                    }
            return true;
        }

        /// <summary>
        /// Determines wether connection to database successfully opened.
        /// </summary>
        /// <returns></returns>
        protected bool isCanConnect()
        {
            try
            {
                new SqlConnection(sGetConnString()).Open();
                MessageBox.Show(CommonMessage.sPrimeDbConnSuccess);
                return true;
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Get the connection string.
        /// </summary>
        /// <returns>string : Connection String</returns>
        protected string sGetConnString()
        {
            return CommonClass.sConnString(sGetDataSource(), sGetDatabase(), sGetUserID(), sGetPassword());
        }

        /// <summary>
        /// Save connection string for the application's configuration and change current connection.
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValid() && isCanConnect())
            {
                SaveData();
                ChangeConnection();
                CommonClass.InputLog(oSqlConn, "", string.IsNullOrEmpty(UserData.sUserID) ? "" : UserData.sUserID, "", CommonMessage.sUpdateDBPrimaryConn, "");
                this.Close();
            }
            else this.DialogResult = DialogResult.None;
        }

        /// <summary>
        /// Save current configuration to application's configuration
        /// </summary>
        protected void SaveData()
        {
            InitData oInitData = new InitData();
            //oInitData.doWriteInitData(sGetDataSource(), sGetDatabase(), sGetUserID(), sGetPassword());
        }

        /// <summary>
        /// Change current application's connection to the new connection string.
        /// </summary>
        protected void ChangeConnection()
        {
            oSqlConn = new SqlConnection(sGetConnString());
        }

        /// <summary>
        /// Close the form.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gbBtnSaveCancel_Enter(object sender, EventArgs e)
        {

        }

        private void gbConnection_Enter(object sender, EventArgs e)
        {

        }

        private void FrmDatabasePrimaryConnection_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}