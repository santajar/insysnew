using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmAutoInit : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Form to Set Auto Init's Parameters
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection String to database</param>
        public FrmAutoInit(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            oSqlConn = _oSqlConn;
        }

        private void FrmAutoInit_Load(object sender, EventArgs e)
        {
            LoadData();
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        /// <summary>
        /// Load Auto Init's parameter data from database to be shown in form
        /// </summary>
        protected void LoadData()
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAutoInitBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sInitTimeOut", SqlDbType.VarChar).Value = "AutoInitTimeOut";
                    oSqlCmd.Parameters.Add("@sMaxConn", SqlDbType.VarChar).Value = "AutoInitMaxConn";

                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                        {
                            txtInitTimeOut.Text = oRead["InitTimeOut"].ToString();
                            txtMaxConn.Text = oRead["MaxConn"].ToString();
                        }
                        oRead.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValid())
                if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                    if (isCanSaveData())
                        this.Close();
        }

        /// <summary>
        /// Determine whether data inputed by user valid or not
        /// </summary>
        /// <returns>boolean : true if data valid</returns>
        protected bool isValid()
        {
            bool isValid = false;
            string sErrMsg = "";

            if (string.IsNullOrEmpty(sGetInitTimeOut()))
                sErrMsg = string.Format("{0} {1} {0}", "Init TimeOut", CommonMessage.sErrorEmpty);
            else if (string.IsNullOrEmpty(sGetMaxConn()))
                sErrMsg = string.Format("{0} {1} {0}", "Maximum Connection", CommonMessage.sErrorEmpty);
            else if (!CommonClass.isFormatValid(sGetInitTimeOut(), "N"))
                sErrMsg = string.Format("{0} {1}", "Init TimeOut", CommonMessage.sErrorFormat);
            else if (!CommonClass.isFormatValid(sGetMaxConn(), "N"))
                sErrMsg = string.Format("{0} {1}", "Maximum Connection", CommonMessage.sErrorFormat);
            else
                isValid = true;

            if (!isValid) CommonClass.doWriteErrorFile(sErrMsg);
            return isValid;
        }

        /// <summary>
        /// Determine whether data successfully saved into database or not
        /// </summary>
        /// <returns>boolean : true if successfull</returns>
        protected bool isCanSaveData()
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAutoInitUpdate, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sInitTimeOut", SqlDbType.VarChar).Value = "AutoInitTimeOut";
                    oSqlCmd.Parameters.Add("@sMaxConn", SqlDbType.VarChar).Value = "AutoInitMaxConn";
                    oSqlCmd.Parameters.Add("@sInitTimeOutFlag", SqlDbType.VarChar).Value = sGetInitTimeOut();
                    oSqlCmd.Parameters.Add("@sMaxConnFlag", SqlDbType.VarChar).Value = sGetMaxConn();

                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    //oSqlCmd.ExecuteNonQuery();
                    string sLogDetail = "";
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        while (oRead.Read())
                        {
                            sLogDetail += string.Format("{0} : {1} --> {2} \n", oRead["ItemName"].ToString(),
                                                oRead["OldValue"].ToString(), oRead["NewValue"].ToString());
                        }
                        oRead.Close();
                    }

                    if (string.IsNullOrEmpty(sLogDetail))
                        sLogDetail = "No Changes Made";

                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", "Update Auto Init Parameters", sLogDetail);
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Get Auto Init Time Out Value
        /// </summary>
        /// <returns>string : Time Out</returns>
        protected string sGetInitTimeOut()
        {
            return txtInitTimeOut.Text.Trim();
        }

        /// <summary>
        /// Get Auto Init Maximum Connections 
        /// </summary>
        /// <returns>string : Maximum connections</returns>
        protected string sGetMaxConn()
        {
            return txtMaxConn.Text.Trim();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}