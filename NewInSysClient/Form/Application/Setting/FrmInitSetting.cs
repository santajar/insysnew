using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO.Ports;
using InSysClass;

namespace InSys
{
    public partial class FrmInitSetting : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Form to set Connection string's parameters value
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connecion string to database</param>
        public FrmInitSetting(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();

            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        private void FrmInitSetting_Load(object sender, EventArgs e)
        {
            LoadComboPort();
            LoadData();
            LoadDataConfig();
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValid())
                if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                    if (isCanSaveData() && isCanSaveDataConfig())
                        this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region "Function"

        /// <summary>
        /// Load data from database then show it in form
        /// </summary>
        protected void LoadData()
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPInitConnBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sMaxConn", SqlDbType.VarChar).Value = txtMaxConn.Tag.ToString();
                    oSqlCmd.Parameters.Add("@sInitPort", SqlDbType.VarChar).Value = txtInitPort.Tag.ToString();
                    oSqlCmd.Parameters.Add("@sSoftwarePort", SqlDbType.VarChar).Value = txtSoftwarePort.Tag.ToString();

                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn.Close();
                        oSqlConn.Open();
                    }

                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                        {
                            txtMaxConn.Text = oRead[txtMaxConn.Tag.ToString()].ToString();
                            txtInitPort.Text = oRead[txtInitPort.Tag.ToString()].ToString();
                            txtSoftwarePort.Text = oRead[txtSoftwarePort.Tag.ToString()].ToString();
                        }

                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        protected void LoadDataConfig()
        {
            InitConsoleConnType oConfigInit = new InitConsoleConnType(Application.StartupPath);
            chkEnableTcpIp.Checked = oConfigInit.IsConnTcpIpActive();

            chkEnableModem.Checked = oConfigInit.IsConnModemActive();
            cmbModem1.Text = oConfigInit.Modem1;
            cmbModem2.Text = oConfigInit.Modem2;
            cmbModem3.Text = oConfigInit.Modem3;
            cmbModem4.Text = oConfigInit.Modem4;
            cmbModem5.Text = oConfigInit.Modem5;

            chkEnableSerial.Checked = oConfigInit.IsConnSerialActive();
            cmbSerial.Text = oConfigInit.SerialPort;
            cmbParity.Text = oConfigInit.SerialParity;
            cmbBaudRate.Text = oConfigInit.SerialBaudRate.ToString();
            cmbDataBit.Text = oConfigInit.SerialDataBit.ToString();
            cmbStopBit.Text = oConfigInit.SerialStopBit.ToString();
        }

        protected void LoadComboPort()
        {
            string[] sArrPort = sArrGetAvailablePort();
            cmbModem1.Items.AddRange(sArrPort);
            cmbModem2.Items.AddRange(sArrPort);
            cmbModem3.Items.AddRange(sArrPort);
            cmbModem4.Items.AddRange(sArrPort);
            cmbModem5.Items.AddRange(sArrPort);
            cmbSerial.Items.AddRange(sArrPort);
        }

        protected string[] sArrGetAvailablePort()
        {
            string[] sArrPortName = SerialPort.GetPortNames();
            return sArrPortName;
        }

        /// <summary>
        /// Get Initiation Maximum Connections value
        /// </summary>
        /// <returns>string : Init Maximum Connections</returns>
        protected string sGetInitMaxConn()
        {
            return txtMaxConn.Text.Trim();
        }

        /// <summary>
        /// Get Port Init Value
        /// </summary>
        /// <returns>string : Port Init</returns>
        protected string sGetPortInit()
        {
            return txtInitPort.Text.Trim();
        }

        /// <summary>
        /// Get Software Port value
        /// </summary>
        /// <returns>string : Software Port</returns>
        protected string sGetPortSoftware()
        {
            return txtSoftwarePort.Text.Trim();
        }

        /// <summary>
        /// Determine whether user input data valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool isValid()
        {
            bool isValid = false;
            string sErrMsg = "";

            if (string.IsNullOrEmpty(sGetInitMaxConn()))
                sErrMsg = string.Format("{0} {1} {0}", "Init Max Connection", CommonMessage.sErrorEmpty);
            else if (!CommonClass.isFormatValid(sGetInitMaxConn(), "N"))
                sErrMsg = string.Format("{0} {1}", "Init Max Connection", CommonMessage.sErrorFormat);
            else if (string.IsNullOrEmpty(sGetPortInit()))
                sErrMsg = string.Format("{0} {1} {0}", "Init Port", CommonMessage.sErrorEmpty);
            else if (!CommonClass.isFormatValid(sGetPortInit(), "N"))
                sErrMsg = string.Format("{0} {1}", "Init Port", CommonMessage.sErrorFormat);
            else if (string.IsNullOrEmpty(sGetPortSoftware()))
                sErrMsg = string.Format("{0} {1} {0}", "Init Software", CommonMessage.sErrorEmpty);
            else if (!CommonClass.isFormatValid(sGetPortSoftware(), "N"))
                sErrMsg = string.Format("{0} {1}", "Init Software", CommonMessage.sErrorFormat);
            else if (chkEnableModem.Checked &&
                string.IsNullOrEmpty(cmbModem1.Text) && string.IsNullOrEmpty(cmbModem2.Text) && string.IsNullOrEmpty(cmbModem3.Text) &&
                string.IsNullOrEmpty(cmbModem4.Text) && string.IsNullOrEmpty(cmbModem5.Text))
                sErrMsg = string.Format("{0} {1}", "Init Modem Port", CommonMessage.sErrorEmpty);
            else if (chkEnableSerial.Checked && string.IsNullOrEmpty(cmbSerial.Text))
                sErrMsg = string.Format("{0} {1}", "Init Serial Port", CommonMessage.sErrorEmpty);
            else if (chkEnableSerial.Checked && string.IsNullOrEmpty(cmbParity.Text))
                sErrMsg = string.Format("{0} {1}", "Init Serial Parity", CommonMessage.sErrorEmpty);
            else if (chkEnableSerial.Checked && string.IsNullOrEmpty(cmbStopBit.Text))
                sErrMsg = string.Format("{0} {1}", "Init Serial StopBit", CommonMessage.sErrorEmpty);
            else if (chkEnableSerial.Checked && !CommonClass.isFormatValid(cmbStopBit.Text, "N"))
                sErrMsg = string.Format("{0} {1}", "Init Serial StopBit", CommonMessage.sErrorFormat);
            else if (chkEnableSerial.Checked && string.IsNullOrEmpty(cmbDataBit.Text))
                sErrMsg = string.Format("{0} {1}", "Init Serial DataBit", CommonMessage.sErrorEmpty);
            else if (chkEnableSerial.Checked && !CommonClass.isFormatValid(cmbDataBit.Text, "N"))
                sErrMsg = string.Format("{0} {1}", "Init Serial DataBit", CommonMessage.sErrorFormat);
            else if (chkEnableSerial.Checked && string.IsNullOrEmpty(cmbBaudRate.Text))
                sErrMsg = string.Format("{0} {1}", "Init Serial BaudRate", CommonMessage.sErrorEmpty);
            else if (chkEnableSerial.Checked && !CommonClass.isFormatValid(cmbBaudRate.Text, "N"))
                sErrMsg = string.Format("{0} {1}", "Init Serial BaudRate", CommonMessage.sErrorFormat);
            else isValid = true;

            if (!isValid) CommonClass.doWriteErrorFile(sErrMsg);

            return isValid;
        }

        /// <summary>
        /// Determine whether successfully saved data to database or not
        /// </summary>
        /// <returns>boolean : true if successfull</returns>
        protected bool isCanSaveData()
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPInitConnUpdate, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sMaxConn", SqlDbType.VarChar).Value = txtMaxConn.Tag.ToString();
                    oSqlCmd.Parameters.Add("@sInitPort", SqlDbType.VarChar).Value = txtInitPort.Tag.ToString();
                    oSqlCmd.Parameters.Add("@sSoftwarePort", SqlDbType.VarChar).Value = txtSoftwarePort.Tag.ToString();
                    oSqlCmd.Parameters.Add("@sMaxConnFlag", SqlDbType.VarChar).Value = sGetInitMaxConn();
                    oSqlCmd.Parameters.Add("@sInitPortFlag", SqlDbType.VarChar).Value = sGetPortInit();
                    oSqlCmd.Parameters.Add("@sSoftwarePortFlag", SqlDbType.VarChar).Value = sGetPortSoftware();

                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn.Close();
                        oSqlConn.Open();
                    }
                    string sLogDetail = "";
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        while (oRead.Read())
                        {
                            sLogDetail += string.Format("{0} : {1} --> {2} \n ", oRead["ItemName"].ToString(),
                                           oRead["OldValue"].ToString(), oRead["NewValue"].ToString());
                        }
                        oRead.Close();
                    }
                    if (string.IsNullOrEmpty(sLogDetail))
                        sLogDetail = "No Changes Made";

                    CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", "Update Initiation Settings", sLogDetail);
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return true;
        }

        protected bool isCanSaveDataConfig()
        {
            try
            {
                InitConsoleConnType oConfigInit = new InitConsoleConnType(Application.StartupPath);
                oConfigInit.SetTcpIpConn(chkEnableTcpIp.Checked);

                oConfigInit.SetModemConn(chkEnableModem.Checked);
                oConfigInit.Modem1 = cmbModem1.Text;
                oConfigInit.Modem2 = cmbModem2.Text;
                oConfigInit.Modem3 = cmbModem3.Text;
                oConfigInit.Modem4 = cmbModem4.Text;
                oConfigInit.Modem5 = cmbModem5.Text;

                oConfigInit.SetSerialConn(chkEnableSerial.Checked);
                oConfigInit.SerialPort = cmbSerial.Text;
                oConfigInit.SerialParity = cmbParity.Text;
                oConfigInit.SerialBaudRate = int.Parse(cmbBaudRate.Text);
                oConfigInit.SerialDataBit = int.Parse(cmbDataBit.Text);
                oConfigInit.SerialStopBit = float.Parse(cmbStopBit.Text);
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return true;
        }
        #endregion
    }
}