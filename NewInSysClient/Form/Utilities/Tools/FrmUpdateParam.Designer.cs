﻿namespace InSys
{
    partial class FrmUpdateParam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUpdateParam));
            this.rtbProgress = new System.Windows.Forms.RichTextBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.pbMoveData = new System.Windows.Forms.ProgressBar();
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbDbSource = new System.Windows.Forms.ComboBox();
            this.lblDbSource = new System.Windows.Forms.Label();
            this.gbFilter = new System.Windows.Forms.GroupBox();
            this.cmbValue = new System.Windows.Forms.ComboBox();
            this.cmbName = new System.Windows.Forms.ComboBox();
            this.txtNameFilter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNewValue = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbItemList = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbFormType = new System.Windows.Forms.ComboBox();
            this.bwUpdateParam = new System.ComponentModel.BackgroundWorker();
            this.gbButton.SuspendLayout();
            this.gbSource.SuspendLayout();
            this.gbFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbProgress
            // 
            this.rtbProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbProgress.Location = new System.Drawing.Point(16, 260);
            this.rtbProgress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rtbProgress.Name = "rtbProgress";
            this.rtbProgress.ReadOnly = true;
            this.rtbProgress.Size = new System.Drawing.Size(857, 154);
            this.rtbProgress.TabIndex = 14;
            this.rtbProgress.Text = "";
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnReset);
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Controls.Add(this.btnExecute);
            this.gbButton.Location = new System.Drawing.Point(17, 458);
            this.gbButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Name = "gbButton";
            this.gbButton.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Size = new System.Drawing.Size(856, 50);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Location = new System.Drawing.Point(568, 15);
            this.btnReset.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(133, 28);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(715, 15);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(133, 28);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnExecute
            // 
            this.btnExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExecute.Location = new System.Drawing.Point(419, 15);
            this.btnExecute.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(133, 28);
            this.btnExecute.TabIndex = 0;
            this.btnExecute.Text = "&Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // pbMoveData
            // 
            this.pbMoveData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbMoveData.Location = new System.Drawing.Point(17, 422);
            this.pbMoveData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbMoveData.Name = "pbMoveData";
            this.pbMoveData.Size = new System.Drawing.Size(856, 28);
            this.pbMoveData.TabIndex = 11;
            // 
            // gbSource
            // 
            this.gbSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSource.Controls.Add(this.btnBrowse);
            this.gbSource.Controls.Add(this.txtFilename);
            this.gbSource.Controls.Add(this.label1);
            this.gbSource.Controls.Add(this.cmbDbSource);
            this.gbSource.Controls.Add(this.lblDbSource);
            this.gbSource.Location = new System.Drawing.Point(16, 15);
            this.gbSource.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbSource.Name = "gbSource";
            this.gbSource.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbSource.Size = new System.Drawing.Size(859, 75);
            this.gbSource.TabIndex = 0;
            this.gbSource.TabStop = false;
            this.gbSource.Text = "Source";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(749, 30);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(100, 25);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtFilename
            // 
            this.txtFilename.Location = new System.Drawing.Point(500, 30);
            this.txtFilename.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(212, 22);
            this.txtFilename.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(392, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Filename";
            // 
            // cmbDbSource
            // 
            this.cmbDbSource.FormattingEnabled = true;
            this.cmbDbSource.Location = new System.Drawing.Point(129, 30);
            this.cmbDbSource.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbDbSource.Name = "cmbDbSource";
            this.cmbDbSource.Size = new System.Drawing.Size(212, 24);
            this.cmbDbSource.TabIndex = 1;
            this.cmbDbSource.SelectedIndexChanged += new System.EventHandler(this.cmbDbSource_SelectedIndexChanged);
            // 
            // lblDbSource
            // 
            this.lblDbSource.AutoSize = true;
            this.lblDbSource.Location = new System.Drawing.Point(21, 33);
            this.lblDbSource.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDbSource.Name = "lblDbSource";
            this.lblDbSource.Size = new System.Drawing.Size(69, 17);
            this.lblDbSource.TabIndex = 0;
            this.lblDbSource.Text = "Database";
            // 
            // gbFilter
            // 
            this.gbFilter.Controls.Add(this.cmbValue);
            this.gbFilter.Controls.Add(this.cmbName);
            this.gbFilter.Controls.Add(this.txtNameFilter);
            this.gbFilter.Controls.Add(this.label5);
            this.gbFilter.Controls.Add(this.txtNewValue);
            this.gbFilter.Controls.Add(this.label4);
            this.gbFilter.Controls.Add(this.cmbItemList);
            this.gbFilter.Controls.Add(this.label3);
            this.gbFilter.Controls.Add(this.label2);
            this.gbFilter.Controls.Add(this.cmbFormType);
            this.gbFilter.Location = new System.Drawing.Point(16, 97);
            this.gbFilter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbFilter.Name = "gbFilter";
            this.gbFilter.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbFilter.Size = new System.Drawing.Size(859, 155);
            this.gbFilter.TabIndex = 1;
            this.gbFilter.TabStop = false;
            this.gbFilter.Text = "Filter";
            // 
            // cmbValue
            // 
            this.cmbValue.FormattingEnabled = true;
            this.cmbValue.Location = new System.Drawing.Point(116, 122);
            this.cmbValue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbValue.Name = "cmbValue";
            this.cmbValue.Size = new System.Drawing.Size(160, 24);
            this.cmbValue.TabIndex = 14;
            // 
            // cmbName
            // 
            this.cmbName.FormattingEnabled = true;
            this.cmbName.Location = new System.Drawing.Point(301, 119);
            this.cmbName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbName.Name = "cmbName";
            this.cmbName.Size = new System.Drawing.Size(160, 24);
            this.cmbName.TabIndex = 13;
            // 
            // txtNameFilter
            // 
            this.txtNameFilter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNameFilter.Location = new System.Drawing.Point(116, 57);
            this.txtNameFilter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNameFilter.Name = "txtNameFilter";
            this.txtNameFilter.Size = new System.Drawing.Size(211, 22);
            this.txtNameFilter.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 59);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Name";
            // 
            // txtNewValue
            // 
            this.txtNewValue.Location = new System.Drawing.Point(116, 87);
            this.txtNewValue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNewValue.Name = "txtNewValue";
            this.txtNewValue.Size = new System.Drawing.Size(732, 22);
            this.txtNewValue.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 91);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "New Value";
            // 
            // cmbItemList
            // 
            this.cmbItemList.FormattingEnabled = true;
            this.cmbItemList.Location = new System.Drawing.Point(379, 23);
            this.cmbItemList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbItemList.Name = "cmbItemList";
            this.cmbItemList.Size = new System.Drawing.Size(205, 24);
            this.cmbItemList.TabIndex = 2;
            this.cmbItemList.SelectedIndexChanged += new System.EventHandler(this.cmbItemList_SelectedIndexChanged);
            this.cmbItemList.Leave += new System.EventHandler(this.cmbItemList_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(297, 28);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Parameter";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Table";
            // 
            // cmbFormType
            // 
            this.cmbFormType.FormattingEnabled = true;
            this.cmbFormType.Items.AddRange(new object[] {
            "Terminal",
            "Acquirer",
            "Issuer"});
            this.cmbFormType.Location = new System.Drawing.Point(116, 23);
            this.cmbFormType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbFormType.Name = "cmbFormType";
            this.cmbFormType.Size = new System.Drawing.Size(160, 24);
            this.cmbFormType.TabIndex = 1;
            this.cmbFormType.SelectedIndexChanged += new System.EventHandler(this.cmbFormType_SelectedIndexChanged);
            // 
            // bwUpdateParam
            // 
            this.bwUpdateParam.WorkerReportsProgress = true;
            this.bwUpdateParam.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwUpdateParam_DoWork);
            this.bwUpdateParam.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwUpdateParam_ProgressChanged);
            this.bwUpdateParam.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwUpdateParam_RunWorkerCompleted);
            // 
            // FrmUpdateParam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 514);
            this.Controls.Add(this.gbFilter);
            this.Controls.Add(this.rtbProgress);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.pbMoveData);
            this.Controls.Add(this.gbSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUpdateParam";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Parameter";
            this.Load += new System.EventHandler(this.FrmUpdateParam_Load);
            this.gbButton.ResumeLayout(false);
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.gbFilter.ResumeLayout(false);
            this.gbFilter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbProgress;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.ProgressBar pbMoveData;
        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbDbSource;
        private System.Windows.Forms.Label lblDbSource;
        private System.Windows.Forms.GroupBox gbFilter;
        private System.Windows.Forms.TextBox txtNameFilter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNewValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbItemList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbFormType;
        private System.ComponentModel.BackgroundWorker bwUpdateParam;
        private System.Windows.Forms.ComboBox cmbValue;
        private System.Windows.Forms.ComboBox cmbName;
    }
}