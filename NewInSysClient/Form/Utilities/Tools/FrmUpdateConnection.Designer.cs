﻿namespace InSys
{
    partial class FrmUpdateConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUpdateConnection));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkFlazzJakrta = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDialSecondary = new System.Windows.Forms.TextBox();
            this.txtDialPrimary = new System.Windows.Forms.TextBox();
            this.cmbConnSecondary = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbConnPrimay = new System.Windows.Forms.ComboBox();
            this.cmbDb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnDetails = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtSingleTID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkAllowRemote = new System.Windows.Forms.CheckBox();
            this.DE040 = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbRD = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.AA011 = new System.Windows.Forms.CheckBox();
            this.AA048 = new System.Windows.Forms.CheckBox();
            this.AA010 = new System.Windows.Forms.CheckBox();
            this.AA040 = new System.Windows.Forms.CheckBox();
            this.AA007 = new System.Windows.Forms.CheckBox();
            this.AA047 = new System.Windows.Forms.CheckBox();
            this.AA006 = new System.Windows.Forms.CheckBox();
            this.AA039 = new System.Windows.Forms.CheckBox();
            this.AA046 = new System.Windows.Forms.CheckBox();
            this.AA038 = new System.Windows.Forms.CheckBox();
            this.AA045 = new System.Windows.Forms.CheckBox();
            this.AA037 = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkFlazzJakrta);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtDialSecondary);
            this.groupBox1.Controls.Add(this.txtDialPrimary);
            this.groupBox1.Controls.Add(this.cmbConnSecondary);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmbConnPrimay);
            this.groupBox1.Controls.Add(this.cmbDb);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(28, 1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(417, 214);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Target";
            // 
            // chkFlazzJakrta
            // 
            this.chkFlazzJakrta.AutoSize = true;
            this.chkFlazzJakrta.Location = new System.Drawing.Point(23, 139);
            this.chkFlazzJakrta.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkFlazzJakrta.Name = "chkFlazzJakrta";
            this.chkFlazzJakrta.Size = new System.Drawing.Size(142, 21);
            this.chkFlazzJakrta.TabIndex = 10;
            this.chkFlazzJakrta.Text = "Dial Flazz Jakarta";
            this.chkFlazzJakrta.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 188);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Input Dial Secondary";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 164);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Input Dial Primary";
            // 
            // txtDialSecondary
            // 
            this.txtDialSecondary.Location = new System.Drawing.Point(216, 182);
            this.txtDialSecondary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDialSecondary.Name = "txtDialSecondary";
            this.txtDialSecondary.Size = new System.Drawing.Size(164, 22);
            this.txtDialSecondary.TabIndex = 7;
            // 
            // txtDialPrimary
            // 
            this.txtDialPrimary.Location = new System.Drawing.Point(216, 150);
            this.txtDialPrimary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDialPrimary.Name = "txtDialPrimary";
            this.txtDialPrimary.Size = new System.Drawing.Size(164, 22);
            this.txtDialPrimary.TabIndex = 6;
            // 
            // cmbConnSecondary
            // 
            this.cmbConnSecondary.FormattingEnabled = true;
            this.cmbConnSecondary.Location = new System.Drawing.Point(216, 94);
            this.cmbConnSecondary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbConnSecondary.Name = "cmbConnSecondary";
            this.cmbConnSecondary.Size = new System.Drawing.Size(160, 24);
            this.cmbConnSecondary.TabIndex = 5;
            this.cmbConnSecondary.Click += new System.EventHandler(this.cmbConnSecondary_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(189, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Select GPRS/ETH Seconday";
            // 
            // cmbConnPrimay
            // 
            this.cmbConnPrimay.FormattingEnabled = true;
            this.cmbConnPrimay.Location = new System.Drawing.Point(216, 62);
            this.cmbConnPrimay.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbConnPrimay.Name = "cmbConnPrimay";
            this.cmbConnPrimay.Size = new System.Drawing.Size(160, 24);
            this.cmbConnPrimay.TabIndex = 3;
            this.cmbConnPrimay.Click += new System.EventHandler(this.cmbConnPrimay_Click);
            // 
            // cmbDb
            // 
            this.cmbDb.FormattingEnabled = true;
            this.cmbDb.Location = new System.Drawing.Point(216, 30);
            this.cmbDb.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbDb.Name = "cmbDb";
            this.cmbDb.Size = new System.Drawing.Size(160, 24);
            this.cmbDb.TabIndex = 2;
            this.cmbDb.SelectedIndexChanged += new System.EventHandler(this.cmbDb_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Select GPRS/ETH Primary";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Database Name";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.progressBar1);
            this.groupBox2.Controls.Add(this.btnDetails);
            this.groupBox2.Controls.Add(this.btnCancel);
            this.groupBox2.Controls.Add(this.btnExecute);
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(28, 217);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(417, 502);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Destination Provider";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(8, 338);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(383, 28);
            this.progressBar1.TabIndex = 9;
            // 
            // btnDetails
            // 
            this.btnDetails.Location = new System.Drawing.Point(276, 462);
            this.btnDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.Size = new System.Drawing.Size(100, 33);
            this.btnDetails.TabIndex = 8;
            this.btnDetails.Text = "DETAILS";
            this.btnDetails.UseVisualStyleBackColor = true;
            this.btnDetails.Click += new System.EventHandler(this.btnDetails_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(141, 462);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 33);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(8, 462);
            this.btnExecute.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(93, 33);
            this.btnExecute.TabIndex = 6;
            this.btnExecute.Text = "EXECUTE";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtSingleTID);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.btnBrowse);
            this.groupBox6.Location = new System.Drawing.Point(8, 369);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Size = new System.Drawing.Size(383, 91);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Browse File";
            // 
            // txtSingleTID
            // 
            this.txtSingleTID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSingleTID.Location = new System.Drawing.Point(8, 59);
            this.txtSingleTID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSingleTID.Name = "txtSingleTID";
            this.txtSingleTID.Size = new System.Drawing.Size(132, 22);
            this.txtSingleTID.TabIndex = 2;
            this.txtSingleTID.TextChanged += new System.EventHandler(this.txtSingleTID_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(129, 33);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "label6";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(11, 25);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(115, 33);
            this.btnBrowse.TabIndex = 0;
            this.btnBrowse.Text = "BROWSE";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkAllowRemote);
            this.groupBox3.Controls.Add(this.DE040);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.cmbRD);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.AA011);
            this.groupBox3.Controls.Add(this.AA048);
            this.groupBox3.Controls.Add(this.AA010);
            this.groupBox3.Controls.Add(this.AA040);
            this.groupBox3.Controls.Add(this.AA007);
            this.groupBox3.Controls.Add(this.AA047);
            this.groupBox3.Controls.Add(this.AA006);
            this.groupBox3.Controls.Add(this.AA039);
            this.groupBox3.Controls.Add(this.AA046);
            this.groupBox3.Controls.Add(this.AA038);
            this.groupBox3.Controls.Add(this.AA045);
            this.groupBox3.Controls.Add(this.AA037);
            this.groupBox3.Location = new System.Drawing.Point(8, 18);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(383, 313);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // chkAllowRemote
            // 
            this.chkAllowRemote.AutoSize = true;
            this.chkAllowRemote.Location = new System.Drawing.Point(208, 238);
            this.chkAllowRemote.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAllowRemote.Name = "chkAllowRemote";
            this.chkAllowRemote.Size = new System.Drawing.Size(115, 21);
            this.chkAllowRemote.TabIndex = 13;
            this.chkAllowRemote.Text = "Allow Remote";
            this.chkAllowRemote.UseVisualStyleBackColor = true;
            // 
            // DE040
            // 
            this.DE040.AutoSize = true;
            this.DE040.Location = new System.Drawing.Point(8, 238);
            this.DE040.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DE040.Name = "DE040";
            this.DE040.Size = new System.Drawing.Size(186, 21);
            this.DE040.TabIndex = 12;
            this.DE040.Text = "Remote Download Name";
            this.DE040.UseVisualStyleBackColor = true;
            this.DE040.Click += new System.EventHandler(this.DE040_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 150);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 18);
            this.label9.TabIndex = 6;
            this.label9.Text = "SET DIAL";
            // 
            // cmbRD
            // 
            this.cmbRD.FormattingEnabled = true;
            this.cmbRD.Location = new System.Drawing.Point(8, 266);
            this.cmbRD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbRD.Name = "cmbRD";
            this.cmbRD.Size = new System.Drawing.Size(324, 24);
            this.cmbRD.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(13, 78);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 18);
            this.label8.TabIndex = 5;
            this.label8.Text = "SET ETHERNET";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 5);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 18);
            this.label7.TabIndex = 3;
            this.label7.Text = "SET GPRS";
            // 
            // AA011
            // 
            this.AA011.AutoSize = true;
            this.AA011.Location = new System.Drawing.Point(208, 192);
            this.AA011.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA011.Name = "AA011";
            this.AA011.Size = new System.Drawing.Size(146, 21);
            this.AA011.TabIndex = 3;
            this.AA011.Text = "Stl Dial Secondary";
            this.AA011.UseVisualStyleBackColor = true;
            // 
            // AA048
            // 
            this.AA048.AutoSize = true;
            this.AA048.Location = new System.Drawing.Point(208, 119);
            this.AA048.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA048.Name = "AA048";
            this.AA048.Size = new System.Drawing.Size(150, 21);
            this.AA048.TabIndex = 3;
            this.AA048.Text = "Stl ETH Secondary";
            this.AA048.UseVisualStyleBackColor = true;
            // 
            // AA010
            // 
            this.AA010.AutoSize = true;
            this.AA010.Location = new System.Drawing.Point(208, 172);
            this.AA010.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA010.Name = "AA010";
            this.AA010.Size = new System.Drawing.Size(126, 21);
            this.AA010.TabIndex = 2;
            this.AA010.Text = "Stl Dial Primary";
            this.AA010.UseVisualStyleBackColor = true;
            // 
            // AA040
            // 
            this.AA040.AutoSize = true;
            this.AA040.Location = new System.Drawing.Point(208, 52);
            this.AA040.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA040.Name = "AA040";
            this.AA040.Size = new System.Drawing.Size(161, 21);
            this.AA040.TabIndex = 3;
            this.AA040.Text = "Stl GPRS Secondary";
            this.AA040.UseVisualStyleBackColor = true;
            // 
            // AA007
            // 
            this.AA007.AutoSize = true;
            this.AA007.Location = new System.Drawing.Point(15, 192);
            this.AA007.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA007.Name = "AA007";
            this.AA007.Size = new System.Drawing.Size(153, 21);
            this.AA007.TabIndex = 1;
            this.AA007.Text = "Txn Dial Secondary";
            this.AA007.UseVisualStyleBackColor = true;
            // 
            // AA047
            // 
            this.AA047.AutoSize = true;
            this.AA047.Location = new System.Drawing.Point(208, 100);
            this.AA047.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA047.Name = "AA047";
            this.AA047.Size = new System.Drawing.Size(130, 21);
            this.AA047.TabIndex = 2;
            this.AA047.Text = "Stl ETH Primary";
            this.AA047.UseVisualStyleBackColor = true;
            // 
            // AA006
            // 
            this.AA006.AutoSize = true;
            this.AA006.Location = new System.Drawing.Point(15, 172);
            this.AA006.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA006.Name = "AA006";
            this.AA006.Size = new System.Drawing.Size(133, 21);
            this.AA006.TabIndex = 0;
            this.AA006.Text = "Txn Dial Primary";
            this.AA006.UseVisualStyleBackColor = true;
            // 
            // AA039
            // 
            this.AA039.AutoSize = true;
            this.AA039.Location = new System.Drawing.Point(208, 31);
            this.AA039.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA039.Name = "AA039";
            this.AA039.Size = new System.Drawing.Size(141, 21);
            this.AA039.TabIndex = 2;
            this.AA039.Text = "Stl GPRS Primary";
            this.AA039.UseVisualStyleBackColor = true;
            // 
            // AA046
            // 
            this.AA046.AutoSize = true;
            this.AA046.Location = new System.Drawing.Point(15, 119);
            this.AA046.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA046.Name = "AA046";
            this.AA046.Size = new System.Drawing.Size(157, 21);
            this.AA046.TabIndex = 1;
            this.AA046.Text = "Txn ETH Secondary";
            this.AA046.UseVisualStyleBackColor = true;
            // 
            // AA038
            // 
            this.AA038.AutoSize = true;
            this.AA038.Location = new System.Drawing.Point(15, 52);
            this.AA038.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA038.Name = "AA038";
            this.AA038.Size = new System.Drawing.Size(168, 21);
            this.AA038.TabIndex = 1;
            this.AA038.Text = "Txn GPRS Secondary";
            this.AA038.UseVisualStyleBackColor = true;
            // 
            // AA045
            // 
            this.AA045.AutoSize = true;
            this.AA045.Location = new System.Drawing.Point(15, 100);
            this.AA045.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA045.Name = "AA045";
            this.AA045.Size = new System.Drawing.Size(137, 21);
            this.AA045.TabIndex = 0;
            this.AA045.Text = "Txn ETH Primary";
            this.AA045.UseVisualStyleBackColor = true;
            // 
            // AA037
            // 
            this.AA037.AutoSize = true;
            this.AA037.Location = new System.Drawing.Point(15, 31);
            this.AA037.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AA037.Name = "AA037";
            this.AA037.Size = new System.Drawing.Size(148, 21);
            this.AA037.TabIndex = 0;
            this.AA037.Text = "Txn GPRS Primary";
            this.AA037.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(453, 15);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(189, 572);
            this.dataGridView1.TabIndex = 2;
            // 
            // FrmUpdateConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 715);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "FrmUpdateConnection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Connection Profile";
            this.Load += new System.EventHandler(this.Fmain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbDb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbConnPrimay;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnDetails;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox cmbConnSecondary;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.CheckBox AA011;
        public System.Windows.Forms.CheckBox AA010;
        public System.Windows.Forms.CheckBox AA007;
        public System.Windows.Forms.CheckBox AA006;
        public System.Windows.Forms.CheckBox AA048;
        public System.Windows.Forms.CheckBox AA047;
        public System.Windows.Forms.CheckBox AA046;
        public System.Windows.Forms.CheckBox AA045;
        public System.Windows.Forms.CheckBox AA040;
        public System.Windows.Forms.CheckBox AA039;
        public System.Windows.Forms.CheckBox AA038;
        public System.Windows.Forms.CheckBox AA037;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDialSecondary;
        private System.Windows.Forms.TextBox txtDialPrimary;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkFlazzJakrta;
        public System.Windows.Forms.TextBox txtSingleTID;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ComboBox cmbRD;
        public System.Windows.Forms.CheckBox DE040;
        public System.Windows.Forms.CheckBox chkAllowRemote;
        //private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        //private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        //private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        //private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
    }
}