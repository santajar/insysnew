﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using InSysClass;
using System.Configuration;
using EmsXml = ipXML.EMS_XML;
using EmsProcess = ipXML.EMS_Process;
using Excel;

namespace InSys
{
    public partial class FrmQC2 : Form
    {
        string sSourceFile;
        string sResultFile;

        SqlConnection oConn = new SqlConnection();
        string sConnString = null;

        OpenFileDialog ofdBrowseFile = new OpenFileDialog();
        SaveFileDialog svdSaveFile = new SaveFileDialog();

        DataTable dtQC = new DataTable();
        DataTable dtEmsXmlMap = new DataTable();
        DataTable dtCompareResult = new DataTable();

        string[] arrsExclude = new string[] { "sofware_version", "software_version", "last_update", "last_init", "master", "onus", "purchase", "kcb", "bizz", "terminal_id_bizz", "merchant_id_bizz", "txn_primary_ph_bizz", "txn_secondary_ph_bizz", "terminal_id_promogemalto _j", "merchant_id_promo_j", "txn_primary_ph_promo_j", "txn_secondary_ph_promo_j", "stl_primary_phone_promo_j", "stl_secondary_ph_promo_j", "installment_Description_promo_j" };

        string[] arrsAdvanceFilter = new string[] { "flazz", "topup_tunai", "flazzperdana", "flazztopup", "flazzpayment" };

        public FrmQC2(SqlConnection _oConn, string suseriD)
        {
            oConn = _oConn;
            InitializeComponent();
        }

        private void frmQC2_Load(object sender, EventArgs e)
        {
            doInitialization();
        }

        private void doInitialization()
        {
            doInitateForm();
            // doInitiateConnection();
            doInitiateFileDialogFilter();
            dtEmsXmlMap = dtGetMapColumn();
        }

        private void btnSourceBrowse_Click(object sender, EventArgs e)
        {
            doBrowseFile();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sSourceFile) && svdSaveFile.ShowDialog() != DialogResult.Cancel)
            {
                doEnableForm(false);
                doInitializeResultTable();
                sResultFile = svdSaveFile.FileName;
                pgBar.Style = ProgressBarStyle.Marquee;
                bwWorker.RunWorkerAsync(svdSaveFile.FileName);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bwWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                bwWorker.WorkerReportsProgress = true;
                bwWorker.ReportProgress(100, "Start");
                string sError = null;
                foreach (DataRow rowQC in dtQC.Rows)
                {
                    DataRow rowResult = dtCompareResult.NewRow();
                    string sTerminalID = rowQC["Terminal_INIT"].ToString();
                    bool bSuccess = true;
                    string sStatus = null;
                    string sRemarks = null;

                    bwWorker.ReportProgress(100, string.Format("Processing {0}", sTerminalID));

                    EmsXml oEmsXmlResult = new EmsXml();
                    oEmsXmlResult = emsInquiryResult(sTerminalID, ref sError);
                    rowResult["Terminal_INIT"] = sTerminalID;
                    if (oEmsXmlResult.Data.ltTerminal.Count > 0)
                    {
                        EmsXml.DataClass.TerminalClass terminalTemp = new EmsXml.DataClass.TerminalClass();
                        terminalTemp = oEmsXmlResult.Data.ltTerminal[0];
                        if (terminalTemp.ColumnValueXML.Count > 0)
                            foreach (EmsXml.DataClass.TerminalClass.ColumnValue colValue in terminalTemp.ColumnValueXML)
                            {
                                string sColName = colValue.sColName;
                                string sColValue = string.IsNullOrEmpty(colValue.sColValue) ? "0" : colValue.sColValue;

                                bool isSkipChecking = false;
                                if (sColName.ToLower().Contains("flazz") && terminalTemp.sGetColumnValue("Flazz") == "0") isSkipChecking = true;
                                else if (sColName.ToLower().Contains("reguler") && terminalTemp.sGetColumnValue("Cicilan_Reguler") == "0") isSkipChecking = true;
                                else if (sColName.ToLower().Contains("promo") && terminalTemp.sGetColumnValue("Cicilan_Promo") == "0") isSkipChecking = true;
                                else if (sColName.ToLower().Contains("topup_tunai") && terminalTemp.sGetColumnValue("TopUP_Tunai") == "0") isSkipChecking = true;
                                else if (sColName.ToLower().Contains("amex") && terminalTemp.sGetColumnValue("Amex") == "0") isSkipChecking = true;
                                else if (sColName.ToLower().Contains("diners") && terminalTemp.sGetColumnValue("Diners") == "0") isSkipChecking = true;

                                if (!arrsExclude.Contains(sColName.ToLower()) && !isSkipChecking)
                                {

                                    string sValueQc = string.IsNullOrEmpty(rowQC[sColName].ToString()) ? "0" : rowQC[sColName].ToString();
                                    if (sValueQc != sColValue)
                                    {
                                        //if (((sValueQc == "0" || string.IsNullOrEmpty(sValueQc)) && sColValue == "1") ||
                                        //    (sValueQc == "1" && (sColValue == "0" || string.IsNullOrEmpty(sColValue)))
                                        //    )
                                        //{
                                        if (arrsAdvanceFilter.Contains(sColName.ToLower()))
                                        {
                                            string sValueXml = terminalTemp.sGetColumnValue("Terminal_ID_FLAZZBCA");
                                            int iOutput = -1;
                                            if (sValueXml.Length == 8)
                                                if (int.TryParse(sValueXml.Substring(2), out iOutput) && sValueQc == "0")
                                                {
                                                    bSuccess = false;
                                                    sRemarks = string.Format("{0}{1} '{2}' NOT MATCH '{3}'#", sRemarks, sColName, rowQC[sColName], sColValue);
                                                }
                                        }
                                        //}
                                        else
                                        {
                                            bSuccess = false;
                                            sRemarks = string.Format("{0}{1} '{2}' NOT MATCH '{3}'#", sRemarks, sColName, rowQC[sColName], sColValue);
                                        }
                                    }
                                }
                            }
                        else
                        {
                            bSuccess = false;
                            sRemarks = "Terminal_INIT NOT FOUND";
                        }
                    }
                    else
                    {
                        bSuccess = false;
                        sRemarks = "Terminal_INIT NOT FOUND";
                    }
                    if (!string.IsNullOrEmpty(sError))
                    {
                        bwWorker.ReportProgress(100, string.Format("Processing {0} FAILED. {1}", sTerminalID, sError));
                        sError = null;
                    }
                    sStatus = bSuccess ? "SUCCESS" : "ERROR";
                    rowResult["Status"] = sStatus;
                    rowResult["Remarks"] = sRemarks;
                    dtCompareResult.Rows.Add(rowResult);
                    //break;
                }
                bwWorker.ReportProgress(100, "DONE");
            }
            catch (Exception ex)
            {
                bwWorker.ReportProgress(100, "ERROR on QC " + ex.Message);
            }
        }

        private void bwWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            rtbConsole.Text += string.Format("{0:ddd, dd MMM yyyy, hh:mm:ss.fff tt}|{1}\n", DateTime.Now, e.UserState);
            rtbConsole.ScrollToCaret();
        }

        private void bwWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                doEnableForm(true);
                if (File.Exists(sSourceFile))
                    File.Delete(sSourceFile);
                doWriteExcel(sResultFile);
            }
            catch (Exception ex)
            {
                bwWorker.ReportProgress(100, "ERROR " + ex.Message);
            }
            finally
            {
                pgBar.Style = ProgressBarStyle.Blocks;
                pgBar.Value = 100;
            }
        }

        protected void doInitateForm()
        {
            //txtSourceFile.ReadOnly = true;
        }

        protected void doInitiateConnection()
        {
            try
            {
                sConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                oConn = new SqlConnection(sConnString);
                oConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected void doInitiateFileDialogFilter()
        {
            ofdBrowseFile.Filter = "Excel 97-2003 (*.xls)|*.xls|Excel 2007  (*.xlsx)|*.xlsx";
            //svdSaveFile.Filter = "Excel 2007  (*.xlsx)|*.xlsx";
            svdSaveFile.Filter = "CSV  (*.csv)|*.csv";
            svdSaveFile.OverwritePrompt = false;
        }

        protected void doInitializeResultTable()
        {
            dtCompareResult = new DataTable();
            //dtCompareResult.Columns.Add("Terminal_INIT", typeof(string));
            //dtCompareResult.Columns.Add("Status", typeof(string));
            //dtCompareResult.Columns.Add("Remarks", typeof(string));

            DataColumn colTerminal_INIT = new DataColumn("Terminal_INIT", typeof(string));
            DataColumn colStatus = new DataColumn("Status", typeof(string));
            DataColumn colRemarks = new DataColumn("Remarks", typeof(string));
            dtCompareResult.Columns.AddRange(new DataColumn[] { colTerminal_INIT, colStatus, colRemarks });
        }

        protected void doWriteExcel(string sFileName)
        {
            try
            {
                if (dtCompareResult != null)
                {
                    dtCompareResult.TableName = "Result";
                    //string sTemplateXLSX = Application.StartupPath + @"\TemplateXlsx.xlsx";

                    //File.Delete(sFileName);
                    //File.Copy(sTemplateXLSX, sFileName);

                    //NewExcelOleDb oXlsOle = new NewExcelOleDb(sFileName);

                    //oXlsOle.CreateWorkSheet(dtCompareResult);
                    //oXlsOle.InsertRow(dtCompareResult);
                    //oXlsOle.Dispose();                    
                    Csv.Write(dtCompareResult, sFileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //protected DataTable dtResultQC(string sFile)
        //{
        //    try
        //    {
        //        DataTable oTable = new DataTable();

        //        doUploadFileExcel();

        //        using (SqlCommand oComm = new SqlCommand(StoredProcedure.sCompareData, oConn))
        //        {
        //            oComm.CommandType = CommandType.StoredProcedure;
        //            //oComm.Parameters.Add("@sPath", SqlDbType.VarChar).Value = sFile;
        //            oComm.CommandTimeout = 0;
        //            SqlDataAdapter oAdapt = new SqlDataAdapter(oComm);

        //            oAdapt.Fill(oTable);

        //            return oTable;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return null;
        //    }
        //}

        protected void doCleanTable()
        {
            using (SqlCommand oComm = new SqlCommand(CommonSP.sSPCleanTableCompare, oConn))
            {
                oComm.CommandType = CommandType.StoredProcedure;
                //oComm.Parameters.Add("@sPath", SqlDbType.VarChar).Value = sFile;
                oComm.CommandTimeout = 0;
                oComm.ExecuteNonQuery();
            }
        }

        protected void doAddColumnNames(NewExcelOleDb oOleDb)
        {
            oOleDb.AddColumnName("[Product ID]");
            oOleDb.AddColumnName("[Merchant ID]");
            oOleDb.AddColumnName("[Merchant Name]");
            oOleDb.AddColumnName("[Alamat 1]");
            oOleDb.AddColumnName("[Alamat 2]");
            oOleDb.AddColumnName("[Flazz]");
            oOleDb.AddColumnName("[TopUp]");
            oOleDb.AddColumnName("Payment");
            oOleDb.AddColumnName("[BCA Card]");
            oOleDb.AddColumnName("Visa");
            oOleDb.AddColumnName("Master");
            oOleDb.AddColumnName("Amex");
            oOleDb.AddColumnName("Diners");
            oOleDb.AddColumnName("Cicilan");
            oOleDb.AddColumnName("[Cicilan Prog]");
            oOleDb.AddColumnName("Debit");
            oOleDb.AddColumnName("Tunai");
        }


        protected DataTable dtReadExcel(string sFileName)
        {
            DataTable dtTable = new DataTable();
            try
            {
                //using (NewExcelOleDb oOleDbConn = new NewExcelOleDb(sFileName))
                //{
                //    dtTable = oOleDbConn.dtReadExcel("QC");
                //    oOleDbConn.Dispose();
                //}

                FileStream stream = File.Open(sFileName, FileMode.Open, FileAccess.Read);
                string ext = Path.GetExtension(sFileName);
                IExcelDataReader excelReader;

                // Support for Excel 2007 XML format
                if (ext.ToLower() == ".xlsx")
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                else excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                excelReader.IsFirstRowAsColumnNames = true;

                DataSet result = excelReader.AsDataSet();
                dtTable = result.Tables["QC"];

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                dtTable = null;
            }
            return dtTable;
        }

        protected void doUploadFileExcel()
        {
            doCleanTable();

            SqlBulkCopy sqlBulk = new SqlBulkCopy(oConn);
            sqlBulk.DestinationTableName = "TempXLS";

            #region "Mapping"
            SqlBulkCopyColumnMapping mapProdID = new SqlBulkCopyColumnMapping("[Product ID]", "TerminalID");
            SqlBulkCopyColumnMapping mapMID = new SqlBulkCopyColumnMapping("[Merchant ID]", "MID");
            SqlBulkCopyColumnMapping mapMerchantName = new SqlBulkCopyColumnMapping("[Merchant Name]", "MerchantName");
            SqlBulkCopyColumnMapping mapAlamat1 = new SqlBulkCopyColumnMapping("[Alamat 1]", "Alamat1");
            SqlBulkCopyColumnMapping mapAlamat2 = new SqlBulkCopyColumnMapping("[Alamat 2]", "Alamat2");
            SqlBulkCopyColumnMapping mapFlazz = new SqlBulkCopyColumnMapping("[Flazz]", "Flazz");
            SqlBulkCopyColumnMapping mapTopUP = new SqlBulkCopyColumnMapping("[TopUp]", "TopUp");
            SqlBulkCopyColumnMapping mapPayment = new SqlBulkCopyColumnMapping("Payment", "Payment");
            SqlBulkCopyColumnMapping mapBCACard = new SqlBulkCopyColumnMapping("[BCA Card]", "BCA_Card");
            SqlBulkCopyColumnMapping mapVisa = new SqlBulkCopyColumnMapping("Visa", "Visa");
            SqlBulkCopyColumnMapping mapMaster = new SqlBulkCopyColumnMapping("Master", "Master");
            SqlBulkCopyColumnMapping mapAmex = new SqlBulkCopyColumnMapping("Amex", "Amex");
            SqlBulkCopyColumnMapping mapDiners = new SqlBulkCopyColumnMapping("Diners", "Diners");
            SqlBulkCopyColumnMapping mapCicilan = new SqlBulkCopyColumnMapping("Cicilan", "Cicilan");
            SqlBulkCopyColumnMapping mapCicilanProg = new SqlBulkCopyColumnMapping("[Cicilan Prog]", "Cicilan_Prog");
            SqlBulkCopyColumnMapping mapDebit = new SqlBulkCopyColumnMapping("Debit", "Debit");
            SqlBulkCopyColumnMapping mapTunai = new SqlBulkCopyColumnMapping("Tunai", "Tunai");

            sqlBulk.ColumnMappings.Add(mapProdID);
            sqlBulk.ColumnMappings.Add(mapMID);
            sqlBulk.ColumnMappings.Add(mapMerchantName);
            sqlBulk.ColumnMappings.Add(mapAlamat1);
            sqlBulk.ColumnMappings.Add(mapAlamat2);
            sqlBulk.ColumnMappings.Add(mapFlazz);
            sqlBulk.ColumnMappings.Add(mapTopUP);
            sqlBulk.ColumnMappings.Add(mapPayment);
            sqlBulk.ColumnMappings.Add(mapBCACard);
            sqlBulk.ColumnMappings.Add(mapVisa);
            sqlBulk.ColumnMappings.Add(mapMaster);
            sqlBulk.ColumnMappings.Add(mapAmex);
            sqlBulk.ColumnMappings.Add(mapDiners);
            sqlBulk.ColumnMappings.Add(mapCicilan);
            sqlBulk.ColumnMappings.Add(mapCicilanProg);
            sqlBulk.ColumnMappings.Add(mapDebit);
            sqlBulk.ColumnMappings.Add(mapTunai);
            #endregion

            sqlBulk.WriteToServer(dtReadExcel(sSourceFile));
        }

        protected void doEnableForm(bool isEnabled)
        {
            gbHeader.Enabled = isEnabled;
            gbButton.Enabled = isEnabled;
        }

        protected void doBrowseFile()
        {
            try
            {
                if (ofdBrowseFile.ShowDialog() != DialogResult.Cancel)
                {
                    string sFileExcel = txtSourceFile.Text = ofdBrowseFile.FileName;
                    sSourceFile = Application.StartupPath + "\\" + ofdBrowseFile.SafeFileName;
                    doCopyFile(sFileExcel, sSourceFile);
                    dtQC = new DataTable();
                    dtQC = dtReadExcel(sSourceFile);
                    ValidateSourceFile();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected void ValidateSourceFile()
        {
            string sMessage = null;
            if (dtEmsXmlMap != null && dtEmsXmlMap.Rows.Count > 0)
            {
                if (dtQC != null && dtQC.Rows.Count > 0)
                {
                    foreach (DataRow rowEms in dtEmsXmlMap.Rows)
                    {
                        string sColumnName = rowEms["EmsXMLColumn"].ToString();
                        if (!dtQC.Columns.Contains(sColumnName))
                        {
                            bwWorker.ReportProgress(100, string.Format("ERROR. Column {0} NOT FOUND", sColumnName));
                            sSourceFile = null;
                            txtSourceFile.Clear();
                            break;
                        }
                    }
                }
                else sMessage = "ERROR. Source file EMPTY or NOT FOUND";
            }
            else
                sMessage = "ERROR. Get EmsXML table connection";
            if (!string.IsNullOrEmpty(sMessage))
                MessageBox.Show(sMessage);
        }

        protected void doCopyFile(string sOriginalFile, string sCopiedFile)
        {
            if (File.Exists(sCopiedFile))
                File.Delete(sCopiedFile);
            File.Copy(sOriginalFile, sCopiedFile, true);
        }

        protected DataTable dtGetMapColumn()
        {
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPXmlGetMapColumn, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sVersion", SqlDbType.VarChar).Value = "2.00";
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }

        protected EmsXml emsInquiryResult(string sTerminalID, ref string sError)
        {
            EmsXml oEmsXml = new EmsXml();
            oEmsXml.Attribute.sVersions = "2.00";
            oEmsXml.Attribute.sSenders = "LOCAL";
            oEmsXml.Attribute.sSenderID = string.Format("LOCALINQ_{0:ddMMyyhhmmss}", DateTime.Now);
            oEmsXml.Attribute.sTypes = "INQ";

            EmsXml.DataClass.FilterClass oFilter = new EmsXml.DataClass.FilterClass();
            oFilter.sFilterFieldName = "Terminal_INIT";
            oFilter.sValues = sTerminalID;
            oEmsXml.Data.doAddFilter(oFilter);

            EmsXml oEmsXmlResult = new EmsXml();
            EmsProcess oProcess = new EmsProcess(oEmsXml, (new SqlConnection(sConnString)));
            //EmsProcess oProcess = new EmsProcess(oEmsXml, oConn);
            oEmsXmlResult = oProcess.emsInquiry();
            sError = oProcess.Error;
            return oEmsXmlResult;
        }
    }
}
