﻿namespace InSys
{
    partial class FrmUploadPTD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUploadPTD));
            this.groupBoxTerminalList = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxButton = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.backgroundWorkerCrp = new System.ComponentModel.BackgroundWorker();
            this.progressBarQueryCrp = new System.Windows.Forms.ProgressBar();
            this.groupBoxTerminalList.SuspendLayout();
            this.groupBoxButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxTerminalList
            // 
            this.groupBoxTerminalList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxTerminalList.Controls.Add(this.btnBrowse);
            this.groupBoxTerminalList.Controls.Add(this.textBox1);
            this.groupBoxTerminalList.Controls.Add(this.label1);
            this.groupBoxTerminalList.Location = new System.Drawing.Point(12, 12);
            this.groupBoxTerminalList.Name = "groupBoxTerminalList";
            this.groupBoxTerminalList.Size = new System.Drawing.Size(600, 4);
            this.groupBoxTerminalList.TabIndex = 0;
            this.groupBoxTerminalList.TabStop = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(508, 15);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 32);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(194, 20);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(292, 22);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Browse Terminal txt File";
            // 
            // groupBoxButton
            // 
            this.groupBoxButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxButton.Controls.Add(this.btnClose);
            this.groupBoxButton.Controls.Add(this.btnExecute);
            this.groupBoxButton.Location = new System.Drawing.Point(12, 63);
            this.groupBoxButton.Name = "groupBoxButton";
            this.groupBoxButton.Size = new System.Drawing.Size(600, 73);
            this.groupBoxButton.TabIndex = 1;
            this.groupBoxButton.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(483, 21);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(101, 34);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnExecute
            // 
            this.btnExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExecute.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnExecute.Location = new System.Drawing.Point(346, 21);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(101, 34);
            this.btnExecute.TabIndex = 0;
            this.btnExecute.Text = "Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // backgroundWorkerCrp
            // 
            this.backgroundWorkerCrp.WorkerReportsProgress = true;
            this.backgroundWorkerCrp.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerCrp_DoWork);
            this.backgroundWorkerCrp.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerCrp_ProgressChanged);
            this.backgroundWorkerCrp.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerCrp_RunWorkerCompleted);
            // 
            // progressBarQueryCrp
            // 
            this.progressBarQueryCrp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarQueryCrp.Location = new System.Drawing.Point(12, 22);
            this.progressBarQueryCrp.Name = "progressBarQueryCrp";
            this.progressBarQueryCrp.Size = new System.Drawing.Size(600, 39);
            this.progressBarQueryCrp.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarQueryCrp.TabIndex = 2;
            // 
            // FrmUploadPTD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 149);
            this.Controls.Add(this.progressBarQueryCrp);
            this.Controls.Add(this.groupBoxButton);
            this.Controls.Add(this.groupBoxTerminalList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmUploadPTD";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Generate Upload PTD (CRP)";
            this.groupBoxTerminalList.ResumeLayout(false);
            this.groupBoxTerminalList.PerformLayout();
            this.groupBoxButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxTerminalList;
        private System.Windows.Forms.GroupBox groupBoxButton;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExecute;
        private System.ComponentModel.BackgroundWorker backgroundWorkerCrp;
        private System.Windows.Forms.ProgressBar progressBarQueryCrp;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
    }
}