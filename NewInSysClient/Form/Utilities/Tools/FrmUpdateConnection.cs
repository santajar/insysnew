﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using InSysClass;
using System.Windows;
using System.Configuration;
using System.Threading;
using System.Diagnostics;


namespace InSys
{
    public partial class FrmUpdateConnection : Form
    {

        //--
        public FrmLogin flog { get; set; }
        string sReturn;
        //static string sConnString = ConfigurationManager.ConnectionStrings["Main"].ConnectionString;
        static string sAppDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
        static string sUserID;
        protected SqlConnection oSqlConn;
        static bool bDebug = true;
        //--
        static bool bFlag = false;
        static bool bFlagOpebFile = false;
        protected DataTable dtChoosingList;
        List<string> lstTag = new List<string>();
        List<string> lst2 = new List<string>();
        static string sDbId;
        List<string> ListToGenerateReport = new List<string>();
        public string sConnString;
        static string sTxtFileName;
        static string sValueAllowRD;
        static string stag;
        static string sTerminalID;
        DataTable DtTerminal;
        DataTable DtTarget;
        DataRow dr;
        OpenFileDialog OfDFile = new OpenFileDialog();

        public FrmUpdateConnection()
        {
            InitializeComponent();
        }

        public FrmUpdateConnection(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            sUserID = _sUserID;
        }

        public enum sType
        {
            TransactionPrimaryGprs = 1,
            TrasactionSecondariGprs = 2,
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            txtSingleTID.Text = "";
            OpenFile();
        }

        public void OpenFile()
        {
            label6.Visible = false;
            sTxtFileName = "";
            dataGridView1.DataSource = null;

            OfDFile.Title = "Select File";
            OfDFile.InitialDirectory = Application.StartupPath;
            OfDFile.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            OfDFile.FilterIndex = 1;
            OfDFile.RestoreDirectory = true;
            try
            {
                if (OfDFile.ShowDialog() == DialogResult.OK)
                {
                    sTxtFileName = OfDFile.FileName;
                    InsertToDataTable(sTxtFileName);
                    bFlagOpebFile = true;
                    dataGridView1.DataSource = DtTerminal;
                    label6.Text = sTxtFileName;
                    label6.Visible = true;

                }
            }
            catch
            {
                MessageBox.Show("Failed Open File");
            }
        }

        public DataTable InsertToDataTable(string sFilePath)
        {
            DtTerminal = new DataTable();
            string[] columns = null;

            var lines = File.ReadAllLines(sFilePath);

            if (lines.Count() > 0)
            {
                columns = lines[0].Split(new char[] { ',' });
                foreach (var column in columns)
                    ////DtTerminal.Columns.Add(column);
                    DtTerminal.Columns.Add("TerminalID");
            }

            for (int i = 0; i < lines.Count(); i++)
            {
                DataRow dr = DtTerminal.NewRow();
                string[] values = lines[i].Split(new char[] { ',' });

                for (int j = 0; j < values.Count() && j < columns.Count(); j++)
                    dr[j] = values[j];

                DtTerminal.Rows.Add(dr);
            }
            return DtTerminal;
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            this.Width = 502;
        }

        protected void InitCmbDB()
        {
            cmbDb.DataSource = dtLoadVersion();
            if (cmbDb.DataSource != null)
            {
                cmbDb.DisplayMember = "DatabaseName";
                cmbDb.ValueMember = "DatabaseID";
                cmbDb.SelectedIndex = -1;
            }
        }

        protected void InitCmbGprsEthPrimary()
        {
            cmbConnPrimay.DataSource = dtLoadGprsEth();
            cmbConnSecondary.DataSource = dtLoadGprsEth();
            if (cmbConnPrimay.DataSource != null)
            {
                cmbConnPrimay.DisplayMember = "GprsName";
                cmbConnPrimay.ValueMember = "GprsTagValue";
                cmbConnSecondary.DisplayMember = "GprsName";
                cmbConnSecondary.ValueMember = "GprsTagValue";
                //cmbConn.SelectedIndex = -1;
            }
        }

        protected void InitCmbRmoteDownloadName()
        {
          
            cmbRD.DataSource = dtLoaRemoteDownloadName();
            //if (cmbConnPrimay.DataSource != null)
            //{
                cmbRD.DisplayMember = "RemoteDownloadName";
                cmbRD.ValueMember = "RemoteDownloadTagValue";
            //}
        }

        protected void InitCmbGprsEthSecondary()
        {
            cmbConnPrimay.DataSource = dtLoadGprsEth();
            if (cmbConnPrimay.DataSource != null)
            {
                cmbConnSecondary.DisplayMember = "GprsName";
                cmbConnSecondary.ValueMember = "GprsTagValue";
                //cmbConn.SelectedIndex = -1;
            }
        }

        protected DataTable dtLoadVersion()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "";
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                        oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                dtTable = null;
            }
            return dtTable;
        }

        protected DataTable dtLoadGprsEth()
        {
            DataTable dtTable = new DataTable();
            try
            {
                string sCmd = " select * from tbProfileGPRS where DatabaseID =" + sDbId + "  AND GPRSTag = 'GP002' ORDER BY GPRSName";
                using (SqlCommand oSqlCmd = new SqlCommand(sCmd, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                        oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                dtTable = null;
            }
            return dtTable;
        }

        protected DataTable dtLoaRemoteDownloadName()
        {
            DataTable dtTable = new DataTable();
            try
            {
                string sCmd = " select * from tbProfileRemoteDownload where DatabaseID =" + sDbId + "  AND RemoteDownloadTag = 'RD001' ORDER BY RemoteDownloadName";
                using (SqlCommand oSqlCmd = new SqlCommand(sCmd, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                        oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                dtTable = null;
            }
            return dtTable;
        }

        private void Fmain_Load(object sender, EventArgs e)
        {
            label6.Visible = false;
            //oSqlConn = new SqlConnection(sConnString);
            InitCmbDB();
            DtTarget = new DataTable();
            DtTarget.Columns.Add("TxnPrimary");
            DtTarget.Columns.Add("TxnSecondary");
            DtTarget.Columns.Add("StlPrimary");
            DtTarget.Columns.Add("StlSecondary");
            dr = DtTarget.NewRow();
            DtTarget.Rows.Add(dr);
            lstTag = new List<string>();
        }

        public void CheckAllCheckBox()
        {
            lstTag.Clear();
            foreach (Control ctrl in groupBox3.Controls)
            {

                CheckBox chk = ctrl as CheckBox;
                if (chk != null)
                {
                    if (chk.Checked == true)
                    {
                        stag = chk.Name.ToString();
                        lstTag.Add(stag);
                        bFlag = true;
                    }
                }
            }
        }

        private void cmbDb_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbConnPrimay.Text = "";
            cmbConnSecondary.Text = "";
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            if (chkAllowRemote.Checked == true)
            {
                sValueAllowRD = "1";
            }
            else
            {
                sValueAllowRD="0";
            }
            progressBar1.Value = 0;
            int counter = 100 / DtTerminal.Rows.Count;
            progressBar1.Maximum = 100;
            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            ClearDtTarget();
            //if (cmbConnPrimay.Text != "" && cmbConnPrimay.Text != "" || txtDialPrimary.Text != "" && txtDialSecondary.Text != "" || cmbRD.Text !="")
            if (cmbConnPrimay.Text != "" && cmbConnPrimay.Text != "" || txtDialPrimary.Text != "" && txtDialSecondary.Text != "" || DE040.Checked == true)
            {
                if (bFlagOpebFile == true || txtSingleTID.Text != "")
                {
                    CheckAllCheckBox();
                    if (bFlag == true)
                    {
                        foreach (DataRow row in DtTerminal.Rows)
                        {
                            int iCounter = 100 / DtTerminal.Rows.Count;
                            sTerminalID = row[0].ToString();
                            foreach (string lst in lstTag)
                            {
                                if (sTerminalID.Length == 8 || sTerminalID != "")
                                {
                                    UpdateData(lst);
                                }
                                else
                                {
                                    ListToGenerateReport.Add(sTerminalID + " | " + "Not Valid");
                                }
                            }
                            progressBar1.Value += counter;
                        }
                        progressBar1.Value = 100;
                        CreateReport();
                        MessageBox.Show("Process Update Done");

                    }
                    else
                    {
                        MessageBox.Show("Please Choose Checkbox");
                        progressBar1.Value = 0;
                    }
                }
                else
                {
                    MessageBox.Show("Please Browse the file before execute");
                    progressBar1.Value = 0;
                }
            }
            else
            {
                MessageBox.Show("Please Select Destionation Connection, Primary or Secondary");
                progressBar1.Value = 0;
            }
            groupBox1.Enabled = true;
            groupBox2.Enabled = true;
        }

        void ProcessObj_Exited(object sender, EventArgs e)
        {
            progressBar1.Visible = false;
        }

        private void CreateReport()
        {
            string sDirectoryAppFile = string.Format(@"{0}\{1}_Report.txt", sAppDirectory, DateTime.Now.ToString("yyMMddHHmmss"));
            using (StreamWriter oSteamWriter = new StreamWriter(sDirectoryAppFile, false))
            {
                foreach (string item in ListToGenerateReport)
                {
                    oSteamWriter.WriteLine(item);
                }
            }
        }

        private void ClearDtTarget()
        {
            DtTarget.Rows[0][0] = "";
            DtTarget.Rows[0][1] = "";
            DtTarget.Rows[0][2] = "";
            DtTarget.Rows[0][3] = "";
        }

        private void cmbConnPrimay_Click(object sender, EventArgs e)
        {
            if (cmbDb.Text != "")
            {
                sDbId = cmbDb.SelectedValue.ToString();
                InitCmbGprsEthPrimary();
            }
        }

        private void cmbConnSecondary_Click(object sender, EventArgs e)
        {

        }

        protected void UpdateData(string _sTag)
        {
            switch (_sTag)
            {
                case ("DE040")://Remote Download Name
                    ExecuteUpdateData(sTitle(_sTag), sTerminalID, _sTag, cmbRD.Text.ToString());
                    break;

                case ("AA037")://Txn GPRS Primary
                case ("AA045")://Primary Txn Eth

                case ("AA039")://Stl GPRS Primary
                case ("AA047")://Primary Stl Eth
                    ExecuteUpdateData(sTitle(_sTag), sTerminalID, _sTag, cmbConnPrimay.Text.ToString());
                    break;

                case ("AA046")://Secondary Txn Eth
                case ("AA038")://Txn GPRS Secondary

                case ("AA048")://Secondary Stl Eth
                case ("AA040")://Stl GPRS Secondary
                    ExecuteUpdateData(sTitle(_sTag), sTerminalID, _sTag, cmbConnSecondary.Text.ToString());
                    break;
                case ("AA006")://Txn Primary Ph
                case ("AA010")://Stl Primary Phone
                    if (chkFlazzJakrta.Checked == true)
                    {
                        ExecuteUpdateData(sTitle(_sTag), sTerminalID, _sTag, txtDialPrimary.Text.ToString());
                    }
                    if (chkFlazzJakrta.Checked == false)
                    {
                        ExecuteUpdateData(sTitle(_sTag), sTerminalID, _sTag, txtDialPrimary.Text.ToString());
                    }
                    break;
                case ("AA007")://Txn Secondary Ph
                case ("AA011")://Stl Secondary Ph
                    if (chkFlazzJakrta.Checked == true)
                    {
                        ExecuteUpdateData(sTitle(_sTag), sTerminalID, _sTag, txtDialSecondary.Text.ToString());
                    }
                    if (chkFlazzJakrta.Checked == false)
                    {
                        ExecuteUpdateData(sTitle(_sTag), sTerminalID, _sTag, txtDialSecondary.Text.ToString());
                    }
                    break;
                default:
                    return;
            }

        }

        public string sTitle(string _sTitle)
        {
            if (_sTitle == "DE040")
            {
                _sTitle = "Remote Download Name";
            }
            if (_sTitle == "AA006")
            {
                _sTitle = "Stl Primary Phone";
            }
            if (_sTitle == "AA007")
            {
                _sTitle = "Txn Secondary Ph";
            }
            if (_sTitle == "AA010")
            {
                _sTitle = "Stl Primary Phone";
            }
            if (_sTitle == "AA011")
            {
                _sTitle = "Stl Secondary Ph";
            }
            if (_sTitle == "AA045")
            {
                _sTitle = "Primary Txn Eth";
            }
            if (_sTitle == "AA046")
            {
                _sTitle = "Secondary Txn Eth";
            }
            if (_sTitle == "AA047")
            {
                _sTitle = "Primary Stl Eth";
            }
            if (_sTitle == "AA048")
            {
                _sTitle = "Secondary Stl Eth";
            }
            if (_sTitle == "AA037")
            {
                _sTitle = "Txn GPRS Primary";
            }
            if (_sTitle == "AA038")
            {
                _sTitle = "Txn GPRS Secondary";
            }
            if (_sTitle == "AA039")
            {
                _sTitle = "Stl GPRS Primary";
            }
            if (_sTitle == "AA040")
            {
                _sTitle = "Stl GPRS Secondary";
            }
            return _sTitle;
        }

        protected void ExecuteUpdateData(string sTitle, string _sTerminalID, string _sTag, string _sTagValue)
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.spUpdateConnAcquirer, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                    oSqlCmd.Parameters.Add("@sTitle", SqlDbType.VarChar).Value = sTitle;
                    oSqlCmd.Parameters.Add("@sTag", SqlDbType.VarChar).Value = _sTag;
                    oSqlCmd.Parameters.Add("@sTagValue", SqlDbType.VarChar).Value = _sTagValue;
                    oSqlCmd.Parameters.Add("@sAllowRD", SqlDbType.VarChar).Value = sValueAllowRD;
                    // oSqlCmd.Parameters.Add("@sFlazzJakarta", SqlDbType.VarChar).Value = _sChkFlazzJakarta;
                    if (chkFlazzJakrta.Checked == true)
                    {
                        oSqlCmd.Parameters.Add("@sFlazzJakarta", SqlDbType.VarChar).Value = "YES";
                    }
                    else
                    {
                        if (sTitle == "Remote Download Name")
                        {
                            oSqlCmd.Parameters.Add("@sFlazzJakarta", SqlDbType.VarChar).Value = "";
                        }
                        else
                        {
                            oSqlCmd.Parameters.Add("@sFlazzJakarta", SqlDbType.VarChar).Value = "NO";
                        }
                    }

                    SqlParameter returnParameter = new SqlParameter("@RetVal", SqlDbType.NVarChar, 100);
                    returnParameter.Direction = ParameterDirection.Output;
                    oSqlCmd.Parameters.Add(returnParameter);
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    oSqlCmd.ExecuteNonQuery();
                    sReturn = (string)returnParameter.Value.ToString();
                    if (sReturn == "Success")
                    {
                        ListToGenerateReport.Add(sTerminalID + " | " + sTitle + "    |" + "Success");
                        CommonClass.InputLog(oSqlConn, "", sUserID, cmbDb.Text, "Update : " + sTerminalID + "-->" + sTitle + "Value : " + _sTagValue, "Update Provider Success");
                    }
                    else
                    {
                        ListToGenerateReport.Add(sTerminalID + " | " + sTitle + "    |" + "Ignore");
                        CommonClass.InputLog(oSqlConn, "", sUserID, cmbDb.Text, "Update : " + sTerminalID + "-->" + sTitle + "Value : " + _sTagValue, "Update Provider TID Not Found");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        
        private void txtSingleTID_TextChanged(object sender, EventArgs e)
        {
            if (txtSingleTID.Text.Length == 8)
            {
                //DtTerminal.Clear();
                DtTerminal = new DataTable();
                DtTerminal.Columns.Add("TerminalID");
                DataRow dr = DtTerminal.NewRow();
                dr[0] = txtSingleTID.Text.ToString();
                DtTerminal.Rows.Add(dr);
            }
            if (txtSingleTID.Text.Length > 8)
            {
                MessageBox.Show("Max Length TID 8 digit");
                txtSingleTID.Clear();
            }
        }

        private void DE040_Click(object sender, EventArgs e)
        {
            if (DE040.Checked == true)
            {
                sDbId = cmbDb.SelectedValue.ToString();
                InitCmbRmoteDownloadName();
            }
        }
    }
}