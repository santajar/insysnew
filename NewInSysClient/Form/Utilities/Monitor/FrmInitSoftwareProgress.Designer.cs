﻿namespace InSys
{
    partial class FrmInitSoftwareProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInitSoftwareProgress));
            this.oDgvSoftwareTerminal = new System.Windows.Forms.DataGridView();
            this.TimerClock = new System.Windows.Forms.Timer(this.components);
            this.sBarMonitor = new System.Windows.Forms.StatusBar();
            this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
            this.StatusTime = new System.Windows.Forms.StatusBarPanel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.TimerRefresh = new System.Windows.Forms.Timer(this.components);
            this.lblGroup = new System.Windows.Forms.Label();
            this.lblRegion = new System.Windows.Forms.Label();
            this.lblTerminalID = new System.Windows.Forms.Label();
            this.lblSoftware = new System.Windows.Forms.Label();
            this.lblSoftwareDownload = new System.Windows.Forms.Label();
            this.lblSerialNumber = new System.Windows.Forms.Label();
            this.lblInstallStatus = new System.Windows.Forms.Label();
            this.txtGroup = new System.Windows.Forms.TextBox();
            this.txtRegion = new System.Windows.Forms.TextBox();
            this.txtTerminalID = new System.Windows.Forms.TextBox();
            this.txtSoftware = new System.Windows.Forms.TextBox();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.txtSoftwareDownload = new System.Windows.Forms.TextBox();
            this.dtpSStartTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEStartTime = new System.Windows.Forms.DateTimePicker();
            this.txtInstallStatus = new System.Windows.Forms.TextBox();
            this.dtpSEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEInstallTime = new System.Windows.Forms.DateTimePicker();
            this.dtpSInstallTime = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gbSearch = new System.Windows.Forms.GroupBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.txtBuildNumber = new System.Windows.Forms.TextBox();
            this.lblBuildNumber = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.chkFinishInstallTime = new System.Windows.Forms.CheckBox();
            this.chkEndTime = new System.Windows.Forms.CheckBox();
            this.chkStartTime = new System.Windows.Forms.CheckBox();
            this.sfdExport = new System.Windows.Forms.SaveFileDialog();
            this.txtTotalOnProgress = new System.Windows.Forms.TextBox();
            this.lblTotalTIDOnProgress = new System.Windows.Forms.Label();
            this.lblTotalEDCSuccess = new System.Windows.Forms.Label();
            this.txtTotalSucces = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.oDgvSoftwareTerminal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusTime)).BeginInit();
            this.gbSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // oDgvSoftwareTerminal
            // 
            this.oDgvSoftwareTerminal.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.oDgvSoftwareTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.oDgvSoftwareTerminal.Location = new System.Drawing.Point(12, 152);
            this.oDgvSoftwareTerminal.MultiSelect = false;
            this.oDgvSoftwareTerminal.Name = "oDgvSoftwareTerminal";
            this.oDgvSoftwareTerminal.ReadOnly = true;
            this.oDgvSoftwareTerminal.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.oDgvSoftwareTerminal.RowHeadersVisible = false;
            this.oDgvSoftwareTerminal.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.oDgvSoftwareTerminal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.oDgvSoftwareTerminal.Size = new System.Drawing.Size(1346, 544);
            this.oDgvSoftwareTerminal.TabIndex = 0;
            // 
            // TimerClock
            // 
            this.TimerClock.Enabled = true;
            this.TimerClock.Tick += new System.EventHandler(this.TimerClock_Tick);
            // 
            // sBarMonitor
            // 
            this.sBarMonitor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sBarMonitor.Location = new System.Drawing.Point(0, 695);
            this.sBarMonitor.Name = "sBarMonitor";
            this.sBarMonitor.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.statusBarPanel1,
            this.StatusTime});
            this.sBarMonitor.ShowPanels = true;
            this.sBarMonitor.Size = new System.Drawing.Size(1354, 38);
            this.sBarMonitor.SizingGrip = false;
            this.sBarMonitor.TabIndex = 3;
            this.sBarMonitor.Text = "1";
            // 
            // statusBarPanel1
            // 
            this.statusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.statusBarPanel1.Name = "statusBarPanel1";
            this.statusBarPanel1.Style = System.Windows.Forms.StatusBarPanelStyle.OwnerDraw;
            this.statusBarPanel1.Text = "statusBarPanel1";
            this.statusBarPanel1.Width = 1290;
            // 
            // StatusTime
            // 
            this.StatusTime.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.StatusTime.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusTime.Icon")));
            this.StatusTime.Name = "StatusTime";
            this.StatusTime.Text = "Time";
            this.StatusTime.Width = 64;
            // 
            // btnSearch
            // 
            this.btnSearch.AutoSize = true;
            this.btnSearch.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSearch.Enabled = false;
            this.btnSearch.Location = new System.Drawing.Point(1081, 100);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(51, 23);
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Visible = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // TimerRefresh
            // 
            this.TimerRefresh.Enabled = true;
            this.TimerRefresh.Interval = 30000;
            this.TimerRefresh.Tick += new System.EventHandler(this.TimerRefresh_Tick);
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.Location = new System.Drawing.Point(20, 43);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(36, 13);
            this.lblGroup.TabIndex = 6;
            this.lblGroup.Text = "Group";
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Location = new System.Drawing.Point(20, 70);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(41, 13);
            this.lblRegion.TabIndex = 7;
            this.lblRegion.Text = "Region";
            // 
            // lblTerminalID
            // 
            this.lblTerminalID.AutoSize = true;
            this.lblTerminalID.Location = new System.Drawing.Point(20, 95);
            this.lblTerminalID.Name = "lblTerminalID";
            this.lblTerminalID.Size = new System.Drawing.Size(58, 13);
            this.lblTerminalID.TabIndex = 8;
            this.lblTerminalID.Text = "TerminalID";
            // 
            // lblSoftware
            // 
            this.lblSoftware.AutoSize = true;
            this.lblSoftware.Location = new System.Drawing.Point(269, 22);
            this.lblSoftware.Name = "lblSoftware";
            this.lblSoftware.Size = new System.Drawing.Size(49, 13);
            this.lblSoftware.TabIndex = 9;
            this.lblSoftware.Text = "Software";
            // 
            // lblSoftwareDownload
            // 
            this.lblSoftwareDownload.AutoSize = true;
            this.lblSoftwareDownload.Location = new System.Drawing.Point(269, 45);
            this.lblSoftwareDownload.Name = "lblSoftwareDownload";
            this.lblSoftwareDownload.Size = new System.Drawing.Size(100, 13);
            this.lblSoftwareDownload.TabIndex = 10;
            this.lblSoftwareDownload.Text = "Software Download";
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.AutoSize = true;
            this.lblSerialNumber.Location = new System.Drawing.Point(269, 95);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(73, 13);
            this.lblSerialNumber.TabIndex = 11;
            this.lblSerialNumber.Text = "Serial Number";
            // 
            // lblInstallStatus
            // 
            this.lblInstallStatus.AutoSize = true;
            this.lblInstallStatus.Location = new System.Drawing.Point(577, 17);
            this.lblInstallStatus.Name = "lblInstallStatus";
            this.lblInstallStatus.Size = new System.Drawing.Size(67, 13);
            this.lblInstallStatus.TabIndex = 15;
            this.lblInstallStatus.Text = "Install Status";
            // 
            // txtGroup
            // 
            this.txtGroup.Location = new System.Drawing.Point(99, 40);
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Size = new System.Drawing.Size(151, 20);
            this.txtGroup.TabIndex = 17;
            // 
            // txtRegion
            // 
            this.txtRegion.Location = new System.Drawing.Point(99, 66);
            this.txtRegion.Name = "txtRegion";
            this.txtRegion.Size = new System.Drawing.Size(151, 20);
            this.txtRegion.TabIndex = 18;
            // 
            // txtTerminalID
            // 
            this.txtTerminalID.Location = new System.Drawing.Point(99, 92);
            this.txtTerminalID.Name = "txtTerminalID";
            this.txtTerminalID.Size = new System.Drawing.Size(151, 20);
            this.txtTerminalID.TabIndex = 19;
            // 
            // txtSoftware
            // 
            this.txtSoftware.Location = new System.Drawing.Point(384, 16);
            this.txtSoftware.Name = "txtSoftware";
            this.txtSoftware.Size = new System.Drawing.Size(151, 20);
            this.txtSoftware.TabIndex = 20;
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.Location = new System.Drawing.Point(384, 93);
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(151, 20);
            this.txtSerialNumber.TabIndex = 21;
            // 
            // txtSoftwareDownload
            // 
            this.txtSoftwareDownload.Location = new System.Drawing.Point(384, 41);
            this.txtSoftwareDownload.Name = "txtSoftwareDownload";
            this.txtSoftwareDownload.Size = new System.Drawing.Size(151, 20);
            this.txtSoftwareDownload.TabIndex = 22;
            // 
            // dtpSStartTime
            // 
            this.dtpSStartTime.CustomFormat = "mmddyy";
            this.dtpSStartTime.Enabled = false;
            this.dtpSStartTime.Location = new System.Drawing.Point(670, 42);
            this.dtpSStartTime.Name = "dtpSStartTime";
            this.dtpSStartTime.Size = new System.Drawing.Size(200, 20);
            this.dtpSStartTime.TabIndex = 24;
            this.dtpSStartTime.Value = new System.DateTime(2013, 11, 1, 16, 11, 0, 0);
            // 
            // dtpEStartTime
            // 
            this.dtpEStartTime.CustomFormat = "mmddyy";
            this.dtpEStartTime.Enabled = false;
            this.dtpEStartTime.Location = new System.Drawing.Point(901, 43);
            this.dtpEStartTime.Name = "dtpEStartTime";
            this.dtpEStartTime.Size = new System.Drawing.Size(200, 20);
            this.dtpEStartTime.TabIndex = 25;
            // 
            // txtInstallStatus
            // 
            this.txtInstallStatus.Location = new System.Drawing.Point(670, 14);
            this.txtInstallStatus.Name = "txtInstallStatus";
            this.txtInstallStatus.Size = new System.Drawing.Size(151, 20);
            this.txtInstallStatus.TabIndex = 26;
            // 
            // dtpSEndTime
            // 
            this.dtpSEndTime.CustomFormat = "mmddyy";
            this.dtpSEndTime.Enabled = false;
            this.dtpSEndTime.Location = new System.Drawing.Point(670, 68);
            this.dtpSEndTime.Name = "dtpSEndTime";
            this.dtpSEndTime.Size = new System.Drawing.Size(200, 20);
            this.dtpSEndTime.TabIndex = 27;
            this.dtpSEndTime.Value = new System.DateTime(2013, 11, 1, 16, 20, 0, 0);
            // 
            // dtpEEndTime
            // 
            this.dtpEEndTime.CustomFormat = "mmddyy";
            this.dtpEEndTime.Enabled = false;
            this.dtpEEndTime.Location = new System.Drawing.Point(901, 68);
            this.dtpEEndTime.Name = "dtpEEndTime";
            this.dtpEEndTime.Size = new System.Drawing.Size(200, 20);
            this.dtpEEndTime.TabIndex = 28;
            // 
            // dtpEInstallTime
            // 
            this.dtpEInstallTime.CustomFormat = "mmddyy";
            this.dtpEInstallTime.Enabled = false;
            this.dtpEInstallTime.Location = new System.Drawing.Point(902, 94);
            this.dtpEInstallTime.Name = "dtpEInstallTime";
            this.dtpEInstallTime.Size = new System.Drawing.Size(200, 20);
            this.dtpEInstallTime.TabIndex = 30;
            // 
            // dtpSInstallTime
            // 
            this.dtpSInstallTime.CustomFormat = "mmddyy";
            this.dtpSInstallTime.Enabled = false;
            this.dtpSInstallTime.Location = new System.Drawing.Point(670, 94);
            this.dtpSInstallTime.Name = "dtpSInstallTime";
            this.dtpSInstallTime.Size = new System.Drawing.Size(200, 20);
            this.dtpSInstallTime.TabIndex = 29;
            this.dtpSInstallTime.Value = new System.DateTime(2013, 11, 1, 16, 21, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(878, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "to";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(879, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "to";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(879, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "to";
            // 
            // gbSearch
            // 
            this.gbSearch.Controls.Add(this.btnExport);
            this.gbSearch.Controls.Add(this.txtBuildNumber);
            this.gbSearch.Controls.Add(this.lblBuildNumber);
            this.gbSearch.Controls.Add(this.txtCity);
            this.gbSearch.Controls.Add(this.lblCity);
            this.gbSearch.Controls.Add(this.chkFinishInstallTime);
            this.gbSearch.Controls.Add(this.chkEndTime);
            this.gbSearch.Controls.Add(this.txtGroup);
            this.gbSearch.Controls.Add(this.label3);
            this.gbSearch.Controls.Add(this.txtSoftwareDownload);
            this.gbSearch.Controls.Add(this.lblSoftwareDownload);
            this.gbSearch.Controls.Add(this.label2);
            this.gbSearch.Controls.Add(this.lblInstallStatus);
            this.gbSearch.Controls.Add(this.txtRegion);
            this.gbSearch.Controls.Add(this.label1);
            this.gbSearch.Controls.Add(this.dtpEInstallTime);
            this.gbSearch.Controls.Add(this.txtTerminalID);
            this.gbSearch.Controls.Add(this.txtSerialNumber);
            this.gbSearch.Controls.Add(this.dtpSInstallTime);
            this.gbSearch.Controls.Add(this.txtSoftware);
            this.gbSearch.Controls.Add(this.btnSearch);
            this.gbSearch.Controls.Add(this.dtpEEndTime);
            this.gbSearch.Controls.Add(this.lblSerialNumber);
            this.gbSearch.Controls.Add(this.dtpSEndTime);
            this.gbSearch.Controls.Add(this.lblGroup);
            this.gbSearch.Controls.Add(this.txtInstallStatus);
            this.gbSearch.Controls.Add(this.lblSoftware);
            this.gbSearch.Controls.Add(this.lblRegion);
            this.gbSearch.Controls.Add(this.dtpSStartTime);
            this.gbSearch.Controls.Add(this.dtpEStartTime);
            this.gbSearch.Controls.Add(this.lblTerminalID);
            this.gbSearch.Controls.Add(this.chkStartTime);
            this.gbSearch.Location = new System.Drawing.Point(12, 12);
            this.gbSearch.Name = "gbSearch";
            this.gbSearch.Size = new System.Drawing.Size(1346, 134);
            this.gbSearch.TabIndex = 34;
            this.gbSearch.TabStop = false;
            this.gbSearch.Text = "Advance Search";
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(1138, 93);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(74, 23);
            this.btnExport.TabIndex = 35;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtBuildNumber
            // 
            this.txtBuildNumber.Location = new System.Drawing.Point(384, 66);
            this.txtBuildNumber.MaxLength = 4;
            this.txtBuildNumber.Name = "txtBuildNumber";
            this.txtBuildNumber.Size = new System.Drawing.Size(151, 20);
            this.txtBuildNumber.TabIndex = 40;
            // 
            // lblBuildNumber
            // 
            this.lblBuildNumber.AutoSize = true;
            this.lblBuildNumber.Location = new System.Drawing.Point(269, 68);
            this.lblBuildNumber.Name = "lblBuildNumber";
            this.lblBuildNumber.Size = new System.Drawing.Size(67, 13);
            this.lblBuildNumber.TabIndex = 39;
            this.lblBuildNumber.Text = "BuildNumber";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(99, 17);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(151, 20);
            this.txtCity.TabIndex = 38;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(20, 20);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(24, 13);
            this.lblCity.TabIndex = 37;
            this.lblCity.Text = "City";
            // 
            // chkFinishInstallTime
            // 
            this.chkFinishInstallTime.AutoSize = true;
            this.chkFinishInstallTime.Location = new System.Drawing.Point(559, 95);
            this.chkFinishInstallTime.Name = "chkFinishInstallTime";
            this.chkFinishInstallTime.Size = new System.Drawing.Size(109, 17);
            this.chkFinishInstallTime.TabIndex = 36;
            this.chkFinishInstallTime.Text = "Finish Install Time";
            this.chkFinishInstallTime.UseVisualStyleBackColor = true;
            this.chkFinishInstallTime.CheckedChanged += new System.EventHandler(this.chkFinishInstallTime_CheckedChanged);
            // 
            // chkEndTime
            // 
            this.chkEndTime.AutoSize = true;
            this.chkEndTime.Location = new System.Drawing.Point(559, 70);
            this.chkEndTime.Name = "chkEndTime";
            this.chkEndTime.Size = new System.Drawing.Size(71, 17);
            this.chkEndTime.TabIndex = 35;
            this.chkEndTime.Text = "End Time";
            this.chkEndTime.UseVisualStyleBackColor = true;
            this.chkEndTime.CheckedChanged += new System.EventHandler(this.chkEndTime_CheckedChanged);
            // 
            // chkStartTime
            // 
            this.chkStartTime.AutoSize = true;
            this.chkStartTime.Location = new System.Drawing.Point(559, 45);
            this.chkStartTime.Name = "chkStartTime";
            this.chkStartTime.Size = new System.Drawing.Size(74, 17);
            this.chkStartTime.TabIndex = 34;
            this.chkStartTime.Text = "Start Time";
            this.chkStartTime.UseVisualStyleBackColor = true;
            this.chkStartTime.CheckedChanged += new System.EventHandler(this.chkStartTime_CheckedChanged);
            // 
            // txtTotalOnProgress
            // 
            this.txtTotalOnProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalOnProgress.Location = new System.Drawing.Point(999, 704);
            this.txtTotalOnProgress.Name = "txtTotalOnProgress";
            this.txtTotalOnProgress.ReadOnly = true;
            this.txtTotalOnProgress.Size = new System.Drawing.Size(40, 21);
            this.txtTotalOnProgress.TabIndex = 35;
            // 
            // lblTotalTIDOnProgress
            // 
            this.lblTotalTIDOnProgress.AutoSize = true;
            this.lblTotalTIDOnProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalTIDOnProgress.Location = new System.Drawing.Point(849, 707);
            this.lblTotalTIDOnProgress.Name = "lblTotalTIDOnProgress";
            this.lblTotalTIDOnProgress.Size = new System.Drawing.Size(144, 15);
            this.lblTotalTIDOnProgress.TabIndex = 36;
            this.lblTotalTIDOnProgress.Text = "Total TID OnProgress";
            // 
            // lblTotalEDCSuccess
            // 
            this.lblTotalEDCSuccess.AutoSize = true;
            this.lblTotalEDCSuccess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalEDCSuccess.Location = new System.Drawing.Point(1045, 707);
            this.lblTotalEDCSuccess.Name = "lblTotalEDCSuccess";
            this.lblTotalEDCSuccess.Size = new System.Drawing.Size(121, 15);
            this.lblTotalEDCSuccess.TabIndex = 37;
            this.lblTotalEDCSuccess.Text = "Total EDC Succes";
            // 
            // txtTotalSucces
            // 
            this.txtTotalSucces.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalSucces.Location = new System.Drawing.Point(1172, 704);
            this.txtTotalSucces.Name = "txtTotalSucces";
            this.txtTotalSucces.ReadOnly = true;
            this.txtTotalSucces.Size = new System.Drawing.Size(40, 21);
            this.txtTotalSucces.TabIndex = 38;
            // 
            // FrmInitSoftwareProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.txtTotalSucces);
            this.Controls.Add(this.lblTotalEDCSuccess);
            this.Controls.Add(this.lblTotalTIDOnProgress);
            this.Controls.Add(this.txtTotalOnProgress);
            this.Controls.Add(this.gbSearch);
            this.Controls.Add(this.sBarMonitor);
            this.Controls.Add(this.oDgvSoftwareTerminal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmInitSoftwareProgress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Init Software Progress";
            this.Load += new System.EventHandler(this.FrmInitSoftwareCon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.oDgvSoftwareTerminal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusTime)).EndInit();
            this.gbSearch.ResumeLayout(false);
            this.gbSearch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView oDgvSoftwareTerminal;
        internal System.Windows.Forms.StatusBar sBarMonitor;
        private System.Windows.Forms.StatusBarPanel statusBarPanel1;
        internal System.Windows.Forms.StatusBarPanel StatusTime;
        private System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.Timer TimerClock;
        internal System.Windows.Forms.Timer TimerRefresh;
        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.Label lblTerminalID;
        private System.Windows.Forms.Label lblSoftware;
        private System.Windows.Forms.Label lblSoftwareDownload;
        private System.Windows.Forms.Label lblSerialNumber;
        private System.Windows.Forms.Label lblInstallStatus;
        private System.Windows.Forms.TextBox txtGroup;
        private System.Windows.Forms.TextBox txtRegion;
        private System.Windows.Forms.TextBox txtTerminalID;
        private System.Windows.Forms.TextBox txtSoftware;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.TextBox txtSoftwareDownload;
        private System.Windows.Forms.DateTimePicker dtpSStartTime;
        private System.Windows.Forms.DateTimePicker dtpEStartTime;
        private System.Windows.Forms.TextBox txtInstallStatus;
        private System.Windows.Forms.DateTimePicker dtpSEndTime;
        private System.Windows.Forms.DateTimePicker dtpEEndTime;
        private System.Windows.Forms.DateTimePicker dtpEInstallTime;
        private System.Windows.Forms.DateTimePicker dtpSInstallTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbSearch;
        private System.Windows.Forms.CheckBox chkFinishInstallTime;
        private System.Windows.Forms.CheckBox chkEndTime;
        private System.Windows.Forms.CheckBox chkStartTime;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.TextBox txtBuildNumber;
        private System.Windows.Forms.Label lblBuildNumber;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.SaveFileDialog sfdExport;
        private System.Windows.Forms.TextBox txtTotalOnProgress;
        private System.Windows.Forms.Label lblTotalTIDOnProgress;
        private System.Windows.Forms.Label lblTotalEDCSuccess;
        private System.Windows.Forms.TextBox txtTotalSucces;
    }
}