using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    /// <summary>
    /// Detail Initialize monitoring window
    /// </summary>
    public partial class FrmUnitedDetail : Form
    {
        /// <summary>
        /// Local SQLConnection variable
        /// </summary>
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// TerminalID where its detailed proccess would be displayed.
        /// </summary>
        protected string sTerminalID = "";

        /// <summary>
        /// Detail Initialize monitoring window constructor
        /// </summary>
        /// <param name="_oSqlConn">SQLConnection : SQL Connection parameter</param>
        /// <param name="_sTerminalID">string : selected TerminalId to monitor</param>
        public FrmUnitedDetail(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sTerminalID)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sTerminalID = _sTerminalID;
            InitializeComponent();
        }

        /// <summary>
        /// Runs while form is loading, load data from database.
        /// </summary>
        private void FrmUnitedDetail_Load(object sender, EventArgs e)
        {
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sViewUnitedDetail, "");
            LoadData();
        }

        /// <summary>
        /// Load data from database to be displayed on DataGridView.
        /// </summary>
        protected void LoadData()
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPInitLogConnDetailBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
                DataTable oDataTableInitLogDetailConn = new DataTable();
                oAdapt.Fill(oDataTableInitLogDetailConn);
                oDgTerminalDetail.DataSource = oDataTableInitLogDetailConn;
                SetDisplayDatagridView();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                this.Close();
            }
        }

        /// <summary>
        /// Set DataGridView's format and appearance.
        /// </summary>
        protected void SetDisplayDatagridView()
        {
            oDgTerminalDetail.Columns["Time"].DefaultCellStyle.Format = "dd MMMM yyyy  HH:mm:ss";
            oDgTerminalDetail.Columns["Time"].Width = 170;
            oDgTerminalDetail.Columns["TerminalID"].Width = 90;
            oDgTerminalDetail.Columns["Description"].Width = 130;
            oDgTerminalDetail.Columns["Name"].Width = 70;
            oDgTerminalDetail.Columns["Percentage"].Width = 80;
            oDgTerminalDetail.Columns["Description"].HeaderText = "Status";
        }

        /// <summary>
        /// Load data and set the selected row.
        /// </summary>
        private void timerRefresh_Tick(object sender, EventArgs e)
        {
            
            int iRowIndex = iGetRowIndex();
            LoadData();
            oDgTerminalDetail[1, iRowIndex].Selected = true;
        }

        /// <summary>
        /// Get the index from current row.
        /// </summary>
        /// <returns>int : Current row's index</returns>
        protected int iGetRowIndex()
        {
            if (oDgTerminalDetail.CurrentRow == null) return 0;
            else 
                return oDgTerminalDetail.CurrentRow.Index;
        }

        /// <summary>
        /// Close the form.
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}