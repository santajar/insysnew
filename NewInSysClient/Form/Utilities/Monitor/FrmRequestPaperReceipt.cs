﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmRequestPaperReceipt : Form
    {
        private SqlConnection oSqlConn;
        private SqlConnection oSqlConnAuditTrail;
        private string sUserID;
        /// <summary>
        /// Needed as control variables at different events.
        /// </summary>
        protected bool isSearch = false;
        public bool isAuditInit = true;

        public FrmRequestPaperReceipt(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
        }

        private void FrmRequestPaperReceipt_Load(object sender, EventArgs e)
        {
            LoadDataReqPaperResecipt("");
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", "Access RequestPaperReceipt", "");

        }


        private void LoadDataReqPaperResecipt(string sTerminalID)
        {
           // int iIndexDGV = 0;
          //  if (dgReqPaper.SelectedCells.Count > 1)
            //{

                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPRequestPaperReceiptVIew, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
            if (oSqlConn.State != ConnectionState.Open) {oSqlConn.Open();}
                SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
                DataTable oDReqPapaerRecipt = new DataTable();
                oAdapt.Fill(oDReqPapaerRecipt);
                dgReqPaper.DataSource = oDReqPapaerRecipt;


            //}
        }

        private void tmRefreshReq_Tick(object sender, EventArgs e)
        {
            if (TxtSearchTID.Text.Length == 8)
            {
                LoadDataReqPaperResecipt(TxtSearchTID.Text);
            }
            else
            {
                LoadDataReqPaperResecipt("");
            }
        }

        private void btnSearchTID_Click(object sender, EventArgs e)
        {
            LoadDataReqPaperResecipt(TxtSearchTID.Text);
            isSearch = true;
        }

        /// <summary>
        /// Get index from current row.
        /// </summary>
        /// <returns>int : Current row index</returns>
        protected int iGetRowIndex()
        {
            if (dgReqPaper.CurrentRow == null)
                return 0;
            else
                return dgReqPaper.CurrentRow.Index;
        }
        private void timerRefresh_Tick(object sender, EventArgs e)
        {
            int iRowIndex = iGetRowIndex();
            int iIndex = dgReqPaper.FirstDisplayedScrollingRowIndex;

            if (isSearch)
                LoadDataReqPaperResecipt(TxtSearchTID.Text); 
            else
                LoadDataReqPaperResecipt("");

            if (dgReqPaper.RowCount > 0)
            {
                dgReqPaper[1, iRowIndex].Selected = true;
                //if (iIndex > -1)
                dgReqPaper.FirstDisplayedScrollingRowIndex = iIndex;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            int iRowIndex = iGetRowIndex();
            LoadDataReqPaperResecipt("");
            //dgReqPaper[1, iRowIndex].Selected = true;
            //dgReqPaper.Select();
            isSearch = false;
        }

        /// <summary>
        /// Get Columns for Init Log and set the type of each columns.
        /// </summary>
        /// <param name="oDGV">DataGridView : one which will be used</param>
        /// <returns>XmlColumn Array : Columns for Xml File</returns>
        private XmlColumn[] oGetColumnCollection(DataGridView oDGV)
        {
            XmlColumn[] oCol = new XmlColumn[oDGV.ColumnCount];
            for (int iCol = 0; iCol < oDGV.ColumnCount; iCol++)
                oCol[iCol] = new XmlColumn(oDGV.Columns[iCol].Width, XmlType.String);
            return oCol;
        }

        /// <summary>
        /// Export logs to file.
        /// </summary>
        /// <param name="oSqlComm">SqlCommand : Commands to get data from database</param>
        /// <param name="oDGV">DataGridView : DataGrid source will be exported </param>
        /// <param name="sFilePath">string : Path to save the file</param>
        /// <param name="sLog">string :LogDescription to add new audit log</param>
        private void ExportFile(SqlCommand oSqlComm, DataGridView oDGV, string sFilePath, string sLog)
        {
            try
            {
                // creating Excel Application  
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                // creating new WorkBook within Excel application  
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                // creating new Excelsheet in workbook  
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                // see the excel sheet behind the program  
                app.Visible = true;
                // get the reference of first sheet. By default its name is Sheet1.  
                // store its reference to worksheet  
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                // changing the name of active sheet  
                worksheet.Name = "Exported from gridview";
                // storing header part in Excel  
                for (int i = 1; i < oDGV.Columns.Count + 1; i++)
                {
                    worksheet.Cells[1, i] = oDGV.Columns[i - 1].HeaderText;
                }
                // storing Each row and column value to excel sheet  
                for (int i = 0; i < oDGV.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < oDGV.Columns.Count; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = oDGV.Rows[i].Cells[j].Value.ToString();
                    }
                }
                // save the application  
                workbook.SaveAs(sFilePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                // Exit from the application  
                //app.Quit();


                oDGV.Select();

                CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", sLog, "Saved as " + sFilePath);
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btnExportDetail_Click(object sender, EventArgs e)
        {

            if (sfdExport.ShowDialog() == DialogResult.OK)                
               ExportFile(new SqlCommand(CommonSP.sSPRequestPaperReceiptVIew, oSqlConn), dgReqPaper, sfdExport.FileName, CommonMessage.sRequestOrReceiptPaperExportDetail);
            dgReqPaper.Select();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        /// <summary>
        /// Delete Logs from oSourceDGV
        /// </summary>
        /// <param name="sStoredProcedure">string : Stored Procedure name</param>
        /// <param name="oSourceDGV">DataGridView : DataGrid source where logs will be deleted</param>
        /// <param name="sSavedLogsPath">string : Path where file for deleted logs saved</param>
        /// <param name="sLogMsg">string : Message saved in file</param>
        private void Delete(string sStoredProcedure, DataGridView oSourceDGV, string sSavedLogsPath, string sLogMsg)
        {
            try
            {
                FrmFilterInitTrail frmFilter = new FrmFilterInitTrail();
                frmFilter.Text = "Filter";
                if (frmFilter.ShowDialog() == DialogResult.OK && MessageBox.Show("Are you sure want to delete the logs?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    SqlParameter[] oSqlParameter = new SqlParameter[2];

                    oSqlParameter[0] = new SqlParameter("@dDateFrom", SqlDbType.DateTime);
                    oSqlParameter[0].Direction = ParameterDirection.Input;
                    oSqlParameter[0].Value = frmFilter.dateTimePickerFrom.Value;

                    oSqlParameter[1] = new SqlParameter("@dDateTo", SqlDbType.DateTime);
                    oSqlParameter[1].Direction = ParameterDirection.Input;
                    oSqlParameter[1].Value = frmFilter.dateTimePickerTo.Value;

                    SqlCommand oSqlComm = new SqlCommand(sStoredProcedure);
                    oSqlComm.CommandType = CommandType.StoredProcedure;
                    oSqlComm.Parameters.AddRange(oSqlParameter);

                    if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                    oSqlComm.Connection = oSqlConn;

                    SqlDataReader oReader = oSqlComm.ExecuteReader();

                    XmlExport oExport = new XmlExport("Sheet1");
                    oExport.Author = UserData.sUserID;
                    oExport.CreationTime = DateTime.Now;
                    oExport.Write(oGetColumnCollection(oSourceDGV), oReader, sSavedLogsPath);
                    oReader.Close();

                    //RefreshView();
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", sLogMsg, "Deleted Logs Saved at " + sSavedLogsPath);
                }
            }
            catch (Exception oException)
            {
                //File.Delete(sSavedLogsPath);
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>
        /// Get the path where DeleteLogs file will be saved.
        /// </summary>
        /// <param name="sFileName">string : Name for the file</param>
        /// <returns>string : File's path</returns>
        private string sGetDeleteLogPath(string sFileName)
        {
            string sDelLogDir = Application.StartupPath + "\\DELETE LOGS";
            if (!Directory.Exists(sDelLogDir)) Directory.CreateDirectory(sDelLogDir);
            return sDelLogDir + "\\RequestReceipt " + sFileName + " " + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00") + " " + DateTime.Now.Hour.ToString("00") + ";" + DateTime.Now.Minute.ToString("00") + ";" + DateTime.Now.Second.ToString("00") + "," + DateTime.Now.Millisecond.ToString("000") + ".xml";
        }

        private void btnDeleteDetail_Click(object sender, EventArgs e)
        {
            Delete(CommonSP.spProfileRequestOrReceiptPaperDelete, dgReqPaper, sGetDeleteLogPath("PaperDelete"), CommonMessage.sRequestOrReceiptPaperExportDelete);
        }
    }
}
