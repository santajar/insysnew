namespace InSys
{
    partial class FrmUnited
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUnited));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.sBarMonitor = new System.Windows.Forms.StatusBar();
            this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
            this.StatusTime = new System.Windows.Forms.StatusBarPanel();
            this.gbTerminal = new System.Windows.Forms.GroupBox();
            this.oDgTerminal = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.timerClock = new System.Windows.Forms.Timer(this.components);
            this.timerRefresh = new System.Windows.Forms.Timer(this.components);
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusTime)).BeginInit();
            this.gbTerminal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.oDgTerminal)).BeginInit();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // sBarMonitor
            // 
            this.sBarMonitor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sBarMonitor.Location = new System.Drawing.Point(0, 488);
            this.sBarMonitor.Name = "sBarMonitor";
            this.sBarMonitor.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.statusBarPanel1,
            this.StatusTime});
            this.sBarMonitor.ShowPanels = true;
            this.sBarMonitor.Size = new System.Drawing.Size(640, 22);
            this.sBarMonitor.SizingGrip = false;
            this.sBarMonitor.TabIndex = 2;
            this.sBarMonitor.Text = "1";
            // 
            // statusBarPanel1
            // 
            this.statusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.statusBarPanel1.Name = "statusBarPanel1";
            this.statusBarPanel1.Style = System.Windows.Forms.StatusBarPanelStyle.OwnerDraw;
            this.statusBarPanel1.Text = "statusBarPanel1";
            this.statusBarPanel1.Width = 576;
            // 
            // StatusTime
            // 
            this.StatusTime.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            this.StatusTime.Icon = ((System.Drawing.Icon)(resources.GetObject("StatusTime.Icon")));
            this.StatusTime.Name = "StatusTime";
            this.StatusTime.Text = "Time";
            this.StatusTime.Width = 64;
            // 
            // gbTerminal
            // 
            this.gbTerminal.Controls.Add(this.oDgTerminal);
            this.gbTerminal.Controls.Add(this.btnClose);
            this.gbTerminal.Location = new System.Drawing.Point(9, 9);
            this.gbTerminal.Name = "gbTerminal";
            this.gbTerminal.Size = new System.Drawing.Size(621, 434);
            this.gbTerminal.TabIndex = 0;
            this.gbTerminal.TabStop = false;
            this.gbTerminal.Text = "TERMINAL";
            // 
            // oDgTerminal
            // 
            this.oDgTerminal.AllowUserToAddRows = false;
            this.oDgTerminal.AllowUserToDeleteRows = false;
            this.oDgTerminal.AllowUserToResizeRows = false;
            this.oDgTerminal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.oDgTerminal.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.oDgTerminal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.oDgTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.oDgTerminal.DefaultCellStyle = dataGridViewCellStyle2;
            this.oDgTerminal.Location = new System.Drawing.Point(7, 19);
            this.oDgTerminal.MultiSelect = false;
            this.oDgTerminal.Name = "oDgTerminal";
            this.oDgTerminal.ReadOnly = true;
            this.oDgTerminal.RowHeadersVisible = false;
            this.oDgTerminal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.oDgTerminal.Size = new System.Drawing.Size(606, 407);
            this.oDgTerminal.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(449, 67);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(10, 10);
            this.btnClose.TabIndex = 1;
            this.btnClose.TabStop = false;
            this.btnClose.Text = "button1";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // timerClock
            // 
            this.timerClock.Enabled = true;
            this.timerClock.Tick += new System.EventHandler(this.timerClock_Tick);
            // 
            // timerRefresh
            // 
            this.timerRefresh.Enabled = true;
            this.timerRefresh.Interval = 3000;
            this.timerRefresh.Tick += new System.EventHandler(this.timerRefresh_Tick);
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnSearch);
            this.gbButton.Controls.Add(this.txtSearch);
            this.gbButton.Controls.Add(this.btnRefresh);
            this.gbButton.Controls.Add(this.btnView);
            this.gbButton.Location = new System.Drawing.Point(9, 445);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(621, 37);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnSearch
            // 
            this.btnSearch.AutoSize = true;
            this.btnSearch.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSearch.Location = new System.Drawing.Point(113, 10);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(51, 23);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSearch.Location = new System.Drawing.Point(7, 12);
            this.txtSearch.MaxLength = 8;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(100, 20);
            this.txtSearch.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(335, 10);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(136, 23);
            this.btnRefresh.TabIndex = 2;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnView
            // 
            this.btnView.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnView.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnView.Location = new System.Drawing.Point(477, 10);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(136, 23);
            this.btnView.TabIndex = 3;
            this.btnView.Text = "View Detail";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // FrmUnited
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(640, 510);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbTerminal);
            this.Controls.Add(this.sBarMonitor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmUnited";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "United";
            this.Load += new System.EventHandler(this.FrmUnited_Load);
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusTime)).EndInit();
            this.gbTerminal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.oDgTerminal)).EndInit();
            this.gbButton.ResumeLayout(false);
            this.gbButton.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.StatusBar sBarMonitor;
        internal System.Windows.Forms.StatusBarPanel StatusTime;
        private System.Windows.Forms.GroupBox gbTerminal;
        internal System.Windows.Forms.Timer timerClock;
        internal System.Windows.Forms.Timer timerRefresh;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.DataGridView oDgTerminal;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.StatusBarPanel statusBarPanel1;
    }
}