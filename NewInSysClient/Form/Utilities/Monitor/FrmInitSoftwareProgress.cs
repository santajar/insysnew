﻿using InSysClass;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Data.Odbc;

namespace InSys
{
    public partial class FrmInitSoftwareProgress : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        protected string sKey; 
        protected string sColoum;
        protected bool isSearch = true;
        protected string sCity, sGroup, sRegion, sTerminalID, sSoftware, sSoftwareDownload, sBuildNumber, sSerialNumber, sInstallStatus;
        protected int iPercentage;
        bool sFlag = false;
        int iTotalMessage = 0;

        public FrmInitSoftwareProgress(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        private void FrmInitSoftwareCon_Load(object sender, EventArgs e)
        {
            LoadData();
            DisplayTime();
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + "Init Software Progress", "");
        }

        protected void LoadData()
        {
            LoadData(null,null,null,null,null,null,null,null,null);
        }
        /// <summary>
        /// Load data and using sKey to filter the data.
        /// </summary>
        /// <param name="sKey">string : keyword to filter data</param>
        protected void LoadData(string sCity,string sGroup,string sRegion,string sTerminalID,string sSoftware,string sSoftwareDownload,string sBuildNumber,string sSerialNumber,string sInstallStatus)
        {
            try
            {
                int iIndexDGV=0;
                if (oDgvSoftwareTerminal.SelectedCells.Count>1)
                    iIndexDGV = oDgvSoftwareTerminal.CurrentRow.Index;
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareProgressBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                if ((!string.IsNullOrEmpty(sCity)) || (!string.IsNullOrEmpty(sGroup)) || (!string.IsNullOrEmpty(sRegion)) || (!string.IsNullOrEmpty(sTerminalID)) || (!string.IsNullOrEmpty(sSoftware)) || (!string.IsNullOrEmpty(sSoftwareDownload)) || (!string.IsNullOrEmpty(sBuildNumber)) || (!string.IsNullOrEmpty(sSerialNumber)) || (!string.IsNullOrEmpty(sInstallStatus)) || (chkStartTime.Checked == true) || (chkEndTime.Checked == true) || (chkFinishInstallTime.Checked == true))
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGetAllCond();
                
                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd);
                DataTable oDataTableInitLogConn = new DataTable();

                oAdapt.Fill(oDataTableInitLogConn);
                if (iIndexDGV > oDataTableInitLogConn.Rows.Count)
                {
                    oDgvSoftwareTerminal.CurrentCell = oDgvSoftwareTerminal.Rows[0].Cells[0];
                    sFlag = true;
                }
                else sFlag = false;
                oDgvSoftwareTerminal.DataSource = oDataTableInitLogConn;
                txtTotalOnProgress.Text = oDataTableInitLogConn.Select("InstallStatus = 'On Progress'").Length.ToString();
                txtTotalSucces.Text = oDataTableInitLogConn.Select("InstallStatus = 'Success'").Length.ToString();

                if (oDataTableInitLogConn.Rows.Count == 0)
                {
                    ResetAllParameter();
                }
            }
            catch (Exception)
            {
                //CommonClass.doWriteErrorFile(ex.Message);
                //if there's no connection, placing this would show error message box over and over everytime United tries to refresh its data
            }
        }

        /// <summary>
        /// Reset Parameter to Default
        /// </summary>
        private void ResetAllParameter()
        {
            txtCity.Text = "";
            txtGroup.Text = "";
            txtRegion.Text = "";
            txtTerminalID.Text = "";
            txtSoftware.Text = "";
            txtSoftwareDownload.Text = "";
            txtBuildNumber.Text = "";
            txtSerialNumber.Text = "";
            txtInstallStatus.Text = "";
            chkStartTime.Checked = false;
            chkEndTime.Checked = false;
            chkFinishInstallTime.Checked = false;
            string sDefaultValueTime = "11/1/2013";
            dtpSStartTime.Value = Convert.ToDateTime(sDefaultValueTime);
            dtpSEndTime.Value = Convert.ToDateTime(sDefaultValueTime);
            dtpSInstallTime.Value = Convert.ToDateTime(sDefaultValueTime);
            dtpEStartTime.Value = DateTime.Now;
            dtpEEndTime.Value = DateTime.Now;
            dtpEInstallTime.Value = DateTime.Now;
        }

        #region Condition
        /// <summary>
        /// Generate condition statement used to filter database
        /// </summary>
        /// <param name="sKey">string : value to filter database</param>
        /// <returns>string : filter statement</returns>
        protected string sGetAllCond()
        {
            string sAllCondition,sCondTemp;
            int i;

            sCondTemp = sGetCond(sCity,sGroup, sRegion, sTerminalID, sSoftware, sSoftwareDownload,sBuildNumber, sSerialNumber, sInstallStatus);

            sAllCondition = sCondTemp.Substring(0, sCondTemp.Length - 4);

            return sAllCondition;
        }

        /// <summary>
        /// Generate condition statement used to filter database
        /// </summary>
        /// <param name="sKey">string : value to filter database</param>
        /// <returns>string : filter statement</returns>
        protected string sGetCond(string sCity,string sGroup,string sRegion,string sTerminalID,string sSoftware,string sSoftwareDownload,string sBuildNumber,string sSerialNumber,string sInstallStatus)
        {
            return string.Format(
                " Where " + sGetCityCond(sCity) + sGetGroupCond(sGroup) + sGetRegionCond(sRegion) + sGetTerminalIDCond(sTerminalID) + sGetSoftwareCond(sSoftware) + sGetSoftwareDownloadCond(sSoftwareDownload) + sGetBuildNumberCond(sBuildNumber) + sGetSerialNumberCond(sSerialNumber) + sGetInstallStatusCond(sInstallStatus) + sGetStartTimeCond() + sGetEndTimeCond() + sGetFinishInstallTimeCond());
                ////"WHERE Time LIKE '%{0}%' OR TerminalID LIKE '%{0}%' OR Description LIKE '%{0}%' OR Percentage LIKE '%{0}%'",
                //"WHERE TerminalID LIKE '%{0}%'",
            
        }

        private string sGetBuildNumberCond(string sBuildNumber)
        {
            if (!string.IsNullOrEmpty(sBuildNumber))
                return string.Format(" BuildNumber like '%" + sBuildNumber + "%' and ");
            else
                return string.Format("");
        }

        private string sGetCityCond(string sCity)
        {
            if (!string.IsNullOrEmpty(sCity))
                return string.Format(" EdcCity like '%" + sCity + "%' and ");
            else
                return string.Format("");
        }

        private string sGetGroupCond(string sGroup)
        {
            if (!string.IsNullOrEmpty(sGroup))
                return string.Format(" EdcGroup like '%" + sGroup + "%' and ");
            else
                return string.Format("");
        }
        private string sGetRegionCond(string sRegion)
        {
            if (!string.IsNullOrEmpty(sRegion))
                return string.Format(" EdcRegion like '%" + sRegion + "%' and ");
            else
                return string.Format("");
        }
        private string sGetTerminalIDCond(string sTerminalID)
        {
            if (!string.IsNullOrEmpty(sTerminalID))
                return string.Format(" TerminalID like '%" + sTerminalID + "%' and ");
            else
                return string.Format("");
        }

        private string sGetSoftwareCond(string sSoftware)
        {
            if (!string.IsNullOrEmpty(sSoftware))
                return string.Format(" Software like '%" + sSoftware + "%' and ");
            else
                return string.Format("");
        }

        private string sGetSoftwareDownloadCond(string sSoftwareDownload)
        {
            if (!string.IsNullOrEmpty(sSoftwareDownload))
                return string.Format(" SoftwareDownload like '%" + sSoftwareDownload + "%' and ");
            else
                return string.Format("");
        }

        private string sGetSerialNumberCond(string sSerialNumber)
        {
            if (!string.IsNullOrEmpty(sSerialNumber))
                return string.Format(" SerialNumber like '%" + sSerialNumber + "%' and ");
            else
                return string.Format("");
        }

        private string sGetInstallStatusCond(string sInstallStatus)
        {
            if (!string.IsNullOrEmpty(sInstallStatus))
                return string.Format(" InstallStatus like '%" + sInstallStatus + "%' and ");
            else
                return string.Format("");
        }

        private string sGetStartTimeCond()
        {
            if (chkStartTime.Checked== true)
                return string.Format(" (datediff ( day, '" + dtpSStartTime.Value.ToString("MM/dd/yyyy") + "', StartTime) >= 0 AND datediff(day,StartTime, '" + dtpEStartTime.Value.ToString("MM/dd/yyyy") + "') >= 0) and ");
            else
                return string.Format("");
        }

        private string sGetEndTimeCond()
        {
            if (chkEndTime.Checked == true)
                return string.Format(" (datediff ( day, '" + dtpSEndTime.Value.ToString("MM/dd/yyyy") + "', EndTime) >= 0 AND datediff(day,EndTime, '" + dtpEEndTime.Value.ToString("MM/dd/yyyy") + "') >= 0) and ");
            else
                return string.Format("");
        }

        private string sGetFinishInstallTimeCond()
        {
            if (chkFinishInstallTime.Checked == true)
                return string.Format(" (datediff ( day, '" + dtpSInstallTime.Value.ToString("MM/dd/yyyy") + "', FinishInstallTime) >= 0 AND datediff(day,FinishInstallTime, '" + dtpEInstallTime.Value.ToString("MM/dd/yyyy") + "') >= 0) and ");
            else
                return string.Format("");
        }

        private string sGetCity()
        {
            if (!string.IsNullOrEmpty(txtCity.Text))
                sCity = txtCity.Text;
            else
                sCity = "";
            return sCity;
        }

        private string sGetBuildNumberCond()
        {
            if (!string.IsNullOrEmpty(txtBuildNumber.Text))
                sBuildNumber = txtBuildNumber.Text;
            else
                sBuildNumber = "";
            return sBuildNumber;
        }

        private string sGetInstallStatus()
        {
            if (!string.IsNullOrEmpty(txtInstallStatus.Text))
                sInstallStatus = txtInstallStatus.Text;
            else
                sInstallStatus = "";
            return sInstallStatus;
        }

        private string sGetSerialNumber()
        {
            if (!string.IsNullOrEmpty(txtSerialNumber.Text))
                sSerialNumber = txtSerialNumber.Text;
            else
                sSerialNumber = "";
            return sSerialNumber;
        }

        private string sGetSoftwareDownload()
        {
            if (!string.IsNullOrEmpty(txtSoftwareDownload.Text))
                sSoftwareDownload = txtSoftwareDownload.Text;
            else
                sSoftwareDownload = "";
            return sSoftwareDownload;
        }

        private string sGetSoftware()
        {
            if (!string.IsNullOrEmpty(txtSoftware.Text))
                sSoftware = txtSoftware.Text;
            else
                sSoftware = "";
            return sSoftware;
        }

        private string sGetTerminalID()
        {
            if (!string.IsNullOrEmpty(txtTerminalID.Text))
                sTerminalID = txtTerminalID.Text;
            else
                sTerminalID = "";
            return sTerminalID;
        }

        private string sGetRegion()
        {
            if (!string.IsNullOrEmpty(txtRegion.Text))
                sRegion = txtRegion.Text;
            else
                sRegion = "";
            return sRegion;
        }

        private string sGetGroup()
        {
            if (!string.IsNullOrEmpty(txtGroup.Text))
                sGroup = txtGroup.Text;
            else
                sGroup = "";
            return sGroup;
        }
        #endregion

        private void TimerRefresh_Tick(object sender, EventArgs e)
        {
            int iRowIndex=iGetRowIndex();            

            int iIndex = oDgvSoftwareTerminal.FirstDisplayedScrollingRowIndex;

            if (isSearch) LoadData(sGetCity(),sGetGroup(), sGetRegion(), sGetTerminalID(), sGetSoftware(), sGetSoftwareDownload(),sGetBuildNumberCond(), sGetSerialNumber(), sGetInstallStatus());
            else LoadData();

            if (sFlag == true)
                iRowIndex = 0;
            
            if (oDgvSoftwareTerminal.RowCount > 0) oDgvSoftwareTerminal[1, iRowIndex].Selected = true;
            if (iIndex > -1) oDgvSoftwareTerminal.FirstDisplayedScrollingRowIndex = iIndex;
        }
        
        /// <summary>
        /// Get index from current row.
        /// </summary>
        /// <returns>int : Current row index</returns>
        protected int iGetRowIndex()
        {
            if (oDgvSoftwareTerminal.CurrentRow == null)
                return 0;
            else
                return oDgvSoftwareTerminal.CurrentRow.Index;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.None;
            //if (txtSearch.Text.Trim().Length > 0)
            //{

            LoadData(sGetCity(), sGetGroup(), sGetRegion(), sGetTerminalID(), sGetSoftware(), sGetSoftwareDownload(), sGetBuildNumberCond(), sGetSerialNumber(), sGetInstallStatus());
                isSearch = true;
                sGetGroup();

            //}
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Set Display Time
        /// </summary>
        protected void DisplayTime()
        {
            StatusTime.Text = DateTime.Now.ToString("hh:mm:ss tt", new CultureInfo("en-US"));
        }

        private void TimerClock_Tick(object sender, EventArgs e)
        {
            DisplayTime();
        }

        private void chkStartTime_CheckedChanged(object sender, EventArgs e)
        {
            if (chkStartTime.Checked == true)
            {
                dtpSStartTime.Enabled = true;
                dtpEStartTime.Enabled = true;
            }
            else {
                dtpSStartTime.Enabled = false;
                dtpEStartTime.Enabled = false;
            }
        }

        private void chkEndTime_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEndTime.Checked == true)
            {
                dtpSEndTime.Enabled = true;
                dtpEEndTime.Enabled = true;
            }
            else
            {
                dtpSEndTime.Enabled = false;
                dtpEEndTime.Enabled = false;
            }
        }

        private void chkFinishInstallTime_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFinishInstallTime.Checked == true)
            {
                dtpSInstallTime.Enabled = true;
                dtpEInstallTime.Enabled = true;
            }
            else
            {
                dtpSInstallTime.Enabled = false;
                dtpEInstallTime.Enabled = false;
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SqlCommand oSqlCmd = GetFilterCommand();
            if (oSqlCmd != null)
            {
                try 
                {
                    sfdExport.Filter = "Excel|*.xls";
                    sfdExport.FileName = "";
                    if (sfdExport.ShowDialog() == DialogResult.OK)
                        using (DataTable dtTemp = new DataTable())
                        {
                            (new SqlDataAdapter(oSqlCmd)).Fill(dtTemp);
                            File.Delete(sfdExport.FileName);

                            File.Copy(string.Format(@"{0}\TEMPLATE\template.xls", Application.StartupPath), sfdExport.FileName);
                            ExcelOleDb oExcel = new ExcelOleDb(sfdExport.FileName);
                            oExcel.CreateWorkSheet(dtTemp);
                            oExcel.InsertRow(dtTemp);
                            oExcel.Dispose();
                        }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Your Computer using 64 bit. File will save in .xlsx");
                    File.Delete(sfdExport.FileName);
                    sfdExport.FileName = sfdExport.FileName.Substring(0,sfdExport.FileName.Length - 4) + ".xlsx";
                        using (DataTable dtTemp = new DataTable())
                        {
                            (new SqlDataAdapter(oSqlCmd)).Fill(dtTemp);
                            File.Delete(sfdExport.FileName);

                            File.Copy(string.Format(@"{0}\TEMPLATE\template.xlsx", Application.StartupPath), sfdExport.FileName);
                            ExcelOleDb oExcel = new ExcelOleDb(sfdExport.FileName);
                            oExcel.CreateWorkSheet(dtTemp);
                            oExcel.InsertRow(dtTemp);
                            oExcel.Dispose();
                        }
                }
                MessageBox.Show("Data Saved");
                CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", "Export form Init Software", "Saved as " + sfdExport.FileName);
            }
        }
        /// <summary>
        /// Refresh data with Filter
        /// </summary>
        private SqlCommand GetFilterCommand()
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareProgressBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            if ((!string.IsNullOrEmpty(sCity)) || (!string.IsNullOrEmpty(sGroup)) || (!string.IsNullOrEmpty(sRegion)) || (!string.IsNullOrEmpty(sTerminalID)) || (!string.IsNullOrEmpty(sSoftware)) || (!string.IsNullOrEmpty(sSoftwareDownload)) || (!string.IsNullOrEmpty(sBuildNumber)) || (!string.IsNullOrEmpty(sSerialNumber)) || (!string.IsNullOrEmpty(sInstallStatus)) || (chkStartTime.Checked == true) || (chkEndTime.Checked == true) || (chkFinishInstallTime.Checked == true))
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGetAllCond();
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            oSqlCmd.Connection = oSqlConn;
            return oSqlCmd;
        }
    }
}
