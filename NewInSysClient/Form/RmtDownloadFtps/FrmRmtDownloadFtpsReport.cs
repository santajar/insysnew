﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmRmtDownloadFtpsReport : Form
    {
        SqlConnection oSqlConn;
        string sUserID;
        SqlConnection oSqlConnAuditTrail;
        string sFilename = null;

        DataTable datatableRD_AuditDownload = new DataTable();

        public class CustomComboBoxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }

            public CustomComboBoxItem(string ItemText, object ItemValue)
            {
                Text = ItemText;
                Value = ItemValue;
            }
        }

        public FrmRmtDownloadFtpsReport(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = oSqlConnAuditTrail = _oSqlConn;
            sUserID = _sUserID;
        }

        private void FrmRmtDownloadFtpsReport_Load(object sender, EventArgs e)
        {
            dateTimePickerStart.Value = DateTime.Now.AddDays(-7);
            dateTimePickerEnd.Value = DateTime.Now;

            CustomComboBoxItem[] arrcomboBoxItem = {
                new CustomComboBoxItem("ALL", 0)                ,
                new CustomComboBoxItem("Software Install Success", 1),
                new CustomComboBoxItem("Software Install Failed", 2),
                new CustomComboBoxItem("SSL Download", 3)
            };
            comboBoxCompletionType.Items.AddRange(arrcomboBoxItem);
            comboBoxCompletionType.SelectedIndex = 0;

            Do_GetRdAuditDownload();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.AppStarting;
            groupBoxFilter.Enabled = groupBoxResult.Enabled = false;

            datatableRD_AuditDownload = (DataTable)dataGridViewTD_AuditDownload.DataSource;
            if (dataGridViewTD_AuditDownload.Rows.Count > 0)
            {
                SaveFileDialog savefile = new SaveFileDialog();
                savefile.Filter = "Csv Files(*.csv)|*.csv";
                if (savefile.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(savefile.FileName))
                {
                    Csv.Write(datatableRD_AuditDownload, savefile.FileName);
                }
            }

            groupBoxFilter.Enabled = groupBoxResult.Enabled = true;
            this.Cursor = Cursors.Default;
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            Do_GetRdAuditDownload();
        }

        private void Do_GetRdAuditDownload()
        {
            dataGridViewTD_AuditDownload.DataSource = null;
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_AuditDownloadBrowse, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = GetCondition();
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    if (reader.HasRows)
                    {
                        datatableRD_AuditDownload = new DataTable();
                        datatableRD_AuditDownload.Load(reader);
                        dataGridViewTD_AuditDownload.DataSource = datatableRD_AuditDownload;
                    }
            }
        }

        private object GetCondition()
        {
            string sCondition = null;
            string sTerminalID = txtBoxTerminalID.Text;
            string sSerialNumber = txtBoxSerialNumber.Text;
            string sSoftwareVersion = txtBoxSoftwareVersion.Text;
            string sCreateDateStart = dateTimePickerStart.Value.ToShortDateString().ToString();
            string sCreateDateEnd = dateTimePickerEnd.Value.ToShortDateString().ToString();
            int iCompletionType = (int)(comboBoxCompletionType.SelectedItem as CustomComboBoxItem).Value;

            sCondition = "WHERE ";
            sCondition = sCondition + string.Format("DATEDIFF(day,'{0}', CreateDate) >= 0 ", sCreateDateStart);
            sCondition = sCondition + string.Format("AND DATEDIFF(day, CreateDate,'{0}') >= 0 ", sCreateDateEnd);
            sCondition = sCondition + (!string.IsNullOrEmpty(sTerminalID) ? string.Format("AND TerminalID='{0}'", sTerminalID) : "");
            sCondition = sCondition + (!string.IsNullOrEmpty(sSerialNumber) ? string.Format("AND SerialNumber='{0}'", sSerialNumber) : "");
            sCondition = sCondition + (!string.IsNullOrEmpty(sSoftwareVersion) ? string.Format("AND SV_Name='{0}'", sSoftwareVersion) : "");
            switch(iCompletionType)
            {
                case 1:
                    sCondition = sCondition + string.Format("AND Filename LIKE '%.PRO' ");
                    break;
                case 2:
                    sCondition = sCondition + string.Format("AND Filename LIKE '%.FAI' ");
                    break;
                case 3:
                    sCondition = sCondition + string.Format("AND Filename LIKE '%.SSL' ");
                    break;
                default: break;
            }
            return sCondition;
        }
    }
}
