﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmRmtDownloadFtps : Form
    {
        SqlConnection oSqlConn;
        string sUserID;
        SqlConnection oSqlConnAuditTrail;
        bool bAdd = true;
        string sSV_Name;
        bool bAllTerminal;
        int iGroupID;
        DataTable dtRD_SwVersion = null;
        DataTable dtRD_SwVersionList = null;
        DataTable dtRD_Group = null;
        OpenFileDialog ofdUpload = new OpenFileDialog();

        public FrmRmtDownloadFtps(SqlConnection _oSqlConn, string _sUserID)
        {
            oSqlConn = oSqlConnAuditTrail = _oSqlConn;
            sUserID = _sUserID;
            InitializeComponent();
        }

        private void FrmRmtDownloadFtps_Load(object sender, EventArgs e)
        {
            EnableGroupBox(true);
            dataGridViewFilelist.RowCount = 1;
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddSwVersion_Click(object sender, EventArgs e)
        {
            EnableGroupBox(false);
            bAdd = true;
        }

        private void btnEditSwVersion_Click(object sender, EventArgs e)
        {
            bAdd = false;
            EnableGroupBox(false);
        }

        private void btnDelSwVersion_Click(object sender, EventArgs e)
        {
            if (dataGridViewSwVersion.SelectedRows != null && dataGridViewSwVersion.SelectedRows.Count > 0)
            {
                sSV_Name = dataGridViewSwVersion.SelectedRows[0].Cells[0].Value.ToString();
                if (!string.IsNullOrEmpty(sSV_Name))
                {
                    DeleteSwVersion();
                }
            }
            EnableGroupBox(true);
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            sSV_Name = txtSwVersionName.Text;

            bAllTerminal = chkUpgradeAll.Checked;
            iGroupID = comboBoxGroup.Items.Count > 0 ? comboBoxGroup.SelectedValue != null ? (int)comboBoxGroup.SelectedValue : 0 : 0;

            if (!string.IsNullOrEmpty(sSV_Name))
            {
                if (!bAdd) DeleteSwVersion();
                if (bAllTerminal) DeleteSwVersion(bAllTerminal);

                if (!IsDuplicateSwVersion(sSV_Name))
                {
                    panelSwVersionList.Enabled = false;
                    progressBarUploadFile.Style = ProgressBarStyle.Marquee;
                    backgroundWorkerUpload.RunWorkerAsync();
                }
                else
                    MessageBox.Show("Duplicate Software Version");
            }
        }

        private void btnUploadCancel_Click(object sender, EventArgs e)
        {
            backgroundWorkerUpload.CancelAsync();
            EnableGroupBox(true);
        }

        private void backgroundWorkerUpload_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (dataGridViewFilelist.RowCount > 0 && dataGridViewFilelist.Rows[0].Cells[2].Value != null)
                {
                    int iSV_ID = 0;
                    InsertSwVersion(ref iSV_ID);
                    foreach (DataGridViewRow row in dataGridViewFilelist.Rows)
                    {
                        if (!string.IsNullOrEmpty((string)row.Cells[2].Value))
                        {
                            string sFilename = row.Cells[2].Value.ToString();
                            string sFileRemote = string.Format(@"OUT\SOFTWARE\{0}\{1}", sSV_Name, sFilename);
                            string sFilepath = row.Cells[3].Value.ToString();
                            byte[] arrbFileContent = arrbReadFilecontent(sFilepath);

                            CommonClass.serviceInSys.UploadFileFtp(sFileRemote, arrbFileContent, arrbFileContent.Length);

                            // upload SV.txt to folder ALL
                            if (bAllTerminal)
                            {
                                CommonClass.serviceInSys.DeleteSubDirectoryFtp(@"OUT\RDS");
                                byte[] arrbSvName = Encoding.ASCII.GetBytes(sSV_Name);
                                CommonClass.serviceInSys.UploadFileFtp(string.Format(@"OUT\RDS\ALL\SV.txt"), arrbSvName, arrbSvName.Length);
                            }
                            InsertSwVersionList(iSV_ID, sFilename, arrbFileContent);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                InSysLogClass.Error(ex);
            }
        }

        private void backgroundWorkerUpload_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBarUploadFile.Style = ProgressBarStyle.Blocks;
            panelSwVersionList.Enabled = true;
            EnableGroupBox(true);
            bAdd = true;
        }

        private void dataGridViewFilelist_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridViewFilelist.Focus();
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dataGridViewFilelist.Columns["gridbtnAddSoftware"].Index)
                {
                    ofdUpload.Filter = "All files|*.*";
                    ofdUpload.ShowDialog();
                    if (!string.IsNullOrEmpty(ofdUpload.FileName))
                    {
                        DataGridViewRow row = (DataGridViewRow)dataGridViewFilelist.Rows[0].Clone();
                        row.Cells[2].Value = ofdUpload.SafeFileName;
                        row.Cells[3].Value = ofdUpload.FileName;
                        dataGridViewFilelist.Rows.Add(row);
                    }
                }
                else if (e.ColumnIndex == dataGridViewFilelist.Columns["gridbtnRemSoftware"].Index)
                {
                    if (!string.IsNullOrEmpty((string)dataGridViewFilelist.Rows[e.RowIndex].Cells[2].Value))
                        dataGridViewFilelist.Rows.RemoveAt(e.RowIndex);
                }
            }
        }

        private void chkUpgradeGroup_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxGroup.Enabled = chkUpgradeGroup.Checked;
            if (chkUpgradeGroup.Checked)
            {
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
                chkUpgradeAll.Checked = false;
            }
        }

        private void chkUpgradeAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUpgradeAll.Checked) chkUpgradeGroup.Checked = false;
        }

        private void dataGridViewSwVersion_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewSwVersion.DataSource != null)
            {
                string sSV_Name = dataGridViewSwVersion.Rows[e.RowIndex].Cells["SV_Name"].Value.ToString();
                string sSV_ID = dataGridViewSwVersion.Rows[e.RowIndex].Cells["SV_ID"].Value.ToString();
                if (!string.IsNullOrEmpty(sSV_Name) && !string.IsNullOrEmpty(sSV_ID))
                {
                    txtSwVersionName.Text = sSV_Name;
                    if (dtRD_SwVersionList != null && dtRD_SwVersionList.Rows.Count > 0)
                    {
                        DataTable dtTemp = dtRD_SwVersion.Select(string.Format("SV_ID='{0}'", sSV_ID)).CopyToDataTable();
                        if (dtTemp != null && dtTemp.Rows.Count > 0)
                        {
                            chkUpgradeAll.CheckState = dtTemp.Rows[0]["ALL"].ToString().ToLower() == "true" ? CheckState.Checked : CheckState.Unchecked;
                            int iGroupID = int.Parse(dtTemp.Rows[0]["GROUP_ID"].ToString());
                            if (iGroupID > 0)
                            {
                                chkUpgradeGroup.CheckState = CheckState.Checked;
                                comboBoxGroup.SelectedValue = iGroupID;
                            }

                            DataTable dtTempList = dtRD_SwVersionList.Select(string.Format("SV_ID='{0}'", sSV_ID)).CopyToDataTable();
                            if (dtTempList != null && dtTempList.Rows.Count > 0)
                            {
                                dataGridViewFilelist.DataSource = dtTempList;
                                foreach (DataGridViewColumn col in dataGridViewFilelist.Columns)
                                    switch (col.HeaderText)
                                    {
                                        case "SV_ID":
                                        case "SVList_Content":
                                            dataGridViewFilelist.Columns[col.HeaderText].Visible = false;
                                            break;
                                        default:
                                            break;
                                    }
                            }
                        }
                    }
                }
            }
        }

        protected void InsertSwVersionList(int iSV_ID, string sSV_Name, byte[] arrbFileContent)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_SwVersionListInsert, oSqlConn))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@iSV_ID", SqlDbType.Int).Value = iSV_ID;
                    sqlcmd.Parameters.Add("@sSVList_Name", SqlDbType.VarChar).Value = sSV_Name;
                    sqlcmd.Parameters.Add("@binSVList_Content", SqlDbType.VarBinary).Value = arrbFileContent;

                    sqlcmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                InSysLogClass.Error(ex);
            }
        }

        protected byte[] arrbReadFilecontent(string sFilepath)
        {
            FileStream fs = new FileStream(sFilepath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            long numbytes = new FileInfo(sFilepath).Length;
            return br.ReadBytes((int)numbytes);
        }

        protected void InsertSwVersion(ref int iSV_ID)
        {
            try
            {
                using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_SwVersionInsert, oSqlConn))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@sSV_Name", SqlDbType.VarChar).Value = sSV_Name;
                    sqlcmd.Parameters.Add("@bAllTerminal", SqlDbType.Bit).Value = bAllTerminal.ToString();
                    sqlcmd.Parameters.Add("@iGroupID", SqlDbType.Int).Value = iGroupID;
                    sqlcmd.Parameters.Add("@iID", SqlDbType.Int).Direction = ParameterDirection.Output;

                    sqlcmd.ExecuteNonQuery();
                    iSV_ID = (int)sqlcmd.Parameters["@iID"].Value;
                }
            }
            catch (Exception ex)
            {
                InSysLogClass.Error(ex);
            }
        }

        protected void InitSwVersionList()
        {
            dtRD_SwVersionList = new DataTable();
            dataGridViewFilelist.DataSource = null;
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_SwVersionListBrowse, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    if (reader.HasRows)
                    {
                        dtRD_SwVersionList.Load(reader);
                    }
            }
        }

        protected bool IsDuplicateSwVersion(string sSV_Name)
        {
            if (bAdd)
            {
                DataTable dtSwVersion_List = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_SwVersionBrowse, oSqlConn))
                {
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                        if (reader.HasRows)
                        {
                            dtSwVersion_List.Load(reader);
                            var row = dtSwVersion_List.Select(string.Format("SV_NAME='{0}'", sSV_Name));
                            if (row != null && row.Length > 0) return true;
                        }
                }
            }
            return false;
        }

        protected void InitSwVersion()
        {
            dtRD_SwVersion = new DataTable();
            dataGridViewSwVersion.DataSource = null;
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_SwVersionBrowse, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    if (reader.HasRows)
                    {
                        dtRD_SwVersion.Load(reader);
                        dataGridViewSwVersion.DataSource = dtRD_SwVersion;
                        foreach (DataGridViewColumn col in dataGridViewSwVersion.Columns)
                            if (col.HeaderText != "SV_NAME")
                                col.Visible = false;
                    }
            }
        }

        protected void EnableGroupBox(bool bEnable)
        {
            // initial condition dataGridViewFileList
            if (dataGridViewFilelist.Columns.Contains("Filename"))
                dataGridViewFilelist.Columns.Remove("Filename");
            if (dataGridViewFilelist.Columns.Contains("Path"))
                dataGridViewFilelist.Columns.Remove("Path");

            if (bAdd)
            {
                txtSwVersionName.Clear();
                chkUpgradeAll.Checked = chkUpgradeGroup.Checked = false;
            }

            if (bEnable)
            {
                InitSwVersion();
                InitSwVersionList();
            }
            else
            {
                if (!dataGridViewFilelist.Columns.Contains("Filename"))
                    dataGridViewFilelist.Columns.Add("Filename", "Filename");

                if (!dataGridViewFilelist.Columns.Contains("Path"))
                    dataGridViewFilelist.Columns.Add("Path", "Path");
            }

            groupBoxSwVersion.Enabled = bEnable;
            groupBoxSwVersionListFile.Enabled = !bEnable;

            dataGridViewFilelist.DataSource = null;
            dataGridViewFilelist.Rows.Clear();

            dataGridViewSwVersion.ClearSelection();
            dataGridViewSwVersion.CurrentCell = null;
        }

        protected void DeleteSwVersion()
        {
            DeleteSwVersion(false);
        }

        protected void DeleteSwVersion(bool bAllTerminal)
        {
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_SwVersionEnableDisable, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sSV_Name", SqlDbType.VarChar).Value = bAllTerminal ? "%" : sSV_Name;
                sqlcmd.Parameters.Add("@iEnable", SqlDbType.Int).Value = 0;
                sqlcmd.ExecuteNonQuery();
            }
        }
    }
}
