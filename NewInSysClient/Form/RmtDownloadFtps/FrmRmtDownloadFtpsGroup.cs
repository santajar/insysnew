﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmRmtDownloadFtpsGroup : Form
    {
        SqlConnection oSqlConn;
        string sUserID;
        SqlConnection oSqlConnAuditTrail;
        string sFilename = null;
        DataTable dtMID = null;
        OpenFileDialog openfileMID = new OpenFileDialog();

        public FrmRmtDownloadFtpsGroup(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = oSqlConnAuditTrail = _oSqlConn;
            sUserID = _sUserID;
        }

        private void FrmRmtDownloadFtpsGroup_Load(object sender, EventArgs e)
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
            AutoHeightDisplay();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAddGroup_Click(object sender, EventArgs e)
        {
            panelButton.Enabled = false;
            panelAddGroup.Visible = true;
            AutoHeightDisplay();
        }

        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.AppStarting;
            groupBoxRD_Group.Enabled = false;
            // execute disable RD_Group dan RD_GroupDetail
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            if (comboBoxGroup.SelectedItem!=null)
            {
                string sGroupName = (string)((DataRowView)comboBoxGroup.SelectedItem).Row["GROUP_NAME"];
                using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupEnableDisable, oSqlConn))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@sGroupName", SqlDbType.VarChar).Value = sGroupName;
                    sqlcmd.ExecuteNonQuery();
                }
            }
            //----------------------------------------
            groupBoxRD_Group.Enabled = true;
            this.Cursor = Cursors.Default;
            panelButton.Enabled = true;
            panelAddGroup.Visible = false;
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
            AutoHeightDisplay();
        }

        private void btnBrowseMidFilename_Click(object sender, EventArgs e)
        {
            openfileMID.Filter = "CSV File(*.csv)|*.csv";
            if (openfileMID.ShowDialog() == DialogResult.OK)
            {
                sFilename = openfileMID.FileName;
                txtMidFilename.Text = openfileMID.SafeFileName;
                Csv.Read(sFilename, ref dtMID);
                if (dtMID == null || dtMID.Rows.Count == 0)
                {
                    MessageBox.Show("No Records available. Please check file content");
                    sFilename = null;
                    txtMidFilename.Clear();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            panelButton.Enabled = true;
            panelAddGroup.Visible = false;
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
            AutoHeightDisplay();
        }

        private void btnUploadMidGroup_Click(object sender, EventArgs e)
        {
            string sGroupName = txtGroupName.Text;
            if (!string.IsNullOrEmpty(sGroupName) && !string.IsNullOrEmpty(sFilename))
            {
                this.Cursor = Cursors.AppStarting;
                groupBoxRD_Group.Enabled = false;

                // add upload MID list process
                int iGroupID = 0;
                SaveRd_Group(sGroupName, ref iGroupID);
                SaveRd_GroupDetail(iGroupID);
                // -------------------------------

                groupBoxRD_Group.Enabled = true;
                this.Cursor = Cursors.Default;
                panelButton.Enabled = true;
                panelAddGroup.Visible = false;
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPRD_GroupBrowse, "", "GROUP_ID", "GROUP_NAME", ref comboBoxGroup);
                AutoHeightDisplay();
            }
            else MessageBox.Show("Please fill GroupName and choose MID Filename");
        }

        private void comboBoxGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                dataGridViewGroupDetail.DataSource = null;
                ComboBox comboboxTemp = (ComboBox)sender;

                if (comboboxTemp != null && comboboxTemp.SelectedItem != null)
                {
                    int iGroupID = (int)((DataRowView)comboBoxGroup.SelectedItem).Row["GROUP_ID"];
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupDetailBrowse, oSqlConn))
                    {
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = $"WHERE a.[ENABLED]=1 AND a.GROUP_ID='{iGroupID}'";
                        using (SqlDataReader reader = sqlcmd.ExecuteReader())
                            if (reader.HasRows)
                            {
                                DataTable dtTemp = new DataTable();
                                dtTemp.Load(reader);

                                dataGridViewGroupDetail.DataSource = dtTemp;
                            }
                    }
                }
                else dataGridViewGroupDetail.DataSource = null;
            }
            catch(Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        protected void SaveRd_Group(string sGroupName, ref int iGroupID)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupInsert, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sGroupName", SqlDbType.VarChar).Value = sGroupName;
                sqlcmd.Parameters.Add("@iID", SqlDbType.Int).Direction = ParameterDirection.Output;

                sqlcmd.ExecuteNonQuery();

                iGroupID = (int)sqlcmd.Parameters["@iID"].Value;
            }
        }

        protected void SaveRd_GroupDetail(int iGroupID)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            foreach (DataRow row in dtMID.Rows)
            {
                string sMID = row[0].ToString();
                if (!string.IsNullOrEmpty(sMID) && long.Parse(sMID) > 0)
                    using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_GroupDetailInsert, oSqlConn))
                    {
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add("@iGroupID", SqlDbType.Int).Value = iGroupID;
                        sqlcmd.Parameters.Add("@sMID", SqlDbType.VarChar).Value = sMID;

                        sqlcmd.ExecuteNonQuery();
                    }
            }
        }

        protected void AutoHeightDisplay()
        {
            comboBoxGroup.SelectedIndex = -1;
            if (comboBoxGroup.Items.Count == 0)
            {
                dataGridViewGroupDetail.DataSource = null;
                comboBoxGroup.Text = null;
            }
            if (panelAddGroup.Visible) panelDataGridViewGroupDetail.Height = flowLayoutPanelGroupDetail.Height - panelAddGroup.Height - 6;
            else panelDataGridViewGroupDetail.Height = flowLayoutPanelGroupDetail.Height - 6;
        }
    }
}
