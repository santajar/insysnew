﻿namespace InSys
{
    partial class FrmRmtDownloadFtpsGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRmtDownloadFtpsGroup));
            this.btnAddGroup = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBoxRD_Group = new System.Windows.Forms.GroupBox();
            this.panelButton = new System.Windows.Forms.Panel();
            this.btnDeleteGroup = new System.Windows.Forms.Button();
            this.comboBoxGroup = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxRDGroup_Detail = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanelGroupDetail = new System.Windows.Forms.FlowLayoutPanel();
            this.panelAddGroup = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnUploadMidGroup = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMidFilename = new System.Windows.Forms.TextBox();
            this.btnBrowseMidFilename = new System.Windows.Forms.Button();
            this.txtGroupName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panelDataGridViewGroupDetail = new System.Windows.Forms.Panel();
            this.dataGridViewGroupDetail = new System.Windows.Forms.DataGridView();
            this.groupBoxRD_Group.SuspendLayout();
            this.panelButton.SuspendLayout();
            this.groupBoxRDGroup_Detail.SuspendLayout();
            this.flowLayoutPanelGroupDetail.SuspendLayout();
            this.panelAddGroup.SuspendLayout();
            this.panelDataGridViewGroupDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroupDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddGroup
            // 
            this.btnAddGroup.Location = new System.Drawing.Point(15, 65);
            this.btnAddGroup.Name = "btnAddGroup";
            this.btnAddGroup.Size = new System.Drawing.Size(127, 32);
            this.btnAddGroup.TabIndex = 0;
            this.btnAddGroup.Text = "Add";
            this.btnAddGroup.UseVisualStyleBackColor = true;
            this.btnAddGroup.Click += new System.EventHandler(this.btnAddGroup_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(299, 65);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(127, 32);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBoxRD_Group
            // 
            this.groupBoxRD_Group.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxRD_Group.Controls.Add(this.panelButton);
            this.groupBoxRD_Group.Controls.Add(this.groupBoxRDGroup_Detail);
            this.groupBoxRD_Group.Location = new System.Drawing.Point(12, 12);
            this.groupBoxRD_Group.Name = "groupBoxRD_Group";
            this.groupBoxRD_Group.Size = new System.Drawing.Size(447, 673);
            this.groupBoxRD_Group.TabIndex = 3;
            this.groupBoxRD_Group.TabStop = false;
            // 
            // panelButton
            // 
            this.panelButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelButton.Controls.Add(this.btnClose);
            this.panelButton.Controls.Add(this.btnDeleteGroup);
            this.panelButton.Controls.Add(this.btnAddGroup);
            this.panelButton.Controls.Add(this.comboBoxGroup);
            this.panelButton.Controls.Add(this.label1);
            this.panelButton.Location = new System.Drawing.Point(3, 18);
            this.panelButton.Name = "panelButton";
            this.panelButton.Size = new System.Drawing.Size(441, 117);
            this.panelButton.TabIndex = 4;
            // 
            // btnDeleteGroup
            // 
            this.btnDeleteGroup.Location = new System.Drawing.Point(157, 65);
            this.btnDeleteGroup.Name = "btnDeleteGroup";
            this.btnDeleteGroup.Size = new System.Drawing.Size(127, 32);
            this.btnDeleteGroup.TabIndex = 7;
            this.btnDeleteGroup.Text = "Delete";
            this.btnDeleteGroup.UseVisualStyleBackColor = true;
            this.btnDeleteGroup.Click += new System.EventHandler(this.btnDeleteGroup_Click);
            // 
            // comboBoxGroup
            // 
            this.comboBoxGroup.FormattingEnabled = true;
            this.comboBoxGroup.Location = new System.Drawing.Point(109, 16);
            this.comboBoxGroup.Name = "comboBoxGroup";
            this.comboBoxGroup.Size = new System.Drawing.Size(317, 24);
            this.comboBoxGroup.TabIndex = 4;
            this.comboBoxGroup.SelectedIndexChanged += new System.EventHandler(this.comboBoxGroup_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Group";
            // 
            // groupBoxRDGroup_Detail
            // 
            this.groupBoxRDGroup_Detail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxRDGroup_Detail.Controls.Add(this.flowLayoutPanelGroupDetail);
            this.groupBoxRDGroup_Detail.Location = new System.Drawing.Point(3, 135);
            this.groupBoxRDGroup_Detail.Name = "groupBoxRDGroup_Detail";
            this.groupBoxRDGroup_Detail.Size = new System.Drawing.Size(441, 535);
            this.groupBoxRDGroup_Detail.TabIndex = 8;
            this.groupBoxRDGroup_Detail.TabStop = false;
            // 
            // flowLayoutPanelGroupDetail
            // 
            this.flowLayoutPanelGroupDetail.Controls.Add(this.panelAddGroup);
            this.flowLayoutPanelGroupDetail.Controls.Add(this.panelDataGridViewGroupDetail);
            this.flowLayoutPanelGroupDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelGroupDetail.Location = new System.Drawing.Point(3, 18);
            this.flowLayoutPanelGroupDetail.Name = "flowLayoutPanelGroupDetail";
            this.flowLayoutPanelGroupDetail.Size = new System.Drawing.Size(435, 514);
            this.flowLayoutPanelGroupDetail.TabIndex = 6;
            // 
            // panelAddGroup
            // 
            this.panelAddGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelAddGroup.Controls.Add(this.btnCancel);
            this.panelAddGroup.Controls.Add(this.btnUploadMidGroup);
            this.panelAddGroup.Controls.Add(this.label2);
            this.panelAddGroup.Controls.Add(this.txtMidFilename);
            this.panelAddGroup.Controls.Add(this.btnBrowseMidFilename);
            this.panelAddGroup.Controls.Add(this.txtGroupName);
            this.panelAddGroup.Controls.Add(this.label3);
            this.panelAddGroup.Location = new System.Drawing.Point(3, 3);
            this.panelAddGroup.Name = "panelAddGroup";
            this.panelAddGroup.Size = new System.Drawing.Size(427, 138);
            this.panelAddGroup.TabIndex = 6;
            this.panelAddGroup.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(283, 92);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(127, 32);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnUploadMidGroup
            // 
            this.btnUploadMidGroup.Location = new System.Drawing.Point(16, 92);
            this.btnUploadMidGroup.Name = "btnUploadMidGroup";
            this.btnUploadMidGroup.Size = new System.Drawing.Size(127, 32);
            this.btnUploadMidGroup.TabIndex = 5;
            this.btnUploadMidGroup.Text = "Upload MID List";
            this.btnUploadMidGroup.UseVisualStyleBackColor = true;
            this.btnUploadMidGroup.Click += new System.EventHandler(this.btnUploadMidGroup_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Group Name";
            // 
            // txtMidFilename
            // 
            this.txtMidFilename.Location = new System.Drawing.Point(77, 48);
            this.txtMidFilename.Name = "txtMidFilename";
            this.txtMidFilename.ReadOnly = true;
            this.txtMidFilename.Size = new System.Drawing.Size(225, 22);
            this.txtMidFilename.TabIndex = 3;
            // 
            // btnBrowseMidFilename
            // 
            this.btnBrowseMidFilename.Location = new System.Drawing.Point(316, 43);
            this.btnBrowseMidFilename.Name = "btnBrowseMidFilename";
            this.btnBrowseMidFilename.Size = new System.Drawing.Size(94, 32);
            this.btnBrowseMidFilename.TabIndex = 4;
            this.btnBrowseMidFilename.Text = "Browse";
            this.btnBrowseMidFilename.UseVisualStyleBackColor = true;
            this.btnBrowseMidFilename.Click += new System.EventHandler(this.btnBrowseMidFilename_Click);
            // 
            // txtGroupName
            // 
            this.txtGroupName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGroupName.Location = new System.Drawing.Point(122, 9);
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Size = new System.Drawing.Size(288, 22);
            this.txtGroupName.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "MID List";
            // 
            // panelDataGridViewGroupDetail
            // 
            this.panelDataGridViewGroupDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelDataGridViewGroupDetail.Controls.Add(this.dataGridViewGroupDetail);
            this.panelDataGridViewGroupDetail.Location = new System.Drawing.Point(3, 147);
            this.panelDataGridViewGroupDetail.Name = "panelDataGridViewGroupDetail";
            this.panelDataGridViewGroupDetail.Size = new System.Drawing.Size(426, 364);
            this.panelDataGridViewGroupDetail.TabIndex = 8;
            // 
            // dataGridViewGroupDetail
            // 
            this.dataGridViewGroupDetail.AllowUserToAddRows = false;
            this.dataGridViewGroupDetail.AllowUserToDeleteRows = false;
            this.dataGridViewGroupDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGroupDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewGroupDetail.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewGroupDetail.Name = "dataGridViewGroupDetail";
            this.dataGridViewGroupDetail.ReadOnly = true;
            this.dataGridViewGroupDetail.RowHeadersVisible = false;
            this.dataGridViewGroupDetail.RowTemplate.Height = 24;
            this.dataGridViewGroupDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewGroupDetail.Size = new System.Drawing.Size(426, 364);
            this.dataGridViewGroupDetail.TabIndex = 7;
            // 
            // FrmRmtDownloadFtpsGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 697);
            this.Controls.Add(this.groupBoxRD_Group);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(493, 744);
            this.Name = "FrmRmtDownloadFtpsGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FTPS Remote Download - Terminal Group";
            this.Load += new System.EventHandler(this.FrmRmtDownloadFtpsGroup_Load);
            this.groupBoxRD_Group.ResumeLayout(false);
            this.panelButton.ResumeLayout(false);
            this.panelButton.PerformLayout();
            this.groupBoxRDGroup_Detail.ResumeLayout(false);
            this.flowLayoutPanelGroupDetail.ResumeLayout(false);
            this.panelAddGroup.ResumeLayout(false);
            this.panelAddGroup.PerformLayout();
            this.panelDataGridViewGroupDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroupDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddGroup;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBoxRD_Group;
        private System.Windows.Forms.ComboBox comboBoxGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelAddGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMidFilename;
        private System.Windows.Forms.Button btnBrowseMidFilename;
        private System.Windows.Forms.TextBox txtGroupName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelGroupDetail;
        private System.Windows.Forms.Button btnDeleteGroup;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnUploadMidGroup;
        private System.Windows.Forms.DataGridView dataGridViewGroupDetail;
        private System.Windows.Forms.GroupBox groupBoxRDGroup_Detail;
        private System.Windows.Forms.Panel panelDataGridViewGroupDetail;
        private System.Windows.Forms.Panel panelButton;
    }
}