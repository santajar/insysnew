﻿namespace InSys
{
    partial class FrmSoftwareProgressCPL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.pnlProgress = new System.Windows.Forms.Panel();
            this.dgProgress = new System.Windows.Forms.DataGridView();
            this.btnDetail = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tmRefresh = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pnlHeader.SuspendLayout();
            this.pnlProgress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProgress)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHeader.Controls.Add(this.textBox1);
            this.pnlHeader.Controls.Add(this.label1);
            this.pnlHeader.Location = new System.Drawing.Point(1, 4);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(918, 49);
            this.pnlHeader.TabIndex = 0;
            // 
            // pnlProgress
            // 
            this.pnlProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlProgress.Controls.Add(this.dgProgress);
            this.pnlProgress.Location = new System.Drawing.Point(1, 59);
            this.pnlProgress.Name = "pnlProgress";
            this.pnlProgress.Size = new System.Drawing.Size(918, 345);
            this.pnlProgress.TabIndex = 1;
            // 
            // dgProgress
            // 
            this.dgProgress.AllowUserToAddRows = false;
            this.dgProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgProgress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProgress.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.btnDetail});
            this.dgProgress.Location = new System.Drawing.Point(3, 3);
            this.dgProgress.Name = "dgProgress";
            this.dgProgress.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgProgress.Size = new System.Drawing.Size(912, 339);
            this.dgProgress.TabIndex = 0;
            this.dgProgress.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProgress_CellClick);
            this.dgProgress.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProgress_CellContentClick);
            // 
            // btnDetail
            // 
            this.btnDetail.HeaderText = "Detail";
            this.btnDetail.Name = "btnDetail";
            this.btnDetail.Text = "View Detail";
            this.btnDetail.UseColumnTextForButtonValue = true;
            // 
            // tmRefresh
            // 
            this.tmRefresh.Enabled = true;
            this.tmRefresh.Interval = 20000;
            this.tmRefresh.Tick += new System.EventHandler(this.tmRefresh_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Serach TID ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(82, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            // 
            // FrmSoftwareProgressCPL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 419);
            this.Controls.Add(this.pnlProgress);
            this.Controls.Add(this.pnlHeader);
            this.Name = "FrmSoftwareProgressCPL";
            this.Text = "FrmSoftwareProgressCPL";
            this.Load += new System.EventHandler(this.FrmSoftwareProgressCPL_Load);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlProgress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProgress)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Panel pnlProgress;
        public System.Windows.Forms.DataGridView dgProgress;
        private System.Windows.Forms.DataGridViewButtonColumn btnDetail;
        private System.Windows.Forms.Timer tmRefresh;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
    }
}