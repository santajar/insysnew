﻿namespace InSys
{
    partial class FrmAppPackageCPL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbAppPackageView = new System.Windows.Forms.GroupBox();
            this.dgAppPackage = new System.Windows.Forms.DataGridView();
            this.cmsAppPackage = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbSetting = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAppPackageDesc = new System.Windows.Forms.RichTextBox();
            this.txtPackageName = new System.Windows.Forms.TextBox();
            this.txtAppPackageFilename = new System.Windows.Forms.TextBox();
            this.txtBuiltNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbAppPackageView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAppPackage)).BeginInit();
            this.cmsAppPackage.SuspendLayout();
            this.gbSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbAppPackageView
            // 
            this.gbAppPackageView.Controls.Add(this.dgAppPackage);
            this.gbAppPackageView.Location = new System.Drawing.Point(12, 12);
            this.gbAppPackageView.Name = "gbAppPackageView";
            this.gbAppPackageView.Size = new System.Drawing.Size(818, 276);
            this.gbAppPackageView.TabIndex = 0;
            this.gbAppPackageView.TabStop = false;
            // 
            // dgAppPackage
            // 
            this.dgAppPackage.AllowUserToAddRows = false;
            this.dgAppPackage.AllowUserToDeleteRows = false;
            this.dgAppPackage.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgAppPackage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAppPackage.ContextMenuStrip = this.cmsAppPackage;
            this.dgAppPackage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgAppPackage.Location = new System.Drawing.Point(3, 16);
            this.dgAppPackage.Name = "dgAppPackage";
            this.dgAppPackage.ReadOnly = true;
            this.dgAppPackage.RowTemplate.ContextMenuStrip = this.cmsAppPackage;
            this.dgAppPackage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAppPackage.Size = new System.Drawing.Size(812, 257);
            this.dgAppPackage.TabIndex = 0;
            // 
            // cmsAppPackage
            // 
            this.cmsAppPackage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.cmsAppPackage.Name = "contextMenuStrip1";
            this.cmsAppPackage.Size = new System.Drawing.Size(108, 70);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // gbSetting
            // 
            this.gbSetting.Controls.Add(this.label4);
            this.gbSetting.Controls.Add(this.txtBuiltNumber);
            this.gbSetting.Controls.Add(this.btnCancel);
            this.gbSetting.Controls.Add(this.btnAdd);
            this.gbSetting.Controls.Add(this.btnBrowse);
            this.gbSetting.Controls.Add(this.label3);
            this.gbSetting.Controls.Add(this.label2);
            this.gbSetting.Controls.Add(this.label1);
            this.gbSetting.Controls.Add(this.txtAppPackageDesc);
            this.gbSetting.Controls.Add(this.txtPackageName);
            this.gbSetting.Controls.Add(this.txtAppPackageFilename);
            this.gbSetting.Location = new System.Drawing.Point(15, 294);
            this.gbSetting.Name = "gbSetting";
            this.gbSetting.Size = new System.Drawing.Size(815, 164);
            this.gbSetting.TabIndex = 1;
            this.gbSetting.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(480, 105);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(399, 105);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 7;
            this.btnAdd.Text = "SAVE";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(399, 21);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 6;
            this.btnBrowse.Text = "BROWSE";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Software Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Package FileName";
            // 
            // txtAppPackageDesc
            // 
            this.txtAppPackageDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAppPackageDesc.Location = new System.Drawing.Point(161, 105);
            this.txtAppPackageDesc.Name = "txtAppPackageDesc";
            this.txtAppPackageDesc.Size = new System.Drawing.Size(199, 53);
            this.txtAppPackageDesc.TabIndex = 3;
            this.txtAppPackageDesc.Text = "";
            // 
            // txtPackageName
            // 
            this.txtPackageName.Location = new System.Drawing.Point(161, 45);
            this.txtPackageName.Name = "txtPackageName";
            this.txtPackageName.Size = new System.Drawing.Size(199, 20);
            this.txtPackageName.TabIndex = 1;
            // 
            // txtAppPackageFilename
            // 
            this.txtAppPackageFilename.Location = new System.Drawing.Point(161, 19);
            this.txtAppPackageFilename.Name = "txtAppPackageFilename";
            this.txtAppPackageFilename.Size = new System.Drawing.Size(199, 20);
            this.txtAppPackageFilename.TabIndex = 0;
            // 
            // txtBuiltNumber
            // 
            this.txtBuiltNumber.Location = new System.Drawing.Point(161, 75);
            this.txtBuiltNumber.Name = "txtBuiltNumber";
            this.txtBuiltNumber.Size = new System.Drawing.Size(31, 20);
            this.txtBuiltNumber.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Built Number";
            // 
            // FrmAppPackageCPL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 470);
            this.Controls.Add(this.gbSetting);
            this.Controls.Add(this.gbAppPackageView);
            this.Name = "FrmAppPackageCPL";
            this.Text = "FrmAppPackageCPL";
            this.Load += new System.EventHandler(this.FrmAppPackageCPL_Load);
            this.gbAppPackageView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgAppPackage)).EndInit();
            this.cmsAppPackage.ResumeLayout(false);
            this.gbSetting.ResumeLayout(false);
            this.gbSetting.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbAppPackageView;
        private System.Windows.Forms.DataGridView dgAppPackage;
        private System.Windows.Forms.GroupBox gbSetting;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtAppPackageDesc;
        private System.Windows.Forms.TextBox txtPackageName;
        private System.Windows.Forms.TextBox txtAppPackageFilename;
        private System.Windows.Forms.ContextMenuStrip cmsAppPackage;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBuiltNumber;
    }
}