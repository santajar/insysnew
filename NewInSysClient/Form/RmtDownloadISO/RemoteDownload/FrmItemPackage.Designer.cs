﻿namespace InSys
{
    partial class FrmItemPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmItemPackage));
            this.dtpTime = new System.Windows.Forms.DateTimePicker();
            this.lblGroup = new System.Windows.Forms.Label();
            this.lblScheduleType = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.txtPackageName = new System.Windows.Forms.TextBox();
            this.cmbScheduleType = new System.Windows.Forms.ComboBox();
            this.gbList = new System.Windows.Forms.GroupBox();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.lblList2 = new System.Windows.Forms.Label();
            this.lblList1 = new System.Windows.Forms.Label();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbList.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtpTime
            // 
            this.dtpTime.CustomFormat = "HH:mm:ss";
            this.dtpTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTime.Location = new System.Drawing.Point(112, 61);
            this.dtpTime.Name = "dtpTime";
            this.dtpTime.ShowUpDown = true;
            this.dtpTime.Size = new System.Drawing.Size(67, 20);
            this.dtpTime.TabIndex = 5;
            this.dtpTime.Value = new System.DateTime(2015, 10, 20, 0, 0, 0, 0);
            // 
            // lblGroup
            // 
            this.lblGroup.AutoSize = true;
            this.lblGroup.Location = new System.Drawing.Point(12, 13);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Size = new System.Drawing.Size(67, 13);
            this.lblGroup.TabIndex = 0;
            this.lblGroup.Text = "Group Name";
            // 
            // lblScheduleType
            // 
            this.lblScheduleType.AutoSize = true;
            this.lblScheduleType.Location = new System.Drawing.Point(12, 38);
            this.lblScheduleType.Name = "lblScheduleType";
            this.lblScheduleType.Size = new System.Drawing.Size(79, 13);
            this.lblScheduleType.TabIndex = 1;
            this.lblScheduleType.Text = "Schedule Type";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(12, 63);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(30, 13);
            this.lblTime.TabIndex = 2;
            this.lblTime.Text = "Time";
            // 
            // txtPackageName
            // 
            this.txtPackageName.Location = new System.Drawing.Point(112, 9);
            this.txtPackageName.Name = "txtPackageName";
            this.txtPackageName.Size = new System.Drawing.Size(199, 20);
            this.txtPackageName.TabIndex = 3;
            // 
            // cmbScheduleType
            // 
            this.cmbScheduleType.FormattingEnabled = true;
            this.cmbScheduleType.Items.AddRange(new object[] {
            "Immidiate Download",
            "Schedule Download"});
            this.cmbScheduleType.Location = new System.Drawing.Point(112, 35);
            this.cmbScheduleType.Name = "cmbScheduleType";
            this.cmbScheduleType.Size = new System.Drawing.Size(199, 21);
            this.cmbScheduleType.TabIndex = 4;
            this.cmbScheduleType.SelectedIndexChanged += new System.EventHandler(this.cmbScheduleType_SelectedIndexChanged);
            // 
            // gbList
            // 
            this.gbList.Controls.Add(this.btnLeft);
            this.gbList.Controls.Add(this.btnRight);
            this.gbList.Controls.Add(this.lblList2);
            this.gbList.Controls.Add(this.lblList1);
            this.gbList.Controls.Add(this.listBox2);
            this.gbList.Controls.Add(this.listBox1);
            this.gbList.Location = new System.Drawing.Point(12, 87);
            this.gbList.Name = "gbList";
            this.gbList.Size = new System.Drawing.Size(390, 374);
            this.gbList.TabIndex = 6;
            this.gbList.TabStop = false;
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(181, 180);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(29, 23);
            this.btnLeft.TabIndex = 8;
            this.btnLeft.Text = "<";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(181, 151);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(29, 23);
            this.btnRight.TabIndex = 7;
            this.btnRight.Text = ">";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // lblList2
            // 
            this.lblList2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblList2.Location = new System.Drawing.Point(223, 17);
            this.lblList2.Name = "lblList2";
            this.lblList2.Size = new System.Drawing.Size(161, 23);
            this.lblList2.TabIndex = 3;
            this.lblList2.Text = "Package";
            this.lblList2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblList1
            // 
            this.lblList1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblList1.Location = new System.Drawing.Point(6, 17);
            this.lblList1.Name = "lblList1";
            this.lblList1.Size = new System.Drawing.Size(161, 23);
            this.lblList1.TabIndex = 2;
            this.lblList1.Text = "Software";
            this.lblList1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(223, 39);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(161, 329);
            this.listBox2.TabIndex = 1;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(6, 40);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(161, 329);
            this.listBox1.TabIndex = 0;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnDelete);
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(12, 460);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(390, 49);
            this.gbButton.TabIndex = 4;
            this.gbButton.TabStop = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(208, 13);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(85, 27);
            this.btnDelete.TabIndex = 19;
            this.btnDelete.Text = "Delete";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(299, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 27);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(117, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 27);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmItemPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 512);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbList);
            this.Controls.Add(this.dtpTime);
            this.Controls.Add(this.cmbScheduleType);
            this.Controls.Add(this.txtPackageName);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblScheduleType);
            this.Controls.Add(this.lblGroup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmItemPackage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Item Package";
            this.Load += new System.EventHandler(this.FrmItemPackage_Load);
            this.gbList.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGroup;
        private System.Windows.Forms.Label lblScheduleType;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.TextBox txtPackageName;
        private System.Windows.Forms.ComboBox cmbScheduleType;
        private System.Windows.Forms.GroupBox gbList;
        private System.Windows.Forms.Label lblList2;
        private System.Windows.Forms.Label lblList1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DateTimePicker dtpTime;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnRight;

    }
}