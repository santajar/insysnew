﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;
using System.Globalization;
using System.Drawing;

namespace InSys
{
    public partial class FrmItemPackage : Form
    {
        SqlConnection oConn;
        string sUserID;
        string sGroupName;
        bool isEdit = false;
        string sType;
        private DataTable dtList1;
        private DataTable dtList2;
        string sAddVariable="";
        string sDelVariable="";

        public FrmItemPackage(SqlConnection _oConn, string _sUID,string _GroupName, bool _isEdit, string _sType)
        {
            oConn = _oConn;
            sUserID = _sUID;
            isEdit = _isEdit;
            sType = _sType;
            sGroupName = _GroupName;
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmItemPackage_Load(object sender, EventArgs e)
        {
            
            loadForm();
        }

        private void loadForm()
        {
            txtPackageName.Enabled = true;
            if (isEdit)
            {
                CommonClass.InputLog(oConn, "", sUserID, "", CommonMessage.sFormOpened + sType, "");
                btnDelete.Enabled = true;
            }
            else
            {
                CommonClass.InputLog(oConn, "", sUserID, "", "Add New " + sType, "");
                btnDelete.Enabled = false;
            }
            if (sType == "Software")
            {
                SetSoftwareDisplay();

                GetSoftwareList();
                GetPackageList();                
            }
            else
            {
                SetTerminalIDDisplay();

                GetTerminalID();
                GetListTerminalID();
            }

        }

        #region Function TerminalID
        private void GetListTerminalID()
        {
            dtList2 = LoadDataListTerminalID();
            if (dtList2 != null)
            {
                listBox2.DataSource = dtList2;
                listBox2.DisplayMember = "TerminalID";
                listBox2.ValueMember = "TerminalID";
                listBox2.ClearSelected();
            }
        }

        private DataTable LoadDataListTerminalID()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPListTerminalIDPackageBrowse, oConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE GroupName = '{0}'",sGroupName);
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        oAdapt.Fill(dtTable);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }

            return dtTable;
        }

        private void GetTerminalID()
        {
            dtList1 = LoadDataTerminalID();
            if (dtList1 != null)
            {
                listBox1.DataSource = dtList1;
                listBox1.DisplayMember = "TerminalID";
                listBox1.ValueMember = "TerminalID";
                listBox1.ClearSelected();
            }
        }

        private DataTable LoadDataTerminalID()
        {
            DataTable dtTable = new DataTable();
            try
            {

                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE TerminalID NOT IN (SELECT TerminalID FROM tbListTerminalIDPackage) AND DatabaseID = '{0}'",cmbScheduleType.SelectedIndex == -1 ? "": cmbScheduleType.SelectedValue.ToString());
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        oAdapt.Fill(dtTable);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }

            return dtTable;
        }

        private void SetTerminalIDDisplay()
        {
            lblList1.Text = "TerminalID";
            lblList2.Text = "List TerminalID";
            lblScheduleType.Text = "DatabaseID";
            setCmbDatabaseID(); 
            lblTime.Visible = false;
            dtpTime.Visible = false;
            gbList.Location = new Point(12, 62);
            gbButton.Location = new Point(12, 442);
            this.Size = new Size(430, 533);
            this.Text = "Form TerminalID Package";
            txtPackageName.Enabled = false;
            txtPackageName.Text = sGroupName;
        }

        private void setCmbDatabaseID()
        {
            cmbScheduleType.DataSource = dtLoadVersion();
            if (cmbScheduleType.DataSource != null)
            {
                cmbScheduleType.DisplayMember = "DatabaseName";
                cmbScheduleType.ValueMember = "DatabaseID";
                cmbScheduleType.SelectedIndex = -1;
            }
        }

        private DataTable dtLoadVersion()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "";
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                        oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }

            return dtTable;
        }

        #endregion


        #region Function Software
        private void SetSoftwareDisplay()
        {
            lblList1.Text = "Software";
            lblList2.Text = "List Software";
            lblScheduleType.Visible = true;
            cmbScheduleType.Visible = true;
            lblTime.Visible = true;
            dtpTime.Visible = true;
            gbList.Location = new Point(12, 87);
            gbButton.Location = new Point(12, 460);
            this.Size = new Size(430, 550);
            this.Text = "Form Software Package";
        }

        private void GetPackageList()
        {
            dtList2 = LoadDataList2();
            if (dtList2 != null)
            {
                listBox2.DataSource = dtList2;
                listBox2.DisplayMember = "SoftwareName";
                listBox2.ValueMember = "SoftwareName";
                listBox2.ClearSelected();

                if (isEdit == true)
                {
                    txtPackageName.Enabled = false;
                    txtPackageName.Text = dtList2.Rows[0][1].ToString();
                    cmbScheduleType.SelectedText = dtList2.Rows[0][3].ToString() == "01" ? "Immidiate Download" : "Schedule Download";
                    string sTime = dtList2.Rows[0][4].ToString();
                    sTime = sTime.Substring(0, 2) + ":" + sTime.Substring(2, 2) + ":" + sTime.Substring(4, 2);
                    dtpTime.Value = DateTime.ParseExact(sTime, "HH:mm:ss", CultureInfo.InvariantCulture);
                    if (cmbScheduleType.Text == "Immidiate Download")
                        dtpTime.Enabled = false;
                }
            }
        }

        private DataTable LoadDataList2()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareGroupPackageBrowse, oConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE GroupName = '{0}'", sGroupName);
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        oAdapt.Fill(dtTable);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }

            return dtTable;
        }

        private void GetSoftwareList()
        {
            dtList1 = LoadDataList1();
            if (dtList1 != null)
            {
                listBox1.DataSource = dtList1;
                listBox1.DisplayMember = "Software Name";
                listBox1.ValueMember = "Software Name";
                listBox1.ClearSelected();
            }
        }

        private DataTable LoadDataList1()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareGroupAvailablePackageBrowse, oConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sGroupName", SqlDbType.VarChar).Value = sGroupName;
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        oAdapt.Fill(dtTable);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }

            return dtTable;
        }

        private void SaveSoftware()
        {
            if (!string.IsNullOrEmpty(txtPackageName.Text))
            {
                if (!string.IsNullOrEmpty(cmbScheduleType.Text))
                {
                    if (!isEdit)
                    {
                        if (!isGroupAlreadyExist(txtPackageName.Text))
                        {
                            DataSaved();
                        }
                        else
                            MessageBox.Show("Group Name Already Exist.");
                    }else
                         DataSaved();
                }
                else
                    MessageBox.Show("Please Choose Schedule Type.");
            }
            else
                MessageBox.Show("Please Fill Group Name.");
        }

        private bool isGroupAlreadyExist(string sGroupName)
        {
            bool bReturn = false;
            
            SqlCommand oCmd = new SqlCommand(string.Format("SELECT * FROM tbSoftwareGroupPackage WHERE GroupName = '{0}'",sGroupName),oConn);
            DataTable dtTemp = new DataTable();
            if (oConn.State == ConnectionState.Closed) oConn.Open();
            new SqlDataAdapter(oCmd).Fill(dtTemp);

            if (dtTemp.Rows.Count > 0)
                bReturn = true;
            else
                bReturn = false;

            return bReturn;
        }

        private void DataSaved()
        {
            try
            {
                if (sType == "Software")
                {
                    doDelete();
                    for (int iCount = 0; iCount < listBox2.Items.Count; iCount++)
                        doSave(((DataRowView)listBox2.Items[iCount])["SoftwareName"].ToString());
                    if (!string.IsNullOrEmpty(sAddVariable))
                        CommonClass.InputLog(oConn, "", sUserID, "", "Add Software List", "Add " + sType + ":" + sAddVariable);
                    if (!string.IsNullOrEmpty (sDelVariable))
                        CommonClass.InputLog(oConn, "", sUserID, "", "Add Software List", "Remove " + sType + ":" + sDelVariable);
                }
                else
                {
                    doDeleteTerminalID();
                    for (int iCount = 0; iCount < listBox2.Items.Count; iCount++)
                        doSaveTerminalID(((DataRowView)listBox2.Items[iCount])["TerminalID"].ToString());
                    if (!string.IsNullOrEmpty(sAddVariable))
                        CommonClass.InputLog(oConn, "", sUserID, "", "Add Software List", "Add " + sType + ":" + sAddVariable);
                    if (!string.IsNullOrEmpty(sDelVariable))
                        CommonClass.InputLog(oConn, "", sUserID, "", "Add Software List", "Remove " + sType + ":" + sDelVariable);
                    
                    SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET RemoteDownload = 1 WHERE TerminalID IN (SELECT TerminalID FROM tbListTerminalIDPackage WHERE GroupName = '{0}')", txtPackageName.Text), oConn);
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    oCmd.ExecuteNonQuery();
                }
                this.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void doDeleteTerminalID()
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPListTerminalIDPackageDelete, oConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE GroupName = '{0}'",sGroupName);
            if (oConn.State == ConnectionState.Closed)
                oConn.Open();
            oSqlCmd.ExecuteNonQuery();
        }

        private void doSaveTerminalID( string sTerminalID)
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPListTerminalIDPackageInsert, oConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sGroupName", SqlDbType.VarChar).Value = txtPackageName.Text;
            oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
            if (oConn.State == ConnectionState.Closed)
                oConn.Open();
            oSqlCmd.ExecuteNonQuery();
        }

        private void doSave(string sSoftwareName)
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareGroupPackageInsert, oConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sGroupName", SqlDbType.VarChar).Value = txtPackageName.Text;
            oSqlCmd.Parameters.Add("@sSoftwareName", SqlDbType.VarChar).Value = sSoftwareName;
            oSqlCmd.Parameters.Add("@sScheduleType", SqlDbType.VarChar).Value = cmbScheduleType.Text == "Immidiate Download" ? "01" : "02";
            oSqlCmd.Parameters.Add("@sScheduleTime", SqlDbType.VarChar).Value = dtpTime.Value.ToString("HHmmss");
            if (oConn.State == ConnectionState.Closed)
                oConn.Open();
            oSqlCmd.ExecuteNonQuery();
        }

        private void doDelete()
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareGroupPackageDelete, oConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sGroupName", SqlDbType.VarChar).Value = txtPackageName.Text;
            if (oConn.State == ConnectionState.Closed)
                oConn.Open();
            oSqlCmd.ExecuteNonQuery();
        }

        protected void AddRemoveApps(ref DataTable oTableAdd, ref DataTable oTableDel, string sColName,
                                         string sAppsName, DataRowView drvView)
        {
            DataRow drRow = oTableAdd.NewRow();
            drRow[sColName] = sAppsName;
            oTableAdd.Rows.Add(drRow);

            oTableDel.Rows.Remove(drvView.Row);

            listBox1.Refresh();
            listBox2.Refresh();
        }
        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (sType == "Software")
                SaveSoftware();
            else
                doSaveTerminalID();
        }

        private void doSaveTerminalID()
        {
            if (!string.IsNullOrEmpty(txtPackageName.Text))
            {
                DataSaved();
            }
            else
                MessageBox.Show("Please Fill Group Name.");
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndices.Count == 1)
            {
                if (!string.IsNullOrEmpty(sAddVariable))
                    sAddVariable = sAddVariable.Replace(listBox1.SelectedValue.ToString() + ",", "").Replace(listBox1.SelectedValue.ToString(), "");
                sAddVariable += listBox1.SelectedValue.ToString() + ",";

                if (sType == "Software")
                {
                    AddRemoveApps(ref dtList2, ref dtList1, "SoftwareName",
                                  listBox1.SelectedValue.ToString(), (DataRowView)listBox1.SelectedItem);
                }
                else
                {
                    AddRemoveApps(ref dtList2, ref dtList1, "TerminalID",
                                  listBox1.SelectedValue.ToString(), (DataRowView)listBox1.SelectedItem);
                }
            }
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedIndices.Count == 1)
            {
                if (!string.IsNullOrEmpty(sDelVariable))
                    sDelVariable = sDelVariable.Replace(listBox2.SelectedValue.ToString() + ",", "").Replace(listBox2.SelectedValue.ToString(), "");
                sDelVariable += listBox2.SelectedValue.ToString() + ",";

                if (sType == "Software")
                {
                    AddRemoveApps(ref dtList1, ref dtList2, "Software Name",
                                  listBox2.SelectedValue.ToString(), (DataRowView)listBox2.SelectedItem);
                }
                else
                {
                    AddRemoveApps(ref dtList1, ref dtList2, "TerminalID",
                                  listBox2.SelectedValue.ToString(), (DataRowView)listBox2.SelectedItem);
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //SqlTransaction oTransaction = oConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                if (sType == "Software")
                {
                    doDelete();
                    CommonClass.InputLog(oConn, "", sUserID, "", "Delete Software List", "Group Name :" + txtPackageName.Text);
                }
                
                doDeleteTerminalID();
                if (!string.IsNullOrEmpty(sDelVariable))
                    CommonClass.InputLog(oConn, "", sUserID, "", "Delete Software List", "Remove " + sType + ":" + sDelVariable);
                if (!string.IsNullOrEmpty(sAddVariable))
                    CommonClass.InputLog(oConn, "", sUserID, "", "Delete Software List", "Add " + sType + ":" + sAddVariable);

                SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET RemoteDownload = 0 WHERE TerminalID IN (SELECT TerminalID FROM tbListTerminalIDPackage WHERE GroupName = '{0}')", txtPackageName.Text), oConn);
                //oCmd.Transaction = oTransaction;
                if (oConn.State != ConnectionState.Open) oConn.Open();
                oCmd.ExecuteNonQuery();

                //oTransaction.Commit();
                this.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                //oTransaction.Rollback();
            }
        }

        private void cmbScheduleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sType == "Software")
            {
                if (cmbScheduleType.Text == "Immidiate Download")
                {
                    dtpTime.Enabled = false;
                    dtpTime.Value = DateTime.ParseExact("00:00:00", "HH:mm:ss", CultureInfo.InvariantCulture);
                }
                else
                    dtpTime.Enabled = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(cmbScheduleType.DisplayMember) && !string.IsNullOrEmpty(cmbScheduleType.ValueMember) && cmbScheduleType.SelectedIndex >= 0)
                    GetTerminalID();
            }
        }

        

    }
}
