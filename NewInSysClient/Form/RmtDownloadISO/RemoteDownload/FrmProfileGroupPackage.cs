﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmProfileGroupPackage : Form
    {
        SqlConnection oConn;
        SqlConnection oSqlConnAuditTrail;
        string sUID;
        string sType;

        public FrmProfileGroupPackage(SqlConnection _oConn, SqlConnection _oSqlConnAuditTrail, string _sUID,string _sType)
        {
            oConn = _oConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUID = _sUID;
            sType = _sType;
            InitializeComponent();
        }

        private void FrmProfileGroupPackage_Load(object sender, EventArgs e)
        {
            if (sType == "Software")
            {
                btnAdd.Visible = true;
                btnEdit.Text = "Edit";
                this.Text = "Form Group Package";
            }
            else
            {
                btnAdd.Visible = false;
                btnEdit.Text = "Edit TID";
                this.Text = "Form TerminalID Package";
            }
            doFillDataGridView();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                FrmItemPackage fItemPackage = new FrmItemPackage(oConn, sUID,"", false, sType);
                fItemPackage.ShowDialog();
                doFillDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                FrmItemPackage fItemPackage = new FrmItemPackage(oConn, sUID, dgGroup.SelectedCells[0].Value.ToString(), true, sType);
                fItemPackage.ShowDialog();
                doFillDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region "Function"
        protected void doFillDataGridView()
        {
            dgGroup.DataSource = null;
            dgGroup.DataSource = dtGroupList().DefaultView.ToTable(true, "GroupName");
            dgGroup.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        protected DataTable dtGroupList()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oComm = new SqlCommand(CommonSP.sSPSoftwareGroupPackageBrowse, oConn))
                {
                    oComm.CommandType = CommandType.StoredProcedure;
                    oComm.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "";
                    SqlDataAdapter oAdapt = new SqlDataAdapter(oComm);
                    oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dtTable;
        }
        #endregion


        
    }
}
