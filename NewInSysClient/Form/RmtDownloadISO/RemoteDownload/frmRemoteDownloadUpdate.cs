﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class frmRemoteDownloadUpdate : Form
    {
        protected SqlConnection oSqlConn;
        protected string sCategory;
        protected string sNameCategory;

        public frmRemoteDownloadUpdate(SqlConnection _oSqlConn, string _sCategory, string _sNameCategory)
        {
            oSqlConn = _oSqlConn;
            sCategory = _sCategory;
            sNameCategory = _sNameCategory;
            InitializeComponent();
        }

        private void frmRemoteDownloadUpdate_Load(object sender, EventArgs e)
        {
            InitComboBoxSoftwareName();
        }

        private void InitComboBoxSoftwareName()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPAppPackageBrowse);
            if (dtSoftware.Rows.Count > 1)
            {
                cmbSoftwareName.DataSource = dtSoftware;
                cmbSoftwareName.DisplayMember = "AppPackageId";
                cmbSoftwareName.ValueMember = "Software Name";
                cmbSoftwareName.SelectedIndex = -1;
            }
        }

       private DataTable dtGetData(string sSP)
        {
            SqlCommand oCmd = new SqlCommand(sSP, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);
            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();
            return dtTemp;
        }
        




    }
}
