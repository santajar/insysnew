using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace InSys
{
    public partial class FrmLogExcel : Form
    {
        protected SqlConnection oSqlConn;
        public FrmLogExcel(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        /// <summary>
        /// Runs while form is loading, insert logs to Audit Trail.
        /// </summary>
        private void Execute_Click(object sender, EventArgs e)
        {
            CommonClass.InputLog(oSqlConn, "", "", "", CommonMessage.sLogInsys, "");
        }
    }
}