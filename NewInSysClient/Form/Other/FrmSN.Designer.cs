namespace InSys
{
    partial class FrmSN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSN));
            this.dgTerminalSN = new System.Windows.Forms.DataGrid();
            this.tabRegister = new System.Windows.Forms.TabPage();
            this.chkColHeader = new System.Windows.Forms.CheckBox();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.rdVertical = new System.Windows.Forms.RadioButton();
            this.rdComma = new System.Windows.Forms.RadioButton();
            this.btnExit = new System.Windows.Forms.Button();
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.btnStartAll = new System.Windows.Forms.Button();
            this.btnBrowseText = new System.Windows.Forms.Button();
            this.lblFileName = new System.Windows.Forms.Label();
            this.lblSeparatorValue = new System.Windows.Forms.Label();
            this.rdSpace = new System.Windows.Forms.RadioButton();
            this.btnReset = new System.Windows.Forms.Button();
            this.lvSN = new System.Windows.Forms.ListView();
            this.tabView = new System.Windows.Forms.TabPage();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnViewSN = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.TabRegisterSN = new System.Windows.Forms.TabControl();
            this.ofdListSN = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dgTerminalSN)).BeginInit();
            this.tabRegister.SuspendLayout();
            this.tabView.SuspendLayout();
            this.TabRegisterSN.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgTerminalSN
            // 
            this.dgTerminalSN.DataMember = "";
            this.dgTerminalSN.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgTerminalSN.Location = new System.Drawing.Point(8, 40);
            this.dgTerminalSN.Name = "dgTerminalSN";
            this.dgTerminalSN.Size = new System.Drawing.Size(776, 344);
            this.dgTerminalSN.TabIndex = 0;
            // 
            // tabRegister
            // 
            this.tabRegister.Controls.Add(this.chkColHeader);
            this.tabRegister.Controls.Add(this.txtFileName);
            this.tabRegister.Controls.Add(this.rdVertical);
            this.tabRegister.Controls.Add(this.rdComma);
            this.tabRegister.Controls.Add(this.btnExit);
            this.tabRegister.Controls.Add(this.Progress);
            this.tabRegister.Controls.Add(this.btnStartAll);
            this.tabRegister.Controls.Add(this.btnBrowseText);
            this.tabRegister.Controls.Add(this.lblFileName);
            this.tabRegister.Controls.Add(this.lblSeparatorValue);
            this.tabRegister.Controls.Add(this.rdSpace);
            this.tabRegister.Controls.Add(this.btnReset);
            this.tabRegister.Controls.Add(this.lvSN);
            this.tabRegister.Location = new System.Drawing.Point(4, 22);
            this.tabRegister.Name = "tabRegister";
            this.tabRegister.Size = new System.Drawing.Size(792, 398);
            this.tabRegister.TabIndex = 0;
            this.tabRegister.Text = "Register SN";
            // 
            // chkColHeader
            // 
            this.chkColHeader.Checked = true;
            this.chkColHeader.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkColHeader.Location = new System.Drawing.Point(8, 72);
            this.chkColHeader.Name = "chkColHeader";
            this.chkColHeader.Size = new System.Drawing.Size(208, 24);
            this.chkColHeader.TabIndex = 5;
            this.chkColHeader.Text = "The Table has column Header";
            // 
            // txtFileName
            // 
            this.txtFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileName.Location = new System.Drawing.Point(120, 8);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(232, 21);
            this.txtFileName.TabIndex = 0;
            // 
            // rdVertical
            // 
            this.rdVertical.Checked = true;
            this.rdVertical.Location = new System.Drawing.Point(344, 40);
            this.rdVertical.Name = "rdVertical";
            this.rdVertical.Size = new System.Drawing.Size(104, 24);
            this.rdVertical.TabIndex = 4;
            this.rdVertical.TabStop = true;
            this.rdVertical.Text = "Vertical Bar";
            // 
            // rdComma
            // 
            this.rdComma.Location = new System.Drawing.Point(120, 40);
            this.rdComma.Name = "rdComma";
            this.rdComma.Size = new System.Drawing.Size(104, 24);
            this.rdComma.TabIndex = 2;
            this.rdComma.Text = "Comma";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(216, 136);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 24);
            this.btnExit.TabIndex = 8;
            this.btnExit.Text = "&Close";
            // 
            // Progress
            // 
            this.Progress.Location = new System.Drawing.Point(8, 104);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(776, 23);
            this.Progress.Step = 1;
            this.Progress.TabIndex = 47;
            // 
            // btnStartAll
            // 
            this.btnStartAll.Location = new System.Drawing.Point(8, 136);
            this.btnStartAll.Name = "btnStartAll";
            this.btnStartAll.Size = new System.Drawing.Size(88, 24);
            this.btnStartAll.TabIndex = 6;
            this.btnStartAll.Text = "&Start";
            // 
            // btnBrowseText
            // 
            this.btnBrowseText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseText.Location = new System.Drawing.Point(368, 8);
            this.btnBrowseText.Name = "btnBrowseText";
            this.btnBrowseText.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseText.TabIndex = 1;
            this.btnBrowseText.Text = "Browse";
            // 
            // lblFileName
            // 
            this.lblFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileName.Location = new System.Drawing.Point(8, 8);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(100, 23);
            this.lblFileName.TabIndex = 39;
            this.lblFileName.Text = "File Name (*.txt)";
            this.lblFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeparatorValue
            // 
            this.lblSeparatorValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeparatorValue.Location = new System.Drawing.Point(8, 40);
            this.lblSeparatorValue.Name = "lblSeparatorValue";
            this.lblSeparatorValue.Size = new System.Drawing.Size(100, 23);
            this.lblSeparatorValue.TabIndex = 38;
            this.lblSeparatorValue.Text = "Separator Value :";
            this.lblSeparatorValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rdSpace
            // 
            this.rdSpace.Location = new System.Drawing.Point(232, 40);
            this.rdSpace.Name = "rdSpace";
            this.rdSpace.Size = new System.Drawing.Size(104, 24);
            this.rdSpace.TabIndex = 3;
            this.rdSpace.Text = "Space";
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(112, 136);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(88, 24);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "&Reset";
            // 
            // lvSN
            // 
            this.lvSN.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvSN.LabelEdit = true;
            this.lvSN.Location = new System.Drawing.Point(8, 168);
            this.lvSN.Name = "lvSN";
            this.lvSN.Size = new System.Drawing.Size(776, 216);
            this.lvSN.TabIndex = 9;
            this.lvSN.UseCompatibleStateImageBehavior = false;
            this.lvSN.View = System.Windows.Forms.View.Details;
            // 
            // tabView
            // 
            this.tabView.Controls.Add(this.dgTerminalSN);
            this.tabView.Controls.Add(this.txtSearch);
            this.tabView.Controls.Add(this.btnViewSN);
            this.tabView.Controls.Add(this.btnSearch);
            this.tabView.Location = new System.Drawing.Point(4, 22);
            this.tabView.Name = "tabView";
            this.tabView.Size = new System.Drawing.Size(792, 398);
            this.tabView.TabIndex = 1;
            this.tabView.Text = "View Terminal-SN";
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(520, 8);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(184, 21);
            this.txtSearch.TabIndex = 41;
            this.txtSearch.Text = "Search...";
            // 
            // btnViewSN
            // 
            this.btnViewSN.Location = new System.Drawing.Point(8, 8);
            this.btnViewSN.Name = "btnViewSN";
            this.btnViewSN.Size = new System.Drawing.Size(128, 21);
            this.btnViewSN.TabIndex = 52;
            this.btnViewSN.Text = "Refresh Table";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(712, 8);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(72, 21);
            this.btnSearch.TabIndex = 45;
            this.btnSearch.Text = "Go";
            // 
            // TabRegisterSN
            // 
            this.TabRegisterSN.Controls.Add(this.tabView);
            this.TabRegisterSN.Controls.Add(this.tabRegister);
            this.TabRegisterSN.Location = new System.Drawing.Point(3, 4);
            this.TabRegisterSN.Name = "TabRegisterSN";
            this.TabRegisterSN.SelectedIndex = 0;
            this.TabRegisterSN.Size = new System.Drawing.Size(800, 424);
            this.TabRegisterSN.TabIndex = 38;
            // 
            // ofdListSN
            // 
            this.ofdListSN.Filter = "Text Files|*.txt";
            // 
            // FrmSN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(805, 431);
            this.Controls.Add(this.TabRegisterSN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmSN";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Terminal Serial Number";
            this.Load += new System.EventHandler(this.FrmSN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgTerminalSN)).EndInit();
            this.tabRegister.ResumeLayout(false);
            this.tabRegister.PerformLayout();
            this.tabView.ResumeLayout(false);
            this.tabView.PerformLayout();
            this.TabRegisterSN.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.DataGrid dgTerminalSN;
        internal System.Windows.Forms.TabPage tabRegister;
        internal System.Windows.Forms.CheckBox chkColHeader;
        internal System.Windows.Forms.TextBox txtFileName;
        internal System.Windows.Forms.RadioButton rdVertical;
        internal System.Windows.Forms.RadioButton rdComma;
        internal System.Windows.Forms.Button btnExit;
        internal System.Windows.Forms.ProgressBar Progress;
        internal System.Windows.Forms.Button btnStartAll;
        internal System.Windows.Forms.Button btnBrowseText;
        internal System.Windows.Forms.Label lblFileName;
        internal System.Windows.Forms.Label lblSeparatorValue;
        internal System.Windows.Forms.RadioButton rdSpace;
        internal System.Windows.Forms.Button btnReset;
        internal System.Windows.Forms.ListView lvSN;
        internal System.Windows.Forms.TabPage tabView;
        internal System.Windows.Forms.TextBox txtSearch;
        internal System.Windows.Forms.Button btnViewSN;
        internal System.Windows.Forms.Button btnSearch;
        internal System.Windows.Forms.TabControl TabRegisterSN;
        internal System.Windows.Forms.OpenFileDialog ofdListSN;
    }
}