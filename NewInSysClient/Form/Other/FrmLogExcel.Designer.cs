namespace InSys
{
    partial class FrmLogExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogExcel));
            this.Progress = new System.Windows.Forms.ProgressBar();
            this.btnClose = new System.Windows.Forms.Button();
            this.RadioButton3 = new System.Windows.Forms.RadioButton();
            this.RadioButton2 = new System.Windows.Forms.RadioButton();
            this.RadioButton1 = new System.Windows.Forms.RadioButton();
            this.Execute = new System.Windows.Forms.Button();
            this.ComboBoxDb = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // Progress
            // 
            this.Progress.Location = new System.Drawing.Point(15, 92);
            this.Progress.Name = "Progress";
            this.Progress.Size = new System.Drawing.Size(400, 16);
            this.Progress.Step = 1;
            this.Progress.TabIndex = 94;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(303, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(112, 24);
            this.btnClose.TabIndex = 93;
            this.btnClose.Text = "&Close";
            // 
            // RadioButton3
            // 
            this.RadioButton3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton3.Location = new System.Drawing.Point(263, 52);
            this.RadioButton3.Name = "RadioButton3";
            this.RadioButton3.Size = new System.Drawing.Size(168, 24);
            this.RadioButton3.TabIndex = 92;
            this.RadioButton3.Text = "Undownloaded Data";
            // 
            // RadioButton2
            // 
            this.RadioButton2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton2.Location = new System.Drawing.Point(103, 52);
            this.RadioButton2.Name = "RadioButton2";
            this.RadioButton2.Size = new System.Drawing.Size(152, 24);
            this.RadioButton2.TabIndex = 91;
            this.RadioButton2.Text = "Downloaded Data";
            // 
            // RadioButton1
            // 
            this.RadioButton1.Checked = true;
            this.RadioButton1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioButton1.Location = new System.Drawing.Point(15, 52);
            this.RadioButton1.Name = "RadioButton1";
            this.RadioButton1.Size = new System.Drawing.Size(80, 24);
            this.RadioButton1.TabIndex = 90;
            this.RadioButton1.TabStop = true;
            this.RadioButton1.Text = "All Data";
            // 
            // Execute
            // 
            this.Execute.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Execute.Location = new System.Drawing.Point(183, 12);
            this.Execute.Name = "Execute";
            this.Execute.Size = new System.Drawing.Size(112, 24);
            this.Execute.TabIndex = 89;
            this.Execute.Text = "E&xecute";
            this.Execute.Click += new System.EventHandler(this.Execute_Click);
            // 
            // ComboBoxDb
            // 
            this.ComboBoxDb.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBoxDb.Location = new System.Drawing.Point(15, 12);
            this.ComboBoxDb.Name = "ComboBoxDb";
            this.ComboBoxDb.Size = new System.Drawing.Size(160, 23);
            this.ComboBoxDb.TabIndex = 88;
            // 
            // FrmLogExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 118);
            this.Controls.Add(this.Progress);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.RadioButton3);
            this.Controls.Add(this.RadioButton2);
            this.Controls.Add(this.RadioButton1);
            this.Controls.Add(this.Execute);
            this.Controls.Add(this.ComboBoxDb);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmLogExcel";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Log Insys";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar Progress;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.RadioButton RadioButton3;
        internal System.Windows.Forms.RadioButton RadioButton2;
        internal System.Windows.Forms.RadioButton RadioButton1;
        internal System.Windows.Forms.Button Execute;
        internal System.Windows.Forms.ComboBox ComboBoxDb;
    }
}