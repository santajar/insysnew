namespace InSys
{
    /// <summary>
    /// To display all the modem settings
    /// </summary>
    partial class FrmModem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmModem));
            this.btnSave = new System.Windows.Forms.Button();
            this.gbAsync_Sync = new System.Windows.Forms.GroupBox();
            this.rdSync = new System.Windows.Forms.RadioButton();
            this.rdAsync = new System.Windows.Forms.RadioButton();
            this.rdBEll = new System.Windows.Forms.RadioButton();
            this.rdtV32Bis = new System.Windows.Forms.RadioButton();
            this.rdTone = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbModemSetting = new System.Windows.Forms.GroupBox();
            this.gbModulation = new System.Windows.Forms.GroupBox();
            this.rdV32Modulation = new System.Windows.Forms.RadioButton();
            this.rdV22BIS = new System.Windows.Forms.RadioButton();
            this.rdV22CCITT = new System.Windows.Forms.RadioButton();
            this.rdV21CCITT = new System.Windows.Forms.RadioButton();
            this.rdBest = new System.Windows.Forms.RadioButton();
            this.gbPulse_Tone = new System.Windows.Forms.GroupBox();
            this.rdPulse = new System.Windows.Forms.RadioButton();
            this.gbCCIT_BELL = new System.Windows.Forms.GroupBox();
            this.rdCCITT = new System.Windows.Forms.RadioButton();
            this.gbTPDU_Setting = new System.Windows.Forms.GroupBox();
            this.txtTPDUorigin = new System.Windows.Forms.TextBox();
            this.txtTPDUId = new System.Windows.Forms.TextBox();
            this.lblOriginalAddress = new System.Windows.Forms.Label();
            this.lblTPDU_ID = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.gbAsync_Sync.SuspendLayout();
            this.gbModemSetting.SuspendLayout();
            this.gbModulation.SuspendLayout();
            this.gbPulse_Tone.SuspendLayout();
            this.gbCCIT_BELL.SuspendLayout();
            this.gbTPDU_Setting.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSave.Location = new System.Drawing.Point(26, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 23);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbAsync_Sync
            // 
            this.gbAsync_Sync.Controls.Add(this.rdSync);
            this.gbAsync_Sync.Controls.Add(this.rdAsync);
            this.gbAsync_Sync.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAsync_Sync.Location = new System.Drawing.Point(8, 228);
            this.gbAsync_Sync.Name = "gbAsync_Sync";
            this.gbAsync_Sync.Size = new System.Drawing.Size(240, 48);
            this.gbAsync_Sync.TabIndex = 5;
            this.gbAsync_Sync.TabStop = false;
            this.gbAsync_Sync.Text = "ASYNC / SYNC";
            // 
            // rdSync
            // 
            this.rdSync.Checked = true;
            this.rdSync.Location = new System.Drawing.Point(120, 16);
            this.rdSync.Name = "rdSync";
            this.rdSync.Size = new System.Drawing.Size(104, 24);
            this.rdSync.TabIndex = 17;
            this.rdSync.TabStop = true;
            this.rdSync.Text = "Synchronous";
            // 
            // rdAsync
            // 
            this.rdAsync.Location = new System.Drawing.Point(16, 16);
            this.rdAsync.Name = "rdAsync";
            this.rdAsync.Size = new System.Drawing.Size(104, 24);
            this.rdAsync.TabIndex = 16;
            this.rdAsync.Text = "Asynchronous";
            // 
            // rdBEll
            // 
            this.rdBEll.Checked = true;
            this.rdBEll.Location = new System.Drawing.Point(120, 16);
            this.rdBEll.Name = "rdBEll";
            this.rdBEll.Size = new System.Drawing.Size(56, 24);
            this.rdBEll.TabIndex = 7;
            this.rdBEll.TabStop = true;
            this.rdBEll.Text = "Bell";
            // 
            // rdtV32Bis
            // 
            this.rdtV32Bis.Location = new System.Drawing.Point(120, 64);
            this.rdtV32Bis.Name = "rdtV32Bis";
            this.rdtV32Bis.Size = new System.Drawing.Size(72, 24);
            this.rdtV32Bis.TabIndex = 15;
            this.rdtV32Bis.Text = "V.32 Bis";
            // 
            // rdTone
            // 
            this.rdTone.Checked = true;
            this.rdTone.Location = new System.Drawing.Point(120, 16);
            this.rdTone.Name = "rdTone";
            this.rdTone.Size = new System.Drawing.Size(56, 24);
            this.rdTone.TabIndex = 9;
            this.rdTone.TabStop = true;
            this.rdTone.Text = "Tone";
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(142, 13);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(110, 23);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gbModemSetting
            // 
            this.gbModemSetting.Controls.Add(this.gbModulation);
            this.gbModemSetting.Controls.Add(this.gbPulse_Tone);
            this.gbModemSetting.Controls.Add(this.gbCCIT_BELL);
            this.gbModemSetting.Controls.Add(this.gbAsync_Sync);
            this.gbModemSetting.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbModemSetting.Location = new System.Drawing.Point(12, 12);
            this.gbModemSetting.Name = "gbModemSetting";
            this.gbModemSetting.Size = new System.Drawing.Size(258, 287);
            this.gbModemSetting.TabIndex = 0;
            this.gbModemSetting.TabStop = false;
            this.gbModemSetting.Text = "Modem Setting";
            // 
            // gbModulation
            // 
            this.gbModulation.Controls.Add(this.rdtV32Bis);
            this.gbModulation.Controls.Add(this.rdV32Modulation);
            this.gbModulation.Controls.Add(this.rdV22BIS);
            this.gbModulation.Controls.Add(this.rdV22CCITT);
            this.gbModulation.Controls.Add(this.rdV21CCITT);
            this.gbModulation.Controls.Add(this.rdBest);
            this.gbModulation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbModulation.Location = new System.Drawing.Point(8, 126);
            this.gbModulation.Name = "gbModulation";
            this.gbModulation.Size = new System.Drawing.Size(240, 96);
            this.gbModulation.TabIndex = 4;
            this.gbModulation.TabStop = false;
            this.gbModulation.Text = "MODULATION";
            // 
            // rdV32Modulation
            // 
            this.rdV32Modulation.Location = new System.Drawing.Point(120, 40);
            this.rdV32Modulation.Name = "rdV32Modulation";
            this.rdV32Modulation.Size = new System.Drawing.Size(112, 24);
            this.rdV32Modulation.TabIndex = 14;
            this.rdV32Modulation.Text = "V.32 Modulation";
            // 
            // rdV22BIS
            // 
            this.rdV22BIS.Location = new System.Drawing.Point(120, 16);
            this.rdV22BIS.Name = "rdV22BIS";
            this.rdV22BIS.Size = new System.Drawing.Size(104, 24);
            this.rdV22BIS.TabIndex = 13;
            this.rdV22BIS.Text = "V.22 Bis";
            // 
            // rdV22CCITT
            // 
            this.rdV22CCITT.Checked = true;
            this.rdV22CCITT.Location = new System.Drawing.Point(16, 64);
            this.rdV22CCITT.Name = "rdV22CCITT";
            this.rdV22CCITT.Size = new System.Drawing.Size(104, 24);
            this.rdV22CCITT.TabIndex = 12;
            this.rdV22CCITT.TabStop = true;
            this.rdV22CCITT.Text = "V.22 ( CCITT )";
            // 
            // rdV21CCITT
            // 
            this.rdV21CCITT.Location = new System.Drawing.Point(16, 40);
            this.rdV21CCITT.Name = "rdV21CCITT";
            this.rdV21CCITT.Size = new System.Drawing.Size(104, 24);
            this.rdV21CCITT.TabIndex = 11;
            this.rdV21CCITT.Text = "V.21 ( CCITT )";
            // 
            // rdBest
            // 
            this.rdBest.Location = new System.Drawing.Point(16, 16);
            this.rdBest.Name = "rdBest";
            this.rdBest.Size = new System.Drawing.Size(104, 24);
            this.rdBest.TabIndex = 10;
            this.rdBest.Text = "Best";
            // 
            // gbPulse_Tone
            // 
            this.gbPulse_Tone.Controls.Add(this.rdTone);
            this.gbPulse_Tone.Controls.Add(this.rdPulse);
            this.gbPulse_Tone.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPulse_Tone.Location = new System.Drawing.Point(8, 72);
            this.gbPulse_Tone.Name = "gbPulse_Tone";
            this.gbPulse_Tone.Size = new System.Drawing.Size(240, 48);
            this.gbPulse_Tone.TabIndex = 3;
            this.gbPulse_Tone.TabStop = false;
            this.gbPulse_Tone.Text = "PULSE / TONE";
            // 
            // rdPulse
            // 
            this.rdPulse.Location = new System.Drawing.Point(16, 16);
            this.rdPulse.Name = "rdPulse";
            this.rdPulse.Size = new System.Drawing.Size(104, 24);
            this.rdPulse.TabIndex = 8;
            this.rdPulse.Text = "Pulse";
            // 
            // gbCCIT_BELL
            // 
            this.gbCCIT_BELL.Controls.Add(this.rdBEll);
            this.gbCCIT_BELL.Controls.Add(this.rdCCITT);
            this.gbCCIT_BELL.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCCIT_BELL.Location = new System.Drawing.Point(8, 16);
            this.gbCCIT_BELL.Name = "gbCCIT_BELL";
            this.gbCCIT_BELL.Size = new System.Drawing.Size(240, 48);
            this.gbCCIT_BELL.TabIndex = 2;
            this.gbCCIT_BELL.TabStop = false;
            this.gbCCIT_BELL.Text = "CCITT / BELL";
            // 
            // rdCCITT
            // 
            this.rdCCITT.Location = new System.Drawing.Point(16, 16);
            this.rdCCITT.Name = "rdCCITT";
            this.rdCCITT.Size = new System.Drawing.Size(72, 24);
            this.rdCCITT.TabIndex = 6;
            this.rdCCITT.Text = "CCITT";
            // 
            // gbTPDU_Setting
            // 
            this.gbTPDU_Setting.Controls.Add(this.txtTPDUorigin);
            this.gbTPDU_Setting.Controls.Add(this.txtTPDUId);
            this.gbTPDU_Setting.Controls.Add(this.lblOriginalAddress);
            this.gbTPDU_Setting.Controls.Add(this.lblTPDU_ID);
            this.gbTPDU_Setting.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbTPDU_Setting.Location = new System.Drawing.Point(12, 307);
            this.gbTPDU_Setting.Name = "gbTPDU_Setting";
            this.gbTPDU_Setting.Size = new System.Drawing.Size(258, 80);
            this.gbTPDU_Setting.TabIndex = 1;
            this.gbTPDU_Setting.TabStop = false;
            this.gbTPDU_Setting.Text = "TPDU Setting";
            // 
            // txtTPDUorigin
            // 
            this.txtTPDUorigin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTPDUorigin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTPDUorigin.Location = new System.Drawing.Point(122, 48);
            this.txtTPDUorigin.MaxLength = 4;
            this.txtTPDUorigin.Name = "txtTPDUorigin";
            this.txtTPDUorigin.Size = new System.Drawing.Size(120, 20);
            this.txtTPDUorigin.TabIndex = 19;
            // 
            // txtTPDUId
            // 
            this.txtTPDUId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTPDUId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTPDUId.Location = new System.Drawing.Point(122, 24);
            this.txtTPDUId.MaxLength = 2;
            this.txtTPDUId.Name = "txtTPDUId";
            this.txtTPDUId.Size = new System.Drawing.Size(120, 20);
            this.txtTPDUId.TabIndex = 18;
            // 
            // lblOriginalAddress
            // 
            this.lblOriginalAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOriginalAddress.Location = new System.Drawing.Point(16, 48);
            this.lblOriginalAddress.Name = "lblOriginalAddress";
            this.lblOriginalAddress.Size = new System.Drawing.Size(100, 23);
            this.lblOriginalAddress.TabIndex = 1;
            this.lblOriginalAddress.Text = "Original Address ";
            // 
            // lblTPDU_ID
            // 
            this.lblTPDU_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTPDU_ID.Location = new System.Drawing.Point(16, 24);
            this.lblTPDU_ID.Name = "lblTPDU_ID";
            this.lblTPDU_ID.Size = new System.Drawing.Size(100, 23);
            this.lblTPDU_ID.TabIndex = 0;
            this.lblTPDU_ID.Text = "TPDU ID ";
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(12, 393);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(258, 45);
            this.gbButton.TabIndex = 22;
            this.gbButton.TabStop = false;
            // 
            // FrmModem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 447);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbModemSetting);
            this.Controls.Add(this.gbTPDU_Setting);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmModem";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Modem";
            this.Load += new System.EventHandler(this.FrmModem_Load);
            this.gbAsync_Sync.ResumeLayout(false);
            this.gbModemSetting.ResumeLayout(false);
            this.gbModulation.ResumeLayout(false);
            this.gbPulse_Tone.ResumeLayout(false);
            this.gbCCIT_BELL.ResumeLayout(false);
            this.gbTPDU_Setting.ResumeLayout(false);
            this.gbTPDU_Setting.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.GroupBox gbAsync_Sync;
        internal System.Windows.Forms.RadioButton rdSync;
        internal System.Windows.Forms.RadioButton rdAsync;
        internal System.Windows.Forms.RadioButton rdBEll;
        internal System.Windows.Forms.RadioButton rdtV32Bis;
        internal System.Windows.Forms.RadioButton rdTone;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.GroupBox gbModemSetting;
        internal System.Windows.Forms.GroupBox gbModulation;
        internal System.Windows.Forms.RadioButton rdV32Modulation;
        internal System.Windows.Forms.RadioButton rdV22BIS;
        internal System.Windows.Forms.RadioButton rdV22CCITT;
        internal System.Windows.Forms.RadioButton rdV21CCITT;
        internal System.Windows.Forms.RadioButton rdBest;
        internal System.Windows.Forms.GroupBox gbPulse_Tone;
        internal System.Windows.Forms.RadioButton rdPulse;
        internal System.Windows.Forms.GroupBox gbCCIT_BELL;
        internal System.Windows.Forms.RadioButton rdCCITT;
        internal System.Windows.Forms.GroupBox gbTPDU_Setting;
        internal System.Windows.Forms.TextBox txtTPDUorigin;
        internal System.Windows.Forms.TextBox txtTPDUId;
        internal System.Windows.Forms.Label lblOriginalAddress;
        internal System.Windows.Forms.Label lblTPDU_ID;
        private System.Windows.Forms.GroupBox gbButton;
    }
}