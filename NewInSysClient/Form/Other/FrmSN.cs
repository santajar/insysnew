using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace InSys
{
    public partial class FrmSN : Form
    {
        protected SqlConnection oSqlConn;
        public FrmSN(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
        }

        /// <summary>
        /// Runs while form is loading, insert logs to Audit Trail.
        /// </summary>
        private void FrmSN_Load(object sender, EventArgs e)
        {
            CommonClass.InputLog(oSqlConn, "", "", "", CommonMessage.sViewSN, "");
        }
    }
}