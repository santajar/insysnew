namespace InSys
{
    /// <summary>
    /// Dynamic prompt form
    /// </summary>
    partial class FrmPromptCombo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPromptCombo));
            this.cmdOK = new System.Windows.Forms.Button();
            this.lblPrompt = new System.Windows.Forms.Label();
            this.cmbPrompt = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cmdOK
            // 
            this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOK.Location = new System.Drawing.Point(324, 11);
            this.cmdOK.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(51, 65);
            this.cmdOK.TabIndex = 5;
            this.cmdOK.Text = "OK";
            // 
            // lblPrompt
            // 
            this.lblPrompt.Location = new System.Drawing.Point(18, 14);
            this.lblPrompt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPrompt.Name = "lblPrompt";
            this.lblPrompt.Size = new System.Drawing.Size(300, 25);
            this.lblPrompt.TabIndex = 3;
            // 
            // cmbPrompt
            // 
            this.cmbPrompt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbPrompt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbPrompt.FormattingEnabled = true;
            this.cmbPrompt.Location = new System.Drawing.Point(22, 43);
            this.cmbPrompt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbPrompt.Name = "cmbPrompt";
            this.cmbPrompt.Size = new System.Drawing.Size(294, 28);
            this.cmbPrompt.TabIndex = 6;
            // 
            // FrmPromptCombo
            // 
            this.AcceptButton = this.cmdOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 83);
            this.Controls.Add(this.cmbPrompt);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.lblPrompt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmPromptCombo";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button cmdOK;
        internal System.Windows.Forms.Label lblPrompt;
        public System.Windows.Forms.ComboBox cmbPrompt;
    }
}