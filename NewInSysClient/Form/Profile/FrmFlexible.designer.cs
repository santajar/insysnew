namespace InSys
{
    partial class FrmFlexible
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFlexible));
            this.chkEnableIP = new System.Windows.Forms.CheckBox();
            this.chkStatusMaster = new System.Windows.Forms.CheckBox();
            this.chkAllowDL = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbStatus = new System.Windows.Forms.GroupBox();
            this.chkLogoReceipt = new System.Windows.Forms.CheckBox();
            this.chkLogoMerchant = new System.Windows.Forms.CheckBox();
            this.chkLogoIdle = new System.Windows.Forms.CheckBox();
            this.chkLogoPrint = new System.Windows.Forms.CheckBox();
            this.chkRemoteDownload = new System.Windows.Forms.CheckBox();
            this.chkInitCompress = new System.Windows.Forms.CheckBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.gbStatus.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkEnableIP
            // 
            this.chkEnableIP.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkEnableIP.Enabled = false;
            this.chkEnableIP.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEnableIP.Location = new System.Drawing.Point(956, 4);
            this.chkEnableIP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkEnableIP.Name = "chkEnableIP";
            this.chkEnableIP.Size = new System.Drawing.Size(107, 25);
            this.chkEnableIP.TabIndex = 2;
            this.chkEnableIP.Text = "Enable IP";
            this.chkEnableIP.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkEnableIP.Visible = false;
            this.chkEnableIP.CheckedChanged += new System.EventHandler(this.chkEnableIP_CheckedChanged);
            // 
            // chkStatusMaster
            // 
            this.chkStatusMaster.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkStatusMaster.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkStatusMaster.Location = new System.Drawing.Point(92, 4);
            this.chkStatusMaster.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkStatusMaster.Name = "chkStatusMaster";
            this.chkStatusMaster.Size = new System.Drawing.Size(96, 25);
            this.chkStatusMaster.TabIndex = 1;
            this.chkStatusMaster.Text = "Master";
            this.chkStatusMaster.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkStatusMaster.CheckedChanged += new System.EventHandler(this.chkStatusMaster_CheckedChanged);
            // 
            // chkAllowDL
            // 
            this.chkAllowDL.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkAllowDL.Checked = true;
            this.chkAllowDL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAllowDL.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAllowDL.Location = new System.Drawing.Point(4, 4);
            this.chkAllowDL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAllowDL.Name = "chkAllowDL";
            this.chkAllowDL.Size = new System.Drawing.Size(80, 25);
            this.chkAllowDL.TabIndex = 0;
            this.chkAllowDL.Text = "Allow Download";
            this.chkAllowDL.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkAllowDL.CheckedChanged += new System.EventHandler(this.chkAllowDL_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(713, 18);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(113, 28);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSave.Location = new System.Drawing.Point(596, 18);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(113, 30);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbStatus
            // 
            this.gbStatus.Controls.Add(this.flowLayoutPanel1);
            this.gbStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbStatus.Location = new System.Drawing.Point(0, 0);
            this.gbStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbStatus.Name = "gbStatus";
            this.gbStatus.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbStatus.Size = new System.Drawing.Size(1415, 53);
            this.gbStatus.TabIndex = 0;
            this.gbStatus.TabStop = false;
            // 
            // chkLogoReceipt
            // 
            this.chkLogoReceipt.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoReceipt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLogoReceipt.Location = new System.Drawing.Point(777, 4);
            this.chkLogoReceipt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkLogoReceipt.Name = "chkLogoReceipt";
            this.chkLogoReceipt.Size = new System.Drawing.Size(171, 25);
            this.chkLogoReceipt.TabIndex = 9;
            this.chkLogoReceipt.Text = "Logo Receipt";
            this.chkLogoReceipt.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoReceipt.Visible = false;
            // 
            // chkLogoMerchant
            // 
            this.chkLogoMerchant.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoMerchant.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLogoMerchant.Location = new System.Drawing.Point(598, 4);
            this.chkLogoMerchant.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkLogoMerchant.Name = "chkLogoMerchant";
            this.chkLogoMerchant.Size = new System.Drawing.Size(171, 25);
            this.chkLogoMerchant.TabIndex = 8;
            this.chkLogoMerchant.Text = "Logo Merchant";
            this.chkLogoMerchant.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoMerchant.Visible = false;
            // 
            // chkLogoIdle
            // 
            this.chkLogoIdle.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoIdle.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLogoIdle.Location = new System.Drawing.Point(474, 4);
            this.chkLogoIdle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkLogoIdle.Name = "chkLogoIdle";
            this.chkLogoIdle.Size = new System.Drawing.Size(116, 25);
            this.chkLogoIdle.TabIndex = 7;
            this.chkLogoIdle.Text = "Logo Idle";
            this.chkLogoIdle.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoIdle.Visible = false;
            this.chkLogoIdle.CheckedChanged += new System.EventHandler(this.chkInitIdleScreen_CheckedChanged);
            // 
            // chkLogoPrint
            // 
            this.chkLogoPrint.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoPrint.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLogoPrint.Location = new System.Drawing.Point(351, 4);
            this.chkLogoPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkLogoPrint.Name = "chkLogoPrint";
            this.chkLogoPrint.Size = new System.Drawing.Size(115, 25);
            this.chkLogoPrint.TabIndex = 6;
            this.chkLogoPrint.Text = "Logo Print";
            this.chkLogoPrint.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkLogoPrint.Visible = false;
            this.chkLogoPrint.CheckedChanged += new System.EventHandler(this.chkInitLogo_CheckedChanged);
            // 
            // chkRemoteDownload
            // 
            this.chkRemoteDownload.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkRemoteDownload.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoteDownload.Location = new System.Drawing.Point(1071, 4);
            this.chkRemoteDownload.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkRemoteDownload.Name = "chkRemoteDownload";
            this.chkRemoteDownload.Size = new System.Drawing.Size(172, 25);
            this.chkRemoteDownload.TabIndex = 5;
            this.chkRemoteDownload.Text = "Remote Download";
            this.chkRemoteDownload.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkRemoteDownload.Visible = false;
            this.chkRemoteDownload.CheckedChanged += new System.EventHandler(this.chkRemoteDownload_CheckedChanged);
            // 
            // chkInitCompress
            // 
            this.chkInitCompress.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkInitCompress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkInitCompress.Location = new System.Drawing.Point(196, 4);
            this.chkInitCompress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkInitCompress.Name = "chkInitCompress";
            this.chkInitCompress.Size = new System.Drawing.Size(147, 25);
            this.chkInitCompress.TabIndex = 4;
            this.chkInitCompress.Text = "Init Compress";
            this.chkInitCompress.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkInitCompress.Visible = false;
            this.chkInitCompress.CheckedChanged += new System.EventHandler(this.chkInitCompress_CheckedChanged);
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbButton.Location = new System.Drawing.Point(0, 702);
            this.gbButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Name = "gbButton";
            this.gbButton.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbButton.Size = new System.Drawing.Size(1415, 59);
            this.gbButton.TabIndex = 11;
            this.gbButton.TabStop = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.chkAllowDL);
            this.flowLayoutPanel1.Controls.Add(this.chkStatusMaster);
            this.flowLayoutPanel1.Controls.Add(this.chkInitCompress);
            this.flowLayoutPanel1.Controls.Add(this.chkLogoPrint);
            this.flowLayoutPanel1.Controls.Add(this.chkLogoIdle);
            this.flowLayoutPanel1.Controls.Add(this.chkLogoMerchant);
            this.flowLayoutPanel1.Controls.Add(this.chkLogoReceipt);
            this.flowLayoutPanel1.Controls.Add(this.chkEnableIP);
            this.flowLayoutPanel1.Controls.Add(this.chkRemoteDownload);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(7, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1415, 40);
            this.flowLayoutPanel1.TabIndex = 12;
            // 
            // FrmFlexible
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1415, 761);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmFlexible";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "    ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmFlexible_FormClosing);
            this.Load += new System.EventHandler(this.FrmFlexible_Load);
            this.gbStatus.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.CheckBox chkEnableIP;
        internal System.Windows.Forms.CheckBox chkStatusMaster;
        internal System.Windows.Forms.CheckBox chkAllowDL;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbStatus;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.CheckBox chkInitCompress;
        internal System.Windows.Forms.CheckBox chkRemoteDownload;
        internal System.Windows.Forms.CheckBox chkLogoIdle;
        internal System.Windows.Forms.CheckBox chkLogoPrint;
        internal System.Windows.Forms.CheckBox chkLogoReceipt;
        internal System.Windows.Forms.CheckBox chkLogoMerchant;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}