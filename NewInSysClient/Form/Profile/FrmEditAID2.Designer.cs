﻿namespace InSys
{
    partial class FrmEditAID2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditAID2));
            this.gbItem = new System.Windows.Forms.GroupBox();
            this.txtEMVTAG = new System.Windows.Forms.TextBox();
            this.txtEMVTRMCAP = new System.Windows.Forms.TextBox();
            this.txtEMVAIDTXNTYPE = new System.Windows.Forms.TextBox();
            this.txtEMVTCC = new System.Windows.Forms.TextBox();
            this.txtEMVTRMFLRLIM = new System.Windows.Forms.TextBox();
            this.txtEMVACQID = new System.Windows.Forms.TextBox();
            this.txtEMVDFTVALTDOL = new System.Windows.Forms.TextBox();
            this.txtEMVDFTVALDDOL = new System.Windows.Forms.TextBox();
            this.txtEMVMAXTARPER = new System.Windows.Forms.TextBox();
            this.txtEMVTARPER = new System.Windows.Forms.TextBox();
            this.txtEMVTHRVAL = new System.Windows.Forms.TextBox();
            this.txtEMVTACONL = new System.Windows.Forms.TextBox();
            this.txtEMVTACDEN = new System.Windows.Forms.TextBox();
            this.txtEMVTACDFT = new System.Windows.Forms.TextBox();
            this.txtEMVNAME = new System.Windows.Forms.TextBox();
            this.txtEMVTRMAVN = new System.Windows.Forms.TextBox();
            this.txtEMVAID = new System.Windows.Forms.TextBox();
            this.lblEMVTAG = new System.Windows.Forms.Label();
            this.lblEMVTRMCAP = new System.Windows.Forms.Label();
            this.lblEMVAIDTXNTYPE = new System.Windows.Forms.Label();
            this.lblEMVTCC = new System.Windows.Forms.Label();
            this.lblEMVTRMFLRLIM = new System.Windows.Forms.Label();
            this.lblEMVACQID = new System.Windows.Forms.Label();
            this.lblEMVDFTVALTDOL = new System.Windows.Forms.Label();
            this.lblEMVDFTVALDDOL = new System.Windows.Forms.Label();
            this.lblEMVMAXTARPER = new System.Windows.Forms.Label();
            this.lblEMVTARPER = new System.Windows.Forms.Label();
            this.lblEMVTHRVAL = new System.Windows.Forms.Label();
            this.lblEMVTACONL = new System.Windows.Forms.Label();
            this.lblEMVTACDEN = new System.Windows.Forms.Label();
            this.lblEMVTACDFT = new System.Windows.Forms.Label();
            this.lblEMVNAME = new System.Windows.Forms.Label();
            this.lblEMVTRMAVN = new System.Windows.Forms.Label();
            this.lblEMVAID = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbItem.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbItem
            // 
            this.gbItem.Controls.Add(this.txtEMVTAG);
            this.gbItem.Controls.Add(this.txtEMVTRMCAP);
            this.gbItem.Controls.Add(this.txtEMVAIDTXNTYPE);
            this.gbItem.Controls.Add(this.txtEMVTCC);
            this.gbItem.Controls.Add(this.txtEMVTRMFLRLIM);
            this.gbItem.Controls.Add(this.txtEMVACQID);
            this.gbItem.Controls.Add(this.txtEMVDFTVALTDOL);
            this.gbItem.Controls.Add(this.txtEMVDFTVALDDOL);
            this.gbItem.Controls.Add(this.txtEMVMAXTARPER);
            this.gbItem.Controls.Add(this.txtEMVTARPER);
            this.gbItem.Controls.Add(this.txtEMVTHRVAL);
            this.gbItem.Controls.Add(this.txtEMVTACONL);
            this.gbItem.Controls.Add(this.txtEMVTACDEN);
            this.gbItem.Controls.Add(this.txtEMVTACDFT);
            this.gbItem.Controls.Add(this.txtEMVNAME);
            this.gbItem.Controls.Add(this.txtEMVTRMAVN);
            this.gbItem.Controls.Add(this.txtEMVAID);
            this.gbItem.Controls.Add(this.lblEMVTAG);
            this.gbItem.Controls.Add(this.lblEMVTRMCAP);
            this.gbItem.Controls.Add(this.lblEMVAIDTXNTYPE);
            this.gbItem.Controls.Add(this.lblEMVTCC);
            this.gbItem.Controls.Add(this.lblEMVTRMFLRLIM);
            this.gbItem.Controls.Add(this.lblEMVACQID);
            this.gbItem.Controls.Add(this.lblEMVDFTVALTDOL);
            this.gbItem.Controls.Add(this.lblEMVDFTVALDDOL);
            this.gbItem.Controls.Add(this.lblEMVMAXTARPER);
            this.gbItem.Controls.Add(this.lblEMVTARPER);
            this.gbItem.Controls.Add(this.lblEMVTHRVAL);
            this.gbItem.Controls.Add(this.lblEMVTACONL);
            this.gbItem.Controls.Add(this.lblEMVTACDEN);
            this.gbItem.Controls.Add(this.lblEMVTACDFT);
            this.gbItem.Controls.Add(this.lblEMVNAME);
            this.gbItem.Controls.Add(this.lblEMVTRMAVN);
            this.gbItem.Controls.Add(this.lblEMVAID);
            this.gbItem.Location = new System.Drawing.Point(12, 12);
            this.gbItem.Name = "gbItem";
            this.gbItem.Size = new System.Drawing.Size(567, 312);
            this.gbItem.TabIndex = 0;
            this.gbItem.TabStop = false;
            this.gbItem.Text = "GE";
            // 
            // txtEMVTAG
            // 
            this.txtEMVTAG.Location = new System.Drawing.Point(398, 195);
            this.txtEMVTAG.MaxLength = 3;
            this.txtEMVTAG.Name = "txtEMVTAG";
            this.txtEMVTAG.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTAG.TabIndex = 33;
            // 
            // txtEMVTRMCAP
            // 
            this.txtEMVTRMCAP.Location = new System.Drawing.Point(398, 169);
            this.txtEMVTRMCAP.MaxLength = 6;
            this.txtEMVTRMCAP.Name = "txtEMVTRMCAP";
            this.txtEMVTRMCAP.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTRMCAP.TabIndex = 32;
            // 
            // txtEMVAIDTXNTYPE
            // 
            this.txtEMVAIDTXNTYPE.Location = new System.Drawing.Point(398, 143);
            this.txtEMVAIDTXNTYPE.MaxLength = 2;
            this.txtEMVAIDTXNTYPE.Name = "txtEMVAIDTXNTYPE";
            this.txtEMVAIDTXNTYPE.Size = new System.Drawing.Size(154, 20);
            this.txtEMVAIDTXNTYPE.TabIndex = 31;
            // 
            // txtEMVTCC
            // 
            this.txtEMVTCC.Location = new System.Drawing.Point(398, 117);
            this.txtEMVTCC.MaxLength = 2;
            this.txtEMVTCC.Name = "txtEMVTCC";
            this.txtEMVTCC.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTCC.TabIndex = 30;
            // 
            // txtEMVTRMFLRLIM
            // 
            this.txtEMVTRMFLRLIM.Location = new System.Drawing.Point(398, 91);
            this.txtEMVTRMFLRLIM.MaxLength = 8;
            this.txtEMVTRMFLRLIM.Name = "txtEMVTRMFLRLIM";
            this.txtEMVTRMFLRLIM.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTRMFLRLIM.TabIndex = 29;
            // 
            // txtEMVACQID
            // 
            this.txtEMVACQID.Location = new System.Drawing.Point(398, 65);
            this.txtEMVACQID.MaxLength = 10;
            this.txtEMVACQID.Name = "txtEMVACQID";
            this.txtEMVACQID.Size = new System.Drawing.Size(154, 20);
            this.txtEMVACQID.TabIndex = 28;
            // 
            // txtEMVDFTVALTDOL
            // 
            this.txtEMVDFTVALTDOL.Location = new System.Drawing.Point(398, 13);
            this.txtEMVDFTVALTDOL.MaxLength = 100;
            this.txtEMVDFTVALTDOL.Multiline = true;
            this.txtEMVDFTVALTDOL.Name = "txtEMVDFTVALTDOL";
            this.txtEMVDFTVALTDOL.Size = new System.Drawing.Size(154, 42);
            this.txtEMVDFTVALTDOL.TabIndex = 27;
            // 
            // txtEMVDFTVALDDOL
            // 
            this.txtEMVDFTVALDDOL.Location = new System.Drawing.Point(117, 247);
            this.txtEMVDFTVALDDOL.MaxLength = 100;
            this.txtEMVDFTVALDDOL.Multiline = true;
            this.txtEMVDFTVALDDOL.Name = "txtEMVDFTVALDDOL";
            this.txtEMVDFTVALDDOL.Size = new System.Drawing.Size(154, 59);
            this.txtEMVDFTVALDDOL.TabIndex = 26;
            // 
            // txtEMVMAXTARPER
            // 
            this.txtEMVMAXTARPER.Location = new System.Drawing.Point(117, 221);
            this.txtEMVMAXTARPER.MaxLength = 2;
            this.txtEMVMAXTARPER.Name = "txtEMVMAXTARPER";
            this.txtEMVMAXTARPER.Size = new System.Drawing.Size(154, 20);
            this.txtEMVMAXTARPER.TabIndex = 25;
            // 
            // txtEMVTARPER
            // 
            this.txtEMVTARPER.Location = new System.Drawing.Point(117, 195);
            this.txtEMVTARPER.MaxLength = 2;
            this.txtEMVTARPER.Name = "txtEMVTARPER";
            this.txtEMVTARPER.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTARPER.TabIndex = 24;
            // 
            // txtEMVTHRVAL
            // 
            this.txtEMVTHRVAL.Location = new System.Drawing.Point(117, 169);
            this.txtEMVTHRVAL.MaxLength = 8;
            this.txtEMVTHRVAL.Name = "txtEMVTHRVAL";
            this.txtEMVTHRVAL.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTHRVAL.TabIndex = 23;
            // 
            // txtEMVTACONL
            // 
            this.txtEMVTACONL.Location = new System.Drawing.Point(117, 143);
            this.txtEMVTACONL.MaxLength = 10;
            this.txtEMVTACONL.Name = "txtEMVTACONL";
            this.txtEMVTACONL.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTACONL.TabIndex = 22;
            // 
            // txtEMVTACDEN
            // 
            this.txtEMVTACDEN.Location = new System.Drawing.Point(117, 117);
            this.txtEMVTACDEN.MaxLength = 10;
            this.txtEMVTACDEN.Name = "txtEMVTACDEN";
            this.txtEMVTACDEN.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTACDEN.TabIndex = 21;
            // 
            // txtEMVTACDFT
            // 
            this.txtEMVTACDFT.Location = new System.Drawing.Point(117, 91);
            this.txtEMVTACDFT.MaxLength = 10;
            this.txtEMVTACDFT.Name = "txtEMVTACDFT";
            this.txtEMVTACDFT.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTACDFT.TabIndex = 20;
            // 
            // txtEMVNAME
            // 
            this.txtEMVNAME.Location = new System.Drawing.Point(117, 65);
            this.txtEMVNAME.MaxLength = 24;
            this.txtEMVNAME.Name = "txtEMVNAME";
            this.txtEMVNAME.Size = new System.Drawing.Size(154, 20);
            this.txtEMVNAME.TabIndex = 19;
            // 
            // txtEMVTRMAVN
            // 
            this.txtEMVTRMAVN.Location = new System.Drawing.Point(117, 39);
            this.txtEMVTRMAVN.MaxLength = 4;
            this.txtEMVTRMAVN.Name = "txtEMVTRMAVN";
            this.txtEMVTRMAVN.Size = new System.Drawing.Size(154, 20);
            this.txtEMVTRMAVN.TabIndex = 18;
            // 
            // txtEMVAID
            // 
            this.txtEMVAID.Location = new System.Drawing.Point(117, 13);
            this.txtEMVAID.MaxLength = 16;
            this.txtEMVAID.Name = "txtEMVAID";
            this.txtEMVAID.Size = new System.Drawing.Size(154, 20);
            this.txtEMVAID.TabIndex = 17;
            // 
            // lblEMVTAG
            // 
            this.lblEMVTAG.AutoSize = true;
            this.lblEMVTAG.Location = new System.Drawing.Point(284, 198);
            this.lblEMVTAG.Name = "lblEMVTAG";
            this.lblEMVTAG.Size = new System.Drawing.Size(70, 13);
            this.lblEMVTAG.TabIndex = 16;
            this.lblEMVTAG.Text = "Indicator Tag";
            // 
            // lblEMVTRMCAP
            // 
            this.lblEMVTRMCAP.AutoSize = true;
            this.lblEMVTRMCAP.Location = new System.Drawing.Point(285, 172);
            this.lblEMVTRMCAP.Name = "lblEMVTRMCAP";
            this.lblEMVTRMCAP.Size = new System.Drawing.Size(95, 13);
            this.lblEMVTRMCAP.TabIndex = 15;
            this.lblEMVTRMCAP.Text = "Terminal Capability";
            // 
            // lblEMVAIDTXNTYPE
            // 
            this.lblEMVAIDTXNTYPE.AutoSize = true;
            this.lblEMVAIDTXNTYPE.Location = new System.Drawing.Point(285, 146);
            this.lblEMVAIDTXNTYPE.Name = "lblEMVAIDTXNTYPE";
            this.lblEMVAIDTXNTYPE.Size = new System.Drawing.Size(49, 13);
            this.lblEMVAIDTXNTYPE.TabIndex = 14;
            this.lblEMVAIDTXNTYPE.Text = "Trx Type";
            // 
            // lblEMVTCC
            // 
            this.lblEMVTCC.AutoSize = true;
            this.lblEMVTCC.Location = new System.Drawing.Point(285, 120);
            this.lblEMVTCC.Name = "lblEMVTCC";
            this.lblEMVTCC.Size = new System.Drawing.Size(77, 13);
            this.lblEMVTCC.TabIndex = 13;
            this.lblEMVTCC.Text = "Category Code";
            // 
            // lblEMVTRMFLRLIM
            // 
            this.lblEMVTRMFLRLIM.AutoSize = true;
            this.lblEMVTRMFLRLIM.Location = new System.Drawing.Point(284, 94);
            this.lblEMVTRMFLRLIM.Name = "lblEMVTRMFLRLIM";
            this.lblEMVTRMFLRLIM.Size = new System.Drawing.Size(54, 13);
            this.lblEMVTRMFLRLIM.TabIndex = 12;
            this.lblEMVTRMFLRLIM.Text = "Floor Limit";
            // 
            // lblEMVACQID
            // 
            this.lblEMVACQID.AutoSize = true;
            this.lblEMVACQID.Location = new System.Drawing.Point(285, 68);
            this.lblEMVACQID.Name = "lblEMVACQID";
            this.lblEMVACQID.Size = new System.Drawing.Size(68, 13);
            this.lblEMVACQID.TabIndex = 11;
            this.lblEMVACQID.Text = "Bank Acq ID";
            // 
            // lblEMVDFTVALTDOL
            // 
            this.lblEMVDFTVALTDOL.AutoSize = true;
            this.lblEMVDFTVALTDOL.Location = new System.Drawing.Point(285, 16);
            this.lblEMVDFTVALTDOL.Name = "lblEMVDFTVALTDOL";
            this.lblEMVDFTVALTDOL.Size = new System.Drawing.Size(73, 13);
            this.lblEMVDFTVALTDOL.TabIndex = 10;
            this.lblEMVDFTVALTDOL.Text = "Default TDOL";
            // 
            // lblEMVDFTVALDDOL
            // 
            this.lblEMVDFTVALDDOL.AutoSize = true;
            this.lblEMVDFTVALDDOL.Location = new System.Drawing.Point(7, 250);
            this.lblEMVDFTVALDDOL.Name = "lblEMVDFTVALDDOL";
            this.lblEMVDFTVALDDOL.Size = new System.Drawing.Size(74, 13);
            this.lblEMVDFTVALDDOL.TabIndex = 9;
            this.lblEMVDFTVALDDOL.Text = "Default DDOL";
            // 
            // lblEMVMAXTARPER
            // 
            this.lblEMVMAXTARPER.AutoSize = true;
            this.lblEMVMAXTARPER.Location = new System.Drawing.Point(7, 224);
            this.lblEMVMAXTARPER.Name = "lblEMVMAXTARPER";
            this.lblEMVMAXTARPER.Size = new System.Drawing.Size(72, 13);
            this.lblEMVMAXTARPER.TabIndex = 8;
            this.lblEMVMAXTARPER.Text = "Max Target %";
            // 
            // lblEMVTARPER
            // 
            this.lblEMVTARPER.AutoSize = true;
            this.lblEMVTARPER.Location = new System.Drawing.Point(7, 198);
            this.lblEMVTARPER.Name = "lblEMVTARPER";
            this.lblEMVTARPER.Size = new System.Drawing.Size(49, 13);
            this.lblEMVTARPER.TabIndex = 7;
            this.lblEMVTARPER.Text = "Target %";
            // 
            // lblEMVTHRVAL
            // 
            this.lblEMVTHRVAL.AutoSize = true;
            this.lblEMVTHRVAL.Location = new System.Drawing.Point(7, 172);
            this.lblEMVTHRVAL.Name = "lblEMVTHRVAL";
            this.lblEMVTHRVAL.Size = new System.Drawing.Size(54, 13);
            this.lblEMVTHRVAL.TabIndex = 6;
            this.lblEMVTHRVAL.Text = "Threshold";
            // 
            // lblEMVTACONL
            // 
            this.lblEMVTACONL.AutoSize = true;
            this.lblEMVTACONL.Location = new System.Drawing.Point(7, 146);
            this.lblEMVTACONL.Name = "lblEMVTACONL";
            this.lblEMVTACONL.Size = new System.Drawing.Size(61, 13);
            this.lblEMVTACONL.TabIndex = 5;
            this.lblEMVTACONL.Text = "TAC Online";
            // 
            // lblEMVTACDEN
            // 
            this.lblEMVTACDEN.AutoSize = true;
            this.lblEMVTACDEN.Location = new System.Drawing.Point(6, 120);
            this.lblEMVTACDEN.Name = "lblEMVTACDEN";
            this.lblEMVTACDEN.Size = new System.Drawing.Size(61, 13);
            this.lblEMVTACDEN.TabIndex = 4;
            this.lblEMVTACDEN.Text = "TAC Denial";
            // 
            // lblEMVTACDFT
            // 
            this.lblEMVTACDFT.AutoSize = true;
            this.lblEMVTACDFT.Location = new System.Drawing.Point(7, 94);
            this.lblEMVTACDFT.Name = "lblEMVTACDFT";
            this.lblEMVTACDFT.Size = new System.Drawing.Size(65, 13);
            this.lblEMVTACDFT.TabIndex = 3;
            this.lblEMVTACDFT.Text = "TAC Default";
            // 
            // lblEMVNAME
            // 
            this.lblEMVNAME.AutoSize = true;
            this.lblEMVNAME.Location = new System.Drawing.Point(6, 68);
            this.lblEMVNAME.Name = "lblEMVNAME";
            this.lblEMVNAME.Size = new System.Drawing.Size(35, 13);
            this.lblEMVNAME.TabIndex = 2;
            this.lblEMVNAME.Text = "Name";
            // 
            // lblEMVTRMAVN
            // 
            this.lblEMVTRMAVN.AutoSize = true;
            this.lblEMVTRMAVN.Location = new System.Drawing.Point(7, 42);
            this.lblEMVTRMAVN.Name = "lblEMVTRMAVN";
            this.lblEMVTRMAVN.Size = new System.Drawing.Size(88, 13);
            this.lblEMVTRMAVN.TabIndex = 1;
            this.lblEMVTRMAVN.Text = "App Ver. Number";
            // 
            // lblEMVAID
            // 
            this.lblEMVAID.AutoSize = true;
            this.lblEMVAID.Location = new System.Drawing.Point(7, 16);
            this.lblEMVAID.Name = "lblEMVAID";
            this.lblEMVAID.Size = new System.Drawing.Size(39, 13);
            this.lblEMVAID.TabIndex = 0;
            this.lblEMVAID.Text = "AID ID";
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(12, 330);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(567, 47);
            this.gbButton.TabIndex = 34;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(477, 18);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(396, 18);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmEditAID2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(593, 389);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbItem);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmEditAID2";
            this.Load += new System.EventHandler(this.FrmEditAID2_Load);
            this.gbItem.ResumeLayout(false);
            this.gbItem.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbItem;
        private System.Windows.Forms.Label lblEMVTAG;
        private System.Windows.Forms.Label lblEMVTRMCAP;
        private System.Windows.Forms.Label lblEMVAIDTXNTYPE;
        private System.Windows.Forms.Label lblEMVTCC;
        private System.Windows.Forms.Label lblEMVTRMFLRLIM;
        private System.Windows.Forms.Label lblEMVACQID;
        private System.Windows.Forms.Label lblEMVDFTVALTDOL;
        private System.Windows.Forms.Label lblEMVDFTVALDDOL;
        private System.Windows.Forms.Label lblEMVMAXTARPER;
        private System.Windows.Forms.Label lblEMVTARPER;
        private System.Windows.Forms.Label lblEMVTHRVAL;
        private System.Windows.Forms.Label lblEMVTACONL;
        private System.Windows.Forms.Label lblEMVTACDEN;
        private System.Windows.Forms.Label lblEMVTACDFT;
        private System.Windows.Forms.Label lblEMVNAME;
        private System.Windows.Forms.Label lblEMVTRMAVN;
        private System.Windows.Forms.Label lblEMVAID;
        private System.Windows.Forms.TextBox txtEMVTAG;
        private System.Windows.Forms.TextBox txtEMVTRMCAP;
        private System.Windows.Forms.TextBox txtEMVAIDTXNTYPE;
        private System.Windows.Forms.TextBox txtEMVTCC;
        private System.Windows.Forms.TextBox txtEMVTRMFLRLIM;
        private System.Windows.Forms.TextBox txtEMVACQID;
        private System.Windows.Forms.TextBox txtEMVDFTVALTDOL;
        private System.Windows.Forms.TextBox txtEMVDFTVALDDOL;
        private System.Windows.Forms.TextBox txtEMVMAXTARPER;
        private System.Windows.Forms.TextBox txtEMVTARPER;
        private System.Windows.Forms.TextBox txtEMVTHRVAL;
        private System.Windows.Forms.TextBox txtEMVTACONL;
        private System.Windows.Forms.TextBox txtEMVTACDEN;
        private System.Windows.Forms.TextBox txtEMVTACDFT;
        private System.Windows.Forms.TextBox txtEMVNAME;
        private System.Windows.Forms.TextBox txtEMVTRMAVN;
        private System.Windows.Forms.TextBox txtEMVAID;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}