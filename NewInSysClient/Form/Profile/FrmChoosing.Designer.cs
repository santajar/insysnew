﻿namespace InSys
{
    partial class FrmChoosing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmChoosing));
            this.btnDel = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnAddCurrentIssuer = new System.Windows.Forms.Button();
            this.cmbChoosingName = new System.Windows.Forms.ComboBox();
            this.lblChoosingName = new System.Windows.Forms.Label();
            this.gbInput = new System.Windows.Forms.GroupBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCopy = new System.Windows.Forms.Button();
            this.gbInput.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDel
            // 
            this.btnDel.Image = ((System.Drawing.Image)(resources.GetObject("btnDel.Image")));
            this.btnDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDel.Location = new System.Drawing.Point(211, 19);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(95, 23);
            this.btnDel.TabIndex = 2;
            this.btnDel.Text = "&Delete";
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(312, 19);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(95, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Tag = "Cancel";
            this.btnCancel.Text = "Close";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEdit.Location = new System.Drawing.Point(110, 19);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(95, 23);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "&Edit";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNew.Location = new System.Drawing.Point(9, 19);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(95, 23);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "&Add";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnAddCurrentIssuer
            // 
            this.btnAddCurrentIssuer.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnAddCurrentIssuer.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCurrentIssuer.Image")));
            this.btnAddCurrentIssuer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddCurrentIssuer.Location = new System.Drawing.Point(211, 19);
            this.btnAddCurrentIssuer.Name = "btnAddCurrentIssuer";
            this.btnAddCurrentIssuer.Size = new System.Drawing.Size(95, 23);
            this.btnAddCurrentIssuer.TabIndex = 0;
            this.btnAddCurrentIssuer.Text = "OK";
            this.btnAddCurrentIssuer.Visible = false;
            // 
            // cmbChoosingName
            // 
            this.cmbChoosingName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbChoosingName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbChoosingName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbChoosingName.Location = new System.Drawing.Point(114, 16);
            this.cmbChoosingName.Name = "cmbChoosingName";
            this.cmbChoosingName.Size = new System.Drawing.Size(295, 24);
            this.cmbChoosingName.TabIndex = 0;
            // 
            // lblChoosingName
            // 
            this.lblChoosingName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChoosingName.Location = new System.Drawing.Point(16, 19);
            this.lblChoosingName.Name = "lblChoosingName";
            this.lblChoosingName.Size = new System.Drawing.Size(115, 23);
            this.lblChoosingName.TabIndex = 14;
            this.lblChoosingName.Text = "Card Name";
            // 
            // gbInput
            // 
            this.gbInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbInput.Controls.Add(this.cmbChoosingName);
            this.gbInput.Controls.Add(this.lblChoosingName);
            this.gbInput.Location = new System.Drawing.Point(12, 4);
            this.gbInput.Name = "gbInput";
            this.gbInput.Size = new System.Drawing.Size(520, 54);
            this.gbInput.TabIndex = 0;
            this.gbInput.TabStop = false;
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnCopy);
            this.gbButton.Controls.Add(this.btnDel);
            this.gbButton.Controls.Add(this.btnAddCurrentIssuer);
            this.gbButton.Controls.Add(this.btnNew);
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnEdit);
            this.gbButton.Location = new System.Drawing.Point(12, 64);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(520, 55);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnCopy
            // 
            this.btnCopy.Enabled = false;
            this.btnCopy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCopy.Location = new System.Drawing.Point(415, 19);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(95, 23);
            this.btnCopy.TabIndex = 4;
            this.btnCopy.Text = "&Copy";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // FrmChoosing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(548, 130);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbInput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmChoosing";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Card Management";
            this.Load += new System.EventHandler(this.FrmChoosing_Load);
            this.gbInput.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnDel;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnEdit;
        internal System.Windows.Forms.Button btnNew;
        internal System.Windows.Forms.Button btnAddCurrentIssuer;
        internal System.Windows.Forms.ComboBox cmbChoosingName;
        internal System.Windows.Forms.Label lblChoosingName;
        private System.Windows.Forms.GroupBox gbInput;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnCopy;
    }
}