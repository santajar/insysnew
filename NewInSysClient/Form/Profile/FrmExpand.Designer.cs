namespace InSys
{
    partial class FrmExpand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExpand));
            this.ButtonIcon = new System.Windows.Forms.ImageList(this.components);
            this.IconList = new System.Windows.Forms.ImageList(this.components);
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.gbView = new System.Windows.Forms.GroupBox();
            this.lblTagDetail = new System.Windows.Forms.Label();
            this.lblDetail = new System.Windows.Forms.Label();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.tvTerminal = new System.Windows.Forms.TreeView();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvTerminal = new System.Windows.Forms.DataGridView();
            this.dgvTagDetail = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gbEdcInf = new System.Windows.Forms.GroupBox();
            this.tabcntrInformation = new System.Windows.Forms.TabControl();
            this.tabPageAutoInit = new System.Windows.Forms.TabPage();
            this.txtICCID = new System.Windows.Forms.TextBox();
            this.txtMemUsage = new System.Windows.Forms.TextBox();
            this.txtMCSN = new System.Windows.Forms.TextBox();
            this.txtReaderSN = new System.Windows.Forms.TextBox();
            this.txtPSAMSN = new System.Windows.Forms.TextBox();
            this.txtEDCSN = new System.Windows.Forms.TextBox();
            this.txtKernelVer = new System.Windows.Forms.TextBox();
            this.txtPABX = new System.Windows.Forms.TextBox();
            this.txtLastInit = new System.Windows.Forms.TextBox();
            this.txtOSVer = new System.Windows.Forms.TextBox();
            this.txtSoftwareVer = new System.Windows.Forms.TextBox();
            this.txtTID = new System.Windows.Forms.TextBox();
            this.lblMemUsage = new System.Windows.Forms.Label();
            this.lblMCSN = new System.Windows.Forms.Label();
            this.lblPSAMSN = new System.Windows.Forms.Label();
            this.lblReaderSN = new System.Windows.Forms.Label();
            this.lblEDCSN = new System.Windows.Forms.Label();
            this.lblKernelVersion = new System.Windows.Forms.Label();
            this.lblOSVer = new System.Windows.Forms.Label();
            this.lblSoftVer = new System.Windows.Forms.Label();
            this.lblPABX = new System.Windows.Forms.Label();
            this.lblLastInitTime = new System.Windows.Forms.Label();
            this.lblTID = new System.Windows.Forms.Label();
            this.lblICCID = new System.Windows.Forms.Label();
            this.tabPageInit = new System.Windows.Forms.TabPage();
            this.txtInitSoftwareVer = new System.Windows.Forms.TextBox();
            this.lblSoftwareVersion = new System.Windows.Forms.Label();
            this.txtTIDInit = new System.Windows.Forms.TextBox();
            this.lblTIDInit = new System.Windows.Forms.Label();
            this.txtEdcSNInit = new System.Windows.Forms.TextBox();
            this.lblEDCSNnInit = new System.Windows.Forms.Label();
            this.lvAppEDC = new System.Windows.Forms.ListView();
            this.btnRemovePackage = new System.Windows.Forms.Button();
            this.btnAddPackage = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.menuExpand = new System.Windows.Forms.MenuStrip();
            this.managementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementCard = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementAID = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementPK = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementTLE = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementEmv = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementRmtDownload = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementGprsEth = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementPinPad = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementPromo = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementInitialFLAZZ = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementCardType = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementEdcMonitor = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRequestPaperReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuManagementSN = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registerTerminalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advanceSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createTerminalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewLastTerminalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSearchTID = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSearchSN = new System.Windows.Forms.ToolStripTextBox();
            this.gbView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTagDetail)).BeginInit();
            this.gbEdcInf.SuspendLayout();
            this.tabcntrInformation.SuspendLayout();
            this.tabPageAutoInit.SuspendLayout();
            this.tabPageInit.SuspendLayout();
            this.menuExpand.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonIcon
            // 
            this.ButtonIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ButtonIcon.ImageStream")));
            this.ButtonIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ButtonIcon.Images.SetKeyName(0, "");
            this.ButtonIcon.Images.SetKeyName(1, "");
            this.ButtonIcon.Images.SetKeyName(2, "");
            // 
            // IconList
            // 
            this.IconList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconList.ImageStream")));
            this.IconList.TransparentColor = System.Drawing.Color.Transparent;
            this.IconList.Images.SetKeyName(0, "");
            this.IconList.Images.SetKeyName(1, "");
            this.IconList.Images.SetKeyName(2, "");
            this.IconList.Images.SetKeyName(3, "");
            this.IconList.Images.SetKeyName(4, "search.jpg");
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.AutoScroll = true;
            this.ContentPanel.Size = new System.Drawing.Size(965, 512);
            // 
            // gbView
            // 
            this.gbView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbView.Controls.Add(this.lblTagDetail);
            this.gbView.Controls.Add(this.lblDetail);
            this.gbView.Controls.Add(this.lblTerminal);
            this.gbView.Controls.Add(this.tvTerminal);
            this.gbView.Controls.Add(this.btnClose);
            this.gbView.Controls.Add(this.dgvTerminal);
            this.gbView.Controls.Add(this.dgvTagDetail);
            this.gbView.Location = new System.Drawing.Point(8, 25);
            this.gbView.Margin = new System.Windows.Forms.Padding(4);
            this.gbView.Name = "gbView";
            this.gbView.Padding = new System.Windows.Forms.Padding(4);
            this.gbView.Size = new System.Drawing.Size(1057, 254);
            this.gbView.TabIndex = 0;
            this.gbView.TabStop = false;
            // 
            // lblTagDetail
            // 
            this.lblTagDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTagDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTagDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTagDetail.Location = new System.Drawing.Point(596, 17);
            this.lblTagDetail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTagDetail.Name = "lblTagDetail";
            this.lblTagDetail.Size = new System.Drawing.Size(452, 31);
            this.lblTagDetail.TabIndex = 2;
            this.lblTagDetail.Text = "TERMINAL DETAIL";
            this.lblTagDetail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDetail
            // 
            this.lblDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDetail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetail.Location = new System.Drawing.Point(263, 17);
            this.lblDetail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Size = new System.Drawing.Size(333, 31);
            this.lblDetail.TabIndex = 1;
            this.lblDetail.Text = "TERMINAL TREE";
            this.lblDetail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTerminal
            // 
            this.lblTerminal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminal.Location = new System.Drawing.Point(9, 17);
            this.lblTerminal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(253, 31);
            this.lblTerminal.TabIndex = 0;
            this.lblTerminal.Text = "TERMINAL";
            this.lblTerminal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tvTerminal
            // 
            this.tvTerminal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tvTerminal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvTerminal.Location = new System.Drawing.Point(263, 49);
            this.tvTerminal.Margin = new System.Windows.Forms.Padding(4);
            this.tvTerminal.Name = "tvTerminal";
            this.tvTerminal.Size = new System.Drawing.Size(332, 198);
            this.tvTerminal.TabIndex = 398;
            this.tvTerminal.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tvTerminal_BeforeLabelEdit);
            this.tvTerminal.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tvTerminal_AfterLabelEdit);
            this.tvTerminal.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvTerminal_AfterSelect);
            this.tvTerminal.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvTerminal_NodeMouseClick);
            this.tvTerminal.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvTerminal_NodeMouseDoubleClick);
            this.tvTerminal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tvTerminal_KeyPress);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(951, 27);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(13, 12);
            this.btnClose.TabIndex = 397;
            this.btnClose.TabStop = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgvTerminal
            // 
            this.dgvTerminal.AllowUserToAddRows = false;
            this.dgvTerminal.AllowUserToDeleteRows = false;
            this.dgvTerminal.AllowUserToResizeColumns = false;
            this.dgvTerminal.AllowUserToResizeRows = false;
            this.dgvTerminal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvTerminal.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTerminal.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvTerminal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTerminal.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTerminal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTerminal.ColumnHeadersVisible = false;
            this.dgvTerminal.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvTerminal.EnableHeadersVisualStyles = false;
            this.dgvTerminal.Location = new System.Drawing.Point(9, 49);
            this.dgvTerminal.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTerminal.MultiSelect = false;
            this.dgvTerminal.Name = "dgvTerminal";
            this.dgvTerminal.ReadOnly = true;
            this.dgvTerminal.RowHeadersVisible = false;
            this.dgvTerminal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvTerminal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvTerminal.Size = new System.Drawing.Size(253, 198);
            this.dgvTerminal.TabIndex = 397;
            this.dgvTerminal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvTerminal_KeyDown);
            this.dgvTerminal.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvTerminal_MouseClick);
            this.dgvTerminal.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgvTerminal_MouseUp);
            // 
            // dgvTagDetail
            // 
            this.dgvTagDetail.AllowUserToAddRows = false;
            this.dgvTagDetail.AllowUserToDeleteRows = false;
            this.dgvTagDetail.AllowUserToResizeRows = false;
            this.dgvTagDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTagDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvTagDetail.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvTagDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvTagDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvTagDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTagDetail.EnableHeadersVisualStyles = false;
            this.dgvTagDetail.Location = new System.Drawing.Point(596, 49);
            this.dgvTagDetail.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTagDetail.MultiSelect = false;
            this.dgvTagDetail.Name = "dgvTagDetail";
            this.dgvTagDetail.ReadOnly = true;
            this.dgvTagDetail.RowHeadersVisible = false;
            this.dgvTagDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTagDetail.Size = new System.Drawing.Size(452, 198);
            this.dgvTagDetail.TabIndex = 399;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.ShowImageMargin = false;
            this.contextMenuStrip.Size = new System.Drawing.Size(36, 4);
            // 
            // gbEdcInf
            // 
            this.gbEdcInf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbEdcInf.Controls.Add(this.tabcntrInformation);
            this.gbEdcInf.Location = new System.Drawing.Point(8, 287);
            this.gbEdcInf.Margin = new System.Windows.Forms.Padding(4);
            this.gbEdcInf.Name = "gbEdcInf";
            this.gbEdcInf.Padding = new System.Windows.Forms.Padding(4);
            this.gbEdcInf.Size = new System.Drawing.Size(1057, 199);
            this.gbEdcInf.TabIndex = 3;
            this.gbEdcInf.TabStop = false;
            this.gbEdcInf.Text = "EDC Information";
            // 
            // tabcntrInformation
            // 
            this.tabcntrInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabcntrInformation.Controls.Add(this.tabPageAutoInit);
            this.tabcntrInformation.Controls.Add(this.tabPageInit);
            this.tabcntrInformation.Location = new System.Drawing.Point(12, 20);
            this.tabcntrInformation.Margin = new System.Windows.Forms.Padding(4);
            this.tabcntrInformation.Name = "tabcntrInformation";
            this.tabcntrInformation.SelectedIndex = 0;
            this.tabcntrInformation.Size = new System.Drawing.Size(1036, 165);
            this.tabcntrInformation.TabIndex = 22;
            // 
            // tabPageAutoInit
            // 
            this.tabPageAutoInit.Controls.Add(this.txtICCID);
            this.tabPageAutoInit.Controls.Add(this.txtMemUsage);
            this.tabPageAutoInit.Controls.Add(this.txtMCSN);
            this.tabPageAutoInit.Controls.Add(this.txtReaderSN);
            this.tabPageAutoInit.Controls.Add(this.txtPSAMSN);
            this.tabPageAutoInit.Controls.Add(this.txtEDCSN);
            this.tabPageAutoInit.Controls.Add(this.txtKernelVer);
            this.tabPageAutoInit.Controls.Add(this.txtPABX);
            this.tabPageAutoInit.Controls.Add(this.txtLastInit);
            this.tabPageAutoInit.Controls.Add(this.txtOSVer);
            this.tabPageAutoInit.Controls.Add(this.txtSoftwareVer);
            this.tabPageAutoInit.Controls.Add(this.txtTID);
            this.tabPageAutoInit.Controls.Add(this.lblMemUsage);
            this.tabPageAutoInit.Controls.Add(this.lblMCSN);
            this.tabPageAutoInit.Controls.Add(this.lblPSAMSN);
            this.tabPageAutoInit.Controls.Add(this.lblReaderSN);
            this.tabPageAutoInit.Controls.Add(this.lblEDCSN);
            this.tabPageAutoInit.Controls.Add(this.lblKernelVersion);
            this.tabPageAutoInit.Controls.Add(this.lblOSVer);
            this.tabPageAutoInit.Controls.Add(this.lblSoftVer);
            this.tabPageAutoInit.Controls.Add(this.lblPABX);
            this.tabPageAutoInit.Controls.Add(this.lblLastInitTime);
            this.tabPageAutoInit.Controls.Add(this.lblTID);
            this.tabPageAutoInit.Controls.Add(this.lblICCID);
            this.tabPageAutoInit.Location = new System.Drawing.Point(4, 25);
            this.tabPageAutoInit.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageAutoInit.Name = "tabPageAutoInit";
            this.tabPageAutoInit.Padding = new System.Windows.Forms.Padding(4);
            this.tabPageAutoInit.Size = new System.Drawing.Size(1028, 136);
            this.tabPageAutoInit.TabIndex = 0;
            this.tabPageAutoInit.Tag = "autoinit";
            this.tabPageAutoInit.Text = "Initialize";
            this.tabPageAutoInit.UseVisualStyleBackColor = true;
            // 
            // txtICCID
            // 
            this.txtICCID.Location = new System.Drawing.Point(636, 103);
            this.txtICCID.Margin = new System.Windows.Forms.Padding(4);
            this.txtICCID.Name = "txtICCID";
            this.txtICCID.ReadOnly = true;
            this.txtICCID.Size = new System.Drawing.Size(125, 22);
            this.txtICCID.TabIndex = 45;
            // 
            // txtMemUsage
            // 
            this.txtMemUsage.Location = new System.Drawing.Point(636, 71);
            this.txtMemUsage.Margin = new System.Windows.Forms.Padding(4);
            this.txtMemUsage.Name = "txtMemUsage";
            this.txtMemUsage.ReadOnly = true;
            this.txtMemUsage.Size = new System.Drawing.Size(125, 22);
            this.txtMemUsage.TabIndex = 43;
            // 
            // txtMCSN
            // 
            this.txtMCSN.Location = new System.Drawing.Point(636, 39);
            this.txtMCSN.Margin = new System.Windows.Forms.Padding(4);
            this.txtMCSN.Name = "txtMCSN";
            this.txtMCSN.ReadOnly = true;
            this.txtMCSN.Size = new System.Drawing.Size(125, 22);
            this.txtMCSN.TabIndex = 41;
            // 
            // txtReaderSN
            // 
            this.txtReaderSN.Location = new System.Drawing.Point(368, 103);
            this.txtReaderSN.Margin = new System.Windows.Forms.Padding(4);
            this.txtReaderSN.Name = "txtReaderSN";
            this.txtReaderSN.ReadOnly = true;
            this.txtReaderSN.Size = new System.Drawing.Size(125, 22);
            this.txtReaderSN.TabIndex = 37;
            // 
            // txtPSAMSN
            // 
            this.txtPSAMSN.Location = new System.Drawing.Point(636, 7);
            this.txtPSAMSN.Margin = new System.Windows.Forms.Padding(4);
            this.txtPSAMSN.Name = "txtPSAMSN";
            this.txtPSAMSN.ReadOnly = true;
            this.txtPSAMSN.Size = new System.Drawing.Size(125, 22);
            this.txtPSAMSN.TabIndex = 39;
            // 
            // txtEDCSN
            // 
            this.txtEDCSN.Location = new System.Drawing.Point(368, 71);
            this.txtEDCSN.Margin = new System.Windows.Forms.Padding(4);
            this.txtEDCSN.Name = "txtEDCSN";
            this.txtEDCSN.ReadOnly = true;
            this.txtEDCSN.Size = new System.Drawing.Size(125, 22);
            this.txtEDCSN.TabIndex = 35;
            // 
            // txtKernelVer
            // 
            this.txtKernelVer.Location = new System.Drawing.Point(368, 39);
            this.txtKernelVer.Margin = new System.Windows.Forms.Padding(4);
            this.txtKernelVer.Name = "txtKernelVer";
            this.txtKernelVer.ReadOnly = true;
            this.txtKernelVer.Size = new System.Drawing.Size(125, 22);
            this.txtKernelVer.TabIndex = 33;
            // 
            // txtPABX
            // 
            this.txtPABX.Location = new System.Drawing.Point(129, 71);
            this.txtPABX.Margin = new System.Windows.Forms.Padding(4);
            this.txtPABX.Name = "txtPABX";
            this.txtPABX.ReadOnly = true;
            this.txtPABX.Size = new System.Drawing.Size(104, 22);
            this.txtPABX.TabIndex = 27;
            // 
            // txtLastInit
            // 
            this.txtLastInit.Location = new System.Drawing.Point(129, 39);
            this.txtLastInit.Margin = new System.Windows.Forms.Padding(4);
            this.txtLastInit.Name = "txtLastInit";
            this.txtLastInit.ReadOnly = true;
            this.txtLastInit.Size = new System.Drawing.Size(104, 22);
            this.txtLastInit.TabIndex = 25;
            // 
            // txtOSVer
            // 
            this.txtOSVer.Location = new System.Drawing.Point(368, 7);
            this.txtOSVer.Margin = new System.Windows.Forms.Padding(4);
            this.txtOSVer.Name = "txtOSVer";
            this.txtOSVer.ReadOnly = true;
            this.txtOSVer.Size = new System.Drawing.Size(125, 22);
            this.txtOSVer.TabIndex = 31;
            // 
            // txtSoftwareVer
            // 
            this.txtSoftwareVer.Location = new System.Drawing.Point(129, 103);
            this.txtSoftwareVer.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoftwareVer.Name = "txtSoftwareVer";
            this.txtSoftwareVer.ReadOnly = true;
            this.txtSoftwareVer.Size = new System.Drawing.Size(104, 22);
            this.txtSoftwareVer.TabIndex = 29;
            // 
            // txtTID
            // 
            this.txtTID.Location = new System.Drawing.Point(129, 7);
            this.txtTID.Margin = new System.Windows.Forms.Padding(4);
            this.txtTID.Name = "txtTID";
            this.txtTID.ReadOnly = true;
            this.txtTID.Size = new System.Drawing.Size(104, 22);
            this.txtTID.TabIndex = 23;
            // 
            // lblMemUsage
            // 
            this.lblMemUsage.AutoSize = true;
            this.lblMemUsage.Location = new System.Drawing.Point(519, 74);
            this.lblMemUsage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMemUsage.Name = "lblMemUsage";
            this.lblMemUsage.Size = new System.Drawing.Size(107, 17);
            this.lblMemUsage.TabIndex = 42;
            this.lblMemUsage.Text = "Memory Usage ";
            // 
            // lblMCSN
            // 
            this.lblMCSN.AutoSize = true;
            this.lblMCSN.Location = new System.Drawing.Point(501, 42);
            this.lblMCSN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMCSN.Name = "lblMCSN";
            this.lblMCSN.Size = new System.Drawing.Size(124, 17);
            this.lblMCSN.TabIndex = 40;
            this.lblMCSN.Text = "Merchant Card SN";
            // 
            // lblPSAMSN
            // 
            this.lblPSAMSN.AutoSize = true;
            this.lblPSAMSN.Location = new System.Drawing.Point(517, 10);
            this.lblPSAMSN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPSAMSN.Name = "lblPSAMSN";
            this.lblPSAMSN.Size = new System.Drawing.Size(108, 17);
            this.lblPSAMSN.TabIndex = 38;
            this.lblPSAMSN.Text = "PSAM Serial No";
            // 
            // lblReaderSN
            // 
            this.lblReaderSN.AutoSize = true;
            this.lblReaderSN.Location = new System.Drawing.Point(240, 106);
            this.lblReaderSN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReaderSN.Name = "lblReaderSN";
            this.lblReaderSN.Size = new System.Drawing.Size(117, 17);
            this.lblReaderSN.TabIndex = 36;
            this.lblReaderSN.Text = "Reader Serial No";
            // 
            // lblEDCSN
            // 
            this.lblEDCSN.AutoSize = true;
            this.lblEDCSN.Location = new System.Drawing.Point(257, 74);
            this.lblEDCSN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEDCSN.Name = "lblEDCSN";
            this.lblEDCSN.Size = new System.Drawing.Size(98, 17);
            this.lblEDCSN.TabIndex = 34;
            this.lblEDCSN.Text = "EDC Serial No";
            // 
            // lblKernelVersion
            // 
            this.lblKernelVersion.AutoSize = true;
            this.lblKernelVersion.Location = new System.Drawing.Point(256, 42);
            this.lblKernelVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKernelVersion.Name = "lblKernelVersion";
            this.lblKernelVersion.Size = new System.Drawing.Size(101, 17);
            this.lblKernelVersion.TabIndex = 32;
            this.lblKernelVersion.Text = "Kernel Version";
            // 
            // lblOSVer
            // 
            this.lblOSVer.AutoSize = true;
            this.lblOSVer.Location = new System.Drawing.Point(276, 10);
            this.lblOSVer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOSVer.Name = "lblOSVer";
            this.lblOSVer.Size = new System.Drawing.Size(80, 17);
            this.lblOSVer.TabIndex = 30;
            this.lblOSVer.Text = "OS Version";
            this.lblOSVer.Click += new System.EventHandler(this.lblOSVer_Click);
            // 
            // lblSoftVer
            // 
            this.lblSoftVer.AutoSize = true;
            this.lblSoftVer.Location = new System.Drawing.Point(1, 106);
            this.lblSoftVer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSoftVer.Name = "lblSoftVer";
            this.lblSoftVer.Size = new System.Drawing.Size(115, 17);
            this.lblSoftVer.TabIndex = 28;
            this.lblSoftVer.Text = "Software Version";
            // 
            // lblPABX
            // 
            this.lblPABX.AutoSize = true;
            this.lblPABX.Location = new System.Drawing.Point(72, 74);
            this.lblPABX.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPABX.Name = "lblPABX";
            this.lblPABX.Size = new System.Drawing.Size(44, 17);
            this.lblPABX.TabIndex = 26;
            this.lblPABX.Text = "PABX";
            // 
            // lblLastInitTime
            // 
            this.lblLastInitTime.AutoSize = true;
            this.lblLastInitTime.Location = new System.Drawing.Point(25, 42);
            this.lblLastInitTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLastInitTime.Name = "lblLastInitTime";
            this.lblLastInitTime.Size = new System.Drawing.Size(92, 17);
            this.lblLastInitTime.TabIndex = 24;
            this.lblLastInitTime.Text = "Last Init Time";
            // 
            // lblTID
            // 
            this.lblTID.AutoSize = true;
            this.lblTID.Location = new System.Drawing.Point(39, 10);
            this.lblTID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTID.Name = "lblTID";
            this.lblTID.Size = new System.Drawing.Size(80, 17);
            this.lblTID.TabIndex = 22;
            this.lblTID.Text = "Terminal ID";
            // 
            // lblICCID
            // 
            this.lblICCID.AutoSize = true;
            this.lblICCID.Location = new System.Drawing.Point(579, 106);
            this.lblICCID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblICCID.Name = "lblICCID";
            this.lblICCID.Size = new System.Drawing.Size(42, 17);
            this.lblICCID.TabIndex = 44;
            this.lblICCID.Text = "ICCID";
            // 
            // tabPageInit
            // 
            this.tabPageInit.Controls.Add(this.txtInitSoftwareVer);
            this.tabPageInit.Controls.Add(this.lblSoftwareVersion);
            this.tabPageInit.Controls.Add(this.txtTIDInit);
            this.tabPageInit.Controls.Add(this.lblTIDInit);
            this.tabPageInit.Controls.Add(this.txtEdcSNInit);
            this.tabPageInit.Controls.Add(this.lblEDCSNnInit);
            this.tabPageInit.Location = new System.Drawing.Point(4, 25);
            this.tabPageInit.Margin = new System.Windows.Forms.Padding(4);
            this.tabPageInit.Name = "tabPageInit";
            this.tabPageInit.Size = new System.Drawing.Size(1028, 136);
            this.tabPageInit.TabIndex = 1;
            this.tabPageInit.Text = "Initialize Trail";
            this.tabPageInit.UseVisualStyleBackColor = true;
            // 
            // txtInitSoftwareVer
            // 
            this.txtInitSoftwareVer.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInitSoftwareVer.Location = new System.Drawing.Point(151, 76);
            this.txtInitSoftwareVer.Margin = new System.Windows.Forms.Padding(4);
            this.txtInitSoftwareVer.Name = "txtInitSoftwareVer";
            this.txtInitSoftwareVer.ReadOnly = true;
            this.txtInitSoftwareVer.Size = new System.Drawing.Size(173, 22);
            this.txtInitSoftwareVer.TabIndex = 51;
            // 
            // lblSoftwareVersion
            // 
            this.lblSoftwareVersion.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblSoftwareVersion.AutoSize = true;
            this.lblSoftwareVersion.Location = new System.Drawing.Point(20, 79);
            this.lblSoftwareVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSoftwareVersion.Name = "lblSoftwareVersion";
            this.lblSoftwareVersion.Size = new System.Drawing.Size(111, 17);
            this.lblSoftwareVersion.TabIndex = 50;
            this.lblSoftwareVersion.Text = "SoftwareVersion";
            // 
            // txtTIDInit
            // 
            this.txtTIDInit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtTIDInit.Location = new System.Drawing.Point(151, 16);
            this.txtTIDInit.Margin = new System.Windows.Forms.Padding(4);
            this.txtTIDInit.Name = "txtTIDInit";
            this.txtTIDInit.ReadOnly = true;
            this.txtTIDInit.Size = new System.Drawing.Size(173, 22);
            this.txtTIDInit.TabIndex = 49;
            // 
            // lblTIDInit
            // 
            this.lblTIDInit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTIDInit.AutoSize = true;
            this.lblTIDInit.Location = new System.Drawing.Point(57, 18);
            this.lblTIDInit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTIDInit.Name = "lblTIDInit";
            this.lblTIDInit.Size = new System.Drawing.Size(76, 17);
            this.lblTIDInit.TabIndex = 48;
            this.lblTIDInit.Text = "TerminalID";
            // 
            // txtEdcSNInit
            // 
            this.txtEdcSNInit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtEdcSNInit.Location = new System.Drawing.Point(151, 47);
            this.txtEdcSNInit.Margin = new System.Windows.Forms.Padding(4);
            this.txtEdcSNInit.Name = "txtEdcSNInit";
            this.txtEdcSNInit.ReadOnly = true;
            this.txtEdcSNInit.Size = new System.Drawing.Size(173, 22);
            this.txtEdcSNInit.TabIndex = 47;
            // 
            // lblEDCSNnInit
            // 
            this.lblEDCSNnInit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblEDCSNnInit.AutoSize = true;
            this.lblEDCSNnInit.Location = new System.Drawing.Point(35, 49);
            this.lblEDCSNnInit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEDCSNnInit.Name = "lblEDCSNnInit";
            this.lblEDCSNnInit.Size = new System.Drawing.Size(98, 17);
            this.lblEDCSNnInit.TabIndex = 46;
            this.lblEDCSNnInit.Text = "EDC Serial No";
            // 
            // lvAppEDC
            // 
            this.lvAppEDC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAppEDC.Location = new System.Drawing.Point(87, 9);
            this.lvAppEDC.Name = "lvAppEDC";
            this.lvAppEDC.Size = new System.Drawing.Size(572, 97);
            this.lvAppEDC.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvAppEDC.TabIndex = 1;
            this.lvAppEDC.UseCompatibleStateImageBehavior = false;
            this.lvAppEDC.View = System.Windows.Forms.View.List;
            // 
            // btnRemovePackage
            // 
            this.btnRemovePackage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemovePackage.Location = new System.Drawing.Point(665, 39);
            this.btnRemovePackage.Name = "btnRemovePackage";
            this.btnRemovePackage.Size = new System.Drawing.Size(147, 23);
            this.btnRemovePackage.TabIndex = 3;
            this.btnRemovePackage.Text = "Remove";
            this.btnRemovePackage.UseVisualStyleBackColor = true;
            this.btnRemovePackage.Click += new System.EventHandler(this.btnRemovePackage_Click);
            // 
            // btnAddPackage
            // 
            this.btnAddPackage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddPackage.Location = new System.Drawing.Point(665, 9);
            this.btnAddPackage.Name = "btnAddPackage";
            this.btnAddPackage.Size = new System.Drawing.Size(147, 23);
            this.btnAddPackage.TabIndex = 2;
            this.btnAddPackage.Text = "Add";
            this.btnAddPackage.UseVisualStyleBackColor = true;
            this.btnAddPackage.Click += new System.EventHandler(this.btnAddPackage_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            // 
            // menuExpand
            // 
            this.menuExpand.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuExpand.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.managementToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.createTerminalToolStripMenuItem,
            this.viewLastTerminalToolStripMenuItem,
            this.viewAllToolStripMenuItem,
            this.toolStripSearchTID,
            this.toolStripSearchSN});
            this.menuExpand.Location = new System.Drawing.Point(0, 0);
            this.menuExpand.Name = "menuExpand";
            this.menuExpand.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuExpand.Size = new System.Drawing.Size(1073, 29);
            this.menuExpand.TabIndex = 4;
            this.menuExpand.Text = "menuStrip1";
            // 
            // managementToolStripMenuItem
            // 
            this.managementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuManagementCard,
            this.mnuManagementAID,
            this.mnuManagementPK,
            this.mnuManagementTLE,
            this.mnuManagementEmv,
            this.mnuManagementRmtDownload,
            this.mnuManagementGprsEth,
            this.mnuManagementPinPad,
            this.mnuManagementPromo,
            this.mnuManagementInitialFLAZZ,
            this.mnuManagementCardType,
            this.mnuManagementEdcMonitor,
            this.mnuRequestPaperReceiptToolStripMenuItem,
            this.mnuManagementSN});
            this.managementToolStripMenuItem.Name = "managementToolStripMenuItem";
            this.managementToolStripMenuItem.Size = new System.Drawing.Size(109, 27);
            this.managementToolStripMenuItem.Text = "Management";
            // 
            // mnuManagementCard
            // 
            this.mnuManagementCard.Name = "mnuManagementCard";
            this.mnuManagementCard.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementCard.Text = "Card";
            this.mnuManagementCard.Click += new System.EventHandler(this.cardToolStripMenuItem_Click);
            // 
            // mnuManagementAID
            // 
            this.mnuManagementAID.Name = "mnuManagementAID";
            this.mnuManagementAID.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementAID.Text = "AID";
            this.mnuManagementAID.Click += new System.EventHandler(this.mnuManagementAID_Click);
            // 
            // mnuManagementPK
            // 
            this.mnuManagementPK.Name = "mnuManagementPK";
            this.mnuManagementPK.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementPK.Text = "Public Key";
            this.mnuManagementPK.Click += new System.EventHandler(this.mnuManagementPK_Click);
            // 
            // mnuManagementTLE
            // 
            this.mnuManagementTLE.Name = "mnuManagementTLE";
            this.mnuManagementTLE.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementTLE.Text = "TLE";
            this.mnuManagementTLE.Click += new System.EventHandler(this.mnuManagementTLE_Click);
            // 
            // mnuManagementEmv
            // 
            this.mnuManagementEmv.Name = "mnuManagementEmv";
            this.mnuManagementEmv.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementEmv.Text = "EMV";
            this.mnuManagementEmv.Click += new System.EventHandler(this.mnuManagementEmv_Click);
            // 
            // mnuManagementRmtDownload
            // 
            this.mnuManagementRmtDownload.Name = "mnuManagementRmtDownload";
            this.mnuManagementRmtDownload.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementRmtDownload.Text = "Remote Download";
            this.mnuManagementRmtDownload.Click += new System.EventHandler(this.mnuManagementRmtDownload_Click);
            // 
            // mnuManagementGprsEth
            // 
            this.mnuManagementGprsEth.Name = "mnuManagementGprsEth";
            this.mnuManagementGprsEth.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementGprsEth.Text = "GPRS/Ethernet";
            this.mnuManagementGprsEth.Click += new System.EventHandler(this.mnuManagementGprsEth_Click);
            // 
            // mnuManagementPinPad
            // 
            this.mnuManagementPinPad.Name = "mnuManagementPinPad";
            this.mnuManagementPinPad.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementPinPad.Text = "PIN Pad";
            this.mnuManagementPinPad.Click += new System.EventHandler(this.mnuManagementPinPad_Click);
            // 
            // mnuManagementPromo
            // 
            this.mnuManagementPromo.Name = "mnuManagementPromo";
            this.mnuManagementPromo.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementPromo.Text = "Promo";
            this.mnuManagementPromo.Click += new System.EventHandler(this.mnuManagementPromo_Click);
            // 
            // mnuManagementInitialFLAZZ
            // 
            this.mnuManagementInitialFLAZZ.Name = "mnuManagementInitialFLAZZ";
            this.mnuManagementInitialFLAZZ.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementInitialFLAZZ.Text = "Initial FLAZZ";
            this.mnuManagementInitialFLAZZ.Click += new System.EventHandler(this.mnuManagementInitialFLAZZ_Click);
            // 
            // mnuManagementCardType
            // 
            this.mnuManagementCardType.Name = "mnuManagementCardType";
            this.mnuManagementCardType.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementCardType.Text = "Card Type";
            this.mnuManagementCardType.Click += new System.EventHandler(this.mnuManagementCardType_Click);
            // 
            // mnuManagementEdcMonitor
            // 
            this.mnuManagementEdcMonitor.Name = "mnuManagementEdcMonitor";
            this.mnuManagementEdcMonitor.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementEdcMonitor.Text = "Edc Monitoring";
            this.mnuManagementEdcMonitor.Click += new System.EventHandler(this.edcMonitoringToolStripMenuItem_Click);
            // 
            // mnuRequestPaperReceiptToolStripMenuItem
            // 
            this.mnuRequestPaperReceiptToolStripMenuItem.Name = "mnuRequestPaperReceiptToolStripMenuItem";
            this.mnuRequestPaperReceiptToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.mnuRequestPaperReceiptToolStripMenuItem.Text = "Request Paper Receipt";
            this.mnuRequestPaperReceiptToolStripMenuItem.Click += new System.EventHandler(this.mnuRequestPaperReceiptToolStripMenuItem_Click);
            // 
            // mnuManagementSN
            // 
            this.mnuManagementSN.Name = "mnuManagementSN";
            this.mnuManagementSN.Size = new System.Drawing.Size(232, 26);
            this.mnuManagementSN.Text = "RDSN Management";
            this.mnuManagementSN.Click += new System.EventHandler(this.mnuManagementSN_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registerTerminalToolStripMenuItem,
            this.advanceSearchToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(56, 27);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // registerTerminalToolStripMenuItem
            // 
            this.registerTerminalToolStripMenuItem.Name = "registerTerminalToolStripMenuItem";
            this.registerTerminalToolStripMenuItem.Size = new System.Drawing.Size(199, 26);
            this.registerTerminalToolStripMenuItem.Text = "Register Terminal";
            this.registerTerminalToolStripMenuItem.Click += new System.EventHandler(this.registerTerminalToolStripMenuItem_Click);
            // 
            // advanceSearchToolStripMenuItem
            // 
            this.advanceSearchToolStripMenuItem.Name = "advanceSearchToolStripMenuItem";
            this.advanceSearchToolStripMenuItem.Size = new System.Drawing.Size(199, 26);
            this.advanceSearchToolStripMenuItem.Text = "Advance Search";
            this.advanceSearchToolStripMenuItem.Click += new System.EventHandler(this.advanceSearchToolStripMenuItem_Click);
            // 
            // createTerminalToolStripMenuItem
            // 
            this.createTerminalToolStripMenuItem.Name = "createTerminalToolStripMenuItem";
            this.createTerminalToolStripMenuItem.Size = new System.Drawing.Size(125, 27);
            this.createTerminalToolStripMenuItem.Text = "Create Terminal";
            this.createTerminalToolStripMenuItem.Click += new System.EventHandler(this.createTerminalToolStripMenuItem_Click);
            // 
            // viewLastTerminalToolStripMenuItem
            // 
            this.viewLastTerminalToolStripMenuItem.Name = "viewLastTerminalToolStripMenuItem";
            this.viewLastTerminalToolStripMenuItem.Size = new System.Drawing.Size(144, 27);
            this.viewLastTerminalToolStripMenuItem.Text = "View Last Terminal";
            this.viewLastTerminalToolStripMenuItem.Click += new System.EventHandler(this.viewLastTerminalToolStripMenuItem_Click);
            // 
            // viewAllToolStripMenuItem
            // 
            this.viewAllToolStripMenuItem.Name = "viewAllToolStripMenuItem";
            this.viewAllToolStripMenuItem.Size = new System.Drawing.Size(75, 27);
            this.viewAllToolStripMenuItem.Text = "View All";
            this.viewAllToolStripMenuItem.Click += new System.EventHandler(this.viewAllToolStripMenuItem_Click);
            // 
            // toolStripSearchTID
            // 
            this.toolStripSearchTID.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.toolStripSearchTID.MaxLength = 8;
            this.toolStripSearchTID.Name = "toolStripSearchTID";
            this.toolStripSearchTID.Size = new System.Drawing.Size(179, 27);
            this.toolStripSearchTID.Text = "SEARCH TERMINALID";
            this.toolStripSearchTID.Enter += new System.EventHandler(this.toolStripSearchTID_Enter);
            this.toolStripSearchTID.Leave += new System.EventHandler(this.toolStripSearchTID_Leave);
            this.toolStripSearchTID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripSearchTID_KeyPress);
            // 
            // toolStripSearchSN
            // 
            this.toolStripSearchSN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.toolStripSearchSN.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.toolStripSearchSN.Name = "toolStripSearchSN";
            this.toolStripSearchSN.Size = new System.Drawing.Size(200, 27);
            this.toolStripSearchSN.Text = "SEARCH SERIAL NUMBER";
            this.toolStripSearchSN.Enter += new System.EventHandler(this.toolStripSearchSN_Enter);
            this.toolStripSearchSN.Leave += new System.EventHandler(this.toolStripSearchSN_Leave);
            this.toolStripSearchSN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripSearchSN_KeyPress);
            // 
            // FrmExpand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(1073, 497);
            this.Controls.Add(this.menuExpand);
            this.Controls.Add(this.gbEdcInf);
            this.Controls.Add(this.gbView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuExpand;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(1241, 766);
            this.Name = "FrmExpand";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Terminal";
            this.Activated += new System.EventHandler(this.FrmExpand_Activated);
            this.Load += new System.EventHandler(this.FrmExpand_Load);
            this.gbView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTerminal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTagDetail)).EndInit();
            this.gbEdcInf.ResumeLayout(false);
            this.tabcntrInformation.ResumeLayout(false);
            this.tabPageAutoInit.ResumeLayout(false);
            this.tabPageAutoInit.PerformLayout();
            this.tabPageInit.ResumeLayout(false);
            this.tabPageInit.PerformLayout();
            this.menuExpand.ResumeLayout(false);
            this.menuExpand.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ImageList ButtonIcon;
        internal System.Windows.Forms.ImageList IconList;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.GroupBox gbView;
        internal System.Windows.Forms.Label lblTagDetail;
        internal System.Windows.Forms.Label lblTerminal;
        internal System.Windows.Forms.Label lblDetail;
        internal System.Windows.Forms.TreeView tvTerminal;
        internal System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.GroupBox gbEdcInf;
        private System.Windows.Forms.TabControl tabcntrInformation;
        private System.Windows.Forms.TabPage tabPageAutoInit;
        private System.Windows.Forms.TextBox txtMemUsage;
        private System.Windows.Forms.TextBox txtMCSN;
        private System.Windows.Forms.TextBox txtReaderSN;
        private System.Windows.Forms.TextBox txtPSAMSN;
        private System.Windows.Forms.TextBox txtEDCSN;
        private System.Windows.Forms.TextBox txtKernelVer;
        private System.Windows.Forms.TextBox txtPABX;
        private System.Windows.Forms.TextBox txtLastInit;
        private System.Windows.Forms.TextBox txtOSVer;
        private System.Windows.Forms.TextBox txtSoftwareVer;
        private System.Windows.Forms.TextBox txtTID;
        private System.Windows.Forms.Label lblMemUsage;
        private System.Windows.Forms.Label lblMCSN;
        private System.Windows.Forms.Label lblPSAMSN;
        private System.Windows.Forms.Label lblReaderSN;
        private System.Windows.Forms.Label lblEDCSN;
        private System.Windows.Forms.Label lblKernelVersion;
        private System.Windows.Forms.Label lblOSVer;
        private System.Windows.Forms.Label lblSoftVer;
        private System.Windows.Forms.Label lblPABX;
        private System.Windows.Forms.Label lblLastInitTime;
        private System.Windows.Forms.Label lblTID;
        private System.Windows.Forms.ListView lvAppEDC;
        private System.Windows.Forms.Button btnRemovePackage;
        private System.Windows.Forms.Button btnAddPackage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtICCID;
        private System.Windows.Forms.Label lblICCID;
        private System.Windows.Forms.TabPage tabPageInit;
        private System.Windows.Forms.TextBox txtEdcSNInit;
        private System.Windows.Forms.Label lblEDCSNnInit;
        private System.Windows.Forms.TextBox txtTIDInit;
        private System.Windows.Forms.Label lblTIDInit;
        private System.Windows.Forms.TextBox txtInitSoftwareVer;
        private System.Windows.Forms.Label lblSoftwareVersion;
        public System.Windows.Forms.DataGridView dgvTerminal;
        public System.Windows.Forms.DataGridView dgvTagDetail;
        private System.Windows.Forms.MenuStrip menuExpand;
        private System.Windows.Forms.ToolStripMenuItem managementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementCard;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementAID;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementPK;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementTLE;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementGprsEth;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementPinPad;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementRmtDownload;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementEmv;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementEdcMonitor;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registerTerminalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advanceSearchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementPromo;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementInitialFLAZZ;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementCardType;
        private System.Windows.Forms.ToolStripMenuItem createTerminalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewLastTerminalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripSearchTID;
        private System.Windows.Forms.ToolStripMenuItem mnuRequestPaperReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripSearchSN;
        private System.Windows.Forms.ToolStripMenuItem mnuManagementSN;
    }
}