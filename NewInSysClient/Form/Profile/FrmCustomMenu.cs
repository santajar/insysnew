﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;
using InSysClass;
using System.Collections.Generic;


namespace InSys
{
    public partial class FrmCustomMenu : Form
    {
        #region "Variable"
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Determines if data is saved or not.
        /// </summary>
        protected bool isSaving;

        /// <summary>
        /// Determines if form is in Editing mode or Adding mode.
        /// </summary>
        protected bool isEdit;

        /// <summary>
        /// Current TerminalID
        /// </summary>
        public string sTerminalID;

        /// <summary>
        /// Current Database's ID
        /// </summary>
        public string sDbID = "";

        /// <summary>
        /// Current Version Name
        /// </summary>
        public string sDbName = "";

        protected DataTable dtSchemaCustomMenu;
        protected DataTable dtDataCustomMenu;
        protected string sResult = "";
        protected string sLog = "";
        protected string sLogDetail = "";
        #endregion

        public FrmCustomMenu(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, bool _isEdit)
        {
            isSaving = false;
            isEdit = _isEdit;
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        private void FrmCustomMenu_Load(object sender, EventArgs e)
        {
            this.Text = string.Format("Form Custom Menu - {0}", sTerminalID);
            InitForm();
        }

        private void tvCustomMenu_AfterCheck(object sender, TreeViewEventArgs e)
        {
            foreach (TreeNode childNode in e.Node.Nodes)
            {
                 childNode.Checked = e.Node.Checked;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            sGenerateResult();
            if (IsSuccessSaveCustomMenu())
            {
                if (sLogDetail.Length > 8000)
                {
                    CommonClass.InputLog(oSqlConnAuditTrail, sTerminalID, UserData.sUserID, sDbName, sLog, sLogDetail.Substring(0,8000));
                    CommonClass.InputLog(oSqlConnAuditTrail, sTerminalID, UserData.sUserID, sDbName, sLog, sLogDetail.Substring(8000,sLogDetail.Length - 8000));
                }else
                    CommonClass.InputLog(oSqlConnAuditTrail, sTerminalID,UserData.sUserID, sDbName, sLog, sLogDetail);
                sResult = "";
                sLog = "";
                sLogDetail = "";
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult drResult = MessageBox.Show("Do you want to save?", "", MessageBoxButtons.YesNo);
            if (drResult == DialogResult.Yes)
            {
                btnSave_Click(sender, e);
            }
            else if (drResult == DialogResult.No)
            {
                CommonClass.InputLog(oSqlConnAuditTrail, sTerminalID,
                                            UserData.sUserID, sDbName, string.Format("View {0} Custom Menu",sTerminalID), "");
            }
        }
        #region "Function"
        private bool IsSuccessSaveCustomMenu()
        {
            string sErrMsg = "";
            try
            {
                SqlCommand oCmd = new SqlCommand(isEdit ? CommonSP.sSPProfileCustomMenuUpdate : CommonSP.sSPProfileCustomMenuInsert, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sResult;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        sErrMsg = oRead[0].ToString();
                    oRead.Close();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            if (sErrMsg != "")
                MessageBox.Show(sErrMsg);
           
            return true;

        }

        private void InitForm()
        {
            dtSchemaCustomMenu = GetCustomMenuSchema();
            if (isEdit)
                dtDataCustomMenu = GetCustomMenuData();
            else
                dtDataCustomMenu = GetDefaultValue();
            GenerateTree();

        }

        private DataTable GetDefaultValue()
        {
            DataTable dtTemp = new DataTable();

            SqlCommand oCmd = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = string.Format(" WHERE DatabaseID = '{0}' AND FormID= 20", sDbID);
            new SqlDataAdapter(oCmd).Fill(dtTemp);

            return dtTemp;
        }

        private void sGenerateResult()
        {
            sResult = "";
            sLog = "";
            sLogDetail = "";

            if (isEdit)
                sLog = string.Format("Edit {0} Custom menu", sTerminalID);
            else
                sLog = string.Format("Add {0} Custom menu", sTerminalID);
            try
            {
                foreach (DataRow drRow in dtSchemaCustomMenu.Rows)
                {
                    string sItemName = drRow["ItemName"].ToString();
                    string sTag = drRow["Tag"].ToString();

                    TreeNodeCollection tncNodes = tvCustomMenu.Nodes;
                    foreach (TreeNode tn in tncNodes)
                    {
                        if (tn.Text == sItemName)
                        {
                            if (tn.Checked == true)
                            {
                                sResult = sResult + sTag + "011";
                                if (isEdit)
                                {
                                    string sDataItem = sGetItemValue(sTag);
                                    if (sDataItem != "1")
                                        sLogDetail = sLogDetail + sItemName + " False -> True; ";
                                }
                                else
                                    sLogDetail = sLogDetail + sItemName + " -> True; ";
                                break;
                            }
                            else
                            {
                                sResult = sResult + sTag + "010";
                                if (isEdit)
                                {
                                    string sDataItem = sGetItemValue(sTag);
                                    if (sDataItem != "0")
                                        sLogDetail = sLogDetail + sItemName + " True -> False; ";
                                }
                                else
                                    sLogDetail = sLogDetail + sItemName + " -> False; ";
                                break;
                            }
                        }
                        GenerateResultChildNode(tn, sItemName, sTag);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Generate Result Failed.");
            }

        }

        private string sGetItemValue(string _sTag)
        {
            string sTempTagValue = "";
            sTempTagValue = (from DataRow dr in dtDataCustomMenu.Rows
                             where (string)dr["Tag"] == _sTag
                             select (string)dr["Value"]).FirstOrDefault();
            return sTempTagValue;
        }   

        private void GenerateResultChildNode(TreeNode tn, string sItemName, string sTag)
        {
            foreach (TreeNode tnChild in tn.Nodes)
            {
                if (tnChild.Text == sItemName)
                {
                    if (tnChild.Checked == true)
                    {
                        sResult = sResult + sTag + "011";
                        if (isEdit)
                        {
                            string sDataItem = sGetItemValue(sTag);
                            if (sDataItem != "1")
                                sLogDetail = sLogDetail + sItemName + ": False -> True;";
                        }
                        else
                            sLogDetail = sLogDetail + sItemName + " -> True; ";
                        break;
                    }
                    else
                    {
                        sResult = sResult + sTag + "010";
                        if (isEdit)
                        {
                            string sDataItem = sGetItemValue(sTag);
                            if (sDataItem != "0")
                                sLogDetail = sLogDetail + sItemName + ": True -> False; ";
                        }
                        else
                            sLogDetail = sLogDetail + sItemName + " -> False; ";
                        break;
                    }
                }
                GenerateResultChildNode(tnChild, sItemName, sTag);
            }
        }

        private void GenerateTree()
        {
            tvCustomMenu.Nodes.Clear();
            try
            {
                DataRow[] parentRows = dtSchemaCustomMenu.Select("ParentTag = ''"); // # Get all root nodes

                foreach (DataRow row in parentRows) // # Add all root nodes to treeview and call for child nodes
                {
                    TreeNode node = new TreeNode();
                    node.Text = row["ItemName"].ToString();
                    Boolean bchecked = false;
                    try
                    {
                        DataRow[] drData = dtDataCustomMenu.Select(string.Format("Tag = '{0}'", (string)row["Tag"]));
                        string sValue = isEdit ? drData[0]["value"].ToString() : drData[0]["DefaultValue"].ToString();
                        if (sValue == "1")
                            bchecked = true;
                    }
                    catch (Exception ex)
                    {
                        bchecked = false;
                    }
                    
                    node.Checked = bchecked;
                    tvCustomMenu.Nodes.Add(node);
                    AddChildNodes1((string)row["Tag"], node);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddChildNodes1(string ParentTag, TreeNode node) // # Recursive method to add child nodes and call for child nodes of each child node
        {
            try
            {
                DataRow[] childRows = dtSchemaCustomMenu.Select("ParentTag = '" + ParentTag.ToString() + "'"); // # Get all child nodes of given node

                if (childRows.Length == 0) { return; } // # Recursion base case; if given node has no child nodes no more action is taken

                foreach (DataRow row in childRows)
                {
                    TreeNode childNode = new TreeNode();
                    childNode.Text = row["ItemName"].ToString();
                    Boolean bchecked = false;
                    try
                    {
                        DataRow[] drData = dtDataCustomMenu.Select(string.Format("Tag = '{0}'", (string)row["Tag"]));
                        string sValue = isEdit ? drData[0]["value"].ToString() : drData[0]["DefaultValue"].ToString();
                        if (sValue == "1")
                            bchecked = true;
                    }
                    catch (Exception ex)
                    {
                        bchecked = false;
                    }

                    childNode.Checked = bchecked;
                    node.Nodes.Add(childNode);
                    AddChildNodes1((string)row["Tag"], childNode);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private DataTable GetCustomMenuSchema()
        {
            DataTable dtTemp = new DataTable();
            try
            {
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPCustomMenuSchemaBrowse, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE DatabaseID ='{0}' Order By ID ASC", sDbID);
                new SqlDataAdapter(oCmd).Fill(dtTemp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dtTemp;
        }

        private DataTable GetCustomMenuData()
        {
            DataTable dtTemp = new DataTable();
            try
            {
                SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileCustomMenuBrowse, oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE TerminalID ='{0}'", sTerminalID);
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                new SqlDataAdapter(oCmd).Fill(dtTemp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dtTemp;
        }

        #endregion

        

        

        

    }
}
