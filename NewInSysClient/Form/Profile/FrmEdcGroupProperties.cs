﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using InSysClass;

namespace InSys
{
    public partial class FrmEdcGroupProperties : Form
    {
        SqlConnection oSqlConn;
        SqlConnection oSqlConnAuditTrail;

        GroupRegionSnType frmIdType;
        DataRow rowData;
        string sTitle;
        string sTypeScheduleDownload;
        string sCheckScheduleDownload;
        string sTimeScheduleDownload;
        int iEnableDownload;
        ActionType actionForm;
        int iGroupId;
        int iRegionId;
        string sScheduleID;
        int iScheduleID;
        int iAppPackageID;
        

        DataTable dtTerminalBrand = new DataTable();
        DataTable dtTerminalType = new DataTable();

        public FrmEdcGroupProperties(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, int _frmIdType,
            DataRow _rowData, string _sTitle, int _iActionType, int _iGroupId, int _iRegionId)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            frmIdType = (GroupRegionSnType)_frmIdType;
            rowData = _rowData;
            sTitle = _sTitle;
            actionForm = (ActionType)_iActionType;
            iGroupId = _iGroupId;
            iRegionId = _iRegionId;
        }

        private void FrmEdcGroupProperties_Load(object sender, EventArgs e)
        {
            this.Text = sTitle;
            Init();
        }

        private void chkEnableSchedule_CheckedChanged(object sender, EventArgs e)
        {
            panelSchedule.Enabled = chkEnableSchedule.Checked;
        }

        private void dateTimePickerClockDaily_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePickerClockDaily.Checked == true)
            {
                chkEnableMonthly.Checked = chkEnableWeekly.Checked = false;
                sTypeScheduleDownload = "DAILY";
                sCheckScheduleDownload = "EVERY DAYS";
                sTimeScheduleDownload = dateTimePickerClockDaily.Value.ToString("HH:mm");
            }
        }

        private void chkWeekly_CheckedChanged(object sender, EventArgs e)
        {
            panelScheduleWeekly.Enabled = chkEnableWeekly.Checked;
            if (chkEnableWeekly.Checked == true)
            {
                chkEnableMonthly.Checked = dateTimePickerClockDaily.Checked = false; 
                sTypeScheduleDownload = "WEEKLY";
                sTimeScheduleDownload = dateTimePickerClockWeekly.Value.ToString("HH:mm");
            }
        }

        private void chkEnableMonthly_CheckedChanged(object sender, EventArgs e)
        {
            panelScheduleMonthly.Enabled = chkEnableMonthly.Checked;
            if (chkEnableMonthly.Checked == true)
            {
                chkEnableWeekly.Checked = dateTimePickerClockDaily.Checked = false;
                sTypeScheduleDownload = "MONTHLY";
                sCheckScheduleDownload = (cmbMonthlyDate.SelectedIndex + 1).ToString();
                sTimeScheduleDownload = dateTimePickerClockMonthly.Value.ToString("HH:mm");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool isExit = false;
            if (actionForm == ActionType.Create || actionForm == ActionType.Update)
                if (frmIdType == GroupRegionSnType.Terminal)
                {
                    string sError = null;
                    //if (actionForm == ActionType.Create)
                    //{
                    //    //insert new
                    //    if (IsValidTerminalSN(ref sError))
                    //    {
                    //        SaveTerminalSN();
                    //        isExit = true;
                    //    }
                    //    else MessageBox.Show("Serial Number Already Exist.");
                    //}
                    //else
                    //{
                    //    //update
                    //}

                    //if (actionForm == ActionType.Create)
                    if (IsValidTerminalSN(ref sError))
                    {
                        if (!IsTerminalSNExist())
                        {
                            SaveTerminalSN();
                            isExit = true;
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(ScheduleIDFromDB()))
                                iScheduleID = -1;
                            else
                                iScheduleID = Convert.ToInt32(ScheduleIDFromDB());
                            if (chkEnableSchedule.Checked == true)
                            {
                                if (!string.IsNullOrEmpty(sTypeScheduleDownload) && !string.IsNullOrEmpty(sCheckScheduleDownload))
                                {
                                    if ((dateTimePickerClockDaily.Checked == true) || (chkSunday.Checked == true || chkMonday.Checked == true || chkTuesday.Checked == true || chkWednesday.Checked == true || chkThursday.Checked == true || chkFriday.Checked == true || chkSaturday.Checked == true) || (cmbMonthlyDate.SelectedIndex > -1))
                                    {
                                        UpdateTerminalSN(txtSerialNumber.Text, iScheduleID,
                                            sTypeScheduleDownload, sCheckScheduleDownload, DateTime.Parse(sTimeScheduleDownload),
                                            chkEnableDownloadSN.Checked == true ? 1 : 0);
                                        isExit = true;
                                        MessageBox.Show("Update");
                                    }
                                    else MessageBox.Show("Please Fill Schedule Correctly");
                                }
                                else
                                {
                                    if ((dateTimePickerClockDaily.Checked == true) || (chkSunday.Checked == true || chkMonday.Checked == true || chkTuesday.Checked == true || chkWednesday.Checked == true || chkThursday.Checked == true || chkFriday.Checked == true || chkSaturday.Checked == true) || (cmbMonthlyDate.SelectedIndex > -1))
                                    {
                                        UpdateTerminalSN(txtSerialNumber.Text, iScheduleID,
                                            null, null, (new DateTime()),
                                            chkEnableDownloadSN.Checked == true ? 1 : 0);
                                        isExit = true;
                                        MessageBox.Show("Update");
                                    } else MessageBox.Show("Please Fill Schedule Correctly");

                                }
                            }
                            else
                            {
                                
                                UpdateTerminalSN(txtSerialNumber.Text, iScheduleID,
                                        null, null, new DateTime(),
                                        chkEnableDownloadSN.Checked == true ? 1 : 0);
                                isExit = true;
                                MessageBox.Show("Update");
                            }
                        }
                    }
                    else
                        MessageBox.Show(sError);
                }
                else
                {
                    if (frmIdType == GroupRegionSnType.Group)
                    {
                        UpdateGroup();
                    }
                    else if (frmIdType == GroupRegionSnType.Region)
                    {
                        UpdateRegion();
                    }
                    SaveGroupRegion();
                    isExit = true;
                }
            if (isExit)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void UpdateRegion()
        {
            SqlCommand oSqlCmdRegion = new SqlCommand(CommonSP.sSPRegionUpdate, oSqlConn);
            oSqlCmdRegion.CommandType = CommandType.StoredProcedure;
            oSqlCmdRegion.Parameters.Add("@iParentIdOld", SqlDbType.Int).Value = Convert.ToInt16(rowData["ParentId"].ToString());
            oSqlCmdRegion.Parameters.Add("@sChildIdOld", SqlDbType.VarChar).Value = rowData["ChildId"].ToString();
            oSqlCmdRegion.Parameters.Add("@sNameOld", SqlDbType.VarChar).Value = rowData["Name"].ToString();
            oSqlCmdRegion.Parameters.Add("@iParentIdNew", SqlDbType.Int).Value = Convert.ToInt16(rowData["ParentId"].ToString());
            oSqlCmdRegion.Parameters.Add("@sChildIdNew", SqlDbType.VarChar).Value = rowData["ChildId"].ToString();
            oSqlCmdRegion.Parameters.Add("@sNameNew", SqlDbType.VarChar).Value = rowData["Name"].ToString();
            if (!string.IsNullOrEmpty(rtbGroupRegionDescription.Text))
                oSqlCmdRegion.Parameters.Add("@sDescription", SqlDbType.VarChar).Value = rtbGroupRegionDescription.Text;
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            oSqlCmdRegion.ExecuteNonQuery();
        }

        private void UpdateGroup()
        {
            SqlCommand oSqlCmdRegion = new SqlCommand(CommonSP.sSPGroupUpdate, oSqlConn);
            oSqlCmdRegion.CommandType = CommandType.StoredProcedure;
            oSqlCmdRegion.Parameters.Add("@iParentIdOld", SqlDbType.Int).Value = Convert.ToInt16(rowData["ParentId"].ToString());
            oSqlCmdRegion.Parameters.Add("@sChildIdOld", SqlDbType.VarChar).Value = rowData["ChildId"].ToString();
            oSqlCmdRegion.Parameters.Add("@sNameOld", SqlDbType.VarChar).Value = rowData["Name"].ToString();
            oSqlCmdRegion.Parameters.Add("@iParentIdNew", SqlDbType.Int).Value = Convert.ToInt16(rowData["ParentId"].ToString());
            oSqlCmdRegion.Parameters.Add("@sChildIdNew", SqlDbType.VarChar).Value = rowData["ChildId"].ToString();
            oSqlCmdRegion.Parameters.Add("@sNameNew", SqlDbType.VarChar).Value = rowData["Name"].ToString();
            if (!string.IsNullOrEmpty(rtbGroupRegionDescription.Text))
                oSqlCmdRegion.Parameters.Add("@sDescription", SqlDbType.VarChar).Value = rtbGroupRegionDescription.Text;
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            oSqlCmdRegion.ExecuteNonQuery();
        }

        private void UpdateTerminalSN(string sSerialNumber, int iScheduleID, string sScheduleType, string sScheduleCheck, DateTime dtScheduleTime, int iEnableDownload)
        {
            //sSPTerminalSNUpdate
            GetiAppPackID();
            SqlCommand oSqlCmdUpdateSchedule = new SqlCommand(CommonSP.sSPTerminalSNUpdate, oSqlConn);
            oSqlCmdUpdateSchedule.CommandType = CommandType.StoredProcedure;
            oSqlCmdUpdateSchedule.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = sSerialNumber;
            oSqlCmdUpdateSchedule.Parameters.Add("@iScheduleID", SqlDbType.Int).Value = iScheduleID;
            if (!string.IsNullOrEmpty(sScheduleType))
                oSqlCmdUpdateSchedule.Parameters.Add("@sScheduleType", SqlDbType.VarChar).Value = sScheduleType;
            if (!string.IsNullOrEmpty(sScheduleCheck))
                oSqlCmdUpdateSchedule.Parameters.Add("@sScheduleCheck", SqlDbType.VarChar).Value = sScheduleCheck;
            if (dtScheduleTime != new DateTime())
                oSqlCmdUpdateSchedule.Parameters.Add("@dtScheduleTime", SqlDbType.DateTime).Value = dtScheduleTime;
            oSqlCmdUpdateSchedule.Parameters.Add("@bEnableDownload", SqlDbType.Bit).Value = iEnableDownload;
            if (iAppPackageID > 0)
                oSqlCmdUpdateSchedule.Parameters.Add("@iAppPackageID", SqlDbType.Int).Value = iAppPackageID;
            if (!string.IsNullOrEmpty(rtbTerminalDescription.Text))
                oSqlCmdUpdateSchedule.Parameters.Add("@sDescription", SqlDbType.VarChar).Value = rtbTerminalDescription.Text;
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            oSqlCmdUpdateSchedule.ExecuteNonQuery();
        }

        private void chkDaysSchedule_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked == true)
            {
                InitScheduleWeekly(sender);
                sCheckScheduleDownload = ((CheckBox)sender).Text;
            }
        }

        private void cmbTerminalBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTerminalBrand.SelectedIndex >= 0 && !string.IsNullOrEmpty(cmbTerminalBrand.Text) && !string.IsNullOrEmpty(cmbTerminalBrand.SelectedValue.ToString()))
                InitTerminalType(cmbTerminalBrand.SelectedValue.ToString());
        }
        
        private void cmbMonthlyDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            sCheckScheduleDownload = (cmbMonthlyDate.SelectedIndex + 1).ToString();
        }

        private void dateTimePickerClockWeekly_ValueChanged(object sender, EventArgs e)
        {
            sTimeScheduleDownload = dateTimePickerClockWeekly.Value.ToString();
        }

        private void dateTimePickerClockMonthly_ValueChanged(object sender, EventArgs e)
        {
            sTimeScheduleDownload = dateTimePickerClockMonthly.Value.ToString();
        }
        
        #region "Function"
        private void Init()
        {            
            InitTabPage();
            LoadData();
        }

        private void InitTabPage()
        {
            if (frmIdType == GroupRegionSnType.Terminal)
            {
                tabPageGroupRegion.Dispose();
                InitTerminalBrandandSoftware();
            }
            else
                tabPageTerminal.Dispose();
            for (int iDate = 1; iDate <= 28; iDate++)
                cmbMonthlyDate.Items.Add(iDate.ToString());
        }

        private void InitTerminalBrandandSoftware()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalBrandBrowse, "", "Name", "Name", ref cmbTerminalBrand);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPAppPackageEdcListBrowse, null, "AppPackId", "AppPackageName", ref cmbSoftwareName);
            
        }

        private void InitTerminalType(string sTerminalBrand)
        {
            string sCondition = string.Format("WHERE TerminalBrand='{0}'",sTerminalBrand);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalTypeBrowse, sCondition, "Name", "Name", ref cmbTerminalType);
        }

        private void LoadData()
        {
            if (frmIdType == GroupRegionSnType.Terminal)
            {
                if (rowData != null)
                {
                    txtSerialNumber.Text = rowData["SerialNumber"].ToString();
                    txtTerminalName.Text = rowData["Name"].ToString();
                    chkEnableDownloadSN.Checked = Convert.ToBoolean(rowData["EnableDownload"].ToString());
                    rtbTerminalDescription.Text = rowData["Description"].ToString();
                    if (!string.IsNullOrEmpty(rowData["TerminalTypeName"].ToString()))
                        LoadDataTerminalBrandType(rowData["TerminalTypeName"].ToString());
                    string sScheduleID = rowData["ScheduleID"].ToString();
                    if (!string.IsNullOrEmpty(sScheduleID))
                    {
                        //chkEnableSchedule.Checked = true;
                        //chkEnableDownloadSN.Checked = true;
                        LoadTerminalSchedule(sScheduleID);
                        if (dateTimePickerClockDaily.Checked==true ||chkEnableWeekly.Checked==true||chkEnableMonthly.Checked==true)
                            chkEnableSchedule.Checked = true;
                    }
                    if (!string.IsNullOrEmpty(rowData["AppPackID"].ToString()))
                        LoadDataSoftwareName(Convert.ToInt32(rowData["AppPackID"].ToString()));
                    if (!string.IsNullOrEmpty(rowData["Description"].ToString()))
                        rtbTerminalDescription.Text = rowData["Description"].ToString();
                }
            }
            else
            {
                txtGroupRegionName.Text = rowData["Name"].ToString();
                rtbGroupRegionDescription.Text = rowData["Description"].ToString();
                txtGroupRegionName.ReadOnly = true;
                //string sScheduleID = rowData["ScheduleID"].ToString();
                if (!string.IsNullOrEmpty(sScheduleID))
                {
                    chkEnableSchedule.Checked = true;
                    chkEnableDownloadGroupRegion.Checked = true;
                    LoadTerminalSchedule(sScheduleID);
                }
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPAppPackageEdcListBrowse, null, "AppPackId", "AppPackageName", ref cmbSoftwareNameGroup);
            
            }
            if (!string.IsNullOrEmpty(txtSerialNumber.Text))
                txtSerialNumber.Enabled = false;
        }

        private void LoadDataSoftwareName(int _iAppPackID)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAppPackageBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE AppPackId='{0}'", _iAppPackID);
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                if (dtTemp.Rows.Count > 0)
                {
                    string sSoftwareName = dtTemp.Rows[0]["Software Name"].ToString();
                    int iIndex = cmbSoftwareName.FindString(sSoftwareName);
                    if (iIndex > -1)
                        cmbSoftwareName.SelectedIndex = iIndex;
                }
            }
        }

        private void LoadTerminalSchedule(string sScheduleID)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPScheduleSoftwareBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "WHERE ScheduleID=" + sScheduleID;
                DataTable dtTempSchedule = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTempSchedule);
                if (dtTempSchedule.Rows.Count > 0)
                {
                    switch (dtTempSchedule.Rows[0]["ScheduleType"].ToString().ToUpper())
                    {
                        case "DAILY":
                            dateTimePickerClockDaily.Checked = true;
                            dateTimePickerClockDaily.Value = DateTime.Parse(dtTempSchedule.Rows[0]["ScheduleTime"].ToString());
                            break;
                        case "WEEKLY":
                            chkEnableWeekly.Checked = true;
                            dateTimePickerClockWeekly.Value = DateTime.Parse(dtTempSchedule.Rows[0]["ScheduleTime"].ToString());
                            string sDay = dtTempSchedule.Rows[0]["ScheduleCheck"].ToString();

                            foreach (Control ctrlChkDays in panelScheduleWeekly.Controls)
                                if (ctrlChkDays is CheckBox)
                                {
                                    if (ctrlChkDays.Tag.ToString().ToLower() == sDay.ToLower())
                                        ((CheckBox)ctrlChkDays).Checked = true;
                                }
                            break;
                        case "MONTHLY":
                            chkEnableMonthly.Checked=true;
                            cmbMonthlyDate.Text = dtTempSchedule.Rows[0]["ScheduleCheck"].ToString();
                            dateTimePickerClockMonthly.Value = DateTime.Parse(dtTempSchedule.Rows[0]["ScheduleTime"].ToString());
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void LoadDataTerminalBrandType(string _sTerminalType)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalTypeBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format("WHERE TerminalTypeName='{0}'", _sTerminalType);
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                if (dtTemp.Rows.Count > 0)
                {
                    string sTerminalBrand = dtTemp.Rows[0]["Brand"].ToString();
                    
                    int iIndex = cmbTerminalBrand.FindString(sTerminalBrand);
                    if(iIndex>-1)
                        cmbTerminalBrand.SelectedIndex = iIndex;
                    //for (int iIndex = 0; iIndex < cmbTerminalBrand.Items.Count; iIndex++)
                    //    if (cmbTerminalBrand.Items[iIndex].ToString() == sTerminalBrand)
                    //    {
                    //        cmbTerminalBrand.SelectedIndex = iIndex;
                    //        break;
                    //    }
                    if (dtTerminalType != null)
                    {
                        iIndex = cmbTerminalType.FindString(_sTerminalType);
                        if (iIndex > -1)
                            cmbTerminalType.SelectedIndex = iIndex;
                        //for (int iIndex = 0; iIndex < cmbTerminalType.Items.Count; iIndex++)
                        //    if (cmbTerminalType.Items[iIndex].ToString() == _sTerminalType)
                        //    {
                        //        cmbTerminalType.SelectedIndex = iIndex;
                        //        break;
                        //    }
                    }
                }
            }
        }

        private void SaveGroupRegion()
        {
            string sTerminalIDList = rowData["TerminalIDList"].ToString();
            iEnableDownload = chkEnableDownloadGroupRegion.Checked ? 1 : 0;
            if (chkEnableSchedule.Checked)
            {
                if (dateTimePickerClockDaily.Checked == true || chkEnableWeekly.Checked == true || chkEnableMonthly.Checked == true)
                {
                    if (dateTimePickerClockDaily.Checked == true && chkEnableWeekly.Checked == false && chkEnableMonthly.Checked == false)
                    {
                        //sTypeScheduleDownload = "DAILY";
                        //sCheckScheduleDownload = "EVERY DAYS";
                        //sTimeScheduleDownload = dateTimePickerClockDaily.Value.ToString("ddMMyy hh:mm");

                        InsertSchedule(sTerminalIDList, sTypeScheduleDownload, sCheckScheduleDownload, dateTimePickerClockDaily.Value, iEnableDownload);
                    }
                    else if (dateTimePickerClockDaily.Checked == false && chkEnableWeekly.Checked == true && chkEnableMonthly.Checked == false)
                    {
                        //sTypeScheduleDownload = "WEEKLY";
                        if (chkSunday.Checked == false && chkMonday.Checked == false &&
                            chkTuesday.Checked == false && chkWednesday.Checked == false &&
                            chkThursday.Checked == false && chkFriday.Checked == false && chkSaturday.Checked == false)
                        {
                            MessageBox.Show("Choose Day First.");
                        }
                        else
                        {
                            //sTimeScheduleDownload = dateTimePickerClockWeekly.Value.ToString("ddMMyy hh:mm");
                            InsertSchedule(sTerminalIDList, sTypeScheduleDownload, sCheckScheduleDownload, dateTimePickerClockWeekly.Value, iEnableDownload);
                        }
                    }
                    else if (dateTimePickerClockDaily.Checked == false && chkEnableWeekly.Checked == false && chkEnableMonthly.Checked == true)
                    {
                        //sTypeScheduleDownload = "MONTHLY";
                        if (cmbMonthlyDate.SelectedIndex > -1)
                        {
                            //int iDate = cmbMonthlyDate.SelectedIndex + 1;
                            //sCheckScheduleDownload = iDate.ToString();
                            InsertSchedule(sTerminalIDList, sTypeScheduleDownload, sCheckScheduleDownload, dateTimePickerClockMonthly.Value, iEnableDownload);
                        }
                        else MessageBox.Show("Choose Date First.");
                    }
                }
                else
                    MessageBox.Show("Choose Schedule First.");
            }
            else
                InsertSchedule(sTerminalIDList, null, null, (new DateTime()), iEnableDownload);

            if (cmbSoftwareNameGroup.SelectedIndex > -1)
            { 
                //update per group
                int iAppPackID = Convert.ToInt32(cmbSoftwareNameGroup.SelectedValue);
                UpdateAppPackIDTerminalSN(Convert.ToString(frmIdType), txtGroupRegionName.Text, iAppPackID);
            }
        }

        private void UpdateAppPackIDTerminalSN(string sFormID,string sRegionGroupName,int iAppPackID)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalSNUpdateAppPackID, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sForm", SqlDbType.VarChar).Value = sFormID;
                oCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = sRegionGroupName;
                oCmd.Parameters.Add("@iAppPackID", SqlDbType.Int).Value = iAppPackID;

                oCmd.ExecuteNonQuery();
            }
        }

        private void InsertSchedule(string sTerminalID, string sScheduleType, string sCheckSchedule, DateTime dtTime, int iEnableDownload)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPScheduleSoftwareInsert, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oCmd.Parameters.Add("@sScheduleType", SqlDbType.VarChar).Value = sScheduleType;
                oCmd.Parameters.Add("@sScheduleCheck", SqlDbType.VarChar).Value = sCheckSchedule;
                if (dtTime != new DateTime())
                    oCmd.Parameters.Add("@dtScheduleTime", SqlDbType.DateTime).Value = dtTime == null ? DateTime.Now : dtTime;
                oCmd.Parameters.Add("@iEnableDownload", SqlDbType.Bit).Value = iEnableDownload;

                oCmd.Parameters.Add("@iScheduleID", SqlDbType.Int).Direction = ParameterDirection.Output;
                
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                oCmd.ExecuteNonQuery();
                
                sCheckSchedule = "";
                iScheduleID = int.Parse(oCmd.Parameters["@iScheduleID"].Value.ToString());
            }
            //UpdateGroupRegionSchedule();
            UpdateGroupRegionSNSchedule();
            MessageBox.Show("Saved");

            CommonClass.InputLog(oSqlConnAuditTrail, sTerminalID, UserData.sUserID, "",
                                "Set Schedule", "Set " + sTerminalID + " TypeSchedule: " + sTypeScheduleDownload +
                                " Cek Update : " + sCheckScheduleDownload + " Date and Time :" + dtTime.ToString() + " Enable Download : " + iEnableDownload);
        }

        //private void UpdateGroupRegionSchedule()
        //{
        //    using (SqlCommand oCmd = new SqlCommand(frmIdType == GroupRegionSnType.Group ? CommonSP.sSPGroupUpdateScheduleID : CommonSP.sSPRegionUpdateScheduleID,
        //        oSqlConn))
        //    {
        //        oCmd.CommandType = CommandType.StoredProcedure;
        //        oCmd.Parameters.Add("@iId", SqlDbType.Int).Value = int.Parse(rowData["Id"].ToString());
        //        oCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = rowData["Name"].ToString();
        //        oCmd.Parameters.Add("@iScheduleID", SqlDbType.Int).Value = iScheduleID;
        //        oCmd.ExecuteNonQuery();
        //    }
        //}

        private void UpdateGroupRegionSNSchedule()
        {
            string sQuery = string.Format("UPDATE tbTerminalSN SET ScheduleID={0} WHERE {1}={2}",
                iScheduleID,
                frmIdType == GroupRegionSnType.Group ? "GroupID" : "RegionID",
                rowData["Id"].ToString());
            using (SqlCommand oCmd = new SqlCommand(sQuery,
                oSqlConn))
            {
                oCmd.ExecuteNonQuery();
            }
        }

        private void InitScheduleWeekly(object sender)
        {
            foreach(Control ctrl in panelScheduleWeekly.Controls)
                if (ctrl is CheckBox)
                {
                    if (ctrl != sender)
                        ((CheckBox)ctrl).Checked = false;
                }
        }

        private bool IsValidTerminalSN(ref string _sError)
        {
            bool bValid = false;
            if (!string.IsNullOrEmpty(txtSerialNumber.Text))
                if (!string.IsNullOrEmpty(txtTerminalName.Text))
                    if (cmbTerminalBrand.SelectedValue != null && cmbTerminalType.SelectedValue != null)
                        //if (!isTerminalSNExist())
                        bValid = true;
                        //else
                        //    _sError = "Terminal SN allready exist";
                    else
                        _sError = "Please choose terminal type and brand";
                else
                    _sError = "Please fill Terminal Name";
            else
                _sError = "Please fill Terminal Serial Number";
            return bValid;
        }

        private bool IsTerminalSNExist()
        {
            bool isExist = false;            
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalSNBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                //oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value =
                //    string.Format("WHERE SerialNumber='{0}' AND TerminalTypeName='{1}'",
                //    txtSerialNumber.Text, cmbTerminalType.SelectedValue.ToString());
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value =
                    string.Format("WHERE SerialNumber='{0}'",
                    txtSerialNumber.Text);
                using (SqlDataReader reader = oCmd.ExecuteReader())
                    if (reader.HasRows)
                        isExist = true;
            }
            return isExist;
        }

        private void SaveTerminalSN()
        {
            GetiAppPackID();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalSNInsert, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = txtSerialNumber.Text;
                oCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = txtTerminalName.Text;
                oCmd.Parameters.Add("@sTerminalTypeName", SqlDbType.VarChar).Value = cmbTerminalType.SelectedValue.ToString();
                if (chkEnableSchedule.Checked)
                {
                    oCmd.Parameters.Add("@sScheduleType", SqlDbType.VarChar).Value = sTypeScheduleDownload;
                    oCmd.Parameters.Add("@sScheduleCheck", SqlDbType.VarChar).Value = sCheckScheduleDownload;
                    oCmd.Parameters.Add("@dtScheduleTime", SqlDbType.DateTime).Value = DateTime.Parse(sTimeScheduleDownload);
                }
                oCmd.Parameters.Add("@iEnableSchedule", SqlDbType.Int).Value = chkEnableSchedule.Checked == true ? 1 : 0;
                oCmd.Parameters.Add("@iGroupId", SqlDbType.VarChar).Value = iGroupId;
                oCmd.Parameters.Add("@iRegionId", SqlDbType.VarChar).Value = iRegionId;
                if (iAppPackageID > 0)
                    oCmd.Parameters.Add("@iAppPackageID", SqlDbType.VarChar).Value = iAppPackageID;
                if (!string.IsNullOrEmpty(rtbTerminalDescription.Text))
                    oCmd.Parameters.Add("@sDescription", SqlDbType.VarChar).Value = rtbTerminalDescription.Text;
                oCmd.ExecuteNonQuery();
                
            }
        }

        private int GetiAppPackID()
        {
            if (cmbSoftwareName.SelectedIndex != -1)
                iAppPackageID = Convert.ToInt32(cmbSoftwareName.SelectedValue);
            else
                iAppPackageID = 0;
            return iAppPackageID;
        }

        private string ScheduleIDFromDB()
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalSNBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value =
                    string.Format("WHERE SerialNumber='{0}'",
                    txtSerialNumber.Text);
                SqlDataReader oReader = oCmd.ExecuteReader();
                while (oReader.Read())
                {
                    sScheduleID = oReader["ScheduleID"].ToString();
                }
                oReader.Close();
                oReader.Dispose();
            }
            return sScheduleID;
        }      
        #endregion
    }
}
