﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using InSysClass;

namespace InSys
{
    public partial class FrmExpand : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        //protected DataTable odtTerminal;
        public DataTable odtTerminal;
        protected DataTable odtRelation;
        protected DataTable odtTag;
        protected DataTable odtEdcInfo;
        protected DataTable odtInitEdcInfo;
        DataTable odtItemList = new DataTable();

        protected DataSet odsTerminal = new DataSet();
        string sTerminalID;
        string sTypeScheduleDownload;
        string sCheckScheduleDownload;
        string sTimeScheduleDownload;
        DateTime dtTimeScheduleDownload;

        int iEnableDownload;
        string sEnableDownload;
        int iScheduleID;
        int iCountTerminal;
        int iRowCount;
        string sDayOfWeek;
        string[] arrsSortedAcquirer;

        string datapasing;

        class Relations
        {
            public string sAcquirerName { get; set; }
            public string sIssuerName { get; set; }
            public string sCardName { get; set; }

            //public void FillValue(int iIndex, string sValue)
            //{
            //    switch (iIndex)
            //    {
            //        case 2: sAcquirerName = sValue; break;
            //        case 1: sIssuerName = sValue; break;
            //        case 0: sCardName = sValue; break;
            //        default: sAcquirerName = sIssuerName = sCardName = ""; break;
            //    }
            //}

            //public string sGetValue(int iIndex)
            //{
            //    switch (iIndex)
            //    {
            //        case 2: return sAcquirerName;
            //        case 1: return sIssuerName;
            //        case 0: return sCardName;
            //        default: return "";
            //    }
            //}
        }
        List<Relations> ltRelations = new List<Relations>();

        class Package
        {
            protected string sAppPackageName;
            protected string sAppPackageId;

            public string Name
            {
                get { return sAppPackageName; }
                set { sAppPackageName = value; }
            }

            public string ID
            {
                get { return sAppPackageId; }
                set { sAppPackageId = value; }
            }
        }
        List<Package> ltPackage = new List<Package>();

        class SortRelation
        {
            protected int iIndex;
            protected string sItemName;

            public void doFillValue(int _iIndex, string _sItemName)
            {
                iIndex = _iIndex;
                sItemName = _sItemName;
            }

            public string sGetItemName()
            {
                return sItemName;
            }

            public int sGetIndex()
            {
                return iIndex;
            }
        }

        /// <summary>
        /// Needed to store value at different events.
        /// </summary>
        protected string sOldRenameKey = "";

        /// <summary>
        /// Needed to store value at different events.
        /// </summary>
        protected string sNewRenameKey = "";

        /// <summary>
        /// Current User
        /// </summary>
        public string sUID;

        /// <summary>
        /// Current Database's ID
        /// </summary>
        public string sDbID;

        /// <summary>
        /// Current Database's Name
        /// </summary>
        public string sDbName;

        public bool isEMVInit;
        public bool isTLEInit;
        public bool isLoyaltyProd;
        public bool isGPRS;
        public bool isCurrency;
        public bool isPinPad;
        public bool isRemoteDownload;
        public bool isPromoManagement;
        public bool isBankCode;
        public bool isProductCode;
        public bool isEMVInitManagement;
        public bool isCustomMenu;
        public bool isInitialFlazzManagement;
        public bool isCardTypeManagement;
        public bool isEnableMonitor;
        public bool isEnableSnManagement;

        //Point pntAIDLocation;
        //Point pntKeyLocation;
        //Point pntTLELocation;
        //Point pntEmvManagementLocation;
        //Point pntRemoteDownloadLocation;
        //Point pntGPRSLocation;
        //Point pntPinPadLocation;
        //Point pntPromoManagementLocation;
        //Point pntInitFlazzManagementLocation;

        //Point[] arrpointManagementButton = new Point[10];
        //Control[] arrbtnManagement = new Control[10];

        /// <summary>
        /// Form to view detail data of selected versoin
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sUID">String : Current active user</param>
        /// <param name="_sDbID">String : Database ID</param>
        /// <param name="_sDbName">String : Database Name</param>
        /// <param name="_IsEMVInit">boolean : true if EMV Init else false</param>
        /// <param name="_IsTLEInit">boolean : true if TLE Init else false</param>
        /// <param name="_IsLoyaltyProd">boolean : true if Loyalty Product else false</param>
        public FrmExpand(SqlConnection _oSqlConn,
             SqlConnection _oSqlConnAuditTrail,
             string _sUID,
             string _sDbID,
             string _sDbName,
             bool _isEMVInit,
             bool _isTLEInit,
             bool _isLoyaltyProd,
             bool _isGPRS,
             bool _isCurrency,
             bool _isPinPad,
             bool _isRemoteDownload,
             bool _isEMVInitManagement)
            : this(_oSqlConn, _oSqlConnAuditTrail, _sUID, _sDbID, _sDbName, _isEMVInit, _isTLEInit, _isLoyaltyProd, _isGPRS, _isCurrency, _isPinPad, _isRemoteDownload, false, false, _isEMVInitManagement, false, false, false, false, false, false)
        {

        }

        public FrmExpand(SqlConnection _oSqlConn,
            SqlConnection _oSqlConnAuditTrail,
            string _sUID,
            string _sDbID,
            string _sDbName,
            bool _isEMVInit,
            bool _isTLEInit,
            bool _isLoyaltyProd,
            bool _isGPRS,
            bool _isCurrency,
            bool _isPinPad,
            bool _isRemoteDownload,
            bool _isBankCode,
            bool _isProductCode,
            bool _isEMVInitManagement,
            bool _isPromoManagement,
            bool _isCustomMenu,
            bool _isInitialFlazzManagement,
            bool _isCardTypeManagement,
            bool _isEnableMonitor,
            bool _isEnableSnManagement)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sUID = _sUID;
            sDbID = _sDbID;
            sDbName = _sDbName;
            isEMVInit = _isEMVInit;
            isTLEInit = _isTLEInit;
            isLoyaltyProd = _isLoyaltyProd;
            isGPRS = _isGPRS;
            isCurrency = _isCurrency;
            isPinPad = _isPinPad;
            isRemoteDownload = _isRemoteDownload;
            isBankCode = _isBankCode;
            isProductCode = _isProductCode;
            isEMVInitManagement = _isEMVInitManagement;
            isPromoManagement = _isPromoManagement;
            isCustomMenu = _isCustomMenu;
            isInitialFlazzManagement = _isInitialFlazzManagement;
            isCardTypeManagement = _isCardTypeManagement;
            isEnableMonitor = _isEnableMonitor;
            isEnableSnManagement = _isEnableSnManagement;
            InitializeComponent();
        }

        protected bool isActivated = false;

        /// <summary>
        /// Call InitData and LoadData function.
        /// </summary>
        private void FrmExpand_Activated(object sender, EventArgs e)
        {
            InitData();
            LoadData(0);
            InitDataPackage();

            // Search Serial Number textbox
            toolStripSearchSN.Visible = toolStripSearchSN.Enabled = IsEnableSearchSN();

            CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, sDbName, CommonMessage.sFormOpened + this.Text, "");
        }

        protected bool IsEnableSearchSN()
        {
            bool bEnableSearchSN = false;
            using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPControlFlagBrowse, oSqlConn))
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sItemName", SqlDbType.VarChar).Value = "Search SN";
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    if (reader.HasRows && reader.Read())
                        bEnableSearchSN = bool.Parse(reader["Flag"].ToString());
            }
            return bEnableSearchSN;
        }

        #region "Data"
        /// <summary>
        /// Initiate objects on form.
        /// </summary>
        private void InitData()
        {
            //CommonClass.doMinimizeChild(this.ParentForm.MdiChildren, this.Name);
            //this.WindowState = FormWindowState.Maximized;
            InitButton();
            InitTreeAndListView();
        }

        /// <summary>
        /// Clear all Trees and ListView in Form.
        /// </summary>
        protected void InitTreeAndListView()
        {
            tvTerminal.Nodes.Clear();
            //lvTerminal.Items.Clear();
            //lvTagDetail.Items.Clear();
            dgvTerminal.DataSource = null;
            dgvTagDetail.DataSource = null;
        }

        /// <summary>
        /// Load data from current DatabaseID
        /// </summary>
        /// <param name="iMode">int : Mode for loading</param>
        protected void LoadData(int iMode)
        {
            FillodtItemList();
            InitTreeAndListView();
            if (isLoadTerminalList(iMode))
                FillListTerminal();
            //else
            //    LoadData(0);
            this.Text = sDbName + " - " + ((odtTerminal == null) ? "0" : odtTerminal.Rows.Count.ToString());
        }

        private void FillodtItemList()
        {
            try
            {
                //DataTable odtItemList = new DataTable();
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = string.Format("WHERE DatabaseID = '{0}'", sDbID);
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    odtItemList.Clear();
                    new SqlDataAdapter(oSqlCmd).Fill(odtItemList);
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Load Package data from database and stored it in List of strings 
        /// </summary>
        protected void InitDataPackage()
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAppPackageBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.HasRows)
                        {
                            DataTable dtTemp = new DataTable();
                            dtTemp.Load(oRead);
                            foreach (DataRow row in dtTemp.Rows)
                            {
                                Package packageTemp = new Package();
                                packageTemp.Name = row["Software Name"].ToString();
                                packageTemp.ID = row["AppPackID"].ToString();
                                ltPackage.Add(packageTemp);
                            }
                        }
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Determines if there're data found when loads data from database.
        /// </summary>
        /// <param name="iMode">int : Load mode, determine condition string</param>
        /// <returns>boolean : true if data found, else false</returns>
        protected bool isLoadTerminalList(int iMode)
        {
            bool isGetData = true;

            odtTerminal = new DataTable();
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sDbConditions(iMode);

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                odtTerminal.Clear();
                odtTerminal.Load(oSqlCmd.ExecuteReader());
                odtTerminal.PrimaryKey = new DataColumn[] { odtTerminal.Columns["TerminalID"] };
                if (odtTerminal.Rows.Count == 0)
                {
                    isGetData = false;
                    //MessageBox.Show("Empty Database");
                }

                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return isGetData;
        }

        /// <summary>
        /// Fill List View terminal with profile's data from database.
        /// </summary>
        protected void FillListTerminal()
        {
            //foreach (DataRow drRow in oDataTableTerminal.Rows)
            //    lvTerminal.Items.Add(drRow["TerminalID"].ToString(), drRow["TerminalID"].ToString(), 2);

            dgvTerminal.DataSource = odtTerminal;
            dgvTerminal.ClearSelection();
            for (int iCol = 0; iCol < dgvTerminal.Columns.Count; iCol++)
                if (dgvTerminal.Columns[iCol].Name != "TerminalID")
                    dgvTerminal.Columns[iCol].Visible = false;

        }

        /// <summary>
        /// Get condition string to filter data from database.
        /// </summary>
        /// <param name="iMode">int : Condition Mode</param>
        /// <returns>string : Condition String</returns>
        protected string sDbConditions(int iMode)
        {
            if (iMode == 0) return "WHERE DATABASEID = " + sDbID;
            if (iMode == 2) return "WHERE SerialNumber = '" + toolStripSearchSN.Text + "' AND DATABASEID = " + sDbID;
            else return "WHERE TerminalID LIKE '" + sGetTIDSearch(toolStripSearchTID.Text) + "%' AND DATABASEID = " + sDbID;
        }

        /// <summary>
        /// Get selected profile's index.
        /// </summary>
        /// <returns>int : Selected index</returns>
        protected int iGetSelectectedIndex()
        {
            //return lvTerminal.SelectedIndices[0];
            return dgvTerminal.CurrentRow.Index;
        }

        /// <summary>
        /// Determine if Profile's relation's data is found or not.
        /// </summary>
        /// <returns>boolean : true if found, else false</returns>
        protected bool isGetProfileRelation()
        {
            bool isGetData = false;
            try
            {
                odtRelation = new DataTable();

                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPRelationBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sGetTerminalID();

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                SqlDataReader oRead = oSqlCmd.ExecuteReader();
                odtRelation.Clear();

                if (oRead.HasRows)
                {
                    odtRelation.Load(oRead);
                    isGetData = true;
                }

                oRead.Close();
                oRead.Dispose();
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return isGetData;
        }

        /// <summary>
        /// Get selected TerminalID.
        /// </summary>
        /// <returns>string : Terminal ID</returns>
        protected string sGetTerminalID()
        {
            return odtTerminal.Rows[iGetSelectectedIndex()]["TerminalID"].ToString();
        }

        /// <summary>
        /// Clear Tree terminal, then fill the profile's relation.
        /// </summary>
        protected void LoadTreeTerminal()
        {
            tvTerminal.Nodes.Clear();
            //lvTagDetail.Items.Clear();
            dgvTagDetail.DataSource = null;

            FillTreeTerminal(isGetProfileRelation());
        }

        /// <summary>
        /// Fill Tree View with ProfileRelation data from database.
        /// </summary>
        /// <param name="isTree">boolean : determines if current node is Terminal node</param>
        protected void FillTreeTerminal(bool isTree)
        {
            ltRelations.Clear();
            int iJmlAcq = (odtRelation.Rows.Count / 3);
            //sArrRelations = new string[iJmlAcq][];

            int iIndexRow = 0;
            for (int iCount = 0; iCount < iJmlAcq; iCount++)
            {
                Relations tempRelation = new Relations();
                //sArrRelations[iCount] = new string[3];
                for (int iIndexArr = 0; iIndexArr < 3; iIndexArr++)
                {
                    //tempRelation.FillValue(iIndexArr, odtRelation.Rows[iIndexRow]["RelationTagValue"].ToString());
                    //sArrRelations[iCount][iIndexArr] = oDataTableRelation.Rows[iIndexRow]["RelationTagValue"].ToString();
                    switch (iIndexArr)
                    {
                        case 0: tempRelation.sCardName = odtRelation.Rows[iIndexRow]["RelationTagValue"].ToString(); break;
                        case 1: tempRelation.sIssuerName = odtRelation.Rows[iIndexRow]["RelationTagValue"].ToString(); break;
                        case 2: tempRelation.sAcquirerName = odtRelation.Rows[iIndexRow]["RelationTagValue"].ToString(); break;
                    }
                    iIndexRow++;
                }
                ltRelations.Add(tempRelation);
            }

            TreeNode oNodeTerminal = tvTerminal.Nodes.Add(sGetTerminalID());
            if (isTree)
            {
                AddAcqNode(oNodeTerminal);

                int iTotalAcquirer = oNodeTerminal.Nodes.Count;
                arrsSortedAcquirer = new string[iTotalAcquirer];
                for (int i = 0; i < iTotalAcquirer; i++)
                    arrsSortedAcquirer[i] = oNodeTerminal.Nodes[i].Name;
            }
            oNodeTerminal.Expand();
        }

        /// <summary>
        /// Add Acquirer node to Tree View
        /// </summary>
        /// <param name="oNodeParent">TreeNode : Parent's node for current Acquirer Node</param>
        protected void AddAcqNode(TreeNode oNodeParent)
        {
            TreeNode oNodeAcq;
            //foreach (Array arrRelations in sArrRelations)
            foreach (Relations tempRelation in ltRelations)
                //if (!oNodeParent.Nodes.ContainsKey(arrRelations.GetValue(2).ToString()))
                //if (!oNodeParent.Nodes.ContainsKey(tempRelation.sGetValue(2)))
                if (!oNodeParent.Nodes.ContainsKey(tempRelation.sAcquirerName))
                {
                    //oNodeAcq = oNodeParent.Nodes.Add(tempRelation.sGetValue(2), tempRelation.sGetValue(2));
                    //AddIssuerNode(oNodeAcq, tempRelation.sGetValue(2));
                    oNodeAcq = oNodeParent.Nodes.Add(tempRelation.sAcquirerName, tempRelation.sAcquirerName);
                    AddIssuerNode(oNodeAcq, tempRelation.sAcquirerName);
                }
        }

        /// <summary>
        /// Add Issuer node to Tree View
        /// </summary>
        /// <param name="oNodeParent">TreeNode : Parent's node for current Issuer Node</param>
        /// <param name="sAcq">string : Acquire's name for current Issuer</param>
        protected void AddIssuerNode(TreeNode oNodeParent, string sAcq)
        {
            TreeNode oNodeIss;
            //foreach (Array arrRelations in sArrRelations)
            //foreach (Relations tempRelation in ltRelations)
            //    if (!string.IsNullOrEmpty(tempRelation.sGetValue(1)) &&
            //            !oNodeParent.Nodes.ContainsKey(tempRelation.sGetValue(1))) // Periksa Acquirernya sudah ada atau belum 
            //        if (sAcq == tempRelation.sGetValue(2)) // Periksa apakah Acquirernya sama dengan Acq yang akan ditambah ke treeview
            //        {
            //            oNodeIss = oNodeParent.Nodes.Add(tempRelation.sGetValue(1), tempRelation.sGetValue(1));
            //            AddCardNode(oNodeIss, tempRelation.sGetValue(1));
            //        }

            foreach (Relations tempRelation in ltRelations)
                if (!string.IsNullOrEmpty(tempRelation.sIssuerName) && !oNodeParent.Nodes.ContainsKey(tempRelation.sIssuerName))
                    if (sAcq == tempRelation.sAcquirerName)
                    {
                        oNodeIss = oNodeParent.Nodes.Add(tempRelation.sIssuerName, tempRelation.sIssuerName);
                        AddCardNode(oNodeIss, tempRelation.sIssuerName);
                    }
        }

        /// <summary>
        /// Add Card node to Tree View
        /// </summary>
        /// <param name="oNodeParent">TreeNode : Parent's node for current Issuer Node</param>
        /// <param name="sIss">string : Issuer's name for current Issuer</param>
        protected void AddCardNode(TreeNode oNodeParent, string sIss)
        {
            TreeNode oNodeCard;
            //foreach (Array arrRelations in sArrRelations)
            //foreach (Relations tempRelation in ltRelations)
            //    if (!string.IsNullOrEmpty(tempRelation.sGetValue(0)) &&
            //            !oNodeParent.Nodes.ContainsKey(tempRelation.sGetValue(0))) // Periksa Issuernya sudah ada atau belum

            //        if (sIss == tempRelation.sGetValue(1)) // Periksa apakah Issuernya sama dengan Issuer yang akan ditambah ke treeview
            //            oNodeCard = oNodeParent.Nodes.Add(tempRelation.sGetValue(0), tempRelation.sGetValue(0));

            foreach (Relations tempRelation in ltRelations)
                if (!string.IsNullOrEmpty(tempRelation.sCardName) && !oNodeParent.Nodes.ContainsKey(tempRelation.sCardName))
                    if (sIss == tempRelation.sIssuerName)
                        oNodeCard = oNodeParent.Nodes.Add(tempRelation.sCardName, tempRelation.sCardName);
        }

        /// <summary>
        /// Load EDC data from database and fill them into objects in form
        /// </summary>
        protected void LoadEdcInfo()
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAutoInitLogInfoBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sGetTerminalID();
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        FillEDCInfo(oRead);
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Clear EDC data shown in form
        /// </summary>
        protected void ClearInitEdcInfo()
        {
            txtTIDInit.Clear();
            txtEdcSNInit.Clear();
            txtInitSoftwareVer.Clear();
        }

        protected void ClearEdcInfo()
        {
            txtTID.Clear();
            txtLastInit.Clear();
            txtPABX.Clear();
            txtSoftwareVer.Clear();
            txtOSVer.Clear();
            txtKernelVer.Clear();
            txtEDCSN.Clear();
            txtReaderSN.Clear();
            txtPSAMSN.Clear();
            txtMCSN.Clear();
            txtMemUsage.Clear();
            txtICCID.Clear();
        }

        /// <summary>
        /// Show EDC Info from database
        /// </summary>
        /// <param name="oReader">SqlDataReader : Data selected from database</param>
        protected void FillEDCInfo(SqlDataReader oReader)
        {
            ClearEdcInfo();
            //ResetSchedule();
            txtTID.Text = sGetTerminalID();
            if (oReader.Read())
            {
                txtLastInit.Text = oReader["LastInitTime"].ToString();
                txtPABX.Text = oReader["PABX"].ToString();
                txtSoftwareVer.Text = oReader["SoftwareVer"].ToString();
                txtOSVer.Text = oReader["OSVer"].ToString();
                txtKernelVer.Text = oReader["KernelVer"].ToString();
                txtEDCSN.Text = oReader["EDCSN"].ToString();
                txtReaderSN.Text = oReader["ReaderSN"].ToString();
                txtPSAMSN.Text = oReader["PSAMSN"].ToString();
                txtMCSN.Text = oReader["MCSN"].ToString();
                txtMemUsage.Text = oReader["MemUsage"].ToString();
                txtICCID.Text = oReader["ICCID"].ToString();
            }
        }

        protected void FillEDCInfo()
        {
            ClearEdcInfo();
            //ResetSchedule();
            txtTID.Text = sGetTerminalID();
            if (odtEdcInfo.Rows.Count > 0)
            {
                txtLastInit.Text = odtEdcInfo.Rows[0]["LastInitTime"].ToString();
                txtPABX.Text = odtEdcInfo.Rows[0]["PABX"].ToString();
                txtSoftwareVer.Text = odtEdcInfo.Rows[0]["SoftwareVer"].ToString();
                txtOSVer.Text = odtEdcInfo.Rows[0]["OSVer"].ToString();
                txtKernelVer.Text = odtEdcInfo.Rows[0]["KernelVer"].ToString();
                txtEDCSN.Text = odtEdcInfo.Rows[0]["EDCSN"].ToString();
                txtReaderSN.Text = odtEdcInfo.Rows[0]["ReaderSN"].ToString();
                txtPSAMSN.Text = odtEdcInfo.Rows[0]["PSAMSN"].ToString();
                txtMCSN.Text = odtEdcInfo.Rows[0]["MCSN"].ToString();
                txtMemUsage.Text = odtEdcInfo.Rows[0]["MemUsage"].ToString();
                if (odtEdcInfo.Columns.Contains("ICCID"))
                    txtICCID.Text = odtEdcInfo.Rows[0]["ICCID"].ToString();

            }
        }

        /// <summary>
        /// Show EDC Info from database
        /// </summary>
        /// <param name="oReader">SqlDataReader : Data selected from database</param>
        protected void FillInitEDCInfo()
        {
            ClearInitEdcInfo();
            txtTIDInit.Text = sGetTerminalID();
            if (odtInitEdcInfo.Rows.Count > 0)
            {
                txtEdcSNInit.Text = odtInitEdcInfo.Rows[0]["SerialNumber"].ToString();
                txtInitSoftwareVer.Text = odtInitEdcInfo.Rows[0]["Software"].ToString();
            }
        }

        /// <summary>
        /// Load Applicatoin Package Data from database for terminal which already selected before
        /// </summary>
        /// <param name="_sTerminalID">String : Terminal ID which Application Package Data will be retrieved</param>
        protected void LoadEdcAppPackage(string _sTerminalID)
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAppPackageBrowseTerminalID, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value
                        = string.IsNullOrEmpty(_sTerminalID) ? sGetTerminalID() : _sTerminalID;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        FillEdcAppPackageInfo(oRead);
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Show Application Package Info from database 
        /// </summary>
        /// <param name="oRead"></param>
        protected void FillEdcAppPackageInfo(SqlDataReader oRead)
        {
            ClearEdcAppPackageInfo();
            if (oRead.HasRows)
            {
                DataTable dt = new DataTable();
                dt.Load(oRead);
                foreach (DataRow row in dt.Rows)
                {
                    lvAppEDC.Items.Add(row["AppPackageName"].ToString());
                }
            }
        }

        protected void FillEdcAppPackageInfo(DataTable dtTemp)
        {
            if (dtTemp.Rows.Count > 0)
            {
                ClearEdcAppPackageInfo();
                foreach (DataRow row in dtTemp.Rows)
                {
                    lvAppEDC.Items.Add(row["AppPackageName"].ToString());
                }
            }
        }

        private void ClearEdcAppPackageInfo()
        {
            lvAppEDC.Items.Clear();
        }

        /// <summary>
        /// Load TagDetail from database and determines if TagDetails is found in database.
        /// </summary>
        /// <returns>boolean : true if is found, else false</returns>
        protected bool isGetTagDetail()
        {
            bool isGetData = false;
            try
            {
                odtTag = new DataTable();
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTerminalID ", SqlDbType.VarChar).Value = tvTerminal.Nodes[0].Text;
                    oSqlCmd.Parameters.Add("@sConditions", SqlDbType.VarChar).Value = sTagConditions();
                    oSqlCmd.Parameters.Add("@sMode", SqlDbType.VarChar).Value = tvTerminal.SelectedNode.Level;

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        odtTag.Clear();
                        if (oRead.HasRows)
                        {
                            odtTag.Load(oRead);
                            isGetData = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return isGetData;
        }

        /// <summary>
        /// Get the condition filter string when loading data from database, each form Terminal, Acquirer, Issuer, and Card.
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sTagConditions()
        {
            string sConditions = "";

            if (tvTerminal.SelectedNode != null)
                switch (tvTerminal.SelectedNode.Level)
                {
                    case 0: sConditions = "WHERE Name = '" + tvTerminal.SelectedNode.Text + "' AND Tag LIKE 'DE%'"; break;
                    case 1: sConditions = "WHERE Name = '" + tvTerminal.SelectedNode.Text + "'"; break;
                    case 2: sConditions = "WHERE Name = '" + tvTerminal.SelectedNode.Text + "'"; break;
                    case 3: sConditions = "WHERE Name = '" + tvTerminal.SelectedNode.Text + "'"; break;
                }
            return sConditions + " ORDER BY Tag";
        }

        /// <summary>
        /// Fill TagDetail data to ListView
        /// </summary>
        protected void FillListTagDetail()
        {
            //lvTagDetail.Items.Clear();

            //foreach (DataRow drTag in odtTag.Rows)
            //{
            //    ListViewItem lvItems = lvTagDetail.Items.Add(drTag["Tag"].ToString());
            //    lvItems.SubItems.Add(!string.IsNullOrEmpty(drTag[3].ToString()) ? drTag["ItemName"].ToString() : "NULL");
            //    lvItems.SubItems.Add(!string.IsNullOrEmpty(drTag[5].ToString()) ? drTag["TagValue"].ToString() : "NULL");
            //}

            dgvTagDetail.DataSource = null;
            dgvTagDetail.DataSource = odtTag;
            for (int iCol = 0; iCol < dgvTagDetail.Columns.Count; iCol++)
                if (dgvTagDetail.Columns[iCol].Name != "Tag"
                    && dgvTagDetail.Columns[iCol].Name != "ItemName"
                    && dgvTagDetail.Columns[iCol].Name != "TagValue")
                    dgvTagDetail.Columns[iCol].Visible = false;
        }

        protected void LoadTreeTerminalEdcInfoEdcAppPackage()
        {
            tvTerminal.Nodes.Clear();
            dgvTagDetail.DataSource = null;
            try
            {
                odsTerminal = new DataSet();

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPBrowseProfileRelationAutoInitLogAppPackage, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sGetTerminalID();
                    using (SqlDataAdapter daTables = new SqlDataAdapter(oSqlCmd))
                    {
                        daTables.Fill(odsTerminal);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            odtRelation = odsTerminal.Tables[0];
            odtEdcInfo = odsTerminal.Tables[1];
            //odtInitEdcInfo = odsTerminal.Tables[3];

            if (odtRelation.Rows.Count > 0)
                FillTreeTerminal(true);
            else if (odtRelation.Rows.Count == 0)
                FillTreeTerminal(false);

            if (odtEdcInfo.Rows.Count > 0)
                FillEDCInfo();
            //if (odtInitEdcInfo.Rows.Count > 0)
            //    FillInitEDCInfo();
            //FillEdcAppPackageInfo(odsTerminal.Tables[2]);
        }
        #endregion

        #region "Button"
        /// <summary>
        /// Initiate buttons on Form based on UserRights.
        /// </summary>
        private void InitButton()
        {
            //mnuManagement.Enabled = mnuManagement.Visible = false;
            mnuManagementCard.Enabled = mnuManagementCard.Visible = UserPrivilege.IsAllowed(PrivilegeCode.RC);
            mnuManagementAID.Enabled = mnuManagementAID.Visible = isEMVInit && UserPrivilege.IsAllowed(PrivilegeCode.AI);
            mnuManagementPK.Enabled = mnuManagementPK.Visible = isEMVInit && UserPrivilege.IsAllowed(PrivilegeCode.PK);
            mnuManagementTLE.Enabled = mnuManagementTLE.Visible = isTLEInit && UserPrivilege.IsAllowed(PrivilegeCode.TL);
            mnuManagementGprsEth.Enabled = mnuManagementGprsEth.Visible = isGPRS && UserPrivilege.IsAllowed(PrivilegeCode.GP);
            mnuManagementPinPad.Enabled = mnuManagementPinPad.Visible = isPinPad && UserPrivilege.IsAllowed(PrivilegeCode.PP);
            mnuManagementRmtDownload.Enabled = mnuManagementRmtDownload.Visible = isRemoteDownload && UserPrivilege.IsAllowed(PrivilegeCode.RD);
            mnuManagementEmv.Enabled = mnuManagementEmv.Visible = isEMVInitManagement && UserPrivilege.IsAllowed(PrivilegeCode.EM);
            mnuManagementPromo.Enabled = mnuManagementPromo.Visible = isPromoManagement && UserPrivilege.IsAllowed(PrivilegeCode.PM);
            mnuManagementInitialFLAZZ.Enabled = mnuManagementInitialFLAZZ.Visible = isInitialFlazzManagement && UserPrivilege.IsAllowed(PrivilegeCode.IF);
            mnuManagementCardType.Enabled = mnuManagementCardType.Visible = isCardTypeManagement && UserPrivilege.IsAllowed(PrivilegeCode.CM);
            mnuManagementEdcMonitor.Enabled = mnuManagementEdcMonitor.Visible = isEnableMonitor && UserPrivilege.IsAllowed(PrivilegeCode.ME);
            mnuManagementSN.Enabled = mnuManagementSN.Visible = isRemoteDownload && UserPrivilege.IsAllowed(PrivilegeCode.RD);
        }

        /// <summary>
        /// Add new terminal to DatabaseID
        /// </summary>
        private void btnNewTerminal_Click(object sender, EventArgs e)
        {
            AddNewTerminal();
        }

        /// <summary>
        /// Show 10 last view profiles.
        /// </summary>
        private void btnTenLast_Click(object sender, EventArgs e)
        {
            if (isGetTopTenTerminalList())
            {
                InitTreeAndListView();
                FillListTerminal();
            }
        }

        /// <summary>
        /// Get ten profiles which last view from database.
        /// </summary>
        private bool isGetTopTenTerminalList()
        {
            bool isGetData = true;
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalListBrowseLast, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = int.Parse(sDbID);
                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                odtTerminal.Clear();
                odtTerminal.Load(oSqlCmd.ExecuteReader());
                if (odtTerminal.Rows.Count <= 0 || odtTerminal == null)
                    isGetData = false;
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return isGetData;
        }

        /// <summary>
        /// Close the form.
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddPackage_Click(object sender, EventArgs e)
        {
            string sAppPackageName = null;
            string sAppPackageID = null;
            if (IsPackageSelected(ref sAppPackageName, ref sAppPackageID))
            {
                if (IsPackageValid(sAppPackageName))
                {
                    lvAppEDC.Items.Add(sAppPackageName);
                    SavePackage(sGetTerminalID());
                    LoadEdcAppPackage(null);
                }
                else
                    MessageBox.Show("Duplicate Package are not allowed. Please choose other package!");
            }
        }

        private void btnRemovePackage_Click(object sender, EventArgs e)
        {
            string sAppPackName = null;
            if (IsListPackageSelected(ref sAppPackName))
            {
                int iIndex = lvAppEDC.SelectedIndices[0];
                lvAppEDC.Items.RemoveAt(iIndex);
                SavePackage(sGetTerminalID());
                LoadEdcAppPackage(null);
            }
        }

        /// <summary>
        /// Search specific profile from database.
        /// </summary>
        protected void SearchProfileTerminal(string _sTerminalID)
        {
            if (!string.IsNullOrEmpty(_sTerminalID) && toolStripSearchTID.ForeColor == Color.Black)
                LoadData(1);
        }

        /// <summary>
        /// Get the key to search profile.
        /// </summary>
        /// <returns></returns>
        private string sGetTIDSearch(string _sTerminalID)
        {
            return _sTerminalID.Trim().ToUpper().Replace("'", "''");
        }
        #endregion

        #region "Events"
        //private void lvTerminal_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (lvTerminal.SelectedItems.Count > 0)
        //    {
        //        LoadTreeTerminal();
        //        LoadEdcInfo();
        //        LoadEdcAppPackage(null);
        //    }
        //}

        //private void lvTerminal_MouseClick(object sender, MouseEventArgs e)
        //{
        //    if (e.Button == MouseButtons.Right)
        //        DisplayCMS(CommonLib.NodeLevel.Terminal, MousePosition);
        //}

        /// <summary>
        /// Show Context Menu for selected profile at List Terminal.
        /// </summary>
        private void dgvTerminal_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && dgvTerminal.SelectedCells.Count == 1)
                DisplayCMS(CommonLib.NodeLevel.Terminal, MousePosition);
        }

        private void dgvTerminal_MouseClick(object sender, MouseEventArgs e)
        {
            if (dgvTerminal.SelectedCells.Count > 0)
            {
                //LoadTreeTerminal();
                //LoadEdcInfo();
                //LoadEdcAppPackage(null);
                ClearEdcInfo();
                ClearInitEdcInfo();
                LoadTreeTerminalEdcInfoEdcAppPackage();
            }
        }

        private void dgvTerminal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                if (dgvTerminal.SelectedCells.Count > 0)
                {
                    //LoadTreeTerminal();
                    //LoadEdcInfo();
                    //LoadEdcAppPackage(null);
                    LoadTreeTerminalEdcInfoEdcAppPackage();
                    e.SuppressKeyPress = true;
                }
        }

        /// <summary>
        /// Fill TagDetail ListView after node selected.
        /// </summary>
        private void tvTerminal_AfterSelect(object sender, TreeViewEventArgs e)
        {
            tvTerminal.SelectedNode = e.Node;

            if (isGetTagDetail() && tvTerminal.SelectedNode != null)
                FillListTagDetail();
        }

        /// <summary>
        /// Display Context Menu for selected node.
        /// </summary>
        private void tvTerminal_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Point oPos = MousePosition; //to capture current mouse pos. so if mouse is moved, the original position is saved.
            tvTerminal.SelectedNode = e.Node;
            if (e.Button == MouseButtons.Right) DisplayCMS((CommonLib.NodeLevel)tvTerminal.SelectedNode.Level, oPos);
        }

        /// <summary>
        /// Call FrmFlexible to show detail tag from selected node.
        /// </summary>
        private void tvTerminal_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            bool isEdit = true;
            if (e.Button == MouseButtons.Left)
            {
                if (tvTerminal.SelectedNode != null)
                {
                    TreeNode nodeTemp = tvTerminal.SelectedNode;
                    if (e.Node.Level != 3) // If not Card Node
                    {
                        //string sTerminalId = ((string[])e.Node.FullPath.Split('\\'))[0];
                        string sTerminalId = ((string[])nodeTemp.FullPath.Split('\\'))[0];
                        string sUserIdOnAccess = null;
                        if (CommonClass.IsAllowUserAccess(oSqlConn, sTerminalId, ref sUserIdOnAccess))
                        {
                            FrmFlexible fFlexible = new FrmFlexible(oSqlConn, oSqlConnAuditTrail, isEdit);
                            //tvTerminal.SelectedNode = e.Node;
                            try
                            {
                                //e.Node.Toggle();
                                //e.Node.FullPath.Split('\\').CopyTo(fFlexible.sArrCurrentRelations, 0);
                                nodeTemp.FullPath.Split('\\').CopyTo(fFlexible.sArrCurrentRelations, 0);
                                fFlexible.sDbID = sDbID;
                                fFlexible.sDbName = sDbName;
                                //fFlexible.sFormID = Convert.ToString(e.Node.Level + 1);
                                //fFlexible.sTerminalID = sGetTerminalID();
                                //fFlexible.sName = e.Node.Text;
                                fFlexible.sFormID = Convert.ToString(nodeTemp.Level + 1);
                                fFlexible.sName = nodeTemp.Text;

                                CommonClass.UserAccessInsert(oSqlConn, sTerminalId);
                                UserData.sTerminalIdActive = sTerminalId;

                                fFlexible.ShowDialog();

                                string sItemPath = dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : nodeTemp.FullPath;
                                //string sItemLevel = (dgvTerminal.Focused ? CommonLib.NodeLevel.Terminal : (CommonLib.NodeLevel)e.Node.Level).ToString();
                                string sItemLevel = (dgvTerminal.Focused ? CommonLib.NodeLevel.Terminal : (CommonLib.NodeLevel)nodeTemp.Level).ToString();

                                if (string.IsNullOrEmpty(fFlexible.sResultTag))
                                    //CommonClass.InputLog(oSqlConn, sTerminalId, UserData.sUserID, sDbName, "View " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "), "");
                                    CommonClass.InputLog(oSqlConnAuditTrail, sTerminalId, UserData.sUserID, sDbName, "View " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "), "");
                                else
                                    //CommonClass.InputLog(oSqlConn, sTerminalId, UserData.sUserID, sDbName, "Edit " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "),
                                    //fFlexible.sActDtl);
                                    CommonClass.InputLog(oSqlConnAuditTrail, sTerminalId, UserData.sUserID, sDbName, "Edit " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "),
                                        fFlexible.sActDtl);

                                if (isGetTagDetail() && tvTerminal.SelectedNode != null)
                                    FillListTagDetail();
                            }
                            catch (Exception ex)
                            {
                                CommonClass.doWriteErrorFile("Expand OnDoubleClick : " + ex.Message);
                            }
                            finally
                            {
                                bool isUpdateProfile = fFlexible.isUpdateProfile;
                                fFlexible.Dispose();
                                //try
                                //{
                                //    if (isUpdateProfile)
                                //        SaveFullContentClass.SaveFullContent(oSqlConn, sTerminalId);
                                //}
                                //catch (Exception ex)
                                //{
                                //    CommonClass.doWriteErrorFile("SaveFullContent : " + ex.Message);
                                //}
                                CommonClass.UserAccessDelete(oSqlConn, sTerminalId);
                                UserData.sTerminalIdActive = "";
                            }
                        }
                        else
                            MessageBox.Show(string.Format("Cannot open '{0}', is being accessed by '{1}'", sTerminalId, sUserIdOnAccess));
                    }
                }
            }
        }

        /// <summary>
        /// Save text from selected node.
        /// </summary>
        private void tvTerminal_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            sOldRenameKey = e.Node.Text;
        }

        /// <summary>
        /// After Terminal is renamed, validating new TerminalID, if valid then save to database and refresh Tree View.
        /// </summary>
        private void tvTerminal_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            try
            {
                if (e.Label != null && (sNewRenameKey = e.Label.Trim().ToUpper()) != sOldRenameKey &&
                    sNewRenameKey.Length > 0 && sNewRenameKey.Length < 16)
                {
                    foreach (char cIndex in sNewRenameKey.ToCharArray())
                        if ((cIndex != ' ') &&
                            (cIndex < 'A' || cIndex > 'Z') &&
                            (cIndex < '0' || cIndex > '9'))
                        {
                            e.CancelEdit = true;
                            tvTerminal.LabelEdit = false;
                            return;
                        }
                    if (isRenameItem((CommonLib.NodeLevel)e.Node.Level))
                    {
                        LoadTreeTerminal();
                        tvTerminal.LabelEdit = false;
                        return;
                    }
                }
            }
            catch (Exception oException)
            {
                tvTerminal.LabelEdit = false;
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tvTerminal.LabelEdit = false;
                e.CancelEdit = true;
            }
        }

        private void tvTerminal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                bool isEdit = true;
                if (tvTerminal.SelectedNode.Level != 3) // If not Card Node
                {
                    string sTerminalId = ((string[])tvTerminal.SelectedNode.FullPath.Split('\\'))[0];
                    string sUserIdOnAccess = null;
                    if (CommonClass.IsAllowUserAccess(oSqlConn, sTerminalId, ref sUserIdOnAccess))
                    {
                        FrmFlexible fFlexible = new FrmFlexible(oSqlConn, oSqlConnAuditTrail, isEdit);
                        try
                        {
                            tvTerminal.SelectedNode.FullPath.Split('\\').CopyTo(fFlexible.sArrCurrentRelations, 0);
                            fFlexible.sDbID = sDbID;
                            fFlexible.sDbName = sDbName;
                            fFlexible.sFormID = Convert.ToString(tvTerminal.SelectedNode.Level + 1);
                            fFlexible.sName = tvTerminal.SelectedNode.Text;

                            CommonClass.UserAccessInsert(oSqlConn, sTerminalId);
                            UserData.sTerminalIdActive = sTerminalId;

                            fFlexible.ShowDialog();

                            string sItemPath = dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : tvTerminal.SelectedNode.FullPath;
                            string sItemLevel = (dgvTerminal.Focused ? CommonLib.NodeLevel.Terminal : (CommonLib.NodeLevel)tvTerminal.SelectedNode.Level).ToString();

                            if (string.IsNullOrEmpty(fFlexible.sResultTag))
                                //CommonClass.InputLog(oSqlConn, sTerminalId, UserData.sUserID, sDbName, "View " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "), "");
                                CommonClass.InputLog(oSqlConnAuditTrail, sTerminalId, UserData.sUserID, sDbName, "View " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "), "");
                            else
                                //CommonClass.InputLog(oSqlConn, sTerminalId, UserData.sUserID, sDbName, "Edit " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "),
                                //    fFlexible.sActDtl);
                                CommonClass.InputLog(oSqlConnAuditTrail, sTerminalId, UserData.sUserID, sDbName, "Edit " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "),
                                    fFlexible.sActDtl);

                            if (isGetTagDetail() && tvTerminal.SelectedNode != null)
                                FillListTagDetail();
                        }
                        catch (Exception ex)
                        {
                            CommonClass.doWriteErrorFile("Expand OnKeyPress : " + ex.Message);
                        }
                        finally
                        {
                            bool isUpdateProfile = fFlexible.isUpdateProfile;
                            fFlexible.Dispose();
                            //try
                            //{
                            //    if (isUpdateProfile)
                            //        SaveFullContentClass.SaveFullContent(oSqlConn, sTerminalId);
                            //}
                            //catch (Exception ex)
                            //{
                            //    CommonClass.doWriteErrorFile("SaveFullContent : " + ex.Message);
                            //}
                            CommonClass.UserAccessDelete(oSqlConn, sTerminalId);
                            UserData.sTerminalIdActive = null;
                        }
                    }
                    else
                        MessageBox.Show(string.Format("Cannot open '{0}', is being accessed by '{1}'", sTerminalId, sUserIdOnAccess));
                }
            }
        }
        #endregion

        #region "Misc Class(es)"
        /// <summary>
        /// A class needed for displaying Context Menu Strip items
        /// </summary>
        private class cmsItem
        {
            public cmsItem(string sText, bool isVisible, EventHandler click)
            {
                this.text = sText;
                this.isVisible = isVisible;
                this.click = click;
            }

            private string text;
            public string Text
            {
                set { text = value; }
                get { return text; }
            }

            private bool isVisible;
            public bool isvisible
            {
                set { isVisible = value; }
                get { return isVisible; }
            }

            private EventHandler click;
            public EventHandler Click
            {
                set { click = value; }
                get { return click; }
            }
        }
        #endregion

        #region "ContextMenuStrip"
        /// <summary>
        /// Gathers items to display on the Context Menu Strip for Terminal menus.
        /// </summary>
        /// <param name="item">A reference to a list item variable</param>
        private void AddTerminalItemCMS(ref List<cmsItem> item)
        {
            item.Add(new cmsItem("View Detail", UserPrivilege.IsAllowed(PrivilegeCode.DE), new EventHandler(cmsExpandItem)));
            item.Add(new cmsItem("Add New Terminal", UserPrivilege.IsAllowed(PrivilegeCode.DE, Privilege.Add), new EventHandler(cmsAddNewItem)));
            item.Add(new cmsItem("Delete Current Terminal", UserPrivilege.IsAllowed(PrivilegeCode.DE, Privilege.Delete), new EventHandler(cmsDeleteCurrentItem)));
            item.Add(new cmsItem("Copy Current Terminal", UserPrivilege.IsAllowed(PrivilegeCode.DE, Privilege.Edit), new EventHandler(cmsCopyCurrentItem)));
            item.Add(new cmsItem("Add New Acquirer", UserPrivilege.IsAllowed(PrivilegeCode.AA, Privilege.Add), new EventHandler(cmsAddNewItem)));
            if (isCustomMenu == true)
                item.Add(new cmsItem("Custom Menu", UserPrivilege.IsAllowed(PrivilegeCode.DE), new EventHandler(cmsAddNewItem)));
            item.Add(new cmsItem("View History", UserPrivilege.IsAllowed(PrivilegeCode.DE), new EventHandler(cmsViewHistory)));
            if (isCustomMenu == true && CheckSerialNumberList() == false)
            {
                item.Add(new cmsItem("Add Serial Number", UserPrivilege.IsAllowed(PrivilegeCode.DE), new EventHandler(cmsAddNewItem)));
            }
            //else
            if (isCustomMenu == true && CheckSerialNumberList() == true)
            {
                item.Add(new cmsItem("Change Serial Number", UserPrivilege.IsAllowed(PrivilegeCode.DE), new EventHandler(cmsAddNewItem)));
            }
            //item.Add(new cmsItem("Set Modem Setting", UserPrivilege.IsAllowed(PrivilegeCode.DE, Privilege.Edit), new EventHandler(cmsSetModemSetting)));
            //item.Add(new cmsItem("Update SN", UserPrivilege.IsAllowed(PrivilegeCode.DE, Privilege.Edit), new EventHandler(cmsUpdateSN)));
        }

        /// <summary>
        /// Gathers items to display on the Context Menu Strip for Acquirer menus.
        /// </summary>
        /// <param name="item">A reference to a list item variable</param>
        private void AddAcquirerItemCMS(ref List<cmsItem> item)
        {
            item.Add(new cmsItem("View Detail", UserPrivilege.IsAllowed(PrivilegeCode.AA), new EventHandler(cmsExpandItem)));
            item.Add(new cmsItem("Delete Current Acquirer", UserPrivilege.IsAllowed(PrivilegeCode.AA, Privilege.Delete), new EventHandler(cmsDeleteCurrentItem)));
            item.Add(new cmsItem("Rename Current Acquirer", UserPrivilege.IsAllowed(PrivilegeCode.AA, Privilege.Edit), new EventHandler(cmsRenameCurrentItem)));
            //item.Add(new cmsItem("Move Top", UserPrivilege.IsAllowed(PrivilegeCode.AA, Privilege.Edit), new EventHandler(cmsMoveItemTop)));
            //item.Add(new cmsItem("Move Up", UserPrivilege.IsAllowed(PrivilegeCode.AA, Privilege.Edit), new EventHandler(cmsMoveItemUp)));
            //item.Add(new cmsItem("Move Down", UserPrivilege.IsAllowed(PrivilegeCode.AA, Privilege.Edit), new EventHandler(cmsMoveItemDown)));
            //item.Add(new cmsItem("Move Bottom", UserPrivilege.IsAllowed(PrivilegeCode.AA, Privilege.Edit), new EventHandler(cmsMoveItemBottom)));
            item.Add(new cmsItem("Add New Issuer", UserPrivilege.IsAllowed(PrivilegeCode.AE, Privilege.Add), new EventHandler(cmsAddNewItem)));
        }

        /// <summary>
        /// Gathers items to display on the Context Menu Strip for Issuer menus.
        /// </summary>
        /// <param name="item">A reference to a list item variable</param>
        private void AddIssuerItemCMS(ref List<cmsItem> item)
        {
            item.Add(new cmsItem("View Detail", UserPrivilege.IsAllowed(PrivilegeCode.AE), new EventHandler(cmsExpandItem)));
            item.Add(new cmsItem("Delete Current Issuer", UserPrivilege.IsAllowed(PrivilegeCode.AE, Privilege.Delete), new EventHandler(cmsDeleteCurrentItem)));
            item.Add(new cmsItem("Rename Current Issuer", UserPrivilege.IsAllowed(PrivilegeCode.AE, Privilege.Edit), new EventHandler(cmsRenameCurrentItem)));
            //item.Add(new cmsItem("Move Top", UserPrivilege.IsAllowed(PrivilegeCode.AE, Privilege.Edit), new EventHandler(cmsMoveItemTop)));
            //item.Add(new cmsItem("Move Up", UserPrivilege.IsAllowed(PrivilegeCode.AE, Privilege.Edit), new EventHandler(cmsMoveItemUp)));
            //item.Add(new cmsItem("Move Down", UserPrivilege.IsAllowed(PrivilegeCode.AE, Privilege.Edit), new EventHandler(cmsMoveItemDown)));
            //item.Add(new cmsItem("Move Bottom", UserPrivilege.IsAllowed(PrivilegeCode.AE, Privilege.Edit), new EventHandler(cmsMoveItemBottom)));
            item.Add(new cmsItem("Add New Card", UserPrivilege.IsAllowed(PrivilegeCode.AC, Privilege.Add), new EventHandler(cmsAddNewItem)));
        }

        /// <summary>
        /// Gathers items to display on the Context Menu Strip for Card menus.
        /// </summary>
        /// <param name="item">A reference to a list item variable</param>
        private void AddCardItemCMS(ref List<cmsItem> item)
        {
            item.Add(new cmsItem("Delete Current Card", UserPrivilege.IsAllowed(PrivilegeCode.AC, (Privilege)PrivilegeDigit.Length + 1), new EventHandler(cmsDeleteCurrentItem)));
        }

        /// <summary>
        /// Fills gathered item list to an array.
        /// </summary>
        /// <param name="items">Item list</param>
        /// <returns></returns>
        private ToolStripItem[] LoadItemCollectionCMS(cmsItem[] items)
        {
            List<ToolStripMenuItem> oListItem = new List<ToolStripMenuItem>();
            foreach (cmsItem item in items)
                if (item.isvisible)
                    oListItem.Add(new ToolStripMenuItem(item.Text, null, item.Click));
            return oListItem.ToArray();
        }

        /// <summary>
        /// Displays items in the Context Menu Strip depending on the selected item.
        /// </summary>
        /// <param name="eLevel">To determine what item is currently selected</param>
        /// <param name="oPoint">To determine the location of the mouse pointer</param>
        private void DisplayCMS(CommonLib.NodeLevel eLevel, Point oPoint)
        {
            List<cmsItem> item = new List<cmsItem>();
            contextMenuStrip.Items.Clear();
            switch (eLevel)
            {
                case CommonLib.NodeLevel.Terminal:
                    AddTerminalItemCMS(ref item);
                    break;
                case CommonLib.NodeLevel.Acquirer:
                    AddAcquirerItemCMS(ref item);
                    break;
                case CommonLib.NodeLevel.Issuer:
                    AddIssuerItemCMS(ref item);
                    break;
                case CommonLib.NodeLevel.Card:
                    AddCardItemCMS(ref item);
                    break;
                default:
                    return;
            }
            if (item.Count > 0)
            {
                contextMenuStrip.Items.AddRange(LoadItemCollectionCMS(item.ToArray()));
                contextMenuStrip.Show(oPoint);
            }
        }

        /// <summary>
        /// Opens a new form to display detailed information about the item. User may Edit those datas if they have the privilege.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsViewHistory(object sender, EventArgs e)
        {
            string sFormID = ((dgvTerminal.Focused ? 0 : tvTerminal.SelectedNode.Level) + 1).ToString();
            string sItemName = dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : tvTerminal.SelectedNode.Text;
            string sItemPath = dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : tvTerminal.SelectedNode.FullPath;
            string sItemLevel = (dgvTerminal.Focused ? CommonLib.NodeLevel.Terminal : (CommonLib.NodeLevel)tvTerminal.SelectedNode.Level).ToString();
            string sTID = dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : (tvTerminal.SelectedNode.FullPath.Split('\\'))[0];
            bool isUpdateProfile = false;
            try
            {
                string sUserIdOnAccess = null;
                if (CommonClass.IsAllowUserAccess(oSqlConn, sTID, ref sUserIdOnAccess))
                {
                    FrmViewHistory frmViewHistory = new FrmViewHistory(oSqlConn, oSqlConnAuditTrail, UserData.sUserID, sTID);
                    CommonClass.UserAccessInsert(oSqlConn, sTID);
                    UserData.sTerminalIdActive = sTID;
                    frmViewHistory.ShowDialog();

                    //isUpdateProfile = frmViewHistory.isUpdateProfile;
                    //if (string.IsNullOrEmpty(frmFlexible.sResultTag))
                    //    CommonClass.InputLog(oSqlConnAuditTrail, sTID, UserData.sUserID, sDbName, "View " + sItemLevel + " History" + " : " + sItemPath.Replace("\\", " \\ "), "");
                    //else
                    //    CommonClass.InputLog(oSqlConnAuditTrail, sTID, UserData.sUserID, sDbName, "Restore " + sItemLevel + " History" + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "),
                    //        frmFlexible.sActDtl);
                    CommonClass.UserAccessDelete(oSqlConn, sTID);
                    UserData.sTerminalIdActive = null;

                    if (dgvTerminal.SelectedCells.Count > 0)
                    {
                        //LoadTreeTerminal();
                        //LoadEdcInfo();
                        //LoadEdcAppPackage(null);
                        ClearEdcInfo();
                        ClearInitEdcInfo();
                        LoadTreeTerminalEdcInfoEdcAppPackage();
                    }

                }
                else
                    MessageBox.Show(string.Format("Cannot open, Profile '{0}' is being accessed by '{1}'", sTID, sUserIdOnAccess));
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            UserData.sTerminalIdActive = "";
        }

        /// <summary>
        /// Opens a new form to display detailed information about the item. User may Edit those datas if they have the privilege.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsExpandItem(object sender, EventArgs e)
        {
            //string sFormID = ((lvTerminal.Focused ? 0 : tvTerminal.SelectedNode.Level) + 1).ToString();
            //string sItemName = lvTerminal.Focused ? lvTerminal.FocusedItem.Text : tvTerminal.SelectedNode.Text;
            //string sItemPath = lvTerminal.Focused ? lvTerminal.FocusedItem.Text : tvTerminal.SelectedNode.FullPath;
            //string sItemLevel = (lvTerminal.Focused ? CommonLib.NodeLevel.Terminal : (CommonLib.NodeLevel)tvTerminal.SelectedNode.Level).ToString();
            //string sTID = lvTerminal.Focused ? lvTerminal.FocusedItem.Text : tvTerminal.TopNode.Text;

            string sFormID = ((dgvTerminal.Focused ? 0 : tvTerminal.SelectedNode.Level) + 1).ToString();
            string sItemName = dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : tvTerminal.SelectedNode.Text;
            string sItemPath = dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : tvTerminal.SelectedNode.FullPath;
            string sItemLevel = (dgvTerminal.Focused ? CommonLib.NodeLevel.Terminal : (CommonLib.NodeLevel)tvTerminal.SelectedNode.Level).ToString();
            string sTID = dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : (tvTerminal.SelectedNode.FullPath.Split('\\'))[0];
            bool isUpdateProfile = false;
            try
            {
                string sUserIdOnAccess = null;
                if (CommonClass.IsAllowUserAccess(oSqlConn, sTID, ref sUserIdOnAccess))
                {
                    FrmFlexible frmFlexible = new FrmFlexible(oSqlConn, oSqlConnAuditTrail, true);
                    frmFlexible.sDbID = sDbID;
                    frmFlexible.sDbName = sDbName;
                    frmFlexible.sFormID = sFormID;
                    frmFlexible.sName = sItemName;
                    if (tvTerminal.SelectedNode == null) frmFlexible.sArrCurrentRelations = new string[] { sItemName, null, null, null };
                    else tvTerminal.SelectedNode.FullPath.Split('\\').CopyTo(frmFlexible.sArrCurrentRelations, 0);
                    //frmFlexible.sTerminalID = sTID;
                    CommonClass.UserAccessInsert(oSqlConn, sTID);
                    UserData.sTerminalIdActive = sTID;
                    frmFlexible.ShowDialog();

                    isUpdateProfile = frmFlexible.isUpdateProfile;
                    if (string.IsNullOrEmpty(frmFlexible.sResultTag))
                        //CommonClass.InputLog(oSqlConn, sTID, UserData.sUserID, sDbName, "View " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "), "");
                        CommonClass.InputLog(oSqlConnAuditTrail, sTID, UserData.sUserID, sDbName, "View " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "), "");
                    else
                        //CommonClass.InputLog(oSqlConn, sTID, UserData.sUserID, sDbName, "Edit " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "),
                        //    frmFlexible.sActDtl);
                        CommonClass.InputLog(oSqlConnAuditTrail, sTID, UserData.sUserID, sDbName, "Edit " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "),
                            frmFlexible.sActDtl);
                }
                else
                    MessageBox.Show(string.Format("Cannot open, Profile '{0}' is being accessed by '{1}'", sTID, sUserIdOnAccess));
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //if (isUpdateProfile)
                //    SaveFullContentClass.SaveFullContent(oSqlConn, sTID);
                CommonClass.UserAccessDelete(oSqlConn, sTID);
                UserData.sTerminalIdActive = null;
            }
            UserData.sTerminalIdActive = "";
        }

        /// <summary>
        /// Add an item. Depending on the location of the pointer it could be add a new Terminal, a new Acquirer, a new Issuer, or a new Card.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsAddNewItem(object sender, EventArgs e)
        {
            try
            {
                switch (((ToolStripItem)sender).Text)
                {
                    case "Add New Terminal":
                        AddNewTerminal();
                        break;
                    case "Add New Acquirer":
                        AddNewAcquirer();
                        break;
                    case "Add New Issuer":
                        AddNewIssuer();
                        break;
                    case "Add New Card":
                        AddNewCard();
                        break;
                    case "Custom Menu":
                        LoadAddCustomMenu();
                        break;
                    case "Add Serial Number":
                        cmsAddSerialNumber();
                        break;
                    case "Change Serial Number":
                        cmsUpdateSerialNumber();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Delete the selected item (either a Terminal, an Acquirer, or an Issuer).
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsDeleteCurrentItem(object sender, EventArgs e)
        {
            try
            {
                string sItemID =
                    //lvTerminal.Focused ? lvTerminal.FocusedItem.Text :
                    dgvTerminal.Focused ? dgvTerminal.SelectedCells[0].Value.ToString() :
                    tvTerminal.Focused ? tvTerminal.SelectedNode.Text :
                    "";

                if (sItemID != "" &&
                    MessageBox.Show("Are you sure want to delete " +
                    //(lvTerminal.Focused ? CommonLib.NodeLevel.Terminal.ToString().ToLower() :
                    (dgvTerminal.Focused ? CommonLib.NodeLevel.Terminal.ToString().ToLower() :
                    ((CommonLib.NodeLevel)tvTerminal.SelectedNode.Level).ToString().ToLower())
                    + " : " + sItemID + " ? ",
                    "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    DeleteItem(dgvTerminal.Focused ? CommonLib.NodeLevel.Terminal : (CommonLib.NodeLevel)tvTerminal.SelectedNode.Level);
                //DeleteItem(lvTerminal.Focused ? CommonLib.NodeLevel.Terminal : (CommonLib.NodeLevel)tvTerminal.SelectedNode.Level);
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Rename the selected item (either a Terminal, an Acquirer, or an Issuer).
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsRenameCurrentItem(object sender, EventArgs e)
        {
            tvTerminal.LabelEdit = true;
            tvTerminal.SelectedNode.BeginEdit();
        }

        /// <summary>
        /// Copy the Terminal.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsCopyCurrentItem(object sender, EventArgs e)
        {
            string sNewTID = "";
            try
            {
                string sTerminalID =
                    //lvTerminal.Focused ? lvTerminal.FocusedItem.Text :
                    dgvTerminal.Focused ? dgvTerminal.SelectedCells[0].Value.ToString() :
                    tvTerminal.Focused ? tvTerminal.SelectedNode.Text :
                    "";
                FrmPromptString oFrmPromptString = new FrmPromptString(CommonMessage.sCopyTerminalTitle, CommonMessage.sCopyTerminalText);
                MaskedTextBox oMaskedBox = CommonClass.CreateOMaskedBox(oFrmPromptString.txtInput.Location, oFrmPromptString.txtInput.Size, ">AAAAAAAA");
                oFrmPromptString.Controls.Add(oMaskedBox);
                oFrmPromptString.txtInput.Visible = false;
                if (sTerminalID != "" &&
                    MessageBox.Show("Are you sure want to copy terminal : " + sTerminalID + " ? ", "Caution", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes &&
                    oFrmPromptString.ShowDialog() == DialogResult.OK)
                {
                    if (oMaskedBox.Text.Replace(" ", "").Length == 8)
                    {
                        string sDbNameCheck = sGetDBName(oMaskedBox.Text);
                        string sUserIdOnAccess = null;
                        if (CommonClass.IsAllowUserAccess(oSqlConn, oMaskedBox.Text, ref sUserIdOnAccess))
                            if (sDbNameCheck == "")
                            {
                                CopyTerminal(sTerminalID, sNewTID = oMaskedBox.Text);
                                LoadData(0);
                                //lvTerminal.Items[oMaskedBox.Text].Selected = true;

                                dgvTerminal.FirstDisplayedScrollingRowIndex = odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(sNewTID));
                                dgvTerminal.CurrentCell = dgvTerminal[0, odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(sNewTID))];
                                //dgvTerminal.Rows[odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(sNewTID))].Cells[0].Selected = true;

                                //tvTerminal.Focus();
                                //tvTerminal.SelectedNode = tvTerminal.TopNode;
                                //CommonClass.InputLog(oSqlConn, sNewTID, sUID, sDbName,
                                //    string.Format("Copy terminal {0} as {1}", sTerminalID, sNewTID), "");
                                CommonClass.InputLog(oSqlConnAuditTrail, sNewTID, sUID, sDbName,
                                    string.Format("Copy terminal {0} as {1}", sTerminalID, sNewTID), "");

                                LoadTreeTerminalEdcInfoEdcAppPackage();
                            }
                            else MessageBox.Show("Terminal already exists in " + sDbNameCheck, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                            MessageBox.Show("Cannot COPY terminal, new TerminalId is being accessed by " + sUserIdOnAccess);
                    }
                    else MessageBox.Show("Invalid Terminal ID", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmsAddSerialNumber()
        {
            string sSerialNumber = "";
            try
            {
                string sTerminalID =
                    dgvTerminal.Focused ? dgvTerminal.SelectedCells[0].Value.ToString() :
                    tvTerminal.Focused ? tvTerminal.SelectedNode.Text :
                    "";

                FrmPromptString oFrmPromptString = new FrmPromptString(CommonMessage.sAddSNTitle, CommonMessage.sAddSNText);
                MaskedTextBox oMaskedBox = CommonClass.CreateOMaskedBox(oFrmPromptString.txtInput.Location, oFrmPromptString.txtInput.Size, ">AAAAAAAAAAAAAAAAAAAAAAAA");
                oFrmPromptString.txtInput.MaxLength = (100);
                oFrmPromptString.Controls.Add(oMaskedBox);
                oFrmPromptString.txtInput.Visible = false;
                oFrmPromptString.ShowDialog();
                {
                    if (oMaskedBox.Text.Replace(" ", "").Length == 15 || oMaskedBox.Text.Replace(" ", "").Length == 24)
                    {
                        sSerialNumber = oMaskedBox.Text;
                        string sUserIdOnAccess = null;
                        if (CommonClass.IsAllowUserAccess(oSqlConn, oMaskedBox.Text, ref sUserIdOnAccess))
                            if (CheckExistSerialNumber(sSerialNumber) == false)
                            {
                                InsertNewOrUpdateSerialNumber(sSerialNumber, sTerminalID, "ADD");
                                LoadData(0);
                                dgvTerminal.FirstDisplayedScrollingRowIndex = odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(sTerminalID));
                                dgvTerminal.CurrentCell = dgvTerminal[0, odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(sTerminalID))];
                                CommonClass.InputLog(oSqlConnAuditTrail, sTerminalID, sUID, sSerialNumber,
                                string.Format("ADD SerialNumber {0} as {1}", sTerminalID, sSerialNumber), "");
                                LoadTreeTerminalEdcInfoEdcAppPackage();
                                MessageBox.Show("Add Serial Number Success with SN");
                            }
                            else MessageBox.Show("SerialNumber already exists in " + sTerminalID, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                            MessageBox.Show("Cannot Add SerialNumber terminal, new SerialNumber is being accessed by " + sUserIdOnAccess);
                    }
                    else MessageBox.Show("Invalid SerialNumber, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error");
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmsUpdateSerialNumber()
        {
            string sSerialNumber = "";
            try
            {
                string sTerminalID =
                    dgvTerminal.Focused ? dgvTerminal.SelectedCells[0].Value.ToString() :
                    tvTerminal.Focused ? tvTerminal.SelectedNode.Text :
                    "";
                string sSerialNumberFromDB = sGetSnBytid(sTerminalID, oSqlConn);

                FrmPromptString oFrmPromptString = new FrmPromptString(CommonMessage.sAddSNTitle, CommonMessage.sAddSNText);
                MaskedTextBox oMaskedBox = CommonClass.CreateOMaskedBox(oFrmPromptString.txtInput.Location, oFrmPromptString.txtInput.Size, ">AAAAAAAAAAAAAAAAAAAAAAAA");
                oFrmPromptString.txtInput.MaxLength = (100);
                oFrmPromptString.Controls.Add(oMaskedBox);
                oFrmPromptString.txtInput.Visible = false;
                if (sSerialNumberFromDB != null)
                {
                    oMaskedBox.Text = sSerialNumberFromDB;
                }
                oFrmPromptString.ShowDialog();

                {
                    if (oMaskedBox.Text != null)
                    {
                        if (oMaskedBox.Text.Replace(" ", "").Length == 15 || oMaskedBox.Text.Replace(" ", "").Length == 24)
                        {
                            sSerialNumber = oMaskedBox.Text;
                            string sUserIdOnAccess = null;


                            if (CommonClass.IsAllowUserAccess(oSqlConn, oMaskedBox.Text, ref sUserIdOnAccess))

                                if (CheckExistSerialNumber(sSerialNumber) == false)
                                {
                                    InsertNewOrUpdateSerialNumber(sSerialNumber, sTerminalID, "UPDATE");
                                    LoadData(0);
                                    dgvTerminal.FirstDisplayedScrollingRowIndex = odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(sTerminalID));
                                    dgvTerminal.CurrentCell = dgvTerminal[0, odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(sTerminalID))];
                                    CommonClass.InputLog(oSqlConnAuditTrail, sTerminalID, sUID, sSerialNumber,
                                    string.Format("Update SerialNumber {0} as {1}", sTerminalID, sSerialNumber), "");
                                    LoadTreeTerminalEdcInfoEdcAppPackage();
                                    MessageBox.Show("Update Serial Number Success");
                                }
                                else MessageBox.Show("SerialNumber already exists in " + sTerminalID, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                            else
                                MessageBox.Show("Cannot Update SerialNumber terminal, new SerialNumber is being accessed by " + sUserIdOnAccess);
                        }



                        else
                        {
                            MessageBox.Show("Invalid SerialNumber, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error");
                        }


                    }

                    else
                    {
                    }

                }
            }


            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static string sGetSnBytid(string sTerminalID, SqlConnection oSqlConn)
        {

            string sSn = null;
            SqlCommand oSqlCmd = new SqlCommand("spGetSerialNumber", oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;

            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }

            SqlDataReader oRead = oSqlCmd.ExecuteReader();
            if (oRead.Read())
            {
                // sDatabaseName = oRead["DatabaseName"].ToString();
                //bvalue = true;
                sSn = oRead["SerialNumber"].ToString();
            }
            oRead.Close();
            oRead.Dispose();
            oSqlCmd.Dispose();
            //}
            //    catch (Exception ex)
            //    {
            //        CommonClass.doWriteErrorFile(ex.Message);
            //    }
            //}
            return sSn;
        }


        private void InsertNewOrUpdateSerialNumber(string sSerialNumber, string sTerminalID, string sFlag)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPInserOrUpdateSerialNumberAdd, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = sSerialNumber;
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oSqlCmd.Parameters.Add("@sFlag", SqlDbType.VarChar).Value = sFlag;

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                oSqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }











        /// <summary>
        /// Reserved for future use (to set Modem Setting)
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsSetModemSetting(object sender, EventArgs e)
        {
            //MessageBox.Show(tvTerminal.SelectedNode.Text + " ( " + ((CommonLib.NodeLevel)(tvTerminal.SelectedNode.Level)).ToString() + " ) ", "Setting");
        }

        /// <summary>
        /// Reserved for future use (to Update SN)
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>


        /// <summary>
        /// Reserved for future use (to move an item to the top)
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsMoveItemTop(object sender, EventArgs e)
        {
            try
            {
                string sItemName = tvTerminal.SelectedNode.Text;
                int iItemLevel = tvTerminal.SelectedNode.Level == 1 ? 2 : 1;
                string sTID = (tvTerminal.SelectedNode.FullPath.Split('\\'))[0];
                if (sItemName != "")
                {
                    if (odtRelation != null)
                    {
                        DataTable dtRelationNew = odtRelation.Clone();
                        List<Relations> ltRelationsNew = new List<Relations>();
                        ltRelationsNew = ltRelations;
                        int iIndex = 0;
                        while (iIndex >= 0)
                        {
                            iIndex = ltRelationsNew.FindIndex(iIndex, delegate (Relations tempRelation)
                            {
                                //return tempRelation.sGetValue(iItemLevel).Equals(sItemName);
                                if (iItemLevel == 2) return tempRelation.sAcquirerName.Equals(sItemName);
                                else return tempRelation.sIssuerName.Equals(sItemName);
                            });
                            if (iIndex >= 0)
                            {
                                Relations tempRelation = ltRelationsNew[iIndex];
                                ltRelationsNew.RemoveAt(iIndex);
                                ltRelationsNew.Insert(0, tempRelation);
                                iIndex++;
                            }
                            else
                                break;
                        }
                        CreateDataTableRelation(sTID, ltRelationsNew, ref dtRelationNew);
                        SaveRelation(sTID, dtRelationNew);
                        LoadTreeTerminal();
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Reserved for future use (to move an item one step upper)
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsMoveItemUp(object sender, EventArgs e)
        {
            try
            {
                //doMoveRelation(1);

                string sItemName = tvTerminal.SelectedNode.Text;
                int iItemLevel = tvTerminal.SelectedNode.Level == 1 ? 2 : 1;
                string sTID = (tvTerminal.SelectedNode.FullPath.Split('\\'))[0];
                if (sItemName != "")
                {
                    if (odtRelation != null)
                    {
                        DataTable dtRelationNew = odtRelation.Clone();
                        List<Relations> ltRelationsNew = new List<Relations>();
                        ltRelationsNew = ltRelations;
                        int iIndex = ltRelationsNew.FindIndex(delegate (Relations tempRelation)
                        {
                            //return tempRelation.sGetValue(iItemLevel).Equals(sItemName);
                            if (iItemLevel == 2) return tempRelation.sAcquirerName.Equals(sItemName);
                            else return tempRelation.sIssuerName.Equals(sItemName);
                        });
                        if (iIndex > 0)
                        {
                            Relations tempRelationUp = ltRelationsNew[iIndex];
                            ltRelationsNew.RemoveAt(iIndex);
                            int iIndexUp = iIndex - 1;
                            while (iIndexUp >= 0 && iIndexUp < iIndex)
                            {
                                if (iItemLevel == 2)
                                {
                                    //if (ltRelationsNew[iIndexUp].sGetValue(iItemLevel) != tempRelationUp.sGetValue(iItemLevel)) break;
                                    if (ltRelationsNew[iIndexUp].sAcquirerName != tempRelationUp.sAcquirerName) break;
                                }
                                else
                                {
                                    //if (ltRelationsNew[iIndexUp].sGetValue(2) == tempRelationUp.sGetValue(2) &&
                                    //    ltRelationsNew[iIndexUp].sGetValue(iItemLevel) != tempRelationUp.sGetValue(iItemLevel))
                                    //    break;
                                    if (ltRelationsNew[iIndexUp].sAcquirerName == tempRelationUp.sAcquirerName &&
                                        ltRelationsNew[iIndexUp].sIssuerName != tempRelationUp.sIssuerName)
                                        break;
                                }
                                iIndexUp--;
                            }
                            ltRelationsNew.Insert(iIndexUp == -1 ? iIndex : iIndexUp, tempRelationUp);
                        }
                        CreateDataTableRelation(sTID, ltRelationsNew, ref dtRelationNew);
                        SaveRelation(sTID, dtRelationNew);
                        LoadTreeTerminal();
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Reserved for future use (to move an item one step under)
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsMoveItemDown(object sender, EventArgs e)
        {
            try
            {
                //doMoveRelation(-1);

                string sItemName = tvTerminal.SelectedNode.Text;
                int iItemLevel = tvTerminal.SelectedNode.Level == 1 ? 2 : 1;
                string sTID = (tvTerminal.SelectedNode.FullPath.Split('\\'))[0];
                if (sItemName != "")
                {
                    if (odtRelation != null)
                    {
                        DataTable dtRelationNew = odtRelation.Clone();
                        List<Relations> ltRelationsNew = new List<Relations>();
                        ltRelationsNew = ltRelations;
                        int iIndex = ltRelationsNew.FindIndex(delegate (Relations tempRelation)
                        {
                            //return tempRelation.sGetValue(iItemLevel).Equals(sItemName);
                            if (iItemLevel == 2) return tempRelation.sAcquirerName.Equals(sItemName);
                            else return tempRelation.sIssuerName.Equals(sItemName);
                        });
                        if (iIndex >= 0)
                        {
                            Relations tempRelation = ltRelationsNew[iIndex];
                            //ltRelationsNew.RemoveAt(iIndex);
                            int iIndexDown = iIndex + 1;
                            while (iIndexDown > iIndex && iIndexDown < ltRelationsNew.Count)
                            {
                                if (iItemLevel == 2)
                                {
                                    //if (ltRelationsNew[iIndexDown].sGetValue(iItemLevel) != tempRelation.sGetValue(iItemLevel))
                                    //    break;
                                    if (ltRelationsNew[iIndexDown].sAcquirerName != tempRelation.sAcquirerName) break;
                                }
                                else
                                {
                                    //if (ltRelationsNew[iIndexDown].sGetValue(2) == tempRelation.sGetValue(2) &&
                                    //    ltRelationsNew[iIndexDown].sGetValue(iItemLevel) != tempRelation.sGetValue(iItemLevel))
                                    //    break;
                                    if (ltRelationsNew[iIndexDown].sAcquirerName == tempRelation.sAcquirerName &&
                                        ltRelationsNew[iIndexDown].sIssuerName != tempRelation.sIssuerName)
                                        break;
                                }
                                iIndexDown++;
                            }
                            //ltRelationsNew.Insert(iIndexDown, tempRelationDown);
                            if (iIndexDown > iIndex)
                                iIndexDown = iIndex;
                            Relations tempRelationUp = ltRelationsNew[iIndexDown];
                            ltRelationsNew.RemoveAt(iIndexDown);
                            ltRelationsNew.Insert(iIndex, tempRelationUp);
                        }
                        CreateDataTableRelation(sTID, ltRelationsNew, ref dtRelationNew);
                        SaveRelation(sTID, dtRelationNew);
                        LoadTreeTerminal();
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Reserved for future use (to move an item to the bottom)
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void cmsMoveItemBottom(object sender, EventArgs e)
        {
            try
            {
                string sItemName = tvTerminal.SelectedNode.Text;
                int iItemLevel = tvTerminal.SelectedNode.Level == 1 ? 2 : 1;
                string sTID = (tvTerminal.SelectedNode.FullPath.Split('\\'))[0];
                if (sItemName != "")
                {
                    if (odtRelation != null)
                    {
                        DataTable dtRelationNew = odtRelation.Clone();
                        List<Relations> ltRelationsNew = new List<Relations>();
                        ltRelationsNew = ltRelations;
                        int iIndex = 0;
                        while (iIndex >= 0)
                        {
                            iIndex = ltRelationsNew.FindIndex(iIndex, delegate (Relations tempRelation)
                           {
                               //return tempRelation.sGetValue(iItemLevel).Equals(sItemName);
                               if (iItemLevel == 2) return tempRelation.sAcquirerName.Equals(sItemName);
                               else return tempRelation.sIssuerName.Equals(sItemName);
                           });
                            if (iIndex >= 0)
                            {
                                Relations tempRelation = ltRelationsNew[iIndex];
                                ltRelationsNew.RemoveAt(iIndex);
                                ltRelationsNew.Add(tempRelation);
                                iIndex++;
                            }
                            else
                                break;
                        }
                        CreateDataTableRelation(sTID, ltRelationsNew, ref dtRelationNew);
                        SaveRelation(sTID, dtRelationNew);
                        LoadTreeTerminal();
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void doMoveRelation(int iIncreament)
        {
            string sItemName = tvTerminal.SelectedNode.Text;
            int iItemLevel = tvTerminal.SelectedNode.Level == 1 ? 2 : 1;
            string sTID = (tvTerminal.SelectedNode.FullPath.Split('\\'))[0];
            if (sItemName != "")
            {
                if (odtRelation != null)
                {
                    DataTable dtRelationNew = odtRelation.Clone();
                    List<Relations> ltRelationsNew = new List<Relations>();
                    ltRelationsNew = ltRelations;
                    int iIndex = ltRelationsNew.FindIndex(delegate (Relations tempRelation)
                    {
                        //return tempRelation.sGetValue(iItemLevel).Equals(sItemName);
                        if (iItemLevel == 2) return tempRelation.sAcquirerName.Equals(sItemName);
                        else return tempRelation.sIssuerName.Equals(sItemName);
                    });
                    if (iIndex > 0)
                    {
                        Relations tempRelationMove = ltRelationsNew[iIndex];

                        List<SortRelation> ltSorted;
                        if (iItemLevel == 2)
                            ltSorted = ltSortAcquirer(ltRelations, iItemLevel);
                        else
                            //ltSorted = ltSortIssuer(iItemLevel, tempRelationMove.sGetValue(2), ltRelations);
                            ltSorted = ltSortIssuer(iItemLevel, tempRelationMove.sAcquirerName, ltRelations);

                        if (iIncreament != -1)
                            ltRelationsNew.RemoveAt(iIndex);

                        int iIndexSort = ltSorted.FindIndex(delegate (SortRelation tempSort)
                        {
                            //return tempSort.sGetItemName().Equals(tempRelationMove.sGetValue(iItemLevel));
                            if (iItemLevel == 2) return tempSort.sGetItemName().Equals(tempRelationMove.sAcquirerName.Equals(sItemName));
                            else return tempSort.sGetItemName().Equals(tempRelationMove.sIssuerName.Equals(sItemName));
                        });
                        if (iIncreament == -1)
                            tempRelationMove = oGetRelation(iIncreament, iIndexSort, ltRelations, ltSorted);

                        //ltRelationsNew.Insert(ltSorted[iIndexSort-iIncreament].sGetIndex(), tempRelationUp);
                        ltRelationsNew.Insert(ltSorted[iIndexSort + iIncreament].sGetIndex(), tempRelationMove);
                    }
                    CreateDataTableRelation(sTID, ltRelationsNew, ref dtRelationNew);
                    SaveRelation(sTID, dtRelationNew);
                    LoadTreeTerminal();
                }
            }
        }

        private Relations oGetRelation(int iIncreament, int iCurrIndex,
                                    List<Relations> ltRelation, List<SortRelation> oSort)
        {
            Relations oRelation = new Relations();
            switch (iIncreament)
            {
                case 1:
                    oRelation = ltRelation[oSort[iCurrIndex].sGetIndex()];
                    break;
                case -1:
                    oRelation = ltRelation[oSort[iCurrIndex + 1].sGetIndex()];
                    break;
                default: break;
            }
            return oRelation;
        }

        private List<SortRelation> ltSortAcquirer(List<Relations> ltCurrRelation, int iItemLevel)
        {
            List<SortRelation> ltSort = new List<SortRelation>();
            SortRelation oSort = new SortRelation();
            //int iIndexSort = -1;
            //string sItemName = "";

            foreach (Relations oRelation in ltCurrRelation)
            {
                oSort = oSortRelation(ltSort, ltCurrRelation, oRelation, iItemLevel);
                if (oSort != null)
                    ltSort.Add(oSort);

                //    sItemName = oRelation.sGetValue(iItemLevel);

                //    iIndexSort = ltSort.FindIndex(delegate(SortRelation tempSort)
                //    {
                //        return tempSort.sGetItemName().Equals(sItemName);
                //    });
                //    if (iIndexSort < 0)
                //    {
                //        oSort = new SortRelation();
                //        oSort.doFillValue(ltCurrRelation.IndexOf(oRelation), sItemName);
                //        ltSort.Add(oSort);
                //    }
            }
            return ltSort;
        }

        private List<SortRelation> ltSortIssuer(int iItemLevel, string sAcqName, List<Relations> ltCurrRelation)
        {
            List<SortRelation> ltSort = new List<SortRelation>();
            SortRelation oSort = new SortRelation();
            //int iIndexSort = -1;

            //string sItemName = "";

            foreach (Relations oRelation in ltCurrRelation)
            {
                if (oRelation.sAcquirerName == sAcqName)
                {
                    oSort = oSortRelation(ltSort, ltCurrRelation, oRelation, iItemLevel);
                    if (oSort != null)
                        ltSort.Add(oSort);
                }
            }
            return ltSort;
        }

        private SortRelation oSortRelation(List<SortRelation> ltSort, List<Relations> ltCurrRelation,
                                            Relations oRelation, int iItemLevel)
        {
            SortRelation oSort;
            //string sItemName = oRelation.sGetValue(iItemLevel);
            string sItemName = iItemLevel == 2 ? oRelation.sAcquirerName : iItemLevel == 1 ? oRelation.sIssuerName : oRelation.sCardName;
            int iIndexSort = ltSort.FindIndex(delegate (SortRelation tempSort)
            {
                return tempSort.sGetItemName().Equals(sItemName);
            });

            if (iIndexSort < 0)
            {
                oSort = new SortRelation();
                oSort.doFillValue(ltCurrRelation.IndexOf(oRelation), sItemName);
            }
            else oSort = null;

            return oSort;
        }
        #endregion

        #region "DB Access"
        /// <summary>
        /// Get Database's Name for given Terminal ID.
        /// </summary>
        /// <param name="sTID">string : Terminal ID</param>
        /// <returns>string : Database's Name</returns>
        private string sGetDBName(string sCardName)
        {
            string sDatabaseName = "";
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPViewTerminalListBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sCardName;

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }

                SqlDataReader oRead = oSqlCmd.ExecuteReader();
                if (oRead.Read())
                    sDatabaseName = oRead["DatabaseName"].ToString();

                oRead.Close();
                oRead.Dispose();
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return sDatabaseName;
        }

        private bool CheckExistSerialNumber(string sSerialNumber)
        {
            bool bvalue = false;
            string sDatabaseName = "";
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPViewSerialNumberListBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = sSerialNumber;
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = "";
                oSqlCmd.Parameters.Add("@sFlag", SqlDbType.VarChar).Value = "SN";

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }

                SqlDataReader oRead = oSqlCmd.ExecuteReader();
                if (oRead.Read())
                {
                    // sDatabaseName = oRead["DatabaseName"].ToString();
                    bvalue = true;
                }
                oRead.Close();
                oRead.Dispose();
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return bvalue;
        }

        private bool CheckSerialNumberList()
        {
            string sTerminalID =
                    dgvTerminal.Focused ? dgvTerminal.SelectedCells[0].Value.ToString() :
                    tvTerminal.Focused ? tvTerminal.SelectedNode.Text :
                    "";

            bool bvalue = false;
            try
            {
                DataTable dt = new DataTable();

                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPViewSerialNumberListBrowse, oSqlConn);

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = "";
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oSqlCmd.Parameters.Add("@sFlag", SqlDbType.VarChar).Value = "SN";

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                SqlDataReader oRead = oSqlCmd.ExecuteReader();
                if (oRead.Read())
                {
                    bvalue = true;
                    string sn = oRead["SerialNumber"].ToString();
                }
                oRead.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return bvalue;
        }
        /// <summary>
        /// Copy Terminal with a different TerminalID.
        /// </summary>
        /// <param name="sOldTerminalID">string : Source TerminalID</param>
        /// <param name="sNewTerminalID">string : New TerminalID</param>
        private void CopyTerminal(string sOldTerminalID, string sNewTerminalID)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPProfileCopy, oSqlConn);
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            //oSqlCommand.Parameters.AddRange(oSqlParameter);
            oSqlCommand.Parameters.Add("@iDatabaseId", SqlDbType.Int).Value = Convert.ToInt32(sDbID);
            oSqlCommand.Parameters.Add("@sOldTID", SqlDbType.VarChar).Value = sOldTerminalID;
            oSqlCommand.Parameters.Add("@sNewTID", SqlDbType.VarChar).Value = sNewTerminalID;
            oSqlCommand.ExecuteNonQuery();
            //CommonClass.InputLog(oSqlConn, sNewTerminalID, UserData.sUserID, sDbName, "Copy Terminal : " + sOldTerminalID + " --> " + sNewTerminalID, "");
            CommonClass.InputLog(oSqlConnAuditTrail, sNewTerminalID, UserData.sUserID, sDbName, "Copy Terminal : " + sOldTerminalID + " --> " + sNewTerminalID, "");
        }

        private void CopyTerminalNew(string sOldTerminalID, string sNewTerminalID)
        {


            //if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            //SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPProfileCopy, oSqlConn);
            //oSqlCommand.CommandType = CommandType.StoredProcedure;
            //oSqlCommand.Parameters.AddRange(oSqlParameter);
            //oSqlCommand.ExecuteNonQuery();
            //CommonClass.InputLog(oSqlConn, sNewTerminalID, UserData.sUserID, sDbName, "Copy Terminal : " + sOldTerminalID + " --> " + sNewTerminalID, "");
        }

        /// <summary>
        /// Delete current node from List and from database.
        /// </summary>
        /// <param name="eNodeLevel">NodeLevel : Terminal, Acquirer, Issuer, or Card</param>
        private void DeleteItem(CommonLib.NodeLevel eNodeLevel)
        {
            int iltRelationIndex = 0;
            string sRelation = "", sLogDesc = "";

            SqlCommand oSqlCommand = new SqlCommand();
            SqlParameter oParamTID = new SqlParameter("@sTerminalID", SqlDbType.VarChar, 8);

            //string sTerminalId = lvTerminal.Focused ? lvTerminal.FocusedItem.Text : tvTerminal.TopNode.Text;
            //string sTerminalId = dgvTerminal.Focused ? dgvTerminal.SelectedRows[0].ToString() : tvTerminal.TopNode.Text;
            string sTerminalId = dgvTerminal.Focused ? dgvTerminal.SelectedCells[0].Value.ToString() : tvTerminal.Nodes[0].Text;

            string sUserIdOnAccess = null;

            if (CommonClass.IsAllowUserAccess(oSqlConn, sTerminalId, ref sUserIdOnAccess))
            {
                CommonClass.UserAccessInsert(oSqlConn, sTerminalId);

                oParamTID.Direction = ParameterDirection.Input;
                oParamTID.Value = sTerminalId;

                SqlTransaction oSqlTrans = oSqlConn.BeginTransaction();
                switch (eNodeLevel)
                {
                    case CommonLib.NodeLevel.Terminal:
                        //oSqlCommand.CommandText = CommonSP.sSPProfileDelete;
                        oSqlCommand = new SqlCommand(CommonSP.sSPProfileDelete, oSqlConn, oSqlTrans);
                        sLogDesc = "Delete Terminal : " + oParamTID.Value.ToString();
                        break;
                    case CommonLib.NodeLevel.Acquirer:
                        #region "Acquirer"
                        iltRelationIndex = 0;

                        while (iltRelationIndex != -1)
                        {
                            iltRelationIndex = iGetRelationIndex(2, tvTerminal.SelectedNode.Text);

                            if (iltRelationIndex != -1)
                                ltRelations.RemoveAt(iltRelationIndex);
                        }

                        sRelation = sCreateRelation();

                        TreeNode node = tvTerminal.SelectedNode.FirstNode;
                        string sIssuerList = "";
                        if (node != null)
                        {
                            sIssuerList += node.Text.Length.ToString("00") + node.Text;
                            while ((node = node.NextNode) != null)
                                sIssuerList += node.Text.Length.ToString("00") + node.Text;
                        }

                        //oSqlCommand.CommandText = CommonSP.sSPAcqDeleteByTID;
                        oSqlCommand = new SqlCommand(CommonSP.sSPAcqDeleteByTID, oSqlConn, oSqlTrans);

                        SqlParameter oParamAcq = new SqlParameter("@sAcquirer", SqlDbType.VarChar, 15);
                        oParamAcq.Direction = ParameterDirection.Input;
                        oParamAcq.Value = tvTerminal.SelectedNode.Text;
                        oSqlCommand.Parameters.Add(oParamAcq);

                        SqlParameter oParamIssuerList = new SqlParameter("@sIssuerList", SqlDbType.VarChar, sIssuerList.Length);
                        oParamIssuerList.Direction = ParameterDirection.Input;
                        oParamIssuerList.Value = sIssuerList;
                        oSqlCommand.Parameters.Add(oParamIssuerList);

                        sLogDesc = "Delete Acquirer : " + tvTerminal.SelectedNode.FullPath;
                        #endregion
                        break;
                    case CommonLib.NodeLevel.Issuer:
                    case CommonLib.NodeLevel.Card:
                        // Delete Issuer / Card :
                        iltRelationIndex = 0;
                        if (eNodeLevel == CommonLib.NodeLevel.Issuer)
                        {
                            //oSqlCommand.CommandText = CommonSP.sSPIssDeleteByTID;
                            oSqlCommand = new SqlCommand(CommonSP.sSPIssDeleteByTID, oSqlConn, oSqlTrans);

                            SqlParameter oParamIss = new SqlParameter("@sIssuer", SqlDbType.VarChar, 15);
                            oParamIss.Direction = ParameterDirection.Input;
                            oParamIss.Value = tvTerminal.SelectedNode.Text;
                            oSqlCommand.Parameters.Add(oParamIss);
                            sLogDesc = "Delete Issuer : " + tvTerminal.SelectedNode.FullPath;

                            iltRelationIndex = iGetRelationIndex(1, tvTerminal.SelectedNode.Text);
                            do
                            {
                                List<Relations> ltTemp = ltRelations.FindAll(temp => temp.sIssuerName == tvTerminal.SelectedNode.Text);
                                if (ltTemp.Count > 1)
                                    ltRelations.RemoveAt(iltRelationIndex);
                                else
                                {
                                    ltRelations[iltRelationIndex].sCardName = "";
                                    ltRelations[iltRelationIndex].sIssuerName = "";
                                }
                                iltRelationIndex = iGetRelationIndex(1, tvTerminal.SelectedNode.Text);
                            } while (iltRelationIndex != -1);
                        }
                        else
                        {
                            //oSqlCommand.CommandText = CommonSP.sSPCardDeleteByTID;
                            oSqlCommand = new SqlCommand(CommonSP.sSPCardDeleteByTID, oSqlConn, oSqlTrans);

                            sLogDesc = "Delete Card : " + tvTerminal.SelectedNode.FullPath;
                            iltRelationIndex = iGetRelationIndex(0, tvTerminal.SelectedNode.Text);
                            ltRelations[iltRelationIndex].sCardName = "";
                        }

                        sRelation = sCreateRelation();
                        break;
                    default:
                        return;
                }

                if (eNodeLevel != CommonLib.NodeLevel.Terminal) oSqlCommand.Parameters.Add("@sRelation", SqlDbType.VarChar, sRelation.Length).Value = sRelation;
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                oSqlCommand.Parameters.Add(oParamTID);
                oSqlCommand.Connection = oSqlConn;
                oSqlCommand.CommandTimeout = 60;
                try
                {
                    oSqlCommand.ExecuteNonQuery();
                    oSqlTrans.Commit();
                }
                catch (Exception ex)
                {
                    oSqlTrans.Rollback();
                    MessageBox.Show("DELETE FAILED. " + ex.Message);
                }

                //if (eNodeLevel == CommonLib.NodeLevel.Terminal)
                //    CommonClass.SyncToEms(oSqlConn, sTerminalId, EmsTypeCommand.D);
                //else
                //    CommonClass.SyncToEms(oSqlConn, sTerminalId, EmsTypeCommand.A);
                try
                {
                    if (CommonLib.NodeLevel.Terminal == eNodeLevel)
                    {
                        oSqlCommand = new SqlCommand(CommonSP.sSPRemoveTIDGroupRegionCity, oSqlConn);
                        {
                            oSqlCommand.CommandType = CommandType.StoredProcedure;
                            oSqlCommand.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                            oSqlCommand.Parameters.Add("@sCategory", SqlDbType.VarChar).Value = (string)sTerminalId;

                            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                            oSqlCommand.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                //CommonClass.InputLog(oSqlConn, oParamTID.Value.ToString(), UserData.sUserID, sDbName, sLogDesc.Replace("\\", " \\ "), "");
                CommonClass.InputLog(oSqlConnAuditTrail, oParamTID.Value.ToString(), UserData.sUserID, sDbName, sLogDesc.Replace("\\", " \\ "), "");
                CommonClass.UserAccessDelete(oSqlConn, sTerminalId);

                //proses delete List TID Remote Download automatic from Delete Terminal
                if (CommonLib.NodeLevel.Terminal == eNodeLevel)
                {
                    try
                    {
                        oSqlCommand = new SqlCommand(string.Format("DELETE FROM tbListTIDRemoteDownload WHERE TerminalID = '{0}'", sTerminalId), oSqlConn);
                        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                        oSqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    { }

                }


                if (eNodeLevel == CommonLib.NodeLevel.Terminal)
                {
                    InitData();
                    LoadData(0);
                }
                else
                {
                    //if (lvTerminal.Focused) tvTerminal.Nodes.Clear();
                    if (dgvTerminal.Focused) tvTerminal.Nodes.Clear();
                    else tvTerminal.SelectedNode.Remove();
                    //if (tvTerminal.Nodes.Count == 0) lvTagDetail.Items.Clear();
                    if (tvTerminal.Nodes.Count == 0) dgvTagDetail.DataSource = null;
                    if (isLoadTerminalList(0))
                    {
                        //lvTerminal.Items.Clear();
                        dgvTerminal.DataSource = null;
                        FillListTerminal();
                    }
                }

                //if (lvTerminal.SelectedItems.Count <= 0 && tvTerminal.Nodes.Count > 0)
                //    lvTerminal.Items[tvTerminal.TopNode.Text].Selected = true;
                if (dgvTerminal.SelectedRows.Count <= 0 && tvTerminal.Nodes.Count > 0)
                    //dgvTerminal.Rows[odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(tvTerminal.TopNode.Text))].Selected = true;
                    dgvTerminal.Rows[odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(tvTerminal.Nodes[0].Text))].Selected = true;
            }
            else
                MessageBox.Show("Cannot DELETE terminal, TerminalId is being accessed by " + sUserIdOnAccess);

        }

        /// <summary>
        /// Get index from List View for selected Type (Acq, Iss, or Card) and Selected node
        /// </summary>
        /// <param name="iArrayIndex"></param>
        /// <param name="sNodeText"></param>
        /// <returns></returns>
        protected int iGetRelationIndex(int iArrayIndex, string sNodeText)
        {
            return ltRelations.FindIndex(delegate (Relations tempRelation)
                {
                    //return tempRelation.sGetValue(iArrayIndex).Equals(sNodeText, StringComparison.Ordinal);
                    return iArrayIndex == 0 ? tempRelation.sCardName.Equals(sNodeText, StringComparison.Ordinal) :
                        iArrayIndex == 1 ? tempRelation.sIssuerName.Equals(sNodeText, StringComparison.Ordinal) :
                        tempRelation.sAcquirerName.Equals(sNodeText, StringComparison.Ordinal);
                });
        }

        /// <summary>
        /// Create long string contain Relations for each Acquirer, Issuer, and Card in selected Terminal ID
        /// </summary>
        /// <returns>String : Relation String in TLV format</returns>
        protected string sCreateRelation()
        {
            List<Relations> relationSorted = new List<Relations>();
            foreach (string sTemp in arrsSortedAcquirer)
            {
                List<Relations> tempSorted = ltRelations.FindAll(temp => temp.sAcquirerName == sTemp);
                if (tempSorted.Count > 0)
                    relationSorted.AddRange(tempSorted);
            }

            string sTempRelation = "";
            ClearDuplicateEmptyRelation(ref relationSorted);
            if (GetTagLength() == 5)
            {
                foreach (Relations tempRelation in relationSorted)
                {
                    string sCard = tempRelation.sCardName;
                    string sIssuer = tempRelation.sIssuerName;
                    string sAcquirer = tempRelation.sAcquirerName;
                    sTempRelation += string.Format("AD001{0:00}{1}AD002{2:00}{3}AD003{4:00}{5}",
                         sCard.Length, sCard,
                         sIssuer.Length, sIssuer,
                         sAcquirer.Length, sAcquirer);
                }
            }
            else
            {
                foreach (Relations tempRelation in relationSorted)
                {
                    string sCard = tempRelation.sCardName;
                    string sIssuer = tempRelation.sIssuerName;
                    string sAcquirer = tempRelation.sAcquirerName;
                    sTempRelation += string.Format("AD01{0:00}{1}AD02{2:00}{3}AD03{4:00}{5}",
                         sCard.Length, sCard,
                         sIssuer.Length, sIssuer,
                         sAcquirer.Length, sAcquirer);
                }
            }
            return sTempRelation;
            //MessageBox.Show(sTempRelation);
        }

        private void ClearDuplicateEmptyRelation(ref List<Relations> relationSorted)
        {
            List<Relations> tempSorted = new List<Relations>();

            tempSorted = relationSorted.FindAll(temp => temp.sCardName == "" || temp.sCardName == null);
            int iTotalEmpty = 1;
            foreach (Relations temp in tempSorted)
            {
                iTotalEmpty = relationSorted.FindAll(
                    tempRel => tempRel.sAcquirerName == temp.sAcquirerName
                        && tempRel.sIssuerName == temp.sIssuerName
                        && (tempRel.sCardName == "" || tempRel.sCardName == null)).Count;
                while (iTotalEmpty > 1)
                {
                    int iIndexEmpty = relationSorted.FindIndex(tempRel => tempRel.sAcquirerName == temp.sAcquirerName
                        && tempRel.sIssuerName == temp.sIssuerName
                        && (tempRel.sCardName == "" || tempRel.sCardName == null));
                    relationSorted.RemoveAt(iIndexEmpty);
                    iTotalEmpty -= 1;
                }
            }
        }

        /// <summary>
        /// Determines if Item is renamed or not.
        /// </summary>
        /// <param name="eNodeLevel">NodeLevel : Terminal, Acquirer, Issuer, or Card</param>
        /// <returns>boolean : true if renamed, else false</returns>
        private bool isRenameItem(CommonLib.NodeLevel eNodeLevel)
        {
            string sLogDesc = "";
            SqlCommand oSqlCommand = new SqlCommand();

            switch (eNodeLevel)
            {
                case CommonLib.NodeLevel.Acquirer:
                    oSqlCommand.CommandText = CommonSP.sSPAcqRename;
                    sLogDesc = "Rename Acquirer : " + tvTerminal.SelectedNode.FullPath + " --> " + sNewRenameKey;
                    break;
                case CommonLib.NodeLevel.Issuer:
                    oSqlCommand.CommandText = CommonSP.sSPIssRename;
                    sLogDesc = "Rename Issuer : " + tvTerminal.SelectedNode.FullPath + " --> " + sNewRenameKey;
                    break;
                default:
                    return false;
            }

            SqlParameter[] oSqlParameter = new SqlParameter[3];

            oSqlParameter[0] = new SqlParameter("@sTerminalID", SqlDbType.VarChar, 8);
            oSqlParameter[0].Direction = ParameterDirection.Input;
            oSqlParameter[0].Value = tvTerminal.TopNode.Text;

            oSqlParameter[1] = new SqlParameter("@sOldKey", SqlDbType.VarChar, 15);
            oSqlParameter[1].Direction = ParameterDirection.Input;
            oSqlParameter[1].Value = sOldRenameKey;

            oSqlParameter[2] = new SqlParameter("@sNewKey", SqlDbType.VarChar, 15);
            oSqlParameter[2].Direction = ParameterDirection.Input;
            oSqlParameter[2].Value = sNewRenameKey;

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            oSqlCommand.Parameters.AddRange(oSqlParameter);
            oSqlCommand.Connection = oSqlConn;

            if (oSqlCommand.ExecuteNonQuery() <= 1)
            {
                MessageBox.Show(eNodeLevel.ToString() + " already exists in terminal " + tvTerminal.TopNode.Text, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            //CommonClass.InputLog(oSqlConn, tvTerminal.TopNode.Text, UserData.sUserID, sDbName, sLogDesc.Replace("\\", " \\ "), "");
            CommonClass.InputLog(oSqlConnAuditTrail, tvTerminal.TopNode.Text, UserData.sUserID, sDbName, sLogDesc.Replace("\\", " \\ "), "");
            return true;
        }

        /// <summary>
        /// Set relation and add new Terminal.
        /// </summary>
        private void AddNewTerminal()
        {
            string[] sArrRel =
                {
                    null,                           //Terminal
                    null,                           //Acquirer
                    null,                           //Issuer
                    null                            //Card
                };
            DoAddItem(CommonLib.NodeLevel.Terminal, sArrRel, CommonSP.sSPTerminalInsert, 0);
        }

        /// <summary>
        /// Set relation and add new Acquirer.
        /// </summary>
        private void AddNewAcquirer()
        {
            string[] sArrRel =
                {
                    dgvTerminal.Focused ?
                        dgvTerminal.SelectedCells[0].Value.ToString() :
                        tvTerminal.TopNode.Text,    //Terminal
                    null,                           //Acquirer
                    null,                           //Issuer
                    null                            //Card
                };
            DoAddItem(CommonLib.NodeLevel.Acquirer, sArrRel, CommonSP.sSPAcqInsert, 0);
        }

        /// <summary>
        /// Set relation and add new Issuer.
        /// </summary>
        private void AddNewIssuer()
        {
            string[] sArrRel =
                {
                    tvTerminal.TopNode.Text,        //Terminal
                    tvTerminal.SelectedNode.Text,   //Acquirer
                    null,                           //Issuer
                    null                            //Card
                };
            DoAddItem(CommonLib.NodeLevel.Issuer, sArrRel, CommonSP.sSPIssInsert, 0);
        }

        private void LoadAddCustomMenu()
        {
            bool isEdit = false;
            if (isCustomMenuDataExist())
            {
                //    string[] sArrRel =
                //    {
                //        ((string[])tvTerminal.SelectedNode.FullPath.Split('\\'))[0],                           //Terminal
                //        null,                                                                                  //Acquirer
                //        null,                                                                                  //Issuer
                //        null                                                                                   //Card
                //    };
                //    DoAddItem(CommonLib.NodeLevel.CustomMenu, sArrRel, CommonSP.sSPTerminalInsert, 14);
                //}
                //else
                //{
                //    string[] sArrRel =
                //    {
                //        ((string[])tvTerminal.SelectedNode.FullPath.Split('\\'))[0],                           //Terminal
                //        null,                                                                                  //Acquirer
                //        null,                                                                                  //Issuer
                //        null                                                                                   //Card
                //    };
                isEdit = true;
            }
            string sTerminalId = dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : (tvTerminal.SelectedNode.FullPath.Split('\\'))[0];
            string sUserIdOnAccess = null;
            if (CommonClass.IsAllowUserAccess(oSqlConn, sTerminalId, ref sUserIdOnAccess))
            {
                //FrmFlexible fFlexible = new FrmFlexible(oSqlConn, oSqlConnAuditTrail, isEdit);
                FrmCustomMenu fCustomMenu = new FrmCustomMenu(oSqlConn, oSqlConnAuditTrail, isEdit);
                try
                {
                    //sArrRel.CopyTo(fFlexible.sArrCurrentRelations, 0);
                    //fFlexible.sDbID = sDbID;
                    //fFlexible.sDbName = sDbName;
                    //fFlexible.sFormID = ((int)CommonLib.NodeLevel.CustomMenu + 1 + 14).ToString();
                    //fFlexible.sName = sTerminalId;

                    //CommonClass.UserAccessInsert(oSqlConn, sTerminalId);
                    //UserData.sTerminalIdActive = sTerminalId;

                    //fFlexible.ShowDialog();

                    fCustomMenu.sDbID = sDbID;
                    fCustomMenu.sDbName = sDbName;
                    fCustomMenu.sTerminalID = sTerminalId;

                    CommonClass.UserAccessInsert(oSqlConn, sTerminalId);
                    UserData.sTerminalIdActive = sTerminalId;

                    fCustomMenu.ShowDialog();

                    string sItemPath = sTerminalId;
                    string sItemLevel = CommonLib.NodeLevel.Terminal.ToString();

                    //if (string.IsNullOrEmpty(fFlexible.sResultTag))
                    //    CommonClass.InputLog(oSqlConnAuditTrail, sTerminalId, UserData.sUserID, sDbName, "View " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "), "");
                    //else
                    //    CommonClass.InputLog(oSqlConnAuditTrail, sTerminalId, UserData.sUserID, sDbName, "Edit " + sItemLevel + " : " + sItemPath.Replace("\\", " \\ "),
                    //        fFlexible.sActDtl);

                    //if (isGetTagDetail() && tvTerminal.SelectedNode != null)
                    //    FillListTagDetail();
                }
                catch (Exception ex)
                {
                    CommonClass.doWriteErrorFile("Expand OnDoubleClick : " + ex.Message);
                }
                finally
                {
                    //bool isUpdateProfile = fFlexible.isUpdateProfile;
                    //fFlexible.Dispose();

                    fCustomMenu.Dispose();

                    CommonClass.UserAccessDelete(oSqlConn, sTerminalId);
                    UserData.sTerminalIdActive = "";
                }
            }
            else
                MessageBox.Show(string.Format("Cannot open '{0}', is being accessed by '{1}'", sTerminalId, sUserIdOnAccess));
            //}

        }

        /// <summary>
        /// Add new item to database.
        /// </summary>
        /// <param name="eType">NodeLevel : Terminal, Acquirer, Issuer, or Card</param>
        /// <param name="sArrRel">string array : Relations for item</param>
        /// <param name="sStoredProcedure">string : Stored Procedure would be used to insert new item</param>
        private void DoAddItem(CommonLib.NodeLevel eType, string[] sArrRel, string sStoredProcedure, int iAddNumber)
        {
            FrmFlexible frmAdd = new FrmFlexible(oSqlConn, oSqlConnAuditTrail, false);

            frmAdd.sDbID = sDbID;
            frmAdd.sDbName = sDbName;
            frmAdd.sFormID = ((int)eType + 1 + iAddNumber).ToString();
            frmAdd.sArrCurrentRelations = sArrRel;

            frmAdd.ShowDialog();

            if (!string.IsNullOrEmpty(frmAdd.sResultTag))
            {
                string sResultItem, sLogTID, sLogDesc; ;
                int iLengthTag = 0;
                iLengthTag = GetTagLength();
                if (iLengthTag == 4)
                    sResultItem = frmAdd.sResultTag.Substring(6, Convert.ToInt32(frmAdd.sResultTag.Substring(4, 2)));
                else
                    sResultItem = frmAdd.sResultTag.Substring(7, Convert.ToInt32(frmAdd.sResultTag.Substring(5, 2)));

                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                SqlTransaction oTrans = oSqlConn.BeginTransaction();
                SqlCommand oSqlComm = new SqlCommand(sStoredProcedure, oSqlConn, oTrans);
                oSqlComm.CommandType = CommandType.StoredProcedure;

                switch (eType)
                {
                    case CommonLib.NodeLevel.Terminal:
                        oSqlComm.Parameters.Add("@sTerminalID", SqlDbType.VarChar, 8).Value = sLogTID = sResultItem;
                        oSqlComm.Parameters.Add("@sDbID", SqlDbType.Int).Value = Convert.ToInt32(sDbID);
                        oSqlComm.Parameters.Add("@bAllowDownload", SqlDbType.Bit).Value = frmAdd.chkAllowDL.Checked;
                        oSqlComm.Parameters.Add("@bStatus", SqlDbType.Bit).Value = frmAdd.chkStatusMaster.Checked;
                        oSqlComm.Parameters.Add("@bEnableIP", SqlDbType.Bit).Value = frmAdd.chkEnableIP.Checked;
                        //oSqlComm.Parameters.Add("@bAutoInit", SqlDbType.Bit).Value = frmAdd.chkAutoInit.Checked;
                        oSqlComm.Parameters.Add("@bInitCompress", SqlDbType.Bit).Value = frmAdd.chkInitCompress.Checked;
                        sLogDesc = "Add Terminal : ";
                        break;
                    case CommonLib.NodeLevel.Acquirer:
                        oSqlComm.Parameters.Add("@sTerminalID", SqlDbType.VarChar, 8).Value = sLogTID = sArrRel[(int)CommonLib.NodeLevel.Terminal];
                        sLogDesc = "Add Acquirer : " + sLogTID + " \\ ";
                        break;
                    case CommonLib.NodeLevel.Issuer:
                        oSqlComm.Parameters.Add("@sTerminalID", SqlDbType.VarChar, 8).Value = sLogTID = sArrRel[(int)CommonLib.NodeLevel.Terminal];
                        oSqlComm.Parameters.Add("@sAcquirerName", SqlDbType.VarChar, 50).Value = sArrRel[(int)CommonLib.NodeLevel.Acquirer];
                        sLogDesc = "Add Issuer : " + sLogTID + " \\ " + sArrRel[(int)CommonLib.NodeLevel.Acquirer] + " \\ ";
                        break;
                    default:
                        return;
                }
                switch (eType)
                {
                    case CommonLib.NodeLevel.Terminal:
                        try
                        {
                            oSqlComm.Parameters.Add("@sContent", SqlDbType.VarChar, frmAdd.sResultTag.Length).Value = frmAdd.sResultTag;
                            oSqlComm.ExecuteNonQuery();
                            oTrans.Commit();
                            //CommonClass.InputLog(oSqlConn, sLogTID, UserData.sUserID, sDbName, sLogDesc + sResultItem, frmAdd.sActDtl);
                            CommonClass.InputLog(oSqlConnAuditTrail, sLogTID, UserData.sUserID, sDbName, sLogDesc + sResultItem, frmAdd.sActDtl);
                        }
                        catch (Exception ex)
                        {
                            oTrans.Rollback();
                            MessageBox.Show("Terminal already exists. ");
                        }
                        break;
                    default:
                        try
                        {
                            oSqlComm.Parameters.Add("@sContent", SqlDbType.VarChar, frmAdd.sResultTag.Length).Value = frmAdd.sResultTag;
                            oSqlComm.ExecuteNonQuery();
                            oTrans.Commit();
                            //CommonClass.InputLog(oSqlConn, sLogTID, UserData.sUserID, sDbName, sLogDesc + sResultItem, frmAdd.sActDtl);
                            CommonClass.InputLog(oSqlConnAuditTrail, sLogTID, UserData.sUserID, sDbName, sLogDesc + sResultItem, frmAdd.sActDtl);
                        }
                        catch (Exception ex)
                        {
                            oTrans.Rollback();
                            MessageBox.Show(ex.Message);
                        }
                        break;
                }


                //if (eType == CommonLib.NodeLevel.Terminal)
                //{
                //    frmSetGroupRegionCity fSetGroupRegionCity = new frmSetGroupRegionCity(oSqlConn, sTerminalID);
                //    try
                //    {
                //        fSetGroupRegionCity.ShowDialog();
                //    }
                //    catch (Exception ex)
                //    {
                //        MessageBox.Show(ex.Message);
                //    }
                //    finally
                //    {
                //        fSetGroupRegionCity.Dispose();
                //    }
                //}

                Relations oRelation = new Relations();

                if (eType != CommonLib.NodeLevel.Terminal)
                {
                    if (eType == CommonLib.NodeLevel.Acquirer)
                    {
                        doAddRelation(eType, sResultItem, "", "");
                        tvTerminal.TopNode.Nodes.Add(sResultItem);
                        tvTerminal.TopNode.Expand();
                    }
                    else
                    {
                        doAddRelation(eType, tvTerminal.SelectedNode.Text, sResultItem, "");
                        tvTerminal.SelectedNode.Nodes.Add(sResultItem);
                        tvTerminal.SelectedNode.Expand();
                    }
                }
                else
                {
                    InitData();
                    LoadData(0);
                }
            }
        }

        private bool isCustomMenuDataExist()
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.isCustomMenuHaveData('{0}')", (dgvTerminal.Focused ? dgvTerminal.CurrentCell.Value.ToString() : (tvTerminal.SelectedNode.FullPath.Split('\\'))[0]));
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                DataTable dtTemp = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                oSqlConn.Close();
            }
            return isAllow;
        }


        private int GetTagLength()
        {
            string sTag = odtItemList.Rows[3]["Tag"].ToString();
            sTag = sTag.Replace(" ", "");
            return sTag.Length;
        }

        /// <summary>
        /// Add new Relation for current version
        /// </summary>
        /// <param name="oNodeLvl">NodeLevel : Node's type, between Terminal, Acquirer, Issuer, or Card</param>
        /// <param name="sAcquirerName">string : Acquirer Name</param>
        /// <param name="sIssuerName">String : Issuer Name</param>
        /// <param name="sCardName">String : Card Name</param>
        protected void doAddRelation(CommonLib.NodeLevel oNodeLvl, string sAcquirerName, string sIssuerName, string sCardName)
        {
            Relations oRelation = new Relations();
            int iIndex = 0;

            if (oNodeLvl == CommonLib.NodeLevel.Acquirer)
            {
                //oRelation.FillValue(2, sAcquirerName);
                //oRelation.FillValue(1, sIssuerName);
                //oRelation.FillValue(0, sCardName);
                oRelation.sAcquirerName = sAcquirerName;
                oRelation.sIssuerName = sIssuerName;
                oRelation.sCardName = sCardName;
                ltRelations.Add(oRelation);
            }
            else if (oNodeLvl == CommonLib.NodeLevel.Issuer || oNodeLvl == CommonLib.NodeLevel.Card)
            {
                while (iIndex > -1)
                {
                    int iNodeLevel = 3 - ((int)oNodeLvl);

                    iIndex = ltRelations.FindIndex(iIndex, delegate (Relations tempRelation)
                    {
                        //return tempRelation.sGetValue(2).Equals(sAcquirerName) &&
                        //        tempRelation.sGetValue(1).Equals(sIssuerName);
                        return tempRelation.sAcquirerName.Equals(sAcquirerName) &&
                                tempRelation.sIssuerName.Equals(sIssuerName);
                    });

                    //iIndex = iGetRelationIndex(iNodeLevel + 1, 
                    //                           (oNodeLvl == CommonLib.NodeLevel.Issuer)? sAcquirerName : sIssuerName
                    //                           );

                    if (iIndex > -1)
                    {
                        string sValue =
                            iNodeLevel == 2 ? ltRelations[iIndex].sAcquirerName :
                            iNodeLevel == 1 ? ltRelations[iIndex].sIssuerName :
                            iNodeLevel == 0 ? ltRelations[iIndex].sCardName :
                            null;
                        if (string.IsNullOrEmpty(sValue))
                        {
                            //ltRelations[iIndex].FillValue(iNodeLevel,
                            //                              (oNodeLvl == CommonLib.NodeLevel.Issuer) ? sIssuerName : sCardName);
                            if (oNodeLvl == CommonLib.NodeLevel.Issuer) ltRelations[iIndex].sIssuerName = sIssuerName;
                            else ltRelations[iIndex].sCardName = sCardName;
                            break;
                        }
                        iIndex++;
                    }
                    else
                    {
                        //oRelation.FillValue(2, sAcquirerName);
                        //oRelation.FillValue(1, sIssuerName);
                        //oRelation.FillValue(0, sCardName);
                        oRelation.sAcquirerName = sAcquirerName;
                        oRelation.sIssuerName = sIssuerName;
                        oRelation.sCardName = sCardName;
                        ltRelations.Add(oRelation);
                    }
                }
            }
            else if (oNodeLvl == CommonLib.NodeLevel.Card)
            {
            }
        }

        /// <summary>
        /// Insert new Card on terminal to database.
        /// </summary>
        private void AddNewCard()
        {
            string sCardName;

            FrmChoosing frmChooseCard = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "Card");
            frmChooseCard.btnCancel.Text = frmChooseCard.btnCancel.Tag.ToString();
            frmChooseCard.btnAddCurrentIssuer.Visible = true;
            frmChooseCard.btnNew.Visible = false;
            frmChooseCard.btnEdit.Visible = false;
            frmChooseCard.btnDel.Visible = false;

            if (frmChooseCard.ShowDialog() == DialogResult.Yes && frmChooseCard.cmbChoosingName.SelectedIndex > -1)
                sCardName = frmChooseCard.cmbChoosingName.SelectedValue.ToString();
            else return;

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPCardInsert, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;
            //oSqlComm.Parameters.Add("@sTerminalID", SqlDbType.VarChar, 8).Value = tvTerminal.TopNode.Text;
            oSqlComm.Parameters.Add("@sTerminalID", SqlDbType.VarChar, 8).Value = tvTerminal.Nodes[0].Text;
            oSqlComm.Parameters.Add("@sAcquirerValue", SqlDbType.VarChar, 50).Value = tvTerminal.SelectedNode.Parent.Text;
            oSqlComm.Parameters.Add("@sIssuerValue", SqlDbType.VarChar, 50).Value = tvTerminal.SelectedNode.Text;
            oSqlComm.Parameters.Add("@sCardValue", SqlDbType.VarChar, 50).Value = sCardName;
            oSqlComm.Parameters.Add("@sTag", SqlDbType.VarChar, 5).Value = GetTagLength() == 5 ? "AD001" : "AD01";
            object oResult = oSqlComm.ExecuteScalar();
            if (oResult == null)
            {
                tvTerminal.SelectedNode.Nodes.Add(sCardName);
                tvTerminal.SelectedNode.Expand();
                //CommonClass.InputLog(oSqlConn, tvTerminal.TopNode.Text, UserData.sUserID, sDbName,
                //    "Add Card : " + tvTerminal.TopNode.Text +       //Terminal
                //    " \\ " + tvTerminal.SelectedNode.Parent.Text +  //Acquirer
                //    " \\ " + tvTerminal.SelectedNode.Text +         //Issuer
                //    " \\ " + sCardName, "");                        //Card
                //doAddRelation(CommonLib.NodeLevel.Card, tvTerminal.SelectedNode.Parent.Text, tvTerminal.SelectedNode.Text, sCardName);
                CommonClass.InputLog(oSqlConnAuditTrail, tvTerminal.TopNode.Text, UserData.sUserID, sDbName,
                    "Add Card : " + tvTerminal.TopNode.Text +       //Terminal
                    " \\ " + tvTerminal.SelectedNode.Parent.Text +  //Acquirer
                    " \\ " + tvTerminal.SelectedNode.Text +         //Issuer
                    " \\ " + sCardName, "");                        //Card
                doAddRelation(CommonLib.NodeLevel.Card, tvTerminal.SelectedNode.Parent.Text, tvTerminal.SelectedNode.Text, sCardName);
            }
            else MessageBox.Show(oResult.ToString(), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void CreateDataTableRelation(string _sTerminalID, List<Relations> _ltRelation, ref DataTable _dtTemp)
        {
            foreach (Relations tempRelation in _ltRelation)
            {
                for (int iLevel = 0; iLevel <= 2; iLevel++)
                {
                    string sTag = iLevel == 0 ? "AD01" : iLevel == 1 ? "AD02" : "AD03";
                    //string sTagValue = tempRelation.sGetValue(iLevel);
                    string sTagValue = iLevel == 0 ? tempRelation.sCardName : iLevel == 1 ? tempRelation.sIssuerName : tempRelation.sAcquirerName;
                    DataRow rowInsert = _dtTemp.NewRow();
                    rowInsert["TerminalID"] = _sTerminalID;
                    rowInsert["RelationTag"] = sTag;
                    rowInsert["RelationTagValue"] = sTagValue;
                    rowInsert["RelationTagLength"] = sTagValue.Length;
                    rowInsert["RelationLengthOfTagLength"] = sTagValue.Length.ToString().Length;
                    _dtTemp.Rows.Add(rowInsert);
                }
            }
        }

        private void SaveRelation(string _sTerminalID, DataTable _dtRelationsNew)
        {
            ClearRelation(_sTerminalID);

            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oSqlConn);
            oSqlBulk.BatchSize = 5000;
            oSqlBulk.BulkCopyTimeout = 0;
            oSqlBulk.DestinationTableName = "tbProfileRelation";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapRelationTag = new SqlBulkCopyColumnMapping("RelationTag", "RelationTag");
            SqlBulkCopyColumnMapping mapRelationTagValue = new SqlBulkCopyColumnMapping("RelationTagValue", "RelationTagValue");
            SqlBulkCopyColumnMapping mapRelationTagLength = new SqlBulkCopyColumnMapping("RelationTagLength", "RelationTagLength");
            SqlBulkCopyColumnMapping mapRelationLengthOfTagLength = new SqlBulkCopyColumnMapping("RelationLengthOfTagLength", "RelationLengthOfTagLength");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapRelationTag);
            oSqlBulk.ColumnMappings.Add(mapRelationTagValue);
            oSqlBulk.ColumnMappings.Add(mapRelationTagLength);
            oSqlBulk.ColumnMappings.Add(mapRelationLengthOfTagLength);
            oSqlBulk.WriteToServer(_dtRelationsNew);
        }

        private void ClearRelation(string _sTerminalID)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPRelationDelete, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalID;
                oCmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region "Application Package"
        /// <summary>
        /// Determine whether Package is selected or not
        /// </summary>
        /// <param name="sPackageName">string : Package Name</param>
        /// <param name="sPackageID">string : Package ID</param>
        /// <returns>boolean : True if selected</returns>
        protected bool IsPackageSelected(ref string sPackageName, ref string sPackageID)
        {
            FrmPromptString frmPrompt = new FrmPromptString(CommonMessage.sSoftwarePackageTitle, CommonMessage.sSoftwarePackageText);
            ComboBox cmbPrompt = new ComboBox();

            cmbPrompt.TabIndex = 0;
            cmbPrompt.ValueMember = "AppPackID";
            cmbPrompt.DisplayMember = "Package Name";
            cmbPrompt.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbPrompt.Location = frmPrompt.txtInput.Location;
            cmbPrompt.Size = frmPrompt.txtInput.Size;
            frmPrompt.txtInput.Visible = false;
            frmPrompt.Controls.Add(cmbPrompt);

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPAppPackageBrowse, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;

            DataTable oTable = new DataTable();
            new SqlDataAdapter(oSqlComm).Fill(oTable);
            cmbPrompt.DataSource = oTable;
            cmbPrompt.SelectedIndex = -1;

            if (frmPrompt.ShowDialog() == DialogResult.OK)
            {
                if (cmbPrompt.SelectedIndex > -1)
                {
                    sPackageID = cmbPrompt.SelectedValue.ToString();
                    sPackageName = cmbPrompt.Text;
                    this.Opacity = 1;
                }
                else
                {
                    MessageBox.Show("No Package was selected.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            else return false;
            return true;
        }

        /// <summary>
        /// Determine whether PackageName valid or not
        /// </summary>
        /// <param name="sPackageName">string : Package Name which will be examined</param>
        /// <returns>boolean : true if valid</returns>
        protected bool IsPackageValid(string sPackageName)
        {
            return lvAppEDC.FindItemWithText(sPackageName) == null ? true : false;
        }

        /// <summary>
        /// Save Package Data into database
        /// </summary>
        /// <param name="sTerminalID">String : TerminalID which has Package Data</param>
        protected void SavePackage(string sTerminalID)
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAppPackageTerminalUpdate, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oSqlCmd.Parameters.Add("@sAppPackIDList", SqlDbType.VarChar).Value = sAppPackIDList();
                oSqlCmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Determine whether there is a package selected or not
        /// </summary>
        /// <param name="sPackageName">string : Package Name</param>
        /// <returns>boolean : true if package is selected</returns>
        protected bool IsListPackageSelected(ref string sPackageName)
        {
            if (lvAppEDC.SelectedItems.Count > 0)
            {
                sPackageName = lvAppEDC.SelectedItems[0].Text;
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Get all Package from Package List
        /// </summary>
        /// <returns>string : Name all packages</returns>
        protected string sAppPackIDList()
        {
            string sValue = null;

            foreach (ListViewItem item in lvAppEDC.Items)
            {
                if (!string.IsNullOrEmpty(sValue)) sValue = sValue + ",";
                sValue = sValue + sGetAppPackID(item.Text);
            }
            //sValue = sValue.Substring(0, sValue.Length - 1);
            return sValue;
        }

        /// <summary>
        /// Get PackageID for Package Name given 
        /// </summary>
        /// <param name="sAppPackName">String : Package Name</param>
        /// <returns>String : Package ID</returns>
        protected string sGetAppPackID(string sAppPackName)
        {
            int iIndex = 0;
            iIndex = ltPackage.FindIndex(delegate (Package temp) { return temp.Name == sAppPackName; });

            return ltPackage[iIndex].ID;
        }
        #endregion

        private void cardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sDbID))
            {
                FrmChoosing fcardmanagement = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "Card");
                try
                {
                    fcardmanagement.ShowDialog();
                }
                finally
                {
                    fcardmanagement.Dispose();
                }
            }
        }

        private void edcMonitoringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sDbID))
            {
                FrmChoosing frmEdcMonitoring = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "edcmonitoring");
                try
                {
                    frmEdcMonitoring.ShowDialog();
                }
                finally
                {
                    frmEdcMonitoring.Dispose();
                }
            }
        }

        /// <summary>
        /// Export data from file into a profile in current DatabaseID.
        /// </summary>
        private void registerTerminalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPromptString fregisteroldfile = new FrmPromptString(CommonMessage.sRegisterTitle, CommonMessage.sRegisterText);
            try
            {
                MaskedTextBox oMaskedBox = CommonClass.CreateOMaskedBox(fregisteroldfile.txtInput.Location, fregisteroldfile.txtInput.Size, ">AAAAAAAA");
                fregisteroldfile.Controls.Add(oMaskedBox);
                fregisteroldfile.txtInput.Visible = false;
                if (fregisteroldfile.ShowDialog() == DialogResult.OK)
                {
                    if (oMaskedBox.Text.Replace(" ", "").Length == 8)
                    {
                        string sDbNameCheck = sGetDBName(oMaskedBox.Text);
                        if (sDbNameCheck == "")
                        {
                            if (CommonClass.isCanRegisterOldFile(Application.StartupPath + "\\FILE\\", oMaskedBox.Text,
                                oSqlConn, int.Parse(sDbID)))
                            {
                                LoadData(0);
                                //lvTerminal.Items[oMaskedBox.Text].Selected = true;
                                dgvTerminal.Rows[odtTerminal.Rows.IndexOf(odtTerminal.Rows.Find(oMaskedBox.Text))].Selected = true;
                                tvTerminal.Focus();
                                tvTerminal.SelectedNode = tvTerminal.TopNode;

                                File.Delete(Application.StartupPath + "\\FILE\\" + oMaskedBox.Text + ".txt");
                            }
                            else MessageBox.Show("File Not Found");
                        }
                        else MessageBox.Show("Terminal already exists in " + sDbNameCheck, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else MessageBox.Show("Invalid Terminal ID", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            finally
            {
                fregisteroldfile.Dispose();
            }
        }

        /// <summary>
        /// Call form Advance Searh and load Parameter Data
        /// </summary>
        private void advanceSearchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = string.Format("WHERE DatabaseID = {0}", sDbID);

            FrmAdvanceSearch frmadvance = new FrmAdvanceSearch(oSqlConn, sDbID, oSqlCmd); //send connection, databaseId & Data Adapter From FrmExpand to Form AdvanceSearch

            frmadvance.frmExpand = this; // allow form AdvanceSearch to acces component from form FrmExpand
            frmadvance.ShowDialog();
        }

        private void mnuManagementAID_Click(object sender, EventArgs e)
        {
            FrmChoosing fAID = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "AID");
            try
            {
                fAID.ShowDialog();
            }
            finally
            {
                fAID.Dispose();
            }
        }

        private void mnuManagementPK_Click(object sender, EventArgs e)
        {
            FrmChoosing fCAPK = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "CAPK");
            try
            {
                fCAPK.ShowDialog();
            }
            finally
            {
                fCAPK.Dispose();
            }
        }

        private void mnuManagementTLE_Click(object sender, EventArgs e)
        {
            FrmChoosing ftle = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "TLE");
            try
            {
                ftle.ShowDialog();
            }
            finally
            {
                ftle.Dispose();
            }
        }

        private void mnuManagementEmv_Click(object sender, EventArgs e)
        {
            FrmChoosing fEMVManagement = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "EMVManagement");
            try
            {
                fEMVManagement.ShowDialog();
            }
            finally
            {
                fEMVManagement.Dispose();
            }
        }

        private void mnuManagementRmtDownload_Click(object sender, EventArgs e)
        {
            FrmChoosing fRemoteDownload = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "remotedownload");
            try
            {
                fRemoteDownload.ShowDialog();
            }
            finally
            {
                fRemoteDownload.Dispose();
            }
        }

        private void mnuManagementGprsEth_Click(object sender, EventArgs e)
        {
            FrmChoosing fgprs = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "gprs");
            try
            {
                fgprs.ShowDialog();
            }
            finally
            {
                fgprs.Dispose();
            }
        }

        private void mnuManagementPinPad_Click(object sender, EventArgs e)
        {
            FrmChoosing fpinpad = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "pinpad");
            try
            {
                fpinpad.ShowDialog();
            }
            finally
            {
                fpinpad.Dispose();
            }
        }

        private void mnuManagementPromo_Click(object sender, EventArgs e)
        {
            FrmChoosing fPromoManagement = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "PROMOMANAGEMENT");
            try
            {
                fPromoManagement.ShowDialog();
                LoadData(0);
            }
            finally
            {
                fPromoManagement.Dispose();
            }
        }

        private void mnuManagementInitialFLAZZ_Click(object sender, EventArgs e)
        {
            FrmChoosing fInitialFlazzMan = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "initialflazz");
            try
            {
                fInitialFlazzMan.ShowDialog();
            }
            finally
            {
                fInitialFlazzMan.Dispose();
            }
        }

        private void mnuManagementCardType_Click(object sender, EventArgs e)
        {
            FrmChoosing fCardTypeManagement = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "CARDTYPE");
            try
            {
                fCardTypeManagement.ShowDialog();
                LoadData(0);
            }
            finally
            {
                fCardTypeManagement.Dispose();
            }
        }

        private void createTerminalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewTerminal();
        }

        private void viewLastTerminalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isGetTopTenTerminalList())
            {
                InitTreeAndListView();
                FillListTerminal();
            }
        }

        private void viewAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadData(0);
        }

        private void toolStripSearchTID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                string sSearchTerminalID = toolStripSearchTID.Text;
                SearchProfileTerminal(sSearchTerminalID);
                //CommonClass.InputLog(oSqlConn, "", sUID, sDbName, "Search Terminal with keyword : " + sGetTIDSearch(), "");
                CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, sDbName, "Search Terminal with keyword : " + sGetTIDSearch(sSearchTerminalID), "");
            }
        }

        private void toolStripSearchTID_Enter(object sender, EventArgs e)
        {
            if (toolStripSearchTID.Text.ToLower() == "search terminalid")
            {
                toolStripSearchTID.Clear();
                toolStripSearchTID.CharacterCasing = CharacterCasing.Upper;
                toolStripSearchTID.ForeColor = Color.Black;
            }
            else
                toolStripSearchTID.SelectAll();
        }

        private void toolStripSearchTID_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(toolStripSearchTID.Text))
            {
                toolStripSearchTID.CharacterCasing = CharacterCasing.Upper;
                toolStripSearchTID.Text = "Search TerminalID";
                toolStripSearchTID.ForeColor = Color.LightGray;
            }
        }

        private void lblOSVer_Click(object sender, EventArgs e)
        {

        }

        private void mnuRequestPaperReceiptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sDbID))
            {
                FrmChoosing frmRequestPaperReceipt = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "requestpaperreceipt");
                try
                {
                    frmRequestPaperReceipt.ShowDialog();
                }
                finally
                {
                    frmRequestPaperReceipt.Dispose();
                }
            }
        }

        private void FrmExpand_Load(object sender, EventArgs e)
        {

        }

        private void toolStripSearchSN_Enter(object sender, EventArgs e)
        {
            if (toolStripSearchSN.Text.ToLower() == "search serial number")
            {
                toolStripSearchSN.Clear();
                toolStripSearchSN.CharacterCasing = CharacterCasing.Upper;
                toolStripSearchSN.ForeColor = Color.Black;
            }
            else
                toolStripSearchSN.SelectAll();
        }

        private void toolStripSearchSN_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                string sSearchTerminalID = toolStripSearchSN.Text;
                //SearchProfileTerminal(sSearchTerminalID);
                if (toolStripSearchSN.ForeColor == Color.Black)
                    LoadData(2);
                //CommonClass.InputLog(oSqlConn, "", sUID, sDbName, "Search Terminal with keyword : " + sGetTIDSearch(), "");
                CommonClass.InputLog(oSqlConnAuditTrail, "", sUID, sDbName, "Search Terminal with serial number : " + sGetTIDSearch(sSearchTerminalID), "");
            }
        }

        private void toolStripSearchSN_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(toolStripSearchTID.Text))
            {
                toolStripSearchSN.CharacterCasing = CharacterCasing.Upper;
                toolStripSearchSN.Text = "Search sERIAL NUMBER";
                toolStripSearchSN.ForeColor = Color.LightGray;
            }
        }

        private void mnuManagementSN_Click(object sender, EventArgs e)
        {
            FrmChoosing fSnManagement = new FrmChoosing(oSqlConn, oSqlConnAuditTrail, sDbID, sDbName, "snmanagement");
            try
            {
                fSnManagement.ShowDialog();
            }
            finally
            {
                fSnManagement.Dispose();
            }
        }
    }
}