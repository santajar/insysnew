﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

using InSysClass;

namespace InSys
{
    public partial class FrmEditAID2 : Form
    {
        /// <summary>
        /// Determines if current mode is Editing or Adding
        /// </summary>
        protected bool IsEdit;
        protected SqlConnection oSqlConn;

        protected string sDbId;
        protected string sDbName;

        protected string sEMVAID;
        protected string sEMVTRMAVN;
        protected string sEMVNAME;
        protected string sEMVTACDFT;
        protected string sEMVTACDEN;
        protected string sEMVTACONL;
        protected string sEMVTHRVAL;
        protected string sEMVTARPER;
        protected string sEMVMAXTARPER;
        protected string sEMVDFTVALDDOL;
        protected string sEMVDFTVALTDOL;
        protected string sEMVACQID;
        protected string sEMVTRMFLRLIM;
        protected string sEMVTCC;
        protected string sEMVAIDTXNTYPE;
        protected string sEMVTRMCAP;
        protected string sEMVTAG;

        protected const string EMVAID = "AI001";
        protected const string EMVTRMAVN = "AI002";
        protected const string EMVNAME = "AI003";
        protected const string EMVTACDFT = "AI004";
        protected const string EMVTACDEN = "AI005";
        protected const string EMVTACONL = "AI006";
        protected const string EMVTHRVAL = "AI007";
        protected const string EMVTARPER = "AI008";
        protected const string EMVMAXTARPER = "AI009";
        protected const string EMVDFTVALDDOL = "AI010";
        protected const string EMVDFTVALTDOL = "AI011";
        protected const string EMVACQID = "AI012";
        protected const string EMVTRMFLRLIM = "AI013";
        protected const string EMVTCC = "AI014";
        protected const string EMVAIDTXNTYPE = "AI015";
        protected const string EMVTRMCAP = "AI016";
        protected const string EMVTAG = "AI017";
        

        /// <summary>
        /// Form to add or edit AID Data
        /// </summary>
        /// <param name="_isEdit">boolean : true if edit mode, false if add mode</param>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sDbId">string : DatabaseID</param>
        public FrmEditAID2(bool _isEdit, SqlConnection _oSqlConn, string _sDbId, string _sDbName) : this(_isEdit, _oSqlConn, _sDbId, _sDbName, "") { }
        public FrmEditAID2(bool _isEdit, SqlConnection _oSqlConn, string _sDbId, string _sDbName , string _sAIDName)
        {
            InitializeComponent();
            IsEdit = _isEdit;
            oSqlConn = _oSqlConn;
            sDbId = _sDbId;
            sDbName = _sDbName;
            sEMVAID = _sAIDName;
        }

        private void FrmEditAID2_Load(object sender, EventArgs e)
        {
            InitAID();
            SetDisplay();
            string sLog = CommonMessage.sFormOpened + this.Text;
            if (IsEdit)
                sLog += " : " + sGetEMVAID;

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, sLog, "");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                if (IsEdit)
                    UpdateAID(sGenAIDContent);
                else
                    SaveAID(sGenAIDContent);
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region "Properties"
        // <summary>
        // Get String
        //</summary>
        protected string sGetEMVAID { get { return txtEMVAID.Text; } }
        protected string sGetEMVTRMAVN { get { return txtEMVTRMAVN.Text; } }
        protected string sGetEMVNAME { get { return txtEMVNAME.Text; } }
        protected string sGetEMVTACDFT { get { return txtEMVTACDFT.Text; } }
        protected string sGetEMVTACDEN { get { return txtEMVTACDEN.Text; } }
        protected string sGetEMVTACONL { get { return txtEMVTACONL.Text; } }
        protected string sGetEMVTHRVAL { get { return txtEMVTHRVAL.Text; } }
        protected string sGetEMVTARPER { get { return txtEMVTARPER.Text; } }
        protected string sGetEMVMAXTARPER { get { return txtEMVMAXTARPER.Text; } }
        protected string sGetEMVDFTVALDDOL { get { return txtEMVDFTVALDDOL.Text; } }
        protected string sGetEMVDFTVALTDOL { get { return txtEMVDFTVALTDOL.Text; } }
        protected string sGetEMVACQID { get { return txtEMVACQID.Text; } }
        protected string sGetEMVTRMFLRLIM { get { return txtEMVTRMFLRLIM.Text; } }
        protected string sGetEMVTCC { get { return txtEMVTCC.Text; } }
        protected string sGetEMVAIDTXNTYPE { get { return txtEMVAIDTXNTYPE.Text; } }
        protected string sGetEMVTRMCAP { get { return txtEMVTRMCAP.Text; } }
        protected string sGetEMVTAG { get { return txtEMVTAG.Text; } }


        /// <summary>
        /// AID Content value
        /// </summary>
        protected string sGenAIDContent
        {
            get
            {
                return sGenTagEMVAID()
                    + sGetTagEMVTRMAVN()
                    + sGenTagEMVNAME()
                    + sGenTagEMVTACDFT()
                    + sGenTagEMVTACDEN()
                    + sGenTagEMVTACONL()
                    + sGenTagEMVTHRVAL()
                    + sGenTagEMVTARPER()
                    + sGenTagEMVMAXTARPER()
                    + sGenTagEMVDFTVALDDOL()
                    + sGenTagEMVDFTVALTDOL()
                    + sGenTagEMVACQID()
                    + sGenTagEMVTRMFLRLIM()
                    + sGenTagEMVTCC()
                    + sGenTagEMVAIDTXNTYPE()
                    + sGenTagEMVTRMCAP()
                    + sGenTagEMVTAG();
            }
        }

        protected string sGenTagEMVAID()
        {
            return string.Format("{0}{1:00}{2}", EMVAID, sGetEMVAID.Length, sGetEMVAID);
        }

        protected string sGetTagEMVTRMAVN()
        {
            return string.Format("{0}{1:00}{2}", EMVTRMAVN, sGetEMVTRMAVN.Length, sGetEMVTRMAVN);
        }

        protected string sGenTagEMVNAME()
        {
            return string.Format("{0}{1:00}{2}", EMVNAME, sGetEMVNAME.Length, sGetEMVNAME);
        }

        protected string sGenTagEMVTACDFT()
        {
            return string.Format("{0}{1:00}{2}", EMVTACDFT, sGetEMVTACDFT.Length, sGetEMVTACDFT);
        }

        protected string sGenTagEMVTACDEN()
        {
            return string.Format("{0}{1:00}{2}", EMVTACDEN, sGetEMVTACDEN.Length, sGetEMVTACDEN);
        }

        protected string sGenTagEMVTACONL()
        {
            return string.Format("{0}{1:00}{2}", EMVTACONL, sGetEMVTACONL.Length, sGetEMVTACONL);
        }

        protected string sGenTagEMVTHRVAL()
        {
            return string.Format("{0}{1:00}{2}", EMVTHRVAL, sGetEMVTHRVAL.Length, sGetEMVTHRVAL);
        }

        protected string sGenTagEMVTARPER()
        {
            return string.Format("{0}{1:00}{2}", EMVTARPER, sGetEMVTARPER.Length, sGetEMVTARPER);
        }

        protected string sGenTagEMVMAXTARPER()
        {
            return string.Format("{0}{1:00}{2}", EMVMAXTARPER, sGetEMVMAXTARPER.Length, sGetEMVMAXTARPER);
        }

        protected string sGenTagEMVDFTVALDDOL()
        {
            return string.Format("{0}{1:00}{2}", EMVDFTVALDDOL, sGetEMVDFTVALDDOL.Length, sGetEMVDFTVALDDOL);
        }

        protected string sGenTagEMVDFTVALTDOL()
        {
            return string.Format("{0}{1:00}{2}", EMVDFTVALTDOL, sGetEMVDFTVALTDOL.Length, sGetEMVDFTVALTDOL);
        }

        protected string sGenTagEMVACQID()
        {
            return string.Format("{0}{1:00}{2}", EMVACQID, sGetEMVACQID.Length, sGetEMVACQID);
        }
        protected string sGenTagEMVTRMFLRLIM()
        {
            return string.Format("{0}{1:00}{2}", EMVTRMFLRLIM, sGetEMVTRMFLRLIM.Length, sGetEMVTRMFLRLIM);
        }
        protected string sGenTagEMVTCC()
        {
            return string.Format("{0}{1:00}{2}", EMVTCC, sGetEMVTCC.Length, sGetEMVTCC);
        }
        protected string sGenTagEMVAIDTXNTYPE()
        {
            return string.Format("{0}{1:00}{2}", EMVAIDTXNTYPE, sGetEMVAIDTXNTYPE.Length, sGetEMVAIDTXNTYPE);
        }
        protected string sGenTagEMVTRMCAP()
        {
            return string.Format("{0}{1:00}{2}", EMVTRMCAP, sGetEMVTRMCAP.Length, sGetEMVTRMCAP);
        }
        protected string sGenTagEMVTAG()
        {
            return string.Format("{0}{1:00}{2}", EMVTAG, sGetEMVTAG.Length, sGetEMVTAG);
        }
        
        #endregion

        #region "Function"
        /// <summary>
        /// Set Form's display.
        /// </summary>
        public void SetDisplay()
        {
            if (!IsEdit)
                this.Text = "AID Management - Add";
            else
                this.Text = "AID Management - Edit";
            txtEMVAID.Enabled = txtEMVTRMAVN.Enabled = txtEMVNAME.Enabled = txtEMVTACDFT.Enabled = 
                txtEMVTACDEN.Enabled = txtEMVTACONL.Enabled = txtEMVTHRVAL.Enabled = txtEMVTARPER.Enabled=
                txtEMVMAXTARPER.Enabled = txtEMVDFTVALDDOL.Enabled = txtEMVDFTVALTDOL.Enabled = txtEMVACQID.Enabled =
                txtEMVTRMFLRLIM.Enabled = txtEMVTCC.Enabled = txtEMVAIDTXNTYPE.Enabled = txtEMVTRMCAP.Enabled = txtEMVTAG.Enabled=
                btnSave.Visible = btnCancel.Visible = UserPrivilege.IsAllowed(PrivilegeCode.AI, Privilege.Edit);

            txtEMVAID.Text = sEMVAID;
            txtEMVTRMAVN.Text = sEMVTRMAVN;
            txtEMVNAME.Text = sEMVNAME;
            txtEMVTACDFT.Text = sEMVTACDFT;
            txtEMVTACDEN.Text = sEMVTACDEN;
            txtEMVTACONL.Text = sEMVTACONL;
            txtEMVTHRVAL.Text = sEMVTHRVAL;
            txtEMVTARPER.Text = sEMVTARPER;
            txtEMVMAXTARPER.Text = sEMVMAXTARPER;
            txtEMVDFTVALDDOL.Text = sEMVDFTVALDDOL;
            txtEMVDFTVALTDOL.Text = sEMVDFTVALTDOL;
            txtEMVACQID.Text = sEMVACQID;
            txtEMVTRMFLRLIM.Text = sEMVTRMFLRLIM;
            txtEMVTCC.Text = sEMVTCC;
            txtEMVAIDTXNTYPE.Text = sEMVAIDTXNTYPE;
            txtEMVTRMCAP.Text = sEMVTRMCAP;
            txtEMVTAG.Text = sEMVTAG;
            txtEMVAID.ReadOnly = IsEdit;
        }

        /// <summary>
        /// Generate WHERE Condition to filter data from database
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sCondition()
        {
            return "WHERE DatabaseId = " + sDbId + " "
                + "AND AIDName = '" + sEMVAID + "'";
        }

        /// <summary>
        /// Assign value from database to form
        /// </summary>
        protected void InitAID()
        {
            DataTable dtAID = dtGetAID();
            foreach (DataRow drow in dtAID.Rows)
            {
                switch (drow["AIDTag"].ToString())
                {
                    case EMVAID:
                        sEMVAID = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTRMAVN:
                        sEMVTRMAVN = drow["AIDTagValue"].ToString();
                        break;
                    case EMVNAME:
                        sEMVNAME = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTACDFT:
                        sEMVTACDFT = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTACDEN:
                        sEMVTACDEN = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTACONL:
                        sEMVTACONL = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTHRVAL:
                        sEMVTHRVAL = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTARPER:
                        sEMVTARPER = drow["AIDTagValue"].ToString();
                        break;
                    case EMVMAXTARPER:
                        sEMVMAXTARPER = drow["AIDTagValue"].ToString();
                        break;
                    case EMVDFTVALDDOL:
                        sEMVDFTVALDDOL = drow["AIDTagValue"].ToString();
                        break;
                    case EMVDFTVALTDOL:
                        sEMVDFTVALTDOL = drow["AIDTagValue"].ToString();
                        break;
                    case EMVACQID:
                        sEMVACQID = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTRMFLRLIM:
                        sEMVTRMFLRLIM = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTCC:
                        sEMVTCC = drow["AIDTagValue"].ToString();
                        break;
                    case EMVAIDTXNTYPE:
                        sEMVAIDTXNTYPE = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTRMCAP:
                        sEMVTRMCAP = drow["AIDTagValue"].ToString();
                        break;
                    case EMVTAG:
                        sEMVTAG = drow["AIDTagValue"].ToString();
                        break;
                }
            }
        }

        /// <summary>
        /// Load data AID from database and store it in DataTable
        /// </summary>
        /// <returns>DataTable : AID Data from database</returns>
        protected DataTable dtGetAID()
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileAIDBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sCondition();
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            oCmd.Dispose();
            return dtTemp;
        }

        /// <summary>
        /// Determine whether data entered valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValid()
        {
            if (txtEMVTRMAVN.Text.Length == 4)
                if (!IsEdit)
                    if (IsValidAIDList())
                    {
                        MessageBox.Show(CommonMessage.sErrAIDInvalid);
                        return false;
                    }
                    else
                        return true;
                else
                    return true;
            else
            {
                MessageBox.Show("App Ver. Number min. Lenght = 4");
                return false;
            }
        }

        /// <summary>
        /// Determine whether data found and can be shown in form
        /// </summary>
        /// <returns>boolean : true if data found</returns>
        protected bool IsValidAIDList()
        {
            sEMVAID = sGetEMVAID;
            DataTable dtTemp = dtGetAID();
            return dtTemp.Rows.Count > 0 ? true : false;
        }

        /// <summary>
        /// Add new AID data to database
        /// </summary>
        /// <param name="sContent">string : AID data content</param>
        protected void SaveAID(string sContent)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileAIDInsert, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDbId;
            oCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
            oCmd.ExecuteNonQuery();
            oCmd.Dispose();

            string sLogDetail = "";

            sLogDetail += string.Format("{0} : {1} \n", "EMVAID", sGetEMVAID);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTRMAVN", sGetEMVTRMAVN);
            sLogDetail += string.Format("{0} : {1} \n", "EMVNAME", sGetEMVNAME);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTACDFT", sGetEMVTACDFT);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTACDEN", sGetEMVTACDEN);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTACONL", sGetEMVTACONL);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTHRVAL", sGetEMVTHRVAL);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTARPER", sGetEMVTARPER);
            sLogDetail += string.Format("{0} : {1} \n", "EMVMAXTARPER", sGetEMVMAXTARPER);
            sLogDetail += string.Format("{0} : {1} \n", "EMVDFTVALDDOL", sGetEMVDFTVALDDOL);
            sLogDetail += string.Format("{0} : {1} \n", "EMVDFTVALTDOL", sGetEMVDFTVALTDOL);
            sLogDetail += string.Format("{0} : {1} \n", "EMVACQID", sGetEMVACQID);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTRMFLRLIM", sGetEMVTRMFLRLIM);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTCC", sGetEMVTCC);
            sLogDetail += string.Format("{0} : {1} \n", "EMVAIDTXNTYPE", sGetEMVAIDTXNTYPE);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTRMCAP", sGetEMVTRMCAP);
            sLogDetail += string.Format("{0} : {1} \n", "EMVTAG", sGetEMVTAG);

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Add AID : " + sGetEMVAID, sLogDetail);
        }

        /// <summary>
        /// Modifying AID data in database based on given value
        /// </summary>
        /// <param name="sContent">string : AID Data content which will be sent to database</param>
        protected void UpdateAID(string sContent)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileAIDUpdate, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDbId;
            oCmd.Parameters.Add("@sAIDName", SqlDbType.VarChar).Value = sGetEMVAID;
            oCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
            //oCmd.ExecuteNonQuery();

            string sLogDetail = "";
            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                while (oRead.Read())
                {
                    sLogDetail += string.Format("{0} : {1} --> {2} \n", oRead["TagName"].ToString(),
                        oRead["OldAIDValue"].ToString(), oRead["NewAIDValue"].ToString());

                }
                oRead.Close();
            }
            oCmd.Dispose();

            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Update AID : " + sEMVAID, sLogDetail);
        }
        #endregion
        
    }
}
