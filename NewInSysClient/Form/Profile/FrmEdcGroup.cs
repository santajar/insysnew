﻿using InSysClass;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace InSys
{
    public partial class FrmEdcGroup : Form
    {
        int iFrmGroupRegion;
        SqlConnection oSqlConn;
        SqlConnection oSqlConnAuditTrail;
        DataTable dtTreeNode = new DataTable();
        DataTable dtTerminalList = new DataTable();

        public FrmEdcGroup(int _iTagForm, SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            iFrmGroupRegion = _iTagForm;
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        private void FrmEdcGroup_Load(object sender, EventArgs e)
        {
            string sTitle = iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode() ? "Group" : "Region";
            this.Text = txtHeaderTree.Text = sTitle.ToUpper();
            Init();
        }

        private void tvGroupRegion_AfterSelect(object sender, TreeViewEventArgs e)
        {
            tvGroupRegion.SelectedNode = e.Node;
            if (tvGroupRegion.SelectedNode != null)
                if (!string.IsNullOrEmpty(tvGroupRegion.SelectedNode.Name))
                    FillTerminalSNList(int.Parse(tvGroupRegion.SelectedNode.Name));
                    //FillTerminalIDList(int.Parse(tvGroupRegion.SelectedNode.Name));
        }

        private void addSubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string sNewSub = null;
            TreeNode treeTemp = tvGroupRegion.SelectedNode;
            if (treeTemp != null)
            {
                string sLabel = iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode() ? GroupRegionSnType.Group.ToString() : GroupRegionSnType.Region.ToString();
                using (FrmPromptString fPromptNewSub = new FrmPromptString("Add Sub " + sLabel, "New Sub " + sLabel))
                {
                    fPromptNewSub.ShowDialog();
                    sNewSub = fPromptNewSub.txtInput.Text;
                    if (!string.IsNullOrEmpty(sNewSub))
                    {
                        using (SqlCommand oCmd = new SqlCommand(
                            iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode() ? CommonSP.sSPGroupInsert : CommonSP.sSPRegionInsert,
                            oSqlConn))
                        {
                            oCmd.CommandType = CommandType.StoredProcedure;
                            oCmd.Parameters.Add("@iParentId", SqlDbType.Int).Value = int.Parse(treeTemp.Name);
                            oCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = sNewSub;
                            oCmd.Parameters.Add("@sChildId", SqlDbType.VarChar).Value = sGetNewChildId(treeTemp.Name);
                            oCmd.Parameters.Add("@sDescription", SqlDbType.VarChar).Value = "";
                            oCmd.ExecuteNonQuery();
                            Init();
                        }
                    }
                }
            }
        }

        private void deleteSubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode treeTemp = tvGroupRegion.SelectedNode;
            if (treeTemp != null && treeTemp.Text.ToLower() != "root")
            {
                using (SqlCommand oCmd = new SqlCommand(
                        iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode() ? CommonSP.sSPGroupDelete : CommonSP.sSPRegionDelete,
                        oSqlConn))
                {
                    DataRow row = (dtTreeNode.Select(string.Format("Id={0} AND Name='{1}'", treeTemp.Name, treeTemp.Text)))[0];
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@iParentId", SqlDbType.Int).Value = int.Parse(row["ParentId"].ToString());
                    oCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = row["Name"].ToString();
                    oCmd.Parameters.Add("@sChildId", SqlDbType.VarChar).Value = row["ChildId"].ToString();
                    oCmd.ExecuteNonQuery();
                    Init();
                }
            }
        }

        private void propertiesGroupRegionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode treeTemp = tvGroupRegion.SelectedNode;
            if (treeTemp != null && !string.IsNullOrEmpty(treeTemp.Name))
            {
                GroupRegionSnType oType;
                string sTitle; 
                int iGroupId = 0;
                int iRegionId = 0;
                if (iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode())
                {
                    oType = GroupRegionSnType.Group;
                    sTitle = "Group Properties";
                    iGroupId = int.Parse(treeTemp.Name);
                }
                else
                {
                    oType = GroupRegionSnType.Region;
                    sTitle = "Region Properties";
                    iRegionId = int.Parse(treeTemp.Name);
                }
                DataRow rowSelected = (dtTreeNode.Select("Id=" + treeTemp.Name))[0];
                (new FrmEdcGroupProperties(oSqlConn, oSqlConnAuditTrail, oType.GetHashCode(), rowSelected, sTitle, ActionType.Read.GetHashCode(), iGroupId, iRegionId)).ShowDialog();
                Init();
            }
        }

        private void addTerminalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode treeTemp = tvGroupRegion.SelectedNode;
            if (treeTemp != null && !string.IsNullOrEmpty(treeTemp.Name))
            {
                GroupRegionSnType oType;
                string sTitle;
                int iGroupId = 0;
                int iRegionId = 0;
                if (iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode())
                {
                    oType = GroupRegionSnType.Group;
                    sTitle = "Group Properties";
                    iGroupId = int.Parse(treeTemp.Name);
                }
                else
                {
                    oType = GroupRegionSnType.Region;
                    sTitle = "Region Properties";
                    iRegionId = int.Parse(treeTemp.Name);
                }
                DataRow rowSelected = (dtTreeNode.Select("Id=" + treeTemp.Name))[0];
                (new FrmEdcGroupProperties(oSqlConn, oSqlConnAuditTrail, oType.GetHashCode(), rowSelected, sTitle, ActionType.Create.GetHashCode(), iGroupId, iRegionId)).ShowDialog();
                Init();
            }
        }

        //private void addTerminalToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    TreeNode treeTemp = tvGroupRegion.SelectedNode;
        //    if (treeTemp != null && treeTemp.Text.ToLower() != "root")
        //    {
        //        string sDatabaseId = null;
        //        using (FrmPromptCombo fPromptCombo = new FrmPromptCombo("Choose Database", "Choose Database"))
        //        {
        //            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref fPromptCombo.cmbPrompt);
        //            if (fPromptCombo.ShowDialog() == DialogResult.OK)
        //                sDatabaseId = fPromptCombo.cmbPrompt.SelectedValue.ToString();
        //        }
        //        if (!string.IsNullOrEmpty(sDatabaseId))
        //        {
        //            using (FrmPromptCheckedList fPromptCheckedList = new FrmPromptCheckedList("Choose TerminalID", "Choose TerminalID"))
        //            {
        //                CommonClass.FillCheckedList(oSqlConn, CommonSP.sSPTerminalListBrowse, "WHERE DatabaseID=" + sDatabaseId, "TerminalID", ref fPromptCheckedList.chkPrompt);
        //                if (fPromptCheckedList.ShowDialog() == DialogResult.OK)
        //                {
        //                    if (fPromptCheckedList.chkPrompt.CheckedItems.Count > 0)
        //                    {
        //                        string sTerminalIdList = null;
        //                        foreach (object oTerminalId in fPromptCheckedList.chkPrompt.CheckedItems)
        //                        {
        //                            if (!string.IsNullOrEmpty(sTerminalIdList))
        //                                sTerminalIdList += ";";
        //                            sTerminalIdList += oTerminalId.ToString();
        //                        }
        //                        AddUpdateGroupTerminal(true, treeTemp.Name, sTerminalIdList);
        //                    }
        //                }
        //            }
        //        }
        //        else
        //            MessageBox.Show("Please choose Database");
        //    }
        //}

        private void removeTerminalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode treeTemp = tvGroupRegion.SelectedNode;
            if (treeTemp != null && treeTemp.Text.ToLower() != "root" && lbTidList.SelectedItem != null)
            {
                string sSelectedNodeId = treeTemp.Name;
                string sSelectedTerminalId = lbTidList.SelectedItem.ToString();
                if (!string.IsNullOrEmpty(sSelectedNodeId) && !string.IsNullOrEmpty(sSelectedTerminalId))
                {
                    //string sTerminalIdList = null;
                    //foreach (var vTID in lbTidList.Items)
                    //{
                    //    if (sSelectedTerminalId != vTID.ToString())
                    //    {
                    //        if (!string.IsNullOrEmpty(sTerminalIdList))
                    //            sTerminalIdList += ";";
                    //        sTerminalIdList += vTID.ToString();
                    //    }
                    //}
                    //AddUpdateGroupTerminal(false, sSelectedNodeId, sTerminalIdList);

                    DataRow[] rows = dtTerminalList.Select(string.Format("SerialNumber='{0}' AND Name='{1}'",
                        (sSelectedTerminalId.Split('\\'))[0],(sSelectedTerminalId.Split('\\'))[1]));                    
                    using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalSNDelete,oSqlConn))
                    {
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = rows[0]["SerialNumber"].ToString();
                        oCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = rows[0]["Name"].ToString();
                        oCmd.Parameters.Add("@sTypeName", SqlDbType.VarChar).Value = rows[0]["TerminalTypeName"].ToString();
                        oCmd.ExecuteNonQuery();
                    }
                    FillTerminalSNList(int.Parse(treeTemp.Name));
                }
                Init();
            }
        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode treeTemp = tvGroupRegion.SelectedNode;
            if (treeTemp != null && !string.IsNullOrEmpty(treeTemp.Name) && lbTidList.Items.Count>0)
            {
                int iGroupId = 0;
                int iRegionId = 0;
                if (iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode())
                    iGroupId = int.Parse(treeTemp.Name);
                else
                    iRegionId = int.Parse(treeTemp.Name);
                GroupRegionSnType oType = GroupRegionSnType.Terminal;
                string sTitle = "Terminal Properties";
                if (lbTidList.SelectedIndex != -1)
                {DataRow[] rowsTerminal =
                    dtTerminalList.Select(string.Format("SerialNumber='{0}'", (lbTidList.Items[lbTidList.SelectedIndex].ToString().Split('\\'))[0]));
                (new FrmEdcGroupProperties(oSqlConn, oSqlConnAuditTrail, oType.GetHashCode(), rowsTerminal[0], sTitle, ActionType.Read.GetHashCode(), iGroupId, iRegionId)).ShowDialog();
                Init();
                }
            }
        }

        private void txtSearchTerminalID_Enter(object sender, EventArgs e)
        {
            txtSearchTerminalID.Clear();
            txtSearchTerminalID.CharacterCasing = CharacterCasing.Upper;
            txtSearchTerminalID.ForeColor = Color.Black;
        }

        private void txtSearchTerminalID_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSearchTerminalID.Text))
            {
                txtSearchTerminalID.CharacterCasing = CharacterCasing.Normal;
                txtSearchTerminalID.Text = "Terminal ID";
                txtSearchTerminalID.ForeColor = Color.Gray;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string sKeyTerminalID = txtSearchTerminalID.Text;
            TreeNode treeTemp = tvGroupRegion.SelectedNode;
            if (sKeyTerminalID.Length <= 8 && !string.IsNullOrEmpty(sKeyTerminalID) && lbTidList.Items.Count > 0)
            {
                lbTidList.Items.Clear();
                string sTerminalIDList = (dtTreeNode.Select("Id = " + treeTemp.Name))[0]["TerminalIDList"].ToString();
                if (!string.IsNullOrEmpty(sTerminalIDList))
                {
                    string[] arrsTerminalID = sTerminalIDList.Split(';');
                    if (arrsTerminalID.Length > 0)
                        foreach (string sTid in arrsTerminalID)
                            if (sTid.Contains(sKeyTerminalID))
                                lbTidList.Items.Add(sTid);
                }
            }
        }

        private void addTerminalListTerminalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TreeNode treeTemp = tvGroupRegion.SelectedNode;
            if (treeTemp != null && !string.IsNullOrEmpty(treeTemp.Name))
            {
                int iGroupId = 0;
                int iRegionId = 0;
                if (iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode())
                    iGroupId = int.Parse(treeTemp.Name);
                else
                    iRegionId = int.Parse(treeTemp.Name);
                GroupRegionSnType oType = GroupRegionSnType.Terminal;
                string sTitle = "Register Terminal";
                if (
                (new FrmEdcGroupProperties(oSqlConn, oSqlConnAuditTrail, oType.GetHashCode(), null, sTitle, ActionType.Create.GetHashCode(), iGroupId, iRegionId)).ShowDialog() == DialogResult.OK)
                    FillTerminalSNList(int.Parse(treeTemp.Name));
                Init();
            }
        }

        //private void addTerminalListTerminalToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    GroupRegionSnType oType = GroupRegionSnType.Terminal;
        //    string sTitle = "Register Terminal";
        //    (new FrmEdcGroupProperties(oSqlConn, oType.GetHashCode(), null, sTitle)).ShowDialog();
        //}

        #region "Function"
        private void Init()
        {
            InitData();
            LoadTreeNode();
            lbTidList.Items.Clear();
        }

        private void InitData()
        {
            dtTreeNode.Clear();
            using (SqlCommand oCmd = new SqlCommand(iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode() ? CommonSP.sSPGroupBrowse : CommonSP.sSPRegionBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                (new SqlDataAdapter(oCmd)).Fill(dtTreeNode);
            }
        }

        private void LoadTreeNode()
        {
            tvGroupRegion.Nodes.Clear();
            foreach (DataRow row in dtTreeNode.Rows)
            {
                string sNodeId = row["Id"].ToString();
                int iParentId = int.Parse(row["ParentId"].ToString());
                string sNodeName = row["Name"].ToString();
                string sChildId = row["ChildId"].ToString();
                string[] arrsChildId = sChildId.Split(';');

                TreeNode oNode = new TreeNode();
                oNode.Name = sNodeId;
                oNode.Text = sNodeName;
                oNode.Tag = sChildId;

                if (iParentId == 0)
                    tvGroupRegion.Nodes.Add(oNode);
                else
                {
                    TreeNode[] oNodeResult = tvGroupRegion.Nodes.Find(iParentId.ToString(), true);
                    if (oNodeResult.Length > 0)
                        oNodeResult[0].Nodes.Add(oNode);
                }
            }
            tvGroupRegion.ExpandAll();
        }

        private void FillTerminalIDList(int iNodeName)
        {
            lbTidList.Items.Clear();
            string sTerminalIDList = (dtTreeNode.Select("Id = " + iNodeName))[0]["TerminalIDList"].ToString();
            if (!string.IsNullOrEmpty(sTerminalIDList))
            {
                string[] arrsTerminalID = sTerminalIDList.Split(';');
                if (arrsTerminalID.Length > 0)
                    lbTidList.Items.AddRange(arrsTerminalID);
            }
        }

        private string sGetNewChildId(string sParentId)
        {
            string sCondition = string.Format("WHERE ChildId LIKE '{0}%' ORDER BY ChildId DESC",
                sParentId);
            string sNewChildId = null;
            string sChildId = null;
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPGroupBrowse, oSqlConn ))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                if (dtTemp != null && dtTemp.Rows.Count > 0)
                {
                    sChildId = dtTemp.Rows[0]["ChildId"].ToString();
                    string[] arrsChildId = sChildId.Split(';');
                    if (arrsChildId.Length > 0)
                    {
                        foreach (string sId in arrsChildId)
                        {
                            if (sId == arrsChildId[arrsChildId.Length - 1]) 
                                sNewChildId += (int.Parse(arrsChildId[arrsChildId.Length - 1]) + 1).ToString();
                            else sNewChildId += sId + ";";
                        }
                    }
                }
                else sNewChildId = sParentId + ";1";
            }
            return sNewChildId;
        }

        private void AddUpdateGroupTerminal(bool isAdd, string sNodeId, string sTerminalIdList)
        {
            string[] arrsTerminalIdNew = null;
            if (!string.IsNullOrEmpty(sTerminalIdList))
                arrsTerminalIdNew = sTerminalIdList.Split(';');
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand(iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode() ? CommonSP.sSPGroupBrowse : CommonSP.sSPRegionBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "WHERE GroupId=" + sNodeId;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            string sTermIdList = null;
            if (dtTemp != null && dtTemp.Rows.Count > 0)
            {
                string sTerminalIdListCurr = dtTemp.Rows[0]["TerminalIDList"].ToString();                
                if (isAdd && !string.IsNullOrEmpty(sTerminalIdListCurr))
                {
                    sTermIdList = sTerminalIdListCurr;
                    foreach (string sTID in arrsTerminalIdNew)
                    {
                        if (!sTerminalIdListCurr.Contains(sTID))
                        {
                            if (!string.IsNullOrEmpty(sTermIdList))
                                sTermIdList += ";";
                            sTermIdList += sTID;
                        }
                    }
                }
                else
                    sTermIdList = sTerminalIdList;
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPGroupTerminalScheduleUpdate, oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@iParentId", SqlDbType.Int).Value = int.Parse(dtTemp.Rows[0]["ParentId"].ToString());
                    oCmd.Parameters.Add("@sChildId", SqlDbType.VarChar).Value = dtTemp.Rows[0]["ChildId"].ToString();
                    oCmd.Parameters.Add("@sName", SqlDbType.VarChar).Value = dtTemp.Rows[0]["Name"].ToString();
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTermIdList;
                    oCmd.ExecuteNonQuery();
                }
                Init();
            }
        }
        
        private void FillTerminalSNList(int iNodeName)
        {
            lbTidList.Items.Clear();
            dtTerminalList = new DataTable();
            if (iNodeName > 1)
            {
                string sCondition = string.Format("WHERE {0}={1}", 
                    iFrmGroupRegion == GroupRegionSnType.Group.GetHashCode() ? "GroupId" : "RegionId", iNodeName);
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalSNBrowse, oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
                    
                    (new SqlDataAdapter(oCmd)).Fill(dtTerminalList);
                    if (dtTerminalList.Rows.Count > 0)
                        foreach (DataRow row in dtTerminalList.Rows)
                            lbTidList.Items.Add(string.Format(@"{0}\{1}", row["SerialNumber"], row["Name"]));
                }
            }
        }
        #endregion
    }
}