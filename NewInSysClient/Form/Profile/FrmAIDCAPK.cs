using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

using InSysClass;

namespace InSys
{
    public partial class FrmAIDCAPK : Form
    {
        /// <summary>
        /// Determine if current mode is for AID or for CAPK.
        /// </summary>
        protected bool isAID;
        protected SqlConnection oSqlConn;
        protected string sDatabaseId;
        protected string sDatabaseName;

        /// <summary>
        /// Form to shown list of AID or CAPK in current Version
        /// </summary>
        /// <param name="_isAID">boolean : Determine whether data shown is AID or CAPK, true if AID</param>
        /// <param name="_oSqlConn">SqlConnection : Connection String to database</param>
        /// <param name="_sDatabaseId">string : current version</param>
        public FrmAIDCAPK(bool _isAID, SqlConnection _oSqlConn, string _sDatabaseId, string _sDatabaseName)
        {
            InitializeComponent();
            isAID = _isAID;
            oSqlConn = _oSqlConn;
            sDatabaseId = _sDatabaseId;
            sDatabaseName = _sDatabaseName;
        }

        /// <summary>
        /// Initiate data and form's display.
        /// </summary>
        private void FrmAIDCAPK_Load(object sender, EventArgs e)
        {
            InitData();
            SetDisplay();
            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDatabaseName, CommonMessage.sFormOpened + this.Text, "");
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (sDatabaseId == "21" || sDatabaseId == "51" || sDatabaseId == "82")
            {
                if (isAID)
                {
                    FrmEditAID fAdd = new FrmEditAID(false, oSqlConn, sDatabaseId, sDatabaseName);
                    try
                    {
                        fAdd.ShowDialog();
                    }
                    finally
                    {
                        fAdd.Dispose();
                    }
                }
                else
                {

                    FrmEditCAPK fAdd = new FrmEditCAPK(false, oSqlConn, sDatabaseId, sDatabaseName);
                    try
                    {
                        fAdd.ShowDialog();
                    }
                    finally
                    {
                        fAdd.Dispose();
                    }
                }
            }
            else
            {
                if (isAID)
                {
                    FrmEditAID2 fAdd = new FrmEditAID2(false, oSqlConn, sDatabaseId, sDatabaseName);
                    try
                    {
                        fAdd.ShowDialog();
                    }
                    finally
                    {
                        fAdd.Dispose();
                    }
                }
                else
                {

                    FrmEditCAPK2 fAdd = new FrmEditCAPK2(false, oSqlConn, sDatabaseId, sDatabaseName);
                    try
                    {
                        fAdd.ShowDialog();
                    }
                    finally
                    {
                        fAdd.Dispose();
                    }
                }
            }
            SetDisplay();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (IsListAIDCAPKSelected())
            {
                if (sDatabaseId == "21" || sDatabaseId == "51" || sDatabaseId == "82")
                {
                    if (isAID)
                    {
                        FrmEditAID fEdit = new FrmEditAID(true, oSqlConn, sDatabaseId, sDatabaseName, sSelectedListAIDCAPK);
                        try
                        {
                            fEdit.ShowDialog();
                        }
                        finally
                        {
                            fEdit.Dispose();
                        }
                    }
                    else
                    {

                        FrmEditCAPK fEdit = new FrmEditCAPK(true, oSqlConn, sDatabaseId, sDatabaseName, sSelectedListAIDCAPK);
                        try
                        {
                            fEdit.ShowDialog();
                        }
                        finally
                        {
                            fEdit.Dispose();
                        }

                    }
                }
                else
                {
                    if (isAID)
                    {
                        FrmEditAID2 fEdit = new FrmEditAID2(true, oSqlConn, sDatabaseId, sDatabaseName, sSelectedListAIDCAPK);
                        try
                        {
                            fEdit.ShowDialog();
                        }
                        finally
                        {
                            fEdit.Dispose();
                        }
                    }
                    else
                    {

                        FrmEditCAPK2 fEdit = new FrmEditCAPK2(true, oSqlConn, sDatabaseId, sDatabaseName, sSelectedListAIDCAPK);
                        try
                        {
                            fEdit.ShowDialog();
                        }
                        finally
                        {
                            fEdit.Dispose();
                        }

                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (IsListAIDCAPKSelected())
            {
                if (isAID)
                {
                    DeleteAID();
                }
                else
                {
                    DeleteCAPK();
                }
                SetDisplay();
            }
        }

        private void btnTesting_Click(object sender, EventArgs e)
        {

        }

        #region "Function"
        /// <summary>
        /// Call function InitButton
        /// </summary>
        private void InitData()
        {
            InitButton();
        }

        /// <summary>
        /// Initiate buttons based on UserRights.
        /// </summary>
        private void InitButton()
        {
            btnAdd.Enabled = UserPrivilege.IsAllowed(isAID ? PrivilegeCode.AI : PrivilegeCode.PK, Privilege.Add);
            btnDelete.Enabled = UserPrivilege.IsAllowed(isAID ? PrivilegeCode.AI : PrivilegeCode.PK, Privilege.Delete);
        }

        /// <summary>
        /// Set Form's display for AID or CAPK.
        /// </summary>
        protected void SetDisplay()
        {
            if (isAID) SetAIDDisplay();
            else SetCAPKDisplay();
            ListDisplay();
        }

        /// <summary>
        /// Set Form's display form AID Management.
        /// </summary>
        protected void SetAIDDisplay()
        {
            this.Text = "AID Management";
            gbData.Text = "AID";
        }

        /// <summary>
        /// Set Form's display for CAPK Management.
        /// </summary>
        protected void SetCAPKDisplay()
        {
            this.Text = "CAPK Management";
            gbData.Text = "CAPK";
        }

        /// <summary>
        /// Fill listvew of AID or CAPK
        /// </summary>
        protected void ListDisplay()
        {
            lvAIDCAPK.Items.Clear();
            DataTable dtList = dtGetAIDCAPKList();
            foreach (DataRow drow in dtList.Rows)
                lvAIDCAPK.Items.Add(drow[0].ToString());
        }

        /// <summary>
        /// Load data from databaseid to be bounded into list view
        /// </summary>
        /// <returns>boolean : DataTable contains data from database</returns>
        protected DataTable dtGetAIDCAPKList()
        {
            DataTable dtTemp = new DataTable();
            SqlCommand oCmd = new SqlCommand();
            if (isAID)
                oCmd = new SqlCommand(CommonSP.sSPProfileAIDBrowseList, oSqlConn);
            else
                oCmd = new SqlCommand(CommonSP.sSPProfileCAPKBrowseList, oSqlConn);

            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDatabaseId.ToString();

            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            new SqlDataAdapter(oCmd).Fill(dtTemp);
            oCmd.Dispose();
            return dtTemp;
        }

        /// <summary>
        /// Determine whether some items selected from List View or not
        /// </summary>
        /// <returns>boolean : true if selected</returns>
        protected bool IsListAIDCAPKSelected()
        {
            return lvAIDCAPK.SelectedItems.Count > 0 ? true : false;
        }

        /// <summary>
        /// Value of the selected item
        /// </summary>
        protected string sSelectedListAIDCAPK
        {
            get
            {
                return lvAIDCAPK.SelectedItems[0].Text;
            }
        }

        /// <summary>
        /// Delete AID data from database
        /// </summary>
        protected void DeleteAID()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileAIDDelete, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDatabaseId;
            oCmd.Parameters.Add("@sAIDName", SqlDbType.VarChar).Value = sSelectedListAIDCAPK;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oCmd.ExecuteNonQuery();
            oCmd.Dispose();
            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDatabaseName, "Delete AID : " + sSelectedListAIDCAPK, "");
        }

        /// <summary>
        /// Delete CAPK Data from database
        /// </summary>
        protected void DeleteCAPK()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileCAPKDelete, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDatabaseId;
            oCmd.Parameters.Add("@sCAPKIndex", SqlDbType.VarChar).Value = sSelectedListAIDCAPK;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oCmd.ExecuteNonQuery();
            oCmd.Dispose();
            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDatabaseName, "Delete CAPK : " + sSelectedListAIDCAPK, "");
        }
        #endregion
    }
}