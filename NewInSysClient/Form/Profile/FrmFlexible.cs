﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;
using InSysClass;
using System.Globalization;


namespace InSys
{
    public partial class FrmFlexible : Form
    {
        #region "Variables"
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected DataTable dtItemList;
        protected DataTable dtItemListChild;
        protected DataTable dtDataProfile;
        protected DataTable dtItemListEnabled;
        protected DataTable dtProfileTerminalList;
        
        /// <summary>
        /// X coordinate when adding new object.
        /// </summary>
        protected int iX = 20;

        /// <summary>
        /// Y coordinate when adding new object.
        /// </summary>
        protected int iY = 0;

        /// <summary>
        /// Determines if data is saved or not.
        /// </summary>
        protected bool isSaving;

        /// <summary>
        /// Determines if form is in Editing mode or Adding mode.
        /// </summary>
        protected bool isEdit;

        /// <summary>
        /// Needed to determine user's view based on their privilege.
        /// </summary>
        protected bool isReadOnly = true; 

        /// <summary>
        /// Determine if any CheckBoxes at header is changed.
        /// </summary>
        protected bool isCheckBoxChanged = false;

        public string sResultTag;
        public string sActDtl;

        /// <summary>
        /// All tag values before any editing from user.
        /// </summary>
        protected string sOldTagValue = "";

        /// <summary>
        /// Current Database's ID
        /// </summary>
        public string sDbID = "";

        /// <summary>
        /// Current Version Name
        /// </summary>
        public string sDbName = "";

        /// <summary>
        /// add log in action detail variable
        /// </summary>
        public string sChkAllowDL = "";
        public string sChkStatusMaster = "";
        public string sChkEnableIP = "";
        public string sChkInitCompress = "";
        public string schkRemoteDownload = "";
        public string schkInitLogo = "";
        public string schkInitIdleScreen = "";
        public string schkLogoMerchant = "";
        public string schkLogoReceipt = "";
        /// <summary>
        /// Current FormID : for Terminal, Acquirer, Issuer, or Card.
        /// </summary>
        //protected string sFrmID = "";
        FormType oFormType;

        public bool isUpdateProfile;

        /// <summary>
        /// TLE Name
        /// </summary>
        public string sName = "";

        /// <summary>
        /// Relation for current Item.
        /// </summary>
        public string[] sArrCurrentRelations = new string[] { null, null, null, null };

        protected bool isUserClosing = false;
        protected byte[] arrbKey = (new UTF8Encoding()).GetBytes("316E67336E316330");
        #endregion

        /// <summary>
        /// Form to modify detail data of Terminal, Acquirer, Issuer, Card, or TLE
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_isEdit">Boolean : True to modify data, falset to add new data</param>
        public FrmFlexible(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, bool _isEdit)
        {
            isSaving = false;
            isEdit = _isEdit;
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        /// <summary>
        /// Initiate Form and Load data to be displayed.
        /// </summary>
        private void FrmFlexible_Load(object sender, EventArgs e)
        {
            sResultTag = "";
            iY = gbStatus.Height + 20;
            InitForm();
            if (isEdit) LoadData();
            string sLogDesc = string.Format("{0}{1}", CommonMessage.sFormOpened + this.Text, "");
            if (isEdit)
                sLogDesc += string.Format(" : {0}", sGenerateTitleRelation());
            CommonClass.InputLog(oSqlConnAuditTrail, (string.IsNullOrEmpty(sArrCurrentRelations[0])) ? "" : sArrCurrentRelations[0], 
                                            UserData.sUserID, sDbName, sLogDesc, "");
            //chkInitCompress.Enabled = chkInitCompress.Visible = true;
        }

        /// <summary>
        /// Runs while form is closing, saved or update data.
        /// </summary>
        private void FrmFlexible_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Cursor = Cursors.AppStarting;
            if (isUserClosing)
            {
                int iCustom = 0;
                if (oFormType == FormType.capk)
                    iCustom = isCustomCAPK();

                string sNewTagValue = sGetProfileContent();
                if ((sOldTagValue != sNewTagValue) || isCheckBoxChanged)
                    if ((isSaving || CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle)))
                    {
                        if (isValid())
                        {
                            sResultTag = sNewTagValue;

                            switch (oFormType)
                            {
                                case FormType.pinpad:
                                case FormType.remotedownload:
                                case FormType.aid:
                                case FormType.EMVManagement:
                                case FormType.promomanagement:
                                case FormType.initialflazz:
                                case FormType.gprs:
                                case FormType.cardtype:
                                case FormType.edcmonitoring:
                                case FormType.requestpaperreceipt:
                                case FormType.snmanagement:
                                    if (!IsSuccessSave(sResultTag)) e.Cancel = true;
                                    break;
                                case FormType.card:
                                    if (!isEdit && IsCardNameExist(sResultTag)) sResultTag = null;
                                    break;
                                case FormType.tle:
                                    if (!IsSuccessSaveTLE(sResultTag)) e.Cancel = true;
                                    break;
                                case FormType.CustomMenu:
                                    if (!IsSuccessSaveCustomMenu(sResultTag)) e.Cancel = true;
                                    break;
                                case FormType.capk:
                                    if (!IsSuccessSaveCAPK(sResultTag, iCustom)) e.Cancel = true;
                                    break;
                                default:
                                    if (isEdit) UpdateData();
                                    break;
                            }

                            //if (isEdit)
                            //    UpdateData();
                            //else
                            //{
                            //    if (oFormType == FormType.card && IsCardNameExist(sResultTag))
                            //        sResultTag = null;
                            //    else if (oFormType == FormType.tle && !IsSuccessSaveTLE(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.CustomMenu && !IsSuccessSaveCustomMenu(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.capk && !IsSuccessSaveCAPK(sResultTag, iCustom))
                            //        e.Cancel = true;

                            //    else if (oFormType == FormType.pinpad && !IsSuccessSave(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.remotedownload && !IsSuccessSave(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.aid && !IsSuccessSave(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.EMVManagement && !IsSuccessSave(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.promomanagement && !IsSuccessSave(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.initialflazz && !IsSuccessSave(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.gprs && !IsSuccessSave(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.cardtype && !IsSuccessSave(sResultTag))
                            //        e.Cancel = true;
                            //    else if (oFormType == FormType.edcmonitoring && !IsSuccessSave(sResultTag))
                            //        e.Cancel = true;
                            //    //else if (oFormType == FormType.bankcode && !IsSuccessSaveBankCode(sResultTag))
                            //    //    e.Cancel = true;
                            //    //else if (oFormType == FormType.productcode && !IsSuccessSaveProdCode(sResultTag))
                            //    //    e.Cancel = true;
                            //    //else if ((oFormType == FormType.loyaltypool || oFormType == FormType.loyaltypool) && !IsSuccessSaveLoyalty(sResultTag))
                            //    //    e.Cancel = true;
                            //    //else if (oFormType == FormType.currency && !IsSuccessSaveCurrency(sResultTag))
                            //    //    e.Cancel = true;
                            //}
                            sActDtl = sGetActionDetail();
                        }
                        else
                            e.Cancel = true;
                    }
            }
            else
                CommonClass.UserAccessDelete(oSqlConn, UserData.sTerminalIdActive);
            this.Cursor = Cursors.Default;
        }

        protected bool IsSuccessSave(string sContent)
        {
            string sErrMsg = "";
            int iTagLength = sGetTagLength();
            string sTag = null;
            string sParamSP = null;
            string sQuery = null;

            switch (oFormType)
            {
                case FormType.pinpad:
                    sTag = iTagLength == 4 ? "PP01" : "PP001";
                    sParamSP = "@sPinPadName";
                    sQuery = !isEdit ? CommonSP.sSPPinPadInsert : CommonSP.sSPPinPadUpdate;
                    break;
                case FormType.remotedownload:
                    sTag = iTagLength == 4 ? "RD01" : "RD001";
                    sParamSP = "@sRemoteDownloadName";
                    sQuery = !isEdit ? CommonSP.sSPRemoteDownloadInsert : CommonSP.sSPRemoteDownloadUpdate;
                    break;
                case FormType.aid:
                    sTag = iTagLength == 4 ? "AI01" : "AI001";
                    sParamSP = "@sAIDName";
                    sQuery = !isEdit ? CommonSP.sSPProfileAIDInsert : CommonSP.sSPProfileAIDUpdate;
                    break;
                case FormType.EMVManagement:
                    sTag = iTagLength == 4 ? "EM01" : "EM001";
                    sParamSP = "@sEMVManagementName";
                    sQuery = !isEdit ? CommonSP.sSPProfileEMVManagementInsert : CommonSP.sSPProfileEMVManagementUpdate;
                    break;
                case FormType.promomanagement:
                    sTag = iTagLength == 4 ? "PR01" : "PR001";
                    sParamSP = "@sPromoManagementName";
                    sQuery = !isEdit ? CommonSP.sSPPromoManagementInsert : CommonSP.sSPPromoManagementUpdate;
                    break;
                case FormType.initialflazz:
                    sTag = iTagLength == 4 ? "IF01" : "IF001";
                    sParamSP = "@sInitialFlazzName";
                    sQuery = !isEdit ? CommonSP.sSPInitialFlazzManagementInsert : CommonSP.sSPInitialFlazzManagementUpdate;
                    break;
                case FormType.gprs:
                    sTag = iTagLength == 4 ? "GP01" : "GP001";
                    sParamSP = "@sGPRSName";
                    sQuery = !isEdit ? CommonSP.sSPGPRSInsert : CommonSP.sSPGPRSUpdate;
                    break;
                case FormType.cardtype:
                    sTag  = iTagLength==4? "CT01" : "CT001";
                    sParamSP = "@sCardTypeName";
                    sQuery = !isEdit ? CommonSP.sSPCardTypeInsert : CommonSP.sSPCardTypeUpdate;
                    break;
                case FormType.edcmonitoring:
                    sTag = iTagLength == 4 ? "MT01" : "MT001";
                    sParamSP = "@sEdcMonitorName";
                    sQuery = !isEdit ? CommonSP.sSPProfileEdcMonitorInsert : CommonSP.sSPProfileEdcMonitorUpdate;
                    break;
                case FormType.requestpaperreceipt:
                    sTag = iTagLength == 4 ? "RQ01" : "RQ001";
                    sParamSP = "@sRequestPaperName";
                    sQuery = !isEdit ? CommonSP.sSPRequestPaperReceiptInsert : CommonSP.sSPRequestPaperReceiptUpdate;
                    break;
                case FormType.snmanagement:
                    sTag = iTagLength == 4 ? "SN01" : "SN001";
                    sParamSP = "@sName";
                    sQuery = CommonSP.sSPProfileSNInsert;
                    break;
            }

            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add(sParamSP, SqlDbType.VarChar).Value = ((TextBox)((Controls.Find(sTag, false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                        if (string.IsNullOrEmpty(sErrMsg))
                            sErrMsg = string.Format("{0} has been saved.", ((TextBox)((Controls.Find(sTag, false))[0])).Text);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg, sTag);
        }

        protected bool IsSuccessSaveCardTypeManagement(string sContent)
        {
            string sErrMsg = "";
            string sTag = sGetTagLength() == 4 ? "CT01" : "CT001";
            try
            {
                string sQuery = !isEdit ? CommonSP.sSPCardTypeInsert : CommonSP.sSPCardTypeUpdate;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add("@sCardTypeName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find(sTag, false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg, sTag);
        }

        private int isCustomCAPK()
        {
            try
            {
                DataTable dtCustomCAPK = new DataTable();

                SqlCommand oSqlCmdCustom = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
                oSqlCmdCustom.CommandType = CommandType.StoredProcedure;
                oSqlCmdCustom.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE DatabaseID = {0}", sDbID); // Load all data with no condition

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                new SqlDataAdapter(oSqlCmdCustom).Fill(dtCustomCAPK);
                string sData = dtCustomCAPK.Rows[0]["Custom_CAPK_Name"].ToString();
                return Convert.ToInt32(sData);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Validate Hash Code in CAPK
        /// </summary>
        /// <returns></returns>
        protected bool IsValidCapkHash()
        {
            string sRID = null;
            string sIndex = null;
            string sModulus = null;
            string sExponent = null;
            string sHash = null;

            foreach (DataRow drRow in dtItemList.Rows)
            {
                Control[] ctrObj = Controls.Find(drRow["Tag"].ToString().Trim(), false);
                string sValue = ((TextBox)ctrObj[0]).Text;
                switch (drRow["ItemName"].ToString().ToLower())
                {
                    case "rid": sRID = sValue; break;
                    case "index": sIndex = sValue.Substring(sValue.Length - 2); break;
                    case "modulus": sModulus = sValue; break;
                    case "exponent": sExponent = sValue; break;
                    case "hash": sHash = sValue; break;
                }
            }
            if (!string.IsNullOrEmpty(sRID)
                && !string.IsNullOrEmpty(sIndex)
                && !string.IsNullOrEmpty(sModulus)
                && !string.IsNullOrEmpty(sExponent)
                && !string.IsNullOrEmpty(sHash))
            {
                if (CommonLib.sCalcHashCAPK(sRID, sIndex, sModulus, sExponent) == sHash)
                    return true;
                else
                    return isShowMessage("HASH Code are INCORRECT", null);
            }
            else return isShowMessage("Empty Field are NOT ALLOWED", null);
        }

        #region "Load"
        /// <summary>
        /// Generate The Relation in the Form's title.
        /// Ex : TerminalID/AcquirerID/IssuerID/CardID
        /// </summary>
        /// <returns>string : Relations for Form's Title</returns>
        protected string sGenerateTitleRelation()
        {
            string sTitleRelation = "";

            for (int iIndex = 0; iIndex < sArrCurrentRelations.Length; iIndex++)
            {
                if (!string.IsNullOrEmpty(sArrCurrentRelations[iIndex]))
                    sTitleRelation += sArrCurrentRelations[iIndex];
                if ((sArrCurrentRelations.Length > iIndex + 1) && !string.IsNullOrEmpty(sArrCurrentRelations[iIndex + 1]))
                    sTitleRelation += "\\";
                else
                    break;
            }
            return sTitleRelation;
        }

        /// <summary>
        /// Determine FormID
        /// </summary>
        public string sFormID
        {
            set
            {
                //switch (sFrmID = value)
                switch (value)
                {
                    case "1":
                        oFormType = FormType.terminal;
                        isReadOnly = isEdit &&
                            !UserPrivilege.IsAllowed(PrivilegeCode.DE, Privilege.Edit);
                        break;
                    case "2":
                        oFormType = FormType.acquirer;
                        isReadOnly = isEdit && 
                            !UserPrivilege.IsAllowed(PrivilegeCode.AA, Privilege.Edit);
                        break;
                    case "3":
                        oFormType = FormType.issuer;
                        isReadOnly = isEdit && 
                            !UserPrivilege.IsAllowed(PrivilegeCode.AE, Privilege.Edit);
                        break;
                    case "4":
                        oFormType = FormType.card;
                        isReadOnly = isEdit && 
                            !UserPrivilege.IsAllowed(PrivilegeCode.RC, Privilege.Edit);
                        break;
                    case "6":
                        oFormType = FormType.tle;
                        isReadOnly = isEdit &&
                            !UserPrivilege.IsAllowed(PrivilegeCode.TL, Privilege.Edit);
                         break;
                    case "10":
                         oFormType = FormType.gprs;
                         isReadOnly = isEdit && false;
                         break;
                    case "11":
                         oFormType = FormType.pinpad;
                         isReadOnly = isEdit &&
                             !UserPrivilege.IsAllowed(PrivilegeCode.PP, Privilege.Edit);
                         break;
                    case "13":
                         oFormType = FormType.remotedownload;
                         isReadOnly = isEdit && false;
                             //!UserPrivilege.IsAllowed(PrivilegeCode.CR, Privilege.Edit);
                         break;
                    case "14":
                         oFormType = FormType.aid;
                         isReadOnly = isEdit &&
                             !UserPrivilege.IsAllowed(PrivilegeCode.AI, Privilege.Edit);
                         break;
                    case "15":
                         oFormType = FormType.capk;
                         isReadOnly = isEdit &&
                             !UserPrivilege.IsAllowed(PrivilegeCode.PK, Privilege.Edit);
                         break;
                    case "16":
                         oFormType = FormType.bankcode;
                         isReadOnly = isEdit && false;
                         break;
                    case "17":
                         oFormType = FormType.productcode;
                         isReadOnly = isEdit && false;
                         break;
                    case "19":
                         oFormType = FormType.EMVManagement;
                         isReadOnly = isEdit && false;
                         break;
                    case "20":
                         oFormType = FormType.CustomMenu;
                         isReadOnly = isEdit && false;
                         break;
                    case "21":
                         oFormType = FormType.promomanagement;
                         isReadOnly = isEdit && false;
                         break;
                    case "22":
                        oFormType = FormType.initialflazz;
                        isReadOnly = isEdit && false;
                        break;
                    case "23":
                        oFormType = FormType.cardtype;
                        isReadOnly = isEdit && false;
                        break;
                    case "24":
                        oFormType = FormType.edcmonitoring;
                        isReadOnly = isEdit && false;
                        break;
                    case "25":
                        oFormType = FormType.requestpaperreceipt;
                        isReadOnly = isEdit && false;
                        break;
                    case "26":
                        oFormType = FormType.snmanagement;
                        isReadOnly = isEdit && false;
                        break;
                        /*
                    case "8":
                        oFormType = FormType.loyaltypool;
                        isReadOnly = isEdit &&
                            !UserPrivilege.IsAllowed(PrivilegeCode.LP, Privilege.Edit);
                        break;
                    case "9":
                        oFormType = FormType.loyaltyprod;
                        isReadOnly = isEdit &&
                            !UserPrivilege.IsAllowed(PrivilegeCode.LP, Privilege.Edit);
                        break;
                        */
                }
            }
        }

        /// <summary>
        /// Set text for Title Bar.
        /// </summary>
        protected void SetTitle()
        {
            string sTitle = null;
            switch (oFormType)
            {
                case FormType.terminal: sTitle = "Terminal "; break;
                case FormType.acquirer: sTitle = "Acquirer "; break;
                case FormType.issuer: sTitle = "Issuer "; break;
                case FormType.card: sTitle = "Card "; break;
                case FormType.tle: sTitle = "TLE "; break;
                case FormType.loyaltypool: sTitle = "LOYALTY POOL "; break;
                case FormType.loyaltyprod: sTitle = "LOYALTY PRODUCT "; break;
                case FormType.gprs: sTitle = "GPRS IP "; break;
                //case FormType.currency: sTitle = "Currency "; break;
                case FormType.pinpad: sTitle = "PinPad "; break;
                case FormType.remotedownload: sTitle = "Remote Download "; break;
                case FormType.bankcode: sTitle = "Bank Code"; break;
                case FormType.productcode: sTitle = "Product Code"; break;
                case FormType.EMVManagement: sTitle = "EMV Management"; break;
                case FormType.CustomMenu: sTitle = "Custom Menu Management"; break;
                //case FormType.promomanagement: sTitle = "Promo Management"; break;
                case FormType.initialflazz: sTitle = "Initial FLAZZ Management"; break;
                case FormType.cardtype: sTitle = "Card Type Management"; break;
                case FormType.edcmonitoring: sTitle = "Edc Monitoring Management"; break;
                case FormType.requestpaperreceipt: sTitle = "Request Paper Receipt Management"; break;
                case FormType.snmanagement: sTitle = "SN Management"; break;
            }

            if (!isEdit) sTitle += "- Add";
            else
            {
                if (isReadOnly) sTitle += "- View";
                else sTitle += "- Edit";
            }
            this.Text = string.Format("{0} {1}", sTitle, sGenerateTitleRelation());
        }

        #region "LoadForm"
        /// <summary>
        /// Initiate Form, set title, add objects, and set form's display.
        /// </summary>
        protected void InitForm()
        {
            SetTitle();         //set title before doing anything...
            if (!string.IsNullOrEmpty(UserData.sGroupID))
                dtItemListEnabled = CommonClass.dtGetItemListEnabled(oSqlConn);
            if (isGetItemList())
            {
                isGetItemListChild();
                LoadForm();
            }
            SetDisplay();
        }

        /// <summary>
        /// Get data from database and create objects based on the data.
        /// </summary>
        protected void LoadForm()
        {
            DataTable oDataTableCmb = oDtGetCmbList();
            try
            {
                foreach (DataRow drRow in dtItemList.Rows)
                    switch (drRow["ObjectName"].ToString())
                    {
                        case "TextBox": CreateTextbox(drRow); break;
                        case "RadioButton": break;
                        case "CheckBox": CreateCheckBox(drRow); break;
                        case "ComboBox": CreateComboBox(drRow, oDataTableCmb); break;
                        case "DateTimePicker": CreateDateTimePicker(drRow); break;
                    }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Set Form's display.
        /// </summary>
        protected void SetDisplay()
        {
            Control[] ctrObj;

            gbStatus.Visible = (oFormType == FormType.terminal);
            ctrObj = Controls.Find(dtItemList.Rows[0]["Tag"].ToString().Trim(), false);
            if(oFormType != FormType.CustomMenu)
                ((TextBox)ctrObj[0]).ReadOnly = isEdit;
            if (oFormType == FormType.tle || oFormType == FormType.currency)
            {
                ctrObj = Controls.Find(dtItemList.Rows[1]["Tag"].ToString().Replace(" ",""), false);
                ((TextBox)ctrObj[0]).ReadOnly = isEdit;
            }
        }

        /// <summary>
        /// Create a new TextBox based on given values.
        /// </summary>
        /// <param name="drRow">DataRow : detail data for adding new TextBox</param>
        protected void CreateTextbox(DataRow drRow)
        {
            CreateLabel(drRow["ItemName"].ToString());
            
            iX += 120;
            TextBox oTxtBox = new TextBox();
            oTxtBox.Name = drRow["Tag"].ToString().Trim();
            oTxtBox.Location = new Point(iX, iY);
            oTxtBox.MaxLength = int.Parse(drRow["vMaxLength"].ToString());
            oTxtBox.CharacterCasing = (drRow["vUpperCase"].ToString().ToLower() == "true") ? CharacterCasing.Upper : CharacterCasing.Normal;
            oTxtBox.Multiline = (oTxtBox.MaxLength > 30);
            oTxtBox.Size = new Size(150, (oTxtBox.Multiline)? 80 : 20);
            oTxtBox.ScrollBars = ScrollBars.Vertical;
            //oTxtBox.ReadOnly = isReadOnly;
            oTxtBox.ReadOnly = dtItemListEnabled == null ? isReadOnly :
                dtItemListEnabled.Select(string.Format("Tag='{0}'", drRow["Tag"])).Length > 0 ? isReadOnly : true;
            if (dtItemList.Columns.Contains("vMasking"))
                if (bool.Parse(drRow["vMasking"].ToString()))
                    oTxtBox.PasswordChar = '*';
            if (!string.IsNullOrEmpty(drRow["DefaultValue"].ToString())) oTxtBox.Text = drRow["DefaultValue"].ToString();
            
            Controls.Add(oTxtBox);
            
            if (oTxtBox.Multiline) iY += 60;
            SetXY();
        }

        /// <summary>
        /// Create a new CheckBox based on given values.
        /// </summary>
        /// <param name="drRow">DataRow : detail data for adding new CheckBox</param>
        protected void CreateCheckBox(DataRow drRow)
        {
            if (drRow.Table.Columns.Contains("Parent") == true && bool.Parse(drRow["Parent"].ToString()) == true)
            {
                string sTag = drRow["Tag"].ToString().Trim();
                var dtTemp = dtItemListChild.Select(
                    string.Format("DatabaseID={0} AND TagParent='{1}'", sDbID, sTag)).AsEnumerable();
                foreach (DataRow row in dtTemp)
                {
                    CreateCheckBoxSub(row);
                }
            }
            else
            {
                iX += 120;

                CheckBox oChkBox = new CheckBox();
                oChkBox.Name = drRow["Tag"].ToString().Trim();
                oChkBox.Text = drRow["ItemName"].ToString();
                oChkBox.AutoSize = true;
                oChkBox.Checked = (drRow["DefaultValue"] == null) ? false : drRow["DefaultValue"].ToString() == "1";
                oChkBox.Location = new Point(iX, iY);
                //oChkBox.Enabled = !isReadOnly;
                oChkBox.Enabled = dtItemListEnabled == null ? !isReadOnly :
                    dtItemListEnabled.Select(string.Format("Tag='{0}'", drRow["Tag"])).Length > 0 ? !isReadOnly : false;
                Controls.Add(oChkBox);

                SetXY();
            }
        }

        protected void CreateCheckBoxSub(DataRow drRow)
        {
            iX += 120;

            CheckBox oChkBox = new CheckBox();
            oChkBox.Name = drRow["TagChild"].ToString().Trim();
            oChkBox.Text = drRow["ItemName"].ToString();
            oChkBox.AutoSize = true;
            oChkBox.Checked = (drRow["DefaultValue"] == null) ? false : drRow["DefaultValue"].ToString() == "1";
            oChkBox.Location = new Point(iX, iY);
            //oChkBox.Enabled = !isReadOnly;
            oChkBox.Enabled = dtItemListEnabled == null ? !isReadOnly :
                dtItemListEnabled.Select(string.Format("Tag='{0}'", drRow["Tag"])).Length > 0 ? !isReadOnly : false;
            Controls.Add(oChkBox);

            SetXY();
        }
        
        /// <summary>
        /// Create a new ComboBox based on given values.
        /// </summary>
        /// <param name="drRow">DataRow : detail data for adding new ComboBox</param>
        /// <param name="oDataTableCmbValue">DataTable : Value for adding ComboBox items</param>
        protected void CreateComboBox(DataRow drRow, DataTable oDataTableCmbValue)
        {
            CreateLabel(drRow["ItemName"].ToString());
            iX += 120;
            ComboBox oCmbBox = new ComboBox();
            oCmbBox.Name = drRow["Tag"].ToString().Trim();
            oCmbBox.Size = new Size(150, 20);
           
            oCmbBox.Location = new Point(iX, iY);
            oCmbBox.DropDownStyle = ComboBoxStyle.DropDown;
            oCmbBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            oCmbBox.AutoCompleteSource = AutoCompleteSource.ListItems;

            DataRow [] dr = oDataTableCmbValue.Select(sGetCmbConditions(drRow["ItemSequence"].ToString()));
            DataView dvCmb = new DataView();
            dvCmb = oDataTableCmbValue.DefaultView;
            dvCmb.RowFilter = sGetCmbConditions(drRow["ItemSequence"].ToString());
            DataTable dtCmb = dvCmb.ToTable();
            oCmbBox.DataSource = dtCmb;
            oCmbBox.DisplayMember = "DisplayValue";
            oCmbBox.ValueMember = "RealValue";
            //oCmbBox.Enabled = !isReadOnly;
            oCmbBox.Enabled = dtItemListEnabled == null ? !isReadOnly : 
                dtItemListEnabled.Select(string.Format("Tag='{0}'", drRow["Tag"])).Length > 0 ? !isReadOnly : false;
            Controls.Add(oCmbBox);
            SetXY();
        }

        protected void CreateDateTimePicker(DataRow drRow)
        {
           CreateLabel(drRow["ItemName"].ToString());
           iX += 120;
           DateTimePicker datetimepicker = new DateTimePicker();
           datetimepicker.Format = DateTimePickerFormat.Custom;
           datetimepicker.CustomFormat = "yyyy/MM/dd";
           datetimepicker.Name = drRow["Tag"].ToString().Trim(); //watch this for looping Controls[] = Objc
           datetimepicker.Size = new Size(200, 20);
           datetimepicker.Location = new Point(iX, iY);
            Controls.Add(datetimepicker);
            SetXY();
        }

        /// <summary>
        /// Create a new Label.
        /// </summary>
        /// <param name="sText">string : Used to set the Name and Text of the Label</param>
        protected void CreateLabel(string sText)
        {
            Label oLabel = new Label();
            oLabel.Name = sText;
            oLabel.Text = sText;
            oLabel.Location = new Point(iX, iY);
            oLabel.Size = new Size(120, 30);
            Controls.Add(oLabel);
        }

        /// <summary>
        /// Set X and Y coordinate for next object.
        /// </summary>
        protected void SetXY()
        {
            if (iY + 70 > gbButton.Location.Y)
            {
                iX += 200;
                iY = gbStatus.Height + 20;
            }
            else
            {
                iX -= 120;
                iY += 30;
            }
        }

        /// <summary>
        /// Get conditions string to add ComboBox items.
        /// </summary>
        /// <param name="sItemSeq">string : item sequence used to filter data</param>
        /// <returns>string : Condition string</returns>
        protected string sGetCmbConditions(string sItemSeq)
        {
            return string.Format("DatabaseID = '{0}' AND FormID = '{1}' AND ItemSequence = '{2}'",
                 sDbID, oFormType.GetHashCode(), sItemSeq);
            //return "DatabaseID = " + sDbID + " AND FormID = " + oFormType.GetHashCode() + " AND ItemSequence = " + sItemSeq ;
        }

        /// <summary>
        /// Load data for ComboBox items from database and store it at DataTable.
        /// </summary>
        /// <returns>DataTable : data from database</returns>
        protected DataTable oDtGetCmbList()
        {
            DataTable oDataTableCmbValue= new DataTable();
            try
            {
                SqlCommand oSqlCmdCmb = new SqlCommand(CommonSP.sSPCmbBrowse, oSqlConn);

                oSqlCmdCmb.CommandType = CommandType.StoredProcedure;

                oSqlCmdCmb.Parameters.Add("@sCond", SqlDbType.VarChar).Value = ""; // Load all data with no condition

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                oDataTableCmbValue.Clear();

                SqlDataReader oReadCmb = oSqlCmdCmb.ExecuteReader();

                if (oReadCmb.HasRows)
                    oDataTableCmbValue.Load(oReadCmb);

                oReadCmb.Close();
                oReadCmb.Dispose();
                oSqlCmdCmb.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return oDataTableCmbValue;
        }

        /// <summary>
        /// Determines if ItemList data from database is found or not.
        /// </summary>
        /// <returns>boolean : true if found, else false</returns>
        protected bool isGetItemList()
        {
            bool isGetData = false;
            try
            {
                dtItemList = new DataTable();

                SqlCommand oSqlCmdItem = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn);
                oSqlCmdItem.CommandType = CommandType.StoredProcedure;
                oSqlCmdItem.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sGetCondition();

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                dtItemList.Clear();

                //SqlDataReader oReadItem = oSqlCmdItem.ExecuteReader();
                //if (oReadItem.HasRows) // Has data
                //{
                //    isGetData = true;
                //    dtItemList.Load(oReadItem);
                //}
                //oReadItem.Dispose();
                //oSqlCmdItem.Dispose();
                (new SqlDataAdapter(oSqlCmdItem)).Fill(dtItemList);
                if (dtItemList != null && dtItemList.Rows.Count > 0)
                    isGetData = true;
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return isGetData;
        }

        /// <summary>
        /// Get Condtion string to filter data.
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sGetCondition()
        {
           
            return string.Format("WHERE DatabaseID = '{0}' AND FormID = '{1}' AND TAG NOT LIKE 'DC%' ORDER BY ItemSequence Asc",
                sDbID, oFormType.GetHashCode().ToString());
        }

        protected bool isGetItemListChild()
        {
            bool isGetData = false;
            try
            {
                dtItemListChild = new DataTable();

                SqlCommand oSqlCmdItem = new SqlCommand(CommonSP.sSPItemChildBrowse, oSqlConn);
                oSqlCmdItem.CommandType = CommandType.StoredProcedure;
                oSqlCmdItem.Parameters.Add("@iDatabaseId", SqlDbType.Int).Value = int.Parse(sDbID);
                oSqlCmdItem.Parameters.Add("@iFormId", SqlDbType.Int).Value = oFormType.GetHashCode();

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                dtItemListChild.Clear();

                SqlDataReader oReadItem = oSqlCmdItem.ExecuteReader();
                if (oReadItem.HasRows) // Has data
                {
                    isGetData = true;
                    dtItemListChild.Load(oReadItem);
                }
                oReadItem.Dispose();
                oSqlCmdItem.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return isGetData;
        }

        #endregion

        #region "InitData"
        /// <summary>
        /// Load data from database and fill the form.
        /// </summary>
        protected void LoadData()
        {
            dtDataProfile = dtGetData();
            if (dtDataProfile != null && dtDataProfile.Rows.Count > 0) FillForm(dtDataProfile);
        }

        /// <summary>
        /// Load data from database to displayed on form.
        /// </summary>
        /// <returns>DataTable : Data from database</returns>
        protected DataTable dtGetData()
        {
            DataTable dtData = new DataTable();
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(sGetSpBrowse(), oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGetSpBrowseCondition();
                if(oFormType == FormType.terminal || oFormType == FormType.acquirer || oFormType== FormType.issuer)
                //if (oFormType != FormType.card && oFormType != FormType.tle
                //    && oFormType != FormType.loyaltypool && oFormType != FormType.loyaltyprod
                //    && oFormType != FormType.gprs && oFormType != FormType.currency
                //    && oFormType != FormType.pinpad && oFormType != FormType.remotedownload
                //    && oFormType != FormType.aid && oFormType != FormType.capk
                //    && oFormType != FormType.bankcode && oFormType != FormType.productcode
                //    && oFormType != FormType.EMVManagement
                //    && oFormType != FormType.promomanagement
                //    && oFormType != FormType.initialflazz
                //    && oFormType != FormType.cardtype)
                    oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sArrCurrentRelations[0];

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                dtData.Clear();
                SqlDataReader oRead = oSqlCmd.ExecuteReader();
                if (oRead.HasRows) // Has data
                {
                    dtData.Load(oRead);
                    LoadDataTerminalHeader(); 
                }

                oRead.Close();
                oRead.Dispose();
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return dtData;
        }

        /// <summary>
        /// Determines which Stored Procedure would be used to get data.
        /// </summary>
        /// <returns>string : Stored Procedure used</returns>
        protected string sGetSpBrowse()
        {
            switch (oFormType)
            {
                case FormType.terminal: return CommonSP.sSPTerminalBrowse;
                case FormType.acquirer: return CommonSP.sSPAcqBrowse;
                case FormType.issuer: return CommonSP.sSPIssBrowse;
                case FormType.card: return CommonSP.sSPCardBrowse;
                case FormType.tle: return CommonSP.sSPTLEBrowse;
                case FormType.loyaltypool: return CommonSP.sSPLoyPoolBrowse;
                case FormType.loyaltyprod: return CommonSP.sSPLoyProdBrowse;
                case FormType.gprs: return CommonSP.sSPGPRSBrowse;
                case FormType.currency: return CommonSP.sSPCurrencyBrowse;
                case FormType.pinpad: return CommonSP.sSPPinPadBrowse;
                case FormType.remotedownload: return CommonSP.sSPRemoteDownloadBrowse;
                case FormType.aid: return CommonSP.sSPProfileAIDBrowse;
                case FormType.capk: return CommonSP.sSPProfileCAPKBrowse;
                case FormType.bankcode: return CommonSP.sSPProfileBankCodeBrowse;
                case FormType.productcode: return CommonSP.sSPProfileProductCodeBrowse;
                case FormType.EMVManagement: return CommonSP.sSPProfileEMVManagementBrowse;
                case FormType.CustomMenu: return CommonSP.sSPProfileCustomMenuBrowse;
                case FormType.promomanagement: return CommonSP.sSPPromoManagementBrowse;
                case FormType.initialflazz:return CommonSP.sSPInitialFlazzManagementBrowse;
                case FormType.cardtype: return CommonSP.sSPCardTypeBrowse;
                case FormType.edcmonitoring: return CommonSP.sSPProfileEdcMonitorBrowse;
                case FormType.requestpaperreceipt: return CommonSP.sSPRequestPaperReceiptBrowse;
                case FormType.snmanagement: return CommonSP.sSPProfileSNBrowse;
                default: return "Stored Procedure not Found";
            }
        }

        /// <summary>
        /// Determines which filter would be used to filtering data get from database.
        /// </summary>
        /// <returns>string : Condition string used</returns>
        protected string sGetSpBrowseCondition()
        {
            switch (oFormType)
            {
                case FormType.terminal:
                    return string.Format("WHERE TerminalID = '{0}' AND TerminalTag NOT LIKE 'DC%'", sArrCurrentRelations[0]);
                case FormType.acquirer:
                    return string.Format("WHERE TerminalID = '{0}' AND AcquirerName = '{1}'", sArrCurrentRelations[0], sName);
                case FormType.issuer:
                    return string.Format("WHERE TerminalID = '{0}' AND IssuerName = '{1}'", sArrCurrentRelations[0], sName);
                case FormType.card:
                    return string.Format("WHERE DatabaseID = '{0}' AND CardName = '{1}'", sDbID, sName);
                case FormType.tle:
                    return string.Format("WHERE DatabaseID = '{0}' AND TLEName = '{1}'", sDbID, sName);
                case FormType.loyaltypool:
                    return string.Format("WHERE DatabaseID = '{0}' AND LoyPoolName = '{1}'", sDbID, sName);
                case FormType.loyaltyprod:
                    return string.Format("WHERE DatabaseID = '{0}' AND LoyProdName = '{1}'", sDbID, sName);
                case FormType.gprs:
                    return string.Format("WHERE DatabaseID = '{0}' AND GPRSName = '{1}'", sDbID, sName);
                case FormType.currency:
                    return string.Format("WHERE DatabaseID = '{0}' AND CurrencyName = '{1}'", sDbID, sName);
                case FormType.pinpad:
                    return string.Format("WHERE DatabaseID = '{0}' AND PinPadName = '{1}'", sDbID, sName);
                case FormType.aid:
                    return string.Format("WHERE DatabaseID = '{0}' AND AIDName = '{1}'", sDbID, sName);
                case FormType.capk:
                    return string.Format("WHERE DatabaseID = '{0}' AND CAPKIndex = '{1}'", sDbID, sName);
                case FormType.remotedownload:
                    return string.Format("WHERE DatabaseID = '{0}' AND RemoteDownloadName = '{1}'", sDbID, sName);
                case FormType.bankcode:
                    return string.Format("WHERE DatabaseID = '{0}' AND BankCode = '{1}'", sDbID, sName);
                case FormType.productcode:
                    return string.Format("WHERE DatabaseID = '{0}' AND ProductCode = '{1}'", sDbID, sName);
                case FormType.EMVManagement:
                    return string.Format("WHERE DatabaseID = '{0}' AND EMVManagementName = '{1}'",sDbID,sName);
                case FormType.CustomMenu:
                    return string.Format("WHERE TerminalID = '{0}'", sArrCurrentRelations[0]);
                case FormType.promomanagement:
                    return string.Format("WHERE DatabaseID = '{0}' AND PromoManagementName = '{1}'", sDbID, sName);
                case FormType.initialflazz:
                    return string.Format("WHERE DatabaseID = '{0}' AND InitialFlazzName = '{1}'", sDbID, sName);
                case FormType.cardtype:
                    return string.Format("WHERE DatabaseID = '{0}' AND CardTypeName = '{1}'", sDbID, sName);
                case FormType.edcmonitoring:
                    return string.Format("WHERE DatabaseID = '{0}' AND Name = '{1}'", sDbID, sName);
                case FormType.requestpaperreceipt:
                    return string.Format("WHERE DatabaseID = '{0}' AND ReqName = '{1}'", sDbID, sName);
                case FormType.snmanagement:
                    return string.Format("WHERE DatabaseID = '{0}' AND SN_Name = '{1}'", sDbID, sName);
                default: return "";
            }
        }
        
        /// <summary>
        /// Get data for Header Terminal, ie : AllowDownload, EnableIP, StatusMaster.
        /// </summary>
        protected void LoadDataTerminalHeader()
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGetTerminalListCondition();
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    dtProfileTerminalList = new DataTable();
                    new SqlDataAdapter(oSqlCmd).Fill(dtProfileTerminalList);

                    if (dtProfileTerminalList.Rows.Count > 0)
                    {
                        sChkAllowDL = dtProfileTerminalList.Rows[0]["AllowDownload"].ToString().ToLower();
                        chkAllowDL.Checked = (sChkAllowDL == "true");
                        sChkEnableIP = dtProfileTerminalList.Rows[0]["EnableIP"].ToString().ToLower();
                        chkEnableIP.Checked = (sChkEnableIP == "true");
                        sChkStatusMaster = dtProfileTerminalList.Rows[0]["StatusMaster"].ToString().ToLower();
                        chkStatusMaster.Checked = (sChkStatusMaster == "true");
                        //chkAutoInit.Checked = (oRead["AutoInit"].ToString().ToLower() == "true");
                        sChkInitCompress = dtProfileTerminalList.Rows[0]["InitCompress"].ToString().ToLower();
                        chkInitCompress.Checked = (sChkInitCompress == "true");
                        if (dtProfileTerminalList.Columns.Contains("RemoteDownload"))
                        {
                            schkRemoteDownload = dtProfileTerminalList.Rows[0]["RemoteDownload"].ToString().ToLower();
                            chkRemoteDownload.Checked = (schkRemoteDownload == "true");
                        }

                        if (dtProfileTerminalList.Columns.Contains("LogoPrint"))
                        {
                            chkLogoPrint.Visible = true;
                            schkInitLogo = dtProfileTerminalList.Rows[0]["LogoPrint"].ToString().ToLower();
                            chkLogoPrint.Checked = (schkInitLogo == "true");
                        }
                        if (dtProfileTerminalList.Columns.Contains("LogoIdle"))
                        {
                            chkLogoIdle.Visible = true;
                            schkInitIdleScreen = dtProfileTerminalList.Rows[0]["LogoIdle"].ToString().ToLower();
                            chkLogoIdle.Checked = (schkInitIdleScreen == "true");
                        }

                        if (dtProfileTerminalList.Columns.Contains("LogoMerchant"))
                        {
                            chkLogoMerchant.Visible = true;
                            schkLogoMerchant = dtProfileTerminalList.Rows[0]["LogoMerchant"].ToString().ToLower();
                            chkLogoMerchant.Checked = (schkLogoMerchant == "true");
                        }

                        if (dtProfileTerminalList.Columns.Contains("LogoReceipt"))
                        {
                            chkLogoReceipt.Visible = true;
                            schkLogoReceipt = dtProfileTerminalList.Rows[0]["LogoReceipt"].ToString().ToLower();
                            chkLogoReceipt.Checked = (schkLogoReceipt == "true");
                        }

                        
                        isCheckBoxChanged = false;
                    }


                    //using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    //{
                    //    if (oRead.Read())
                    //    {
                    //        sChkAllowDL = oRead["AllowDownload"].ToString().ToLower();
                    //        chkAllowDL.Checked = (sChkAllowDL == "true");
                    //        sChkEnableIP = oRead["EnableIP"].ToString().ToLower();
                    //        chkEnableIP.Checked = (sChkEnableIP == "true");
                    //        sChkStatusMaster = oRead["StatusMaster"].ToString().ToLower();
                    //        chkStatusMaster.Checked = (sChkStatusMaster == "true");
                    //        //chkAutoInit.Checked = (oRead["AutoInit"].ToString().ToLower() == "true");
                    //        sChkInitCompress = oRead["InitCompress"].ToString().ToLower();
                    //        chkInitCompress.Checked = (sChkInitCompress == "true");
                    //        schkRemoteDownload = oRead["RemoteDownload"].ToString().ToLower();
                    //        chkRemoteDownload.Checked = (schkRemoteDownload == "true");
                    //        isCheckBoxChanged = false;
                    //    }
                    //    oRead.Close();
                    //}
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Get the condition to filter Terminal List from database.
        /// </summary>
        /// <returns>string : Condition string</returns>
        protected string sGetTerminalListCondition()
        {
            return "WHERE TerminalID = '" + sArrCurrentRelations[0] + "' AND DatabaseID = " + sDbID;
        }

        /// <summary>
        /// Fill the objects with values get from database.
        /// </summary>
        /// <param name="oDataTableData"></param>
        protected void FillForm(DataTable dtTemp)
        {
            try
            {
                foreach (DataRow drRow in dtTemp.Rows)
                {
                    string sTagValue = drRow["Value"].ToString();
                    string sTag = drRow["Tag"].ToString().Trim();
                    
                    DataRow[] drItemRow = dtItemList.Select("Tag LIKE '" + sTag + "'");
                    Control[] ctrObj;
                    sOldTagValue += CommonClass.sGenerateTLV(sTag,
                        sTagValue, drItemRow[0]["LengthOfTagLength"].ToString());
                    if (dtItemList.Columns.Contains("Parent") && bool.Parse(drItemRow[0]["Parent"].ToString()))
                    {
                        var dtTempChild = dtItemListChild.Select(
                            string.Format("DatabaseID={0} AND TagParent='{1}'", sDbID, sTag));
                        if (dtTempChild.Length > 0)
                            FillSubForm(drItemRow[0], dtTempChild, sTagValue);
                    }
                    else
                    {
                        ctrObj = Controls.Find(sTag, false);
                        switch (ctrObj[0].GetType().Name) //Determines type of objects
                        {
                            case "TextBox":
                                string sValue;
                                if (dtItemList.Columns.Contains("vMasking"))
                                {
                                    if (bool.Parse(drItemRow[0]["vMasking"].ToString()))
                                    {
                                        try
                                        {
                                            sValue = EncryptionLib.Decrypt3DES(CommonLib.sHexToStringUTF8(sTagValue), arrbKey);
                                        }
                                        catch(Exception)
                                        {
                                            sValue = "";
                                        }
                                    }
                                    else sValue = sTagValue;
                                }else sValue = sTagValue;
                                
                                ((TextBox)ctrObj[0]).Text = sValue;

                                //((TextBox)ctrObj[0]).Text = sTagValue;
                                break;
                            case "RadioButton": break;
                            case "CheckBox": ((CheckBox)ctrObj[0]).Checked = (drRow["Value"].ToString() == "1" ? true : false); break;
                            case "ComboBox": ((ComboBox)ctrObj[0]).SelectedValue = drRow["Value"].ToString(); break;
                            case "DateTimePicker": ((DateTimePicker)ctrObj[0]).Text = Convert.ToDateTime(drRow["Value"].ToString()).ToShortDateString(); break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                string sErrMsg = "There are some invalid Tags in Terminal. Please delete and recreate Terminal ID.";
                CommonClass.doWriteErrorFile(sErrMsg);
                this.Close();
            }
        }

        protected void FillSubForm(DataRow drParent, DataRow[] arrdrChild, string sValue)
        {
            Control[] ctrObj;
            int iOffset = 0;
            int iMaxLength = int.Parse( drParent["vMaxLength"].ToString());
            foreach (DataRow row in arrdrChild)
            {
                string sTempValue =
                    iOffset < sValue.Length ?
                    sValue.Substring(iOffset, iMaxLength) : "";
                string sTagChild = row["TagChild"].ToString().Trim();
                ctrObj = Controls.Find(sTagChild, false);
                switch (ctrObj[0].GetType().Name) //Determines type of objects
                {
                    case "TextBox": ((TextBox)ctrObj[0]).Text = sTempValue; break;
                    case "RadioButton": break;
                    case "CheckBox": ((CheckBox)ctrObj[0]).Checked = (sTempValue == "1" ? true : false); break;
                    case "ComboBox": ((ComboBox)ctrObj[0]).SelectedValue = sTempValue; break;
                    case "DateTimePicker": ((DateTimePicker)ctrObj[0]).Text = sTempValue; break;
                }
                iOffset += iMaxLength;
            }
        }
        #endregion

        #endregion

        #region "Button"
        /// <summary>
        /// Set isSaving status to true and close the form.
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            isUserClosing = true;
            isSaving = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            isUserClosing = true;
            isSaving = false;
        }

        /// <summary>
        /// Determines wheter user's input is valid.
        /// </summary>
        /// <returns>boolean : true if valid, else false</returns>
        protected bool isValid()
        {
            foreach (DataRow drRow in dtItemList.Rows)
            {
                Control[] ctrObj = Controls.Find(drRow["Tag"].ToString().Trim(), false);
                if (ctrObj.Length > 0)
                    switch (ctrObj[0].GetType().Name)
                    {
                        case "TextBox": if (!isTextBoxValid((TextBox)ctrObj[0], drRow)) return false; break;
                        case "RadioButton": break;
                    }
            }
            foreach (DataRow drRow in dtItemListChild.Rows)
            {

                Control[] ctrObj = Controls.Find(drRow["TagChild"].ToString().Trim(), false);
                if (ctrObj.Length > 0)
                    switch (ctrObj[0].GetType().Name)
                    {
                        case "TextBox": if (!isTextBoxValid((TextBox)ctrObj[0], drRow)) return false; break;
                        case "RadioButton": break;
                    }
            }
            return true;
        }

        /// <summary>
        /// Determines if data in textbox is valid.
        /// </summary>
        /// <param name="txtBox">TextBox : Control which hold the value</param>
        /// <param name="drRow">DataRow : Data to be validated</param>
        /// <returns>boolean : true if valid, else false</returns>
        protected bool isTextBoxValid(TextBox txtBox, DataRow drRow)
        {
            string sErrMsg = drRow["ValidationMsg"].ToString() + "\n";
            string sItemName = drRow["ItemName"].ToString();
            if (!isEdit)
            {
                switch (txtBox.Name)
                {
                    case "DE01": // If Terminal
                        sArrCurrentRelations[0] = txtBox.Text;
                        break;
                    default:
                        sName = txtBox.Text;
                        break;
                }
            }
            
            if (isNullorEmpty(txtBox.Text, drRow)) // tidak boleh kosong atau null
            {
                sErrMsg += sItemName + " is still empty. Please fill the " + sItemName;
            }
            else if (isNotPassMinLengthLimit(txtBox.Text, drRow)) // length < min length
            {
                sErrMsg += sItemName + " should be " + drRow["vMinLength"].ToString() + " digit(s)";
            }
            else if (isFormatNotValid(txtBox.Text, drRow)) // validasi format; A : alphanumeric, N : numeric, T : time, S : kec ~,`,^
            {
                sErrMsg += sItemName + " format is invalid";
            }
            else if (isPassMinValueLimit(txtBox.Text, drRow)) // validasi max value
            {
                sErrMsg += sItemName + " exceed minimum value (Min. Value is " + drRow["vMinValue"].ToString() + ")";
            }
            else if (isPassMaxValueLimit(txtBox.Text, drRow)) // validasi max value
            {
                sErrMsg += sItemName + " exceed maximum value (Max. Value is " + drRow["vMaxValue"].ToString() + ")";
            }
            else if (isIdNotValid(txtBox.Name, txtBox.Text)) // validasi double ID
            {
                sErrMsg += sItemName + " has been used. Please choose another " + sItemName;
            }
            else
                return true;

            CommonClass.doShowErrorMessage(sErrMsg, txtBox, ObjectType.TEXTBOX, ErrorType.ERROR);
            return false;
        }

        /// <summary>
        /// Determines wheter user's input is null/empty or not.
        /// </summary>
        /// <param name="sText">string : User's input</param>
        /// <param name="drRow">DataRow : Validation from database</param>
        /// <returns>boolean : true if empty, else false</returns>
        protected bool isNullorEmpty(string sText, DataRow drRow)
        {
            return string.IsNullOrEmpty(sText.Trim()) && drRow["vAllowNull"].ToString().ToLower() == "false";
        }

        /// <summary>
        /// Determines if user's input length has pass its Min Length.
        /// </summary>
        /// <param name="sText">string : User's input</param>
        /// <param name="drRow">DataRow : Validation from database</param>
        /// <returns>boolean : true if not pass, else false</returns>
        protected bool isNotPassMinLengthLimit(string sText, DataRow drRow)
        {
            if (drRow["vAllowNull"].ToString().ToLower() == "true" && string.IsNullOrEmpty(sText.Trim())) return false;
            else return sText.Length < int.Parse(drRow["vMinLength"].ToString());
        }

        /// <summary>
        /// Determines wheter user's input match its desired format.
        /// </summary>
        /// <param name="sText">string : User's input</param>
        /// <param name="drRow">DataRow : Validation from database</param>
        /// <returns>boolean : true if format not valid, else false</returns>
        protected bool isFormatNotValid(string sText, DataRow drRow)
        {
            if (string.IsNullOrEmpty(sText.Trim()) && drRow["vAllowNull"].ToString().ToLower() == "true" ) return false;
            else return !CommonClass.isFormatValid(sText, drRow["vType"].ToString());
        }

        /// <summary>
        /// Determines wheter user's input pass its maximum value or not.
        /// </summary>
        /// <param name="sText">string : User's input</param>
        /// <param name="drRow">DataRow : Validation from database</param>
        /// <returns>boolean : true if exceeded, else false</returns>
        protected bool isPassMaxValueLimit(string sText, DataRow drRow)
        {
            return drRow["vType"].ToString().ToUpper() == "N" && drRow["vMaxValue"].ToString() != "0" &&
                        int.Parse(sText) > int.Parse(drRow["vMaxValue"].ToString());
        }

        /// <summary>
        /// Determines wheter user's input pass its maximum value or not.
        /// </summary>
        /// <param name="sText">string : User's input</param>
        /// <param name="drRow">DataRow : Validation from database</param>
        /// <returns>boolean : true if exceeded, else false</returns>
        protected bool isPassMinValueLimit(string sText, DataRow drRow)
        {
            return drRow["vType"].ToString().ToUpper() == "N" && drRow["vMinValue"].ToString() != "0" &&
                        int.Parse(sText) < int.Parse(drRow["vMinValue"].ToString());
        }

        /// <summary>
        /// Determines wheter user's input for ID is valid or not.
        /// </summary>
        /// <param name="sText">string : User's input</param>
        /// <param name="drRow">DataRow : Validation from database</param>
        /// <returns>boolean : true if not valid, else false</returns>
        protected bool isIdNotValid(string sText, string sID)
        {
            return !isEdit &&
                (
                    sText == "DE001" ||    //Terminal
                    sText == "AA001" ||    //Acquirer
                    sText == "AE001" ||    //Issuer
                    sText == "AC001" ||     //Card
                    sText == "DE01" ||    //Terminal
                    sText == "AA01" ||    //Acquirer
                    sText == "AE01" ||    //Issuer
                    sText == "AC01"
                ) && (isIdFound(sText, sID));
        }

        /// <summary>
        /// Determines wheter ID is unique or not.
        /// </summary>
        /// <param name="sTag">string : Tag to Determine type of id (Terminal, Acquirer, etc)</param>
        /// <param name="sID">string : ID to search</param>
        /// <returns>boolean : true if found, else false</returns>
        protected bool isIdFound (string sTag,string sID)
        {
            bool isFound = false;

            DataTable oDataTable = new DataTable();
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPRelationBrowseCondition, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionListName(sTag, sID);
            SqlDataReader oRead = oSqlCmd.ExecuteReader();
            isFound = oRead.HasRows;
            oRead.Close();
            oRead.Dispose();
            oSqlCmd.Dispose();
            return isFound;
        }

        /// <summary>
        /// Get the condition to filter ListName for Terminal, Acquirer, Issuer, or Card.
        /// </summary>
        /// <param name="sTag">string : Tag to Determine type of id (Terminal, Acquirer, etc)</param>
        /// <param name="sValue">string : Filtering's keyword</param>
        /// <returns>string : Condition string</returns>
        protected string sConditionListName(string sTag, string sValue)
        {
            switch (sTag)
            {
                //case "DE01": return " WHERE TERMINALID = '" + sValue + "'";
                //case "AA01": return "WHERE TERMINALID = '" + sArrCurrentRelations[0] + "' AND RelationTag = 'AD03' AND RelationTagValue = '" + sValue + "'";
                //case "AE01": return "WHERE TERMINALID = '" + sArrCurrentRelations[0] + "' AND RelationTag = 'AD02' AND RelationTagValue = '" + sValue + "'";
                case "DE01": return " WHERE TERMINALID = '" + sValue + "'";
                case "AA01": return "WHERE TERMINALID = '" + sArrCurrentRelations[0] + "' AND TagAcquirer = 'AD03' AND ValueAcquirer = '" + sValue + "'";
                case "AE01": return "WHERE TERMINALID = '" + sArrCurrentRelations[0] + "' AND TagIssuer = 'AD02' AND ValueIssuer = '" + sValue + "'";
                case "AC01": return "WHERE 1 = 2";

                case "DE001": return " WHERE TERMINALID = '" + sValue + "'";
                case "AA001": return "WHERE TERMINALID = '" + sArrCurrentRelations[0] + "' AND TagAcquirer = 'AD03' AND ValueAcquirer = '" + sValue + "'";
                case "AE001": return "WHERE TERMINALID = '" + sArrCurrentRelations[0] + "' AND TagIssuer = 'AD02' AND ValueIssuer = '" + sValue + "'";
                case "AC001": return "WHERE 1 = 2";
                default: return "";
            }
        }

        /// <summary>
        /// Get the profile detailed data.
        /// </summary>
        /// <returns>string : Profile detailed data as one message</returns>
        protected string sGetProfileContent()
        {
            
            string sNewTagValue = "";
            string sTag = "";
            string sLength = "";
            foreach (DataRow drRow in dtItemList.Select("", "Tag Asc"))
            {
                Control[] ctrObj;
                sTag = drRow["Tag"].ToString();
                if (dtItemList.Columns.Contains("Parent") && bool.Parse(drRow["Parent"].ToString()))
                {
                    var dtTempChild = dtItemListChild.Select(
                        string.Format("DatabaseID={0} AND TagParent='{1}'", sDbID, sTag));
                    if (dtTempChild.Length > 0)
                        sNewTagValue += CommonClass.sGenerateTLV(sTag, sGetProfileContentSub(dtTempChild), sLength);
                }
                else 
                {
                    ctrObj = Controls.Find(drRow["Tag"].ToString().Trim(), false);
                    sLength = drRow["LengthOfTagLength"].ToString();
                    switch (ctrObj[0].GetType().Name)
                    {

                        case "TextBox":
                            //sNewTagValue +=
                            //    CommonClass.sGenerateTLV(((TextBox)ctrObj[0]).Name, ((TextBox)ctrObj[0]).Text, sLength);

                            if (dtItemList.Columns.Contains("vMasking"))
                            {
                                sNewTagValue += !bool.Parse(drRow["vMasking"].ToString()) ?
                                   CommonClass.sGenerateTLV(((TextBox)ctrObj[0]).Name, ((TextBox)ctrObj[0]).Text, sLength) :
                                   CommonClass.sGenerateTLV(
                                       ((TextBox)ctrObj[0]).Name,
                                       CommonLib.sStringToHex(
                                           EncryptionLib.Encrypt3DES(
                                               ((TextBox)ctrObj[0]).Text,
                                               arrbKey)),
                                   sLength);
                            }
                            else
                            {
                                sNewTagValue +=
                                    CommonClass.sGenerateTLV(((TextBox)ctrObj[0]).Name, ((TextBox)ctrObj[0]).Text, sLength);
                            }
                            break;
                        case "RadioButton":
                            break;
                        case "CheckBox":
                            sNewTagValue +=
                                CommonClass.sGenerateTLV(((CheckBox)ctrObj[0]).Name, ((CheckBox)ctrObj[0]).Checked ? "1" : "0", sLength);
                            break;
                        case "ComboBox":
                            sNewTagValue +=
                                CommonClass.sGenerateTLV(((ComboBox)ctrObj[0]).Name,
                                (((ComboBox)ctrObj[0]).SelectedValue == null) ? "" : ((ComboBox)ctrObj[0]).SelectedValue.ToString(),
                                sLength);
                            break;
                        case "DateTimePicker":
                            sNewTagValue +=
                                CommonClass.sGenerateTLV(((DateTimePicker)ctrObj[0]).Name, ((DateTimePicker)ctrObj[0]).Text.ToString(), sLength);
                            break;
                                
                    }
                }
            }
            return sNewTagValue;
        }

        protected string sGetProfileContentSub(DataRow[] arrdrTemp)
        {
            string sValue = "";

            foreach (DataRow row in arrdrTemp)
            {
                Control[] ctrObj = Controls.Find(row["TagChild"].ToString().Trim(), false);
                switch (ctrObj[0].GetType().Name)
                {
                    case "TextBox":
                        sValue += ((TextBox)ctrObj[0]).Text;
                        break;
                    case "RadioButton":
                        break;
                    case "CheckBox":
                        sValue += ((CheckBox)ctrObj[0]).Checked ? "1" : "0";
                        break;
                    case "ComboBox":
                        sValue +=
                            (((ComboBox)ctrObj[0]).SelectedValue == null) ? "" : ((ComboBox)ctrObj[0]).SelectedValue.ToString();
                        break;
                    case "DateTimePicker":
                        //sValue += ((DateTimePicker)ctrObj[0]).Value.ToString();
                        sValue += ((DateTimePicker)ctrObj[0]).Text;
                        break;
                }
            }
            return sValue;
        }

        /// <summary>
        /// Update changes made by user to database.
        /// </summary>
        protected void UpdateData()
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(sGetUpdateSP(), oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    string sTerminalId = null;
                    switch (oFormType)
                    {
                        case FormType.terminal:
                            Control[] oCtr;
                            if (sGetTagLength() == 4)
                                oCtr = Controls.Find("DE01", false);
                            else
                                oCtr = Controls.Find("DE001", false);
                            sTerminalId = ((TextBox)oCtr[0]).Text;
                            oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                            oSqlCmd.Parameters.Add("@sDbID", SqlDbType.Int).Value = sDbID;
                            oSqlCmd.Parameters.Add("@bAllowDownload", SqlDbType.Bit).Value = chkAllowDL.Checked;
                            oSqlCmd.Parameters.Add("@bStatus", SqlDbType.Bit).Value = chkStatusMaster.Checked;
                            oSqlCmd.Parameters.Add("@bEnableIP", SqlDbType.Bit).Value = chkEnableIP.Checked;
                            oSqlCmd.Parameters.Add("@bInitCompress", SqlDbType.Bit).Value = chkInitCompress.Checked;
                            if (dtProfileTerminalList.Columns.Contains("RemoteDownload"))
                                oSqlCmd.Parameters.Add("@bRemoteDownload", SqlDbType.Bit).Value = chkRemoteDownload.Checked;
                            if (dtProfileTerminalList.Columns.Contains("LogoPrint"))
                                oSqlCmd.Parameters.Add("@bLogoPrint", SqlDbType.Bit).Value = chkLogoPrint.Checked;
                            if (dtProfileTerminalList.Columns.Contains("LogoIdle"))
                                oSqlCmd.Parameters.Add("@bLogoIdle", SqlDbType.Bit).Value = chkLogoIdle.Checked;
                            if (dtProfileTerminalList.Columns.Contains("LogoMerchant"))
                                oSqlCmd.Parameters.Add("@bLogoMerchant", SqlDbType.Bit).Value = chkLogoMerchant.Checked;
                            if (dtProfileTerminalList.Columns.Contains("LogoReceipt"))
                                oSqlCmd.Parameters.Add("@bLogoReceipt", SqlDbType.Bit).Value = chkLogoReceipt.Checked;

                            isUpdateProfile = true;
                            break;
                        //case FormType.loyaltypool:
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sLoyPoolName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("LA01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sLoyPoolName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("LA001", false))[0])).Text;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = sDbID;
                        //    break;
                        //case FormType.loyaltyprod:
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sLoyProdName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("LB01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sLoyProdName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("LB001", false))[0])).Text;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = sDbID;
                        //    break;
                        //case FormType.gprs:
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sGPRSName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("GP01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sGPRSName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("GP001", false))[0])).Text;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = sDbID;
                        //    break;
                        //case FormType.remotedownload:
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sRemoteDownloadName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("RD01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sRemoteDownloadName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("RD001", false))[0])).Text;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = sDbID;
                        //    break;
                        //case FormType.pinpad:
                        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sPinPadName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("PP01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sPinPadName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("PP001", false))[0])).Text;
                        //    break;
                        //case FormType.aid:
                        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sAIDName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("AI01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sAIDName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("AI001", false))[0])).Text;
                        //    break;
                        //case FormType.capk:
                        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sCAPKIndex", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("PK01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sCAPKIndex", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("PK001", false))[0])).Text;
                        //    break;
                        //case FormType.bankcode:
                        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sBankCode", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("KB01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sBankCode", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("KB001", false))[0])).Text;
                        //    break;
                        //case FormType.productcode:
                        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sProductCode", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("KP01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sProductCode", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("KP001", false))[0])).Text;
                        //    break;
                        //case FormType.EMVManagement:
                        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sEMVManagementName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("EM01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sEMVManagementName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("EM001", false))[0])).Text;
                        //    break;
                        //case FormType.promomanagement:
                        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sPromoManagementName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("PR01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sPromoManagementName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("PR001", false))[0])).Text;
                        //    break;
                        //case FormType.initialflazz:
                        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sInitialFlazzName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("IF01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sInitialFlazzName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("IF001", false))[0])).Text;
                        //    break;
                        //case FormType.cardtype:
                        //    oSqlCmd.CommandType = CommandType.StoredProcedure;
                        //    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                        //    if (sGetTagLength() == 4)
                        //        oSqlCmd.Parameters.Add("@sCardTypeName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("CT01", false))[0])).Text;
                        //    else
                        //        oSqlCmd.Parameters.Add("@sCardTypeName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("CT001", false))[0])).Text;
                        //    break;
                        default:
                            sTerminalId = sArrCurrentRelations[0];
                            oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                            isUpdateProfile = true;
                            break;
                    }                   

                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sResultTag;

                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn.Close();
                        oSqlConn.Open();
                    }

                    oSqlCmd.ExecuteNonQuery();

                    if (oFormType == FormType.remotedownload)
                    {
                        MessageBox.Show(string.Format("{0} has been updated.", sGetTagLength() == 4 ? ((TextBox)((Controls.Find("RD01", false))[0])).Text : ((TextBox)((Controls.Find("RD001", false))[0])).Text));
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Determines which Stored Procedure would be used to update data.
        /// </summary>
        /// <returns>string : Stored Procedure used</returns>
        protected string sGetUpdateSP()
        {
            switch (oFormType)
            {
                case FormType.terminal: return CommonSP.sSPTerminalUpdate;
                case FormType.acquirer: return CommonSP.sSPAcqUpdate;
                case FormType.issuer: return CommonSP.sSPIssUpdate;
                //case FormType.loyaltypool: return CommonSP.sSPLoyPoolUpdate;
                //case FormType.loyaltyprod: return CommonSP.sSPLoyProdUpdate;
                //case FormType.gprs: return CommonSP.sSPGPRSUpdate;
                //case FormType.remotedownload: return CommonSP.sSPRemoteDownloadUpdate;
                //case FormType.pinpad: return CommonSP.sSPPinPadUpdate;
                //case FormType.aid: return CommonSP.sSPProfileAIDUpdate;
                //case FormType.capk: return CommonSP.sSPProfileCAPKUpdate;
                //case FormType.bankcode: return CommonSP.sSPProfileBankCodeUpdate;
                //case FormType.productcode: return CommonSP.sSPProfileProductCodeUpdate;
                //case FormType.EMVManagement: return CommonSP.sSPProfileEMVManagementUpdate;
                //case FormType.promomanagement: return CommonSP.sSPPromoManagementUpdate;
                //case FormType.initialflazz: return CommonSP.sSPInitialFlazzManagementUpdate;
                //case FormType.cardtype: return CommonSP.sSPCardTypeUpdate;
                default: return "Stored Procedure Not Found";
            }
        }

        /// <summary>
        /// Runs when EnableIP is changed, to set isCheckBoxChanged status.
        /// </summary>
        private void chkEnableIP_CheckedChanged(object sender, EventArgs e)
        {
            isCheckBoxChanged = true;
        }

        /// <summary>
        /// Runs when AutoInit is changed, to set isCheckBoxChanged status.
        /// </summary>
        private void chkAutoInit_CheckedChanged(object sender, EventArgs e)
        {
            isCheckBoxChanged = true;
        }

        /// <summary>
        /// Runs when StatusMaster is changed, to set isCheckBoxChanged status.
        /// </summary>
        private void chkStatusMaster_CheckedChanged(object sender, EventArgs e)
        {
            isCheckBoxChanged = true;
        }

        /// <summary>
        /// Runs when AllowDl is changed, to set isCheckBoxChanged status.
        /// </summary>
        private void chkAllowDL_CheckedChanged(object sender, EventArgs e)
        {
            isCheckBoxChanged = true;
        }

        /// <summary>
        /// Runs when InitCompress is changed, to set isCheckBoxChanged status.
        /// </summary>
        private void chkInitCompress_CheckedChanged(object sender, EventArgs e)
        {
            isCheckBoxChanged = true;
        }

        /// <summary>
        /// Runs when Remote Download is changed, to set isCheckBoxChanged status.
        /// </summary>
        private void chkRemoteDownload_CheckedChanged(object sender, EventArgs e)
        {
            isCheckBoxChanged = true;
        }

        /// <summary>
        /// Runs when Remote Download is changed, to set isCheckBoxChanged status.
        /// </summary>
        private void chkInitLogo_CheckedChanged(object sender, EventArgs e)
        {
            isCheckBoxChanged = true;
        }

        /// <summary>
        /// Runs when Remote Download is changed, to set isCheckBoxChanged status.
        /// </summary>
        private void chkInitIdleScreen_CheckedChanged(object sender, EventArgs e)
        {
            isCheckBoxChanged = true;
        }

        /// <summary>
        /// Determine whether Card Name already exist in current version
        /// </summary>
        /// <param name="sCardValue">String : Card Name</param>
        /// <returns>Boolean : true if Card Name already exist</returns>
        protected bool IsCardNameExist(string sCardValue)
        {
            string sCardName = CommonLib.sGetTagValue(sCardValue, "AC01", 2);
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCardBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sGetCardNameCondition(sCardName);
            SqlDataReader oRead = oSqlCmd.ExecuteReader();
            bool isResultRow = oRead.HasRows;
            oRead.Close();
            oSqlCmd.Dispose();
            return isResultRow;
        }

        /// <summary>
        /// Determine whether TLE saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of TLE data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSaveTLE(string sContent)
        {
            string sErrMsg = "";
            string sTLEID = "";
            int iLengthTag = sGetTagLength();
            if (iLengthTag == 4)
            {
                sName = CommonLib.sGetTagValueNew(sContent, "TL01", 2, iLengthTag);
                sTLEID = CommonLib.sGetTagValueNew(sContent, "TL02", 2, iLengthTag);
            }
            else
            {
                sName = CommonLib.sGetTagValueNew(sContent, "TL001", 2, iLengthTag);
                sTLEID = CommonLib.sGetTagValueNew(sContent, "TL002", 2, iLengthTag);
            }

            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(sGetSpTLE(), oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
                    oSqlCmd.Parameters.Add("@sTLEName", SqlDbType.VarChar).Value = sName;
                    oSqlCmd.Parameters.Add("@sTLID", SqlDbType.VarChar).Value = sTLEID;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }

            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether Loyalty saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of Loyalty data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSaveLoyalty(string sContent)
        {
            string sErrMsg = "";
            try
            {
                string sQuery = oFormType == FormType.loyaltypool ? CommonSP.sSPLoyPoolInsert : CommonSP.sSPLoyProdInsert;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
                    if (oFormType == FormType.loyaltypool)
                        oSqlCmd.Parameters.Add("@sLoyPoolName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("LA01", false))[0])).Text;
                    else if (oFormType == FormType.loyaltyprod)
                        oSqlCmd.Parameters.Add("@sLoyProdName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find("LB01", false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }

            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether GPRS saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of GPRS data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSaveGPRS(string sContent)
        {
            string sErrMsg = "";
            string sTag = sGetTagLength() == 4 ? "GP01" : "GP001";
            try
            {
                string sQuery = CommonSP.sSPGPRSInsert;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
                    oSqlCmd.Parameters.Add("@sGPRSName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find(sTag, false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }

            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether Currency saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of Currency data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSaveCurrency(string sContent)
        {
            string sErrMsg = "";
            string sCurrencyId = CommonLib.sGetTagValue(sContent, "CR01", 2);
            sName = CommonLib.sGetTagValue(sContent, "CR02", 2);
            try
            {
                string sQuery = sGetSPCurrency();
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
                    oSqlCmd.Parameters.Add("@iCurrencyID", SqlDbType.VarChar).Value = sCurrencyId;
                    oSqlCmd.Parameters.Add("@sCurrencyName", SqlDbType.VarChar).Value = sName;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }

            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether Pinpad saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of Pinpad data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSavePinPad(string sContent)
        {
            string sErrMsg = "";
            try
            {
                string sQuery = CommonSP.sSPPinPadInsert;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add("@sPinPadName", SqlDbType.VarChar).Value = sGetTagLength() == 4 ? ((TextBox)((Controls.Find("PP01", false))[0])).Text : ((TextBox)((Controls.Find("PP001", false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether Remote Download saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of Remote Download data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSaveRemoteDownload(string sContent)
        {
            string sErrMsg = "";
            try
            {
                string sQuery = CommonSP.sSPRemoteDownloadInsert;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add("@sRemoteDownloadName", SqlDbType.VarChar).Value = sGetTagLength() == 4 ? ((TextBox)((Controls.Find("RD01", false))[0])).Text : ((TextBox)((Controls.Find("RD001", false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();

                        if (string.IsNullOrEmpty(sErrMsg))
                            sErrMsg = string.Format("{0} has been saved.", sGetTagLength() == 4 ? ((TextBox)((Controls.Find("RD01", false))[0])).Text : ((TextBox)((Controls.Find("RD001", false))[0])).Text);

                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether AID saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of AID data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSaveAID(string sContent)
        {
            string sErrMsg = "";
            try
            {
                string sQuery = !isEdit ? CommonSP.sSPProfileAIDInsert : CommonSP.sSPProfileAIDUpdate;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add("@sAIDName", SqlDbType.VarChar).Value = sGetTagLength() == 4 ? ((TextBox)((Controls.Find("AI01", false))[0])).Text : ((TextBox)((Controls.Find("AI001", false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether CAPK saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of CAPK data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSaveCAPK(string sContent, int iCostum)
        {
            string sErrMsg = "";
            try
            {
                string sQuery = !isEdit ? CommonSP.sSPProfileCAPKInsert : CommonSP.sSPProfileCAPKUpdate;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    if (iCostum == 1)
                    {
                        if (!isEdit)
                        {
                            oSqlCmd.Parameters.Add("@sCAPKName", SqlDbType.VarChar).Value = sGetTagLength() == 4 ?
                                string.Format("{0} - {1}", ((TextBox)((Controls.Find("PK01", false))[0])).Text, ((TextBox)((Controls.Find("PK02", false))[0])).Text)
                                :
                                string.Format("{0} - {1}", ((TextBox)((Controls.Find("PK001", false))[0])).Text, ((TextBox)((Controls.Find("PK002", false))[0])).Text);
                        }
                        else
                        {
                            oSqlCmd.Parameters.Add("@sCAPKIndex", SqlDbType.VarChar).Value = sGetTagLength() == 4 ?
                                string.Format("{0} - {1}", ((TextBox)((Controls.Find("PK01", false))[0])).Text, ((TextBox)((Controls.Find("PK02", false))[0])).Text)
                                :
                                string.Format("{0} - {1}", ((TextBox)((Controls.Find("PK001", false))[0])).Text, ((TextBox)((Controls.Find("PK002", false))[0])).Text);
                        }
                    }
                    else
                    {
                        if (!isEdit)
                        {
                            oSqlCmd.Parameters.Add("@sCAPKName", SqlDbType.VarChar).Value = sGetTagLength() == 4 ? ((TextBox)((Controls.Find("PK01", false))[0])).Text : ((TextBox)((Controls.Find("PK001", false))[0])).Text;
                        }
                        else
                        {
                            oSqlCmd.Parameters.Add("@sCAPKIndex", SqlDbType.VarChar).Value = sGetTagLength() == 4 ? ((TextBox)((Controls.Find("PK01", false))[0])).Text : ((TextBox)((Controls.Find("PK001", false))[0])).Text;
                        }
                    }

                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        protected bool IsSuccessSavePromoManagement(string sContent)
        {
            string sErrMsg = "";
            string sTag = sGetTagLength() == 4 ? "PR01" : "PR001";
            try
            {
                string sQuery = !isEdit ? CommonSP.sSPPromoManagementInsert : CommonSP.sSPPromoManagementUpdate;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add("@sPromoManagementName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find(sTag, false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        protected bool IsSuccessSaveInitialFlazzManagement(string sContent)
        {
            string sErrMsg = "";
            string sTag = sGetTagLength() == 4 ? "IF01" : "IF001";
            try
            {
                string sQuery = !isEdit ? CommonSP.sSPInitialFlazzManagementInsert : CommonSP.sSPInitialFlazzManagementUpdate;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add("@sInitialFlazzName", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find(sTag, false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether Product Code saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of Product Code data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSaveProdCode(string sContent)
        {
            string sErrMsg = "";
            string sTag = sGetTagLength() == 4 ? "KP01" : "KP001";
            try
            {
                string sQuery = !isEdit ? CommonSP.sSPProfileProductCodeInsert : CommonSP.sSPProfileProductCodeUpdate;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add("@sProductCode", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find(sTag, false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether Bank Code saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of Bank Code data</param>
        /// <returns>boolean : True if succeessful</returns>
        protected bool IsSuccessSaveBankCode(string sContent)
        {
            string sErrMsg = "";
            string sTag = sGetTagLength() == 4 ? "KB01" : "KB001";
            try
            {
                string sQuery = !isEdit ? CommonSP.sSPProfileBankCodeInsert : CommonSP.sSPProfileBankCodeUpdate;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add("@sBankCode", SqlDbType.VarChar).Value = ((TextBox)((Controls.Find(sTag, false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether EMVMangament saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of EMVMangament data</param>
        /// <returns>boolean : True if succeessful</returns>
        private bool IsSuccessSaveEMVManagement(string sContent)
        {
            string sErrMsg = "";
            try
            {
                string sQuery = !isEdit ? CommonSP.sSPProfileEMVManagementInsert : CommonSP.sSPProfileEMVManagementUpdate;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = int.Parse(sDbID);
                    oSqlCmd.Parameters.Add("@sEMVManagementName", SqlDbType.VarChar).Value = sGetTagLength() == 4 ? ((TextBox)((Controls.Find("EM01", false))[0])).Text : ((TextBox)((Controls.Find("EM001", false))[0])).Text;
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Determine whether EMVMangament saved successfully into database or not
        /// </summary>
        /// <param name="sContent">String : The content of EMVMangament data</param>
        /// <returns>boolean : True if succeessful</returns>
        private bool IsSuccessSaveCustomMenu(string sContent)
        {
            string sErrMsg = "";
            try
            {
                string sQuery = !isEdit ? CommonSP.sSPProfileCustomMenuInsert : CommonSP.sSPProfileCustomMenuUpdate;
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sArrCurrentRelations[0];
                    oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sContent;
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                            sErrMsg = oRead[0].ToString();
                        oRead.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
            return isShowMessage(sErrMsg);
        }

        /// <summary>
        /// Get Store Procedure Currency
        /// </summary>
        /// <returns>String: Name of Store Procedure</returns>
        public string sGetSPCurrency()
        {
            return (isEdit) ? CommonSP.sSPCurrencyUpdate : CommonSP.sSPCurrencyInsert;
        }

        /// <summary>
        /// Obtain Stored Procedure that will be used in current operation
        /// </summary>
        /// <returns>String : Stored Procedure Name</returns>
        protected string sGetSpTLE()
        {
            if (isEdit) return CommonSP.sSPTLEUpdate;
            return CommonSP.sSPTLEInsert;
        }

        /// <summary>
        /// Determine whether Message Box will be showed or not
        /// </summary>
        /// <param name="sMsg">String : Message that will be showed in MessageBox</param>
        /// <returns>Boolean : True if MessageBox is showed</returns>
        protected bool isShowMessage(string sMsg, string sTag)
        {
            if (!string.IsNullOrEmpty(sMsg))
            {
                MessageBox.Show(sMsg);
                if (sMsg == string.Format("{0} has been saved.", ((TextBox)((Controls.Find(sTag, false))[0])).Text))
                    return true;
                else
                    return false;
            } 
            return true;
        }

        protected bool isShowMessage(string sMsg)
        {
            string sTag = sGetTagLength() == 4 ? "RD01" : "RD001";
            if (!string.IsNullOrEmpty(sMsg))
            {
                MessageBox.Show(sMsg);
                if (sMsg == string.Format("{0} has been saved.", ((TextBox)((Controls.Find(sTag, false))[0])).Text))
                    return true;
                else
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Generate Condition string to filter Card data
        /// </summary>
        /// <param name="sCardName">String : Card's Name</param>
        /// <returns>String : Condition string</returns>
        protected string sGetCardNameCondition(string sCardName)
        {
            return @"WHERE DatabaseId=" + sDbID + " AND cl.CardName='" + sCardName + "'";
        }

        /// <summary>
        /// Create Detail Action of Changes
        /// </summary>
        /// <returns>String: Detail of Changes </returns>
        protected string sGetActionDetail()
        {
            string sActionDetail = null;
            int iLengthTag = sGetTagLength();
            foreach (DataRow row in dtItemList.Rows)
            {
                string sObjectType = row["ObjectName"].ToString();
                string sTag = row["Tag"].ToString();
                sTag = sTag.Replace(" ", "");
                string sItemName = row["ItemName"].ToString();
                string sOldValue = "";

                if (isEdit)
                {
                    var varOldValue = from oldDataProfile in dtDataProfile.AsEnumerable()
                                      where oldDataProfile.Field<string>("Tag") == sTag
                                      select oldDataProfile.Field<string>("Value");
                    if (varOldValue.ToArray().Length > 0)
                    {
                        sOldValue = !string.IsNullOrEmpty((varOldValue.ToArray())[0]) ? (varOldValue.ToArray())[0] : "";
                        if (sObjectType.ToLower() == "checkbox")
                            sOldValue = sOldValue == "0" ? "FALSE" : "TRUE";
                    }
                    else
                        sOldValue = "NULL";
                }
                int iLen = 0;
                string sNewValue = "";
                if (iLengthTag == 4)
                {
                    iLen = sTag == "AE37" || sTag == "AE38" || sTag == "TL11" ? 3 : 2;

                    sNewValue = sResultTag.Substring(sResultTag.IndexOf(sTag) + 4 + iLen,
                        int.Parse(sResultTag.Substring(sResultTag.IndexOf(sTag) + 4, iLen)));
                }
                else
                { 
                    iLen = sTag == "AE037" || sTag == "AE038" || sTag == "TL011" ? 3 : 2;

                    sNewValue = sResultTag.Substring(sResultTag.IndexOf(sTag) + 5 + iLen,
                        int.Parse(sResultTag.Substring(sResultTag.IndexOf(sTag) + 5, iLen)));
                }
                if (sObjectType.ToLower() == "checkbox")
                    sNewValue = sNewValue == "0" ? "FALSE" : "TRUE";
                //MessageBox.Show(sOldValue + " " + sNewValue);

                if (sOldValue != sNewValue)
                    sActionDetail = string.IsNullOrEmpty(sActionDetail) ?
                        string.Format("{0} : {1} -> {2}", sItemName, sOldValue, sNewValue) :
                        string.Format("{0},\n{1} : {2} -> {3}", sActionDetail, sItemName, sOldValue, sNewValue);
            }
            //if (chkAllowDL.Checked.ToString().ToLower() != sChkAllowDL)
            //    sActionDetail = string.IsNullOrEmpty(sActionDetail) ?
            //        string.Format("{0} : {1} -> {2}", "Allow Download", sChkAllowDL.ToUpper(), chkAllowDL.Checked.ToString().ToUpper()) :
            //        string.Format("{0},\n{1} : {2} -> {3}", sActionDetail, "Allow Download", sChkAllowDL.ToUpper(), chkAllowDL.Checked.ToString().ToUpper());
            
            if (chkStatusMaster.Checked.ToString().ToLower() != sChkStatusMaster)
                sActionDetail = string.IsNullOrEmpty(sActionDetail) ?
                    string.Format("{0} : {1} -> {2}", "Status Master", sChkStatusMaster.ToUpper(), chkStatusMaster.Checked.ToString().ToUpper()) :
                    string.Format("{0},\n{1} : {2} -> {3}", sActionDetail, "Status Master", sChkStatusMaster.ToUpper(), chkStatusMaster.Checked.ToString().ToUpper());
            if (chkEnableIP.Checked.ToString().ToLower() != sChkEnableIP)
                sActionDetail = string.IsNullOrEmpty(sActionDetail) ?
                    string.Format("{0} : {1} -> {2}", "Enable IP", sChkEnableIP.ToUpper(), chkEnableIP.Checked.ToString().ToUpper()) :
                    string.Format("{0},\n{1} : {2} -> {3}", sActionDetail, "Enable IP", sChkEnableIP.ToUpper(), chkEnableIP.Checked.ToString().ToUpper());
            if (chkInitCompress.Checked.ToString().ToLower() != sChkInitCompress)
                sActionDetail = string.IsNullOrEmpty(sActionDetail) ?
                    string.Format("{0} : {1} -> {2}", "Init Compress", sChkInitCompress.ToUpper(), chkInitCompress.Checked.ToString().ToUpper()) :
                    string.Format("{0},\n{1} : {2} -> {3}", sActionDetail, "Init Compress", sChkInitCompress.ToUpper(), chkInitCompress.Checked.ToString().ToUpper());
            if (chkRemoteDownload.Checked.ToString().ToLower() != schkRemoteDownload)
                sActionDetail = string.IsNullOrEmpty(sActionDetail) ?
                    string.Format("{0} : {1} -> {2}", "Remote download", schkRemoteDownload.ToUpper(), chkRemoteDownload.Checked.ToString().ToUpper()) :
                    string.Format("{0},\n{1} : {2} -> {3}", sActionDetail, "Remote Download", schkRemoteDownload.ToUpper(), chkRemoteDownload.Checked.ToString().ToUpper());
            return (sActionDetail == null) ? "" : sActionDetail;
        }

        /// <summary>
        /// Get lenght of tag
        /// </summary>
        /// <returns>int : Lenght of tag</returns>
        private int sGetTagLength()
        {
            string sTag = "";

            sTag=dtItemList.Rows[0]["Tag"].ToString();
            sTag = sTag.Replace(" ", "");
            return sTag.Length;
        }
        #endregion
    }
}