﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmChoosing : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Database's ID where have the cards.
        /// </summary>
        protected string sDbID;

        /// <summary>
        /// Database's Name where have the cards.
        /// </summary>
        protected string sDbName;
        protected DataTable dtChoosingList;
        //protected string sType;

        FormType oFrmType;

        /// <summary>
        /// Form to show card list in given DatabaseID
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_sDbId">string : Database ID</param>
        /// <param name="_sDbName">string : Database Name</param>
        /// <param name="_sType">string : Type</param>
        public FrmChoosing(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sDbId, string _sDbName, string _sType)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sDbID = _sDbId;
            sDbName = _sDbName;
            //sType = _sType;
            switch (_sType.ToLower())
            {
                case "card":
                    oFrmType = FormType.card;
                    break;
                case "tle":
                    oFrmType = FormType.tle;
                    break;
                case "loyaltypool":
                    oFrmType = FormType.loyaltypool;
                    break;
                case "loyaltyprod":
                    oFrmType = FormType.loyaltyprod;
                    break;
                case "gprs":
                    oFrmType = FormType.gprs;
                    break;
                //case "currency":
                //    oFrmType = FormType.currency;
                //    break;
                case "pinpad":
                    oFrmType = FormType.pinpad;
                    break;
                case "remotedownload":
                    oFrmType = FormType.remotedownload;
                    break;
                case "aid":
                    oFrmType = FormType.aid;
                    break;
                case "capk":
                    oFrmType = FormType.capk;
                    break;
                case "bankcode":
                    oFrmType = FormType.bankcode;
                    break;
                case "productcode":
                    oFrmType = FormType.productcode;
                    break;
                case "Keymanagement":
                    oFrmType = FormType.keymanagement;
                    break;
                case "emvmanagement":
                    oFrmType = FormType.EMVManagement;
                    break;
                case "promomanagement":
                    oFrmType = FormType.promomanagement;
                    break;
                case "initialflazz":
                    oFrmType = FormType.initialflazz;
                    break;
                case "cardtype":
                    oFrmType = FormType.cardtype;
                    break;
                case "edcmonitoring":
                    oFrmType = FormType.edcmonitoring;
                    break;
                case "requestpaperreceipt":
                    oFrmType = FormType.requestpaperreceipt;
                    break;
                case "snmanagement":
                    oFrmType = FormType.snmanagement;
                    break;
            }
            InitializeComponent();
        }

        #region Custom Function(s)
        /// <summary>
        /// Update card's data to database.
        /// </summary>
        /// <param name="sCardName">string : Card's Name</param>
        /// <param name="sFormatedMsg">string : Card's data in formatted message</param>
        /// <param name="sLogDesc">string : Log's description to insert new logs</param>
        private void EditCard(string sCardName, string sFormatedMsg, ref string sLogDesc)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPCardListUpdate, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;
            oSqlComm.Parameters.Add("@sDatabaseID", SqlDbType.VarChar, 3).Value = sDbID;
            oSqlComm.Parameters.Add("@sCardName", SqlDbType.VarChar, 50).Value = sCardName;
            oSqlComm.Parameters.Add("@sContent", SqlDbType.VarChar, sFormatedMsg.Length).Value = sFormatedMsg;

            SqlDataReader oReader = oSqlComm.ExecuteReader();
            while (oReader.Read())
                sLogDesc +=
                    oReader.GetValue(0).ToString() + " : " +
                    oReader.GetValue(1).ToString() + " --> " +
                    oReader.GetValue(2).ToString() + "\n";
            oReader.Close();
        }

        /// <summary>
        /// Copy Card Data to New Card Name
        /// </summary>
        /// <param name="sOldCardName">string : Old Card's Name</param>
        /// <param name="sNewCardName">string : new Card's Name</param>
        private void CopyTerminal(string sOldCardName, string sNewCardName)
        {
            {
                SqlParameter[] oSqlParameter = new SqlParameter[3];

                oSqlParameter[0] = new SqlParameter("@sDatabaseID", SqlDbType.Int);
                oSqlParameter[0].Direction = ParameterDirection.Input;
                oSqlParameter[0].Value = Convert.ToInt32(sDbID);

                oSqlParameter[1] = new SqlParameter("@sCardName", SqlDbType.VarChar, 8);
                oSqlParameter[1].Direction = ParameterDirection.Input;
                oSqlParameter[1].Value = sOldCardName;

                oSqlParameter[2] = new SqlParameter("@sContent", SqlDbType.VarChar, 8);
                oSqlParameter[2].Direction = ParameterDirection.Input;
                oSqlParameter[2].Value = sNewCardName;

                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPCardNameCopy, oSqlConn);
                oSqlCommand.CommandType = CommandType.StoredProcedure;
                oSqlCommand.Parameters.AddRange(oSqlParameter);
                oSqlCommand.ExecuteNonQuery();
                //CommonClass.InputLog(oSqlConn, sNewCardName, UserData.sUserID, sDbName, "Copy Terminal : " + sOldCardName + " --> " + sNewCardName, "");
                CommonClass.InputLog(oSqlConnAuditTrail, sNewCardName, UserData.sUserID, sDbName, "Copy Terminal : " + sOldCardName + " --> " + sNewCardName, "");
            }

        }
        /// <summary>
        /// Remove card to database.
        /// </summary>
        /// <param name="sCardName">string : Card's name</param>
        private void RemoveCard(string sCardName)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPCardListDelete, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;
            oSqlComm.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = Convert.ToInt16(sDbID);
            oSqlComm.Parameters.Add("@sCardName", SqlDbType.VarChar, 50).Value = sCardName;
            oSqlComm.ExecuteNonQuery();
        }

        /// <summary>
        /// Insert new card to database.
        /// </summary>
        /// <param name="sCardName">string : Card's name</param>
        /// <param name="sFormatedMsg">string : Card's data in formatted message</param>
        private void AddCard(string sCardName, string sFormatedMsg)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPCardListInsert, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;
            oSqlComm.Parameters.Add("@sDatabaseID", SqlDbType.VarChar, 3).Value = sDbID;
            oSqlComm.Parameters.Add("@sCardName", SqlDbType.VarChar, 50).Value = sCardName;
            oSqlComm.Parameters.Add("@sContent", SqlDbType.VarChar, sFormatedMsg.Length).Value = sFormatedMsg;
            oSqlComm.ExecuteNonQuery();
        }
        #endregion

        /// <summary>
        /// Initiate form's data.
        /// </summary>
        private void FrmChoosing_Load(object sender, EventArgs e)
        {           
            InitData();
            InitDisplay();
            //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, CommonMessage.sFormOpened + this.Text, "");
            CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, CommonMessage.sFormOpened + this.Text, "");
        }

        /// <summary>
        /// Click NewCard button if want to add new card data.
        /// Call Flexible Form to set Card's detail settings.
        /// Save new card to database.
        /// </summary>
        private void btnNew_Click(object sender, EventArgs e)
        {
            switch (oFrmType)
            {
                case FormType.card:
                    SaveNewCard();
                    break;
                case FormType.pinpad:
                    if (dtChoosingList.Rows.Count == 0)
                        SaveChoosing(false);
                    break;
                case FormType.snmanagement:
                    if (cmbChoosingName.Items.Count == 0)
                        SaveChoosing(false);
                    else
                        MessageBox.Show("SN Management cannot more than 1. Please Delete first.");
                    break;
                default:
                    SaveChoosing(false);
                    break;
            }
        }

        /// <summary>
        /// Update selected Card by clicking Edit button.
        /// Save the changes to database.
        /// </summary>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (cmbChoosingName.SelectedIndex > -1)
            {
                switch (oFrmType)
                {
                    case FormType.card:
                        SaveExistingCard();
                        break;
                    default:
                        SaveChoosing(true);
                        break;
                }
            }
            else
                MessageBox.Show(string.Format("Please Choose {0}", lblChoosingName.Text));
        }

        /// <summary>
        /// Delete selected card by clicking Delete button.
        /// Delete card's data from database.
        /// </summary>
        private void btnDel_Click(object sender, EventArgs e)
        {
                switch (oFrmType)
                {
                    case FormType.card:
                        DeleteCard();
                        break;
                    default:
                        DeleteChoosing();
                        break;
                }
            }

        /// <summary>
        /// Close and dispose the form.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        #region "Function"
        /// <summary>
        /// Initiate Form's header text
        /// </summary>
        protected void InitDisplay()
        {
            string sChoosingName = null;
            string sTitle = null;

            switch (oFrmType)
            {
                case FormType.card:
                    sTitle = "Card Management";
                    sChoosingName = "Card Name";
                    break;
                case FormType.tle:
                    sTitle = "TLE Management";
                    sChoosingName = "TLE-EFTSec";
                    break;
                case FormType.loyaltypool:
                    sTitle = "Loyalty Pool Management";
                    sChoosingName = "PoolID";
                    break;
                case FormType.loyaltyprod:
                    sTitle = "Loyalty Product Management";
                    sChoosingName = "Prod Code";
                    break;
                case FormType.gprs:
                    sTitle = "GPRS/Eth Management";
                    sChoosingName = "GPRS/Eth Name";
                    break;
                //case FormType.currency:
                //    sTitle = "Currency Management";
                //    sChoosingName = "Currency Code";
                //    break;
                case FormType.pinpad:
                    sTitle = "PinPad Key Management";
                    sChoosingName = "PinPad Key";
                    break;
                case FormType.remotedownload:
                    sTitle = "Remote Download Management";
                    sChoosingName = "Name";
                    break;
                case FormType.aid:
                    sTitle = "AID Management";
                    sChoosingName = "AID";
                    break;
                case FormType.capk:
                    sTitle = "Public Key Management";
                    sChoosingName = "Public Key";
                    break;
                case FormType.bankcode:
                    sTitle = "Bank Code Management";
                    sChoosingName = "Bank Name";
                    break;
                case FormType.productcode:
                    sTitle = "Product Code Management";
                    sChoosingName = "Product Name";
                    break;
                case FormType.EMVManagement:
                    sTitle = "EMV Management";
                    sChoosingName = "EMV";
                    break;
                case FormType.addcardterminal:
                    sTitle = "Add Card On Terminal";
                    sChoosingName = "Card Name On Terminal";
                    break;
                case FormType.promomanagement:
                    sTitle = "Promo Management";
                    sChoosingName = "Promo Management";
                    break;
                case FormType.initialflazz:
                    sTitle = "Initial Flazz Management";
                    sChoosingName = "Initial Flazz";
                    break;
                case FormType.cardtype:
                    sTitle = "Card Type Management";
                    sChoosingName = "Card Type";
                    break;
                case FormType.edcmonitoring:
                    sTitle = "Edc Monitoring Management";
                    sChoosingName = "Name";
                    break;
                case FormType.requestpaperreceipt:
                    sTitle = "Request Paper Receipt Management";
                    sChoosingName = "Name";
                    break;
                case FormType.snmanagement:
                    sTitle = "Serial Number Management";
                    sChoosingName = "Name";
                    break;
            }
            this.Text = sTitle;
            lblChoosingName.Text = sChoosingName;
        }

        /// <summary>
        /// Call InitButton and FillComboBox.
        /// </summary>
        private void InitData()
        {
            InitButton();
            if (isGetData()) FillComboBox();
        }

        /// <summary>
        /// Initiate buttons in forms based UserRights.
        /// </summary>
        private void InitButton()
        {
            switch (oFrmType)
            {
                case FormType.card:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RC, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RC, Privilege.Delete);
                    btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RC, Privilege.Add);
                    break;
                case FormType.tle:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.TL, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.TL, Privilege.Delete);
                    btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.TL, Privilege.Add);
                    break;
                case FormType.aid:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.AI, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.AI, Privilege.Delete);
                    btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.AI, Privilege.Add);
                    break;
                case FormType.capk:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.PK, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.PK, Privilege.Delete);
                    btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.PK, Privilege.Add);
                    break;case FormType.gprs:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.GP, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.GP, Privilege.Delete);
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.GP, Privilege.Edit);
                    btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.GP, Privilege.Add);
                    break;
                case FormType.remotedownload:
                case FormType.snmanagement:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Delete);
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Edit);
                    btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RD, Privilege.Add);
                    break;
                case FormType.EMVManagement:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.EM, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.EM, Privilege.Delete);
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.EM, Privilege.Edit);
                    btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.EM, Privilege.Add);
                    break;
                case FormType.addcardterminal:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.CT, Privilege.Add);
                    break;
                case FormType.promomanagement:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.PM, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.PM, Privilege.Delete);
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.PM, Privilege.Edit);
                    btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.PM, Privilege.Add);
                    break;
                case FormType.initialflazz:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IF, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IF, Privilege.Delete);
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IF, Privilege.Edit);
                    //btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IF, Privilege.Add);
                    break;
                case FormType.cardtype:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.CM, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.CM, Privilege.Delete);
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.CM, Privilege.Edit);
                    //btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IF, Privilege.Add);
                    break;
                case FormType.edcmonitoring:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.ME, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.ME, Privilege.Delete);
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.ME, Privilege.Edit);
                    //btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IF, Privilege.Add);
                    break;
                case FormType.requestpaperreceipt:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RQ, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RQ, Privilege.Delete);
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.RQ, Privilege.Edit);
                    //btnCopy.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.IF, Privilege.Add);
                    break;
                    /*
                case FormType.loyaltypool:
                case FormType.loyaltyprod:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.LP, Privilege.Add);
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.LP, Privilege.Delete);
                    break;
                case FormType.bankcode:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.KB, Privilege.Add);//mycode
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.KB, Privilege.Delete);//mycode
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.KB, Privilege.Edit);//mycod
                    break;                
                case FormType.productcode:
                    btnNew.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.KP, Privilege.Add);//mycode
                    btnDel.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.KP, Privilege.Delete);//mycode
                    btnEdit.Enabled = UserPrivilege.IsAllowed(PrivilegeCode.KP, Privilege.Edit);//mycod
                    break;
                    */
            }
            if (!btnCopy.Enabled)
            {
                this.Width = 460;//configure acces button
                btnCopy.Visible = false;
            }
        }

        /// <summary>
        /// Load Card's or TLE's data from database.
        /// </summary>
        /// <returns>boolean : true if data found, else false</returns>
        protected bool isGetData()
        {
            bool isGetData = false;
            try
            {
                dtChoosingList = new DataTable();
                cmbChoosingName.DataSource = null;
                SqlCommand oSqlCmd = new SqlCommand(sGetLoadDataCondition(), oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sDbID;

                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }

                (new SqlDataAdapter(oSqlCmd)).Fill(dtChoosingList);
                if (dtChoosingList != null && dtChoosingList.Rows.Count > 0)
                    isGetData = true;
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }

            return isGetData;
        }

        /// <summary>
        /// Determine which data will be browsed, Card data or TLE data 
        /// </summary>
        /// <returns></returns>
        protected string sGetLoadDataCondition()
        {
            switch (oFrmType)
            {
                case FormType.tle: return CommonSP.sSPTLEListBrowse;
                case FormType.loyaltypool: return CommonSP.sSPLoyPoolListBrowse;
                case FormType.loyaltyprod: return CommonSP.sSPLoyProdListBrowse;
                case FormType.gprs: return CommonSP.sSPGPRSListBrowse;
                //case FormType.currency: return CommonSP.sSPCurrencyListBrowse;
                case FormType.pinpad: return CommonSP.sSPPinPadListBrowse;
                case FormType.remotedownload: return CommonSP.sSPRemoteDownloadListBrowse;
                case FormType.aid: return CommonSP.sSPProfileAIDBrowseList;
                case FormType.capk: return CommonSP.sSPProfileCAPKBrowseList;
                case FormType.bankcode: return CommonSP.sSPProfileBankCodeListBrowse;
                case FormType.productcode: return CommonSP.sSPProfileProductCodeListBrowse;
                case FormType.EMVManagement: return CommonSP.sSPProfileEMVManagementListBrowse;
                case FormType.promomanagement: return CommonSP.sSPPromoManagementBrowseList;
                case FormType.initialflazz: return CommonSP.sSPInitialFlazzManagementBrowseList;
                case FormType.cardtype: return CommonSP.sSPCardTypeBrowseList;
                case FormType.edcmonitoring: return CommonSP.sSPProfileEdcMonitorBrowseList;
                case FormType.requestpaperreceipt: return CommonSP.sSPRequestPaperReceiptBrowseList;
                case FormType.snmanagement:return CommonSP.sSPProfileSNBrowseList;
                default: return CommonSP.sSPCardListBrowse;
            }
        }

        /// <summary>
        /// Fill ComboBox Cards by assigning DataSource to ComboBox
        /// </summary>
        protected void FillComboBox()
        {
            cmbChoosingName.DataSource = dtChoosingList;
            cmbChoosingName.DisplayMember = "Name";
            cmbChoosingName.ValueMember = "Name";
            cmbChoosingName.SelectedIndex = -1;
        }

        #region "Card"
        /// <summary>
        /// Add new Card data into database
        /// </summary>
        protected void SaveNewCard()
        {
            bool isEdit = false;
            FrmFlexible fAdd = new FrmFlexible(oSqlConn, oSqlConnAuditTrail, isEdit);
            try
            {
                fAdd.sDbID = sDbID;
                fAdd.sName = "";
                fAdd.sFormID = "4";
                fAdd.ShowDialog();
                if (!string.IsNullOrEmpty(fAdd.sResultTag))
                {
                    string sFormatedMsg = fAdd.sResultTag;
                    string sCardName = "";
                    if (sGetTagLength() == 4)
                        sCardName = sFormatedMsg.Substring(6, Convert.ToInt32(sFormatedMsg.Substring(4, 2)));
                    else
                        sCardName = sFormatedMsg.Substring(7, Convert.ToInt32(sFormatedMsg.Substring(5, 2)));

                    AddCard(sCardName, sFormatedMsg);
                    InitData();
                    cmbChoosingName.SelectedValue = sCardName;
                    //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Add Card : " + sCardName, "");
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Add Card : " + sCardName, "");
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                fAdd.Dispose();
            }
        }

        /// <summary>
        /// Update existing card data in database
        /// </summary>
        protected void SaveExistingCard()
        {
            bool isEdit = true;
            FrmFlexible fEdit = new FrmFlexible(oSqlConn,oSqlConnAuditTrail, isEdit);
            try
            {
                if (cmbChoosingName.SelectedIndex > -1)
                {
                    fEdit.sDbID = sDbID;
                    fEdit.sName = cmbChoosingName.SelectedValue.ToString();
                    fEdit.sFormID = "4";
                    fEdit.ShowDialog();
                    if (!string.IsNullOrEmpty(fEdit.sResultTag))
                    {
                        string sFormatedMsg = fEdit.sResultTag;
                        string sCardName = "";
                        if (sGetTagLength() == 4)
                            sCardName = sFormatedMsg.Substring(6, Convert.ToInt32(sFormatedMsg.Substring(4, 2)));
                        else
                            sCardName = sFormatedMsg.Substring(7, Convert.ToInt32(sFormatedMsg.Substring(5, 2)));
                        string sLogDesc = "";

                        EditCard(sCardName, sFormatedMsg, ref sLogDesc);
                        InitData();
                        cmbChoosingName.SelectedValue = sCardName;
                        //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Edit Card : " + sCardName, sLogDesc);
                        CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Edit Card : " + sCardName, sLogDesc);
                    }
                    else
                    {
                        //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "View Card : " + cmbChoosingName.SelectedValue.ToString(), "");
                        CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "View Card : " + cmbChoosingName.SelectedValue.ToString(), "");
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                fEdit.Dispose();
            }
        }
        //========
        protected void CopyCard()
        {
            if (cmbChoosingName.SelectedIndex > -1)
            {
                string sNewCardName = "";
                FrmPromptString fcopyoldcard = new FrmPromptString(CommonMessage.sCardNameText, CommonMessage.sCardNameText);
                MaskedTextBox oMaskedBox = CommonClass.CreateOMaskedBoxCopy(fcopyoldcard.txtInput.Location, fcopyoldcard.txtInput.Size);

                fcopyoldcard.Controls.Add(oMaskedBox);
                fcopyoldcard.txtInput.Visible = false;

                string sCardName = cmbChoosingName.Text;


                DialogResult result = MessageBox.Show("Are u sure want to copy Cardname :" + cmbChoosingName.SelectedValue.ToString() + " ? ", "caution", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {

                    fcopyoldcard.ShowDialog();


                    if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                    {
                        sNewCardName = oMaskedBox.Text.ToUpper();

                        oSqlConn.Close();
                        if (cekdataCARD(sNewCardName) == true)
                        {
                            MessageBox.Show("The Name CARD has already exist");
                        }
                      
                        {
                            CopyCardName(sCardName, sNewCardName);
                            if (isGetData()) FillComboBox();
                        }
                    }
                    else
                        MessageBox.Show("Please Fill New Card Name.");

                }
                else
                    MessageBox.Show("Please Choose Card Name.");
            }
        }
        //copy card on terminal
        protected void CopyCardOnTerminal()
        {
            if (cmbChoosingName.SelectedIndex > -1)
            {
                string sNewCardName = "";
                FrmPromptString fcopyoldcard = new FrmPromptString(CommonMessage.sCardNameTextOnTerminal, CommonMessage.sCardNameTextOnTerminal);
                MaskedTextBox oMaskedBox = CommonClass.CreateOMaskedBoxCopy(fcopyoldcard.txtInput.Location, fcopyoldcard.txtInput.Size);

                fcopyoldcard.Controls.Add(oMaskedBox);
                fcopyoldcard.txtInput.Visible = false;

                string sCardName = cmbChoosingName.Text;


                DialogResult result = MessageBox.Show("Are u sure want to copy Cardname On Terminal :" + cmbChoosingName.SelectedValue.ToString() + " ? ", "caution", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {

                    fcopyoldcard.ShowDialog();


                    if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                    {
                        sNewCardName = oMaskedBox.Text.ToUpper();

                        oSqlConn.Close();
                        if (cekdataCARD(sNewCardName) == true)
                        {
                            MessageBox.Show("The Name CARD has already exist");
                        }

                        {
                            CopyCardName(sCardName, sNewCardName);
                            if (isGetData()) FillComboBox();
                        }
                    }
                    else
                        MessageBox.Show("Please Fill New Card Name.");

                }
                else
                    MessageBox.Show("Please Choose Card Name.");
            }
        }

        /// <summary>
        /// Copy Card Data to New Card Name
        /// </summary>
        /// <param name="sOldCardName">string : Old Card's Name</param>
        /// <param name="sNewCardName">string : new Card's Name</param>
        private void CopyCardName(string sOldCardName, string sNewCardName)
        {
            SqlParameter[] oSqlParameter = new SqlParameter[3];

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPCardNameCopy, oSqlConn);
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            //oSqlCommand.Parameters.AddRange(oSqlParameter);
           oSqlCommand.Parameters.Add("@sNewCardName", SqlDbType.VarChar).Value = sNewCardName;
           oSqlCommand.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
           oSqlCommand.Parameters.Add("@sOldCardName", SqlDbType.VarChar).Value = sOldCardName;
            oSqlCommand.ExecuteNonQuery();
            //CommonClass.InputLog(oSqlConn, sNewCardName, UserData.sUserID, sDbName, "Copy CardName : " + sOldCardName + " --> " + sNewCardName, "");
            CommonClass.InputLog(oSqlConnAuditTrail, sNewCardName, UserData.sUserID, sDbName, "Copy CardName : " + sOldCardName + " --> " + sNewCardName, "");
        }

        /// <summary>
        /// Copy Existing TLE Data to New TLE Name
        /// </summary>
        /// <param name="sOldTleName">string : Old Card's Name</param>
        /// <param name="sNewCardName">string : new Card's Name</param>
        private void CopyTLEName(string sOldTleName, string sNewTleName, string sNewTleId)
        {

            SqlParameter[] oSqlParameter = new SqlParameter[4];
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPTleNameCopy, oSqlConn);
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            //oSqlCommand.Parameters.AddRange(oSqlParameter);
            oSqlCommand.Parameters.Add("@sNewTleName", SqlDbType.VarChar).Value = sNewTleName;
            oSqlCommand.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
            oSqlCommand.Parameters.Add("@sOldTleName", SqlDbType.VarChar).Value = sOldTleName;
            oSqlCommand.Parameters.Add("@sNewTleId", SqlDbType.VarChar).Value = sNewTleId;
            oSqlCommand.ExecuteNonQuery();
            //CommonClass.InputLog(oSqlConn, sNewTleName, UserData.sUserID, sDbName, "Copy TLE Name : " + sOldTleName + " --> " + sNewTleName, "");
            CommonClass.InputLog(oSqlConnAuditTrail, sNewTleName, UserData.sUserID, sDbName, "Copy TLE Name : " + sOldTleName + " --> " + sNewTleName, "");

        }

        /// <summary>
        /// Copy Existing AID Data to New AID Name
        /// </summary>
        /// <param name="sOldAIDName">string : Old AID Name</param>
        /// <param name="sNewAIDName">string : New AID Name</param>
        private void CopyAIDName(string sOldAIDName, string sNewAIDName)
        {
            SqlParameter[] oSqlParameter = new SqlParameter[3];


            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPAIDNameCopy, oSqlConn);
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            //oSqlCommand.Parameters.AddRange(oSqlParameter);
            oSqlCommand.Parameters.Add("@sNewAIDName", SqlDbType.VarChar).Value = sNewAIDName;
            oSqlCommand.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
            oSqlCommand.Parameters.Add("@sOldAIDName", SqlDbType.VarChar).Value = sOldAIDName;
            oSqlCommand.ExecuteNonQuery();
            //CommonClass.InputLog(oSqlConn, sNewAIDName, UserData.sUserID, sDbName, "Copy AID Name : " + sOldAIDName + " --> " + sNewAIDName, "");
            CommonClass.InputLog(oSqlConnAuditTrail, sNewAIDName, UserData.sUserID, sDbName, "Copy AID Name : " + sOldAIDName + " --> " + sNewAIDName, "");

        }

        /// <summary>
        /// Copy Existing GPRS Data to New GPRS Name
        /// </summary>
        /// <param name="sOldGprsName">string : Old GPRS Name</param>
        /// <param name="sNewGprsName">string : New GPRS Name</param>
        private void CopyGprsName(string sOldGprsName, string sNewGprsName)
        {

            SqlParameter[] oSqlParameter = new SqlParameter[3];

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPGprsCopy, oSqlConn);
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            //oSqlCommand.Parameters.AddRange(oSqlParameter);
            oSqlCommand.Parameters.Add("@sNewGprsName", SqlDbType.VarChar).Value = sNewGprsName;
            oSqlCommand.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
            oSqlCommand.Parameters.Add("@sOldGprsName", SqlDbType.VarChar).Value = sOldGprsName;
            oSqlCommand.ExecuteNonQuery();
            //CommonClass.InputLog(oSqlConn, sNewGprsName, UserData.sUserID, sDbName, "Copy AID Name : " + sOldGprsName + " --> " + sNewGprsName, "");
            CommonClass.InputLog(oSqlConnAuditTrail, sNewGprsName, UserData.sUserID, sDbName, "Copy AID Name : " + sOldGprsName + " --> " + sNewGprsName, "");

        }

        private void CopyPromoManagement(string sOldPromoManagementName, string sNewPromoManagementName)
        {

            SqlParameter[] oSqlParameter = new SqlParameter[3];

            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPPromoManagementCopy, oSqlConn);
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            //oSqlCommand.Parameters.AddRange(oSqlParameter);
            oSqlCommand.Parameters.Add("@sNewPromoManagementName", SqlDbType.VarChar).Value = sNewPromoManagementName;
            oSqlCommand.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
            oSqlCommand.Parameters.Add("@sOldPromoManagementName", SqlDbType.VarChar).Value = sOldPromoManagementName;
            oSqlCommand.ExecuteNonQuery();
            //CommonClass.InputLog(oSqlConn, sNewGprsName, UserData.sUserID, sDbName, "Copy AID Name : " + sOldGprsName + " --> " + sNewGprsName, "");
            CommonClass.InputLog(oSqlConnAuditTrail, sNewPromoManagementName, UserData.sUserID, sDbName, "Copy Promo Management Name : " + sOldPromoManagementName + " --> " + sNewPromoManagementName, "");

        }

        private void CopyRemoteDownloadName(string sOldRDName, string sNewRDName)
        {
            SqlParameter[] oSqlParameter = new SqlParameter[3];
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPRemoteDownloadManagementCopy, oSqlConn);
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            oSqlCommand.Parameters.Add("@sNewRDName", SqlDbType.VarChar).Value = sNewRDName;
            oSqlCommand.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
            oSqlCommand.Parameters.Add("@sOldRDName", SqlDbType.VarChar).Value = sOldRDName;
            oSqlCommand.ExecuteNonQuery();
            CommonClass.InputLog(oSqlConnAuditTrail, sNewRDName, UserData.sUserID, sDbName, "Copy Remote Download Name : " + sOldRDName + " --> " + sNewRDName, "");
        }

        private void CopyEmvManagementName(string sOldEMVName, string sNewEMVName)
        {
            SqlParameter[] oSqlParameter = new SqlParameter[3];
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPpProfileEMVManagementCopy, oSqlConn);
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            oSqlCommand.Parameters.Add("@sNewEmvName", SqlDbType.VarChar).Value = sNewEMVName;
            oSqlCommand.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
            oSqlCommand.Parameters.Add("@sOldEmvName", SqlDbType.VarChar).Value = sOldEMVName;
            oSqlCommand.ExecuteNonQuery();
            CommonClass.InputLog(oSqlConnAuditTrail, sNewEMVName, UserData.sUserID, sDbName, "Copy EMV Management : " + sOldEMVName + " --> " + sNewEMVName, "");
        }

        /// <summary>
        /// Copy Existing CAPK Data to New CAPK Name
        /// </summary>
        /// <param name="sOldCAPKName">string : Old CAPK Name</param>
        /// <param name="sNewCAPKName">string : New CAPK Name</param>
        /// <returns></returns>
        private string CopyCAPKName(string sOldCAPKName, string sNewCAPKName)
        {
            string sErrorMessage = "";
            
            if (oSqlConn.State != ConnectionState.Open) 
                oSqlConn.Open(); 
            SqlCommand oSqlCommand = new SqlCommand(CommonSP.sSPCAPKCopy, oSqlConn);
            oSqlCommand.CommandType = CommandType.StoredProcedure;
            oSqlCommand.Parameters.Add("@sNewCAPKName", SqlDbType.VarChar).Value = sNewCAPKName;
            oSqlCommand.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sDbID;
            oSqlCommand.Parameters.Add("@sOldCAPKName", SqlDbType.VarChar).Value = sOldCAPKName;
            using (SqlDataReader oRead = oSqlCommand.ExecuteReader())
            {
                while (oRead.Read())
                    sErrorMessage = oRead[0].ToString();
                oRead.Close();
            }
            CommonClass.InputLog(oSqlConnAuditTrail, sNewCAPKName, UserData.sUserID, sDbName, "Copy CAPK Name : " + sOldCAPKName + " --> " + sNewCAPKName, "");

            return sErrorMessage;
        }

        /// <summary>
        /// Delete Card data from database
        /// </summary>
        protected void DeleteCard()
        {

            try
            {
                if (cmbChoosingName.SelectedIndex > -1 &&
                    CommonClass.isYesMessage(CommonMessage.sCardDelConfirmText, CommonMessage.sConfirmationTitle))
                {
                    RemoveCard(cmbChoosingName.SelectedValue.ToString());
                    //CommonClass.InputLog(oSqlConn, "", UserData.sUserID, sDbName, "Delete Card : " + cmbChoosingName.SelectedValue.ToString(), "");
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Delete Card : " + cmbChoosingName.SelectedValue.ToString(), "");
                    InitData();
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region "TLE, Loyalty Pool, Loyalty Product, GPRS"
        /// <summary>
        /// Save TLE Data to database
        /// </summary>
        /// <param name="isEdit">boolean : True if Edit mode, False if Add Mode</param>
        protected void SaveChoosing(bool isEdit)
        {
            FrmFlexible fFlexible = new FrmFlexible(oSqlConn,oSqlConnAuditTrail, isEdit);
            try
            {
                fFlexible.sDbID = sDbID;
                fFlexible.sName = (isEdit) ? cmbChoosingName.SelectedValue.ToString() : "";
                fFlexible.sFormID = (oFrmType.GetHashCode()).ToString();
               
                fFlexible.ShowDialog();

                if (!string.IsNullOrEmpty(fFlexible.sResultTag))
                {
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, ((isEdit) ?
                                        "Edit " + oFrmType.ToString().ToUpperInvariant() + ": "
                                        : "Add " + oFrmType.ToString().ToUpperInvariant() + ": ") + fFlexible.sName, fFlexible.sActDtl);

                }
                else if (isEdit)
                    CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, sDbName,
                        "View " + oFrmType.ToString().ToUpperInvariant() + ": " + cmbChoosingName.SelectedValue.ToString(),
                        string.IsNullOrEmpty(fFlexible.sActDtl) ? "" : fFlexible.sActDtl);

                InitData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                fFlexible.Dispose();
            }
        }

        /// <summary>
        /// Delete TLE Data from database
        /// </summary>
        protected void DeleteChoosing()
        {
            try
            {
                string sConfirmMsg = CommonMessage.sDelConfirmText;
                string sParamName = null;
                string sQuery = null;
                string sActDesc = null;
                switch (oFrmType)
                {
                    case FormType.tle:
                        sConfirmMsg = string.Format("{0} TLE?", sConfirmMsg);
                        sParamName = "@sTLEName";
                        sQuery = CommonSP.sSPTLEDelete;
                        sActDesc = "Delete TLE : ";
                        break;
                    case FormType.loyaltypool:
                        sConfirmMsg = string.Format("{0} Loyalty Pool?", sConfirmMsg);
                        sParamName = "@sLoyPoolName";
                        sQuery = CommonSP.sSPLoyPoolDelete;
                        sActDesc = "Delete Loyalty Pool : ";
                        break;
                    case FormType.loyaltyprod:
                        sConfirmMsg = string.Format("{0} Loyalty Product?", sConfirmMsg);
                        sParamName = "@sLoyProdName";
                        sQuery = CommonSP.sSPLoyProdDelete;
                        sActDesc = "Delete Loyalty Product : ";
                        break;
                    case FormType.gprs:
                        sConfirmMsg = string.Format("{0} GPRS \"{1}\"?", sConfirmMsg, cmbChoosingName.SelectedValue.ToString());
                        sParamName = "@sGPRSName";
                        sQuery = CommonSP.sSPGPRSDelete;
                        sActDesc = "Delete GPRS Name: ";
                        break;
                    case FormType.currency:
                        sConfirmMsg = string.Format("{0} Currency?", sConfirmMsg);
                        @sParamName = "@sCurrencyName";
                        sQuery = CommonSP.sSPCurrencyDelete;
                        sActDesc = "Delete Currency Name: ";
                        break;
                    case FormType.pinpad:
                        sConfirmMsg = string.Format("{0} PinPad?", sConfirmMsg);
                        @sParamName = "@sPinPadName";
                        sQuery = CommonSP.sSPPinPadDelete;
                        sActDesc = "Delete PinPad Name: ";
                        break;
                    case FormType.remotedownload:
                        sConfirmMsg = string.Format("{0} {1}?", sConfirmMsg, cmbChoosingName.Text);
                        @sParamName = "@sRemoteDownloadName";
                        sQuery = CommonSP.sSPRemoteDownloadDelete;
                        sActDesc = "Delete Remote Download Name: ";
                        break;
                    case FormType.aid:
                        sConfirmMsg = string.Format("{0} AID?", sConfirmMsg);
                        @sParamName = "@sAIDName";
                        sQuery = CommonSP.sSPProfileAIDDelete;
                        sActDesc = "Delete AID Name: ";
                        break;
                    case FormType.capk:
                        sConfirmMsg = string.Format("{0} Public Key?", sConfirmMsg);
                        @sParamName = "@sCAPKIndex";
                        sQuery = CommonSP.sSPProfileCAPKDelete;
                        sActDesc = "Delete Public Key Name: ";
                        break;
                    case FormType.productcode:
                        sConfirmMsg = string.Format("{0} Product Code?", sConfirmMsg);
                        @sParamName = "@sProductCode";
                        sQuery = CommonSP.sSPProfileProductCodeDelete;
                        sActDesc = "Delete ProductCode: ";
                        break;
                    case FormType.bankcode:
                        sConfirmMsg = string.Format("{0} Bank Code?", sConfirmMsg);
                        @sParamName = "@sBankCode";
                        sQuery = CommonSP.sSPProfileBankCodeDelete;
                        sActDesc = "Delete BankCode: ";
                        break;
                    case FormType.EMVManagement:
                        sConfirmMsg = string.Format("{0} EMV Management?", sConfirmMsg);
                        @sParamName = "@sEMVManagementName";
                        sQuery = CommonSP.sSPProfileEMVManagementDelete;
                        sActDesc = "Delete EMV Managament: ";
                        break;
                    case FormType.promomanagement:
                        sConfirmMsg = string.Format("{0} Promo Management?", sConfirmMsg);
                        @sParamName = "@sPromoManagementName";
                        sQuery = CommonSP.sSPPromoManagementDelete;
                        sActDesc = "Delete Promo Management";
                        break;
                    case FormType.initialflazz:
                        sConfirmMsg = string.Format("{0} Initial Flazz Management?", sConfirmMsg);
                        @sParamName = "@sInitialFlazzName";
                        sQuery = CommonSP.sSPInitialFlazzManagementDelete;
                        sActDesc = "Delete Initial Flazz Management";
                        break;
                    case FormType.cardtype:
                        sConfirmMsg = string.Format("{0} Card Type Management?", sConfirmMsg);
                        @sParamName = "@sCardTypeName";
                        sQuery = CommonSP.sSPCardTypeDelete;
                        sActDesc = "Delete Card Type Management";
                        break;
                    case FormType.edcmonitoring:
                        sConfirmMsg = string.Format("{0} Edc Monitor configuration?", sConfirmMsg);
                        @sParamName = "@sEdcMonitorName";
                        sQuery = CommonSP.sSPProfileEdcMonitorDelete;
                        sActDesc = "Delete Edc Monitor configuration";
                        break;
                    case FormType.requestpaperreceipt:
                        sConfirmMsg = string.Format("{0} Request Paper Receipt?", sConfirmMsg);
                        @sParamName = "@sRequestPaperReceiptName";
                        sQuery = CommonSP.sSPRequestPaperReceiptDelete;
                        sActDesc = "Delete Request Paper Receipt";
                        break;
                    case FormType.snmanagement:
                        sConfirmMsg = string.Format("{0} {1}?", sConfirmMsg, cmbChoosingName.Text);
                        @sParamName = "@sName";
                        sQuery = CommonSP.sSPProfileSNDelete;
                        sActDesc = "Delete Serial Number Name: ";
                        break;
                }

                if (cmbChoosingName.SelectedIndex > -1 &&
                        CommonClass.isYesMessage(sConfirmMsg, CommonMessage.sConfirmationTitle))
                {
                    using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                    {
                        oSqlCmd.CommandType = CommandType.StoredProcedure;
                        oSqlCmd.Parameters.Add("@iDatabaseId", SqlDbType.VarChar).Value = sDbID;
                        oSqlCmd.Parameters.Add(sParamName, SqlDbType.VarChar).Value = cmbChoosingName.SelectedValue.ToString();
                        oSqlCmd.Parameters.Add("@bDeleteData", SqlDbType.VarChar).Value = "1";
                        oSqlCmd.CommandTimeout = 60000;
                        if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                        using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                        {
                            bool isNotSuccess = false;

                            if ((isNotSuccess = oRead.Read()))
                                MessageBox.Show(oRead[0].ToString());
                            oRead.Close();

                            if (!isNotSuccess)
                                CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, sActDesc + cmbChoosingName.SelectedValue.ToString(), "");

                            if (oFrmType == FormType.remotedownload)
                                MessageBox.Show(string.Format("{0} has been deleted.", cmbChoosingName.SelectedValue.ToString()));

                            InitData();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion;

        /// <summary>
        /// Return lenght of Tag
        /// </summary>
        private int sGetTagLength()
        {
            string sTag = "";
            DataTable dtItemList = new DataTable();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = string.Format("WHERE DatabaseID = {0}", sDbID);
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtItemList);
                sTag = dtItemList.Rows[0]["Tag"].ToString();
                sTag = sTag.Replace(" ", "");
            }
            return sTag.Length;
        }
        #endregion

        /// <summary>
        /// Call Copy Function
        /// </summary>
        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (cmbChoosingName.SelectedIndex > -1)
            {
                switch (oFrmType)
                {
                    case FormType.card:
                        CopyCard();
                        break;
                    case FormType.addcardterminal:
                        CopyCardOnTerminal();
                        break;
                    default:
                        CopyChoosing();
                        break;
                }
            }
        }

        /// <summary>
        /// Call Function Copy depend on Form Type
        /// </summary>
        protected void CopyChoosing()
        {
            try
            {
                string sConfirmMsg = CommonMessage.sCopyNewCardName;
                //string sParamName = null;
                //string sQuery = null;
                //string sActDesc = null;
                string sNewName = "";
                string sNewTleId = "";
                string sItemcmb = "";
                FrmPromptString FrmCopyTle = new FrmPromptString(CommonMessage.sTleNameText, CommonMessage.sTleNameText);
                MaskedTextBox oMaskedBox = CommonClass.CreateOMaskedBoxCopy(FrmCopyTle.txtInput.Location, FrmCopyTle.txtInput.Size);
                
                FrmPromptString FrmCopyTleId = new FrmPromptString(CommonMessage.sTleIdText, CommonMessage.sTleIdText);//for tleid
                MaskedTextBox oMaskedBoxTleId = CommonClass.CreateOMaskedBoxCopyIdTle(FrmCopyTleId.txtInput.Location, FrmCopyTleId.txtInput.Size);//for tleid

                FrmCopyTle.Controls.Add(oMaskedBox);
                FrmCopyTleId.Controls.Add(oMaskedBoxTleId);

                FrmCopyTleId.txtInput.Visible = false;
                FrmCopyTle.txtInput.Visible = false;

                FrmCopyTle.txtInput.ToString().ToUpper();

                string sOldName = cmbChoosingName.Text;
                string sItemCmb = cmbChoosingName.ValueMember;
                switch (oFrmType)
                {
                    case FormType.tle:

                        sConfirmMsg = string.Format("{0} TLE?" + cmbChoosingName.SelectedValue.ToString(), sConfirmMsg);
                        if (cmbChoosingName.SelectedIndex > -1 && CommonClass.isYesMessage(sConfirmMsg, CommonMessage.sConfirmationTitle))

                            FrmCopyTle.ShowDialog();
                        if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                        {
                            FrmCopyTleId.ShowDialog();

                            if (oMaskedBoxTleId.Text.Replace(" ", "").Length > 0)
                            {
                                sNewName = oMaskedBox.Text.ToUpper();
                                sNewTleId = oMaskedBoxTleId.Text.ToUpper();
                                sItemcmb = cmbChoosingName.Items.ToString();

                                oSqlConn.Close();
                                if (cekdataTLE(sNewName) == true)
                                {
                                    MessageBox.Show("The name of TLE has already exist");
                                    return;
                                }
                                oSqlConn.Close();
                                if (cekdataTLEId(sNewTleId) == true)
                                {
                                    MessageBox.Show("The TLEID already has Exist");
                                    return;
                                }
                                CopyTLEName(sOldName, sNewName, sNewTleId);
                                
                                if (isGetData()) FillComboBox();
                                //sParamName = "@sTLEName";
                                //sParamName = "@sTleId";
                                //sQuery = CommonSP.sSPProfileTLECopy;
                                //sActDesc = "Copy TLE : ";
                            }
                            else
                            {
                                MessageBox.Show("Please Fill TLEID");

                            }
                        }
                        else
                        {
                            MessageBox.Show("Please Fill New Card Name.");
                        }
                        break;

                    //case FormType.loyaltypool:
                    //    //sConfirmMsg = string.Format("{0} Loyalty Pool?", sConfirmMsg);
                    //    sParamName = "@sLoyPoolName";
                    //    sQuery = CommonSP.sSPLoyPoolDelete;
                    //    sActDesc = "Delete Loyalty Pool : ";
                    //    break;
                    //case FormType.loyaltyprod:
                    //    //sConfirmMsg = string.Format("{0} Loyalty Product?", sConfirmMsg);
                    //    sParamName = "@sLoyProdName";
                    //    sQuery = CommonSP.sSPLoyProdDelete;
                    //    sActDesc = "Delete Loyalty Product : ";
                    //    break;
                    case FormType.gprs:

                        sConfirmMsg = string.Format("{0} GPRS?" + cmbChoosingName.SelectedValue.ToString(), sConfirmMsg);
                        if (cmbChoosingName.SelectedIndex > -1 &&
                        CommonClass.isYesMessage(sConfirmMsg, CommonMessage.sConfirmationTitle))
                            FrmCopyTle.ShowDialog();

                        if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                        {
                            sNewName = oMaskedBox.Text.ToUpper();

                            oSqlConn.Close();
                            if (cekdataGPRS(sNewName) == true)
                            {
                                MessageBox.Show("The Name GPRS has already exist");
                            }
                            CopyGprsName(sOldName, sNewName);
                            if (isGetData()) FillComboBox();
                            //sParamName = "@sGPRSName";
                            //sQuery = CommonSP.sSPGprsCopy;
                            //sActDesc = "Copy GPRS Name: ";
                        }

                        else
                        {
                            MessageBox.Show("Please Fill New GPRS/ETH Name.");
                        }
                        break;

                    case FormType.promomanagement:

                        sConfirmMsg = string.Format("{0} Promo Management?" + cmbChoosingName.SelectedValue.ToString(), sConfirmMsg);
                        if (cmbChoosingName.SelectedIndex > -1 &&
                        CommonClass.isYesMessage(sConfirmMsg, CommonMessage.sConfirmationTitle))
                            FrmCopyTle.ShowDialog();
                        if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                        {
                            sNewName = oMaskedBox.Text.ToUpper();

                            oSqlConn.Close();
                            if (cekdataPromoManagement(sNewName) == true)
                            {
                                MessageBox.Show("The Name Promo Management has already exist");
                            }
                            CopyPromoManagement(sOldName, sNewName);
                            if (isGetData()) FillComboBox();
                            //sParamName = "@sPromoManagementName";
                            //sQuery = CommonSP.sSPPromoManagementDelete;
                            //sActDesc = "Delete Profile Promo Management Name: ";
                        }

                        else
                        {
                            MessageBox.Show("Please Fill New Promo Management Name.");
                        }
                        break;

                    case FormType.remotedownload:
                        sConfirmMsg = string.Format("{0}, Remote Download ?" + cmbChoosingName.SelectedValue.ToString(), sConfirmMsg);
                        if (cmbChoosingName.SelectedIndex > -1 && CommonClass.isYesMessage(sConfirmMsg, CommonMessage.sConfirmationTitle))
                            FrmCopyTle.ShowDialog();
                        if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                        {
                            sNewName = oMaskedBox.Text.ToUpper();
                            oSqlConn.Close();
                            if (cekdataRemoteDownloadName(sNewName) == true)
                            {
                                MessageBox.Show("Remote Download Name Already exist");

                            }
                            CopyRemoteDownloadName(sOldName, sNewName);
                            if (isGetData()) FillComboBox();
                            //sParamName = "@sRemoteDownloadName";
                            //sQuery = CommonSP.sSPGprsCopy;
                            //sActDesc = "Copy Remote Download Name: ";
                        }
                        else
                        {
                            MessageBox.Show("Please Filll Remote Download Name");
                        }

                        break;

                    case FormType.EMVManagement:
                        sConfirmMsg = string.Format("{0}, EMV Management ?" + cmbChoosingName.SelectedValue.ToString(), sConfirmMsg);
                        if (cmbChoosingName.SelectedIndex > -1 && CommonClass.isYesMessage(sConfirmMsg, CommonMessage.sConfirmationTitle))
                            FrmCopyTle.ShowDialog();
                        if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                        {
                            sNewName = oMaskedBox.Text.ToUpper();
                            oSqlConn.Close();
                            if (cekdataEmvManagementName(sNewName) == true)
                            {
                                MessageBox.Show("EMV Management Name Already exist");

                            }
                            CopyEmvManagementName(sOldName, sNewName);
                            if (isGetData()) FillComboBox();
                            //sParamName = "@sEMVManagementName";
                            //sQuery = CommonSP.sSPGprsCopy;
                            //sActDesc = "Please Fill EMV Management Name: ";
                        }
                        break;

                    //case FormType.currency:
                    //    //sConfirmMsg = string.Format("{0} Currency?", sConfirmMsg);
                    //    @sParamName = "@sCurrencyName";
                    //    sQuery = CommonSP.sSPCurrencyDelete;
                    //    sActDesc = "Delete Currency Name: ";
                    //    break;
                    //case FormType.pinpad:
                    //    //sConfirmMsg = string.Format("{0} PinPad?", sConfirmMsg);
                    //    @sParamName = "@sPinPadName";
                    //    sQuery = CommonSP.sSPPinPadDelete;
                    //    sActDesc = "Delete PinPad Name: ";
                    //    break;
                    //case FormType.remotedownload:
                    //    //sConfirmMsg = string.Format("{0} Remote Download?", sConfirmMsg);
                    //    @sParamName = "@sRemoteDownloadName";
                    //    sQuery = CommonSP.sSPRemoteDownloadDelete;
                    //    sActDesc = "Delete Remote Download Name: ";
                    //    break;

                    case FormType.aid:
                        sConfirmMsg = string.Format("{0} AID?" + cmbChoosingName.SelectedValue.ToString(), sConfirmMsg);
                        if (cmbChoosingName.SelectedIndex > -1 &&
                      CommonClass.isYesMessage(sConfirmMsg, CommonMessage.sConfirmationTitle))
                            
                            FrmCopyTle.ShowDialog();

                        if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                        {
                            sNewName = oMaskedBox.Text.ToUpper();

                            oSqlConn.Close();
                            if (cekdataAID(sNewName) == true)
                            {
                                MessageBox.Show("The Name AID has already exist");
                            }


                            CopyAIDName(sOldName, sNewName);
                            if (isGetData()) FillComboBox();
                            //@sParamName = "@sAIDName";
                            //sQuery = CommonSP.sSPProfileAIDDelete;
                            //sActDesc = "Delete AID Name: ";

                        }
                        else
                        {
                            MessageBox.Show("Please Fill New Card Name.");
                        }
                        break;


                    case FormType.capk:
                        sConfirmMsg = string.Format("{0} CAPK?" + cmbChoosingName.SelectedValue.ToString(), sConfirmMsg);

                        if (cmbChoosingName.SelectedIndex > -1 &&
                        CommonClass.isYesMessage(sConfirmMsg, CommonMessage.sConfirmationTitle))
                            FrmCopyTle.ShowDialog();

                        if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                        {
                            sNewName = oMaskedBox.Text.ToUpper();
                            int iCustom = 0;
                            iCustom = isCustomCAPK();

                            if (iCustom == 1)
                            {
                                string sTagPK002 = GetPK002Value(sOldName, sDbID);
                                sNewName = sNewName + " - " + sTagPK002;
                            }
                
                            string sErrMessage = CopyCAPKName(sOldName, sNewName);
                            if (!string.IsNullOrEmpty(sErrMessage))
                                MessageBox.Show(sErrMessage);
                            
                            if (isGetData()) FillComboBox();
                        }
                        else
                        {
                            MessageBox.Show("Please Fill New Card Name.");
                        }
                        break;

                    case FormType.initialflazz:
                        //sConfirmMsg = string.Format("{0} Initial FLAZZ Management?" + cmbChoosingName.SelectedValue.ToString(), sConfirmMsg);
                        //if (cmbChoosingName.SelectedIndex > -1 &&
                        //CommonClass.isYesMessage(sConfirmMsg, CommonMessage.sConfirmationTitle))
                        //    FrmCopyTle.ShowDialog();
                        //if (oMaskedBox.Text.Replace(" ", "").Length > 0)
                        //{
                        //    sNewName = oMaskedBox.Text.ToUpper();

                        //    oSqlConn.Close();
                        //    if (cekdataPromoManagement(sNewName) == true)
                        //    {
                        //        MessageBox.Show("The Name Initial FLAZZ Management has already exist");
                        //    }
                        //    CopyPromoManagement(sOldName, sNewName);
                        //    if (isGetData()) FillComboBox();
                        //    sParamName = "@sPromoManagementName";
                        //    sQuery = CommonSP.sSPPromoManagementDelete;
                        //    sActDesc = "Delete Profile Promo Management Name: ";
                        //}

                        //else
                        //{
                        //    MessageBox.Show("Please Fill New Card Name.");
                        //}
                        break;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private int isCustomCAPK()
        {
            try
            {
                DataTable dtCustomCAPK = new DataTable();

                SqlCommand oSqlCmdCustom = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
                oSqlCmdCustom.CommandType = CommandType.StoredProcedure;
                oSqlCmdCustom.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE DatabaseID = {0}", sDbID); // Load all data with no condition

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                new SqlDataAdapter(oSqlCmdCustom).Fill(dtCustomCAPK);
                string sData = dtCustomCAPK.Rows[0]["Custom_CAPK_Name"].ToString();
                return Convert.ToInt32(sData);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private string GetPK002Value(string sOldName,string sDBID)
        {
            string sValue = "";
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(string.Format("SELECT CAPKTagValue FROM tbProfileCAPK WHERE DatabaseId = '{0}' AND CAPKIndex = '{1}' AND CAPKTag IN ('PK02','PK002')",sDbID,sOldName), oSqlConn);
                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }

                SqlDataReader oRead = oSqlCmd.ExecuteReader();
                if (oRead.Read())
                    sValue = oRead[0].ToString();
                oRead.Close();
                oRead.Dispose();
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            { 
            
            }
            return sValue;
        }

        /// <summary>
        /// Get Database Name
        /// </summary>
        /// <param name="sTerminalID">string : Terminal ID</param>
        /// <returns>String : Database Name</returns>
        private string sGetDBName(string sTerminalID)
        {
            string sDatabaseName = "";
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPViewTerminalListBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;

                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }

                SqlDataReader oRead = oSqlCmd.ExecuteReader();
                if (oRead.Read())
                    sDatabaseName = oRead["DatabaseName"].ToString();

                oRead.Close();
                oRead.Dispose();
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return sDatabaseName;
        }

        /// <summary>
        /// Check Existing GPRS Name
        /// </summary>
        /// <param name="sGPRSName">String: GPRS Name</param>
        /// <returns>Bool : true if Existing</returns>
        private Boolean cekdataGPRS(string sGPRSName)
        {
            sGPRSName = sGPRSName.ToString();
            oSqlConn.Open();
            string sql = "select GPRSName from tbProfileGPRS";
            SqlCommand command = new SqlCommand(sql, oSqlConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sGPRSName)                
                {
                    reader.Close();
                    return true;
                }
            }
                    oSqlConn.Close();
                    reader.Close();
                    return false;
              
        }
        
        private Boolean cekdataPromoManagement(string sPromoManagementName)
        {
            sPromoManagementName = sPromoManagementName.ToString();
            oSqlConn.Open();
            string sql = "select PromoManagementName from tbProfilePromoManagement";
            SqlCommand command = new SqlCommand(sql, oSqlConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sPromoManagementName)
                {
                    reader.Close();
                    return true;
                }
            }
            oSqlConn.Close();
            reader.Close();
            return false;

        }

        private Boolean cekdataRemoteDownloadName(string sRemoteDownloadName)
        {
           // sPromoManagementName = sPromoManagementName.ToString();
            oSqlConn.Open();
            string sql = "select RemoteDownloadName from tbProfileRemoteDownload";
            SqlCommand command = new SqlCommand(sql, oSqlConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sRemoteDownloadName)
                {
                    reader.Close();
                    return true;
                }
            }
            oSqlConn.Close();
            reader.Close();
            return false;

        }

        private Boolean cekdataEmvManagementName(string sEmvNamagementName)
        {
            // sPromoManagementName = sPromoManagementName.ToString();
            oSqlConn.Open();
            string sql = "select EmvManagementName from tbProfileEMVManagement";
            SqlCommand command = new SqlCommand(sql, oSqlConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sEmvNamagementName)
                {
                    reader.Close();
                    return true;
                }
            }
            oSqlConn.Close();
            reader.Close();
            return false;

        }

        /// <summary>
        /// Check Existing AID Name
        /// </summary>
        /// <param name="sAIDName">string : AID Name</param>
        /// <returns>Bool : true if Existing</returns>
        private Boolean cekdataAID(string sAIDName)
        {
            sAIDName = sAIDName.ToString();
            oSqlConn.Open();
            string sql = "select AIDName from tbProfileAID";
            SqlCommand command = new SqlCommand(sql, oSqlConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sAIDName)
                {
                    reader.Close();
                    return true;
                }
            }
            oSqlConn.Close();
            reader.Close();
            return false;
            }

        /// <summary>
        /// Check Existing CAPK Name
        /// </summary>
        /// <param name="sCAPKName">string : CAPK Name</param>
        /// <returns>Bool : true if Existing</returns>
        private Boolean cekdataCAPK(string sCAPKName)
        {
            sCAPKName = sCAPKName.ToString();
            oSqlConn.Open();
            string sql = "select CAPKIndex from tbProfileCAPK";
            SqlCommand command = new SqlCommand(sql, oSqlConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sCAPKName)
                {
                    reader.Close();
                    return true;
                }
            }
            oSqlConn.Close();
            reader.Close();
            return false;
        }

        /// <summary>
        /// Check Existing Card Name
        /// </summary>
        /// <param name="sCardName">string: Card Name</param>
        /// <returns>Bool : true if Existing</returns>
        private Boolean cekdataCARD(string sCardName)
        {
            sCardName = sCardName.ToString();
            oSqlConn.Open();
            string sql = "select CardTagValue from tbProfileCARD";
            SqlCommand command = new SqlCommand(sql, oSqlConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sCardName)
                {
                    reader.Close();
                    return true;
                }
            }
            oSqlConn.Close();
            reader.Close();
            return false;
        }

        /// <summary>
        /// Check Existing TLE Name
        /// </summary>
        /// <param name="sTLEName">string : TLE Name</param>
        /// <returns>Bool : true if Existing</returns>
        private Boolean cekdataTLE(string sTLEName)
        {
            sTLEName = sTLEName.ToString();
            oSqlConn.Open();
            string sql = "select TLETagValue from tbProfileTLE WHERE TLETag ='TL001'";
          
            SqlCommand command = new SqlCommand(sql, oSqlConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sTLEName) 
                {
                    reader.Close();
                    return true;
                }
            }
            oSqlConn.Close();
            reader.Close();
            return false;
        }

        /// <summary>
        /// Check Existing TLE ID Name
        /// </summary>
        /// <param name="sTLEID">string: TLE ID</param>
        /// <returns>Bool : true if Existing</returns>
        private Boolean cekdataTLEId(string sTLEID)
        {
            sTLEID = sTLEID.ToString();
            oSqlConn.Open();
            string sql = "select TLETagValue from tbProfileTLE WHERE TLETag ='TL002' ";
            SqlCommand command = new SqlCommand(sql, oSqlConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sTLEID)
                {
                    reader.Close();
                    return true;
                }
            }
            oSqlConn.Close();
            reader.Close();
            return false;
        }
        
    }
 }






        
    

        
    
