namespace InSys
{
    partial class FrmEditCAPK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditCAPK));
            this.txtHash = new System.Windows.Forms.TextBox();
            this.txtRID = new System.Windows.Forms.TextBox();
            this.txtModulus = new System.Windows.Forms.TextBox();
            this.txtLength = new System.Windows.Forms.TextBox();
            this.txtExponent = new System.Windows.Forms.TextBox();
            this.txtIndex = new System.Windows.Forms.TextBox();
            this.LblHash = new System.Windows.Forms.Label();
            this.LblRID = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblModulus = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.lblExponent = new System.Windows.Forms.Label();
            this.lblIndex = new System.Windows.Forms.Label();
            this.gbHeader = new System.Windows.Forms.GroupBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.gbHeader.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtHash
            // 
            this.txtHash.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHash.Location = new System.Drawing.Point(118, 240);
            this.txtHash.MaxLength = 500;
            this.txtHash.Multiline = true;
            this.txtHash.Name = "txtHash";
            this.txtHash.Size = new System.Drawing.Size(232, 48);
            this.txtHash.TabIndex = 5;
            this.txtHash.Text = "TEXTBOX2";
            // 
            // txtRID
            // 
            this.txtRID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRID.Location = new System.Drawing.Point(118, 216);
            this.txtRID.MaxLength = 50;
            this.txtRID.Name = "txtRID";
            this.txtRID.Size = new System.Drawing.Size(232, 20);
            this.txtRID.TabIndex = 4;
            this.txtRID.Text = "TEXTBOX1";
            // 
            // txtModulus
            // 
            this.txtModulus.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtModulus.Location = new System.Drawing.Point(118, 88);
            this.txtModulus.Multiline = true;
            this.txtModulus.Name = "txtModulus";
            this.txtModulus.Size = new System.Drawing.Size(232, 120);
            this.txtModulus.TabIndex = 3;
            this.txtModulus.Text = "TEXTBOX4";
            // 
            // txtLength
            // 
            this.txtLength.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLength.Location = new System.Drawing.Point(118, 64);
            this.txtLength.MaxLength = 5;
            this.txtLength.Name = "txtLength";
            this.txtLength.Size = new System.Drawing.Size(232, 20);
            this.txtLength.TabIndex = 2;
            this.txtLength.Text = "TEXTBOX3";
            // 
            // txtExponent
            // 
            this.txtExponent.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtExponent.Location = new System.Drawing.Point(118, 40);
            this.txtExponent.MaxLength = 6;
            this.txtExponent.Name = "txtExponent";
            this.txtExponent.Size = new System.Drawing.Size(232, 20);
            this.txtExponent.TabIndex = 1;
            this.txtExponent.Text = "TEXTBOX2";
            // 
            // txtIndex
            // 
            this.txtIndex.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIndex.Location = new System.Drawing.Point(118, 16);
            this.txtIndex.MaxLength = 2;
            this.txtIndex.Name = "txtIndex";
            this.txtIndex.Size = new System.Drawing.Size(232, 20);
            this.txtIndex.TabIndex = 0;
            this.txtIndex.Text = "TEXTBOX1";
            // 
            // LblHash
            // 
            this.LblHash.Location = new System.Drawing.Point(6, 240);
            this.LblHash.Name = "LblHash";
            this.LblHash.Size = new System.Drawing.Size(100, 23);
            this.LblHash.TabIndex = 26;
            this.LblHash.Text = "Hash";
            // 
            // LblRID
            // 
            this.LblRID.Location = new System.Drawing.Point(6, 216);
            this.LblRID.Name = "LblRID";
            this.LblRID.Size = new System.Drawing.Size(100, 23);
            this.LblRID.TabIndex = 25;
            this.LblRID.Text = "RID";
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(241, 11);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSave.Location = new System.Drawing.Point(120, 11);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(115, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblModulus
            // 
            this.lblModulus.Location = new System.Drawing.Point(6, 87);
            this.lblModulus.Name = "lblModulus";
            this.lblModulus.Size = new System.Drawing.Size(100, 23);
            this.lblModulus.TabIndex = 19;
            this.lblModulus.Text = "Modulus";
            // 
            // lblLength
            // 
            this.lblLength.Location = new System.Drawing.Point(6, 65);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(100, 23);
            this.lblLength.TabIndex = 17;
            this.lblLength.Text = "Length";
            // 
            // lblExponent
            // 
            this.lblExponent.Location = new System.Drawing.Point(6, 41);
            this.lblExponent.Name = "lblExponent";
            this.lblExponent.Size = new System.Drawing.Size(100, 16);
            this.lblExponent.TabIndex = 16;
            this.lblExponent.Text = "Exponent";
            // 
            // lblIndex
            // 
            this.lblIndex.Location = new System.Drawing.Point(6, 16);
            this.lblIndex.Name = "lblIndex";
            this.lblIndex.Size = new System.Drawing.Size(100, 23);
            this.lblIndex.TabIndex = 14;
            this.lblIndex.Text = "Index";
            // 
            // gbHeader
            // 
            this.gbHeader.Controls.Add(this.lblIndex);
            this.gbHeader.Controls.Add(this.txtHash);
            this.gbHeader.Controls.Add(this.lblExponent);
            this.gbHeader.Controls.Add(this.txtRID);
            this.gbHeader.Controls.Add(this.lblLength);
            this.gbHeader.Controls.Add(this.txtModulus);
            this.gbHeader.Controls.Add(this.lblModulus);
            this.gbHeader.Controls.Add(this.txtLength);
            this.gbHeader.Controls.Add(this.LblRID);
            this.gbHeader.Controls.Add(this.txtExponent);
            this.gbHeader.Controls.Add(this.LblHash);
            this.gbHeader.Controls.Add(this.txtIndex);
            this.gbHeader.Location = new System.Drawing.Point(12, 2);
            this.gbHeader.Name = "gbHeader";
            this.gbHeader.Size = new System.Drawing.Size(362, 299);
            this.gbHeader.TabIndex = 27;
            this.gbHeader.TabStop = false;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(12, 302);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(362, 40);
            this.gbButton.TabIndex = 28;
            this.gbButton.TabStop = false;
            // 
            // FrmEditCAPK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 350);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmEditCAPK";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.FrmEditCAPK_Load);
            this.gbHeader.ResumeLayout(false);
            this.gbHeader.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TextBox txtHash;
        internal System.Windows.Forms.TextBox txtRID;
        internal System.Windows.Forms.TextBox txtModulus;
        internal System.Windows.Forms.TextBox txtLength;
        internal System.Windows.Forms.TextBox txtExponent;
        internal System.Windows.Forms.TextBox txtIndex;
        internal System.Windows.Forms.Label LblHash;
        internal System.Windows.Forms.Label LblRID;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Label lblModulus;
        internal System.Windows.Forms.Label lblLength;
        internal System.Windows.Forms.Label lblExponent;
        internal System.Windows.Forms.Label lblIndex;
        private System.Windows.Forms.GroupBox gbHeader;
        private System.Windows.Forms.GroupBox gbButton;
    }
}