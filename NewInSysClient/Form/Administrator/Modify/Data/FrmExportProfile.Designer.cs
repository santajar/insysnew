namespace InSys
{
    partial class FrmExportProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExportProfile));
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.chkFullTable = new System.Windows.Forms.CheckBox();
            this.txtTerminalID = new System.Windows.Forms.TextBox();
            this.chkMasterProfile = new System.Windows.Forms.CheckBox();
            this.chkProfile = new System.Windows.Forms.CheckBox();
            this.cmbDatabase = new System.Windows.Forms.ComboBox();
            this.lblDatabase = new System.Windows.Forms.Label();
            this.pbExport = new System.Windows.Forms.ProgressBar();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.bwExportProgress = new System.ComponentModel.BackgroundWorker();
            this.gbSource.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSource
            // 
            this.gbSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSource.Controls.Add(this.chkFullTable);
            this.gbSource.Controls.Add(this.txtTerminalID);
            this.gbSource.Controls.Add(this.chkMasterProfile);
            this.gbSource.Controls.Add(this.chkProfile);
            this.gbSource.Controls.Add(this.cmbDatabase);
            this.gbSource.Controls.Add(this.lblDatabase);
            this.gbSource.Location = new System.Drawing.Point(12, 12);
            this.gbSource.Name = "gbSource";
            this.gbSource.Size = new System.Drawing.Size(363, 122);
            this.gbSource.TabIndex = 0;
            this.gbSource.TabStop = false;
            // 
            // chkFullTable
            // 
            this.chkFullTable.AutoSize = true;
            this.chkFullTable.Location = new System.Drawing.Point(290, 71);
            this.chkFullTable.Name = "chkFullTable";
            this.chkFullTable.Size = new System.Drawing.Size(67, 17);
            this.chkFullTable.TabIndex = 4;
            this.chkFullTable.Text = "All Table";
            this.chkFullTable.UseVisualStyleBackColor = true;
            this.chkFullTable.CheckedChanged += new System.EventHandler(this.chkFullTable_CheckedChanged);
            // 
            // txtTerminalID
            // 
            this.txtTerminalID.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtTerminalID.Location = new System.Drawing.Point(105, 69);
            this.txtTerminalID.MaxLength = 8;
            this.txtTerminalID.Name = "txtTerminalID";
            this.txtTerminalID.Size = new System.Drawing.Size(100, 20);
            this.txtTerminalID.TabIndex = 3;
            this.txtTerminalID.Text = "TerminalID";
            this.txtTerminalID.Leave += new System.EventHandler(this.txtTerminalID_Leave);
            this.txtTerminalID.Enter += new System.EventHandler(this.txtTerminalID_Enter);
            // 
            // chkMasterProfile
            // 
            this.chkMasterProfile.AutoSize = true;
            this.chkMasterProfile.Location = new System.Drawing.Point(19, 94);
            this.chkMasterProfile.Name = "chkMasterProfile";
            this.chkMasterProfile.Size = new System.Drawing.Size(90, 17);
            this.chkMasterProfile.TabIndex = 2;
            this.chkMasterProfile.Text = "Master Profile";
            this.chkMasterProfile.UseVisualStyleBackColor = true;
            // 
            // chkProfile
            // 
            this.chkProfile.AutoSize = true;
            this.chkProfile.Location = new System.Drawing.Point(20, 71);
            this.chkProfile.Name = "chkProfile";
            this.chkProfile.Size = new System.Drawing.Size(55, 17);
            this.chkProfile.TabIndex = 1;
            this.chkProfile.Text = "Profile";
            this.chkProfile.UseVisualStyleBackColor = true;
            // 
            // cmbDatabase
            // 
            this.cmbDatabase.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDatabase.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDatabase.FormattingEnabled = true;
            this.cmbDatabase.Location = new System.Drawing.Point(103, 29);
            this.cmbDatabase.Name = "cmbDatabase";
            this.cmbDatabase.Size = new System.Drawing.Size(254, 21);
            this.cmbDatabase.TabIndex = 0;
            // 
            // lblDatabase
            // 
            this.lblDatabase.AutoSize = true;
            this.lblDatabase.Location = new System.Drawing.Point(17, 32);
            this.lblDatabase.Name = "lblDatabase";
            this.lblDatabase.Size = new System.Drawing.Size(53, 13);
            this.lblDatabase.TabIndex = 0;
            this.lblDatabase.Text = "Database";
            // 
            // pbExport
            // 
            this.pbExport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbExport.Location = new System.Drawing.Point(12, 144);
            this.pbExport.Name = "pbExport";
            this.pbExport.Size = new System.Drawing.Size(363, 23);
            this.pbExport.TabIndex = 1;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(6, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(95, 25);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "&Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(134, 12);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(95, 25);
            this.btnReset.TabIndex = 2;
            this.btnReset.Text = "&Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(262, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(95, 25);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnStart);
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnReset);
            this.gbButton.Location = new System.Drawing.Point(12, 170);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(363, 43);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // bwExportProgress
            // 
            this.bwExportProgress.WorkerReportsProgress = true;
            this.bwExportProgress.WorkerSupportsCancellation = true;
            this.bwExportProgress.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwExportProgress_DoWork);
            this.bwExportProgress.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwExportProgress_RunWorkerCompleted);
            // 
            // FrmExportProfile
            // 
            this.AcceptButton = this.btnStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 224);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.pbExport);
            this.Controls.Add(this.gbSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmExportProfile";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Export to Text File";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmExportProfile_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmExportData_FormClosed);
            this.Load += new System.EventHandler(this.FrmExportData_Load);
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.ComboBox cmbDatabase;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.CheckBox chkMasterProfile;
        private System.Windows.Forms.CheckBox chkProfile;
        private System.Windows.Forms.ProgressBar pbExport;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox gbButton;
        private System.ComponentModel.BackgroundWorker bwExportProgress;
        private System.Windows.Forms.TextBox txtTerminalID;
        private System.Windows.Forms.CheckBox chkFullTable;
    }
}