using System;
using System.ComponentModel;
using System.Linq;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using InSysClass;


namespace InSys
{
    /// <summary>
    /// Form to import the raw text profile into database
    /// </summary>
    public partial class FrmImportProfile : Form
    {
        /*
         * Import profile from txt file to database
         * Old flow, 
         * 1. app get list of txt filename, txt filename must be 8 digit
         * 2. app get txt content
         * 3. update terminalid tag based on filename
         * 4. app send content, terminalid, databaseid to database
         * 5. database do the import and separate the content based on tag
         * 
         * New flow,
         * 1. do the old step 1-3
         * 2. get itemlist of selected databaseid
         * 3. get table schema of tbProfileTerminalList, tbProfileTerminal, tbProfileAcquirer, tbProfileIssuer, tbProfileRelation
         * 4. check each tag of each file
         * 5. import profile
         */

        /// <summary>
        /// Local SQL Connection variable
        /// </summary>
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Boolan to replace all selected profile
        /// </summary>
        protected bool bReplaceAll;

        /// <summary>
        /// Folder path of raw text
        /// </summary>
        protected string sFolderPath;

        /// <summary>
        /// Selected DatabaseId to import
        /// </summary>
        protected int iDatabaseId;
        protected bool bCustomMenu= false;

        string[] arrsTerminalId;
        DataSet dsTables;

        /// <summary>
        /// Import profile form contructor
        /// </summary>
        /// <param name="_oSqlConn">SQLConnection : connection object</param>
        public FrmImportProfile(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            InitializeComponent();
        }

        private void FrmImportData_Load(object sender, EventArgs e)
        {
            InitComboBox();
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            ToggleOnOff(false);
            InitProperties();
            bwImport.RunWorkerAsync();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fBDialogSource = new FolderBrowserDialog())
            {
                fBDialogSource.RootFolder = Environment.SpecialFolder.MyComputer;
                fBDialogSource.SelectedPath = Application.StartupPath;
                fBDialogSource.ShowDialog();
                
                if (!string.IsNullOrEmpty(fBDialogSource.SelectedPath))
                {
                    txtFolder.Text = fBDialogSource.SelectedPath;
                    InitListFile(txtFolder.Text);
                }
            }
        }

        private void chkUpdateAll_CheckedChanged(object sender, EventArgs e)
        {
            bReplaceAll = chkUpdateAll.Checked;
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            SelectAllFile(chkSelectAll.Checked);
        }

        private void lvFileName_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            string sFileTerminal = e.Item.Text;
            if (e.Item.Checked == true)
                lvTerminalName.Items.Add(sFileTerminal);
            else
            {
                int iIndex = SearchFileIndex(sFileTerminal);
                if (iIndex >= 0) lvTerminalName.Items[iIndex].Remove();
            }
        }

        private void lvFileName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ListViewItem lvItem = lvFileName.SelectedItems[0];
                if (!string.IsNullOrEmpty(lvItem.Text))
                {
                    if (lvItem.Checked)
                        lvFileName.SelectedItems[0].Checked = false;
                    else
                        lvFileName.SelectedItems[0].Checked = true;
                }
            }
            catch (Exception)
            { }
        }

        private void bwImport_DoWork(object sender, DoWorkEventArgs e)
        {
            //StartImport(arrsTerminalId);
            StartNewImport(arrsTerminalId);
        }

        private void bwImport_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbImport.Value = e.ProgressPercentage;
            pbImport.PerformStep();
        }

        private void bwImport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string sDatabaseName = ((DataRowView)cmbDbDestination.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = "Terminal ID : \n";
            foreach (string sTID in arrsTerminalId)
                sLogDetail += string.Format("{0} \n", sTID);
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDatabaseName, "Import Terminal ID", sLogDetail);
            ToggleOnOff(true);            

            if (!string.IsNullOrEmpty(txtFolder.Text))
            {
                lvTerminalName.Items.Clear();
                lvFileName.Items.Clear();
                InitListFile(txtFolder.Text);
            }
        }

        #region "Function"
        /// <summary>
        /// Fill the combobox of database name with the database name
        /// and value with the database id
        /// </summary>
        protected void  InitComboBox()
        {
            DataTable dtDatabaseName = dtGetDatabase();
            if(dtDatabaseName.Rows.Count > 0)
            {
                cmbDbDestination.DataSource = dtDatabaseName;
                cmbDbDestination.DisplayMember = "DatabaseName";
                cmbDbDestination.ValueMember = "DatabaseId";
            }
        }

        /// <summary>
        /// Get the list of DatabaseName and DatabaseId
        /// </summary>
        /// <returns>DataTable : datatable of databasename</returns>
        protected DataTable dtGetDatabase()
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);
            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();
            return dtTemp;
        }

        /// <summary>
        /// Get the list of text file on the selected folder
        /// </summary>
        /// <param name="sFolderPath">string : folder name</param>
        protected void InitListFile(string sFolderPath)
        {
            DirectoryInfo di = new DirectoryInfo(sFolderPath);
            FileInfo[] arrfiTxt = di.GetFiles("*.txt");
            foreach (FileInfo fi in arrfiTxt)
                lvFileName.Items.Add(fi.Name.Replace(".txt", ""));
        }

        /// <summary>
        /// Set the list file is checked or not
        /// </summary>
        /// <param name="IsChecked">bool : check value</param>
        protected void SelectAllFile(bool IsChecked)
        {
            foreach (ListViewItem item in lvFileName.Items)
                item.Checked = IsChecked;
        }

        /// <summary>
        /// Get the index of list TerminalId
        /// </summary>
        /// <param name="sFileTerminal">string : TerminalId file</param>
        /// <returns>int : index of TerminalId</returns>
        protected int SearchFileIndex(string sFileTerminal)
        {
            foreach (ListViewItem item in lvTerminalName.Items)
                if (item.Text == sFileTerminal)
                    return lvTerminalName.Items.IndexOf(item);
            return -1;
        }

        /// <summary>
        /// Check the terminal textfile is exist or not
        /// </summary>
        /// <param name="sFileTerminal">string : filename</param>
        /// <returns>bool : </returns>
        protected bool IsFileTerminalExist(string sFileTerminal)
        {
            foreach (ListViewItem item in lvTerminalName.Items)
                if (item.Text == sFileTerminal)
                    return true;
            return false;
        }

        /// <summary>
        /// Enable/Disable of Group Box
        /// </summary>
        /// <param name="IsOnOff">bool : Value of Group Box</param>
        protected void ToggleOnOff(bool IsOnOff)
        {
            gbFolder.Enabled = IsOnOff;
            gbListView.Enabled = IsOnOff;
            gbButton.Enabled = IsOnOff;
        }

        /// <summary>
        /// Initialize all the variable properties
        /// </summary>
        protected void InitProperties()
        {
            sFolderPath = txtFolder.Text;
            bReplaceAll = chkUpdateAll.Checked;
            iDatabaseId = int.Parse(cmbDbDestination.SelectedValue.ToString());
            arrsTerminalId = new string[lvTerminalName.Items.Count];
            for (int i = 0; i < lvTerminalName.Items.Count; i++)
                arrsTerminalId[i] = lvTerminalName.Items[i].Text;
            InitTable();
        }

        /// <summary>
        /// Start the import process
        /// </summary>
        /// <param name="arrsTemp">string : array of string of TerminalId text file</param>
        protected void StartImport(string[] arrsTemp)
        {
            int iTotal = arrsTemp.Length;
            int iCount = 0;
            float fPercent;
            foreach (string sTerminalId in arrsTemp)
            {
                if(IsTerminalIdExist(sTerminalId) && bReplaceAll)
                    DeleteProfileTerminal(sTerminalId);

                if (!IsTerminalIdExist(sTerminalId))
                    if(CommonClass.isCanRegisterOldFile(sFolderPath + "\\", sTerminalId,
                        oSqlConn, iDatabaseId))
                        File.Delete(sFolderPath + "\\" + sTerminalId + ".txt");

                iCount++;
                fPercent = iCount / iTotal * 100;

                bwImport.ReportProgress(int.Parse(fPercent.ToString()));
            }
        }

        protected void StartNewImport(string[] arrsTemp)
        {
            int iTotal = arrsTemp.Length;
            int iCount = 0;
            float fPercent;            
            foreach (string sTerminalId in arrsTemp)
            {
                string  sTerminalID = sTerminalId.ToUpper();
                bool bTerminalExist = IsTerminalIdExist(sTerminalID);
                if (bTerminalExist && bReplaceAll)
                    DeleteProfileTerminal(sTerminalID);

                if (!bTerminalExist || bReplaceAll)
                {
                    string sContent = CommonLib.sReadTxtFile(string.Format(@"{0}\{1}.txt", sFolderPath, sTerminalID));
                    int iIndexStart, iIndexofDE001=-1;
                    iIndexofDE001 = sContent.IndexOf("DE001");
                    if (iIndexofDE001 == -1)
                        iIndexStart = sContent.IndexOf("DE0108") + "DE0108".Length;
                    else
                        iIndexStart = sContent.IndexOf("DE00108") + "DE00108".Length;
                    string sOldValue = sContent.Substring(iIndexStart, 8);
                    
                    StringBuilder sbContent = new StringBuilder(sContent);
                    sContent = sbContent.Replace(sOldValue, sTerminalID, iIndexStart, sTerminalID.Length).ToString();

                    sContent = sContent.Replace("'", "''");

                    DataTable dtProfileTLV = dtGetProfileTLV(sTerminalID, sContent);

                    SqlTransaction oTrans = oSqlConn.BeginTransaction();
                    string sError = null;
                    
                    // error happens whenever terminal exist without acquirer table
                    if (CommonClass.isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "relation", dtProfileTLV, dsTables, oTrans, oSqlConn, ref sError))
                        if (CommonClass.isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "issuer", dtProfileTLV, dsTables, oTrans, oSqlConn, ref sError))
                            if (CommonClass.isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "acquirer", dtProfileTLV, dsTables, oTrans, oSqlConn, ref sError))
                                if (CommonClass.isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "terminal", dtProfileTLV, dsTables, oTrans, oSqlConn, ref sError))
                                    if (CommonClass.isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "count", dtProfileTLV, dsTables, oTrans, oSqlConn, ref sError))
                                    {
                                        if (bCustomMenu)
                                            if (CommonClass.isBulkImportProfileSuccess(sTerminalID, iDatabaseId, "custommenu", dtProfileTLV, dsTables, oTrans, oSqlConn, ref sError))
                                            {
                                                oTrans.Commit();
                                                File.Delete(sFolderPath + "\\" + sTerminalID + ".txt");
                                                sContent = "";
                                            }
                                            else
                                            { oTrans.Rollback(); CommonClass.doWriteErrorFile(sError, false); MessageBox.Show(sError); }
                                        else
                                        {
                                            oTrans.Commit();
                                            File.Delete(sFolderPath + "\\" + sTerminalID + ".txt");
                                            sContent = "";
                                        }
                                    }
                                    else
                                    { oTrans.Rollback(); CommonClass.doWriteErrorFile(sError, false); MessageBox.Show(sError); }
                                else
                                { oTrans.Rollback(); CommonClass.doWriteErrorFile(sError, false); MessageBox.Show(sError); }
                            else
                            { oTrans.Rollback(); CommonClass.doWriteErrorFile(sError, false); }
                        else
                        { oTrans.Rollback(); CommonClass.doWriteErrorFile(sError, false); }
                    else
                    { oTrans.Rollback(); CommonClass.doWriteErrorFile(sError, false); }

                }
                iCount++;
                fPercent = iCount / iTotal * 100;

                bwImport.ReportProgress(int.Parse(fPercent.ToString()));
            }
        }

        /// <summary>
        /// Return the Query condition with specified TerminalId
        /// </summary>
        /// <param name="sTerminalId">string : TerminalId</param>
        /// <returns>string : Query condition</returns>
        protected string sCondition(string sTerminalId)
        {
            return "WHERE TerminalId='" + sTerminalId + "'";
        }

        /// <summary>
        /// Return the boolean of existence of specified TerminalId
        /// </summary>
        /// <param name="sTerminalId">string : TerminalId</param>
        /// <returns>bool : True, TerminalId allready exist otherwise False</returns>
        protected bool IsTerminalIdExist(string sTerminalId)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition(sTerminalId);
            SqlDataAdapter da = new SqlDataAdapter(oCmd);
            DataTable dtTemp = new DataTable();
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            da.Fill(dtTemp);
            da.Dispose();
            oCmd.Dispose();

            return dtTemp.Rows.Count > 0 ? true : false;
        }

        /// <summary>
        /// To Delete the specified TerminalId
        /// </summary>
        /// <param name="sTerminalId">string : TerminalId</param>
        protected void DeleteProfileTerminal(string sTerminalId)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileDelete, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oCmd.ExecuteNonQuery();
            oCmd.Dispose();
        }

        protected void InitTable()
        {
            dsTables = new DataSet();
            string sQuery = "";
            try
            {
                sQuery = string.Format("exec spItemListBrowse 'WHERE DatabaseID={0}'\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}"
                    , iDatabaseId
                    , "SELECT * FROM tbProfileTerminalList WHERE TerminalID = null"
                    , "SELECT TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue FROM tbProfileTerminal WHERE TerminalID = null"
                    , "SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = null"
                    , "SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue FROM tbProfileIssuer WHERE TerminalID = null"
                    , "SELECT TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue FROM tbProfileRelation WHERE TerminalID = null"
                    , "SELECT TerminalID,CustomMenuTag,CustomMenuLengthOfTagLength,CustomMenuTagLength,CustomMenuTagValue FROM tbProfileCustomMenu WHERE TerminalID = null");
                bCustomMenu = true;
            }
            catch
            {
                sQuery = string.Format("exec spItemListBrowse 'WHERE DatabaseID={0}'\n{1}\n{2}\n{3}\n{4}\n{5}"
                    , iDatabaseId
                    , "SELECT * FROM tbProfileTerminalList WHERE TerminalID = null"
                    , "SELECT TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue FROM tbProfileTerminal WHERE TerminalID = null"
                    , "SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = null"
                    , "SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue FROM tbProfileIssuer WHERE TerminalID = null"
                    , "SELECT TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue FROM tbProfileRelation WHERE TerminalID = null"
                    , "SELECT TerminalID,CustomMenuTag,CustomMenuLengthOfTagLength,CustomMenuTagLength,CustomMenuTagValue FROM tbProfileCustomMenu WHERE TerminalID = null");
            }
            using (SqlCommand oCmd = new SqlCommand(sQuery,oSqlConn))
            {
                SqlDataAdapter oAdapt = new SqlDataAdapter(oCmd);
                oAdapt.Fill(dsTables);
            }
        }

        protected DataTable dtGetProfileTLV(string sTerminalID, string sContent)
        {
            DataTable dtItemList = dsTables.Tables[0];
            DataTable dtTemp = new DataTable();

            #region "OLD"            
            //DataColumn colRowId = new DataColumn("RowID");
            //colRowId.AutoIncrement = true;
            //colRowId.AutoIncrementSeed = 1;
            //DataColumn colTerminalID = new DataColumn("TerminalID");
            //colTerminalID.DefaultValue = sTerminalID;
            //DataColumn colName = new DataColumn("Name");
            //DataColumn colTag = new DataColumn("Tag");
            //DataColumn colTagLength = new DataColumn("TagLength");
            //DataColumn colTagValue = new DataColumn("TagValue");
            //dtTemp.Columns.Add(colRowId);
            //dtTemp.Columns.Add(colTerminalID);
            //dtTemp.Columns.Add(colName);
            //dtTemp.Columns.Add(colTag);
            //dtTemp.Columns.Add(colTagLength);
            //dtTemp.Columns.Add(colTagValue);

            //int iIndex = 0;
            //string sTag;
            //int iLengthOfTagLength = 2;
            //string sName = sTerminalID;
            //string sTagLength;
            //string sTagValue;
            //while (iIndex < sContent.Length)
            //{
            //    sTag = sContent.Substring(iIndex, 4);
            //    iIndex += 4;

            //    DataRow[] rowResult = dtItemList.Select(string.Format("Tag='{0}'", sTag));
            //    if (rowResult != null && rowResult.Length > 0)
            //    {
            //        iLengthOfTagLength = int.Parse(rowResult[0]["LengthOfTagLength"].ToString());
            //        sTagLength = sContent.Substring(iIndex, iLengthOfTagLength);
            //        iIndex += iLengthOfTagLength;
            //        sTagValue = sContent.Substring(iIndex, int.Parse(sTagLength));
            //        iIndex += int.Parse(sTagLength);

            //        if (sTag.Substring(2) == "01" && sTag != "DC01")
            //            sName = sTagValue;

            //        DataRow rowNew = dtTemp.NewRow();
            //        rowNew["Tag"] = sTag;
            //        rowNew["TagLength"] = sTagLength;
            //        rowNew["TagValue"] = sTagValue;
            //        rowNew["Name"] = sName;
            //        dtTemp.Rows.Add(rowNew);
            //    }
            //    else
            //        iIndex = sContent.IndexOf(sTag.Substring(0, 2), iIndex + 2);
            //}
            #endregion
            int iIndex = -1;
            iIndex = sContent.IndexOf("DE001");
            if (iIndex == -1)
            {
                using (SqlCommand oCmd = new SqlCommand(
                    string.Format("select * from dbo.fn_tbProfileTLV('{0}','{1}')", sTerminalID, sContent)
                    , oSqlConn))
                {
                    new SqlDataAdapter(oCmd).Fill(dtTemp);
                }
            }
            else
            {
                using (SqlCommand oCmd = new SqlCommand(
                    string.Format("select * from dbo.fn_tbProfileTLV5('{0}','{1}')", sTerminalID, sContent)
                    , oSqlConn))
                {
                    new SqlDataAdapter(oCmd).Fill(dtTemp);
                }
            }

            //string stag1,stag2;
            if (dtItemList.Select("Tag = 'DE01'").Length == 0 && iIndex == -1)
            {
                dtTemp.Select()
                    .ToList<DataRow>()
                    .ForEach(Temp =>
                        {
                            Temp["Tag"] = Temp["Tag"].ToString().Substring(0, 2) + "0" + Temp["Tag"].ToString().Substring(2, 2); 
                            //stag1 = Temp["Tag"].ToString().Substring(0, 2);
                            //stag2 = Temp["Tag"].ToString().Substring(2, 2); 
                        });
            }
            dtTemp = dtGetProfileTLVDefault(dtTemp, dtItemList);
            return dtTemp;
        }

        protected DataTable dtGetProfileTLVDefault(DataTable dtTLV, DataTable dtItemList)
        {
            DataTable dtFormId = dtItemList.DefaultView.ToTable(true, "FormID");
            foreach (DataRow row in dtFormId.Rows)
            {
                string sFormID = row["FormID"].ToString();
                DataRow[] drItempTemp = dtItemList.Select(string.Format("FormID='{0}'", sFormID));
                foreach (DataRow rowItem in drItempTemp)
                {
                    string sNewTag = rowItem["Tag"].ToString();
                    if (dtTLV.Select(string.Format("Tag='{0}'", sNewTag)).Length == 0)
                    {
                        string sNewTagValue = rowItem["DefaultValue"].ToString();
                        // filter per DE01/AA01/AE01/AD01/TL01
                        string sTagFilter = "0";
                        if (dtItemList.Select("Tag = 'DE01'").Length > 0)
                        {
                            switch (sFormID)
                            {
                                case "1":
                                    sTagFilter = "DE01"; break;
                                case "2":
                                    sTagFilter = "AA01"; break;
                                case "3":
                                    sTagFilter = "AE01"; break;
                            }
                        }
                        else
                        {
                            switch (sFormID)
                            {
                                case "1":
                                    sTagFilter = "DE001"; break;
                                case "2":
                                    sTagFilter = "AA001"; break;
                                case "3":
                                    sTagFilter = "AE001"; break;
                            }                        
                        }
                        if (sTagFilter != "0")
                        {
                            DataRow[] drTLVHeader = dtTLV.Select(string.Format("Tag='{0}'", sTagFilter));
                            if (drTLVHeader.Length > 0)
                                foreach (DataRow rowTLV in drTLVHeader)
                                {
                                    string sName = rowTLV["Name"].ToString();
                                    DataRow rowNewTLV = dtTLV.NewRow();
                                    rowNewTLV["Name"] = sName;
                                    rowNewTLV["Tag"] = sNewTag;
                                    rowNewTLV["TagLength"] = sNewTagValue.Length;
                                    rowNewTLV["TagValue"] = sNewTagValue;
                                    dtTLV.Rows.Add(rowNewTLV);
                                }
                        }
                    }
                }
            }
            return dtTLV;
        }
        #endregion
    }
}