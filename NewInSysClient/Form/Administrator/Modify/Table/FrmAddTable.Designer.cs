namespace InSys
{
    partial class FrmAddTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAddTable));
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.chkMsOnly = new System.Windows.Forms.CheckBox();
            this.chkIncludeMs = new System.Windows.Forms.CheckBox();
            this.cmbTerminal = new System.Windows.Forms.ComboBox();
            this.cmbDbSource = new System.Windows.Forms.ComboBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.lblDbSource = new System.Windows.Forms.Label();
            this.gbNewCategory = new System.Windows.Forms.GroupBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.chkMoveCard = new System.Windows.Forms.CheckBox();
            this.txtIssuer = new System.Windows.Forms.TextBox();
            this.txtAcquirer = new System.Windows.Forms.TextBox();
            this.lblssuer = new System.Windows.Forms.Label();
            this.lblAcquirer = new System.Windows.Forms.Label();
            this.rdCard = new System.Windows.Forms.RadioButton();
            this.rdIssuer = new System.Windows.Forms.RadioButton();
            this.rdAcquirer = new System.Windows.Forms.RadioButton();
            this.pbAddTable = new System.Windows.Forms.ProgressBar();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbSource.SuspendLayout();
            this.gbNewCategory.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSource
            // 
            this.gbSource.Controls.Add(this.chkMsOnly);
            this.gbSource.Controls.Add(this.chkIncludeMs);
            this.gbSource.Controls.Add(this.cmbTerminal);
            this.gbSource.Controls.Add(this.cmbDbSource);
            this.gbSource.Controls.Add(this.lblTerminal);
            this.gbSource.Controls.Add(this.lblDbSource);
            this.gbSource.Location = new System.Drawing.Point(15, 12);
            this.gbSource.Name = "gbSource";
            this.gbSource.Size = new System.Drawing.Size(300, 154);
            this.gbSource.TabIndex = 0;
            this.gbSource.TabStop = false;
            this.gbSource.Text = "Source";
            // 
            // chkMsOnly
            // 
            this.chkMsOnly.AutoSize = true;
            this.chkMsOnly.Location = new System.Drawing.Point(18, 119);
            this.chkMsOnly.Name = "chkMsOnly";
            this.chkMsOnly.Size = new System.Drawing.Size(82, 17);
            this.chkMsOnly.TabIndex = 4;
            this.chkMsOnly.Text = "Master Only";
            this.chkMsOnly.UseVisualStyleBackColor = true;
            // 
            // chkIncludeMs
            // 
            this.chkIncludeMs.AutoSize = true;
            this.chkIncludeMs.Location = new System.Drawing.Point(19, 94);
            this.chkIncludeMs.Name = "chkIncludeMs";
            this.chkIncludeMs.Size = new System.Drawing.Size(96, 17);
            this.chkIncludeMs.TabIndex = 3;
            this.chkIncludeMs.Text = "Include Master";
            this.chkIncludeMs.UseVisualStyleBackColor = true;
            // 
            // cmbTerminal
            // 
            this.cmbTerminal.FormattingEnabled = true;
            this.cmbTerminal.Location = new System.Drawing.Point(97, 60);
            this.cmbTerminal.Name = "cmbTerminal";
            this.cmbTerminal.Size = new System.Drawing.Size(185, 21);
            this.cmbTerminal.TabIndex = 2;
            // 
            // cmbDbSource
            // 
            this.cmbDbSource.FormattingEnabled = true;
            this.cmbDbSource.Location = new System.Drawing.Point(97, 24);
            this.cmbDbSource.Name = "cmbDbSource";
            this.cmbDbSource.Size = new System.Drawing.Size(185, 21);
            this.cmbDbSource.TabIndex = 1;
            // 
            // lblTerminal
            // 
            this.lblTerminal.AutoSize = true;
            this.lblTerminal.Location = new System.Drawing.Point(16, 63);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(47, 13);
            this.lblTerminal.TabIndex = 1;
            this.lblTerminal.Text = "Terminal";
            // 
            // lblDbSource
            // 
            this.lblDbSource.AutoSize = true;
            this.lblDbSource.Location = new System.Drawing.Point(16, 27);
            this.lblDbSource.Name = "lblDbSource";
            this.lblDbSource.Size = new System.Drawing.Size(53, 13);
            this.lblDbSource.TabIndex = 0;
            this.lblDbSource.Text = "Database";
            // 
            // gbNewCategory
            // 
            this.gbNewCategory.Controls.Add(this.btnLoad);
            this.gbNewCategory.Controls.Add(this.chkMoveCard);
            this.gbNewCategory.Controls.Add(this.txtIssuer);
            this.gbNewCategory.Controls.Add(this.txtAcquirer);
            this.gbNewCategory.Controls.Add(this.lblssuer);
            this.gbNewCategory.Controls.Add(this.lblAcquirer);
            this.gbNewCategory.Controls.Add(this.rdCard);
            this.gbNewCategory.Controls.Add(this.rdIssuer);
            this.gbNewCategory.Controls.Add(this.rdAcquirer);
            this.gbNewCategory.Location = new System.Drawing.Point(336, 12);
            this.gbNewCategory.Name = "gbNewCategory";
            this.gbNewCategory.Size = new System.Drawing.Size(350, 154);
            this.gbNewCategory.TabIndex = 1;
            this.gbNewCategory.TabStop = false;
            this.gbNewCategory.Text = "Add New Category";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(240, 125);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(104, 23);
            this.btnLoad.TabIndex = 8;
            this.btnLoad.Text = "Load Item";
            this.btnLoad.UseVisualStyleBackColor = true;
            // 
            // chkMoveCard
            // 
            this.chkMoveCard.AutoSize = true;
            this.chkMoveCard.Location = new System.Drawing.Point(127, 97);
            this.chkMoveCard.Name = "chkMoveCard";
            this.chkMoveCard.Size = new System.Drawing.Size(111, 17);
            this.chkMoveCard.TabIndex = 7;
            this.chkMoveCard.Text = "Move Card if Exist";
            this.chkMoveCard.UseVisualStyleBackColor = true;
            // 
            // txtIssuer
            // 
            this.txtIssuer.Location = new System.Drawing.Point(185, 71);
            this.txtIssuer.Name = "txtIssuer";
            this.txtIssuer.Size = new System.Drawing.Size(150, 20);
            this.txtIssuer.TabIndex = 6;
            // 
            // txtAcquirer
            // 
            this.txtAcquirer.Location = new System.Drawing.Point(185, 45);
            this.txtAcquirer.Name = "txtAcquirer";
            this.txtAcquirer.Size = new System.Drawing.Size(150, 20);
            this.txtAcquirer.TabIndex = 5;
            // 
            // lblssuer
            // 
            this.lblssuer.AutoSize = true;
            this.lblssuer.Location = new System.Drawing.Point(124, 74);
            this.lblssuer.Name = "lblssuer";
            this.lblssuer.Size = new System.Drawing.Size(35, 13);
            this.lblssuer.TabIndex = 4;
            this.lblssuer.Text = "Issuer";
            // 
            // lblAcquirer
            // 
            this.lblAcquirer.AutoSize = true;
            this.lblAcquirer.Location = new System.Drawing.Point(124, 48);
            this.lblAcquirer.Name = "lblAcquirer";
            this.lblAcquirer.Size = new System.Drawing.Size(46, 13);
            this.lblAcquirer.TabIndex = 3;
            this.lblAcquirer.Text = "Acquirer";
            // 
            // rdCard
            // 
            this.rdCard.AutoSize = true;
            this.rdCard.Location = new System.Drawing.Point(16, 72);
            this.rdCard.Name = "rdCard";
            this.rdCard.Size = new System.Drawing.Size(47, 17);
            this.rdCard.TabIndex = 2;
            this.rdCard.TabStop = true;
            this.rdCard.Text = "Card";
            this.rdCard.UseVisualStyleBackColor = true;
            // 
            // rdIssuer
            // 
            this.rdIssuer.AutoSize = true;
            this.rdIssuer.Location = new System.Drawing.Point(16, 46);
            this.rdIssuer.Name = "rdIssuer";
            this.rdIssuer.Size = new System.Drawing.Size(53, 17);
            this.rdIssuer.TabIndex = 1;
            this.rdIssuer.TabStop = true;
            this.rdIssuer.Text = "Issuer";
            this.rdIssuer.UseVisualStyleBackColor = true;
            // 
            // rdAcquirer
            // 
            this.rdAcquirer.AutoSize = true;
            this.rdAcquirer.Location = new System.Drawing.Point(16, 19);
            this.rdAcquirer.Name = "rdAcquirer";
            this.rdAcquirer.Size = new System.Drawing.Size(64, 17);
            this.rdAcquirer.TabIndex = 0;
            this.rdAcquirer.TabStop = true;
            this.rdAcquirer.Text = "Acquirer";
            this.rdAcquirer.UseVisualStyleBackColor = true;
            // 
            // pbAddTable
            // 
            this.pbAddTable.Location = new System.Drawing.Point(15, 219);
            this.pbAddTable.Name = "pbAddTable";
            this.pbAddTable.Size = new System.Drawing.Size(671, 23);
            this.pbAddTable.TabIndex = 3;
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnSave);
            this.gbButton.Location = new System.Drawing.Point(15, 172);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(671, 41);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(565, 12);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(459, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FrmAddTable
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(701, 630);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.pbAddTable);
            this.Controls.Add(this.gbNewCategory);
            this.Controls.Add(this.gbSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmAddTable";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add New Table";
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.gbNewCategory.ResumeLayout(false);
            this.gbNewCategory.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.CheckBox chkMsOnly;
        private System.Windows.Forms.CheckBox chkIncludeMs;
        private System.Windows.Forms.ComboBox cmbTerminal;
        private System.Windows.Forms.ComboBox cmbDbSource;
        private System.Windows.Forms.Label lblTerminal;
        private System.Windows.Forms.Label lblDbSource;
        private System.Windows.Forms.GroupBox gbNewCategory;
        private System.Windows.Forms.RadioButton rdCard;
        private System.Windows.Forms.RadioButton rdIssuer;
        private System.Windows.Forms.RadioButton rdAcquirer;
        private System.Windows.Forms.Label lblssuer;
        private System.Windows.Forms.Label lblAcquirer;
        private System.Windows.Forms.CheckBox chkMoveCard;
        private System.Windows.Forms.TextBox txtIssuer;
        private System.Windows.Forms.TextBox txtAcquirer;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.ProgressBar pbAddTable;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
    }
}