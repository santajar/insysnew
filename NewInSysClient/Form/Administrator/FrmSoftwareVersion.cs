using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmSoftwareVersion : Form
    {
        private SqlConnection oSqlConn;
        private SqlConnection oSqlConnAuditTrail;
        private DataTable dtAvailableSW;
        private DataTable dtRegisteredSW;

        /// <summary>
        /// Form to assign Software version to Version Profile
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        public FrmSoftwareVersion(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        private void FrmSoftwareVersion_Load(object sender, EventArgs e)
        {
            InitCmb();
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        /// <summary>
        /// Initiate ComboBox
        /// </summary>
        protected void InitCmb()
        {
            cmbDatabaseID.DataSource = dtLoadVersion();
            if (cmbDatabaseID.DataSource != null)
            {
                cmbDatabaseID.DisplayMember = "DatabaseName";
                cmbDatabaseID.ValueMember = "DatabaseID";
                cmbDatabaseID.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// Load Version Profile data from database
        /// </summary>
        /// <returns>DataTable : Representing data from database</returns>
        protected DataTable dtLoadVersion()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalDBBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "";
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                        oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }

            return dtTable;
        }

        /// <summary>
        /// Get Version ID for selected Version Profile
        /// </summary>
        /// <returns>string : Versiong ID</returns>
        protected string sGetDbID()
        {
            return (cmbDatabaseID.SelectedIndex >= 0) ? cmbDatabaseID.SelectedValue.ToString() : "";
        }

        private void cmbDatabaseID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDatabaseID.DisplayMember) && !string.IsNullOrEmpty(cmbDatabaseID.ValueMember) &&
                 cmbDatabaseID.SelectedIndex >= 0)
            {
                InitAvailableList();
                InitRegisteredList();
            }
        }

        /// <summary>
        /// Initiate Listbox of available Software
        /// </summary>
        protected void InitAvailableList()
        {
            dtAvailableSW = dtLoadAvailableSW();
            if (dtAvailableSW != null)
            {
                lbAvailableSW.DataSource = dtAvailableSW;
                lbAvailableSW.DisplayMember = "ApplicationName";
                lbAvailableSW.ValueMember = "ApplicationName";
                lbAvailableSW.ClearSelected();
            }
        }

        /// <summary>
        /// Load Available software data from database
        /// </summary>
        /// <returns>DataTable : Representing data from database</returns>
        protected DataTable dtLoadAvailableSW()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareAvailableBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sGetDbID();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        oAdapt.Fill(dtTable);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }

            return dtTable;
        }

        /// <summary>
        /// Initiate listbox for Registered software
        /// </summary>
        protected void InitRegisteredList()
        {
            dtRegisteredSW = dtLoadRegisteredSW();
            if (dtRegisteredSW != null)
            {
                lbRegisteredSW.DataSource = dtRegisteredSW;
                lbRegisteredSW.DisplayMember = "SoftwareName";
                lbRegisteredSW.ValueMember = "SoftwareName";
                lbRegisteredSW.ClearSelected();
            }
        }

        /// <summary>
        /// Load Registered software data from database
        /// </summary>
        /// <returns>DataTable : Representing data from database</returns>
        protected DataTable dtLoadRegisteredSW()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareRegisteredBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.VarChar).Value = sGetDbID();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        oAdapt.Fill(dtTable);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }

            return dtTable;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (lbAvailableSW.SelectedIndices.Count == 1)
            {
                AddRemoveApps(ref dtRegisteredSW, ref dtAvailableSW, "SoftwareName",
                              lbAvailableSW.SelectedValue.ToString(), (DataRowView)lbAvailableSW.SelectedItem);
            }
        }

        /// <summary>
        /// Moving selected Software from Available listbox to Registered listbox, vice versa
        /// </summary>
        /// <param name="oTableAdd">DataTable : DataTable which new application will be added</param>
        /// <param name="oTableDel">DataTable : DataTable which selected application will be deleted</param>
        /// <param name="sColName">String : ColumnName from DataTable which new application will be added</param>
        /// <param name="sAppsName">String : Application's name that is moved</param>
        /// <param name="drvView">DataRowView : DataRowView from selected item</param>
        protected void AddRemoveApps(ref DataTable oTableAdd, ref DataTable oTableDel, string sColName, 
                                         string sAppsName, DataRowView drvView)
        {
            DataRow drRow = oTableAdd.NewRow();
            drRow[sColName] = sAppsName;
            oTableAdd.Rows.Add(drRow);

            oTableDel.Rows.Remove(drvView.Row);

            lbAvailableSW.Refresh();
            lbRegisteredSW.Refresh();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lbRegisteredSW.SelectedIndices.Count == 1)
            {
                AddRemoveApps(ref dtAvailableSW, ref dtRegisteredSW, "ApplicationName", 
                              lbRegisteredSW.SelectedValue.ToString(),(DataRowView)lbRegisteredSW.SelectedItem);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
            //    if (isDataSaved())
            //        this.Close();
            if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                isDataSaved();
        }

        /// <summary>
        /// Determine whether all data saved successfully to database or not
        /// </summary>
        /// <returns>bool : True if successful, else false</returns>
        protected bool isDataSaved()
        {
            SqlTransaction oTransaction = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                doDeleteApplication(oTransaction);

                for (int iCount = 0; iCount < lbRegisteredSW.Items.Count; iCount++)
                    doSaveApplications(oTransaction, ((DataRowView)lbRegisteredSW.Items[iCount])["SoftwareName"].ToString());
                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                oTransaction.Rollback();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Delete all applications for selected version before inserting new data
        /// </summary>
        /// <param name="oTransaction">SqlTransaction : Transaction used in current operation</param>
        protected void doDeleteApplication(SqlTransaction oTransaction)
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareDelete, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Transaction = oTransaction;
            oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = sGetDbID();
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();
            oSqlCmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Insert new Application data to database
        /// </summary>
        /// <param name="oTransaction">SqlTransaction : Transaction used in current operation</param>
        /// <param name="sApplicationName">String : Application name that will be inserted to database</param>
        protected void doSaveApplications(SqlTransaction oTransaction, string sApplicationName)
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSoftwareInsert, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Transaction = oTransaction;
            oSqlCmd.Parameters.Add("@sSoftwareName", SqlDbType.VarChar).Value = sApplicationName;
            oSqlCmd.Parameters.Add("@iDatabaseID", SqlDbType.Int).Value = sGetDbID();
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();
            oSqlCmd.ExecuteNonQuery();
        }
    }
}