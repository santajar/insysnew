using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmRegister : Form
    {
        protected SqlConnection oSqlConn;

        /// <summary>
        /// Form to register application 
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        public FrmRegister(SqlConnection _oSqlConn)
        {
            oSqlConn = _oSqlConn;
            InitializeComponent();
            CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
                
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValid())
                {
                    if (IsRegisterValid())
                    {
                        CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sRegisterSuccess, "");
                        bRegistered = true;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Registration failed. Please check Username, Company, and License Key field.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                    MessageBox.Show("Username, Company, and License Key are required.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception oException)
            {
                if (oException.Message.StartsWith("Access to the path") && oException.Message.EndsWith("is denied."))
                {
                    MessageBox.Show("You don't have enough privilege on this computer. Please contact your system administrator.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (oException.Message == "Invalid length for a Base-64 char array." || oException.Message == "Length of the data to decrypt is invalid.")
                {
                    MessageBox.Show("Registration failed. Please check Username, Company, and License Key field.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtClear();
                }
                else CommonClass.doWriteErrorFile(oException.Message);
            }
        }

        #region"Properties"
        /// <summary>
        /// Username to licensed the application
        /// </summary>
        protected string sUserName
        {
            get
            {
                txtUsername.Focus();
                return txtUsername.Text;
            }
        }
        
        /// <summary>
        /// Customer's Company Name
        /// </summary>
        protected string sCompany
        {
            get
            {
                txtCompany.Focus();
                return txtCompany.Text;
            }
        }

        /// <summary>
        /// License Key to register application
        /// </summary>
        protected string sLicenseKey
        {
            get
            {
                txtLicense.Focus();
                return txtLicense.Text;
            }
        }
        
        /// <summary>
        /// Clear all entered data 
        /// </summary>
        protected void TxtClear()
        {
            txtUsername.Clear();
            txtCompany.Clear();
            txtLicense.Clear();
            txtUsername.Focus();
        }

        /// <summary>
        /// Determine whether application have already been registered or not
        /// </summary>
        public bool bRegistered = false;
        #endregion

        #region "Validation"
        /// <summary>
        /// Determine whether inputed data valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsValid()
        {
            return (!string.IsNullOrEmpty(sUserName) 
                && !string.IsNullOrEmpty(sCompany) 
                && !string.IsNullOrEmpty(sLicenseKey));
        }

        /// <summary>
        /// Determine wheteher Registration valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool IsRegisterValid()
        {
            LicenseIP oLicense = new LicenseIP(Application.ProductName);
            return oLicense.IsActivated(sUserName, sCompany, sLicenseKey);
        }
        #endregion
    }
}