using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmApplicationsMgmt : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        protected DataTable oTableApps;

        /// <summary>
        /// Form to manage Applications data
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        public FrmApplicationsMgmt(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        private void FrmApplications_Load(object sender, EventArgs e)
        {
            try
            {
                InitForm();
                InitData();
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void lbApplicationName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(lbApplicationName.DisplayMember) && !string.IsNullOrEmpty(lbApplicationName.ValueMember))
                FillDetailData();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (gbDetailData.Enabled)
                    AddData();
                else
                {
                    InitDetailData(true, false, "new");
                    ClearDetailData();
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbApplicationName.SelectedIndex >= 0)
                {
                    if (string.IsNullOrEmpty(txtApplicationName.Text))
                        FillDetailData();
                    if (gbDetailData.Enabled)
                        UpdateData();
                    else
                        InitDetailData(true, true, "edit");
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbApplicationName.SelectedIndex >= 0)
                    if (CommonClass.isYesMessage(CommonMessage.sConfirmationDelete, CommonMessage.sConfirmationTitle))
                        DeleteData();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (gbDetailData.Enabled)
                InitDetailData(false, !txtApplicationName.Enabled,"");
            else
                this.Close();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (lbApplicationName.SelectedIndex >= 0)
            {
                FrmPromptString fPromptAppName = new FrmPromptString("Copy Application", "New Application Name");
                fPromptAppName.txtInput.MaxLength = txtApplicationName.MaxLength;
                if (fPromptAppName.ShowDialog() == DialogResult.OK)
                {
                    string sNewAppName = fPromptAppName.txtInput.Text;
                    if (!string.IsNullOrEmpty(sNewAppName))
                        isSuccessSave(false, sNewAppName);
                }
            }
        }

        #region "Function"
        /// <summary>
        /// Load data from database then show it on form
        /// </summary>
        protected void InitData()
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPApplicationBrowse, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                {
                    oTableApps = new DataTable();
                    oAdapt.Fill(oTableApps);
                    FillListBox();
                }
            }
        }

        /// <summary>
        /// Fill ListBox with data from database
        /// </summary>
        protected void FillListBox()
        {
            lbApplicationName.DataSource = oTableApps;
            lbApplicationName.DisplayMember = "ApplicationName";
            lbApplicationName.ValueMember = "ApplicationName";
            lbApplicationName.SelectedIndex = -1;
        }

        /// <summary>
        /// Initiate Form display
        /// </summary>
        protected void InitForm()
        {
            gbDetailData.Enabled = false;
        }

        /// <summary>
        /// Get Application Name
        /// </summary>
        /// <returns>string : Application Name</returns>
        protected string sGetAppsName()
        {
            return txtApplicationName.Text.Trim();
        }

        /// <summary>
        /// Get Allow Compress Value
        /// </summary>
        /// <returns>string : Allow Compress</returns>
        protected string sGetAllowCompress()
        {
            return (rdAllowCompressNo.Checked) ? "0" :
                (rdAllowCompressYes.Checked) ? "1" : "";
        }

        protected string sGetStandardIso()
        {
            return (chkStandartISO8583.Checked==false) ? "0" : (chkStandartISO8583.Checked==true) ? "1" : "";
        }

        /// <summary>
        /// Get Bit Map value
        /// </summary>
        /// <returns>string : Bit Map</returns>
        protected string sGetBitMap()
        {
            return txtBitMap.Text.Trim();
        }

        /// <summary>
        /// Get Hex Bit Map Value
        /// </summary>
        /// <returns>string : Hex Bit Map</returns>
        protected string sGetHexBitMap()
        {
            return txtHexBitMap.Text.Trim();
        }

        /// <summary>
        /// Clear data shown on form
        /// </summary>
        protected void ClearDetailData()
        {
            txtApplicationName.Clear();
            rdAllowCompressNo.Checked = true;
            txtBitMap.Clear();
            txtHexBitMap.Clear();
        }

        /// <summary>
        /// Initiate objects which hold Application's detail data
        /// </summary>
        /// <param name="isEnable">boolean : true if enabled</param>
        /// <param name="isEdit">boolean : True if Edit mode, False in Add Mode</param>
        /// <param name="sStatus">string : current object's status</param>
        protected void InitDetailData(bool isEnable, bool isEdit, string sStatus)
        {
            gbDetailData.Enabled = isEnable;
            txtApplicationName.Enabled = !isEdit;
            //txtApplicationName.Enabled = !isEnable;
            //gbListApplication.Enabled = !isEdit;
            gbListApplication.Enabled = !isEnable;
            btnDelete.Enabled = !isEnable;
            btnCopy.Enabled = !isEnable;
            btnEdit.Enabled = isEdit || (sStatus != "new");
            btnAdd.Enabled = !isEdit || (sStatus != "edit");

            if (isEdit)
                btnEdit.Text = (isEnable) ? "Save" : "Edit";
            else
                btnAdd.Text = (isEnable) ? "Save" : "Add";
        }

        /// <summary>
        /// Deleted selected data from database
        /// </summary>
        protected void DeleteData()
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPApplicationDelete, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sApplicationName", SqlDbType.VarChar).Value = sGetAppsName();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", "Delete Application : " + sGetAppsName(), "");
                ClearDetailData();
                InitData();
            }
        }

        /// <summary>
        /// Update modified data into database
        /// </summary>
        protected void UpdateData()
        {
            if (isValid())
                if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                    if (isSuccessSave(true, null))
                    {
                        InitData();
                        lbApplicationName.SelectedValue = sGetAppsName();
                        InitDetailData(false, true, "");
                    }
        }

        /// <summary>
        /// Add new data to database
        /// </summary>
        protected void AddData()
        {
            if (isValid())
                if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                    if (isSuccessSave(false, null))
                    {
                        InitData();
                        lbApplicationName.SelectedValue = sGetAppsName();
                        InitDetailData(false, false, "");
                    }
        }

        /// <summary>
        /// Determine whether entered data by user valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool isValid()
        {
            string sErrMsg = "";
            if (string.IsNullOrEmpty(sGetAppsName()))
                sErrMsg = string.Format("{0}{1}{0}", "Application Name", CommonMessage.sFieldEmpty);
            else if (string.IsNullOrEmpty(sGetAllowCompress()))
                sErrMsg = CommonMessage.sAllowCompressEmpty;
            else if (string.IsNullOrEmpty(sGetBitMap()))
                sErrMsg = string.Format("{0}{1}{0}", "Bit Map", CommonMessage.sFieldEmpty);
            else if (string.IsNullOrEmpty(sGetHexBitMap()))
                sErrMsg = string.Format("{0}{1}{0}", "Hex Bit Map", CommonMessage.sFieldEmpty);
            else if (!CommonClass.isFormatValid(sGetBitMap(), "N"))
                sErrMsg = string.Format("{0}{1}", "Bit Map", CommonMessage.sFieldNotNumeric);
            else if (!CommonClass.isFormatValid(sGetHexBitMap(), "H"))
                sErrMsg = string.Format("{0}{1}", "Hex Bit Map", CommonMessage.sFieldNotNumeric);
            else return true;

            CommonClass.doShowErrorMessage(sErrMsg, null, ObjectType.NULL, ErrorType.ERROR);
            return false;
        }

        /// <summary>
        /// Try to save data to database and Determine whether data saved successfuly or not
        /// </summary>
        /// <param name="isEdit">boolean : true if Edit mode, false if Add mode</param>
        /// <returns>boolean : true success</returns>
        protected bool isSuccessSave(bool isEdit)
        {
            //bool isSuccess = true;

            //using (SqlCommand oSqlCmd = new SqlCommand(sGetSPAddEdit(isEdit), oSqlConn))
            //{
            //    string sLogDetail = "";

            //    oSqlCmd.CommandType = CommandType.StoredProcedure;
            //    oSqlCmd.Parameters.Add("@sApplicationName", SqlDbType.VarChar).Value = sGetAppsName();
            //    oSqlCmd.Parameters.Add("@bAllowCompress", SqlDbType.VarChar).Value = sGetAllowCompress();
            //    oSqlCmd.Parameters.Add("@sBitMap", SqlDbType.VarChar).Value = sGetBitMap();
            //    oSqlCmd.Parameters.Add("@sHexBitMap", SqlDbType.VarChar).Value = sGetHexBitMap();
            //    oSqlCmd.Parameters.Add("@sLogDetail", SqlDbType.VarChar, 200).Value = sLogDetail;
            //    oSqlCmd.Parameters["@sLogDetail"].Direction = ParameterDirection.Output;

            //    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            //    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
            //    {
            //        sLogDetail = oSqlCmd.Parameters["@sLogDetail"].Value.ToString();

            //        if (oRead.Read())
            //        {
            //            CommonClass.doShowErrorMessage(oRead[0].ToString(), null, ObjectType.NULL, ErrorType.ERROR);
            //            isSuccess = false;
            //        }
            //        oRead.Close();

            //        if (isSuccess)
            //            CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, "", ((isEdit) ? "Edit " : "Add ")
            //                                    + "Application : " + sGetAppsName(), sLogDetail);
            //    }
            //}
            return isSuccessSave(isEdit, null);
        }

        protected bool isSuccessSave(bool isEdit, string sAppName)
        {
            bool isSuccess = true;

            using (SqlCommand oSqlCmd = new SqlCommand(sGetSPAddEdit(isEdit), oSqlConn))
            {
                string sLogDetail = "";

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sApplicationName", SqlDbType.VarChar).Value = string.IsNullOrEmpty(sAppName) ? sGetAppsName() : sAppName;
                oSqlCmd.Parameters.Add("@bAllowCompress", SqlDbType.VarChar).Value = sGetAllowCompress().ToString();
                oSqlCmd.Parameters.Add("@sBitMap", SqlDbType.VarChar).Value = sGetBitMap();
                oSqlCmd.Parameters.Add("@sHexBitMap", SqlDbType.VarChar).Value = sGetHexBitMap();
                oSqlCmd.Parameters.Add("@bStandartISO8583", SqlDbType.VarChar).Value = sGetStandardIso();
                oSqlCmd.Parameters.Add("@sLogDetail", SqlDbType.VarChar, 200).Value = sLogDetail;
                oSqlCmd.Parameters["@sLogDetail"].Direction = ParameterDirection.Output;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    sLogDetail = oSqlCmd.Parameters["@sLogDetail"].Value.ToString();

                    if (oRead.Read())
                    {
                        CommonClass.doShowErrorMessage(oRead[0].ToString(), null, ObjectType.NULL, ErrorType.ERROR);
                        isSuccess = false;
                    }
                    oRead.Close();

                    if (isSuccess)
                        CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", ((isEdit) ? "Edit " : "Add ")
                                                + "Application : " + sGetAppsName(), sLogDetail);
                }
            }
            return isSuccess;
        }

        /// <summary>
        /// Determine which stored procedure will be used, based on isEdit parameter
        /// </summary>
        /// <param name="isEdit">boolean : true if Edit mode, false if Add mode</param>
        /// <returns>string : Stored Procedure's name</returns>
        public string sGetSPAddEdit(bool isEdit)
        {
            return (isEdit) ? CommonSP.sSPApplicationUpdate : CommonSP.sSPApplicationInsert;
        }

        /// <summary>
        /// Fill detail data for selected Application Name on ListBox
        /// </summary>
        protected void FillDetailData()
        {
            try
            {
                if (!string.IsNullOrEmpty(sGetSelectedAppsName()))
                {
                    DataRow[] drRow = oTableApps.Select(string.Format("ApplicationName LIKE '{0}'", sGetSelectedAppsName()));
                    if (drRow.Length > 0)
                    {
                        ClearDetailData();
                        txtApplicationName.Text = sGetSelectedValue(drRow[0], "ApplicationName");
                        SetAllowCompress(sGetSelectedValue(drRow[0], "AllowCompress"));
                        txtBitMap.Text = sGetSelectedValue(drRow[0], "BitMap");
                        txtHexBitMap.Text = sGetSelectedValue(drRow[0], "HexBitMap");
                        if (oTableApps.Columns.Contains("StandartISO8583"))
                            chkStandartISO8583.Checked = bGetSelectedvalue(drRow[0], "StandartISO8583");
                        else
                            chkStandartISO8583.Checked = false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Get value of selected Application Name
        /// </summary>
        /// <returns>string : Application Name</returns>
        protected string sGetSelectedAppsName()
        {
            //DataRowView drView = (DataRowView) lbApplicationName.SelectedValue;

            //return (drView == null) ? "" : drView["ApplicationName"].ToString();
            return (lbApplicationName.SelectedValue == null) ? "" : lbApplicationName.SelectedValue.ToString();
        }

        /// <summary>
        /// Get value from selected row for given column name
        /// </summary>
        /// <param name="drRow">DataRow : selected row to search value</param>
        /// <param name="sColumnName">string : Column Name which value desired</param>
        /// <returns>string : Selected value</returns>
        protected string sGetSelectedValue(DataRow drRow, string sColumnName)
        {
            string sValue = drRow[sColumnName].ToString();
            return (string.IsNullOrEmpty(sValue)) ? "" : sValue;
        }

        protected bool bGetSelectedvalue(DataRow drRow, string sColumnName)
        {
            return bool.Parse(drRow[sColumnName].ToString());
        }

        /// <summary>
        /// Assign Allow Compress value to objec
        /// </summary>
        /// <param name="sValue">string : Allow Compress value</param>
        protected void SetAllowCompress(string sValue)
        {
            rdAllowCompressYes.Checked = Convert.ToBoolean(sValue);
            rdAllowCompressNo.Checked = !rdAllowCompressYes.Checked;
        }
        #endregion
    }
}