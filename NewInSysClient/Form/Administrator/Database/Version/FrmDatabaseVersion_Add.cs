using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmDatabaseVersion_Add : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Form to add new Version to database
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection: Connection string to database</param>
        public FrmDatabaseVersion_Add(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        private void FrmDatabaseVersion_Add_Load(object sender, EventArgs e)
        {
            InitCmb();
            cmbDbId.Enabled = pnlNewDb.Enabled = false;
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        /// <summary>
        /// Initiate ComboBox, fill with list of Versions from database
        /// </summary>
        protected void InitCmb()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbId);
        }

        private void rdCopyDb_CheckedChanged(object sender, EventArgs e)
        {
            cmbDbId.Enabled = rdCopyDb.Checked;
            pnlNewDb.Enabled = rdNewDb.Checked;
        }

        /// <summary>
        /// Get DatabaseID Value
        /// </summary>
        /// <returns>string : Database value</returns>
        protected string sGetNewDbID()
        {
            return txtDbID.Text.Trim();
        }

        /// <summary>
        /// Get Version Name
        /// </summary>
        /// <returns>string : Version Name</returns>
        protected string sGetDbName()
        {
            return txtDbName.Text.Trim();
        }

        /// <summary>
        /// Get Existing Version ID which will be copied as New Version
        /// </summary>
        /// <returns></returns>
        protected string sGetExistingDbId()
        {
            return (cmbDbId.SelectedIndex >=0) ? cmbDbId.SelectedValue.ToString() : "";
        }

        /// <summary>
        /// Determine whether New Version is copied from other version or newly created
        /// </summary>
        /// <returns>boolean : true if not copied from other version</returns>
        protected bool isNewDbID()
        {
            return rdNewDb.Checked;
        }

        /// <summary>
        /// Determine whether New Version or Copy Version is selected
        /// </summary>
        /// <returns>boolean : true if either is selected</returns>
        protected bool isNewOrExistingDbIdChecked()
        {
            return (rdNewDb.Checked || rdCopyDb.Checked);
        }

        /// <summary>
        /// Determine whether New Init Status selected or not
        /// </summary>
        /// <returns>boolean : true if selected</returns>
        protected bool isNewInitChecked()
        {
            return (rdNewInitYes.Checked || rdNewInitNo.Checked);
        }

        /// <summary>
        /// Determine whether EMV Init selected or not
        /// </summary>
        /// <returns>boolean : true if selected</returns>
        protected bool isEmvInitChecked()
        {
            return (rdEMVInitYes.Checked || rdEMVInitNo.Checked);
        }

        /// <summary>
        /// Determine whether TLE Init selected or not
        /// </summary>
        /// <returns>boolean : true if selected</returns>
        protected bool isTLEInitChecked()
        {
            return (rdTLEInitYes.Checked || rdTLEInitNo.Checked);
        }

        /// <summary>
        /// Determine whether Loyalty Product selected or not
        /// </summary>
        /// <returns>boolean : true if selected</returns>
        protected bool isLoyaltyProductChecked()
        {
            return (rdLoyaltyProdYes.Checked || rdLoyaltyProdNo.Checked);
        }

        /// <summary>
        /// Determine whether Bank Code selected or not
        /// </summary>
        /// <returns>boolean : true if selected</returns>
        protected bool isBankCodeChecked()
        {
            return (rdBankCodeYes.Checked || rdBankCodeNo.Checked);
        }

        /// <summary>
        /// Determine whether Product Code selected or not
        /// </summary>
        /// <returns>boolean : true if selected</returns>
        protected bool isProductCodeChecked()
        {
            return (rdProductCodeYes.Checked || rdProductCodeNo.Checked);
        }

        /// <summary>
        /// Run save Function
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (isValid())
                    if (isSaveDatabase())
                        this.Close();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        /// <summary>
        /// Determine whether user input data valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool isValid()
        {
            if (string.IsNullOrEmpty(sGetNewDbID()))
                MessageBox.Show(CommonMessage.sDbIdNewEmpty);
            else if (string.IsNullOrEmpty(sGetDbName()))
                MessageBox.Show(CommonMessage.sDbNameEmpty);
            else if (!isNewOrExistingDbIdChecked())
                MessageBox.Show(string.Format("{0}{1}", "New or Existing Database", CommonMessage.sNotChecked));
            else if (cmbDbId.Enabled && string.IsNullOrEmpty(sGetExistingDbId()))
                MessageBox.Show(CommonMessage.sDbIdExistingEmpty);
            else if (cmbDbId.Enabled && !isDbIdValid())
                MessageBox.Show(CommonMessage.sDbIDNotValid);
            else if (pnlNewDb.Enabled && !isNewInitChecked())
                MessageBox.Show(string.Format("{0}{1}", "New Init", CommonMessage.sNotChecked));
            else if (pnlNewDb.Enabled && !isEmvInitChecked())
                MessageBox.Show(string.Format("{0}{1}", "EMV Init", CommonMessage.sNotChecked));
            else if (pnlTLEInit.Enabled && !isTLEInitChecked())
                MessageBox.Show(string.Format("{0}{1}", "TLE Init", CommonMessage.sNotChecked));
            else if (pnlLoyaltyProd.Enabled && !isLoyaltyProductChecked())
                MessageBox.Show(string.Format("{0}{1}", "Loyalty Product", CommonMessage.sNotChecked));
            else if (pnlBankCode.Enabled && !isBankCodeChecked())
                MessageBox.Show(string.Format("{0}{1}", "Bank Code", CommonMessage.sNotChecked));
            else if (pnlProductCode.Enabled && !isProductCodeChecked())
                MessageBox.Show(string.Format("{0}{1}", "Product Code", CommonMessage.sNotChecked));
            else return true;

            return false;
        }

        /// <summary>
        /// Determine whether DatabaseID entered unique or not
        /// </summary>
        /// <returns>boolean : true if unique</returns>
        protected bool isDbIdValid()
        {
            DataTable oTable = (DataTable)cmbDbId.DataSource;
            DataRow[] drRows = oTable.Select(string.Format("DatabaseID = '{0}'", sGetNewDbID()));
            return (drRows.Length == 0);
        }

        /// <summary>
        /// Determine whether data saved successfully or not
        /// </summary>
        /// <returns>boolean : true if successfullfull</returns>
        protected bool isSaveDatabase()
        {
            if (isNewOrExistingDbIdChecked())
                if (rdNewDb.Checked)
                    return isInsertNewDb();
                else
                    return isCopyExistingDb();
            return false;
        }

        /// <summary>
        /// Determine whether new Version data is added successfully or not
        /// </summary>
        /// <returns>boolean : true if successfullful</returns>
        protected bool isInsertNewDb()
        {
            string sErrMsg = "" ;
            string sLogDetail = "";

            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalDBInsert, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDbID", SqlDbType.VarChar).Value = sGetNewDbID();
                    oSqlCmd.Parameters.Add("@sDatabaseName", SqlDbType.VarChar).Value = sGetDbName();
                    oSqlCmd.Parameters.Add("@sNewInit", SqlDbType.VarChar).Value = sGetBitValue(rdNewInitYes.Checked);
                    oSqlCmd.Parameters.Add("@sEMVInit", SqlDbType.VarChar).Value = sGetBitValue(rdEMVInitYes.Checked);
                    oSqlCmd.Parameters.Add("@sTLEInit", SqlDbType.VarChar).Value = sGetBitValue(rdTLEInitYes.Checked);
                    oSqlCmd.Parameters.Add("@sLoyaltyProd", SqlDbType.VarChar).Value = sGetBitValue(rdLoyaltyProdYes.Checked);
                    oSqlCmd.Parameters.Add("@sGPRSInit", SqlDbType.VarChar).Value = sGetBitValue(rdGPRSInitYes.Checked);
                    oSqlCmd.Parameters.Add("@sBankCode", SqlDbType.VarChar).Value = sGetBitValue(rdBankCodeYes.Checked);
                    oSqlCmd.Parameters.Add("@sProductCode", SqlDbType.VarChar).Value = sGetBitValue(rdProductCodeYes.Checked);
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    oSqlCmd.ExecuteNonQuery();

                    sLogDetail = string.Format("{0} : {1} \n ", "Version ID", sGetNewDbID());
                    sLogDetail += string.Format("{0} : {1} \n ", "Version Name", sGetDbName());
                    sLogDetail += string.Format("{0} : {1} \n ", "New Init", sBitToString(rdNewInitYes.Checked));
                    sLogDetail += string.Format("{0} : {1} \n ", "EMV Init", sBitToString(rdEMVInitYes.Checked));
                    sLogDetail += string.Format("{0} : {1} \n ", "TLE Init", sBitToString(rdTLEInitYes.Checked));
                    sLogDetail += string.Format("{0} : {1} \n ", "Loyalty Product", sBitToString(rdLoyaltyProdYes.Checked));
                    sLogDetail += string.Format("{0} : {1} \n ", "GPRS Init", sBitToString(rdGPRSInitYes.Checked));
                    sLogDetail += string.Format("{0} : {1} \n ", "Bank Code", sBitToString(rdBankCodeYes.Checked));
                    sLogDetail += string.Format("{0} : {1} \n ", "Product Code", sBitToString(rdProductCodeYes.Checked));

                    CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sGetDbName(), "Add New Version", sLogDetail);
                }
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
            }

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Convert value from boolean to string
        /// </summary>
        /// <param name="isValue">boolean : bool value which will be converted</param>
        /// <returns>string : string value resulted from conversion</returns>
        protected string sGetBitValue(bool isValue)
        {
            return (isValue) ? "1" : "0";
        }

        /// <summary>
        ///  Convert value from boolean to string
        /// </summary>
        /// <param name="isValue">boolean : bool value which will be converted</param>
        /// <returns>string : string value resulted from conversion</returns>
        protected string sBitToString(bool isValue)
        {
            return (isValue) ? "TRUE" : "FALSE";
        }

        /// <summary>
        /// Determine whether Copying Version as new version succeed or failed
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        protected bool isCopyExistingDb()
        {
            string sErrMsg = "";
            string sLogDetail = "";

            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPVersionCopy, oSqlConn))
                {

                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDbSourceID", SqlDbType.VarChar).Value = sGetExistingDbId();
                    oSqlCmd.Parameters.Add("@sDbDestinationID", SqlDbType.VarChar).Value = sGetNewDbID();
                    oSqlCmd.Parameters.Add("@sDatabaseName", SqlDbType.VarChar).Value = sGetDbName();
                    oSqlCmd.Transaction = oSqlTrans;

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                    oSqlCmd.ExecuteNonQuery();
                    oSqlTrans.Commit();

                    string sExistingDbName = ((DataRowView)cmbDbId.SelectedItem)["DatabaseName"].ToString();

                    sLogDetail = string.Format("{0} : {1} \n ", "Version ID", sGetNewDbID());
                    sLogDetail += string.Format("{0} : {1} \n ", "Version Name", sGetDbName());
                    sLogDetail += "Version copied from : " + sExistingDbName;

                    CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sGetDbName(), "Copy Existing Version", sLogDetail);
                }
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }
            oSqlTrans.Dispose();
            return string.IsNullOrEmpty(sErrMsg);
        }
    }
}