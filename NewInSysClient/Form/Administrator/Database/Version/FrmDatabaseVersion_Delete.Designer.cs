namespace InSys
{
    partial class FrmDatabaseVersion_Delete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDatabaseVersion_Delete));
            this.gbDatabaseID = new System.Windows.Forms.GroupBox();
            this.cmbDatabaseID = new System.Windows.Forms.ComboBox();
            this.lblDatabaseID = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.gbDatabaseID.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDatabaseID
            // 
            this.gbDatabaseID.Controls.Add(this.cmbDatabaseID);
            this.gbDatabaseID.Controls.Add(this.lblDatabaseID);
            this.gbDatabaseID.Location = new System.Drawing.Point(9, 2);
            this.gbDatabaseID.Name = "gbDatabaseID";
            this.gbDatabaseID.Size = new System.Drawing.Size(345, 54);
            this.gbDatabaseID.TabIndex = 0;
            this.gbDatabaseID.TabStop = false;
            // 
            // cmbDatabaseID
            // 
            this.cmbDatabaseID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDatabaseID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDatabaseID.FormattingEnabled = true;
            this.cmbDatabaseID.Location = new System.Drawing.Point(114, 19);
            this.cmbDatabaseID.Name = "cmbDatabaseID";
            this.cmbDatabaseID.Size = new System.Drawing.Size(210, 21);
            this.cmbDatabaseID.TabIndex = 1;
            // 
            // lblDatabaseID
            // 
            this.lblDatabaseID.AutoSize = true;
            this.lblDatabaseID.Location = new System.Drawing.Point(14, 22);
            this.lblDatabaseID.Name = "lblDatabaseID";
            this.lblDatabaseID.Size = new System.Drawing.Size(84, 13);
            this.lblDatabaseID.TabIndex = 0;
            this.lblDatabaseID.Text = "Database Name";
            // 
            // gbButton
            // 
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnDelete);
            this.gbButton.Location = new System.Drawing.Point(9, 57);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(345, 52);
            this.gbButton.TabIndex = 1;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(224, 15);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnDelete.Location = new System.Drawing.Point(103, 15);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(115, 28);
            this.btnDelete.TabIndex = 0;
            this.btnDelete.Text = "Delete ";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // FrmDatabaseVersion_Delete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 115);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbDatabaseID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmDatabaseVersion_Delete";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Database Version - Delete";
            this.Load += new System.EventHandler(this.FrmDatabaseVersion_Delete_Load);
            this.gbDatabaseID.ResumeLayout(false);
            this.gbDatabaseID.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDatabaseID;
        private System.Windows.Forms.Label lblDatabaseID;
        private System.Windows.Forms.ComboBox cmbDatabaseID;
        private System.Windows.Forms.GroupBox gbButton;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnCancel;
    }
}