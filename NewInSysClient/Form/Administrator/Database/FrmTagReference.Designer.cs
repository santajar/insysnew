namespace InSys
{
    partial class FrmTagReference
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTagReference));
            this.gbDatabase = new System.Windows.Forms.GroupBox();
            this.cmbFormType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkCheckAll = new System.Windows.Forms.CheckBox();
            this.cmbDbId = new System.Windows.Forms.ComboBox();
            this.lblDbID = new System.Windows.Forms.Label();
            this.gbTag = new System.Windows.Forms.GroupBox();
            this.dgViewTagItem = new System.Windows.Forms.DataGridView();
            this.colEdit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colDelete = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gbTagDetail = new System.Windows.Forms.GroupBox();
            this.pnlDynamicCmb = new System.Windows.Forms.Panel();
            this.cmbRefValueCombo = new System.Windows.Forms.ComboBox();
            this.cmbRefDisplayCombo = new System.Windows.Forms.ComboBox();
            this.cmbRefFormID = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlMasking = new System.Windows.Forms.Panel();
            this.rdMaskingNo = new System.Windows.Forms.RadioButton();
            this.rdMaskingYes = new System.Windows.Forms.RadioButton();
            this.lblMasking = new System.Windows.Forms.Label();
            this.cmbForm = new System.Windows.Forms.ComboBox();
            this.lblItemSequence = new System.Windows.Forms.Label();
            this.txtItemSequence = new System.Windows.Forms.TextBox();
            this.txtMaxLength = new System.Windows.Forms.TextBox();
            this.txtMinLength = new System.Windows.Forms.TextBox();
            this.lblMaxLength = new System.Windows.Forms.Label();
            this.lblMinLength = new System.Windows.Forms.Label();
            this.txtValidationMsg = new System.Windows.Forms.TextBox();
            this.txtMaxValue = new System.Windows.Forms.TextBox();
            this.txtMinValue = new System.Windows.Forms.TextBox();
            this.pnlCmbValue = new System.Windows.Forms.Panel();
            this.dgCmbValue = new System.Windows.Forms.DataGridView();
            this.bntDeleteCmbValue = new System.Windows.Forms.Button();
            this.btnAddComboBoxValue = new System.Windows.Forms.Button();
            this.txtCmbRealValue = new System.Windows.Forms.TextBox();
            this.txtCmbDisplayValue = new System.Windows.Forms.TextBox();
            this.lblRealValue = new System.Windows.Forms.Label();
            this.lblDisplayValue = new System.Windows.Forms.Label();
            this.txtDefaultValue = new System.Windows.Forms.TextBox();
            this.lblComboBoxValue = new System.Windows.Forms.Label();
            this.pnlValueType = new System.Windows.Forms.Panel();
            this.rdValueTypeIpAddress = new System.Windows.Forms.RadioButton();
            this.rdValueTypeNum = new System.Windows.Forms.RadioButton();
            this.rdValueTypeString = new System.Windows.Forms.RadioButton();
            this.rdValueTypeTime = new System.Windows.Forms.RadioButton();
            this.rdValueTypeAlfaNumSpace = new System.Windows.Forms.RadioButton();
            this.rdValueTypeAlfaNum = new System.Windows.Forms.RadioButton();
            this.pnlUpperCase = new System.Windows.Forms.Panel();
            this.rdUpperCaseNo = new System.Windows.Forms.RadioButton();
            this.rdUpperCaseYes = new System.Windows.Forms.RadioButton();
            this.pnlAllowNull = new System.Windows.Forms.Panel();
            this.rdAllowNullNo = new System.Windows.Forms.RadioButton();
            this.rdAllowNullYes = new System.Windows.Forms.RadioButton();
            this.pnlObject = new System.Windows.Forms.Panel();
            this.rdObjCmbDynamic = new System.Windows.Forms.RadioButton();
            this.rbObjDate = new System.Windows.Forms.RadioButton();
            this.rdObjCmb = new System.Windows.Forms.RadioButton();
            this.rdObjTxt = new System.Windows.Forms.RadioButton();
            this.rdObjChk = new System.Windows.Forms.RadioButton();
            this.rdObjRd = new System.Windows.Forms.RadioButton();
            this.lblValidationMsg = new System.Windows.Forms.Label();
            this.lblMaxValue = new System.Windows.Forms.Label();
            this.lblMinValue = new System.Windows.Forms.Label();
            this.lblValueType = new System.Windows.Forms.Label();
            this.lblUpperCase = new System.Windows.Forms.Label();
            this.lblAllowNull = new System.Windows.Forms.Label();
            this.lblDefaultValue = new System.Windows.Forms.Label();
            this.lblObjectID = new System.Windows.Forms.Label();
            this.lblFormID = new System.Windows.Forms.Label();
            this.txtTagName = new System.Windows.Forms.TextBox();
            this.lblTagName = new System.Windows.Forms.Label();
            this.txtTagID = new System.Windows.Forms.TextBox();
            this.lblTagID = new System.Windows.Forms.Label();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddSave = new System.Windows.Forms.Button();
            this.gbDatabase.SuspendLayout();
            this.gbTag.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgViewTagItem)).BeginInit();
            this.gbTagDetail.SuspendLayout();
            this.pnlDynamicCmb.SuspendLayout();
            this.pnlMasking.SuspendLayout();
            this.pnlCmbValue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCmbValue)).BeginInit();
            this.pnlValueType.SuspendLayout();
            this.pnlUpperCase.SuspendLayout();
            this.pnlAllowNull.SuspendLayout();
            this.pnlObject.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDatabase
            // 
            this.gbDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDatabase.Controls.Add(this.cmbFormType);
            this.gbDatabase.Controls.Add(this.label1);
            this.gbDatabase.Controls.Add(this.chkCheckAll);
            this.gbDatabase.Controls.Add(this.cmbDbId);
            this.gbDatabase.Controls.Add(this.lblDbID);
            this.gbDatabase.Location = new System.Drawing.Point(12, -1);
            this.gbDatabase.Name = "gbDatabase";
            this.gbDatabase.Size = new System.Drawing.Size(1132, 64);
            this.gbDatabase.TabIndex = 0;
            this.gbDatabase.TabStop = false;
            // 
            // cmbFormType
            // 
            this.cmbFormType.FormattingEnabled = true;
            this.cmbFormType.Location = new System.Drawing.Point(108, 40);
            this.cmbFormType.Name = "cmbFormType";
            this.cmbFormType.Size = new System.Drawing.Size(177, 21);
            this.cmbFormType.TabIndex = 32;
            this.cmbFormType.SelectedIndexChanged += new System.EventHandler(this.cmbFormType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Form Type";
            // 
            // chkCheckAll
            // 
            this.chkCheckAll.AutoSize = true;
            this.chkCheckAll.Location = new System.Drawing.Point(324, 15);
            this.chkCheckAll.Name = "chkCheckAll";
            this.chkCheckAll.Size = new System.Drawing.Size(71, 17);
            this.chkCheckAll.TabIndex = 7;
            this.chkCheckAll.Text = "Check All";
            this.chkCheckAll.UseVisualStyleBackColor = true;
            this.chkCheckAll.CheckedChanged += new System.EventHandler(this.chkCheckAll_CheckedChanged);
            // 
            // cmbDbId
            // 
            this.cmbDbId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDbId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDbId.FormattingEnabled = true;
            this.cmbDbId.Location = new System.Drawing.Point(108, 13);
            this.cmbDbId.Name = "cmbDbId";
            this.cmbDbId.Size = new System.Drawing.Size(210, 21);
            this.cmbDbId.TabIndex = 6;
            this.cmbDbId.SelectedIndexChanged += new System.EventHandler(this.cmbDbId_SelectedIndexChanged);
            // 
            // lblDbID
            // 
            this.lblDbID.AutoSize = true;
            this.lblDbID.Location = new System.Drawing.Point(8, 17);
            this.lblDbID.Name = "lblDbID";
            this.lblDbID.Size = new System.Drawing.Size(84, 13);
            this.lblDbID.TabIndex = 5;
            this.lblDbID.Text = "Database Name";
            // 
            // gbTag
            // 
            this.gbTag.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTag.Controls.Add(this.dgViewTagItem);
            this.gbTag.Location = new System.Drawing.Point(12, 63);
            this.gbTag.Name = "gbTag";
            this.gbTag.Size = new System.Drawing.Size(1132, 341);
            this.gbTag.TabIndex = 1;
            this.gbTag.TabStop = false;
            // 
            // dgViewTagItem
            // 
            this.dgViewTagItem.AllowUserToAddRows = false;
            this.dgViewTagItem.AllowUserToDeleteRows = false;
            this.dgViewTagItem.AllowUserToResizeRows = false;
            this.dgViewTagItem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgViewTagItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgViewTagItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgViewTagItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colEdit,
            this.colDelete});
            this.dgViewTagItem.Location = new System.Drawing.Point(6, 11);
            this.dgViewTagItem.Name = "dgViewTagItem";
            this.dgViewTagItem.ReadOnly = true;
            this.dgViewTagItem.RowHeadersVisible = false;
            this.dgViewTagItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgViewTagItem.Size = new System.Drawing.Size(1119, 324);
            this.dgViewTagItem.TabIndex = 0;
            this.dgViewTagItem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgViewTagItem_CellContentClick);
            this.dgViewTagItem.SelectionChanged += new System.EventHandler(this.dgViewTagItem_SelectionChanged);
            // 
            // colEdit
            // 
            this.colEdit.HeaderText = "Edit";
            this.colEdit.Name = "colEdit";
            this.colEdit.ReadOnly = true;
            this.colEdit.Text = "Edit";
            this.colEdit.UseColumnTextForButtonValue = true;
            this.colEdit.Width = 60;
            // 
            // colDelete
            // 
            this.colDelete.HeaderText = "Delete";
            this.colDelete.Name = "colDelete";
            this.colDelete.ReadOnly = true;
            this.colDelete.Width = 60;
            // 
            // gbTagDetail
            // 
            this.gbTagDetail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTagDetail.Controls.Add(this.pnlDynamicCmb);
            this.gbTagDetail.Controls.Add(this.label2);
            this.gbTagDetail.Controls.Add(this.pnlMasking);
            this.gbTagDetail.Controls.Add(this.lblMasking);
            this.gbTagDetail.Controls.Add(this.cmbForm);
            this.gbTagDetail.Controls.Add(this.lblItemSequence);
            this.gbTagDetail.Controls.Add(this.txtItemSequence);
            this.gbTagDetail.Controls.Add(this.txtMaxLength);
            this.gbTagDetail.Controls.Add(this.txtMinLength);
            this.gbTagDetail.Controls.Add(this.lblMaxLength);
            this.gbTagDetail.Controls.Add(this.lblMinLength);
            this.gbTagDetail.Controls.Add(this.txtValidationMsg);
            this.gbTagDetail.Controls.Add(this.txtMaxValue);
            this.gbTagDetail.Controls.Add(this.txtMinValue);
            this.gbTagDetail.Controls.Add(this.pnlCmbValue);
            this.gbTagDetail.Controls.Add(this.txtDefaultValue);
            this.gbTagDetail.Controls.Add(this.lblComboBoxValue);
            this.gbTagDetail.Controls.Add(this.pnlValueType);
            this.gbTagDetail.Controls.Add(this.pnlUpperCase);
            this.gbTagDetail.Controls.Add(this.pnlAllowNull);
            this.gbTagDetail.Controls.Add(this.pnlObject);
            this.gbTagDetail.Controls.Add(this.lblValidationMsg);
            this.gbTagDetail.Controls.Add(this.lblMaxValue);
            this.gbTagDetail.Controls.Add(this.lblMinValue);
            this.gbTagDetail.Controls.Add(this.lblValueType);
            this.gbTagDetail.Controls.Add(this.lblUpperCase);
            this.gbTagDetail.Controls.Add(this.lblAllowNull);
            this.gbTagDetail.Controls.Add(this.lblDefaultValue);
            this.gbTagDetail.Controls.Add(this.lblObjectID);
            this.gbTagDetail.Controls.Add(this.lblFormID);
            this.gbTagDetail.Controls.Add(this.txtTagName);
            this.gbTagDetail.Controls.Add(this.lblTagName);
            this.gbTagDetail.Controls.Add(this.txtTagID);
            this.gbTagDetail.Controls.Add(this.lblTagID);
            this.gbTagDetail.Location = new System.Drawing.Point(12, 404);
            this.gbTagDetail.Name = "gbTagDetail";
            this.gbTagDetail.Size = new System.Drawing.Size(1132, 242);
            this.gbTagDetail.TabIndex = 2;
            this.gbTagDetail.TabStop = false;
            // 
            // pnlDynamicCmb
            // 
            this.pnlDynamicCmb.Controls.Add(this.cmbRefValueCombo);
            this.pnlDynamicCmb.Controls.Add(this.cmbRefDisplayCombo);
            this.pnlDynamicCmb.Controls.Add(this.cmbRefFormID);
            this.pnlDynamicCmb.Controls.Add(this.label5);
            this.pnlDynamicCmb.Controls.Add(this.label3);
            this.pnlDynamicCmb.Controls.Add(this.label4);
            this.pnlDynamicCmb.Enabled = false;
            this.pnlDynamicCmb.Location = new System.Drawing.Point(396, 167);
            this.pnlDynamicCmb.Name = "pnlDynamicCmb";
            this.pnlDynamicCmb.Size = new System.Drawing.Size(445, 63);
            this.pnlDynamicCmb.TabIndex = 34;
            // 
            // cmbRefValueCombo
            // 
            this.cmbRefValueCombo.FormattingEnabled = true;
            this.cmbRefValueCombo.Location = new System.Drawing.Point(304, 31);
            this.cmbRefValueCombo.Name = "cmbRefValueCombo";
            this.cmbRefValueCombo.Size = new System.Drawing.Size(121, 21);
            this.cmbRefValueCombo.TabIndex = 6;
            // 
            // cmbRefDisplayCombo
            // 
            this.cmbRefDisplayCombo.FormattingEnabled = true;
            this.cmbRefDisplayCombo.Location = new System.Drawing.Point(156, 31);
            this.cmbRefDisplayCombo.Name = "cmbRefDisplayCombo";
            this.cmbRefDisplayCombo.Size = new System.Drawing.Size(121, 21);
            this.cmbRefDisplayCombo.TabIndex = 5;
            // 
            // cmbRefFormID
            // 
            this.cmbRefFormID.FormattingEnabled = true;
            this.cmbRefFormID.Location = new System.Drawing.Point(15, 31);
            this.cmbRefFormID.Name = "cmbRefFormID";
            this.cmbRefFormID.Size = new System.Drawing.Size(121, 21);
            this.cmbRefFormID.TabIndex = 4;
            this.cmbRefFormID.SelectedIndexChanged += new System.EventHandler(this.cmbRefFormID_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Reference Form";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(300, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Reference Value";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(152, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Reference Display";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(330, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 51);
            this.label2.TabIndex = 33;
            this.label2.Text = "Dynamic ComboBox Value";
            // 
            // pnlMasking
            // 
            this.pnlMasking.Controls.Add(this.rdMaskingNo);
            this.pnlMasking.Controls.Add(this.rdMaskingYes);
            this.pnlMasking.Location = new System.Drawing.Point(962, 62);
            this.pnlMasking.Name = "pnlMasking";
            this.pnlMasking.Size = new System.Drawing.Size(123, 22);
            this.pnlMasking.TabIndex = 32;
            // 
            // rdMaskingNo
            // 
            this.rdMaskingNo.AutoSize = true;
            this.rdMaskingNo.Location = new System.Drawing.Point(52, 3);
            this.rdMaskingNo.Name = "rdMaskingNo";
            this.rdMaskingNo.Size = new System.Drawing.Size(39, 17);
            this.rdMaskingNo.TabIndex = 1;
            this.rdMaskingNo.TabStop = true;
            this.rdMaskingNo.Tag = "False";
            this.rdMaskingNo.Text = "No";
            this.rdMaskingNo.UseVisualStyleBackColor = true;
            // 
            // rdMaskingYes
            // 
            this.rdMaskingYes.AutoSize = true;
            this.rdMaskingYes.Location = new System.Drawing.Point(3, 3);
            this.rdMaskingYes.Name = "rdMaskingYes";
            this.rdMaskingYes.Size = new System.Drawing.Size(43, 17);
            this.rdMaskingYes.TabIndex = 0;
            this.rdMaskingYes.TabStop = true;
            this.rdMaskingYes.Tag = "True";
            this.rdMaskingYes.Text = "Yes";
            this.rdMaskingYes.UseVisualStyleBackColor = true;
            // 
            // lblMasking
            // 
            this.lblMasking.AutoSize = true;
            this.lblMasking.Location = new System.Drawing.Point(855, 67);
            this.lblMasking.Name = "lblMasking";
            this.lblMasking.Size = new System.Drawing.Size(47, 13);
            this.lblMasking.TabIndex = 31;
            this.lblMasking.Text = "Masking";
            // 
            // cmbForm
            // 
            this.cmbForm.FormattingEnabled = true;
            this.cmbForm.Location = new System.Drawing.Point(119, 97);
            this.cmbForm.Name = "cmbForm";
            this.cmbForm.Size = new System.Drawing.Size(121, 21);
            this.cmbForm.TabIndex = 30;
            // 
            // lblItemSequence
            // 
            this.lblItemSequence.AutoSize = true;
            this.lblItemSequence.Location = new System.Drawing.Point(21, 210);
            this.lblItemSequence.Name = "lblItemSequence";
            this.lblItemSequence.Size = new System.Drawing.Size(79, 13);
            this.lblItemSequence.TabIndex = 10;
            this.lblItemSequence.Text = "Item Sequence";
            // 
            // txtItemSequence
            // 
            this.txtItemSequence.Location = new System.Drawing.Point(119, 207);
            this.txtItemSequence.MaxLength = 3;
            this.txtItemSequence.Name = "txtItemSequence";
            this.txtItemSequence.Size = new System.Drawing.Size(100, 20);
            this.txtItemSequence.TabIndex = 11;
            // 
            // txtMaxLength
            // 
            this.txtMaxLength.Location = new System.Drawing.Point(962, 112);
            this.txtMaxLength.MaxLength = 4;
            this.txtMaxLength.Name = "txtMaxLength";
            this.txtMaxLength.Size = new System.Drawing.Size(120, 20);
            this.txtMaxLength.TabIndex = 23;
            // 
            // txtMinLength
            // 
            this.txtMinLength.Location = new System.Drawing.Point(962, 86);
            this.txtMinLength.MaxLength = 4;
            this.txtMinLength.Name = "txtMinLength";
            this.txtMinLength.Size = new System.Drawing.Size(120, 20);
            this.txtMinLength.TabIndex = 21;
            // 
            // lblMaxLength
            // 
            this.lblMaxLength.AutoSize = true;
            this.lblMaxLength.Location = new System.Drawing.Point(855, 113);
            this.lblMaxLength.Name = "lblMaxLength";
            this.lblMaxLength.Size = new System.Drawing.Size(87, 13);
            this.lblMaxLength.TabIndex = 22;
            this.lblMaxLength.Text = "Maximum Length";
            // 
            // lblMinLength
            // 
            this.lblMinLength.AutoSize = true;
            this.lblMinLength.Location = new System.Drawing.Point(855, 89);
            this.lblMinLength.Name = "lblMinLength";
            this.lblMinLength.Size = new System.Drawing.Size(84, 13);
            this.lblMinLength.TabIndex = 20;
            this.lblMinLength.Text = "Minimum Length";
            // 
            // txtValidationMsg
            // 
            this.txtValidationMsg.Location = new System.Drawing.Point(962, 191);
            this.txtValidationMsg.MaxLength = 50;
            this.txtValidationMsg.Multiline = true;
            this.txtValidationMsg.Name = "txtValidationMsg";
            this.txtValidationMsg.Size = new System.Drawing.Size(155, 36);
            this.txtValidationMsg.TabIndex = 29;
            // 
            // txtMaxValue
            // 
            this.txtMaxValue.Location = new System.Drawing.Point(962, 164);
            this.txtMaxValue.MaxLength = 2560;
            this.txtMaxValue.Name = "txtMaxValue";
            this.txtMaxValue.Size = new System.Drawing.Size(120, 20);
            this.txtMaxValue.TabIndex = 27;
            // 
            // txtMinValue
            // 
            this.txtMinValue.Location = new System.Drawing.Point(962, 138);
            this.txtMinValue.MaxLength = 2560;
            this.txtMinValue.Name = "txtMinValue";
            this.txtMinValue.Size = new System.Drawing.Size(120, 20);
            this.txtMinValue.TabIndex = 25;
            // 
            // pnlCmbValue
            // 
            this.pnlCmbValue.Controls.Add(this.dgCmbValue);
            this.pnlCmbValue.Controls.Add(this.bntDeleteCmbValue);
            this.pnlCmbValue.Controls.Add(this.btnAddComboBoxValue);
            this.pnlCmbValue.Controls.Add(this.txtCmbRealValue);
            this.pnlCmbValue.Controls.Add(this.txtCmbDisplayValue);
            this.pnlCmbValue.Controls.Add(this.lblRealValue);
            this.pnlCmbValue.Controls.Add(this.lblDisplayValue);
            this.pnlCmbValue.Enabled = false;
            this.pnlCmbValue.Location = new System.Drawing.Point(396, 62);
            this.pnlCmbValue.Name = "pnlCmbValue";
            this.pnlCmbValue.Size = new System.Drawing.Size(359, 105);
            this.pnlCmbValue.TabIndex = 15;
            // 
            // dgCmbValue
            // 
            this.dgCmbValue.AllowUserToAddRows = false;
            this.dgCmbValue.AllowUserToDeleteRows = false;
            this.dgCmbValue.AllowUserToResizeRows = false;
            this.dgCmbValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCmbValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgCmbValue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCmbValue.Location = new System.Drawing.Point(3, 57);
            this.dgCmbValue.Name = "dgCmbValue";
            this.dgCmbValue.ReadOnly = true;
            this.dgCmbValue.RowHeadersVisible = false;
            this.dgCmbValue.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCmbValue.Size = new System.Drawing.Size(353, 45);
            this.dgCmbValue.TabIndex = 6;
            // 
            // bntDeleteCmbValue
            // 
            this.bntDeleteCmbValue.Image = ((System.Drawing.Image)(resources.GetObject("bntDeleteCmbValue.Image")));
            this.bntDeleteCmbValue.Location = new System.Drawing.Point(225, 28);
            this.bntDeleteCmbValue.Name = "bntDeleteCmbValue";
            this.bntDeleteCmbValue.Size = new System.Drawing.Size(55, 25);
            this.bntDeleteCmbValue.TabIndex = 5;
            this.bntDeleteCmbValue.Click += new System.EventHandler(this.bntDeleteCmbValue_Click);
            // 
            // btnAddComboBoxValue
            // 
            this.btnAddComboBoxValue.Image = ((System.Drawing.Image)(resources.GetObject("btnAddComboBoxValue.Image")));
            this.btnAddComboBoxValue.Location = new System.Drawing.Point(225, 2);
            this.btnAddComboBoxValue.Name = "btnAddComboBoxValue";
            this.btnAddComboBoxValue.Size = new System.Drawing.Size(55, 25);
            this.btnAddComboBoxValue.TabIndex = 4;
            this.btnAddComboBoxValue.Click += new System.EventHandler(this.btnAddComboBoxValue_Click);
            // 
            // txtCmbRealValue
            // 
            this.txtCmbRealValue.Location = new System.Drawing.Point(99, 32);
            this.txtCmbRealValue.MaxLength = 20;
            this.txtCmbRealValue.Name = "txtCmbRealValue";
            this.txtCmbRealValue.Size = new System.Drawing.Size(120, 20);
            this.txtCmbRealValue.TabIndex = 3;
            // 
            // txtCmbDisplayValue
            // 
            this.txtCmbDisplayValue.Location = new System.Drawing.Point(99, 6);
            this.txtCmbDisplayValue.MaxLength = 40;
            this.txtCmbDisplayValue.Name = "txtCmbDisplayValue";
            this.txtCmbDisplayValue.Size = new System.Drawing.Size(120, 20);
            this.txtCmbDisplayValue.TabIndex = 1;
            // 
            // lblRealValue
            // 
            this.lblRealValue.AutoSize = true;
            this.lblRealValue.Location = new System.Drawing.Point(12, 35);
            this.lblRealValue.Name = "lblRealValue";
            this.lblRealValue.Size = new System.Drawing.Size(59, 13);
            this.lblRealValue.TabIndex = 2;
            this.lblRealValue.Text = "Real Value";
            // 
            // lblDisplayValue
            // 
            this.lblDisplayValue.AutoSize = true;
            this.lblDisplayValue.Location = new System.Drawing.Point(12, 9);
            this.lblDisplayValue.Name = "lblDisplayValue";
            this.lblDisplayValue.Size = new System.Drawing.Size(71, 13);
            this.lblDisplayValue.TabIndex = 0;
            this.lblDisplayValue.Text = "Display Value";
            // 
            // txtDefaultValue
            // 
            this.txtDefaultValue.Location = new System.Drawing.Point(119, 71);
            this.txtDefaultValue.MaxLength = 50;
            this.txtDefaultValue.Name = "txtDefaultValue";
            this.txtDefaultValue.Size = new System.Drawing.Size(200, 20);
            this.txtDefaultValue.TabIndex = 5;
            // 
            // lblComboBoxValue
            // 
            this.lblComboBoxValue.Location = new System.Drawing.Point(327, 63);
            this.lblComboBoxValue.Name = "lblComboBoxValue";
            this.lblComboBoxValue.Size = new System.Drawing.Size(65, 35);
            this.lblComboBoxValue.TabIndex = 14;
            this.lblComboBoxValue.Text = "ComboBox Value";
            // 
            // pnlValueType
            // 
            this.pnlValueType.Controls.Add(this.rdValueTypeIpAddress);
            this.pnlValueType.Controls.Add(this.rdValueTypeNum);
            this.pnlValueType.Controls.Add(this.rdValueTypeString);
            this.pnlValueType.Controls.Add(this.rdValueTypeTime);
            this.pnlValueType.Controls.Add(this.rdValueTypeAlfaNumSpace);
            this.pnlValueType.Controls.Add(this.rdValueTypeAlfaNum);
            this.pnlValueType.Location = new System.Drawing.Point(119, 124);
            this.pnlValueType.Name = "pnlValueType";
            this.pnlValueType.Size = new System.Drawing.Size(200, 75);
            this.pnlValueType.TabIndex = 9;
            // 
            // rdValueTypeIpAddress
            // 
            this.rdValueTypeIpAddress.AutoSize = true;
            this.rdValueTypeIpAddress.Location = new System.Drawing.Point(3, 49);
            this.rdValueTypeIpAddress.Name = "rdValueTypeIpAddress";
            this.rdValueTypeIpAddress.Size = new System.Drawing.Size(76, 17);
            this.rdValueTypeIpAddress.TabIndex = 4;
            this.rdValueTypeIpAddress.TabStop = true;
            this.rdValueTypeIpAddress.Tag = "N1";
            this.rdValueTypeIpAddress.Text = "IP Address";
            this.rdValueTypeIpAddress.UseVisualStyleBackColor = true;
            // 
            // rdValueTypeNum
            // 
            this.rdValueTypeNum.AutoSize = true;
            this.rdValueTypeNum.Location = new System.Drawing.Point(128, 46);
            this.rdValueTypeNum.Name = "rdValueTypeNum";
            this.rdValueTypeNum.Size = new System.Drawing.Size(64, 17);
            this.rdValueTypeNum.TabIndex = 5;
            this.rdValueTypeNum.TabStop = true;
            this.rdValueTypeNum.Tag = "N";
            this.rdValueTypeNum.Text = "Numeric";
            this.rdValueTypeNum.UseVisualStyleBackColor = true;
            // 
            // rdValueTypeString
            // 
            this.rdValueTypeString.AutoSize = true;
            this.rdValueTypeString.Location = new System.Drawing.Point(128, 3);
            this.rdValueTypeString.Name = "rdValueTypeString";
            this.rdValueTypeString.Size = new System.Drawing.Size(52, 17);
            this.rdValueTypeString.TabIndex = 1;
            this.rdValueTypeString.TabStop = true;
            this.rdValueTypeString.Tag = "S";
            this.rdValueTypeString.Text = "String";
            this.rdValueTypeString.UseVisualStyleBackColor = true;
            // 
            // rdValueTypeTime
            // 
            this.rdValueTypeTime.AutoSize = true;
            this.rdValueTypeTime.Location = new System.Drawing.Point(128, 26);
            this.rdValueTypeTime.Name = "rdValueTypeTime";
            this.rdValueTypeTime.Size = new System.Drawing.Size(48, 17);
            this.rdValueTypeTime.TabIndex = 3;
            this.rdValueTypeTime.TabStop = true;
            this.rdValueTypeTime.Tag = "T";
            this.rdValueTypeTime.Text = "Time";
            this.rdValueTypeTime.UseVisualStyleBackColor = true;
            // 
            // rdValueTypeAlfaNumSpace
            // 
            this.rdValueTypeAlfaNumSpace.AutoSize = true;
            this.rdValueTypeAlfaNumSpace.Location = new System.Drawing.Point(3, 26);
            this.rdValueTypeAlfaNumSpace.Name = "rdValueTypeAlfaNumSpace";
            this.rdValueTypeAlfaNumSpace.Size = new System.Drawing.Size(119, 17);
            this.rdValueTypeAlfaNumSpace.TabIndex = 2;
            this.rdValueTypeAlfaNumSpace.TabStop = true;
            this.rdValueTypeAlfaNumSpace.Tag = "A1";
            this.rdValueTypeAlfaNumSpace.Text = "Alfa Numeric Space";
            this.rdValueTypeAlfaNumSpace.UseVisualStyleBackColor = true;
            // 
            // rdValueTypeAlfaNum
            // 
            this.rdValueTypeAlfaNum.AutoSize = true;
            this.rdValueTypeAlfaNum.Location = new System.Drawing.Point(3, 3);
            this.rdValueTypeAlfaNum.Name = "rdValueTypeAlfaNum";
            this.rdValueTypeAlfaNum.Size = new System.Drawing.Size(85, 17);
            this.rdValueTypeAlfaNum.TabIndex = 0;
            this.rdValueTypeAlfaNum.TabStop = true;
            this.rdValueTypeAlfaNum.Tag = "A";
            this.rdValueTypeAlfaNum.Text = "Alfa Numeric";
            this.rdValueTypeAlfaNum.UseVisualStyleBackColor = true;
            // 
            // pnlUpperCase
            // 
            this.pnlUpperCase.Controls.Add(this.rdUpperCaseNo);
            this.pnlUpperCase.Controls.Add(this.rdUpperCaseYes);
            this.pnlUpperCase.Location = new System.Drawing.Point(962, 38);
            this.pnlUpperCase.Name = "pnlUpperCase";
            this.pnlUpperCase.Size = new System.Drawing.Size(123, 22);
            this.pnlUpperCase.TabIndex = 19;
            // 
            // rdUpperCaseNo
            // 
            this.rdUpperCaseNo.AutoSize = true;
            this.rdUpperCaseNo.Location = new System.Drawing.Point(52, 3);
            this.rdUpperCaseNo.Name = "rdUpperCaseNo";
            this.rdUpperCaseNo.Size = new System.Drawing.Size(39, 17);
            this.rdUpperCaseNo.TabIndex = 1;
            this.rdUpperCaseNo.TabStop = true;
            this.rdUpperCaseNo.Tag = "False";
            this.rdUpperCaseNo.Text = "No";
            this.rdUpperCaseNo.UseVisualStyleBackColor = true;
            // 
            // rdUpperCaseYes
            // 
            this.rdUpperCaseYes.AutoSize = true;
            this.rdUpperCaseYes.Location = new System.Drawing.Point(3, 3);
            this.rdUpperCaseYes.Name = "rdUpperCaseYes";
            this.rdUpperCaseYes.Size = new System.Drawing.Size(43, 17);
            this.rdUpperCaseYes.TabIndex = 0;
            this.rdUpperCaseYes.TabStop = true;
            this.rdUpperCaseYes.Tag = "True";
            this.rdUpperCaseYes.Text = "Yes";
            this.rdUpperCaseYes.UseVisualStyleBackColor = true;
            // 
            // pnlAllowNull
            // 
            this.pnlAllowNull.Controls.Add(this.rdAllowNullNo);
            this.pnlAllowNull.Controls.Add(this.rdAllowNullYes);
            this.pnlAllowNull.Location = new System.Drawing.Point(962, 14);
            this.pnlAllowNull.Name = "pnlAllowNull";
            this.pnlAllowNull.Size = new System.Drawing.Size(123, 23);
            this.pnlAllowNull.TabIndex = 17;
            // 
            // rdAllowNullNo
            // 
            this.rdAllowNullNo.AutoSize = true;
            this.rdAllowNullNo.Location = new System.Drawing.Point(52, 4);
            this.rdAllowNullNo.Name = "rdAllowNullNo";
            this.rdAllowNullNo.Size = new System.Drawing.Size(39, 17);
            this.rdAllowNullNo.TabIndex = 1;
            this.rdAllowNullNo.TabStop = true;
            this.rdAllowNullNo.Tag = "False";
            this.rdAllowNullNo.Text = "No";
            this.rdAllowNullNo.UseVisualStyleBackColor = true;
            // 
            // rdAllowNullYes
            // 
            this.rdAllowNullYes.AutoSize = true;
            this.rdAllowNullYes.Location = new System.Drawing.Point(3, 4);
            this.rdAllowNullYes.Name = "rdAllowNullYes";
            this.rdAllowNullYes.Size = new System.Drawing.Size(43, 17);
            this.rdAllowNullYes.TabIndex = 0;
            this.rdAllowNullYes.TabStop = true;
            this.rdAllowNullYes.Tag = "True";
            this.rdAllowNullYes.Text = "Yes";
            this.rdAllowNullYes.UseVisualStyleBackColor = true;
            // 
            // pnlObject
            // 
            this.pnlObject.Controls.Add(this.rdObjCmbDynamic);
            this.pnlObject.Controls.Add(this.rbObjDate);
            this.pnlObject.Controls.Add(this.rdObjCmb);
            this.pnlObject.Controls.Add(this.rdObjTxt);
            this.pnlObject.Controls.Add(this.rdObjChk);
            this.pnlObject.Controls.Add(this.rdObjRd);
            this.pnlObject.Location = new System.Drawing.Point(396, 14);
            this.pnlObject.Name = "pnlObject";
            this.pnlObject.Size = new System.Drawing.Size(359, 48);
            this.pnlObject.TabIndex = 13;
            // 
            // rdObjCmbDynamic
            // 
            this.rdObjCmbDynamic.AutoSize = true;
            this.rdObjCmbDynamic.Location = new System.Drawing.Point(242, 25);
            this.rdObjCmbDynamic.Name = "rdObjCmbDynamic";
            this.rdObjCmbDynamic.Size = new System.Drawing.Size(123, 17);
            this.rdObjCmbDynamic.TabIndex = 5;
            this.rdObjCmbDynamic.TabStop = true;
            this.rdObjCmbDynamic.Tag = "DynamicComboBox";
            this.rdObjCmbDynamic.Text = "Dynamic Combo Box";
            this.rdObjCmbDynamic.UseVisualStyleBackColor = true;
            this.rdObjCmbDynamic.CheckedChanged += new System.EventHandler(this.rdObjCmbDynamic_CheckedChanged);
            // 
            // rbObjDate
            // 
            this.rbObjDate.AutoSize = true;
            this.rbObjDate.Location = new System.Drawing.Point(242, 2);
            this.rbObjDate.Name = "rbObjDate";
            this.rbObjDate.Size = new System.Drawing.Size(107, 17);
            this.rbObjDate.TabIndex = 4;
            this.rbObjDate.TabStop = true;
            this.rbObjDate.Tag = "DateTimePicker";
            this.rbObjDate.Text = "Date Time Picker";
            this.rbObjDate.UseVisualStyleBackColor = true;
            // 
            // rdObjCmb
            // 
            this.rdObjCmb.AutoSize = true;
            this.rdObjCmb.Location = new System.Drawing.Point(140, 25);
            this.rdObjCmb.Name = "rdObjCmb";
            this.rdObjCmb.Size = new System.Drawing.Size(79, 17);
            this.rdObjCmb.TabIndex = 3;
            this.rdObjCmb.TabStop = true;
            this.rdObjCmb.Tag = "ComboBox";
            this.rdObjCmb.Text = "Combo Box";
            this.rdObjCmb.UseVisualStyleBackColor = true;
            this.rdObjCmb.CheckedChanged += new System.EventHandler(this.rdObjCmb_CheckedChanged);
            // 
            // rdObjTxt
            // 
            this.rdObjTxt.AutoSize = true;
            this.rdObjTxt.Location = new System.Drawing.Point(15, 3);
            this.rdObjTxt.Name = "rdObjTxt";
            this.rdObjTxt.Size = new System.Drawing.Size(64, 17);
            this.rdObjTxt.TabIndex = 0;
            this.rdObjTxt.TabStop = true;
            this.rdObjTxt.Tag = "TextBox";
            this.rdObjTxt.Text = "TextBox";
            this.rdObjTxt.UseVisualStyleBackColor = true;
            // 
            // rdObjChk
            // 
            this.rdObjChk.AutoSize = true;
            this.rdObjChk.Location = new System.Drawing.Point(15, 25);
            this.rdObjChk.Name = "rdObjChk";
            this.rdObjChk.Size = new System.Drawing.Size(77, 17);
            this.rdObjChk.TabIndex = 2;
            this.rdObjChk.TabStop = true;
            this.rdObjChk.Tag = "CheckBox";
            this.rdObjChk.Text = "Check Box";
            this.rdObjChk.UseVisualStyleBackColor = true;
            // 
            // rdObjRd
            // 
            this.rdObjRd.AutoSize = true;
            this.rdObjRd.Location = new System.Drawing.Point(140, 2);
            this.rdObjRd.Name = "rdObjRd";
            this.rdObjRd.Size = new System.Drawing.Size(87, 17);
            this.rdObjRd.TabIndex = 1;
            this.rdObjRd.TabStop = true;
            this.rdObjRd.Tag = "RadioButton";
            this.rdObjRd.Text = "Radio Button";
            this.rdObjRd.UseVisualStyleBackColor = true;
            // 
            // lblValidationMsg
            // 
            this.lblValidationMsg.AutoSize = true;
            this.lblValidationMsg.Location = new System.Drawing.Point(855, 191);
            this.lblValidationMsg.Name = "lblValidationMsg";
            this.lblValidationMsg.Size = new System.Drawing.Size(99, 13);
            this.lblValidationMsg.TabIndex = 28;
            this.lblValidationMsg.Text = "Validation Message";
            // 
            // lblMaxValue
            // 
            this.lblMaxValue.AutoSize = true;
            this.lblMaxValue.Location = new System.Drawing.Point(855, 165);
            this.lblMaxValue.Name = "lblMaxValue";
            this.lblMaxValue.Size = new System.Drawing.Size(81, 13);
            this.lblMaxValue.TabIndex = 26;
            this.lblMaxValue.Text = "Maximum Value";
            // 
            // lblMinValue
            // 
            this.lblMinValue.AutoSize = true;
            this.lblMinValue.Location = new System.Drawing.Point(855, 141);
            this.lblMinValue.Name = "lblMinValue";
            this.lblMinValue.Size = new System.Drawing.Size(78, 13);
            this.lblMinValue.TabIndex = 24;
            this.lblMinValue.Text = "Minimum Value";
            // 
            // lblValueType
            // 
            this.lblValueType.AutoSize = true;
            this.lblValueType.Location = new System.Drawing.Point(19, 124);
            this.lblValueType.Name = "lblValueType";
            this.lblValueType.Size = new System.Drawing.Size(61, 13);
            this.lblValueType.TabIndex = 8;
            this.lblValueType.Text = "Value Type";
            // 
            // lblUpperCase
            // 
            this.lblUpperCase.AutoSize = true;
            this.lblUpperCase.Location = new System.Drawing.Point(855, 43);
            this.lblUpperCase.Name = "lblUpperCase";
            this.lblUpperCase.Size = new System.Drawing.Size(63, 13);
            this.lblUpperCase.TabIndex = 18;
            this.lblUpperCase.Text = "Upper Case";
            // 
            // lblAllowNull
            // 
            this.lblAllowNull.AutoSize = true;
            this.lblAllowNull.Location = new System.Drawing.Point(855, 19);
            this.lblAllowNull.Name = "lblAllowNull";
            this.lblAllowNull.Size = new System.Drawing.Size(53, 13);
            this.lblAllowNull.TabIndex = 16;
            this.lblAllowNull.Text = "Allow Null";
            // 
            // lblDefaultValue
            // 
            this.lblDefaultValue.AutoSize = true;
            this.lblDefaultValue.Location = new System.Drawing.Point(20, 74);
            this.lblDefaultValue.Name = "lblDefaultValue";
            this.lblDefaultValue.Size = new System.Drawing.Size(71, 13);
            this.lblDefaultValue.TabIndex = 4;
            this.lblDefaultValue.Text = "Default Value";
            // 
            // lblObjectID
            // 
            this.lblObjectID.AutoSize = true;
            this.lblObjectID.Location = new System.Drawing.Point(327, 19);
            this.lblObjectID.Name = "lblObjectID";
            this.lblObjectID.Size = new System.Drawing.Size(65, 13);
            this.lblObjectID.TabIndex = 12;
            this.lblObjectID.Text = "Object Type";
            // 
            // lblFormID
            // 
            this.lblFormID.AutoSize = true;
            this.lblFormID.Location = new System.Drawing.Point(19, 97);
            this.lblFormID.Name = "lblFormID";
            this.lblFormID.Size = new System.Drawing.Size(33, 13);
            this.lblFormID.TabIndex = 6;
            this.lblFormID.Text = "Form ";
            // 
            // txtTagName
            // 
            this.txtTagName.Location = new System.Drawing.Point(119, 45);
            this.txtTagName.MaxLength = 24;
            this.txtTagName.Name = "txtTagName";
            this.txtTagName.Size = new System.Drawing.Size(200, 20);
            this.txtTagName.TabIndex = 3;
            // 
            // lblTagName
            // 
            this.lblTagName.AutoSize = true;
            this.lblTagName.Location = new System.Drawing.Point(20, 48);
            this.lblTagName.Name = "lblTagName";
            this.lblTagName.Size = new System.Drawing.Size(57, 13);
            this.lblTagName.TabIndex = 2;
            this.lblTagName.Text = "Tag Name";
            // 
            // txtTagID
            // 
            this.txtTagID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTagID.Location = new System.Drawing.Point(119, 19);
            this.txtTagID.MaxLength = 5;
            this.txtTagID.Name = "txtTagID";
            this.txtTagID.Size = new System.Drawing.Size(100, 20);
            this.txtTagID.TabIndex = 1;
            // 
            // lblTagID
            // 
            this.lblTagID.AutoSize = true;
            this.lblTagID.Location = new System.Drawing.Point(19, 22);
            this.lblTagID.Name = "lblTagID";
            this.lblTagID.Size = new System.Drawing.Size(40, 13);
            this.lblTagID.TabIndex = 0;
            this.lblTagID.Text = "Tag ID";
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnCancel);
            this.gbButton.Controls.Add(this.btnAddSave);
            this.gbButton.Location = new System.Drawing.Point(12, 647);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(1132, 49);
            this.gbButton.TabIndex = 3;
            this.gbButton.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnCancel.Location = new System.Drawing.Point(969, 14);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddSave
            // 
            this.btnAddSave.Image = ((System.Drawing.Image)(resources.GetObject("btnAddSave.Image")));
            this.btnAddSave.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnAddSave.Location = new System.Drawing.Point(838, 14);
            this.btnAddSave.Name = "btnAddSave";
            this.btnAddSave.Size = new System.Drawing.Size(125, 28);
            this.btnAddSave.TabIndex = 0;
            this.btnAddSave.Text = "Add";
            this.btnAddSave.Click += new System.EventHandler(this.btnAddSave_Click);
            // 
            // FrmTagReference
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1154, 706);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbTagDetail);
            this.Controls.Add(this.gbTag);
            this.Controls.Add(this.gbDatabase);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTagReference";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tag - ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmTagReference_FormClosing);
            this.Load += new System.EventHandler(this.FrmTagReference_Load);
            this.gbDatabase.ResumeLayout(false);
            this.gbDatabase.PerformLayout();
            this.gbTag.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgViewTagItem)).EndInit();
            this.gbTagDetail.ResumeLayout(false);
            this.gbTagDetail.PerformLayout();
            this.pnlDynamicCmb.ResumeLayout(false);
            this.pnlDynamicCmb.PerformLayout();
            this.pnlMasking.ResumeLayout(false);
            this.pnlMasking.PerformLayout();
            this.pnlCmbValue.ResumeLayout(false);
            this.pnlCmbValue.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCmbValue)).EndInit();
            this.pnlValueType.ResumeLayout(false);
            this.pnlValueType.PerformLayout();
            this.pnlUpperCase.ResumeLayout(false);
            this.pnlUpperCase.PerformLayout();
            this.pnlAllowNull.ResumeLayout(false);
            this.pnlAllowNull.PerformLayout();
            this.pnlObject.ResumeLayout(false);
            this.pnlObject.PerformLayout();
            this.gbButton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDatabase;
        private System.Windows.Forms.ComboBox cmbDbId;
        private System.Windows.Forms.Label lblDbID;
        private System.Windows.Forms.GroupBox gbTag;
        private System.Windows.Forms.DataGridView dgViewTagItem;
        private System.Windows.Forms.GroupBox gbTagDetail;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Label lblTagID;
        private System.Windows.Forms.TextBox txtTagID;
        private System.Windows.Forms.TextBox txtTagName;
        private System.Windows.Forms.Label lblTagName;
        private System.Windows.Forms.Label lblObjectID;
        private System.Windows.Forms.Label lblFormID;
        private System.Windows.Forms.Label lblMaxValue;
        private System.Windows.Forms.Label lblMinValue;
        private System.Windows.Forms.Label lblValueType;
        private System.Windows.Forms.Label lblUpperCase;
        private System.Windows.Forms.Label lblAllowNull;
        private System.Windows.Forms.Label lblDefaultValue;
        private System.Windows.Forms.Panel pnlAllowNull;
        private System.Windows.Forms.Panel pnlObject;
        private System.Windows.Forms.Label lblValidationMsg;
        private System.Windows.Forms.Panel pnlUpperCase;
        private System.Windows.Forms.Panel pnlCmbValue;
        private System.Windows.Forms.TextBox txtDefaultValue;
        private System.Windows.Forms.Label lblComboBoxValue;
        private System.Windows.Forms.Panel pnlValueType;
        private System.Windows.Forms.TextBox txtValidationMsg;
        private System.Windows.Forms.TextBox txtMaxValue;
        private System.Windows.Forms.TextBox txtMinValue;
        private System.Windows.Forms.TextBox txtCmbRealValue;
        private System.Windows.Forms.TextBox txtCmbDisplayValue;
        private System.Windows.Forms.Label lblRealValue;
        private System.Windows.Forms.Label lblDisplayValue;
        private System.Windows.Forms.RadioButton rdValueTypeIpAddress;
        private System.Windows.Forms.RadioButton rdValueTypeNum;
        private System.Windows.Forms.RadioButton rdValueTypeString;
        private System.Windows.Forms.RadioButton rdValueTypeTime;
        private System.Windows.Forms.RadioButton rdValueTypeAlfaNumSpace;
        private System.Windows.Forms.RadioButton rdValueTypeAlfaNum;
        private System.Windows.Forms.RadioButton rdObjCmb;
        private System.Windows.Forms.RadioButton rdObjTxt;
        private System.Windows.Forms.RadioButton rdObjChk;
        private System.Windows.Forms.RadioButton rdObjRd;
        private System.Windows.Forms.RadioButton rdUpperCaseNo;
        private System.Windows.Forms.RadioButton rdUpperCaseYes;
        private System.Windows.Forms.RadioButton rdAllowNullNo;
        private System.Windows.Forms.RadioButton rdAllowNullYes;
        private System.Windows.Forms.TextBox txtMaxLength;
        private System.Windows.Forms.TextBox txtMinLength;
        private System.Windows.Forms.Label lblMaxLength;
        private System.Windows.Forms.Label lblMinLength;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnAddSave;
        private System.Windows.Forms.DataGridViewButtonColumn colEdit;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colDelete;
        private System.Windows.Forms.CheckBox chkCheckAll;
        internal System.Windows.Forms.Button btnAddComboBoxValue;
        internal System.Windows.Forms.Button bntDeleteCmbValue;
        private System.Windows.Forms.DataGridView dgCmbValue;
        private System.Windows.Forms.Label lblItemSequence;
        private System.Windows.Forms.TextBox txtItemSequence;
        private System.Windows.Forms.ComboBox cmbForm;
        private System.Windows.Forms.ComboBox cmbFormType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlMasking;
        private System.Windows.Forms.RadioButton rdMaskingNo;
        private System.Windows.Forms.RadioButton rdMaskingYes;
        private System.Windows.Forms.Label lblMasking;
        private System.Windows.Forms.RadioButton rbObjDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdObjCmbDynamic;
        private System.Windows.Forms.Panel pnlDynamicCmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbRefValueCombo;
        private System.Windows.Forms.ComboBox cmbRefDisplayCombo;
        private System.Windows.Forms.ComboBox cmbRefFormID;
        private System.Windows.Forms.Label label5;
    }
}