using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmCopyCard : Form
    {
        SqlConnection oSqlConn;
        SqlConnection oSqlConnAuditTrail;

        /// <summary>
        /// Form to copy card range from one version to another
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        /// <param name="_oSqlConnAuditTrail">SqlConnection : Connection string to Audit Trail database</param>
        public FrmCopyCard(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        /// <summary>
        /// Load Data in Form Copy Card
        /// </summary>
        private void FrmCopyCard_Load(object sender, EventArgs e)
        {
         
            InitData();
            //CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        /// <summary>
        /// Initiate Data from database to Form
        /// </summary>
        protected void InitData()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref CmbDbDest);
            FillChl();
        }

        /// <summary>
        /// Fill CheckedListBox with list of card from database
        /// </summary>
        protected void FillChl()
        {
            chlCardList.DataSource = null;
            chlCardList.Items.Clear();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCardListBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sGetDbIdSource();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                        DataTable oDataTable = new DataTable();
                        oAdapt.Fill(oDataTable);

                        chlCardList.DataSource = oDataTable;
                        chlCardList.DisplayMember = "Name";
                        chlCardList.ValueMember = "CardID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Get Database ID value that will be used as Source Version
        /// </summary>
        /// <returns>string : Database ID</returns>
        protected string sGetDbIdSource()
        {
            return (cmbDbSource.SelectedIndex >= 0) ? cmbDbSource.SelectedValue.ToString() : "";
        }

        /// <summary>
        /// Get Version ID value which will be used as Destination
        /// </summary>
        /// <returns>string : Version ID</returns>
        protected string sGetDbIdDest()
        {
            return (CmbDbDest.SelectedIndex >= 0) ? CmbDbDest.SelectedValue.ToString() : "";
        }

        /// <summary>
        /// Call Function when Combo box Selected
        /// </summary>
        private void cmbDbSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.Items.Count > 0)
                    FillChl();
        }

        /// <summary>
        /// Validate Data and Run Save Function
        /// </summary>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValid())
                if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                    if (isSaveData())
                        this.Close();
        }

        /// <summary>
        /// Determine whether user entered data valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool isValid()
        {
            string sErrMsg = "";

            if (string.IsNullOrEmpty(sGetDbIdSource()))
                sErrMsg = string.Format("{0}{1}", "Database Source", CommonMessage.sNotChecked);
            else if (string.IsNullOrEmpty(sGetDbIdDest()))
                sErrMsg = string.Format("{0}{1}", "Database Destination", CommonMessage.sNotChecked);
            else if (sGetDbIdDest() == sGetDbIdSource())
                sErrMsg = CommonMessage.sDbSourceEqDbDest;

            if (!string.IsNullOrEmpty(sErrMsg))
                MessageBox.Show(sErrMsg);

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Determine whether data saved successfully to database or not
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        protected bool isSaveData()
        {
            if (chlCardList.CheckedItems.Count > 0)
                return isSaveSelectedCard();
            else return isSaveAllCard();
        }

        /// <summary>
        /// Determine whether all data card copied successfully from source version to destination version
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        protected bool isSaveAllCard()
        {
            string sErrMsg = "";

            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCardListCopyByDbID, oSqlConn))
            {
                try
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sNewDatabaseID", SqlDbType.VarChar).Value = sGetDbIdDest();
                    oSqlCmd.Parameters.Add("@sExistingDatabaseID", SqlDbType.VarChar).Value = sGetDbIdSource();

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                    oSqlCmd.ExecuteNonQuery();

                    string sDbName = ((DataRowView)cmbDbSource.SelectedItem)["DatabaseName"].ToString();
                    string sLogDetail = string.Format("Copied Card from {0} : ", sDbName.Replace(" ",""));
                    sDbName = ((DataRowView)CmbDbDest.SelectedItem)["DatabaseName"].ToString();

                    foreach (DataRowView drView in chlCardList.Items)
                    {
                        sLogDetail += string.Format("{0} \n", drView["Name"].ToString());
                    }
                    //CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, sDbName, "Copy Card Range", sLogDetail);
                    CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Copy Card Range", sLogDetail);
                }
                catch (Exception ex)
                {
                    sErrMsg = ex.Message;
                    CommonClass.doWriteErrorFile(sErrMsg);
                }
            }

            return string.IsNullOrEmpty(sErrMsg) ;
        }

        /// <summary>
        /// Determine whether selected cards saved successfully from source version into destination version 
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        protected bool isSaveSelectedCard()
        {
            string sErrMsg = "";
            string sDbName = ((DataRowView)cmbDbSource.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = string.Format("Copied Card from {0} : ", sDbName.Replace(" ", ""));
            sDbName = ((DataRowView)CmbDbDest.SelectedItem)["DatabaseName"].ToString();

            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                foreach (DataRowView drView in chlCardList.CheckedItems)
                    if (!string.IsNullOrEmpty((sErrMsg = SaveDataByID(drView["Name"].ToString(), oSqlTrans))))
                    {
                        oSqlTrans.Rollback();
                        CommonClass.doWriteErrorFile(sErrMsg);
                        return string.IsNullOrEmpty(sErrMsg);
                    }
                    else
                        sLogDetail += string.Format("{0} \n", drView["Name"].ToString());

                oSqlTrans.Commit();
                //CommonClass.InputLogSU(oSqlConn, "", UserData.sUserID, sDbName, "Copy Card Range", sLogDetail);
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Copy Card Range", sLogDetail);
             }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Copy each selected card in database
        /// </summary>
        /// <param name="sCardName">string : Card's name that will be save</param>
        /// <param name="oSqlTrans">SqlTransaction : Sql Transaction session to save all selected cards</param>
        /// <returns>string : error message while trying to save given card</returns>
        protected string SaveDataByID(string sCardName, SqlTransaction oSqlTrans)
        {
            string sMessage = "";
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCardListCopyByID, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sNewDatabaseID", SqlDbType.VarChar).Value = sGetDbIdDest();
                oSqlCmd.Parameters.Add("@sExistingDatabaseID", SqlDbType.VarChar).Value = sGetDbIdSource();
                oSqlCmd.Parameters.Add("@sCardName", SqlDbType.VarChar).Value = sCardName;
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        sMessage = oRead[0].ToString();
                    oRead.Close();
                }
            }
            return sMessage;
        }

        /// <summary>
        /// Check All function
        /// </summary>
        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chlCardList.Items.Count > 0)
                for (int iIndex = 0; iIndex < chlCardList.Items.Count; iIndex++)
                    chlCardList.SetItemChecked(iIndex, true);
        }
    }
}