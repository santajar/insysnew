﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSys
{
    public partial class FrmCopyGPRS : Form
    {
        SqlConnection oSqlConn;
        SqlConnection oSqlConnAuditTrail;
        string sFormType;

        /// <summary>
        /// Form to copy GPRS Data from one version to another
        /// </summary>
        /// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        public FrmCopyGPRS(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sFormType)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
            sFormType = _sFormType;
        }

        /// <summary>
        /// Load Form Copy GPRS
        /// </summary>
        private void frmCopyGPRS_Load(object sender, EventArgs e)
        {
            InitData();
            this.Text = "Copy " + sFormType;
            lblGPRSName.Text = sFormType + " Name";
            chkAll.Text = "All " + sFormType;
            CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + this.Text, "");
        }

        /// <summary>
        /// Initiate Data from database to Form
        /// </summary>
        protected void InitData()
        {
            if (sFormType == "GPRS")
            {
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPDBListGPRSBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPDBListGPRSBrowse, "", "DatabaseID", "DatabaseName", ref CmbDbDest);
                FillChl();
            }
            else if (sFormType == "TLE")
            {
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPDBListTLEBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPDBListTLEBrowse, "", "DatabaseID", "DatabaseName", ref CmbDbDest);
                FillChlTLE();
            }
            else if (sFormType == "CAPK" || sFormType == "AID")
            {
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPDBListEMVBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
                CommonClass.FillComboBox(oSqlConn, CommonSP.sSPDBListEMVBrowse, "", "DatabaseID", "DatabaseName", ref CmbDbDest);
                if (sFormType == "CAPK")
                    FillChlCAPK();
                else
                    FillChlAID();
            }
            else if (sFormType == "Remote Download")
            {
                CommonClass.FillComboBox(oSqlConn, CommonSP.spDBListRDBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
                CommonClass.FillComboBox(oSqlConn, CommonSP.spDBListRDBrowse, "", "DatabaseID", "DatabaseName", ref CmbDbDest);
                FillChlRD();
            }

            else if (sFormType == "PROMO MANAGEMENT")
            {
                CommonClass.FillComboBox(oSqlConn, CommonSP.spDBListPromoManagementBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
                CommonClass.FillComboBox(oSqlConn, CommonSP.spDBListPromoManagementBrowse, "", "DatabaseID", "DatabaseName", ref CmbDbDest);
                FillChlPromoManagement();
            }
        }

        /// <summary>
        /// Fill CheckedListBox with list of GPRS name from database
        /// </summary>
        private void FillChlRD()
        {
            chlGPRSList.DataSource = null;
            chlGPRSList.Items.Clear();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileRDCopyListBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sGetDbIdSource();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                        DataTable oDataTable = new DataTable();
                        oAdapt.Fill(oDataTable);

                        chlGPRSList.DataSource = oDataTable;
                        chlGPRSList.DisplayMember = "RemoteDownloadName";
                        chlGPRSList.ValueMember = "RemoteDownloadTagID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void FillChlPromoManagement()
        {
            chlGPRSList.DataSource = null;
            chlGPRSList.Items.Clear();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.spDBListPromoManagementListBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sGetDbIdSource();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                        DataTable oDataTable = new DataTable();
                        oAdapt.Fill(oDataTable);

                        chlGPRSList.DataSource = oDataTable;
                        chlGPRSList.DisplayMember = "PromoManagementName";
                        chlGPRSList.ValueMember = "PromoManagementID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Fill CheckedListBox with list of EMV (AID) name from database
        /// </summary>
        private void FillChlAID()
        {
            chlGPRSList.DataSource = null;
            chlGPRSList.Items.Clear();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileAIDCopyListBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sGetDbIdSource();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                        DataTable oDataTable = new DataTable();
                        oAdapt.Fill(oDataTable);

                        chlGPRSList.DataSource = oDataTable;
                        chlGPRSList.DisplayMember = "AIDName";
                        chlGPRSList.ValueMember = "AIDTagID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Fill CheckedListBox with list of EMV (CAPK) name from database
        /// </summary>
        private void FillChlCAPK()
        {
            chlGPRSList.DataSource = null;
            chlGPRSList.Items.Clear();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileCAPKCopyListBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sGetDbIdSource();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                        DataTable oDataTable = new DataTable();
                        oAdapt.Fill(oDataTable);

                        chlGPRSList.DataSource = oDataTable;
                        chlGPRSList.DisplayMember = "CAPKIndex";
                        chlGPRSList.ValueMember = "CAPKTagID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Fill CheckedListBox with list of GPRS name from database
        /// </summary>
        private void FillChlTLE()
        {
            chlGPRSList.DataSource = null;
            chlGPRSList.Items.Clear();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTLECopyListBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sGetDbIdSource();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                        DataTable oDataTable = new DataTable();
                        oAdapt.Fill(oDataTable);

                        chlGPRSList.DataSource = oDataTable;
                        chlGPRSList.DisplayMember = "TLEName";
                        chlGPRSList.ValueMember = "TLETagID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Fill CheckedListBox with list of GPRS name from database
        /// </summary>
        protected void FillChl()
        {
            chlGPRSList.DataSource = null;
            chlGPRSList.Items.Clear();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPGPRSCopyListBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = sGetDbIdSource();
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                    {
                        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                        DataTable oDataTable = new DataTable();
                        oAdapt.Fill(oDataTable);

                        chlGPRSList.DataSource = oDataTable;
                        chlGPRSList.DisplayMember = "GPRSName";
                        chlGPRSList.ValueMember = "GPRSTagID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Get Database ID value that will be used as Source Version
        /// </summary>
        /// <returns>string : Database ID</returns>
        protected string sGetDbIdSource()
        {
            return (cmbDbSource.SelectedIndex >= 0) ? cmbDbSource.SelectedValue.ToString() : "";
        }

        /// <summary>
        /// Get Version ID value which will be used as Destination
        /// </summary>
        /// <returns>string : Version ID</returns>
        protected string sGetDbIdDest()
        {
            return (CmbDbDest.SelectedIndex >= 0) ? CmbDbDest.SelectedValue.ToString() : "";
        }

        private void cmbDbSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.Items.Count > 0)
                    if (sFormType == "GPRS")
                        FillChl();
                    else if (sFormType == "TLE")
                        FillChlTLE();
                    else if (sFormType == "AID")
                        FillChlAID();
                    else if (sFormType == "CAPK")
                        FillChlCAPK();
                    else if (sFormType == "Remote Download")
                        FillChlRD();
                    else if (sFormType == "PROMO MANAGEMENT")
                        FillChlPromoManagement();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                if (CommonClass.isYesMessage(CommonMessage.sConfirmationText, CommonMessage.sConfirmationTitle))
                    if (isSaveData())
                        this.Close();
            }
        }

        /// <summary>
        /// Determine whether user entered data valid or not
        /// </summary>
        /// <returns>boolean : true if valid</returns>
        protected bool isValid()
        {
            string sErrMsg = "";

            if (string.IsNullOrEmpty(sGetDbIdSource()))
                sErrMsg = string.Format("{0}{1}", "Database Source", CommonMessage.sNotChecked);
            else if (string.IsNullOrEmpty(sGetDbIdDest()))
                sErrMsg = string.Format("{0}{1}", "Database Destination", CommonMessage.sNotChecked);
            else if (sGetDbIdDest() == sGetDbIdSource())
                sErrMsg = CommonMessage.sDbSourceEqDbDest;

            if (!string.IsNullOrEmpty(sErrMsg))
                MessageBox.Show(sErrMsg);

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Determine whether data saved successfully to database or not
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        protected bool isSaveData()
        {

            if (chlGPRSList.CheckedItems.Count > 0)
                if (sFormType == "GPRS")
                    return isSaveSelectedGPRS();
                else if (sFormType == "TLE")
                    return isSaveSelectedTLE();
                else if (sFormType == "AID")
                    return isSaveSelectedAID();
                else if (sFormType == "CAPK")
                    return isSaveSelectedCAPK();
                else if (sFormType == "Remote Download")
                    return isSaveSelectedRemoteDownload();
                //return false;
                else if (sFormType == "PROMO MANAGEMENT")
                    return isSaveSelectedPromoManagement();
                 return false;
           
            //else return isSaveAllGPRS();
        }

        /// <summary>
        /// Determine whether selected Remote Download saved successfully from source version into destination version 
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        private bool isSaveSelectedRemoteDownload()
        {
            string sErrMsg = "";
            string sDbName = ((DataRowView)cmbDbSource.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = string.Format("Copied Remote Download from {0} : ", sDbName.Replace(" ", ""));
            sDbName = ((DataRowView)CmbDbDest.SelectedItem)["DatabaseName"].ToString();

            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                foreach (DataRowView drView in chlGPRSList.CheckedItems)
                    if (!string.IsNullOrEmpty((sErrMsg = SaveDataRemoteDownloadByID(drView["RemoteDownloadName"].ToString(), oSqlTrans))))
                    {
                        oSqlTrans.Rollback();
                        CommonClass.doWriteErrorFile(sErrMsg);
                        return string.IsNullOrEmpty(sErrMsg);
                    }
                    else
                        sLogDetail += string.Format("{0} \n", drView["RemoteDownloadName"].ToString());

                oSqlTrans.Commit();
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Copy Remote Download", sLogDetail);
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Determine whether selected CAPK saved successfully from source version into destination version 
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        private bool isSaveSelectedCAPK()
        {
            string sErrMsg = "";
            string sDbName = ((DataRowView)cmbDbSource.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = string.Format("Copied CAPK from {0} : ", sDbName.Replace(" ", ""));
            sDbName = ((DataRowView)CmbDbDest.SelectedItem)["DatabaseName"].ToString();

            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                foreach (DataRowView drView in chlGPRSList.CheckedItems)
                    if (!string.IsNullOrEmpty((sErrMsg = SaveDataCAPKByID(drView["CAPKIndex"].ToString(), oSqlTrans))))
                    {
                        oSqlTrans.Rollback();
                        CommonClass.doWriteErrorFile(sErrMsg);
                        return string.IsNullOrEmpty(sErrMsg);
                    }
                    else
                        sLogDetail += string.Format("{0} \n", drView["CAPKIndex"].ToString());

                oSqlTrans.Commit();
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Copy CAPK", sLogDetail);
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }

            return string.IsNullOrEmpty(sErrMsg);
        }

        private bool isSaveSelectedPromoManagement()
        {
            string sErrMsg = "";
            string sDbName = ((DataRowView)cmbDbSource.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = string.Format("Copied Promo Management from {0} : ", sDbName.Replace(" ", ""));
            sDbName = ((DataRowView)CmbDbDest.SelectedItem)["DatabaseName"].ToString();

            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                foreach (DataRowView drView in chlGPRSList.CheckedItems)
                    if (!string.IsNullOrEmpty((sErrMsg = SaveDataPromoManagementByID(drView["PromoManagementName"].ToString(), oSqlTrans))))
                    {
                        oSqlTrans.Rollback();
                        CommonClass.doWriteErrorFile(sErrMsg);
                        return string.IsNullOrEmpty(sErrMsg);
                    }
                    else
                        sLogDetail += string.Format("{0} \n", drView["PromoManagementName"].ToString());

                oSqlTrans.Commit();
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Copy  Promo Management Name", sLogDetail);
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Determine whether selected AID saved successfully from source version into destination version 
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        private bool isSaveSelectedAID()
        {
            string sErrMsg = "";
            string sDbName = ((DataRowView)cmbDbSource.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = string.Format("Copied AID from {0} : ", sDbName.Replace(" ", ""));
            sDbName = ((DataRowView)CmbDbDest.SelectedItem)["DatabaseName"].ToString();

            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                foreach (DataRowView drView in chlGPRSList.CheckedItems)
                    if (!string.IsNullOrEmpty((sErrMsg = SaveDataAIDByID(drView["AIDName"].ToString(), oSqlTrans))))
                    {
                        oSqlTrans.Rollback();
                        CommonClass.doWriteErrorFile(sErrMsg);
                        return string.IsNullOrEmpty(sErrMsg);
                    }
                    else
                        sLogDetail += string.Format("{0} \n", drView["AIDName"].ToString());

                oSqlTrans.Commit();
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Copy AID", sLogDetail);
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Determine whether selected TLE saved successfully from source version into destination version 
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        private bool isSaveSelectedTLE()
        {
            string sErrMsg = "";
            string sDbName = ((DataRowView)cmbDbSource.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = string.Format("Copied TLE from {0} : ", sDbName.Replace(" ", ""));
            sDbName = ((DataRowView)CmbDbDest.SelectedItem)["DatabaseName"].ToString();

            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                foreach (DataRowView drView in chlGPRSList.CheckedItems)
                    if (!string.IsNullOrEmpty((sErrMsg = SaveDataTLEByID(drView["TLEName"].ToString(), oSqlTrans))))
                    {
                        oSqlTrans.Rollback();
                        CommonClass.doWriteErrorFile(sErrMsg);
                        return string.IsNullOrEmpty(sErrMsg);
                    }
                    else
                        sLogDetail += string.Format("{0} \n", drView["TLEName"].ToString());

                oSqlTrans.Commit();
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Copy TLE", sLogDetail);
            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Determine whether selected GPRS saved successfully from source version into destination version 
        /// </summary>
        /// <returns>boolean : true if successful</returns>
        protected bool isSaveSelectedGPRS()
        {
            string sErrMsg = "";
            string sDbName = ((DataRowView)cmbDbSource.SelectedItem)["DatabaseName"].ToString();
            string sLogDetail = string.Format("Copied GPRS from {0} : ", sDbName.Replace(" ", ""));
            sDbName = ((DataRowView)CmbDbDest.SelectedItem)["DatabaseName"].ToString();

            SqlTransaction oSqlTrans = oSqlConn.BeginTransaction(UserData.sUserID + DateTime.Now.ToString("dd MMM yyy   HH:mm:ss"));
            try
            {
                foreach (DataRowView drView in chlGPRSList.CheckedItems)
                    if (!string.IsNullOrEmpty((sErrMsg = SaveDataByID(drView["GPRSName"].ToString(), oSqlTrans))))
                    {
                        oSqlTrans.Rollback();
                        CommonClass.doWriteErrorFile(sErrMsg);
                        return string.IsNullOrEmpty(sErrMsg);
                    }
                    else
                        sLogDetail += string.Format("{0} \n", drView["GPRSName"].ToString());

                oSqlTrans.Commit();
                CommonClass.InputLogSU(oSqlConnAuditTrail, "", UserData.sUserID, sDbName, "Copy GPRS", sLogDetail);
             }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
                CommonClass.doWriteErrorFile(sErrMsg);
                oSqlTrans.Rollback();
            }

            return string.IsNullOrEmpty(sErrMsg);
        }

        /// <summary>
        /// Copy each selected card in database
        /// </summary>
        /// <param name="sGPRSName">string : GPRS's name that will be save</param>
        /// <param name="oSqlTrans">SqlTransaction : Sql Transaction session to save all selected GPRS</param>
        /// <returns>string : error message while trying to save given GPRS</returns>
        private string SaveDataAIDByID(string sAIDName, SqlTransaction oSqlTrans)
        {
            string sMessage = "";
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileAIDCopy, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@NewDatabaseID", SqlDbType.VarChar).Value = sGetDbIdDest();
                oSqlCmd.Parameters.Add("@OldDatabaseID", SqlDbType.VarChar).Value = sGetDbIdSource();
                oSqlCmd.Parameters.Add("@sAIDName", SqlDbType.VarChar).Value = sAIDName;
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        sMessage = oRead[0].ToString();
                    oRead.Close();
                }
            }
            return sMessage;
        }

        /// <summary>
        /// Copy each selected Remote Download in database
        /// </summary>
        /// <param name="sGPRSName">string : Remote Download's name that will be save</param>
        /// <param name="oSqlTrans">SqlTransaction : Sql Transaction session to save all selected Remote Download</param>
        /// <returns>string : error message while trying to save given Remote Download</returns>
        private string SaveDataRemoteDownloadByID(string sRemoteDownloadName, SqlTransaction oSqlTrans)
        {
            string sMessage = "";
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileRemoteDownloadCopy, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@NewDatabaseID", SqlDbType.VarChar).Value = sGetDbIdDest();
                oSqlCmd.Parameters.Add("@OldDatabaseID", SqlDbType.VarChar).Value = sGetDbIdSource();
                oSqlCmd.Parameters.Add("@sRemoteDownloadName", SqlDbType.VarChar).Value = sRemoteDownloadName;
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        sMessage = oRead[0].ToString();
                    oRead.Close();
                }
            }
            return sMessage;
        }

        /// <summary>
        /// Copy each selected CAPK in database
        /// </summary>
        /// <param name="sGPRSName">string : CAPK ID's name that will be save</param>
        /// <param name="oSqlTrans">SqlTransaction : Sql Transaction session to save all selected CAPK</param>
        /// <returns>string : error message while trying to save given CAPK</returns>
        private string SaveDataCAPKByID(string sCAPKIndex, SqlTransaction oSqlTrans)
        {
            string sMessage = "";
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileCAPKCopy, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@NewDatabaseID", SqlDbType.VarChar).Value = sGetDbIdDest();
                oSqlCmd.Parameters.Add("@OldDatabaseID", SqlDbType.VarChar).Value = sGetDbIdSource();
                oSqlCmd.Parameters.Add("@sCAPKIndex", SqlDbType.VarChar).Value = sCAPKIndex;
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        sMessage = oRead[0].ToString();
                    oRead.Close();
                }
            }
            return sMessage;
        }

        private string SaveDataPromoManagementByID(string sPromoManagementName, SqlTransaction oSqlTrans)
        {
            string sMessage = "";
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPPromoManagementCopyByDbId, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@NewDatabaseID", SqlDbType.VarChar).Value = sGetDbIdDest();
                oSqlCmd.Parameters.Add("@OldDatabaseID", SqlDbType.VarChar).Value = sGetDbIdSource();
                oSqlCmd.Parameters.Add("@sPromoManagementName", SqlDbType.VarChar).Value = sPromoManagementName;
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        sMessage = oRead[0].ToString();
                    oRead.Close();
                }
            }
            return sMessage;
        }

        /// <summary>
        /// Copy each selected card in database
        /// </summary>
        /// <param name="sGPRSName">string : GPRS's name that will be save</param>
        /// <param name="oSqlTrans">SqlTransaction : Sql Transaction session to save all selected GPRS</param>
        /// <returns>string : error message while trying to save given GPRS</returns>
        protected string SaveDataByID(string sGPRSName, SqlTransaction oSqlTrans)
        {
            string sMessage = "";
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileGPRSInsertByIdAndName, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@NewDatabaseID", SqlDbType.VarChar).Value = sGetDbIdDest();
                oSqlCmd.Parameters.Add("@OldDatabaseID", SqlDbType.VarChar).Value = sGetDbIdSource();
                oSqlCmd.Parameters.Add("@GPRSName", SqlDbType.VarChar).Value = sGPRSName;
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        sMessage = oRead[0].ToString();
                    oRead.Close();
                }
            }
            return sMessage;
        }

        /// <summary>
        /// Copy each selected TLE in database
        /// </summary>
        /// <param name="sTLEName">string : TLE's name that will be save</param>
        /// <param name="oSqlTrans">SqlTransaction : Sql Transaction session to save all selected GPRS</param>
        /// <returns>string : error message while trying to save given TLE</returns>
        protected string SaveDataTLEByID(string sTLEName, SqlTransaction oSqlTrans)
        {
            string sMessage = "";
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileTLECopy, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@NewDatabaseID", SqlDbType.VarChar).Value = sGetDbIdDest();
                oSqlCmd.Parameters.Add("@OldDatabaseID", SqlDbType.VarChar).Value = sGetDbIdSource();
                oSqlCmd.Parameters.Add("@sTLEName", SqlDbType.VarChar).Value = sTLEName;
                oSqlCmd.Transaction = oSqlTrans;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        sMessage = oRead[0].ToString();
                    oRead.Close();
                }
            }
            return sMessage;
        }

        private void chkAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chlGPRSList.Items.Count > 0)
                for (int iIndex = 0; iIndex < chlGPRSList.Items.Count; iIndex++)
                    chlGPRSList.SetItemChecked(iIndex, true);
        }
    }
}
