﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;
using System.Globalization;
using InSysClass;

namespace InSys
{
    public partial class FrmImportTag : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        string sItemListFile = null;
        string sItemComboBoxFile = null;
        string sDatabaseId = null;
        
        /// <summary>
        /// initialize Form Import Tag
        /// </summary>
        /// <param name="_oSqlConn"></param>
        /// <param name="_oSqlConnAuditTrail"></param>
        public FrmImportTag(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        {
            InitializeComponent();

            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        /// <summary>
        /// Load Data Form Tag
        /// </summary>
        private void FrmImportTag_Load(object sender, EventArgs e)
        {
            InitCmb();
        }

        /// <summary>
        /// Browse File Location
        /// </summary>
        private void BrowseFile(object sender, EventArgs e)
        {
            OpenFileDialog ofdImport = new OpenFileDialog();
            if (((Button)sender).Tag == "itemlist")
                ofdImport.Filter = "Excel CSV(*.csv)|*ItemList.csv";
            else
                ofdImport.Filter = "Excel CSV(*.csv)|*ItemComboBox.csv";
            ofdImport.InitialDirectory = Environment.CurrentDirectory + @"\FILE";
            if (ofdImport.ShowDialog() == DialogResult.OK)
            {
                string sFilename = ofdImport.FileName;
                if (((Button)sender).Tag == "itemlist")
                    txtItemListFile.Text = sFilename;
                else
                    txtItemComboBoxFile.Text = sFilename;
            }
        }

        /// <summary>
        /// Run Import Function
        /// </summary>
        private void btnImport_Click(object sender, EventArgs e)
        {
            sItemComboBoxFile = txtItemComboBoxFile.Text;
            sItemListFile = txtItemListFile.Text;
            sDatabaseId = cmbDatabase.SelectedValue.ToString();
            if (!string.IsNullOrEmpty(sDatabaseId))
                if (!string.IsNullOrEmpty(sItemListFile) || !string.IsNullOrEmpty(sItemComboBoxFile))
                {
                    InitDisplay(false);
                    bwImport.RunWorkerAsync();
                }
                else
                    MessageBox.Show("Please choose ItemList or ItemComboBox file.");
            else
                MessageBox.Show("Please choose Database version destination.");
        }

        private void bwImport_DoWork(object sender, DoWorkEventArgs e)
        {
            bwImport.ReportProgress(0, "Starting Import.");
            #region "ItemList"
            if (!string.IsNullOrEmpty(sItemListFile))
            {
                // loading itemlist
                bwImport.ReportProgress(0, "Loading ItemList file. START.");
                DataTable dtItemList = new DataTable();
                dtItemList.Clear();
                dtItemList = dtGetDataTableFromCsv(sItemListFile, true);
                DataColumn colDbId = new DataColumn("DatabaseID");
                colDbId.DefaultValue = sDatabaseId;
                dtItemList.Columns.Remove("DatabaseID");
                dtItemList.Columns.Add(colDbId);

                DataTable dtItemListSchema = new DataTable();
                dtItemListSchema = dtGetSchemaItemListCmbBox(true);
                bwImport.ReportProgress(0, "Loading ItemList file. DONE.");

                // import itemlist
                bwImport.ReportProgress(0, "Import ItemList. START.");
                SqlTransaction oTrans = oSqlConn.BeginTransaction();
                try
                {
                    SqlBulkCopy sqlBulkItemList = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.Default, oTrans);
                    sqlBulkItemList.DestinationTableName = "tbItemList";
                    foreach (DataColumn col in dtItemList.Columns)
                    {
                        if (col.ColumnName != "ObjectID" && col.ColumnName != "LengthofTagValueLength")
                            if (dtItemListSchema.Columns.Contains(col.ColumnName))
                                sqlBulkItemList.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            else
                                bwImport.ReportProgress(0, "Error invalid ItemList : " + col.ColumnName);
                        else
                            sqlBulkItemList.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }
                    sqlBulkItemList.WriteToServer(dtItemList);
                    oTrans.Commit();
                }
                catch (Exception ex)
                {
                    bwImport.ReportProgress(0, "Error import ItemList : " + ex.Message);
                    oTrans.Rollback();
                }
                bwImport.ReportProgress(0, "Import ItemList. DONE.");
            }
            #endregion
            #region "ItemComboBox"
            if (!string.IsNullOrEmpty(sItemComboBoxFile))
            {
                bwImport.ReportProgress(0, "Loading ItemComboBox file. START.");
                DataTable dtItemCmbBox = new DataTable();
                dtItemCmbBox.Clear();
                //dtItemCmbBox = dtGetDataTableFromCsv(sItemComboBoxFile, true);
                dtItemCmbBox = dtCsvToDataTable(sItemComboBoxFile, true);

                DataColumn colDbId = new DataColumn("DatabaseID");
                colDbId.DefaultValue = sDatabaseId;
                dtItemCmbBox.Columns.Remove("DatabaseID");
                dtItemCmbBox.Columns.Add(colDbId);

                DataTable dtItemCmbBoxSchema = new DataTable();
                dtItemCmbBoxSchema = dtGetSchemaItemListCmbBox(false);
                bwImport.ReportProgress(0, "Loading ItemComboBox file. DONE.");

                // import ItemComboBox
                bwImport.ReportProgress(0, "Import ItemComboBox. START.");
                SqlTransaction oTrans = oSqlConn.BeginTransaction();
                try
                {
                    SqlBulkCopy sqlBulkItemCmbBox = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.Default, oTrans);
                    sqlBulkItemCmbBox.DestinationTableName = "tbItemComboBoxValue";

                    foreach (DataColumn col in dtItemCmbBox.Columns)
                    {
                        if (dtItemCmbBoxSchema.Columns.Contains(col.ColumnName))
                            sqlBulkItemCmbBox.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                        else
                            bwImport.ReportProgress(0, "Error invalid ItemComboBox : " + col.ColumnName);
                    }
                    sqlBulkItemCmbBox.WriteToServer(dtItemCmbBox);
                    oTrans.Commit();
                }
                catch (Exception ex)
                {
                    bwImport.ReportProgress(0, "Error import ItemComboBox : " + ex.Message);
                    oTrans.Rollback();
                }
                bwImport.ReportProgress(0, "Import ItemComboBox. DONE.");
            }
            #endregion
            bwImport.ReportProgress(100, "Complete.");
        }

        private void bwImport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            InitDisplay(true);
        }

        private void bwImport_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string sMessage = string.Format("[{0:dd-MM-yyyy hh:mm:ss.fff tt}] {1}", DateTime.Now, e.UserState);
            if (string.IsNullOrEmpty(rtbStatus.Text))
                rtbStatus.AppendText(sMessage);
            else
                rtbStatus.AppendText("\n" + sMessage);
        }

        #region "Function"
        /// <summary>
        /// Initialize Database
        /// </summary>
        protected void InitCmb()
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDatabase);
        }

        /// <summary>
        /// Set Display 
        /// </summary>
        protected void InitDisplay(bool isEnable)
        {
            gbData.Enabled = isEnable;
            gbButton.Enabled = isEnable;
            pbImportProcess.Style = isEnable ? ProgressBarStyle.Blocks : ProgressBarStyle.Marquee;
            if (!isEnable) rtbStatus.Clear();
        }

        protected DataTable dtGetDataTableFromCsv(string sPath, bool isFirstRowHeader)
        {
            string sHeader = isFirstRowHeader ? "Yes" : "No";

            string sPathOnly = Path.GetDirectoryName(sPath) + @"\";
            string sFileName = Path.GetFileName(sPath);
            string sTable = sFileName.Replace(".csv", "");

            string sQuery = @"SELECT * FROM [" + sFileName + "]";

            using (OleDbConnection oCsvConn = new OleDbConnection(string.Format(
                      @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='{0}';Extended Properties=""Text;HDR={1};FMT=Delimited(,)""",
                      sPathOnly, sHeader)))
            {
                oCsvConn.Open();
                using (OleDbCommand oCmd = new OleDbCommand(sQuery, oCsvConn))
                using (OleDbDataAdapter oAdapter = new OleDbDataAdapter(oCmd))
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp.Locale = CultureInfo.CurrentCulture;
                    oAdapter.Fill(dtTemp);
                    return dtTemp;
                }
            }
        }

        protected DataTable dtCsvToDataTable(string sFilename, bool isRowOneHeader)
        {
            DataTable csvDataTable = new DataTable();

            //no try/catch - add these in yourselfs or let exception happen
            string[] csvData = File.ReadAllLines(sFilename);

            //if no data in file ‘manually’ throw an exception
            if (csvData.Length == 0)
            {
                throw new Exception("CSV File Appears to be Empty");
            }

            string[] headings = csvData[0].Split(',');
            int index = 0; //will be zero or one depending on isRowOneHeader

            if (isRowOneHeader) //if first record lists headers
            {
                index = 1; //so we won’t take headings as data
                //for each heading
                for (int i = 0; i < headings.Length; i++)
                {
                    //replace spaces with underscores for column names
                    headings[i] = headings[i].Replace(" ", "_");

                    //add a column for each heading
                    csvDataTable.Columns.Add(headings[i], typeof(string));
                }
            }
            else //if no headers just go for col1, col2 etc.
            {
                for (int i = 0; i < headings.Length; i++)
                {
                    //create arbitary column names
                    csvDataTable.Columns.Add("col" + (i + 1).ToString(), typeof(string));
                }
            }

            //populate the DataTable
            for (int i = index; i < csvData.Length; i++)
            {
                //create new rows
                DataRow row = csvDataTable.NewRow();
                for (int j = 0; j < headings.Length; j++)
                {
                    //fill them
                    row[j] = csvData[i].Split(',')[j];
                }
                //add rows to over DataTable
                csvDataTable.Rows.Add(row);
            }
            //return the CSV DataTable
            return csvDataTable;
        }

        /// <summary>
        /// Get schema of combo box list
        /// </summary>
        protected DataTable dtGetSchemaItemListCmbBox(bool isItemList)
        {
            using (SqlCommand oCmd = new SqlCommand(isItemList ? CommonSP.sSPItemBrowse : CommonSP.sSPCmbBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCond", SqlDbType.VarChar).Value = "WHERE DatabaseID=0";
                using (SqlDataAdapter oAdapter = new SqlDataAdapter(oCmd))
                {
                    DataTable dtTemp = new DataTable();
                    oAdapter.Fill(dtTemp);
                    return dtTemp;
                }
            }
        }
        #endregion
    }
}