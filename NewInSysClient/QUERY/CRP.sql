set nocount on
IF OBJECT_ID ('Tempdb..#output') IS NOT NULL
      DROP TABLE #output
      
IF OBJECT_ID ('Tempdb..#acquirer') IS NOT NULL
      DROP TABLE #acquirer
      
IF OBJECT_ID ('Tempdb..#acquirerMerch') IS NOT NULL
      DROP TABLE #acquirerMerch
      
                  
create table #output(id int identity(1,1), kolom varchar(30))

insert into #output(kolom)
select left(Cast(('0'+convert(varchar,getDate(),112)) as varchar(29)) + '                             ',29)

IF OBJECT_ID ('Tempdb..#acquirer') IS NOT NULL
      DROP TABLE #acquirer
select TerminalID, AcquirerName, AA004, AA005
into #acquirer
from tbprofileacquirer with(nolock)
--where acquirertag in ('aa04','aa05','aa004','aa005')

select * from #acquirer

select a.TerminalID,a.AcquirerName,a.AA005 [TID], b.AA004 [MID] 
into #acquirerMerch
from (select TerminalID,AcquirerName,AA005 from #acquirer ) a join (
select TerminalID,AcquirerName,AA004 from #acquirer ) b on a.TerminalID=b.TerminalID and a.AcquirerName=b.AcquirerName

--select convert(varchar,'1')+TerminalID+[TID]++isnull([MID],'') from #acquirerMerch where [MID] is null

insert into #output(kolom)
select left(Cast((convert(varchar,'1')+left(Cast(TerminalID as varchar(8)) + '        ',8)+left(Cast([TID] as varchar(8)) + '        ',8)+right(convert(varchar,isnull([MID],'')),12)) as varchar(29)) + '                             ',29)
from #acquirerMerch
union all
select left(Cast((convert(varchar,'9')+convert(varchar,count(*))) as varchar(29)) + '                             ',29)
from #acquirerMerch



select kolom from #output
order by id

