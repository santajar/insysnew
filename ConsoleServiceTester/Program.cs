﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using ipXML;
using System.Web.Configuration;
using System.Globalization;

namespace ConsoleServiceTester
{
    class Program
    {
        static int iTotalColumn = 294;
        static string sLog, sLogAddUpdate;
        static string[] asFilename;
        static void Main(string[] args)
        {
            int iTimeOut = Int32.Parse(WebConfigurationManager.AppSettings["TimeOut"]);

            try
            {
                string sCurrDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                sLog = sCurrDir + @"\LOG\";

                if (!Directory.Exists(sLog))
                    Directory.CreateDirectory(sLog);

                sLogAddUpdate = sCurrDir + @"\XML\";
                if (!Directory.Exists(sLogAddUpdate))
                    Directory.CreateDirectory(sLogAddUpdate);

                EMSCommonClass.Directory = sCurrDir;
                EMSCommonClass.DirectoryLog = sLog;

                if (Directory.Exists(sLogAddUpdate))
                    asFilename = Directory.GetFiles(sLogAddUpdate, @"*.xml", SearchOption.TopDirectoryOnly);

                iTotalColumn = int.Parse(ConfigurationManager.AppSettings.Get("Total Param EMS"));

                Trace.Write(sLog, Trace.sDateYYYYMMDD() + " LOG CALL", string.Format("[{0}] : ==>INSTANT&METHOD OUT LOOPS;Set Time:" + iTimeOut, Trace.sFullTime()));
                Console.WriteLine(Trace.sFullTime() + " ==>INSTANT&METHOD OUT LOOPS;Set Time:" + iTimeOut);

                string sInstantBefore = Trace.sFullTime();
                Console.WriteLine(sInstantBefore + " Before Instant");
                Trace.Write(sLog, Trace.sDateYYYYMMDD() + " LOG CALL", string.Format("[{0}] :Before Instant", sInstantBefore));
                WebServiceTms.ServiceSoap serviceTms = new WebServiceTms.ServiceSoapClient();

                WebServiceTms.sStartXMLWebServiceRequest request = new WebServiceTms.sStartXMLWebServiceRequest();
                request.Body = new WebServiceTms.sStartXMLWebServiceRequestBody();
                WebServiceTms.sStartXMLWebServiceResponse response = new WebServiceTms.sStartXMLWebServiceResponse();
                response.Body = new WebServiceTms.sStartXMLWebServiceResponseBody();
                string sInstantAfter = Trace.sFullTime();
                Console.WriteLine(sInstantAfter + " After Instant");
                Trace.Write(sLog, Trace.sDateYYYYMMDD() + " LOG CALL", string.Format("[{0}] :After Instant", sInstantAfter));

                Console.WriteLine("=========================================================================");
                Trace.Write(sLog, Trace.sDateYYYYMMDD() + " LOG CALL", "=========================================================================");

                foreach (string sFilename in asFilename)
                {
                    if (!string.IsNullOrEmpty(sFilename))
                    {
                        try
                        {
                            FileInfo fiTemp = new FileInfo(sFilename);
                            List<DataTable> dtXmlTemp = new List<DataTable>();
                            ReadEMSFile(fiTemp.FullName, ref dtXmlTemp);

                            EMS_XML localparam = new EMS_XML(dtXmlTemp);

                            WebServiceTms.EMS_XML param = new WebServiceTms.EMS_XML();

                            param.Attribute = new WebServiceTms.AttributeClass();
                            param.Attribute.sSenderID = localparam.Attribute.sSenderID;
                            param.Attribute.sSenders = localparam.Attribute.sSenders;
                            param.Attribute.sTypes = localparam.Attribute.sTypes;
                            param.Attribute.sVersions = localparam.Attribute.sVersions;

                            param.Data = new WebServiceTms.DataClass();

                            //add tatang
                            WebServiceTms.FilterClass oFilter = new WebServiceTms.FilterClass();
                            oFilter.sFilterFieldName = "Terminal_Init";
                            oFilter.sValues = localparam.Data.Filter.sValues;

                            WebServiceTms.FilterClass[] oFilterArray = new WebServiceTms.FilterClass[1];
                            oFilterArray[0] = oFilter;
                            param.Data.ltFilter = oFilterArray;
                            //

                            WebServiceTms.ColumnValue[] emsparam = new WebServiceTms.ColumnValue[iTotalColumn];
                            EMS_XML.DataClass.TerminalClass.ColumnValue[] localcolumnvalue = new EMS_XML.DataClass.TerminalClass.ColumnValue[iTotalColumn];
                            iTotalColumn = localcolumnvalue.Length;

                            if (param.Attribute.sTypes == "ADD_UPDATE")
                            {
                                localcolumnvalue = localparam.Data.ltTerminal[0].ColumnValueXML.ToArray();

                                for (int i = 0; i < iTotalColumn; i++)
                                {
                                    try
                                    {
                                        if (localcolumnvalue[i].sColName == null)
                                        {
                                            Console.WriteLine("i : {0}", i + " null ");
                                        }
                                        else
                                        {
                                            WebServiceTms.ColumnValue colvalTemp = new WebServiceTms.ColumnValue();
                                            colvalTemp.sColName = localcolumnvalue[i].sColName;
                                            colvalTemp.sColValue = localcolumnvalue[i].sColValue;
                                            emsparam[i] = colvalTemp;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("i : {0}", i + " " + ex.Message);
                                    }
                                }
                                WebServiceTms.TerminalClass terminal = new WebServiceTms.TerminalClass();
                                terminal.ColumnValueXML = emsparam;

                                WebServiceTms.TerminalClass[] terminalArray = new WebServiceTms.TerminalClass[1];
                                terminalArray[0] = terminal;
                                param.Data.ltTerminal = terminalArray;
                            }


                            request.Body._oXML = param;

                            IAsyncResult Asyncresult;

                            string stimestart = Trace.sFullTime();
                            string sInstanttoStart = "Time Before Instant to Start:" + (Convert.ToDateTime(stimestart) - Convert.ToDateTime(sInstantBefore));
                            Console.WriteLine(sInstanttoStart);
                            Trace.Write(sLog, Trace.sDateYYYYMMDD() + " LOG CALL", sInstanttoStart);
                            Trace.Write(sLog, Trace.sDateYYYYMMDD() + " LOG CALL", string.Format("[{0}] :START;Set Time:" + iTimeOut, stimestart));
                            string sRequest = "->" + stimestart.ToString() + " START;Set:" + iTimeOut;
                            Console.WriteLine(sRequest);

                            string sMethod;

                            Action action = () =>
                            {
                                response.Body = serviceTms.sStartXMLWebService(request).Body;
                                WebServiceTms.ResultClass result = response.Body.sStartXMLWebServiceResult.Data.Terminal.Result;
                            };

                            Asyncresult = action.BeginInvoke(null, null);

                            if (Asyncresult.AsyncWaitHandle.WaitOne(iTimeOut))
                            {
                                sMethod = "Successful";
                            }
                            else
                            {
                                sMethod = "Time Out";
                            }

                            string stimeend = Trace.sFullTime();
                            Trace.Write(sLog, Trace.sDateYYYYMMDD() + " LOG CALL", string.Format("[{0}] :END:Time Taken:" + (Convert.ToDateTime(stimeend) - Convert.ToDateTime(stimestart)) + " " + sMethod, stimeend));
                            string sResponse = "->" + stimeend.ToString() + " END Time Taken:" + (Convert.ToDateTime(stimeend) - Convert.ToDateTime(stimestart));
                            Console.WriteLine(sResponse + " " + sMethod);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("->Error:" + sFilename.ToString());
                            Trace.Write(sLog, Trace.sDateYYYYMMDD() + " LOG CALL", string.Format("[{0}] Cannot Call : " + sFilename + ex.Message, Trace.sFullTime()));
                            Trace.Write(sLog, Trace.sDateYYYYMMDD(), string.Format("[{0}] Cannot Call : " + ex.Message, Trace.sFullTime()));
                        }
                        finally
                        {

                        }

                    }

                }



            }
            catch (Exception ex)
            {
                Trace.Write(sLog, Trace.sDateYYYYMMDD(), string.Format("Error : [{0}] ", Trace.sFullTime()) + ex.Message);
            }
            Console.WriteLine("========== FINISH CALL ==========");
            Console.Read();
        }

        static void ReadEMSFile(string sPath, ref List<DataTable> dtXML)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(sPath);
            foreach (DataTable dtTemp in ds.Tables)
            {
                dtXML.Add(dtTemp);
            }
            ds.Dispose();
        }

    }

    class Trace
    {
        static string sPath = Environment.CurrentDirectory;
        //Path.GetDirectoryName(            Assembly.GetExecutingAssembly().GetName().CodeBase);

        public static string sDirName = "";
        public static string sFilename = "Trace_";
        static string sFileType = ".log";

        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static void Write(string sMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFilename + sDateYYYYMMDD() + sFileType;
                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                while (true)
                {
                    if (IsOpenFileAllowed(sFile))
                        break;
                }

                Write2File(sFile, sFullTime() + " " +
                    sMessage,
                    true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static void Write(string sFileName, string sMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFileName;
                sFile = sFile.Contains(sFileType) ? sFile : sFile + sFileType;

                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                while (true)
                {
                    if (IsOpenFileAllowed(sFile))
                        break;
                }
                Write2File(sFile, sMessage, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void Write(string sPath, string sFileName, string sMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFileName;
                sFile = sFile.Contains(sFileType) ? sFile : sFile + sFileType;

                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                while (true)
                {
                    if (IsOpenFileAllowed(sFile))
                        break;
                }
                Write2File(sFile, sMessage, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static bool isTraceFolderExist()
        {
            return Directory.Exists(sPath + sDirName);
        }

        public static bool IsOpenFileAllowed(string sFilename)
        {
            bool bAllow = false;
            FileStream fs = null;
            try
            {
                fs = File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite);
                fs.Close();
                bAllow = true;
            }
            catch (IOException)
            {
                bAllow = false;
            }
            return bAllow;
        }

        /// <summary>
        /// Write Value in string format to text file
        /// </summary>
        /// <param name="sFileName"> string : File name including file path which will be written</param>
        /// <param name="sValue">string : Value to be written to the file</param>
        /// <param name="bNewLine">bool : Value to be using new line on the file</param>
        public static void Write2File(string sFileName, string sValue, bool bNewLine)
        {
            //FileStream oFileStream;
            //StreamWriter oStreamWriter;
            try
            {
                using (FileStream oFileStream = new FileStream(sFileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter oStreamWriter = new StreamWriter(oFileStream))
                    {
                        oStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        if (bNewLine) oStreamWriter.WriteLine(sValue);
                        else oStreamWriter.Write(sValue);
                    }
                    //oStreamWriter.Close();
                    //oStreamWriter.Dispose();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //oFileStream.Close();
            //oFileStream.Dispose();
        }
    }

}
