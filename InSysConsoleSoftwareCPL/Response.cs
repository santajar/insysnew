﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using InSysClass;
using Nito.KitchenSink.CRC;

namespace InSysConsoleSoftwareCPL
{
    class Response : IDisposable
    {
        protected SqlConnection oConn;
        protected bool bCheckAllowInit;
        protected ConnType cType;

        protected string sTerminalId;
        protected string sAppName = null;
        protected string sSoftwareVersion = null;
        protected string sSerialNum = null;
        protected string sCRC = null;
        protected int iIndexApp = 0;
        protected int iTotalIndex = 0;
        protected string sDateTime = null;
        protected bool bInit = false;
        protected bool bStart = false;
        protected bool bComplete = false;
        protected int iPercentage = 0;
        protected int iMaxByte = 400;
        protected bool bEnable48 = false;
        protected bool bEnableCompress = false;
        protected bool bEnableRemoteDownload = false;
        //protected bool bCustom48_1;

        //tambahan untuk bit 48 message = autoinit
        protected string sLastInitTime = null;
        protected string sPABX = null;
        protected string sOSVers = null;
        protected string sKernelEmvVers = null;
        protected string sReaderSN = null;
        protected string sPSAMSN = null;
        protected string sMCSN = null;
        protected string sMemUsg = null;
        protected string sICCID = null;

        public string TerminalId { get { return sTerminalId; } }
        public string AppName { get { return sAppName; } }
        public string SerialNum { get { return sSerialNum; } }
        public bool IsProcessInit { get { return bInit; } }
        public bool IsStartInit { get { return bStart; } }
        public bool IsCompleteInit { get { return bComplete; } }
        public int Percentage { get { return iPercentage; } }

        protected string sProcessCode = "930000";
        public string ProcessCode { get { return sProcessCode; } }
        protected static bool bDebug = true;

        protected byte[] arrbKey = (new UTF8Encoding()).GetBytes("316E67336E316330");
        protected byte[] arrbReceive;

        public Response(string _sConnString, bool _IsCheckAllowInit, ConnType _cType) : this(_sConnString, _IsCheckAllowInit, _cType, false) { }
        public Response(string _sConnString, bool _IsCheckAllowInit, ConnType _cType, bool _bDebug) : this(null, _sConnString, _IsCheckAllowInit, _cType, false) { }
        public Response(byte[] _arrbReceive, string _sConnString, bool _IsCheckAllowInit, ConnType _cType) : this(_arrbReceive, _sConnString, _IsCheckAllowInit, _cType, false) { }
        public Response(byte[] _arrbReceive, string _sConnString, bool _IsCheckAllowInit, ConnType _cType, bool _bDebug)
        {
            try
            {
                //Console.WriteLine("Create Object Response2");
                oConn = new SqlConnection();
                oConn = SQLConnLib.EstablishConnection(_sConnString);
                bCheckAllowInit = _IsCheckAllowInit;
                cType = _cType;
                iMaxByte = iGetMaxByte();
                bDebug = _bDebug;
                arrbReceive = _arrbReceive;
            }
            catch (Exception)
            {
                //Trace.Write("Create Object Error: " + ex.Message);
                //Console.WriteLine("Create Object Error: " + ex.Message);
                oConn.Close();
                oConn.Dispose();
            }
        }

        /// <summary>
        /// Close SQL Connection
        /// </summary>
        public void Dispose()
        {
            oConn.Close();
            oConn.Dispose();
        }

        public byte[] arrbResponse() { return arrbResponse(arrbReceive); }

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public byte[] arrbResponse(byte[] arrbReceive)
        {
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                //Console.WriteLine(string.Format("arrbReceive : {0} ", CommonLib.sByteArrayToHexString(arrbReceive)));
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);
                //Console.WriteLine("Send Message: {0}", sSendMessage);

                if (!string.IsNullOrEmpty(sSendMessage))
                {
                    arrbTemp = new byte[iIsoLen + 2];
                    //arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);

                    sSendMessage = sSendMessage.Replace(" ", ""); // Remove all white space
                    byte[] arrbbuffer = new byte[sSendMessage.Length / 2];
                    for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                    {
                        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);
                    }
                    arrbTemp = arrbbuffer;
                }
            }
            catch (Exception ex)
            {
                Trace.Write("|[RESPONSE]|Error arrbResponse : " + ex.Message);
                //Console.WriteLine("|[RESPONSE]|Error arrbResponse : " + ex.Message);
                //Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTag = null;
            int iErrorCode = -1;
            //Console.WriteLine("Begin Get Response");

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = CommonConsole.iISOLenReceive(sTemp.Substring(0, 4));

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            Trace.Write("sTemp : " + sTemp);

            try
            {
                // parse received ISO
                ISO8583Lib oIsoParse = new ISO8583Lib();
                ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
                //Console.WriteLine("ISO Length: {0}", oIsoMsg.sBit.Length.ToString());
                //Console.WriteLine("ArrbTemp");

                byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);

                //Console.WriteLine("Unpack Iso");
                oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
                string sProcCode = oIsoMsg.sBit[3];
                sTerminalId = oIsoMsg.sBit[41];
                string sBit11 = oIsoMsg.sBit[11];

                // kirim ke sp,...
                try
                {
                    if (oIsoMsg.MTI == "0800")
                    {
                        ProcCode oProcCodeTemp = (ProcCode)int.Parse(sProcCode);
                        DataTable dtTempSoftware;
                        switch (oProcCodeTemp)
                        {
                            case ProcCode.DownloadSoftwareListStartEnd:
                                #region "991000"
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                    {
                                        CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                    }
                                    //get data DataTable
                                    DataTable dtTempData = new DataTable();
                                    dtTempData = dtGetListSoftware(sTerminalId);

                                    SoftwareListStatus terminalSoftwareList = new SoftwareListStatus();
                                    terminalSoftwareList.TerminalID = sTerminalId;

                                    if (dtTempData.Rows.Count > 0)
                                    {
                                        string sContent = dtTempData.Rows[0][2].ToString() + dtTempData.Rows[0][3].ToString();
                                        sContent = sContent + GenerateLength2(dtTempData.Rows.Count.ToString());
                                        foreach (DataRow dr in dtTempData.Rows)
                                        {
                                            sContent = sContent + GenerateLength4(dr[1].ToString().Length.ToString()) + dr[1].ToString();

                                            terminalSoftwareList.SoftwareNameAdd(dr[1].ToString(), dtGetSoftwareData(dr[1].ToString()).Rows.Count);
                                        }
                                        bComplete = true;
                                        sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, false, false, false, "", "", "00");
                                        iErrorCode = 13;

                                        Program.listTerminalSoftwareList.Add(terminalSoftwareList);

                                        ClearSoftwareProgressCPL(sTerminalId);
                                    }
                                    else
                                    {
                                        iErrorCode = 4;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                        //Console.WriteLine(string.Format("TID: {0}, not have software package", sTerminalId));
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    //Trace.Write(string.Format("INVALID TID: {0}", sTerminalId));
                                    //Console.WriteLine(string.Format("INVALID TID: {0}", sTerminalId));
                                }
                                #endregion
                                break;
                            case ProcCode.DownloadSoftwareStartEnd:
                                #region "990000"
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    if (IsAllowRemoteDownload(sTerminalId))
                                    {
                                        if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                        {
                                            //Console.WriteLine("BIT48 NOT NULL");
                                            CommonConsole.GetAppNameSNSoftVer(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum, ref sSoftwareVersion);
                                        }
                                        if (!string.IsNullOrEmpty(oIsoMsg.sBit[57]))
                                        {
                                            //Console.WriteLine("BIT57 NOT NULL");
                                            CommonConsole.GetIndexTotalIndexDate(oIsoMsg.sBit[57], ref iIndexApp, ref iTotalIndex, ref sDateTime);
                                        }

                                        int iIndexSoftware = Program.ltClassSoftware.FindIndex(classSoftwareTemp => classSoftwareTemp.SoftVer == sTerminalId + sSoftwareVersion);

                                        //if (Program.ltClassSoftware.Count == 0 && iIndexSoftware == -1)
                                        if (iIndexSoftware == -1)
                                        {
                                            Trace.Write(string.Format("masuk ke ltClassSoftware dan Find index ke {0}, {1}", iIndexApp, sTerminalId + sSoftwareVersion));

                                            //Console.WriteLine("masuk ke ltClassSoftware dan Find index");
                                            dtTempSoftware = new DataTable();
                                            dtTempSoftware = dtGetSoftwareData(sSoftwareVersion);
                                            if (dtTempSoftware.Rows.Count > 0)
                                            {
                                                ClassSoftware classSoftwareNew = new ClassSoftware(sTerminalId + sSoftwareVersion);
                                                classSoftwareNew.ID = 0;
                                                classSoftwareNew.dtResponses = dtTempSoftware;
                                                //Console.WriteLine("dtResponse Valid");
                                                if (iIndexSoftware > -1)
                                                    Program.ltClassSoftware.RemoveAt(iIndexSoftware);
                                                Program.ltClassSoftware.Add(classSoftwareNew);
                                                iIndexSoftware = Program.ltClassSoftware.FindIndex(classSoftwareTemp => classSoftwareTemp.SoftVer == sTerminalId + sSoftwareVersion);
                                            }
                                            else
                                                Trace.Write("1.dtTempSoftware.Rows.Count = 0");
                                        }
                                        else
                                        {
                                            Program.ltClassSoftware[iIndexSoftware].ID = iIndexApp;

                                            Trace.Write(string.Format("masuk ke ltClassSoftware dan Find index ke {0}, {1}", iIndexApp, sTerminalId + sSoftwareVersion));

                                            if (!isTimeValid(sSoftwareVersion, Program.ltClassSoftware[iIndexSoftware].DateTime))
                                            {
                                                //Console.WriteLine("Update Software Content, cause there is update on the database");
                                                dtTempSoftware = new DataTable();
                                                dtTempSoftware = dtGetSoftwareData(sSoftwareVersion);
                                                if (dtTempSoftware.Rows.Count > 0)
                                                {
                                                    ClassSoftware classSoftware = new ClassSoftware(sTerminalId + sSoftwareVersion);
                                                    classSoftware.ID = 0;
                                                    classSoftware.dtResponses = dtTempSoftware;

                                                    if (iIndexSoftware > -1) Program.ltClassSoftware.RemoveAt(iIndexSoftware);
                                                    Program.ltClassSoftware.Add(classSoftware);
                                                }
                                                else Trace.Write("TIME VALID");
                                            }
                                            //else
                                            //    Console.WriteLine("TIME NOT VALID");
                                        }

                                        string sIndexAppDB = Program.ltClassSoftware[iIndexSoftware].ID.ToString().PadLeft(4, '0');
                                        //string sIndexAppDB = sIndexApp.ToString().PadLeft(4, '0');
                                        string sTotalIndexDB = Program.ltClassSoftware[iIndexSoftware].TotalID.ToString().PadLeft(4, '0');
                                        string sDateTimeDB = Program.ltClassSoftware[iIndexSoftware].DateTime;
                                        //string sDataBit57 = CommonLib.sHexToStringUTF8(sIndexAppDB + sTotalIndexDB + sDateTimeDB.Replace("-", "").Replace(":", "").Replace(" ", ""));
                                        string sDataBit57 = sIndexAppDB + sTotalIndexDB + sDateTimeDB.Replace("-", "").Replace(":", "").Replace(" ", "");
                                        string sDataBit60 = Program.ltClassSoftware[iIndexSoftware].Content;
                                        iErrorCode = 14;

                                        sTag = sSoftwareVersion;
                                        sTempReceive = CommonConsole.sGenerateISOSend(sDataBit60, oIsoMsg, bComplete, false, false, false, "", sDataBit57, "00");

                                        if (Program.ltClassSoftware[iIndexSoftware].isLastCounter)
                                        {
                                            bComplete = true;
                                            iErrorCode = 15;
                                            Program.ltClassSoftware.RemoveAt(iIndexSoftware);
                                            Trace.Write("FINISH " + sTerminalId + sSoftwareVersion);
                                        }
                                    }
                                    else
                                    {
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("NOTALLOWED"), oIsoMsg);
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    Trace.Write(string.Format("INVALID TID: {0}", sTerminalId));
                                    //Console.WriteLine(string.Format("INVALID TID: {0}", sTerminalId));
                                }

                                #endregion
                                break;
                            case ProcCode.DownloadSoftwareCont:
                                #region "990001"
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    if (IsAllowRemoteDownload(sTerminalId))
                                    {
                                        if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                        {
                                            //Console.WriteLine("BIT48 NOT NULL");
                                            CommonConsole.GetAppNameSNSoftVer(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum, ref sSoftwareVersion);
                                        }
                                        if (!string.IsNullOrEmpty(oIsoMsg.sBit[57]))
                                        {
                                            Trace.Write(string.Format("ProcCode.DownloadSoftwareCont GetIndexTotalIndexDate, 57:{0}, {1}", oIsoMsg.sBit[57], sTerminalId + sSoftwareVersion));

                                            //Console.WriteLine("BIT57 NOT NULL");
                                            CommonConsole.GetIndexTotalIndexDate(oIsoMsg.sBit[57], ref iIndexApp, ref iTotalIndex, ref sDateTime);
                                        }

                                        int iIndexSoftware = Program.ltClassSoftware.FindIndex(classSoftwareTemp => classSoftwareTemp.SoftVer == sTerminalId + sSoftwareVersion);

                                        if (Program.ltClassSoftware.Count == 0 || iIndexSoftware == -1)
                                        {
                                            Trace.Write(string.Format("ProcCode.DownloadSoftwareCont masuk ke ltClassSoftware dan Find index ke {0}, {1}", iIndexApp, sTerminalId + sSoftwareVersion));

                                            //Console.WriteLine("masuk ke ltClassSoftware dan Find index");
                                            dtTempSoftware = new DataTable();
                                            dtTempSoftware = dtGetSoftwareData(sSoftwareVersion);
                                            if (dtTempSoftware.Rows.Count > 0)
                                            {
                                                ClassSoftware classSoftwareNew = new ClassSoftware(sTerminalId + sSoftwareVersion);
                                                classSoftwareNew.ID = 0;
                                                classSoftwareNew.dtResponses = dtTempSoftware;
                                                //Console.WriteLine("dtResponse Valid");
                                                if (iIndexSoftware > -1)
                                                    Program.ltClassSoftware.RemoveAt(iIndexSoftware);
                                                Program.ltClassSoftware.Add(classSoftwareNew);
                                                iIndexSoftware = Program.ltClassSoftware.FindIndex(classSoftwareTemp => classSoftwareTemp.SoftVer == sTerminalId + sSoftwareVersion);
                                            }
                                            else Trace.Write("ProcCode.DownloadSoftwareCont 1.dtTempSoftware.Rows.Count = 0");
                                        }
                                        else
                                        {
                                            Program.ltClassSoftware[iIndexSoftware].ID = iIndexApp;

                                            Trace.Write(string.Format("ProcCode.DownloadSoftwareCont masuk ke ltClassSoftware dan Find index ke {0}, {1}", iIndexApp, sTerminalId + sSoftwareVersion));

                                            if (!isTimeValid(sSoftwareVersion, Program.ltClassSoftware[iIndexSoftware].DateTime))
                                            {
                                                //Console.WriteLine("Update Software Content, cause there is update on the database");
                                                dtTempSoftware = new DataTable();
                                                dtTempSoftware = dtGetSoftwareData(sSoftwareVersion);
                                                if (dtTempSoftware.Rows.Count > 0)
                                                {
                                                    ClassSoftware classSoftware = new ClassSoftware(sTerminalId + sSoftwareVersion);
                                                    classSoftware.ID = 0;
                                                    classSoftware.dtResponses = dtTempSoftware;

                                                    if (iIndexSoftware > -1) Program.ltClassSoftware.RemoveAt(iIndexSoftware);
                                                    Program.ltClassSoftware.Add(classSoftware);
                                                }
                                                else Trace.Write("TIME VALID");
                                            }
                                            //else
                                            //    Console.WriteLine("TIME NOT VALID");
                                        }

                                        string sIndexAppDB = Program.ltClassSoftware[iIndexSoftware].ID.ToString().PadLeft(4, '0');
                                        //string sIndexAppDB = sIndexApp.ToString().PadLeft(4, '0');
                                        string sTotalIndexDB = Program.ltClassSoftware[iIndexSoftware].TotalID.ToString().PadLeft(4, '0');
                                        string sDateTimeDB = Program.ltClassSoftware[iIndexSoftware].DateTime;
                                        //string sDataBit57 = CommonLib.sHexToStringUTF8(sIndexAppDB + sTotalIndexDB + sDateTimeDB.Replace("-", "").Replace(":", "").Replace(" ", ""));
                                        string sDataBit57 = sIndexAppDB + sTotalIndexDB + sDateTimeDB.Replace("-", "").Replace(":", "").Replace(" ", "");
                                        string sDataBit60 = Program.ltClassSoftware[iIndexSoftware].Content;

                                        if (Program.ltClassSoftware[iIndexSoftware].isLastCounter)
                                        {
                                            bComplete = true;
                                            iErrorCode = 15;
                                            Program.ltClassSoftware.RemoveAt(iIndexSoftware);
                                            Trace.Write("FINISH " + sTerminalId + sSoftwareVersion);
                                        }
                                        else
                                            iErrorCode = 14;

                                        sTag = sSoftwareVersion;
                                        //Console.WriteLine("TerminalID :" + sTerminalId +"; Software : " + sSoftwareVersion + "; IndexDB : " + sIndexAppDB + "; TotalIndex :" + sTotalIndexDB + "; Index APP :" + sIndexApp + "; Total App :" + sTotalIndex);
                                        sTempReceive = CommonConsole.sGenerateISOSend(sDataBit60, oIsoMsg, bComplete, false, false, false, "", sDataBit57, "00");
                                    }
                                    else
                                    {
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("NOTALLOWED"), oIsoMsg);
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    Trace.Write(string.Format("INVALID TID: {0}", sTerminalId));
                                    //Console.WriteLine(string.Format("INVALID TID: {0}", sTerminalId));
                                }
                                #endregion  
                                break;
                            case ProcCode.SoftwareValidationStartEnd:
                                #region "992000"
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                    {
                                        CommonConsole.GetAppNameSNSoftVerCRC(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum, ref sSoftwareVersion, ref sCRC);
                                    }
                                    else
                                        Trace.Write("992000, Bit48 is NULL");

                                    string sSoftVer = sTerminalId + sSoftwareVersion;

                                    if (!string.IsNullOrEmpty(oIsoMsg.sBit[57]))
                                    {
                                        CommonConsole.GetIndexTotalIndexDate(oIsoMsg.sBit[57], ref iIndexApp, ref iTotalIndex, ref sDateTime);
                                    }
                                    else
                                        Trace.Write("992000, Bit57 is NULL");

                                    if (Program.ltClassSoftware.FindIndex(classInitImageTemp => classInitImageTemp.SoftVer == sTerminalId + sSoftwareVersion) == -1)
                                    //if (Program.ltClassSoftware.FindIndex(classInitImageTemp => classInitImageTemp.SoftVer == sSoftVer) == -1)
                                    {
                                        dtTempSoftware = new DataTable();
                                        dtTempSoftware = dtGetSoftwareData(sSoftwareVersion);
                                        if (dtTempSoftware.Rows.Count > 0)
                                        {
                                            ClassSoftware classSwValidation = new ClassSoftware(sTerminalId + sSoftwareVersion);
                                            classSwValidation.ID = 0;
                                            classSwValidation.dtResponses = dtTempSoftware;

                                            sSoftVer = sTerminalId + sSoftwareVersion;
                                            int iIndexListSoftVer = Program.ltClassSoftware.FindIndex(classInitImageTemp => classInitImageTemp.SoftVer == sSoftVer);

                                            if (iIndexListSoftVer > -1)
                                                Program.ltClassSoftware.RemoveAt(iIndexListSoftVer);

                                            Program.ltClassSoftware.Add(classSwValidation);
                                        }
                                        else
                                            Trace.Write("SOFTWARE NOT FOUND : " + sSoftwareVersion);
                                    }
                                    else
                                        Trace.Write("LIST SOFTWARE IS FOUND : " + sTerminalId + sSoftwareVersion + " " + sSoftVer);

                                    CRC16 calcCrc = new CRC16();
                                    byte[] arrbCrcCount = calcCrc.ComputeHash(CommonLib.HexStringToByteArray(Program.ltClassSoftware[Program.ltClassSoftware.FindIndex(classInitImageTemp => classInitImageTemp.SoftVer == sSoftVer)].GetAllContent));

                                    if (sSoftwareVersion == "VERSION.PAR")
                                    {
                                        SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET RemoteDownload = 0 WHERE TerminalID = '{0}'", sTerminalId), oConn);
                                        oCmd.ExecuteNonQuery();
                                        //    dtTempSoftware = dtGetSoftwareData("VERSION.PAR");
                                        //    string sNewCRC = dtTempSoftware.Rows[0][2].ToString();
                                        //    arrbCrcCount = calcCrc.ComputeHash(CommonLib.HexStringToByteArray(sNewCRC));
                                        //    //Library.WriteErrorLog(string.Format("Ini GetData Software Version: {0}", sSoftwareVersion));
                                        //    //Library.WriteErrorLog(string.Format("ini Crc Software Version: {0}", sNewCRC));
                                    }

                                    string sContentCRC = CommonLib.sByteArrayToHexString(arrbCrcCount);
                                    string sBit39;

                                    Trace.Write(string.Format("CRC msg : {0} CRC count : {1}, {2}", sCRC, sContentCRC, sSoftVer));

                                    if (sContentCRC.Replace(" ", "").ToLower() == sCRC.ToLower())
                                    {
                                        //sTag = "Success";
                                        sBit39 = "00";

                                        // sTerminalId + sSoftwareVersion
                                        SoftwareListStatus tempSoftwareListStatus = Program.listTerminalSoftwareList.Find(temp => temp.TerminalID == sTerminalId);
                                        if (tempSoftwareListStatus != null)
                                        {
                                            tempSoftwareListStatus.SoftwareNameUpdate(sSoftwareVersion, false);
                                            iIndexApp = tempSoftwareListStatus.SoftwareNameIndex;
                                        }
                                    }
                                    else
                                    {
                                        sTag = "Failed";
                                        sBit39 = "FF";
                                    }

                                    iErrorCode = 16;
                                    sTempReceive = CommonConsole.sGenerateISOSend("", oIsoMsg, bComplete, false, false, false, "", "", sBit39);

                                    if (sBit39 == "00")
                                    {
                                        UpdateTempSoftwareList(sTerminalId, sSoftwareVersion);
                                    }

                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                    Trace.Write(string.Format("INVALID TID: {0}", sTerminalId));
                                    //Console.WriteLine(string.Format("INVALID TID: {0}", sTerminalId));
                                }
                                #endregion
                                break;
                        }
                    }
                    else
                    {
                        iErrorCode = 5;
                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDMTI"), oIsoMsg);
                    }
                    if (!string.IsNullOrEmpty(sTempReceive))
                    {
                        // tambahkan total length
                        _iIsoLenResponse = sTempReceive.Length / 2;
                        sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                        if (bDebug)
                            Trace.Write(string.Format("|[RESPONSE]|{0}|ErrorCode : {2}|sTempReceive : {1}", sTerminalId, sTempReceive, iErrorCode));
                        //Console.WriteLine(string.Format("|[RESPONSE]|{0}|ErrorCode : {2}|sTempReceive : {1}", sTerminalId, sTempReceive, iErrorCode));

                        if (iErrorCode == 1 && sProcCode == "990000")
                        {
                            SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET AllowDownload = 0 WHERE TerminalID = '{0}'", sTerminalId), oConn);
                            oCmd.ExecuteNonQuery();
                        }
                        //tampilkan progress pada console
                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag) + "     [" + iIndexApp + "] --> [" + iTotalIndex + "]");
                        //InsertProgressDetail(sTerminalId, sAppName, sTag, sSerialNum, Convert.ToInt32(sIndexApp));
                        InsertProgress(sTerminalId, sAppName, sTag, sSerialNum, Convert.ToInt32(iIndexApp));
                    }
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                    //    ex.ToString(), sTemp);
                    Trace.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1} | sTerminalID : {2} | sProccode : {3}", ex.ToString(), sTemp, sTerminalId, sProcCode));
                    //Console.WriteLine(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1} | sTerminalID : {2} | sProccode : {3}", ex.ToString(), sTemp, sTerminalId, sProcCode));
                }
            }
            catch (Exception ex)
            {
                Trace.Write("Parsing Error: " + ex.Message);
                //Console.WriteLine("Parsing Error: " + ex.Message);
            }
            return sTempReceive;
        }

        private void ClearSoftwareProgressCPL(string sTerminalId)
        {
            if (oConn.State == ConnectionState.Closed) oConn.Open();
            using (SqlCommand oCmdAuditInitSoftware = new SqlCommand("spSoftwareProgressClearCPL", oConn))
            {
                oCmdAuditInitSoftware.CommandType = CommandType.StoredProcedure;
                oCmdAuditInitSoftware.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmdAuditInitSoftware.ExecuteNonQuery();
            }
        }

        private void InsertProgress(string _sTerminalID, string _sAppName, string _sAppNameDownload, string _sSerialNumber, int iCounterApp)
        {
            //if (iCounterApp == 1)
            // {
            try
            {

                if (oConn.State == ConnectionState.Closed) oConn.Open();
                using (SqlCommand oCmdAuditInitSoftwareInsert = new SqlCommand("spSoftwareProgressInsertCPL", oConn))
                {
                    oCmdAuditInitSoftwareInsert.CommandType = CommandType.StoredProcedure;
                    //oCmdAuditInitSoftwareInsert.CommandTimeout = 30;
                    //oCmdAuditInitSoftwareInsert.Parameters.Add("@sId", SqlDbType.Int).Value = 1;
                    oCmdAuditInitSoftwareInsert.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                    oCmdAuditInitSoftwareInsert.Parameters.Add("@sSoftware", SqlDbType.VarChar).Value = _sAppName;
                    oCmdAuditInitSoftwareInsert.Parameters.Add("@sSoftwareDownload", SqlDbType.VarChar).Value = _sAppNameDownload;
                    oCmdAuditInitSoftwareInsert.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = _sSerialNumber;
                    //oCmdAuditInitSoftwareInsert.Parameters.Add("@fIndex", SqlDbType.Int).Value = iCounterApp;
                    oCmdAuditInitSoftwareInsert.Parameters.Add("@fIndex", SqlDbType.Int).Value = iCounterApp + 1;
                    // oCmdAuditInitSoftwareInsert.Parameters.Add("@iBuildNumber", SqlDbType.Int).Value = iBuildNumber;
                    oCmdAuditInitSoftwareInsert.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                //Console.WriteLine(ex.StackTrace, string.Format("Insert Progress 1"));
                //Console.WriteLine(ex.StackTrace.ToString());

            }
        }

        private void UpdateTempSoftwareList(string _sTerminalId, string _sSoftwareVersion)
        {
            SqlCommand oCmd = new SqlCommand(CommonSP.sSPTempSoftwareListUpdateDelete, oConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalId;
            oCmd.Parameters.Add("@sSoftwareName", SqlDbType.VarChar).Value = _sSoftwareVersion;
            if (oConn.State == ConnectionState.Closed) oConn.Open();
            oCmd.ExecuteNonQuery();
        }

        private string GenerateLength4(string sData)
        {
            string sReturn = "";
            if (sData.Length == 0)
                sReturn = "0000" + sData;
            else if (sData.Length == 1)
                sReturn = "000" + sData;
            else if (sData.Length == 2)
                sReturn = "00" + sData;
            else if (sData.Length == 3)
                sReturn = "0" + sData;
            else
                sReturn = sData;
            return sReturn;
        }

        private bool isTimeValid(string sSoftwareVersion, string sDateTime)
        {

            bool bReturn = false;

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT CONVERT(varchar(23), UploadTime,120) AS UploadTime FROM tbAppPackageEDCList WHERE AppPackageName = '{0}'", sSoftwareVersion), oConn);
            if (oConn.State == ConnectionState.Closed) oConn.Open();
            DataTable dtTemp = new DataTable();
            new SqlDataAdapter(oCmd).Fill(dtTemp);


            string sDBDateTime = dtTemp.Rows[0][0].ToString();
            Trace.Write("DBDateTime : {0} ", sDBDateTime);
            //Console.WriteLine("DBDateTime : {0}", sDateTime);

            if (sDBDateTime == sDateTime)
                bReturn = true;
            else
                bReturn = false;

            Trace.Write(string.Format("sSoftwareVersion='{0}', sDateTime='{1}', sDBDateTime='{2}', bReturn={3}", sSoftwareVersion, sDateTime, sDBDateTime, bReturn));

            return bReturn;
        }

        private DataTable dtGetSoftwareData(string sSoftwareVersion)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPSoftwareGetData, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sSoftwareName", SqlDbType.VarChar).Value = sSoftwareVersion;
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                return dtTemp;
            }
        }

        private string GenerateLength2(string sData)
        {
            string sReturn = "";
            if (sData.Length != 2)
            {
                sReturn = "0" + sData;
            }
            else
                sReturn = sData;
            return sReturn;
        }

        private DataTable dtGetListSoftware(string sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPSoftwareListGetData, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                return dtTemp;
            }
        }

        protected string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP || cType == ConnType.Modem)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            //sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Download Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Download Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                case 6:
                    sTemp = "Application Not Allow";
                    break;
                case 7:
                    sTemp = "Processing Echo";
                    break;
                case 8:
                    sTemp = "Processing Download " + sDetailProcess(sTag);
                    break;
                case 9:
                    sTemp = "Download Profile COMPLETE";
                    break;
                case 10:
                    sTemp = "Processing Autoinit Phase-1";
                    break;
                case 11:
                    sTemp = "Processing Autoinit Phase-3";
                    break;
                case 12:
                    sTemp = "Processing Init FLAZZ";
                    break;
                case 13:
                    sTemp = "Download Software List Complete.";
                    break;
                case 14:
                    sTemp = "Process Download Software " + sTag;
                    break;
                case 15:
                    sTemp = "Download Software " + sTag + " Complete.";
                    break;
                case 16:
                    sTemp = "Validate Software " + sTag;
                    break;
                case 17:
                    sTemp = "Software Package Not Set.";
                    break;
                default:
                    sTemp = "Unknown Error Code : " + iCode.ToString();
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
                case "AI":
                    sTemp = "AID";
                    break;
                case "PK":
                    sTemp = "CAPK";
                    break;
                case "PP":
                    sTemp = "PinPad";
                    break;
                case "GP":
                    sTemp = "GPRS";
                    break;
                default:
                    sTemp = "";
                    break;
            }
            return sTemp;
        }

        /// <summary>
        /// Get List Content Profile from database
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <param name="_sContent">string : Content</param>
        /// <returns>List Content</returns>
        protected List<ContentProfile> ltGetContentProfile(string _sTerminalId, ref string _sContent, ProcCode oProcCodeTemp)
        {
            List<ContentProfile> ltCPTemp = new List<ContentProfile>();
            string sQuery;
            if (oProcCodeTemp == ProcCode.DownloadProfileStartEnd)
                sQuery = CommonSP.sSPProfileTextFullTableDLProfile;
            else
                sQuery = CommonSP.sSPProfileTextFullTable;
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);

                string[] arrsMaskedTag = arrsGetMaskedTag(_sTerminalId);

                if (dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow rowTemp in dtTemp.Rows)
                    {
                        string sTag = rowTemp["Tag"].ToString();
                        string sValue = rowTemp["TagValue"].ToString();
                        try
                        {
                            if (arrsMaskedTag.Length > 0 && arrsMaskedTag.Contains(sTag))
                                sValue = EncryptionLib.Decrypt3DES(CommonLib.sHexToStringUTF8(sValue), arrbKey);

                        }
                        catch (Exception)
                        {
                            sValue = rowTemp["TagValue"].ToString();
                        }

                        string sContent;
                        if (sTag != "AE37" && sTag != "AE38" && sTag != "TL11" && sTag.Substring(0, 2) != "PK")
                            sContent = string.Format("{0}{1:00}{2}", sTag, sValue.Length, sValue);
                        else
                        {
                            if (sTag == "AE37" || sTag == "AE38")
                                sContent = string.Format("{0}{1:000}{2}", sTag, sValue.Length, sValue);
                            else if (sTag == "PK04" && sTag == "PK06")
                                sContent = string.Format("{0}{1}{2}",
                                    CommonLib.sStringToHex(sTag),
                                    CommonLib.sStringToHex(string.Format("{0:000}", sValue.Length)),
                                    CommonLib.sStringToHex(sValue));
                            else
                                sContent = string.Format("{0}{1}{2}",
                                    sTag,
                                    string.Format("{0:000}", rowTemp["TagLength"]),
                                    sValue);
                            //sContent = string.Format("{0}{1}{2}",
                            //        CommonLib.sStringToHex(sTag),
                            //        CommonLib.sStringToHex(string.Format("{0:000}", rowTemp["TagLength"])),
                            //        sValue);
                        }
                        _sContent += sContent;
                        ContentProfile oCPTemp = new ContentProfile(sTag.Substring(0, 2), sContent);
                        int iIndexSearch = -1;
                        if (ltCPTemp.Count == 0)
                            ltCPTemp.Add(oCPTemp);
                        else
                            if ((iIndexSearch = ltCPTemp.FindIndex(oCPSearch => oCPSearch.Tag == oCPTemp.Tag)) > -1)
                        {
                            ContentProfile oCPSearchResult = ltCPTemp[iIndexSearch];
                            ContentProfile oCPNew = new ContentProfile(oCPTemp.Tag, oCPSearchResult.Content + oCPTemp.Content);
                            ltCPTemp[iIndexSearch] = oCPNew;
                        }
                        else
                            ltCPTemp.Add(oCPTemp);
                    }
                }
            }
            return ltCPTemp;
        }

        /// <summary>
        /// Check TerminalID is masking or not
        /// </summary>
        /// <param name="_sTerminalId"> string : Terminal ID</param>
        /// <returns>List Masking Tag</returns>
        protected string[] arrsGetMaskedTag(string _sTerminalId)
        {
            List<string> ltTag = new List<string>();
            string sQuery = string.Format("SELECT * FROM tbItemList WHERE DatabaseID IN (SELECT DatabaseID FROM tbProfileTerminalList WHERE TerminalID='{0}') AND vMasking=1", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                new SqlDataAdapter(oCmd).Fill(dtTemp);
                if (dtTemp != null && dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow row in dtTemp.Rows)
                        ltTag.Add(row["Tag"].ToString());
                }
            }
            return ltTag.ToArray();
        }

        /// <summary>
        /// Create File Compress
        /// </summary>
        /// <param name="sFilename">string : File Name</param>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>Bool : true if Success</returns>
        protected bool IsNetCompressSuccess(string sFilename, string sTerminalId)
        {
            bool bReturn = false;
            if (File.Exists(sFilename))
            {
                try
                {
                    FileInfo fi = new FileInfo(sFilename);
                    using (FileStream inFile = fi.OpenRead())
                    {
                        string sGzipFile = sFilename + ".gz";
                        if (File.Exists(sGzipFile))
                            File.Delete(sGzipFile);
                        using (FileStream outFile = File.Create(sFilename + ".gz"))
                        {
                            using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                            {
                                byte[] buffer = new byte[fi.Length];
                                inFile.Read(buffer, 0, Convert.ToInt32(fi.Length));
                                compress.Write(buffer, 0, Convert.ToInt32(fi.Length));
                                bReturn = true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    //MessageBox.Show("NetCompress " + ex.Message);
                }
            }
            return bReturn;
        }

        /// <summary>
        /// Check Allow Compress
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : true if init Compress</returns>
        protected bool IsAllowCompress(string sTerminalId)
        {
            if (oConn.State != ConnectionState.Open) oConn.Open();
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsCompressInit('{0}')", sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        /// <summary>
        /// Check Allow Remote Download
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : True is allow remote Download</returns>
        protected bool IsAllowRemoteDownload(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsRemoteDownloadAllowed('{0}')", sTerminalId);
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp.Load(oCmd.ExecuteReader());
                    if (dtTemp.Rows.Count > 0)
                        isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
                }
            }
            catch (Exception)
            {

            }
            //Console.WriteLine("AllowRemoteDownload Valid");
            return isAllow;
        }

        /// <summary>
        /// Get Max Byte from Database
        /// </summary>
        /// <returns>int : Max Byte</returns>
        protected int iGetMaxByte()
        {
            if (oConn.State != ConnectionState.Open) oConn.Open();
            int iMaxLength = 400;
            string sQuery = string.Format("SELECT dbo.iPacketLength() [MaxByte]");
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        iMaxLength = int.Parse(oRead["MaxByte"].ToString());
                }
            }
            return iMaxLength;
        }

        /// <summary>
        /// Create Temporary Table for Init Data
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <param name="isCompress">bool: True if Compress</param>
        /// <returns>Data Table </returns>
        protected DataTable dtGetInitTemp(string _sTerminalId, ref bool isCompress, ProcCode oProcCodeTemp)
        {
            DataTable dtInitTemp = new DataTable();
            dtInitTemp.Columns.Add("TerminalId", typeof(string));
            dtInitTemp.Columns.Add("Content", typeof(string));
            dtInitTemp.Columns.Add("Tag", typeof(string));
            dtInitTemp.Columns.Add("Flag", typeof(bool));

            string sContent = null;
            List<ContentProfile> _ltCPTemp = ltGetContentProfile(_sTerminalId, ref sContent, oProcCodeTemp);
            if (_ltCPTemp != null && _ltCPTemp.Count > 0)
            {
                isCompress = bEnableCompress;
                string sTag;
                int iOffset = 0;
                int iFlag = 0;
                if (bEnableCompress)
                {
                    #region "GZ part"
                    sTag = "GZ";
                    iOffset = 0;
                    string sContentCompressed = null;
                    // write to file locally and compress
                    if (!Directory.Exists(Environment.CurrentDirectory + @"\ZIP"))
                        Directory.CreateDirectory(Environment.CurrentDirectory + @"\ZIP");
                    string sFilename = Environment.CurrentDirectory + @"\ZIP\" + _sTerminalId;

                    if (File.Exists(sFilename))
                        File.Delete(sFilename);
                    CommonLib.Write2File(sFilename, sContent, false);

                    if (IsNetCompressSuccess(sFilename, _sTerminalId))
                    {
                        FileStream fsRead = new FileStream(sFilename + ".gz", FileMode.Open);
                        byte[] arrbContent = new byte[fsRead.Length];
                        fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
                        fsRead.Close();
                        sContentCompressed = CommonLib.sByteArrayToHexString(arrbContent).Replace(" ", "");
                        while (!string.IsNullOrEmpty(sContentCompressed) && iOffset < sContentCompressed.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            rows["Content"] = sContentCompressed.Substring(iOffset,
                                iOffset + (iMaxByte * 2) <= sContentCompressed.Length ?
                                (iMaxByte * 2) :
                                sContentCompressed.Length - iOffset);
                            dtInitTemp.Rows.Add(rows);
                            iOffset += (iMaxByte * 2);
                        }
                    }
                    #endregion
                }
                else
                {
                    iFlag = 0;
                    iOffset = 0;
                    #region "non-GZ part"
                    foreach (ContentProfile oCPTemp in _ltCPTemp)
                    {
                        sTag = oCPTemp.Tag;
                        sContent = oCPTemp.Content;
                        iOffset = 0;
                        //while (iOffset <= sContent.Length), error, row menjadi null ketika value nya kosong.
                        while (iOffset < sContent.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            int iOffsetHeader = sContent.IndexOf(sTag + "01", iOffset + 1);
                            int iLength = 0;

                            if (sTag != "AI" && sTag != "PK")
                            {
                                if (iOffset + iMaxByte <= sContent.Length)
                                    if (iOffsetHeader > 0)
                                        if (iOffset + iMaxByte <= iOffsetHeader)
                                            iLength = iMaxByte;
                                        else
                                            iLength = iOffsetHeader - iOffset;
                                    else
                                        iLength = iMaxByte;
                                else
                                    iLength = sContent.Length - iOffset;
                            }
                            else
                            {
                                if (iOffset + iMaxByte <= sContent.Length || iOffsetHeader > 0)
                                    if (iOffsetHeader > 0)
                                        if (iOffset + iMaxByte <= iOffsetHeader)
                                            iLength = iMaxByte;
                                        else
                                            iLength = iOffsetHeader - iOffset;
                                    else
                                        iLength = iMaxByte;
                                else
                                    iLength = sContent.Length - iOffset;
                            }

                            rows["Content"] = sContent.Substring(iOffset, iLength);

                            iOffset += iLength;
                            dtInitTemp.Rows.Add(rows);
                        }
                    }
                    #endregion
                }
            }
            return dtInitTemp;
        }

        /// <summary>
        /// Determine Application Allow Init
        /// </summary>
        /// <param name="_sTerminalId"> string : Terminal ID</param>
        /// <param name="_sAppName">string : Application Name</param>
        /// <returns>bool: true if Allow Init</returns>
        protected bool IsApplicationAllowInit(string _sTerminalId, string _sAppName)
        {
            if (oConn.State != ConnectionState.Open) oConn.Open();
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsApplicationAllowInit('{0}','{1}')", _sTerminalId, _sAppName);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        /// <summary>
        /// Determine Init Allow
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <returns>bool: true is allow</returns>        
        protected bool IsInitAllowed(string _sTerminalId)
        {
            if (oConn.State != ConnectionState.Open) oConn.Open();
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsInitAllowed('{0}')", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        //BEGIN
        //string sTIDRemoteDownload;
        private Boolean CheckTIDRemoteDownload(string sTIDRemoteDownload)
        {
            sTIDRemoteDownload = sTIDRemoteDownload.ToString();

            if (oConn.State != ConnectionState.Open) oConn.Open();
            //string sql = "select TerminalID from tbListTIDRemoteDownload WHere TerminalID";
            SqlCommand command = new SqlCommand("spTIDRemoteDownload", oConn);
            command.Parameters.AddWithValue("@sTerminalID", sTerminalId);
            //SqlCommand command = new SqlCommand(sql, oConn);
            command.ExecuteNonQuery();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                if (reader[0].ToString() == sTIDRemoteDownload)
                {
                    reader.Close();
                    return true;
                }
            }
            oConn.Close();
            reader.Close();
            return false;
        }

        //LAST
        /// <summary>
        /// Determine Valid Terminal ID
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <returns>Bool : true if valid</returns>
        protected bool IsValidTerminalID(string _sTerminalId)
        {
            if (oConn.State != ConnectionState.Open) oConn.Open();
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsValidTerminalID('{0}')", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            //Console.WriteLine("TerminalID Valid");
            return isAllow;
        }
    }
}
