﻿using System.Linq;
using System.Data;
using System;
using System.Collections.Generic;

namespace InSysConsoleSoftwareCPL
{
    class ClassSoftware
    {
        protected string sSoftVer;
        protected string sContent;
        protected string sDateTime;
        protected int iID;
        protected DataTable dtSoftwareResponse = new DataTable();

        public ClassSoftware() { }
        public ClassSoftware(string _sSoftVer)
        {
            sSoftVer = _sSoftVer;
        }

        public string SoftVer { get { return sSoftVer; } set { sSoftVer = value; } }
        public int ID { get { return iID; } set { iID = value; } }

        public string Content { get { return sGetContent(iID); } }
        public string DateTime { get { return sGetDateTime(); } }
        public int TotalID { get { return sGetTotalID(); } }
        public bool isLastCounter { get { return sGetLastCounter(iID); } }
        public string GetAllContent { get { return sGetAllContent(); } }

        private string sGetAllContent()
        {
            string sReturn = "";
            int iIndexContent = 0;
            int iTotalIndex = dtSoftwareResponse.Rows.Count;

            while (iIndexContent < iTotalIndex)
            {
                sReturn += dtSoftwareResponse.Select(string.Format("Id = {0}", iIndexContent))[0]["AppPackageContent"].ToString();
                iIndexContent += 1;
            }

            return sReturn;
        }

        private int sGetTotalID()
        {
            int iTotalCounter = 0;
            try
            {
                iTotalCounter = int.Parse(dtSoftwareResponse.Rows.Count.ToString());
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Total Counter");
                //Console.WriteLine(ex.Message);
            }
            return iTotalCounter;
        }

        public DataTable dtResponses
        {
            set { dtSoftwareResponse = value; }
        }

        //public string sGetSoftVer()
        //{ 
        //   string sTempSoftVer="";
        //   try
        //   {
        //       sTempSoftVer = dtSoftwareResponse.Select(string.Format("Id = {0}", iID))[0]["AppPackageName"].ToString();
        //   }
        //   catch (Exception ex)
        //   {
        //       Console.WriteLine("Software Version");
        //       Console.WriteLine(ex.Message);
        //   }
        //   return sTempSoftVer;

        //}

        public int iGetID()
        {
            int iTempIndex = 0;
            try
            {
                iTempIndex = int.Parse(dtSoftwareResponse.Select(string.Format("AppPackageName = {0}", sSoftVer))[0]["Id"].ToString());
            }
            catch (Exception)
            {
                //Console.WriteLine("Index");
                //Console.WriteLine(ex.Message);
            }
            return iTempIndex;

        }

        public string sGetContent(int iID)
        {
            string sTempContent = "";
            try
            {
                sTempContent = dtSoftwareResponse.Select(string.Format("Id = {0}", iID))[0]["AppPackageContent"].ToString();
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Content");
                //Console.WriteLine(ex.Message);
            }
            return sTempContent;

        }

        public string sGetDateTime()
        {
            string sTempDatetime = "";
            try
            {
                sTempDatetime = dtSoftwareResponse.Select(string.Format("Id = {0}", iID))[0]["UploadTime"].ToString();
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Datetime");
                //Console.WriteLine(ex.Message);
            }
            return sTempDatetime;

        }
        private bool sGetLastCounter(int iCounterID)
        {
            bool isLastCounter = false;
            try
            {
                if (dtSoftwareResponse.Rows.Count - 1 == iCounterID)
                    isLastCounter = true;
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Last Counter");
                //Console.WriteLine(ex.Message);
            }
            return isLastCounter;
        }

    }

    class SoftwareListStatus : IDisposable
    {
        // Flag: Has Dispose already been called?
        bool disposed = false;

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }

        ~SoftwareListStatus()
        {
            Dispose(false);
        }

        public string TerminalID { get; set; }

        List<SoftwareDownloadStatus> listSoftwareStatus = new List<SoftwareDownloadStatus>();

        public void SoftwareNameAdd(string SoftwareName, int TotalIndex)
        {
            SoftwareDownloadStatus softwareStatus = new SoftwareDownloadStatus();
            softwareStatus.SoftwareName = SoftwareName;
            softwareStatus.DownloadStatus = true;
            softwareStatus.TotalIndex = TotalIndex;
            listSoftwareStatus.Add(softwareStatus);
        }

        public bool SoftwareNameUpdate(string SoftwareName, bool NewStatus)
        {
            SoftwareDownloadStatus softwareDownloadStatus = listSoftwareStatus.FirstOrDefault(temp => temp.SoftwareName == SoftwareName);
            if (softwareDownloadStatus == null)
                return false;
            else
            {
                softwareDownloadStatus.DownloadStatus = NewStatus;
                return true;
            }
        }

        public bool SoftwareNameStatus(string SoftwareName)
        {
            SoftwareDownloadStatus softwareDownloadStatus = listSoftwareStatus.FirstOrDefault(temp => temp.SoftwareName == SoftwareName);
            if (softwareDownloadStatus != null)
            {
                return softwareDownloadStatus.DownloadStatus;
            }
            else return false;
        }

        public bool IsSoftwareDownloadFinish()
        {
            if (PercentageSoftwareDownloadFinish() < 100) return false;
            else return true;
        }

        public float PercentageSoftwareDownloadFinish()
        {
            int iTotal = listSoftwareStatus.Count;
            int iFinish = 0;
            int iInProgress = 0;

            foreach (SoftwareDownloadStatus softwareDownloadStatus in listSoftwareStatus)
                if (softwareDownloadStatus.DownloadStatus == true)
                    iInProgress++;
                else iFinish++;

            return iFinish / iTotal * 100;
        }

        public int SoftwareNameIndex
        {
            get
            {
                int Total = 0;
                foreach (SoftwareDownloadStatus softwareDownloadStatus in listSoftwareStatus)
                    if (softwareDownloadStatus.DownloadStatus == false)
                        Total += softwareDownloadStatus.TotalIndex;
                return Total;
            }

        }
    }

    class SoftwareDownloadStatus
    {
        public string SoftwareName { get; set; }
        public bool DownloadStatus { get; set; }
        public int TotalIndex { get; set; }
    }
}
