﻿using System;
using System.Globalization;
using System.IO;
using InSysClass;

namespace InSysConsoleSoftwareCPL
{
    public enum ConnType
    {
        TcpIP = 1,
        Serial = 2,
        Modem = 3
    }

    public enum ProcCode
    {
        ProfileStartEnd = 930000,
        ProfileCont = 930001,
        AutoProfileStartEnd = 960000,
        AutoProfileCont = 960001,
        Flazz = 940000,
        AutoPrompt = 950000,
        AutoCompletion = 970000,
        TerminalEcho = 800000,
        DownloadProfileStartEnd = 980000,
        DownloadProfileCont = 980001,

        DownloadSoftwareListStartEnd = 991000,
        DownloadSoftwareStartEnd = 990000,
        DownloadSoftwareCont = 990001,
        SoftwareValidationStartEnd = 992000,
    }
    
    class CommonConsole
    {
        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sTimeHHMMSS()
        {
            return DateTime.Now.ToString("hhmmss", ciUSFormat);
        }

        public static string sDateMMddyy()
        {
            return DateTime.Now.ToString("MMddyy", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = CommonConsole.sFullTime();
            Console.BufferWidth = 1000; //untuk menangani case spasi kosong Init setelah remote 
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }

        public static string sGenerateISOSend(string sContent, ISO8583_MSGLib oIsoTemp,
            bool isLast, 
            bool isEnable48, bool isEnableCompress, bool isEnableRemoteDownload,
            string sAppName, string sBit57, string sbit39)
        {
            string sIsoResponse;
            ISO8583Lib oIso = new ISO8583Lib();
            oIsoTemp.MTI = "0810";
            oIsoTemp.sBinaryBitMap = "0010000000100000000000010000000000000010100000000000000000000000";
            ProcCode tempProcCode = (ProcCode)int.Parse(oIsoTemp.sBit[3].ToString());
            switch (tempProcCode)
            {
                //case ProcCode.ProfileStartEnd:
                //case ProcCode.ProfileCont:
                //    oIso.SetField_Str(3, ref oIsoTemp,
                //        isLast ? ProcCode.ProfileStartEnd.GetHashCode().ToString() : ProcCode.ProfileCont.GetHashCode().ToString());
                //    break;
                //case ProcCode.DownloadProfileStartEnd:
                //case ProcCode.DownloadProfileCont:
                //    oIso.SetField_Str(3, ref oIsoTemp,
                //        isLast ? ProcCode.DownloadProfileStartEnd.GetHashCode().ToString() : ProcCode.DownloadProfileCont.GetHashCode().ToString());
                //    break;

                //case ProcCode.AutoProfileStartEnd:
                //case ProcCode.AutoProfileCont:
                //    oIso.SetField_Str(3, ref oIsoTemp,
                //        isLast ? ProcCode.AutoProfileStartEnd.GetHashCode().ToString() : ProcCode.AutoProfileCont.GetHashCode().ToString());
                //    break;
                //case ProcCode.TerminalEcho:
                //    oIso.SetField_Str(3, ref oIsoTemp, ProcCode.TerminalEcho.GetHashCode().ToString());
                //    break;


                case ProcCode.DownloadSoftwareListStartEnd:
                    oIso.SetField_Str(3, ref oIsoTemp, ProcCode.DownloadSoftwareListStartEnd.GetHashCode().ToString());
                    oIso.SetField_Str(60, ref oIsoTemp, sContent);
                    break;
                case ProcCode.DownloadSoftwareStartEnd:
                case ProcCode.DownloadSoftwareCont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        isLast ? ProcCode.DownloadSoftwareStartEnd.GetHashCode().ToString() : ProcCode.DownloadSoftwareCont.GetHashCode().ToString());
                    oIso.SetField_Str(57, ref oIsoTemp, sBit57);
                    oIso.SetField_Str(60, ref oIsoTemp, sContent);
                    break;
                case ProcCode.SoftwareValidationStartEnd:
                    oIso.SetField_Str(3, ref oIsoTemp, ProcCode.SoftwareValidationStartEnd.GetHashCode().ToString());
                    break;
            }
            oIso.SetField_Str(12, ref oIsoTemp, string.Format("{0}", DateTime.Now.ToString("hhmmss", ciINAFormat)));
            oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));
            
            //if (isEnable48)
            //{
            //    oIso.SetField_Str(12, ref oIsoTemp, string.Format("{0}", DateTime.Now.ToString("hhmmss", ciINAFormat)));
            //    if (tempProcCode == ProcCode.TerminalEcho)
            //        oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));
            //    else
            //        if (string.IsNullOrEmpty(sAppName) ||
            //            Program.dtISO8583.Select(string.Format("SoftwareName='{0}' AND StandartISO8583=1", sAppName)).Length == 0)
            //            oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMddyy}", DateTime.Now));
            //        else
            //            oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));

            //    if (string.IsNullOrEmpty(sAppName) ||
            //            Program.dtISO8583.Select(string.Format("SoftwareName='{0}' AND SentBit48=1", sAppName)).Length > 0)
            //        oIso.SetField_Str(48, ref oIsoTemp, CommonLib.sStringToHex(oIsoTemp.sBit[48]));
            //}

            oIso.SetField_Str(39, ref oIsoTemp, sbit39);

            switch (tempProcCode)
            {
                case ProcCode.DownloadSoftwareListStartEnd:
                    sIsoResponse = oIso.PackToISOHexSoftwareListDownload(oIsoTemp);
                    break;
                default:
                    sIsoResponse = oIso.PackToISOHex(oIsoTemp, 3);
                    break;
            }
            //sIsoResponse = oIso.PackToISOHexSoftwareListDownload(oIsoTemp);
            return sIsoResponse;
        }
        
        public static string sGenerateISOError(string sContent, ISO8583_MSGLib oIsoTemp)
        {
            return string.Format("{0}{1}{2}{3}", oIsoTemp.sTPDU, oIsoTemp.sOriginAddr, oIsoTemp.sDestNII, sContent);
        }

        public static void GetCounterAppDLLastDLBuildNumber(string sBit57, ref int _iCounterApp, ref string _sAppNameDownload, ref string _sLastDownload, ref int _iBuildNumber)
        {
            string sHexBit57 = CommonLib.sStringToHex(sBit57).Replace(" ", "");
            //Console.WriteLine(string.Format("Bit57 From EDC : {0}", sHexBit57));
            _iCounterApp = int.Parse(sHexBit57.Substring(0, 4));
            _sLastDownload = sHexBit57.Substring(4, 10);
            int iLenApp = int.Parse(sHexBit57.Substring(14, 4));
            if (iLenApp > 0)
            {
                _sAppNameDownload = CommonLib.sHexToStringUTF7(sHexBit57.Substring(18, iLenApp * 2));
                _iBuildNumber = int.Parse(sHexBit57.Substring(18 + iLenApp * 2, 4));
            }

        }

        public static void GetAppNameSN(string sBit48, ref string _sAppName, ref string _sSN)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            //Console.WriteLine("HexBit48: {0}", sHexBit48);
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            //Console.WriteLine("Len 48: {0}", iLenSN.ToString());
            _sSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            //Console.WriteLine("SN: {0}", _sSN);
            int iLenAppName = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2), 4));
            //Console.WriteLine("LenAppName: {0}", iLenAppName);
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2), iLenAppName * 2));
            //Console.WriteLine("AppName: {0}", _sAppName);
        }

        public static void GetAppNameSNSoftVer(string sBit48, ref string _sAppName, ref string _sSN, ref string _sSoftVer)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            //Console.WriteLine("HexBit48: {0}", sHexBit48);
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            //Console.WriteLine("Len 48: {0}", iLenSN.ToString());
            _sSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            //Console.WriteLine("SN: {0}", _sSN);_sSoftVer
            int iLenAppName = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2), 4));
            //Console.WriteLine("LenAppName: {0}", iLenAppName);
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2), iLenAppName * 2));

            if (sHexBit48.Length > (4 + (iLenSN * 2) + 4 + (iLenAppName * 2)))
            {
                int iLenSoftVer = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2) + 4 + (iLenAppName * 2), 4));
                _sSoftVer = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2) + 4 + (iLenAppName * 2), iLenSoftVer * 2));
            }
        }

        public static void GetAppNameSNSoftVerCRC(string sBit48, ref string _sAppName, ref string _sSN, ref string _sSoftVer,ref string _sCRC)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            //Console.WriteLine("HexBit48: {0}", sHexBit48);
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            //Console.WriteLine("Len 48: {0}", iLenSN.ToString());
            _sSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            //Console.WriteLine("SN: {0}", _sSN);_sSoftVer
            int iLenAppName = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2), 4));
            //Console.WriteLine("LenAppName: {0}", iLenAppName);
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2), iLenAppName * 2));
            int iLenSoftVer = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2) + 4 + (iLenAppName * 2), 4));
            _sSoftVer = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2) + 4 + (iLenAppName * 2), iLenSoftVer * 2));
            _sCRC = sHexBit48.Substring(8 + (iLenSN * 2) + 4 + (iLenAppName * 2) + (iLenSoftVer * 2), 4);

        }

        internal static void GetIndexTotalIndexDate(string sBit57, ref int _sIndexApp, ref int _sTotalIndex, ref string _sDateTime)
        {
            //string sHexBit57 = CommonLib.sStringToHex(sBit57).Replace(" ", "");
            
            string sHexBit57 = CommonLib.sStringToHex(sBit57).Replace(" ", "");
            Trace.Write(string.Format("ProcCode.DownloadSoftwareCont GetIndexTotalIndexDate, {0}", sHexBit57));

            _sIndexApp = int.Parse(sHexBit57.Substring(0,4));
            _sTotalIndex = int.Parse(sHexBit57.Substring(4,4));
            _sDateTime = sHexBit57.Substring(8, 14);
        }
        
        public static string[] arrsGetBit48Values(string sBit48)
        {
            string[] arrsValues = new string[3];
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iIndex = 0;

            // SN <SN Length><SN>
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            iIndex += 4;
            arrsValues[0] = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            iIndex += (iLenSN * 2);

            // AppName <AppName Length><AppName>
            int iLenAppName = int.Parse(sHexBit48.Substring(iIndex, 4));
            iIndex += 4;
            arrsValues[1] = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, iLenAppName * 2));
            iIndex += (iLenAppName * 2);

            // Tipe Comms <CommsId>
            arrsValues[2] = sHexBit48.Substring(iIndex, 4);

            return arrsValues;
        }

        public static bool IsOpenFileAllowed(string sFilename)
        {
            bool bAllow = false;
            FileStream fs = null;
            try
            {
                fs = File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite);
                fs.Close();
                bAllow = true;
            }
            catch (IOException)
            {
                bAllow = false;
            }
            return bAllow;
        }

        public static int iISOLenReceive(string sTempLen)
        {
            int iReturn = 0;
            iReturn = CommonLib.iHexStringToInt(sTempLen);
            return iReturn;
        }
    }
}
