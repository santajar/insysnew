using System;
using System.Data;
using System.Data.SqlClient;
using InSysClass;

namespace InSysConsoleSerial
{
    class Response
    {
        public static SqlConnection oConn;
        public static bool bCheckAllowInit;

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public static byte[] arrbResponse(byte[] arrbReceive, ConnType cType)
        {
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);
                arrbTemp = new byte[iIsoLen + 2];
                arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : {0}", ex.Message);
                Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected static string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTerminalId = null;
            string sTag = null;
            int iErrorCode = -1;

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            Trace.Write(" Recv1 : " + sTemp);

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);
                
                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            Trace.Write(" Recv : " + sTemp);

            SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn);

            oCmd.CommandType = CommandType.StoredProcedure;
            //oCmd.CommandTimeout = 600;
            oCmd.CommandTimeout = 0;
            oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;

            oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

            // kirim ke sp,...
            try
            {
                #region "Using DataReader"
                SqlDataReader reader;
                
                //IAsyncResult result = oCmd.BeginExecuteReader();
                //while (!result.IsCompleted)
                //    Thread.Sleep(100);
                //reader = oCmd.EndExecuteReader(result);

                reader = oCmd.ExecuteReader();

                while (reader.Read())
                {
                    iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                    sTerminalId = reader["TerminalId"].ToString();
                    sTemp = reader["ISO"].ToString();
                    sTag = reader["Tag"].ToString();
                    string scont = reader["Content"].ToString();

                    //Trace.Write(" Cont : " + scont);
                }
                reader.Close();
                reader.Dispose();
                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                    ex.ToString(), sTemp);
                //Console.WriteLine("{0}", ex.Message.ToString());
                Trace.Write(string.Format("sResponse failed : {0}\nMsg : {1}\n\n",
                    ex.ToString(), sTemp));
                Console.ReadKey();
                sTemp = null;
            }

            // tambahkan total length
            _iIsoLenResponse = sTemp.Length / 2;
            sTemp = sISOLenSend(_iIsoLenResponse, cType) + sTemp;
            Trace.Write("ISO : " + sTemp);
            Trace.Write("ISO Len : " + sTemp.Length.ToString());
            oCmd.Dispose();

            CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));

            Trace.Write(sTerminalId + ".txt",
                string.Format("{0} {1}", CommonConsole.sFullTime(), sProcessing(iErrorCode, sTag)));
            
            return sTemp;
        }

        protected static int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            //if (cType == ConnType.TcpIP)
            iReturn = CommonLib.iHexStringToInt(sTempLen);
            //else if (cType == ConnType.Serial)
            //    iReturn = int.Parse(sTempLen);
            return iReturn;
        }

        protected static string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
                //sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected static string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Initialize Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Initialize Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                default:
                    sTemp = "Unknown Error";
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected static string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "AI":
                    sTemp = "AID";
                    break;
                case "PK":
                    sTemp = "CAPK";
                    break;
            }
            return sTemp;
        }
    }
}
