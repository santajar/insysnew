using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using InSysClass;
using System.Threading;

namespace InSysConsoleSerial
{
    class Program
    {
        static bool bCheckAllowInit;
        static SqlConnection oConn = new SqlConnection();
        static string sConnString;
        static string sAppDirectory = Directory.GetCurrentDirectory();

        public static List<ClassInit> ltClassInit = new List<ClassInit>();
        public static DataTable dtISO8583 = new DataTable();

        #region "Serial Port Connection"
        static bool IsConnSerialActive;

        static string sSerialPort;
        static int iSerialBaudrate;
        static float fSerialStopbit;
        static int iSerialDataBit;
        static string sSerialParity;
        #endregion

        static void Main(string[] args)
        {
            try
            {
                oConn = InitConnection();

                if (oConn != null && oConn.State == ConnectionState.Open)
                {
                    InitTableSoftwareISO();
                    InitControlAllowInit();

                    Response.oConn = oConn;
                    Response.bCheckAllowInit = bCheckAllowInit;

                    Serial.bCheckAllowInit = bCheckAllowInit;
                    Serial.sConnString = sConnString;

                    InitConnType();
                    
                    // Start the Serial thread
                    if (IsConnSerialActive)
                    {
                        InitSerial();
                        while (true)
                        {
                            Serial.ResetPort();
                            Thread.Sleep(5000);
                        }
                    }
                }
                else
                {
                    CommonConsole.WriteToConsole("Database Connection Failed!...");
                    Console.WriteLine("Press ENTER to Terminate or R to Restart the program...");
                    string sKey = Console.ReadLine();
                    if (sKey.ToUpper() == "R")
                    {
                        Process oInSys = new Process();
                        oInSys.StartInfo.FileName = sAppDirectory + "\\InSysSerialConsole.exe";
                        oInSys.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonConsole.WriteToConsole(ex.Message);
                Thread.Sleep(3000);
                Process oInSys = new Process();
                oInSys.StartInfo.FileName = sAppDirectory + "\\InSysSerialConsole.exe";
                oInSys.Start();
            }
            //Console.Read();
        }

        #region "Function"
        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception ex)
            {
                oSqlTempConn = null;
                Trace.Write("InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }

        /// <summary>
        /// Set the Allow Initialization configuration
        /// </summary>
        static void InitControlAllowInit()
        {
            bCheckAllowInit = bGetControlAllowInit();
        }

        /// <summary>
        /// Get the Allow Init configuration from the database
        /// </summary>
        /// <returns>bool : boolean allow to initialize or not</returns>
        static bool bGetControlAllowInit()
        {
            bool bTemp = false;
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();

                SqlCommand oCmd = new SqlCommand(CommonSP.sSPFlagControlAllowInit, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter oda = new SqlDataAdapter(oCmd);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                if (dt.Rows.Count > 0)
                    bTemp = Convert.ToInt16(dt.Rows[0][0].ToString()) == 1 ? true : false;

                oCmd.Dispose();
                oda.Dispose();
                dt.Dispose();
            }
            catch (Exception ex)
            {
                Trace.Write("InitControlAllowInit : " + ex.Message);
            }
            return bTemp;
        }

        /// <summary>
        /// initialize connection Type
        /// </summary>
        static void InitConnType()
        {
            InitConsoleConnType oConnType = new InitConsoleConnType(sAppDirectory);
            IsConnSerialActive = oConnType.IsConnSerialActive();

            sSerialPort = oConnType.SerialPort;
            iSerialBaudrate = oConnType.SerialBaudRate;
            iSerialDataBit = oConnType.SerialDataBit;
            fSerialStopbit = oConnType.SerialStopBit;
            sSerialParity = oConnType.SerialParity;
        }

        /// <summary>
        /// initialize Serial Connection
        /// </summary>
        static void InitSerial()
        {
            Serial.sSerialPort = sSerialPort;
            Serial.sSerialParity = sSerialParity;
            Serial.iSerialBaudrate = iSerialBaudrate;
            Serial.iSerialDataBit = iSerialDataBit;
            Serial.fSerialStopbit = fSerialStopbit;

            Serial.InitClassSerial();
        }

        /// <summary>
        /// Get Value from tbBitMap
        /// </summary>
        static void InitTableSoftwareISO()
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPBitmapProfileSoftwareBrowse, oConn))
            {
                if (oConn.State != ConnectionState.Open) oConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtISO8583);
            }
        }
        #endregion
    }
}
