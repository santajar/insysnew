using System;
using System.Globalization;
using System.IO;
using InSysClass;

namespace InSysConsoleSerial
{
    public enum ConnType
    {
        TcpIP = 1,
        Serial = 2,
        Modem = 3
    }

    public enum ProcCode
    {
        ProfileStartEnd = 930000,
        ProfileCont = 930001,
        AutoProfileStartEnd = 960000,
        AutoProfileCont = 960001,
        Flazz = 940000,
        AutoPrompt = 950000,
        AutoCompletion = 970000,
        TerminalEcho = 980000,
    }

    class CommonConsole
    {
        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);
        static CultureInfo ciINAFormat = new CultureInfo("id-ID", true);

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sTimeHHMMSS()
        {
            return DateTime.Now.ToString("hhmmss", ciUSFormat);
        }

        public static string sDateMMddyy()
        {
            return DateTime.Now.ToString("MMddyy", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = CommonConsole.sFullTime();
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }

        public static string sGenerateISOSend(string sContent, ISO8583_MSGLib oIsoTemp,
            bool isLast, bool isHexContent,
            bool isEnable48, bool isEnableCompress,
            string sAppName)
        {
            string sIsoResponse;
            ISO8583Lib oIso = new ISO8583Lib();
            oIsoTemp.MTI = "0810";
            oIsoTemp.sBinaryBitMap = "0010000000100000000000010000000000000010100000000000000000000000";
            ProcCode tempProcCode = (ProcCode)int.Parse(oIsoTemp.sBit[3].ToString());
            switch (tempProcCode)
            {
                case ProcCode.ProfileStartEnd:
                case ProcCode.ProfileCont:
                    oIso.SetField_Str(3, ref oIsoTemp, 
                        isLast ? ProcCode.ProfileStartEnd.GetHashCode().ToString() : ProcCode.ProfileCont.GetHashCode().ToString());
                    break;
                case ProcCode.AutoProfileStartEnd:
                case ProcCode.AutoProfileCont:
                    oIso.SetField_Str(3, ref oIsoTemp,
                        isLast ? ProcCode.AutoProfileStartEnd.GetHashCode().ToString() : ProcCode.AutoProfileCont.GetHashCode().ToString());
                    break;
                case ProcCode.TerminalEcho:
                    oIso.SetField_Str(3, ref oIsoTemp, ProcCode.TerminalEcho.GetHashCode().ToString());
                    break;
            }
            //if (oIsoTemp.sBit[3] == "930000" || oIsoTemp.sBit[3] == "930001")
            //    oIso.SetField_Str(3, ref oIsoTemp, isLast ? "930000" : "930001");
            //else
            //    oIso.SetField_Str(3, ref oIsoTemp, isLast ? "960000" : "960001");
            if (isEnable48)
            {
                oIso.SetField_Str(12, ref oIsoTemp, string.Format("{0}", DateTime.Now.ToString("hhmmss", ciINAFormat)));
                if (tempProcCode == ProcCode.TerminalEcho)
                    oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));
                else
                    if (string.IsNullOrEmpty(sAppName) ||
                        Program.dtISO8583.Select(string.Format("SoftwareName='{0}' AND StandartISO8583=1", sAppName)).Length == 0)
                        oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMddyy}", DateTime.Now));
                    else
                        oIso.SetField_Str(13, ref oIsoTemp, string.Format("{0:MMdd}", DateTime.Now));
                oIso.SetField_Str(48, ref oIsoTemp, CommonLib.sStringToHex(oIsoTemp.sBit[48]));
            }
            oIso.SetField_Str(39, ref oIsoTemp, "00");
            
            if (isEnableCompress)
                oIso.SetField_Str(58, ref oIsoTemp, "1");

            if (!string.IsNullOrEmpty(sContent))
                oIso.SetField_Str(60, ref oIsoTemp, isHexContent ? sContent : CommonLib.sStringToHex(sContent));

            sIsoResponse = oIso.PackToISOHex(oIsoTemp,0);
            return sIsoResponse;
        }

        public static string sGenerateISOError(string sContent, ISO8583_MSGLib oIsoTemp)
        {
            return string.Format("{0}{1}{2}{3}", oIsoTemp.sTPDU, oIsoTemp.sOriginAddr, oIsoTemp.sDestNII, sContent);
        }

        public static void GetAppNameSN(string sBit48, ref string _sAppName, ref string _sSN)
        {
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            //Console.WriteLine("HexBit48: {0}", sHexBit48);
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            //Console.WriteLine("Len 48: {0}", iLenSN.ToString());
            _sSN = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            //Console.WriteLine("SN: {0}", _sSN);
            int iLenAppName = int.Parse(sHexBit48.Substring(4 + (iLenSN * 2), 4));
            //Console.WriteLine("LenAppName: {0}", iLenAppName);
            _sAppName = CommonLib.sHexToStringUTF7(sHexBit48.Substring(8 + (iLenSN * 2), iLenAppName * 2));
            //Console.WriteLine("AppName: {0}", _sAppName);
        }

        public static string[] arrsGetBit48Values(string sBit48)
        {
            string[] arrsValues = new string[3];
            string sHexBit48 = CommonLib.sStringToHex(sBit48).Replace(" ", "");
            int iIndex = 0;

            // SN <SN Length><SN>
            int iLenSN = int.Parse(sHexBit48.Substring(0, 4));
            iIndex += 4;
            arrsValues[0] = CommonLib.sHexToStringUTF7(sHexBit48.Substring(4, iLenSN * 2));
            iIndex += (iLenSN * 2);
            
            // AppName <AppName Length><AppName>
            int iLenAppName = int.Parse(sHexBit48.Substring(iIndex, 4));
            iIndex += 4;
            arrsValues[1] = CommonLib.sHexToStringUTF7(sHexBit48.Substring(iIndex, iLenAppName * 2));
            iIndex += (iLenAppName * 2);

            // Tipe Comms <CommsId>
            arrsValues[2] = sHexBit48.Substring(iIndex, 4);
            
            return arrsValues;
        }

        public static bool IsOpenFileAllowed(string sFilename)
        {
            bool bAllow = false;
            FileStream fs = null;
            try
            {
                fs = File.Open(sFilename, FileMode.Open, FileAccess.ReadWrite);
                fs.Close();
                bAllow = true;
            }
            catch (IOException)
            {
                bAllow = false;
            }
            return bAllow;
        }
    }
    
    class CommonSP
    {
        public static string sSPFlagControlAllowInit { get { return "spControlFlagAllowInitBrowse"; } }
        public static string sSPProcessMessage { get { return "spOtherParseProcess"; } }
        public static string sSPPortInit { get { return "spOtherParsePortInit"; } }
        public static string sSPInitConnBrowse { get { return "spControlFlagInitBrowse"; } }
        public static string sSPInitGetTableInit { get { return "spInitGetTableInit"; } }
        public static string sSPInitLogConnNewInit { get { return "spInitLogConnNewInit"; } }
        public static string sSPAuditInitInsert { get { return "spAuditInitInsert"; } }
        public static string sSPInitLogConnNewInitConsole { get { return "spInitLogConnNewInitConsole"; } }
        public static string sSPProfileTextFullTable { get { return "spProfileTextFullTable"; } }
        public static string sSPAuditTerminalSNEchoInsert { get { return "spAuditTerminalSNEchoInsert"; } }
        public static string sSPAuditTerminalSNEchoBrowse { get { return "spAuditTerminalSNEchoBrowse"; } }
        public static string sSPBitmapProfileSoftwareBrowse { get { return "spBitMapProfileSoftwareBrowse"; } }
    }
}