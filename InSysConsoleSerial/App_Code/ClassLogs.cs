using System.IO;
using InSysClass;

namespace InSysConsoleSerial
{
    class Logs
    {
        protected static string sGetErrorDirectory(string _sDirectory)
        {
            return _sDirectory + "\\LOGS";
        }

        protected static bool isHaveLogDir(string _sDirectory)
        {
            if (Directory.Exists(sGetErrorDirectory(_sDirectory)))
                return true;
            else
                return false;
        }

        public static void doWriteErrorFile(string sDirectory, string sError)
        {
            if (!isHaveLogDir(sDirectory))
                Directory.CreateDirectory(sGetErrorDirectory(sDirectory));
            CommonLib.Log(sDirectory, sError);
        }
    }
}
