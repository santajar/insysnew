﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using InSysClass;

namespace InSysConsoleSerial
{
    class Response3
    {
        protected SqlConnection oConn;
        protected bool bCheckAllowInit;
        protected ConnType cType;

        protected string sTerminalId;
        protected string sAppName = null;
        protected string sSerialNum = null;
        protected bool bInit = false;
        protected bool bStart = false;
        protected bool bComplete = false;
        protected int iPercentage = 0;
        protected int iMaxByte = 400;
        protected bool bEnable48 = false;
        protected bool bEnableCompress = false;

        public string TerminalId { get { return sTerminalId; } }
        public string AppName { get { return sAppName; } }
        public string SerialNum { get { return sSerialNum; } }
        public bool IsProcessInit { get { return bInit; } }
        public bool IsStartInit { get { return bStart; } }
        public bool IsCompleteInit { get { return bComplete; } }
        public int Percentage { get { return iPercentage; } }

        protected string sProcessCode = "930000";
        public string ProcessCode { get { return sProcessCode; } }

        protected byte[] arrbKey = (new UTF8Encoding()).GetBytes("316E67336E316330");

        public Response3(string _sConnString, bool _IsCheckAllowInit, ConnType _cType)
        {
            try
            {
                //Console.WriteLine("Create Object Response2");

                oConn = new SqlConnection();
                oConn = SQLConnLib.EstablishConnection(_sConnString);
                bCheckAllowInit = _IsCheckAllowInit;
                cType = _cType;
                iMaxByte = iGetMaxByte();
            }
            catch (Exception ex)
            {
                Trace.Write("Create Object Error: " + ex.Message);
                oConn.Close();
                oConn.Dispose();
            }
        }

        public void Dispose()
        {
            oConn.Close();
            oConn.Dispose();
        }

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public byte[] arrbResponse(byte[] arrbReceive)
        {            
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);
                //Console.WriteLine("Send Message: {0}", sSendMessage);

                if (!string.IsNullOrEmpty(sSendMessage))
                {
                    arrbTemp = new byte[iIsoLen + 2];
                    //arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);

                    sSendMessage = sSendMessage.Replace(" ", ""); // Remove all white space
                    byte[] arrbbuffer = new byte[sSendMessage.Length / 2];
                    for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                    {
                        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);
                    }
                    arrbTemp = arrbbuffer;
                }
            }
            catch (Exception ex)
            {
                Trace.Write("|[RESPONSE]|Error arrbResponse : " + ex.Message);
                //Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTag = null;
            int iErrorCode = -1;
            //Console.WriteLine("Begin Get Response");

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            try
            {
                // parse received ISO
                ISO8583Lib oIsoParse = new ISO8583Lib();
                ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
                //Console.WriteLine("ISO Length: {0}", oIsoMsg.sBit.Length.ToString());
                //Console.WriteLine("ArrbTemp");
                byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
                //Console.WriteLine("Unpack Iso");
                oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
                string sProcCode = oIsoMsg.sBit[3];
                sTerminalId = oIsoMsg.sBit[41];
                //Console.WriteLine("sProcCode = {0}", sProcCode);

                // kirim ke sp,...
                try
                {
                    if (oIsoMsg.MTI == "0800")
                    {
                        ProcCode oProcCodeTemp = (ProcCode)int.Parse(sProcCode);
                        switch (oProcCodeTemp)
                        {
                            case ProcCode.TerminalEcho:
                                Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);
                                #region "980000"
                                WriteLogTerminalSNEcho(oIsoMsg);
                                sTempReceive = CommonConsole.sGenerateISOSend(null, oIsoMsg, bComplete, false, true, false, null);
                                iErrorCode = 7;
                                #endregion
                                break;
                            default:
                                if (IsValidTerminalID(sTerminalId))
                                {
                                    if (bCheckAllowInit == false || (bCheckAllowInit && IsInitAllowed(sTerminalId)))
                                    {
                                        int iIndex = -1;
                                        switch (oProcCodeTemp)
                                        {
                                            case ProcCode.AutoProfileStartEnd:
                                            case ProcCode.ProfileStartEnd:                                                
                                                #region "930000 AND 960000"
                                                sProcessCode = sProcCode;
                                                if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                                                {
                                                    CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                                                    bEnable48 = true;
                                                    bEnableCompress = IsAllowCompress(sTerminalId);
                                                }
                                                if (string.IsNullOrEmpty(sAppName) ||
                                                    (!string.IsNullOrEmpty(sAppName) && IsApplicationAllowInit(sTerminalId, sAppName)))
                                                {
                                                    iErrorCode = 0;
                                                    Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

                                                    bInit = true;
                                                    string sContent = null;
                                                    bool bHexContent = false;
                                                    DataTable dtInitTemp = dtGetInitTemp(sTerminalId, ref bHexContent);
                                                    if (dtInitTemp.Rows.Count > 0)
                                                    {
                                                        ClassInit oClass = new ClassInit(sTerminalId);
                                                        oClass.dtResponses = dtInitTemp;
                                                        oClass.SerialNumber = sSerialNum;
                                                        oClass.AppName = sAppName;
                                                        oClass.HexContent = bHexContent;
                                                        oClass.Enable48 = bEnable48;
                                                        oClass.EnableCompress = bEnableCompress;

                                                        iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                                                        if (iIndex > -1) Program.ltClassInit.RemoveAt(iIndex);

                                                        Program.ltClassInit.Add(oClass);
                                                        sTag = oClass.Tag;
                                                        sContent = oClass.Content;
                                                        bComplete = oClass.IsLastPacket;
                                                        oClass.UpdateFlag();
                                                        iPercentage = oClass.Percentage;

                                                        iErrorCode = bComplete ? 1 : 0;

                                                        Trace.Write(string.Format("|[sContent]|{0}|sTempReceive {1}", sContent, sProcCode));
                                                        sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, bHexContent, bEnable48, bEnableCompress, sAppName);
                                                        bStart = true;
                                                    }
                                                }
                                                else
                                                {
                                                    iErrorCode = 6;
                                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("APPLICATION NOT ALLOWED"), oIsoMsg);
                                                }
                                                #endregion
                                                break;
                                            case ProcCode.ProfileCont:
                                            case ProcCode.AutoProfileCont:
                                                Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);
                                                #region "930001 AND 960001"
                                                sProcessCode = sProcCode;
                                                bInit = true;

                                                iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                                                if (iIndex > -1)
                                                {
                                                    bComplete = Program.ltClassInit[iIndex].IsLastPacket;
                                                    iErrorCode = bComplete ? 1 : 0;
                                                    sTag = Program.ltClassInit[iIndex].Tag;
                                                    string sContent = Program.ltClassInit[iIndex].Content;
                                                    //bool bHexContent = Program.ltClassInit[iIndex].HexContent;
                                                    bool bHexContent = sTag == "PK" ? true : Program.ltClassInit[iIndex].HexContent;
                                                    bEnable48 = Program.ltClassInit[iIndex].Enable48;
                                                    bEnableCompress = Program.ltClassInit[iIndex].EnableCompress;
                                                    sAppName = Program.ltClassInit[iIndex].AppName;
                                                    sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, bComplete, bHexContent, bEnable48, bEnableCompress, sAppName);
                                                    Program.ltClassInit[iIndex].UpdateFlag();
                                                    iPercentage = Program.ltClassInit[iIndex].Percentage;

                                                    if (bComplete)
                                                    {
                                                        Program.ltClassInit.RemoveAt(iIndex);
                                                        Trace.Write(string.Format("Total List : {0}", Program.ltClassInit.Count));
                                                    }
                                                }
                                                #endregion
                                                break;
                                            default:
                                                Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);
                                                #region "NOT 930000 AND NOT 960000"
                                                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                                                {
                                                    oCmd.CommandType = CommandType.StoredProcedure;
                                                    oCmd.CommandTimeout = 25;
                                                    oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                                                    oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                                                    #region "Using DataReader"
                                                    using (SqlDataReader reader = oCmd.ExecuteReader())
                                                    {
                                                        while (reader.Read())
                                                        {
                                                            iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                                            sTerminalId = reader["TerminalId"].ToString();
                                                            sTempReceive = reader["ISO"].ToString();
                                                            sTag = reader["Tag"].ToString();
                                                            string scont = reader["Content"].ToString();
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                #endregion
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        iErrorCode = 3;
                                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("NOTALLOWED"), oIsoMsg);
                                    }
                                }
                                else
                                {
                                    iErrorCode = 4;
                                    sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                                }
                                break;
                        }
                    }
                    else
                    {
                        iErrorCode = 5;
                        sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDMTI"), oIsoMsg);
                    }
                    if (!string.IsNullOrEmpty(sTempReceive))
                    {
                        // tambahkan total length
                        _iIsoLenResponse = sTempReceive.Length / 2;
                        sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                        Trace.Write(string.Format("|[RESPONSE]|{0}|ErrorCode : {2}|sTempReceive : {1}", sTerminalId, sTempReceive, iErrorCode));

                        if (iErrorCode == 1 && sProcCode == "930001")
                        {
                            SqlCommand oCmd = new SqlCommand(string.Format("UPDATE tbProfileTerminalList SET AllowDownload = 0 WHERE TerminalID = '{0}'", sTerminalId), oConn);
                            oCmd.ExecuteNonQuery();
                        }
                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                        
                    }
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                    //    ex.ToString(), sTemp);
                    Trace.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                        ex.ToString(), sTemp));
                }
            }
            catch (Exception ex)
            {
                Trace.Write("Parsing Error: " + ex.Message);
            }
            return sTempReceive;
        }

        /// <summary>
        /// Write log Terminal SN Echo 
        /// </summary>
        /// <param name="oIsoMsg"></param>
        private void WriteLogTerminalSNEcho(ISO8583_MSGLib oIsoMsg)
        {
            string[] arrsBit48Values = CommonConsole.arrsGetBit48Values(oIsoMsg.sBit[48]);
            if (arrsBit48Values != null)
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAuditTerminalSNEchoInsert, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalSN", SqlDbType.VarChar).Value = arrsBit48Values[0];
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = oIsoMsg.sBit[41];
                    oCmd.Parameters.Add("@sAppName", SqlDbType.VarChar).Value = arrsBit48Values[1];
                    oCmd.Parameters.Add("@iCommsID", SqlDbType.Int).Value = int.Parse(arrsBit48Values[2]);
                    oCmd.ExecuteNonQuery();
                }
        }

        /// <summary>
        /// Convert lenght of receiving iso to integer
        /// </summary>
        /// <param name="sTempLen">string : temporary lenght</param>
        /// <param name="cType">ConnType : Connection type </param>
        /// <returns>int: lenght Receiving ISO</returns>
        protected int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            //if (cType == ConnType.TcpIP)
            iReturn = CommonLib.iHexStringToInt(sTempLen);
            //else if (cType == ConnType.Serial)
            //    iReturn = int.Parse(sTempLen);
            return iReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iLen"></param>
        /// <param name="cType"></param>
        /// <returns></returns>
        protected string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP || cType == ConnType.Modem)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
                //sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Initialize Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Initialize Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                case 6:
                    sTemp = "Application Not Allow";
                    break;
                case 7:
                    sTemp = "Processing Echo";
                    break;
                default:
                    sTemp = "Unknown Error Code : " + iCode.ToString();
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
                case "AI":
                    sTemp = "AID";
                    break;
                case "PK":
                    sTemp = "CAPK";
                    break;
                case "PP":
                    sTemp="PinPad";
                    break;
                case "GP":
                    sTemp = "GPRS";
                    break;
                default:
                    sTemp = "";
                    break;
            }
            return sTemp;
        }

        /// <summary>
        /// Get List Content of Profile
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <param name="_sContent">string : Content</param>
        /// <returns>list : content of profile</returns>
        protected List<ContentProfile> ltGetContentProfile(string _sTerminalId, ref string _sContent)
        {
            List<ContentProfile> ltCPTemp = new List<ContentProfile>();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileTextFullTable, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);

                string[] arrsMaskedTag = arrsGetMaskedTag(_sTerminalId);

                if (dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow rowTemp in dtTemp.Rows)
                    {
                        string sTag = rowTemp["Tag"].ToString();
                        string sValue = rowTemp["TagValue"].ToString();
                        try
                        {
                            if (arrsMaskedTag.Length > 0 && arrsMaskedTag.Contains(sTag))
                                sValue = EncryptionLib.Decrypt3DES(CommonLib.sHexToStringUTF8(sValue), arrbKey);

                        }
                        catch (Exception)
                        {
                            sValue = rowTemp["TagValue"].ToString();
                        }

                        string sContent;
                        if (sTag != "AE37" && sTag != "AE38" && sTag != "TL11" && sTag.Substring(0, 2) != "PK")
                            sContent = string.Format("{0}{1:00}{2}", sTag, sValue.Length, sValue);
                        else
                        {
                            if (sTag == "AE37" || sTag == "AE38")
                                sContent = string.Format("{0}{1:000}{2}", sTag, sValue.Length, sValue);
                            else if (sTag == "PK04" && sTag == "PK06")
                                sContent = string.Format("{0}{1}{2}",
                                    CommonLib.sStringToHex(sTag),
                                    CommonLib.sStringToHex(string.Format("{0:000}", sValue.Length)),
                                    CommonLib.sStringToHex(sValue));
                            else
                                sContent = string.Format("{0}{1}{2}",
                                    sTag,
                                    string.Format("{0:000}", rowTemp["TagLength"]),
                                    sValue);
                        }
                        _sContent += sContent;
                        ContentProfile oCPTemp = new ContentProfile(sTag.Substring(0, 2), sContent);
                        int iIndexSearch = -1;
                        if (ltCPTemp.Count == 0)
                            ltCPTemp.Add(oCPTemp);
                        else
                            if ((iIndexSearch = ltCPTemp.FindIndex(oCPSearch => oCPSearch.Tag == oCPTemp.Tag)) > -1)
                            {
                                ContentProfile oCPSearchResult = ltCPTemp[iIndexSearch];
                                ContentProfile oCPNew = new ContentProfile(oCPTemp.Tag, oCPSearchResult.Content + oCPTemp.Content);
                                ltCPTemp[iIndexSearch] = oCPNew;
                            }
                            else
                                ltCPTemp.Add(oCPTemp);
                    }
                }
            }
            return ltCPTemp;
        }

        /// <summary>
        /// Get Masked Tag
        /// </summary>
        /// <param name="_sTerminalId">string : Terminal ID</param>
        /// <returns>array string : list masked tag</returns>
        protected string[] arrsGetMaskedTag(string _sTerminalId)
        {
            List<string> ltTag=new List<string>();
            string sQuery = string.Format("SELECT * FROM tbItemList WHERE DatabaseID IN (SELECT DatabaseID FROM tbProfileTerminalList WHERE TerminalID='{0}') AND vMasking=1", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                new SqlDataAdapter(oCmd).Fill(dtTemp);
                if (dtTemp != null && dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow row in dtTemp.Rows)
                        ltTag.Add(row["Tag"].ToString());
                }
            }
            return ltTag.ToArray();
        }

        /// <summary>
        /// Determine making Compress File
        /// </summary>
        /// <param name="sFilename">string : File Name</param>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : Succes if Complete</returns>
        protected bool IsNetCompressSuccess(string sFilename, string sTerminalId)
        {
            bool bReturn = false;
            if (File.Exists(sFilename))
            {
                try
                {
                    FileInfo fi = new FileInfo(sFilename);
                    using (FileStream inFile = fi.OpenRead())
                    {
                        string sGzipFile = sFilename + ".gz";
                        if (File.Exists(sGzipFile))
                            File.Delete(sGzipFile);
                        using (FileStream outFile = File.Create(sFilename + ".gz"))
                        {
                            using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                            {
                                byte[] buffer = new byte[fi.Length];
                                inFile.Read(buffer, 0, Convert.ToInt32(fi.Length));
                                compress.Write(buffer, 0, Convert.ToInt32(fi.Length));
                                bReturn = true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    //MessageBox.Show("NetCompress " + ex.Message);
                }
            }
            return bReturn;
        }

        /// <summary>
        /// Determine Allow Compress
        /// </summary>
        /// <param name="sTerminalId">string : Terminal ID</param>
        /// <returns>bool : True if Compress</returns>
        protected bool IsAllowCompress(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsCompressInit('{0}')", sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        /// <summary>
        /// Determine Max Byte
        /// </summary>
        /// <returns>int: max Byte</returns>
        protected int iGetMaxByte()
        {
            int iMaxLength = 400;
            string sQuery = string.Format("SELECT dbo.iPacketLength() [MaxByte]");
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        iMaxLength = int.Parse(oRead["MaxByte"].ToString());
                }
            }
            return iMaxLength;
        }

        /// <summary>
        /// Get Temporary data Profile
        /// </summary>
        /// <param name="_sTerminalId">string: Terminal ID</param>
        /// <param name="isCompress">bool: true of compress</param>
        /// <returns>data</returns>
        protected DataTable dtGetInitTemp(string _sTerminalId, ref bool isCompress)
        {
            DataTable dtInitTemp = new DataTable();
            dtInitTemp.Columns.Add("TerminalId", typeof(string));
            dtInitTemp.Columns.Add("Content", typeof(string));
            dtInitTemp.Columns.Add("Tag", typeof(string));
            dtInitTemp.Columns.Add("Flag", typeof(bool));

            string sContent = null;
            List<ContentProfile> _ltCPTemp = ltGetContentProfile(_sTerminalId, ref sContent);
            if (_ltCPTemp != null && _ltCPTemp.Count > 0)
            {
                isCompress = bEnableCompress;
                string sTag;
                int iOffset = 0;
                int iFlag = 0;
                if (bEnableCompress)
                {
                    #region "GZ part"
                    sTag = "GZ";
                    iOffset = 0;
                    string sContentCompressed = null;
                    // write to file locally and compress
                    if (!Directory.Exists(Environment.CurrentDirectory + @"\ZIP"))
                        Directory.CreateDirectory(Environment.CurrentDirectory + @"\ZIP");
                    string sFilename = Environment.CurrentDirectory + @"\ZIP\" + _sTerminalId;

                    if (File.Exists(sFilename))
                        File.Delete(sFilename);
                    CommonLib.Write2File(sFilename, sContent, false);

                    if (IsNetCompressSuccess(sFilename, _sTerminalId))
                    {
                        FileStream fsRead = new FileStream(sFilename + ".gz", FileMode.Open);
                        byte[] arrbContent = new byte[fsRead.Length];
                        fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
                        fsRead.Close();
                        sContentCompressed = CommonLib.sByteArrayToHexString(arrbContent).Replace(" ", "");
                        while (!string.IsNullOrEmpty(sContentCompressed) && iOffset <= sContentCompressed.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            rows["Content"] = sContentCompressed.Substring(iOffset,
                                iOffset + (iMaxByte * 2) <= sContentCompressed.Length ?
                                (iMaxByte * 2) :
                                sContentCompressed.Length - iOffset);
                            dtInitTemp.Rows.Add(rows);
                            iOffset += (iMaxByte * 2);
                        }
                    }
                    #endregion
                }
                else
                {
                    iFlag = 0;
                    iOffset = 0;
                    // non-GZ part
                    foreach (ContentProfile oCPTemp in _ltCPTemp)
                    {
                        sTag = oCPTemp.Tag;
                        sContent = oCPTemp.Content;
                        iOffset = 0;
                        //while (iOffset <= sContent.Length), error, row menjadi null ketika value nya kosong.
                        while (iOffset < sContent.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            int iOffsetHeader = sContent.IndexOf(sTag + "01", iOffset + 1);
                            int iLength = 0;

                            //if (iOffset + iMaxByte <= sContent.Length)
                            //    if (iOffsetHeader > 0)
                            //        if (iOffset + iMaxByte <= iOffsetHeader)
                            //            iLength = iMaxByte;
                            //        else
                            //            iLength = iOffsetHeader - iOffset;
                            //    else
                            //        iLength = iMaxByte;
                            //else
                            //    iLength = sContent.Length - iOffset;

                            if (sTag != "AI" && sTag != "PK")
                            {
                                if (iOffset + iMaxByte <= sContent.Length)
                                    if (iOffsetHeader > 0)
                                        if (iOffset + iMaxByte <= iOffsetHeader)
                                            iLength = iMaxByte;
                                        else
                                            iLength = iOffsetHeader - iOffset;
                                    else
                                        iLength = iMaxByte;
                                else
                                    iLength = sContent.Length - iOffset;
                            }
                            else
                            {
                                if (iOffset + iMaxByte <= sContent.Length || iOffsetHeader > 0)
                                    if (iOffsetHeader > 0)
                                        if (iOffset + iMaxByte <= iOffsetHeader)
                                            iLength = iMaxByte;
                                        else
                                            iLength = iOffsetHeader - iOffset;
                                    else
                                        iLength = iMaxByte;
                                else
                                    iLength = sContent.Length - iOffset;
                            }

                            //rows["Content"] = sContent.Substring(iOffset, iLength);

                            if (sTag != "AI" && sTag != "PK")
                                rows["Content"] = sContent.Substring(iOffset, iLength);
                            else
                            {
                                if (sTag == "PK")
                                    rows["Content"] = CommonLib.sStringToHex(sTag) + 
                                        CommonLib.sStringToHex(sContent.Substring(iOffset, iLength));
                                else
                                    rows["Content"] = sTag + sContent.Substring(iOffset, iLength);
                            }
                            iOffset += iLength;
                            dtInitTemp.Rows.Add(rows);
                        }
                    }
                }
            }
            return dtInitTemp;
        }

        /// <summary>
        /// Determine Application allow init
        /// </summary>
        /// <param name="_sTerminalId">String : TerminalID</param>
        /// <param name="_sAppName">Application Name</param>
        /// <returns>bool: True if Allow</returns>
        protected bool IsApplicationAllowInit(string _sTerminalId, string _sAppName)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsApplicationAllowInit('{0}','{1}')", _sTerminalId, _sAppName);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        /// <summary>
        /// Determine allow Init
        /// </summary>
        /// <param name="_sTerminalId">String : Terminal ID</param>
        /// <returns>bool : true if Allow</returns>
        protected bool IsInitAllowed(string _sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsInitAllowed('{0}')", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        /// <summary>
        /// Determine Terminal ID is valid or not
        /// </summary>
        /// <param name="_sTerminalId">string : TerminalID</param>
        /// <returns>bool: True if Valid</returns>
        protected bool IsValidTerminalID(string _sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsValidTerminalID('{0}')", _sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }
    }
}
