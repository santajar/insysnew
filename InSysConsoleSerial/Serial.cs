using System;
using System.IO.Ports;
using System.Threading;
using InSysClass;

namespace InSysConsoleSerial
{
    public class Serial
    {
        public static string sSerialPort;
        public static int iSerialBaudrate;
        public static float fSerialStopbit;
        public static int iSerialDataBit;
        public static string sSerialParity;

        public static string sConnString;
        public static bool bCheckAllowInit;

        static SerialPort serialport;

        const byte STX = 2;
        const byte ETX = 3;

        /// <summary>
        /// Serial Class constructor
        /// </summary>
        public static void InitClassSerial() { InitClassSerial(true); }

        public static void InitClassSerial(bool bShowStatus)
        {
            Parity paritySerial = SetParity();
            StopBits stopBits = SetStopBits();

            try
            {
                serialport = new SerialPort(sSerialPort, iSerialBaudrate, paritySerial, iSerialDataBit, stopBits);
                serialport.DtrEnable = true;
                serialport.Open();
                if (bShowStatus) CommonConsole.WriteToConsole("Ready...");
                serialport.DataReceived += new SerialDataReceivedEventHandler(serialport_DataReceived);
            }
            catch (Exception ex)
            {
                Console.WriteLine("1:{0}", ex.Message);
            }
        }

        /// <summary>
        /// SerialClass DataReceived event
        /// </summary>
        static void serialport_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(200);
            int iBytesToRead = serialport.BytesToRead;
            byte[] arrbReceive = new byte[iBytesToRead];
            serialport.Read(arrbReceive, 0, iBytesToRead);
            if (arrbReceive.Length != 0)
            {
                if (IsDataValid(arrbReceive))
                {
                    //Console.WriteLine(CommonLib.sByteArrayToHexString(arrbReceive));
                    //Console.WriteLine("Data valid");
                    Trace.Write("Receive : " + CommonLib.sByteArrayToHexString(arrbReceive));
                    byte[] arrbRec = new byte[arrbReceive.Length - 3];
                    Array.Copy(arrbReceive, 1, arrbRec, 0, arrbReceive.Length - 3);

                    byte[] arrbSend = arrbGetResponse(arrbRec);

                    string s = CommonLib.sByteArrayToHexString(arrbSend);
                    Trace.Write("Send : " + s.Replace(" ", ""));
                    Trace.Write("Send Length : " + s.Replace(" ", "").Length.ToString());
                    //Console.WriteLine(CommonLib.sByteArrayToHexString(arrbSend));
                    serialport.Write(arrbSend, 0, arrbSend.Length);
                    SerialFlush();
                }
                else
                {
                    Trace.Write("Data Tidak Valid : " + CommonLib.sByteArrayToString(arrbReceive));
                    //Console.WriteLine("Data tdk valid");
                }
            }
            else
            {
                Trace.Write("Empty Record : " + CommonLib.sByteArrayToString(arrbReceive));
                //Console.WriteLine("Empty Record");
            }
        }

        /// <summary>
        /// Set the serial port parity
        /// </summary>
        static Parity SetParity()
        {
            Parity parityTemp = Parity.None;
            switch (sSerialParity.ToUpper())
            {
                case "EVEN":
                    parityTemp = Parity.Even;
                    break;
                case "MARK":
                    parityTemp = Parity.Mark;
                    break;
                case "ODD":
                    parityTemp = Parity.Odd;
                    break;
                case "SPACE":
                    parityTemp = Parity.Space;
                    break;
                default:
                    parityTemp = Parity.None;
                    break;
            }
            return parityTemp;
        }

        /// <summary>
        /// Set the serial port stopbits
        /// </summary>
        static StopBits SetStopBits()
        {
            StopBits stopbitsTemp = StopBits.One;
            if (fSerialStopbit == 0)
                stopbitsTemp = StopBits.None;
            else if (fSerialStopbit == 0)
                stopbitsTemp = StopBits.One;
            else if (fSerialStopbit == 0)
                stopbitsTemp = StopBits.OnePointFive;
            else if (fSerialStopbit == 0)
                stopbitsTemp = StopBits.Two;
            return stopbitsTemp;
        }

        /// <summary>
        /// Count the checksum value
        /// </summary>
        /// <param name="sValue">string : hexstring message to count</param>
        /// <returns>String : checksum result in hexstring</returns>
        static string sCheckSum(string sValue)
        {
            Int32 iResult = 0;
            for (int iCount = 0; iCount < sValue.Length; iCount += 2)
                iResult = iResult ^ Convert.ToInt32(sValue.Substring(iCount, 2), 16);

            string sResultBinary = Convert.ToString(iResult, 2);
            string sResultHex = Convert.ToString(Convert.ToInt32(sResultBinary, 2), 16);
            sResultHex = "00".Substring(0, 2 - sResultHex.Length) + sResultHex;
            return sResultHex.ToUpper();
        }

        /// <summary>
        /// To check the received data is contains STX-ETX code, and the checksum result is correct.
        /// </summary>
        /// <param name="arrbTemp">byte[] : array of byte of received message</param>
        /// <returns>bool : TRUE, if message contains of STX-ETX and checksum result is correct,
        /// otherwise FALSE</returns>
        static bool IsDataValid(byte[] arrbTemp)
        {
            if (arrbTemp[0] == STX && arrbTemp[arrbTemp.Length - 2] == ETX)
            {
                string sValue = CommonLib.sByteArrayToHexString(arrbTemp).Replace(" ", "");
                if (sValue.Substring(sValue.Length - 2) == sCheckSum(sValue.Substring(2, sValue.Length - 4)))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Send the received message to Database to get the response
        /// </summary>
        /// <param name="_arrbReceive">byte[] : array of byte of received message</param>
        /// <returns>byte[] : array of byte of response message</returns>
        static byte[] arrbGetResponse(byte[] _arrbReceive)
        {
            byte[] arrbTemp = (new Response3(sConnString, bCheckAllowInit, ConnType.TcpIP)).arrbResponse(_arrbReceive);
            string sISOMsg = CommonLib.sByteArrayToHexString(arrbTemp).Replace(" ", "");
            string sISOSend = STX.ToString("00")
                + sISOMsg
                + ETX.ToString("00")
                + sCheckSum(sISOMsg + ETX.ToString("00"));
            return CommonLib.HexStringToByteArray(sISOSend);
        }

        static void SerialFlush()
        {
            serialport.DiscardInBuffer();
            serialport.DiscardOutBuffer();
        }

        public static void ResetPort()
        {
            if (!serialport.CDHolding)
            {
                serialport.Close();
                InitClassSerial(false);
            }
            //else
            //    Console.WriteLine("OPEN");
        }
    }
}
