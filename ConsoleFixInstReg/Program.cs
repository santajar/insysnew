﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using InSysClass;

namespace ConsoleFixInstReg
{
    class Program
    {
        static SqlConnection oSqlConn = new SqlConnection();
        static List<string> ltAcquirer = new List<string> { "BCA", "CREDIT", "JCB", "JCB BCA", 
            "MASTER BCA", "MASTERCARD", "SMARTCASH", "VISA", "VISA BCA" };

        static void Main(string[] args)
        {
            try
            {
                InitConn();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Main Error : {0}", ex.Message);
            }
            Console.Read();
        }

        static void Init()
        {
            InitConn();
        }

        static void InitConn()
        {
            oSqlConn = new SqlConnection((new InitData()).sGetConnString());
            oSqlConn.Open();
        }

        static List<string> GetListTerminalId(string _sFilename)
        {
            List<string> _arrsTemp = new List<string>();
            string sFilename = Environment.CurrentDirectory + _sFilename;
            StreamReader sr = new StreamReader(sFilename);
            while (!sr.EndOfStream)
                _arrsTemp.Add(sr.ReadLine());
            sr.Close();
            return _arrsTemp;
        }

        static DataTable dtGetUpdateList()
        {
            DataTable dtTemp = new DataTable();
            return dtTemp;
        }

        static string sListToCommand(List<string> _ltTemp)
        {
            string sReturn = null;
            foreach (string sTemp in _ltTemp)
                sReturn = string.IsNullOrEmpty(sReturn) ? string.Format("'{0}'", sTemp) :
                    string.Format("{0},'{1}'", sReturn, sTemp);
            return sReturn;
        }
    }
}
