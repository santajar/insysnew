using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using ipXML;
using InSysClass;
using Ini;


namespace ConsoleSyncFileEms
{
    class Program
    {
        const string ArgTerminalId = "-T";
        const string ArgCommand = "-C";
        const string ArgPath = "-P";
        
        enum TypeCommand
        {
            A,  //AddUpdate,
            D,  //Delete
        }

        static string sCurrDirectory = Directory.GetCurrentDirectory();
        static SqlConnection oSqlConn = new SqlConnection();

        static void Main(string[] args)
        {
            if (args.Length == 3)
            {
                string sArg1 = args[0];
                string sArg2 = args[1];
                string sArg3 = args[2];

                sCurrDirectory = sArg3.Replace(ArgPath, "");
                ShowWriteError(sCurrDirectory);

                oSqlConn = new SqlConnection(new InitData(sCurrDirectory).sGetConnString());
                oSqlConn.Open();

                if (oSqlConn.State == ConnectionState.Open)
                    ShowWriteError("OPEN");
                else
                    ShowWriteError("NOT OPEN");
                //sCurrDirectory = sAppDirectory(oSqlConn);

                string sTargetDir = sInitTargetPath(sCurrDirectory);
                
                string sTerminalId = sArg1.Substring(0, 2).ToUpper() == ArgTerminalId ? sGetTerminalId(sArg1) : sGetTerminalId(sArg2);
                TypeCommand oTypeCmd = sArg1.Substring(0, 2).ToUpper() == ArgCommand ? typeCmd(sArg1) : typeCmd(sArg2);
                
                ShowWriteError(sCurrDirectory);
                ShowWriteError("sTerminalId : " + sTerminalId);
                ShowWriteError("oTypeCmd : " + oTypeCmd.ToString());
                
                if (oTypeCmd.ToString() == TypeCommand.D.ToString())
                {
                    ShowWriteError("Processing Delete ");
                    GenerateDeleteXml(sTerminalId, sTargetDir);
                }
                else if (oTypeCmd.ToString() == TypeCommand.A.ToString())
                {
                    ShowWriteError("Processing AddUpdate ");
                    GenerateAddUpdXml(sTerminalId, sTargetDir);
                }
                ShowWriteError(sTerminalId + " Processed");
            }
            else if (args[0].ToLower() == "/h" || args[0] == "/?")
                ShowHelp();
            else
            {
                Console.WriteLine("Invalid command. Type /? or /H. To show Help");
                Console.Read();
            }
        }

        static string sInitTargetPath(string sPath)
        {
            string sEmsSyncConfig = sPath + @"\EmsSync.config";
            IniFile oIni = new IniFile(sEmsSyncConfig);
            return oIni.IniReadValue("EMS", "Target");
        }

        static void GenerateDeleteXml(string sTerminalId, string sPath)
        {
            try
            {
                EMS_XML oEms = new EMS_XML();
                oEms.Attribute.sSenders = "INSYS";
                oEms.Attribute.sTypes = EMSCommonClass.XMLRequestType.DELETE.ToString();
                oEms.Attribute.sVersions = "1.00";
                oEms.Attribute.sSenderID = string.Format("INSYS{0:yyyyMMddHHmmss}", DateTime.Now);

                EMS_XML.DataClass.TerminalClass oTerminal = new EMS_XML.DataClass.TerminalClass();
                oTerminal.AddColumnValue("Terminal_INIT", sTerminalId);
                oEms.Data.doAddTerminal(oTerminal);

                string sFilename = string.Format(@"{0}\{1}.xml", sPath, oEms.Attribute.sSenderID);
                oEms.CreateXmlFile(sFilename, false);
                ShowWriteError(sFilename);
                ShowWriteError(oEms.Attribute.sSenderID);
            }
            catch (Exception ex)
            {
                ShowWriteError("Error : " + ex.Message);
            }
        }

        static void GenerateAddUpdXml(string sTerminalId, string sPath)
        {
            try
            {
                EMS_XML oEms = new EMS_XML();
                oEms.Attribute.sSenders = "INSYS";
                oEms.Attribute.sTypes = EMSCommonClass.XMLRequestType.INQ.ToString();
                oEms.Attribute.sVersions = "1.00";
                oEms.Attribute.sSenderID = string.Format("INSYS{0:yyyyMMddHHmmss}", DateTime.Now);

                EMS_XML.DataClass.FilterClass oFilter = new EMS_XML.DataClass.FilterClass();
                oFilter.sFilterFieldName = "Terminal_INIT";
                oFilter.sValues = sTerminalId;
                oEms.Data.doAddFilter(oFilter);

                EMS_XML oEmsResult = new EMS_XML();
                oEmsResult.Attribute.sSenders = "INSYS";
                oEmsResult.Attribute.sTypes = EMSCommonClass.XMLRequestType.ADD_UPDATE.ToString();
                oEmsResult.Attribute.sVersions = "1.00";
                oEmsResult.Attribute.sSenderID = string.Format("INSYS{0:yyyyMMddHHmmss}", DateTime.Now);

                string sErrMsg = null;

                SqlEMS_XML oSqlXml = new SqlEMS_XML(oSqlConn, oEms.Attribute.sSenderID, oEms);
                if (oSqlXml.IsCreateTable(ref sErrMsg, ref oEmsResult))
                    if (oSqlXml.IsInsertRow(ref sErrMsg, ref oEmsResult))
                        if (oSqlXml.IsXMLInquiry(oEms.Attribute.sSenderID, ref sErrMsg, ref oEmsResult, ""))
                        {
                            string sFilename = string.Format(@"{0}\{1}.xml", sPath, oEms.Attribute.sSenderID);
                            oEmsResult.Attribute.sTypes = EMSCommonClass.XMLRequestType.ADD_UPDATE.ToString();
                            oEmsResult.CreateXmlFile(sFilename, false);
                            ShowWriteError(sFilename);
                            ShowWriteError(oEmsResult.Attribute.sSenderID);
                        }
            }
            catch (Exception ex)
            {
                ShowWriteError("Error : " + ex.Message);
            }
        }

        static string sGetTerminalId(string sArguments)
        {
            return sArguments.Substring(2,8).ToUpper();
        }

        static TypeCommand typeCmd(string sArguments)
        {
            if (sArguments.Substring(2).ToUpper() == TypeCommand.A.ToString())
                return TypeCommand.A;
            else
                return TypeCommand.D;
        }

        static void ShowHelp()
        {
            string sHelpFile = sCurrDirectory + @"\Help.hlp";
            StreamReader sr = new StreamReader(sHelpFile);
            string sHelpText = sr.ReadToEnd();
            sr.Close();
            Console.WriteLine(sHelpText);
        }

        public static void ShowWriteError(string sErrorMsg)
        {
            string sDirLog = sCurrDirectory + @"\LOG";
            if (!Directory.Exists(sDirLog))
                Directory.CreateDirectory(sDirLog);
            string sFilename = string.Format(@"{0}\{1:yyyyMMdd}.log", sDirLog, DateTime.Now);
            
            StreamWriter sw = new StreamWriter(sFilename, true);
            sw.WriteLine("[{0:yyyy-MM-dd HH:mm:ss}] {1}", DateTime.Now, sErrorMsg);
            sw.Close();

            Console.WriteLine("[{0:yyyy-MM-dd HH:mm:ss}] {1}", DateTime.Now, sErrorMsg);
        }

        static string sAppDirectory(SqlConnection _oSqlConn)
        {
            string sPath = null;
            SqlCommand oCmd = new SqlCommand("spXMLSyncEmsPath", _oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sPath", SqlDbType.VarChar).Direction = ParameterDirection.Output;
            if (_oSqlConn.State != ConnectionState.Open)
                _oSqlConn.Open();
            oCmd.ExecuteNonQuery();
            oCmd.Dispose();
            return sPath;
        }
    }
}
