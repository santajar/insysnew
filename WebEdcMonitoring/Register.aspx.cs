﻿using InSysClass;
using WebClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using WebClass;

public partial class Register : Page
{
    protected ConnectionClass sqlconn = new ConnectionClass();

    protected Struct[] UserRight;
    protected CheckBox[] arrAdministrator;
    protected CheckBox[] arrSPV;
    protected CheckBox[] arrOP;
    protected CheckBox[] arrGuest;
    protected DataTable oDataTable = new DataTable();


    /// <summary>
    /// Initiate CheckboxStruct for each UserRighs.
    /// </summary>
    public void InitCheckBoxStruct()
    {
        UserRight = new Struct[]
        {
            new Struct ("Administrator", PrivilegeCode.W1.ToString(),chkAdministrator,arrAdministrator, 19),
            new Struct ("Supervisor", PrivilegeCode.W2.ToString(),chkSPV,arrSPV, 19),
            new Struct ("Operator", PrivilegeCode.W3.ToString(),chkOP,arrOP, 19),
            new Struct ("Guest", PrivilegeCode.W4.ToString(),chkGS,arrGuest, 19),
        };
    }        
    public void InitArray()
    {
        arrAdministrator = new CheckBox[] { chkAdministrator,chkdtMonitoring, chkdtMonitoringExport, chkdtMonitoringDelete, chkHbMonitoring, chkHbMonitoringExport, chkHbMonitoringDelete,chkUserManagement, chkUserRegisterMenu, chkUserRegisterApprovedMenu, chkUserRegisterAccess, chkUserRegisterActived, chkUserRegisterApproved, chkUserRegisterLocked, chkUserRegisterDelete, chkUserList, chkActivityLog, chkActivityLogLogin, chkUploadData };
        arrSPV = new CheckBox[] { chkSPV, chkdtMonitoring, chkdtMonitoringExport, chkdtMonitoringDelete, chkHbMonitoring, chkHbMonitoringExport, chkHbMonitoringDelete, chkUserManagement, chkUserRegisterMenu, chkUserRegisterApprovedMenu, chkUserRegisterAccess, chkUserRegisterActived, chkUserRegisterApproved, chkUserRegisterLocked, chkUserRegisterDelete, chkUserList, chkActivityLog, chkActivityLogLogin, chkUploadData };
        arrOP = new CheckBox[] { chkOP, chkdtMonitoring, chkdtMonitoringExport, chkdtMonitoringDelete, chkHbMonitoring, chkHbMonitoringExport, chkHbMonitoringDelete, chkUserManagement, chkUserRegisterMenu, chkUserRegisterApprovedMenu, chkUserRegisterAccess, chkUserRegisterActived, chkUserRegisterApproved, chkUserRegisterLocked, chkUserRegisterDelete, chkUserList, chkActivityLog, chkActivityLogLogin, chkUploadData };
        arrGuest = new CheckBox[] { chkGS, chkdtMonitoring, chkdtMonitoringExport, chkdtMonitoringDelete, chkHbMonitoring, chkHbMonitoringExport, chkHbMonitoringDelete, chkUserManagement, chkUserRegisterMenu, chkUserRegisterApprovedMenu, chkUserRegisterAccess, chkUserRegisterActived, chkUserRegisterApproved, chkUserRegisterLocked, chkUserRegisterDelete, chkUserList, chkActivityLog, chkActivityLogLogin, chkUploadData };
    }
    protected struct Struct
    {
        readonly string sDesc;
        readonly string sKey;
        CheckBox chkView;
        CheckBox[] arrCheckBox;
        int iArraySize;

        public Struct(string _sDesc, string _sKey, CheckBox _chkView, CheckBox[] _arrCheckBox, int _iArraySize)
        {
            sDesc = _sDesc;
            sKey = _sKey;
            chkView = _chkView;
            arrCheckBox = _arrCheckBox;
            iArraySize = _iArraySize;
        }

        public string sDescription { get { return sDesc; } }
        public string sFieldKey { get { return sKey; } }
        public CheckBox chkMaster { get { return chkView; } }
        public CheckBox[] chkDetail { get { return arrCheckBox; } }
        public int iSize { get { return iArraySize; } }
    }

    /// <summary>
    /// Current UserID
    /// </summary>
    protected static string sUserID;

    /// <summary>
    /// CurrentUserName
    /// </summary>
    protected static string sUserName;

    /// <summary>
    /// Set current password
    /// </summary>
    protected static string sPassword;
    protected int iStatus;

    /// <summary>
    /// Is form is called from Main Menu
    /// </summary>
    protected bool isFromMenu;
    protected bool isForcePassword;

    public string sClearNewPassword;
    
    protected string sGetEncryptPassword(string sPassword)
    {
        return DataEncryptionClass.sGetEncrypt(sPassword);
    }
    protected string sGetEncryptPassword()
    {
        return DataEncryptionClass.sGetEncrypt(txtPassword.Text.ToString());
    }
    /// <summary>
    /// Get the encrypted new password
    /// </summary>
    /// <returns>string : Encrypted New Password</returns>
    public string sGetNewPassEncrypted()
    {
        return sPassword;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HttpCookie UserInfo = Request.Cookies["UserInfo"];

            if (UserInfo != null)
            {
                if (UserInfo.Values.Count != 0)
                {
                    if (Session.Count != 0)
                    {
                        sUserID = Session["UserID"].ToString();
                        if (Session["UserSession"] != null)
                        {
                        }

                    }
                    else
                    {
                        Response.Redirect("~/Account/Login.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

    }
    protected void MessageBox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }

    /// <summary>
    /// Validates the passwords inputed by user.
    /// </summary>
    /// <returns>boolean : true if valid, else false</returns>
    protected bool isValid()
    {
        bool isValid = false;
        if (txtPassword.Text.Length <= 0) MessageBox("Password Error ");
        else if (txtConfirmPass.Text.Length <= 0) MessageBox("Confirm Password Error ");
        else if (txtUserID.Text.Length <= 0) MessageBox("User ID Error");
        else if (txtPassword.Text != txtConfirmPass.Text) MessageBox("Password & Confirm Password Error");
        else isValid = true;

        return isValid;
    }
    
    protected bool isNewPasswordLikeOldPassword()
    {
        bool bExist = true;
        try
        {
            if (txtPassword.Text != txtConfirmPass.Text.ToString())
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCheckHistoryPassword, sqlconn.MyConnection);
                oSqlCmd.CommandType = CommandType.StoredProcedure;

                oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = sUserID;
                oSqlCmd.Parameters.Add("@sEncrypPassword", SqlDbType.VarChar).Value = sGetEncryptPassword();
                oSqlCmd.Parameters.Add("@bReturn", SqlDbType.Bit).Value = bExist;
                oSqlCmd.Parameters["@bReturn"].Direction = ParameterDirection.Output;
                if (sqlconn.MyConnection.State == ConnectionState.Closed) sqlconn.MyConnection.Open();

                oSqlCmd.ExecuteNonQuery();
                bExist = (bool)oSqlCmd.Parameters["@bReturn"].Value;

                oSqlCmd.Dispose();
            }
            else
            {
                bExist = true;
            }
        }
        catch (Exception ex)
        {
            MessageBox("Error : " + ex.Message);
        }
        return bExist;
    }

    /// <summary>
    /// Converting boolean value to string.
    /// True = 1, False = 0
    /// </summary>
    /// <param name="isRights">boolean : value needed to be converted</param>
    /// <returns>string : result from the conversion</returns>
    public string sBoolToString(bool isRights)
    {
        if (isRights) return "1";
        else return "0";
    }

    /// <summary>
    /// Create UserRights in string format based on userrights' checkboxes
    /// </summary>
    /// <returns>string : UserRights for the user</returns>
    public string sCreateUserRights()
    {
        string sRights = "";
        for (int iCount = 0; iCount < UserRight.Length; iCount++)
        {
            if (UserRight[iCount].chkMaster.Checked)
            {
                sRights += UserRight[iCount].sFieldKey + "00".Substring(1, 2 - UserRight[iCount].iSize.ToString().Length) + UserRight[iCount].iSize.ToString();
                for (int iPos = 0; iPos < UserRight[iCount].iSize; iPos++)
                {
                    sRights += sBoolToString(UserRight[iCount].chkDetail[iPos].Checked);
                    //sRights += ddlUserAccess.SelectedIndex;
                }
            }
        }

        //if (isSuperUser &&
        //    (sGetUserID().ToUpper() == "GEMALTO") || sGetUserID().ToUpper() == "INGENICO") //Add rights as Super User
        //    sRights += "SU011";

        return sRights;
    }

    protected void InsertUserMenu(string sUserID)
    {
        try
        {
            SqlCommand oSqlCmd = new SqlCommand("spUserMenuInsert", sqlconn.MyConnection);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = sGetUserID();
            if (sqlconn.MyConnection.State == ConnectionState.Closed) sqlconn.MyConnection.Open();
            oSqlCmd.ExecuteReader();
        }
        catch (Exception ex)
        {
            //CommonClass.doWriteErrorFile(ex.Message);
        }
    }

    /// <summary>
    /// Insert new data to database.
    /// </summary>
    public void SaveData()
    {
        try
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserInsert, sqlconn.MyConnection);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sUID", SqlDbType.VarChar).Value = sGetUserID();
            oSqlCmd.Parameters.Add("@sUserName", SqlDbType.VarChar).Value = txtUserName.Text.ToString();
            oSqlCmd.Parameters.Add("@sPassword", SqlDbType.VarChar).Value = sGetEncryptPassword(txtPassword.Text.ToString());
            oSqlCmd.Parameters.Add("@sUserRights", SqlDbType.VarChar).Value = sCreateUserRights();
            if (oDataTable.Columns.Contains("Locked"))
                oSqlCmd.Parameters.Add("@sLocked", SqlDbType.VarChar).Value = "False";
            if (oDataTable.Columns.Contains("Revoke"))
                oSqlCmd.Parameters.Add("@sRevoke", SqlDbType.VarChar).Value = "False";

            if (sqlconn.MyConnection.State == ConnectionState.Closed) sqlconn.MyConnection.Open();

            string sLogDetail = "";

            using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
            {
                while (oRead.Read())
                    sLogDetail += string.Format("{0} : {1} \n", oRead["TagName"].ToString(), oRead["UserRight"].ToString());
                oRead.Close();
            }
            oSqlCmd.Dispose();

            if (txtUserID.Text.Trim().ToUpper() != "ADMIN")
            {
                //CommonClass.InputLog(sqlconn.MyConnection, "", UserData.sUserID, "", "Add User : " + txtUserID.Text.Trim().ToUpper(), sLogDetail);
                //isLogged = true;
            }
            InsertUserMenu(sGetUserID());

            MessageBox("Save Success");
        }
        catch (Exception ex)
        {
            //CommonClass.doWriteErrorFile(ex.Message);
            MessageBox("Save Error : " + ex.Message);
        }
    }

    
    /// <summary>
    /// Get Min and Max Lenght Password
    /// </summary>
    /// <param name="sDatabaseID">string : Database ID Value</param>
    /// <returns>int : Lenght of Tag</returns>
    private int iGetPasswordLenght(string sFunction)
    {
        int iLen = 0;
        SqlCommand oCmd = new SqlCommand(string.Format("SELECT {0}", sFunction), sqlconn.MyConnection);
        if (sqlconn.MyConnection.State == ConnectionState.Closed)
            sqlconn.MyConnection.Open();

        using (SqlDataReader oRead = oCmd.ExecuteReader())
        {
            if (oRead.Read())
                iLen = int.Parse(oRead[0].ToString());
            oRead.Close();
        }
        return iLen;
    }

    private bool PasswordIsComplex()
    {
        int iValidConditions = 0;
        string sPassword = txtPassword.Text.ToString();
        int iMinPawsswordLength = iGetPasswordLenght("DBO.iGetMinLengthPassword()");
        int iMaxPasswordLength = iGetPasswordLenght("DBO.iGetMaxLengthPassword()");

        if (sPassword.Length >= iMinPawsswordLength && sPassword.Length <= iMaxPasswordLength)
            iValidConditions++;

        if (iValidConditions == 0)
        {
            MessageBox(string.Format("New Passwod Lenght Min {0} and Max {1}.", iMinPawsswordLength, iMaxPasswordLength));
            return false;
        }

        foreach (char c in sPassword)
        {
            if (c >= 'a' && c <= 'z')
            {
                iValidConditions++;
                break;
            }
        }

        foreach (char c in sPassword)
        {
            if (c >= 'A' && c <= 'Z')
            {
                iValidConditions++;
                break;
            }
        }

        if (iValidConditions == 1 || iValidConditions == 2)
        {
            MessageBox("New Passwod must contain one Capital letter and one small letter.");
            return false;
        }

        foreach (char c in sPassword)
        {
            if (c >= '0' && c <= '9')
            {
                iValidConditions++;
                break;
            }
        }

        if (iValidConditions == 3)
        {
            MessageBox("New Password must contain one number.");
            return false;
        }

        if (iValidConditions == 4)
        {
            char[] special = { '@', '#', '$', '%', '^', '&', '+', '=' };

            if (sPassword.IndexOfAny(special) == -1)
            {
                MessageBox("New Password Must Contain Special Character (@,#,$,%,^,&,+,=)");
                return false;
            }
        }

        return true;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Home.aspx", false);
    }
    /// <summary>
    /// Get the text from UserID textbox.
    /// </summary>
    /// <returns>string : UserID</returns>
    public string sGetUserID()
    {
        return txtUserID.Text;
    }

    /// <summary>
    /// Condition which will be used when inserting new data
    /// </summary>
    /// <returns>string : Condition string</returns>
    public string sAddConditions()
    {
        return "WHERE UserID = '" + sGetUserID() + "'";
    }

    /// <summary>
    /// Get user's data from the database
    /// </summary>
    public void LoadDataUser()
    {
        try
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, sqlconn.MyConnection);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sAddConditions();
            if (sqlconn.MyConnection.State == ConnectionState.Closed) sqlconn.MyConnection.Open();

            SqlDataReader oRead = oSqlCmd.ExecuteReader();
            oDataTable.Clear();
            oDataTable.Load(oRead);

            oSqlCmd.Dispose();
            oRead.Dispose();
        }
        catch (Exception ex)
        {
            
        }
    }
    /// <summary>
    /// Determines if UserID entered already exists in database
    /// </summary>
    /// <returns>boolean : true if UserID unique, else false</returns>
    public bool isUserIdUnique()
    {
        LoadDataUser();
        return !(oDataTable.Rows.Count > 0 );
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            sqlconn.Connection();
            //if (isValid() && isUserIdUnique())
            if (isValid())
            {
                if (PasswordIsComplex())
                {
                    InitCheckBox();
                    SaveData();
                }
            }
        }
        catch (Exception ex)
        {
            
        }
    }

    /// <summary>
    /// Enable or disable checkboxes based on specifc user right.
    /// </summary>
    /// <param name="sKeyName">string : UserRights key</param>
    /// <param name="isEnable">boolean : status enable for the objects</param>
    public void SetEnable(string sKeyName, bool isEnable)
    {
        int iIndex = 0;
        for (int iCount = 0; iCount < UserRight.Length; iCount++)
            if (UserRight[iCount].sFieldKey == sKeyName)
            {
                iIndex = iCount;
                break;
            }

        for (int iCount = 0; iCount < UserRight[iIndex].iSize; iCount++)
            UserRight[iIndex].chkDetail[iCount].Enabled = isEnable;
    }

    /// <summary>
    /// Initiate the checkboxes for the CheckBox_Struct.
    /// </summary>
    public void InitCheckBox()
    {
        InitArray();
        InitCheckBoxStruct();
    }

    protected void chkAdministrator_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkAdministrator.Checked);
    }
    

    protected void chkdtMonitoringExport_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkdtMonitoringExport.Checked);
    }

    protected void chkdtMonitoringDelete_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkdtMonitoringDelete.Checked);
    }

    protected void chkHbMonitoring_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkHbMonitoring.Checked);
    }

    protected void chkHbMonitoringExport_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkHbMonitoringExport.Checked);
    }

    protected void chkHbMonitoringDelete_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkHbMonitoringDelete.Checked);
    }

    protected void chkdtMonitoring_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkdtMonitoring.Checked);
    }

    protected void chkSPV_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W2", chkSPV.Checked);
    }
    
    protected void chkOP_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W2", chkOP.Checked);
    }
    
    protected void chkGS_CheckedChanged(object sender, EventArgs e)
    {

        InitCheckBox();
        SetEnable("W2", chkGS.Checked);
    }
    
    protected void chkUserManagement_CheckedChanged(object sender, EventArgs e)
    {

        InitCheckBox();
        SetEnable("W1", chkUserManagement.Checked);
    }
    protected void chkUserRegisterMenu_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkUserRegisterMenu.Checked);
    }
    protected void chkUserRegisterApprovedMenu_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkUserRegisterApprovedMenu.Checked);
    }
    protected void chkUserRegisterAccess_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkUserRegisterAccess.Checked);
    }
    protected void chkUserRegisterActived_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkUserRegisterActived.Checked);
    }
    protected void chkUserRegisterApproved_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkUserRegisterApproved.Checked);
    }
    protected void chkUserRegisterLocked_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkUserRegisterLocked.Checked);
    }
    protected void chkUserRegisterDelete_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkUserRegisterDelete.Checked);
    }
    protected void chkUserList_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkUserList.Checked);
    }
    protected void chkActivityLog_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkActivityLog.Checked);
    }
    protected void chkActivityLogLogin_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkActivityLogLogin.Checked);
    }
    protected void chkUploadData_CheckedChanged(object sender, EventArgs e)
    {
        InitCheckBox();
        SetEnable("W1", chkUploadData.Checked);
    }
    
}