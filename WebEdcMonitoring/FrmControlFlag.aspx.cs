﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using WebClass;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using InSysClass;

public partial class FrmControlFlag : System.Web.UI.Page
{
    protected ConnectionClass oSqlConn = new ConnectionClass();
    protected string sUserDomain = "";
    protected string sUserSession = "";
    DataTable oDataTableInitLogConn = new DataTable();
    protected DataTable oDataTable = new DataTable();
    protected static string sUserID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserSession"] != null)
            {
                try
                {
                    sUserID = Session["UserID"].ToString();
                    oSqlConn.Connection();
                    UserPrivilege.sPrivilege = WebEdcMonitoringClass.sGetUserRight(oSqlConn.MyConnection, sUserID);
                    if ((UserPrivilege.IsAllowed(PrivilegeCode.W1)))
                    {
                        if ((UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserRegisterApproved)))
                        {
                            LoadData();
                            ControlObjectDetail("OPEN");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Traces.Write("Error: " + ex.StackTrace);
                }

            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }


    }

    protected void SaveOrUpdate(object sender, EventArgs e)
    {

        if (Session["UserSession"] != null)
        {
            Button saveOrUpdate = (sender as Button);
            RepeaterItem item = saveOrUpdate.NamingContainer as RepeaterItem;
            string tbItemName = (item.FindControl("tbItemName") as TextBox).Text;
            string tbFlag = (item.FindControl("tbFlag") as TextBox).Text;
            string tbHexString = (item.FindControl("tbHexString") as TextBox).Text;
            string tbDescription = (item.FindControl("tbDescription") as TextBox).Text;

            oSqlConn.Connection();
            switch (saveOrUpdate.CommandName)
            {
                case "Update":
                    this.SaveData(tbItemName, tbFlag, tbHexString, tbDescription);
                    break;
                case "Save":
                     this.SaveData(tbItemName, tbFlag, tbHexString, tbDescription);
                    break;
            }

            Response.Redirect("FrmControlFlag.aspx");
        }
        else Response.Redirect("~/Account/Login.aspx");



    }
    protected void btnClickDelete(object sender, EventArgs e)
    {

        if (Session["UserSession"] != null)
        {
            Button btnClickDelete = (sender as Button);
            RepeaterItem item = btnClickDelete.NamingContainer as RepeaterItem;
            string tbItemName = (item.FindControl("tbItemName") as TextBox).Text;
            string tbFlag = (item.FindControl("tbFlag") as TextBox).Text;
            string tbHexString = (item.FindControl("tbHexString") as TextBox).Text;
            string tbDescription = (item.FindControl("tbDescription") as TextBox).Text;

            oSqlConn.Connection();
            switch (btnClickDelete.CommandName)
            {
                case "Delete":
                    this.DeleteData(oSqlConn.MyConnection, tbItemName);
                    break;
            }

            Response.Redirect("FrmControlFlag.aspx");
        }
        else Response.Redirect("~/Account/Login.aspx");


    }
    private void SaveData(string ItemName, string Flag, string HexString, string Description)
    {
        try
        {
            using (SqlCommand Cmd = new SqlCommand("spControl_Flag_Update", oSqlConn.MyConnection))
            {
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@ItemName", SqlDbType.VarChar).Value = ItemName;
                Cmd.Parameters.Add("@Flag", SqlDbType.VarChar).Value = Flag;
                Cmd.Parameters.Add("@HexString", SqlDbType.VarChar).Value = HexString;
                Cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = Description;

                if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();

                Cmd.ExecuteNonQuery();
                Cmd.Dispose();

            }
        }
        catch (Exception ex)
        {
            Traces.Write("Error: " + ex.StackTrace);
            Messagebox("Error : " + ex.Message);
        }
    }
    private void DeleteData(SqlConnection oSqlConn, string tbItemName)
    {
        string sQuery = string.Format("DELETE dbo.tbControlFlag");
        sQuery += string.Format(" Where ItemName = '{0}' ", tbItemName);
        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
        using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
        {
            oSqlCmd.ExecuteNonQuery();
            oSqlCmd.Dispose();
        }
    }
    protected void Messagebox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }


    protected string sCondition()
    {
        return " order by ItemName asc";
    }
    protected void LoadData()
    {
        try
        {
            oSqlConn.Connection();

            SqlCommand oSqlCmd = new SqlCommand("spControlFlag_Browse", oSqlConn.MyConnection);
            oSqlCmd.CommandType = CommandType.StoredProcedure;

            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();
            if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();

            SqlDataReader oAdapt = oSqlCmd.ExecuteReader();

            oDataTableInitLogConn.Load(oAdapt);
            repeaterData.DataSource = oDataTableInitLogConn;
            repeaterData.DataBind();

              oSqlCmd.Dispose();
            oAdapt.Dispose();
            oSqlConn.MyConnection.Close();
        }

        catch (Exception ex)
        {
            Traces.Write("Error: " + ex.StackTrace);
            Messagebox("Invalid data");
        }
    }
    private void ControlObjectDetail(string mode)
    {
        oSqlConn.Connection();

        UserPrivilege.sPrivilege = WebEdcMonitoringClass.sGetUserRight(oSqlConn.MyConnection, Session["UserID"].ToString());
        foreach (RepeaterItem item in repeaterData.Items)
        {
            Button btnSave = item.FindControl("btnSave") as Button;
            Button btnDelete = item.FindControl("btnDelete") as Button;


            btnSave.Visible = true;
            btnDelete.Visible = true;
        }

    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        if (Session["UserSession"] != null)
        {
            Response.Redirect("~/Home.aspx");
        }
        else Response.Redirect("~/Account/Login.aspx");

    }
}