﻿<%@ Application Language="C#" %>
<%@ Import Namespace="NewInSys" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Routing" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        RouteConfig.RegisterRoutes(RouteTable.Routes);
        BundleConfig.RegisterBundles(BundleTable.Bundles);
    }
    //private int mnSessionMinutes = 30;
    //private string msSessionCacheName = "SessionTimeOut";

    //public void InsertSessionCacheItem(string sUserID)
    //{

    //    try
    //    {
    //        if (this.GetSessionCacheItem(sUserID) != "") { return; }
    //        CacheItemRemovedCallback oRemove = new CacheItemRemovedCallback(this.SessionEnded);
    //        System.Web.HttpContext.Current.Cache.Insert(msSessionCacheName + sUserID, sUserID, null, DateTime.MaxValue,

    //        TimeSpan.FromMinutes(mnSessionMinutes), CacheItemPriority.High, oRemove);
    //    }
    //    catch (Exception e) { Response.Write(e.Message); }
    //}

    //public string GetSessionCacheItem(string sUserID)
    //{
    //    string sRet = "";

    //    try
    //    {
    //        sRet = (string)System.Web.HttpContext.Current.Cache[msSessionCacheName + sUserID];
    //        if (sRet == null) { sRet = ""; }
    //    }
    //    catch (Exception) { }
    //    return sRet;
    //}


    //public void SessionEnded(string key, object val, CacheItemRemovedReason r)
    //{

    //    string sUserID = "";
    //    string sSessionTest = "";

    //    try
    //    {
    //        sSessionTest = Session["Test"].ToString();
    //    }
    //    catch (Exception e) { sSessionTest = e.Message; }

    //    try
    //    {

    //        //sUserID = (string)val;
    //        //// Make sure your SMTP service has started in order for this to work

    //        //System.Web.Mail.MailMessage message = new System.Web.Mail.MailMessage();

    //        //message.Body = "your session has ended for user : " + sUserID + " - Session String: " + sSessionTest;
    //        //message.To = "your email goes here";
    //        //message.From = "info@eggheadcafe.com";
    //        //message.BodyFormat = System.Web.Mail.MailFormat.Text;
    //        //message.Subject = "Session On End";

    //        //System.Web.Mail.SmtpMail.Send(message);

    //        //message = null;

    //    }
    //    catch (Exception) { }

    //}

</script>
