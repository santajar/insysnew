﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using WebClass;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using InSysClass;

public partial class FrmUserEdit : System.Web.UI.Page
{
    protected ConnectionClass oSqlConn = new ConnectionClass();
    protected string sUserDomain = "";
    protected string sUserSession = "";
    DataTable oDataTableInitLogConn = new DataTable();
    protected DataTable oDataTable = new DataTable();
    protected static string sUserID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserSession"] != null)
            {
                try
                {
                    sUserID = Session["UserID"].ToString();
                    oSqlConn.Connection();
                    UserPrivilege.sPrivilege = WebEdcMonitoringClass.sGetUserRight(oSqlConn.MyConnection, sUserID);
                    if ((UserPrivilege.IsAllowed(PrivilegeCode.W1)))
                    {
                        if ((UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserRegisterApproved)))
                        {
                            LoadData();
                            ControlObjectDetail("OPEN");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Traces.Write("Error: " + ex.StackTrace);
                    MessageBox(ex.Message);
                }


            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }


    }
    protected void MessageBox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }
    protected void SaveOrUpdate(object sender, EventArgs e)
    {

        if (Session["UserSession"] != null)
        {
            try
            {
                Button saveOrUpdate = (sender as Button);
                RepeaterItem item = saveOrUpdate.NamingContainer as RepeaterItem;
                string tb1 = (item.FindControl("tb1") as Label).Text;
                string tb3 = (item.FindControl("tb3") as TextBox).Text;
                string tb4 = (item.FindControl("tb4") as TextBox).Text;

                oSqlConn.Connection();
                switch (saveOrUpdate.CommandName)
                {
                    case "Update":
                        this.SaveData(tb1, tb3, tb4);
                        break;
                    case "Save":
                        this.SaveData(tb1, tb3, tb4);
                        break;
                }

                Response.Redirect("FrmUserEdit.aspx");
            }
            catch (Exception ex)
            {
                Traces.Write("Error: " + ex.StackTrace);
                MessageBox("Please Contact Administrator "+ex.Message);
            }

        }
        else Response.Redirect("~/Account/Login.aspx");



    }
    private void SaveData(string tb1, string tb3, string tb4)
    {
        try
        {
            using (SqlCommand Cmd = new SqlCommand("spUser_Edit_Update", oSqlConn.MyConnection))
            {
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@UserDomain", SqlDbType.VarChar).Value = tb1;
                Cmd.Parameters.Add("@LastLogin", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", tb3);
                Cmd.Parameters.Add("@TotalInvalidPassword", SqlDbType.VarChar).Value = tb4;

                if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();

                Cmd.ExecuteNonQuery();
                Cmd.Dispose();

            }
        }
        catch (Exception ex)
        {
            Traces.Write("Error: " + ex.StackTrace);
            Messagebox("Error : " + ex.Message);
        }
    }
    private void DeleteData(SqlConnection oSqlConn, string tbItemName)
    {
        try
        {
            string sQuery = string.Format("DELETE dbo.tbControlFlag");
            sQuery += string.Format(" Where ItemName = '{0}' ", tbItemName);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
            }
        }
        catch (Exception ex)
        {
            Traces.Write("Error: " + ex.StackTrace);
            MessageBox("Please Contact Administrator "+ex.Message);
        }

    }
    protected void Messagebox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }


    protected string sCondition()
    {
        return " order by LastLogin desc";
    }
    protected void LoadData()
    {
        try
        {
            oSqlConn.Connection();

            SqlCommand oSqlCmd = new SqlCommand("spUser_Domain_Browse", oSqlConn.MyConnection);
            oSqlCmd.CommandType = CommandType.StoredProcedure;

            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();
            if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();

            SqlDataReader oAdapt = oSqlCmd.ExecuteReader();

            oDataTableInitLogConn.Load(oAdapt);
            repeaterData.DataSource = oDataTableInitLogConn;
            repeaterData.DataBind();

              oSqlCmd.Dispose();
            oAdapt.Dispose();
            oSqlConn.MyConnection.Close();
        }
        catch (Exception ex)
        {
            Traces.Write("Error: " + ex.StackTrace);
            Messagebox("Invalid data");
        }
    }
    private void ControlObjectDetail(string mode)
    {
        oSqlConn.Connection();

        UserPrivilege.sPrivilege = WebEdcMonitoringClass.sGetUserRight(oSqlConn.MyConnection, Session["UserID"].ToString());
        foreach (RepeaterItem item in repeaterData.Items)
        {
            Button btnSave = item.FindControl("btnSave") as Button;


            btnSave.Visible = true;
        }

    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        if (Session["UserSession"] != null)
        {
            Response.Redirect("~/FrmRegistrationApprove.aspx");
        }
        else Response.Redirect("~/Account/Login.aspx");

    }
}