﻿<%@ Page Title="Register Approve" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="FrmControlFlag.aspx.cs" Inherits="FrmControlFlag" %>

<asp:Content ID="Content1" ContentPlaceHolderID="script_css" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            font-size: large;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="jumbotron"></div>
      <div class="panel panel-default">
        <div class="auto-style1" data-toggle="collapse" data-target="#filter"><strong>CONTROL FLAG</strong></div>
        <div class="panel-body" id="filter">
            <div class="row">
            <div class="col-md-12 padding-top">  
                    <div class="panel panel-default">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group pull-left padding-center">
                            <asp:Button ID="Cancel" runat="server" Text="Back" CssClass="btn btn-info" OnClick="Cancel_Click" />                                    
     
                        </div>
                    </div>
                    </div>
                </div>

            </div>
            </div>
            <div class="row">
                    <div class="col-md-12">     
                        <div class="table-responsive" overflow: scroll; >       
      
                                      
                            <asp:Repeater ID="repeaterData" runat="server" >
                                <HeaderTemplate>
                                    <div class="template">
                                    <table id="dataTable" class="display responsive no-wrap" width="100%" border="0">
                                        <thead>
                                            <tr>
                                                <th>Item Name</th>
                                                <th>Flag</th>
                                                <th>Hexstring</th>
                                                <th>Description</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                 </HeaderTemplate>
                                 <ItemTemplate>
                                                
                                        <tr class="input-sm">
                                            <td>
                                                <asp:TextBox ID="tbItemName" CssClass="control-label input-sm" runat="server" Text='<%# Eval("ItemName") %>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbFlag" CssClass="control-label input-sm" runat="server" Text='<%# Eval("Flag") %>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbHexString" CssClass="control-label input-sm" runat="server" Text='<%# Eval("HexString") %>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbDescription" CssClass="control-label input-sm" runat="server" Text='<%# Eval("Description") %>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnSave" Text="Save" CommandName="Save" OnClick="SaveOrUpdate" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnDelete" Text="Delete" CommandName="Delete" OnClick="btnClickDelete" runat="server" />
                                            </td>
                                        </tr>
                                                
                                  </ItemTemplate>
                                    <FooterTemplate>
                                    </tbody>
                                    </table>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
            </div>   

        </div>        
    </div>
    <div>


    </div>
</asp:Content>

