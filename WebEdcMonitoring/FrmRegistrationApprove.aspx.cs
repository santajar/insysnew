﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using WebClass;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using InSysClass;

public partial class FrmRegistrationApprove : System.Web.UI.Page
{
    protected ConnectionClass oSqlConn = new ConnectionClass();
    protected string sUserDomain = "";
    protected string sUserSession = "";
    DataTable oDataTableInitLogConn = new DataTable();
    protected DataTable oDataTable = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HttpCookie UserInfo = Request.Cookies["UserInfo"];

            if (UserInfo != null)
            {
                if (UserInfo.Values.Count != 0)
                {
                    if (Session.Count != 0)
                    {
                        if (Session["UserSession"] != null)
                        {
                            LoadData();
                            ControlObjectDetail("OPEN");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Account/Login.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }
        
    }

    protected void SaveOrUpdate(object sender, EventArgs e)
    {
        Button saveOrUpdate = (sender as Button);
        RepeaterItem item = saveOrUpdate.NamingContainer as RepeaterItem;
        string UserDomain = (item.FindControl("lblUserDomain") as Label).Text;
        string UserIDAccess = (item.FindControl("ddlUserIDAccess") as DropDownList).Text;
        string Actived = (item.FindControl("ddlActived") as DropDownList).Text;
        string Approved = (item.FindControl("ddlApproved") as DropDownList).Text;
        string Locked = (item.FindControl("ddlLocked") as DropDownList).Text;
        string sUserName = (item.FindControl("lblUserName") as Label).Text;
        string sEmail = (item.FindControl("lblEmail") as Label).Text;
        string sApprovedDate = string.Format("{0:MM/dd/yyyy H:mm:ss}", DateTime.Now);

        oSqlConn.Connection();
        switch (saveOrUpdate.CommandName)
        {
            case "Update":
                if (WebEdcMonitoringClass.isGetUserDomainActived(oSqlConn.MyConnection, UserDomain) != int.Parse(Actived))
                    SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Actived", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
                else if (WebEdcMonitoringClass.isGetUserDomainModifUserRight(oSqlConn.MyConnection, UserDomain) != UserIDAccess.ToString())
                    SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Modified (Access)", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
                else if (WebEdcMonitoringClass.isGetUserDomainapprove(oSqlConn.MyConnection, UserDomain) != int.Parse(Approved))
                    SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Approved", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
                else if (WebEdcMonitoringClass.isGetUserDomainapprove(oSqlConn.MyConnection, UserDomain) != int.Parse(Locked))
                    SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Modified (Lock/UnLock)", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);

                this.SaveData(UserDomain, UserIDAccess, Actived, Approved, Locked);
                break;
            case "Save":
                if (WebEdcMonitoringClass.isGetUserDomainActived(oSqlConn.MyConnection, UserDomain) != int.Parse(Actived))
                    SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Actived", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
                else if (WebEdcMonitoringClass.isGetUserDomainModifUserRight(oSqlConn.MyConnection, UserDomain) != UserIDAccess.ToString())
                    SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Modified (Access)", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
                else if (WebEdcMonitoringClass.isGetUserDomainapprove(oSqlConn.MyConnection, UserDomain) != int.Parse(Approved))
                    SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Approved", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
                else if (WebEdcMonitoringClass.isGetUserDomainapprove(oSqlConn.MyConnection, UserDomain) != int.Parse(Locked))
                    SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Modified (Lock/UnLock)", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
                this.SaveData(UserDomain, UserIDAccess, Actived, Approved, Locked);
                break;
        }

        Response.Redirect("FrmRegistrationApprove.aspx");

    }
    protected void btnClickDelete(object sender, EventArgs e)
    {
        Button btnClickDelete = (sender as Button);
        RepeaterItem item = btnClickDelete.NamingContainer as RepeaterItem;
        string UserDomain = (item.FindControl("lblUserDomain") as Label).Text;
        string UserIDAccess = (item.FindControl("ddlUserIDAccess") as DropDownList).Text;
        string Actived = (item.FindControl("ddlActived") as DropDownList).Text;
        string Approved = (item.FindControl("ddlApproved") as DropDownList).Text;
        string Locked = (item.FindControl("ddlLocked") as DropDownList).Text;
        string sUserName = (item.FindControl("lblUserName") as Label).Text;
        string sEmail = (item.FindControl("lblEmail") as Label).Text;
        string sApprovedDate = string.Format("{0:MM/dd/yyyy H:mm:ss}", DateTime.Now);

        oSqlConn.Connection();
        switch (btnClickDelete.CommandName)
        {
            case "Delete":
                this.DeleteData(oSqlConn.MyConnection,UserDomain);
                SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User Web has been Delete", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
                break;
        }

        Response.Redirect("FrmRegistrationApprove.aspx");

    }
    private void SaveData(string UserDomain, string UserIDAccess, string Actived, string Approved, string Locked)
    {
        try
        {
            using (SqlCommand Cmd = new SqlCommand("spUSER_DOMAIN_UPDATE", oSqlConn.MyConnection))
            {
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Parameters.Add("@UserDomain", SqlDbType.VarChar).Value = UserDomain.ToString();
                Cmd.Parameters.Add("@UserIDAccess", SqlDbType.VarChar).Value = UserIDAccess.ToString();
                Cmd.Parameters.Add("@Actived", SqlDbType.VarChar).Value = Actived;
                Cmd.Parameters.Add("@Approved", SqlDbType.VarChar).Value = Approved;
                Cmd.Parameters.Add("@Locked", SqlDbType.VarChar).Value = Locked;

                if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();

                sUserSession = Session["UserSession"].ToString();
                if (WebEdcMonitoringClass.isGetUserDomainapprove(oSqlConn.MyConnection, UserDomain) == 1)
                {
                    if (WebEdcMonitoringClass.isGetUserDomainActived(oSqlConn.MyConnection, UserDomain) != int.Parse(Actived))
                        WebEdcMonitoringClass.InputLog(oSqlConn.MyConnection, "", sUserSession, "", CommonMessage.sEnabledisableuserDomain + "=>" + UserDomain + "=>" + UserIDAccess.ToString() + "=>" + Actived , "");
                    else if (WebEdcMonitoringClass.isGetUserDomainModifUserRight(oSqlConn.MyConnection, UserDomain) != UserIDAccess.ToString())
                        WebEdcMonitoringClass.InputLog(oSqlConn.MyConnection, "", sUserSession, "", CommonMessage.sModifUserProfileRight + "=>" + UserDomain + "=>" + UserIDAccess.ToString(), "");
                    else if (WebEdcMonitoringClass.isGetUserDomainapprove(oSqlConn.MyConnection, UserDomain) != int.Parse(Approved))
                        WebEdcMonitoringClass.InputLog(oSqlConn.MyConnection, "", sUserSession, "", CommonMessage.sEnabledisableuserDomain + "=>" + UserDomain + "=>" + UserIDAccess.ToString(), "");
                    else if (WebEdcMonitoringClass.isGetUserDomainapprove(oSqlConn.MyConnection, UserDomain) != int.Parse(Locked))
                        WebEdcMonitoringClass.InputLog(oSqlConn.MyConnection, "", sUserSession, "", CommonMessage.sEnabledisableuserDomainLock + "=>" + UserDomain + "=>" + UserIDAccess.ToString(), "");
                }
                else WebEdcMonitoringClass.InputLog(oSqlConn.MyConnection, "", sUserSession, "", CommonMessage.sCreateUserProfile + "=>" + UserDomain + "=>" + UserIDAccess.ToString(), "");

                Cmd.ExecuteNonQuery();
                Cmd.Dispose();

            }
        }
        catch (Exception ex)
        {
            Messagebox("Error : " + ex.Message);
        }
    }
    private void DeleteData(SqlConnection oSqlConn, string UserDomain)
    {
        string sQuery = string.Format("DELETE dbo.tbUserLoginDomain");
        sQuery += string.Format(" Where UserDomain = '{0}' ", UserDomain);
        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
        using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
        {
            sUserSession = Session["UserSession"].ToString();
            WebEdcMonitoringClass.InputLog(oSqlConn, "", sUserSession, "", CommonMessage.sDeleteUserProfile + "=>" + UserDomain , "");
            oSqlCmd.ExecuteNonQuery();
            oSqlCmd.Dispose();
        }
    }

    protected void SendMailApprove(string sFrom, string MailSubject, string SendTo, string [] CCTO, string sRequest, string sDate, string sUserDomain, string sUserAccess, string sEmail)
    {
        string strUrl = "";
        Uri url = HttpContext.Current.Request.Url;
        string Url = url.GetLeftPart(UriPartial.Authority);
        string HtmlBody = "";
        try
        {
            string strCorporateName = "";
            strCorporateName = WebEdcMonitoringClass.sGetCorporateName(oSqlConn.MyConnection);

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(sFrom, "Notification Email System " + strCorporateName);
            msg.Subject = MailSubject;
            msg.To.Add(new MailAddress(SendTo));
            if (CCTO.Length != 0)
            {
                for (int i = 0; i <= CCTO.Length - 1; i++)
                {
                    string[] strCC = CCTO[i].Split(';');
                    msg.CC.Add(strCC[0].Trim());
                }
            }
            string[] Bcc = WebEdcMonitoringClass.LoadRecipientAdmin(oSqlConn.MyConnection, "SUPADMIN");
            if (Bcc.Length != 0)
            {
                for (int i = 0; i <= Bcc.Length - 1; i++)
                {
                    string[] strCC = Bcc[i].Split(';');
                    msg.Bcc.Add(strCC[0].Trim());
                }
            }

            strUrl = WebEdcMonitoringClass.sGetURLWeb(oSqlConn.MyConnection);

            HtmlBody = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
                        "<html xmlns='http://www.w3.org/1999/xhtml'>" +
                        "<head>" +
                        "<title></title>" +
                        "</head>" +
                        "<body>" +
                        "<div style='font-size:12px; font-family: Calibri;'>" +
                        "<div>" +
                        "<br /><br />" +
                        "Dear&nbsp;<b>" + sRequest + ",</b><br /><br />" +
                        "User request has been approved with details below:<br />" +
                        "<table style='font-size: 11px; font-family: Tahoma, Geneva, sans-serif'>" +
                        "<tr><td>Request From</td><td>:</td><td>" + sRequest + "</td></tr>" +
                        "<tr><td>Date Approved</td><td>:</td><td>" + sDate + "</td></tr>" +
                        "<tr><td>User Domain</td><td>:</td><td>" + sUserDomain + "</td></tr>" +
                        "<tr><td>User Access Type</td><td>:</td><td>" + sUserAccess + "</td></tr>" +
                        "<tr><td>Email</td><td>:</td><td>" + sEmail + "</td></tr>" +
                        "</table><br />" +
                        "Please click folowing link to view the Application:<br /><br />" +
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                        "<a href='" + strUrl + "'>" + strUrl + "</a>" +
                        "<br />" +
                        "<br />" +
                        "* try to copy and paste the link into your web browser if it&#39;s not working.</div>" +
                        "<br />" +
                        "<br />" +
                        "Best Regards,<br /><br />" +
                        "Email System " + strCorporateName +
                        "<div>" +
                        "<br />" +
                        "<br />" +
                        "<b>This is an automatic email notifier from Notification System " + strCorporateName + "<br />You don&#39;t need to reply it.</b></div>" +
                        "</div>" +
                        "</div>" +
                        "</body>" +
                        "</html>";
            msg.IsBodyHtml = true;
            msg.Body = HtmlBody;
            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), WebEdcMonitoringClass.sGetSenderPassword(oSqlConn.MyConnection));
            //client.Port = 587; // You can use Port 25 if 587 is blocked (mine is!)
            client.Port = WebEdcMonitoringClass.iGetSenderPort(oSqlConn.MyConnection); // You can use Port 25 if 587 is blocked (mine is!)
            //client.Host = "smtp.office365.com";
            client.Host = WebEdcMonitoringClass.sGetSenderHost(oSqlConn.MyConnection);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Send(msg);
        }
        catch (Exception ex)
        {
            //Response.Write("Could not send the e-mail - error: " + ex.Message);
            Messagebox("Could not send the e-mail - error: : " + ex.Message);

        }

    }
    protected void Messagebox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }


    protected string sCondition()
    {
        return " order by Approved asc";
    }
    protected void LoadData()
    {
        try
        {

            oSqlConn.Connection();

            SqlCommand oSqlCmd = new SqlCommand("spUSER_DOMAIN_BROWSE", oSqlConn.MyConnection);
            oSqlCmd.CommandType = CommandType.StoredProcedure;

            oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();
            if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();

            SqlDataReader oAdapt = oSqlCmd.ExecuteReader();

            oDataTableInitLogConn.Load(oAdapt);
            repeaterData.DataSource = oDataTableInitLogConn;
            repeaterData.DataBind();

            foreach (RepeaterItem item in repeaterData.Items)
            {
                DropDownList ddlUserIDAccess = item.FindControl("ddlUserIDAccess") as DropDownList;
                ddlUserIDAccess.DataSource = oDataTableInitLogConn;
                ddlUserIDAccess.DataValueField = "UserIDAccess";
                ddlUserIDAccess.DataTextField = "UserIDAccess";
                ddlUserIDAccess.DataBind();

                DropDownList ddlActived = item.FindControl("ddlActived") as DropDownList;
                ddlActived.DataSource = oDataTableInitLogConn;
                ddlActived.DataValueField = "Actived";
                ddlActived.DataTextField = "Actived";
                ddlActived.DataBind();

                DropDownList ddlApproved = item.FindControl("ddlApproved") as DropDownList;
                ddlApproved.DataSource = oDataTableInitLogConn;
                ddlApproved.DataValueField = "Approved";
                ddlApproved.DataTextField = "Approved";
                ddlApproved.DataBind();

                DropDownList ddlLocked = item.FindControl("ddlLocked") as DropDownList;
                ddlLocked.DataSource = oDataTableInitLogConn;
                ddlLocked.DataValueField = "Locked";
                ddlLocked.DataTextField = "Locked";
                ddlLocked.DataBind();
            }

            oSqlCmd.Dispose();
            oAdapt.Dispose();
            oSqlConn.MyConnection.Close();
        }

        catch (Exception)
        {
            Messagebox("Invalid data");
        }
    }
    private void ControlObjectDetail(string mode)
    {
        oSqlConn.Connection();

        UserPrivilege.sPrivilege = WebEdcMonitoringClass.sGetUserRight(oSqlConn.MyConnection, Session["UserID"].ToString());
        foreach (RepeaterItem item in repeaterData.Items)
        {
            Label lbl_UserIDAccess = item.FindControl("lbl_UserIDAccess") as Label;
            Label lblActived = item.FindControl("lblActived") as Label;
            Label lblApproved = item.FindControl("lblApproved") as Label;
            Label lblLocked = item.FindControl("lblLocked") as Label;
            Button btnSave = item.FindControl("btnSave") as Button;
            Button btnDelete = item.FindControl("btnDelete") as Button;
            DropDownList ddlUserIDAccess = item.FindControl("ddlUserIDAccess") as DropDownList;
            DropDownList ddlActived = item.FindControl("ddlActived") as DropDownList;
            DropDownList ddlApproved = item.FindControl("ddlApproved") as DropDownList;
            DropDownList ddlLocked = item.FindControl("ddlLocked") as DropDownList;

            if (mode == "OPEN")
            {

                bool sStatusDelete = true;
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W1)))
                    sStatusDelete = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserRegisterDelete));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W2)))
                    sStatusDelete = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkUserRegisterDelete));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W3)))
                    sStatusDelete = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkUserRegisterDelete));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W4)))
                    sStatusDelete = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkUserRegisterDelete));

                lbl_UserIDAccess.Visible = false;
                ddlUserIDAccess.Visible = true;
                ddlUserIDAccess.Enabled = false;
                LoadItem(ddlUserIDAccess);
                foreach (ListItem listItem1 in ddlUserIDAccess.Items)
                {
                    if (listItem1.Text == lbl_UserIDAccess.Text)
                    {
                        ddlUserIDAccess.Items.FindByValue(listItem1.Value).Selected = true;
                    }
                }

                lblActived.Visible = false;
                ddlActived.Visible = true;
                ddlActived.Enabled = false;
                ddlActived.Items.Clear();
                LoadItemActived(ddlActived);
                foreach (ListItem listItem2 in ddlActived.Items)
                {
                    if (listItem2.Text == lblActived.Text)
                    {
                        ddlActived.Items.FindByValue(listItem2.Value).Selected = true;
                    }
                }
                lblApproved.Visible = false;
                ddlApproved.Visible = true;
                ddlApproved.Enabled = false;
                ddlApproved.Items.Clear();
                LoadItemApproved(ddlApproved);
                foreach (ListItem listItem3 in ddlApproved.Items)
                {
                    if (listItem3.Text == lblApproved.Text)
                    {
                        ddlApproved.Items.FindByValue(listItem3.Value).Selected = true;

                    }
                }

                lblLocked.Visible = false;
                ddlLocked.Visible = true;
                ddlLocked.Enabled = false;
                ddlLocked.Items.Clear();
                LoadItemLocked(ddlLocked);
                foreach (ListItem listItem4 in ddlLocked.Items)
                {
                    if (listItem4.Text == lblLocked.Text)
                    {
                        ddlLocked.Items.FindByValue(listItem4.Value).Selected = true;

                    }
                }

                btnSave.Visible = false;
                //bSave.Visible = false;
                btnDelete.Visible = true;
                btnDelete.Enabled = sStatusDelete;
            }
            else if (mode == "EDIT")
            {
                bool sStatusAccess = true;
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W1)))
                    sStatusAccess = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserRegisterAccess));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W2)))
                    sStatusAccess = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkUserRegisterAccess));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W3)))
                    sStatusAccess = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkUserRegisterAccess));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W4)))
                    sStatusAccess = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkUserRegisterAccess));

                lbl_UserIDAccess.Visible = false;
                ddlUserIDAccess.Visible = true;
                ddlUserIDAccess.Enabled = sStatusAccess;
                LoadItem(ddlUserIDAccess);
                foreach (ListItem listItem1 in ddlUserIDAccess.Items)
                {
                    if (listItem1.Text == lbl_UserIDAccess.Text)
                    {
                        ddlUserIDAccess.Items.FindByValue(listItem1.Value).Selected = true;
                    }
                }

                bool sStatusActive = true;
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W1)))
                    sStatusActive = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserRegisterActived));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W2)))
                    sStatusActive = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkUserRegisterActived));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W3)))
                    sStatusActive = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkUserRegisterActived));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W4)))
                    sStatusActive = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkUserRegisterActived));

                lblActived.Visible = false;
                ddlActived.Enabled = sStatusActive;
                ddlActived.Visible = true;
                ddlActived.Items.Clear();
                LoadItemActived(ddlActived);
                foreach (ListItem listItem2 in ddlActived.Items)
                {
                    if (listItem2.Text == lblActived.Text)
                    {
                        ddlActived.Items.FindByValue(listItem2.Value).Selected = true;
                    }
                }


                bool sStatusApproved = true;
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W1)))
                    sStatusApproved = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserRegisterApproved));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W2)))
                    sStatusApproved = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkUserRegisterApproved));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W3)))
                    sStatusApproved = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkUserRegisterApproved));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W4)))
                    sStatusApproved = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkUserRegisterApproved));

                lblApproved.Visible = false;
                ddlApproved.Visible = true;
                ddlApproved.Enabled = sStatusApproved;
                ddlApproved.Visible = true;
                ddlApproved.Items.Clear();
                LoadItemApproved(ddlApproved);
                foreach (ListItem listItem3 in ddlApproved.Items)
                {
                    if (listItem3.Text == lblApproved.Text)
                    {
                        ddlApproved.Items.FindByValue(listItem3.Value).Selected = true;

                    }
                }


                bool sStatusLocked = true;
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W1)))
                    sStatusLocked = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserRegisterLocked));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W2)))
                    sStatusLocked = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkUserRegisterLocked));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W3)))
                    sStatusLocked = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkUserRegisterLocked));
                if ((UserPrivilege.IsAllowed(PrivilegeCode.W4)))
                    sStatusLocked = (UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkUserRegisterLocked));

                lblLocked.Visible = false;
                ddlLocked.Visible = true;
                ddlLocked.Enabled = sStatusLocked;
                ddlLocked.Visible = true;
                ddlLocked.Items.Clear();
                LoadItemLocked(ddlLocked);
                foreach (ListItem listItem4 in ddlLocked.Items)
                {
                    if (listItem4.Text == lblLocked.Text)
                    {
                        ddlLocked.Items.FindByValue(listItem4.Value).Selected = true;

                    }
                }

                btnSave.Visible = true;
                //bSave.Visible = true;
                btnDelete.Visible = false;
            }
        }
    }

    protected string sConditionUserDomain()
    {
        return " WHERE Locked=0";
    }

    private void LoadItem(DropDownList ddl)
    {
        oSqlConn.Connection();

        SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn.MyConnection);
        oSqlCmd.CommandType = CommandType.StoredProcedure;

        oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionUserDomain();
        if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();

        SqlDataReader oAdapt = oSqlCmd.ExecuteReader();

        DataTable oDataTable = new DataTable();
        oDataTable.Load(oAdapt);
        ddl.DataSource = oDataTable;

        ddl.DataValueField = "UserID";
        ddl.DataTextField = "UserID";
        ddl.DataBind();

        ddl.Items.Insert(0, new ListItem("", "0"));
    }
    private void LoadItemApproved(DropDownList ddl)
    {
        List<ListItem> items = new List<ListItem>();
        items.Add(new ListItem("False", "0"));
        items.Add(new ListItem("True", "1"));
        items.Sort(delegate (ListItem item1, ListItem item2) { return item1.Text.CompareTo(item2.Text); });
        ddl.Items.AddRange(items.ToArray());

    }
    private void LoadItemLocked(DropDownList ddl)
    {
        List<ListItem> items = new List<ListItem>();
        items.Add(new ListItem("False", "0"));
        items.Add(new ListItem("True", "1"));
        items.Sort(delegate (ListItem item1, ListItem item2) { return item1.Text.CompareTo(item2.Text); });
        ddl.Items.AddRange(items.ToArray());

    }

    private void LoadItemActived(DropDownList ddl)
    {
        List<ListItem> items = new List<ListItem>();
        items.Add(new ListItem("False", "0"));
        items.Add(new ListItem("True", "1"));
        items.Sort(delegate (ListItem item1, ListItem item2) { return item1.Text.CompareTo(item2.Text); });
        ddl.Items.AddRange(items.ToArray());
    }


    //protected void bSave_Click(object sender, EventArgs e)
    //{
    //    oSqlConn.Connection();

    //    foreach (RepeaterItem item in repeaterData.Items)
    //    {
    //        string UserDomain = (item.FindControl("lblUserDomain") as Label).Text;
    //        DropDownList ddlUserIDAccess = (DropDownList)item.FindControl("ddlUserIDAccess");
    //        string UserIDAccess = ddlUserIDAccess.SelectedItem.ToString();

    //        string sEmail = (item.FindControl("lblEmail") as Label).Text;
    //        string sUserName = (item.FindControl("lblUserName") as Label).Text;

    //        string Actived = (item.FindControl("ddlActived") as DropDownList).Text;
    //        string Approved = (item.FindControl("ddlApproved") as DropDownList).Text;
    //        string Locked = (item.FindControl("ddlLocked") as DropDownList).Text;

    //        string sApprovedDate = string.Format("{0:MM/dd/yyyy H:mm:ss}", DateTime.Now);

    //        if (WebEdcMonitoringClass.isGetUserDomainActived(oSqlConn.MyConnection, UserDomain) != int.Parse(Actived))
    //                SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Actived", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
    //            else if (WebEdcMonitoringClass.isGetUserDomainModifUserRight(oSqlConn.MyConnection, UserDomain) != UserIDAccess.ToString())
    //                SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Modified (Access)", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
    //            else if (WebEdcMonitoringClass.isGetUserDomainapprove(oSqlConn.MyConnection, UserDomain) != int.Parse(Approved))
    //                SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Approved", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);
    //            else if (WebEdcMonitoringClass.isGetUserDomainapprove(oSqlConn.MyConnection, UserDomain) != int.Parse(Locked))
    //                SendMailApprove(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User web has been Modified (Lock/UnLock)", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sUserName, sApprovedDate, UserDomain, UserIDAccess, sEmail);

    //        SaveData(UserDomain, UserIDAccess, Actived, Approved,Locked);

    //    }

    //    Response.Redirect("FrmRegistrationApprove.aspx");


    //}


    protected void bEdit_Click(object sender, EventArgs e)
    {
        ControlObjectDetail("EDIT");
    }

    protected void Cancel_Click(object sender, EventArgs e)
    {

        Response.Redirect("~/Home.aspx");
    }
}