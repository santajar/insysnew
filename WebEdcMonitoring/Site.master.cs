﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using InSysClass;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using WebClass;
using System.Web.UI.WebControls;
using System.Text;
    
public partial class SiteMaster : MasterPage
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;

    /// <summary>
    /// Current UserID
    /// </summary>
    protected string sUserID = "";
    protected string sUserDomain = "";
    protected string sUnsuccessfulLogin = "";
    protected string sLoginTime = "";
    protected string sExpires = "";

    protected ConnectionClass oSqlConn = new ConnectionClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        //// The code below helps to protect against XSRF attacks
        //var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        //Guid requestCookieGuidValue;
        //if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        //{
        //    // Use the Anti-XSRF token from the cookie
        //    _antiXsrfTokenValue = requestCookie.Value;
        //    Page.ViewStateUserKey = _antiXsrfTokenValue;
        //}
        //else
        //{
        //    // Generate a new Anti-XSRF token and save to the cookie
        //    _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
        //    Page.ViewStateUserKey = _antiXsrfTokenValue;

        //    var responseCookie = new HttpCookie(AntiXsrfTokenKey)
        //    {
        //        HttpOnly = true,
        //        Value = _antiXsrfTokenValue
        //    };
        //    if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
        //    {
        //        responseCookie.Secure = true;
        //    }
        //    Response.Cookies.Set(responseCookie);
        //}

        //Page.PreLoad += master_Page_PreLoad;
    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    // Set Anti-XSRF token
        //    ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
        //    ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        //}
        //else
        //{
        //    // Validate the Anti-XSRF token
        //    if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
        //        || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
        //    {
        //        throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
        //    }
        //}
    }
    protected void PopulateMenu(DataTable dt, int parentMenuId, MenuItem parentMenuItem)
    {
        string currentPage = Path.GetFileName(Request.Url.AbsolutePath);
        foreach (DataRow row in dt.Rows)
        {
            MenuItem menuItem = new MenuItem
            {
                Value = row["MenuId"].ToString(),
                Text = row["LinkText"].ToString(),
                NavigateUrl = row["MenuURL"].ToString(),
                Selected = row["MenuURL"].ToString().EndsWith(currentPage, StringComparison.CurrentCultureIgnoreCase)
            };
            if (parentMenuId == 0)
            {
                Menu1.Items.Add(menuItem);
                DataTable dtChild = this.GetData(int.Parse(menuItem.Value), sUserID);
                PopulateMenu(dtChild, int.Parse(menuItem.Value), menuItem);
            }
            else
            {
                parentMenuItem.ChildItems.Add(menuItem);
                DataTable dtChild = this.GetData(int.Parse(menuItem.Value), sUserID);
                PopulateMenu(dtChild, int.Parse(menuItem.Value), menuItem);
            }


        }
    }

    protected DataTable GetData(int parentMenuId, string UserID)
    {
        DataTable MyDataSet = new DataTable();
        try
        {
            oSqlConn.Connection();
            using (SqlCommand oSqlCmd = new SqlCommand("SP_GET_USER_MENU_HOME_ID", oSqlConn.MyConnection))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Clear();
                oSqlCmd.Parameters.Add("@ParentId", SqlDbType.Int).Value = parentMenuId;
                oSqlCmd.Parameters.Add("@UserId", SqlDbType.Char).Value = UserID;


                using (SqlDataAdapter MyAdapter = new SqlDataAdapter())
                {
                    MyAdapter.SelectCommand = oSqlCmd;
                    MyAdapter.SelectCommand.CommandTimeout = 10000;
                    MyAdapter.Fill(MyDataSet);
                }
                oSqlCmd.Dispose();
                oSqlConn.MyConnection.Close();

            }

        }
        catch (Exception ex)
        {
            
        }
        return MyDataSet;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            HttpCookie UserInfo = Request.Cookies["UserInfo"];
            if (UserInfo != null)
            {
                if (UserInfo.Values.Count != 0)
                {

                    if (Session["UserSession"] != null)
                            {
                                oSqlConn.Connection();

                                if (Session["UserDomain"] != null)
                                {
                                    sUserID = Session["UserID"].ToString();
                                    sUserDomain = Session["UserDomain"].ToString();
                                    //if (!WebEdcMonitoringClass.CheckTimeAutomaticLogOut(oSqlConn.MyConnection, sUserDomain, Request, Response))
                                    //{
                                    Onlyread();
                                    //}
                                    //else Response.Redirect("~/Account/Login.aspx", true);
                                }
                                else if (Session["UserRequest"] == null)
                                {

                                    if (Application["LoginTime"] != null) sLoginTime = Application["LoginTime"].ToString(); else sLoginTime = "";
                                    sExpires = Session["ExpiresTime"].ToString();
                                    sUserID = Session["UserID"].ToString();

                                    if (!WebEdcMonitoringClass.isExpire(oSqlConn.MyConnection, sUserID, sLoginTime, sExpires))
                                    {
                                        Onlyread();
                                    }
                                    else
                                    {
                                        WebEdcMonitoringClass.DeleteSingleUserActive(oSqlConn.MyConnection, Application["LoginTime"].ToString());
                                        MessageBox("Your Account Expire Session, Please back login");
                                        Session["UserSession"] = "";
                                        Session.Clear();
                                        Response.Cookies.Clear();
                                        Response.Redirect("~/Account/Login.aspx", true);
                                    }
                                }

                       }
                    //else
                    //{
                    //    Response.Redirect("~/Account/Login.aspx");
                    //}
                }
            }




        }
        //if (!Page.IsPostBack)
        //{
        //    oSqlConn.Connection();
        //    int iTotalTimeExpires = WebEdcMonitoringClass.iGetTotalTimeAutomaticLogOut(oSqlConn.MyConnection);
        //    int iTotalMaxTimeExpires = WebEdcMonitoringClass.iGetTotalMaxTimeAutomaticLogOut(oSqlConn.MyConnection);
        //    if (iTotalTimeExpires > iTotalMaxTimeExpires) MessageBox("Configuration Invalid, Please Contact Administrator Web");
        //    else
        //    {
        //        HttpCookie Cookie = Request.Cookies["UserInfo"];

        //        if (Cookie != null)
        //        {
        //            if (Cookie.Values.Count != 0)
        //            {

        //                if (Session["UserID"] != null)
        //                {

        //                    sUserID = Session["UserID"].ToString();

        //                    if (Session["UserDomain"] != null)
        //                    {
        //                        sUserDomain = Session["UserDomain"].ToString();
        //                        if (!WebEdcMonitoringClass.CheckTimeAutomaticLogOut(oSqlConn.MyConnection, sUserDomain, Request, Response))
        //                        {
        //                            Onlyread();
        //                        }
        //                        else Response.Redirect("~/Account/Login.aspx", true);
        //                    }
        //                    else
        //                    {
        //                        if (Application["LoginTime"] != null) sLoginTime = Application["LoginTime"].ToString(); else sLoginTime = "";
        //                        sExpires = Session["ExpiresTime"].ToString();
        //                        if (!WebEdcMonitoringClass.isExpire(oSqlConn.MyConnection, sUserID, sLoginTime, sExpires))
        //                        {
        //                            Onlyread();
        //                        }
        //                        else
        //                        {
        //                            MessageBox("Your Account Expire Session, Please back login");
        //                            Session["UserID"] = "";
        //                            Response.Redirect("~/Account/Login.aspx", true);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    HttpCookie UserInfo = new HttpCookie("UserInfo");
        //                    UserInfo.Value = DateTime.Now.ToString();
        //                    UserInfo.Expires = DateTime.Now.AddMinutes(iTotalTimeExpires);

        //                    Application["LoginTime"] = UserInfo.Value;
        //                    Session["ExpiresTime"] = UserInfo.Expires;
        //                    Response.Redirect("~/Account/Login.aspx", true);
        //                }
        //            }
        //        }
        //    }
        //}
    }

    protected void MessageBox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }
    private void Onlyread()
    {
        string LUnsuccessfulLogin = " Unsuccessful Logon : ";
        if (Application["LastLogin"] == null) Application["LastLogin"] = ""; else LabelLastLogin.Text = " Last Login : " + Application["LastLogin"].ToString() + " ";
        if (Application["NotesAccess"] == null) Application["NotesAccess"] = ""; else LabelNote.Text = Application["NotesAccess"].ToString() ;
        sUnsuccessfulLogin = Application["UnsuccessfulLogin"].ToString();
        if (sUnsuccessfulLogin == "")
        {
            Application["UnsuccessfulLogin"] = "";
            LabelUnsuccessfulLogin.Visible = false;
            LUnsuccessfulLogin = "";
        }
        else LabelUnsuccessfulLogin.Text = LUnsuccessfulLogin + Application["UnsuccessfulLogin"].ToString();

        Menu1.Items.Clear();
        //oSqlConn.MyConnection.Open();

        DataTable dt2 = this.GetData(0, sUserID);
        PopulateMenu(dt2, 0, null);
        var menu = Page.Master.FindControl("Menu1") as Menu;
        MenuItem homeMenuItem = menu.Items[0];
        if (menu != null)
        {
            UserPrivilege.sPrivilege = WebEdcMonitoringClass.sGetUserRight(oSqlConn.MyConnection, sUserID);
            if ((UserPrivilege.IsAllowed(PrivilegeCode.W1)))
            {
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkAccess)))
                {
                    MenuItem temp = menu.FindItem("1");
                    if (temp != null) menu.Items.Remove(menu.FindItem("1"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkdtMonitoring)))
                {
                    MenuItem temp = menu.FindItem("2");
                    if (temp != null) menu.Items.Remove(menu.FindItem("2"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkHbMonitoring)))
                {
                    MenuItem temp = menu.FindItem("3");
                    if (temp != null) menu.Items.Remove(menu.FindItem("3"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserManagement)))
                {
                    MenuItem temp = menu.FindItem("4");
                    if (temp != null) menu.Items.Remove(menu.FindItem("4"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUploadData)))
                {
                    MenuItem temp = menu.FindItem("5");
                    if (temp != null) menu.Items.Remove(menu.FindItem("5"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserRegisterMenu)))
                {
                    MenuItem temp = menu.FindItem("6");
                    if (temp != null) menu.Items.Remove(menu.FindItem("6"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserRegisterApprovedMenu)))
                {
                    MenuItem temp = menu.FindItem("4");
                    MenuItem tempSub = menu.FindItem("1");
                    if (temp != null && temp.ChildItems.Count > 0) temp.ChildItems.Remove(tempSub);
                            //ChildItems.Remove; menu.Items.Remove(menu.FindItem("6").ChildItems.Remove());
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkUserList)))
                {
                    MenuItem temp = menu.FindItem("8");
                    if (temp != null) menu.Items.Remove(menu.FindItem("8"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkActivityLog)))
                {
                    MenuItem temp = menu.FindItem("9");
                    if (temp != null) menu.Items.Remove(menu.FindItem("9"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W1, PrivilegeWeb.chkActivityLogLogin)))
                {
                    MenuItem temp = menu.FindItem("10");
                    if (temp != null) menu.Items.Remove(menu.FindItem("10"));
                }
            }
            //--
            if ((UserPrivilege.IsAllowed(PrivilegeCode.W2)))
            {
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkAccess)))
                {
                    MenuItem temp = menu.FindItem("1");
                    if (temp != null) menu.Items.Remove(menu.FindItem("1"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkdtMonitoring)))
                {
                    MenuItem temp = menu.FindItem("2");
                    if (temp != null) menu.Items.Remove(menu.FindItem("2"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkHbMonitoring)))
                {
                    MenuItem temp = menu.FindItem("3");
                    if (temp != null) menu.Items.Remove(menu.FindItem("3"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkUserManagement)))
                {
                    MenuItem temp = menu.FindItem("4");
                    if (temp != null) menu.Items.Remove(menu.FindItem("4"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkUploadData)))
                {
                    MenuItem temp = menu.FindItem("5");
                    if (temp != null) menu.Items.Remove(menu.FindItem("5"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W2, PrivilegeWeb.chkUserManagement)))
                {
                    MenuItem temp = menu.FindItem("6");
                    if (temp != null) menu.Items.Remove(menu.FindItem("6"));
                }
            }

            //--
            if ((UserPrivilege.IsAllowed(PrivilegeCode.W3)))
            {
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkAccess)))
                {
                    MenuItem temp = menu.FindItem("1");
                    if (temp != null) menu.Items.Remove(menu.FindItem("1"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkdtMonitoring)))
                {
                    MenuItem temp = menu.FindItem("2");
                    if (temp != null) menu.Items.Remove(menu.FindItem("2"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkHbMonitoring)))
                {
                    MenuItem temp = menu.FindItem("3");
                    if (temp != null) menu.Items.Remove(menu.FindItem("3"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkUserManagement)))
                {
                    MenuItem temp = menu.FindItem("4");
                    if (temp != null) menu.Items.Remove(menu.FindItem("4"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkUploadData)))
                {
                    MenuItem temp = menu.FindItem("5");
                    if (temp != null) menu.Items.Remove(menu.FindItem("5"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W3, PrivilegeWeb.chkUserManagement)))
                {
                    MenuItem temp = menu.FindItem("6");
                    if (temp != null) menu.Items.Remove(menu.FindItem("6"));
                }
            }

            //--
            if ((UserPrivilege.IsAllowed(PrivilegeCode.W4)))
            {
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkAccess)))
                {
                    MenuItem temp = menu.FindItem("1");
                    if (temp != null) menu.Items.Remove(menu.FindItem("1"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkdtMonitoring)))
                {
                    MenuItem temp = menu.FindItem("2");
                    if (temp != null) menu.Items.Remove(menu.FindItem("2"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkHbMonitoring)))
                {
                    MenuItem temp = menu.FindItem("3");
                    if (temp != null) menu.Items.Remove(menu.FindItem("3"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkUserManagement)))
                {
                    MenuItem temp = menu.FindItem("4");
                    if (temp != null) menu.Items.Remove(menu.FindItem("4"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkUploadData)))
                {
                    MenuItem temp = menu.FindItem("5");
                    if (temp != null) menu.Items.Remove(menu.FindItem("5"));
                }
                if (!(UserPrivilege.IsAllowedWeb(PrivilegeCode.W4, PrivilegeWeb.chkUserManagement)))
                {
                    MenuItem temp = menu.FindItem("6");
                    if (temp != null) menu.Items.Remove(menu.FindItem("6"));
                }
            }

        }
        Literal2.Text = Generate_Logout();
    }
    private string Generate_Logout()
    {
        StringBuilder sb_builder = new StringBuilder();
        sb_builder.Append("<ul class='nav navbar-nav navbar-right'>");
        sb_builder.Append(String.Format(@"<li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href=""#"">{0} <span class='glyphicon glyphicon-user'></span></a>", Session["UserSession"]));
        sb_builder.Append("<ul class='dropdown-menu' role='menu'>");

        UserPrivilege.sPrivilege = WebEdcMonitoringClass.sGetUserRight(oSqlConn.MyConnection, sUserID);
        bool isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
        if (isSuperUser)
        {
            sb_builder.Append(String.Format(@"<li class='dropdown'><a href=""{0}"">{1}</a>", "ChangePassword.aspx", "Change Password"));
            sb_builder.Append("</li>");
        }
        sb_builder.Append(String.Format(@"<li class='dropdown'><a href=""{0}"">{1}</a>", "Account/Logout.aspx", "Logout"));
        sb_builder.Append("</li>");
        sb_builder.Append("</ul>");
        sb_builder.Append("</li>");
        sb_builder.Append("</ul>");
        return sb_builder.ToString();
    }

    private string Generate_Login()
    {
        StringBuilder sb_builder = new StringBuilder();
        sb_builder.Append("<ul class='nav navbar-nav navbar-right'>");
        sb_builder.Append(String.Format(@"<li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href=""#"">{0} <span class='glyphicon glyphicon-user'></span></a>", Session["UserSession"]));
        sb_builder.Append("<ul class='dropdown-menu' role='menu'>");
        sb_builder.Append(String.Format(@"<li class='dropdown'><a href=""{0}"">{1}</a>", "Account/Login", "Login"));
        sb_builder.Append("</li>");
        sb_builder.Append("</ul>");
        sb_builder.Append("</li>");
        sb_builder.Append("</ul>");
        return sb_builder.ToString();
    }
    //protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
    //{
    //    Context.GetOwinContext().Authentication.SignOut();
    //}
}