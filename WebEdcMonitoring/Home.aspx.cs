﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WebClass;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

public partial class Home : Page
{
    protected DataTable dtDataMonitoring;
    protected DataTable dtDataHeartBeat;

    protected string sUserID = "";
    protected string sUserDomain = "";
    protected string sUnsuccessfulLogin = "";
    protected string sLoginTime = "";
    protected string sExpires = "";

    protected ConnectionClass sqlconn = new ConnectionClass();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Session["UserSession"] != null)
            {
                sqlconn.Connection();
                Onlyread();
            } else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
            //HttpCookie UserInfo = Request.Cookies["UserInfo"];

            //if (UserInfo != null)
            //{
            //    if (UserInfo.Values.Count != 0)
            //    {
            //        if (Session.Count != 0)
            //        {
            //sqlconn.Connection();
            //        Onlyread();
            //        }
            //        else
            //        {
            //            Response.Redirect("~/Account/Login.aspx");
            //        }
            //    }
            //    else
            //    {
            //        Response.Redirect("~/Account/Login.aspx");
            //    }
            //}
            //else
            //{
            //    Response.Redirect("~/Account/Login.aspx");
            //}
        }

    }

    private void Onlyread()
    {
        if (Session["UserSession"] != null)
        {
            if (Session["UserID"] != null) LoadData(null);
        }
        else Response.Redirect("~/Account/Login.aspx", true);
    }

    
    protected void MessageBox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }
    
    protected void LoadData(string sKey)
    {
        dtDataMonitoring = dtGetDataMonitoring(CommonSP.sSPEdcMonitorGraph1);
        chartSoftwareTotal.DataSource = dtDataMonitoring;
        chartSoftwareTotal.Series[0].XValueMember = "SoftwareVersion";
        chartSoftwareTotal.Series[0].YValueMembers = "Total";
        chartSoftwareTotal.Legends.Add("Keterangan");
        chartSoftwareTotal.Series[0].IsValueShownAsLabel = true;
        chartSoftwareTotal.DataBind();

        dtDataMonitoring = dtGetDataMonitoring(CommonSP.sSPEdcMonitorGraph2);
        chartTerminalTrx.DataSource = dtDataMonitoring;
        chartTerminalTrx.Series[0].XValueMember = "TerminalID";
        chartTerminalTrx.Series[0].YValueMembers = "TotalTransaction";
        chartTerminalTrx.Legends.Add("Keterangan");
        chartTerminalTrx.Series[0].IsValueShownAsLabel = true;
        chartTerminalTrx.DataBind();

        dtDataMonitoring = dtGetDataMonitoring(CommonSP.sSPEdcMonitorGraph3);
        chartCardTotalAmount.DataSource = dtDataMonitoring;
        chartCardTotalAmount.Series[0].XValueMember = "CardName";
        chartCardTotalAmount.Series[0].YValueMembers = "TotalAmount";
        chartCardTotalAmount.Legends.Add("Keterangan");
        chartCardTotalAmount.Series[0].IsValueShownAsLabel = true;
        chartCardTotalAmount.DataBind();

        dtDataHeartBeat = dtGetDataMonitoring(CommonSP.sSPEdcHeartBeatGraph1);
        chartHearBeat.DataSource = dtDataHeartBeat;
        chartHearBeat.Series[0].XValueMember = "Status";
        chartHearBeat.Series[0].YValueMembers = "CSI";
        chartHearBeat.Legends.Add("Keterangan");
        chartHearBeat.Series[0].IsValueShownAsLabel = true;
        chartHearBeat.DataBind();

    }
    protected void LoadData()
    {
        LoadData(null);
    }

    protected DataTable dtGetDataMonitoring(string _sQuery)
    {
        try
        {
            sqlconn.Connection();
            DataTable dtTemp = new DataTable();
            SqlCommand sqlcmd = new SqlCommand(_sQuery, sqlconn.MyConnection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            if (sqlconn.MyConnection.State != ConnectionState.Open) sqlconn.MyConnection.Open();
            using (SqlDataReader reader = sqlcmd.ExecuteReader())
            {
                if (reader.HasRows)
                    dtTemp.Load(reader);
                return dtTemp;

            }
        }
        finally
        {
            sqlconn.MyConnection.Close();
        }
    }

    protected void timerRefresh_Tick(object sender, EventArgs e)
    {
        Onlyread();
        lblTimer.Text = "Panel refreshed at: " + DateTime.Now.ToLongTimeString();
    }
}