﻿<%@ Page Title="SIM Monitoring Page" Language="C#" AutoEventWireup="true" CodeFile="dataMonitorSIM.aspx.cs" Inherits="dataMonitorSIM" MasterPageFile="~/Site.Master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="script_css" runat="server">
    <style type="text/css">
        .gvwCasesPager a {
                color: #fff;
                text-decoration:underline;
                margin-left:5px;
                margin-right:5px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2>Data Monitoring SIM</h2>
    </div>
    <div class="row">
        <a href="javascript: history.go(-1)">Go Back</a>
    </div>
    <br />
    <div class="row">
        <asp:Label ID="lblTimestamp" runat="server"></asp:Label>
        <asp:Label ID="lblSerial" runat="server"></asp:Label>
        <asp:GridView ID="gridviewSIM" runat="server" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerSettings Position="TopAndBottom" />
            <PagerStyle BackColor="#2461BF" ForeColor="Yellow" HorizontalAlign="Center" CssClass ="gvwCasesPager" Font-Bold="true"/> 
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    </div>
    <br />
    <div class="row">
        <div id="googleMap" style="width: 100%; height: 400px;"></div>
        <script>
            var mylat = "<%= latitude %>";
            var mylon = "<%= longitude %>";
            var infoWindow = new google.maps.InfoWindow;
            function initMap() {
                var myLatLong;
                if (mylat != 0 && mylon != 0)
                    myLatLong = new google.maps.LatLng(mylat, mylon);                

                var myOptions = {
                    zoom: 15,
                    center: myLatLong,
                };
                var map = new google.maps.Map(document.getElementById("googleMap"), myOptions);
                var marker = new google.maps.Marker({
                    position: myLatLong,
                    map: map
                });
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=<%=mapsKey%>&callback=initMap"></script>
    </div>
</asp:Content>
