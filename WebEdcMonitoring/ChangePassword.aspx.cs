﻿using InSysClass;
using WebClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using WebClass;

public partial class ChangePassword : Page
{
    protected ConnectionClass oSqlConn = new ConnectionClass();

    /// <summary>
    /// Current UserID
    /// </summary>
    protected static string sUserID;

    /// <summary>
    /// CurrentUserName
    /// </summary>
    protected static string sUserName;

    /// <summary>
    /// Set current password
    /// </summary>
    protected static string sPassword;
    protected int iStatus;

    /// <summary>
    /// Is form is called from Main Menu
    /// </summary>
    protected bool isFromMenu;
    protected bool isForcePassword;

    public string sClearNewPassword;

    /// <summary>
    /// Encrypts New Password and returns the result
    /// </summary>
    /// <returns>string : Encrypted New Password</returns>
    protected string sGetEncryptPassword()
    {
        return DataEncryptionClass.sGetEncrypt(txtConfirmNewPass.Text);
    }

    /// <summary>
    /// Get the encrypted new password
    /// </summary>
    /// <returns>string : Encrypted New Password</returns>
    public string sGetNewPassEncrypted()
    {
        return sPassword;
    }

    /// <summary>
    /// Get the old password from the textbox.
    /// </summary>
    /// <returns>string : Old password</returns>
    protected string sGetOldPassword()
    {
        return txtOldPassword.Text;//.Trim();
    }

    /// <summary>
    /// Get text from NewPassword textbox.
    /// </summary>
    /// <returns>string : NewPassword</returns>
    protected string sGetNewPassword()
    {
        return txtNewPassword.Text;//.Trim();
    }

    /// <summary>
    /// Get text from ConfirmPassword textbox.
    /// </summary>
    /// <returns>string : ConfirmPassword</returns>
    protected string sGetConfirmPassword()
    {
        return txtConfirmNewPass.Text;//.Trim();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HttpCookie UserInfo = Request.Cookies["UserInfo"];

            if (UserInfo != null)
            {
                if (UserInfo.Values.Count != 0)
                {
                    if (Session.Count != 0)
                    {
                        sUserID = Session["UserID"].ToString();
                        if (Session["UserSession"] != null)
                        {
                            txtLoginName.Text = sUserID;
                        }
                    }
                    else
                    {
                        Response.Redirect("~/Account/Login.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

    }
    protected void MessageBox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }

    /// <summary>
    /// Validates the passwords inputed by user.
    /// </summary>
    /// <returns>boolean : true if valid, else false</returns>
    protected bool isValid()
    {
        bool isValid = false;
        if (sGetOldPassword().Length <= 0) MessageBox("Old Password Error ");
        else if (sGetNewPassword().Length <= 0) MessageBox("New Password Error ");
        else if (sGetConfirmPassword().Length <= 0) MessageBox("Confirm Password Error");
        else if (sGetNewPassword() != sGetConfirmPassword()) MessageBox("New & Confirm Password Error");
        else isValid = true;

        return isValid;
    }


    /// <summary>
    /// Get the saved password from the database.
    /// </summary>
    /// <returns>string : Old Password</returns>
    protected string sOldPasswordDb()
    {

        string sOldPwd = "";
        SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn.MyConnection);
        oSqlCmd.CommandType = CommandType.StoredProcedure;
        oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "WHERE UserID = '" + sUserID + "'";
        SqlDataReader oRead = oSqlCmd.ExecuteReader();
        if (oRead.Read())
            sOldPwd = oRead["Password"].ToString();
        oRead.Close();
        oRead.Dispose();
        oSqlCmd.Dispose();
        return sOldPwd;
    }

    protected bool isNewPasswordLikeOldPassword()
    {
        bool bExist = true;
        try
        {
            if (txtOldPassword.Text != sGetNewPassword())
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCheckHistoryPassword, oSqlConn.MyConnection);
                oSqlCmd.CommandType = CommandType.StoredProcedure;

                oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = sUserID;
                oSqlCmd.Parameters.Add("@sEncrypPassword", SqlDbType.VarChar).Value = sGetEncryptPassword();
                oSqlCmd.Parameters.Add("@bReturn", SqlDbType.Bit).Value = bExist;
                oSqlCmd.Parameters["@bReturn"].Direction = ParameterDirection.Output;
                if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();

                oSqlCmd.ExecuteNonQuery();
                bExist = (bool)oSqlCmd.Parameters["@bReturn"].Value;

                oSqlCmd.Dispose();
            }
            else
            {
                bExist = true;
            }
        }
        catch (Exception ex)
        {
            MessageBox("Error : " + ex.Message);
        }
        return bExist;
    }



    /// <summary>
    /// Condition for updating password in database.
    /// </summary>
    /// <returns>string : Condition string</returns>
    protected string sConditions()
    {
        return "SET PASSWORD = '" + sGetEncryptPassword()
                        + "', LastModifyPassword = GETDATE() WHERE USERID = '" + sUserID + "'";
    }

    /// <summary>
    /// Save the new password to database
    /// </summary>
    protected void pChangePassword()
    {
        try
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPSuperUserUpdate, oSqlConn.MyConnection);
            oSqlCmd.CommandType = CommandType.StoredProcedure;

            oSqlCmd.Parameters.Add("@sConditions", SqlDbType.VarChar).Value = sConditions();

            if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();

            oSqlCmd.ExecuteNonQuery();

            oSqlCmd.Dispose();
        }
        catch (Exception ex)
        {
            MessageBox(ex.Message);
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        try
        {
            oSqlConn.Connection();
            if (isValid()) // Validating
                if (!isNewPasswordLikeOldPassword())
                {
                    if (PasswordIsComplex())
                    {
                        pChangePassword();
                        string ReqUrl = Request["ReturnUrl"];
                        if (ReqUrl != null)
                        { Response.Redirect(ReqUrl); }
                        Response.Redirect("~/Account/Login.aspx", false);
                    }
                }
                else
                    MessageBox("New password Can not same with Old History.");
        }
        catch (Exception ex)
        {
            MessageBox("Error : " + ex.Message);
        }
    }


    /// <summary>
    /// Get Min and Max Lenght Password
    /// </summary>
    /// <param name="sDatabaseID">string : Database ID Value</param>
    /// <returns>int : Lenght of Tag</returns>
    private int iGetPasswordLenght(string sFunction)
    {
        int iLen = 0;
        SqlCommand oCmd = new SqlCommand(string.Format("SELECT {0}", sFunction), oSqlConn.MyConnection);
        if (oSqlConn.MyConnection.State == ConnectionState.Closed)
            oSqlConn.MyConnection.Open();

        using (SqlDataReader oRead = oCmd.ExecuteReader())
        {
            if (oRead.Read())
                iLen = int.Parse(oRead[0].ToString());
            oRead.Close();
        }
        return iLen;
    }
    private bool PasswordIsComplex()
    {
        int iValidConditions = 0;
        string sPassword = sGetNewPassword();
        int iMinPawsswordLength = iGetPasswordLenght("DBO.iGetMinLengthPassword()");
        int iMaxPasswordLength = iGetPasswordLenght("DBO.iGetMaxLengthPassword()");

        if (sPassword.Length >= iMinPawsswordLength && sPassword.Length <= iMaxPasswordLength)
            iValidConditions++;

        if (iValidConditions == 0)
        {
            MessageBox(string.Format("New Passwod Lenght Min {0} and Max {1}.", iMinPawsswordLength, iMaxPasswordLength));
            return false;
        }

        foreach (char c in sPassword)
        {
            if (c >= 'a' && c <= 'z')
            {
                iValidConditions++;
                break;
            }
        }

        foreach (char c in sPassword)
        {
            if (c >= 'A' && c <= 'Z')
            {
                iValidConditions++;
                break;
            }
        }

        if (iValidConditions == 1 || iValidConditions == 2)
        {
            MessageBox("New Passwod must contain one Capital letter and one small letter.");
            return false;
        }

        foreach (char c in sPassword)
        {
            if (c >= '0' && c <= '9')
            {
                iValidConditions++;
                break;
            }
        }

        if (iValidConditions == 3)
        {
            MessageBox("New Password must contain one number.");
            return false;
        }

        if (iValidConditions == 4)
        {
            char[] special = { '@', '#', '$', '%', '^', '&', '+', '=' };

            if (sPassword.IndexOfAny(special) == -1)
            {
                MessageBox("New Password Must Contain Special Character (@,#,$,%,^,&,+,=)");
                return false;
            }
        }

        return true;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Home.aspx", false);
    }

}