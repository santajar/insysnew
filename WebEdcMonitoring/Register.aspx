﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-target="#filter"><b>Users Access Matrix</b></div>
        <div class="panel-body" id="filter">
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">User ID</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtUserID" Width="200" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">Name</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtUserName" Width="200" runat="server" CssClass="form-control input-sm" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">User Access</label>
                    </div>
                    <div class="col-md-2">
                        <asp:CheckBox ID="chkAdministrator" CssClass="form-control input-sm" runat="server" Text="ADMINISTRATOR" AutoPostBack="True" OnCheckedChanged="chkAdministrator_CheckedChanged" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2">
                        <asp:CheckBox ID="chkSPV" CssClass="form-control input-sm" runat="server" Text="SUPERVISOR" AutoPostBack="True" OnCheckedChanged="chkSPV_CheckedChanged" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2">
                        <asp:CheckBox ID="chkOP" CssClass="form-control input-sm" runat="server" Text="OPERATOR" AutoPostBack="True" OnCheckedChanged="chkOP_CheckedChanged" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2">
                        <asp:CheckBox ID="chkGS" CssClass="form-control input-sm" runat="server" Text="GUEST" AutoPostBack="True" OnCheckedChanged="chkGS_CheckedChanged" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkdtMonitoring" CssClass="form-control input-sm" runat="server" Text="Data Monitoring" OnCheckedChanged="chkdtMonitoring_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkdtMonitoringExport" CssClass="form-control input-sm" runat="server" Text="Data Monitoring Export" OnCheckedChanged="chkdtMonitoringExport_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkdtMonitoringDelete" CssClass="form-control input-sm" runat="server" Text="Data Monitoring Delete" OnCheckedChanged="chkdtMonitoringDelete_CheckedChanged" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkHbMonitoring" CssClass="form-control input-sm" runat="server" Text="HeartBeat Monitoring" OnCheckedChanged="chkHbMonitoring_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkHbMonitoringExport" CssClass="form-control input-sm" runat="server" Text="HeartBeat Monitoring Export" OnCheckedChanged="chkHbMonitoringExport_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkHbMonitoringDelete" CssClass="form-control input-sm" runat="server" Text="HeartBeat Monitoring Delete" OnCheckedChanged="chkHbMonitoringDelete_CheckedChanged" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUserManagement" CssClass="form-control input-sm" runat="server" Text="User Management" OnCheckedChanged="chkUserManagement_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUserRegisterMenu" CssClass="form-control input-sm" runat="server" Text="User Register Menu" OnCheckedChanged="chkUserRegisterMenu_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUserRegisterApprovedMenu" CssClass="form-control input-sm" runat="server" Text="Register Approve Menu" OnCheckedChanged="chkUserRegisterApprovedMenu_CheckedChanged" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUserRegisterAccess" CssClass="form-control input-sm" runat="server" Text="User Register Access" OnCheckedChanged="chkUserRegisterAccess_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUserRegisterActived" CssClass="form-control input-sm" runat="server" Text="User Register Actived" OnCheckedChanged="chkUserRegisterActived_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUserRegisterApproved" CssClass="form-control input-sm" runat="server" Text="User Register Approved" OnCheckedChanged="chkUserRegisterApproved_CheckedChanged" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>

                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUserRegisterLocked" CssClass="form-control input-sm" runat="server" Text="User Register Locked" OnCheckedChanged="chkUserRegisterLocked_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUserRegisterDelete" CssClass="form-control input-sm" runat="server" Text="User Register Delete" OnCheckedChanged="chkUserRegisterDelete_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUserList" CssClass="form-control input-sm" runat="server" Text="User List" OnCheckedChanged="chkUserList_CheckedChanged" />
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>

                    <div class="col-md-3">
                        <asp:CheckBox ID="chkActivityLog" CssClass="form-control input-sm" runat="server" Text="Activity Log" OnCheckedChanged="chkActivityLog_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkActivityLogLogin" CssClass="form-control input-sm" runat="server" Text="Activity Log Login" OnCheckedChanged="chkActivityLogLogin_CheckedChanged" />
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkUploadData" CssClass="form-control input-sm" runat="server" Text="UploadData" OnCheckedChanged="chkUploadData_CheckedChanged" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">Password</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtPassword" Width="200" runat="server" CssClass="form-control input-sm" TextMode="Password" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">Confirm Password</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtConfirmPass" Width="200" runat="server" CssClass="form-control input-sm" TextMode="Password" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">

                    <div class="panel panel-default">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-left padding-center">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn-success btn-sm" OnClick="btnSave_Click" Width="80px" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="btn-primary btn-sm" OnClick="btnCancel_Click" Width="80px" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
