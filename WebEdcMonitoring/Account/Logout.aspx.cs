﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Web.UI;
using NewInSys;
using System.Configuration;
using System.Data.SqlClient;
using InSysClass;
using System.Data;
using WebClass;

public partial class Account_Logout : Page
{

    protected static string sUserSession = "";
    protected ConnectionClass oSqlConn = new ConnectionClass();
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie aCookie;
        string cookieName;
        int limit = Request.Cookies.Count;
        for (int i = 0; i < limit; i++)
        {
            cookieName = Request.Cookies[i].Name;
            aCookie = new HttpCookie(cookieName);
            aCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(aCookie);
        }
        if (Application["UserSession"] != null) sUserSession = Application["UserSession"].ToString(); else sUserSession = "";
        if (sUserSession != "")
        {
            oSqlConn.Connection();
            WebEdcMonitoringClass.DeleteSingleUserActive(oSqlConn.MyConnection, sUserSession);
            Session.Clear();
            Response.Cookies.Clear();
            Response.Redirect("~/Account/Login.aspx");
        }
        else
        {
            Session.Clear();
            Response.Cookies.Clear();
            Response.Redirect("~/Account/Login.aspx");
        }

    }
}