﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebClass;
using System.Configuration;
using System.Data.SqlClient;
using InSysClass;
using System.Data;
using System.Collections.Generic;
using System.DirectoryServices;
using ActiveDirectoryAuthentication.Helper;
using System.Net.Mail;
using System.Web.Security;
using WELog;
using ASP;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.Providers.Entities;

public partial class Account_Login : Page
{
    private ActiveDirectoryConfiguration _currentActiveDirectoryConfiguration = null;

    protected bool IsActiveDirectoryEnabled
    {
        get
        {
            //return ActiveDirectorySettings.Enabled;
            return WebEdcMonitoringClass.sGetldapEnable(oSqlConn.MyConnection);
        }
    }

    private static ActiveDirectoryConfiguration activeDirectorySettings = null;
    public static ActiveDirectoryConfiguration ActiveDirectorySettings
    {
        get
        {
            try
            {
                if (activeDirectorySettings == null)
                {
                    activeDirectorySettings = (ActiveDirectoryConfiguration)ConfigurationManager.GetSection("ldapConfiguration");
                }
            }
            catch
            {
            }
            return activeDirectorySettings;
        }
    }
    protected ConnectionClass oSqlConn = new ConnectionClass();

    protected string sUserDomain = "";

    protected string sNotesAccess = "Warning! this system may only be accessed by authorized users";

    protected static string domainName;

    /// <summary>
    /// Determines if user can login or not.
    /// </summary>
    public bool isCanLoginToSystem = false;

    /// <summary>
    /// Determines if super user can login or not.
    /// </summary>
    public bool isCanLoginToSuperSystem = false;

    /// <summary>
    /// Current UserName
    /// </summary>
    protected string sUserName = "";

    /// <summary>
    /// Current UserID
    /// </summary>
    protected string sUserID = "";


    protected string sEmail = "";
    protected string sLastLogin = "";
    protected string sLastLoginInvalid = "";


    /// <summary>
    /// Current Password
    /// </summary>
    protected string sPwd = "";

    protected string sGroupID = "";

    /// <summary>
    /// Codes for errors
    /// </summary>
    private enum ErrorCode
    {
        NoErr = 1, //avoid start from 0
        ErrLoginUserID,
        ErrLoginUserIDPasswordNotFound
    }


    /// <summary>
    /// Get User ID
    /// </summary>
    /// <returns>string : UserID</returns>
    public string sGetUserID()
    {
        return UserName.Text/*.Trim()*//*.Replace("'", "''")*/;
    }

    /// <summary>
    /// Get Password
    /// </summary>
    /// <returns>string : Password</returns>
    public string sGetPassword()
    {
        return Password.Text;
    }

    /// <summary>
    /// Get Password
    /// </summary>
    /// <returns>string : Password</returns>
    public string sGetPasswordDomain()
    {
        return Password.Text;
    }

    /// <summary>
    /// Get User Name
    /// </summary>
    /// <returns>string : User Name</returns>
    public string sGetUserName()
    {
        return sUserName.ToUpper();
    }

    public string sGetGroupID()
    {
        return sGroupID;
    }

    /// <summary>
    /// Get the encryption of the Password inputed by user
    /// </summary>
    /// <returns>string : Result of Password Encryption</returns>
    protected string sGetEncryptPassword()
    {
        return DataEncryptionClass.sGetEncrypt(sGetPassword());
    }

    /// <summary>
    /// Condition to limit UserID search based on UserID and Encrypted Password
    /// </summary>
    /// <returns>string : the filter string</returns>
    protected string sCondition()
    {
        return " WHERE UserID='" + sGetUserID() + "' AND Password='" + sGetEncryptPassword() + "'";
    }

    protected string sConditionUser()
    {
        return " WHERE UserID='" + sUserID+ "'" ;
    }

    protected bool isUserIDFound()
    {
        bool isPass = false;
        try
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn.MyConnection))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();
                if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    using (DataTable dtUser = new DataTable())
                    {
                        dtUser.Load(oRead);
                        if (dtUser.Rows.Count > 0) // UserID Found
                        {
                            HttpCookie UserInfo = new HttpCookie("UserInfo");
                            isPass = true;
                            UserPrivilege.sPrivilege = dtUser.Rows[0]["UserRights"].ToString();
                            sUserName = dtUser.Rows[0]["UserName"].ToString();
                            UserInfo["LastLogin"] = dtUser.Rows[0]["LastLogin"].ToString();

                            if (dtUser.Columns.IndexOf("GroupID") > -1)
                                if (dtUser.Columns["GroupID"] != null)
                                    sGroupID = dtUser.Rows[0]["GroupID"].ToString();
                        }
                    }
                }
                Dispose();
            }
        }
        catch (Exception ex)
        {
            HttpContext.Current.Session["errorMessage"] = ex.Message.ToString();
            HttpContext.Current.Session["errorDetails"] = ex.StackTrace.ToString();
            string Causes = "";
            if (ex.InnerException != null) { Causes = ex.Source.ToString() + "<br />" + ex.InnerException.ToString(); }
            else { Causes = ex.Source.ToString(); }
            HttpContext.Current.Session["errorCauses"] = Causes.ToString();
            HttpContext.Current.Response.Redirect("~/Errors.aspx");
        }
        return isPass;
    }

    protected bool isCanLogin()
    {
        if (isUserIDFound()) // If is User
        {
            isCanLoginToSystem = true;
            return true;
        }
        else return false;
    }

    //void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    //{
    //    //e.Authenticated = UserManager.Default.ValidateUser(sGetUserID(), sGetPassword());
    //}
    //public void ProcessPage()
    //{
    //    Session["Test"] = "test string to see if session variables still exist after app recycle.";

    //   // ((global_asax)Context.ApplicationInstance).InsertSessionCacheItem(sUserID);

    //    // Test to access the cache and make sure it still exists
    //    msSessionCacheValue = ((global_asax)Context.ApplicationInstance).GetSessionCacheItem(sUserID);
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                //HttpCookie UserInfo = Request.Cookies["UserInfo"];
                //if (UserInfo != null)
                //{
                //    if (UserInfo.Values.Count != 0)
                //    {
                        if (Session["UserSession"] != null)
                        {
                            string ReqUrl = Request["ReturnUrl"];
                           // SetSessionValue(UserInfo);
                            if (ReqUrl != null)
                            { Response.Redirect(ReqUrl); }
                            Response.Redirect("~/Home.aspx", false);
                        }
                //    }
                //}
                UserName.Focus();
                lblError.Text = "";
            }
            catch (Exception ex)
            {
                Messagebox("Error : " + ex.Message);
            }
        }

    }
    private void SetSessionValue(HttpCookie MyCookie)
    {
        if (Session.Count <= 0)
        {
            Session["UserID"] = MyCookie["UserID"].ToString().Trim();
            Session["UserName"] = MyCookie["UserName"].ToString().Trim();
            Session["LastLogin"] = MyCookie["LastLogin"].ToString();            
            Session["UserSession"] = MyCookie["UserSession"].ToString().Trim();
            Session["ExpiresTime"] = MyCookie["ExpiresTime"].ToString();
            Session["UserDomain"] = MyCookie["UserDomain"].ToString();

        }
    }

    protected bool CheckDaysLocked()
    {
        bool bReturn = true;
        try
        {
            SqlCommand oSqlCmd;
            oSqlCmd = new SqlCommand(CommonSP.sSPLockedUserIDbyDays, oSqlConn.MyConnection);

            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = sGetUserID();
            oSqlCmd.Parameters.Add("@iLocked", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;

            if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
            oSqlCmd.ExecuteNonQuery();
            bReturn = oSqlCmd.Parameters["@iLocked"].Value.ToString() == "1" ? true : false;
            oSqlCmd.Dispose();
        }
        catch (Exception ex)
        {
            //HttpContext.Current.Session["errorMessage"] = ex.Message.ToString();
            //HttpContext.Current.Session["errorDetails"] = ex.StackTrace.ToString();
            //string Causes = "";
            //if (ex.InnerException != null) { Causes = ex.Source.ToString() + "<br />" + ex.InnerException.ToString(); }
            //else { Causes = ex.Source.ToString(); }
            //HttpContext.Current.Session["errorCauses"] = Causes.ToString();
            //HttpContext.Current.Response.Redirect("~/Errors.aspx");
        }

        return bReturn;
    }
    protected bool CheckDaysLockedUserDomain()
    {
        bool bReturn = true;
        try
        {
            SqlCommand oSqlCmd;
            oSqlCmd = new SqlCommand("spUserLoginDomainLockedbyDays", oSqlConn.MyConnection);

            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sUserDomain", SqlDbType.VarChar).Value = sGetUserID();
            oSqlCmd.Parameters.Add("@iLocked", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;

            if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
            oSqlCmd.ExecuteNonQuery();
            bReturn = oSqlCmd.Parameters["@iLocked"].Value.ToString() == "1" ? true : false;
            oSqlCmd.Dispose();
            if (bReturn == true)
            {
                WebEdcMonitoringClass.InsertAuditTrailLogin(oSqlConn.MyConnection,sGetUserID(), "User Locked (90 day not login)");
                SendMailUserlocked(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User has been 90 day not login", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), sGetUserName(), sLastLogin, sGetUserID(), sUserID, sEmail);
                Messagebox("Your Account Login, 90 day not login & it will be disabled");
            }

        }
        catch (Exception ex)
        {
            //HttpContext.Current.Session["errorMessage"] = ex.Message.ToString();
            //HttpContext.Current.Session["errorDetails"] = ex.StackTrace.ToString();
            //string Causes = "";
            //if (ex.InnerException != null) { Causes = ex.Source.ToString() + "<br />" + ex.InnerException.ToString(); }
            //else { Causes = ex.Source.ToString(); }
            //HttpContext.Current.Session["errorCauses"] = Causes.ToString();
            //HttpContext.Current.Response.Redirect("~/Errors.aspx");
        }

        return bReturn;
    }


 
    protected void Messagebox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }
    private int iGetPasswordLenght(string sFunction)
    {
        int iLen = 0;
        SqlCommand oCmd = new SqlCommand(string.Format("SELECT {0}", sFunction), oSqlConn.MyConnection);
        if (oSqlConn.MyConnection.State == ConnectionState.Closed)
            oSqlConn.MyConnection.Open();

        using (SqlDataReader oRead = oCmd.ExecuteReader())
        {
            if (oRead.Read())
                iLen = int.Parse(oRead[0].ToString());
            oRead.Close();
        }
        return iLen;
    }

    private bool ValidatePassword(string password, out string ErrorMessage)
    {
        var input = password;
        ErrorMessage = string.Empty;

        if (string.IsNullOrWhiteSpace(input))
        {
            //throw new Exception("Password should not be empty");
            ErrorMessage="Password should not be empty";
        }
        
        int iMinPawsswordLength = iGetPasswordLenght("DBO.iGetMinLengthPassword()");
        int iMaxPasswordLength = iGetPasswordLenght("DBO.iGetMaxLengthPassword()");

        var hasNumber = new Regex(@"[0-9]+");
        var hasUpperChar = new Regex(@"[A-Z]+");
        var hasMiniMaxChars = new Regex(@".{"+ iMinPawsswordLength+","+ iMaxPasswordLength+"}");
        var hasLowerChar = new Regex(@"[a-z]+");
        var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
        var hasPass = new Regex(@"Ppa@sswo0rd!");

        if (!hasLowerChar.IsMatch(input))
        {
            ErrorMessage = "Password should contain At least one lower case letter";
            return false;
        }
        else if (!hasUpperChar.IsMatch(input))
        {
            ErrorMessage = "Password should contain At least one upper case letter";
            return false;
        }
        else if (!hasMiniMaxChars.IsMatch(input))
        {
            ErrorMessage = "password that has at least eight alphanumeric characters,Passwod Lenght Min " + iMinPawsswordLength+"and Max "+ iMaxPasswordLength;
            return false;
        }
        else if (!hasNumber.IsMatch(input))
        {
            ErrorMessage = "Password should contain At least one numeric value";
            return false;
        }

        else if (!hasSymbols.IsMatch(input))
        {
            ErrorMessage = "Password should contain At least one special case characters";
            return false;
        }
        //else if (!hasPass.IsMatch(input))
        //{
        //    ErrorMessage = "Password should contain At P@ssw0rd! case characters, please change password and contact Administrator";
        //    return false;
        //}
        else
        {
            return true;
        }
    }
    private bool PasswordIsComplex()
    {
        int iValidConditions = 0;
        string sPassword = sGetPasswordDomain();
        int iMinPawsswordLength = iGetPasswordLenght("DBO.iGetMinLengthPassword()");
        int iMaxPasswordLength = iGetPasswordLenght("DBO.iGetMaxLengthPassword()");

        if (sPassword.Length >= iMinPawsswordLength && sPassword.Length <= iMaxPasswordLength)
            iValidConditions++;

        if (iValidConditions == 0)
        {
            Messagebox(string.Format("password that has at least eight alphanumeric characters,Passwod Lenght Min {0} and Max {1}.", iMinPawsswordLength, iMaxPasswordLength));
            return false;
        }

        foreach (char c in sPassword)
        {
            if (c >= 'a' && c <= 'z')
            {
                iValidConditions++;
                break;
            }
        }

        foreach (char c in sPassword)
        {
            if (c >= 'A' && c <= 'Z')
            {
                iValidConditions++;
                break;
            }
        }

        if (iValidConditions == 1 || iValidConditions == 2)
        {
            Messagebox("Passwod must contain one Capital letter and one small letter.");
            return false;
        }

        foreach (char c in sPassword)
        {
            if (c >= '0' && c <= '9')
            {
                iValidConditions++;
                break;
            }
        }

        if (iValidConditions == 3)
        {
            Messagebox("Password must contain one number.");
            return false;
        }

        if (iValidConditions == 4)
        {
            //char[] special = { '@', '#', '$', '%', '^', '&', '+', '=' }; // or whatever
            char[] special = { '#', '$', '%', '^', '&', '+', '=' }; // or whatever

            if (sPassword.IndexOfAny(special) == -1)
            {
                //Messagebox("Password Must Contain Special Character (@,#,$,%,^,&,+,=)");
                Messagebox("Password Must Contain Special Character (#,$,%,^,&,+,=)");
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Validate UserID and Password. 
    /// </summary>
    /// <returns>boolean : true if UserID and Password valid, else false</returns>
    protected bool isValid()
    {
        sUserID = sGetUserID();
        sPwd = sGetPassword();
        if (sUserID.Length == 0)
        {
            Messagebox("User ID Not Valid");
            return false;
        }
        
        if (sPwd.Length <= 8)
        {
            Messagebox("password that has at least eight alphanumeric characters");
            return false;
        } else
        {

        }



        return true;
    }

    //private int iGetTotalTimeAutomaticLogOut()
    //{
    //    int iLen = 0;

    //    using (SqlCommand oCmdC = new SqlCommand(string.Format("SELECT dbo.iGetTotalTimeAutomaticLogOut('{0}')", "TotalTimeAutomaticLogOut"), oSqlConn.MyConnection))
    //    {
    //        if (oSqlConn.MyConnection.State == ConnectionState.Closed)
    //            oSqlConn.MyConnection.Open();
    //        using (SqlDataReader oReadC = oCmdC.ExecuteReader())
    //        {
    //            if (oReadC.Read())
    //                iLen = int.Parse(oReadC[0].ToString());
    //            oReadC.Close();
    //        }
    //        oCmdC.Dispose();
    //    }
    //    return iLen;
    //}


    protected int iGetTotalTimeAutomaticLogOut()
    {
        int iLen = 0;

        SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.iGetTotalTimeAutomaticLogOut('{0}')", "TotalTimeAutomaticLogOut"), oSqlConn.MyConnection);
        if (oSqlConn.MyConnection.State == ConnectionState.Closed)
            oSqlConn.MyConnection.Open();

        using (SqlDataReader oRead = oCmd.ExecuteReader())
        {
            if (oRead.Read())
                iLen = int.Parse(oRead[0].ToString());
            oRead.Close();
        }
        oCmd.Dispose();

        return iLen;
    }

    /// <summary>
    /// Check UserID locked or not
    /// </summary>
    /// <returns>True if locked</returns>
    private bool isLocked()
    {
        bool iLen = true;

        SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.isUserIDDomainLocked('{0}')", sUserID), oSqlConn.MyConnection);
        if (oSqlConn.MyConnection.State == ConnectionState.Closed)
            oSqlConn.MyConnection.Open();

        using (SqlDataReader oRead = oCmd.ExecuteReader())
        {
            if (oRead.Read())
                iLen = bool.Parse(oRead[0].ToString());
            oRead.Close();
        }
        oCmd.Dispose();

        return iLen;
    }

    private bool isLockedUserDomain()
    {
        bool iLen = true;

        SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.isUserIDDomainLocked('{0}')", sGetUserID()), oSqlConn.MyConnection);
        if (oSqlConn.MyConnection.State == ConnectionState.Closed)
            oSqlConn.MyConnection.Open();

        using (SqlDataReader oRead = oCmd.ExecuteReader())
        {
            if (oRead.Read())
                iLen = bool.Parse(oRead[0].ToString());
            oRead.Close();
        }
        oCmd.Dispose();

        return iLen;
    }

    private bool isLockedUserLocal()
    {
        bool iLen = true;

        SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.isUserIDLocked('{0}')", sGetUserID()), oSqlConn.MyConnection);
        if (oSqlConn.MyConnection.State == ConnectionState.Closed)
            oSqlConn.MyConnection.Open();

        using (SqlDataReader oRead = oCmd.ExecuteReader())
        {
            if (oRead.Read())
                iLen = bool.Parse(oRead[0].ToString());
            oRead.Close();
        }
        oCmd.Dispose();

        return iLen;
    }


    protected void LogIn(object sender, EventArgs e)
    {
        //if (IsValid)
        //{
        //    // Validate the user password
        //    var manager = new UserManager();
        //    ApplicationUser user = manager.Find(UserName.Text, Password.Text);
        //    if (user != null)
        //    {
        //        IdentityHelper.SignIn(manager, user, RememberMe.Checked);
        //        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
        //    }
        //    else
        //    {
        //        FailureText.Text = "Invalid username or password.";
        //        ErrorMessage.Visible = true;
        //    }
        //}
    }

    protected string sConditionUserDomain()
    {
        return " WHERE UserDomain='" + UserName.Text + "'";
    }
    protected bool isUserDomainFoundActive()
    {
        bool isUserDomain = false;
        try
        {
            using (SqlCommand oSqlCmd = new SqlCommand("spUser_Domain_Browse", oSqlConn.MyConnection))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionUserDomain();
                if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    using (DataTable dtUser = new DataTable())
                    {
                        dtUser.Load(oRead);
                        if (dtUser.Rows.Count > 0) // UserID Found
                        {

                            if (dtUser.Rows[0]["Approved"].ToString() == "True")
                            {
                                isUserDomain = true;
                                sUserDomain = dtUser.Rows[0]["UserDomain"].ToString();
                                Application["UserDomain"] = sUserDomain;
                                sUserID = dtUser.Rows[0]["UserIDAccess"].ToString();
                                sUserName = dtUser.Rows[0]["UserName"].ToString();
                                sLastLogin = string.Format("{0:MM/dd/yyyy H:mm:ss} ", dtUser.Rows[0]["LastLogin"].ToString());
                                sLastLoginInvalid = string.Format("{0:MM/dd/yyyy H:mm:ss} ", dtUser.Rows[0]["LastLoginInvalid"].ToString());
                                sEmail = dtUser.Rows[0]["Email"].ToString();
                            }
                            else { Application["Notes"]="Your Account Not yet Approve";  isUserDomain = false;  }
                        }
                    }
                }
                oSqlCmd.Dispose();
            }
        }
        catch (Exception ex)
        {
            
            Messagebox(ex.Message);
        }
        return isUserDomain;
    }

    protected bool isUserFoundActive()
    {
        bool isUser = false;
        try
        {
            using (SqlCommand oSqlCmd = new SqlCommand("spUserLoginBrowse", oSqlConn.MyConnection))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionUser();
                if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    using (DataTable dtUser = new DataTable())
                    {
                        dtUser.Load(oRead);
                        if (dtUser.Rows.Count > 0) // UserID Found
                        {
                            isUser = true;
                        }
                    }
                }
                oSqlCmd.Dispose();
            }
        }
        catch (Exception ex)
        {

            Messagebox(ex.Message);
        }
        return isUser;
    }
    protected void GetLdapDomain()
    {
        var configuration = WebConfigurationManager.OpenWebConfiguration("~");
        var appsettings = (AppSettingsSection)configuration.GetSection("appSettings");
        appsettings = (AppSettingsSection)configuration.
        GetSection("appSettings");
        if (appsettings != null)
        {
            configuration.AppSettings.Settings["DefaultActiveDirectoryServer"].Value = WebEdcMonitoringClass.sGetldapDomain(oSqlConn.MyConnection);
            configuration.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
    protected void LoginProcess()
    {
        string adPath = "LDAP://" + System.Configuration.ConfigurationSettings.AppSettings["DefaultActiveDirectoryServer"];
        ActiveDirectoryValidator adAuth = new ActiveDirectoryValidator(adPath);
        domainName = System.Environment.UserDomainName;
        int iTotalTimeExpires = WebEdcMonitoringClass.iGetTotalTimeAutomaticLogOut(oSqlConn.MyConnection);
        int iTotalMaxTimeExpires = WebEdcMonitoringClass.iGetTotalMaxTimeAutomaticLogOut(oSqlConn.MyConnection);
        if (iTotalTimeExpires > iTotalMaxTimeExpires) Messagebox("Configuration Invalid, Please Contact Administrator Web"); else
        if (!string.IsNullOrEmpty(domainName))
        {
           if (true == adAuth.IsAuthenticated1(domainName, sGetUserID(), sGetPassword()))
            //if (true == adAuth.validateUserByBind(sGetUserID(), sGetPassword()))
            {
                if (isUserDomainFoundActive()) // If is User
                {
                        if (!CheckDaysLockedUserDomain())
                        {
                            if (!isLockedUserDomain())
                            {
                                try
                                {
                                    using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn.MyConnection))
                                    {
                                        oSqlCmd.CommandType = CommandType.StoredProcedure;
                                        oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionUser();
                                        if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
                                        using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                                        {
                                            using (DataTable dtUser = new DataTable())
                                            {
                                                dtUser.Load(oRead);
                                                if (dtUser.Rows.Count == 1) // UserID Found
                                                {
                                                    UserPrivilege.sPrivilege = dtUser.Rows[0]["UserRights"].ToString();

                                                    bool isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
                                                    if (isSuperUser)
                                                    {
                                                        sUserName = dtUser.Rows[0]["UserName"].ToString().Trim();

                                                    HttpCookie UserInfo = new HttpCookie("UserInfo");
                                                    UserInfo["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                    //sUserID = UserInfo["UserID"].ToString();
                                                    UserInfo.Value = DateTime.Now.ToString();
                                                    //UserInfo.Expires = DateTime.Now.AddDays(1);
                                                    UserInfo.Expires = DateTime.Now.AddMinutes(iTotalTimeExpires);
                                                    Response.Cookies.Add(UserInfo);


                                                    //HttpCookie UserInfo = new HttpCookie("UserInfo");
                                                    //    UserInfo["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                    //    //UserInfo.Value = DateTime.Now.ToString();
                                                    //    UserInfo.Expires = DateTime.Now.AddMinutes(iTotalTimeExpires);
                                                    //    Response.Cookies.Add(UserInfo);

                                                        if (dtUser.Columns.IndexOf("GroupID") > -1)
                                                            if (dtUser.Columns["GroupID"] != null)
                                                                sGroupID = dtUser.Rows[0]["GroupID"].ToString();

                                                        if (UserInfo != null)
                                                        {
                                                            if (UserInfo.Values.Count != 0)
                                                            {

                                                            Session["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                            Session["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                            Session["ExpiresTime"] = UserInfo.Expires;
                                                            Session["UserDomain"] = Application["UserDomain"];
                                                            UserInfo["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                            Application["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                            Application["LastLogin"] = dtUser.Rows[0]["LastLogin"].ToString();
                                                            Application["UnsuccessfulLogin"] = sLastLoginInvalid;
                                                            //Application["UnsuccessfulLogin"] = DateTime.Now.AddDays(-1);
                                                            Application["LoginTime"] = DateTime.Now.ToString();
                                                            Application["NotesAccess"] = sNotesAccess;

                                                                //Session["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                                //Session["UserSession"] = sUserDomain;
                                                                //Session["ExpiresTime"] = UserInfo.Expires;
                                                                //Session["UserDomain"] = Application["UserDomain"];
                                                                //Application["UserSession"] = sUserDomain;
                                                                //Application["LastLogin"] = sLastLogin;
                                                                //Application["UnsuccessfulLogin"] = sLastLoginInvalid;
                                                                //Application["LoginTime"] = UserInfo.Value;
                                                                //Application["NotesAccess"] = sNotesAccess;

                                                                //WebEdcMonitoringClass.InsertSingleUserActive(oSqlConn.MyConnection, sUserDomain, DateTime.Now);
                                                                string sInfosecEventLogs = WebEdcMonitoringClass.sGetsInfosecEventLogs();
                                                                WebEdcMonitoringClass.InsertAuditTrailLogin(oSqlConn.MyConnection, sGetUserID(), sInfosecEventLogs);
                                                                WebEdcMonitoringClass.UpdateSuccessLogin(oSqlConn.MyConnection,sGetUserID(),DateTime.Now.ToString());
                                                            }
                                                            string ReqUrl = Request["ReturnUrl"];
                                                            if (ReqUrl != null)
                                                            { Response.Redirect(ReqUrl); }
                                                            Response.Redirect("~/Home.aspx", false);

                                                        }
                                                    }
                                                    else
                                                    {
                                                                //sUserName = dtUser.Rows[0]["UserName"].ToString().Trim();
                                                                HttpCookie UserInfo = new HttpCookie("UserInfo");
                                                                UserInfo["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                                //sUserID = UserInfo["UserID"].ToString();
                                                                UserInfo.Value = DateTime.Now.ToString();
                                                                //UserInfo.Expires = DateTime.Now.AddDays(1);
                                                                UserInfo.Expires = DateTime.Now.AddMinutes(iTotalTimeExpires);
                                                                Response.Cookies.Add(UserInfo);

                                                                //HttpCookie UserInfo = new HttpCookie("UserInfo");
                                                                //UserInfo["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                                //UserInfo.Value = DateTime.Now.ToString();
                                                                //UserInfo.Expires = DateTime.Now.AddMinutes(iTotalTimeExpires);
                                                                //Response.Cookies.Add(UserInfo);


                                                                if (dtUser.Columns.IndexOf("GroupID") > -1)
                                                                    if (dtUser.Columns["GroupID"] != null)
                                                                        sGroupID = dtUser.Rows[0]["GroupID"].ToString();

                                                                if (UserInfo != null)
                                                                {
                                                                    if (UserInfo.Values.Count != 0)
                                                                    {

                                                            Session["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                            Session["UserSession"] = sUserDomain;
                                                            Session["ExpiresTime"] = UserInfo.Expires;
                                                            Session["UserDomain"] = Application["UserDomain"];
                                                            UserInfo["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                            Application["UserSession"] = sUserDomain;
                                                            Application["LastLogin"] = dtUser.Rows[0]["LastLogin"].ToString();
                                                            Application["UnsuccessfulLogin"] = sLastLoginInvalid;
                                                            //Application["UnsuccessfulLogin"] = DateTime.Now.AddDays(-1);
                                                            Application["LoginTime"] =  UserInfo.Value;
                                                            Application["NotesAccess"] = sNotesAccess;

                                                            //Session["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                            //Session["UserSession"] = sUserDomain;
                                                            //Session["ExpiresTime"] = UserInfo.Expires;
                                                            //Session["UserDomain"] = Application["UserDomain"];
                                                            //Application["UserSession"] = sUserDomain;
                                                            //Application["LastLogin"] = sLastLogin;
                                                            //Application["UnsuccessfulLogin"] = sLastLoginInvalid;
                                                            //Application["LoginTime"] = UserInfo.Value;
                                                            //Application["NotesAccess"] = sNotesAccess;

                                                            //WebEdcMonitoringClass.InsertSingleUserActive(oSqlConn.MyConnection, dtUser.Rows[0]["UserDomain"].ToString(), DateTime.Now);
                                                            string sInfosecEventLogs = WebEdcMonitoringClass.sGetsInfosecEventLogs();
                                                                        WebEdcMonitoringClass.InsertAuditTrailLogin(oSqlConn.MyConnection, sGetUserID(), sInfosecEventLogs);
                                                                        WebEdcMonitoringClass.UpdateSuccessLogin(oSqlConn.MyConnection,sGetUserID(),DateTime.Now.ToString());
                                                                    }
                                                                    string ReqUrl = Request["ReturnUrl"];
                                                                                if (ReqUrl != null)
                                                                    { Response.Redirect(ReqUrl); }
                                                                    Response.Redirect("~/Home.aspx", false);

                                                                }

                                                    }

                                                }
                                                else
                                                {
                                                    Messagebox("User Cant Login");
                                                }

                                            }
                                        }
                                        Dispose();
                                    }

                                }
                                catch (Exception ex)
                                {
                                    //HttpContext.Current.Session["errorMessage"] = ex.Message.ToString();
                                    //HttpContext.Current.Session["errorDetails"] = ex.StackTrace.ToString();
                                    //string Causes = "";
                                    //if (ex.InnerException != null) { Causes = ex.Source.ToString() + "<br />" + ex.InnerException.ToString(); }
                                    //else { Causes = ex.Source.ToString(); }
                                    //HttpContext.Current.Session["errorCauses"] = Causes.ToString();
                                    //HttpContext.Current.Response.Redirect("~/Errors.aspx");

                                }
                            }
                        }

                }
        else
        {
            Session["UserSession"] = UserName.Text;
            Session["UserRequest"] = sGetUserID();
            Application["sUserDomain"] = UserName.Text;

            Response.Redirect("~/FrmRegistration.aspx");

        }
    } else
            {
                if (isUserDomainFoundActive()) // If is User
                {
                    AddInvalidPassword(sGetUserID());
                }
                else
                {
                    sUserID = sGetUserID();
                    if (!isUserFoundActive())
                        WebEdcMonitoringClass.InsertAuditTrailLogin(oSqlConn.MyConnection, sGetUserID(), "unsuccessful Log-on attempts");
                    else
                    {
                        if (!CheckDaysLocked())
                        {
                            if (!isLockedUserLocal())
                            {
                                try
                                {
                                    using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn.MyConnection))
                                    {
                                        oSqlCmd.CommandType = CommandType.StoredProcedure;
                                        oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();
                                        if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
                                        using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                                        {
                                            using (DataTable dtUser = new DataTable())
                                            {
                                                dtUser.Load(oRead);
                                                oSqlCmd.Dispose();
                                                oRead.Close();
                                                if (dtUser.Rows.Count == 1) // UserID Found
                                                {
                                                    UserPrivilege.sPrivilege = dtUser.Rows[0]["UserRights"].ToString();

                                                    bool isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
                                                    if (isSuperUser)
                                                    {
                                                        sUserName = dtUser.Rows[0]["UserName"].ToString().Trim();

                                                        HttpCookie UserInfo = new HttpCookie("UserInfo");

                                                        UserInfo["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                        sUserID = UserInfo["UserID"].ToString();
                                                        UserInfo.Value = DateTime.Now.ToString();
                                                        //UserInfo.Expires = DateTime.Now.AddDays(1);
                                                        UserInfo.Expires = DateTime.Now.AddMinutes(iTotalTimeExpires);
                                                        Response.Cookies.Add(UserInfo);

                                                        if (dtUser.Columns.IndexOf("GroupID") > -1)
                                                            if (dtUser.Columns["GroupID"] != null)
                                                                sGroupID = dtUser.Rows[0]["GroupID"].ToString();

                                                        if (UserInfo != null)
                                                        {
                                                            if (UserInfo.Values.Count != 0)
                                                            {
                                                                Session["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                                Session["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                                UserInfo["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                                Application["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                                Application["LastLogin"] = dtUser.Rows[0]["LastLogin"].ToString();
                                                                Application["UnsuccessfulLogin"] = sLastLoginInvalid;
                                                                //Application["UnsuccessfulLogin"] = DateTime.Now.AddDays(-1);
                                                                Session["ExpiresTime"] = UserInfo.Expires;
                                                                Application["LoginTime"] = DateTime.Now.ToString();
                                                                Application["NotesAccess"] = sNotesAccess;

                                                                // WebEdcMonitoringClass.InsertSingleUserActive(oSqlConn.MyConnection, dtUser.Rows[0]["UserID"].ToString(), DateTime.Now);
                                                                string sInfosecEventLogs = WebEdcMonitoringClass.sGetsInfosecEventLogs();
                                                                WebEdcMonitoringClass.InsertAuditTrailLogin(oSqlConn.MyConnection, sGetUserID(), sInfosecEventLogs);

                                                            }
                                                            string ReqUrl = Request["ReturnUrl"];
                                                            if (ReqUrl != null)
                                                            { Response.Redirect(ReqUrl); }
                                                            Response.Redirect("~/Home.aspx", false);
                                                            //Response.Redirect("~/Home.aspx");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        sUserName = dtUser.Rows[0]["UserName"].ToString().Trim();

                                                        HttpCookie UserInfo = new HttpCookie("UserInfo");
                                                        UserInfo["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                        sUserID = UserInfo["UserID"].ToString();
                                                        UserInfo.Value = DateTime.Now.ToString();
                                                        //UserInfo.Expires = DateTime.Now.AddDays(1);
                                                        UserInfo.Expires = DateTime.Now.AddMinutes(iTotalTimeExpires);
                                                        Response.Cookies.Add(UserInfo);

                                                        if (dtUser.Columns.IndexOf("GroupID") > -1)
                                                            if (dtUser.Columns["GroupID"] != null)
                                                                sGroupID = dtUser.Rows[0]["GroupID"].ToString();

                                                        if (UserInfo != null)
                                                        {
                                                            if (UserInfo.Values.Count != 0)
                                                            {
                                                                Session["UserID"] = dtUser.Rows[0]["UserID"].ToString();
                                                                Session["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                                UserInfo["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                                Application["UserSession"] = dtUser.Rows[0]["UserID"].ToString();
                                                                Application["LastLogin"] = dtUser.Rows[0]["LastLogin"].ToString();

                                                                Application["UnsuccessfulLogin"] = sLastLoginInvalid;
                                                                //Application["UnsuccessfulLogin"] = DateTime.Now.AddDays(-1);
                                                                Session["ExpiresTime"] = UserInfo.Expires;
                                                                Application["LoginTime"] = DateTime.Now.ToString();
                                                                Application["NotesAccess"] = sNotesAccess;

                                                                // WebEdcMonitoringClass.InsertSingleUserActive(oSqlConn.MyConnection, dtUser.Rows[0]["UserID"].ToString(), DateTime.Now);
                                                                string sInfosecEventLogs = WebEdcMonitoringClass.sGetsInfosecEventLogs();
                                                                WebEdcMonitoringClass.InsertAuditTrailLogin(oSqlConn.MyConnection, sGetUserID(), sInfosecEventLogs);
                                                            }
                                                            string ReqUrl = Request["ReturnUrl"];
                                                            if (ReqUrl != null)
                                                            { Response.Redirect(ReqUrl); }
                                                            Response.Redirect("~/Home.aspx", false);

                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Messagebox("User Cant Login");
                                                }

                                            }
                                        }
                                        Dispose();
                                    }

                                }
                                catch (Exception ex)
                                {
                                    //HttpContext.Current.Session["errorMessage"] = ex.Message.ToString();
                                    //HttpContext.Current.Session["errorDetails"] = ex.StackTrace.ToString();
                                    //string Causes = "";
                                    //if (ex.InnerException != null) { Causes = ex.Source.ToString() + "<br />" + ex.InnerException.ToString(); }
                                    //else { Causes = ex.Source.ToString(); }
                                    //HttpContext.Current.Session["errorCauses"] = Causes.ToString();
                                    //HttpContext.Current.Response.Redirect("~/Errors.aspx");

                                }
                            }
                        }
                    }

                }
            }

        }
        else Messagebox("Your Not Have User Domain");
    }



    protected void AddInvalidPassword(string UserDomain)
    {
        bool bReturn, bReturnCount,binvalid = true;
        if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
        using (SqlCommand oSqlCmd = new SqlCommand("spUserLoginAddInvalidPass", oSqlConn.MyConnection))
        {
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@sUserDomain", SqlDbType.VarChar).Value = UserDomain;
            oSqlCmd.Parameters.Add("@iLocked", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            oSqlCmd.Parameters.Add("@iCount", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
            oSqlCmd.Parameters.Add("@iInvalid", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output; 
            oSqlCmd.ExecuteNonQuery();
            bReturn = oSqlCmd.Parameters["@iLocked"].Value.ToString() == "1" ? true : false;
            bReturnCount = oSqlCmd.Parameters["@iCount"].Value.ToString() == "1" ? true : false;
            binvalid = oSqlCmd.Parameters["@iCount"].Value.ToString() == "1" ? true : false;
            oSqlCmd.Dispose();
        }
        if (bReturn == true) {
            WebEdcMonitoringClass.InsertAuditTrailLogin(oSqlConn.MyConnection,sGetUserID(), "User Locked (5 wrong password attempted)");
            Messagebox("Your account has been Disable, because 5 wrong password attempted");
        }
        if (!(bReturn) && bReturnCount == true)
        {            
            WebEdcMonitoringClass.InsertAuditTrailLogin(oSqlConn.MyConnection, sGetUserID(), "2 wrong password attempted");
            SendMailInvalidPassword(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "User has been 2 wrong password attempted", sEmail, WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection,"ADMIN"), sUserName, sLastLoginInvalid, UserDomain, sUserID, sEmail);
            Messagebox("Your Account Login, 2 wrong password attempted. After 5 wrong times it will be disabled");
        }
        if (binvalid == true)
        {
            WebEdcMonitoringClass.InsertAuditTrailLogin(oSqlConn.MyConnection, sGetUserID(), "Invalid Password");
            Messagebox("Your account wrong password , After 5 wrong times it will be disabled");
        }
    }

    protected void SendMailInvalidPassword(string sFrom, string MailSubject, string SendTo, string[] CCTO, string sRequest, string sDate, string sUserDomain, string sUserAccess, string sEmail)
    {
        string strUrl = "";
        Uri url = HttpContext.Current.Request.Url;
        string Url = url.GetLeftPart(UriPartial.Authority);
        string HtmlBody = "";
        try
        {
            string strCorporateName = "";
            strCorporateName = WebEdcMonitoringClass.sGetCorporateName(oSqlConn.MyConnection);

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(sFrom,"Notification Email System " + strCorporateName);
            msg.Subject = MailSubject;
            msg.To.Add(new MailAddress(SendTo));
            if (CCTO.Length != 0)
            {
                for (int i = 0; i <= CCTO.Length - 1; i++)
                {
                    string[] strCC = CCTO[i].Split(';');
                    msg.CC.Add(strCC[0].Trim());
                }
            }
            string[] Bcc = WebEdcMonitoringClass.LoadRecipientAdmin(oSqlConn.MyConnection, "SUPADMIN");
            if (Bcc.Length != 0)
            {
                for (int i = 0; i <= Bcc.Length - 1; i++)
                {
                    string[] strCC = Bcc[i].Split(';');
                    msg.Bcc.Add(strCC[0].Trim());
                }
            }
            strUrl = WebEdcMonitoringClass.sGetURLWeb(oSqlConn.MyConnection);
            HtmlBody = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
                        "<html xmlns='http://www.w3.org/1999/xhtml'>" +
                        "<head>" +
                        "<title></title>" +
                        "</head>" +
                        "<body>" +
                        "<div style='font-size:12px; font-family: Calibri;'>" +
                        "<div>" +
                        "<br /><br />" +
                        "Dear&nbsp;<b>" + sRequest + ",</b><br /><br />" +
                        "User has been 2 wrong password attempted with details below:<br />" +
                        "<table style='font-size: 11px; font-family: Tahoma, Geneva, sans-serif'>" +
                        "<tr><td>User Name</td><td>:</td><td>" + sRequest + "</td></tr>" +
                        "<tr><td>Date Last Login Invalid</td><td>:</td><td>" + sDate + "</td></tr>" +
                        "<tr><td>User Domain</td><td>:</td><td>" + sUserDomain + "</td></tr>" +
                        "<tr><td>User Access Type</td><td>:</td><td>" + sUserAccess + "</td></tr>" +
                        "<tr><td>Email</td><td>:</td><td>" + sEmail + "</td></tr>" +
                        "</table><br />" +
                        "Please click folowing link to view the Application:<br /><br />" +
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                        "<a href='" + strUrl + "'>" + strUrl + "</a>" +
                        "<br />" +
                        "<br />" +
                        "* try to copy and paste the link into your web browser if it&#39;s not working.</div>" +
                        "<br />" +
                        "<br />" +
                        "Best Regards,<br /><br />" +
                        "Email System " + strCorporateName +
                        "<div>" +
                        "<br />" +
                        "<br />" +
                        "<b>This is an automatic email notifier from Notification System "+ strCorporateName+"<br />You don&#39;t need to reply it.</b></div>" +
                        "</div>" +
                        "</div>" +
                        "</body>" +
                        "</html>";
            msg.IsBodyHtml = true;
            msg.Body = HtmlBody;
            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), WebEdcMonitoringClass.sGetSenderPassword(oSqlConn.MyConnection));
            //client.Port = 587; // You can use Port 25 if 587 is blocked (mine is!)
            client.Port = WebEdcMonitoringClass.iGetSenderPort(oSqlConn.MyConnection); // You can use Port 25 if 587 is blocked (mine is!)
            //client.Host = "smtp.office365.com";
            client.Host = WebEdcMonitoringClass.sGetSenderHost(oSqlConn.MyConnection);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Send(msg);
        }
        catch (Exception ex)
        {
            //Response.Write("Could not send the e-mail - error: " + ex.Message);
            Messagebox("Could not send the e-mail - error: : " + ex.Message);

        }

    }

    protected void SendMailUserlocked(string sFrom, string MailSubject, string SendTo, string[] CCTO, string sRequest, string sDate, string sUserDomain, string sUserAccess, string sEmail)
    {
        string strUrl = "";
        Uri url = HttpContext.Current.Request.Url;
        string Url = url.GetLeftPart(UriPartial.Authority);
        string HtmlBody = "";
        try
        {
            string strCorporateName = "";
            strCorporateName = WebEdcMonitoringClass.sGetCorporateName(oSqlConn.MyConnection);

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(sFrom, "Notification Email System " + strCorporateName);
            msg.Subject = MailSubject;
            msg.To.Add(new MailAddress(SendTo));
            if (CCTO.Length != 0)
            {
                for (int i = 0; i <= CCTO.Length - 1; i++)
                {
                    string[] strCC = CCTO[i].Split(';');
                    msg.CC.Add(strCC[0].Trim());
                }
            }
            string[] Bcc = WebEdcMonitoringClass.LoadRecipientAdmin(oSqlConn.MyConnection, "SUPADMIN");
            if (Bcc.Length != 0)
            {
                for (int i = 0; i <= Bcc.Length - 1; i++)
                {
                    string[] strCC = Bcc[i].Split(';');
                    msg.Bcc.Add(strCC[0].Trim());
                }
            }

            strUrl = WebEdcMonitoringClass.sGetURLWeb(oSqlConn.MyConnection);

            HtmlBody = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
                        "<html xmlns='http://www.w3.org/1999/xhtml'>" +
                        "<head>" +
                        "<title></title>" +
                        "</head>" +
                        "<body>" +
                        "<div style='font-size:12px; font-family: Calibri;'>" +
                        "<div>" +
                        "<br /><br />" +
                        "Dear&nbsp;<b>" + sRequest + ",</b><br /><br />" +
                        MailSubject+" with details below:<br />" +
                        "<table style='font-size: 11px; font-family: Tahoma, Geneva, sans-serif'>" +
                        "<tr><td>User Name</td><td>:</td><td>" + sRequest + "</td></tr>" +
                        "<tr><td>Last Login </td><td>:</td><td>" + sDate + "</td></tr>" +
                        "<tr><td>User Domain</td><td>:</td><td>" + sUserDomain + "</td></tr>" +
                        "<tr><td>User Access Type</td><td>:</td><td>" + sUserAccess + "</td></tr>" +
                        "<tr><td>Email</td><td>:</td><td>" + sEmail + "</td></tr>" +
                        "</table><br />" +
                        "Please click folowing link to view the Application:<br /><br />" +
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                        "<a href='" + strUrl + "'>" + strUrl + "</a>" +
                        "<br />" +
                        "<br />" +
                        "* try to copy and paste the link into your web browser if it&#39;s not working.</div>" +
                        "<br />" +
                        "<br />" +
                        "Best Regards,<br /><br />" +
                        "Email System " + strCorporateName +
                        "<div>" +
                        "<br />" +
                        "<br />" +
                        "<b>This is an automatic email notifier from Notification System " + strCorporateName + "<br />You don&#39;t need to reply it.</b></div>" +
                        "</div>" +
                        "</div>" +
                        "</body>" +
                        "</html>";
            msg.IsBodyHtml = true;
            msg.Body = HtmlBody;
            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), WebEdcMonitoringClass.sGetSenderPassword(oSqlConn.MyConnection));
            //client.Port = 587; // You can use Port 25 if 587 is blocked (mine is!)
            client.Port = WebEdcMonitoringClass.iGetSenderPort(oSqlConn.MyConnection); // You can use Port 25 if 587 is blocked (mine is!)
            //client.Host = "smtp.office365.com";
            client.Host = WebEdcMonitoringClass.sGetSenderHost(oSqlConn.MyConnection);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Send(msg);
        }
        catch (Exception ex)
        {
            //Response.Write("Could not send the e-mail - error: " + ex.Message);
            Messagebox("Could not send the e-mail - error: : " + ex.Message);

        }

    }
    
    protected bool isUserDomainFound()
    {
        bool isUserDomain = false;
        try
        {
            oSqlConn.Connection();
            using (SqlCommand oSqlCmd = new SqlCommand("spUser_Domain_Browse", oSqlConn.MyConnection))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionUserDomain();
                if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    using (DataTable dtUser = new DataTable())
                    {
                        dtUser.Load(oRead);
                        if (dtUser.Rows.Count > 0) // UserID Found
                        {
                            isUserDomain = true;
                            sUserDomain = dtUser.Rows[0]["UserDomain"].ToString();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Messagebox(ex.Message);
        }
        return isUserDomain;
    }


    protected string msUserID = "201";
    protected string msSessionCacheValue = "";

    protected string x;
    //protected void LoginUser_Authenticate(object sender, AuthenticateEventArgs e)
    //{
    //    try
    //    {
    //        if (IsActiveDirectoryEnabled)
    //        {
    //            if (ActiveDirectoryConnector.IsUserLoggedIn(LoginUser.UserName, LoginUser.Password))
    //            //if (ActiveDirectoryConnector.IsUserLoggedIn(sGetUserID(), sGetPassword()))
    //            {
    //                sUserDomain = LoginUser.UserName;
    //                e.Authenticated = true;
    //            }
    //            else
    //            {
    //                e.Authenticated = false;
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        e.Authenticated = false;
    //        lblError.Visible = false;
    //        lblError.Text = ex.Message;
    //    }
    //}

    protected void login_Click(object sender, EventArgs e)
    {
        oSqlConn.Connection();
        if (!WebEdcMonitoringClass.IsSingleUserActive(oSqlConn.MyConnection,sGetUserID()))
          {
            if (ValidatePassword(sGetPasswordDomain(), out x))
            {
                LoginProcess();
            } 
            lblError.Text = x;

        } else Messagebox("Canot Login,You have two parallel sessions");
    }

    protected void bRegistration_Click(object sender, EventArgs e)
    {
        if (isUserDomainFound()) // If is User
        {
            Messagebox("has been Registred, Please Click Back Button ");
        }
        else
        {
            //GetLdapDomain();
            string adPath = "LDAP://" + System.Configuration.ConfigurationSettings.AppSettings["DefaultActiveDirectoryServer"];
            ActiveDirectoryValidator adAuth = new ActiveDirectoryValidator(adPath);
            domainName = System.Environment.UserDomainName;
            if (!string.IsNullOrEmpty(domainName))
            {
                if (true == adAuth.IsAuthenticated1(domainName, sGetUserID(), sGetPassword()))
                {
                    HttpCookie UserInfo = new HttpCookie("UserInfo");
                    UserInfo.Value = DateTime.Now.ToString();
                    Response.Cookies.Add(UserInfo);

                    Session["UserSession"] = sGetUserID();
                    Session["UserRequest"] = sGetUserID();
                    Application["sUserDomain"] = UserName.Text;
                    //Response.Redirect("~/FrmRegistration.aspx", false);

                    Response.Redirect("~/FrmRegistration.aspx");
                }
                else Messagebox("Not Connected to Domain");
            }
            else Messagebox("Your Not Have User Domain");

        }
    }
}