﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Account_Login" Async="true" %>

<%--<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>--%>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <%--    <h2><%: Title %>.</h2>--%>
    <div class="jumbotron">
    </div>
    <div class="panel panel-default">
        <div class="row">
            <div class="col-md-8">

                <div class="col-md-12">
                </div>
                <div class="col-md-12">
                    <div class="page-header">
                        <div class="form-group">
                            <%--<h1><img src="../images/ingenico_logo.png" class="img-responsive" /></h1>--%>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="form-group">
                        <right><h2>EDC MONITORING</h2></right>
                    </div>
                    <div class="bottom-footer">
                        <div class="form-group">
                            <right><h5>Version 1.05</h5></right>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row" ></div>
                <div class="panel panel-default">
                    <div class="panel-body">
<%--                        <div class="page-header">
                            <h3>Login</h3>
                        </div>
                        <div>
                                    <asp:Login ID="LoginUser" runat="server" Width="478px">
                                    </asp:Login>
                        </div>--%>
                        <div class="form-group">
                            <label for="UserID">User ID</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-user"></span>
                                </span>
                                <asp:TextBox ID="UserName" runat="server" CssClass="form-control" TextMode="SingleLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Password">Password</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-star"></span>
                                </span>
                                <asp:TextBox ID="Password" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                        <hr />
                        <div class="col-lg-offset-1">
                            <asp:Label ID="lblError" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="input-group pull-right">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-lock"></span>
                            </span>
                            <asp:Button ID="login" runat="server" Text="Login" CssClass="btn btn-success" OnClick="login_Click" />
                            <asp:Button ID="bRegistration" runat="server" Text="Registration" CssClass="btn btn-info" OnClick="bRegistration_Click"  />   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div>
        <HTML><BODY>
<%--
 <table border="0" ">
  <tr><td align="left"><%=  Session["Test"].ToString()  %></td></tr>
  <tr><td align="left"><%= msSessionCacheValue %></td></tr>
 </table>--%>

</body></html>
    </div>
</asp:Content>

