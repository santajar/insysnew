﻿using ClosedXML.Excel;
using InSysClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebClass;

public partial class dataMonitorTrx : System.Web.UI.Page
{
    protected string sConnString = null;
    protected ConnectionClass sqlconn = new ConnectionClass();
    public static DataTable dtDataMonitoring;
    protected string sTimestamp;
    protected string sSerialNumber;

    protected static string sUserID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HttpCookie UserInfo = Request.Cookies["UserInfo"];

            if (UserInfo != null)
            {
                if (UserInfo.Values.Count != 0)
                {
                    if (Session.Count != 0)
                    {
                        sUserID = Session["UserID"].ToString();
                        if (Session["UserSession"] != null)
                        {
                            sqlconn.Connection();
                            sTimestamp = Request.QueryString["Time"];
                            sSerialNumber = Request.QueryString["Serial"];
                            LoadData();
                        }

                    }
                    else
                    {
                        Response.Redirect("~/Account/Login.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }
    }


    private void LoadData()
    {
        if (!string.IsNullOrEmpty(sTimestamp) && !string.IsNullOrEmpty(sSerialNumber))
        {
            gridviewTransaction.DataSource = dtGetDetailTrx(sTimestamp, sSerialNumber);
            gridviewTransaction.DataBind();
        }

    }

    protected void MessageBox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }

    public DataTable dtGetDetailTrx(string sTimestamp, string sSerialNumber)
    {
        sqlconn.Connection();
        DataTable dtTemp = new DataTable();
        SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPEdcMonitorBrowseDetailTrx, sqlconn.MyConnection);
        sqlcmd.CommandType = CommandType.StoredProcedure;
        sqlcmd.Parameters.Add("@sTimestamp", SqlDbType.VarChar).Value = sTimestamp;
        sqlcmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = sSerialNumber;
        using (SqlDataReader reader = sqlcmd.ExecuteReader())
            if (reader.HasRows)
                dtTemp.Load(reader);
        dtDataMonitoring = dtTemp.Copy();
      
        return dtTemp;
    }

        protected void gridviewTransaction_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
       gridviewTransaction.PageIndex = e.NewPageIndex;
       LoadData();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportToExlsx();
    }



    protected void ExportToExlsx()
    {
        sTimestamp = Request.QueryString["Time"];
        sSerialNumber = Request.QueryString["Serial"];
        if (!string.IsNullOrEmpty(sTimestamp) && !string.IsNullOrEmpty(sSerialNumber))
        {
            sqlconn.Connection();
            DataTable dtTemp = new DataTable();
            SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPEdcMonitorBrowseDetailTrx, sqlconn.MyConnection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.Parameters.Add("@sTimestamp", SqlDbType.VarChar).Value = sTimestamp;
            sqlcmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = sSerialNumber;
            using (SqlDataReader reader = sqlcmd.ExecuteReader())
                if (reader.HasRows)
                    dtTemp.Load(reader);

            DataSet ds = new DataSet();
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                sqlcmd.Connection = sqlconn.MyConnection;
                sda.SelectCommand = sqlcmd;
                sda.Fill(ds);
                ds.Tables[0].TableName = "tbEdcMonitor";
            }

            string serverRoot = "";
            serverRoot = Server.MapPath("~");
            string sLogDir = serverRoot + "\\Document\\Export";
            if (!Directory.Exists(sLogDir)) Directory.CreateDirectory(sLogDir);

            string FileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "DataMonitoringDetail.xlsx";

            for (int i = 0; i < ds.Tables.Count; i++)
            {
                DataTable dt = ds.Tables[i];
                XLWorkbook wb = new XLWorkbook();
                wb.Worksheets.Add(dt, dt.TableName);
                wb.SaveAs(Server.MapPath("~/Document/Export/") + FileName);
            }

            string allowedExtensions = ".xlsx";

            string fileName = FileName;
            string filePath = "Document/Export/";

            if (fileName != "" && fileName.IndexOf(".") > 0)
            {
                bool extensionAllowed = false;
                string fileExtension = fileName.Substring(fileName.LastIndexOf('.'), fileName.Length - fileName.LastIndexOf('.'));

                string[] extensions = allowedExtensions.Split(',');
                for (int a = 0; a < extensions.Length; a++)
                {
                    if (extensions[a] == fileExtension)
                    {
                        extensionAllowed = true;
                        break;
                    }
                }

                if (extensionAllowed)
                {
                    if (File.Exists(Server.MapPath(filePath + fileName)))
                    {
                        HttpResponse Response = HttpContext.Current.Response;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                        Response.ContentType = "application/octet-stream";
                        Response.WriteFile(Server.MapPath(filePath + fileName));
                        Response.Flush();
                        Response.End();
                    }
                    else
                    {
                        MessageBox("File could not be found");
                    }
                }
                else
                {
                    MessageBox("File extension is not allowed");
                }
            }
            else
            {
                MessageBox("Error - no file to download");
            }
        }
    }
}