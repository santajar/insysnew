﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using InSysClass;
using System.Text;
using System.IO;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using ClosedXML.Excel;
using WebClass;

public partial class LookUpCSI : System.Web.UI.Page
{
    protected string sConnString = null;
    protected ConnectionClass sqlconn = new ConnectionClass();
    public static DataTable dtDataMonitoring;
    protected DataTable oTable;

    protected SqlCommand oLastComm;

    public enum operand
    {
        Is = 1,
        Contains,
        BeginsWith,
        EndsWith,
    }
    
    /// <summary>
    /// Current UserID
    /// </summary>
    protected static string sUserID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HttpCookie UserInfo = Request.Cookies["UserInfo"];

            if (UserInfo != null)
            {
                if (UserInfo.Values.Count != 0)
                {
                    if (Session.Count != 0)
                    {
                        sUserID = Session["UserID"].ToString();
                        if (Session["UserSession"] != null)
                        {
                            sqlconn.Connection();
                            string sCondition = sGenerateCondition();
                            dtDataMonitoring = dtGetDataMonitoring(sCondition);
                            gridviewMonitoring.DataSource = dtDataMonitoring;
                            gridviewMonitoring.DataBind();
                        }

                    }
                    else
                    {
                        Response.Redirect("~/Account/Login.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }
    }

    protected DataTable dtGetDataMonitoring(string sCondition)
    {
        DataTable dtTemp = new DataTable();
        SqlCommand sqlcmd = new SqlCommand("spEdcMonitorCSIBrowse", sqlconn.MyConnection);
        sqlcmd.CommandType = CommandType.StoredProcedure;
        if ((!string.IsNullOrEmpty(txtDateStart.Text)) || (!string.IsNullOrEmpty(txtDateEnd.Text)))
        { 
        sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", txtDateStart.Text);
        sqlcmd.Parameters.Add("@sDateEnd", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", txtDateEnd.Text);
        }else
        {
            sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
            sqlcmd.Parameters.Add("@sDateEnd", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
        }        
        sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
        using (SqlDataReader reader = sqlcmd.ExecuteReader())
            if (reader.HasRows)
                dtTemp.Load(reader);
        return dtTemp;
    }

    private SqlCommand GetFilterCommand(string sCondition)
    {
        DataTable dtTemp = new DataTable();
        SqlCommand sqlcmd = new SqlCommand("spEdcMonitorCSIBrowse", sqlconn.MyConnection);
        sqlcmd.CommandType = CommandType.StoredProcedure;
        if ((!string.IsNullOrEmpty(txtDateStart.Text)) || (!string.IsNullOrEmpty(txtDateEnd.Text)))
        {
            sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", txtDateStart.Text);
            sqlcmd.Parameters.Add("@sDateEnd", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", txtDateEnd.Text);
        }
        else
        {
            sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
            sqlcmd.Parameters.Add("@sDateEnd", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
        }
        sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
        using (SqlDataReader reader = sqlcmd.ExecuteReader())
            if (reader.HasRows) dtTemp.Load(reader);
        return sqlcmd;
    }

    protected void gridviewMonitoring_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridviewMonitoring.PageIndex = e.NewPageIndex;
        StartFilter();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        StartFilter();
    }

    protected void StartFilter()
    {
        sqlconn.Connection();
        string sCondition = sGenerateCondition();
        if (sqlconn.MyConnection.State == ConnectionState.Open)
        {
            dtDataMonitoring = dtGetDataMonitoring(sCondition);
            gridviewMonitoring.DataSource = dtDataMonitoring;
            gridviewMonitoring.DataBind();
        }
    }

    protected string sGenerateCondition()
    {
        string sCondition = null;
        sCondition = sGenerateDetailCondition(dropdownlistSN, txtboxSN, "TID", sCondition);
        sCondition += sGenerateDetailCondition(dropdownlistTID, txtboxTID, "TerminalID", sCondition);
        sCondition += sGenerateDetailCondition(dropdownlistSoftware, txtboxSoftware, "MID", sCondition);
        return sCondition;
    }      

    private string sGenerateDetailCondition(DropDownList dropdownlist, TextBox textbox, string sColumnName
        , string sCondition)
    {
        string sTemp = null;
        operand iOperand = operand.Is;
        if (!string.IsNullOrEmpty(textbox.Text))
        {
            switch (dropdownlist.SelectedValue)
            {
                case "1":
                    iOperand = operand.Is;
                    break;
                case "2":
                    iOperand = operand.Contains;
                    break;
                case "3":
                    iOperand = operand.BeginsWith;
                    break;
                case "4":
                    iOperand = operand.EndsWith;
                    break;
            }
            if (string.IsNullOrEmpty(sCondition))
                sTemp = string.Format(" WHERE {0} ", sColumnName);
            else
                sTemp = string.Format(" AND {0} ", sColumnName);

            switch (iOperand)
            {
                case operand.Is:
                    sTemp = string.Format("{0} = '{1}' ", sTemp, textbox.Text);
                    break;
                case operand.Contains:
                    sTemp = string.Format("{0} LIKE '%{1}%' ", sTemp, textbox.Text);
                    break;
                case operand.BeginsWith:
                    sTemp = string.Format("{0} LIKE '{1}%' ", sTemp, textbox.Text);
                    break;
                case operand.EndsWith:
                    sTemp = string.Format("{0} LIKE '%{1}' ", sTemp, textbox.Text);
                    break;
            }
        }
        return sTemp;
    }
    protected void gridviewMonitoring_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        sqlconn.Connection();
        Int32 selectedrowindex = Convert.ToInt32(e.ToString());
    }

    protected void btnExportXls_Click(object sender, EventArgs e)
    {
        ExportToExlsx();
    }
    
    protected void ExportToExlsx()
    {
        sqlconn.Connection();
        DataSet ds = new DataSet();

        string sCondition = sGenerateCondition();
        oLastComm = GetFilterCommand(sCondition);
        DataTable dtTemp = new DataTable();
        SqlCommand sqlcmd = new SqlCommand("spEdcMonitorCSIBrowse", sqlconn.MyConnection);
        sqlcmd.CommandType = CommandType.StoredProcedure;
        if ((!string.IsNullOrEmpty(txtDateStart.Text)) || (!string.IsNullOrEmpty(txtDateEnd.Text)))
        {
            sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", txtDateStart.Text);
            sqlcmd.Parameters.Add("@sDateEnd", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", txtDateEnd.Text);
        }
        else
        {
            sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
            sqlcmd.Parameters.Add("@sDateEnd", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
        }
        sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
        using (SqlDataReader reader = sqlcmd.ExecuteReader())
            if (reader.HasRows) dtTemp.Load(reader);

        gridviewMonitoring.DataSource = dtTemp;
        gridviewMonitoring.DataBind();

        using (SqlDataAdapter sda = new SqlDataAdapter())
        {
            sqlcmd.Connection = sqlconn.MyConnection;
            sda.SelectCommand = sqlcmd;
            sda.Fill(ds);
            ds.Tables[0].TableName = "tbEdcMonitorCSI";
        }

        string serverRoot = "";
        serverRoot = Server.MapPath("~");
        string sLogDir = serverRoot + "\\Document\\Export";
        if (!Directory.Exists(sLogDir)) Directory.CreateDirectory(sLogDir);

        string FileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "ExportCSI.xlsx";

        for (int i = 0; i < ds.Tables.Count; i++)
        {
            DataTable dt = ds.Tables[i];
            XLWorkbook wb = new XLWorkbook();
            wb.Worksheets.Add(dt, dt.TableName);
            wb.SaveAs(Server.MapPath("~/Document/Export/") + FileName);
        }

        string allowedExtensions = ".xlsx";

        string fileName = FileName;
        string filePath = "Document/Export/";

        if (fileName != "" && fileName.IndexOf(".") > 0)
        {
            bool extensionAllowed = false;
            string fileExtension = fileName.Substring(fileName.LastIndexOf('.'), fileName.Length - fileName.LastIndexOf('.'));

            string[] extensions = allowedExtensions.Split(',');
            for (int a = 0; a < extensions.Length; a++)
            {
                if (extensions[a] == fileExtension)
                {
                    extensionAllowed = true;
                    break;
                }
            }

            if (extensionAllowed)
            {
                if (File.Exists(Server.MapPath(filePath + fileName)))
                {
                    HttpResponse Response = HttpContext.Current.Response;
                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                    Response.ContentType = "application/octet-stream";
                    Response.WriteFile(Server.MapPath(filePath + fileName));
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    MessageBox("File could not be found");
                }
            }
            else
            {
                MessageBox("File extension is not allowed");
            }
        }
        else
        {
            MessageBox("Error - no file to download");
        }
    }
    protected void MessageBox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }               
    
    protected void gridviewMonitoring_Sorting(object sender, GridViewSortEventArgs e)
    {
        StartFilter();
    }

    
}
