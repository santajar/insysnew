﻿<%@ Page Title="Upload Data" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="UploadData.aspx.cs" Inherits="UploadData" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="script_css" runat="server">
    <script type="text/jscript">
        var validFilesTypes = ["xls"];
        function ValidateFile() {
            var file = document.getElementById("<%=FUAttach.ClientID%>");
            var path = file.value;
            var ext = path.substring(path.lastIndexOf(".") + 1, path.length).toLowerCase();
            var isValidFile = false;
            for (var i = 0; i < validFilesTypes.length; i++) {
                if (ext == validFilesTypes[i]) {
                    isValidFile = true;
                    break;
                }
            }
            if (!isValidFile) {
                alert("Invalid uploaded file type.\nPlease upload a file with extension: " + validFilesTypes.join(", ") + "!");
                return false;
            }
            return isValidFile;
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-target="#filter"><b>Download Template File</b></div>
        <div class="panel-body" id="Body1">
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <asp:Button ID="btnDownload" runat="server" Text="Download Template File" CssClass="btn-primary btn-sm" Font-Bold="True" OnClick="btnDownload_Click" Width="160px" />
                    </div>
                    <div class="col-md-2">
                        <asp:Button ID="btnLookUp" runat="server" Text="Look Up CSI" CssClass="btn-primary btn-sm" Font-Bold="True" OnClick="btnLookUp_Click" Width="160px"  />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-target="#filter"><b>Upload Data File</b></div>
        <div class="panel-body" id="filter">
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">Choose File</label>
                    </div>
                    <div class="col-md-2">
                        <asp:FileUpload ID="FUAttach" CssClass="btn-info btn-sm" Visible="true" runat="server" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-1">
                        <asp:Button ID="btnUpload" runat="server" Width="80px" Text="Upload" CssClass="btn-success btn-sm" OnClick="btnUpload_Click" Font-Bold="True" />
                    </div>
                    <div class="col-md-1">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="panel panel-default" visible="false">
                    <div class="panel-heading" visible="false" data-toggle="collapse" data-target="#list"><strong>Upload Data List</strong></div>
                    <div class="panel-body" id="panel_grid">
                        <div style="overflow: scroll">
                            <asp:GridView ID="gv" runat="server" Width="100%">
                                <%--EmptyDataText="No records found">--%>
                                <HeaderStyle BackColor="#89A0FE" />
                                <Columns>
                                    <%--                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="txtCSI" Text='<%# Eval("CSI") %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:BoundField DataField="TerminalID" HeaderText="TerminalID" ItemStyle-Width="30" />
                                <asp:BoundField DataField="MerchantID" HeaderText="MerchantID" ItemStyle-Width="150" />--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
