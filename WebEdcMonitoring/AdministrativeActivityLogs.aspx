﻿<%@ Page Title="AdministrativeActivityLogs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="AdministrativeActivityLogs.aspx.cs" Inherits="AdministrativeActivityLogs" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="script_css" runat="server">
    <style type="text/css">
        .hideGridColumn {
            display: none;
        }        
        .gvwCasesPager a {
            color: #fff;
            text-decoration:underline;
            margin-left:5px;
            margin-right:5px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2>Administrative Activity Logs</h2>
        <asp:Panel ID="panelFilter" GroupingText="Filter" runat="server" Font-Size="Small">
            <table style="width: 80%;">
                <tr>
                    <td>User ID</td>
                    <td>
                        <asp:DropDownList ID="dropdownlistSN" runat="server">
                            <asp:ListItem Value="1">Is</asp:ListItem>
                            <asp:ListItem Value="2">Contains</asp:ListItem>
                            <asp:ListItem Value="3">Begins with</asp:ListItem>
                            <asp:ListItem Value="4">Ends with</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="txtboxSN" runat="server" Height="20px" Width="250px" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Action Description</td>
                    <td>
                        <asp:DropDownList ID="dropdownlistTID" runat="server">
                            <asp:ListItem Value="1">Is</asp:ListItem>
                            <asp:ListItem Value="2">Contains</asp:ListItem>
                            <asp:ListItem Value="3">Begins with</asp:ListItem>
                            <asp:ListItem Value="4">Ends with</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="txtboxTID" runat="server" Height="20px" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="height: 29px">Access Time From</td>
                    <td style="height: 29px">
                        <asp:TextBox ID="txtDateStart" runat="server" Height="20px" Width="150px" />
                        <ajaxToolkit:CalendarExtender ID="dateStart" runat="server" TargetControlID="txtDateStart" Format="yyyy-MM-dd" />
                    </td>
                    <td style="height: 29px">Date To </td>
                    <td style="height: 29px">
                        <asp:TextBox ID="txtDateEnd" runat="server" Height="20px" Width="150px" />
                        <ajaxToolkit:CalendarExtender ID="dateEnd" runat="server" TargetControlID="txtDateEnd" Format="yyyy-MM-dd" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSubmit" CssClass="btn-primary btn-sm"  runat="server" Text="Filter" OnClick="btnSubmit_Click" Width="168px" Font-Bold="True" />
                    </td>

                    <td>
                        <asp:Button ID="btnExportXls" CssClass="btn-primary btn-sm"  runat="server" Text="Export To Excel" Width="168px" OnClick="btnExportXls_Click" Font-Bold="True" /></td>

                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Font-Size="Small">
            <asp:Label ID="lblText" runat="server" ForeColor="#009933" />
        </asp:Panel>
        <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblepanelFilter" runat="server" TargetControlID="panelFilter" CollapseControlID="pHeader" ExpandControlID="pHeader"
            Collapsed="true" TextLabelID="lblText" CollapsedText="Show Filter" ExpandedText="Hide Filter"
            CollapsedSize="0"></ajaxToolkit:CollapsiblePanelExtender>
    </div>
    <div class="row">
        <div class="panel panel-default" visible="false">
            <div class="panel-heading" visible="false" data-toggle="collapse" data-target="#list"><strong>Administrative Activity List</strong></div>
            <div class="panel-body" id="panel_grid">
                <div style="overflow: scroll">

                    <asp:GridView ID="gridviewMonitoring" runat="server" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="gridviewMonitoring_PageIndexChanging" AutoGenerateColumns="False" AllowSorting="True" HorizontalAlign="Center"  OnSelectedIndexChanging="gridviewMonitoring_SelectedIndexChanging" OnSorting="gridviewMonitoring_Sorting" >
                        <AlternatingRowStyle BackColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerSettings Position="TopAndBottom" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Yellow" HorizontalAlign="Center" CssClass ="gvwCasesPager" Font-Bold="true"/> 
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        <Columns>
                            <asp:TemplateField HeaderText="No" ItemStyle-Width="50">
                                <ItemTemplate>
                                    <asp:Label ID="lblNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="AccessTime" HeaderText="AccessTime" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fff}" SortExpression="AccessTime" />
                            <asp:BoundField DataField="UserID" HeaderText="User ID" ReadOnly="True" SortExpression="UserID" />
                            <asp:BoundField DataField="ActionDescription" HeaderText="Action Description" ReadOnly="True" SortExpression="ActionDescription" />

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
