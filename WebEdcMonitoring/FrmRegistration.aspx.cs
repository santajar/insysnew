﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using WebClass;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Account_FrmRegistration : System.Web.UI.Page
{
    protected ConnectionClass oSqlConn = new ConnectionClass();
    protected string sUserDomain = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserSession"] != null)
            {
                if (Session["UserRequest"] != null)
                {
                    txtUserDomain.Text = Application["sUserDomain"].ToString();
                    if (Application["Notes"] != null) lblNotes.Text = Application["Notes"].ToString(); else lblNotes.Text = "(New Registration)";
                }
                }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }

            //HttpCookie UserInfo = Request.Cookies["UserInfo"];
            //if (UserInfo != null)
            //{
            //    if (UserInfo.Values.Count != 0)
            //    {
            //        if (Session.Count != 0)
            //        {
            //            if (Session["UserRequest"]!=null)
            //            txtUserDomain.Text = Application["sUserDomain"].ToString();
            //        }
            //        else
            //        {
            //            Response.Redirect("~/Account/Login.aspx");
            //        }
            //    }
            //    else
            //    {
            //        Response.Redirect("~/Account/Login.aspx");
            //    }
            //}
            //else
            //{
            //    Response.Redirect("~/Account/Login.aspx");
            //}
        }

    }
    protected void Messagebox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }

    protected string sConditionDomain()
    {
        return " WHERE UserDomain='" + txtUserDomain.Text + "'";
    }
    protected bool isUserDomainFound()
    {
        bool isUserDomain = false;
        try
        {
            oSqlConn.Connection();
            using (SqlCommand oSqlCmd = new SqlCommand("spUser_Domain_Browse", oSqlConn.MyConnection))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sConditionDomain();
                if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    using (DataTable dtUser = new DataTable())
                    {
                        dtUser.Load(oRead);
                        if (dtUser.Rows.Count > 0) // UserID Found
                        {
                            isUserDomain = true;
                            sUserDomain = dtUser.Rows[0]["UserDomain"].ToString();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Messagebox(ex.Message);
        }
        return isUserDomain;
    }

    protected void SendMail(string sFrom, string MailSubject, string SendTo, string [] CCTO, string sRequest, string sDate, string sUserDomain, string sEmail)
    {
        string strUrl = "";
        Uri url = HttpContext.Current.Request.Url;
        string Url = url.GetLeftPart(UriPartial.Authority);
        string HtmlBody = "";
        try
        {
            string strCorporateName = "";
            strCorporateName = WebEdcMonitoringClass.sGetCorporateName(oSqlConn.MyConnection);

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(sFrom, "Notification Email System " + strCorporateName);
            msg.Subject = MailSubject;
            msg.To.Add(new MailAddress(SendTo));
            if (CCTO.Length != 0)
            {
                for (int i = 0; i <= CCTO.Length - 1; i++)
                {
                    string[] strCC = CCTO[i].Split(';');
                    msg.CC.Add(strCC[0].Trim());
                }
            }
            string[] Bcc = WebEdcMonitoringClass.LoadRecipientAdmin(oSqlConn.MyConnection, "SUPADMIN");
            if (Bcc.Length != 0)
            {
                for (int i = 0; i <= Bcc.Length - 1; i++)
                {
                    string[] strCC = Bcc[i].Split(';');
                    msg.Bcc.Add(strCC[0].Trim());
                }
            }

            strUrl = WebEdcMonitoringClass.sGetURLWeb(oSqlConn.MyConnection)+"/FrmRegistrationApprove.aspx";

            HtmlBody = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
                        "<html xmlns='http://www.w3.org/1999/xhtml'>" +
                        "<head>" +
                        "<title></title>" +
                        "</head>" +
                        "<body>" +
                        "<div style='font-size:12px; font-family: Calibri;'>" +
                        "<div>" +
                        "<br /><br />" +
                        "Dear&nbsp;<b> Admin,</b><br /><br />" +
                        "A new User request has been submitted with details below:<br />" +
                        "<table style='font-size: 11px; font-family: Tahoma, Geneva, sans-serif'>" +
                        "<tr><td>Request From</td><td>:</td><td>" + sRequest + "</td></tr>" +
                        "<tr><td>Date</td><td>:</td><td>" + sDate + "</td></tr>" +
                        "<tr><td>User Domain</td><td>:</td><td>" + sUserDomain + "</td></tr>" +
                        "<tr><td>Email</td><td>:</td><td>" + sEmail + "</td></tr>" +
                        "</table><br />" +
                        "It&#39;s with pleasure to request your approval process." +
                        "Please click folowing link to view the details:<br /><br />" +
                        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                        "<a href='" + strUrl + "'>" + strUrl + "</a>" +
                        "<br />" +
                        "<br />" +
                        "* try to copy and paste the link into your web browser if it&#39;s not working.</div>" +
                        "<br />" +
                        "<br />" +
                        "Best Regards,<br /><br />" +
                        "Email System " + strCorporateName +
                        "<div>" +
                        "<br />" +
                        "<br />" +
                        "<b>This is an automatic email notifier from Notification System " + strCorporateName + "<br />You don&#39;t need to reply it.</b></div>" +
                        "</div>" +
                        "</div>" +
                        "</body>" +
                        "</html>";
            msg.IsBodyHtml = true;
            msg.Body = HtmlBody;
            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), WebEdcMonitoringClass.sGetSenderPassword(oSqlConn.MyConnection));
            //client.Port = 587; // You can use Port 25 if 587 is blocked (mine is!)
            client.Port = WebEdcMonitoringClass.iGetSenderPort(oSqlConn.MyConnection); ; // You can use Port 25 if 587 is blocked (mine is!)
            //client.Host = "smtp.office365.com";
            client.Host = WebEdcMonitoringClass.sGetSenderHost(oSqlConn.MyConnection);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Send(msg);
        }
        catch (Exception ex)
        {
            Messagebox("Could not send the e-mail - error: : " + ex.Message);
        }

    }

    protected void Cancel_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Cookies.Clear();
        Response.Redirect("~/Account/Login.aspx");
    }

    protected void sendmail2(string sFrom, string SendTo, string MailSubject, string Body)
        {
         try
        {
            MailMessage mailObj = new MailMessage(sFrom, SendTo, MailSubject, Body);
  
            SmtpClient SMTPServer = new SmtpClient(WebEdcMonitoringClass.sGetSenderHost(oSqlConn.MyConnection));
            try
            {
                SMTPServer.Send(mailObj);
            }
            catch (Exception ex)
            {
                
            }
        }
        catch (Exception ex)
        {

        }
        }
    protected void bRequest_Click(object sender, EventArgs e)
    {
        if (isUserDomainFound()) // If is User
        {
            Messagebox("has been Registred, Please Click Back Button ");
        }
        else
        {
            try
            {
                oSqlConn.Connection();
                using (SqlCommand Cmd = new SqlCommand("spUser_Domain_Insert", oSqlConn.MyConnection))
                {
                    Cmd.CommandType = CommandType.StoredProcedure;
                    Cmd.Parameters.Add("@UserDomain", SqlDbType.VarChar).Value = txtUserDomain.Text.ToString();
                    Cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = txtUserName.Text.ToString();
                    Cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = txtEmail.Text.ToString();

                    if (oSqlConn.MyConnection.State == ConnectionState.Closed) oSqlConn.MyConnection.Open();
                    Cmd.ExecuteNonQuery();

                    string sDate = string.Format("{0:MM/dd/yyyy H:mm:ss}", DateTime.Now);
                    SendMail(WebEdcMonitoringClass.sGetSenderEmail(oSqlConn.MyConnection), "A new User WebEdcMonitoring request", txtEmail.Text.ToString(), WebEdcMonitoringClass.LoadRecipient(oSqlConn.MyConnection, "ADMIN"), txtUserName.Text.ToString(), sDate, txtUserDomain.Text.ToString(), txtEmail.Text.ToString());
                    Response.Redirect("~/Account/Login.aspx");
                }


            }
            catch (Exception ex)
            {
                Messagebox("Error : " + ex.Message);
            }
        }

    }
}