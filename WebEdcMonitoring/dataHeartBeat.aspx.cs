﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using InSysClass;
using WebClass;
using System.Text;
using System.Drawing;
using System.IO;
using System.Web.UI.HtmlControls;
using ClosedXML.Excel;

public partial class dataMonitor : System.Web.UI.Page
{
    protected ConnectionClass sqlconn = new ConnectionClass();
    protected DataTable dtDataHeartBeat;    
    protected SqlCommand oLastComm;

    public enum operand
    {
        Is = 1,
        Contains,
        BeginsWith,
        EndsWith,
    }

    enum DetailType
    {
        TRX = 1,
        SIM,
    }

    protected string sUserSession = "";
    protected string sUserID = "";
    protected string sUserDomain = "";
    protected string sUnsuccessfulLogin = "";
    protected string sLoginTime = "";
    protected string sExpires = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HttpCookie UserInfo = Request.Cookies["UserInfo"];

            if (UserInfo != null)
            {
                if (UserInfo.Values.Count != 0)
                {
                    if (Session.Count != 0)
                    {
                        if (Session["UserSession"] != null)
                        {
                            sqlconn.Connection();
                            Onlyread();
                        } else Response.Redirect("~/Account/Login.aspx");

                    }
                    else
                    {
                        Response.Redirect("~/Account/Login.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                if (Application["UserSession"] != null) sUserSession = Application["UserSession"].ToString(); else sUserSession = "";
                sqlconn.Connection();
                WebEdcMonitoringClass.DeleteSingleUserActive(sqlconn.MyConnection, sUserSession);
                Session.Clear();
                Response.Clear();
                Response.Redirect("~/Account/Login.aspx");

            }
        }
        //if (!IsPostBack)
        //{
        //    HttpCookie UserInfo = Request.Cookies["UserInfo"];

        //    if (UserInfo != null)
        //    {
        //        if (UserInfo.Values.Count != 0)
        //        {
        //            if (Session["UserID"] != null)
        //            {
        //                sUserID = Session["UserID"].ToString();

        //                sqlconn.Connection();
        //                if (Session["UserDomain"] != null)
        //                {
        //                    sUserDomain = Session["UserDomain"].ToString();
        //                    if (!WebEdcMonitoringClass.CheckTimeAutomaticLogOut(sqlconn.MyConnection, sUserDomain, Request, Response))
        //                    {
        //                        Onlyread();
        //                    }
        //                    else
        //                    {
        //                        Response.Redirect("~/Account/Login.aspx", true);
        //                        MessageBox("Your Account Expire Session, Please back login");
        //                        Session["UserID"] = "";
        //                    }
        //                }
        //                else
        //                {
        //                    if (Application["LoginTime"] != null) sLoginTime = Application["LoginTime"].ToString(); else sLoginTime = "";
        //                    sExpires = Session["ExpiresTime"].ToString();
        //                    if (!WebEdcMonitoringClass.isExpire(sqlconn.MyConnection, sUserID, sLoginTime, sExpires))
        //                    {
        //                        Onlyread();
        //                    }
        //                    else
        //                    {
        //                        MessageBox("Your Account Expire Session, Please back login");
        //                        Session["UserID"] = "";
        //                        Response.Redirect("~/Account/Login.aspx", true);
        //                    }
        //                }
        //            }
        //            else Response.Redirect("~/Account/Login.aspx", true);
        //        }
        //    }
        //}
    }

    private void Onlyread()
    {
            sqlconn.Connection();
            string sCondition = sGenerateCondition2();
            dtDataHeartBeat = dtGetDataHeartBeatByDate2(sCondition);
            gridviewMonitoring.DataSource = dtDataHeartBeat;
            gridviewMonitoring.DataBind();

    }
    protected DataTable dtGetDataHeartBeatByDate2(string sCondition)
    {
        DataTable dtTemp = new DataTable();
        SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPHeartBeatrBrowseByDate, sqlconn.MyConnection);
        sqlcmd.CommandType = CommandType.StoredProcedure;
        if (!string.IsNullOrEmpty(txtDateEnd.Text))  sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", txtDateEnd.Text); 
        else sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
        sqlcmd.Parameters.Add("@sStatus", SqlDbType.Int).Value = DropDownStatus.SelectedIndex;
        sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
        using (SqlDataReader reader = sqlcmd.ExecuteReader())
            if (reader.HasRows)
                dtTemp.Load(reader);
        try
        {
            DataRow[] resultTidActive = dtTemp.Select("Status = 'ACTIVE'");
            DataRow[] resultTidInActive = dtTemp.Select("Status = 'INACTIVE'");
            DataRow[] resultTidNotList = dtTemp.Select("Status = 'NOT LIST'");
            lblActive.Text = resultTidActive.Length.ToString();
            //lblActive.Text = resultTidActive.Count().ToString();
            lblInActive.Text = resultTidInActive.Length.ToString();
            //lblInActive.Text = resultTidInActive.Count().ToString();
            //lblNotList.Text = resultTidNotList.Count().ToString();
            lblNotList.Text = resultTidNotList.Length.ToString();
        }
        catch (Exception ex)
        {
        }

        return dtTemp;
    }
    protected void gridviewMonitoring_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridviewMonitoring.PageIndex = e.NewPageIndex;
        StartFilter();
    }

    protected void StartFilter()
    {
        string sCondition = sGenerateCondition2();
        dtDataHeartBeat = dtGetDataHeartBeatByDate2(sCondition);
        gridviewMonitoring.DataSource = dtDataHeartBeat;
        gridviewMonitoring.DataBind();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        sqlconn.Connection();
        StartFilter();
    }
    
    protected string sGenerateCondition2()
    {
        string sCondition = null;
        sCondition = sGenerateDetailCondition2(dropdownlistSN, txtboxSN, "SerialNumber", sCondition);
        sCondition += sGenerateDetailCondition2(dropdownlistTID, txtboxTID, "CSI", sCondition);
        sCondition += sGenerateDetailCondition2(dropdownlistSoftware, txtboxSoftware, "SoftwareVersion", sCondition);
        return sCondition;
    }
    
    private string sGenerateStatusCondition(string sCondition)
    {
        string sTemp = "";
        if (!string.IsNullOrEmpty(DropDownStatus.Text))
        {
            if (!string.IsNullOrEmpty(sCondition))
                sTemp += " AND ";
            else sTemp += " WHERE ";
            sTemp += string.Format("Status = '" + DropDownStatus.Text + "'");
        }
        return sTemp;
    }
    
    private string sGenerateDetailCondition2(DropDownList dropdownlist, TextBox textbox, string sColumnName
        , string sCondition)
    {
        string sTemp = null;
        operand iOperand = operand.Is;
        if (!string.IsNullOrEmpty(textbox.Text))
        {
            switch (dropdownlist.SelectedValue)
            {
                case "1":
                    iOperand = operand.Is;
                    break;
                case "2":
                    iOperand = operand.Contains;
                    break;
                case "3":
                    iOperand = operand.BeginsWith;
                    break;
                case "4":
                    iOperand = operand.EndsWith;
                    break;
            }
            if (string.IsNullOrEmpty(sCondition))
                sTemp = string.Format(" WHERE {0} ", sColumnName);

            switch (iOperand)
            {
                case operand.Is:
                    sTemp = string.Format("{0} = '{1}' ", sTemp, textbox.Text);
                    break;
                case operand.Contains:
                    sTemp = string.Format("{0} LIKE '%{1}%' ", sTemp, textbox.Text);
                    break;
                case operand.BeginsWith:
                    sTemp = string.Format("{0} LIKE '{1}%' ", sTemp, textbox.Text);
                    break;
                case operand.EndsWith:
                    sTemp = string.Format("{0} LIKE '%{1}' ", sTemp, textbox.Text);
                    break;
            }
        }
        return sTemp;
    }

    protected void Export(string FileName, GridView dgView)
    {
       // dt = city.GetAllCity();//your datatable
        string attachment = "attachment; filename="+ FileName +".xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.ms-excel";
        //Response.Cells.NumberFormat = "@";
        
        string tab = "";
        foreach (DataColumn dc in dtDataHeartBeat.Columns)
        {
            
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");
        int i;
        foreach (DataRow dr in dtDataHeartBeat.Rows)
        {
            tab = "";
            for (i = 0; i < dtDataHeartBeat.Columns.Count; i++)
            {
                Response.Write(string.Format(tab + "' "+ dr[i].ToString() ));
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }

    
    

    private void ExportToExcel2(DataTable table, string filePath)
    {
        StreamWriter sw = new StreamWriter(filePath, false);
        sw.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
        sw.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
        sw.Write("<BR><BR><BR>");
        sw.Write("<Table border='1' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
        int columnscount = table.Columns.Count;

        for (int j = 0; j < columnscount; j++)
        {
            sw.Write("<Td>");
            sw.Write("<B>");
            sw.Write(table.Columns[j].ToString());
            sw.Write("</B>");
            sw.Write("</Td>");
        }
        sw.Write("</TR>");
        foreach (DataRow row in table.Rows)
        {
            sw.Write("<TR>");
            for (int i = 0; i < table.Columns.Count; i++)
            {
                sw.Write("<Td>");
                sw.Write("'"+row[i].ToString());
                sw.Write("</Td>");
            }
            sw.Write("</TR>");
        }
        sw.Write("</Table>");
        sw.Write("</font>");
        sw.Close();
    }

    protected void gridviewMonitoring_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var linkbuttonDetailSIM = (LinkButton)e.Row.FindControl("lnkDetailSIM");
            linkbuttonDetailSIM.CommandArgument = string.Format("{0};{1}", DetailType.SIM.GetHashCode(), e.Row.RowIndex);
        }
    }

    protected void gridviewMonitoring_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string sCmdArgument = (string)e.CommandArgument;
        if (sCmdArgument.Contains(";"))
        {
            DetailType dettype = int.Parse(sCmdArgument.Split(';')[0]) == DetailType.TRX.GetHashCode() ? DetailType.TRX : DetailType.SIM;
            int iIndex = Convert.ToInt32(sCmdArgument.Split(';')[1]);
            if (iIndex > -1)
            {
                string sTimestamp = (gridviewMonitoring.Rows[iIndex]).Cells[2].Text;
                string sSerialNumber = (gridviewMonitoring.Rows[iIndex]).Cells[5].Text;
              //  string sProCode = (gridviewMonitoring.Rows[iIndex]).Cells[4].Text;
                if (!string.IsNullOrEmpty(sTimestamp) && !string.IsNullOrEmpty(sSerialNumber))
                {
                    //if (dettype == DetailType.TRX)
                    //    Response.Redirect(string.Format("~/dataMonitorTrx.aspx?Time={0}&Serial={1}", sTimestamp, sSerialNumber));
                    //else
                        Response.Redirect(string.Format("~/dataMonitorSIM.aspx?Time={0}&Serial={1}&Flag={2}", sTimestamp, sSerialNumber, "heartbeat"));
                }
            }
        }
    }

    private SqlCommand GetFilterCommand(string sCondition)
    {
        DataTable dtTemp = new DataTable();
        SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPHeartBeatrBrowseByDate, sqlconn.MyConnection);
        sqlcmd.CommandType = CommandType.StoredProcedure;
        if (!string.IsNullOrEmpty(txtDateEnd.Text)) sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = txtDateEnd.Text;
        else sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
        sqlcmd.Parameters.Add("@sStatus", SqlDbType.Int).Value = DropDownStatus.SelectedIndex;
        sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
        using (SqlDataReader reader = sqlcmd.ExecuteReader())
            if (reader.HasRows)
                dtTemp.Load(reader);
        try
        {
            DataRow[] resultTidActive = dtTemp.Select("Status = 'Active'");
            DataRow[] resultTidInActive = dtTemp.Select("Status = 'InActive'");
            DataRow[] resultTidNotList = dtTemp.Select("Status = 'Not List'");
            lblActive.Text = resultTidActive.Length.ToString();
            //lblActive.Text = resultTidActive.Count().ToString();
            lblActive.Text = resultTidInActive.Length.ToString();
            //lblInActive.Text = resultTidInActive.Count().ToString();
            //lblNotList.Text = resultTidNotList.Count().ToString();
            lblNotList.Text = resultTidNotList.Length.ToString();
        }
        catch (Exception ex)
        {
        }
        return sqlcmd;

    }
    
    protected void btnExportExel_Click(object sender, EventArgs e)
    {

        try
        {
            sqlconn.Connection();
            string sCondition = sGenerateCondition2();
            oLastComm = GetFilterCommand(sCondition);
            DataTable dtTemp = new DataTable();
            
            SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPHeartBeatrBrowseByDate, sqlconn.MyConnection);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            if (!string.IsNullOrEmpty(txtDateEnd.Text)) sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = txtDateEnd.Text;
            else sqlcmd.Parameters.Add("@sDate", SqlDbType.VarChar).Value = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
            sqlcmd.Parameters.Add("@sStatus", SqlDbType.Int).Value = DropDownStatus.SelectedIndex;
            sqlcmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition;
            using (SqlDataReader reader = sqlcmd.ExecuteReader())
                if (reader.HasRows)
                    dtTemp.Load(reader);

            gridviewMonitoring.DataSource = dtTemp;
            gridviewMonitoring.DataBind();

            DataSet ds = new DataSet();
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                sqlcmd.Connection = sqlconn.MyConnection;
                sda.SelectCommand = sqlcmd;
                sda.Fill(ds);
                ds.Tables[0].TableName = "tbEdcMonitorHeartBeat";
            }

            string serverRoot = "";
            serverRoot = Server.MapPath("~");
            string sLogDir = serverRoot + "\\Document\\Export";
            if (!Directory.Exists(sLogDir)) Directory.CreateDirectory(sLogDir);

            string FileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "ExportHeartBeat.xlsx";

            for (int i = 0; i < ds.Tables.Count; i++)
            {
                DataTable dt = ds.Tables[i];
                XLWorkbook wb = new XLWorkbook();
                wb.Worksheets.Add(dt, dt.TableName);
                wb.SaveAs(Server.MapPath("~/Document/Export/") + FileName);
            }

            string allowedExtensions = ".xlsx";

            string fileName = FileName;
            string filePath = "Document/Export/";

            if (fileName != "" && fileName.IndexOf(".") > 0)
            {
                bool extensionAllowed = false;
                string fileExtension = fileName.Substring(fileName.LastIndexOf('.'), fileName.Length - fileName.LastIndexOf('.'));

                string[] extensions = allowedExtensions.Split(',');
                for (int a = 0; a < extensions.Length; a++)
                {
                    if (extensions[a] == fileExtension)
                    {
                        extensionAllowed = true;
                        break;
                    }
                }

                if (extensionAllowed)
                {
                    if (File.Exists(Server.MapPath(filePath + '/' + fileName)))
                    {
                        HttpResponse Response = HttpContext.Current.Response;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                        Response.ContentType = "application/octet-stream";
                        Response.WriteFile(Server.MapPath(filePath + fileName));
                        Response.Flush();
                        Response.End();
                    }
                    else
                    {
                        MessageBox("File could not be found");
                    }
                }
                else
                {
                    MessageBox("File extension is not allowed");
                }
            }
            else
            {
                MessageBox("Error - no file to download");
            }

        }
        catch (Exception oException)
        {
            MessageBox(oException.Message);
        }

    }


    protected void MessageBox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }

    protected void gridviewMonitoring_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Label lbldeleteID = (Label)gridviewMonitoring.Rows[e.RowIndex].Cells[1].FindControl("lblID");
        string sQuery = string.Format("DELETE FROM tbEdcMonitorHeartBeat where ID = {0}", lbldeleteID.Text.ToString());
        SqlCommand sqlcmd = new SqlCommand(sQuery, sqlconn.MyConnection);
        if (sqlconn.MyConnection.State != ConnectionState.Open) sqlconn.MyConnection.Open();
        SqlDataReader myReader = default(SqlDataReader);
        myReader = sqlcmd.ExecuteReader();

        StartFilter();
        MessageBox("Delete Success");
    }

    //protected void timerRefresh_Tick(object sender, EventArgs e)
    //{
    //    Onlyread();
    //    lblTimer.Text = "Panel refreshed at: " + DateTime.Now.ToLongTimeString();
    //}
}