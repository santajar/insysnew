using System;
using System.IO;
using System.Threading;
using InSysClass;

namespace WebClass
{
    public  class Traces
    {
        //static string sPath = Environment.CurrentDirectory;

        //Path.GetDirectoryName(            Assembly.GetExecutingAssembly().GetName().CodeBase);

        static string sDirName = "\\TRACE\\";
        static string sFilename = "Trace_";
        static string sFileType = ".log";

        static string sPath = System.Web.HttpContext.Current.Server.MapPath("~");


        public static void Write(string sMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFilename + CommonConsole.sDateYYYYMMDD() + sFileType;
                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                while (true)
                {
                    if (CommonConsole.IsOpenFileAllowed(sFile))
                        break;
                    else
                        Thread.Sleep(100);
                }
                CommonLib.Write2File(sFile, CommonConsole.sFullTime() + " " +
                    sMessage,
                    true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void Write(byte[] arrbMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFilename + CommonConsole.sDateYYYYMMDD() + sFileType;
                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                byte[] arrbValue = new byte[1024];

                arrbValue = CommonLib.arrbStringtoByteArray(CommonConsole.sFullTime());

                Array.Copy(arrbMessage, 0, arrbValue, arrbValue.Length + 1, arrbMessage.Length);
                while (true)
                {
                    if (CommonConsole.IsOpenFileAllowed(sFile))
                        break;
                    else
                        Thread.Sleep(100);
                }
                CommonLib.Write2File(sFile, arrbValue);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void Write(string sFileName, string sMessage)
        {
            try
            {
                string sFile = null;
                if (!isTraceFolderExist())
                    Directory.CreateDirectory(sPath + sDirName);
                sFile = sPath + sDirName + sFileName;
                if (!File.Exists(sFile))
                    using (FileStream fs = new FileStream(sFile, FileMode.OpenOrCreate))
                        fs.Close();
                while (true)
                {
                    if (CommonConsole.IsOpenFileAllowed(sFile))
                        break;
                    else
                        Thread.Sleep(100);
                }
                CommonLib.Write2File(sFile, sMessage, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static bool isTraceFolderExist()
        {

            return Directory.Exists(sPath + sDirName);
        }
        
    }
}