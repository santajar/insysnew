﻿using System;
using System.DirectoryServices;

namespace WebClass
{
    public class ActiveDirectoryValidator
    {

        private string _path;
        private string _filterAttribute;

        public ActiveDirectoryValidator(string path)
        {
            _path = path;
        }
        public static bool IsInRole(string userName, string role)
        {
            try
            {
                role = role.ToLowerInvariant();
                DirectorySearcher ds = new DirectorySearcher(new DirectoryEntry(null));
                ds.Filter = "SAMAccountName=" + userName;
                SearchResult sr = ds.FindOne();
                DirectoryEntry de = sr.GetDirectoryEntry();
                PropertyValueCollection dir = de.Properties["memberOf"];
                for (int i = 0; i < dir.Count; ++i)
                {
                    string s = dir[i].ToString().Substring(3);
                    s = s.Substring(0, s.IndexOf(',')).ToLowerInvariant();
                    if (s == role) return true;
                }
                throw new Exception();
            }
            catch
            {
                return false;
            }
        }
        public bool IsAuthenticated(string domainName, string userName, string password)
        {
            string domainAndUsername = domainName + @"\" + userName;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, password);
            try
            {
                // Bind to the native AdsObject to force authentication.
                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(&(objectClass=user)(objectcategory=person)(mail=" + userName + "*))";

                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (null == result)
                {
                    return false;
                }
                // Update the new path to the user in the directory
                _path = result.Path;
                _filterAttribute = (String)result.Properties["cn"][0];
            }
            //catch (LdapException ex)
            //{
            //    if (ex.ErrorCode == 49)
            //    {
            //        throw new Exception("Invalid user authentication. Please input a valid user name & pasword and try again.", ex);
            //    }
            //    else
            //    {
            //        throw new Exception("Active directory server not found.", ex);
            //    }
            //}
            //catch (DirectoryOperationException ex)
            //{
            //    throw new Exception("Invalid active directory path.", ex);
            //}
            //catch (DirectoryServicesCOMException ex)
            //{
            //    if (ex.ExtendedError == 8333)
            //    {
            //        throw new Exception("Invalid active directory path.", ex);
            //    }
            //    else
            //    {
            //        throw new Exception("Invalid user authentication. Please input a valid user name & pasword and try again.", ex);
            //    }
            //}
            //catch (System.Runtime.InteropServices.COMException ex)
            //{
            //    throw new Exception("Active directory server not found.", ex);
            //}
            //catch (ArgumentException ex)
            //{
            //    if (ex.Source == "System.DirectoryServices")
            //    {
            //        throw new Exception("Invalid search filter expression.", ex);
            //    }
            //    else
            //    {
            //        throw new Exception("Unhandeled exception occured while authenticating user using active directory.", ex);
            //    }
            //}
            catch (Exception ex)
            {
                return false;
                throw new Exception("Unhandeled exception occured while authenticating user using active directory.", ex);
            }
            return true;
        }
        public bool IsAuthenticated1(string domainName, string userName, string password)
        {
            string domainAndUsername = domainName + @"\" + userName;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, password);
            try
            {
                // Bind to the native AdsObject to force authentication.
                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + userName + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (null == result)
                {
                    return false;
                }
                // Update the new path to the user in the directory
                _path = result.Path;
                _filterAttribute = (String)result.Properties["cn"][0];
            }
            catch (Exception ex)
            {
                //throw new Exception("Login Error: " + ex.Message);
                //throw new Exception("Can't Login with Domain");
                return false;


            }
            return true;
        }


        /// <summary>
        /// Another way of validating a user is to perform a bind. In this case, the server
        /// queries its own database to validate the credentials. The server defines
        /// how a user is mapped to its directory.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns>true if the credentials are valid, false otherwise</returns>
        public bool validateUserByBind(string username, string password)
        {
            bool result = true;
            //var credentials = new NetworkCredential(username, password);
            //var serverId = new LdapDirectoryIdentifier(connection.SessionOptions.HostName);

            //var conn = new LdapConnection(serverId, credentials);
            //try
            //{
            //    conn.Bind();
            //}
            //catch (Exception)
            //{
            //    result = false;
            //}

            //conn.Dispose();

            return result;
        }
    }
}