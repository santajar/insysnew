﻿
using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using InSysClass;
using Ini;
using System.Web;
using System.Web.Configuration;

namespace WebClass
{
    public class ConnectionClass
    {
        protected static string sDataSource;
        protected static string sDatabase;
        protected static string sUserID;
        protected static string sPassword;
        protected static string sDataSourceAuditTrail;
        protected static string sDatabaseAuditTrail;
        protected static string sUserIDAuditTrail;
        protected static string sPasswordAuditTrail;

        protected static string sActiveDir;
        protected static string sFileName;
        protected static string sFileNameConfig;
        protected static string sEncryptFileName;

        public SqlConnection MyConnection = null;
        public SqlConnection oSqlConnAuditTrail = null;


        #region Data Source Properties
        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            //return InSysWeb.ConnectionClass.sConnString(sDataSource, sDatabase, sUserID, sPassword);
            return sConnString(sDataSource, sDatabase, sUserID, sPassword);
        }

        public string sGetDataSourceAuditTrail()
        {
            return sDataSourceAuditTrail;
        }

        public string sGetDatabaseAuditTrail()
        {
            return sDatabaseAuditTrail;
        }

        public string sGetUserIDAuditTrail()
        {
            return sUserIDAuditTrail;
        }

        public string sGetPasswordAuditTrail()
        {
            return sPasswordAuditTrail;
        }

        public string sGetConnStringAuditTrail()
        {
            return sConnString(sDataSourceAuditTrail, sDatabaseAuditTrail, sUserIDAuditTrail, sPasswordAuditTrail);
        }
        #endregion

        public static bool isHaveConfiguration()
        {
            sActiveDir = HttpContext.Current.Request.PhysicalApplicationPath;
            sFileName = sActiveDir + "Application.conf";

            if (File.Exists(sFileName)) return true;
            else return false;
        }
        
        public static string sConnString(string sHost, string sDatabaseName, string sUserID, string sPassword)
        {
            //return "server=" + sHost + ";uid=" + sUserID + ";pwd=" + sPassword + ";database=" + sDatabaseName + "; Connection Lifetime=0;";
            return "Data Source=" + sHost + ";Initial Catalog=" + sDatabaseName + ";Integrated Security=True;User ID=" + sUserID + ";Password=" + sPassword + ";";

        }

        public static void doGetInitData()
        {
            if (isHaveConfiguration())
            {

                sFileNameConfig = sActiveDir + "Application.config";
                sEncryptFileName = sFileName;

                if (File.Exists(sEncryptFileName))
                {
                    EncryptionLib.DecryptFile(sEncryptFileName, sFileNameConfig);
                    try
                    {
                        IniFile objIniFile = new IniFile(sFileNameConfig);
                        sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                        sDatabase = objIniFile.IniReadValue("Database", "Database");
                        sUserID = objIniFile.IniReadValue("Database", "UserID");
                        sPassword = objIniFile.IniReadValue("Database", "Password");
                        sDataSourceAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "DataSource");
                        sDatabaseAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "Database");
                        sUserIDAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "UserID");
                        sPasswordAuditTrail = objIniFile.IniReadValue("DatabaseAuditTrail", "Password");
                    }
                    catch (Exception ex)
                    {
                        //CommonClass.doWriteErrorFile(ex.Message);
                    };
                    File.Delete(sFileNameConfig);
                }
            }

        }

        public void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword, string sDataSourceAuditTrail, string sDatabaseAuditTrail, string sUserIDAuditTrail, string sPasswordAuditTrail)
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            try
            {
                IniFile objIniFile = new IniFile(sFileName);
                objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
                objIniFile.IniWriteValue("Database", "Database", sDatabase);
                objIniFile.IniWriteValue("Database", "UserID", sUserID);
                objIniFile.IniWriteValue("Database", "Password", sPassword);
                objIniFile.IniWriteValue("DatabaseAuditTrail", "DataSource", sDataSourceAuditTrail);
                objIniFile.IniWriteValue("DatabaseAuditTrail", "Database", sDatabaseAuditTrail);
                objIniFile.IniWriteValue("DatabaseAuditTrail", "UserID", sUserIDAuditTrail);
                objIniFile.IniWriteValue("DatabaseAuditTrail", "Password", sPasswordAuditTrail);
            }
            catch (Exception ex)
            {
                //CommonClass.doWriteErrorFile(ex.Message);
            };
            EncryptionLib.EncryptFile(sEncryptFileName, sFileName);
            File.Delete(sFileName);
        }

        static string sCreateConnString()
        {
            string sTemp = null;

            while (string.IsNullOrEmpty(sTemp))
            {
                
                doGetInitData();

                string sConnString = string.Format("server={0};database={1};uid={2};pwd={3};", sDataSource, sDatabase, sUserID, sPassword);
                SqlConnection osqlconnection = new SqlConnection(sConnString);
                try
                {
                    osqlconnection.Open();
                    sTemp = sConnString;

                    sTemp = EncryptionLib.Encrypt3DES(sConnString);

                    var configuration = WebConfigurationManager.OpenWebConfiguration("~");
                    var section = (ConnectionStringsSection)configuration.GetSection("connectionStrings");
                    section.ConnectionStrings["DefaultConnection"].ConnectionString = sTemp;
                    configuration.Save();

                    ConfigurationManager.RefreshSection("connectionStrings");
                    configuration.Save();

                }
                catch (SqlException sqlex)
                {
                    Console.WriteLine("ERROR : {0}", sqlex.Message);
                    Console.Read();
                    Console.Clear();
                }

            }
            return sTemp;
        }

        public void Connection()
        {
            string MyConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            if (string.IsNullOrEmpty(MyConnectionString))
            {
                MyConnectionString = sCreateConnString();
            }
            MyConnectionString = EncryptionLib.Decrypt3DES(MyConnectionString);
            MyConnection = new SqlConnection(MyConnectionString);
            if (MyConnection.State == ConnectionState.Open)
            {
                MyConnection.Close();
                MyConnection.Dispose();
            }

                MyConnection.Open();

        }



        public void Disconnect()
        {
            MyConnection.Dispose();
            MyConnection.Close();
            oSqlConnAuditTrail.Dispose();
            oSqlConnAuditTrail.Close();
        }

    }
}