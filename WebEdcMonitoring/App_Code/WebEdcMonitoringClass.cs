﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Web;
using System.Net;
using WELog;
using System.Diagnostics;

namespace WebClass
{

    public class CommonMessage
    {
        public static string sUnregisteredExpiry = "Unregistered or Expired Application.";
        public static string sConfirmationTitle = "Confirmation";
        public static string sConfirmationText = "Do you want to save the changes?";
        public static string sConfirmationDelete = "Are You Sure Want to Delete?";
        public static string sConfirmationRestoreText = "Do you want to Restore?";

        #region "Menu"
        //Login
        public static string sErrLoginUserID = "User ID still empty. Please fill User ID.";
        public static string sErrLoginUserIDFormat = "User ID must only contains alphanumeric.";
        public static string sErrLoginPassword = "Password still empty. Please fill Password.";
        public static string sErrLoginUserIDPasswordNotFound = "User ID or Password not match. Please contact Administator.";
        public static string sErrLockedUserID = "User ID has been locked. Please contact Administator.";

        //Exit
        public static string sExitMessageText = "Are You Sure want to Exit?";
        public static string sExitMessageTitle = "Exit";

        //Logoff
        public static string sLogoutText = "Are You Sure want to Logout?";
        public static string sLogoutTitle = "Logout";

        //AllowInit
        public static string sInitDataText = "Use permission to Initialize Data?" + "\n" + "Status : ";
        public static string sInitDataTitle = "Initialize Data";

        //AutoInit
        public static string sErrorEmpty = "is still empty. Please fill";
        public static string sErrorFormat = "can only receive Numeric format. Please fill other value.";

        //User Management
        public static string sErrUserManUserName = "User Name still empty. Please fill User Name.";
        public static string sErrUserManUserNameFormat = "User Name must only contains alphanumeric.";
        public static string sErrUserManPassFormat = "Password must only contains alphanumeric.";
        public static string sErrUserManUidDouble = "User ID has already been used. Please change the User ID.";

        public static string sUserManDeleteTitle = "Delete User";
        public static string sUserMandDeleteUserText = "Are you sure want to delete user : '";

        //Change Password
        public static string sErrChangePassOldPassEmpty = "Old Password still Emtpy. Please Fill Your Old Password.";
        public static string sErrChangePassNewPassEmpty = "New Password still Emtpy. Please Fill Your New Password.";
        public static string sErrChangePassConfirmPassEmtyp = "Confirm Password still Emtpy. Please Fill Your Confirm Password.";
        public static string sErrChangePassMatchOldPass = "Old Password not match. Please Check Your Old Password";
        public static string sErrChangePassMatchNewPass = "New Password not match. Please Check Your New Password";
        public static string sChangePassConfirmText = "Are you sure want to change password?";
        public static string sChangePassSuccess = "Change Password Success.";

        //Primary DB
        public static string sErrPrimeDbDatabaseSource = "Server still empty. Please fill Server.";
        public static string sErrPrimeDbUserIDDB = "User ID still empty. Please fill User ID.";
        public static string sErrPrimeDbPasswordDB = "Password still empty. Please fill Password.";
        public static string sPrimeDbConnSuccess = "Connection to Database Success.";
        public static string sErrPrimeDbConnection = "Connection Failed. Please Contact Administrator.";

        //Audit Trail DB
        public static string sErrAuditTrailDBDatabaseSource = "Server Audit Trail still empty. Please fill Server Audit Trail.";
        public static string sErrAuditTrailDBUserIDDB = "User ID Audit Trail still empty. Please fill User ID Audit Trail.";
        public static string sErrAuditTrailDBPasswordDB = "Password Audit Trail still empty. Please fill Password Audit Trail.";
        public static string sAuditTrailDBConnSucces = "Connection to Audit Trail Database Success.";
        public static string sErrAuditTrailDBConnection = "Connection Failed. Please Contact Administrator.";

        //Database Template
        public static string sDatabaseTemplateTitle = "Choose Database as Template";
        public static string sDatabaseTemplateText = "Please select Database:";

        //"Upload"
        public static string sErrUploadDefinitionTitle = "Upload Definition";
        public static string sErrUploadTitle = "Upload.";
        public static string sErrUploadText = "Error Template Definition file or file is not exist.\nCreate Template Definition file?";

        public static string sErrUploadDataTitle = "Upload";
        public static string sErrUploadDataSubTitle = "Upload.";
        public static string sErrUploadDataText = "Error Map Definition file or file is not exist.\nCreate Map Definition file?";


        //"Export Profile to Text"
        public static string sErrExportProfileToText = "Database is Not selected or \"Profile/Master Profile\" is has not been selected";

        //"Testing"
        public static string sTerminal = "Terminal : ";
        public static string sInitPwd = "\nInit Pwd : ";
        public static string sF2Pwd = "\nF.2 Pwd : ";
        public static string sF99Pwd = "\nF.99 Pwd : ";

        //"Register"
        public static string sRegisterTitle = "Register Old File";
        public static string sRegisterText = "Please fill the Terminal ID";
        #endregion

        #region "Profile"
        //"Profile"
        public static string sCardDelConfirmText = "Are you sure want to delete card?";
        public static string sDelConfirmText = "Are you sure want to delete ";
        public static string sCopyNewCardName = "Are you sure want to copy ";//add code (ibnu) 20-10-2014
        public static string sCardNameText = "Please fill the new name Card Name";//add code (ibnu) 20-10-2014
        public static string sCopyNewCardNameOnTerminal = "Are you sure want to copy ";//add code (ibnu) 20-10-2014
        public static string sCardNameTextOnTerminal = "Please fill the new name Card Name";//add code (ibnu) 20-10-2014
        public static string sTleNameText = "Please fill the new  Name";//add code (ibnu) 20-10-2014
        public static string sAIDNameText = "Please fill the new name Tle Name";//add code (ibnu) 27-10-2014
        public static string sTleIdText = "Please fill the new TLEID";
        public static string sModifUserProfile = "Renaming of a user profile";
        public static string sDeleteUserProfile = "Deletion of a user profile";
        public static string sCreateUserProfile = "Creation of a user profile";
        public static string sModifUserProfileRight = "Modification of a user profile access right";
        public static string sEnabledisableuserDomain = "Enable/Disable user";
        public static string sEnabledisableuserDomainLock = "Lock user";
        //"Copy Terminal"
        public static string sCopyTerminalTitle = "New Terminal ID";
        public static string sCopyTerminalText = "Please fill the new Terminal ID";

        //"ADD SN or CHANGE SN"
        public static string sAddSNTitle = "Add Serial Number";
        public static string sChangeSNTitle = "Change Serial Number";
        public static string sAddSNText = "Please fill the new Serial Number";
        public static string sChangeSNText = "Please fill the change Serial Number";

        //"SQL Error Message"
        public static string sSQLErrorPKConstraint = "Violation of PRIMARY KEY constraint";
        public static string sSQLErrorDuplicateKey = "Cannot insert duplicate key in object";

        //message copy AID
        public static string sCopyAID = "Are you sure want to copy ";//add code (ibnu) 22-10-2014
        public static string sCopyCardPromtText = "Please fill the new AID Name ";//add code (ibnu) 22-10-2014

        #endregion

        #region "Log"
        public static string sRegisterSuccess = "Register Success";
        public static string sAccessLog = "Access Log";
        public static string sDeleteLog = "Delete Log";
        public static string sExportDataToExcel = "Export Data to Excel File";
        public static string sImportDataFromExcel = "Import Data from Excel File";
        public static string sUpdateDBPrimaryConn = "Update Primary Database Connection";
        public static string sChangePass = "User Change Password";
        public static string sLoginSucccess = "Login Success";
        public static string sLogoutSuccess = "User Logout";
        public static string sLogOutSuccess_ConnStringChanged = sLogoutSuccess + " caused by changing Connection String.";
        public static string sAddUserMngmt = "Add User Management";
        public static string sDeleteUserMngmt = "Delete User Management";
        public static string sEditUserMngmt = "Edit User Management";
        public static string sUpdateUserMngmt = "Update User Management";
        public static string sTrackEDC = "View EDC Tracker"; //"Browse"
        public static string sDownload = "Download File"; //"Download"
        public static string sViewUnited = "View United"; //"Monitor"
        public static string sViewUnitedDetail = "View United Detail";
        public static string sUpdDBConn = "Update Database Connection";
        public static string sDebugAndTesting = "Debug and Testing";
        public static string sFrmDebugOld = "Debug";
        public static string sLogInsys = "Log Insys";
        public static string sSetModem = "Setting Modem";
        public static string sAccessInitTrail = "Access Init Trail";
        public static string sInitTrailImportDetail = "Init Trail Import Detail";
        public static string sInitTrailImportSummary = "Init Trail Import Summary";
        public static string sInitTrailExportDetail = "Init Trail Export Detail";
        public static string sInitTrailExportSummary = "Init Trail Export Summary";
        public static string sInitTrailDeleteDetail = "Init Trail Delete Detail";
        public static string sInitTrailDeleteSummary = "Init Trail Delete Summary";

        public static string sAccessInitSoftwareTrail = "Access Init Software Trail";
        public static string sInitSoftwareTrailImportDetail = "Init Software Trail Import Detail";
        public static string sInitSoftwareTrailExportDetail = "Init Software Trail Export Detail";
        public static string sInitSoftwareTrailDeleteDetail = "Init Software Trail Delete Detail";
        public static string sExportProfileToText = "Export profile to Textfile";
        public static string sFormOpened = "View Form ";
        #endregion

        public static string sInputPassword = "Input Password";
        public static string sFrmPromptString = "Prompt String";
        public static string sViewSN = "View Terminal Serial Number";

        public static string sAutoInitPathEmpty = "Auto Init Report Path is still empty. Please fill Auto Init Report Path.";
        public static string sInitCompressEmpty = "Init Compress Path is still empty. Please fill Init Compress Path";

        public static string sRequestOrReceiptPaperExportDetail = "Request Or Receipt Paper Log Export";
        public static string sRequestOrReceiptPaperExportDelete = "Request Or Receipt Paper Log Delete";

        #region "AID"
        public static string sErrAIDInvalid = "Invalid AID or AID List allready exist";
        public static string sErrAIDEmptyField = "AID Field cannot empty";
        #endregion

        #region "CAPK"
        public static string sErrCAPKInvalid = "Invalid CAPK or CAPK Index allready exist";
        public static string sErrCAPKEmptyField = "CAPK Field cannot empty.";
        #endregion

        #region "Version"
        public static string sDbIDNotValid = "Database ID is not valid. Please input other value.";
        public static string sDbIdNewEmpty = "New Database ID is still empty. Please fill new Database ID.";
        public static string sDbIdExistingEmpty = "Existing Database ID is still empty. Please fill existing Database ID";
        public static string sDbSourceEqDbDest = "Database source is similar with Database Destination. Please choose other Database Destination.";
        public static string sDbNameEmpty = "Database Name is still empty. Please fill Database Name.";
        public static string sNotChecked = " is not selected.";
        public static string sIsEmpty = "is empty. Please fill";
        public static string sConfirmDeleteDbVersion = "Are you sure want to delete this version ?";
        public static string sConfirmDeleteDbVersionProfile = "Deleting this version will also delete all profiles in this Version.\nAre you sure want to delete this Version ?";
        public static string sConfirmDeleteDbVersionCardList = "Deleting this version will also delete all Card List in this Version.\nAre you sure want to delete this Version ?";
        #endregion

        #region "Application"
        public static string sFieldEmpty = " is still empty. Please fill ";
        public static string sAllowCompressEmpty = "Please select Allow Compress value.";
        public static string sFieldNotNumeric = " has invalid format (can only receive numbers).";
        #endregion

        //"Software Package"
        public static string sSoftwarePackageTitle = "Choose Software Package Name";
        public static string sSoftwarePackageText = "Please select Package name :";

    }

    public class WebEdcMonitoringClass
    {

        public static void InsertAuditTrailLogin(SqlConnection oSqlConn, string UserDomain, string Description)
        {
            string sQuery = string.Format("INSERT INTO tbAuditTrailLogin (UserDomain, Notes)");
            sQuery += string.Format(" VALUES  {0},{1} ", "('" + UserDomain + "'", "'" + Description + "')");
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
            }
            //oSqlConn.Dispose();
        }


        public static void InputLog(SqlConnection oSqlConn, string sTID, string sUsrID, string sDBName, string sActDesc, string sActDetl)
        {
            try
            {
                SqlParameter[] oSqlParam = new SqlParameter[5];
                oSqlParam[0] = new SqlParameter("@sTerminalID", System.Data.SqlDbType.VarChar, 8);
                oSqlParam[0].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[0].Value = sTID;
                oSqlParam[1] = new SqlParameter("@sUserId", System.Data.SqlDbType.VarChar, 10);
                oSqlParam[1].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[1].Value = sUsrID;
                oSqlParam[2] = new SqlParameter("@sDatabaseName", System.Data.SqlDbType.VarChar, 50);
                oSqlParam[2].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[2].Value = sDBName;
                oSqlParam[3] = new SqlParameter("@sActionDesc", System.Data.SqlDbType.VarChar, sActDesc.Length);
                oSqlParam[3].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[3].Value = sActDesc;
                oSqlParam[4] = new SqlParameter("@sActionDetail", System.Data.SqlDbType.VarChar, sActDetl.Length);
                oSqlParam[4].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[4].Value = (string.IsNullOrEmpty(sActDetl)) ? null : sActDetl;
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAuditTrailInsert, oSqlConn);
                if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
                oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                oSqlCmd.Parameters.AddRange(oSqlParam);
                oSqlCmd.ExecuteNonQuery();

                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                //CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        public static void InsertAuditTrail(SqlConnection oSqlConn, string sDate ,string UserDomain, string Description)
        {
            string sQuery = string.Format("INSERT INTO tbAuditTrail (AccessTime,UserId, ActionDescription)");
            sQuery += string.Format(" VALUES  {0},{1},{2} ", "('" + sDate + "'", "'" + UserDomain + "'", "'" + Description + "')");
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
            }
        }
        public static void CloseUserSession(SqlConnection oSqlConn, string sUserID)
        {
            string sQuery = string.Format("UPDATE tbAuditTrailSession SET Session=1 Where UserID = {0}","'"+ sUserID+ "'");
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
            }
        }
        public static void UpdateSuccessLogin(SqlConnection oSqlConn, string sUserID,string sDate)
        {
            string sQuery = string.Format("UPDATE tbUserLoginDomain SET LastLogin={0} Where UserDomain = {1}", "'" + sDate + "'", "'" + sUserID + "'");
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
            }
        }
        public static void sendmail2(string sFrom, string SendTo, string MailSubject, string Body)
        {
            try
            {
                MailMessage mailObj = new MailMessage(sFrom, SendTo, MailSubject, Body);

                SmtpClient SMTPServer = new SmtpClient("10.25.135.181");
                try
                {
                    SMTPServer.Send(mailObj);
                }
                catch (Exception ex)
                {

                }
            }
            catch (Exception ex)
            {

            }
        }

        public static bool CheckTimeAutomaticLogOut(SqlConnection oSqlConn, string sUsrID,HttpRequest Request, HttpResponse Response)
        {
            bool bReturn = true;
            try
            {
                SqlCommand oSqlCmd;
                oSqlCmd = new SqlCommand("spTimeAutomaticLogOut", oSqlConn);

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sUserDomain", SqlDbType.VarChar).Value = sUsrID;
                oSqlCmd.Parameters.Add("@iStatus", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
                bReturn = oSqlCmd.Parameters["@iStatus"].Value.ToString() == "1" ? true : false;
                oSqlCmd.Dispose();
                if (bReturn == true)
                {
                    InsertAuditTrailLogin(oSqlConn,sUsrID, "User Automatic Log Out");
                    WebEdcMonitoringClass.DeleteSingleUserActive(oSqlConn, sUsrID);
                    string ReqUrl = Request["ReturnUrl"];
                    if (ReqUrl != null)
                    { Response.Redirect(ReqUrl); }
                    Response.Redirect("~/Account/Login.aspx", false);
                 
                }

            }
            catch (Exception ex)
            {
                //HttpContext.Current.Session["errorMessage"] = ex.Message.ToString();
                //HttpContext.Current.Session["errorDetails"] = ex.StackTrace.ToString();
                //string Causes = "";
                //if (ex.InnerException != null) { Causes = ex.Source.ToString() + "<br />" + ex.InnerException.ToString(); }
                //else { Causes = ex.Source.ToString(); }
                //HttpContext.Current.Session["errorCauses"] = Causes.ToString();
                //HttpContext.Current.Response.Redirect("~/Errors.aspx");
            }

            return bReturn;
        }


        public static bool CheckTimeAutomaticLogOutLocal(SqlConnection oSqlConn, string sUsrID, HttpRequest Request, HttpResponse Response)
        {
            bool bReturn = true;
            try
            {
                
                using (SqlCommand oSqlCmd = new SqlCommand("spTimeAutomaticLogOutLocal", oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = sUsrID;
                    oSqlCmd.Parameters.Add("@iStatus", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    oSqlCmd.ExecuteNonQuery();
                    bReturn = oSqlCmd.Parameters["@iStatus"].Value.ToString() == "1" ? true : false;
                    oSqlCmd.Dispose();
                }
                if (bReturn == true)
                {
                    InsertAuditTrailLogin(oSqlConn, sUsrID, "User Automatic Log Out");


                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session["errorMessage"] = ex.Message.ToString();
                HttpContext.Current.Session["errorDetails"] = ex.StackTrace.ToString();
                string Causes = "";
                if (ex.InnerException != null) { Causes = ex.Source.ToString() + "<br />" + ex.InnerException.ToString(); }
                else { Causes = ex.Source.ToString(); }
                HttpContext.Current.Session["errorCauses"] = Causes.ToString();
                HttpContext.Current.Response.Redirect("~/Errors.aspx");
            }

            return bReturn;
        }

        public static bool IsSingleUserActive(SqlConnection oSqlConn, string sUsrID)
        {
            bool bReturn = true;
            try
            {
                SqlCommand oSqlCmd;
                oSqlCmd = new SqlCommand("spUserSingleActive", oSqlConn);

                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sUserID", SqlDbType.VarChar).Value = sUsrID;
                oSqlCmd.Parameters.Add("@iStatus", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
                bReturn = oSqlCmd.Parameters["@iStatus"].Value.ToString() == "1" ? true : false;
                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session["errorMessage"] = ex.Message.ToString();
                HttpContext.Current.Session["errorDetails"] = ex.StackTrace.ToString();
                string Causes = "";
                if (ex.InnerException != null) { Causes = ex.Source.ToString() + "<br />" + ex.InnerException.ToString(); }
                else { Causes = ex.Source.ToString(); }
                HttpContext.Current.Session["errorCauses"] = Causes.ToString();
                HttpContext.Current.Response.Redirect("~/Errors.aspx");
            }

            return bReturn;
        }

        public static void InsertSingleUserActive(SqlConnection oSqlConn, string UserID, DateTime sDate)
        {
            string sQuery = string.Format("INSERT INTO tbUserSession (UserID,AccessTime)");
            sQuery += string.Format(" VALUES  {0},{1} ", "('" + UserID + "'", "'" + string.Format("{0:MM/dd/yyyy H:mm:ss}", sDate) + "')");
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
            }
        }
        public static void DeleteSingleUserActive(SqlConnection oSqlConn, string UserID)
        {
            string sQuery = string.Format("DELETE FROM dbo.tbUserSession where UserID = {0}", "'" + UserID + "'");
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
            }
        }
        public static bool isExpire(SqlConnection oSqlConn,string sUserID, string sLoginTime,string sCurrentTime)
        {
            bool iLen = true;

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.isUserExpireSession('{0}','{1}','{2}')", sUserID, sLoginTime, sCurrentTime), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    iLen = bool.Parse(oRead[0].ToString());
                oRead.Close();
            }
            oCmd.Dispose();

            return iLen;
        }



        public static int iGetTotalTimeAutomaticLogOut(SqlConnection oSqlConn)
        {
            int iLen = 0;

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.iGetTotalTimeAutomaticLogOut()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    iLen = int.Parse(oRead[0].ToString());
                oRead.Close();
            }
            oCmd.Dispose();

            return iLen;
        }

        public static int iGetSenderPort(SqlConnection oSqlConn)
        {
            int iLen = 0;

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.iGetSenderPort()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    iLen = int.Parse(oRead[0].ToString());
                oRead.Close();
            }
            oCmd.Dispose();

            return iLen;
        }



        public static int iGetTotalMaxTimeAutomaticLogOut(SqlConnection oSqlConn)
        {
            int iLen = 0;

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.iGetTotalMaxTimeAutomaticLogOut()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    iLen = int.Parse(oRead[0].ToString());
                oRead.Close();
            }
            oCmd.Dispose();

            return iLen;
        }


        public static string sGetldapServer(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetldapServer()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string sGetSenderEmail(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetSenderEmail()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string sGetCorporateName(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetCorporateName()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string sGetURLWeb(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetURLWeb()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string sGetSenderPassword(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetSenderPassword()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string sGetSenderHost(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetSenderHost()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string GetCompCode()  // Get Computer Name
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            return strHostName;
        }
        public static string GetIpAddress()  // Get IP Address
        {
            string ip = "";
            IPHostEntry ipEntry = Dns.GetHostEntry(GetCompCode());
            IPAddress[] addr = ipEntry.AddressList;
            ip = addr[2].ToString();
            return ip;
        }
        public static string sGetsInfosecEventLogs()
        {
            
            string sName = "";
            string sLoggedUserName = "Event : Current Logged: " + Environment.UserName;
            string sHostName = Dns.GetHostName();
            string sIPAddress4 = ""; string sIPAddress6 = "";
            IPAddress[] ipaddress = Dns.GetHostAddresses(sHostName);
            foreach (IPAddress ip4 in ipaddress.Where(ip => ip.AddressFamily==System.Net.Sockets.AddressFamily.InterNetwork))
            { sIPAddress4= "ip4 "+ip4.ToString(); }
            foreach (IPAddress ip6 in ipaddress.Where(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6))
            { sIPAddress6 = "ip6 " + ip6.ToString(); }
            string sLocalMachineName = " Local Machine :" + Environment.MachineName + " "+sIPAddress4 +" "+ sIPAddress6;

            WELog.ClsEventLog ObjClsEventLog;
            ObjClsEventLog = new ClsEventLog();
            ObjClsEventLog.WriteToEventLog("Login EDC Monitoring", "Login Success", sLoggedUserName + sLocalMachineName);

            //if (!EventLog.SourceExists("WebEdcMonitoring1"))
            //{ EventLog.CreateEventSource("WebEdcMonitoring1", "WebEdcMonitoring2"); }
            //// Create an EventLog instance and assign its source.
            //EventLog eventLog = new EventLog();
            //// Setting the source
            //eventLog.Source = "WebEdcMonitoring2";
            //// Write an entry to the event log.
            //eventLog.WriteEntry(sLoggedUserName + sLocalMachineName, EventLogEntryType.Information, 1002);

            string sSource;
            string sLog;
            string sEvent;

            sSource = "Web Edc Monitoring Ingenico";
            sLog = "Application";
            sEvent = sLoggedUserName + sLocalMachineName;

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, sEvent);
            EventLog.WriteEntry(sSource, sEvent,
            EventLogEntryType.SuccessAudit, 234);
        

            return                   sName = sLoggedUserName + sLocalMachineName;
        }
        public static int isGetUserDomainActived(SqlConnection oSqlConn, string UserDomain)
        {
            int sFlag = 0;

            SqlCommand oCmdr = new SqlCommand(string.Format("SELECT dbo.isGetUserDomainActived('{0}')", UserDomain), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();
            using (SqlDataReader oReadr = oCmdr.ExecuteReader())
            {
                if (oReadr.Read())
                    sFlag = int.Parse(oReadr[0].ToString());
                oReadr.Close();
            }
            oCmdr.Dispose();

            return sFlag;
        }
        public static int isGetUserDomainapprove(SqlConnection oSqlConn, string UserDomain)
        {
            int sFlag = 0;

            SqlCommand oCmdr = new SqlCommand(string.Format("SELECT dbo.isGetUserDomainapprove('{0}')", UserDomain),oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();
            using (SqlDataReader oReadr = oCmdr.ExecuteReader())
            {
                if (oReadr.Read())
                    sFlag = int.Parse(oReadr[0].ToString());
                oReadr.Close();
            }
            oCmdr.Dispose();

            return sFlag;
        }

        public static string[] LoadRecipient(SqlConnection oSqlConn,String TypeTo)
        {
            List<string> Items = new List<string>(10);

            using (SqlCommand oSqlCmd = new SqlCommand("spUserEmailBrowse", oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = " WHERE UserIDAccess='" + TypeTo + "'";
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.HasRows)
                    {
                        while (oRead.Read())
                        {
                            Items.Add(oRead["EMAIL"].ToString() + ";");
                        }
                    }
                    oRead.Dispose();
                }
                oSqlCmd.Dispose();
            }
            
            return Items.ToArray();
        }
        public static string[] LoadRecipientAdmin(SqlConnection oSqlConn, String TypeTo)
        {
            List<string> Items = new List<string>(10);

            using (SqlCommand oSqlCmd = new SqlCommand("spUserEmailAdminBrowse", oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = " WHERE UserType='" + TypeTo + "'";
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                {
                    if (oRead.HasRows)
                    {
                        while (oRead.Read())
                        {
                            Items.Add(oRead["EMAIL"].ToString() + ";");
                        }
                    }
                    oRead.Dispose();
                }
                oSqlCmd.Dispose();
            }

            return Items.ToArray();
        }
        public static string isGetUserDomainModifUserRight(SqlConnection oSqlConn, string UserDomain)
        {
            string sFlag = "";

            SqlCommand oCmdr = new SqlCommand(string.Format("SELECT dbo.isGetUserDomainModifUserRight('{0}')", UserDomain), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();
            using (SqlDataReader oReadr = oCmdr.ExecuteReader())
            {
                if (oReadr.Read())
                    sFlag = oReadr[0].ToString();
                oReadr.Close();
            }
            oCmdr.Dispose();

            return sFlag;
        }
        public static bool sGetldapEnable(SqlConnection oSqlConn)
        {
            bool sFlag = true;

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetldapEnable()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sFlag = bool.Parse(oRead[0].ToString());
                oRead.Close();
            }
            oCmd.Dispose();

            return sFlag;
        }
        public static string sGetUserRight(SqlConnection oSqlConn,string sUserID)
        {
            string sName = "";

                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = " WHERE UserID='" + sUserID + "'";
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        using (DataTable dtUser = new DataTable())
                        {
                            dtUser.Load(oRead);
                            oSqlCmd.Dispose();
                            oRead.Close();
                            if (dtUser.Rows.Count == 1) // UserID Found
                            {
                                sName = dtUser.Rows[0]["UserRights"].ToString();
                            }
                        }
                        oSqlCmd.Dispose();
                    }
                }
            return sName;
        }
        public static string sGetldapDomain(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetldapDomain()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }
        public static string sGetldapDirectoryPath(SqlConnection oSqlConn)
        {
            string sName = "";

            SqlCommand oCmd = new SqlCommand(string.Format("SELECT dbo.sGetldapDirectoryPath()"), oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();

            using (SqlDataReader oRead = oCmd.ExecuteReader())
            {
                if (oRead.Read())
                    sName = oRead[0].ToString();
                oRead.Close();
            }
            oCmd.Dispose();

            return sName;
        }


    }
}