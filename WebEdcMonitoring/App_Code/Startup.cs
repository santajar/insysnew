﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewInSys.Startup))]
namespace NewInSys
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
