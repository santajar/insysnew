using System;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Web;
using System.Web.Security;
using System.Configuration;
using WebClass;

namespace ActiveDirectoryAuthentication.Helper
{
    public static class ActiveDirectoryConnector
    {

        private static ConnectionClass oSqlConn = new ConnectionClass();

        #region Member Variables

        private static ActiveDirectoryConfiguration _currentActiveDirectoryConfiguration = null;

        #endregion

        #region Properties

        private static ActiveDirectoryConfiguration activeDirectorySettings = null;
        public static ActiveDirectoryConfiguration ActiveDirectorySettings
        {
            get
            {
                try
                {
                    if (activeDirectorySettings == null)
                    {
                        //string ldapEnable = WebEdcMonitoringClass.sGetldapEnable(oSqlConn.MyConnection).ToString().ToString();
                        //string sactiveDirectorySettings = "< ldapConfiguration enabled = " + ldapEnable + " pageLevelSecurityCheck = false server = " + WebEdcMonitoringClass.sGetldapServer(oSqlConn.MyConnection) + " domain = " + WebEdcMonitoringClass.sGetldapDomain(oSqlConn.MyConnection) + " directoryPath = " + WebEdcMonitoringClass.sGetldapDirectoryPath(oSqlConn.MyConnection) + " groupName = elixtrauser filter = (and(objectCategory=person)(objectClass=user)(samaccountname=usertosearch)) filterReplace = usertosearch /> ";

                        activeDirectorySettings = (ActiveDirectoryConfiguration)ConfigurationManager.GetSection("DefaultActiveDirectoryServer");
                        //activeDirectorySettings = (ActiveDirectoryConfiguration)ConfigurationManager.GetSection(sactiveDirectorySettings);
                    }
                }
                catch
                {
                }
                return activeDirectorySettings;
            }
        }

        #endregion

        #region Methods

        public static bool IsActiveDirectoryEnabled
        {
            get
            {
                return ActiveDirectorySettings.Enabled;
            }
        }

        public static bool IsUserLoggedIn(string userName, string password)
        {
            try
            {
               //if (WebEdcMonitoringClass.sGetldapEnable(oSqlConn.MyConnection) )

                    if (IsActiveDirectoryEnabled)
                    {
                    int startIndex = userName.IndexOf("@");
                    if (startIndex >= 0)
                    {
                        userName = userName.Substring(0, startIndex);
                    }
                    
                    //DirectoryEntry ldapConnection = new DirectoryEntry("LDAP://" + ActiveDirectorySettings.Server + "/" + ActiveDirectorySettings.DirectoryPath, userName, password);
                    DirectoryEntry ldapConnection = new DirectoryEntry("LDAP://" + WebEdcMonitoringClass.sGetldapServer(oSqlConn.MyConnection) + "/" + WebEdcMonitoringClass.sGetldapDirectoryPath(oSqlConn.MyConnection), userName, password);
                    DirectorySearcher searcher = new DirectorySearcher(ldapConnection);
                    searcher.Filter = ActiveDirectorySettings.Filter.Replace("and", "&");
                    searcher.Filter = searcher.Filter.Replace(ActiveDirectorySettings.FilterReplace, userName);
                    searcher.PropertiesToLoad.Add("memberOf");
                    searcher.PropertiesToLoad.Add("userAccountControl");

                    SearchResult directoryUser = searcher.FindOne();
                    if (directoryUser != null)
                    {
                        int flags = Convert.ToInt32(directoryUser.Properties["userAccountControl"][0].ToString());
                        if (!Convert.ToBoolean(flags & 0x0002))
                        {
                            string desiredGroupName = ActiveDirectorySettings.GroupName.ToLower();
                            if (desiredGroupName!=string.Empty)
                            {
                                desiredGroupName = "cn=" + desiredGroupName + ",";
                                int numberOfGroups = directoryUser.Properties["memberOf"].Count;
                                bool isWithinGroup = false;
                                for (int i = 0; i < numberOfGroups; i++)
                                {
                                    string groupName = directoryUser.Properties["memberOf"][i].ToString().ToLower();
                                    if (groupName.Contains(desiredGroupName))
                                    {
                                        isWithinGroup = true;
                                        break;
                                    }
                                }
                                if (!isWithinGroup)
                                {
                                    throw new Exception("User [" + userName + "] is not a member of the desired group.");
                                }
                            }
                            return true;
                        }
                        else
                        {
                            throw new Exception("User [" + userName + "] is inactive.");
                        }
                    }
                    else
                    {
                        throw new Exception("User [" + userName + "] not found in the specified active directory path.");
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (LdapException ex)
            {
                if (ex.ErrorCode == 49)
                {
                    throw new Exception("Invalid user authentication. Please input a valid user name & pasword and try again.",ex);
                }
                else
                {
                    throw new Exception("Active directory server not found.", ex);
                }
            }
            catch (DirectoryOperationException ex)
            {
                throw new Exception("Invalid active directory path.", ex);
            }
            catch (DirectoryServicesCOMException ex)
            {
                if (ex.ExtendedError == 8333)
                {
                    throw new Exception("Invalid active directory path.", ex);
                }
                else
                {
                    throw new Exception("Invalid user authentication. Please input a valid user name & pasword and try again.", ex);
                }
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                throw new Exception("Active directory server not found.", ex);
            }
            catch (ArgumentException ex)
            {
                if (ex.Source == "System.DirectoryServices")
                {
                    throw new Exception("Invalid search filter expression.", ex);
                }
                else
                {
                    throw new Exception("Unhandeled exception occured while authenticating user using active directory.", ex);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unhandeled exception occured while authenticating user using active directory.", ex);
            }
        }

        public static void UserAuthenticationCheck()
        {
            try
            {
                if (ActiveDirectorySettings.Enabled)
                {
                    if ((ActiveDirectorySettings.PageLevelSecurityCheck) && !HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("Login.aspx"))
                    {
                        if (HttpContext.Current.User != null)
                        {
                            if (HttpContext.Current.User.Identity.IsAuthenticated)
                            {
                                if (HttpContext.Current.User.Identity is FormsIdentity)
                                {
                                    FormsIdentity formIdentity = (FormsIdentity)HttpContext.Current.User.Identity;
                                    FormsAuthenticationTicket userAuthTicket = formIdentity.Ticket;
                                    if (!IsUserLoggedIn(userAuthTicket.Name, userAuthTicket.UserData))
                                    {
                                        FormsAuthentication.SignOut();
                                        FormsAuthentication.RedirectToLoginPage();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        #endregion
    }
}
