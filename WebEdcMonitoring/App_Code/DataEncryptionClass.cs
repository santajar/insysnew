﻿using InSysClass;
using System.Security.Cryptography;
using System.Text;

namespace WebClass
{
    public class DataEncryptionClass
    {
        public static string sGetMD5Result(string _sToEncrypt)
        {
            byte[] bSource = ASCIIEncoding.ASCII.GetBytes(_sToEncrypt);

            MD5CryptoServiceProvider oMD5 = new MD5CryptoServiceProvider();

            byte[] bHash = oMD5.ComputeHash(bSource);
            return CommonLib.sByteArrayToString(bHash);
        }

        
        public static string sGetEncrypt(string sClearString)
        {
            return sGetMD5Result(sClearString);
        }
    }
}