﻿using System;
using System.Diagnostics;

namespace WELog
{
    public class ClsEventLog
    {
       
        public bool CreateLog(string strLogName)
        {
            bool Reasult = false;

            try
            {
                    System.Diagnostics.EventLog.CreateEventSource(strLogName, strLogName);
                    System.Diagnostics.EventLog SQLEventLog = new System.Diagnostics.EventLog();

                    SQLEventLog.Source = strLogName;
                    SQLEventLog.Log = strLogName;

                    SQLEventLog.Source = strLogName;
                    SQLEventLog.WriteEntry("The " + strLogName + " was successfully initialize component.", EventLogEntryType.Information);


                    Reasult = true;

            }
            catch
            {
                Reasult = false;
            }


            return Reasult;
        }
        public void WriteToEventLog(string strLogName
                                  , string strSource
                                  , string strErrDetail)
        {
            System.Diagnostics.EventLog SQLEventLog = new System.Diagnostics.EventLog();

            try
            {
                if (!System.Diagnostics.EventLog.SourceExists(strLogName)) this.CreateLog(strLogName); 
                    

                SQLEventLog.Source = strLogName;
                SQLEventLog.WriteEntry(Convert.ToString(strSource)
                                      + Convert.ToString(strErrDetail), EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                SQLEventLog.Source = strLogName;
                SQLEventLog.WriteEntry(Convert.ToString("INFORMATION: ")
                                      + Convert.ToString(ex.Message), EventLogEntryType.Information);


            }
            finally
            {
                SQLEventLog.Dispose();
                SQLEventLog = null;

            }

        }
    }
}
