﻿<%@ Page Title="ChangePassword" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
    </div>
    <%--    <asp:ScriptManager ID="ScriptManager1" runat="server"> </asp:ScriptManager>--%>
    <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-target="#filter"><b>Change Password</b></div>
        <div class="panel-body" id="filter">
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">Login Name</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtLoginName" Width="200" runat="server" CssClass="form-control input-sm" Enabled="False"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">Old Password</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtOldPassword" Width="200" runat="server" CssClass="form-control input-sm" TextMode="Password">Password</asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">New Password</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtNewPassword" Width="200" runat="server" CssClass="form-control input-sm" TextMode="Password">Password</asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">
                        <label class="control-label input-sm">Confirm New Password</label>
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtConfirmNewPass" Width="200" runat="server" CssClass="form-control input-sm" TextMode="Password">Password</asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">

                    <div class="panel panel-default">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-left padding-center">
                                    <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="btn-success btn-sm" OnClick="btnOK_Click" Width="80px" Font-Bold="True" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="btn-primary btn-sm" OnClick="btnCancel_Click" Width="80px" Font-Bold="True" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
