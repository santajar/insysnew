﻿<%@ Page Title="Register Approve" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="FrmRegistrationApprove.aspx.cs" Inherits="FrmRegistrationApprove" %>

<asp:Content ID="Content1" ContentPlaceHolderID="script_css" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            font-size: large;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="jumbotron"></div>
      <div class="panel panel-default">
        <div class="auto-style1" data-toggle="collapse" data-target="#filter"><strong>USER PROFILE</strong></div>
        <div class="panel-body" id="filter">
            <div class="row">
            <div class="col-md-12 padding-top">  
                    <div class="panel panel-default">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group pull-left padding-center">
                            <asp:Button ID="bEdit" runat="server" Text="Edit" CssClass="btn btn-success" OnClick="bEdit_Click" />      
                            <asp:Button ID="bSave" runat="server" Text="Save" CssClass="btn btn-success"  Visible="False" />                                       
                            <asp:Button ID="Cancel" runat="server" Text="Back" CssClass="btn btn-info" OnClick="Cancel_Click" />                                    
     
                        </div>
                    </div>
                    </div>
                </div>

            </div>
            </div>
            <div class="row">
                    <div class="col-md-12">     
                        <div class="table-responsive" overflow: scroll; >       
      
                                      
                            <asp:Repeater ID="repeaterData" runat="server" >
                                <HeaderTemplate>
                                    <div class="template">
                                    <table id="dataTable" class="display responsive no-wrap" width="100%" border="0">
                                        <thead>
                                            <tr>
                                                <th>User Domain</th>
                                                <th>User Name</th>
                                                <th>Email</th>
                                                <th>User ID Access</th>
                                                <th>Actived</th>
                                                <th>Approved</th>
                                                <th>ApprovedDate</th>
                                                <th>Locked</th>
                                                <th>CreateDate</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                 </HeaderTemplate>
                                 <ItemTemplate>
                                                
                                        <tr class="input-sm">
                                            <td>
                                                <asp:Label ID="lblUserDomain" CssClass="control-label input-sm" runat="server" Text='<%# Eval("UserDomain") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUserName" CssClass="control-label input-sm" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmail" CssClass="control-label input-sm" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lbl_UserIDAccess" CssClass="control-label input-sm" runat="server" Text='<%# Eval("UserIDAccess") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlUserIDAccess" TabIndex="1" CssClass="fullSelect" runat="server" > </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblActived" CssClass="control-label input-sm" runat="server" Text='<%# Eval("Actived") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlActived" TabIndex="2" CssClass="fullSelect" runat="server" BindingHint="Actived"  > </asp:DropDownList>

                                            </td>
                                            <td>
                                                <asp:Label ID="lblApproved" CssClass="control-label input-sm" runat="server" Text='<%# Eval("Approved") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlApproved" TabIndex="3" CssClass="fullSelect" runat="server" BindingHint="Approved" > </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblApprovedDate" CssClass="control-label input-sm" runat="server" Text='<%# Eval("ApprovedDate") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLocked" CssClass="control-label input-sm" runat="server" Text='<%# Eval("Locked") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlLocked" TabIndex="4" CssClass="fullSelect" runat="server" BindingHint="Locked" > </asp:DropDownList>
                                            </td>                                    
                                            <td>
                                                <asp:Label ID="Label2" CssClass="control-label input-sm" runat="server" Text='<%# Eval("CreateDate") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnSave" Text="Save" CommandName="Save" OnClick="SaveOrUpdate" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnDelete" Text="Delete" CommandName="Delete" OnClick="btnClickDelete" runat="server" />
                                            </td>
                                        </tr>
                                                
                                  </ItemTemplate>
                                    <FooterTemplate>
                                    </tbody>
                                    </table>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
            </div>   

        </div>        
    </div>
    <div>


    </div>
</asp:Content>

