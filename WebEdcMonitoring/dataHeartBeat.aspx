﻿<%@ Page Title="Heart Beat" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="dataHeartBeat.aspx.cs" Inherits="dataMonitor" EnableEventValidation="false" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="script_css" runat="server">
    <style type="text/css">
        .hideGridColumn {
            display: none;
        }
        .gvwCasesPager a {
                color: #fff;
                text-decoration:underline;
                margin-left:5px;
                margin-right:5px;
        }
  </style>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        
<%--    <div class="row">
        <asp:Label ID="lblTimer" Text="" runat="server" />
        <asp:Timer ID="timerRefresh" runat="server" Interval="30000" OnTick="timerRefresh_Tick">
        </asp:Timer>
    </div>--%>
        
        <h2>HeartBeat Monitoring</h2>
        <asp:Panel ID="panelFilter" GroupingText="Filter" runat="server" Font-Size="Small">
            <table style="width: 80%;">
                <tr>
                    <td>Serial Number</td>
                    <td>
                        <asp:DropDownList ID="dropdownlistSN" runat="server">
                            <asp:ListItem Value="1">Is</asp:ListItem>
                            <asp:ListItem Value="2">Contains</asp:ListItem>
                            <asp:ListItem Value="3">Begins with</asp:ListItem>
                            <asp:ListItem Value="4">Ends with</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 318px">
                        <asp:TextBox ID="txtboxSN" runat="server" Height="20px" Width="250px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td style="width: 318px; height: 29px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Status</td>
                    <td style="width: 318px; height: 29px">&nbsp;<asp:DropDownList ID="DropDownStatus" runat="server" Width="133px">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem>ACTIVE</asp:ListItem>
                        <asp:ListItem>INACTIVE</asp:ListItem>
                        <asp:ListItem>NOT LIST</asp:ListItem>
                    </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>CSI</td>
                    <td>
                        <asp:DropDownList ID="dropdownlistTID" runat="server">
                            <asp:ListItem Value="1">Is</asp:ListItem>
                            <asp:ListItem Value="2">Contains</asp:ListItem>
                            <asp:ListItem Value="3">Begins with</asp:ListItem>
                            <asp:ListItem Value="4">Ends with</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 318px">
                        <asp:TextBox ID="txtboxTID" runat="server" Height="20px" Width="150px" MaxLength="8"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Software Version</td>
                    <td>
                        <asp:DropDownList ID="dropdownlistSoftware" runat="server">
                            <asp:ListItem Value="1">Is</asp:ListItem>
                            <asp:ListItem Value="2">Contains</asp:ListItem>
                            <asp:ListItem Value="3">Begins with</asp:ListItem>
                            <asp:ListItem Value="4">Ends with</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 318px">
                        <asp:TextBox ID="txtboxSoftware" runat="server" Height="20px" Width="150px" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="height: 29px">View Last Active Date</td>
                    <td style="height: 29px">
                        <asp:TextBox ID="txtDateEnd" runat="server" Height="20px" Width="150px" />
                        <ajaxToolkit:CalendarExtender ID="dateEnd" runat="server" TargetControlID="txtDateEnd" Format="MM/dd/yyyy" />
                    </td>
                    <td style="width: 318px; height: 29px">&nbsp;</td>
                    <td style="height: 29px">&nbsp;</td>
                </tr>
                <tr>
                    
                    <td>
                        <div class="col-md-2">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn-primary btn-sm" Text="Filter" OnClick="btnSubmit_Click" Width="168px" Font-Bold="True" Font-Size="Small" />
                        </div>
                    </td>
                    <td>
                        <div class="col-md-2">
                            <asp:Button ID="btnExportExel" runat="server" CssClass="btn-primary btn-sm"  Text="Export To Excel" Width="168px" OnClick="btnExportExel_Click" Font-Bold="True" Font-Size="Small" />
                        </div>
                    </td>

                </tr>
            </table>


        </asp:Panel>
        <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Font-Size="Small">
            <asp:Label ID="lblText" runat="server" CssClass="list-inline" />

            <table align="right">
                <tr>
                    <td id="lblActiv">Count CSI Activ :
                        <asp:Label ID="lblActive" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>|| Count CSI InActiv :<asp:Label ID="lblInActive" runat="server" Text="Label"></asp:Label>
                    <td>|| Count CSI Not List :<asp:Label ID="lblNotList" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
            </table>

        </asp:Panel>

        <asp:Panel ID="Panel1" runat="server" CssClass="cpHeader" Font-Size="Small">
            <asp:Label ID="Label1" runat="server" ForeColor="#009933" />
        </asp:Panel>
        <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblepanelFilter" runat="server" TargetControlID="panelFilter" CollapseControlID="pHeader" ExpandControlID="pHeader"
            Collapsed="true" TextLabelID="lblText" CollapsedText="Show Filter" ExpandedText="Hide Filter"
            CollapsedSize="0"></ajaxToolkit:CollapsiblePanelExtender>
    </div>
    <div class="row">
        <div class="panel panel-default" visible="false">
            <div class="panel-heading" visible="false" data-toggle="collapse" data-target="#list"><strong>HeartBeat Monitoring</strong></div>
            <div class="panel-body" id="panel_grid">
                <div style="overflow: scroll">

                    <asp:GridView ID="gridviewMonitoring" runat="server" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="gridviewMonitoring_PageIndexChanging" AutoGenerateColumns="False" OnRowCommand="gridviewMonitoring_RowCommand" OnRowCreated="gridviewMonitoring_RowCreated" HorizontalAlign="Center">
                        <AlternatingRowStyle BackColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />
                        <PagerSettings Position="TopAndBottom" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Yellow" HorizontalAlign="Center" CssClass ="gvwCasesPager" Font-Bold="true"/> 
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />

                        <Columns>
                            <%--                            <asp:TemplateField HeaderText="No" ItemStyle-Width="50">
                                <ItemTemplate>
                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:BoundField DataField="No" HeaderText="No" />
                            <asp:TemplateField HeaderText="StId" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%#Eval ("ID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Last Activate Date" HeaderText="Last Activate Date" DataFormatString="{0:yyyy-MM-dd HH:mm:ss.fff}" />
                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                            <asp:BoundField DataField="CSI" HeaderText="CSI" />
                            <asp:BoundField DataField="SerialNumber" HeaderText="Serial Number" />
                            <asp:BoundField DataField="TerminalID" HeaderText="TerminalID" SortExpression="TerminalID" />
                            <asp:BoundField DataField="MerchantID" HeaderText="MerchantID" SortExpression="MerchantID" />
                            <asp:BoundField DataField="SoftwareVersion" HeaderText="Software Version" />
                            <asp:BoundField DataField="ICCID" HeaderText="ICCID" />
                            <asp:TemplateField ShowHeader="false">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDetailSIM" runat="server" CausesValidation="false" CommandName="gridviewMonitoring_RowCommand" Text="Location Area">
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <%--<asp:CommandField HeaderText="Delete" ShowDeleteButton="true" ShowHeader="true" />--%>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <%--    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="panelReport" CollapseControlID="pHeader" ExpandControlID="pHeader"
        Collapsed="true" TextLabelID="lblText" CollapsedText="Show Report" ExpandedText="Hide Report"
        CollapsedSize="0"></ajaxToolkit:CollapsiblePanelExtender>
    <asp:Panel ID="panelReport" GroupingText="Report" runat="server" Font-Size="Small">
        <div class="panel panel-default" visible="false">
            <div class="panel-heading" visible="false" data-toggle="collapse" data-target="#list"></div>
            <div class="panel-body" id="report">
                <div style="overflow: scroll">
                    <fieldset>
                        <CR:CrystalReportViewer ID="CReport" runat="server" AutoDataBind="true" DisplayToolbar="true" EnableDatabaseLogonPrompt="False"
                            DisplayGroupTree="False" HasCrystalLogo="False" HasExportButton="True" HasPrintButton="True"
                            HasGotoPageButton="False" HasViewList="False" Style="margin: auto;"
                            HasSearchButton="True" />
                    </fieldset>
                </div>
            </div>
        </div>
    </asp:Panel>--%>
</asp:Content>
