﻿<%@ Page Title="Form Registration" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="FrmRegistration.aspx.cs" Inherits="Account_FrmRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="script_css" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="jumbotron"></div>
    <div class="panel panel-default">
        <div class="panel-heading" data-toggle="collapse" data-target="#filter">Registration <asp:Label ID="lblNotes" runat="server" CssClass="form-control input-sm"></asp:Label></div></div>
        <div class="panel-body" id="filter">
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">User Domain</div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtUserDomain" runat="server" CssClass="form-control input-sm" Enabled="false" Width="200px" ></asp:TextBox></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top"></div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">User Name</div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control input-sm"></asp:TextBox></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top"></div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">
                    <div class="col-md-2">Email</div>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control input-sm"></asp:TextBox></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top"></div>
            </div>
            <div class="row">
                <div class="col-md-12 padding-top">

                    <div class="panel panel-default">
                        <div class="row">
                            <div class="col-md-12">
                                <%--<div class="btn-group pull-left padding-center">--%>
                                    <div class="col-md-2">
                                        <asp:Button ID="bRequest" runat="server" Text="Request" CssClass="btn btn-success" OnClick="bRequest_Click" Width="160px"  />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="Cancel" runat="server" Text="Back" CssClass="btn btn-info" OnClick="Cancel_Click" Width="160px"  />
                                    </div>
                               <%-- </div>--%>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</asp:Content>

