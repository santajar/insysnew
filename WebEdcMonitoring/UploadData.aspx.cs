﻿using InSysClass;
using NewInSys;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Data.Common;
using iTextSharp.text;
using WebClass;

public partial class UploadData : Page
{
    protected static string sUserID;
    protected string sConnString = null, fileType, sexcelconnectionstring, path;
    protected ConnectionClass sqlconn = new ConnectionClass();
    protected DataTable Exceldt;
    string constr, Query;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HttpCookie UserInfo = Request.Cookies["UserInfo"];

            if (UserInfo != null)
            {
                if (UserInfo.Values.Count != 0)
                {
                    if (Session.Count != 0)
                    {
                        sUserID = UserInfo.Value;
                    }
                    else
                    {
                        Response.Redirect("~/Account/Login.aspx");
                    }
                }
                else
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Account/Login.aspx");
            }
        }

    }
    protected void MessageBox(string Message)
    {
        string script = @"alert('" + Message + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", script, true);
    }   
    protected string GetUploadFile()
    {
        if (Session.Count != 0)
        {
            path = null;
            try
            {
                if (FUAttach.HasFile)
                {

                    fileType = Path.GetExtension(FUAttach.FileName);
                    if (fileType == ".xls" || fileType == ".xlsx" || fileType == ".csv")
                    {
                        string serverRoot = "";
                        serverRoot = Server.MapPath("~");
                        string sLogDir = serverRoot + "\\Document\\Upload";
                        if (!Directory.Exists(sLogDir)) Directory.CreateDirectory(sLogDir);
                        
                        string FileName = DateTime.Now.ToString("yyyyMMddHHmmss")+"Upload" + FUAttach.FileName;
                        path = string.Concat(Server.MapPath("~/Document/Upload/" + FileName));

                        FUAttach.PostedFile.SaveAs(path);
                    }
                    else MessageBox("Extention file has been : xls,csv,xlsx|| Your Extention file Error ");
                }
            }
            catch (Exception ex)
            {

            }
        }
        return path;
    }

    protected void SendInsert()
    {
        sqlconn.Connection();
        foreach (GridViewRow row in gv.Rows)
        {
            string CSI = row.Cells[0].Text;
            string TerminalID = row.Cells[1].Text;
            string MerchantID = row.Cells[2].Text;
            try
            {            
                int result = 0;
                using (SqlCommand scCommand = new SqlCommand("spEdcMonitorCSIInsertUpdate", sqlconn.MyConnection))
                {
                    scCommand.CommandType = CommandType.StoredProcedure;
                    scCommand.Parameters.Add("@CSI", SqlDbType.VarChar, 50).Value = CSI;
                    scCommand.Parameters.Add("@TerminalID", SqlDbType.VarChar, 50).Value = TerminalID;
                    scCommand.Parameters.Add("@MerchantID ", SqlDbType.VarChar, 50).Value = MerchantID;
                    scCommand.Parameters.Add("@Result ", SqlDbType.Int).Direction = ParameterDirection.Output;
                    try
                    {
                        if (scCommand.Connection.State == ConnectionState.Closed)
                        {
                            scCommand.Connection.Open();
                        }
                        scCommand.ExecuteNonQuery();
                       // result = Convert.ToInt32(scCommand.Parameters["@Result"].Value);
                    }
                    catch (Exception )
                    {
                        //MessageBox("Error : " + c.Message);
                    }
                    finally
                    {
                        scCommand.Connection.Close();
                        Response.Write(result);
                    }
                }
            }
            catch (Exception c)
            {
                MessageBox("Error : " + c.Message);
            }
        }

    }
    protected void importdatafromexcel(string excelfilepath)
    {
        string myexceldataquery = "select CSI,TerminalID,MerchantID from [sheet1$]";
        try
        {
            if ((fileType == ".xls") || (fileType == ".xlsx"))
            {
                if (fileType == ".xls") {
                    sexcelconnectionstring = string.Format(@"provider=microsoft.jet.oledb.4.0;data source=" + excelfilepath + ";extended properties=" + "\"excel 8.0;hdr=yes;\"");
                }
                if (fileType == ".xlsx") {
                    //sexcelconnectionstring = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR=YES;""", excelfilepath);
                    sexcelconnectionstring = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES\";", excelfilepath);                  
                }

                OleDbConnection oledbconn = new OleDbConnection(sexcelconnectionstring);
                OleDbCommand oledbcmd = new OleDbCommand(myexceldataquery, oledbconn);
                oledbconn.Open();
                OleDbDataAdapter dr = new OleDbDataAdapter(myexceldataquery, oledbconn);
                DataSet ds = new DataSet();
                dr.Fill(ds);
                Exceldt = ds.Tables[0];
            } else 
            if (fileType == ".csv")
            { 
                StreamReader sr = new StreamReader(path);
                string line = sr.ReadLine();
                string[] value = line.Split(',');
                Exceldt = new DataTable();
                DataRow rows;
                foreach (string dc in value)
                {
                    Exceldt.Columns.Add(new DataColumn(dc));
                }

                while (!sr.EndOfStream)
                {
                    value = sr.ReadLine().Split(',');
                    if (value.Length == Exceldt.Columns.Count)
                    {
                        rows = Exceldt.NewRow();
                        rows.ItemArray = value;
                        Exceldt.Rows.Add(rows);
                    }
                }
            }
            gv.DataSource = Exceldt;
            gv.DataBind();

            SendInsert();
        }
        catch (Exception ex)
        {
            MessageBox("Error Upload : " + ex);
            gv.DataSource = null; gv.DataBind();
        }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        importdatafromexcel(GetUploadFile());
    }
        protected void btnDownload_Click(object sender, EventArgs e)
    {
        string fileName = "CSI_Template.xls";
        string filePath = "Reports/";
        if (File.Exists(Server.MapPath(filePath + fileName)))
        {
            HttpResponse Response = HttpContext.Current.Response;
            Response.Clear();
            Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/octet-stream";
            //Response.WriteFile(Server.MapPath(filePath + fileName));
            Response.TransmitFile(Server.MapPath(filePath + fileName));
            Response.Flush();
            Response.End();            
        }
        else
        {
            MessageBox("File could not be found");
        }

    }

    protected void btnLookUp_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/LookUpCSI.aspx", false);
    }
}