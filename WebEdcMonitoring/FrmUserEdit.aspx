﻿<%@ Page Title="Register Approve" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="FrmUserEdit.aspx.cs" Inherits="FrmUserEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="script_css" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
            font-size: large;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="jumbotron"></div>
      <div class="panel panel-default">
        <div class="auto-style1" data-toggle="collapse" data-target="#filter"><strong>LAST LOGIN USER</strong></div>
        <div class="panel-body" id="filter">
            <div class="row">
            <div class="col-md-12 padding-top">  
                    <div class="panel panel-default">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group pull-left padding-center">
                            <asp:Button ID="Cancel" runat="server" Text="Back" CssClass="btn btn-info" OnClick="Cancel_Click" />                                    
     
                        </div>
                    </div>
                    </div>
                </div>

            </div>
            </div>
            <div class="row">
                    <div class="col-md-12">     
                        <div class="table-responsive" overflow: scroll; >       
      
                                      
                            <asp:Repeater ID="repeaterData" runat="server" >
                                <HeaderTemplate>
                                    <div class="template">
                                    <table id="dataTable" class="display responsive no-wrap" width="100%" border="0">
                                        <thead>
                                            <tr>
                                                <th>User Domain</th>
                                                <th>User Name</th>
                                                <th>Last Login</th>
                                                <th>Total Invalid Password</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                 </HeaderTemplate>
                                 <ItemTemplate>
                                                
                                        <tr class="input-sm">
                                            <td>
                                                <asp:Label ID="tb1" CssClass="control-label input-sm" runat="server" Text='<%# Eval("UserDomain") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="tb2" CssClass="control-label input-sm" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb3" CssClass="control-label input-sm" runat="server" Text='<%# Eval("LastLogin") %>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb4" CssClass="control-label input-sm" runat="server" Text='<%# Eval("TotalInvalidPassword") %>'></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnSave" Text="Save" CommandName="Save" OnClick="SaveOrUpdate" runat="server" />
                                            </td>
                                        </tr>
                                                
                                  </ItemTemplate>
                                    <FooterTemplate>
                                    </tbody>
                                    </table>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>
                    </div>
            </div>   

        </div>        
    </div>
    <div>


    </div>
</asp:Content>

