﻿<%@ Page Title="Trx Monitoring Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="dataMonitorTrx.aspx.cs" Inherits="dataMonitorTrx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="script_css" runat="server">
    <style type="text/css">
        .gvwCasesPager a {
                color: #fff;
                text-decoration:underline;
                margin-left:5px;
                margin-right:5px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2>Data Monitoring Transaction</h2>
        <br />
        <asp:Button ID="btnExport" CssClass="btn-primary btn-sm"  runat="server" Text="Export" OnClick="btnExport_Click" Font-Bold="True" Font-Size="Medium" />

    </div>
    <div class="row">
        <a href="javascript: history.go(-1)">Go Back</a>
    </div>
    <br />

    <div class="row">
        <div class="panel panel-default" visible="false">
            <div class="panel-heading" visible="false" data-toggle="collapse" data-target="#list"><strong>Data Monitoring Transaction List</strong></div>
            <div class="panel-body" id="panel_grid">
                <div style="overflow: scroll">

                    <asp:Label ID="lblTimestamp" runat="server"></asp:Label>
                    <asp:Label ID="lblSerial" runat="server"></asp:Label>
                    <asp:GridView ID="gridviewTransaction" runat="server" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="gridviewTransaction_PageIndexChanging">
                        <AlternatingRowStyle BackColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerSettings Position="TopAndBottom" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Yellow" HorizontalAlign="Center" CssClass ="gvwCasesPager" Font-Bold="true"/> 
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
