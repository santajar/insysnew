﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebClass;

public partial class dataMonitorSIM : System.Web.UI.Page
{
    protected ConnectionClass sqlconn = new ConnectionClass();
    protected DataTable dtDataMonitoringSIM;
    protected string sTimestamp;
    protected string sSerialNumber;
    protected string sFlag;
    protected double latitude = 0;
    protected double longitude = 0;
    protected string mapsKey = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            mapsKey = ConfigurationManager.AppSettings["GoogleMapsKey"].ToString();

            sTimestamp = Request.QueryString["Time"];
            sSerialNumber = Request.QueryString["Serial"];
            sFlag = Request.QueryString["Flag"];
            if (!string.IsNullOrEmpty(sTimestamp) && !string.IsNullOrEmpty(sSerialNumber))
            {
                sqlconn.Connection();
                dtDataMonitoringSIM = dtGetDetailSIM(sTimestamp, sSerialNumber, sFlag);
                if (dtDataMonitoringSIM != null && dtDataMonitoringSIM.Rows.Count > 0)
                {
                    gridviewSIM.DataSource = dtDataMonitoringSIM;
                    gridviewSIM.DataBind();

                    int MCC, MNC, LAC, CellID;
                    MCC = int.Parse(dtDataMonitoringSIM.Rows[0]["MCC"].ToString());
                    MNC = int.Parse(dtDataMonitoringSIM.Rows[0]["MNC"].ToString());
                    LAC = int.Parse(dtDataMonitoringSIM.Rows[0]["LAC"].ToString());
                    CellID = int.Parse(dtDataMonitoringSIM.Rows[0]["CID"].ToString());

                    string[] s = GoogleMapsApi.GetLatLng(MCC, MNC, LAC, CellID);

                    Boolean b1 = double.TryParse(s[0], out latitude);
                    Boolean b2 = double.TryParse(s[1], out longitude);
                }
                else
                {
                    lblTimestamp.Text = "No Records available";
                    latitude = 0;
                    longitude = 0;
                }
            }
        }
    }

    protected DataTable dtGetDetailSIM(string sTimestamp, string sSerialNumber, string sFlag)
    {
        DataTable dtTemp = new DataTable();
        SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPEdcMonitorBrowseSIM, sqlconn.MyConnection);
        sqlcmd.CommandType = CommandType.StoredProcedure;
        sqlcmd.Parameters.Add("@sTimestamp", SqlDbType.VarChar).Value = sTimestamp;
        sqlcmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = sSerialNumber;
        sqlcmd.Parameters.Add("@sFlag", SqlDbType.VarChar).Value = sFlag;
        using (SqlDataReader reader = sqlcmd.ExecuteReader())
            if (reader.HasRows)
                dtTemp.Load(reader);
        return dtTemp;
    }
}