﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%: DateTime.Now.AddDays(-7).ToString("MMM dd, yyyy") %>
    <div class="row">
        <div class="col-md-4">
            <h3>Total Settlement Terminal</h3>
            <h4>Based on Software Version</h4>
            <asp:Chart ID="chartSoftwareTotal" runat="server" Height="300px" Width="300px">
                <Series>
                    <asp:Series Name="Series1" ChartType="Pie">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        <div class="col-md-6">
            <h3>Total HeartBeat Active, InActive & Not List</h3>
            <h4>Today: <%: DateTime.Now.ToString("MMM dd, yyyy  HH:mm:ss")%></h4>
            <asp:Chart ID="chartHearBeat" runat="server" Height="300px" Width="300px">
                <Series>
                    <asp:Series Name="TerminalID" ChartType="Pie">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>

        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h3>Total Transaction Terminal</h3>
            <h4><%: DateTime.Now.AddDays(-7).ToString("MMM dd, yyyy") %> to <%: DateTime.Now.ToString("MMM dd, yyyy") %></h4>
            <asp:Chart ID="chartTerminalTrx" runat="server" Height="300px" Width="300px">
                <Series>
                    <asp:Series Name="TerminalID" ChartType="Pie">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
        <div class="col-md-6">
            <h3>Total Transaction Amount by Card Type</h3>
                <h4><%: DateTime.Now.AddDays(-7).ToString("MMM dd, yyyy") %> to <%: DateTime.Now.ToString("MMM dd, yyyy") %></h4>
            <asp:Chart ID="chartCardTotalAmount" runat="server" Height="300px" Width="300px">
                <Series>
                    <asp:Series Name="TerminalID" ChartType="Pie">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </div>
    </div>
    <div class="row">
        <asp:Label ID="lblTimer" Text="" runat="server" />
        <asp:Timer ID="timerRefresh" runat="server" Interval="30000" OnTick="timerRefresh_Tick">
        </asp:Timer>
    </div>
</asp:Content>
