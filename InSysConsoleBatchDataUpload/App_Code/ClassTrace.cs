using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using InSysClass;

namespace InSysConsoleBatchDataUpload
{
    class Trace
    {
        static string sPath = Environment.CurrentDirectory;
            //Path.GetDirectoryName(            Assembly.GetExecutingAssembly().GetName().CodeBase);

        static string sDirName = "\\TRACE\\";
        static string sFilename = "Trace_";
        static string sFileType = ".log";

        public static void Write(string sMessage)
        {
            string sFile = null;
            if (!isTraceFolderExist())
                Directory.CreateDirectory(sPath + sDirName);
            sFile = sPath + sDirName + sFilename + CommonConsole.sDateYYYYMMDD() + sFileType;
            CommonLib.Write2File(sFile, CommonConsole.sFullTime() + " " + sMessage, true);
        }

        public static void Write(byte[] arrbMessage)
        {
            string sFile = null;
            if (!isTraceFolderExist())
                Directory.CreateDirectory(sPath + sDirName);
            sFile = sPath + sDirName + sFilename + CommonConsole.sDateYYYYMMDD() + sFileType;
            byte[] arrbValue = new byte[1024];

            arrbValue = CommonLib.arrbStringtoByteArray(CommonConsole.sFullTime());

            Array.Copy(arrbMessage, 0, arrbValue, arrbValue.Length + 1, arrbMessage.Length);
            //Array.Copy(arrbMessage, 1, arrbValue, arrbValue.Length + 1, arrbMessage.Length);
            CommonLib.Write2File(sFile, arrbValue);
        }

        public static void Write(string sFileName, string sMessage)
        {
            string sFile = null;
            if (!isTraceFolderExist())
                Directory.CreateDirectory(sPath + sDirName);
            sFile = sPath + sDirName + sFileName;
            CommonLib.Write2File(sFile, sMessage, true);
        }

        private static bool isTraceFolderExist()
        {
            return Directory.Exists(sPath + sDirName);
        }
    }
}
