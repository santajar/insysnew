using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace InSysConsoleBatchDataUpload
{
    public enum ConnType
    {
        TcpIP = 1,
        Serial = 2,
        Modem = 3
    }

    class CommonConsole
    {
        static CultureInfo ciUSFormat = new CultureInfo("en-US", true);

        public static string sFullTime()
        {
            return DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
        }

        public static string sDateYYYYMMDD()
        {
            return DateTime.Now.ToString("yyyyMMdd", ciUSFormat);
        }

        public static string sTimeHHMMSS()
        {
            return DateTime.Now.ToString("hhmmss", ciUSFormat);
        }

        public static string sDateMMddyy()
        {
            return DateTime.Now.ToString("MMddyy", ciUSFormat);
        }

        public static void WriteToConsole(string sMessage)
        {
            string sTime = CommonConsole.sFullTime();
            Console.WriteLine("[{0}] {1}", sTime, sMessage);
        }
    }
    
    class CommonSP
    {
        public static string sSPFlagControlAllowInit = "spControlFlagAllowInitBrowse";
        public static string sSPDownloadProcessMessage = "spOtherParseDownloadProcess";        
        public static string sSPControlFlagBrowse = "spControlFlagBrowseItem";
    }
}