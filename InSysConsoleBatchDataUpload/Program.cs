﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using InSysClass;

namespace InSysConsoleBatchDataUpload
{
    class Program
    {
        /*
         * InSysConsoleBatchDataUpload
         * Automate the DataUpload process on specific time and specific folder.
         * The time itself will be setup on windows task scheduler.
         */
       
        static SqlConnection oConn = new SqlConnection();
        static string sConnString;
        static string sAppDirectory = Directory.GetCurrentDirectory();

        static ClassDataUpload oDataUpload;

        enum UploadMode
        {
            Add = 1,
            Update,
            Delete,
        }

        static void Main(string[] args)
        {
            try
            {
                oConn = InitConnection();
                StartUploadConsole(UploadMode.Delete);
                StartUploadConsole(UploadMode.Add);
                StartUploadConsole(UploadMode.Update);
            }
            catch (Exception ex)
            {
                Trace.Write("ERROR|" + ex.Message);
            }
        }

        #region "Function"
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception ex)
            {
                oSqlTempConn = null;
                Trace.Write("InitConn : " + ex.Message);
            }
            return oSqlTempConn;
        }

        static List<string> ltGetExcelFiles(string sFolder)
        {
            List<string> ltFileTemp = new List<string>();
            DirectoryInfo di = new DirectoryInfo(string.Format(@"{0}\{1}\", sAppDirectory, sFolder));
            FileInfo[] arrfiExcel = di.GetFiles("*.xls");
            foreach (FileInfo fi in arrfiExcel)
                ltFileTemp.Add(fi.FullName);
            return ltFileTemp;
        }

        static DataTable dtGetExcelUpload(string sFilename, ref string _sError)
        {
            DataTable dtTemp = new DataTable();
            OleDbConnection oOleConn = new OleDbConnection(OleDBConnLib.sConn(sFilename, ""));
            oOleConn.Open();
            new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Sheet1$]", oOleConn)).Fill(dtTemp);
            oOleConn.Close();
            ClassDataUpload oClassDUTemp = new ClassDataUpload(oConn, dtTemp);
            string sErrorColumn = null;
            int iRowIndex = -1;
            string sAcquirerName = null;
            if (!oClassDUTemp.IsValidExcel(ref sErrorColumn, ref iRowIndex, ref sAcquirerName))
                _sError = "Empty field on Column \"" + sErrorColumn.Replace(" ", "_") + "_" + sAcquirerName + "\", Row \"" + (iRowIndex + 1) + "\"";
            return dtTemp;
        }

        static void WriteUploadLog(string sFilename, string _sLog)
        {
            string sLogFile = sFilename + ".log";
            CommonLib.Write2File(sLogFile, _sLog, true);
            File.Copy(sFilename, sFilename + ".done");
            File.Delete(sFilename);
        }

        static void StartUploadConsole(UploadMode oUploadMode)
        {
            List<string> ltUploadFiles = new List<string>();
            ltUploadFiles = ltGetExcelFiles(oUploadMode.ToString().ToUpper());
            if (ltUploadFiles.Count > 0)
                foreach (string sFilename in ltUploadFiles)
                {
                    string sError = null;
                    DataTable dtTempExcelUpload = dtGetExcelUpload(sFilename, ref sError);
                    if (string.IsNullOrEmpty(sError))
                    {
                        oDataUpload = new ClassDataUpload(oConn, dtTempExcelUpload);
                        oDataUpload.iUploadMode = oUploadMode.GetHashCode();
                        oDataUpload.UserID = "UPLOAD_" + oUploadMode.ToString().ToUpper();
                        oDataUpload.LogMessage = null;
                        oDataUpload.StartUpload();
                        WriteUploadLog(sFilename, oDataUpload.LogMessage);
                    }
                }
        }
        #endregion
    }
}
