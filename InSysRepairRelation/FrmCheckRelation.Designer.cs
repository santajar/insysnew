﻿namespace InSysRepairRelation
{
    partial class FrmCheckRelation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbParameters = new System.Windows.Forms.GroupBox();
            this.btnFixIt = new System.Windows.Forms.Button();
            this.btnCheck = new System.Windows.Forms.Button();
            this.txtMaster = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTerminalID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbStatus = new System.Windows.Forms.GroupBox();
            this.rtbStatus = new System.Windows.Forms.RichTextBox();
            this.bwProcess = new System.ComponentModel.BackgroundWorker();
            this.cmsClearStatus = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbParameters.SuspendLayout();
            this.gbStatus.SuspendLayout();
            this.cmsClearStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbParameters
            // 
            this.gbParameters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbParameters.Controls.Add(this.btnFixIt);
            this.gbParameters.Controls.Add(this.btnCheck);
            this.gbParameters.Controls.Add(this.txtMaster);
            this.gbParameters.Controls.Add(this.label2);
            this.gbParameters.Controls.Add(this.txtTerminalID);
            this.gbParameters.Controls.Add(this.label1);
            this.gbParameters.Location = new System.Drawing.Point(14, 14);
            this.gbParameters.Margin = new System.Windows.Forms.Padding(4);
            this.gbParameters.Name = "gbParameters";
            this.gbParameters.Padding = new System.Windows.Forms.Padding(4);
            this.gbParameters.Size = new System.Drawing.Size(776, 51);
            this.gbParameters.TabIndex = 0;
            this.gbParameters.TabStop = false;
            // 
            // btnFixIt
            // 
            this.btnFixIt.Location = new System.Drawing.Point(675, 13);
            this.btnFixIt.Margin = new System.Windows.Forms.Padding(4);
            this.btnFixIt.Name = "btnFixIt";
            this.btnFixIt.Size = new System.Drawing.Size(88, 26);
            this.btnFixIt.TabIndex = 5;
            this.btnFixIt.Text = "Fix It";
            this.btnFixIt.UseVisualStyleBackColor = true;
            this.btnFixIt.Click += new System.EventHandler(this.btnFixIt_Click);
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(570, 13);
            this.btnCheck.Margin = new System.Windows.Forms.Padding(4);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(88, 26);
            this.btnCheck.TabIndex = 4;
            this.btnCheck.Text = "Check";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // txtMaster
            // 
            this.txtMaster.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaster.Location = new System.Drawing.Point(389, 15);
            this.txtMaster.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaster.MaxLength = 8;
            this.txtMaster.Name = "txtMaster";
            this.txtMaster.Size = new System.Drawing.Size(154, 21);
            this.txtMaster.TabIndex = 3;
            this.txtMaster.TextChanged += new System.EventHandler(this.txtMaster_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(305, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Master";
            // 
            // txtTerminalID
            // 
            this.txtTerminalID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTerminalID.Location = new System.Drawing.Point(105, 15);
            this.txtTerminalID.Margin = new System.Windows.Forms.Padding(4);
            this.txtTerminalID.MaxLength = 8;
            this.txtTerminalID.Name = "txtTerminalID";
            this.txtTerminalID.Size = new System.Drawing.Size(154, 21);
            this.txtTerminalID.TabIndex = 1;
            this.txtTerminalID.TextChanged += new System.EventHandler(this.txtTerminalID_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "TerminalID";
            // 
            // gbStatus
            // 
            this.gbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbStatus.Controls.Add(this.rtbStatus);
            this.gbStatus.Location = new System.Drawing.Point(14, 71);
            this.gbStatus.Margin = new System.Windows.Forms.Padding(4);
            this.gbStatus.Name = "gbStatus";
            this.gbStatus.Padding = new System.Windows.Forms.Padding(4);
            this.gbStatus.Size = new System.Drawing.Size(776, 217);
            this.gbStatus.TabIndex = 1;
            this.gbStatus.TabStop = false;
            // 
            // rtbStatus
            // 
            this.rtbStatus.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rtbStatus.ContextMenuStrip = this.cmsClearStatus;
            this.rtbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbStatus.Location = new System.Drawing.Point(4, 18);
            this.rtbStatus.Margin = new System.Windows.Forms.Padding(4);
            this.rtbStatus.Name = "rtbStatus";
            this.rtbStatus.ReadOnly = true;
            this.rtbStatus.Size = new System.Drawing.Size(768, 195);
            this.rtbStatus.TabIndex = 0;
            this.rtbStatus.Text = "";
            // 
            // bwProcess
            // 
            this.bwProcess.WorkerReportsProgress = true;
            this.bwProcess.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwProcess_DoWork);
            this.bwProcess.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwProcess_RunWorkerCompleted);
            this.bwProcess.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwProcess_ProgressChanged);
            // 
            // cmsClearStatus
            // 
            this.cmsClearStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem});
            this.cmsClearStatus.Name = "cmsClearStatus";
            this.cmsClearStatus.Size = new System.Drawing.Size(102, 26);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // FrmCheckRelation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 302);
            this.Controls.Add(this.gbStatus);
            this.Controls.Add(this.gbParameters);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmCheckRelation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InSys Check & Repair Relation";
            this.Load += new System.EventHandler(this.FrmCheckRelation_Load);
            this.gbParameters.ResumeLayout(false);
            this.gbParameters.PerformLayout();
            this.gbStatus.ResumeLayout(false);
            this.cmsClearStatus.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbParameters;
        private System.Windows.Forms.TextBox txtMaster;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTerminalID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbStatus;
        private System.Windows.Forms.RichTextBox rtbStatus;
        private System.Windows.Forms.Button btnFixIt;
        private System.Windows.Forms.Button btnCheck;
        private System.ComponentModel.BackgroundWorker bwProcess;
        private System.Windows.Forms.ContextMenuStrip cmsClearStatus;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
    }
}

