﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using InSysClass;
using Ini;

namespace InSysRepairRelation
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;

        public InitData()
        {
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";

            if (File.Exists(sFileName))
            {
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                };
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnString(sDataSource, sDatabase, sUserID, sPassword);
        }

        public void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword)
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";

            try
            {
                IniFile objIniFile = new IniFile(sFileName);
                objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
                objIniFile.IniWriteValue("Database", "Database", sDatabase);
                objIniFile.IniWriteValue("Database", "UserID", sUserID);
                objIniFile.IniWriteValue("Database", "Password", sPassword);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            };
        }
    }
}
