﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using InSysClass;

namespace InSysRepairRelation
{
    public partial class FrmCheckRelation : Form
    {
        public FrmCheckRelation()
        {
            InitializeComponent();
        }

        SqlConnection oConn;
        string sTerminalId;
        string sMasterId;

        DataTable dtAcquirerList = new DataTable();
        DataTable dtAcquirerListMaster = new DataTable();
        DataTable dtAcquirer = new DataTable();
        DataTable dtIssuerList = new DataTable();
        DataTable dtIssuerListMaster = new DataTable();
        DataTable dtIssuer = new DataTable();
        DataTable dtRelation = new DataTable();
        DataTable dtRelationMaster = new DataTable();

        private void FrmCheckRelation_Load(object sender, EventArgs e)
        {
            Init();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            SetDisplay(false);
            // pass false boolean to check terminalid is broken or not
            if (IsValid())
                bwProcess.RunWorkerAsync(false);
        }

        private void btnFixIt_Click(object sender, EventArgs e)
        {
            SetDisplay(false);
            // pass true to repair broken terminalId
            if (IsValid())
                bwProcess.RunWorkerAsync(true);
        }

        private void bwProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            if (IsBroken())
            {
                if ((bool)e.Argument == false)
                    bwProcess.ReportProgress(100, string.Format("TerminalID '{0}' is broken. Please press 'FixIt' button.", sTerminalId));
                else
                {
                    bwProcess.ReportProgress(100, string.Format("TerminalID '{0}' is broken. Start fixing...", sTerminalId));
                    FixIt();
                    bwProcess.ReportProgress(100, string.Format("Fixing TerminalID '{0}' is DONE.", sTerminalId));
                }
            }
            else
                bwProcess.ReportProgress(100, string.Format("TerminalID '{0}' is not broken.", sTerminalId));
        }

        private void bwProcess_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            WriteStatus(e.UserState.ToString());
        }

        private void bwProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay(true);
        }

        private void txtTerminalID_TextChanged(object sender, EventArgs e)
        {
            if (txtTerminalID.Text.Length == 8) sTerminalId = txtTerminalID.Text;
        }

        private void txtMaster_TextChanged(object sender, EventArgs e)
        {
            if (txtMaster.Text.Length == 8) sMasterId = txtMaster.Text;
        }

        protected void Init()
        {
            try
            {
                oConn = new SqlConnection((new InitData()).sGetConnString());
                oConn.Open();
            }
            catch (Exception ex)
            {
                WriteStatus("ERROR : " + ex.Message);
            }
        }

        protected void WriteStatus(string _sMessage)
        {
            rtbStatus.AppendText(
                string.Format("[{0:MMM dd, yyyy, hh:mm:ss:fff tt}] {1}\n", DateTime.Now, _sMessage));
        }

        protected bool IsValid()
        {
            return !string.IsNullOrEmpty(sTerminalId) && !string.IsNullOrEmpty(sMasterId) ? true : false;
        }

        protected bool IsBroken()
        {
            InitTable();
            if (dtAcquirerList.Rows.Count > 0
                && dtAcquirerList.Rows.Count == dtAcquirerListMaster.Rows.Count)
                if (dtIssuerList.Rows.Count > 0
                    && dtIssuerList.Rows.Count == dtIssuerListMaster.Rows.Count)
                    if (IsDataTableEquals((new DataView(dtRelation.Select("RelationTag='AD03'").CopyToDataTable())).ToTable(true, "RelationTagValue"),
                        (new DataView(dtRelationMaster.Select("RelationTag='AD03'").CopyToDataTable())).ToTable(true, "RelationTagValue")))
                        if (IsDataTableEquals((new DataView(dtRelation.Select("RelationTag='AD02'").CopyToDataTable())).ToTable(true, "RelationTagValue"),
                        (new DataView(dtRelationMaster.Select("RelationTag='AD02'").CopyToDataTable())).ToTable(true, "RelationTagValue")))
                            return false;
            return true;
        }

        protected bool IsDataTableEquals(DataTable dtTable1, DataTable dtTable2)
        {
            if (dtTable1.Rows.Count == dtTable2.Rows.Count)
                if (dtTable1.Columns.Count == dtTable2.Columns.Count)
                {
                    foreach (DataRow row in dtTable1.Rows)
                        if (!string.IsNullOrEmpty(row[0].ToString()))
                            if (dtTable2.Select(string.Format("RelationTagValue='{0}'", row[0])).Length == 0)
                                return false;
                    return true;
                }
            return false;
        }

        protected void FixIt()
        {
            string sUserId = null;
            if (IsAllowUserAccess(sTerminalId, ref sUserId))
            {
                UserAccessInsert(sTerminalId);
                foreach (DataRow rowAcq in dtAcquirerList.Rows)
                    if (dtAcquirerListMaster.Rows.Find(rowAcq[0]) == null)
                        DeleteAcquirer(rowAcq[0].ToString());
                //foreach (DataRow rowAcq in dtAcquirerListMaster.Rows)
                //    if (dtAcquirerList.Rows.Find(rowAcq[0]) == null)
                //        AddAcquirer(rowAcq[0].ToString());

                foreach (DataRow rowIss in dtIssuerList.Rows)
                    if (dtIssuerListMaster.Rows.Find(rowIss[0]) == null)
                        DeleteIssuer(rowIss[0].ToString());
                foreach (DataRow rowIss in dtIssuerListMaster.Rows)
                    if (dtIssuerList.Rows.Find(rowIss[0]) == null)
                        AddIssuer(rowIss[0].ToString());
                if (
                    !IsDataTableEquals((new DataView(dtRelation.Select("RelationTag='AD03'").CopyToDataTable())).ToTable(true, "RelationTagValue"),
                            (new DataView(dtRelationMaster.Select("RelationTag='AD03'").CopyToDataTable())).ToTable(true, "RelationTagValue"))
                    ||
                    !IsDataTableEquals((new DataView(dtRelation.Select("RelationTag='AD02'").CopyToDataTable())).ToTable(true, "RelationTagValue"),
                    (new DataView(dtRelationMaster.Select("RelationTag='AD02'").CopyToDataTable())).ToTable(true, "RelationTagValue")))
                {
                    dtRelation = null;
                    dtRelation = dtRelationMaster.Copy();
                    DataColumn col = new DataColumn("TerminalID");
                    col.DefaultValue = sTerminalId;
                    dtRelation.Columns.Remove(col.ColumnName);
                    dtRelation.Columns.Add(col);
                    DeleteRelation();
                    StartUploadAddBulkRelation(dtRelation);
                }
                SaveFullContentClass.SaveFullContent(oConn,sTerminalId);
                UserAccessDelete(sTerminalId);
            }
            else
                bwProcess.ReportProgress(100, string.Format("Fixing TerminalID '{0}' ERROR,.", sTerminalId));
        }

        protected void InitTable()
        {
            dtAcquirerList = new DataTable();
            dtAcquirerListMaster = new DataTable();
            dtAcquirer = new DataTable();
            dtIssuerList = new DataTable();
            dtIssuerListMaster = new DataTable();
            dtIssuer = new DataTable();
            dtRelation = new DataTable();
            dtRelationMaster = new DataTable();

            string sQuery = string.Format(
                "SELECT TerminalId, RelationTag, RelationLengthOfTagLength, RelationTagLength, RelationTagValue " +
                "FROM tbProfileRelation WHERE TerminalID IN ('{0}','{1}') " +
                "SELECT TerminalId, AcquirerName, AcquirerTag, AcquirerLengthOfTagLength, AcquirerTagLength, AcquirerTagValue " +
                "FROM tbProfileAcquirer WHERE TerminalID IN ('{0}','{1}') " +
                "SELECT TerminalId, IssuerName, IssuerTag, IssuerLengthOfTagLength, IssuerTagLength, IssuerTagValue " +
                "FROM tbProfileIssuer WHERE TerminalID IN ('{0}','{1}') "
                , sMasterId, sTerminalId);
            DataSet dsTemp = new DataSet();
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                (new SqlDataAdapter(oCmd)).Fill(dsTemp);
            dtRelation = (
                from relation in dsTemp.Tables[0].AsEnumerable()
                where relation.Field<string>("TerminalID") == sTerminalId
                select relation).CopyToDataTable();
            dtRelationMaster = (
                from relation in dsTemp.Tables[0].AsEnumerable()
                where relation.Field<string>("TerminalID") == sMasterId
                select relation).CopyToDataTable();            
            dtAcquirerListMaster.Columns.Add("AcquirerName");
            dtAcquirerListMaster.PrimaryKey = new DataColumn[] { dtAcquirerListMaster.Columns["AcquirerName"] };
            foreach (var rowAcqList in
                (from relMaster in dtRelationMaster.AsEnumerable()
                 where relMaster.Field<string>("RelationTag") == "AD03" && relMaster.Field<string>("RelationTagValue") != null
                 select relMaster.Field<string>("RelationTagValue")).Distinct())
                dtAcquirerListMaster.Rows.Add(rowAcqList);
            dtIssuerListMaster.Columns.Add("IssuerName");
            dtIssuerListMaster.PrimaryKey = new DataColumn[] { dtIssuerListMaster.Columns["IssuerName"] };
            foreach (var rowIssList in
                (from relMaster in dtRelationMaster.AsEnumerable()
                 where relMaster.Field<string>("RelationTag") == "AD02" && relMaster.Field<string>("RelationTagValue") != null
                 select relMaster.Field<string>("RelationTagValue")).Distinct())
                dtIssuerListMaster.Rows.Add(rowIssList);
            dtAcquirer = dsTemp.Tables[1];
            dtIssuer = dsTemp.Tables[2];

            dtAcquirerList.Columns.Add("AcquirerName");
            dtAcquirerList.PrimaryKey = new DataColumn[] { dtAcquirerList.Columns["AcquirerName"] };
            if (dtAcquirer.Select(string.Format("TerminalID='{0}' AND AcquirerTag='AA01'", sTerminalId)).Length > 0)
                foreach (var rowAcq in
                        (
                        from acq in dtAcquirer.AsEnumerable()
                        where acq.Field<string>("TerminalID") == sTerminalId
                        orderby acq.Field<string>("AcquirerName")
                        select acq.Field<string>("AcquirerName")
                        ).Distinct())
                    dtAcquirerList.Rows.Add(rowAcq.ToString());
            dtIssuerList.Columns.Add("IssuerName");
            dtIssuerList.PrimaryKey = new DataColumn[] { dtIssuerList.Columns["IssuerName"] };
            if (dtIssuer.Select(string.Format("TerminalID='{0}' AND IssuerTag='AE01'", sTerminalId)).Length > 0)
                foreach (var rowIss in
                        (from iss in dtIssuer.AsEnumerable()
                         where iss.Field<string>("TerminalID") == sTerminalId
                         orderby iss.Field<string>("IssuerName")
                         select iss.Field<string>("IssuerName")
                        ).Distinct())
                    dtIssuerList.Rows.Add(rowIss.ToString());
        }

        protected void SetDisplay(bool _isEnable)
        {
            gbParameters.Enabled = _isEnable;
            if (_isEnable)
                this.Cursor = Cursors.Arrow;
            else
                this.Cursor = Cursors.WaitCursor;
        }

        protected void AddAcquirer(string sAcquirerName)
        {
            DataTable dtTemp = new DataTable();
            dtTemp = dtAcquirer.Select(
                string.Format("TerminalID='{0}' AND AcquirerName='{1}'", sMasterId, sAcquirerName)).CopyToDataTable();
            DataColumn colTerminalID = new DataColumn("TerminalID");
            colTerminalID.DefaultValue = sTerminalId;
            dtTemp.Columns.Remove(colTerminalID.ColumnName);
            dtTemp.Columns.Add(colTerminalID);
            StartUploadAddBulkAcquirer(dtTemp);
        }

        protected void DeleteAcquirer(string sAcquirerName)
        {
            string sQuery = string.Format("DELETE FROM tbProfileAcquirer WHERE TerminalID='{0}' AND AcquirerName='{1}'", sTerminalId, sAcquirerName);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                oCmd.ExecuteNonQuery();
        }

        protected void StartUploadAddBulkAcquirer(DataTable _dtAcquirer)
        {
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oConn);
            oSqlBulk.DestinationTableName = "tbProfileAcquirer";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapAcquirerTag = new SqlBulkCopyColumnMapping("AcquirerTag", "AcquirerTag");
            SqlBulkCopyColumnMapping mapAcquirerName = new SqlBulkCopyColumnMapping("AcquirerName", "AcquirerName");
            SqlBulkCopyColumnMapping mapAcquirerTagValue = new SqlBulkCopyColumnMapping("AcquirerTagValue", "AcquirerTagValue");
            SqlBulkCopyColumnMapping mapAcquirerTagLength = new SqlBulkCopyColumnMapping("AcquirerTagLength", "AcquirerTagLength");
            SqlBulkCopyColumnMapping mapAcquirerLengthOfTagLength = new SqlBulkCopyColumnMapping("AcquirerLengthOfTagLength", "AcquirerLengthOfTagLength");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapAcquirerName);
            oSqlBulk.ColumnMappings.Add(mapAcquirerTag);
            oSqlBulk.ColumnMappings.Add(mapAcquirerTagValue);
            oSqlBulk.ColumnMappings.Add(mapAcquirerTagLength);
            oSqlBulk.ColumnMappings.Add(mapAcquirerLengthOfTagLength);
            oSqlBulk.WriteToServer(_dtAcquirer);
        }

        protected void AddIssuer(string sIssuerName)
        {
            DataTable dtTemp = new DataTable();
            dtTemp = dtIssuer.Select(
                string.Format("TerminalID='{0}' AND IssuerName='{1}'", sMasterId, sIssuerName)).CopyToDataTable();
            DataColumn colTerminalID = new DataColumn("TerminalID");
            colTerminalID.DefaultValue = sTerminalId;
            dtTemp.Columns.Remove(colTerminalID.ColumnName);
            dtTemp.Columns.Add(colTerminalID);
            StartUploadAddBulkIssuer(dtTemp);
        }

        protected void DeleteIssuer(string sIssuerName)
        {
            string sQuery = string.Format("DELETE FROM tbProfileIssuer WHERE TerminalID='{0}' AND IssuerName='{1}'", sTerminalId, sIssuerName);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                oCmd.ExecuteNonQuery();
        }

        protected void StartUploadAddBulkIssuer(DataTable _dtIssuer)
        {
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oConn);
            oSqlBulk.DestinationTableName = "tbProfileIssuer";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapIssuerTag = new SqlBulkCopyColumnMapping("IssuerTag", "IssuerTag");
            SqlBulkCopyColumnMapping mapIssuerName = new SqlBulkCopyColumnMapping("IssuerName", "IssuerName");
            SqlBulkCopyColumnMapping mapIssuerTagValue = new SqlBulkCopyColumnMapping("IssuerTagValue", "IssuerTagValue");
            SqlBulkCopyColumnMapping mapIssuerTagLength = new SqlBulkCopyColumnMapping("IssuerTagLength", "IssuerTagLength");
            SqlBulkCopyColumnMapping mapIssuerLengthOfTagLength = new SqlBulkCopyColumnMapping("IssuerLengthOfTagLength", "IssuerLengthOfTagLength");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapIssuerName);
            oSqlBulk.ColumnMappings.Add(mapIssuerTag);
            oSqlBulk.ColumnMappings.Add(mapIssuerTagValue);
            oSqlBulk.ColumnMappings.Add(mapIssuerTagLength);
            oSqlBulk.ColumnMappings.Add(mapIssuerLengthOfTagLength);
            oSqlBulk.WriteToServer(_dtIssuer);
        }

        protected void DeleteRelation()
        {
            string sQuery = string.Format("DELETE FROM tbProfileRelation WHERE TerminalID='{0}'", sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
                oCmd.ExecuteNonQuery();
        }

        protected void StartUploadAddBulkRelation(DataTable _dtRelation)
        {
            SqlBulkCopy oSqlBulk = new SqlBulkCopy(oConn);
            oSqlBulk.DestinationTableName = "tbProfileRelation";
            SqlBulkCopyColumnMapping mapTerminalId = new SqlBulkCopyColumnMapping("TerminalID", "TerminalID");
            SqlBulkCopyColumnMapping mapRelationTag = new SqlBulkCopyColumnMapping("RelationTag", "RelationTag");
            SqlBulkCopyColumnMapping mapRelationTagValue = new SqlBulkCopyColumnMapping("RelationTagValue", "RelationTagValue");
            SqlBulkCopyColumnMapping mapRelationTagLength = new SqlBulkCopyColumnMapping("RelationTagLength", "RelationTagLength");
            SqlBulkCopyColumnMapping mapRelationLengthOfTagLength = new SqlBulkCopyColumnMapping("RelationLengthOfTagLength", "RelationLengthOfTagLength");
            oSqlBulk.ColumnMappings.Add(mapTerminalId);
            oSqlBulk.ColumnMappings.Add(mapRelationTag);
            oSqlBulk.ColumnMappings.Add(mapRelationTagValue);
            oSqlBulk.ColumnMappings.Add(mapRelationTagLength);
            oSqlBulk.ColumnMappings.Add(mapRelationLengthOfTagLength);
            oSqlBulk.WriteToServer(_dtRelation);
        }

         void UserAccessInsert(string _sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand("spUserAccessInsert", oConn))
            {
                if (oConn.State != ConnectionState.Open)
                    oConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = "GEMALTO";
                oCmd.ExecuteNonQuery();
            }
        }

         void UserAccessDelete(string _sTerminalId)
        {
            if (!string.IsNullOrEmpty("GEMALTO"))
                using (SqlCommand oCmd = new SqlCommand("spUserAccessDelete", oConn))
                {
                    if (oConn.State != ConnectionState.Open)
                        oConn.Open();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = "GEMALTO";
                    oCmd.ExecuteNonQuery();
                }
        }

         bool IsAllowUserAccess(string _sTerminalId, ref string sUserIdOnAccess)
        {
            bool isAllow = true;
            using (SqlCommand oCmd = new SqlCommand("spUserAccessBrowse", oConn))
            {
                if (oConn.State != ConnectionState.Open)
                    oConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@sOutput", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();

                sUserIdOnAccess = oCmd.Parameters["@sUserId"].Value.ToString();
                isAllow = oCmd.Parameters["@sOutput"].Value.ToString() == "1" ? false : true;
            }
            return isAllow;
        }

         private void clearToolStripMenuItem_Click(object sender, EventArgs e)
         {
             rtbStatus.Clear();
         }
    }
}
