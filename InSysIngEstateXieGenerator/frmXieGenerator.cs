﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace InSysIngEstateXieGenerator
{
    public partial class frmXieGenerator : Form
    {
        public frmXieGenerator()
        {
            InitializeComponent();
        }

        SqlConnection oSqlConn;
        DataTable dtEstateMaster = new DataTable();

        private void frmXieGenerator_Load(object sender, EventArgs e)
        {
            Init();
        }

        protected void Init()
        {
            InitDisplayValue();
            InitConnection();
            InitEstatemaster();
        }

        protected void InitEstatemaster()
        {
            string sQuery = "SELECT * "
                + "FROM TERMINAL "
                + "WHERE [SIGNATURE] LIKE 'ESTATE%' "
                + "ORDER BY [SIGNATURE]";
            SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            (new SqlDataAdapter(oCmd)).Fill(dtEstateMaster);

            if (dtEstateMaster != null && dtEstateMaster.Rows.Count > 0)
            {
                cmbEstateMaster.DataSource = dtEstateMaster;
                cmbEstateMaster.DisplayMember = "NAME";
                cmbEstateMaster.ValueMember = "SIGNATURE";
                cmbEstateMaster.SelectedIndex = -1;
            }
        }

        protected DataTable dtGetEstateMaster(string sSignature, string sVersion)
        {
            DataTable dtTemp = new DataTable();
            string sQuery = sGetMasterParameterValues(sSignature, sVersion);
            SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        protected void InitConnection()
        {
            try
            {
                string sConnString = ConfigurationManager.ConnectionStrings["IngEstateDB"].ConnectionString;
                oSqlConn = new SqlConnection(sConnString);
                oSqlConn.Open();
            }
            catch (SqlException sqlEx)
            {
                MessageBox.Show("INIT SQL FAILED, {0}", sqlEx.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("INIT FAILED, {0}", ex.Message);
            }
        }

        protected void InitDisplayValue()
        {
            txtTemplateId.Text = ConfigurationManager.AppSettings["templateId"].ToString();
            txtTemplateVersion.Text = ConfigurationManager.AppSettings["templateVersion"].ToString();
        }

        private string sGetMasterParameterValues(string sSignature, string sVersion)
        {
            return "SELECT * "
                + "FROM PARAMETER_VALUES "
                + "WHERE DATASET_ID IN ( "
                + "SELECT DATASET_ID "
                + "FROM PARAMETER_SET "
                + "WHERE TERMINAL_ID IN ( "
                + "SELECT TERMINAL_ID "
                + "FROM TERMINAL "
                + "WHERE [SIGNATURE]='" + sSignature +"' "
                + ") AND TEMPLATE_VERSION='"+ sVersion +"')";
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (cmbEstateMaster.SelectedIndex > -1 && !string.IsNullOrEmpty(txtTemplateVersion.Text))
            {
                DataTable dtMaster = dtGetEstateMaster(cmbEstateMaster.SelectedValue.ToString(), txtTemplateVersion.Text);
                GenerateXieFile(dtMaster);
                MessageBox.Show("OK");
            }
        }

        protected void GenerateXieFile(DataTable dtMaster)
        {
            string sFilename = Environment.CurrentDirectory + @"\output.xie";
            StreamWriter sw = new StreamWriter(sFilename);
            sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sw.WriteLine("<script version=\"1.0\">");

            sw.WriteLine("<context>");
            sw.WriteLine("<estate signature=\"ESTATE02000\"/>");
            sw.WriteLine("</context>");

            sw.WriteLine("<terminal signature=\"TEST0001\">");
            sw.WriteLine("<name value=\"TEST0001-MANDIRI\"/>");
            sw.WriteLine("<status value=\"enabled\"/>");
            sw.WriteLine("<type value=\"ISO8583_INSYS\"/>");

            //sw.WriteLine("<task name=\"Download Parameters\">");
            //sw.WriteLine("<status value=\"enabled\"/>");
            //sw.WriteLine("<priority value=\"100\"/>");
            //sw.WriteLine("<permanent value=\"true\"/>");
            //sw.WriteLine("<description />");
            //sw.WriteLine("<scenario>");
            //sw.WriteLine("<freeText>");
            //sw.WriteLine("<![CDATA[");
            //sw.WriteLine("<SCENARIO>");
            //sw.WriteLine("<downloadParameters template=\"MDR_INSYS_TMS_01.00\" version=\"0101\" />");
            //sw.WriteLine("</SCENARIO>");
            //sw.WriteLine("]]>");
            //sw.WriteLine("</freeText>");
            //sw.WriteLine("</scenario>");
            //sw.WriteLine("</task>");

            sw.WriteLine("<parameterSet templateId=\"MDR_INSYS_TMS_01.00\" templateVersion=\"0101\" >");
            foreach (DataRow row in dtMaster.Rows)
            {
                string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                sw.WriteLine(sLine);
            }
            sw.WriteLine("</parameterSet>");
            sw.WriteLine("</terminal>");
            sw.WriteLine("</script>");
            sw.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
        }
    }
}
