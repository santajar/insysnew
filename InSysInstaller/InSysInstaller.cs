using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using Ini;
using InSysClass;

namespace InSysInstaller
{
    [RunInstaller(true)]
    public partial class InSysInstaller : Installer
    {
        public InSysInstaller()
        {
            InitializeComponent();
        }

        string sSource = "InSys Installer";
        string sLog = "InSys";
        string sEvent;

        private string sGetSql(string sFilename)
        {
            try
            {
                // Gets the current assembly.
                Assembly Asm = Assembly.GetExecutingAssembly();

                // Resources are named using a fully qualified name.
                Stream strm = Asm.GetManifestResourceStream(Asm.GetName().Name + "." + sFilename);

                // Reads the contents of the embedded file.
                StreamReader reader = new StreamReader(strm);
                return reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(sSource, "Error Installation In GetSQL: " + ex.Message);
                throw ex;
            }
        }

        private void ExecuteSql(string sConnString, string sSql)
        {

            SqlConnection oMasterConn = new SqlConnection(sConnString);
            SqlCommand Command = new SqlCommand(sSql, oMasterConn);
            try
            {
                // Initialize the connection, open it, and set it to the "master" database
                Command.Connection.Open();
                //Command.Connection.ChangeDatabase(DatabaseName);

                Command.ExecuteNonQuery();
            }
            finally
            {
                // Closing the connection should be done in a Finally block
                Command.Connection.Close();
            }
        }

        protected bool IsAddDBTable(string sDbName, string sServerName, string sUsername, string sPwd)
        {
            try
            {
                // Creates the database.
                ExecuteSql(sConnString(sServerName, "master", sUsername, sPwd),
                    "CREATE DATABASE " + sDbName);

                // Creates the tables.
                ExecuteSql(sConnString(sServerName, sDbName, sUsername, sPwd),
                    sGetSql("NewInSys.sql"));
                return true;
            }
            catch (Exception ex)
            {
                // Reports any errors and abort.
                //Interaction.MsgBox("In exception handler: " + ex.Message);
                EventLog.WriteEntry(sSource, "Error Installation : " + ex.Message);
                EventLog.WriteEntry(sSource, "sConnString : " + sConnString(sServerName, "master", sUsername, sPwd));
                throw ex;
            }
        }

        private string sConnString(string sServer, string sDatabaseName, string sUID, string sPwd)
        {
            string sConn = null;
            sConn += "Server=" + sServer + ";";
            sConn += "Database=" + sDatabaseName + ";";
            sConn += "UID=" + sUID + ";";
            sConn += "Password=" + sPwd + ";";
            return sConn;
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);
                
            base.Install(stateSaver);

            string sServer = Context.Parameters["ServerName"];
            string sUsername = Context.Parameters["Username"];
            string sPassword = Context.Parameters["Password"];

            if (IsAddDBTable("NewInSys", sServer, sUsername, sPassword))
            {
            }
        }

        private void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword)
        {
            string sActiveDir = Context.Parameters["TARGETDIR"];
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            try
            {
                IniFile objIniFile = new IniFile(sFileName);
                objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
                objIniFile.IniWriteValue("Database", "Database", sDatabase);
                objIniFile.IniWriteValue("Database", "UserID", sUserID);
                objIniFile.IniWriteValue("Database", "Password", sPassword);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(sSource, "Error Installation : " + ex.Message);
            };
            EncryptionLib.EncryptFile(sEncryptFileName, sFileName);
            File.Delete(sFileName);
        }
    }
}