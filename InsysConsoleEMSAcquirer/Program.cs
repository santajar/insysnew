﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsysConsoleEMSAcquirer
{
    class Program
    {
        static int intDecrement = int.Parse((ConfigurationManager.AppSettings["Jml Pengurangan Hari"].ToString()));
        static SqlConnection sqlconn = new SqlConnection();
        static SqlConnection sqlconn2 = new SqlConnection();
        static string sConnString;        

        static string sAppDirectory = Directory.GetCurrentDirectory();

        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
            }
            return oSqlTempConn;
        }

        static string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static string configFile = System.IO.Path.Combine(appPath, "config.ini");


        static void Main(string[] args)
        {
            sqlconn = InitConnection();

            sqlconn2 = InitConnection();

            IniFile ini = new IniFile(configFile.ToString());
            //ini.IniWriteValue("ConsoleEMS", "Procedure", "spGetInsysToEMS");
            string sProcedure = ini.IniReadValue("ConsoleEMS", "Procedure");
            string sType = ini.IniReadValue("ConsoleEMS", "Type");
            string sDateStart = ini.IniReadValue("ConsoleEMS", "DateFrom");
            string sDateEnd = ini.IniReadValue("ConsoleEMS", "DateTo");
            string sDateLastRun = ini.IniReadValue("ConsoleEMS", "LastRun");
            string sNote = ini.IniReadValue("ConsoleEMS", "Notes");
            
            using (StreamWriter writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\\LOGS\\logconsEMS.txt"))

            {
                try
                {
                    Console.WriteLine("================ Aplikasi Turunan Insys ke EMS ================");
                    writer.WriteLine("================ Aplikasi Turunan Insys ke EMS ================");

                    if (string.IsNullOrEmpty(sDateLastRun))
                    {
                        sDateLastRun = string.Format("{0:MM/dd/yyyy}", LastRun(DateTime.Now));
                    }
                    else
                    {
                        if (sType.ToString() == "0")
                            sDateLastRun = string.Format("{0:MM/dd/yyyy}", sDateStart.ToString());
                        else
                            sDateLastRun = string.Format("{0:MM/dd/yyyy}", sDateLastRun.ToString());
                    }
                    Console.WriteLine("Last Run : " + string.Format("{0:MM/dd/yyyy}", sDateLastRun.ToString()));
                    writer.WriteLine("Last Run : " + string.Format("{0:MM/dd/yyyy}", sDateLastRun.ToString()));

                    Console.WriteLine("---------------------------------------------------------------");
                    writer.WriteLine("---------------------------------------------------------------");

                    if (string.IsNullOrEmpty(sDateStart))
                    {
                        sDateStart = string.Format("{0:MM/dd/yyyy}", sDateLastRun.ToString());
                    }
                    else
                    {
                        if (sType.ToString() == "0")
                            sDateStart = string.Format("{0:MM/dd/yyyy}", sDateStart.ToString());
                        else
                            sDateStart = string.Format("{0:MM/dd/yyyy}", sDateLastRun.ToString());
                    }

                    
                    Console.WriteLine("Date From : " + string.Format("{0:MM/dd/yyyy}", sDateStart.ToString()));
                    writer.WriteLine("Date From : " + string.Format("{0:MM/dd/yyyy}", sDateStart.ToString()));

                    if (string.IsNullOrEmpty(sDateEnd))
                    {
                        sDateEnd = string.Format("{0:MM/dd/yyyy}", LastRun(DateTime.Now));
                    }
                    else
                    {
                        if (sType.ToString() == "1")
                            sDateEnd = string.Format("{0:MM/dd/yyyy}", LastRun(DateTime.Now));
                        else
                            sDateEnd = string.Format("{0:MM/dd/yyyy}", sDateEnd.ToString());
                    }
                    Console.WriteLine("Date To : " + string.Format("{0:MM/dd/yyyy}", sDateEnd.ToString()));
                    writer.WriteLine("Date To : " + string.Format("{0:MM/dd/yyyy}", sDateEnd.ToString()));

                    Console.WriteLine("---------------------------------------------------------------");
                    writer.WriteLine("---------------------------------------------------------------");

                    try
                    {
                        if (sqlconn != null && sqlconn.State == ConnectionState.Open)
                        {
                            Console.WriteLine("Connection Start...");
                            writer.WriteLine("Connection Start...");
                            Console.WriteLine("---------------------------------------------------------------");
                            writer.WriteLine("---------------------------------------------------------------");

                            if (!string.IsNullOrEmpty(sProcedure))
                            {
                                Console.WriteLine("Process ...");
                                writer.WriteLine("Process ...");

                                using (SqlCommand cmd = new SqlCommand("spGetInsysToEMSAcquirer", sqlconn))
                                {
                                    cmd.CommandType = CommandType.StoredProcedure;

                                    DateTime DateFrom = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", sDateStart.ToString()));
                                    DateTime DateTo = Convert.ToDateTime(string.Format("{0:MM/dd/yyyy}", sDateEnd.ToString()));

                                    cmd.Parameters.Add("@DateFrom", SqlDbType.DateTime).Value = string.Format("{0:MM/dd/yyyy}", DateFrom);
                                    cmd.Parameters.Add("@DateTo", SqlDbType.DateTime).Value = string.Format("{0:MM/dd/yyyy}", DateTo);
                                    cmd.Parameters.Add("@SpGroupAcq", SqlDbType.VarChar).Value = sProcedure.ToString();
                                    cmd.Parameters.Add("@Notes", SqlDbType.VarChar).Value = sNote.ToString();
                                    cmd.CommandTimeout = 20000;
                                    cmd.ExecuteNonQuery();

                                    sqlconn.Dispose();
                                    Console.WriteLine("================ SUCCESS ================");
                                    writer.WriteLine("================ SUCCESS ================");
                                    try
                                    {

                                        ini.IniWriteValue("ConsoleEMS", "DateFrom", string.Format("{0:MM/dd/yyyy}", sDateStart.ToString()));
                                        ini.IniWriteValue("ConsoleEMS", "DateTo", string.Format("{0:MM/dd/yyyy}", sDateEnd.ToString()));
                                        if (sType.ToString() == "0")
                                            ini.IniWriteValue("ConsoleEMS", "LastRun", string.Format("{0:MM/dd/yyyy}", sDateEnd.ToString()));
                                        else
                                            ini.IniWriteValue("ConsoleEMS", "LastRun", string.Format("{0:MM/dd/yyyy}", LastRun(DateTime.Now)));

                                        Console.WriteLine("Kelompok Acquirer : " + sNote.ToString());
                                        writer.WriteLine("Kelompok Acquirer : " + sNote.ToString());
                                        Console.WriteLine("Config Success");
                                        writer.WriteLine("Config Success");
                                        try
                                        {
                                            using (SqlCommand Cmd = new SqlCommand("sptbConsLogRunInsert", sqlconn2))
                                            {
                                                Cmd.CommandType = CommandType.StoredProcedure;
                                                Cmd.Parameters.Add("@DateFrom", SqlDbType.DateTime).Value = string.Format("{0:MM/dd/yyyy}", sDateStart.ToString());
                                                Cmd.Parameters.Add("@DateTo", SqlDbType.DateTime).Value = string.Format("{0:MM/dd/yyyy}", sDateEnd.ToString());
                                                Cmd.Parameters.Add("@LastRun", SqlDbType.DateTime).Value = string.Format("{0:MM/dd/yyyy}", sDateLastRun.ToString());
                                                Cmd.Parameters.Add("@Flag", SqlDbType.Bit).Value = 1;

                                                if (sqlconn2.State != ConnectionState.Open) sqlconn2.Open();
                                                Cmd.ExecuteNonQuery();

                                                Console.WriteLine("Save Log Success");
                                                writer.WriteLine("Save Log Success");
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine("Save Log Error : " + ex.Message);
                                            writer.WriteLine("Save Log Error : " + ex.Message);
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("Config Error : " + ex.Message);
                                        writer.WriteLine("Config Error : " + ex.Message);
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("CONFIG. NULL Procedure");
                                writer.WriteLine("CONFIG. NULL Procedure");
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Connection Failed" + ex.Message);
                        writer.WriteLine("Connection Failed" + ex.Message);
                    }
                    //Console.Read();

                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception : " + e.Message);
                    writer.WriteLine("Exception : " + e.Message);
                }
                finally
                {
                    Console.WriteLine("Executing finally block");
                    writer.WriteLine("Executing finally block");
                }
                writer.Close();
            }
        }

        public static DateTime FirstDateOfMonth(DateTime DtTime)
        {
            return new DateTime(DtTime.Year, DtTime.Month, 1);
        }

        public static DateTime LastDateOfMonth(DateTime DtTime)
        {
            DateTime FirstDate = new DateTime(DtTime.Year, DtTime.Month, 1);
            return FirstDate.AddMonths(1).AddDays(-1);
        }

        public static DateTime LastRun(DateTime DtTime)
        {
            DateTime FirstDate = new DateTime(DtTime.Year, DtTime.Month, DtTime.Day);
            return FirstDate.AddDays(-intDecrement);
        }

    
    }
}
