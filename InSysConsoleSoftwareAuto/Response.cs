﻿using System;
using System.Data;
using System.Data.SqlClient;
using InSysClass;

namespace InSysConsoleSoftwareAuto
{
    class Response
    {
        protected static SqlConnection oConn;
        protected static bool bCheckAllowInit;

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        public static byte[] arrbResponse(byte[] arrbReceive, ConnType cType, SqlConnection _oConn, bool _IsCheckAllowInit,ref string sAppName)
        {
            oConn = _oConn;
            bCheckAllowInit = _IsCheckAllowInit;
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessageContent = null;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType, ref sSendMessageContent, ref sAppName);
                //if (!string.IsNullOrEmpty(sSendMessage))
                //{
                //    arrbTemp = new byte[iIsoLen + 2];

                //    sSendMessage = sSendMessage.Replace(" ", ""); // Remove all white space
                //    byte[] arrbbuffer = new byte[sSendMessage.Length / 2];
                //    for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                //    {
                //        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);
                //    }
                //    arrbTemp = arrbbuffer;
                //}
                if (!string.IsNullOrEmpty(sSendMessageContent))
                {
                    arrbTemp = new byte[iIsoLen + 2];

                    sSendMessageContent = sSendMessageContent.Replace(" ", ""); // Remove all white space
                    byte[] arrbbuffer = new byte[sSendMessageContent.Length / 2];
                    for (int iCount = 0; iCount < sSendMessageContent.Length; iCount += 2)
                    {
                        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessageContent.Substring(iCount, 2), 16);
                    }
                    arrbTemp = arrbbuffer;
                }
            }
            catch (Exception ex)
            {
                Logs.doWriteErrorFile("|[RESPONSE]|Error arrbResponse : " + ex.Message);
                //Console.WriteLine("[RESPONSE] Error : {0}", ex.Message);
                //Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected static string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, 
            ConnType cType, ref string _sContentReal, ref string sAppName)
        {
            string sTerminalId = null;
            string sTag = null;
            string sAppNameDownload = null;
            string sAppFilenameDownload = null;
            int iErrorCode = -1;

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            int iCurrCounter = 0;
            sAppName = sGetAppNameToDownload(_arrbRecv, cType, ref iCurrCounter);

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            Trace.Write("|[RESPONSE]|sTemp to SQL Procedure : " + sTemp);

            // kirim ke sp,...
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPDownloadProcessMessage, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 27;
                    //oCmd.CommandTimeout = 0;
                    oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                    oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                    #region "Using DataReader"
                    //SqlDataReader reader;
                    using (SqlDataReader reader = oCmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            DataTable dtTemp = new DataTable();
                            dtTemp.Load(reader);
                            iErrorCode = int.Parse(dtTemp.Rows[0]["ErrorCode"].ToString());
                            sTerminalId = dtTemp.Rows[0]["TerminalId"].ToString();
                            sTempReceive = dtTemp.Rows[0]["ISO"].ToString();
                            sTag = dtTemp.Rows[0]["Tag"].ToString();
                            sAppNameDownload = string.IsNullOrEmpty(dtTemp.Rows[0]["AppName"].ToString()) ? "" : dtTemp.Rows[0]["AppName"].ToString();
                            sAppFilenameDownload = string.IsNullOrEmpty(dtTemp.Rows[0]["AppFilename"].ToString()) ? "" : dtTemp.Rows[0]["AppFilename"].ToString();
                            _sContentReal = string.Format("{0}{1}{2}",
                                iErrorCode == 0 ? sGetProcCode(sTempReceive) : "990000",
                                sExtraData(iCurrCounter, sAppNameDownload, sAppFilenameDownload),
                                iErrorCode == 0 || iErrorCode == 1 ? dtTemp.Rows[0]["Content"].ToString() : CommonLib.sStringToHex(dtTemp.Rows[0]["Content"].ToString()));
                            //_sContentReal = _sContentReal + sCheckSum(_sContentReal);
                            //Trace.Write("[RESPONSE] Cont : " + scont);
                            sAppName = sAppNameDownload;
                        }
                    }
                    //reader.Close();
                    //reader.Dispose();
                    #endregion
                }

                //if (!string.IsNullOrEmpty(sTempReceive))
                //{
                //    // tambahkan total length
                //    _iIsoLenResponse = sTempReceive.Length / 2;
                //    sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                //    Trace.Write(string.Format("[RESPONSE]|{0}|sTempReceive : {1}", sTerminalId, sTempReceive));

                //    if (iErrorCode == 0 || iErrorCode == 1)
                //        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                //}

                if (!string.IsNullOrEmpty(_sContentReal))
                {
                    // tambahkan total length
                    _iIsoLenResponse = _sContentReal.Length / 2;
                    _sContentReal = sISOLenSend(_iIsoLenResponse, cType) + _sContentReal;
                    Trace.Write(string.Format("[RESPONSE]|{0}|sTempReceive : {1}", sTerminalId, _sContentReal));

                    //if (iErrorCode == 0 || iErrorCode == 1)
                    CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                    sTempReceive = _sContentReal;
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                //    ex.ToString(), sTemp);
                Logs.doWriteErrorFile(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                    ex.ToString(), sTemp));
                //Console.ReadKey();
                //sTemp = null;
            }
            return sTempReceive;
        }

        protected static int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            if (cType == ConnType.TcpIP)
                iReturn = CommonLib.iHexStringToInt(sTempLen);
            else if (cType == ConnType.Serial)
                iReturn = int.Parse(sTempLen);
            return iReturn;
        }

        protected static string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP || cType == ConnType.Modem)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected static string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Download Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Initialize Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                case 6:
                    sTemp = "Invalid Software";
                    break;
                default:
                    sTemp = "Unknown Error";
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected static string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
            }
            return sTemp;
        }

        protected static string sExtraData(int iCounter, string sAppName, string sAppFilename)
        {
            string sExtra = "";
            sExtra = string.Format("{0:0000}{1:0000}{2}{3:0000}{4}",
                iCounter, 
                sAppName.Length, CommonLib.sStringToHex(sAppName),
                sAppFilename.Length, CommonLib.sStringToHex(sAppFilename));
            int iTotal = 200 - sExtra.Length;
            //fill extra 100 byte
            for (int i = 1; i <= iTotal; i++)
                sExtra += "F";
            return sExtra;
        }

        protected static string sGetProcCode(string sISOMsg)
        {
            return sISOMsg.Substring(30, 6);
        }

        protected static string sCheckSum(string sValue)
        {
            Int32 iResult = 0;
            for (int iCount = 0; iCount < sValue.Length; iCount += 2)
                iResult = iResult ^ Convert.ToInt32(sValue.Substring(iCount, 2), 16);

            string sResultBinary = Convert.ToString(iResult, 2);
            string sResultHex = Convert.ToString(Convert.ToInt32(sResultBinary, 2), 16);
            sResultHex = "00".Substring(0, 2 - sResultHex.Length) + sResultHex;
            return sResultHex.ToUpper();
        }

        protected static string sGetAppNameToDownload(byte[] _arrbReceive, ConnType _cType, ref int iCurrCounter)
        {
            string sAppName = null;
            byte[] arrbMsg;
            if (_cType != ConnType.Modem)
            {
                // buang total length & tpdu, 3 byte
                arrbMsg = new byte[_arrbReceive.Length - 2];
                Array.Copy(_arrbReceive, 2, arrbMsg, 0, arrbMsg.Length);
            }
            else
            {
                arrbMsg = new byte[_arrbReceive.Length];
                Array.Copy(_arrbReceive, arrbMsg, _arrbReceive.Length);
            }
            ISO8583Lib oIso = new ISO8583Lib();
            ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
            oIso.UnPackISO(arrbMsg, ref oIsoMsg);

            string sBit57 = CommonLib.sStringToHex(oIsoMsg.sBit[57]);
            iCurrCounter = int.Parse(sBit57.Substring(0, 4));

            sAppName = CommonLib.sHexToStringUTF8(sBit57.Substring(18, int.Parse(sBit57.Substring(14, 4)) * 2));
            return sAppName;
        }
    }
}
