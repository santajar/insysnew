using System;
using System.Data.SqlClient;
using System.IO.Ports;
using System.Threading;
using InSysClass;

namespace InSysConsoleSoftwareAuto
{
    public class SerialModem : IDisposable
    {
        protected string sModemPort;

        protected const byte TPDU = 0x60;
        protected const string MTI = "0800";
        protected const string ProcCodeStart = "930000";
        protected const string ProcCodeContinue = "930001";

        protected SerialPort oSerialPort;

        protected bool IsStartReceive;

        protected string sISOMessage;
        protected string sISOMessageOld;

        protected byte[] arrbSendOld;

        public static SqlConnection oConn;
        public static bool bCheckAllowInit;

        /// <summary>
        /// SerialModem class constructor
        /// </summary>
        /// <param name="_sModemPort">string : Modem port name</param>
        public SerialModem(string _sModemPort)
        {
            sModemPort = _sModemPort;

            InitModem();
            if (oSerialPort.IsOpen)
            {
                InitConfigModem();
                oSerialPort.DataReceived += new SerialDataReceivedEventHandler(this.oSerialPort_DataReceived);
            }
        }

        public void Open()
        {
            oSerialPort.Open();
        }

        public void Close()
        {
            oSerialPort.Close();
        }

        /// <summary>
        /// Modem Serialport Deconstructor, close the port and dispose it.
        /// </summary>
        public void Dispose()
        {
            if (oSerialPort.IsOpen) oSerialPort.Close();
            oSerialPort.Dispose();
        }

        /// <summary>
        /// Init the serialport modem configuration
        /// </summary>
        protected void InitModem()
        {
            oSerialPort = new SerialPort(sModemPort);

            oSerialPort.PortName = sModemPort;
            //oSerialPort.BaudRate = 19200;
            oSerialPort.BaudRate = 9600;
            //oSerialPort.BaudRate = 2400;
            oSerialPort.StopBits = StopBits.One;
            oSerialPort.Parity = Parity.None;
            oSerialPort.DataBits = 8;
            oSerialPort.Handshake = Handshake.None;
            oSerialPort.ReadBufferSize = 1024;
            //oSerialPort.ReceivedBytesThreshold = 1024;

            oSerialPort.Open();

            Thread.Sleep(100);
            if (oSerialPort.IsOpen)
            {
                oSerialPort.DiscardInBuffer();
                oSerialPort.DiscardOutBuffer();

                oSerialPort.DtrEnable = true;
                oSerialPort.RtsEnable = true;
            }
        }

        /// <summary>
        /// Init the serialport modem AT command configuration.
        /// </summary>
        protected void InitConfigModem()
        {
            //oSerialPort.Write("ATE0B0&C0&D0&G0S0=1S10=50\r");

            //oSerialPort.Write("ATW1\r");
            //oSerialPort.Write("AT&FS0=1L=1E0&K0");

            oSerialPort.Write("AT&FS0=1L=1E0\r");

            // US Robotics
            //oSerialPort.Write("AT&M0\r");

            //oSerialPort.Write("AT&W0");
        }

        /// <summary>
        /// Serialport modem data received event.
        /// </summary>
        protected void oSerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(500);
            if (oSerialPort.BytesToRead > 0)
            {
                int iBytesToRead = oSerialPort.BytesToRead;
                byte[] arrbRead = new byte[iBytesToRead];
                oSerialPort.Read(arrbRead, 0, iBytesToRead);

                string sProcCodeResponse = "990001";
                bool isReceiveEnd = false;
                int iCountDelay = 1;
                while (sProcCodeResponse == "990001" && !isReceiveEnd)
                {
                    string sAppName = null;
                    Trace.Write("|[MODEM]|Receive : " + CommonLib.sByteArrayToString(arrbRead));
                    byte[] arrbSend = arrbGenResponse(arrbRead, ref sAppName);

                    if (arrbSend != null)
                    {
                        oSerialPort.Write(arrbSend, 0, arrbSend.Length);
                        Trace.Write("|[MODEM]|Send : " + CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", ""));

                        //sProcCodeResponse = CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", "").Substring(34, 6);
                        sProcCodeResponse = CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", "").Substring(4, 6);

                        Thread.Sleep(1000);

                        if (iCountDelay % 2 == 0) Thread.Sleep(1000);

                        if (sProcCodeResponse == "990000")
                            isReceiveEnd = true;
                        if (!isReceiveEnd)
                        {
                            arrbRead[17] = 1;
                            // update counter + 1
                            UpdateCounterAppName(ref arrbRead, sAppName);
                        }
                    }
                    else
                        isReceiveEnd = true;
                    iCountDelay++;
                }
            }
        }

        protected byte[] arrbGenResponse(byte[] _arrbReceive, ref string sAppName)
        {
            int iCount;
            byte[] arrbResponse = null;
            for (iCount = 0; iCount < _arrbReceive.Length; iCount++)
            {
                if (_arrbReceive[iCount] == 0x0A && (iCount + 1) < _arrbReceive.Length)
                {
                    if (_arrbReceive[iCount + 1] == 0x60)
                    {
                        byte[] arrbRead = new byte[_arrbReceive.Length - iCount - 1];
                        for (int i = 0; i < arrbRead.Length && iCount < _arrbReceive.Length; i++)
                        {
                            arrbRead[i] = _arrbReceive[iCount + 1];
                            iCount++;
                        }

                        arrbResponse = Response.arrbResponse(arrbRead, ConnType.Modem, oConn, bCheckAllowInit, ref sAppName);
                        break;
                    }
                }
                else if (_arrbReceive[iCount] == 0x60 && _arrbReceive.Length > 8)
                {
                    if (_arrbReceive[iCount + 5] == 0x8 && _arrbReceive[iCount + 6] == 0x0)
                    {
                        arrbResponse = Response.arrbResponse(_arrbReceive, ConnType.Modem, oConn, bCheckAllowInit, ref sAppName);
                        break;
                    }
                }
            }
            //if (arrbResponse != null)
            //    Trace.Write("[MODEM] Response : \n" + CommonLib.sByteArrayToHexString(arrbResponse));
            return arrbResponse;
        }

        private static void UpdateCounterAppName(ref byte[] _arrbRecv, string sAppName)
        {
            byte[] arrbMsg;
            int iCounter;
            // buang total length & tpdu, 3 byte
            arrbMsg = new byte[_arrbRecv.Length];
            Array.Copy(_arrbRecv, arrbMsg, _arrbRecv.Length);

            ISO8583Lib oIso = new ISO8583Lib();
            ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
            oIso.UnPackISO(arrbMsg, ref oIsoMsg);

            string sBit57 = CommonLib.sStringToHex(oIsoMsg.sBit[57]);
            iCounter = int.Parse(sBit57.Substring(0, 4));
            iCounter = iCounter + 1;

            string sBit57New = string.Format("{0:0000}{1:ddMMyyHHmm}{2:0000}{3}",
                iCounter, DateTime.Now, sAppName.Length, CommonLib.sStringToHex(sAppName));
            oIso.SetField_Str(57, ref oIsoMsg, CommonLib.sHexToStringUTF7(sBit57New));

            // TCP/IP mode
            //byte[] arrbRecvTemp = oIso.PackToISO(oIsoMsg);
            //byte[] arrbLength = CommonLib.HexStringToByteArray(
            //    string.Format("{0:0000}", int.Parse(CommonLib.sConvertDecToHex(arrbRecvTemp.Length))));
            //_arrbRecv = new byte[arrbLength.Length + arrbRecvTemp.Length];
            //Array.Copy(arrbLength, 0, _arrbRecv, 0, 2);
            //Array.Copy(arrbRecvTemp, 0, _arrbRecv, 2, arrbRecvTemp.Length);

            // SerialModem Mode
            _arrbRecv = oIso.PackToISO(oIsoMsg);
        }
    }
}
