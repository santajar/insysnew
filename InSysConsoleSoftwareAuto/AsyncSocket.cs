using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Data.SqlClient;
using InSysClass;

namespace InSysConsoleSoftwareAuto
{
    // State object for reading client data asynchronously
    public class StateObject
    {
        // Client  socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 1024;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }
    
    public class AsyncSocket
    {
        #region "TCP/IP"
        // Thread signal.
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        public static int iPort;
        public static SqlConnection oConn;
        public static bool bCheckAllowInit;
        public static int iConnMax;
        protected static int iConnSum = 0;

        /// <summary>
        /// 
        /// </summary>
        public static void StartListening()
        {
            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.
            // The DNS name of the computer
            // running the listener is "host.contoso.com".
            IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, iPort);
            //IPEndPoint localEndPoint = new IPEndPoint((IPAddress)((IPHostEntry)(Dns.Resolve("localhost"))).AddressList[0], iPort);
            //IPEndPoint localEndPoint = new IPEndPoint((IPAddress)((IPHostEntry)(Dns.Resolve("192.168.1.90"))).AddressList[0], iPort);

            // Create a TCP/IP socket.
            Socket listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(150);

                while (true)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    //Console.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);

                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }

            }
            catch (Exception e)
            {
                //Logs.doWriteErrorFile("[TCP] StartListen Error : " + e.ToString());
            }
        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            //CommonConsole.WriteToConsole("Waiting for incoming Initialize...");
            // Signal the main thread to continue.
            allDone.Set();

            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            CommonConsole.WriteToConsole(string.Format("Accepted connection from {0}:{1}\nReady to receive initialize...",
                ((IPEndPoint)handler.RemoteEndPoint).Address, ((IPEndPoint)handler.RemoteEndPoint).Port));

            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }

        public static void ReadCallback(IAsyncResult ar)
        {
            try
            {
                String content = String.Empty;

                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket handler = state.workSocket;

                // Read data from the client socket. 
                //int bytesRead = handler.EndReceive(ar);

                // below updated on June 19, 2012.
                SocketError errorCode;
                int bytesRead = handler.EndReceive(ar, out errorCode);
                if (errorCode != SocketError.Success)
                    bytesRead = 0;

                if (bytesRead > 0)
                {
                    //if (iConnSum <= iConnMax)
                    //{
                        iConnSum++;
                        Logs.doWriteErrorFile("|Receive|iConnSum : " + iConnSum.ToString());
                        // There  might be more data, so store the data received so far.
                        state.sb.Append(Encoding.ASCII.GetString(
                            state.buffer, 0, bytesRead));

                        // Check for end-of-file tag. If it is not there, read 
                        // more data.
                        content = state.sb.ToString();

                        // Not all data received. Get more.
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                            new AsyncCallback(ReadCallback), state);

                        #region "Debug Receive Message"
                        // Start the Iso message processing
                        byte[] arrbDebug = new byte[256];
                        Array.Copy(state.buffer, arrbDebug, 256);
                        Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|",
                            iConnSum,
                            CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ",""))); //debug
                        #endregion

                        //byte[] arrbReceive = new byte[StateObject.BufferSize];
                        byte[] arrbReceive = new byte[state.buffer.Length];
                        arrbReceive = state.buffer;

                        #region "AutoSendResponse"
                        string sProcCodeResponse = "990001";
                        bool isReceiveEnd = false;
                        int iCountDelay = 1;
                        while (sProcCodeResponse == "990001" && !isReceiveEnd)
                        {
                            string sAppName = null;
                            byte[] arrbSend = Response.arrbResponse(arrbReceive, ConnType.TcpIP, oConn, bCheckAllowInit, ref sAppName);
                            if (arrbSend != null && arrbSend[1] != 0)
                            {
                                #region "Debug Send Message"
                                Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : {2} ",
                                    iConnSum,
                                    CommonLib.sByteArrayToHexString(arrbReceive).Replace(" ", ""),
                                    CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", ""))); //debug
                                #endregion

                                // Send the response
                                if ((handler.Poll(-1, SelectMode.SelectWrite) && handler.Available == 0))
                                    Send(handler, arrbSend);
                                else
                                {
                                    handler.Disconnect(true);
                                    break;
                                }
                            }
                            else
                                Trace.Write(string.Format("|iConnSum : {0}|Recv : {1}|Sent : NULL",
                                    iConnSum,
                                    CommonLib.sByteArrayToHexString(arrbDebug).Replace(" ", ""))); //debug

                            //sProcCodeResponse = CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", "").Substring(34, 6);
                            sProcCodeResponse = CommonLib.sByteArrayToHexString(arrbSend).Replace(" ", "").Substring(4, 6);

                            Thread.Sleep(300);

                            //if (iCountDelay % 2 == 0) Thread.Sleep(500);

                            if (sProcCodeResponse == "990000")
                                isReceiveEnd = true;
                            if (!isReceiveEnd)
                            {
                                // change proccode to 990001
                                arrbReceive[19] = 1;
                                // update counter + 1
                                UpdateCounterAppName(ref arrbReceive, sAppName);
                            }
                        }
                        #endregion

                        //iConnSum--;
                        Logs.doWriteErrorFile("|Send|iConnSum : " + iConnSum.ToString());
                    //}
                }
            }
            catch (Exception ex)
            {
                //Logs.doWriteErrorFile("[TCP] ReadCallBack Error : " + ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }

        private static void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void Send(Socket handler, byte[] byteData)
        {
            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
                //Console.WriteLine("Sent {0} bytes to client.", bytesSent);
                handler.ReceiveBufferSize = 0;
                handler.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer, 0);
            }
            catch (Exception e)
            {
                //Logs.doWriteErrorFile("[TCP] SendCallback Error : " + e.ToString());
                //Console.WriteLine(e.ToString());
            }
        }

        private static void UpdateCounterAppName(ref byte[] _arrbRecv, string sAppName)
        {
            byte[] arrbMsg;
            int iCounter;
            // buang total length & tpdu, 3 byte
            arrbMsg = new byte[_arrbRecv.Length - 2];
            Array.Copy(_arrbRecv, 2, arrbMsg, 0, arrbMsg.Length);
            
            ISO8583Lib oIso = new ISO8583Lib();
            ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
            oIso.UnPackISO(arrbMsg, ref oIsoMsg);

            string sBit57 = CommonLib.sStringToHex(oIsoMsg.sBit[57]);
            iCounter = int.Parse(sBit57.Substring(0, 4));
            iCounter = iCounter + 1;

            string sBit57New = string.Format("{0:0000}{1:ddMMyyHHmm}{2:0000}{3}", 
                iCounter, DateTime.Now, sAppName.Length, CommonLib.sStringToHex(sAppName));
            oIso.SetField_Str(57, ref oIsoMsg, CommonLib.sHexToStringUTF7(sBit57New));

            byte[] arrbRecvTemp = oIso.PackToISO(oIsoMsg);
            byte[] arrbLength = CommonLib.HexStringToByteArray(
                string.Format("{0:0000}", int.Parse(CommonLib.sConvertDecToHex(arrbRecvTemp.Length))));
            _arrbRecv = new byte[arrbLength.Length + arrbRecvTemp.Length];
            Array.Copy(arrbLength, 0, _arrbRecv, 0, 2);
            Array.Copy(arrbRecvTemp, 0, _arrbRecv, 2, arrbRecvTemp.Length);
        }
        #endregion
    }
}
