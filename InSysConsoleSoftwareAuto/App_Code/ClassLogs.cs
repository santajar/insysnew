using System;
using System.IO;
using InSysClass;

namespace InSysConsoleSoftwareAuto
{
    class Logs
    {
        static string sDirectory = Environment.CurrentDirectory;
        protected static string sGetErrorDirectory()
        {
            return sDirectory + "\\LOGS";
        }

        protected static bool isHaveLogDir()
        {
            if (Directory.Exists(sGetErrorDirectory()))
                return true;
            else
                return false;
        }

        public static void doWriteErrorFile(string sError)
        {
            doWriteErrorFile(sGetErrorDirectory(), sError);
        }

        public static void doWriteErrorFile(string sDirectory, string sError)
        {
            if (!isHaveLogDir())
                Directory.CreateDirectory(sGetErrorDirectory());
            CommonLib.Log(sDirectory, sError);
        }
    }
}
