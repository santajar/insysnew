﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using InSysClass;

namespace LicenseRegistrator
{
    public partial class frmLicenseRegistrator : Form
    {
        public frmLicenseRegistrator()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCompanyName.Text) && !string.IsNullOrEmpty(rtbOutput.Text))
            {
                string sCompanyName = txtCompanyName.Text;
                string sRegKey = EncryptionLib.Decrypt3DES(CommonLib.sHexToStringUTF8(rtbOutput.Text));

                int iValidPeriod = int.Parse(sRegKey.Substring(0, 10));
                string sCompanyNameReg = sRegKey.Substring(10, 50).TrimStart(' ');
                string sUnlimited = sRegKey.Substring(60, 1);
                
                //MessageBox.Show(string.Format("Valid Period : {0}\nCompany Name : {1}\nUnlimited : {2}",
                //    iValidPeriod,
                //    sCompanyNameReg,
                //    sUnlimited == "0" ? "FAlSE" : "TRUE"));
                if (sCompanyName == sCompanyNameReg)
                {
                    LicenseKeepAliveCounter oLicense = new LicenseKeepAliveCounter();
                    oLicense.Unlimited = sUnlimited == "1" ? true : false;
                    oLicense.CounterRemain = iValidPeriod;
                    oLicense.RegisterLicense();
                }
                else
                    MessageBox.Show("Incorrect Registration key.\nPlease check the Company Name & Registration Key");
            }
            else
                MessageBox.Show("Please fill the Company Name & Registration Key");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
