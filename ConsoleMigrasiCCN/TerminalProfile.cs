﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleMigrasiCCN
{
    public class Terminal
    {
        protected string sTerminalID;
        protected string sCategory;
        protected int iDatabaseID;

        public string TerminalID { get { return sTerminalID; } set { sTerminalID = value; } }
        public string Category { get { return sCategory; } set { sCategory = value; } }
        public int DatabaseID { get { return iDatabaseID; } set { iDatabaseID = value; } }
        public List<Acquirer> ListAcquirer = new List<Acquirer>();
        public List<Issuer> ListIssuer = new List<Issuer>();
    }

    public class Acquirer
    {
        protected string sAcquirerName;        
        public string Name { get { return sAcquirerName; } set { sAcquirerName = value; } }
        //public List<Issuer> IssuerList = new List<Issuer>();
    }

    public class Issuer
    {
        protected string sIssuerName;
        protected string sPaymentCoBrandCode;
        public string Name { get { return sIssuerName; } set { sIssuerName = value; } }
        public string PaymentCoBrandCode { get { return sPaymentCoBrandCode; } set { sPaymentCoBrandCode = value; } }
        //public List<Card> CardList = new List<Card>();
    }

    public class Card
    {
        protected string sCardName;
        public string Name { get { return sCardName; } set { sCardName = value; } }
    }

    public class MasterProfile
    {
        protected string sSource;
        protected string sTarget;
        protected string[] arrsValue;
        protected string[] arrsTag;
        protected string[] arrsCondition;

        public string Source { get { return sSource; } set { sSource = value; } }
        public string Target { get { return sTarget; } set { sTarget = value; } }
        public string[] Value { get { return arrsValue; } set { arrsValue = value; } }
        public string[] Tag { get { return arrsTag; } set { arrsTag = value; } }
        public string[] Condition { get { return arrsCondition; } set { arrsCondition = value; } }

        public MasterProfile(string _sSource,
            string _sTarget,
            string[] _arrsTag,
            string[] _arrsValue)
            : this(_sSource, _sTarget, _arrsTag, _arrsValue, null) { }

        public MasterProfile(string _sSource,
            string _sTarget,
            string[] _arrsTag,
            string[] _arrsValue,
            string[] _arrsCondition)
        {
            sSource = _sSource;
            sTarget = _sTarget;
            arrsTag = _arrsTag;
            arrsValue = _arrsValue;
            arrsCondition = _arrsCondition;
        }
    }
}
