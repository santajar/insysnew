﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Globalization;
using InSysClass;
using Ini;

namespace ConsoleMigrasiCCN
{
    class Program
    {
        static string sSqlConnString = null;
        static int iTotalProcess = 0;
        static string[] arrsSourceDatabase = new string[10];
        static string[] arrsFilterMaster = new string[20];
        static string sTargetDatabaseName = null;
        static SqlConnection oSqlConn;
        static List<Terminal> listMasterSource = new List<Terminal>();
        static List<MasterProfile> listMasterProfileTarget = new List<MasterProfile>();
        static bool bUpdate;
        DataTable dtTerminalProfile = new DataTable();
        DataTable dtTempMaster = new DataTable();
        //DataSet dsTables;
        static string sDatabaseIDSource = null;
        static string sDatabaseIDTarget = "0";
        static string sSBIDTarget = "75";
        static DataTable dtTerminalMasterListSource;

        static void Main(string[] args)
        {
            
            Init();
            if (!string.IsNullOrEmpty(sSqlConnString))
            {
                try
                {
                    oSqlConn = new SqlConnection(sSqlConnString);
                    oSqlConn.Open();
                    sDatabaseIDTarget = sGetDatabaseID(sTargetDatabaseName);
                    
                    foreach (string sDbName in arrsSourceDatabase)
                        sDatabaseIDSource = string.IsNullOrEmpty(sDatabaseIDSource) ? string.Format("{0}", sGetDatabaseID(sDbName)) : string.Format("{0},{1}", sDatabaseIDSource, sGetDatabaseID(sDbName));

                    Display("Defining MASTER...");
                    //DefineMasterProfile();
                    DataTable dtAllMasterProfile = GetAllMasterProfile();
                    DataTable dtMasterCount = CreateFilterOfMaster(dtAllMasterProfile);
                    //DataTable dtMasterCount = CreateFilterOfMasterinSQL();
                    Display("Defining MASTER... DONE");

                    Display(string.Format("Populating {0} Profile to process", iTotalProcess));

                    //use 2 database
                    //DataTable dtTerminal = dtGetTerminalList(string.Format("WHERE DATABASEID IN ({0},{1}) AND STATUSMASTER=0",
                    //   sGetDatabaseID(arrsSourceDatabase[0]), sGetDatabaseID(arrsSourceDatabase[1])));
                    //use 1 database
                    //DataTable dtTerminal = dtGetTerminalList(string.Format("WHERE DATABASEID IN ({0}) AND STATUSMASTER=0",
                    //   sGetDatabaseID(arrsSourceDatabase[0])));
                    
                    DataTable dtTerminal = dtGetTerminalList(string.Format("WHERE DATABASEID IN ({0}) AND STATUSMASTER=0", sDatabaseIDSource));
                    if (iTotalProcess > dtTerminal.Select().Length)
                        iTotalProcess = dtTerminal.Select().Length;
                    Display("Populating DONE");
                    int iCountSuccesTID = 0;
                    int iCountConnectionLostTID = 0;
                    int iCountErrorTID = 0;
                    int iCountTIDNotAccess = 0;
                    for (int i = 0; i < iTotalProcess; i++)
                    {
                        //CultureInfo USFormat = new CultureInfo("en-US");
                        //DateTime dtTimeNow = DateTime.Parse(DateTime.Now.ToString("hh:mm:ss tt", USFormat));
                        DateTime dtTimeNow = DateTime.Now;
                        DateTime dtStartMigrasi = DateTime.Parse("20:00:00.000");
                        DateTime dtEndMigrasi = DateTime.Parse("07:30:00.000");
                        //Console.WriteLine(dtTimeNow);
                        //Console.WriteLine(dtStartMigrasi);
                        //Console.WriteLine(dtEndMigrasi);

                        //if (dtTimeNow >= dtStartMigrasi || dtTimeNow < dtEndMigrasi)
                        //{
                            string sError = null;
                            string sDatabaseID = dtTerminal.Rows[i]["DatabaseID"].ToString();
                            string sTerminalId = dtTerminal.Rows[i]["TerminalID"].ToString();
                            
                            string sNewTerminalID;
                            do
                                sNewTerminalID = bUpdate ? sTerminalId : sRandomTerminalID();

                            while (!bUpdate && IsTerminalIdValid(sNewTerminalID));
                            Display(string.Format("Starting TerminalID : {0}", sTerminalId));

                            if (isAllowAccesTID(sTerminalId))
                            {
                                insertUserAccess(sTerminalId, "GEMALTO");
                                DataTable dtProfile = new DataTable();
                                try
                                {
                                    WriteLogTagInsert("Start spTextFullTable");
                                    dtProfile = dtGetProfileTable(sTerminalId);
                                    WriteLogTagInsert("Done spTextFullTable");
                                }
                                catch (Exception ex)
                                {
                                    Display(string.Format("ERROR. {0}", ex.Message));
                                }
                                //use APP
                                if (dtProfile.Rows.Count != 0)
                                {
                                    //Display(string.Format("Start Backup TID : {0}", sTerminalId));
                                    WriteLogTagInsert("Start BackUp");
                                    BackUpPerTID(sTerminalId, dtProfile);
                                    WriteLogTagInsert("Done BackUp");
                                    //Display(string.Format("Backup TID : {0} Done", sTerminalId));
                                    
                                    string sSelectedMaster = "";
                                    if (isProfileValid(dtProfile, sTerminalId))
                                    {
                                        try
                                        {
                                            WriteLogTagInsert("Searching Master");
                                            sSelectedMaster = sSearchMasterTerminal(sDatabaseID, sTerminalId, dtProfile, dtMasterCount, dtAllMasterProfile, ref sError);
                                            WriteLogTagInsert("Searching done");
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine("ERROR. {0}", ex.Message);
                                        }
                                    }
                                    else
                                    {
                                        Display(string.Format(" Master of {0} not found. Problem Data", sTerminalId));
                                    }
                                    
                                    //Use SQL
                                    //string sSelectedMaster = sSearchMasterTerminal(sTerminalId, ref sError);
                                    if (!string.IsNullOrEmpty(sSelectedMaster))
                                    {
                                        Display(string.Format("{0}, Master FOUND : {1}", sTerminalId, sSelectedMaster));
                                        string sMasterCCN = GetMasterCCN(sSelectedMaster);
                                        if (!string.IsNullOrEmpty(sMasterCCN))
                                        {
                                            Display(string.Format(" Moving Process.. TID:{0}, Master:{1}, MasterCCN:{2}, NewTID: {3} ", sTerminalId, sSelectedMaster, sMasterCCN, sNewTerminalID));
                                            if (IsMasterCCNExist(sMasterCCN))
                                            {
                                                if (isProfileValid(dtProfile, sTerminalId))
                                                {
                                                    try
                                                    {
                                                        //Use App
                                                        WriteLogTagInsert("Start Moving");
                                                        StartCopyorMoveTID(sNewTerminalID, sTerminalId, sMasterCCN, sDatabaseID, sSBIDTarget, dtProfile, dtAllMasterProfile, bUpdate ? 1 : 0);

                                                        //use SQL
                                                        //ExecuteMovingTerminalID(sNewTerminalID, sTerminalId, sMasterCCN, sDatabaseID);
                                                        Display(string.Format(" Moving TerminalID : {0} Done", sTerminalId));
                                                        iCountSuccesTID = iCountSuccesTID + 1;
                                                        WriteLogTagInsert("Done Moving");
                                                        DeleteUserAccess(sTerminalId, "GEMALTO");
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        iCountErrorTID = iCountErrorTID + 1;
                                                        Console.WriteLine("ERROR. {0}", ex.Message);
                                                        DeleteUserAccess(sTerminalId, "GEMALTO");
                                                    }
                                                }else
                                                {
                                                    Display(string.Format("Error Data Profile {0}.", sTerminalId));
                                                    iCountErrorTID = iCountErrorTID + 1;
                                                }
                                            }
                                            else
                                            {
                                                iCountErrorTID = iCountErrorTID + 1;
                                                Display(string.Format("Master {0} Not Found", sMasterCCN));
                                                DeleteUserAccess(sTerminalId, "GEMALTO");
                                            }
                                        }
                                        else
                                        {
                                            iCountErrorTID = iCountErrorTID + 1;
                                            Display(string.Format("{0} can not move", sTerminalId));
                                            DeleteUserAccess(sTerminalId, "GEMALTO");
                                        }

                                    }
                                    else
                                    {
                                        string sMasterCCN = "";
                                        GetMasterOfNotFoundMaster(dtProfile, ref sMasterCCN);
                                        if (!string.IsNullOrEmpty(sMasterCCN) || sMasterCCN.Length == 8)
                                        {
                                            if (isProfileValid(dtProfile, sTerminalId))
                                            {
                                                try
                                                {
                                                    Display(string.Format(" Moving Default Process.. TID:{0}, MasterCCN:{1}, NewTID: {2} ", sTerminalId, sMasterCCN, sNewTerminalID));
                                                    
                                                    WriteLogTagInsert("Start Moving");
                                                    StartCopyorMoveTID(sNewTerminalID, sTerminalId, sMasterCCN, sDatabaseID, sSBIDTarget, dtProfile, dtAllMasterProfile, bUpdate ? 1 : 0);
                                                    WriteLogTagInsert("Done Move");
                                                    
                                                    Display(string.Format(" Moving {0} Done", sTerminalId));
                                                    
                                                    iCountSuccesTID = iCountSuccesTID + 1;
                                                    DeleteUserAccess(sTerminalId, "GEMALTO");
                                                }
                                                catch (Exception ex)
                                                {
                                                    iCountErrorTID = iCountErrorTID + 1;
                                                    Console.WriteLine("ERROR. {0}", ex.Message);
                                                    DeleteUserAccess(sTerminalId, "GEMALTO");
                                                }
                                            }
                                            else
                                            {
                                                Display(string.Format("Error Data Profile {0}. Use Default.", sTerminalId));
                                                iCountErrorTID = iCountErrorTID + 1;
                                            }
                                        }
                                        else
                                        {
                                            iCountErrorTID = iCountErrorTID + 1;
                                            Display(string.Format("{0}, Master NOT FOUND. {1}", sTerminalId, sError));
                                            DeleteUserAccess(sTerminalId, "GEMALTO");
                                        }
                                    }
                                }
                                else
                                {
                                    iCountConnectionLostTID = iCountConnectionLostTID + 1;
                                    Display(string.Format("{0} Have no Data or connection to long", sTerminalId));
                                    DeleteUserAccess(sTerminalId, "GEMALTO");
                                }
                            }
                            else
                            {
                                iCountTIDNotAccess = iCountTIDNotAccess + 1;
                                Display(string.Format("{0} Browse by Another Process", sTerminalId));
                            }
                        //}
                        //else
                        //    break;
                      }
                    Display(string.Format("Total Succes TID : {0}", iCountSuccesTID));
                    Display(string.Format("Total TID Not Allow Acces : {0} ", iCountTIDNotAccess));
                    Display(string.Format("Total Connection Failed TID : {0} ", iCountConnectionLostTID));
                    Display(string.Format("Total Problem TID : {0}", iCountErrorTID));
                    Display(string.Format("ALL Process Done"));
                    //use SQL
                    //DropFilterOfMasterinSQL();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("ERROR. {0}", ex.Message));
                }
            }
            else
                Console.WriteLine("ERROR. Empty DATABASE connection.");
            Console.Read();
        }

        private static bool isProfileValid(DataTable dtProfile, string sTerminalId)
        {
            if (dtProfile.Select(String.Format("Tag like 'DE%'")).Length > 0)
            {
                if (dtProfile.Select(String.Format("Tag like 'AA%'")).Length > 0)
                {
                    if (dtProfile.Select(String.Format("Tag like 'AE%'")).Length > 0)
                    {
                        if (dtProfile.Select(String.Format("Tag like 'AD%'")).Length > 0)
                        {
                            return true;
                        }
                        else { Display(string.Format("TerminalID {0} : Error Data Relation", sTerminalId)); return false; }
                    }
                    else { Display(string.Format("TerminalID {0} : Error Data Issuer", sTerminalId)); return false; }
                }
                else { Display(string.Format("TerminalID {0} : Error Data Acquirer", sTerminalId)); return false; }
            }
            else { Display(string.Format("TerminalID {0} : Error Data Terminal", sTerminalId)); return false; }
        }

        private static bool isAllowAccesTID(string sTerminalId)
        {
            DataTable dtTempUser = new DataTable();
            string sQuery = string.Format("SELECT TerminalID,UserId FROM tbUserAccess WHERE TerminalID = '{0}'",sTerminalId);
            SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed)
                oSqlConn.Open();
            new SqlDataAdapter(oCmd).Fill(dtTempUser);

            if (dtTempUser.Rows.Count > 0)
                return false;
            else
                return true;
        }
        
        private static void DeleteUserAccess(string sTerminalId, string sUserID)
        {
            using (SqlCommand oCmd = new SqlCommand("spUserAccessDelete", oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = sUserID;
                if (oSqlConn.State == ConnectionState.Closed)
                    oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }

        private static void insertUserAccess(string sTerminalId, string sUserID)
        {
            using (SqlCommand oCmd = new SqlCommand("spUserAccessInsert", oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = sUserID;
                if (oSqlConn.State == ConnectionState.Closed)
                    oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }

        private static void BackUpPerTID(string sTerminalId, DataTable dtProfile)
        {
            string sFolderPath = string.Format(@"{0}\BackUpTID{1}", Environment.CurrentDirectory, DateTime.Now.ToString("yyMMdd"));
            if (!Directory.Exists(sFolderPath)) Directory.CreateDirectory(sFolderPath);
            string sFileName = null;
            string sContent = null;

            foreach (DataRow drProfile in dtProfile.Rows)
            {
                string sTag = drProfile["Tag"].ToString();
                string sTagValue = drProfile["TagValue"].ToString();
                string sLen = sTagValue.Length.ToString();

                if (sLen.Length == 1)
                    sLen = "0" + sLen;
                else if (sLen.Length == 0)
                    sLen = "00";

                sContent = sContent + sTag + sLen + sTagValue;
            }

            sFileName = sFolderPath + "\\" + sTerminalId + ".txt";
            File.Delete(sFileName);
            CommonLib.Write2File(sFileName, sContent, false);
        }

        private static string GetMasterOfNotFoundMaster(DataTable dtProfile, ref string sMasterCCN)
        {
            int iCountBCA, iCountBCA1, iCountBCA2, iCountDebit, iCountFlazz, iCountLoyalty, iCountPromo;

            iCountBCA = dtProfile.Select("Tag = 'AA01' AND TagValue = 'BCA'").Length;
            iCountBCA1 = dtProfile.Select("Tag = 'AA01' AND TagValue = 'BCA1'").Length;
            iCountBCA2 = dtProfile.Select("Tag = 'AA01' AND TagValue = 'BCA2'").Length;
            iCountDebit = dtProfile.Select("Tag = 'AA01' AND TagValue = 'DEBIT'").Length;
            iCountFlazz = dtProfile.Select("Tag = 'AA01' AND TagValue = 'FLAZZBCA'").Length;
            iCountLoyalty = dtProfile.Select("Tag = 'AA01' AND TagValue = 'LOYALTY'").Length;
            iCountPromo = dtProfile.Select("Tag = 'AA01' AND TagValue LIKE 'PROMO%'").Length;

            if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty == 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNRET02";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz == 0 && iCountLoyalty == 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNRET02";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit == 0 && iCountFlazz == 0 && iCountLoyalty == 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNRET02";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty > 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNRET07";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty > 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNRET09";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty == 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNRET04";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz == 0 && iCountLoyalty == 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNRET04";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit == 0 && iCountFlazz == 0 && iCountLoyalty == 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNRET04";
            }
            else if (iCountBCA == 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit == 0 && iCountFlazz > 0 && iCountLoyalty > 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNRET07";
            }
            else if (iCountBCA == 0 && iCountBCA1 > 0 && iCountBCA2 > 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty == 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNONOF2";
            }
            else if (iCountBCA == 0 && iCountBCA1 > 0 && iCountBCA2 > 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty == 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNONOF4";
            }
            else if (iCountBCA == 0 && iCountBCA1 == 0 && iCountBCA2 > 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty > 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNONOF3";
            }
            else if (iCountBCA == 0 && iCountBCA1 > 0 && iCountBCA2 > 0 && iCountDebit > 0 && iCountFlazz == 0 && iCountLoyalty == 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNONOF4";
            }
            else if (iCountBCA == 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz == 0 && iCountLoyalty == 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNRET04";
            }
            else if (iCountBCA == 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty == 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNRET04";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz == 0 && iCountLoyalty > 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNRET09";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz == 0 && iCountLoyalty > 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNRET07";
            }
            else if (iCountBCA > 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit == 0 && iCountFlazz == 0 && iCountLoyalty > 0 && iCountPromo == 0)
            {
                sMasterCCN = "CCNRET09";
            }
            else if (iCountBCA == 0 && iCountBCA1 > 0 && iCountBCA2 > 0 && iCountDebit > 0 && iCountFlazz == 0 && iCountLoyalty == 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNONOF2";
            }
            else if (iCountBCA == 0 && iCountBCA1 == 0 && iCountBCA2 > 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty > 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNONOF1";
            }
            else if (iCountBCA == 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz == 0 && iCountLoyalty == 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNRET04";
            }
            else if (iCountBCA == 0 && iCountBCA1 == 0 && iCountBCA2 == 0 && iCountDebit > 0 && iCountFlazz > 0 && iCountLoyalty == 0 && iCountPromo > 0)
            {
                sMasterCCN = "CCNRET04";
            }

            else
            {   
                sMasterCCN = ""; 
            }

            return sMasterCCN;
        }

        private static DataTable CreateFilterOfMaster(DataTable dtAllMasterProfile)
        {
            DataTable dtTempMasterCount = new DataTable();
            DataColumn colID = new DataColumn("ID", typeof(int));
            colID.AutoIncrement = true;
            dtTempMasterCount.Columns.Add(colID);

            dtTempMasterCount.Columns.Add("DatabaseID", typeof(int));
            dtTempMasterCount.Columns.Add("MasterTerminalID", typeof(string));
            dtTempMasterCount.Columns.Add("Category", typeof(string));
            dtTempMasterCount.Columns.Add("TotalAcquirer", typeof(int));
            dtTempMasterCount.Columns.Add("TotalIssuer", typeof(int));

            DataRow drTempMasterCount = dtTempMasterCount.NewRow();
            int iDatabaseID;
            int iTotalAcquirer = 0;
            int iTotalIssuer = 0;
            string sCategory = "";

            foreach (string sDatabaseName in arrsSourceDatabase)
            {
                iDatabaseID = int.Parse(sGetDatabaseID(sDatabaseName));
                DataTable dtTerminalMasterListSource = dtGetTerminalList(string.Format("WHERE DatabaseID={0} AND StatusMaster=1", iDatabaseID));

                if (dtTerminalMasterListSource != null && dtTerminalMasterListSource.Rows.Count > 0)
                {
                    foreach (DataRow rowTerminalMaster in dtTerminalMasterListSource.Rows)
                    {
                        iDatabaseID = int.Parse(rowTerminalMaster["DatabaseID"].ToString());
                        string sTerminalID = rowTerminalMaster["TerminalID"].ToString();
                        
                        sCategory = (dtAllMasterProfile.Select(string.Format("TerminalId = '{0}' And Tag ='DE05'", sTerminalID)))[0]["TagValue"].ToString();

                        iTotalAcquirer = dtAllMasterProfile.Select(string.Format("TerminalId = '{0}' And Tag ='AA01' AND Name NOT LIKE 'PROMO SQ%' AND Name NOT IN ('PROMO F','PROMO G','PROMO H','PROMO I','PROMO J','AMEX','DINERS')", sTerminalID)).Length;
                        iTotalIssuer = dtAllMasterProfile.Select(string.Format("TerminalId = '{0}' And Tag ='AE01' AND Name NOT IN ('VISA SQ')", sTerminalID)).Length;

                        drTempMasterCount = dtTempMasterCount.NewRow();
                        drTempMasterCount["DatabaseID"] = iDatabaseID;
                        drTempMasterCount["MasterTerminalID"] = sTerminalID;
                        drTempMasterCount["Category"] = sCategory;
                        drTempMasterCount["TotalAcquirer"] = iTotalAcquirer;
                        drTempMasterCount["TotalIssuer"] = iTotalIssuer;
                        dtTempMasterCount.Rows.Add(drTempMasterCount);
                    }
                }
            }
            return dtTempMasterCount;
        }

        private static DataTable GetAllMasterProfile()
        {
            DataTable dtTempAllMasterProfile = new DataTable();
            DataTable dtTerminalMasterListSource = dtGetTerminalList(string.Format("WHERE DatabaseID IN ({0},{1}) AND StatusMaster=1", sDatabaseIDSource,sDatabaseIDTarget));
                DataTable dtTerminalMasterContent = new DataTable();
                if (dtTerminalMasterListSource != null && dtTerminalMasterListSource.Rows.Count > 0)
                {
                    foreach (DataRow rowTerminalMaster in dtTerminalMasterListSource.Rows)
                    {
                        string sTerminalID = rowTerminalMaster["TerminalID"].ToString();
                        dtTerminalMasterContent = new DataTable();
                        dtTerminalMasterContent = dtGetProfileTable(sTerminalID);

                        if (dtTempAllMasterProfile.Columns.Count == 0)
                            dtTempAllMasterProfile = dtTerminalMasterContent.Clone();
                        dtTerminalMasterContent.Select("")
                            .ToList<DataRow>()
                            .ForEach(MasterTerminal =>
                            {
                                dtTempAllMasterProfile.ImportRow(MasterTerminal);
                            });
                    }
                }

            return dtTempAllMasterProfile;
        }

        /*
         * NOTES :
         * 1. int bFunction, kalo cuma TRUE or FALSE, ga perlu di-convert ke integer
         * 2. 600 lines of code? next time, Be Efficient! Remember, MORE Efficient, LESS Headache.
         * 3. pisahin semua bagian yg punya kesamaan, mis : update -> update issuer, update acquirer, update terminal
         */
        private static void StartCopyorMoveTID(string sNewTerminalID, string sTerminalId, string sMasterCCN, string sDatabaseID, string sDatabaseIDTarget, DataTable dtProfile, DataTable dtProfileMaster, int bFunction)
        {
            try
            {
                //Display("Function");
                if (bFunction == 0)
                {
                    //update TerminalID and Tag DE01
                    dtProfile.Select(string.Format("[TerminalID] = '{0}'", sTerminalId))
                                    .ToList<DataRow>()
                                    .ForEach(Profile =>
                                    {
                                        Profile["TerminalID"] = sNewTerminalID;
                                    });
                    dtProfile.Select(string.Format("[Tag] = 'DE01'"))
                                    .ToList<DataRow>()
                                    .ForEach(Profile =>
                                    {
                                        Profile["TagValue"] = sNewTerminalID;
                                    });
                }
                else
                {
                    sNewTerminalID = sTerminalId;
                }
                //updateTag
                WriteLogTagInsert("UPDATE TAG");
                int iCountItemList = dtProfile.Select(String.Format("Tag= 'AA36' And ItemName ='Print Settle Receipt'")).Length;
                if (iCountItemList == 0)
                {
                    dtProfile.Select(string.Format("[Tag] = 'AA39'"))
                                .ToList<DataRow>()
                                .ForEach(Profile =>
                                {
                                    Profile["Tag"] = "AA40";
                                });
                    dtProfile.Select(string.Format("[Tag] = 'AA38'"))
                                .ToList<DataRow>()
                                .ForEach(Profile =>
                                {
                                    Profile["Tag"] = "AA39";
                                });
                    dtProfile.Select(string.Format("[Tag] = 'AA37'"))
                                .ToList<DataRow>()
                                .ForEach(Profile =>
                                {
                                    Profile["Tag"] = "AA38";
                                });
                    dtProfile.Select(string.Format("[Tag] = 'AA36'"))
                                .ToList<DataRow>()
                                .ForEach(Profile =>
                                {
                                    Profile["Tag"] = "AA37";
                                });
                }
                DataRow drProfile = dtProfile.NewRow();

                // BCA CASH AND CASH BCA
                WriteLogTagInsert("CASH BCA");
                int iCountTID = 0, iCountMaster = 0, iCountRowProfile = 0;
                string sTagUsebyTID = "";
                iCountTID = dtProfile.Select(String.Format("Tag ='AE01' AND Name = 'BCA CASH'")).Length;
                iCountMaster = dtProfileMaster.Select(String.Format("Tag ='AE01' AND Name = 'CASH BCA' AND TerminalId = '{0}'", sMasterCCN)).Length;
                iCountRowProfile = dtProfile.Select(String.Format("TerminalId = '{0}'", sNewTerminalID)).Length;
                if (iCountTID > 0 && iCountMaster > 0)
                {
                    sTagUsebyTID = "";

                    dtProfile.Select(string.Format("Name ='BCA CASH' AND Tag LIKE 'AE%'"))
                                           .ToList<DataRow>()
                                           .ForEach(Profile =>
                                               {
                                                   sTagUsebyTID = sTagUsebyTID + string.Format("'{0}',", Profile["Tag"].ToString());
                                               });

                    dtProfileMaster.Select(string.Format("Name ='CASH BCA' AND Tag Not in ({0}) AND TerminalId = '{1}'", sTagUsebyTID.Substring(0, sTagUsebyTID.Length - 1), sMasterCCN))
                                                  .ToList<DataRow>()
                                                  .ForEach(ProfileMaster =>
                                                  {
                                                      iCountRowProfile = iCountRowProfile + 1;
                                                      drProfile = dtProfile.NewRow();
                                                      drProfile["Id"] = iCountRowProfile;
                                                      drProfile["TerminalId"] = sNewTerminalID;
                                                      drProfile["Name"] = "BCA CASH";
                                                      drProfile["Tag"] = ProfileMaster["Tag"];
                                                      drProfile["ItemName"] = ProfileMaster["ItemName"];
                                                      drProfile["TagLength"] = ProfileMaster["TagLength"];
                                                      drProfile["TagValue"] = ProfileMaster["TagValue"];
                                                      dtProfile.Rows.Add(drProfile);
                                                  });
                    iCountTID = 0; iCountMaster = 0;
                }

                //Delete Credit AND Add Union Pay
                WriteLogTagInsert("UNIONPAY");
                iCountTID = dtProfile.Select(String.Format("Tag ='AE01' AND Name = 'CREDIT'")).Length;
                iCountMaster = dtProfileMaster.Select(String.Format("Tag ='AE01' AND Name LIKE 'UNIONPAY%' AND TerminalId = '{0}'", sMasterCCN)).Length;
                if (iCountTID > 0 && iCountMaster > 0)
                {

                    sTagUsebyTID = "";
                    //Select ID Profile
                    dtProfile.Select(string.Format("TagValue ='CREDIT' AND Tag like 'AD02'"))
                        .ToList<DataRow>()
                        .ForEach(Profile =>
                        {
                            sTagUsebyTID = sTagUsebyTID + string.Format("'{0}',", Convert.ToInt64(Profile["ID"].ToString()) - 1);
                            sTagUsebyTID = sTagUsebyTID + string.Format("'{0}',", Convert.ToInt64(Profile["ID"].ToString()));
                            sTagUsebyTID = sTagUsebyTID + string.Format("'{0}',", Convert.ToInt64(Profile["ID"].ToString()) + 1);
                        });
                    //Delete Acquirer Credit and relation
                    dtProfile.Select(string.Format("(Name ='CREDIT' AND Tag like 'AE%') OR (ID in ({0}))", sTagUsebyTID.Substring(0, sTagUsebyTID.Length - 1)))
                        .ToList<DataRow>()
                        .ForEach(Profile =>
                            {
                                dtProfile.Rows.Remove(Profile);
                            });
                    //Add Relation Union Pay
                    sTagUsebyTID = "";
                    dtProfileMaster.Select(string.Format("Tag ='AD02' AND TagValue LIKE 'UNIONPAY%' AND TerminalId = '{0}'", sMasterCCN))
                        .ToList<DataRow>()
                        .ForEach(ProfileMaster =>
                        {
                            dtProfileMaster.Select(string.Format("id ={0} AND TerminalId = '{1}'", Convert.ToInt64(ProfileMaster["ID"].ToString()) - 1, sMasterCCN))
                                .ToList<DataRow>()
                                .ForEach(ProfileMaster1 =>
                                    {
                                        iCountRowProfile = iCountRowProfile + 1;
                                        drProfile = dtProfile.NewRow();
                                        drProfile["Id"] = iCountRowProfile;
                                        drProfile["TerminalId"] = sNewTerminalID;
                                        drProfile["Name"] = ProfileMaster1["Name"];
                                        drProfile["Tag"] = ProfileMaster1["Tag"];
                                        drProfile["ItemName"] = ProfileMaster1["ItemName"];
                                        drProfile["TagLength"] = ProfileMaster1["TagLength"];
                                        drProfile["TagValue"] = ProfileMaster1["TagValue"];
                                        dtProfile.Rows.Add(drProfile);
                                    });
                            dtProfileMaster.Select(string.Format("id ={0} AND TerminalId = '{1}'", Convert.ToInt64(ProfileMaster["ID"].ToString()), sMasterCCN))
                                .ToList<DataRow>()
                                .ForEach(ProfileMaster2 =>
                                {
                                    iCountRowProfile = iCountRowProfile + 1;
                                    drProfile = dtProfile.NewRow();
                                    drProfile["Id"] = iCountRowProfile;
                                    drProfile["TerminalId"] = sNewTerminalID;
                                    drProfile["Name"] = ProfileMaster2["Name"];
                                    drProfile["Tag"] = ProfileMaster2["Tag"];
                                    drProfile["ItemName"] = ProfileMaster2["ItemName"];
                                    drProfile["TagLength"] = ProfileMaster2["TagLength"];
                                    drProfile["TagValue"] = ProfileMaster2["TagValue"];
                                    dtProfile.Rows.Add(drProfile);
                                });
                            dtProfileMaster.Select(string.Format("id ={0} AND TerminalId = '{1}'", Convert.ToInt64(ProfileMaster["ID"].ToString()) + 1, sMasterCCN))
                                .ToList<DataRow>()
                                .ForEach(ProfileMaster3 =>
                                {
                                    iCountRowProfile = iCountRowProfile + 1;
                                    drProfile = dtProfile.NewRow();
                                    drProfile["Id"] = iCountRowProfile;
                                    drProfile["TerminalId"] = sNewTerminalID;
                                    drProfile["Name"] = ProfileMaster3["Name"];
                                    drProfile["Tag"] = ProfileMaster3["Tag"];
                                    drProfile["ItemName"] = ProfileMaster3["ItemName"];
                                    drProfile["TagLength"] = ProfileMaster3["TagLength"];
                                    drProfile["TagValue"] = ProfileMaster3["TagValue"];
                                    dtProfile.Rows.Add(drProfile);
                                });
                        });
                    //Add UnionPay
                    dtProfileMaster.Select(string.Format("Name LIKE 'UNIONPAY%' AND Tag LIKE 'AE%' AND TerminalId = '{0}'", sMasterCCN))
                        .ToList<DataRow>()
                        .ForEach(ProfileMaster =>
                            {
                                iCountRowProfile = iCountRowProfile + 1;
                                drProfile = dtProfile.NewRow();
                                drProfile["Id"] = iCountRowProfile;
                                drProfile["TerminalId"] = sNewTerminalID;
                                drProfile["Name"] = ProfileMaster["Name"];
                                drProfile["Tag"] = ProfileMaster["Tag"];
                                drProfile["ItemName"] = ProfileMaster["ItemName"];
                                drProfile["TagLength"] = ProfileMaster["TagLength"];
                                drProfile["TagValue"] = ProfileMaster["TagValue"];
                                dtProfile.Rows.Add(drProfile);
                            });

                    iCountTID = 0; iCountMaster = 0;
                }
                WriteLogTagInsert("BCA SYARIAH");
                iCountTID = dtProfile.Select(String.Format("Tag ='AD01' AND TagValue = 'BCA Syariah'")).Length;
                iCountMaster = dtProfileMaster.Select(String.Format("Tag ='AD02' AND TagValue = 'BCA Syariah' AND TerminalId = '{0}'", sMasterCCN)).Length;
                if (iCountTID > 0 && iCountMaster > 0)
                {
                    sTagUsebyTID = "";
                    //Get Relation ID for Remove
                    dtProfile.Select(string.Format("Tag ='AD01' AND TagValue = 'BCA Syariah'"))
                        .ToList<DataRow>()
                        .ForEach(Profile =>
                            {
                                sTagUsebyTID = sTagUsebyTID + string.Format("'{0}',", Convert.ToInt64(Profile["ID"].ToString()));
                                sTagUsebyTID = sTagUsebyTID + string.Format("'{0}',", Convert.ToInt64(Profile["ID"].ToString()) + 1);
                                sTagUsebyTID = sTagUsebyTID + string.Format("'{0}',", Convert.ToInt64(Profile["ID"].ToString()) + 2);
                            });
                    //Delete Relation by ID
                    dtProfile.Select(string.Format("ID in ({0})", sTagUsebyTID.Substring(0, sTagUsebyTID.Length - 1)))
                        .ToList<DataRow>()
                        .ForEach(Profile =>
                            {
                                dtProfile.Rows.Remove(Profile);
                            });
                    sTagUsebyTID = "";
                    //Get Relation ID for Copy data
                    dtProfileMaster.Select(string.Format("Tag ='AD02' AND TagValue = 'BCA Syariah' AND TerminalId = '{0}'", sMasterCCN))
                        .ToList<DataRow>()
                        .ForEach(ProfileMaster =>
                        {
                            dtProfileMaster.Select(string.Format("id ={0} AND TerminalId = '{1}'", Convert.ToInt64(ProfileMaster["ID"].ToString()) - 1, sMasterCCN))
                                .ToList<DataRow>()
                                .ForEach(ProfileMaster1 =>
                                {
                                    iCountRowProfile = iCountRowProfile + 1;
                                    drProfile = dtProfile.NewRow();
                                    drProfile["Id"] = iCountRowProfile;
                                    drProfile["TerminalId"] = sNewTerminalID;
                                    drProfile["Name"] = ProfileMaster1["Name"];
                                    drProfile["Tag"] = ProfileMaster1["Tag"];
                                    drProfile["ItemName"] = ProfileMaster1["ItemName"];
                                    drProfile["TagLength"] = ProfileMaster1["TagLength"];
                                    drProfile["TagValue"] = ProfileMaster1["TagValue"];
                                    dtProfile.Rows.Add(drProfile);
                                });
                            dtProfileMaster.Select(string.Format("id ={0} AND TerminalId = '{1}'", Convert.ToInt64(ProfileMaster["ID"].ToString()), sMasterCCN))
                                .ToList<DataRow>()
                                .ForEach(ProfileMaster2 =>
                                {
                                    iCountRowProfile = iCountRowProfile + 1;
                                    drProfile = dtProfile.NewRow();
                                    drProfile["Id"] = iCountRowProfile;
                                    drProfile["TerminalId"] = sNewTerminalID;
                                    drProfile["Name"] = ProfileMaster2["Name"];
                                    drProfile["Tag"] = ProfileMaster2["Tag"];
                                    drProfile["ItemName"] = ProfileMaster2["ItemName"];
                                    drProfile["TagLength"] = ProfileMaster2["TagLength"];
                                    drProfile["TagValue"] = ProfileMaster2["TagValue"];
                                    dtProfile.Rows.Add(drProfile);
                                });
                            dtProfileMaster.Select(string.Format("id ={0} AND TerminalId = '{1}'", Convert.ToInt64(ProfileMaster["ID"].ToString()) + 1, sMasterCCN))
                                .ToList<DataRow>()
                                .ForEach(ProfileMaster3 =>
                                {
                                    iCountRowProfile = iCountRowProfile + 1;
                                    drProfile = dtProfile.NewRow();
                                    drProfile["Id"] = iCountRowProfile;
                                    drProfile["TerminalId"] = sNewTerminalID;
                                    drProfile["Name"] = ProfileMaster3["Name"];
                                    drProfile["Tag"] = ProfileMaster3["Tag"];
                                    drProfile["ItemName"] = ProfileMaster3["ItemName"];
                                    drProfile["TagLength"] = ProfileMaster3["TagLength"];
                                    drProfile["TagValue"] = ProfileMaster3["TagValue"];
                                    dtProfile.Rows.Add(drProfile);
                                });
                        });
                    //Add Profile BCA SYARIAH And Relation
                    dtProfileMaster.Select(string.Format("Name = 'BCA SYARIAH' AND Tag like 'AE%' AND TerminalId = '{0}'", sMasterCCN))
                        .ToList<DataRow>()
                        .ForEach(ProfileMaster =>
                            {
                                iCountRowProfile = iCountRowProfile + 1;
                                drProfile = dtProfile.NewRow();
                                drProfile["Id"] = iCountRowProfile;
                                drProfile["TerminalId"] = sNewTerminalID;
                                drProfile["Name"] = ProfileMaster["Name"];
                                drProfile["Tag"] = ProfileMaster["Tag"];
                                drProfile["ItemName"] = ProfileMaster["ItemName"];
                                drProfile["TagLength"] = ProfileMaster["TagLength"];
                                drProfile["TagValue"] = ProfileMaster["TagValue"];
                                dtProfile.Rows.Add(drProfile);
                            });
                    iCountTID = 0; iCountMaster = 0;
                }
                WriteLogTagInsert("Maestro");
                iCountTID = dtProfile.Select(String.Format("Tag ='AE01' AND Name = 'MAESTRO'")).Length;
                iCountMaster = dtProfileMaster.Select(String.Format("Tag ='AE01' AND Name = 'MAESTRO' AND TerminalId = '{0}'", sMasterCCN)).Length;
                if (iCountTID > 0 && iCountMaster > 0)
                {
                    sTagUsebyTID = "";
                    //Get Relation ID for Copy data
                    dtProfileMaster.Select(string.Format("Tag ='AD01' AND TagValue = 'MAESTRO' AND TerminalId = '{0}'", sMasterCCN))
                        .ToList<DataRow>()
                        .ForEach(ProfileMaster =>
                        {
                            dtProfileMaster.Select(string.Format("id ={0} AND TerminalId = '{1}'", Convert.ToInt64(ProfileMaster["ID"].ToString()), sMasterCCN))
                                .ToList<DataRow>()
                                .ForEach(ProfileMaster1 =>
                                {
                                    iCountRowProfile = iCountRowProfile + 1;
                                    drProfile = dtProfile.NewRow();
                                    drProfile["Id"] = iCountRowProfile;
                                    drProfile["TerminalId"] = sNewTerminalID;
                                    drProfile["Name"] = ProfileMaster1["Name"];
                                    drProfile["Tag"] = ProfileMaster1["Tag"];
                                    drProfile["ItemName"] = ProfileMaster1["ItemName"];
                                    drProfile["TagLength"] = ProfileMaster1["TagLength"];
                                    drProfile["TagValue"] = ProfileMaster1["TagValue"];
                                    dtProfile.Rows.Add(drProfile);
                                });
                            dtProfileMaster.Select(string.Format("id ={0} AND TerminalId = '{1}'", Convert.ToInt64(ProfileMaster["ID"].ToString()) + 1, sMasterCCN))
                                .ToList<DataRow>()
                                .ForEach(ProfileMaster2 =>
                                {
                                    iCountRowProfile = iCountRowProfile + 1;
                                    drProfile = dtProfile.NewRow();
                                    drProfile["Id"] = iCountRowProfile;
                                    drProfile["TerminalId"] = sNewTerminalID;
                                    drProfile["Name"] = ProfileMaster2["Name"];
                                    drProfile["Tag"] = ProfileMaster2["Tag"];
                                    drProfile["ItemName"] = ProfileMaster2["ItemName"];
                                    drProfile["TagLength"] = ProfileMaster2["TagLength"];
                                    drProfile["TagValue"] = ProfileMaster2["TagValue"];
                                    dtProfile.Rows.Add(drProfile);
                                });
                            dtProfileMaster.Select(string.Format("id ={0} AND TerminalId = '{1}'", Convert.ToInt64(ProfileMaster["ID"].ToString()) + 2, sMasterCCN))
                                .ToList<DataRow>()
                                .ForEach(ProfileMaster3 =>
                                {
                                    iCountRowProfile = iCountRowProfile + 1;
                                    drProfile = dtProfile.NewRow();
                                    drProfile["Id"] = iCountRowProfile;
                                    drProfile["TerminalId"] = sNewTerminalID;
                                    drProfile["Name"] = ProfileMaster3["Name"];
                                    drProfile["Tag"] = ProfileMaster3["Tag"];
                                    drProfile["ItemName"] = ProfileMaster3["ItemName"];
                                    drProfile["TagLength"] = ProfileMaster3["TagLength"];
                                    drProfile["TagValue"] = ProfileMaster3["TagValue"];
                                    dtProfile.Rows.Add(drProfile);
                                });
                        });
                    iCountTID = 0; iCountMaster = 0;
                }

                WriteLogTagInsert("Tambah semua tag yg null");
                string sTagMaster = "";
                string sNameMaster = "";
                int iCountRow = 0;
                int iCountRowRelation = 0;
                int iCount2 = 0;
                string sTagMaster2 = "";
                //DEVIDE
                //DE
                WriteLogTagInsert("tag DE Start");
                dtProfileMaster.Select(string.Format("TerminalId ='{0}' AND Tag LIKE 'DE%'", sMasterCCN))
                    .ToList<DataRow>()
                    .ForEach(ProfileMaster =>
                        {
                            sTagMaster = ProfileMaster["Tag"].ToString();
                            if (sTagMaster.Substring(0, 2) == "DE")
                            {
                                sTagUsebyTID = "";
                                iCountRow = dtProfile.Select(String.Format("Tag = '{0}'", sTagMaster)).Length;
                                if (iCountRow == 0)
                                {
                                    sTagUsebyTID = sTagUsebyTID + "'" + ProfileMaster["Tag"].ToString() + "',";
                                }

                            }
                            if (!string.IsNullOrEmpty(sTagUsebyTID))
                            {
                                dtProfileMaster.Select(string.Format("Tag in ({0}) AND TerminalId = '{1}'", sTagUsebyTID.Substring(0, sTagUsebyTID.Length - 1), sMasterCCN))
                                    .ToList<DataRow>()
                                    .ForEach(ProfileMasterDE =>
                                        {
                                            iCountRowProfile = iCountRowProfile + 1;
                                            //DataRow drProfile = dtProfile.NewRow();
                                            drProfile = dtProfile.NewRow();
                                            drProfile["Id"] = iCountRowProfile;
                                            drProfile["TerminalId"] = sNewTerminalID;
                                            drProfile["Name"] = ProfileMaster["Name"];
                                            drProfile["Tag"] = ProfileMaster["Tag"];
                                            drProfile["ItemName"] = ProfileMaster["ItemName"];
                                            drProfile["TagLength"] = ProfileMaster["TagLength"];
                                            drProfile["TagValue"] = ProfileMaster["TagValue"];
                                            dtProfile.Rows.Add(drProfile);
                                        });
                            }

                        });
                WriteLogTagInsert("tag DE End");

                WriteLogTagInsert("tag AA Start");
                dtProfileMaster.Select(string.Format("Tag = 'AA01' AND TerminalId = '{0}'", sMasterCCN))
                    .ToList<DataRow>()
                    .ForEach(ProfileMaster =>
                        {
                            sTagMaster = ProfileMaster["Tag"].ToString();
                            sNameMaster = ProfileMaster["Name"].ToString();

                            iCountRowRelation = dtProfile.Select(String.Format("Tag = 'AD03' AND TagValue = '{0}'", sNameMaster)).Length;
                            if (iCountRowRelation > 0)
                            {
                                iCount2 = 0;
                                sTagMaster2 = "";
                                sTagUsebyTID = "";
                                dtProfileMaster.Select(String.Format("Tag like 'AA%' AND Name ='{0}'  AND TerminalId = '{1}'", sNameMaster, sMasterCCN))
                                    .ToList<DataRow>()
                                    .ForEach(ProfileMaster1 =>
                                        {
                                            sTagMaster2 = ProfileMaster1["Tag"].ToString();
                                            iCount2 = dtProfile.Select(String.Format("Tag = '{0}' AND Name = '{1}'", sTagMaster2, sNameMaster)).Length;
                                            if (iCount2 == 0)
                                            {
                                                sTagUsebyTID = sTagUsebyTID + "'" + ProfileMaster1["Tag"].ToString() + "',";
                                            }
                                        });
                                if (!string.IsNullOrEmpty(sTagUsebyTID))
                                {
                                    dtProfileMaster.Select(string.Format("Tag in ({0}) AND Name = '{1}' AND TerminalId = '{2}'", sTagUsebyTID.Substring(0, sTagUsebyTID.Length - 1), sNameMaster, sMasterCCN))
                                    .ToList<DataRow>()
                                    .ForEach(ProfileMasterAA =>
                                    {
                                        iCountRowProfile = iCountRowProfile + 1;
                                        drProfile = dtProfile.NewRow();
                                        drProfile["Id"] = iCountRowProfile;
                                        drProfile["TerminalId"] = sNewTerminalID;
                                        drProfile["Name"] = ProfileMasterAA["Name"];
                                        drProfile["Tag"] = ProfileMasterAA["Tag"];
                                        drProfile["ItemName"] = ProfileMasterAA["ItemName"];
                                        drProfile["TagLength"] = ProfileMasterAA["TagLength"];
                                        drProfile["TagValue"] = ProfileMasterAA["TagValue"];
                                        dtProfile.Rows.Add(drProfile);
                                    });
                                }
                            }

                        });
                WriteLogTagInsert("tag AA END");

                WriteLogTagInsert("tag AE Start");
                dtProfileMaster.Select(string.Format("Tag = 'AE01' AND TerminalId = '{0}'", sMasterCCN))
                   .ToList<DataRow>()
                   .ForEach(ProfileMaster =>
                       {
                           sTagMaster = ProfileMaster["Tag"].ToString();
                           sNameMaster = ProfileMaster["Name"].ToString();

                           iCountRowRelation = dtProfile.Select(String.Format("Tag = 'AD02' AND TagValue = '{0}'", sNameMaster)).Length;
                           if (iCountRowRelation > 0)
                           {
                               sTagUsebyTID = "";
                               dtProfileMaster.Select(String.Format("Tag like 'AE%' AND Name ='{0}' AND TerminalId = '{1}'", sNameMaster, sMasterCCN))
                                   .ToList<DataRow>()
                                   .ForEach(ProfileMaster1 =>
                                   {
                                       sTagMaster2 = ProfileMaster1["Tag"].ToString();
                                       iCount2 = dtProfile.Select(String.Format("Tag = '{0}' AND Name = '{1}'", sTagMaster2, sNameMaster)).Length;
                                       if (iCount2 == 0)
                                       {
                                           sTagUsebyTID = sTagUsebyTID + "'" + ProfileMaster1["Tag"].ToString() + "',";
                                       }
                                   });
                               if (!string.IsNullOrEmpty(sTagUsebyTID))
                               {
                                   dtProfileMaster.Select(string.Format("Tag in ({0}) AND Name = '{1}' AND TerminalId = '{2}'", sTagUsebyTID.Substring(0, sTagUsebyTID.Length - 1), sNameMaster, sMasterCCN))
                                       .ToList<DataRow>()
                                       .ForEach(ProfileMasterAE =>
                                           {
                                               iCountRowProfile = iCountRowProfile + 1;
                                               drProfile = dtProfile.NewRow();
                                               drProfile["Id"] = iCountRowProfile;
                                               drProfile["TerminalId"] = sNewTerminalID;
                                               drProfile["Name"] = ProfileMasterAE["Name"];
                                               drProfile["Tag"] = ProfileMasterAE["Tag"];
                                               drProfile["ItemName"] = ProfileMasterAE["ItemName"];
                                               drProfile["TagLength"] = ProfileMasterAE["TagLength"];
                                               drProfile["TagValue"] = ProfileMasterAE["TagValue"];
                                               dtProfile.Rows.Add(drProfile);
                                           });
                               }
                           }

                       });
                WriteLogTagInsert("tag AE END");

                WriteLogTagInsert("masuk insert");
                if (isProfileValid(dtProfile, sTerminalId))
                {
                    if (bFunction != 0)
                    {
                        //Delete TID
                        using (SqlCommand oCmd = new SqlCommand("spProfileTerminalDeleteProcess", oSqlConn))
                        {
                            oCmd.CommandType = CommandType.StoredProcedure;
                            oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                            if (oSqlConn.State == ConnectionState.Closed)
                                oSqlConn.Open();
                            oCmd.ExecuteNonQuery();
                        }
                        insertAuditTrail(sTerminalId, "GEMALTO", "", string.Format("Delete TID : {0}", sTerminalId));
                    }

                    DataSet dsTables = new DataSet();
                    string sQuery = string.Format("exec spItemListBrowse 'WHERE DatabaseID={0}'\n{1}\n{2}\n{3}\n{4}\n{5}"
                        , sDatabaseIDTarget
                        , "SELECT * FROM tbProfileTerminalList WHERE TerminalID = null"
                        , "SELECT TerminalID,TerminalTag,TerminalLengthOfTagLength,TerminalTagLength,TerminalTagValue FROM tbProfileTerminal WHERE TerminalID = null"
                        , "SELECT TerminalID,AcquirerName,AcquirerTag,AcquirerLengthOfTagLength,AcquirerTagLength,AcquirerTagValue FROM tbProfileAcquirer WHERE TerminalID = null"
                        , "SELECT TerminalID,IssuerName,IssuerTag,IssuerLengthOfTagLength,IssuerTagLength,IssuerTagValue FROM tbProfileIssuer WHERE TerminalID = null"
                        , "SELECT TerminalID,RelationTag,RelationLengthOfTagLength,RelationTagLength,RelationTagValue FROM tbProfileRelation WHERE TerminalID = null");
                    using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
                    {
                        SqlDataAdapter oAdapt = new SqlDataAdapter(oCmd);
                        oAdapt.Fill(dsTables);
                    }

                    /*
                     * SPECIAL NOTE :
                     * pastiin semua profile yg mau di-import ke database, punya table yg lengkap. kalo ilang salah 1, import pasti gagal
                     */
                    SqlTransaction oTrans = oSqlConn.BeginTransaction();
                    string sError = null;
                    if (isBulkImportProfileSuccess(sNewTerminalID, Convert.ToInt16(sDatabaseIDTarget), "relation", dtProfile, dsTables, oTrans, oSqlConn, ref sError))
                        if (isBulkImportProfileSuccess(sNewTerminalID, Convert.ToInt16(sDatabaseIDTarget), "issuer", dtProfile, dsTables, oTrans, oSqlConn, ref sError))
                            if (isBulkImportProfileSuccess(sNewTerminalID, Convert.ToInt16(sDatabaseIDTarget), "acquirer", dtProfile, dsTables, oTrans, oSqlConn, ref sError))
                                if (isBulkImportProfileSuccess(sNewTerminalID, Convert.ToInt16(sDatabaseIDTarget), "terminal", dtProfile, dsTables, oTrans, oSqlConn, ref sError))
                                    if (isBulkImportProfileSuccess(sNewTerminalID, Convert.ToInt16(sDatabaseIDTarget), "count", dtProfile, dsTables, oTrans, oSqlConn, ref sError))
                                    {
                                        oTrans.Commit();
                                        insertAuditTrail(sTerminalId, "GEMALTO", "BCAI22051CCN_MI", string.Format("Insert TID : {0}", sTerminalId));
                                    }
                                    else
                                    { oTrans.Rollback(); Display(sError); }
                                else
                                { oTrans.Rollback(); Display(sError); }
                            else
                            { oTrans.Rollback(); Display(sError); }
                        else
                        { oTrans.Rollback(); Display(sError); }
                    else
                    { oTrans.Rollback(); Display(sError); }
                }
                else { Display(string.Format("{0} Can not insert. Data Not Complete")); }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("ERROR. {0},{1}", sTerminalId, ex.Message));
            }
        }
        
        private static void insertAuditTrail(string sTerminalId, string sUserID, string sDatabaseName, string sActionDesc)
        {
            using (SqlCommand oCmd = new SqlCommand("spAuditTrailInsert", oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = sUserID;
                oCmd.Parameters.Add("@sDatabaseName", SqlDbType.VarChar).Value = sDatabaseName;
                oCmd.Parameters.Add("@sActionDesc", SqlDbType.VarChar).Value = sActionDesc;
                if (oSqlConn.State == ConnectionState.Closed)
                    oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }

        public static bool isBulkImportProfileSuccess(string _sTerminalID, int iDatabaseId, string sTableType,
            DataTable _dtProfileTLV, DataSet _dsTables,
            SqlTransaction _oTrans, SqlConnection oSqlConn,
            ref string sError)
        {
            string sTagHeader = "DE";
            int iTableID = 2;
            string sDestTableName = "tbProfileTerminal";
            switch (sTableType.ToLower())
            {
                case "terminal":
                    sTagHeader = "DE";
                    iTableID = 2;
                    sDestTableName = "tbProfileTerminal";
                    break;
                case "acquirer":
                    sTagHeader = "AA";
                    iTableID = 3;
                    sDestTableName = "tbProfileAcquirer";
                    break;
                case "issuer":
                    sTagHeader = "AE";
                    iTableID = 4;
                    sDestTableName = "tbProfileIssuer";
                    break;
                case "relation":
                    sTagHeader = "AD";
                    iTableID = 5;
                    sDestTableName = "tbProfileRelation";
                    break;
                default:
                    sTagHeader = "DC";
                    iTableID = 2;
                    sDestTableName = "tbProfileTerminal";
                    break;
            }

            DataTable dtFilteredProfileTLV = _dtProfileTLV.Select(string.Format("Tag LIKE '{0}%'", sTagHeader), "Id").CopyToDataTable();
            DataTable dtProfile = _dsTables.Tables[iTableID];
            dtProfile.Rows.Clear();

            foreach (DataRow rowFilteredProfile in dtFilteredProfileTLV.Rows)
            {
                DataRow rowProfile = dtProfile.NewRow();
                rowProfile[0] = _sTerminalID;
                rowProfile[1] = sTableType.ToLower() != "terminal"
                    && sTableType.ToLower() != "count"
                    && sTableType.ToLower() != "relation" ?
                    rowFilteredProfile["Name"] : rowFilteredProfile["Tag"];
                rowProfile[2] = sTableType.ToLower() != "terminal"
                    && sTableType.ToLower() != "count"
                    && sTableType.ToLower() != "relation" ?
                    rowFilteredProfile["Tag"] : rowFilteredProfile["TagLength"].ToString().Length;
                rowProfile[3] = sTableType.ToLower() != "terminal"
                    && sTableType.ToLower() != "count"
                    && sTableType.ToLower() != "relation" ?
                    rowFilteredProfile["TagLength"].ToString().Length : rowFilteredProfile["TagLength"];
                rowProfile[4] = sTableType.ToLower() != "terminal"
                    && sTableType.ToLower() != "count"
                    && sTableType.ToLower() != "relation" ?
                    rowFilteredProfile["TagLength"] : rowFilteredProfile["TagValue"];
                if (sTableType.ToLower() != "terminal"
                    && sTableType.ToLower() != "count"
                    && sTableType.ToLower() != "relation")
                    rowProfile[5] = rowFilteredProfile["TagValue"];
                dtProfile.Rows.Add(rowProfile);
            }

            try
            {
                SqlBulkCopy sqlBulkProfile = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.Default, _oTrans);
                sqlBulkProfile.DestinationTableName = sDestTableName;
                foreach (DataColumn col in dtProfile.Columns)
                {
                    if (dtProfile.Columns.Contains(col.ColumnName))
                        sqlBulkProfile.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                }
                sqlBulkProfile.WriteToServer(dtProfile);

                if (sTableType.ToLower() == "terminal")
                {
                    DataTable dtProfileTerminalList = _dsTables.Tables[1];
                    dtProfileTerminalList.Rows.Clear();
                    DataRow rowTerminalList = dtProfileTerminalList.NewRow();
                    rowTerminalList["TerminalID"] = _sTerminalID;
                    rowTerminalList["DatabaseID"] = iDatabaseId;
                    rowTerminalList["AllowDownload"] = 1;
                    rowTerminalList["StatusMaster"] = 0;
                    dtProfileTerminalList.Rows.Add(rowTerminalList);

                    SqlBulkCopy sqlBulkTerminalList = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.Default, _oTrans);
                    sqlBulkTerminalList.DestinationTableName = "tbProfileTerminalList";
                    foreach (DataColumn col in dtProfileTerminalList.Columns)
                    {
                        if (dtProfileTerminalList.Columns.Contains(col.ColumnName))
                            sqlBulkTerminalList.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                    }
                    sqlBulkTerminalList.WriteToServer(dtProfileTerminalList);
                }
                return true;
            }
            catch (Exception ex)
            {
                sError = ex.Message;
                return false;
            }
        }

        private static void DropFilterOfMasterinSQL()
        {
            using (SqlCommand oCmd = new SqlCommand("Drop Table TempMaster", oSqlConn))
            {
                if (oSqlConn.State == ConnectionState.Closed)
                    oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }

        private static DataTable CreateFilterOfMasterinSQL()
        {
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spMigrasiCCNCountAcquirerIssuer", oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                if (oSqlConn.State == ConnectionState.Closed)
                    oSqlConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtTemp);
            }
            return dtTemp;
        }

        private static bool IsMasterCCNExist(string sMasterCCN)
        {
            DataTable dtTerminal = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spProfileTerminalListBrowse", oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = "Where TerminalID = '" + sMasterCCN+"'";
                if (oSqlConn.State == ConnectionState.Closed)
                    oSqlConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtTerminal);
            }
            if (dtTerminal.Rows.Count == 1)
                return true;
            else
                return false;
        }

        private static void ExecuteMovingTerminalID(string sNewTerminalID, string sTerminalId, string sMasterCCN, string sDatabaseID)
        {
            using (SqlCommand oCmd = new SqlCommand("spProfileCopyDataTerminalID", oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sNewTerminalID", SqlDbType.VarChar).Value = sNewTerminalID;
                oCmd.Parameters.Add("@sRefTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                oCmd.Parameters.Add("@sMasterTerminalID", SqlDbType.VarChar).Value = sMasterCCN;
                oCmd.Parameters.Add("@iBaseDB", SqlDbType.VarChar).Value = sDatabaseID;
                oCmd.Parameters.Add("@iTypeFunction", SqlDbType.Int).Value = bUpdate ? 1:0;
                if (oSqlConn.State == ConnectionState.Closed)
                    oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }

        private static string GetMasterCCN(string sSelectedMaster)
        {
            string sMasterCCN = null;
            if (sSelectedMaster == "MIQAIR01") sMasterCCN = "CCNAIR01";
            else if (sSelectedMaster == "MIQAIR02")sMasterCCN = "CCNAIR03";
            else if (sSelectedMaster == "MIQAIR03")sMasterCCN = "CCNAIR02";
            else if (sSelectedMaster == "MIQAIR04")sMasterCCN = "CCNAIR04";
            else if (sSelectedMaster == "MIQCABP1")sMasterCCN = "CCNCABP1";
            else if (sSelectedMaster == "MIQECHN1")sMasterCCN = "CCNECHN1";
            else if (sSelectedMaster == "MIQONOF1")sMasterCCN = "CCNONOF3";
            else if (sSelectedMaster == "MIQONOF2")sMasterCCN = "CCNONOF4";
            else if (sSelectedMaster == "MIQONOF3")sMasterCCN = "CCNONOF2";
            else if (sSelectedMaster == "MIQONOF4")sMasterCCN = "CCNONOF1";
            else if (sSelectedMaster == "MIQONOF5")sMasterCCN = "CCNONOF5";
            else if (sSelectedMaster == "MIQRES01")sMasterCCN = "CCNRES01";
            else if (sSelectedMaster == "MIQRES02")sMasterCCN = "CCNRES03";
            else if (sSelectedMaster == "MIQRES03")sMasterCCN = "CCNRES02";
            else if (sSelectedMaster == "MIQRES04")sMasterCCN = "CCNRES04";
            else if (sSelectedMaster == "MIQRET01")sMasterCCN = "CCNRET07";
            else if (sSelectedMaster == "MIQRET02")sMasterCCN = "CCNRET09";
            else if (sSelectedMaster == "MIQRET03")sMasterCCN = "CCNRET02";
            else if (sSelectedMaster == "MIQRET04")sMasterCCN = "CCNRET04";
            else if (sSelectedMaster == "MIQRET05")sMasterCCN = "CCNRET13";
            else if (sSelectedMaster == "MIQRET06")sMasterCCN = "CCNRET06";
            else if (sSelectedMaster == "MIQRET07")sMasterCCN = "CCNRET14";
            else if (sSelectedMaster == "MIQRETP1")sMasterCCN = "CCNRET07";
            else if (sSelectedMaster == "MIQRETP1")sMasterCCN = "CCNRET07";
            else if (sSelectedMaster == "MIQRETP2")sMasterCCN = "CCNRET09";
            else if (sSelectedMaster == "MIQRETP3")sMasterCCN = "CCNRET02";
            else if (sSelectedMaster == "MIQRETP4")sMasterCCN = "CCNRET04";
            else if (sSelectedMaster == "MIQRETP5")sMasterCCN = "CCNRET13";
            else if (sSelectedMaster == "MIQRETP6")sMasterCCN = "CCNRET06";
            else if (sSelectedMaster == "MIQSPLT1")sMasterCCN = "CCNHTL03";
            else if (sSelectedMaster == "MIQSPLT2")sMasterCCN = "CCNHTL04";
            else if (sSelectedMaster == "MIQSTMC1")sMasterCCN = "CCNSTMC1";
            else if (sSelectedMaster == "NIQAIR01")sMasterCCN = "CCNAIR01";
            else if (sSelectedMaster == "NIQAIR02")sMasterCCN = "CCNAIR03";
            else if (sSelectedMaster == "NIQAIR03")sMasterCCN = "CCNAIR02";
            else if (sSelectedMaster == "NIQAIR04")sMasterCCN = "CCNAIR04";
            else if (sSelectedMaster == "NIQCABP1")sMasterCCN = "CCNCABP1";
            else if (sSelectedMaster == "NIQECHN1")sMasterCCN = "CCNECHN1";
            else if (sSelectedMaster == "NIQONOF1")sMasterCCN = "CCNONOF3";
            else if (sSelectedMaster == "NIQONOF2")sMasterCCN = "CCNONOF4";
            else if (sSelectedMaster == "NIQONOF3")sMasterCCN = "CCNONOF2";
            else if (sSelectedMaster == "NIQONOF4")sMasterCCN = "CCNONOF1";
            else if (sSelectedMaster == "NIQONOF5")sMasterCCN = "CCNONOF5";
            else if (sSelectedMaster == "NIQONOF6")sMasterCCN = "CCNONOF6";
            else if (sSelectedMaster == "NIQRES01")sMasterCCN = "CCNRES01";
            else if (sSelectedMaster == "NIQRES02")sMasterCCN = "CCNRES03";
            else if (sSelectedMaster == "NIQRES03")sMasterCCN = "CCNRES02";
            else if (sSelectedMaster == "NIQRES04")sMasterCCN = "CCNRES04";
            else if (sSelectedMaster == "NIQRET01")sMasterCCN = "CCNRET07";
            else if (sSelectedMaster == "NIQRET02")sMasterCCN = "CCNRET09";
            else if (sSelectedMaster == "NIQRET03")sMasterCCN = "CCNRET02";
            else if (sSelectedMaster == "NIQRET04")sMasterCCN = "CCNRET04";
            else if (sSelectedMaster == "NIQRET05")sMasterCCN = "CCNRET13";
            else if (sSelectedMaster == "NIQRET06")sMasterCCN = "CCNRET06";
            else if (sSelectedMaster == "NIQRET07")sMasterCCN = "CCNRET14";
            else if (sSelectedMaster == "NIQRET08")sMasterCCN = "CCNRET15";
            else if (sSelectedMaster == "NIQRETP1")sMasterCCN = "CCNRET07";
            else if (sSelectedMaster == "NIQRETP2")sMasterCCN = "CCNRET09";
            else if (sSelectedMaster == "NIQRETP3")sMasterCCN = "CCNRET02";
            else if (sSelectedMaster == "NIQRETP4")sMasterCCN = "CCNRET04";
            else if (sSelectedMaster == "NIQRETP5")sMasterCCN = "CCNRET13";
            else if (sSelectedMaster == "NIQRETP6")sMasterCCN = "CCNRET06";
            else if (sSelectedMaster == "NIQSPLT1")sMasterCCN = "CCNHTL03";
            else if (sSelectedMaster == "NIQSPLT2")sMasterCCN = "CCNHTL04";
            else if (sSelectedMaster == "NIQSTMC1")sMasterCCN = "CCNSTMC1";
            else if (sSelectedMaster == "NIQEAL01")sMasterCCN = "CCNEAL01";
            return sMasterCCN;
        }

        private static string sSearchMasterTerminal(string sTerminalId, ref string sError)
        {
            string sReturn=null;
            DataTable dtTemp =  new DataTable();

            using (SqlCommand oCmd = new SqlCommand("spProfileSearchMasterTID", oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                if (oSqlConn.State == ConnectionState.Closed)
                    oSqlConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtTemp);

            }
            if (dtTemp.Rows.Count == 1)
            { sReturn = dtTemp.Rows[0]["TerminalID"].ToString(); }
            else if (dtTemp.Rows.Count == 0)
            { sError = "Master Not Found"; }
            else sError= "Master More Than One.";
            return sReturn;
        }

        private static string sSearchMasterTerminal(string sDatabaseID, string sTerminalId, DataTable dtProfile, DataTable dtMasterCount, DataTable dtAllMasterProfile, ref string sError)
        {
            string sReturn = null;
            string sCategory= "";
            int iCountAcquirer=0;
            int iCountIssuer=0;
            string sListMasterFound = "";

            sCategory=(dtProfile.Select("Tag='DE05'"))[0]["TagValue"].ToString();
            
            DataRow[] rowsAcquirer = dtProfile.Select("Tag='AA01' AND Name NOT LIKE 'PROMO SQ%' AND Name NOT IN ('PROMO F','PROMO G','PROMO H','PROMO I','PROMO J','AMEX','DINERS')");
            iCountAcquirer = rowsAcquirer.Count();
            
            DataRow[] rowsIssuer = dtProfile.Select("Tag='AE01' AND Name NOT IN('VISA SQ')");
            iCountIssuer = rowsIssuer.Count();

            DataRow[] rowsMaster = dtMasterCount.Select("DatabaseID = "+sDatabaseID+" AND Category='"+sCategory+"'AND TotalAcquirer='"+iCountAcquirer+"'AND TotalIssuer='"+iCountIssuer+"'");

            if (rowsMaster.Count()==1)
                sReturn= rowsMaster[0]["MasterTerminalID"].ToString();
            else if (rowsMaster.Count() == 0)
                sError = "Master Not Found.";
            else
            {
                //get acq Name
                string sAcqUse = "";
                int iCountAcq2 = 0, iCountMaster2=0;
                string sMaster = "";
              
                // generate acquirer list of terminal, using comma(',') separator
                dtProfile.Select("Tag='AA01' AND Name NOT LIKE 'PROMO SQ%' AND Name NOT IN ('PROMO F','PROMO G','PROMO H','PROMO I','PROMO J','AMEX','DINERS')")
                    .ToList<DataRow>()
                    .ForEach( Profile =>
                        {
                            sAcqUse = sAcqUse + string.Format("'{0}',",Profile["Name"].ToString());
                        });

                // search on master terminal list based on acquirerlist above
                dtMasterCount.Select("DatabaseID = "+sDatabaseID+" AND Category='"+sCategory+"'AND TotalAcquirer='"+iCountAcquirer+"'AND TotalIssuer='"+iCountIssuer+"'")
                    .ToList<DataRow>()
                    .ForEach(RowCount =>
                        {
                            iCountAcq2 = 0;
                            iCountAcq2 = dtAllMasterProfile.Select(string.Format("TerminalId = '{0}' AND Tag = 'AA01' And Name in ({1})", RowCount["MasterTerminalID"].ToString(), sAcqUse.Substring(0, sAcqUse.Length - 1))).Length;
                            //DataTable dtTempMaster = dtGetProfileTable(RowCount["MasterTerminalID"].ToString());
                            //iCountAcq2 = dtTempMaster.Select(string.Format("Tag = 'AA01' And Name in ({0})", sAcqUse.Substring(0, sAcqUse.Length - 1))).Length;
                            if (iCountAcquirer == iCountAcq2)
                                sMaster = sMaster + "'" + RowCount["MasterTerminalID"].ToString() + "',";
                        });

                if (sMaster.Length == 0)
                    iCountMaster2 = 0;
                else
                    iCountMaster2 = dtMasterCount.Select(string.Format("MasterTerminalID in ({0})", sMaster.Substring(0, sMaster.Length - 1))).Length;
                
                if (iCountMaster2 == 0)
                {
                    dtMasterCount.Select("DatabaseID = " + sDatabaseID + " AND Category='" + sCategory + "'AND TotalAcquirer='" + iCountAcquirer + "'AND TotalIssuer='" + iCountIssuer + "'")
                        .ToList<DataRow>()
                        .ForEach(ListMaster => 
                        {
                            sListMasterFound = sListMasterFound + " " + ListMaster["MasterTerminalID"].ToString() + ",";
                        });
                    sError = string.Format("Master More Than One : {0}", sListMasterFound.Substring(0,sListMasterFound.Length-1));
                }
                else if (iCountMaster2 == 1)
                    sReturn = sMaster.Substring(1, sMaster.Length - 3);
                else
                {
                    string sProfilepaymentCo = "";
                    string sMasterPaymentCo = "";
                    string sMasterListPayment = "";
                    int iCountMasterPaymentCo = 0;
                    string sListCoMasterFound = "";
                    // search based on payment co-brand code
                    dtMasterCount.Select(string.Format("MasterTerminalID in ({0})", sMaster.Substring(0, sMaster.Length - 1)))
                        .ToList<DataRow>()
                        .ForEach(MasTerCount =>
                        {
                            //DataTable dtTempMaster = dtGetProfileTable(MasTerCount["MasterTerminalID"].ToString());
                            //dtTempMaster.Select("Tag = 'AE38' AND Name = 'FLAZZ BCA'")
                            dtAllMasterProfile.Select(string.Format("TerminalId = '{0}' And Tag = 'AE38' And Name = 'FLAZZ BCA'", MasTerCount["MasterTerminalID"].ToString()))
                                .ToList<DataRow>()
                                .ForEach(TempMaster =>
                                    {
                                        sMasterPaymentCo = TempMaster["TagValue"].ToString();

                                        dtProfile.Select("Tag = 'AE38' AND Name = 'FLAZZ BCA'")
                                        .ToList<DataRow>()
                                        .ForEach(Profile =>
                                        {
                                            sProfilepaymentCo = Profile["TagValue"].ToString();
                                        });
                                        if (sMasterPaymentCo == sProfilepaymentCo)
                                            sMasterListPayment = sMasterListPayment + "'" + MasTerCount["MasterTerminalID"].ToString() + "',";
                                    });
                        });
                    iCountMasterPaymentCo = dtMasterCount.Select(string.Format("MasterTerminalID in ({0})", sMasterListPayment.Substring(0, sMasterListPayment.Length - 1))).Length;

                    if (iCountMasterPaymentCo == 1)
                        sReturn = sMasterListPayment.Substring(1, sMasterListPayment.Length - 3);
                    else
                    {
                        dtMasterCount.Select(string.Format("MasterTerminalID in ({0})", sMaster.Substring(0, sMaster.Length - 1)))
                            .ToList<DataRow>()
                            .ForEach(ProfileMaster =>
                            {
                                sListCoMasterFound = sListCoMasterFound + " " + ProfileMaster["MasterTerminalID"].ToString() + ",";
                            });
                        sError = string.Format("Master More Than One : {0}", sListCoMasterFound.Substring(0, sListCoMasterFound.Length - 1));
                    }
                }
            }

            return sReturn;
        }

        protected static bool IsTerminalIdValid(string _sTerminalID)
        {
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (DataTable dtTemp = new DataTable())
            {
                using (SqlCommand oCmd = new SqlCommand("spProfileTerminalListBrowse", oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format("WHERE TerminalID='{0}'", _sTerminalID);
                    (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                }
                if (dtTemp != null && dtTemp.Rows.Count > 0)
                    return true;
            }
            return false;
        }

        static void Init()
        {
            sSqlConnString = ConfigurationManager.ConnectionStrings["Database"].ConnectionString;
            iTotalProcess = int.Parse(sKeyConfigValue("Total"));
            arrsSourceDatabase = sKeyConfigValue("Source").Split(',');
            sTargetDatabaseName = sKeyConfigValue("Target");
            bUpdate = bool.Parse(sKeyConfigValue("Update"));

            InitMappingMasterProfile();
        }

        static void InitMappingMasterProfile()
        {
            #region "Define Mapping"
            MasterProfile[] arrMstProfile = new MasterProfile[]{
                (new MasterProfile("MIQAIR01","CCNAIR01",null,null)),
                (new MasterProfile("MIQAIR02","CCNAIR03",null,null)),
                (new MasterProfile("MIQAIR03","CCNAIR02",null,null)),
                (new MasterProfile("MIQAIR04","CCNAIR04",null,null)),
                (new MasterProfile("MIQCABP1","CCNCABP1",null,null)),
                (new MasterProfile("MIQECHN1","CCNECHN1",null,null)),
                (new MasterProfile("MIQONOF1","CCNONOF3",null,null)),
                (new MasterProfile("MIQONOF2","CCNONOF4",null,null)),
                (new MasterProfile("MIQONOF3","CCNONOF2",null,null)),
                (new MasterProfile("MIQONOF4","CCNONOF1",null,null)),
                (new MasterProfile("MIQONOF5","CCNONOF5",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("MIQRES01","CCNRES01",null,null)),
                (new MasterProfile("MIQRES02","CCNRES03",null,null)),
                (new MasterProfile("MIQRES03","CCNRES02",null,null)),
                (new MasterProfile("MIQRES04","CCNRES04",null,null)),
                (new MasterProfile("MIQRET01","CCNRET07",null,null)),
                (new MasterProfile("MIQRET02","CCNRET09",null,null)),
                (new MasterProfile("MIQRET03","CCNRET02",null,null)),
                (new MasterProfile("MIQRET04","CCNRET04",null,null)),
                (new MasterProfile("MIQRET05","CCNRET13",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("MIQRET06","CCNRET06",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("MIQRET07","CCNRET14",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("MIQRETP1","CCNRET07",(new string[]{"AE38"}),
                    (new string[]{"8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"AE01=='FLAZZBCA'"}))),
                (new MasterProfile("MIQRETP2","CCNRET09",(new string[]{"AE38"}),
                    (new string[]{"8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"AE01=='FLAZZBCA'"}))),
                (new MasterProfile("MIQRETP3","CCNRET02",(new string[]{"AE38"}),
                    (new string[]{"8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"AE01=='FLAZZBCA'"}))),
                (new MasterProfile("MIQRETP4","CCNRET04",(new string[]{"AE38"}),
                    (new string[]{"8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"AE01=='FLAZZBCA'"}))),
                (new MasterProfile("MIQRETP5","CCNRET13",(new string[]{"DE04","AE38"}),
                    (new string[]{"SLCS","8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"","AE01=='FLAZZBCA'"}))),
                (new MasterProfile("MIQRETP6","CCNRET06",(new string[]{"DE04","AE38"}),
                    (new string[]{"SLCS","8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"","AE01=='FLAZZBCA'"}))),
                (new MasterProfile("MIQSPLT1","CCNHTL03",null,null)),
                (new MasterProfile("MIQSPLT2","CCNHTL04",null,null)),
                (new MasterProfile("MIQSTMC1","CCNSTMC1",null,null)),
                (new MasterProfile("NIQAIR01","CCNAIR01",null,null)),
                (new MasterProfile("NIQAIR02","CCNAIR03",null,null)),
                (new MasterProfile("NIQAIR03","CCNAIR02",null,null)),
                (new MasterProfile("NIQAIR04","CCNAIR04",null,null)),
                (new MasterProfile("NIQCABP1","CCNCABP1",null,null)),
                (new MasterProfile("NIQECHN1","CCNECHN1",null,null)),
                (new MasterProfile("NIQONOF1","CCNONOF3",null,null)),
                (new MasterProfile("NIQONOF2","CCNONOF4",null,null)),
                (new MasterProfile("NIQONOF3","CCNONOF2",null,null)),
                (new MasterProfile("NIQONOF4","CCNONOF1",null,null)),
                (new MasterProfile("NIQONOF5","CCNONOF5",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("NIQONOF6","CCNONOF6",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("NIQRES01","CCNRES01",null,null)),
                (new MasterProfile("NIQRES02","CCNRES03",null,null)),
                (new MasterProfile("NIQRES03","CCNRES02",null,null)),
                (new MasterProfile("NIQRES04","CCNRES04",null,null)),
                (new MasterProfile("NIQRET01","CCNRET07",null,null)),
                (new MasterProfile("NIQRET02","CCNRET09",null,null)),
                (new MasterProfile("NIQRET03","CCNRET02",null,null)),
                (new MasterProfile("NIQRET04","CCNRET04",null,null)),
                (new MasterProfile("NIQRET05","CCNRET13",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("NIQRET06","CCNRET06",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("NIQRET07","CCNRET14",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("NIQRET08","CCNRET15",(new string[]{"DE04"}),(new string[]{"SLCS"}))),
                (new MasterProfile("NIQRETP1","CCNRET07",(new string[]{"AE38"}),
                    (new string[]{"8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"AE01=='FLAZZBCA'"}))),
                (new MasterProfile("NIQRETP2","CCNRET09",(new string[]{"AE38"}),
                    (new string[]{"8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"AE01=='FLAZZBCA'"}))),
                (new MasterProfile("NIQRETP3","CCNRET02",(new string[]{"AE38"}),
                    (new string[]{"8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"AE01=='FLAZZBCA'"}))),
                (new MasterProfile("NIQRETP4","CCNRET04",(new string[]{"AE38"}),
                    (new string[]{"8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"AE01=='FLAZZBCA'"}))),
                (new MasterProfile("NIQRETP5","CCNRET13",(new string[]{"DE04","AE38"}),
                    (new string[]{"SLCS","8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"","AE01=='FLAZZBCA'"}))),
                (new MasterProfile("NIQRETP6","CCNRET06",(new string[]{"DE04","AE38"}),
                    (new string[]{"SLCS","8182838485868788899091929394959697989900000000000000000000000000000000000000000000000000000000000000"}),
                    (new string[]{"","AE01=='FLAZZBCA'"}))),
                (new MasterProfile("NIQSPLT1","CCNHTL03",null,null)),
                (new MasterProfile("NIQSPLT2","CCNHTL04",null,null)),
                (new MasterProfile("NIQSTMC1","CCNSTMC1",null,null)),
                (new MasterProfile("NIQEAL01","CCNEAL01",null,null))
            };
            #endregion
            listMasterProfileTarget.AddRange(arrMstProfile);
        }

        static string sKeyConfigValue(string _sKeyName)
        {
            return ConfigurationManager.AppSettings.Get(_sKeyName);
        }

        static DataTable dtGetTerminalList(string _sCondition)
        {
            DataTable dtTemp = new DataTable();
            string sQuery = string.Format("SELECT DatabaseID, TerminalID \n{0} \n{1}",
                    "FROM tbProfileTerminalList WITH(NOLOCK)",
                    _sCondition);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        static string sGetDatabaseID(string _sDatabaseName)
        {
            string sValue = null;

            string sQuery = string.Format("select dbo.iDatabaseIdByName('{0}')", _sDatabaseName);
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (DataTable dtTemp = new DataTable())
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                if (string.IsNullOrEmpty(sValue))
                    sValue = dtTemp.Rows[0][0].ToString();
                else
                    sValue = string.Format("{0},{1}", sValue, dtTemp.Rows[0][0]);
            }
            return sValue;
        }

        static DataTable dtGetProfileTable(string _sTerminalID)
        {
            DataTable dtTemp = new DataTable();
            try
            {
                using (SqlCommand oCmd = new SqlCommand("spProfileTextFullTable", oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 0;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalID;
                    (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                }
            }
            catch (Exception ex)
            {
                Display(string.Format("Error Data Profile : {0}",ex.Message));
            }
            return dtTemp;
        }

        static void Display(string _sMessage)
        {
            Console.Write("[{0:ddd, MMM dd, yyyy hh:mm:ss.fff tt}] {1}\n", DateTime.Now, _sMessage);
            WriteLog(_sMessage);
        }

        static void WriteLog(string _sMessage)
        {
            string sLogDirectory = string.Format(@"{0}\LOGS", Environment.CurrentDirectory);
            if (!Directory.Exists(sLogDirectory))
                Directory.CreateDirectory(sLogDirectory);
            string sFilelog = string.Format(@"{0}\{1:yyyyMMdd}.log", sLogDirectory, DateTime.Now);

            using (StreamWriter sw = new StreamWriter(sFilelog, true))
            {
                sw.WriteLine(string.Format("[{0:ddd, MMM dd, yyyy hh:mm:ss.fff tt}] {1}", DateTime.Now, _sMessage));
            }
        }

        static void WriteLogTagInsert(string _sMessage)
        {
            string sLogDirectory = string.Format(@"{0}\LOGSTagInsert", Environment.CurrentDirectory);
            if (!Directory.Exists(sLogDirectory))
                Directory.CreateDirectory(sLogDirectory);
            string sFilelog = string.Format(@"{0}\{1:yyyyMMdd}TagInsert.log", sLogDirectory, DateTime.Now);

            using (StreamWriter sw = new StreamWriter(sFilelog, true))
            {
                sw.WriteLine(string.Format("[{0:ddd, MMM dd, yyyy hh:mm:ss.fff tt}] {1}", DateTime.Now, _sMessage));
            }
        }

        static void DefineMasterProfile()
        {
            int iDatabaseID;
            foreach (string sDatabaseName in arrsSourceDatabase)
            {
                iDatabaseID = int.Parse(sGetDatabaseID(sDatabaseName));
                DataTable dtTerminalMasterSource = dtGetTerminalList(string.Format("WHERE DatabaseID={0} AND StatusMaster=1", iDatabaseID));
                DataTable dtTerminal = new DataTable();
                if (dtTerminalMasterSource != null && dtTerminalMasterSource.Rows.Count > 0)
                {
                    foreach (DataRow rowTerminalMaster in dtTerminalMasterSource.Rows)
                    {
                        string sTerminalID = rowTerminalMaster["TerminalID"].ToString();
                        dtTerminal = new DataTable();
                        dtTerminal = dtGetProfileTable(sTerminalID);

                        Terminal tempTerminal = terminalGetFromDataTable(iDatabaseID, sTerminalID, dtTerminal);
                        listMasterSource.Add(tempTerminal);
                    }
                }
            }
        }

        static Terminal terminalGetFromDataTable(int _iDatabaseID, string _sTerminalID, DataTable _dtTemp)
        {
            #region "Terminal"
            Terminal tempTerminal = new Terminal();
            tempTerminal.TerminalID = _sTerminalID;
            tempTerminal.DatabaseID = _iDatabaseID;
            tempTerminal.Category = (_dtTemp.Select("Tag='DE05'"))[0]["TagValue"].ToString();
            #endregion

            #region "Acquirer"
            DataRow[] rowsAcquirer = _dtTemp.Select("Tag='AA01' AND Name NOT LIKE 'PROMO SQ%' AND Name NOT IN ('PROMO F','PROMO G','PROMO H','PROMO I','PROMO J','AMEX','DINERS')");
            foreach (DataRow rowAcq in rowsAcquirer)
            {
                Acquirer acqTemp = new Acquirer();
                acqTemp.Name = rowAcq["Name"].ToString();
                tempTerminal.ListAcquirer.Add(acqTemp);
            }
            #endregion

            #region "Issuer"
            DataRow[] rowsIssuer = _dtTemp.Select("Tag='AE01' AND Name NOT IN('VISA SQ')");
            foreach (DataRow rowIss in rowsIssuer)
            {
                Issuer issTemp = new Issuer();
                issTemp.Name = rowIss["Name"].ToString();
                issTemp.PaymentCoBrandCode = (_dtTemp.Select(string.Format("Tag='AE38' AND Name='{0}'",issTemp.Name)))[0]["TagValue"].ToString();
                tempTerminal.ListIssuer.Add(issTemp);
            }
            #endregion

            #region "Card"
            //List<Card> listcardTemp = new List<Card>();
            //DataRow[] rowsCard = dtTerminal.Select("Tag='AC01'");
            //foreach (DataRow rowCard in rowsCard)
            //{
            //    Card cardTemp = new Card();
            //    cardTemp.Name = rowCard["Name"].ToString();
            //    listcardTemp.Add(cardTemp);
            //}
            #endregion

            return tempTerminal;
        }

        static string sRandomTerminalID()
        {
            int iRandom1 = (new Random()).Next(9999);
            int iRandom2 = (new Random()).Next(9999);

            return string.Format("{0}{1}", iRandom1, iRandom2).PadLeft(8, '0');
        }
    }
}
