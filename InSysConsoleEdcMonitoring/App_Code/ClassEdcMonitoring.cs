﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace InSysConsoleEdcMonitoring
{
    public class ClassEdcMonitoring
    {
        public static string sConnString;
        public static string sConnStringAuditTrail;

        public static SqlConnection oConn;
        public static SqlConnection oConnAuditTrail;

        public List<tlvMessage> listTagMessage = new List<tlvMessage>();
        public List<tlvMessage> listTempTagMessage = new List<tlvMessage>();


        //public List<tlvCard> listCard = new List<tlvCard>();
        //public List<tlvSIM> listSIM = new List<tlvSIM>();
        // public List<tlvAcquirer> listAcquirer = new List<tlvAcquirer>();

        string sTerminalID;
        string sRawMessage;
        string sProcCode;

        const string sSPEdcMonitorInsert = "spEdcMonitorInsert";
        const string sSPEdcMonitorDetailInsert = "spEdcMonitorDetailInsert";
        const string sSPEdcMonitorSIMInsert = "spEdcMonitorSIMInsert";
        const string sSPGetTIDorMID = "spGetTIDorMID";
        const string sSPHEartBeatInsert = "spHEartBeatInsert";
        const string sSPGetTIDMIDTemp = "spGetTIDMIDTemp";
        const string sSPInsertAcqSequenceTemp = "spInsertAcqSequenceTemp";

        public ClassEdcMonitoring(string _sTerminalID, string _sRawMessage, string _sProcCode) : this()
        {
            sTerminalID = _sTerminalID;
            sRawMessage = _sRawMessage;
            sProcCode = _sProcCode;
        }

        public ClassEdcMonitoring()
        {
            oConn = new SqlConnection(sConnString);
            oConnAuditTrail = new SqlConnection(sConnStringAuditTrail);
        }

        public void PreProcessMsg(string sMessage)
        {
            

            string sMessageHex = CommonLib.sStringToHex(sMessage);
            //string[] lines = Regex.Split(sMessageHex, "4d4801".ToLower());
            string sTag, sLength, sTagValue;
            int iIndex = 0;
            while (iIndex < sMessageHex.Length)
                {
                    sTag = sMessageHex.Substring(iIndex, 6);
                    iIndex += 6;
                    sLength = sMessageHex.Substring(iIndex, 2);
                    int iLength = CommonLib.iHexStringToInt(sLength);
                    iIndex += 2;
                    sTagValue = sMessageHex.Substring(iIndex, iLength * 2);
                    tlvMessage tlvTemp = new tlvMessage(sTag, sLength, sTagValue);
                    listTagMessage.Add(tlvTemp);
                    iIndex += (iLength * 2);
                }
            
        }

         class MsgMonitoring
        {
            public string sSerialNumber;
            public string sSoftwareVersion;
            public string sLifetimeCounter;
            public string sTotalAllTransaction;
            public string sTotalSwipe;
            public string sTotalDip;
            public string sApprovedSettlementDate;
            public string sICCID;
            public string sAcquirerName;
            public string sTID;
            public string sMID;
            public int sTower;
            public string sMCC;
            public string sLAC;
            public string sCellID;
            public List<tlvCard> listCard = new List<tlvCard>();
            public List<tlvSIM> listSIM = new List<tlvSIM>();
            public List<tlvAcq> listAcq = new List<tlvAcq>();
            public List<ctlvTIDMID> listTIDMID = new List<ctlvTIDMID>();

        }
        List<MsgMonitoring> listMsgMonitoring = new List<MsgMonitoring>();

        public void fillTlvTIDMID(ref List<ctlvTIDMID> listTIDMID, int iParamIndex)
        {
            string TID, MID, sAcq;
            if (listTagMessage.Count > 0)
            {

                // Acquirer Name
                int iIndexAcq = listTempTagMessage.FindIndex(x => x.Tag.ToLower() == "4D4202".ToLower());
                int iIndexTIDMID = listTempTagMessage.FindIndex(x => x.Tag.ToLower() == "4D4203".ToLower());
                while ((iIndexAcq >= 0) && (iIndexTIDMID >= 0))
                {
                    tlvMessage tlvMsgTemp = listTempTagMessage[iIndexAcq];
                    sAcq = tlvMsgTemp.TagValue;

                    tlvMessage tlvMsgTempTIDMID = listTempTagMessage[iIndexTIDMID];
                    TID = tlvMsgTempTIDMID.TagValue.Substring(0, 8);
                    MID = tlvMsgTempTIDMID.TagValue.Substring(9, 13);

                    iIndexTIDMID++;
                    iIndexTIDMID = listTempTagMessage.FindIndex(iIndexTIDMID, x => x.Tag.ToLower() == "4D4203".ToLower());

                    iIndexAcq++;
                    iIndexAcq = listTempTagMessage.FindIndex(iIndexAcq, x => x.Tag.ToLower() == "4D4202".ToLower());
                    InsertToTabletemp(sTerminalID, sAcq, TID, MID);

                }

                //int iIndexTIDMID = listTempTagMessage.FindIndex(x => x.Tag.ToLower() == "4D4203".ToLower());
                //while (iIndexTIDMID >= 0)
                //{
                //    tlvMessage tlvMsgTempTIDMID = listTempTagMessage[iIndexTIDMID];
                //    TID = tlvMsgTempTIDMID.TagValue.Substring(0, 8);
                //    MID = tlvMsgTempTIDMID.TagValue.Substring(9, 13);

                //    iIndexTIDMID++;

                //    ctlvTIDMID tlvTempTIDMID = new ctlvTIDMID(TID, MID);
                //    listTIDMID.Add(tlvTempTIDMID);
                //    iIndexTIDMID = listTempTagMessage.FindIndex(iIndexTIDMID, x => x.Tag.ToLower() == "4D4203".ToLower());
                //}

            }

        }
        private void InsertToTabletemp(string sTerminalID, string sAcquirer, string sTID, string sMID)
        {
            string sQuery = string.Format("INSERT INTO tbEdcMonitorTempAcqSeq (TerminalID, AcquirerName,TID, MID)");
            sQuery += string.Format(" VALUES  {0},{1},{2},{3} ", "('" + sTerminalID + "'", "'" + sAcquirer + "'", "'" + sTID + "'", "'" + sMID + "')");
            SqlCommand sqlcmd = new SqlCommand(sQuery, oConn);
            sqlcmd.CommandTimeout = 60;
            if (oConn.State != ConnectionState.Open)
                oConn.Open();
            sqlcmd.ExecuteNonQuery();
        }

        public void ProcessMsg(string sFlagMsg)
        {
            var result = Enumerable.Range(0, listTagMessage.Count).Where(i => listTagMessage[i].Tag == "4d4801").ToList();
            int loop = 1;
            int maxCount = (listTagMessage.Count);
            int a = 0;
            foreach (var vIndex in result)
            {
                
                if (loop < result.Count)
                {
                    a = result[loop] - result[loop-1];
                }
                else
                {
                     a = maxCount-vIndex + 1;
            
                }
                //var lNewList = listTagMessage.ToList().GetRange(vIndex, a - 1);
                //listTempTagMessage = listTagMessage.GetRange(vIndex, a - 1);
                listTempTagMessage = listTagMessage.GetRange(vIndex, a - 1);
                loop++;
                MsgMonitoring tempMsgMonitoring = new MsgMonitoring();
                tempMsgMonitoring.sSerialNumber = sSearchTagValue("4D4801", (int)vIndex);
                tempMsgMonitoring.sSoftwareVersion = sSearchTagValue("4D4802", (int)vIndex);
                tempMsgMonitoring.sApprovedSettlementDate = sSearchTagValue("4D4201", (int)vIndex);
                tempMsgMonitoring.sICCID = sSearchTagValue("4D4705", (int)vIndex);
                tempMsgMonitoring.sLifetimeCounter = sSearchTagValue("4D4803", (int)vIndex);
                tempMsgMonitoring.sTotalAllTransaction = sSearchTagValue("4D4804", (int)vIndex);
                tempMsgMonitoring.sTotalSwipe = sSearchTagValue("4D4805", (int)vIndex);
                tempMsgMonitoring.sTotalDip = sSearchTagValue("4D4806", (int)vIndex);

                if (sFlagMsg == "monitoring")
                {
                    tempMsgMonitoring.sAcquirerName = sSearchTagValue("4D4202", (int)vIndex);
                    fillTlvCardList(ref tempMsgMonitoring.listCard, (int)vIndex, tempMsgMonitoring.sAcquirerName);
                    fillTlvSIM(ref tempMsgMonitoring.listSIM, (int)vIndex);
                }
                ////tempMsgMonitoring.sTID = (sSearchTagValue("4D4203", (int)vIndex)).Substring(0, 8);
                //tempMsgMonitoring.sMID = (sSearchTagValue("4D4203", (int)vIndex)).Substring(9, 13);
                if (sFlagMsg == "heartbeat")
                {
                    fillTlvSIMToHeartBeat(ref tempMsgMonitoring.listSIM, (int)vIndex, ref tempMsgMonitoring.sTower, ref tempMsgMonitoring.sMCC, ref tempMsgMonitoring.sLAC, ref tempMsgMonitoring.sCellID);
                    //fillTlvTIDMID(ref tempMsgMonitoring.listTIDMID, (int)vIndex);
                }
                listMsgMonitoring.Add(tempMsgMonitoring);
                listTempTagMessage.Clear();
            }
        }

        protected void fillTlvSIM(ref List<tlvSIM> listSIM,int iParamIndex)
        {
            if (listTagMessage.Count > 0)
            {
                //int iIndexSIM = listTagMessage.FindIndex(x => x.Tag.ToLower() == "4D4701".ToLower());
                int iIndexSIM = listTagMessage.FindIndex(x => x.Tag.ToLower() == "4D4701".ToLower());
                while (iIndexSIM >= 0)
                {
                    tlvMessage tlvMsgTempMCCMNC = listTagMessage[iIndexSIM];
                    string sMCCMNC = tlvMsgTempMCCMNC.TagValue;
                    
                    iIndexSIM++;
                    tlvMessage tlvMsgTempLAC = listTagMessage[iIndexSIM];
                    string sLAC = tlvMsgTempLAC.TagValue;

                    iIndexSIM++;
                    tlvMessage tlvMsgTempCellID = listTagMessage[iIndexSIM];
                    string sCellID = tlvMsgTempCellID.TagValue;

                    iIndexSIM++;
                    tlvMessage tlvMsgTempTowerNumber = listTagMessage[iIndexSIM];
                    string sTowerNumber = tlvMsgTempCellID.TagValue;

                    tlvSIM tlvSIMTemp = new tlvSIM(int.Parse(sTowerNumber), sMCCMNC, sLAC, sCellID);
                    listSIM.Add(tlvSIMTemp);

                    iIndexSIM = listTagMessage.FindIndex(iIndexSIM, x => x.Tag.ToLower() == "4D4701".ToLower());
                }
            }
        }
        protected void fillTlvAcq(ref List<tlvAcq> listAcq, int iParamIndex)
        {
            if (listTagMessage.Count > 0)
            {
                //int iIndexSIM = listTagMessage.FindIndex(x => x.Tag.ToLower() == "4D4701".ToLower());
                int iIndexSIM = listTagMessage.FindIndex(x => x.Tag.ToLower() == "4D4202".ToLower());
                while (iIndexSIM >= 0)
                {
                    tlvMessage tlvMsgTempAcq = listTagMessage[iIndexSIM];
                    string sAcquirerName = tlvMsgTempAcq.TagValue;
                    iIndexSIM++;
                    tlvAcq tlvAcqTemp = new tlvAcq(sAcquirerName);
                    listAcq.Add(tlvAcqTemp);
                    iIndexSIM = listTagMessage.FindIndex(iIndexSIM, x => x.Tag.ToLower() == "4D4202".ToLower());
                }
            }
        }
        protected void fillTlvSIMToHeartBeat(ref List<tlvSIM> listSIM, int iParamIndex, ref int Tower, ref string Mcc, ref string Lac, ref string CellID)
        {
            if (listTagMessage.Count > 0)
            {
                int iIndexSIM = listTagMessage.FindIndex(x => x.Tag.ToLower() == "4D4701".ToLower());
                while (iIndexSIM >= 0)
                {
                    tlvMessage tlvMsgTempMCCMNC = listTagMessage[iIndexSIM];
                    Mcc = tlvMsgTempMCCMNC.TagValue;

                    iIndexSIM++;
                    tlvMessage tlvMsgTempLAC = listTagMessage[iIndexSIM];
                     Lac = tlvMsgTempLAC.TagValue;

                    iIndexSIM++;
                    tlvMessage tlvMsgTempCellID = listTagMessage[iIndexSIM];
                     CellID = tlvMsgTempCellID.TagValue;

                    iIndexSIM++;
                    tlvMessage tlvMsgTempTowerNumber = listTagMessage[iIndexSIM];
                     Tower = int.Parse(tlvMsgTempCellID.TagValue);

                    //tlvSIM tlvSIMTemp = new tlvSIM(int.Parse(Tower), Mcc, sLAC, sCellID);
                    //listSIM.Add(tlvSIMTemp);

                    iIndexSIM = listTagMessage.FindIndex(iIndexSIM, x => x.Tag.ToLower() == "4D4701".ToLower());
                }
            }
        }

        public bool isSaveMsgSuccess()
        {
            //run query to save to database 
            foreach (MsgMonitoring list in listMsgMonitoring)
            {
                
                #region save
                try
                {
                    int iID_Header = 0;
                    if (oConn.State != ConnectionState.Open) oConn.Open();
                    using (SqlCommand sqlcmd = new SqlCommand(sSPEdcMonitorInsert, oConn))
                    {
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add("@sRawMsg", SqlDbType.VarChar).Value = sRawMessage;
                        sqlcmd.Parameters.Add("@sProcCode", SqlDbType.VarChar).Value = sProcCode;
                        sqlcmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                        sqlcmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = list.sSerialNumber;
                        sqlcmd.Parameters.Add("@sSoftwareVersion", SqlDbType.VarChar).Value = list.sSoftwareVersion;
                        sqlcmd.Parameters.Add("@iLifetimeCounter", SqlDbType.Int).Value = int.Parse(list.sLifetimeCounter);
                        sqlcmd.Parameters.Add("@iTotalAllTransaction", SqlDbType.Int).Value = int.Parse(list.sTotalAllTransaction);
                        sqlcmd.Parameters.Add("@iTotalSwipe", SqlDbType.Int).Value = int.Parse(list.sTotalSwipe);
                        sqlcmd.Parameters.Add("@iTotalDip", SqlDbType.Int).Value = int.Parse(list.sTotalDip);
                        sqlcmd.Parameters.Add("@sSettlementDateTime", SqlDbType.VarChar).Value = list.sApprovedSettlementDate;
                        sqlcmd.Parameters.Add("@sICCID", SqlDbType.VarChar).Value = list.sICCID;
                        sqlcmd.Parameters.Add("@sTID", SqlDbType.VarChar).Value = sGetTIDorMID(sTerminalID, list.sAcquirerName, "TID");//list.sTID;
                        sqlcmd.Parameters.Add("@sMID", SqlDbType.VarChar).Value = sGetTIDorMID(sTerminalID, list.sAcquirerName, "MID");//list.sMID;
                        sqlcmd.Parameters.Add("@iID_Header", SqlDbType.Int).Direction = ParameterDirection.Output;

                        sqlcmd.ExecuteNonQuery();
                        iID_Header = (int)sqlcmd.Parameters["@iID_Header"].Value;
                    }

                    if (iID_Header > 0 && list.listCard.Count > 0)
                    {
                       // isSaveEdcMonitorDetail(iID_Header, list);
                        foreach (tlvCard cardTemp in list.listCard)
                        {
                            using (SqlCommand sqlcmdDetail = new SqlCommand(sSPEdcMonitorDetailInsert, oConn))
                            {
                                sqlcmdDetail.CommandType = CommandType.StoredProcedure;
                                sqlcmdDetail.Parameters.Add("@iID_Header", SqlDbType.Int).Value = iID_Header;
                                sqlcmdDetail.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                                sqlcmdDetail.Parameters.Add("@sSoftwareVersion", SqlDbType.VarChar).Value = list.sSoftwareVersion;
                                sqlcmdDetail.Parameters.Add("@sSettlementDateTime", SqlDbType.VarChar).Value = list.sApprovedSettlementDate;
                                sqlcmdDetail.Parameters.Add("@sCardName", SqlDbType.VarChar).Value = cardTemp.CardName;
                                sqlcmdDetail.Parameters.Add("@iCardTotalAmount", SqlDbType.Int).Value = cardTemp.TotalAmount;
                                sqlcmdDetail.Parameters.Add("@iCardTotalTrx", SqlDbType.Int).Value = cardTemp.TotalTrx;
                                sqlcmdDetail.Parameters.Add("@sAcquirerName", SqlDbType.VarChar).Value = cardTemp.AcqName;
                                sqlcmdDetail.Parameters.Add("@sTID", SqlDbType.VarChar).Value = sGetTIDorMID(sTerminalID, list.sAcquirerName, "TID");//list.sTID; 
                                sqlcmdDetail.Parameters.Add("@sMID", SqlDbType.VarChar).Value = sGetTIDorMID(sTerminalID, list.sAcquirerName, "MID");//list.sMID; 
                                sqlcmdDetail.ExecuteNonQuery();
                            }
                        }
                    }
                    if (iID_Header > 0 && list.listSIM.Count > 0)
                    {
                        foreach (tlvSIM simTemp in list.listSIM)
                        {
                            using (SqlCommand sqlcmdDetail = new SqlCommand(sSPEdcMonitorSIMInsert, oConn))
                            {
                                sqlcmdDetail.CommandType = CommandType.StoredProcedure;
                                sqlcmdDetail.Parameters.Add("@iID_Header", SqlDbType.Int).Value = iID_Header;
                                sqlcmdDetail.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                                sqlcmdDetail.Parameters.Add("@sSoftwareVersion", SqlDbType.VarChar).Value = list.sSoftwareVersion;
                                sqlcmdDetail.Parameters.Add("@sMCCMNC", SqlDbType.VarChar).Value = simTemp.MCCMNC;
                                sqlcmdDetail.Parameters.Add("@sLAC", SqlDbType.VarChar).Value = simTemp.LAC;
                                sqlcmdDetail.Parameters.Add("@sCellID", SqlDbType.Int).Value = simTemp.CellID;
                                sqlcmdDetail.Parameters.Add("@iTowerNumber", SqlDbType.Int).Value = simTemp.TowerNumber;
                                sqlcmdDetail.ExecuteNonQuery();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Trace.Write("EdcMonitoring Save Failed : " + ex.Message);
                    return false;
                }
                #endregion save
                //return true;
                
            }
            return true;
        }

        public void InsertAcqTIDMID(string _sTerminalID)
        {
            using (SqlCommand sqlcommand = new SqlCommand(sSPInsertAcqSequenceTemp, oConn))
            {
                if (oConn.State != ConnectionState.Open) oConn.Open();
                sqlcommand.CommandType = CommandType.StoredProcedure;
                sqlcommand.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                sqlcommand.ExecuteNonQuery();
            }
        }

        public string sGetTIDorMID(string _sTerminalID, string _sAcquirerName, string _sFlag)
        {
            string sResult = "";
            using (SqlCommand sqlcommand = new SqlCommand(sSPGetTIDorMID, oConn))
            {
                if (oConn.State != ConnectionState.Open) oConn.Open();
                sqlcommand.CommandType = CommandType.StoredProcedure;
                sqlcommand.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                sqlcommand.Parameters.Add("@sAcquirerName", SqlDbType.VarChar).Value = _sAcquirerName;
                sqlcommand.Parameters.Add("@sFLAG", SqlDbType.VarChar).Value = _sFlag;
                SqlDataReader sqldatareader = sqlcommand.ExecuteReader();
                if (sqldatareader.Read())
                {
                    sResult = sqldatareader.GetString(0);
                }
            }
            return sResult;
        }


        public string sGetTIDMID(string sFlag,string sTerminalID)
        {
            string _sValue = "";

            SqlDataReader sqldatareader = null;
            try
            {
                SqlCommand sqlcmd = new SqlCommand(sSPGetTIDMIDTemp, oConn);
                sqlcmd.CommandTimeout = 60;
                if (oConn.State != ConnectionState.Open)
                    oConn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sFlag", SqlDbType.VarChar).Value = sFlag;
                sqlcmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                sqldatareader = sqlcmd.ExecuteReader();
                if (sqldatareader.Read())
                {
                    _sValue = sqldatareader.GetString(0);
                }
            }
            catch (Exception ex)
            {
                Trace.Write("EdcMonitoring Hearbeat Read Data Failed : " + ex.Message);
            }               

            return _sValue;
        }
        private void DeleteTabletemp()
        {
            string sQuery = string.Format("TRUNCATE TABLE tbEdcMonitorTempAcqSeq");
            SqlCommand sqlcmd = new SqlCommand(sQuery, oConn);
            sqlcmd.CommandTimeout = 60;
            if (oConn.State != ConnectionState.Open) oConn.Open();
            sqlcmd.ExecuteNonQuery();
        }
        public bool iSaveHeartBeat()
        {
            try
            {               
                //tlvSIM a = tlvSIM simTemp in  list.listSIM;
                if (oConn.State != ConnectionState.Open) oConn.Open();
                int iID_Header = 0;
                //tlvSIM lis2t = MsgMonitoring;

                InsertAcqTIDMID(sTerminalID);

                foreach (MsgMonitoring list in listMsgMonitoring)
                {
                    
                    using (SqlCommand sqlcmd = new SqlCommand(sSPHEartBeatInsert, oConn))
                    {
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.Add("@sRawMsg", SqlDbType.VarChar).Value = sRawMessage;
                        sqlcmd.Parameters.Add("@sProcCode", SqlDbType.VarChar).Value = sProcCode;
                        sqlcmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                        sqlcmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = list.sSerialNumber;
                        sqlcmd.Parameters.Add("@sSoftwareVersion", SqlDbType.VarChar).Value = list.sSoftwareVersion;
                        if (!string.IsNullOrEmpty(list.sLifetimeCounter))
                        sqlcmd.Parameters.Add("@iLifetimeCounter", SqlDbType.Int).Value = int.Parse(list.sLifetimeCounter);
                        if (!string.IsNullOrEmpty(list.sTotalAllTransaction))
                            sqlcmd.Parameters.Add("@iTotalAllTransaction", SqlDbType.Int).Value = int.Parse(list.sTotalAllTransaction);
                        if (!string.IsNullOrEmpty(list.sTotalSwipe))
                            sqlcmd.Parameters.Add("@iTotalSwipe", SqlDbType.Int).Value = int.Parse(list.sTotalSwipe);
                        if (!string.IsNullOrEmpty(list.sTotalDip))
                            sqlcmd.Parameters.Add("@iTotalDip", SqlDbType.Int).Value = int.Parse(list.sTotalDip);
                        sqlcmd.Parameters.Add("@sSettlementDateTime", SqlDbType.VarChar).Value = list.sApprovedSettlementDate;
                        sqlcmd.Parameters.Add("@sICCID", SqlDbType.VarChar).Value = list.sICCID;

                        sqlcmd.Parameters.Add("@sTID", SqlDbType.VarChar).Value = sGetTIDMID("TID",sTerminalID);
                        sqlcmd.Parameters.Add("@sMID", SqlDbType.VarChar).Value = sGetTIDMID("MID",sTerminalID);

                        //sqlcmd.Parameters.Add("@Mcc_Mnc_Data", SqlDbType.VarChar).Value = list.sMCC;
                        //sqlcmd.Parameters.Add("@Lac_Data", SqlDbType.VarChar).Value = list.sLAC;
                        //sqlcmd.Parameters.Add("@Cell_Id_Data", SqlDbType.VarChar).Value = list.sCellID;
                        //sqlcmd.Parameters.Add("@TowerNumber", SqlDbType.Int).Value = list.sTower;

                        sqlcmd.Parameters.Add("@iID_Header", SqlDbType.Int).Direction = ParameterDirection.Output;
                        sqlcmd.ExecuteNonQuery();

                        DeleteTabletemp();

                        iID_Header = (int)sqlcmd.Parameters["@iID_Header"].Value;

                        if (iID_Header > 0 && list.listSIM.Count > 0)
                        {
                            foreach (tlvSIM simTemp in list.listSIM)
                            {
                                using (SqlCommand sqlcmdDetail = new SqlCommand(sSPEdcMonitorSIMInsert, oConn))
                                {
                                    sqlcmdDetail.CommandType = CommandType.StoredProcedure;
                                    sqlcmdDetail.Parameters.Add("@iID_Header", SqlDbType.Int).Value = iID_Header;
                                    sqlcmdDetail.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                                    sqlcmdDetail.Parameters.Add("@sSoftwareVersion", SqlDbType.VarChar).Value = list.sSoftwareVersion;
                                    sqlcmdDetail.Parameters.Add("@sMCCMNC", SqlDbType.VarChar).Value = simTemp.MCCMNC;
                                    sqlcmdDetail.Parameters.Add("@sLAC", SqlDbType.VarChar).Value = simTemp.LAC;
                                    sqlcmdDetail.Parameters.Add("@sCellID", SqlDbType.Int).Value = simTemp.CellID;
                                    sqlcmdDetail.Parameters.Add("@iTowerNumber", SqlDbType.Int).Value = simTemp.TowerNumber;
                                    sqlcmdDetail.ExecuteNonQuery();
                                }
                            }
                        }
                    }


                }
            }
            catch (Exception ex)
            {
               return false;
            }
            return true;
        }

        protected string sSearchTagValue(string sTag, int iParamIndex)
        {
            string sTemp = null;
            try
            {
                if (listTagMessage.Count > 0)
                {
                    iParamIndex = listTagMessage.FindIndex(iParamIndex, x => x.Tag.ToLower() == sTag.ToLower());
                    //iParamIndex = listTagMessage.FindIndex(x => x.Tag.ToLower() == sTag.ToLower());
                    tlvMessage tlvSearch = listTagMessage[iParamIndex];
                    //iParamIndex = listTagMessage.FindIndex(iParamIndex, x=> x.Tag.ToLower() == sTag.ToLower());
                    if (tlvSearch != null) sTemp = tlvSearch.TagValue;
                }
                
            }
            catch (Exception ex)
            {

            }
            return sTemp;
        }

        public void fillTlvCardList(ref List<tlvCard> listCard, int iParamIndex, string sAcqName)
        {
            string acqName = sAcqName;
            if (listTagMessage.Count>0)
            {

                int iIndexCard = listTempTagMessage.FindIndex(x => x.Tag.ToLower() == "4D4301".ToLower());
              //  int iIndexCard = list.FindIndex(x => x.Tag.ToLower() == "4D4301".ToLower());
                while (iIndexCard>=0)
                {
                    tlvMessage tlvMsgTempCardName = listTempTagMessage[iIndexCard];
                    string sCardName = tlvMsgTempCardName.TagValue;

                    iIndexCard++;
                    tlvMessage tlvMsgTempCardTotalAmount = listTempTagMessage[iIndexCard];
                    string sCardTotalAmount = tlvMsgTempCardTotalAmount.TagValue;

                    iIndexCard++;
                    tlvMessage tlvMsgTempCardTotalTrx = listTempTagMessage[iIndexCard];
                    string sCardTotalTrx = tlvMsgTempCardTotalTrx.TagValue;

                    tlvCard tlvCardTemp = new tlvCard(sCardName, sCardTotalAmount, sCardTotalTrx, acqName);
                    listCard.Add(tlvCardTemp);
                    iIndexCard = listTempTagMessage.FindIndex(iIndexCard, x => x.Tag.ToLower() == "4D4301".ToLower());
                }
            }
        }



    }

    public class tlvMessage
    {
        public string Tag { get { return sTag; } }
        public string TagValue { get { return sMessageAscii; } }
        public int Length { get { return iLength; } }

        protected string sTag;
        protected string sLength;
        protected string sMessage;
        protected string sMessageAscii;
        protected int iLength;

        public tlvMessage(string _sTag, string _sLength, string _sMessage)
        {
            sTag = _sTag;
            sLength = _sLength;
            sMessage = _sMessage;
            iLength = CommonLib.iHexStringToInt(sLength);
            sMessageAscii = CommonLib.sHexToStringUTF8(sMessage);
        }
    }

    public class tlvTempMessage
    {
        public string Tag { get { return sTag; } }
        public string TagValue { get { return sMessageAscii; } }
        public int Length { get { return iLength; } }

        protected string sTag;
        protected string sLength;
        protected string sMessage;
        protected string sMessageAscii;
        protected int iLength;

        public tlvTempMessage(string _sTag, string _sLength, string _sMessage)
        {
            sTag = _sTag;
            sLength = _sLength;
            sMessage = _sMessage;
            iLength = CommonLib.iHexStringToInt(sLength);
            sMessageAscii = CommonLib.sHexToStringUTF8(sMessage);
        }
    }

    public class tlvCard
    {
        public string CardName { get { return sCardName; } }
        public int TotalAmount { get { return int.Parse(sCardTotalAmount); } }
        public int TotalTrx { get { return int.Parse(sCardTotalTrx); } }

        public string AcqName { get { return sAcqName; } }

        protected string sCardName;
        protected string sCardTotalAmount;
        protected string sCardTotalTrx;
        protected string sAcqName;

        public tlvCard(string _sCardName, string _sCardTotalAmount, string _sCardTotalTrx, string _sAcqName)
        {
            sCardName = _sCardName;
            sCardTotalAmount = _sCardTotalAmount;
            sCardTotalTrx = _sCardTotalTrx;
            sAcqName = _sAcqName;
        }
    }


    public class tlvSIM
    {
        public int TowerNumber { get { return iTowerNumber; } }
        public string MCCMNC { get { return sMCCMNC; } }
        public string LAC { get { return sLAC; } }
        public string CellID { get { return sCellID; } }

        protected int iTowerNumber;
        protected string sMCCMNC;
        protected string sLAC;
        protected string sCellID;

        public tlvSIM(int _iTowerNumber, string _sMCCMNC, string _sLAC, string _sCellID)
        {
            iTowerNumber = _iTowerNumber;
            sMCCMNC = _sMCCMNC;
            sLAC = _sLAC;
            sCellID = _sCellID;
        }
    }

    public class tlvAcq
    {
        public string AcquirerName { get { return sAcquirerName; } }

        protected string sAcquirerName;

        public tlvAcq(string _sAcquirerName)
        {
            sAcquirerName = _sAcquirerName;
        }
    }
    public class ctlvTIDMID
    {
        public string TID { get { return sTID; } }
        public string MID { get { return sMID; } }

        protected string sTID;
        protected string sMID;

        public ctlvTIDMID(string _sTID, string _sMID)
        {
            sTID = _sTID;
            sMID = _sMID;
        }
    }
    public class ClassHeader
    {
        public int TowerNumber { get { return iTowerNumber; } }
        public string MCCMNC { get { return sMCCMNC; } }
        public string LAC { get { return sLAC; } }
        public string CellID { get { return sCellID; } }

        protected int iTowerNumber;
        protected string sMCCMNC;
        protected string sLAC;
        protected string sCellID;

        //public tlvSIM(int _iTowerNumber, string _sMCCMNC, string _sLAC, string _sCellID)
        //{
        //    iTowerNumber = _iTowerNumber;
        //    sMCCMNC = _sMCCMNC;
        //    sLAC = _sLAC;
        //    sCellID = _sCellID;
        //}
    }
}
