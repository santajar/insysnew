﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace InSysConsoleEdcMonitoring
{
    class Program
    {
        static bool bCheckAllowInit;
        static SqlConnection oConn = new SqlConnection();
        static SqlConnection oConnAuditTrail = new SqlConnection();
        static string sConnString;
        static string sConnStringAuditTrail;
        static string sAppDirectory = Directory.GetCurrentDirectory();

        public static List<ClassInit> ltClassInit = new List<ClassInit>();
        public static DataTable dtISO8583 = new DataTable();
        static bool bDebug = true;

        #region "TCP/IP Connection"
        static bool IsConnTcpIpActive;
        static Thread threadIP;
        #endregion
        
        /// <summary>
        /// Initialize Component
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                bDebug = bool.Parse(ConfigurationManager.AppSettings["Debug"].ToString());
                AppDomain.CurrentDomain.ProcessExit += new EventHandler(ProcessExit);

                oConn = InitConnection();
                oConnAuditTrail = InitConnectionAuditTrail();

                if (oConn != null && oConn.State == ConnectionState.Open && oConnAuditTrail != null && oConnAuditTrail.State == ConnectionState.Open)
                {
                    //InitTableSoftwareISO(); tutup krn dbmonitor
                    InitControlAllowInit();
                    AsyncSocket.oConn = oConn;
                    AsyncSocket.oConnAuditTrail = oConnAuditTrail;
                    AsyncSocket.bCheckAllowInit = bCheckAllowInit;
                    AsyncSocket.sConnString = sConnString;
                    AsyncSocket.sConnStringAuditTrail = sConnStringAuditTrail;
                    AsyncSocket.bDebug = bDebug;
                    AsyncSocket.iMaxBytePerPacket = int.Parse(ConfigurationManager.AppSettings["MaxBytePerPacket"].ToString());
                    int iMaxConn = int.Parse(ConfigurationManager.AppSettings["MaxConn"].ToString());
                    AsyncSocket.iPort = int.Parse(ConfigurationManager.AppSettings["Port"].ToString());
                    AsyncSocket.iConnMax = iMaxConn;

                    ClassEdcMonitoring.sConnString = sConnString;
                    ClassEdcMonitoring.sConnStringAuditTrail = sConnStringAuditTrail;

                    IsConnTcpIpActive = bool.Parse(ConfigurationManager.AppSettings["TcpIp"].ToString());

                    // Start the TcpIp thread
                    if (IsConnTcpIpActive)
                    {
                        threadIP = new Thread(new ThreadStart(AsyncSocket.StartListening));
                        threadIP.Start();

                        CommonConsole.WriteToConsole("Ready...");
                    }
                }
                else
                {
                    CommonConsole.WriteToConsole("Database Connection Failed!...");
                    Console.WriteLine("Press ENTER to Terminate or R to Restart the program...");
                    string sKey = Console.ReadLine();
                    if (sKey.ToUpper() == "R")
                    {
                        Process oInSys = new Process();
                        oInSys.StartInfo.FileName = sAppDirectory + @"\InSysConsoleEdcMonitoring.exe";
                        oInSys.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.Write("[Main] Error : " + ex.StackTrace);
                //CommonConsole.WriteToConsole(ex.Message);
                //Console.Read();
                Process oInSys = new Process();
                oInSys.StartInfo.FileName = sAppDirectory + @"\InSysConsoleEdcMonitoring.exe";
                oInSys.Start();
            }
        }

        /// <summary>
        /// Stop Aplication
        /// </summary>
        static void ProcessExit(object sender, EventArgs e)
        {
            AsyncSocket.Stop();
        }

        /// <summary>
        /// Get data Bitmap in Database
        /// </summary>
        static void InitTableSoftwareISO()
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPBitmapProfileSoftwareBrowse, oConn))
            {
                if (oConn.State != ConnectionState.Open) oConn.Open();
                new SqlDataAdapter(oCmd).Fill(dtISO8583);
            }
        }

        #region "Function"
        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                sConnString = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnectionAuditTrail()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitDataAuditTrail oObjInit = new InitDataAuditTrail(sAppDirectory);
                sConnStringAuditTrail = oObjInit.sGetConnString();
                sConnStringAuditTrail = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnStringAuditTrail);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }

        /// <summary>
        /// Set the Allow Initialization configuration
        /// </summary>
        static void InitControlAllowInit()
        {
            bCheckAllowInit = bGetControlAllowInit();
        }

        /// <summary>
        /// Get the Allow Init configuration from the database
        /// </summary>
        /// <returns>bool : boolean allow to initialize or not</returns>
        static bool bGetControlAllowInit()
        {
            bool bTemp = false;
            try
            {
                if (oConn.State == ConnectionState.Closed)
                    oConn.Open();

                SqlCommand oCmd = new SqlCommand(CommonSP.sSPFlagControlAllowInit, oConn);
                oCmd.CommandType = CommandType.StoredProcedure;

                if (oConn.State != ConnectionState.Open) oConn.Open();

                SqlDataAdapter oda = new SqlDataAdapter(oCmd);
                DataTable dt = new DataTable();
                oda.Fill(dt);
                if (dt.Rows.Count > 0)
                    bTemp = Convert.ToInt16(dt.Rows[0][0].ToString()) == 1 ? true : false;

                oCmd.Dispose();
                oda.Dispose();
                dt.Dispose();
            }
            catch (Exception)
            {
                //Logs.doWriteErrorFile("[MAIN] InitControlAllowInit : " + ex.Message);
            }
            return bTemp;
        }
        
        /// <summary>
        /// Set Connection
        /// </summary>
        static void InitConnType()
        {
            InitConsoleConnType oConnType = new InitConsoleConnType(sAppDirectory);
            IsConnTcpIpActive = oConnType.IsConnTcpIpActive();
        }        
        #endregion
    }
}
