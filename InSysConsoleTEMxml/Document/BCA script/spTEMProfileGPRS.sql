
IF OBJECT_ID ('dbo.spTEMProfileGPRS') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileGPRS
GO

CREATE Procedure [dbo].[spTEMProfileGPRS]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
		
	SELECT DISTINCT(GPRSName) AS GPRSName 
	INTO #TempDis
	FROM tbProfileGPRS
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY GPRSName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.GPRSName,a.GPRSTag,
	'FF14.'+b.s_index+'.'+a.GPRSTag AS FIELD_ID ,a.GPRSTagValue AS VALUE
	FROM tbProfileGPRS a RIGHT JOIN #TempDisNo b ON a.GPRSName = b.GPRSName
	WHERE a.DatabaseID = @sDatabaseID	  
	ORDER BY a.DatabaseID,a.GPRSName,a.GPRSTag	
 
END

GO
