IF OBJECT_ID ('dbo.spTEMProfileMenu') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileMenu
GO

CREATE Procedure [dbo].[spTEMProfileMenu]
	@sTerminalID VARCHAR(8)
AS
BEGIN

	IF OBJECT_ID('TempDB..##Temp') IS NOT NULL
	DROP TABLE ##Temp

	SELECT TerminalID,'FF08.1.'+CustomMenuTag AS FIELD_ID ,CustomMenuTagValue as VALUE 
	FROM tbProfileCustomMenu WITH (NOLOCK) 
	WHERE TerminalID =  @sTerminalID 
	ORDER BY TerminalID, CustomMenuTag
 
END






GO

