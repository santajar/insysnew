IF OBJECT_ID ('dbo.spTEMGetSignature') IS NOT NULL
	DROP PROCEDURE dbo.spTEMGetSignature
GO

CREATE Procedure [dbo].[spTEMGetSignature]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempTerminal') IS NOT NULL
	DROP TABLE #TempTerminal

	SELECT TOP 200 TerminalID 
	INTO #TempTerminal
	FROM tbProfileTerminalList 
	WHERE DatabaseID = @sDatabaseID AND TerminalId IN (
	SELECT TerminalId FROM tbAuditInit WHERE Statusdesc LIKE  '%Complete'
	)
	
 	-- SELECT * FROM #TempTerminal
 	
	IF OBJECT_ID('TempDB..#TempInitID') IS NOT NULL
	DROP TABLE #TempInitID
	
	SELECT max(InitID) AS InitID,TerminalId
	INTO #TempInitID
	FROM tbAuditInit WITH (NOLOCK)
	WHERE Statusdesc LIKE  '%Complete'
	AND TerminalId IN (SELECT TerminalID FROM #TempTerminal)
	group by TerminalId

	IF OBJECT_ID('TempDB..#TempSNTerminalInit') IS NOT NULL
	DROP TABLE #TempSNTerminalInit
	
	SELECT a.InitID,a.InitTime,a.TerminalId,a.Software,a.SerialNumber
	INTO #TempSNTerminalInit
	FROM tbAuditInit a WITH (NOLOCK)
	INNER JOIN 
	#TempInitID	b ON a.InitID = b.InitID AND a.TerminalID = b.TerminalID
	
	--SELECT * FROM #TempSNTerminalInit
	
 	IF OBJECT_ID('TempDB..#TempSNTerminal') IS NOT NULL
	DROP TABLE #TempSNTerminal 	

	SELECT InitID,InitTime,TerminalID,Software,SerialNumber,
	ROW_NUMBER() OVER(PARTITION BY SerialNumber ORDER BY  InitID DESC) s_index
	INTO #TempSNTerminal
	FROM #TempSNTerminalInit 

	--SELECT * FROM #TempSNTerminal


 	IF OBJECT_ID('TempDB..#SNTerminal') IS NOT NULL
	DROP TABLE #SNTerminal 	
	
	SELECT InitID,InitTime,TerminalID,Software,SerialNumber
	INTO #SNTerminal
	FROM #TempSNTerminal
	WHERE s_index = 1
	
	SELECT * FROM #SNTerminal
	

 
END







GO

