
IF OBJECT_ID ('dbo.spTEMProfileCard') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileCard
GO

CREATE Procedure [dbo].[spTEMProfileCard]
	@sTerminalID VARCHAR(8)
AS
BEGIN

IF OBJECT_ID('TempDB..##tbProfileRelation') IS NOT NULL
DROP TABLE ##tbProfileRelation
	
SELECT TerminalID,RelationTag,RelationTagLength,RelationTagValue,
ROW_NUMBER() OVER (ORDER BY TerminalID,RelationTagID ASC)AS RelationTagID
INTO ##tbProfileRelation
FROM tbProfileRelation WITH (NOLOCK)
WHERE TerminalID = @sTerminalID
ORDER BY TerminalID,RelationTagID ASC

--SELECT * FROM ##tbProfileRelation
--DROP TABLE ##tbProfileRelation


IF OBJECT_ID('TempDB..##tbProfileRelationAcquirer') IS NOT NULL
DROP TABLE ##tbProfileRelationAcquirer

SELECT a.RelationTagID,a.TerminalID,a.RelationTag,a.RelationTagLength,a.RelationTagValue AS Acquirer,a.RelationTagID-1 AS RelationTagID2
INTO ##tbProfileRelationAcquirer
FROM ##tbProfileRelation a WITH (NOLOCK)
WHERE a.TerminalID = @sTerminalID
AND a.RelationTag like 'ad%03'


--SELECT * FROM ##tbProfileRelationAcquirer

IF OBJECT_ID('TempDB..##tbProfileRelationIssuer') IS NOT NULL
DROP TABLE ##tbProfileRelationIssuer


-- tbProfileRelation Issuer
SELECT a.RelationTagID,a.TerminalID,a.RelationTag,a.RelationTagLength,a.RelationTagValue AS IssuerName,b.Acquirer,a.RelationTagID-1 AS RelationTagIDCard
INTO ##tbProfileRelationIssuer
FROM ##tbProfileRelation a WITH(NOLOCK)
INNER JOIN ##tbProfileRelationAcquirer b ON b.TerminalID = a.TerminalID AND b.RelationTagID2 = a.RelationTagID
WHERE a.RelationTag like 'ad%02'

--SELECT * FROM ##tbProfileRelationIssuer 
--DROP TABLE ##tbProfileRelationIssuer

IF OBJECT_ID('TempDB..##tbProfileRelationCard') IS NOT NULL
DROP TABLE ##tbProfileRelationCard

-- tbProfileRelation Card
SELECT a.RelationTagID,a.TerminalID,a.RelationTag,a.RelationTagLength,a.RelationTagValue AS CardName,b.IssuerName,b.Acquirer
INTO ##tbProfileRelationCard
FROM ##tbProfileRelation a WITH(NOLOCK)
INNER JOIN ##tbProfileRelationIssuer b ON b.TerminalID = a.TerminalID AND b.RelationTagIDCard = a.RelationTagID
WHERE a.RelationTag like 'ad%01'

-- SELECT * FROM ##tbProfileRelationCard 
-- DROP TABLE ##tbProfileRelationCard


IF OBJECT_ID('TempDB..##CardMapping') IS NOT NULL
DROP TABLE ##CardMapping

SELECT DISTINCT(CardName),TerminalID,IssuerName,Acquirer 
INTO ##CardMapping
FROM ##tbProfileRelationCard 
WHERE cardName IS NOT NULL

-- SELECT * FROM ##CardMapping 
-- DROP TABLE ##CardMapping

	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY CardName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM ##CardMapping  
	
	--SELECT * FROM #TempDisNo 				
	
	DECLARE @sDatabaseID VARCHAR(8)
	
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalList WITH (NOLOCK)
	WHERE TerminalID=@sTerminalID	

	IF OBJECT_ID('TempDB..##tbProfileCard') IS NOT NULL
	DROP TABLE ##tbProfileCard
		
 	SELECT b.CardName,a.CardTag,a.CardTagValue
	INTO ##tbProfileCard
	FROM tbProfileCard a WITH (NOLOCK)
	INNER JOIN tbProfileCardList b WITH (NOLOCK) ON a.CardID = b.CardID
	WHERE b.DatabaseID = @sDatabaseID
		
    --SELECT * FROM ##tbProfileCard	   
    	
	IF OBJECT_ID('TempDB..##TempCard1') IS NOT NULL
	DROP TABLE ##TempCard1
			
	SELECT a.CardName,'FF04.'+b.s_index+'.'+a.CardTag AS FIELD_ID ,a.CardTagValue AS VALUE
	INTO ##TempCard1
	FROM 
	( SELECT CardName,CardTag,CardTagValue
		FROM ##tbProfileCard
	)	
	a RIGHT JOIN #TempDisNo b ON a.CardName = b.CardName
	ORDER BY a.CardName,a.CardTag	

	--SELECT * FROM ##TempCard1

	DECLARE @sAC25 VARCHAR(5)
	DECLARE @sAC26 VARCHAR(5)
	
	
	DECLARE @sAC25Value VARCHAR(5)
	DECLARE @sAC26Value VARCHAR(5)


	SELECT TOP 1 @sAC25=CardTag FROM ##tbProfileCard
	IF (len(@sAC25)=4) BEGIN SET @sAC25Value='AC25' END ELSE SET @sAC25Value='AC025'
	
	IF OBJECT_ID('TempDB..##TempCardAC25') IS NOT NULL
	DROP TABLE ##TempCardAC25
			
	SELECT CardName,'FF04.'+s_index+'.'+@sAC25Value AS FIELD_ID ,IssuerName AS VALUE,s_index
	INTO ##TempCardAC25
	FROM #TempDisNo 
		
	--SELECT * FROM #TempDisNo
	--SELECT * FROM ##TempCardAC25	
	
	
	SELECT TOP 1 @sAC26=CardTag FROM ##tbProfileCard
	IF (len(@sAC26)=4) BEGIN SET  @sAC26Value='AC26' END ELSE SET @sAC26Value='AC026'
		
	IF OBJECT_ID('TempDB..##TempCardAC26') IS NOT NULL
	DROP TABLE ##TempCardAC26
			
	SELECT CardName,'FF04.'+s_index+'.'+@sAC26Value AS FIELD_ID ,Acquirer AS VALUE,s_index
	INTO ##TempCardAC26
	FROM #TempDisNo 
		
	--SELECT * FROM #TempDisNo
	--SELECT * FROM ##TempCardAC26	
	
	IF OBJECT_ID('TempDB..##TEMCard') IS NOT NULL
	DROP TABLE ##TEMCard
	
	SELECT a.CardName,a.FIELD_ID,a.VALUE,b.TerminalID
	INTO ##TEMCard
	FROM ##TempCard1 a 
	INNER JOIN #TempDisNo b
	ON a.CardName = b.CardName
	UNION ALL
	SELECT a.CardName,a.FIELD_ID,a.VALUE,b.TerminalID
	FROM ##TempCardAC25 a 
	INNER JOIN #TempDisNo b
	ON a.CardName = b.CardName
	UNION ALL
	SELECT a.CardName,a.FIELD_ID,a.VALUE,b.TerminalID
	FROM ##TempCardAC26 a 
	INNER JOIN #TempDisNo b
	ON a.CardName = b.CardName
	
	--SELECT CardName,FIELD_ID,VALUE FROM ##TEMCard
	
	SELECT * FROM ##TEMCard
	ORDER BY CardName,FIELD_ID
	
END

go