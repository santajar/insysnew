
IF OBJECT_ID ('dbo.spTEMProfileSN') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileSN
GO

CREATE Procedure [dbo].[spTEMProfileSN]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
		
	SELECT DISTINCT(SN_Name) AS SN_Name 
	INTO #TempDis
	FROM tbProfileSN
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY SN_Name)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.SN_Name,a.SN_Tag,
	'FF10.'+b.s_index+'.'+a.SN_Tag AS FIELD_ID ,a.SN_TagValue AS VALUE
	FROM tbProfileSN a RIGHT JOIN #TempDisNo b ON a.SN_Name = b.SN_Name
	WHERE a.DatabaseID = @sDatabaseID	  
	ORDER BY a.DatabaseID,a.SN_Name,a.SN_Tag	
 
END


GO

