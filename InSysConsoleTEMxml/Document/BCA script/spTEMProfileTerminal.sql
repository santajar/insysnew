
IF OBJECT_ID ('dbo.spTEMProfileTerminal') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileTerminal
GO

CREATE Procedure [dbo].[spTEMProfileTerminal]
	@sTerminalID VARCHAR(8)
AS
BEGIN

	IF OBJECT_ID('TempDB..##TempDE') IS NOT NULL
	DROP TABLE ##TempDE

	SELECT TerminalID,TerminalTag as FIELD_ID,TerminalTagValue as VALUE 
	INTO ##TempDE
	FROM tbProfileTerminal WITH (NOLOCK) 
	WHERE TerminalID =  @sTerminalID and terminaltag like 'DE%' 
	ORDER BY TerminalID, TerminalTag
	
	SELECT * FROM ##TempDE
	
	SELECT TerminalID,FIELD_ID, VALUE 
	FROM ##TempDE WITH (NOLOCK) 
	WHERE TerminalID =  @sTerminalID and FIELD_ID like 'DE%' 
	union all 
	SELECT TerminalID, TerminalTag as FIELD_ID, TerminalTagValue as VALUE 
	FROM tbProfileTerminal WITH(NOLOCK) 
	WHERE TerminalID = @sTerminalID and terminaltag like 'DC%' 
 
END





GO

