
IF OBJECT_ID ('dbo.spTEMProfileRQ') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileRQ
GO

CREATE Procedure [dbo].[spTEMProfileRQ]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
		
	SELECT DISTINCT(ReqName) AS ReqName 
	INTO #TempDis
	FROM tbProfileReqPaperReceiptManagement
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY ReqName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.ReqName,a.ReqTag,
	'FF12.'+b.s_index+'.'+a.ReqTag AS FIELD_ID ,a.ReqTagValue AS VALUE
	FROM tbProfileReqPaperReceiptManagement a RIGHT JOIN #TempDisNo b ON a.ReqName = b.ReqName
	WHERE a.DatabaseID = @sDatabaseID	  
	ORDER BY a.DatabaseID,a.ReqName,a.ReqTag	
 
END

GO
