
IF OBJECT_ID ('dbo.spTEMProfileRD') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileRD
GO

CREATE Procedure [dbo].[spTEMProfileRD]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
		
	SELECT DISTINCT(RemoteDownloadName) AS RemoteDownloadName 
	INTO #TempDis
	FROM tbProfileRemoteDownload
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY RemoteDownloadName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.RemoteDownloadName,a.RemoteDownloadTag,
	'FF13.'+b.s_index+'.'+a.RemoteDownloadTag AS FIELD_ID ,a.RemoteDownloadTagValue AS VALUE
	FROM tbProfileRemoteDownload a RIGHT JOIN #TempDisNo b ON a.RemoteDownloadName = b.RemoteDownloadName
	WHERE a.DatabaseID = @sDatabaseID	  
	ORDER BY a.DatabaseID,a.RemoteDownloadName,a.RemoteDownloadTag	
 
END

GO
