
IF OBJECT_ID ('dbo.spTEMProfileTLE') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileTLE
GO

CREATE Procedure [dbo].[spTEMProfileTLE]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
	
	--SELECT * FROM tbProfileTLE
	
	SELECT DISTINCT(TLEName) AS TLEName 
	INTO #TempDis
	FROM tbProfileTLE
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY TLEName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.TLEName,a.TLETag,
	'FF05.'+b.s_index+'.'+a.TLETag AS FIELD_ID ,a.TLETagValue AS VALUE
	FROM tbProfileTLE a RIGHT JOIN #TempDisNo b ON a.TLEName = b.TLEName
	WHERE a.DatabaseID = @sDatabaseID	  
	ORDER BY a.DatabaseID,a.TLEName,a.TLETag	
 
END





GO

