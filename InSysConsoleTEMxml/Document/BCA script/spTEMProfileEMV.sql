
IF OBJECT_ID ('dbo.spTEMProfileEMV') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileEMV
GO

CREATE Procedure [dbo].[spTEMProfileEMV]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
	
	--SELECT * FROM tbProfileTLE
	
	SELECT DISTINCT(EMVManagementName) AS EMVManagementName 
	INTO #TempDis
	FROM tbProfileEMVManagement
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY EMVManagementName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.EMVManagementName,a.EMVManagementTag,
	'FF09.'+b.s_index+'.'+a.EMVManagementTag AS FIELD_ID ,a.EMVManagementTagValue AS VALUE
	FROM tbProfileEMVManagement a RIGHT JOIN #TempDisNo b ON a.EMVManagementName = b.EMVManagementName
	WHERE a.DatabaseID = @sDatabaseID	  
	ORDER BY a.DatabaseID,a.EMVManagementName,a.EMVManagementTag	
 
END





GO

