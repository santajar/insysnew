
IF OBJECT_ID ('dbo.spTEMProfileIF') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileIF
GO

CREATE Procedure [dbo].[spTEMProfileIF]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
		
	SELECT DISTINCT(InitialFlazzName) AS InitialFlazzName 
	INTO #TempDis
	FROM tbProfileInitialFlazz
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY InitialFlazzName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.InitialFlazzName,a.InitialFlazzTag,
	'FF11.'+b.s_index+'.'+a.InitialFlazzTag AS FIELD_ID ,a.InitialFlazzTagValue AS VALUE
	FROM tbProfileInitialFlazz a RIGHT JOIN #TempDisNo b ON a.InitialFlazzName = b.InitialFlazzName
	WHERE a.DatabaseID = @sDatabaseID	  
	ORDER BY a.DatabaseID,a.InitialFlazzName,a.InitialFlazzTag	
 
END

GO

