
IF OBJECT_ID ('dbo.spTEMProfileCardType') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileCardType
GO

CREATE Procedure [dbo].[spTEMProfileCardType]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
	
	--SELECT * FROM tbProfileTLE
	
	SELECT DISTINCT(CardTypeName) AS CardTypeName 
	INTO #TempDis
	FROM tbProfileCardType
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY CardTypeName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.CardTypeName,a.CardTypeTag,
	'FF09.'+b.s_index+'.'+a.CardTypeTag AS FIELD_ID ,a.CardTypeTagValue AS VALUE
	FROM tbProfileCardType a RIGHT JOIN #TempDisNo b ON a.CardTypeName = b.CardTypeName
	WHERE a.DatabaseID = @sDatabaseID	  
	ORDER BY a.DatabaseID,a.CardTypeName,a.CardTypeTag	
 
END




GO

