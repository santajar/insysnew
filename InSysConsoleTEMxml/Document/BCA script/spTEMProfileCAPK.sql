


IF OBJECT_ID ('dbo.spTEMProfileCAPK') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileCAPK
GO

CREATE Procedure [dbo].[spTEMProfileCAPK]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
	
	--SELECT * FROM tbProfileCAPK
	
	SELECT DISTINCT(CAPKIndex) AS CAPKIndex 
	INTO #TempDis
	FROM tbProfileCAPK
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY CAPKIndex)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.CAPKIndex,a.CAPKTag,
	'FF07.'+b.s_index+'.'+a.CAPKTag AS FIELD_ID ,a.CAPKTagValue AS VALUE
	FROM tbProfileCAPK a RIGHT JOIN #TempDisNo b ON a.CAPKIndex = b.CAPKIndex
	WHERE a.DatabaseID = @sDatabaseID 
	ORDER BY a.DatabaseID,a.CAPKIndex,a.CAPKTag	
 
END





GO

