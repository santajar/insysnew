
IF OBJECT_ID ('dbo.spTEMProfileAcquirer') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileAcquirer
GO

CREATE Procedure [dbo].[spTEMProfileAcquirer]
	@sTerminalID VARCHAR(8)
AS
BEGIN

	IF OBJECT_ID('TempDB..#TempDisAcq') IS NOT NULL
	DROP TABLE #TempDisAcq
	
	SELECT DISTINCT(AcquirerName) AS AcquirerName 
	INTO #TempDisAcq
	FROM tbProfileAcquirer
	WHERE TerminalID = @sTerminalID  	  
	
	IF OBJECT_ID('TempDB..#TempDisAcqNo') IS NOT NULL
	DROP TABLE #TempDisAcqNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY AcquirerName)AS VARCHAR ) s_index
	INTO #TempDisAcqNo
	FROM #TempDisAcq   
	
	--SELECT * FROM #TempDisAcqNo 				
	
	SELECT a.TerminalID,a.AcquirerName,a.AcquirerTag,
	'FF02.'+b.s_index+'.'+a.AcquirerTag AS FIELD_ID ,a.AcquirerTagValue AS VALUE
	FROM tbProfileAcquirer a RIGHT JOIN #TempDisAcqNo b ON a.AcquirerName = b.AcquirerName
	WHERE a.TerminalID = @sTerminalID  
	ORDER BY a.TerminalID,a.AcquirerName,a.AcquirerTag	
 
END





GO

