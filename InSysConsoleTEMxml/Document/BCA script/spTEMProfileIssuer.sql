IF OBJECT_ID ('dbo.spTEMProfileIssuer') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileIssuer
GO

CREATE Procedure [dbo].[spTEMProfileIssuer]
	@sTerminalID VARCHAR(8)
AS
BEGIN

	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
	
	SELECT DISTINCT(IssuerName) AS IssuerName 
	INTO #TempDis
	FROM tbProfileIssuer
	WHERE TerminalID = @sTerminalID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY IssuerName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.TerminalID,a.IssuerName,a.IssuerTag,
	'FF03.'+b.s_index+'.'+a.IssuerTag AS FIELD_ID ,a.IssuerTagValue AS VALUE
	FROM tbProfileIssuer a RIGHT JOIN #TempDisNo b ON a.IssuerName = b.IssuerName
	WHERE a.TerminalID = @sTerminalID	  
	ORDER BY a.TerminalID,a.IssuerName,a.IssuerTag	
 
END






GO

