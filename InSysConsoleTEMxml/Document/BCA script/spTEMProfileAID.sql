

IF OBJECT_ID ('dbo.spTEMProfileAID') IS NOT NULL
	DROP PROCEDURE dbo.spTEMProfileAID
GO

CREATE Procedure [dbo].[spTEMProfileAID]
	@sDatabase VARCHAR(20)
AS
BEGIN

	DECLARE @sDatabaseID VARCHAR(20)
	SELECT @sDatabaseID=DatabaseID FROM tbProfileTerminalDB
	WHERE DatabaseName =  @sDatabase
	
	IF OBJECT_ID('TempDB..#TempDis') IS NOT NULL
	DROP TABLE #TempDis
	
	--SELECT * FROM tbProfileAID
	
	SELECT DISTINCT(AIDName) AS AIDName 
	INTO #TempDis
	FROM tbProfileAID
	WHERE DatabaseID = @sDatabaseID  	  
	
	--SELECT * FROM #TempDis
	
	IF OBJECT_ID('TempDB..#TempDisNo') IS NOT NULL
	DROP TABLE #TempDisNo
	
	SELECT *,CAST(row_number() OVER (ORDER BY AIDName)AS VARCHAR ) s_index
	INTO #TempDisNo
	FROM #TempDis  
	
	--SELECT * FROM #TempDisNo 				
	
	SELECT a.DatabaseID,a.AIDName,a.AIDTag,
	'FF06.'+b.s_index+'.'+a.AIDTag AS FIELD_ID ,a.AIDTagValue AS VALUE
	FROM tbProfileAID a RIGHT JOIN #TempDisNo b ON a.AIDName = b.AIDName
	WHERE a.DatabaseID = @sDatabaseID 
	ORDER BY a.DatabaseID,a.AIDName,a.AIDTag	
 
END





GO

