﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace InSysConsoleTEMxml
{
    class Program
    {

        public static SqlConnection oSqlConn;
        public static SqlConnection oSqlConnInSys;

        public static DataTable dtSignature = new DataTable();
        public static DataTable dtTerminalValue = new DataTable();
        public static DataTable dtCardValue = new DataTable();
        public static DataTable dtIssuerValue = new DataTable();
        public static DataTable dtAcquirerValue = new DataTable();
        public static DataTable dtTLEValue = new DataTable();
        public static DataTable dtEMVValue = new DataTable();
        public static DataTable dtAIDValue = new DataTable();
        public static DataTable dtCAPKValue = new DataTable();
        public static DataTable dtCardTypeValue = new DataTable();
        public static DataTable dtGPRSValue = new DataTable();
        public static DataTable dtSNValue = new DataTable();
        public static DataTable dtIFValue = new DataTable();
        public static DataTable dtRQValue = new DataTable();
        public static DataTable dtRDValue = new DataTable();
        public static DataTable dtMenuValue = new DataTable();
        public static string sPathXie;
        static void Main(string[] args)
        {
            try
            {
                InitConnection();

                string DatabaseName = ConfigurationManager.AppSettings["DatabaseName"].ToString();
                string template = ConfigurationManager.AppSettings["Template"].ToString();
                string version = ConfigurationManager.AppSettings["version"].ToString();
                string estateRegion = ConfigurationManager.AppSettings["estateRegion"].ToString();
                string estateSponsors = ConfigurationManager.AppSettings["estateSponsors"].ToString();
                bool bManagement = bool.Parse(ConfigurationManager.AppSettings["management"].ToString());


                string filenameXIE = DatabaseName+".xie";
                //string filenameXIE = DatabaseName + ".xml";
                sPathXie = ConfigurationManager.AppSettings["PathXie"].ToString() + filenameXIE;
                
                CommonLib.Write2File(sPathXie, "<?xml version=\"1.0\" encoding=\"utf-8\"?>",true);
                CommonLib.Write2File(sPathXie, "<script version=\"1.0\">", true);
                //sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                //sw.WriteLine("<script version=\"1.0\">");

                dtSignature = dtGetSignature(DatabaseName);
                
                //sw.WriteLine("<context>");
                CommonLib.Write2File(sPathXie, "<context>", true);
                string sestatesignature = string.Format("<estate signature=\"{0}\">", estateRegion);
                //sw.WriteLine(sestatesignature);
                CommonLib.Write2File(sPathXie, sestatesignature, true);
                //string sestate2signature = string.Format("<estate signature=\"{0}\">", estateSponsors);
                string sestate2signature = string.Format("<estate signature=\"{0}\"/>", estateSponsors);
                //sw.WriteLine(sestate2signature);
                CommonLib.Write2File(sPathXie, sestate2signature, true);
                //if (estateSponsors != "")
                //{
                //    string ssponsorsignature = string.Format("<estate signature=\"{0}\"/>", estateSponsors);
                //    CommonLib.Write2File(sPathXie, ssponsorsignature, true);
                //}                
                //CommonLib.Write2File(sPathXie, "</estate>", true);
                CommonLib.Write2File(sPathXie, "</estate>", true);
                //sw.WriteLine("</context>");
                CommonLib.Write2File(sPathXie, "</context>", true);
                int irow = 0;

                Console.WriteLine("START");
                foreach (DataRow Srow in dtSignature.Rows)
                {
                    string sTEMterminal = Srow["SerialNumber"].ToString();
                    string sINSYSterminal = Srow["TerminalID"].ToString();
                    string sterminalsignature = string.Format("<terminal signature=\"{0}\">", sTEMterminal);
                    //sw.WriteLine(sterminalsignature);
                    CommonLib.Write2File(sPathXie, sterminalsignature, true);
                    string sterminalvalue = string.Format("<name value=\"{0}\"/>", (sINSYSterminal + " - " + sTEMterminal));
                    //sw.WriteLine(sterminalvalue);
                    CommonLib.Write2File(sPathXie, sterminalvalue, true);
                    //sw.WriteLine("<status value=\"enabled\"/>");
                    CommonLib.Write2File(sPathXie, "<status value=\"enabled\"/>", true);
                    //sw.WriteLine("<type value=\"TETRA\"/>");
                    CommonLib.Write2File(sPathXie, "<type value=\"TETRA\"/>", true);
                    //string stemplate = string.Format("<parameterProfileSet templateId=\"{0}\" templateVersion=\"{1}\" profileId=\"{2}\" profileSetName=\"{3}\" sponsor=\"{4}\">", template, version, version, template+ version, estateSponsors); //Path.GetFileNameWithoutExtension(filename)
                    string stemplate = string.Format("<parameterSet templateId=\"{0}\" templateVersion=\"{1}\" >", template, version);
                    //sw.WriteLine(stemplate);
                    CommonLib.Write2File(sPathXie, stemplate, true);

                    dtTerminalValue = dtGetValueTerminal(sINSYSterminal);
                    foreach (DataRow row in dtTerminalValue.Rows)
                    {
                        string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                        //sw.WriteLine(sLine);
                        CommonLib.Write2File(sPathXie, sLine, true);
                        //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                    }
                    Console.WriteLine("Generate Terminal");
                    dtCardValue = dtGetValueCard(sINSYSterminal);
                    foreach (DataRow row in dtCardValue.Rows)
                    {
                        string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                        //sw.WriteLine(sLine);
                        CommonLib.Write2File(sPathXie, sLine, true);
                        //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                    }
                    Console.WriteLine("Generate Card");
                    dtIssuerValue = dtGetValueIssuer(sINSYSterminal);
                    foreach (DataRow row in dtIssuerValue.Rows)
                    {
                        string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                        //sw.WriteLine(sLine);
                        CommonLib.Write2File(sPathXie, sLine, true);
                        //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                    }
                    Console.WriteLine("Generate Issuer");
                    dtAcquirerValue = dtGetValueAcquirer(sINSYSterminal);
                    foreach (DataRow row in dtAcquirerValue.Rows)
                    {
                        string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                        //sw.WriteLine(sLine);
                        CommonLib.Write2File(sPathXie, sLine, true);
                        //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                    }
                    Console.WriteLine("Generate Acquirer");
                    dtTLEValue = dtGetValueTLE(DatabaseName);
                    foreach (DataRow row in dtTLEValue.Rows)
                    {
                        string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                        //sw.WriteLine(sLine);
                        CommonLib.Write2File(sPathXie, sLine, true);
                        //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                    }
                    if (bManagement == true)
                    {
                        Console.WriteLine("Generate TLE");
                        dtEMVValue = dtGetValueEMV(DatabaseName);
                        foreach (DataRow row in dtEMVValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate EMV");
                        dtAIDValue = dtGetValueAID(DatabaseName);
                        foreach (DataRow row in dtAIDValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate AID");
                        dtCAPKValue = dtGetValueCAPK(DatabaseName);
                        foreach (DataRow row in dtCAPKValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate CAPK");
                        dtGPRSValue = dtGetValueGPRS(DatabaseName);
                        foreach (DataRow row in dtGPRSValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate GPRS");
                        dtCardTypeValue = dtGetValueCardType(DatabaseName);
                        foreach (DataRow row in dtCardTypeValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate CardType");
                        dtSNValue = dtGetValueSN(DatabaseName);
                        foreach (DataRow row in dtSNValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate SN");
                        dtIFValue = dtGetValueIF(DatabaseName);
                        foreach (DataRow row in dtIFValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate IF");
                        dtRQValue = dtGetValueRQ(DatabaseName);
                        foreach (DataRow row in dtRQValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate RQ");
                        dtRDValue = dtGetValueRD(DatabaseName);
                        foreach (DataRow row in dtRDValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate RD");
                        dtMenuValue = dtGetValueMenu(sINSYSterminal);
                        foreach (DataRow row in dtMenuValue.Rows)
                        {
                            string sLine = string.Format("<parameter fieldId=\"{0}\" value=\"{1}\"/>", row["FIELD_ID"], row["VALUE"]);
                            //sw.WriteLine(sLine);
                            CommonLib.Write2File(sPathXie, sLine, true);
                            //Console.WriteLine(row["FIELD_ID"] + " " + row["VALUE"]);
                        }
                        Console.WriteLine("Generate MN");
                        //sw.WriteLine("</parameterSet>");
                    }
                    CommonLib.Write2File(sPathXie, "</parameterSet>", true);
                    //sw.WriteLine("</terminal>");
                    CommonLib.Write2File(sPathXie, "</terminal>", true);
                    irow = irow + 1;
                    Console.WriteLine(irow.ToString()+" " +sINSYSterminal + " Success,"+ DatabaseName + " ->:" + estateRegion+" SN:" + sTEMterminal +  " " +template+ " " + version);
                    CommonClass.doWritelogFile(string.Format("{0}", ",'"+sINSYSterminal+ "',")); 
                }
                //sw.WriteLine("</script>");
                CommonLib.Write2File(sPathXie, "</script>", true);
                //Console.WriteLine("</script>");
                Console.WriteLine("FINISH");
                //sw.Close();


                Console.ReadLine();
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Generate Failed : "+ ex.Message, ex.Message);
                Console.ReadKey();
            }

        }

        protected static void InitConnection()
        {
            try
            {
                string sConnString = ConfigurationManager.ConnectionStrings["IngEstateDB"].ConnectionString;
                oSqlConn = new SqlConnection(sConnString);
                oSqlConn.Open();
            }
            catch (SqlException sqlEx)
            {
                Console.WriteLine("DB TEM FAILED, {0}", sqlEx.Message);
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("DB TEM FAILED, {0}", ex.Message);
                Console.ReadKey();
            }

            try
            {
                string sConnString = ConfigurationManager.ConnectionStrings["InSysDB"].ConnectionString;
                oSqlConnInSys = new SqlConnection(sConnString);
                oSqlConnInSys.Open();
            }
            catch (SqlException sqlEx)
            {
                Console.WriteLine("DB InSys FAILED, {0}", sqlEx.Message);
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("DB InSys FAILED, {0}", ex.Message);
                Console.ReadKey();
            }
        }
        protected static DataTable dtGetSignature(string _sDatabase)
        {
            DataTable dtTemp = new DataTable();
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            using (SqlCommand oCmd = new SqlCommand("spTEMGetSignature", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }       

        protected static DataTable dtGetValueTerminal(string _sTerminalID)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileTerminal", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }

        protected static DataTable dtGetValueIssuer(string _sTerminalID)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileIssuer", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }

        protected static DataTable dtGetValueCard(string _sTerminalID)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileCard", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueAcquirer(string _sTerminalID)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileAcquirer", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueTLE(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileTLE", oSqlConnInSys))
            {
               // oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueEMV(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileEMV", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueAID(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileAID", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueCAPK(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileCAPK", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueGPRS(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileGPRS", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueCardType(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileCardType", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueSN(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileSN", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueIF(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileIF", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueRQ(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileRQ", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        protected static DataTable dtGetValueRD(string _sDatabase)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileRD", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sDatabase", SqlDbType.VarChar).Value = _sDatabase;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }

        protected static DataTable dtGetValueMenu(string _sTerminalID)
        {
            if (oSqlConnInSys.State != ConnectionState.Open) oSqlConnInSys.Open();
            DataTable dtTemp = new DataTable();
            using (SqlCommand oCmd = new SqlCommand("spTEMProfileMenu", oSqlConnInSys))
            {
                //oCmd.CommandTimeout = 0;
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = _sTerminalID;
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }
        static DataTable ConvertListToDataTable(List<DataTable> list)
        {
            // New table.
            DataTable table = new DataTable();

            // Get max columns.
            int columns = 0;
            foreach (var array in list)
            {
                //if (array.Length > columns)
                //{
                //    columns = array.Length;
                //}
            }

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
        }
        static void ReadEMSFile(string sPath, ref List<DataTable> dtXML)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(sPath);
            foreach (DataTable dtTemp in ds.Tables)
            {
                dtXML.Add(dtTemp);
            }
            ds.Dispose();
        }

        
    }
}
