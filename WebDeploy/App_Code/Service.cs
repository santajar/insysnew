using System;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using InSysClass;
using ipXML;

[WebService(Namespace = "http://www.integra-pratama.co.id/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Service : System.Web.Services.WebService
{
    public Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    protected List<string> ltStringColumnXML;    
    protected List<SqlEMS_XML.ValueXML> ltObjValueXML;
    //protected SqlConnection oSqlConn;
    protected string sConnString;
    
    protected string sXmlLog;
    protected string sXmlFile;
    protected string sXmlResult;
    protected string sFileResult = "";

    protected bool bDebug = false;
    protected string sPath = "";

    [WebMethod]
    public EMS_XML sStartXMLWebService(EMS_XML _oXML)
    {
        EMS_XML oXml = new EMS_XML();
        EMS_XML oXmlResponse = new EMS_XML();

        bDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"].ToString());
        sPath = ConfigurationManager.AppSettings["Path"].ToString();
        sXmlFile = sPath;
        sXmlLog = sPath;
        sXmlResult = sPath;

        doInitiateConnectionString();
        oXml = oXmlResponse = _oXML;

        InitDirectory();
        string sResult = sProcessXML(ref oXmlResponse);
        return oXmlResponse;
    }

    protected void InitDirectory()
    {
        //string sCurrDir = HostingEnvironment.ApplicationPhysicalPath;
        //sXmlLog = sCurrDir + @"\BIN\EMS\LOG";
        //sXmlFile = sCurrDir + @"\BIN\EMS\File";
        //sXmlResult = sCurrDir + @"\BIN\EMS\Result";

        string sCurrDir = ConfigurationManager.AppSettings["Path"].ToString();
        sXmlLog = sCurrDir + @"\LOG";
        sXmlFile = sCurrDir + @"\FILE";
        sXmlResult = sCurrDir + @"\RESULT";

        if (!Directory.Exists(sXmlLog))
            Directory.CreateDirectory(sXmlLog);
        if (!Directory.Exists(sXmlFile))
            Directory.CreateDirectory(sXmlFile);
        if (!Directory.Exists(sXmlResult))
            Directory.CreateDirectory(sXmlResult);

        EMSCommonClass.Directory = sCurrDir;
        EMSCommonClass.DirectoryLog = sXmlLog;
        EMSCommonClass.DirectoryXml = sXmlFile;
        EMSCommonClass.DirectoryResult = sXmlResult;
    }

    protected void doInitiateConnectionString()
    {
        //oSqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
        //if (oSqlConn.State != ConnectionState.Open)
        //{
        //    oSqlConn.Close();
        //    oSqlConn.Open();
        //}
        sConnString = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
    }

    protected string sGetSenderID(EMS_XML _oXml)
    {
        return (!string.IsNullOrEmpty(_oXml.Attribute.sSenderID)) ? _oXml.Attribute.sSenderID : "NullID";
    }

    protected string sProcessXML(ref EMS_XML _oXml)
    {
        string sErrMessage = "XML Process Success";
        int iError = 0;
        try
        {
            EMSCommonClass.XMLRequestType oReqType = XMLAttributeType(_oXml);
            if (bDebug) sFileResult = string.Format(@"{1}\{0}.xml", _oXml.Attribute.sSenderID, sPath);
            EMSCommonClass.UserAccessDelete(new SqlConnection(sConnString), null);
            switch (oReqType)
            {
                case EMSCommonClass.XMLRequestType.ADD_UPDATE:
                    //EMSCommonClass.IsXMLAddUpdate(oSqlConn, ref sErrMessage, ref _oXml, null, sFileResult);
                    //EMSCommonClass.IsXMLAddUpdate(sConnString, ref sErrMessage, ref _oXml, null, sFileResult);
                    EMSCommonClass.IsXMLAddUpdate(sConnString, ref sErrMessage, ref iError, ref _oXml, null, sFileResult);
                    break;
                case EMSCommonClass.XMLRequestType.DELETE:
                    //EMSCommonClass.IsXMLDelete(oSqlConn, ref sErrMessage, ref _oXml, null, sFileResult);
                    //EMSCommonClass.IsXMLDelete(sConnString, ref sErrMessage, ref _oXml, null, sFileResult);
                    EMSCommonClass.IsXMLDelete(sConnString, ref sErrMessage, ref iError, ref _oXml, null, sFileResult);
                    break;
                case EMSCommonClass.XMLRequestType.INQ:
                case EMSCommonClass.XMLRequestType.INQINIT:
                    //EMSCommonClass.IsXMLInquiry(oSqlConn, ref sErrMessage, ref _oXml, null, sFileResult);
                    //EMSCommonClass.IsXMLInquiry(sConnString, ref sErrMessage, ref _oXml, null, sFileResult);
                    EMSCommonClass.IsXMLInquiry(sConnString, ref sErrMessage, ref iError, ref _oXml, null, sFileResult);
                    break;
            }
        }
        catch (Exception ex)
        {
            sErrMessage = ex.StackTrace + " : " + ex.Message;
            EMSCommonClass.WriteLog(sErrMessage);

            EMS_XML oResultEms = new EMS_XML();
            oResultEms.Attribute = _oXml.Attribute;

            foreach (EMS_XML.DataClass.TerminalClass oTerminalClass in _oXml.Data.ltTerminal)
            {
                EMS_XML.DataClass.TerminalClass oTempTerminalClass = new EMS_XML.DataClass.TerminalClass();

                oTempTerminalClass.AddColumnValue("Terminal_Init", oTerminalClass.sGetColumnValue("Terminal_Init".ToUpper()));
                oTempTerminalClass.AddColumnValue("Process_Type".ToUpper(), _oXml.Attribute.sTypes);

                oResultEms.Data.Terminal.AddColumnValue("Response_Code", "001");
                oTempTerminalClass.Result.sResultFieldName = "WEBSERVICE";
                oTempTerminalClass.Result.sResultCode = "001";
                oTempTerminalClass.Result.sResultDesc = sErrMessage;
                oTempTerminalClass.Result.sTimeProcess = DateTime.Now.ToString("ddMMyyyyhhmmss");

                oResultEms.Data.ltTerminal.Add(oTempTerminalClass);
            }
            _oXml = oResultEms;
        }
        return sErrMessage;
    }

    protected EMSCommonClass.XMLRequestType XMLAttributeType(EMS_XML _oXml)
    {
        if (_oXml.Attribute.sTypes.ToLower().Contains("inq"))
            return EMSCommonClass.XMLRequestType.INQ;
        else if (_oXml.Attribute.sTypes.ToLower().Contains("inqinit"))
            return EMSCommonClass.XMLRequestType.INQINIT;
        else if (_oXml.Attribute.sTypes.ToLower().Contains("del"))
            return EMSCommonClass.XMLRequestType.DELETE;
        else
            return EMSCommonClass.XMLRequestType.ADD_UPDATE;
    }
}