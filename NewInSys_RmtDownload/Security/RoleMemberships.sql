﻿ALTER ROLE [db_owner] ADD MEMBER [insys];


GO
ALTER ROLE [db_owner] ADD MEMBER [tedie];


GO
ALTER ROLE [db_owner] ADD MEMBER [ibnu];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [tedie];


GO
ALTER ROLE [db_ddladmin] ADD MEMBER [ibnu];


GO
ALTER ROLE [db_datareader] ADD MEMBER [tedie];


GO
ALTER ROLE [db_datareader] ADD MEMBER [ibnu];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [tedie];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [ibnu];

