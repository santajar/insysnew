﻿CREATE TABLE [dbo].[tbControlFlag] (
    [ItemName]    VARCHAR (50)  NULL,
    [Flag]        VARCHAR (150) NULL,
    [HexString]   VARCHAR (16)  NULL,
    [Description] VARCHAR (500) NULL
);

