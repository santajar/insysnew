﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. parse the ISO message value to get the bi-48 value, EDC serial number and application name
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseBit48] 
	@sSessionTime VARCHAR(MAX),
	@sBit48 VARCHAR(MAX) OUTPUT,
	@sEdcSn VARCHAR(MAX) OUTPUT,
	@sAppName VARCHAR(MAX) OUTPUT
AS
SET NOCOUNT ON
DECLARE @sSqlStmt NVARCHAR(MAX)

SET @sSqlStmt = N'SELECT @Bit48=BitValue FROM ##tempBitMap' + @sSessionTime + 
	' WHERE BitId=48'
EXEC sp_executesql @sSqlStmt,N'@Bit48 VARCHAR(MAX) OUTPUT',@Bit48=@sBit48 OUTPUT

DECLARE @iLenSn INT
DECLARE @iLenApp INT
DECLARE @sTempSn VARCHAR(50)
DECLARE @sTempApp VARCHAR(50)
SET @iLenSn = CONVERT(INT,SUBSTRING(@sBit48,5,4))
SET @sTempSn = SUBSTRING(@sBit48,9,@iLenSn*2)
EXEC spOtherParseHexStringToString @sTempSn, @sEdcSn OUTPUT

SET @iLenApp = CONVERT(INT,SUBSTRING(@sBit48,9+(@iLenSn*2),4))
SET @sTempApp = SUBSTRING(@sBit48,13+(@iLenSn*2),@iLenApp*2)
EXEC spOtherParseHexStringToString @sTempApp, @sAppName OUTPUT


