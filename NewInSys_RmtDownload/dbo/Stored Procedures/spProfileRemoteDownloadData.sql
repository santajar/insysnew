﻿-- =============================================
-- Author:		<Tedie Scorfia>
-- Create date: <3 Nov 2014>
-- Description:	<List Data Download>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRemoteDownloadData]
	@sTerminalID NVARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sQuery NVARCHAR(MAX),
			@sLinkDatabase NVARCHAR(50),
			@ParmDefinition NVARCHAR(500),
			@iAppPackID BIGINT,
			@sQuery2 NVARCHAR(MAX),
			@sQuery3 NVARCHAR(MAX)

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

CREATE TABLE #TempDataTID
(
	TerminalID VARCHAR(8),
	AppPackID BIGINT
)

CREATE TABLE #TempDataSoftware
(
	BuildNumber INT,
	AppPackageName VARCHAR(150),
	AppPackageFilename VARCHAR(150),
	UploadTime DATETIME,
	AppPackID BIGINT
)

CREATE TABLE #TempDataContentSoftware
(
	AppPackID BIGINT,
	Id BIGINT,
	AppPackageContent VARCHAR(MAX)
)

	SET @sQuery = N'SELECT TerminalID,AppPackID
			FROM '+@sLinkDatabase+'tbListTIDRemoteDownload WITH (NOLOCK)
			WHERE TerminalID = '''+@sTerminalID+''''

	INSERT INTO #TempDataTID (TerminalID,AppPackID)
	EXEC (@sQuery)

	SELECT @iAppPackID = AppPackID FROM #TempDataTID

	SET @sQuery2 = N'SELECT BuildNumber,AppPackageName,AppPackageFilename,UploadTime,AppPackID
					FROM '+@sLinkDatabase+'tbAppPackageEDCList WITH (NOLOCK)
					WHERE AppPackID ='''+ CONVERT(NVARCHAR,@iAppPackID)+''''
	INSERT INTO #TempDataSoftware(BuildNumber,AppPackageName,AppPackageFilename,UploadTime,AppPackID)
	EXEC (@sQuery2)
	
	SET @sQuery3 = N'SELECT AppPackID,Id,AppPackageContent
				FROM '+@sLinkDatabase+'tbAppPackageEDCListTemp WITH (NOLOCK)
				WHERE AppPackID = '''+ CONVERT(NVARCHAR,@iAppPackID)+''''
	
	INSERT INTO #TempDataContentSoftware(AppPackID,Id,AppPackageContent)
	EXEC (@sQuery3)

	SELECT TerminalID,AppPackageName,BuildNumber,AppPackageFilename,Id,AppPackageContent,UploadTime
	FROM #TempDataTID TID
	JOIN
	#TempDataSoftware SOFTWARE
	ON TID.AppPackID = SOFTWARE.AppPackID
	JOIN
	#TempDataContentSoftware CONTENT
	ON TID.AppPackID = CONTENT.AppPackID


	--SET NOCOUNT ON;
	--DECLARE @sQuery NVARCHAR(MAX),
	--		@sLinkDatabase NVARCHAR(50),
	--		@ParmDefinition NVARCHAR(500),
	--		@iAppPackID BIGINT
	
	--SELECT @sLinkDatabase = FLAG
	--FROM tbControlFlag
	--WHERE ItemName= 'LinkedRemoteDownloadServer'

	--CREATE TABLE #TempDataTID
	--(
	--	TerminalID VARCHAR(8),
	--	BuildNumber INT,
	--	AppPackageName VARCHAR(150),
	--	AppPackageFilename VARCHAR(150),
	--	UploadTime DATETIME,
	--	AppPackID BIGINT
	--)

	--CREATE TABLE #TempDataSoftware
	--(
	--	AppPackID BIGINT,
	--	Id BIGINT,
	--	AppPackageContent VARCHAR(MAX)
	--)
	
	--INSERT INTO #TempDataTID (TerminalID,BuildNumber,AppPackageName,AppPackageFilename,UploadTime,AppPackID)
	--EXEC spProfileRemoteDownloadDataTerminalID @sTerminalID

	--SELECT @iAppPackID = AppPackID
	--FROM #TempDataTID

	--SET @sQuery = N'SELECT AppPackID,Id,AppPackageContent
	--			FROM '+@sLinkDatabase+'tbAppPackageEDCListTemp WITH (NOLOCK)
	--			WHERE AppPackID = @iParamAppPackID 
	--			order by Id';

	--SET @ParmDefinition = N'@iParamAppPackID BIGINT';

	--INSERT INTO #TempDataSoftware(AppPackID,Id,AppPackageContent)
	--EXECUTE sp_executesql @sQuery,@ParmDefinition,@iParamAppPackID = @iAppPackID;
	
	--SELECT TerminalID,AppPackageName,BuildNumber,AppPackageFilename,Id,AppPackageContent,UploadTime
	--FROM #TempDataTID TID
	--JOIN
	--#TempDataSoftware SOFTWARE
	--ON TID.AppPackID = SOFTWARE.AppPackID

END
