﻿CREATE PROCEDURE [dbo].[spIsValidTerminalID] 
	@sTerminalID NVARCHAR(8),
	@bReturn BIT OUTPUT
AS
BEGIN
	DECLARE @sQuery NVARCHAR(MAX),
			@ParmDefinition NVARCHAR(500),
			@iCount INT,
			@sLinkDatabase NVARCHAR(50)
			

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

	set @sQuery = N'SELECT @iParamCount = COUNT(*)
				  FROM '+@sLinkDatabase+'tbProfileTerminalList 
				  WITH(NOLOCK)
				  WHERE TerminalId=@sParmTerminalID'
	
	SET @ParmDefinition = N'@sParmTerminalID VARCHAR(8), @iParamCount INT OUTPUT';

	EXECUTE sp_executesql @sQuery,@ParmDefinition, @sParmTerminalID = @sTerminalID, @iParamCount = @iCount OUTPUT;

	if @iCount>0
		set @bReturn=1
	else
		set @bReturn=0

END