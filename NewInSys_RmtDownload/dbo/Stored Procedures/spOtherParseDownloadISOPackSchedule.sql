﻿-- =============================================
-- Author:		Tobias Supriyadi
-- Create date: Sept 21, 2010
-- Modify	  :
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. pack the response message schedule into ISO format
-- =============================================
CREATE PROCEDURE [dbo].[spOtherParseDownloadISOPackSchedule]
	@iCheckInit INT,
	@sNII VARCHAR(4),
	@sAddress VARCHAR(4),
	@sMTI VARCHAR(4),
	@sSessionTime VARCHAR(10),
	@sTerminalId VARCHAR(8) OUTPUT
AS
SET NOCOUNT ON
DECLARE @sContent VARCHAR(MAX),
	@sTag VARCHAR(4),
	@sISOMessage VARCHAR(MAX),
	@bValidTerminalId BIT,
	@iErrorCode INT, -- Error Init status, open tbMSErrorInit for detail.
	@sProcCode VARCHAR(6),
	@sBit11 VARCHAR(MAX),
	@sBit48 VARCHAR(MAX),
	@sBit57 VARCHAR (MAX),
	@sBit58 VARCHAR (10),
	@sEdcSn VARCHAR(25),
	@sAppName VARCHAR(15),
	@iCounterApp INT,
	@sAppNameDownload VARCHAR(MAX),
	@sAppName2Download VARCHAR(MAX),
	@sAppFilenameDownload VARCHAR(MAX),
	@sLastDownload VARCHAR(10),
	@iEnableDownload int,
	@icountScheduleSoftware int

IF dbo.IsInitMTI(@sMTI) = 1
BEGIN
	-- get the bit-bit value
	EXEC spOtherParseProcCode @sSessionTime, @sProcCode OUTPUT
	EXEC spOtherParseTerminalId @sSessionTime, @sTerminalId OUTPUT
	EXEC spOtherParseBit11 @sSessionTime, @sBit11 OUTPUT
	EXEC spOtherParseBit48 @sSessionTime, @sBit48 OUTPUT, @sEdcSn OUTPUT, @sAppName OUTPUT
	print '@sEdcSn ' + @sEdcSn
	print @sBit48
	--IF dbo.IsValidTerminalID(@sTerminalID) = 1
	IF dbo.IsValidTerminalSN(@sEdcSn) = 1
	BEGIN 
		
			EXEC spScheduleSoftwareSendtoEDC @sTerminalId, @sEdcSn, @sContent OUTPUT
			PRINT 'CONTENT SCHEDULE :'+@sContent
			SET @iErrorCode= 7
			SET @sAppName = 'DlSchedule'
			SET @iCounterApp= 0
		
	END
	ELSE
	BEGIN
		--SET @sContent = 'INVALIDTID'
		SET @sContent = 'INVALIDSN'
		SET @sBit11 = NULL
		SET @iErrorCode = 4
		
	END
END
ELSE
BEGIN
	SET @sContent = 'INVALIDMTI'
	SET @sBit11 = NULL
	SET @iErrorCode = 5
END

print '--send---'
print @sTerminalID
print @sProcCode
print @sNII
print @sAddress
print @sBit11
print @sBit48
print @sContent
print @sAppName
print @iCounterApp
print 1
print '--'

SELECT @sISOMessage = dbo.sGenerateDownloadISOSend(@sTerminalID,
	@sProcCode, @sNII, @sAddress, @sBit11, @sBit48, @sContent, @sAppName, @iCounterApp, 0)
print '@sTerminalID ' + @sTerminalID
PRINT 'Content : ' + ISNULL(@sContent,'null')
PRINT 'Procode : ' + @sProcCode
PRINT 'NII : ' + @sNII
PRINT 'Procode : ' + @sProcCode
PRINT '@sTag : ' + ISNULL(@sTag,'null') 
PRINT '@sAppNameDownload ' + ISNULL(@sAppNameDownload,'null') 
--print '@sBit48 : ' + ISNULL(@sBit48,'null')
print '@sBit11 : ' + ISNULL(@sBit11,'null')
print '@sISOMessage : ' + ISNULL(@sISOMessage,'null')
print '@sAppFilenameDownload : ' + ISNULL(@sAppFilenameDownload,'null')

SELECT @sTerminalID [TERMINALID]
	,@sContent [CONTENT]
	,@sTag [TAG]
	,@sISOMessage [ISO]
	,@iErrorCode [ERRORCODE]
	,@sAppNameDownload [APPNAME]
	,@sAppFilenameDownload [APPFILENAME]



