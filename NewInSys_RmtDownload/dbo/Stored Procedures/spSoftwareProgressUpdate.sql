﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Agt 19, 2013
-- Modify date:
--				1. 
-- Description:	Insert into tbSoftwareProgress
-- =============================================

CREATE PROCEDURE [dbo].[spSoftwareProgressUpdate]
@sTerminalID Nvarchar(8),
@sSoftware Nvarchar(50),
@sSoftwareDownload Nvarchar(50),
@sSerialNumber Nvarchar(25),
@fIndex float,
@iBuildNumber INT,
@sStatus Nvarchar(50) =null

AS
DECLARE
	@iTotalIndex int,
	@fPercentage float,
	@sEdcGroup varchar(25),
	@sEdcRegion varchar(25),
	@sEdcCity VARCHAR(25),
	@iApppackID INT,
	@iCount INT

DECLARE @sLinkDatabase NVARCHAR(50),
		@sQueryGetData NVARCHAR(MAX),
		@ParmDefinitionGetData NVARCHAR(500)

SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

SET @sQueryGetData = N'SELECT @sParamEdcGroup = B.GroupRDName,@sParamEdcRegion =C.RegionRDName,@sParamEdcCity = D.CityRDName,@iParamAppPackId = F.AppPackId
FROM (
	(SELECT *
	FROM '+@sLinkDatabase+'tbListTIDRemoteDownload WITH (NOLOCK)) A
	LEFT JOIN
	(SELECT *
	FROM '+@sLinkDatabase+'tbGroupRemoteDownload WITH (NOLOCK)) B
	ON A.IdGroupRD= B.IdGroupRD
	LEFT JOIN
	(SELECT * 
	FROM '+@sLinkDatabase+'tbRegionRemoteDownload WITH (NOLOCK)) C
	ON A.IdRegionRD = C.IdRegionRD
	LEFT JOIN
	(SELECT *
	FROM '+@sLinkDatabase+'tbCityRemoteDownload WITH (NOLOCK)) D
	ON A.IdCityRD = D.IdCityRD
	LEFT JOIN
	(SELECT * 
	FROM '+@sLinkDatabase+'tbScheduleSoftware WITH (NOLOCK)) E
	ON A.ScheduleID = E.ScheduleID
	LEFT JOIN
	(SELECT *
	FROM '+@sLinkDatabase+'tbAppPackageEDCList WITH (NOLOCK)) F
	ON A.AppPackID = F.AppPackId
)
WHERE A.TerminalID = @sParamTerminalID'

SET @ParmDefinitionGetData = N'@sParamEdcGroup varchar(25) OUTPUT,@sParamEdcRegion varchar(25) OUTPUT,@sParamEdcCity Varchar(25) OUTPUT, @iParamAppPackId INT OUTPUT, @sParamTerminalID VARCHAR(8)';

EXECUTE sp_executesql @sQueryGetData,@ParmDefinitionGetData,@sParamEdcGroup = @sEdcGroup OUTPUT,@sParamEdcRegion = @sEdcRegion OUTPUT,@sParamEdcCity = @sEdcCity OUTPUT, @iParamAppPackId = @iAppPackId OUTPUT,  @sParamTerminalID = @sTerminalID;

--total index
PRINT 'INDEX'
DECLARE @sQueryGetTotalIndex NVARCHAR(MAX),
		@ParmDefinitionGetTotalIndex NVARCHAR(500)

SET @sQueryGetTotalIndex = N'SELECT @iParamTotalIndex=COUNT(*) 
FROM '+@sLinkDatabase+'tbAppPackageEDCListTemp WITH (NOLOCK)
WHERE AppPackId = @iParamAppPackId'

SET @ParmDefinitionGetTotalIndex = N'@iParamTotalIndex INT OUTPUT, @iParamAppPackId INT';

EXECUTE sp_executesql @sQueryGetTotalIndex,@ParmDefinitionGetTotalIndex,@iParamTotalIndex = @iTotalIndex OUTPUT,@iParamAppPackId = @iAppPackId;



SET @fPercentage = @fIndex/(@iTotalIndex-1)*100

PRINT '-masuk progress update'
PRINT @sSerialNumber
PRINT @sSoftwareDownload
PRINT @sSoftware
PRINT @fPercentage
PRINT @fIndex
PRINT @iTotalIndex
PRINT '--'


DECLARE @sQueryGetCount NVARCHAR(MAX),
		@ParmDefinitionGetCount NVARCHAR(500)
SET @sQueryGetCount = N'
SELECT @iParamCount=COUNT(*)
FROM '+@sLinkDatabase+'tbSoftwareProgress WITH (NOLOCK)
WHERE InstallStatus = '''+'On Progress'+'''
	AND TerminalID = @sParamTerminalID
	AND Software = @sParamSoftware
	AND SoftwareDownload = @sParamSoftwareDownload
	AND SerialNumber= @sParamSerialNumber
	AND BuildNumber = @iParamBuildNumber'

SET @ParmDefinitionGetCount = N'@iParamCount INT OUTPUT, @sParamTerminalID VARCHAR(8), @sParamSoftware VARCHAR(50), @sParamSoftwareDownload VARCHAR(50), @sParamSerialNumber VARCHAR(25), @iParamBuildNumber INT';

EXECUTE sp_executesql @sQueryGetCount,@ParmDefinitionGetCount, @iParamCount = @iCount OUTPUT, @sParamTerminalID = @sTerminalID, @sParamSoftware = @sSoftware, @sParamSoftwareDownload = @sSoftwareDownload, @sParamSerialNumber = @sSerialNumber, @iParamBuildNumber = @iBuildNumber;

IF @fIndex IS NOT NULL
BEGIN
	PRINT 'MASUK UPDATE LOG'
	IF @iCount = 1
	BEGIN
		DECLARE @sQueryUpdate1 NVARCHAR(MAX),
				@ParmDefinitionUpdate1 NVARCHAR(500)
		
		SET @sQueryUpdate1 = N'
		UPDATE '+@sLinkDatabase+'tbSoftwareProgress 
		SET Percentage = @fParamPercentage, EndTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP)
		WHERE TerminalID =@sParamTerminalID 
			AND SerialNumber = @sParamSerialNumber
			AND Software = @sParamSoftware  
			AND SoftwareDownload = @sParamSoftwareDownload 
			AND BuildNumber = @iParamBuildNumber
			AND InstallStatus = '''+'On Progress'+''''

		SET @ParmDefinitionUpdate1 = N'@fParamPercentage FLOAT,@sParamTerminalID VARCHAR(8),@sParamSerialNumber VARCHAR(25),@sParamSoftware VARCHAR(50),@sParamSoftwareDownload VARCHAR(50),@iParamBuildNumber INT';

		EXECUTE sp_executesql @sQueryUpdate1, @ParmDefinitionUpdate1, @fParamPercentage = @fPercentage, @sParamTerminalID = @sTerminalID,@sParamSerialNumber = @sSerialNumber,@sParamSoftware = @sSoftware, @sParamSoftwareDownload = @sSoftwareDownload,@iParamBuildNumber = @iBuildNumber;

	END
	ELSE IF @iCount = 0
	BEGIN
		PRINT 'MASUK UPDATE LOG AND INSERT'
		--IF @fIndex=0
		--BEGIN
		DECLARE @sQueryInsert NVARCHAR(MAX),
		@ParmDefinitionInsert NVARCHAR(500)
		SET @sQueryInsert = N'
		INSERT INTO '+@sLinkDatabase+'tbSoftwareprogress 
				(EdcGroup,
				EdcRegion,
				EdcCity,
				TerminalID,
				Software,
				BuildNumber,
				SoftwareDownload,
				SerialNumber,
				StartTime,
				Percentage,
				InstallStatus)
		VALUES(@sParamEdcGroup,
			   @sParamEdcRegion,
			   @sParamEdcCity,
			   @sParamTerminalID,
			   @sParamSoftware,
			   @iParamBuildNumber,
			   @sParamSoftwareDownload,
			   @sParamSerialNumber,
			   CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
			   @fParamPercentage,
			   '''+'On Progress'+''')'

	   SET @ParmDefinitionInsert = N'@sParamEdcGroup VARCHAR(25), @sParamEdcRegion VARCHAR(25), @sParamEdcCity VARCHAR(25),@sParamTerminalID VARCHAR(8),@sParamSoftware VARCHAR(50),@iParamBuildNumber INT,@sParamSoftwareDownload VARCHAR(50),@sParamSerialNumber VARCHAR(25),@fParamPercentage FLOAT';

	   EXECUTE sp_executesql @sQueryInsert,@ParmDefinitionInsert, @sParamEdcGroup = @sEdcGroup, @sParamEdcRegion = @sEdcRegion, @sParamEdcCity = @sEdcCity,@sParamTerminalID = @sTerminalID,@sParamSoftware = @sSoftware,@iParamBuildNumber = @iBuildNumber,@sParamSoftwareDownload = @sSoftwareDownload,@sParamSerialNumber = @sSerialNumber,@fParamPercentage =@fPercentage;

		--END
	END
END

IF @sStatus IS NOT NULL
BEGIN
	PRINT '-masuk status'
	PRINT @sStatus
	PRINT @sSerialNumber
	PRINT @sSoftwareDownload
	PRINT @sSoftware
	PRINT '---'

	DECLARE @sQueryUpdate2 NVARCHAR(MAX),
		@ParmDefinitionUpdate NVARCHAR(500)

SET @sQueryUpdate2 = N'
	UPDATE '+@sLinkDatabase+'tbSoftwareProgress 
	SET InstallStatus = @sParamStatus, FinishInstallTime = CONVERT(VARCHAR(30), CURRENT_TIMESTAMP)
	WHERE TerminalID = @sParamTerminalID 
		AND SerialNumber = @sParamSerialNumber 
		AND SoftwareDownload=@sParamSoftwareDownload 
		AND Software=@sParamSoftware 
		AND InstallStatus ='''+'On Progress'+'''
		AND BuildNumber = @iParamBuildNumber 
		AND Percentage= 100'

SET @ParmDefinitionUpdate= N' @sParamStatus VARCHAR(50),@sParamTerminalID VARCHAR(8),@sParamSerialNumber VARCHAR(25),@sParamSoftwareDownload VARCHAR(50),@sParamSoftware VARCHAR(50),@iParamBuildNumber INT'

EXECUTE sp_executesql @sQueryUpdate2,@ParmDefinitionUpdate, @sParamStatus = @sStatus, @sParamTerminalID = @sTerminalID, @sParamSerialNumber = @sSerialNumber,@sParamSoftwareDownload = @sSoftwareDownload,@sParamSoftware = @sSoftware,@iParamBuildNumber = @iBuildNumber;

END

