﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spProfileListTerminalID] 
	@sTerminalID VARCHAR(8)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @sQuery NVARCHAR(MAX),
			@sLinkDatabase NVARCHAR(50)
	
	SET @sLinkDatabase = dbo.[sGetFlagValue]('LinkedRemoteDownloadServer')

    SET @sQuery = N'SELECT TerminalID,AppPackID
					FROM '+@sLinkDatabase+'tbListTIDRemoteDownload WITH (NOLOCK)
					WHERE TerminalID = '''+@sTerminalID+''''
	EXEC (@sQuery)

END