﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spsGetDownloadApplicationName]
	@sTerminalID NVARCHAR(8)
	
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sLinkDatabase NVARCHAR(50),
		@sQuery NVARCHAR(MAX),
		@ParmDefinitionGetData NVARCHAR(500),
		@sReturn NVARCHAR(20)

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'
	
	SET @sQuery = N'
	SELECT @sParamDlAppName = AppPackageName
	FROM '+@sLinkDatabase+'tbListTIDRemoteDownload A WITH (NOLOCK)
	LEFT JOIN '+@sLinkDatabase+'tbAppPackageEDCList B  WITH (NOLOCK)
	ON A.AppPackID=B.AppPackId
	WHERE A.TerminalID =@sParamTerminalID'

	SET @ParmDefinitionGetData = N'@sParamDlAppName VARCHAR(50) OUTPUT,@sParamTerminalID VARCHAR(50)  ';

	EXECUTE sp_executesql @sQuery,@ParmDefinitionGetData, @sParamDlAppName = @sReturn OUTPUT, @sParamTerminalID = @sTerminalID 
	
	SELECT @sReturn
END
