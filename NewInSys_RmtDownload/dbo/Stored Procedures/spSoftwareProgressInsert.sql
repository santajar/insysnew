﻿-- =============================================
-- Author:		Tedie Scorfia
-- Create date: Agt 19, 2013
-- Modify date:
--				1. 
-- Description:	Insert into tbSoftwareProgress
-- =============================================

CREATE PROCEDURE [dbo].[spSoftwareProgressInsert]
@sTerminalID nvarchar(8),
@sSoftware nvarchar(50),
@sSoftwareDownload nvarchar(50),
@sSerialNumber nvarchar(25),
@fIndex float,
@iBuildNumber int

AS
DECLARE
	@iTotalIndex int,
	@fPercentage float,
	@sEdcGroup nvarchar(25),
	@sEdcRegion nvarchar(25),
	@sEdcCity nVarchar(25),
	@iAppPackId INT,
	@iCount INT
	

DECLARE @sLinkDatabase NVARCHAR(50),
		@sQueryGetData NVARCHAR(MAX),
		@ParmDefinitionGetData NVARCHAR(500)

SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

SET @sQueryGetData = N'SELECT @sParamEdcGroup=B.GroupRDName,@sParamEdcRegion=C.RegionRDName,@sParamEdcCity=D.CityRDName, @iParamAppPackId=E.AppPackId
FROM (
	(SELECT *
	FROM '+@sLinkDatabase+'tbListTIDRemoteDownload WITH (NOLOCK)) A
	LEFT JOIN
	(SELECT *
	FROM '+@sLinkDatabase+'tbGroupRemoteDownload WITH (NOLOCK)) B
	ON A.IdGroupRD= B.IdGroupRD
	LEFT JOIN
	(SELECT * 
	FROM '+@sLinkDatabase+'tbRegionRemoteDownload WITH (NOLOCK)) C
	ON A.IdRegionRD = C.IdRegionRD
	LEFT JOIN
	(SELECT *
	FROM '+@sLinkDatabase+'tbCityRemoteDownload WITH (NOLOCK)) D
	ON A.IdCityRD = D.IdCityRD
	LEFT JOIN
	(SELECT *
	FROM '+@sLinkDatabase+'tbAppPackageEDCList WITH (NOLOCK)) E
	ON A.AppPackID = E.AppPackId
)
WHERE A.TerminalID = @sParamTerminalID'

SET @ParmDefinitionGetData = N'@sParamEdcGroup varchar(25) OUTPUT,@sParamEdcRegion varchar(25) OUTPUT,@sParamEdcCity Varchar(25) OUTPUT, @iParamAppPackId INT OUTPUT, @sParamTerminalID VARCHAR(8)';

EXECUTE sp_executesql @sQueryGetData,@ParmDefinitionGetData,@sParamEdcGroup = @sEdcGroup OUTPUT,@sParamEdcRegion = @sEdcRegion OUTPUT,@sParamEdcCity = @sEdcCity OUTPUT, @iParamAppPackId = @iAppPackId OUTPUT, @sParamTerminalID = @sTerminalID;

--total index
DECLARE @sQueryGetTotalIndex NVARCHAR(MAX),
		@ParmDefinitionGetTotalIndex NVARCHAR(500)

SET @sQueryGetTotalIndex = N'SELECT @iParamTotalIndex=COUNT(*) 
							FROM '+@sLinkDatabase+'tbAppPackageEDCListTemp WITH (NOLOCK)
							WHERE AppPackId = @iParamAppPackId';

SET @ParmDefinitionGetTotalIndex = N'@iParamTotalIndex INT OUTPUT, @iParamAppPackId INT';

EXECUTE sp_executesql @sQueryGetTotalIndex,@ParmDefinitionGetTotalIndex,@iParamTotalIndex = @iTotalIndex OUTPUT,@iParamAppPackId = @iAppPackId;


PRINT 'MASUK SOFTWARE PROGRESS INSERT'
PRINT 'SOFTWARENAME = '+@sSoftware
PRINT 'SOFTWAREDOWNLOAD = '+@sSoftwareDownload
PRINT @fIndex
PRINT @iTotalIndex
PRINT '----'

SET @fPercentage = @fIndex / @iTotalIndex * 100


DECLARE @sQueryGetCount NVARCHAR(MAX),
		@ParmDefinitionGetCount NVARCHAR(500)

SET @sQueryGetCount = N'
SELECT @iParamCount=COUNT(*)
FROM '+ @sLinkDatabase +'tbSoftwareProgress WITH (NOLOCK)
WHERE InstallStatus = '''+ 'On Progress' +'''
	AND TerminalID = @sParamTerminalID
	AND Software = @sParamSoftware
	AND SoftwareDownload = @sParamSoftwareDownload
	AND SerialNumber= @sParamSerialNumber
	AND BuildNumber = @iParamBuildNumber'

SET @ParmDefinitionGetCount = N'@iParamCount INT OUTPUT, @sParamTerminalID VARCHAR(8), @sParamSoftware VARCHAR(50), @sParamSoftwareDownload VARCHAR(50), @sParamSerialNumber VARCHAR(25), @iParamBuildNumber INT';

EXECUTE sp_executesql @sQueryGetCount,@ParmDefinitionGetCount, @iParamCount = @iCount OUTPUT, @sParamTerminalID = @sTerminalID, @sParamSoftware = @sSoftware, @sParamSoftwareDownload = @sSoftwareDownload, @sParamSerialNumber = @sSerialNumber, @iParamBuildNumber = @iBuildNumber;

IF (@iCount>0)
BEGIN
	EXEC spSoftwareProgressUpdate
		@sTerminalID,
		@sSoftware,
		@sSoftwareDownload,
		@sSerialNumber,
		@fPercentage,
		@iBuildNumber,
		'On Progress'
END
ELSE
BEGIN

DECLARE @sQueryInsert NVARCHAR(MAX),
		@ParmDefinitionInsert NVARCHAR(500)
SET @sQueryInsert = N'
	INSERT INTO '+@sLinkDatabase+'tbSoftwareprogress 
				(EdcGroup,
				EdcRegion,
				EdcCity,
				TerminalID,
				Software,
				BuildNumber,
				SoftwareDownload,
				SerialNumber,
				StartTime,
				Percentage,
				InstallStatus)
	VALUES(@sParamEdcGroup,
		   @sParamEdcRegion,
		   @sParamEdcCity,
		   @sParamTerminalID,
		   @sParamSoftware,
		   @iParamBuildNumber,
		   @sParamSoftwareDownload,
		   @sParamSerialNumber,
		   CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
		   @fParamPercentage,
		   '''+ 'On Progress' +''')'

SET @ParmDefinitionInsert = N'@sParamEdcGroup VARCHAR(25), @sParamEdcRegion VARCHAR(25), @sParamEdcCity VARCHAR(25),@sParamTerminalID VARCHAR(8),@sParamSoftware VARCHAR(50),@iParamBuildNumber INT,@sParamSoftwareDownload VARCHAR(50),@sParamSerialNumber VARCHAR(25),@fParamPercentage FLOAT';

EXECUTE sp_executesql @sQueryInsert,@ParmDefinitionInsert, @sParamEdcGroup = @sEdcGroup, @sParamEdcRegion = @sEdcRegion, @sParamEdcCity = @sEdcCity,@sParamTerminalID = @sTerminalID,@sParamSoftware = @sSoftware,@iParamBuildNumber = @iBuildNumber,@sParamSoftwareDownload = @sSoftwareDownload,@sParamSerialNumber = @sSerialNumber,@fParamPercentage =@fPercentage;

END
