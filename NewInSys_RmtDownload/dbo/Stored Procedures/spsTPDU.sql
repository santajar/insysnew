﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spsTPDU]
	@sReturn NVARCHAR(50) OUTPUT
AS
BEGIN

DECLARE @sLinkDatabase NVARCHAR(50),
		@sQuery NVARCHAR(MAX),
		@ParmDefinitionGetData NVARCHAR(500)

SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'
	
	SET NOCOUNT ON;

SET @sQuery = N'SELECT @sParamReturn = Flag 
		FROM '+@sLinkDatabase+'tbControlflag
		WITH (NOLOCK)
		WHERE ItemName='''+'TPDU'+''''

SET @ParmDefinitionGetData = N'@sParamReturn varchar(50) OUTPUT';

EXECUTE sp_executesql @sQuery,@ParmDefinitionGetData, @sParamReturn = @sReturn OUTPUT

END
