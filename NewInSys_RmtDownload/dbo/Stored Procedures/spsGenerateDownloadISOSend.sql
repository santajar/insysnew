﻿
CREATE PROCEDURE [dbo].[spsGenerateDownloadISOSend]
	@sTerminalId NVARCHAR(8),
	@sProcCodeRecv NVARCHAR(6),
	@sNII NVARCHAR(4),
	@sAdress NVARCHAR(4),
	@sBit11 NVARCHAR(6)=NULL,
	@sBit48 NVARCHAR(MAX)=NULL,
	@sBit60 NVARCHAR(MAX),
	@sAppName NVARCHAR(MAX),
	@iCounterApp INT,
	@iDownloadApp BIT = 0,
	@sISO NVARCHAR(MAX) OUTPUT
AS
BEGIN
	DECLARE @sBit12 NVARCHAR(6), 
		@sBit13 NVARCHAR(6),
		@iCountAppName int
	
	DECLARE @sLinkDatabase NVARCHAR(50)
	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'


	DECLARE @sTPDU NVARCHAR(50)
	EXEC spsTPDU @sTPDU OUTPUT
	
	SET @sISO = @sTPDU
			+ @sAdress
			+ @sNII
			
	DECLARE @sBitMap NVARCHAR(MAX)
	IF @sBit11 IS NOT NULL
	BEGIN
		DECLARE @sMtiResponse NVARCHAR(MAX)
		EXEC spsMTIResponse @sMtiResponse OUTPUT
		SET @sISO = @sISO 
					+ @sMtiResponse
		
		
		-- BitMap
		IF @sBit48 IS NULL
		begin
			
			if @sAppName is not null 
			begin
				DECLARE @sQuery VARCHAR(MAX),
						@ParmDefinition VARCHAR(500)
				
				SET @sQuery  = N'select @iParamCountAppName=count(ApplicationName) from '+@sLinkDatabase+'tbBitMap where ApplicationName = @sParamAppName'
				SET @ParmDefinition = N'@iParamCountAppName INT OUTPUT,@sParamAppName VARCHAR(MAX)'
				EXECUTE SP_EXECUTESQL @sQuery, @ParmDefinition, @iParamCountAppName = @iCountAppName OUTPUT, @sParamAppName = @sAppName
				
				if @iCountAppName=1
				BEGIN
					EXEC spsBitmap @sAppName, @sBitMap OUTPUT
					SET @sISO = @sISO 
					+ @sBitMap
					--+ dbo.sBitMap(@sAppName)
				END
				else
				BEGIN
					EXEC spsBitmap NULL, @sBitMap OUTPUT
					SET @sISO = @sISO 
					+ @sBitMap
				END
			end
			else
			BEGIN
			EXEC spsBitmap NULL, @sBitMap OUTPUT
			SET @sISO = @sISO 
				+ @sBitMap
			END
			end
		ELSE
		BEGIN
		
			IF @iDownloadApp=0
			BEGIN
				EXEC spsBitmap @sAppName, @sBitMap OUTPUT
				SET @sISO = @sISO + @sBitMap

			END
			ELSE
			BEGIN
				EXEC spsBitmap 'DownloadApp', @sBitMap OUTPUT
				SET @sISO = @sISO + @sBitMap
			END
			PRINT '1?'
		PRINT @sISO
		PRINT @sAppName
			SELECT @sBit12 = dbo.sGetTimeHHMMSS()
			if @sProcCodeRecv= '990000' or @sProcCodeRecv = '990001' or @sProcCodeRecv='990010' or @sProcCodeRecv = '990011' OR @sProcCodeRecv = '990020' --proccode untuk download software
			begin
			SELECT @sBit13 = dbo.sGetDateMMDD()
			end
			else
			begin
			SELECT @sBit13 = dbo.sGetDateMMDDYY()
			end
		END
		
		-- Bit3
		SET @sISO = @sISO 
				+ dbo.sProcCode(@sTerminalId, @sProcCodeRecv, @iCounterApp)

		-- Bit11,12,13,24,39,41,48
		IF @sBit48 IS NOT NULL
		BEGIN
			SET @sISO = @sISO 
					+ @sBit11
					+ @sBit12
					+ @sBit13
					+ @sNII
					+ dbo.sStringToHexString('00')
					+ dbo.sStringToHexString(@sTerminalId)
					+ @sBit48
			IF @iDownloadApp=1
			BEGIN
				DECLARE @sGenerateDownloadFiller VARCHAR(200)
				EXEC spsGenerateDownloadFiller @sTerminalId, @iCounterApp, @sGenerateDownloadFiller OUTPUT
				SET @sISO = @sISO 
					+ '0100' +@sGenerateDownloadFiller
			END
		END
		ELSE
		BEGIN
			SET @sISO = @sISO 
					+ @sBit11
					+ @sNII
					+ dbo.sStringToHexString('00')
					+ dbo.sStringToHexString(@sTerminalId)
		END

		-- Bit60
		IF SUBSTRING(@sBit60,1,4) = dbo.sStringToHexString('PK')
		BEGIN			
			SET @sISO = @sISO
				+ dbo.sPadLeft(LEN(@sBit60) / 2,4-LEN(LEN(@sBit60) / 2),'0')
			SET @sISO = @sISO 
				+ @sBit60
		END
		ELSE
		BEGIN
			IF ISNULL(@sBit60,'') <> ''
			BEGIN
				SET @sISO = @sISO + dbo.sPadLeft(LEN(@sBit60) / 2,4-LEN(LEN(@sBit60) / 2),'0')
				SET @sISO = @sISO + @sBit60
			END
		END
	END
	ELSE
	BEGIN
		IF ISNULL(@sBit60,'') <> ''
			SET @sISO = @sISO + dbo.sStringToHexString(@sBit60)
	END
	
END

