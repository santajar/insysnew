﻿CREATE PROCEDURE [dbo].[spControlFlagInitBrowse]
	@sMaxConn VARCHAR(50),
	@sInitPort VARCHAR(50),
	@sSoftwarePort VARCHAR(50)
AS
	DECLARE @sMaxConnValue VARCHAR(64)
	DECLARE @sInitPortValue VARCHAR(64)
	DECLARE @sSoftwarePortValue VARCHAR(64)

	EXEC spControlFlagBrowse @sMaxConn, @sMaxConnValue OUTPUT
	EXEC spControlFlagBrowse @sInitPort, @sInitPortValue OUTPUT
	EXEC spControlFlagBrowse @sSoftwarePort, @sSoftwarePortValue OUTPUT

	DECLARE @sStmt VARCHAR(200)
	SET @sStmt = 'SELECT ''' + ISNULL(@sMaxConnValue,'') + '''[' + @sMaxConn + '], ''' + 
				 ISNULL(@sInitPortValue,'') + ''' [' + @sInitPort + '], ''' + 
				 ISNULL(@sSoftwarePortValue,'') + ''' [' + @sSoftwarePort + ']' 
	--SELECT @sMaxConnValue [@sMaxConn], @sInitPortValue AS [@sInitPort], @sSoftwarePortValue AS [@sSoftwarePort]
	EXEC (@sStmt)


