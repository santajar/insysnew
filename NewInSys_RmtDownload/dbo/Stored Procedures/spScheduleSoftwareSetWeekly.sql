﻿CREATE PROCEDURE [dbo].[spScheduleSoftwareSetWeekly]
	@sScheduleType VARCHAR(10),
	@sScheduleCek VARCHAR(10),
	@dtScheduleTime DATETIME,
	@sTYPE VARCHAR(2) OUTPUT,
	@sCek VARCHAR(2) OUTPUT,
	@sTime VARCHAR(4) OUTPUT
AS
DECLARE @sHour VARCHAR(2),
		@sMinute VARCHAR(2)

SET @sType = 'WE'
print '@sType ' + @sType
IF (@dtScheduleTime is not null)
BEGIN
	print '@dtScheduleTime'
	SET @sHour = RIGHT('0'+CONVERT(VARCHAR, DATEPART(HOUR,@dtScheduleTime)),2)
	SET @sMinute = RIGHT('0'+CONVERT(VARCHAR, DATEPART(MINUTE,@dtScheduleTime)),2)
	SET @sTime = @sHour+@sMinute
	print '@sTime '+@sTime
END
ELSE
BEGIN
	SET @sTime = '0000'
END
IF ISNULL(@sScheduleCek,'')<>''
BEGIN
	-- validasi to set Days					
	IF (@sScheduleCek='Sunday')
	BEGIN
		SET @sCek = '00'
	END
	ELSE IF (@sScheduleCek='Monday')
	BEGIN
		SET @sCek = '01'
	END
	ELSE IF (@sScheduleCek='Tuesday')
	BEGIN
		SET @sCek = '02'
	END
	ELSE IF (@sScheduleCek='Wednesday')
	BEGIN
		SET @sCek = '03'
	END
	ELSE IF (@sScheduleCek='Thursday')
	BEGIN
		SET @sCek = '04'
	END
	ELSE IF (@sScheduleCek='Friday')
	BEGIN
		SET @sCek = '05'
	END
	ELSE IF (@sScheduleCek='Saturday')
	BEGIN
		SET @sCek = '06'
	END
ELSE
BEGIN
	SET @sCek = '00'
END
	PRINT '@sCek '+@sCek
END
ELSE
BEGIN
	PRINT 'Day of schedule is not setting'
END
