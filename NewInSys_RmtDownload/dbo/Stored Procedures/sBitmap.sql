﻿
-- ====================================================================== --
-- Function Name : sBitmap
-- Parameter	 : 
--		1. 
-- Return		 :
--		ex.: sBitmap() --> '0810'
-- Description	 :
--		return string Flag from tbControlFlag where ItemName is HexBitMap
-- ====================================================================== --
Create Procedure [dbo].[sBitmap](@sAppName VARCHAR(MAX),
	 @sReturn VARCHAR(MAX) OUTPUT
)
AS
BEGIN
	DECLARE @sValue NVARCHAR(MAX)
	

	EXEC spsGetApplicationName @sAppName, @sReturn OUTPUT
	SET @sAppName = @sReturn
	
	DECLARE @sLinkDatabase NVARCHAR(50)
	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag
	WHERE ItemName= 'LinkedRemoteDownloadServer'

	DECLARE @sQuery NVARCHAR(MAX),
			@sParamDefinition NVARCHAR(MAX)

	SET @sQuery =N'
	SELECT @sParamValue=HexBitMap 
	FROM tbBitMap
	WHERE ApplicationName=@sParamAppName'

	SET @sParamDefinition = N'@sParamValue VARCHAR(MAX) OUTPUT, @sParamAppName VARCHAR(MAX)';

	EXECUTE sp_executesql @sQuery, @sParamDefinition, @sParamValue = @sValue OUTPUT, @sParamAppName = @sAppName;

	RETURN @sValue
END





