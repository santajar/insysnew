﻿CREATE PROCEDURE [dbo].[spsGenerateDownloadFiller]
	@sTerminalId VARCHAR(8),
	@iCounterApp INT,
	@sReturn VARCHAR(200) OUTPUT
	
AS
BEGIN
DECLARE @sExtraData VARCHAR(200),
	@sDownloadedAppName VARCHAR(150),
	@sDownloadedAppFilename VARCHAR(150),
	@sHexDownloadedAppName VARCHAR(300),
	@sHexDownloadedAppFilename VARCHAR(300),
	@iLenDownloadedAppName INT,
	@iLenDownloadedAppFilename INT,
	@sLenDownloadedAppName VARCHAR(4),
	@sLenDownloadedAppFilename VARCHAR(4),
	@sCounterApp VARCHAR(4),
	@iBuildNumber INT,
	@sBuildNumber VARCHAR(4)

DECLARE @sLinkDatabase NVARCHAR(50),
		@sQuery NVARCHAR(MAX),
		@sParamDefinition NVARCHAR(MAX)

SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName= 'LinkedRemoteDownloadServer'

SET @sQuery =N'
SELECT @sParamDownloadedAppName = AppPackageName, @sParamDownloadedAppFilename = AppPackageFilename, @iParamBuildNumber=BuildNumber
FROM '+@sLinkDatabase+'tbAppPackageEDCList app WITH (NOLOCK) JOIN '+@sLinkDatabase+'tbListTIDRemoteDownload terminal WITH (NOLOCK)
ON app.AppPackId = terminal.AppPackId
WHERE TerminalID = @sParamTerminalId'

SET @sParamDefinition = N'@sParamDownloadedAppName VARCHAR(150) OUTPUT,@sParamDownloadedAppFilename VARCHAR(150) OUTPUT,@iParamBuildNumber INT OUTPUT,@sParamTerminalId VARCHAR(8)';

EXECUTE sp_executesql @sQuery,@sParamDefinition,@sParamDownloadedAppName = @sDownloadedAppName OUTPUT, @sParamDownloadedAppFilename = @sDownloadedAppFilename OUTPUT,@iParamBuildNumber = @iBuildNumber OUTPUT, @sParamTerminalId = @sTerminalId

SET @iLenDownloadedAppName = LEN(@sDownloadedAppName)
SET @iLenDownloadedAppFilename = LEN(@sDownloadedAppFilename)

SET @sLenDownloadedAppName = REPLICATE('0', 4-LEN(@iLenDownloadedAppName)) 
	+ CONVERT(VARCHAR,@iLenDownloadedAppName)
SET @sLenDownloadedAppFilename = REPLICATE('0', 4-LEN(@iLenDownloadedAppFilename)) 
	+ CONVERT(VARCHAR,@iLenDownloadedAppFilename)
SET @sCounterApp = REPLICATE('0', 4-LEN(@iCounterApp)) 
	+ CONVERT(VARCHAR,@iCounterApp)
SET @sBuildNumber = REPLICATE('0', 4-LEN(@iBuildNumber)) 
	+ CONVERT(VARCHAR,@iBuildNumber)

SET @sHexDownloadedAppName = dbo.sStringToHexString(@sDownloadedAppName)
SET @sHexDownloadedAppFilename = dbo.sStringToHexString(@sDownloadedAppFilename)
	
SET @sReturn = @sCounterApp 
	+ @sLenDownloadedAppName + @sHexDownloadedAppName
	+ @sBuildNumber
	+ @sLenDownloadedAppFilename + @sHexDownloadedAppFilename
	+ REPLICATE('F',200 - 4 - 4 - LEN(@sHexDownloadedAppName) - 4 - LEN(@sHexDownloadedAppFilename))


END	



