﻿

-- =============================================
-- Author:		SUPRIYADI SETYO
-- Create date: Agt 19, 2013
-- Modify date:
--				1. 
-- Description:	Insert into tbAuditInit
-- =============================================
CREATE PROCEDURE [dbo].[spAuditInitSoftwareInsert]
	@sTerminalID NVARCHAR(8),
	@sSoftware NVARCHAR(25) = Null,
	@SoftwareDownload NVARCHAR(25)=Null,
	@sSerialNum NVARCHAR(20),
	@iIndex INT
AS

DECLARE @sQuery NVARCHAR(MAX),
		@sLinkDatabase NVARCHAR(50),
		@ParmDefinition NVARCHAR(500)

	SELECT @sLinkDatabase = FLAG
	FROM tbControlFlag
	WHERE ItemName= 'LinkedRemoteDownloadServer'

	SET @sQuery = N'INSERT INTO	'+@sLinkDatabase+'tbAuditInitSoftware(
		TerminalId,
		Software,
		SoftwareDownload,
		SerialNumber,
		InitTime,
		IndexSoftware)
	VALUES(
		@sParamTerminalID,
		@sParamSoftware,
		@sParamSoftwareDownload,
		@sParamSerialNum,
		CONVERT(VARCHAR(30), CURRENT_TIMESTAMP),
		@iParamIndex)'

	SET @ParmDefinition = N'@sParamTerminalID VARCHAR(8),@sParamSoftware VARCHAR(25),@sParamSoftwareDownload VARCHAR(25),@sParamSerialNum VARCHAR(20),@iParamIndex INT';

	EXECUTE sp_executesql @sQuery,@ParmDefinition,@sParamTerminalID = @sTerminalID,@sParamSoftware = @sSoftware,@sParamSoftwareDownload = @SoftwareDownload ,@sParamSerialNum = @sSerialNum,@iParamIndex = @iIndex;


