﻿
-- =============================================
-- Description:	Get FlagValue for given ItemName
-- =============================================

CREATE PROCEDURE [dbo].[spControlFlagBrowse]
	@sItemName VARCHAR(50) = '',
	@sFlagValue VARCHAR(64) OUTPUT
AS
	SELECT @sFlagValue = Flag 
	FROM tbControlFlag WITH (NOLOCK)
	WHERE ItemName = @sItemName
