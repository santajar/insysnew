﻿CREATE FUNCTION [dbo].[sGetFlagValue] (@sItemName VARCHAR(50))
	RETURNS VARCHAR(64)
BEGIN
	DECLARE @sFlagValue VARCHAR(64)
	SELECT @sFlagValue = Flag 
	FROM tbControlFlag
	WITH (NOLOCK)
	WHERE ItemName = @sItemName 
	
	RETURN @sFlagValue 
END

