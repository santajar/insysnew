﻿CREATE FUNCTION [dbo].[IsRemoteDownloadAllowed](@sTerminalId VARCHAR(8))
RETURNS BIT
BEGIN
	RETURN(SELECT RemoteDownload FROM development.newinsys.dbo.tbProfileTerminalList
		WHERE TerminalId=@sTerminalId)
END

