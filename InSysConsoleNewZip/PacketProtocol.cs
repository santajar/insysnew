﻿using InSysClass;
using System.Collections.Generic;
using System.Configuration;

namespace InSysConsoleNewZip
{
    public class PacketProtocol
    {
        byte[] arrbPacket;
        List<byte[]> ltarrbRecvPacket = new List<byte[]>();
        int iTotalPacket = 0;
        static string sTpduNii = "600996";

        public PacketProtocol(byte[] _arrbPacket)
        {
            arrbPacket = new byte[_arrbPacket.Length];
            arrbPacket = _arrbPacket;
            sTpduNii = ConfigurationManager.AppSettings["TPDU NII"].ToString();

            ProcessPacket();
        }

        protected void ProcessPacket()
        {
            int iIndex = 0;

            string sTemp = CommonLib.sByteArrayToHexString(arrbPacket).Replace(" ", "");
            int iLengthOfRecvPacket = sTemp.Length;

            while ((iIndex = sTemp.IndexOf(sTpduNii, iIndex)) > -1)
            {
                int iLengthOfPacket = CommonConsole.iISOLenReceive(sTemp.Substring(iIndex - 4, 4));

                string sTempPacket = sTemp.Substring(iIndex - 4, 4 + iLengthOfPacket * 2);
                if (!string.IsNullOrEmpty(sTempPacket))
                {
                    ltarrbRecvPacket.Add(CommonLib.HexStringToByteArray(sTempPacket));
                    iTotalPacket++;

                    iIndex += iLengthOfPacket * 2;
                    if (iIndex == iLengthOfRecvPacket || iIndex > iLengthOfRecvPacket) break;
                }
                else
                    break;
            };
        }

        public void GetPacketList(ref List<byte[]> _ltarrbPacket, ref int _iTotalPacket)
        {
            _ltarrbPacket = ltarrbRecvPacket;
            _iTotalPacket = iTotalPacket;
        }
    }
}
