using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Threading;
using InSysClass;

namespace InSysConsoleNewZip_License
{
    public class ContentProfile
    {
        string sTag;
        public string Tag { get { return sTag; } }
        string sContent;
        public string Content { get { return sContent; } }

        public ContentProfile(string _sTag, string _sContent)
        {
            sTag = _sTag;
            sContent = _sContent;
        }
    }

    public class ClassInit
    {        
        protected string sTerminalID;
        protected string sAppName;
        protected string sSerialNum;
        protected int iPercentage = 0;
        protected DataTable dtInitResponse = new DataTable();
        protected bool bHexContent;
        protected bool bEnable48;
        protected bool bEnableCompress;

        public ClassInit() { }
        public ClassInit(string _sTID)
        {
            sTerminalID = _sTID;
        }

        public string TerminalID { get { return sTerminalID; } set { sTerminalID = value; } }
        public string AppName { get { return sAppName; } set { sAppName = value; } }
        public string SerialNumber { get { return sSerialNum; } set { sSerialNum = value; } }
        public bool IsLastPacket { get { return bIsLastPacket(); } }
        public int Percentage { get { return iPercentage; } }
        public bool HexContent { get { return bHexContent; } set { bHexContent = value; } }
        public bool Enable48 { get { return bEnable48; } set { bEnable48 = value; } }
        public bool EnableCompress { get { return bEnableCompress; } set { bEnableCompress = value; } }

        public DataTable dtResponses
        {
            set { dtInitResponse = value; }
        }

        public string Content { get { return sContent(); } }

        protected string sContent()
        {
            string sTempContent = "";

            var sTemp = (from tableInit in dtInitResponse.AsEnumerable()
                        where tableInit.Field<bool>("Flag") == false
                        select tableInit.Field<string>("Content")).Take(1);
            sTempContent = (sTemp.ToArray())[0];

            return sTempContent;
        }

        public string Tag { get { return sTag(); } }

        protected string sTag()
        {
            string sTempTag = "";

            var sTemp = (from tableInit in dtInitResponse.AsEnumerable()
                         where tableInit.Field<bool>("Flag") == false
                         select tableInit.Field<string>("Tag")).Take(1);
            sTempTag = (sTemp.ToArray())[0];

            return sTempTag;
        }

        public void UpdateFlag()
        {
            DataRow[] arrrowsUpdate = dtInitResponse.Select("Flag=0");
            int iIndexRow = dtInitResponse.Rows.IndexOf(arrrowsUpdate[0]);
            dtInitResponse.Rows[iIndexRow]["Flag"] = 1;

            UpdatePercentage();
        }

        protected void UpdatePercentage()
        {
            iPercentage = (dtInitResponse.Select("Flag=1").Length * 100) / dtInitResponse.Rows.Count;
        }
        
        protected bool bIsLastPacket()
        {
            DataRow[] arrrowsUpdate = dtInitResponse.Select("Flag=0");
            return (arrrowsUpdate.Length == 1);
        }

    }

}
