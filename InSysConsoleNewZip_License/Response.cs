using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Threading;
using InSysClass;

namespace InSysConsoleNewZip_License
{
    class Response
    {
        /*
         *  Old Response Class
         */
        protected SqlConnection oConn;
        protected bool bCheckAllowInit;

        protected int iCountInit = 0;
        public int iMaxInit = 32; 
        protected bool bInit = false;
        protected int iPercentage = 0;
        protected bool bStart = false;
        protected bool bComplete = false;
        protected int iMaxByte = 400;
        protected bool bEnable48 = false;
        protected bool bEnableCompress = false;

        /// <summary>
        /// Start the process of get response
        /// </summary>
        /// <param name="arrbReceive">byte array : the received message from the NAC or terminal</param>
        /// <returns>byte array : ISO message response</returns>
        //public static byte[] arrbResponse(byte[] arrbReceive, ConnType cType, SqlConnection _oConn, bool _IsCheckAllowInit)
        public byte[] arrbResponse(byte[] arrbReceive, ConnType cType, SqlConnection _oConn, bool _IsCheckAllowInit)
        {
            oConn = _oConn;
            bCheckAllowInit = _IsCheckAllowInit;
            byte[] arrbTemp = new byte[1024];
            try
            {
                int iIsoLen = 0;
                string sSendMessage = sBeginGetResponse(arrbReceive, ref iIsoLen, cType);
                if (!string.IsNullOrEmpty(sSendMessage))
                {
                    arrbTemp = new byte[iIsoLen + 2];
                    //arrbTemp = CommonLib.HexStringToByteArray(sSendMessage);

                    sSendMessage = sSendMessage.Replace(" ", ""); // Remove all white space
                    byte[] arrbbuffer = new byte[sSendMessage.Length / 2];
                    for (int iCount = 0; iCount < sSendMessage.Length; iCount += 2)
                    {
                        arrbbuffer[iCount / 2] = (byte)Convert.ToByte(sSendMessage.Substring(iCount, 2), 16);
                    }
                    arrbTemp = arrbbuffer;
                }
            }
            catch (Exception ex)
            {
                Logs.Write("|[RESPONSE]|Error arrbResponse : " + ex.Message);
                //Console.WriteLine("[RESPONSE] Error : {0}", ex.Message);
                //Console.ReadKey();
            }
            return arrbTemp;
        }

        /// <summary>
        /// Get the ISO message response from the database
        /// </summary>
        /// <param name="_arrbRecv">byte array : Received message from the NAC or Terminal</param>
        /// <param name="_iIsoLenResponse">int : ISO message response length</param>
        /// <returns>string : ISO message response in HexString</returns>
        protected string sBeginGetResponseOld(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTerminalId = null;
            string sTag = null;
            int iErrorCode = -1;

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            Trace.Write("|[RESPONSE]|sTemp to SQL Procedure : " + sTemp);

            // kirim ke sp,...
            try
            {
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 27;
                    //oCmd.CommandTimeout = 0;
                    oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                    oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                    #region "Using DataReader"
                    //SqlDataReader reader;
                    using (SqlDataReader reader = oCmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                            sTerminalId = reader["TerminalId"].ToString();
                            sTempReceive = reader["ISO"].ToString();
                            sTag = reader["Tag"].ToString();
                            string scont = reader["Content"].ToString();

                            //Trace.Write("[RESPONSE] Cont : " + scont);
                        }
                    }
                    //reader.Close();
                    //reader.Dispose();
                    #endregion
                }
                //oCmd.Dispose();

                if (!string.IsNullOrEmpty(sTempReceive))
                {
                    // tambahkan total length
                    _iIsoLenResponse = sTempReceive.Length / 2;
                    sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                    Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive : {1}", sTerminalId, sTempReceive));

                    if (iErrorCode == 0 || iErrorCode == 1)
                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                }
                //Trace.Write(string.Format("[RESPONSE] {0} {1}", sTerminalId, sProcessing(iErrorCode, sTag)));
            }
            catch (Exception ex)
            {
                //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                //    ex.ToString(), sTemp);
                Logs.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                    ex.ToString(), sTemp));
                //Console.ReadKey();
                //sTemp = null;
            }
            return sTempReceive;
        }
        protected string sBeginGetResponseBatch(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTerminalId = null;
            string sAppName = null;
            string sSerialNum = null;
            string sTag = null;
            int iErrorCode = -1;

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            // parse received ISO
            ISO8583Lib oIsoParse = new ISO8583Lib();
            ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
            byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
            oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
            string sProcCode = oIsoMsg.sBit[3];
            sTerminalId = oIsoMsg.sTerminalId;
            
            Console.WriteLine("sProcCode = {0}", sProcCode);

            // kirim ke sp,...
            try
            {
                if (sProcCode != "930001" && sProcCode != "960001")
                {
                    Trace.Write("|[RESPONSE]|sTemp to SQL Procedure : " + sTemp);
                    if (sProcCode != "930000" && sProcCode != "960000")
                    {
                        Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

                        #region "NOT 930000 AND NOT 960000"
                        using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                        {
                            oCmd.CommandType = CommandType.StoredProcedure;
                            oCmd.CommandTimeout = 25;
                            oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                            oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                            #region "Using DataReader"
                            using (SqlDataReader reader = oCmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                    sTerminalId = reader["TerminalId"].ToString();
                                    sTempReceive = reader["ISO"].ToString();
                                    sTag = reader["Tag"].ToString();
                                    string scont = reader["Content"].ToString();
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        iErrorCode = 0;
                        Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

                        #region "930000 AND 960000"
                        if (!string.IsNullOrEmpty(oIsoMsg.sBit[48]))
                        {
                            //Console.WriteLine("AppNameSN {0}", sTemp);

                            CommonConsole.GetAppNameSN(oIsoMsg.sBit[48], ref sAppName, ref sSerialNum);
                            bEnable48 = true;
                            bEnableCompress = IsCompressInit(sTerminalId);
                        }

                        if (IsApplicationAllowInit(sTerminalId, sAppName))
                        {
                            bInit = true;
                            string sContent = null;
                            bool bHexContent = false;
                            DataTable dtInitTemp = dtGetInitTemp(sTerminalId, ref bHexContent, sAppName);
                            if (dtInitTemp.Rows.Count > 0)
                            {
                                ClassInit oClass = new ClassInit(sTerminalId);
                                oClass.dtResponses = dtInitTemp;
                                oClass.SerialNumber = sSerialNum;
                                oClass.AppName = sAppName;
                                oClass.HexContent = bHexContent;
                                oClass.Enable48 = bEnable48;
                                oClass.EnableCompress = bEnableCompress;

                                int iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                                if (iIndex > -1)
                                    Program.ltClassInit.RemoveAt(iIndex);

                                Program.ltClassInit.Add(oClass);
                                sTag = oClass.Tag;
                                sContent = oClass.Content;
                                oClass.UpdateFlag();
                                iPercentage = oClass.Percentage;

                                Trace.Write(string.Format("|[sContent]|{0}|sTempReceive {1}", sContent, sProcCode));
                                sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, false, bHexContent, bEnable48, bEnableCompress);
                                bStart = true;
                            }
                            else
                            {
                                iErrorCode = 4;
                                sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("INVALIDTID"), oIsoMsg);
                            }
                        }
                        else
                        {
                            iErrorCode = 6;
                            sTempReceive = CommonConsole.sGenerateISOError(CommonLib.sStringToHex("APPLICATION NOT ALLOWED"), oIsoMsg);
                        }
                        #endregion
                    }
                }
                else
                {
                    Trace.Write("|[RESPONSE]|sTemp to SQL Procedure " + sProcCode + " : " + sTemp);

                    #region "930001 AND 960001"
                    bInit = true;

                    int iIndex = Program.ltClassInit.FindIndex(classInitTemp => classInitTemp.TerminalID == sTerminalId);
                    if (iIndex > -1)
                    {
                        bComplete = Program.ltClassInit[iIndex].IsLastPacket;
                        iErrorCode = bComplete ? 1 : 0;
                        sTag = Program.ltClassInit[iIndex].Tag;
                        string sContent = Program.ltClassInit[iIndex].Content;
                        bool bHexContent = Program.ltClassInit[iIndex].HexContent; 
                        bEnable48 = Program.ltClassInit[iIndex].Enable48;
                        bEnableCompress = Program.ltClassInit[iIndex].EnableCompress;
                        sTempReceive = CommonConsole.sGenerateISOSend(sContent, oIsoMsg, false, bHexContent, bEnable48, bEnableCompress);
                        Program.ltClassInit[iIndex].UpdateFlag();
                        iPercentage = Program.ltClassInit[iIndex].Percentage;
                    }
                    #endregion
                }
                if (!string.IsNullOrEmpty(sTempReceive))
                {
                    // tambahkan total length
                    _iIsoLenResponse = sTempReceive.Length / 2;
                    sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                    Trace.Write(string.Format("|[RESPONSE]|{0}|ErrorCode : {2}|sTempReceive : {1}", sTerminalId, sTempReceive, iErrorCode));

                    //if (iErrorCode == 0 || iErrorCode == 1)
                    CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                //    ex.ToString(), sTemp);
                Logs.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                    ex.ToString(), sTemp));
                //Console.ReadKey();
                //sTemp = null;
            }
            return sTempReceive;
        }

        protected string sBeginGetResponse(byte[] _arrbRecv, ref int _iIsoLenResponse, ConnType cType)
        {
            string sTerminalId = null;
            string sTag = null;
            int iErrorCode = -1;

            // convert _arrbRecv ke hexstring,
            string sTemp = CommonLib.sByteArrayToHexString(_arrbRecv).Replace(" ", "");
            string sTempReceive = null;

            if (cType != ConnType.Modem)
            {
                int iLen = iISOLenReceive(sTemp.Substring(0, 4), cType);

                // buang total length & tpdu
                sTemp = sTemp.Substring(6, (iLen - 1) * 2);
            }
            else
                sTemp = sTemp.Substring(2);

            // parse received ISO
            ISO8583Lib oIsoParse = new ISO8583Lib();
            ISO8583_MSGLib oIsoMsg = new ISO8583_MSGLib();
            byte[] arrbTemp = CommonLib.HexStringToByteArray("60" + sTemp);
            oIsoParse.UnPackISO(arrbTemp, ref oIsoMsg);
            string sProcCode = oIsoMsg.sBit[3];
            sTerminalId = oIsoMsg.sTerminalId;

            Console.WriteLine("sProcCode = {0}", sProcCode);

            // kirim ke sp,...
            try
            {
                Trace.Write("|[RESPONSE]|sTemp to SQL Procedure : " + sTemp);
                if (sProcCode == "950000")
                {
                }
                else if (sProcCode != "930000" && sProcCode != "930001")
                {
                    #region "NOT 930000/1"
                    using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                    {
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.CommandTimeout = 27;
                        //oCmd.CommandTimeout = 0;
                        oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                        oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                        #region "Using DataReader"
                        using (SqlDataReader reader = oCmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                sTerminalId = reader["TerminalId"].ToString();
                                sTempReceive = reader["ISO"].ToString();
                                sTag = reader["Tag"].ToString();
                                string scont = reader["Content"].ToString();
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                else
                {
                    #region "930000/1"
                    //if (iCountInit < iMaxInit)
                    using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProcessMessage, oConn))
                    {
                        iCountInit++;
                        oCmd.CommandType = CommandType.StoredProcedure;
                        oCmd.CommandTimeout = 27;
                        //oCmd.CommandTimeout = 0;
                        oCmd.Parameters.Add("@sMessage", SqlDbType.VarChar).Value = sTemp;
                        oCmd.Parameters.Add("@iCheckInit", SqlDbType.VarChar).Value = bCheckAllowInit == true ? 1 : 0;

                        #region "Using DataReader"
                        using (SqlDataReader reader = oCmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                iErrorCode = int.Parse(reader["ErrorCode"].ToString());
                                sTerminalId = reader["TerminalId"].ToString();
                                sTempReceive = reader["ISO"].ToString();
                                sTag = reader["Tag"].ToString();
                                string scont = reader["Content"].ToString();
                            }
                        }
                        #endregion
                        iCountInit--;
                    }
                    #endregion
                }
                if (!string.IsNullOrEmpty(sTempReceive))
                {
                    // tambahkan total length
                    _iIsoLenResponse = sTempReceive.Length / 2;
                    sTempReceive = sISOLenSend(_iIsoLenResponse, cType) + sTempReceive;
                    Trace.Write(string.Format("|[RESPONSE]|{0}|sTempReceive : {1}", sTerminalId, sTempReceive));

                    if (iErrorCode == 0 || iErrorCode == 1)
                        CommonConsole.WriteToConsole(sTerminalId + " " + sProcessing(iErrorCode, sTag));
                }
                //Trace.Write(string.Format("|[RESPONSE] {0} {1}", sTerminalId, sProcessing(iErrorCode, sTag)));
            }
            catch (Exception ex)
            {
                //Console.WriteLine("sResponse failed : {0}\nMsg : {1}\n\n",
                //    ex.ToString(), sTemp);
                Logs.Write(string.Format("|[RESPONSE]|sResponse failed : {0}|@sMessage : {1}",
                    ex.ToString(), sTemp));
                //Console.ReadKey();
                //sTemp = null;
            }
            return sTempReceive;
        }

        protected int iISOLenReceive(string sTempLen, ConnType cType)
        {
            int iReturn = 0;
            if (cType == ConnType.TcpIP)
                iReturn = CommonLib.iHexStringToInt(sTempLen);
            else if (cType == ConnType.Serial)
                iReturn = int.Parse(sTempLen);
            return iReturn;
        }

        protected string sISOLenSend(int iLen, ConnType cType)
        {
            string sReturn = null;
            if (cType == ConnType.TcpIP || cType == ConnType.Modem)
                sReturn = CommonLib.sConvertDecToHex(iLen).PadLeft(4, '0');
            else if (cType == ConnType.Serial)
                sReturn = iLen.ToString("0000");
            return sReturn;
        }

        /// <summary>
        /// Get the current processing
        /// </summary>
        /// <param name="iCode">int : initialize code</param>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : processing message</returns>
        protected string sProcessing(int iCode, string sTag)
        {
            string sTemp = null;
            switch (iCode)
            {
                case 0:
                    sTemp = "Processing " + sDetailProcess(sTag);
                    break;
                case 1:
                    sTemp = "Initialize Complete";
                    break;
                case 2:
                    sTemp = "Invalid Proc. Code";
                    break;
                case 3:
                    sTemp = "Initialize Not Allow";
                    break;
                case 4:
                    sTemp = "Invalid Terminal Id";
                    break;
                case 5:
                    sTemp = "Invalid MTI";
                    break;
                default:
                    sTemp = "Unknown Error";
                    break;
            };
            return sTemp;
        }

        /// <summary>
        /// Get the detail processing message
        /// </summary>
        /// <param name="sTag">string : Tag code</param>
        /// <returns>string : detail processing message</returns>
        protected string sDetailProcess(string sTag)
        {
            string sTemp = null;
            switch (sTag)
            {
                case "DE":
                    sTemp = "Terminal";
                    break;
                case "DC":
                    sTemp = "Count";
                    break;
                case "AA":
                    sTemp = "Acquirer";
                    break;
                case "AE":
                    sTemp = "Issuer";
                    break;
                case "AC":
                    sTemp = "Card";
                    break;
                case "AD":
                    sTemp = "Relation";
                    break;
                case "GZ":
                    sTemp = "Compressed Init";
                    break;
                case "TL":
                    sTemp = "TLE";
                    break;
            }
            return sTemp;
        }

        protected List<ContentProfile> ltGetContentProfile(string _sTerminalId, ref string _sContent)
        {
            List<ContentProfile> ltCPTemp = new List<ContentProfile>();
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPProfileTextFullTable, oConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                DataTable dtTemp = new DataTable();
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
                if (dtTemp.Rows.Count > 0)
                {
                    foreach (DataRow rowTemp in dtTemp.Rows)
                    {
                        string sTag = rowTemp["Tag"].ToString();
                        string sValue = rowTemp["TagValue"].ToString();
                        string sContent = sTag != "AE37" && sTag != "AE38" && sTag != "TL11" ?
                            string.Format("{0}{1:00}{2}", sTag, sValue.Length, sValue) :
                            string.Format("{0}{1:000}{2}", sTag, sValue.Length, sValue);
                        _sContent += sContent;
                        ContentProfile oCPTemp = new ContentProfile(sTag.Substring(0, 2), sContent);
                        int iIndexSearch = -1;
                        if (ltCPTemp.Count == 0)
                            ltCPTemp.Add(oCPTemp);
                        else
                            if ((iIndexSearch = ltCPTemp.FindIndex(oCPSearch => oCPSearch.Tag == oCPTemp.Tag)) > -1)
                            {
                                ContentProfile oCPSearchResult = ltCPTemp[iIndexSearch];
                                ContentProfile oCPNew = new ContentProfile(oCPTemp.Tag, oCPSearchResult.Content + oCPTemp.Content);
                                ltCPTemp[iIndexSearch] = oCPNew;
                            }
                            else
                                ltCPTemp.Add(oCPTemp);
                    }
                }
            }
            return ltCPTemp;
        }

        protected bool IsNetCompressSuccess(string sFilename, string sTerminalId)
        {
            bool bReturn = false;
            if (File.Exists(sFilename))
            {
                try
                {
                    FileInfo fi = new FileInfo(sFilename);
                    using (FileStream inFile = fi.OpenRead())
                    {
                        string sGzipFile = sFilename + ".gz";
                        if (File.Exists(sGzipFile))
                            File.Delete(sGzipFile);
                        using (FileStream outFile = File.Create(sFilename + ".gz"))
                        {
                            using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                            {
                                byte[] buffer = new byte[fi.Length];
                                inFile.Read(buffer, 0, Convert.ToInt32(fi.Length));
                                compress.Write(buffer, 0, Convert.ToInt32(fi.Length));
                                bReturn = true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    //MessageBox.Show("NetCompress " + ex.Message);
                }
            }
            return bReturn;
        }

        protected bool IsCompressInit(string sTerminalId)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsCompressInit('{0}')", sTerminalId);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        protected bool IsAllowCompress(string sAppName)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsAllowCompress('{0}')", sAppName);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        protected int iGetMaxByte()
        {
            int iMaxLength = 400;
            string sQuery = string.Format("SELECT dbo.iPacketLength() [MaxByte]");
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                using (SqlDataReader oRead = oCmd.ExecuteReader())
                {
                    if (oRead.Read())
                        iMaxLength = int.Parse(oRead["MaxByte"].ToString());
                }
            }
            return iMaxLength;
        }

        protected DataTable dtGetInitTemp(string _sTerminalId, ref bool isCompress, string sAppName)
        {
            DataTable dtInitTemp = new DataTable();
            dtInitTemp.Columns.Add("TerminalId", typeof(string));
            dtInitTemp.Columns.Add("Content", typeof(string));
            dtInitTemp.Columns.Add("Tag", typeof(string));
            dtInitTemp.Columns.Add("Flag", typeof(bool));

            string sContent = null;
            List<ContentProfile> _ltCPTemp = ltGetContentProfile(_sTerminalId, ref sContent);
            if (_ltCPTemp != null && _ltCPTemp.Count > 0)
            {
                string sTag;
                int iOffset = 0;
                int iFlag = 0;
                if (IsAllowCompress(sAppName) && IsCompressInit(_sTerminalId))
                {
                    isCompress = true;
                    // GZ part
                    sTag = "GZ";
                    iOffset = 0;
                    string sContentCompressed = null;
                    // write to file locally and compress
                    if (!Directory.Exists(Environment.CurrentDirectory + @"\ZIP"))
                        Directory.CreateDirectory(Environment.CurrentDirectory + @"\ZIP");
                    string sFilename = Environment.CurrentDirectory + @"\ZIP\" + _sTerminalId;

                    if (File.Exists(sFilename))
                        File.Delete(sFilename);
                    CommonLib.Write2File(sFilename, sContent, false);

                    if (IsNetCompressSuccess(sFilename, _sTerminalId))
                    {
                        FileStream fsRead = new FileStream(sFilename + ".gz", FileMode.Open);
                        byte[] arrbContent = new byte[fsRead.Length];
                        fsRead.Read(arrbContent, 0, Convert.ToInt32(fsRead.Length));
                        fsRead.Close();
                        sContentCompressed = CommonLib.sByteArrayToHexString(arrbContent).Replace(" ", "");
                        while (!string.IsNullOrEmpty(sContentCompressed) && iOffset <= sContentCompressed.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            rows["Content"] = sContentCompressed.Substring(iOffset,
                                iOffset + (iMaxByte * 2) <= sContentCompressed.Length ?
                                (iMaxByte * 2) :
                                sContentCompressed.Length - iOffset);
                            dtInitTemp.Rows.Add(rows);
                            iOffset += (iMaxByte * 2);
                        }
                    }
                }
                else
                {
                    iFlag = 0;
                    iOffset = 0;
                    // non-GZ part
                    foreach (ContentProfile oCPTemp in _ltCPTemp)
                    {
                        sTag = oCPTemp.Tag;
                        sContent = oCPTemp.Content;
                        iOffset = 0;
                        //while (iOffset <= sContent.Length), error, row menjadi null ketika value nya kosong.
                        while (iOffset < sContent.Length)
                        {
                            DataRow rows = dtInitTemp.NewRow();
                            rows["TerminalId"] = _sTerminalId;
                            rows["Tag"] = sTag;
                            rows["Flag"] = iFlag == 1 ? true : false;
                            int iOffsetHeader = sContent.IndexOf(sTag + "01", iOffset + 1);
                            int iLength = 0;
                            if (iOffset + iMaxByte <= sContent.Length)
                                if (iOffsetHeader > 0)
                                    if (iOffset + iMaxByte <= iOffsetHeader)
                                        iLength = iMaxByte;
                                    else
                                        iLength = iOffsetHeader - iOffset;
                                else
                                    iLength = iMaxByte;
                            else
                                iLength = sContent.Length - iOffset;
                            rows["Content"] = sContent.Substring(iOffset, iLength);
                            iOffset += iLength;

                            dtInitTemp.Rows.Add(rows);
                        }
                    }
                }
            }
            return dtInitTemp;
        }

        protected bool IsApplicationAllowInit(string _sTerminalId, string _sAppName)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsApplicationAllowInit('{0}','{1}')", _sTerminalId, _sAppName);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oConn))
            {
                DataTable dtTemp = new DataTable();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }
    }
}
