using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.IO.Ports;
using System.Threading;
using InSysClass;

namespace InSysConsoleNewZip_License
{
    public class SerialModem : IDisposable
    {
        protected string sModemPort;

        protected const byte TPDU = 0x60;
        protected const string MTI = "0800";
        protected const string ProcCodeStart = "930000";
        protected const string ProcCodeContinue = "930001";

        protected SerialPort oSerialPort;

        protected bool IsStartReceive;

        protected string sISOMessage;
        protected string sISOMessageOld;

        protected byte[] arrbSendOld;
        
        public static SqlConnection oConn;
        public static bool bCheckAllowInit;

        /// <summary>
        /// SerialModem class constructor
        /// </summary>
        /// <param name="_sModemPort">string : Modem port name</param>
        public SerialModem(string _sModemPort)
        {
            sModemPort = _sModemPort;

            InitModem();
            if (oSerialPort.IsOpen)
            {
                InitConfigModem();
                oSerialPort.DataReceived += new SerialDataReceivedEventHandler(this.oSerialPort_DataReceived);
            }
        }

        public void Open()
        {
            oSerialPort.Open();
        }

        public void Close()
        {
            oSerialPort.Close();
        }

        /// <summary>
        /// Modem Serialport Deconstructor, close the port and dispose it.
        /// </summary>
        public void Dispose()
        {
            if (oSerialPort.IsOpen) oSerialPort.Close();
            oSerialPort.Dispose();
        }

        /// <summary>
        /// Init the serialport modem configuration
        /// </summary>
        protected void InitModem()
        {
            oSerialPort = new SerialPort(sModemPort);

            oSerialPort.PortName = sModemPort;
            //oSerialPort.BaudRate = 19200;
            oSerialPort.BaudRate = 9600;
            oSerialPort.StopBits = StopBits.One;
            oSerialPort.Parity = Parity.None;
            oSerialPort.DataBits = 8;
            oSerialPort.Handshake = Handshake.None;
            oSerialPort.ReadBufferSize = 1024;
            //oSerialPort.ReceivedBytesThreshold = 1024;

            oSerialPort.Open();

            Thread.Sleep(100);
            if (oSerialPort.IsOpen)
            {
                oSerialPort.DiscardInBuffer();
                oSerialPort.DiscardOutBuffer();

                oSerialPort.DtrEnable = true;
                oSerialPort.RtsEnable = true;
            }
        }

        /// <summary>
        /// Init the serialport modem AT command configuration.
        /// </summary>
        protected void InitConfigModem()
        {
            //oSerialPort.Write("ATE0B0&C0&D0&G0S0=1S10=50\r");
            //oSerialPort.Write("ATW1\r");
            oSerialPort.Write("AT&FS0=1L=1E0&K0");

            // US Robotics
            //oSerialPort.Write("AT&M0\r");

            //oSerialPort.Write("AT&W0");
        }

        /// <summary>
        /// Serialport modem data received event.
        /// </summary>
        protected void oSerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(500);
            if (oSerialPort.BytesToRead > 0)
            {
                int iBytesToRead = oSerialPort.BytesToRead;
                byte[] arrbRead = new byte[iBytesToRead];
                oSerialPort.Read(arrbRead, 0, iBytesToRead);

                //Trace.Write("[MODEM] Received Data");
                Trace.Write("|[MODEM]|Receive : " + CommonLib.sByteArrayToString(arrbRead));
                byte[] arrbSend = arrbGenResponse(arrbRead);

                if (arrbSend != null)
                {
                    oSerialPort.Write(arrbSend, 0, arrbSend.Length);
                    //Trace.Write("[MODEM] Sent Data");
                    Trace.Write("|[MODEM]|Send : " + CommonLib.sByteArrayToHexString(arrbSend));
                }
            }
        }

        protected byte[] arrbGenResponse(byte[] _arrbReceive)
        {
            int iCount;
            byte[] arrbResponse = null;
            for (iCount = 0; iCount < _arrbReceive.Length; iCount++)
            {
                if (_arrbReceive[iCount] == 0x0A && (iCount + 1) < _arrbReceive.Length)
                {
                    if (_arrbReceive[iCount + 1] == 0x60)
                    {
                        byte[] arrbRead = new byte[_arrbReceive.Length - iCount - 1];
                        for (int i = 0; i < arrbRead.Length && iCount < _arrbReceive.Length; i++)
                        {
                            arrbRead[i] = _arrbReceive[iCount + 1];
                            iCount++;
                        }

                        arrbResponse = (new Response()).arrbResponse(arrbRead, ConnType.Modem, oConn, bCheckAllowInit);
                        break;
                    }
                }
                else if (_arrbReceive[iCount] == 0x60 && _arrbReceive.Length > 8)
                {
                    if (_arrbReceive[iCount + 5] == 0x8 && _arrbReceive[iCount + 6] == 0x0)
                    {
                        arrbResponse = (new Response()).arrbResponse(_arrbReceive, ConnType.Modem, oConn, bCheckAllowInit);
                        break;
                    }
                }
            }
            //if (arrbResponse != null)
            //    Trace.Write("[MODEM] Response : \n" + CommonLib.sByteArrayToHexString(arrbResponse));
            return arrbResponse;
        }
    }
}
