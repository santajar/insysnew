﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Ini;
using InSysClass;
using System.Configuration;

namespace ConsoleUpdateQuery
{
    class Program
    {
        static void Main(string[] args)
        {
            //try
            //{
            //    Console.WriteLine("Preparing the database connection...");
            //    SqlConnection oSqlConn = new SqlConnection((new InitData()).sGetConnString());
            //    oSqlConn.Open();

            //    //string sActiveDir = Directory.GetCurrentDirectory();
            //    //string sFileName = sActiveDir + "\\Script.txt";

            //    //if (File.Exists(sFileName))
            //    //{
            //    //using (SqlCommand oCmd = new SqlCommand(sGetQueryFileContent(sFileName), oSqlConn))
            //    //oCmd.ExecuteNonQuery();
            //    ////File.Delete(sFileName);
            //    //}
            //    //else
            //    //{
            //    //    Console.WriteLine("file not exist");
            //    //}

            //    //UpdateMinTopUp(oSqlConn);

            //    //UpdateMaskingMasterKey(oSqlConn);

            //    UpdateNII(oSqlConn);

            //    Console.WriteLine("Update success");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //Console.Read();

            ExtractTidMid();
        }

        private static void ExtractTidMid()
        {
            DateTime dateCurrent = DateTime.Now;
            try
            {
                string sConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection oSqlConn = new SqlConnection(sConnString);
                oSqlConn.Open();

                //string sQueryFile = string.Format(@"{0}\{1}",Environment.CurrentDirectory, ConfigurationManager.AppSettings["Query File"]);
                string sQueryFile = ConfigurationManager.AppSettings["Query File"].ToString();
                string sQuery = (new StreamReader(sQueryFile)).ReadToEnd();
                if (!sQuery.Contains("UPDATE") && !sQuery.Contains("INSERT") && !sQuery.Contains("DELETE"))
                {
                    SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn);
                    oCmd.CommandTimeout = 0;
                    DataTable dtTemp = new DataTable();
                    (new SqlDataAdapter(oCmd)).Fill(dtTemp);

                    if (dtTemp != null && dtTemp.Rows.Count > 0)
                    {
                        string sOutputFile = ConfigurationManager.AppSettings["Output File"].ToString();
                        StreamWriter sw = new StreamWriter(sOutputFile, false);

                        foreach (DataRow row in dtTemp.Rows)
                            //sw.WriteLine(row[0].ToString());
                            if (!string.IsNullOrEmpty(row[0].ToString()))
                                sw.Write("{0}\n", row[0]);

                        sw.Close();
                        Console.WriteLine("SUCCESS");
                    }
                    else
                    {
                        Console.WriteLine("INFO : No Record to write");
                    }
                }
                else
                {
                    Console.WriteLine("FAILED : UPDATE, INSERT, DELETE are NOT ALLOWED");
                }
            }
            catch (SqlException sqlex)
            {
                Console.WriteLine("INFO : FAILED SQL {0}", sqlex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("INFO : FAILED {0}", ex.Message);
            }
        }

        private static void UpdateNII(SqlConnection _oSqlConn)
        {
            Console.WriteLine("Start Update NII");
            string sQuery = string.Format("UPDATE tbProfileAcquirer SET AcquirerTagValue='016' WHERE AcquirerName='CREDIT_P' AND AcquirerTagValue='015'");
            sQuery = string.Format("{0}/nGO", sQuery);
            sQuery = string.Format("{0}/nUPDATE tbProfileAcquirer SET AcquirerTagValue='002' WHERE AcquirerName='DEBIT_P' AND AcquirerTagValue='006'", sQuery);
            sQuery = string.Format("{0}/nGO", sQuery);

            SqlTransaction sqlTran = _oSqlConn.BeginTransaction();            
            try
            {
                using (SqlCommand oCmd = new SqlCommand(sQuery, _oSqlConn, sqlTran))
                {
                    if (_oSqlConn.State != ConnectionState.Open) _oSqlConn.Open();
                    int iRow = oCmd.ExecuteNonQuery();
                    Console.WriteLine("SUCCESS : {0} row Updated", iRow);
                }
            }
            catch (Exception ex)
            {
                sqlTran.Rollback();
                Console.WriteLine("ERROR : {0}", ex.Message);
            }

            Console.WriteLine("Stop Update NII");
        }

        //static void Main(string[] args)
        //{
        //    string sTerminalInit = "00000000";
        //    string sF99 = "99";
        //    string sF2 = "0000";

        //    while (true)
        //    {
        //        Console.Write("TerminalInit : ");
        //        sTerminalInit = Console.ReadLine();

        //        string sINIT_PASSWORD = sTerminalInit.Substring(5, 3) + chekdigit(sTerminalInit.Substring(5, 3)).ToString();
        //        sF2 = chekdigit(sTerminalInit.Substring(5, 3)).ToString() + sTerminalInit.Substring(7, 1) + sTerminalInit.Substring(6, 1) + sTerminalInit.Substring(5, 1);

        //        string _tmp = (chekdigit(sTerminalInit.Substring(5, 3)) + 2).ToString();
        //        string tmp = _tmp.Substring(_tmp.Length - 1, 1);
        //        sF99 = "99" + sTerminalInit.Substring(5, 3) + tmp;

        //        Console.Write("F51 : {0}\nF2  : {1}\nF99 : {2}\n\n", sINIT_PASSWORD, sF2, sF99);
        //        Console.Write("Exit ? ");
        //        string sKey = Console.ReadLine();
        //        if (sKey.ToLower() == "y") break;
        //    }
        //}

        public static int chekdigit(string OrigVal)
        {
            int i_digit = 0;
            int calc_digit = 0;

            if (OrigVal.Length % 2 == 1)
            {
                OrigVal = OrigVal + "0";
            }

            calc_digit = 0;
            int j = 1;
            int msg_len = OrigVal.Length - 1;
            //int i_digit = 0;
            for (int i = msg_len; i >= 0; i += -1)
            {
                string digit = OrigVal.Substring(i, 1);
                foreach (char c in digit)
                {
                    i_digit = (int)c;
                }
                // int i_digit = (int)digit;
                if (i_digit >= (int)'0' & i_digit <= (int)'9')
                {
                    i_digit = i_digit - 48;
                }
                else
                {
                    if (i_digit >= (int)'A' & i_digit <= (int)'Z')
                    {
                        i_digit = (i_digit - 58) % 10;
                    }
                    else
                    {
                        if (i_digit >= (int)'a' & i_digit <= (int)'z')
                        {
                            i_digit = (i_digit - 58 - 32) % 10;
                        }
                        else
                        {
                            i_digit = (i_digit - 32) % 10;
                        }
                    }
                }
                if (j % 2 == 1)
                {
                    i_digit = i_digit * 2;
                }
                if (i_digit >= 10)
                {
                    i_digit = ((i_digit - (i_digit % 10)) / 10) + (i_digit % 10);
                }
                calc_digit = calc_digit + i_digit;
                j = j + 1;
            }

            int temp_mod = calc_digit % 10;
            int temp_div = (calc_digit - (calc_digit % 10)) / 10;
            if (temp_mod != 0)
            {
                temp_div = temp_div + 1;
            }
            return (temp_div * 10) - calc_digit;
        }

        private static void UpdateMaskingMasterKey(SqlConnection _oSqlConn)
        {
            // update for bluebird Mandiri
            string sQueryUpdate = string.Format("update tbItemList set vMasking=1 where Tag in (select Tag from tbItemList where ItemName like 'Master Key 1' or ItemName like 'Master Key 2')");
            using (SqlCommand oCmd = new SqlCommand(sQueryUpdate, _oSqlConn))
                oCmd.ExecuteNonQuery();
        }

        private static void UpdateMinTopUp(SqlConnection _oSqlConn)
        {
            DataTable dtTerminalList = dtGetTerminalList(_oSqlConn);
            if(dtTerminalList!=null && dtTerminalList.Rows.Count>0)
                foreach (DataRow row in dtTerminalList.Rows)
                {
                    string sMsg = null;
                    try
                    {
                        string sQueryUpdate = string.Format("update tbProfileAcquirer set AcquirerTagValue='{0:00000000}' where TerminalID='{1}' and AcquirerName='{2}' and AcquirerTag='aa19'",
                            20000, row["TerminalID"], row["AcquirerName"]);
                        using (SqlCommand oCmd = new SqlCommand(sQueryUpdate, _oSqlConn))
                            oCmd.ExecuteNonQuery();
                        sMsg = string.Format("Updating '{0}', '{1}' DONE", row["AcquirerName"], row["TerminalID"]);
                        UpdateLastUpdateValue(_oSqlConn, row["TerminalID"].ToString());
                    }
                    catch (Exception ex)
                    {
                        sMsg = string.Format("Updating '{0}', '{1}' FAILED", row["AcquirerName"], row["TerminalID"]);
                        WriteLog(sMsg + " " + ex.Message);
                    }
                    ShowLog(sMsg);
                }
        }

        private static void ShowLog(string sMsg)
        {
            sMsg = string.Format("{0:dd MMM yyyy, hh:mm:ss.fff tt}| {1}", DateTime.Now, sMsg);
            Console.WriteLine(sMsg);
            WriteLog(sMsg);
        }

        private static void UpdateLastUpdateValue(SqlConnection _oSqlConn, string sTerminalID)
        {
            using (SqlCommand oCmd = new SqlCommand("spProfileTerminalUpdateLastUpdate", _oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oCmd.ExecuteNonQuery();
            }
        }

        private static void WriteLog(string sMsg)
        {
            string sDir = Environment.CurrentDirectory + @"\LOGS";
            if (!Directory.Exists(sDir))
                Directory.CreateDirectory(sDir);
            using (StreamWriter sw = new StreamWriter(string.Format(@"{0}\{1:yyyyMMdd}.log", sDir, DateTime.Now), true))
                sw.WriteLine(sMsg);
        }

        private static DataTable dtGetTerminalList(SqlConnection _oSqlConn)
        {
            int iTotalTerminal = iGetTotalTerminal();
            DataTable dtTemp = new DataTable();
            if (iTotalTerminal > 0)
            {
                string sQuery = string.Format("select distinct TerminalID,AcquirerName " +
                    "from tbProfileAcquirer with(nolock)" +
                    "where AcquirerTag='aa19' and AcquirerTagValue='00050000'");
                if (_oSqlConn.State != ConnectionState.Open) _oSqlConn.Open();
                using (SqlCommand oCmd = new SqlCommand(sQuery, _oSqlConn))
                    (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            }
            return dtTemp;
        }

        private static int iGetTotalTerminal()
        {
            int iValue = 0;
            IniFile oAppConfig = new IniFile(Environment.CurrentDirectory + @"\app.config");
            iValue = int.Parse(oAppConfig.IniReadValue("TERMINALID", "TOTAL"));
            return iValue;
        }

        static string sGetQueryFileContent(string sFileName)
        {
                StreamReader sr = new StreamReader(sFileName);
                string sQuery = sr.ReadToEnd();
                sr.Close();
                return sQuery;
        }

        static string sUpdate_spProfileIssuerUpdate()
        {
            string sValue = string.Format("ALTER PROCEDURE [dbo].[spProfileIssuerUpdate]\n@sTerminalID VARCHAR(8),\n@sContent VARCHAR(MAX)\nAS\nDECLARE @sTID VARCHAR(8)\n	DECLARE @sName VARCHAR(50)\n	DECLARE @sTag VARCHAR(4)\n	DECLARE @iLengthOfTagLength INT\n	DECLARE @iTagLength INT\n	DECLARE @sValue VARCHAR(150)\n\n	IF (OBJECT_ID('TEMPDB..#TempIss') IS NOT NULL)\n		DROP TABLE #TempIss\n\n	SELECT TerminalID,\n			[Name],\n			Tag,\n			LEN(TagLength) [LengthOfTagLength],\n			TagLength,\n			TagValue\n	INTO #TempIss\n	FROM fn_tbprofiletlv(@sTerminalID, @sContent)\n\n	DECLARE @sDbID VARCHAR(5),\n			@sIssName VARCHAR(50)\n\n	SET @sDbID = dbo.iDatabaseIdByTerminalId(@sTerminalID)\n	SELECT DISTINCT @sIssName = [Name] FROM #TempIss\n\n	EXEC spProfileGenerateLog @sDbID, @sTerminalID, 'AE', @sIssName, @sContent, 1\n\n\n	INSERT INTO [tbProfileIssuer]\n	   ([TerminalID]\n	   ,[IssuerName]\n	   ,[IssuerTag]\n	   ,[IssuerLengthOfTagLength]\n	   ,[IssuerTagLength]\n	   ,[IssuerTagValue])\n	SELECT TerminalID, \n			[Name],\n			Tag,\n			LengthOfTagLength,\n			TagLength,\n			TagValue\n	FROM #TempIss\n	WHERE Tag NOT IN (\n						SELECT IssuerTag\n						FROM tbProfileIssuer\n						WHERE TerminalID = @sTerminalID AND \n							  IssuerName = [Name]\n					 )\n\n	UPDATE  Issuer\n		SET Issuer.IssuerLengthOfTagLength = Temp.LengthOfTagLength,\n			Issuer.IssuerTagLength = Temp.TagLength,\n			Issuer.IssuerTagValue =  Temp.TagValue\n	FROM tbProfileIssuer Issuer, #TempIss Temp\n	WHERE Issuer.TerminalID = @sTerminalID AND \n		  Issuer.IssuerName = Temp.[Name] AND \n		  Issuer.IssuerTag = Temp.Tag\n\nEXEC spProfileTerminalListUpdateAllowDownload NULL, @sTerminalID\nEXEC spProfileTerminalUpdateLastUpdate @sTerminalID\n");            
            return sValue;
        }

        static void InsertNewTagBlueBird206(SqlConnection _oSqlConn)
        {
            string sQuery = "select DatabaseID from tbProfileTerminalDB where DatabaseName='BLUEBIRD_206'";
        }

        static string sNewParameterBlueBird206()
        {
            return "insert into tbItemList(DatabaseID,FormID,ItemSequence,ItemName,ObjectID,DefaultValue,LengthofTagValueLength,vAllowNull,vUpperCase,vType,vMinLength,vMaxLength,vMinValue,vMaxValue,ValidationMsg,Tag)\nselect 'DBID' DatabaseID,'2' FormID,'36' ItemSequence,'MID Segment 1'ItemName,'3'ObjectID,'0'DefaultValue,'2'LengthofTagValueLength,'0'vAllowNull,'0'vUpperCase,'N'vType,'1'vMinLength,'1'vMaxLength,'0'vMinValue,'1'vMaxValue,'MID Segment 1'ValidationMsg,'AA34'Tag\nunion select\n'DBID','2','37','MID Segment 2','3','0','2','0','0','N','1','1','0','1','MID Segment 2','AA35'\nunion select\n'DBID','2','38','MID Segment 3','3','0','2','0','0','N','1','1','0','1','MID Segment 3','AA36'\nunion select\n'DBID','2','17','Print Out TID','4','1','2','0','1','S','0','1','0','0','No Information','AA37'\nunion select\n'DBID','2','32','TC Advice','3','0','2','0','0','N','1','1','0','1','TC Advice','AA38'\n";
        }
    }

    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;

        public InitData()
        {
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception ex)
                {
                    //CommonClass.doWriteErrorFile(ex.Message);
                };
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return sConnString(sDataSource, sDatabase, sUserID, sPassword);
        }

        public void doWriteInitData(string sDataSource, string sDatabase, string sUserID, string sPassword)
        {
            string sActiveDir = Directory.GetCurrentDirectory();
            string sFileName = sActiveDir + "\\Application.config";
            string sEncryptFileName = sActiveDir + "\\Application.conf";

            try
            {
                IniFile objIniFile = new IniFile(sFileName);
                objIniFile.IniWriteValue("Database", "DataSource", sDataSource);
                objIniFile.IniWriteValue("Database", "Database", sDatabase);
                objIniFile.IniWriteValue("Database", "UserID", sUserID);
                objIniFile.IniWriteValue("Database", "Password", sPassword);
            }
            catch (Exception ex)
            {
                //CommonClass.doWriteErrorFile(ex.Message);
            };
            EncryptionLib.EncryptFile(sEncryptFileName, sFileName);
            File.Delete(sFileName);
        }

        static string sConnString(string sHost, string sDatabaseName, string sUserID, string sPassword)
        {
            return "server=" + sHost + ";uid=" + sUserID + ";pwd=" + sPassword + ";database=" + sDatabaseName + "; Connection Lifetime=0;";
        }
    }
}
