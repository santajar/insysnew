﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using ipXML;

namespace ConsoleServiceTester
{
    class Program
    {
        static int iTotalColumn = 294;
        static void Main(string[] args)
        {
            try
            {
                string sFilename = ConfigurationManager.AppSettings.Get("Filename");
                iTotalColumn = int.Parse(ConfigurationManager.AppSettings.Get("Total Param EMS"));
                if (!string.IsNullOrEmpty(sFilename))
                {
                    FileInfo fiTemp = new FileInfo(sFilename);
                    List<DataTable> dtXmlTemp = new List<DataTable>();
                    ReadEMSFile(fiTemp.FullName, ref dtXmlTemp);

                    WebServiceTms.ServiceSoap serviceTms = new WebServiceTms.ServiceSoapClient();
                    WebServiceTms.sStartXMLWebServiceRequest request = new WebServiceTms.sStartXMLWebServiceRequest();
                    request.Body = new WebServiceTms.sStartXMLWebServiceRequestBody();

                    WebServiceTms.sStartXMLWebServiceResponse response = new WebServiceTms.sStartXMLWebServiceResponse();
                    response.Body = new WebServiceTms.sStartXMLWebServiceResponseBody();

                    WebServiceTms.AttributeClass parameter = new WebServiceTms.AttributeClass();

                    EMS_XML localparam = new EMS_XML(dtXmlTemp);
                    WebServiceTms.EMS_XML param = new WebServiceTms.EMS_XML();

                    WebServiceTms.AttributeClass attrib = new WebServiceTms.AttributeClass();

                    param.Attribute = new WebServiceTms.AttributeClass();
                    param.Attribute.sSenderID = localparam.Attribute.sSenderID;
                    param.Attribute.sSenders = localparam.Attribute.sSenders;
                    param.Attribute.sTypes = localparam.Attribute.sTypes;
                    param.Attribute.sVersions = localparam.Attribute.sVersions;

                    param.Data = new WebServiceTms.DataClass();
                    
                    WebServiceTms.ColumnValue[] emsparam = new WebServiceTms.ColumnValue[iTotalColumn];
                    EMS_XML.DataClass.TerminalClass.ColumnValue[] localcolumnvalue = new EMS_XML.DataClass.TerminalClass.ColumnValue[iTotalColumn];
                    localcolumnvalue = localparam.Data.ltTerminal[0].ColumnValueXML.ToArray();
                    for (int i = 0; i < iTotalColumn; i++)
                    {
                        WebServiceTms.ColumnValue colvalTemp = new WebServiceTms.ColumnValue();
                        colvalTemp.sColName = localcolumnvalue[i].sColName;
                        colvalTemp.sColValue = localcolumnvalue[i].sColValue;
                        emsparam[i] = colvalTemp;
                        //Console.WriteLine("i : {0}", i);
                    }

                    WebServiceTms.TerminalClass terminal = new WebServiceTms.TerminalClass();
                    terminal.ColumnValueXML = emsparam;

                    WebServiceTms.TerminalClass[] terminalArray = new WebServiceTms.TerminalClass[1];
                    terminalArray[0] = terminal;
                    param.Data.ltTerminal = terminalArray;

                    request.Body._oXML = param;

                    response.Body = serviceTms.sStartXMLWebService(request).Body;
                    WebServiceTms.ResultClass result = response.Body.sStartXMLWebServiceResult.Data.Terminal.Result;
                    Console.WriteLine("{0} : {1}. DONE", result.sResultCode, result.sResultDesc);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.Read();
        }

        static void ReadEMSFile(string sPath, ref List<DataTable> dtXML)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(sPath);
            foreach (DataTable dtTemp in ds.Tables)
            {
                dtXML.Add(dtTemp);
            }
            ds.Dispose();
        }

    }
}
