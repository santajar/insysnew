﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace EMS
{
    public partial class FrmEmsXmlViewer : Form
    {
        public FrmEmsXmlViewer()
        {
            InitializeComponent();
        }

        string sFilename = null;
        string sFoldername = null;
        DataTable dtTerminal;

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            //OpenFileDialog ofdXmlFile = new OpenFileDialog();
            //ofdXmlFile.Filter = "Xml File|*.xml";
            //ofdXmlFile.ShowDialog();
            //if (!string.IsNullOrEmpty(ofdXmlFile.FileName))
            //    sFilename = ofdXmlFile.FileName;
            FolderBrowserDialog fbdEmsXmlFolder = new FolderBrowserDialog();
            fbdEmsXmlFolder.ShowDialog();
            if (!string.IsNullOrEmpty(fbdEmsXmlFolder.SelectedPath))
                sFoldername = fbdEmsXmlFolder.SelectedPath;
            dgvEmsTerminal.DataSource = null;
            dtTerminal = null;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sFoldername))
            {
                DirectoryInfo diXmlEmsFolder = new DirectoryInfo(sFoldername);
                FileInfo[] arrfiXmlEms = diXmlEmsFolder.GetFiles("*.xml");
                if (arrfiXmlEms.Count() >= 1)
                {
                    foreach (FileInfo fiXmlEms in arrfiXmlEms)
                    {
                        DataSet datasetXmlEms = new DataSet();
                        datasetXmlEms.ReadXml(fiXmlEms.FullName);
                        if (dtTerminal == null)
                            dtTerminal = datasetXmlEms.Tables["Terminal"];
                        else
                        {
                            DataRow[] arrRowXmlEms = new DataRow[datasetXmlEms.Tables["Terminal"].Rows.Count];
                            datasetXmlEms.Tables["Terminal"].Rows.CopyTo(arrRowXmlEms, 0);
                            foreach (DataRow rowTerminal in arrRowXmlEms)
                            {
                                dtTerminal.Rows.Add(rowTerminal.ItemArray);
                                dtTerminal.AcceptChanges();
                            }
                        }
                    }
                    dgvEmsTerminal.DataSource = dtTerminal;
                }
            }
        }
    }
}
