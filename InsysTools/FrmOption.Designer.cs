﻿namespace AppUpdateConnection
{
    partial class FrmOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnUpdateParam = new System.Windows.Forms.Button();
            this.btnUpdateConn = new System.Windows.Forms.Button();
            this.btnMoveData = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnUpdateParam);
            this.groupBox1.Controls.Add(this.btnUpdateConn);
            this.groupBox1.Controls.Add(this.btnMoveData);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 237);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnUpdateParam
            // 
            this.btnUpdateParam.Location = new System.Drawing.Point(6, 48);
            this.btnUpdateParam.Name = "btnUpdateParam";
            this.btnUpdateParam.Size = new System.Drawing.Size(120, 23);
            this.btnUpdateParam.TabIndex = 2;
            this.btnUpdateParam.Text = "Update Parameter";
            this.btnUpdateParam.UseVisualStyleBackColor = true;
            this.btnUpdateParam.Click += new System.EventHandler(this.btnUpdateParam_Click);
            // 
            // btnUpdateConn
            // 
            this.btnUpdateConn.Location = new System.Drawing.Point(134, 19);
            this.btnUpdateConn.Name = "btnUpdateConn";
            this.btnUpdateConn.Size = new System.Drawing.Size(120, 23);
            this.btnUpdateConn.TabIndex = 1;
            this.btnUpdateConn.Text = "Update Connection";
            this.btnUpdateConn.UseVisualStyleBackColor = true;
            this.btnUpdateConn.Click += new System.EventHandler(this.btnUpdateConn_Click);
            // 
            // btnMoveData
            // 
            this.btnMoveData.Location = new System.Drawing.Point(6, 19);
            this.btnMoveData.Name = "btnMoveData";
            this.btnMoveData.Size = new System.Drawing.Size(120, 23);
            this.btnMoveData.TabIndex = 0;
            this.btnMoveData.Text = "Move Data";
            this.btnMoveData.UseVisualStyleBackColor = true;
            this.btnMoveData.Click += new System.EventHandler(this.btnMoveData_Click);
            // 
            // FrmOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOption";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tools Option";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmOption_FormClosed);
            this.Load += new System.EventHandler(this.FrmOption_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnUpdateConn;
        private System.Windows.Forms.Button btnMoveData;
        private System.Windows.Forms.Button btnUpdateParam;
    }
}