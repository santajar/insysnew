﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InsysTools
{
    public partial class FrmMasking : Form
    {
        protected SqlConnection oSqlConn;
        static string sTerminalID,sDatabaseName;
        protected int iDatabaseID;
        private string sUserID;
        protected static DataTable dtTableCard, dtTableCardTerminal = null;

        public FrmMasking(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sUserID)
        {
            InitializeComponent();

            oSqlConn = _oSqlConn;
            sUserID = _sUserID;
        }

        protected void InitAcquirer()
        {
            DataTable dtAcquirer = null;
            dtAcquirer = dtLoadAcquirer();

            if (dtAcquirer.Rows.Count > 0)
            {
                cmbAcquirer.DataSource = dtAcquirer;
                cmbAcquirer.DisplayMember = "Acquirer";
                cmbAcquirer.ValueMember = "Acquirer";
                
            }
           
        }

        protected void InitIssuer()
        {
           
            if (cmbAcquirer.DataSource != null)
            {
                DataTable dtIssuer = null;
                dtIssuer = dtLoadIssuer();
                if (dtIssuer.Rows.Count > 0)
                {
                    cmbIssuer.DataSource = dtIssuer;
                    cmbIssuer.DisplayMember = "Issuer";
                    cmbIssuer.ValueMember = "Issuer";
                }
                else
                {
                    cmbIssuer.DataSource = null;
                    cmbIssuer.Items.Clear();
                    cmbIssuer.ResetText();
                    listCard.Items.Clear();
                    listCardNew.Items.Clear();
                }
                //else MessageBox.Show("Issuer Empty");
            }
        }
        protected void InitCard()
        {
           
            listCard.Items.Clear();
            GetdtCard();
            foreach (DataRow row in dtTableCard.Rows)
                listCard.Items.Add(row["Card"].ToString());

            lCardIssuer.Text=(listCard.Items.Count).ToString();
            lIssuer.Text= cmbIssuer.SelectedValue.ToString();

        }
        protected void InitCardNew()
        {
            listCardNew.Items.Clear();
            foreach (DataRow row in dtTableCard.Rows)
                listCardNew.Items.Add(row["Card"].ToString());

            if (listCardNew.Items.Count != 0) lCardNew.Text = (listCardNew.Items.Count).ToString();
            else
                lCardNew.Text = "0";
            lIssuer.Text = cmbIssuer.SelectedValue.ToString();
            lSelisih.Text = "(" + (listCard.Items.Count - listCardNew.Items.Count).ToString() + ")";

        }

        protected DataTable dtLoadAcquirer()
        {
            DataTable dtTable = new DataTable();
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileRelationBrowseAcquirerByTerminal, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                        oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }
            return dtTable;
        }

        protected DataTable dtLoadIssuer()
        {
            DataTable dtTable = new DataTable();
            try
            {
                if (cmbAcquirer.Text.Length != 0)
                {
                    string sAcquirer = cmbAcquirer.SelectedValue.ToString();
                    using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileRelationBrowseIssuerByAcquirer, oSqlConn))
                    {
                        oSqlCmd.CommandType = CommandType.StoredProcedure;
                        oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                        oSqlCmd.Parameters.Add("@sAcquirer", SqlDbType.VarChar).Value = sAcquirer;
                        using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                            oAdapt.Fill(dtTable);
                    }
                }
                else
               
                cmbAcquirer.Text = "";
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTable = null;
            }
            return dtTable;
        }


        protected void GetdtCard()
        {
            dtTableCard = new DataTable();
            try
            {
                if (cmbIssuer.Text.Length != 0)
                {
                    string sIssuer = cmbIssuer.SelectedValue.ToString();
                    using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileRelationBrowseCardByIssuer, oSqlConn))
                    {
                        oSqlCmd.CommandType = CommandType.StoredProcedure;
                        oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                        oSqlCmd.Parameters.Add("@sIssuer", SqlDbType.VarChar).Value = sIssuer;
                        using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                            oAdapt.Fill(dtTableCard);
                    }
                }
                else
                    cmbAcquirer.Text = "";
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTableCard = null;
            }
        }

        protected void GetdtCardByTerminal()
        {
            dtTableCardTerminal = new DataTable();
            try
            {
                if (cmbIssuer.Text.Length != 0)
                {
                    string sIssuer = cmbIssuer.SelectedValue.ToString();
                    using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileRelationBrowseCardByTerminal, oSqlConn))
                    {
                        oSqlCmd.CommandType = CommandType.StoredProcedure;
                        oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                        using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                            oAdapt.Fill(dtTableCardTerminal);
                    }
                }
                else
                    cmbAcquirer.Text = "";
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                dtTableCardTerminal = null;
            }
        }

        protected void LoaddtCard()
        {
            try
            {
                DataTable dtChoosingList = new DataTable();
                cmbCard.DataSource = null;
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPCardListBrowse, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = iDatabaseID;

                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }

                (new SqlDataAdapter(oSqlCmd)).Fill(dtChoosingList);
                if (dtChoosingList != null)                    
                {
                    cmbCard.DataSource = dtChoosingList;
                    cmbCard.DisplayMember = "Name";
                    cmbCard.ValueMember = "Name";
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }
        protected string sGetDatabaseNameByTerminalID(string sTerminalID)
        {
            string sQuery = string.Format("SELECT A.DatabaseName FROM tbProfileTerminalDB A WITH(NOLOCK) JOIN tbProfileTerminalList B WITH(NOLOCK) ON  A.DatabaseID=B.DatabaseID WHERE B.TerminalID='{0}'", sTerminalID);
            string sReturn = "";

            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                oCmd.CommandTimeout = 0;
                if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
                using (DataTable dtTemp = new DataTable())
                {
                    new SqlDataAdapter(oCmd).Fill(dtTemp);
                    sReturn = dtTemp.Rows[0][0].ToString();
                }
            }

            return sReturn;
        }
        protected void LoadView()
        {
            try
            {
                if ((txtTerminal.Text).Length == 8)
                {
                    sTerminalID = txtTerminal.Text;
                    lTerminalID.Text = txtTerminal.Text;
                    iDatabaseID = iGetDatabaseId(sTerminalID);
                    sDatabaseName = sGetDatabaseNameByTerminalID(sTerminalID);
                    if (iDatabaseID != 0)
                    {
                        InitAcquirer();
                        if (cmbAcquirer.ValueMember != null) InitIssuer();
                        if (cmbIssuer.ValueMember != null)
                        { InitCard(); InitCardNew(); }
                        LoaddtCard();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void txtTerminal_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == Convert.ToChar(Keys.Return))
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                LoadView();
            }
        }
        protected int iGetDatabaseId(string sTerminalID)
        {
            int iValue = 0;
            using (SqlCommand oCmd = new SqlCommand(
                string.Format("SELECT DatabaseID FROM tbProfileTerminalList Where TerminalID ='{0}'", sTerminalID), oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlDataReader read = oCmd.ExecuteReader())
                    if (read.HasRows)
                        if (read.Read())
                            iValue = int.Parse(read[0].ToString());
            }
            return iValue;
        }

        private void cmbAcquirer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((txtTerminal.Text).Length == 8)
            {
                sTerminalID = txtTerminal.Text;
                InitIssuer();
            }
        }

        private void cmbAcquirer_MouseClick(object sender, MouseEventArgs e)
        {
            //if ((txtTerminal.Text).Length == 8)
            //{
            //    sTerminalID = txtTerminal.Text;
            //    InitIssuer();
            //}
        }
        

        private void cmbIssuer_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GetdtCard();
            if (cmbAcquirer.SelectedValue.ToString().Contains("PROMO"))
            {
                cmbIssuer.ResetText();
                listCard.Items.Clear();
                listCardNew.Items.Clear();
                lIssuer.Text = "";


            }
            else
            {
                InitCard();
                InitCardNew();
            }
           
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string sValueSelected = cmbCard.SelectedValue.ToString();
                var confirmDialog = MessageBox.Show("Are you sure to Add card " + sValueSelected + " ? ", "Confirm !", MessageBoxButtons.YesNo);
                if (confirmDialog == DialogResult.Yes)
                {
                    GetdtCardByTerminal();
                    bool bCardTerminal = false;
                    if (dtTableCardTerminal != null)
                        if (dtTableCardTerminal.Rows.Count >= 0)
                            foreach (DataRow drow in dtTableCardTerminal.Rows)
                            {
                                string sCardTerminal = drow["Card"].ToString();
                                if (sCardTerminal == sValueSelected) bCardTerminal = true;
                            }
                    if (bCardTerminal == false)
                    {
                        GetdtCard();
                        bool bRow = false;
                        if (dtTableCard.Rows.Count >= 0)
                            foreach (DataRow drow in dtTableCard.Rows)
                            {
                                string sValue = drow["Card"].ToString();
                                if (sValue == sValueSelected) bRow = true;
                            }
                        if (bRow == false)
                        {
                            listCardNew.ClearSelected();

                            int index = listCardNew.FindString(cmbCard.SelectedValue.ToString());

                            if (index < 0)
                            {
                                listCardNew.Items.Add(sValueSelected);
                                if (listCardNew.Items.Count != 0)
                                {
                                    lCardNew.Text = (listCardNew.Items.Count).ToString();
                                    lSelisih.Text = "(" + (listCard.Items.Count - listCardNew.Items.Count).ToString() + ")";
                                }
                                else
                                    lCardNew.Text = "0";
                            }
                            else
                            {
                                listCardNew.SelectedIndex = index;
                                MessageBox.Show("Item is has been ");
                                //listCardNew.ClearSelected();
                            }
                        }
                        else MessageBox.Show(sValueSelected + " is Existing in issuer : " + cmbIssuer.SelectedValue.ToString());
                    }
                    else MessageBox.Show(sValueSelected + " is Existing out issuer, please delete card from other issuer");
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int jmlCard = listCardNew.Items.Count;
                int icount = 0;
                foreach (var item in listCardNew.CheckedItems.OfType<string>().ToList())
                {
                    icount++;
                }

                if(jmlCard == icount)
                {
                    MessageBox.Show("Cannot Delete All Card", "Information", MessageBoxButtons.OK);
                }else
                {
                    string sValueSelected = null;
                    sValueSelected = listCardNew.SelectedItem.ToString();
                    int index = listCardNew.FindString(sValueSelected);
                    GetdtCard();

                    CultureInfo ciUSFormat = new CultureInfo("en-US", true);

                    foreach (var item in listCardNew.CheckedItems.OfType<string>().ToList())
                    {
                        var confirmDialog = MessageBox.Show("Are you sure to delete this " + item + " ? ", "Confirm Delete!", MessageBoxButtons.YesNo);
                        if (confirmDialog == DialogResult.Yes)
                        {
                            bool bRow = false;
                            if (dtTableCard.Rows.Count >= 0)
                                foreach (DataRow drow in dtTableCard.Rows)
                                {
                                    string sValue = drow["Card"].ToString();
                                    if (sValue == item) bRow = true;
                                }
                            if (bRow == true)
                            {
                                DeleteCard(lTerminalID.Text, cmbAcquirer.SelectedValue.ToString(), lIssuer.Text, item);
                                listCardNew.Items.RemoveAt(listCardNew.FindString(item));
                                listCard.Items.RemoveAt(listCard.FindString(item));

                                string stimestart = DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);

                                string sMessage = "Delete Card : " + sTerminalID + " \\ " + cmbAcquirer.SelectedValue.ToString() + " \\ " + lIssuer.Text + " \\ " + item + " SUCCESS";
                                CommonClass.doWritelogFile(string.Format("{0}, {1}", stimestart, sMessage));
                                DisplayStatus(string.Format("{0}, {1}", stimestart, sMessage));
                                CommonClass.InputLog(oSqlConn, lTerminalID.Text, sUserID, sDatabaseName, sMessage, "From Insys Tools Masking");

                            }
                            else
                            {
                                listCardNew.Items.RemoveAt(listCardNew.FindString(item));
                                listCard.Items.RemoveAt(listCard.FindString(item));

                            }
                            listCardNew.Refresh();
                            listCard.Refresh();
                            lCardNew.Text = (listCardNew.Items.Count).ToString();
                            lSelisih.Text = "(" + (listCard.Items.Count - listCardNew.Items.Count).ToString() + ")";
                        }
                    }


                    listCardNew.Update();
                    listCard.Update();
                }
               
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }

        }
        private void DisplayStatus(string sStatus)
        {
            string[] arrsStatus = sStatus.Split('\n');
            foreach (string sTemp in arrsStatus)
                rtbProgress.AppendText(sTemp + Environment.NewLine);
            rtbProgress.SelectionStart = rtbProgress.TextLength;
            rtbProgress.ScrollToCaret();
        }
        private void DeleteCard(string sTerminalID, string sAcquirer, string sIssuer, string sCard)
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileRelationDeleteCardByTerminalAcqIss, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oSqlCmd.Parameters.Add("@sAcquirerValue", SqlDbType.VarChar).Value = sAcquirer;
                oSqlCmd.Parameters.Add("@sIssuerValue", SqlDbType.VarChar).Value = sIssuer;
                oSqlCmd.Parameters.Add("@sCardValue", SqlDbType.VarChar).Value = sCard;
                oSqlCmd.ExecuteNonQuery();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (listCardNew.Items.Count > 0)
            {
                GetdtCardByTerminal();
                
                for (int i = 0; listCardNew.Items.Count > i; i++)
                {
                    bool bCardTerminal = false;
                    if (dtTableCardTerminal != null)
                        if (dtTableCardTerminal.Rows.Count >= 0)
                            foreach (DataRow drow in dtTableCardTerminal.Rows)
                            {
                                string sCardTerminal = drow["Card"].ToString();
                                if (sCardTerminal == listCardNew.Items[i].ToString()) bCardTerminal = true;
                            }

                    CultureInfo ciUSFormat = new CultureInfo("en-US", true);
                    string stimestart = DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm:ss.fff tt", ciUSFormat);
                    if (bCardTerminal == false)
                    {                        
                        using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPProfileRelationInsert, oSqlConn))
                        {
                            oSqlCmd.CommandType = CommandType.StoredProcedure;
                            oSqlCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                            oSqlCmd.Parameters.Add("@sAcquirerValue", SqlDbType.VarChar).Value = cmbAcquirer.SelectedValue.ToString();
                            oSqlCmd.Parameters.Add("@sIssuerValue", SqlDbType.VarChar).Value = lIssuer.Text;
                            oSqlCmd.Parameters.Add("@sCardValue", SqlDbType.VarChar).Value = listCardNew.Items[i].ToString();
                            oSqlCmd.ExecuteNonQuery();
                        }
                        string sMessage = "Add Card : "+ sTerminalID +" \\ "+ cmbAcquirer.SelectedValue.ToString() +" \\ "+ lIssuer.Text + " \\ "+ listCardNew.Items[i].ToString() + " SUCCESS";
                        CommonClass.doWritelogFile(string.Format("{0}, {1}", stimestart, sMessage));
                        DisplayStatus(string.Format("{0}, {1}", stimestart, sMessage));
                        CommonClass.InputLog(oSqlConn, lTerminalID.Text, sUserID, sDatabaseName, sMessage, "From Insys Tools Masking");

                    }
                    else
                    {
                        string sMessage = listCardNew.Items[i].ToString() + " EXISTING DATA";
                        DisplayStatus(string.Format("{0}, {1}", stimestart, sMessage));
                        CommonClass.doWritelogFile(string.Format("{0}, {1}", stimestart, sMessage));
                    }
                }
            }
            else MessageBox.Show("Cannot Save");
        }

        private void FrmMasking_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadView();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            listCardNew.Items.Clear();
            GetdtCard();
            InitCardNew();
        }
    }
}
