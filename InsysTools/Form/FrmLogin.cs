﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using InSysClass;

namespace InsysTools
{
    
    public partial class FrmLogin : Form
    {
        public string sUserName;
        public string sPassword;
        public string sPassFromDB;
        public string sUserNameFromDB;

        //string sConnString = ConfigurationManager.ConnectionStrings["Main"].ConnectionString;
        static string sAppDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
        //protected SqlConnection oSqlConn;


        static SqlConnection oSqlConn ;
        static SqlConnection oConnAuditTrail = new SqlConnection();

        static string sConnString;
        static string sConnStringAuditTrail;

        public bool bLogin = false;

        private enum ErrorCode
        {
            NoErr = 1, //avoid start from 0
            ErrLoginUserID,
            ErrLoginUserIDPasswordNotFound,
            ErrLockedUserID
        }
       
        public FrmLogin()
        {
            InitializeComponent();              
            //oSqlConn = new SqlConnection(sConnString);
            //oSqlConn.Open();
        }
        
        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                sConnString = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }


        private void btnLogin_Click(object sender, EventArgs e)
        {
            oSqlConn = InitConnection();
            //oSqlConn = new SqlConnection(sConnString);
            sUserName = txtName.Text;
            sPassword = CommonClass.sGetEncrypt(txtPass.Text);
            SetDisplay(false);
            wrkLogin.RunWorkerAsync();
        }

        //protected bool bValidHelpDesk()
        //{
        //    bool bvalid = false;
        //    try
        //    {
        //        string sCmd = " select HelpDesk from tbUserLogin where UserID =" + "'" + txtName.Text + "'";
        //        SqlCommand cmd = new SqlCommand(sCmd, oSqlConn);
        //        cmd.CommandType = CommandType.Text;
        //        using (SqlDataReader reader = cmd.ExecuteReader())
        //        {
        //            if (reader.HasRows)
        //            {
        //                reader.Read();
        //                string sValid = reader["DatabaseID"].ToString();
        //                if (sValid == "1")
        //                {
        //                    bvalid = true;
        //                }
        //                else
        //                {
        //                    bvalid = false;
        //                }
        //            }
        //            else
        //            {
        //                bvalid = false;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        bvalid = false;
        //    }
        //    return bvalid;

        //}

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void wrkLogin_DoWork(object sender, DoWorkEventArgs e)
        {
            if (isCanLogin())
                wrkLogin.ReportProgress(100, ErrorCode.NoErr);
            else
                wrkLogin.ReportProgress(100, ErrorCode.ErrLoginUserIDPasswordNotFound);
        }

        protected bool isCanLogin()
        {
            if (isUserIDFound()) // If is User
            {
                return true;
            }
            else return false;
        }

        protected string sCondition()
        {
           return " WHERE UserID='" + sUserName + "' AND Password='" + sPassword + "'";
        }

        protected bool isUserIDFound()
        {
            try
            {
               using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPUserBrowse, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCondition();
                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        using (DataTable dtUser = new DataTable())
                        {
                            dtUser.Load(oRead);
                            if (dtUser.Rows.Count > 0) // UserID Found
                            {
                                bLogin = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return bLogin;
        }

        private void wrkLogin_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 100)
            {
                switch ((ErrorCode)e.UserState)
                {
                    case ErrorCode.NoErr:
                        this.Close();
                        bLogin = true;
                        break;
                    case ErrorCode.ErrLoginUserID:
                        bLogin = false;
                        break;
                    case ErrorCode.ErrLoginUserIDPasswordNotFound:
                        bLogin = false;
                        break;
                    case ErrorCode.ErrLockedUserID:
                        bLogin = false;
                        break;
                    default:
                        break;
                }
            }
        }

        private void wrkLogin_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay(true);
            if (bLogin)
                CommonClass.InputLog(oSqlConn, "", sUserName, "", "Login Success By AppUpdateMoveProfile ", "Login AppUpdateMoveProfile");
        }

        private void SetDisplay(bool isEnabled)
        {
            this.Cursor = isEnabled ? Cursors.Default : Cursors.AppStarting;
            this.ControlBox = isEnabled;

            txtName.Enabled = isEnabled;
            txtPass.Enabled = isEnabled;

            btnLogin.Enabled = isEnabled;
            btnCancel.Enabled = isEnabled;
        }
    }
}
