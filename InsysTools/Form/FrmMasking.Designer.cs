﻿namespace InsysTools
{
    partial class FrmMasking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lTerminalID = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnView = new System.Windows.Forms.Button();
            this.lCardIssuer = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.listCard = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbIssuer = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbAcquirer = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTerminal = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lSelisih = new System.Windows.Forms.Label();
            this.rtbProgress = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.listCardNew = new System.Windows.Forms.CheckedListBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.lCardNew = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lIssuer = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbCard = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lTerminalID);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.btnView);
            this.groupBox1.Controls.Add(this.lCardIssuer);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.listCard);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmbIssuer);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbAcquirer);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtTerminal);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1123, 275);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "VIEW";
            // 
            // lTerminalID
            // 
            this.lTerminalID.AutoSize = true;
            this.lTerminalID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTerminalID.Location = new System.Drawing.Point(358, 29);
            this.lTerminalID.Name = "lTerminalID";
            this.lTerminalID.Size = new System.Drawing.Size(63, 13);
            this.lTerminalID.TabIndex = 28;
            this.lTerminalID.Text = "Terminal :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(287, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Terminal :";
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(188, 20);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(75, 23);
            this.btnView.TabIndex = 26;
            this.btnView.Text = "&View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.button1_Click);
            // 
            // lCardIssuer
            // 
            this.lCardIssuer.AutoSize = true;
            this.lCardIssuer.Location = new System.Drawing.Point(345, 249);
            this.lCardIssuer.Name = "lCardIssuer";
            this.lCardIssuer.Size = new System.Drawing.Size(29, 13);
            this.lCardIssuer.TabIndex = 19;
            this.lCardIssuer.Text = "Card";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(275, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Total Card : ";
            // 
            // listCard
            // 
            this.listCard.FormattingEnabled = true;
            this.listCard.Location = new System.Drawing.Point(361, 112);
            this.listCard.Name = "listCard";
            this.listCard.Size = new System.Drawing.Size(155, 134);
            this.listCard.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(287, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Card";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Issuer";
            // 
            // cmbIssuer
            // 
            this.cmbIssuer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIssuer.FormattingEnabled = true;
            this.cmbIssuer.Location = new System.Drawing.Point(360, 81);
            this.cmbIssuer.MaxDropDownItems = 20;
            this.cmbIssuer.Name = "cmbIssuer";
            this.cmbIssuer.Size = new System.Drawing.Size(156, 21);
            this.cmbIssuer.TabIndex = 11;
            this.cmbIssuer.SelectedIndexChanged += new System.EventHandler(this.cmbIssuer_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(287, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Acquirer";
            // 
            // cmbAcquirer
            // 
            this.cmbAcquirer.FormattingEnabled = true;
            this.cmbAcquirer.Location = new System.Drawing.Point(360, 51);
            this.cmbAcquirer.Name = "cmbAcquirer";
            this.cmbAcquirer.Size = new System.Drawing.Size(156, 21);
            this.cmbAcquirer.TabIndex = 9;
            this.cmbAcquirer.SelectedIndexChanged += new System.EventHandler(this.cmbAcquirer_SelectedIndexChanged);
            this.cmbAcquirer.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbAcquirer_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Terminal Init";
            // 
            // txtTerminal
            // 
            this.txtTerminal.Location = new System.Drawing.Point(85, 22);
            this.txtTerminal.Name = "txtTerminal";
            this.txtTerminal.Size = new System.Drawing.Size(97, 20);
            this.txtTerminal.TabIndex = 7;
            this.txtTerminal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTerminal_KeyPress);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.lSelisih);
            this.groupBox2.Controls.Add(this.rtbProgress);
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Controls.Add(this.listCardNew);
            this.groupBox2.Controls.Add(this.btnReset);
            this.groupBox2.Controls.Add(this.lCardNew);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.lIssuer);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnAdd);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cmbCard);
            this.groupBox2.Location = new System.Drawing.Point(12, 293);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1123, 227);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "CARD";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(523, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "LOG";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(229, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "==>";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(380, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "<=>";
            // 
            // lSelisih
            // 
            this.lSelisih.AutoSize = true;
            this.lSelisih.Location = new System.Drawing.Point(417, 203);
            this.lSelisih.Name = "lSelisih";
            this.lSelisih.Size = new System.Drawing.Size(10, 13);
            this.lSelisih.TabIndex = 27;
            this.lSelisih.Text = " ";
            // 
            // rtbProgress
            // 
            this.rtbProgress.BackColor = System.Drawing.SystemColors.Window;
            this.rtbProgress.Location = new System.Drawing.Point(526, 55);
            this.rtbProgress.Multiline = true;
            this.rtbProgress.Name = "rtbProgress";
            this.rtbProgress.ReadOnly = true;
            this.rtbProgress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbProgress.Size = new System.Drawing.Size(591, 135);
            this.rtbProgress.TabIndex = 26;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(480, 198);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // listCardNew
            // 
            this.listCardNew.FormattingEnabled = true;
            this.listCardNew.Location = new System.Drawing.Point(360, 36);
            this.listCardNew.Name = "listCardNew";
            this.listCardNew.Size = new System.Drawing.Size(151, 154);
            this.listCardNew.TabIndex = 24;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(277, 94);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 23;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lCardNew
            // 
            this.lCardNew.AutoSize = true;
            this.lCardNew.Location = new System.Drawing.Point(345, 203);
            this.lCardNew.Name = "lCardNew";
            this.lCardNew.Size = new System.Drawing.Size(29, 13);
            this.lCardNew.TabIndex = 22;
            this.lCardNew.Text = "Card";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(275, 203);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Total Card : ";
            // 
            // lIssuer
            // 
            this.lIssuer.AutoSize = true;
            this.lIssuer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lIssuer.Location = new System.Drawing.Point(424, 16);
            this.lIssuer.Name = "lIssuer";
            this.lIssuer.Size = new System.Drawing.Size(41, 13);
            this.lIssuer.TabIndex = 20;
            this.lIssuer.Text = "Issuer";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(357, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "From Issuer : ";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(277, 65);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 18;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(277, 36);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 17;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Card";
            // 
            // cmbCard
            // 
            this.cmbCard.FormattingEnabled = true;
            this.cmbCard.Location = new System.Drawing.Point(61, 36);
            this.cmbCard.MaxDropDownItems = 20;
            this.cmbCard.Name = "cmbCard";
            this.cmbCard.Size = new System.Drawing.Size(146, 21);
            this.cmbCard.TabIndex = 14;
            // 
            // FrmMasking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 532);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "FrmMasking";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmMasking";
            this.Load += new System.EventHandler(this.FrmMasking_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtTerminal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbIssuer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbAcquirer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listCard;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbCard;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lCardIssuer;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lCardNew;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lIssuer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.CheckedListBox listCardNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Label lTerminalID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox rtbProgress;
        private System.Windows.Forms.Label lSelisih;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
    }
}