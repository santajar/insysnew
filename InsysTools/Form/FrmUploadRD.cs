﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InSysClass;
using System.Data.SqlClient;
using System.IO;

namespace InsysTools
{
    public partial class FrmUploadRD : Form
    {
        SqlConnection oSqlConn;
        string sUserID = "";
        string sTxtFileName;
        DataTable DtTerminal;
        bool bFlagOpenFile;
        string sTerminalID;
        string sConditionRegion = "", sConditionGroup = "", sConditionCity = "", sConditionALL = "";
        string sCategory = "";
        string sValueCategory = "";
        static List<string> ListToGenerateReport = new List<string>();
        public FrmUploadRD(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            sUserID = _sUserID;

            LoadComboBox();
        }

        private void LoadComboBox()
        {
            InitComboBoxCity();
            InitComboBoxGroup();
            InitComboBoxRegion();
            InitComboBoxSoftwareName();
            //InitComboBoxDatabaseName();
        }
        private void InitComboBoxRegion()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPBrowseRegion, "");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbRegion.DataSource = dtSoftware;
                cmbRegion.DisplayMember = "Region";
                cmbRegion.ValueMember = "IdRegionRD";
                cmbRegion.SelectedIndex = -1;
            }
        }

        private void InitComboBoxCity()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPBrowseCity, "");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbCity.DataSource = dtSoftware;
                cmbCity.DisplayMember = "City";
                cmbCity.ValueMember = "IdCityRD";
                cmbCity.SelectedIndex = -1;
            }
        }

        private void InitComboBoxGroup()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPBrowseGroup, "");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbGroup.DataSource = dtSoftware;
                cmbGroup.DisplayMember = "Group";
                cmbGroup.ValueMember = "IdGroupRD";
                cmbGroup.SelectedIndex = -1;
            }
        }

        private void InitComboBoxSoftwareName()
        {
            DataTable dtSoftware = dtGetData(CommonSP.sSPAppPackageBrowse, "");
            if (dtSoftware.Rows.Count > 0)
            {
                cmbSoftwareName.DataSource = dtSoftware;
                cmbSoftwareName.DisplayMember = "Software Name";
                cmbSoftwareName.ValueMember = "BuildNumber";
                cmbSoftwareName.SelectedIndex = -1;
            }
        }
     

        private DataTable dtGetData(string sSP, string sCond)
        {
            SqlCommand oCmd = new SqlCommand(sSP, oSqlConn);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = sCond;
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oRead = oCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            if (oRead.HasRows)
                dtTemp.Load(oRead);
            oRead.Close();
            oRead.Dispose();
            oCmd.Dispose();
            return dtTemp;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        public void OpenFile()
        {
            //label1.Visible = false;

            sTxtFileName = "";
            dataGridView1.DataSource = null;

            OfDFile.Title = "Select File";
            OfDFile.InitialDirectory = Application.StartupPath;
            OfDFile.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            OfDFile.FilterIndex = 1;
            OfDFile.RestoreDirectory = true;
            try
            {
                if (OfDFile.ShowDialog() == DialogResult.OK)
                {
                    sTxtFileName = OfDFile.FileName;
                    InsertToDataTable(sTxtFileName);
                    bFlagOpenFile = true;
                    dataGridView1.DataSource = DtTerminal;
                    //panel1.Enabled = true;
                    //label1.Text = sTxtFileName;
                    //label1.Visible = true;
                   // textBox1.Text = sTxtFileName;
                    //label3.Text = DtTerminal.Rows.Count.ToString();

                }
            }
            catch
            {
                MessageBox.Show("Failed Open File");
            }
        }

        public DataTable InsertToDataTable(string sFilePath)
        {
            DtTerminal = new DataTable();
            string[] columns = null;

            var lines = File.ReadAllLines(sFilePath);

            if (lines.Count() > 0)
            {
                columns = lines[0].Split(new char[] { ',' });
                foreach (var column in columns)
                    ////DtTerminal.Columns.Add(column);
                    DtTerminal.Columns.Add("TerminalID");
            }

            for (int i = 0; i < lines.Count(); i++)
            {
                DataRow dr = DtTerminal.NewRow();
                string[] values = lines[i].Split(new char[] { ',' });

                for (int j = 0; j < values.Count() && j < columns.Count(); j++)
                    dr[j] = values[j];

                DtTerminal.Rows.Add(dr);
            }
            return DtTerminal;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            sCondition();
            StartUpload(DtTerminal);
            lstResult.DataSource = ListToGenerateReport;
        }

        public void StartUpload(DataTable dt)
        {
            SqlCommand oCmd;
            foreach (DataRow dr in dt.Rows)
            {
                oCmd = new SqlCommand("spRDInsertProcessListTid", oSqlConn);
                oCmd.CommandType = CommandType.StoredProcedure;
                sTerminalID = dr[0].ToString();
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oCmd.Parameters.Add("@sCategory", SqlDbType.VarChar).Value = sCategory;
                oCmd.Parameters.Add("@sCategoryName", SqlDbType.VarChar).Value = sValueCategory;
                oCmd.Parameters.Add("@dtScheduleTime", SqlDbType.DateTime).Value = dtSchedule.Text.ToString();
                oCmd.Parameters.Add("@sSoftwareName", SqlDbType.VarChar).Value = cmbSoftwareName.Text.ToString();
                oCmd.Parameters.Add("@BuildNumber", SqlDbType.Int).Value = Convert.ToInt32(txtBuildNumber.Text.ToString());
                try
                {
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    oCmd.ExecuteNonQuery();
                    CommonClass.InputLog(oSqlConn, "", sUserID, "", sTerminalID + " Insert List TID RD By Tools  ", sTerminalID + " Insert List TID RD By Tools Success");
                    ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + " Insert List TID RD By Tools Success");
                }
                catch (Exception ex)
                {
                    CommonClass.InputLog(oSqlConn, "", sUserID, "", sTerminalID + " Insert List TID RD By Tools  ", sTerminalID + " Insert List TID RD By Tools Failed");
                    ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + " Insert List TID RD By Tools Failed");
                }
            }

        }


        public void sCondition()
        {

            if (cmbRegion.SelectedIndex > -1)
            {
                sCategory = "Region";
                sValueCategory = cmbRegion.Text;
            }

            if (cmbGroup.SelectedIndex > -1)
            {
                sCategory = "Group";
                sValueCategory = cmbGroup.Text;
            }

            if (cmbCity.SelectedIndex > -1)
            {
                sCategory = "City";
                sValueCategory = cmbCity.Text;
            }
            
        }

        private void cmbSoftwareName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSoftwareName.SelectedIndex > -1)
                txtBuildNumber.Text = cmbSoftwareName.SelectedValue.ToString();
            else
                txtBuildNumber.Text = "";
        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableOtherInActive();
        }


        private void DisableOtherInActive()
        {
            if (cmbGroup.SelectedIndex > 0)
            {
                gbCity.Enabled = false;
                gbRegion.Enabled = false;
            }
            if (cmbCity.SelectedIndex > 0)
            {
                gbGroup.Enabled = false;
                gbRegion.Enabled = false;
            }
            if (cmbRegion.SelectedIndex > 0)
            {
                gbCity.Enabled = false;
                gbGroup.Enabled = false;
            }
        }

       

        private void cmbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableOtherInActive();
        }

        private void cmbCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableOtherInActive();
        }

        private void cmbGroup_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            DisableOtherInActive();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            DtTerminal.Clear();
            //gbCity.Enabled = true;
            //gbGroup.Enabled = true;
            //gbRegion.Enabled = true;
            cmbGroup.SelectedIndex = 0;
            cmbCity.SelectedIndex = 0;
            cmbRegion.SelectedIndex = 0;
            cmbSoftwareName.SelectedIndex = 0;
            txtBuildNumber.Text = "-";
           // panel1.Enabled = false;
        }

        private void FrmUploadRD_Load(object sender, EventArgs e)
        {

        }

       
       
    }
}
