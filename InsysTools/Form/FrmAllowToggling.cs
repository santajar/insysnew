﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace InsysTools
{
    public partial class FrmAllowToggling : Form
    {
        string sTxtFileName;
        DataTable DtTerminal;
        SqlConnection oSqlConn;
        string sUserID;
        bool bFlagOpenFile = false;
        string sTerminalID = "";
        string sValueTagDC003 = "";
        public int iMaxRetry;
        
         static List<string> ListToGenerateReport = new List<string>();


        

        public FrmAllowToggling(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            sUserID = _sUserID;
            iMaxRetry = int.Parse(ConfigurationManager.AppSettings["MaxRetry"].ToString());
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        public void OpenFile()
        {
            label1.Visible = false;
            sTxtFileName = "";
            dataGridView1.DataSource = null;

            OfDFile.Title = "Select File";
            OfDFile.InitialDirectory = Application.StartupPath;
            OfDFile.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            OfDFile.FilterIndex = 1;
            OfDFile.RestoreDirectory = true;
            try
            {
                if (OfDFile.ShowDialog() == DialogResult.OK)
                {
                    sTxtFileName = OfDFile.FileName;
                    InsertToDataTable(sTxtFileName);
                    bFlagOpenFile = true;
                    dataGridView1.DataSource = DtTerminal;
                    label1.Text = sTxtFileName;
                    label1.Visible = true;
                    label3.Text = DtTerminal.Rows.Count.ToString();

                }
            }
            catch
            {
                MessageBox.Show("Failed Open File");
            }
        }

        public DataTable InsertToDataTable(string sFilePath)
        {
            DtTerminal = new DataTable();
            string[] columns = null;

            var lines = File.ReadAllLines(sFilePath);

            if (lines.Count() > 0)
            {
                columns = lines[0].Split(new char[] { ',' });
                foreach (var column in columns)
                    ////DtTerminal.Columns.Add(column);
                    DtTerminal.Columns.Add("TerminalID");
            }

            for (int i = 0; i < lines.Count(); i++)
            {
                DataRow dr = DtTerminal.NewRow();
                string[] values = lines[i].Split(new char[] { ',' });

                for (int j = 0; j < values.Count() && j < columns.Count(); j++)
                    dr[j] = values[j];

                DtTerminal.Rows.Add(dr);
            }
            return DtTerminal;
        }


        //private void SetDataTID(DataTable dt)
        //{
        //    string sTID = "";
        //    int iDtRows = dt.Rows.Count;
        //    int iLastRows = 0;
        //    foreach (DataRow dr in dt.Rows)
        //    {

        //        iLastRows = iLastRows + 1 ;
        //        if (iLastRows == iDtRows)
        //        {
        //            sTID += "'" + dr[0].ToString() + "'";
        //        }
        //        else
        //        {
        //            sTID += "'" + dr[0].ToString() + "',";
        //        }
               
        //    }
        //}

        private void StartUpdate(DataTable dt)
        {
            //string sQuery;
            SqlCommand oCmd;
            int iValue;
            if (chkAllow.Checked == true)
            {
                iValue = 1;
            }
            else
            {
                iValue = 0;
            }
           // int iCounterRows = 0, iNotSucces = 0;
            //Logs.Write("Start Process");

            foreach (DataRow dr in dt.Rows)
            {
                //sQuery = string.Format("UPDATE tbProfileTerminalList SET AllowDownload = 1, AutoInitTimeStamp=GETDATE() WHERE TerminalID ='{0}'", dr[0].ToString());
                //oCmd = new SqlCommand("sptoolsUnAllowToggling", oSqlConn);
                oCmd = new SqlCommand("spToolsAllowUnAllowToggling", oSqlConn); 
                oCmd.CommandType = CommandType.StoredProcedure;
                sTerminalID = dr[0].ToString();
                sValueTagDC003 = DateTime.Now.ToString("MMddyyHHmmss").Replace("/", "").Replace(".", "").Replace(":", "");
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                oCmd.Parameters.Add("@sDateTime", SqlDbType.VarChar).Value = sValueTagDC003;
                oCmd.Parameters.Add("@sMaxInitRetry", SqlDbType.Int).Value = iMaxRetry;
                oCmd.Parameters.Add("@sValue", SqlDbType.Int).Value = iValue;
                SqlParameter returnParameter = new SqlParameter("@RetVal", SqlDbType.NVarChar, 100);
                returnParameter.Direction = ParameterDirection.Output;
                oCmd.Parameters.Add(returnParameter);
                try
                {
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    oCmd.ExecuteNonQuery();
                    string sReturn = (string)returnParameter.Value.ToString();    

                    // iCounterRows = iCounterRows + 1;
                    if (sReturn == "Success")
                    {
                        bool sAutoInitFtps = bool.Parse(ConfigurationManager.AppSettings["AutoInitFtps"]);
                        if (iValue == 1)
                        {
                            ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + "Allow Initialize Success With MaxRetry : " + iMaxRetry.ToString());
                            CommonClass.InputLog(oSqlConn, "", sUserID, "", sTerminalID + " Allow Initialize By Tools With MaxRetry : " + iMaxRetry.ToString(), sTerminalID + " Allow Initialize Success With MaxRetry : " + iMaxRetry.ToString());
                            if (sAutoInitFtps == true)
                            {
                                ServiceInSysAutoInitFtps.ServiceInSysClient serviceInSys = new ServiceInSysAutoInitFtps.ServiceInSysClient();
                                serviceInSys.ExportToProfile(sTerminalID);
                            }
                        }
                        else
                        {
                            ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + "Uncheck Allow Initialize Success With MaxRetry : 0" );
                            CommonClass.InputLog(oSqlConn, "", sUserID, "", sTerminalID + "Uncheck Allow Initialize By Tools With MaxRetry : 0 ", sTerminalID + "Uncheck Allow Initialize Success With MaxRetry : 0");
                            if (sAutoInitFtps == true)
                            {
                                ServiceInSysAutoInitFtps.ServiceInSysClient serviceInSys = new ServiceInSysAutoInitFtps.ServiceInSysClient();
                                serviceInSys.UploadFileNotAllow(sTerminalID);
                            }
                        }
                    }
                    else
                    {
                        if (iValue == 1)
                        {
                            ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + "Failed");
                            CommonClass.InputLog(oSqlConn, sTerminalID, sUserID, "", sTerminalID + " Allow Initialize By Tools   : " + iMaxRetry.ToString(), sTerminalID + " Allow Initialize Failed With " + iMaxRetry.ToString());
                        }
                        else
                        {
                            ListToGenerateReport.Add(sTerminalID + " | " + "" + "    |" + "Failed");
                            CommonClass.InputLog(oSqlConn, "", sUserID, "", sTerminalID + "Uncheck Allow Initialize By Tools With MaxRetry : 0 ", sTerminalID + "Uncheck Allow Initialize failed With MaxRetry : 0");
                        }
                    }
                    //Logs.Write(string.Format("Terminal ID : {0} Done.", sLine));
                }
                catch (Exception ex)
                {
                   // iNotSucces = iNotSucces + 1;
                   //iCounterRows = iCounterRows + 1;
                    ListToGenerateReport.Add(ex.StackTrace);
                   // Logs.Write(string.Format("Terminal ID : {0} Error.", sLine));
                }
                              

            }
        }

        private void StartTagDC003(DataTable dt)
        {
            string sQuery;
            SqlCommand oCmd;
            int iSucces = 0, iNotSucces = 0;
            //Logs.Write("Start Process");
            foreach (DataRow dr in dt.Rows)
            {
                sQuery = string.Format("UPDATE tbProfileTerminal SET TerminalTagValue = '{0}' WHERE TerminalTag ='DC003' AND TerminalID IN ('{1}')", DateTime.Now.ToString("MMddyyHHms").Replace("/","").Replace(".","").Replace(":", "") , dr[0].ToString());
                oCmd = new SqlCommand(sQuery, oSqlConn);

                try
                {
                    if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                    oCmd.ExecuteNonQuery();
                    iSucces = iSucces + 1;
                    //Logs.Write(string.Format("Terminal ID : {0} Done.", sLine));
                }
                catch (Exception ex)
                {
                    iNotSucces = iNotSucces + 1;
                    // Logs.Write(string.Format("Terminal ID : {0} Error.", sLine));
                }


            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            
            StartUpdate(DtTerminal);
           
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 100)
            {
                progressBar1.Style = ProgressBarStyle.Blocks;
                this.Cursor = Cursors.Default;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay("STOP");
            lstResult.DataSource = ListToGenerateReport;
            MessageBox.Show("UNINTALLIZE ALLOW DONE");
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            SetDisplay("START");
            backgroundWorker1.RunWorkerAsync();
    
        }

        private void SetDisplay(string sFlag)
        {
            if (sFlag == "START")
            {
                progressBar1.Value = 0;
                progressBar1.Maximum = 100;
                progressBar1.Style = ProgressBarStyle.Marquee;
                dataGridView1.Enabled = false;
                btnBrowse.Enabled = false;
                btnExecute.Enabled = false;
            }
            if (sFlag == "STOP")
            {
                btnBrowse.Enabled = true;
                btnExecute.Enabled = true;
                dataGridView1.Enabled = true;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 100;
            }
        }

        private void FrmAllowToggling_Load(object sender, EventArgs e)
        {

        }

    }
}
