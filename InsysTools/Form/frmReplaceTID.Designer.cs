﻿namespace InsysTools
{
    partial class frmReplaceTID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReplaceTID));
            this.gbFile = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStartProcess = new System.Windows.Forms.Button();
            this.pbProgress = new System.Windows.Forms.ProgressBar();
            this.btnClose = new System.Windows.Forms.Button();
            this.gbProgressBar = new System.Windows.Forms.GroupBox();
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnViewLog = new System.Windows.Forms.Button();
            this.gbMesssage = new System.Windows.Forms.GroupBox();
            this.rtbMessage = new System.Windows.Forms.RichTextBox();
            this.ofdReplaceFile = new System.Windows.Forms.OpenFileDialog();
            this.bgExcelWorker = new System.ComponentModel.BackgroundWorker();
            this.gbFile.SuspendLayout();
            this.gbProgressBar.SuspendLayout();
            this.gbButton.SuspendLayout();
            this.gbMesssage.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbFile
            // 
            this.gbFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbFile.Controls.Add(this.btnBrowse);
            this.gbFile.Controls.Add(this.txtFileName);
            this.gbFile.Controls.Add(this.label1);
            this.gbFile.Location = new System.Drawing.Point(5, 1);
            this.gbFile.Name = "gbFile";
            this.gbFile.Size = new System.Drawing.Size(447, 58);
            this.gbFile.TabIndex = 0;
            this.gbFile.TabStop = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowse.Location = new System.Drawing.Point(356, 17);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(80, 30);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(124, 23);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            this.txtFileName.Size = new System.Drawing.Size(210, 20);
            this.txtFileName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "File Name";
            // 
            // btnStartProcess
            // 
            this.btnStartProcess.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnStartProcess.Location = new System.Drawing.Point(270, 13);
            this.btnStartProcess.Name = "btnStartProcess";
            this.btnStartProcess.Size = new System.Drawing.Size(80, 30);
            this.btnStartProcess.TabIndex = 3;
            this.btnStartProcess.Text = "Start";
            this.btnStartProcess.UseVisualStyleBackColor = true;
            this.btnStartProcess.Click += new System.EventHandler(this.btnStartProcess_Click);
            // 
            // pbProgress
            // 
            this.pbProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pbProgress.Location = new System.Drawing.Point(5, 11);
            this.pbProgress.Name = "pbProgress";
            this.pbProgress.Size = new System.Drawing.Size(436, 23);
            this.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pbProgress.TabIndex = 4;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(356, 13);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 30);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // gbProgressBar
            // 
            this.gbProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbProgressBar.Controls.Add(this.pbProgress);
            this.gbProgressBar.Location = new System.Drawing.Point(5, 59);
            this.gbProgressBar.Name = "gbProgressBar";
            this.gbProgressBar.Size = new System.Drawing.Size(447, 39);
            this.gbProgressBar.TabIndex = 1;
            this.gbProgressBar.TabStop = false;
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnViewLog);
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Controls.Add(this.btnStartProcess);
            this.gbButton.Location = new System.Drawing.Point(5, 298);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(447, 50);
            this.gbButton.TabIndex = 2;
            this.gbButton.TabStop = false;
            // 
            // btnViewLog
            // 
            this.btnViewLog.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnViewLog.Location = new System.Drawing.Point(7, 13);
            this.btnViewLog.Name = "btnViewLog";
            this.btnViewLog.Size = new System.Drawing.Size(80, 30);
            this.btnViewLog.TabIndex = 5;
            this.btnViewLog.Text = "View Log";
            this.btnViewLog.UseVisualStyleBackColor = true;
            this.btnViewLog.Click += new System.EventHandler(this.btnViewLog_Click);
            // 
            // gbMesssage
            // 
            this.gbMesssage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbMesssage.Controls.Add(this.rtbMessage);
            this.gbMesssage.Location = new System.Drawing.Point(5, 98);
            this.gbMesssage.Name = "gbMesssage";
            this.gbMesssage.Size = new System.Drawing.Size(446, 200);
            this.gbMesssage.TabIndex = 3;
            this.gbMesssage.TabStop = false;
            // 
            // rtbMessage
            // 
            this.rtbMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbMessage.Location = new System.Drawing.Point(5, 12);
            this.rtbMessage.Name = "rtbMessage";
            this.rtbMessage.ReadOnly = true;
            this.rtbMessage.Size = new System.Drawing.Size(435, 182);
            this.rtbMessage.TabIndex = 0;
            this.rtbMessage.Text = "";
            // 
            // bgExcelWorker
            // 
            this.bgExcelWorker.WorkerReportsProgress = true;
            this.bgExcelWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgExcelWorker_DoWork);
            this.bgExcelWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgExcelWorker_RunWorkerCompleted);
            this.bgExcelWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgExcelWorker_ProgressChanged);
            // 
            // frmReplaceTID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(455, 351);
            this.ControlBox = false;
            this.Controls.Add(this.gbMesssage);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.gbProgressBar);
            this.Controls.Add(this.gbFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmReplaceTID";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Replace TID";
            this.Load += new System.EventHandler(this.frmReplaceTID_Load);
            this.gbFile.ResumeLayout(false);
            this.gbFile.PerformLayout();
            this.gbProgressBar.ResumeLayout(false);
            this.gbButton.ResumeLayout(false);
            this.gbMesssage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbFile;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStartProcess;
        private System.Windows.Forms.ProgressBar pbProgress;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox gbProgressBar;
        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.GroupBox gbMesssage;
        private System.Windows.Forms.RichTextBox rtbMessage;
        private System.Windows.Forms.OpenFileDialog ofdReplaceFile;
        private System.ComponentModel.BackgroundWorker bgExcelWorker;
        private System.Windows.Forms.Button btnViewLog;
    }
}

