﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InsysTools
{
    public partial class FrmOption : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;
        static string sConnString;
        static string sConnStringAuditTrail;

        static string sAppDirectory = Directory.GetCurrentDirectory();

        int iDatabaseIdSource;
        string sDatabaseIDSource;
        int iDatabaseIdDest;
        string sDatabaseIDDest;
        List<string> ltTerminalId = new List<string>();
        DataSet dsTables;
        string sFilename;
        static string sUserID;

        public FrmOption()
        {
            InitializeComponent();
            //oSqlConn = _oSqlConn;
            //oSqlConnAuditTrail = _oSqlConnAuditTrail;
        }

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                sConnString = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }


        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnectionAuditTrail()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitDataAuditTrail oObjInit = new InitDataAuditTrail(sAppDirectory);
                sConnStringAuditTrail = oObjInit.sGetConnString();
                sConnStringAuditTrail = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnStringAuditTrail);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }

        private void FrmOption_Load(object sender, EventArgs e)
        {
            if (oSqlConn == null)
            {
                //oSqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString);
                //oSqlConnAuditTrail = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString);
                oSqlConn = InitConnection();
                //oSqlConnAuditTrail = new SqlConnection(sConnString);
                oSqlConnAuditTrail = InitConnectionAuditTrail();
            }

            if (string.IsNullOrEmpty(sUserID))
            {
                FrmLogin login = new FrmLogin();
                login.ShowDialog();
                if (login.bLogin)
                {
                    sUserID = login.sUserName;
                    UserData.sUserID = sUserID;
                    if (bValidHelpDesk() == true)
                    {
                        btnMoveData.Enabled = false;
                        btnReplceTID.Enabled = false;
                        btnQCEMS.Enabled = false;
                        btnUploadTIDRD.Enabled = false;
                        btnUploadEMS.Enabled = false;
                        btnUpdateParam.Enabled = false;
                    }
                }
                else
                    this.Close();
                login.Dispose();
            }
        }

        private void btnMoveData_Click(object sender, EventArgs e)
        {
            FrmMoveData fMoveData = new FrmMoveData(oSqlConn, sUserID);
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Akses Menu Move Data", "Akses Menu Move Data");
            fMoveData.ShowDialog();
        }

        private void btnUpdateConn_Click(object sender, EventArgs e)
        {
            FrmUpdateConnection fUpdateConn = new FrmUpdateConnection(oSqlConn, sUserID);
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Akses Menu Update Connection ", "Akses Menu Update Connection");
            fUpdateConn.ShowDialog();
        }

        private void btnUpdateParam_Click(object sender, EventArgs e)
        {
            FrmUpdateParam fUpdateParam = new FrmUpdateParam(oSqlConn, sUserID);
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Akses Menu Update Parameter ", "Akses Menu Update Parameter");
            fUpdateParam.ShowDialog();
        }

        private void FrmOption_FormClosed(object sender, FormClosedEventArgs e)
        {
            CommonClass.UserAccessDelete(oSqlConn, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmQC2 frmqc = new frmQC2(oSqlConn, sUserID);
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Akses Menu Open QC EMS ", "Akses Menu QC EMS");
            frmqc.ShowDialog();
        }

        private void btnQCReport_Click(object sender, EventArgs e)
        {

        }

        private void btnReplceTID_Click(object sender, EventArgs e)
        {
            frmReplaceTID FrmRep = new frmReplaceTID(oSqlConn, sUserID);
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Akses Menu Replce TID ", "Akses Menu Replace TID");
            FrmRep.Show();

        }

        private void btnToggling_Click(object sender, EventArgs e)
        {
            FrmAllowToggling FrmToogling = new FrmAllowToggling(oSqlConn, sUserID);
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Akses Menu Open Toggling ", "Akses Menu Open Toggling");
            FrmToogling.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmUploadRD FrmRd = new FrmUploadRD(oSqlConn, sUserID);
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Akses Menu Upload RD ", "Akses Menu Upload RD");
            FrmRd.Show();


        }
        
        private void btnUploadEMS_Click(object sender, EventArgs e)
        {
            ////if (oSqlConn == null)
            ////{
            //// oSqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString);
            //oSqlConn = InitConnection();
            //// oSqlConnAuditTrail = new SqlConnection(ConfigurationManager.ConnectionStrings["Main"].ConnectionString);
            //oSqlConnAuditTrail = InitConnectionAuditTrail();
            ////}
            FrmUploadDataEms FrmDataEMS = new FrmUploadDataEms(oSqlConn, oSqlConnAuditTrail,sUserID);
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Akses Menu Upload EMS ", "Akses Menu Upload EMS");
            FrmDataEMS.Show();
        }

        protected bool bValidHelpDesk()
        {
            bool bvalid = false;
            try
            {
                string sCmd = " select HelpDesk from tbUserLogin where UserID =" + "'" + sUserID + "'";
                SqlCommand cmd = new SqlCommand(sCmd, oSqlConn);
                cmd.CommandType = CommandType.Text;
                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        bvalid = (bool)reader["HelpDesk"];
                    }
                    else
                    {
                        bvalid = true;
                    }
                    reader.Close();
                    reader.Dispose();
                }
            }
            catch (Exception ex)
            {
                bvalid = false;
            }

            return bvalid;

        }

        private void btnUploadPTD_Click(object sender, EventArgs e)
        {

        }

        private void btnMasking_Click(object sender, EventArgs e)
        {
            FrmMasking Frmmasking = new FrmMasking(oSqlConn, oSqlConnAuditTrail, sUserID);
            CommonClass.InputLog(oSqlConn, "", sUserID, "", "Akses Menu Masking", "Akses Menu Masking");
            Frmmasking.ShowDialog();
        }

        private void btnUploadPTD_Click_1(object sender, EventArgs e)
        {

        }
    }
}
