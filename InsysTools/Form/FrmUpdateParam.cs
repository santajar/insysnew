﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace InsysTools
{
    public partial class FrmUpdateParam : Form
    {
        protected SqlConnection oSqlConn;
        protected SqlConnection oSqlConnAuditTrail;

        int iDatabaseIdSource;
        string sDatabaseIDSource;
        int iDatabaseIdDest;
        string sDatabaseIDDest;

        List<string> ltTerminalId = new List<string>();
        DataSet dsTables;
        string sFilename;
        static string sUserID;

        DataTable dtItemList;
        DataTable dtName;
        DataTable dtValue;
        bool bValidObjectCmb = false;
        string sCurrDbID = null;
        string sFormTypeID = null;
        string sTag = null;
        string sTagName = null;
        string sNewValue = null;
        string sNameFilter = null;
        string sItemSequence = null;
        string sObjectID = null;
        DataTable dtCmb = new DataTable();
        protected DataTable dtItemListEnabled;

        public int iMaxRetry;

        public FrmUpdateParam(SqlConnection _oSqlConn, string _sUserID)
        {
            InitializeComponent();
            oSqlConn = _oSqlConn;
            oSqlConnAuditTrail = _oSqlConn;
            sUserID = _sUserID;
            iMaxRetry = int.Parse(ConfigurationManager.AppSettings["MaxRetry"].ToString());
        }

        private void FrmUpdateParam_Load(object sender, EventArgs e)
        {
            CommonClass.FillComboBox(oSqlConn, CommonSP.sSPTerminalDBBrowse, "", "DatabaseID", "DatabaseName", ref cmbDbSource);
            cmbName.Hide();
            //string s = "WHERE DatabaseID = 130";
            //dtItemListEnabled = dtGetItemListEnabled(oSqlConn);
            //dtCmb = oDtGetCmbList(s);
        }

        protected DataTable oDtGetCmbList(string _sCond)
        {
            DataTable oDataTableCmbValue = new DataTable();
            try
            {
                SqlCommand oSqlCmdCmb = new SqlCommand(CommonSP.sSPCmbBrowse, oSqlConn);

                oSqlCmdCmb.CommandType = CommandType.StoredProcedure;

                oSqlCmdCmb.Parameters.Add("@sCond", SqlDbType.VarChar).Value = ""; // Load all data with no condition

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                oDataTableCmbValue.Clear();

                SqlDataReader oReadCmb = oSqlCmdCmb.ExecuteReader();

                if (oReadCmb.HasRows)
                    oDataTableCmbValue.Load(oReadCmb);

                oReadCmb.Close();
                oReadCmb.Dispose();
                oSqlCmdCmb.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
            return oDataTableCmbValue;
        }
        public static DataTable dtGetItemListEnabled(SqlConnection _oSqlConn)
        {
            DataTable dtTemp = new DataTable();
            using (SqlCommand oComm = new SqlCommand(CommonSP.sSPItemEnableBrowse, _oSqlConn))
            {
                oComm.CommandType = CommandType.StoredProcedure;
                oComm.Parameters.Add("@sGroupID", SqlDbType.VarChar).Value = UserData.sGroupID;
                using (SqlDataAdapter oAdapt = new SqlDataAdapter(oComm))
                    oAdapt.Fill(dtTemp);
            }
            return dtTemp;
        }

        private void cmbDbSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.Items.Count > 0)
                    if (sCurrDbID != sGetDbId())
                        {
                            sCurrDbID = sGetDbId();
                            InitData();
                        }
        }

        protected string sGetDbId()
        {
            return (cmbDbSource.SelectedIndex >= 0) ? cmbDbSource.SelectedValue.ToString() : "";
        }

        protected void InitData()
        {
            if (cmbDbSource.SelectedIndex >= 0)
                using (SqlCommand sqlcommand = new SqlCommand(CommonSP.sSPItemBrowse, oSqlConn))
                {
                    sqlcommand.CommandType = CommandType.StoredProcedure;
                    sqlcommand.Parameters.Add("@sCond", SqlDbType.VarChar).Value = sGetTagCondition();

                    if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();

                    using (SqlDataReader sqldatareader = sqlcommand.ExecuteReader())
                        if (sqldatareader.HasRows)
                        {
                            dtItemList = new DataTable();
                            dtItemList.Load(sqldatareader);

                            cmbItemList.DataSource = null;
                            cmbItemList.Items.Clear();
                        
                                cmbItemList.DataSource = dtItemList;
                                cmbItemList.DisplayMember = "ItemName";
                                cmbItemList.ValueMember = "Tag";
                            
                        }
                }
        }

        protected string sFormID()
        {
            string sForm = "";
            string sFormType = cmbFormType.Text;
            switch (sFormType.ToLower())
            {
                case "terminal":
                    sForm = "1";
                    break;
                case "acquirer":
                    sForm = "2";
                    break;
                case "issuer":
                    sForm = "3";
                    break;
                default:
                    sForm = "";
                    break;
            }
            return sForm;
        }

        protected string sGetTagCondition()
        {
            string sCondition1 = string.Format("WHERE DatabaseID = {0}", sGetDbId());
            sFormTypeID = sFormID();
            string sCondition2 = !string.IsNullOrEmpty(sFormTypeID) ? string.Format("FormID={0}", sFormTypeID) : "";

            if (!string.IsNullOrEmpty(sCondition2))
                sCondition1 = string.Format("{0} AND {1} AND (Tag NOT LIKE '%01') ORDER BY FormID, Tag", sCondition1, sCondition2);
            else
                sCondition1 = string.Format("{0} AND (Tag NOT LIKE '%01') ORDER BY FormID, Tag", sCondition1);
            return sCondition1;
        }

        private void cmbFormType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.Items.Count > 0)
                    if (sCurrDbID != sGetDbId())
                    {
                        sCurrDbID = sGetDbId();
                        InitData();
                    }
                    else
                        InitData();                    
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openfileText = new OpenFileDialog();
            openfileText.Filter = "Text File(*.txt)|*.txt";
            openfileText.InitialDirectory = Environment.CurrentDirectory;
            openfileText.ShowDialog();
            if (!string.IsNullOrEmpty(openfileText.FileName))
            {
                txtFilename.Text = openfileText.FileName;
                sFilename = openfileText.FileName;
            }
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                SetDisplay(false);
                bwUpdateParam.RunWorkerAsync();
            }
        }

        private void SetDisplay(bool isEnable)
        {
            bValidObjectCmb = false;
            gbButton.Enabled = isEnable;
            gbFilter.Enabled = isEnable;
            gbSource.Enabled = isEnable;
            txtFilename.Enabled = true;
            txtNewValue.Enabled = true;
           
            pbMoveData.Style = isEnable ? ProgressBarStyle.Blocks : ProgressBarStyle.Marquee;
        }

        private bool IsValid()
        {
            iDatabaseIdSource = int.Parse(cmbDbSource.SelectedValue.ToString());
            sDatabaseIDSource = cmbDbSource.Text;
            sFormTypeID = sFormID();
            sTag = cmbItemList.SelectedValue.ToString();
            if (bValidObjectCmb == false)
            {
                sTagName = cmbItemList.Text;
                sNewValue = txtNewValue.Text;
                sNameFilter = txtNameFilter.Text;
            }
            else
            {
                sTagName = cmbItemList.Text;
                sNewValue = cmbValue.SelectedValue.ToString();
                sNameFilter = txtNameFilter.Text;
            }
            
            if (!string.IsNullOrEmpty(sFilename))
            {
                InitTerminalID();
                if (!string.IsNullOrEmpty(sFormTypeID) && !string.IsNullOrEmpty(sTag))
                {
                    if (iDatabaseIdSource != 0)
                    {
                        if (!string.IsNullOrEmpty(sNewValue) && IsValidValue())
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        protected bool IsValidValue()
        {
            bool bReturn = true;
            DataRow drRow = ((DataRow[])dtItemList.Select(string.Format("Tag='{0}'", sTag)))[0];
            string sObjectName = drRow["ObjectName"].ToString();
            switch (sObjectName.ToLower())
            {
                case "textbox": //textbox
                    bReturn = isTextBoxValid(drRow);
                    break;
                case "radiobutton": //radio button
                    break;
                case "checkbox": //checkbox
                    if (sNewValue == "1" || sNewValue == "0") bReturn = true;
                    else
                        bReturn = false;
                    break;
                case "combobox": //combobox
                    
                    //bReturn = false;
                    bReturn = true;
                    break;
            }
            return bReturn;
        }

        protected bool isTextBoxValid(DataRow drRow)
        {            
            string sErrMsg = drRow["ValidationMsg"].ToString() + "\n";
            string sItemName = drRow["ItemName"].ToString();
            
            if (isNullorEmpty(sNewValue, drRow)) // tidak boleh kosong atau null
            {
                sErrMsg += sItemName + " is still empty. Please fill the " + sItemName;
            }
            else if (isNotPassMinLengthLimit(sNewValue, drRow)) // length < min length
            {
                sErrMsg += sItemName + " should be " + drRow["vMinLength"].ToString() + " digit(s)";
            }
            else if (isFormatNotValid(sNewValue, drRow)) // validasi format; A : alphanumeric, N : numeric, T : time, S : kec ~,`,^
            {
                sErrMsg += sItemName + " format is invalid";
            }
            else if (isPassMinValueLimit(sNewValue, drRow)) // validasi max value
            {
                sErrMsg += sItemName + " exceed minimum value (Min. Value is " + drRow["vMinValue"].ToString() + ")";
            }
            else if (isPassMaxValueLimit(sNewValue, drRow)) // validasi max value
            {
                sErrMsg += sItemName + " exceed maximum value (Max. Value is " + drRow["vMaxValue"].ToString() + ")";
            }
            else
                return true;
            return false;
        }

        protected bool isPassMinValueLimit(string sText, DataRow drRow)
        {
            return drRow["vType"].ToString().ToUpper() == "N" && drRow["vMinValue"].ToString() != "0" &&
                        int.Parse(sText) < int.Parse(drRow["vMinValue"].ToString());
        }

        protected bool isNullorEmpty(string sText, DataRow drRow)
        {
            return string.IsNullOrEmpty(sText.Trim()) && drRow["vAllowNull"].ToString().ToLower() == "false";
        }

        protected bool isNotPassMinLengthLimit(string sText, DataRow drRow)
        {
            if (drRow["vAllowNull"].ToString().ToLower() == "true" && string.IsNullOrEmpty(sText.Trim())) return false;
            else return sText.Length < int.Parse(drRow["vMinLength"].ToString());
        }

        protected bool isFormatNotValid(string sText, DataRow drRow)
        {
            if (string.IsNullOrEmpty(sText.Trim()) && drRow["vAllowNull"].ToString().ToLower() == "true") return false;
            else return !CommonClass.isFormatValid(sText, drRow["vType"].ToString());
        }

        protected bool isPassMaxValueLimit(string sText, DataRow drRow)
        {
            return drRow["vType"].ToString().ToUpper() == "N" && drRow["vMaxValue"].ToString() != "0" &&
                        int.Parse(sText) > int.Parse(drRow["vMaxValue"].ToString());
        }

        private void InitTerminalID()
        {
            ltTerminalId = new List<string>();
            using (StreamReader sr = new StreamReader(sFilename))
            {
                string sTerminalID;
                while ((sTerminalID = sr.ReadLine()) != null)
                    ltTerminalId.Add(sTerminalID);
            }
        }

        private void bwUpdateParam_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (string sTerminalID in ltTerminalId)
            {
                string sMessage = null;
                try
                {
                    if (IsValidTerminalSource(sTerminalID))
                    {
                        UpdateParamValue(sTerminalID);
                        UpdateParamLastUpdate(sTerminalID);
                        bool sAutoInitFtps= bool.Parse(ConfigurationManager.AppSettings["AutoInitFtps"]);
                        if (sAutoInitFtps == true)
                        {
                            ServiceInSysAutoInitFtps.ServiceInSysClient serviceInSys = new ServiceInSysAutoInitFtps.ServiceInSysClient();
                            serviceInSys.ExportToProfile(sTerminalID);
                        }
                        sMessage = string.Format("Update '{0}' SUCCESS, '{1}' = '{2}'", sTerminalID, sTagName, sNewValue);
                        CommonClass.InputLog(oSqlConn, "", sUserID, "", sMessage, sMessage);
                    }
                    else sMessage = string.Format("{0} FAILED", sTerminalID);
                }
                catch (SqlException sqlex)
                {
                    sMessage = string.Format("{0} FAILED : {1}", sTerminalID, sqlex);
                }
                bwUpdateParam.ReportProgress(50, sMessage);
            }
        }

        protected void UpdateParamValue(string sTerminalID)
        {
            string sQuery = null;
            switch (sFormTypeID)
            {
                case "1":
                    sQuery = string.Format("UPDATE tbProfileTerminal SET TerminalTagValue='{0}', TerminalTagLength={1}", sNewValue, sNewValue.Length);
                    sQuery = string.Format("{0} WHERE TerminalTag='{1}'", sQuery, sTag);
                    break;
                case "2":
                    sQuery = string.Format("UPDATE tbProfileAcquirer SET AcquirerTagValue='{0}', AcquirerTagLength={1}", sNewValue, sNewValue.Length);
                    sQuery = string.Format("{0} WHERE AcquirerTag='{1}'", sQuery, sTag);
                    if(!string.IsNullOrEmpty(sNameFilter))
                        sQuery = string.Format("{0} AND AcquirerName='{1}'", sQuery, sNameFilter);
                    break;
                case "3":
                    sQuery = string.Format("UPDATE tbProfileIssuer SET IssuerTagValue='{0}', IssuerTagLength={1}", sNewValue, sNewValue.Length);
                    sQuery = string.Format("{0} WHERE IssuerTag='{1}'", sQuery, sTag);
                    if (!string.IsNullOrEmpty(sNameFilter))
                        sQuery = string.Format("{0} AND IssuerName='{1}'", sQuery, sNameFilter);
                    break;
            }
            sQuery = string.Format("{0} AND TerminalID='{1}'", sQuery, sTerminalID);
            sQuery = string.Format("{0} UPDATE tbProfileTerminalList SET AllowDownload = 1, MaxInitRetry = {1} WHERE TerminalID='{2}'", sQuery, 1, sTerminalID);
            
            using (SqlCommand sqlcommand = new SqlCommand(sQuery, oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                try
                {
                    sqlcommand.ExecuteNonQuery();
                }
                catch(Exception ex)
                {
                    
                }
            }
        }

        protected void UpdateParamLastUpdate(string sTerminalID)
        {
            using (SqlCommand sqlcommand = new SqlCommand("spProfileTerminalUpdateLastUpdate", oSqlConn))
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                sqlcommand.CommandType = CommandType.StoredProcedure;
                sqlcommand.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalID;
                sqlcommand.ExecuteNonQuery();
            }
        }

        private void bwUpdateParam_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 50)
                rtbProgress.Text += string.Format("[{0:dd MMM yyyy, hh:mm:ss.fff tt}] {1}\n", DateTime.Now, e.UserState.ToString());
            rtbProgress.SelectionStart = rtbProgress.Text.Length;
            rtbProgress.ScrollToCaret();
        }

        private void bwUpdateParam_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SetDisplay(true);
            txtNewValue.Text = "";
                txtNameFilter.Text = "";
            try
            {
                cmbValue.SelectedIndex = -1;
            }
            catch
            {

            }
            cmbValue.Enabled = true;
           

        }

        private bool IsValidTerminalSource(string sTerminalID)
        {
            int iDbTerminal;
            using (SqlCommand cmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value = string.Format(" WHERE TerminalID='{0}'", sTerminalID);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        iDbTerminal = int.Parse(reader["DatabaseID"].ToString());
                    }
                    else
                        return false;
                }
            }
            if (iDbTerminal == iDatabaseIdSource) return true;
            else return false;
        }

        private void cmbItemList_SelectedIndexChanged(object sender, EventArgs e)
        {
             
        }

        private void cmbItemList_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbDbSource.DisplayMember) && !string.IsNullOrEmpty(cmbDbSource.ValueMember))
                if (cmbDbSource.Items.Count > 0)
                    if (cmbFormType.Text != "")
                        if (cmbItemList.Text != "")
                        {
                            if (GetObjectCmb() == true)
                            {
                                if (dtName.Rows.Count > 0)
                                {
                                    //txtNameFilter.Enabled = false;
                                    txtNewValue.Enabled = false;
                                    //cmbName.Enabled = true;
                                    cmbValue.Enabled = true;
                                    cmbName.DataSource = dtName;
                                    cmbName.DisplayMember = "DisplayValue";
                                    cmbName.ValueMember = "Realvalue";
                                    cmbValue.DataSource = dtValue;
                                    cmbValue.DisplayMember = "DisplayValue";
                                    cmbValue.ValueMember = "Realvalue";
                                    bValidObjectCmb = true;
                                }
                               
                            }

                            else
                            {
                                txtNameFilter.Enabled = true;
                                txtNewValue.Enabled = true;
                                cmbValue.Text = "";
                                cmbValue.Enabled = false;
                            }
                        }
            
        }

        private bool GetObjectCmb()
        {
            bool bObjValid = false;
            string s = cmbItemList.Text;
            string sQuery = string.Format("SELECT ItemSequence, objectID From tbItemList WHere DatabaseID = {0} AND FormID={1} AND ItemName = {2}", sGetDbId(), sFormTypeID, "'" + cmbItemList.Text + "'");
            SqlCommand cmd = new SqlCommand(sQuery, oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            try
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        reader.Read();
                        sItemSequence = reader["ItemSequence"].ToString();
                        sObjectID = reader["objectID"].ToString();
                        if (sObjectID == "4")
                        {
                            bObjValid = true;
                            reader.Dispose();
                            reader.Close();
                            dtName = GetItemSequence();
                            dtValue = dtName.Copy();
                            reader.Close();
                            reader.Dispose();
                        }
                        else
                        {
                            bObjValid = false;
                        }
                    }
                    else
                    {
                        reader.Close();
                        reader.Dispose();
                        bObjValid = false;
                    }
                }
                return bObjValid;
            }
            catch
            {
                bObjValid = false;
                return bObjValid;
            }
        }

        private bool isGetObjectID()
        {
            bool bvalid = false;
            string sQuery = string.Format("SELECT ObjectID From tbItemList WHere DatabaseID = {0} AND FormID={1} AND ItemName {2}", sGetDbId(), sFormTypeID, "'" + cmbItemList.Text + "'");
            return bvalid;
        }

        private DataTable GetItemSequence()
        {
            DataTable dt = new DataTable();
            string sQuerys = string.Format("SELECT DisplayValue, RealValue From tbItemComboBoxValue WHere DatabaseID = {0} AND FormID={1} AND  ItemSequence = {2}", sGetDbId(), sFormTypeID, sItemSequence);
            SqlCommand oSqlCmdCmb = new SqlCommand(sQuerys, oSqlConn);
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oReadCmb = oSqlCmdCmb.ExecuteReader();
            if (oReadCmb.HasRows)
                dt.Load(oReadCmb);
                oReadCmb.Close();
                oReadCmb.Dispose();
                return dt;
        }
    }
}
