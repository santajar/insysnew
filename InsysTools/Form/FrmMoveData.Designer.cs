﻿namespace InsysTools
{
    partial class FrmMoveData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbButton = new System.Windows.Forms.GroupBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.pbMoveData = new System.Windows.Forms.ProgressBar();
            this.gbDestination = new System.Windows.Forms.GroupBox();
            this.cmbDbDestination = new System.Windows.Forms.ComboBox();
            this.lblDbDestination = new System.Windows.Forms.Label();
            this.gbSource = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbDbSource = new System.Windows.Forms.ComboBox();
            this.lblDbSource = new System.Windows.Forms.Label();
            this.bwMoveData = new System.ComponentModel.BackgroundWorker();
            this.rtbProgress = new System.Windows.Forms.TextBox();
            this.gbButton.SuspendLayout();
            this.gbDestination.SuspendLayout();
            this.gbSource.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbButton
            // 
            this.gbButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbButton.Controls.Add(this.btnReset);
            this.gbButton.Controls.Add(this.btnClose);
            this.gbButton.Controls.Add(this.btnExecute);
            this.gbButton.Location = new System.Drawing.Point(12, 346);
            this.gbButton.Name = "gbButton";
            this.gbButton.Size = new System.Drawing.Size(642, 41);
            this.gbButton.TabIndex = 8;
            this.gbButton.TabStop = false;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Location = new System.Drawing.Point(426, 12);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(100, 23);
            this.btnReset.TabIndex = 1;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(536, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnExecute
            // 
            this.btnExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExecute.Location = new System.Drawing.Point(314, 12);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(100, 23);
            this.btnExecute.TabIndex = 0;
            this.btnExecute.Text = "&Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // pbMoveData
            // 
            this.pbMoveData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbMoveData.Location = new System.Drawing.Point(12, 322);
            this.pbMoveData.Name = "pbMoveData";
            this.pbMoveData.Size = new System.Drawing.Size(642, 23);
            this.pbMoveData.TabIndex = 1;
            // 
            // gbDestination
            // 
            this.gbDestination.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDestination.Controls.Add(this.cmbDbDestination);
            this.gbDestination.Controls.Add(this.lblDbDestination);
            this.gbDestination.Location = new System.Drawing.Point(11, 124);
            this.gbDestination.Name = "gbDestination";
            this.gbDestination.Size = new System.Drawing.Size(644, 55);
            this.gbDestination.TabIndex = 1;
            this.gbDestination.TabStop = false;
            this.gbDestination.Text = "Destination";
            // 
            // cmbDbDestination
            // 
            this.cmbDbDestination.FormattingEnabled = true;
            this.cmbDbDestination.Location = new System.Drawing.Point(97, 23);
            this.cmbDbDestination.Name = "cmbDbDestination";
            this.cmbDbDestination.Size = new System.Drawing.Size(160, 21);
            this.cmbDbDestination.TabIndex = 0;
            // 
            // lblDbDestination
            // 
            this.lblDbDestination.AutoSize = true;
            this.lblDbDestination.Location = new System.Drawing.Point(16, 27);
            this.lblDbDestination.Name = "lblDbDestination";
            this.lblDbDestination.Size = new System.Drawing.Size(53, 13);
            this.lblDbDestination.TabIndex = 0;
            this.lblDbDestination.Text = "Database";
            // 
            // gbSource
            // 
            this.gbSource.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSource.Controls.Add(this.btnBrowse);
            this.gbSource.Controls.Add(this.txtFilename);
            this.gbSource.Controls.Add(this.label1);
            this.gbSource.Controls.Add(this.cmbDbSource);
            this.gbSource.Controls.Add(this.lblDbSource);
            this.gbSource.Location = new System.Drawing.Point(11, 12);
            this.gbSource.Name = "gbSource";
            this.gbSource.Size = new System.Drawing.Size(644, 106);
            this.gbSource.TabIndex = 0;
            this.gbSource.TabStop = false;
            this.gbSource.Text = "Source";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(284, 66);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 20);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtFilename
            // 
            this.txtFilename.Location = new System.Drawing.Point(97, 66);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(160, 20);
            this.txtFilename.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Filename";
            // 
            // cmbDbSource
            // 
            this.cmbDbSource.FormattingEnabled = true;
            this.cmbDbSource.Location = new System.Drawing.Point(97, 24);
            this.cmbDbSource.Name = "cmbDbSource";
            this.cmbDbSource.Size = new System.Drawing.Size(160, 21);
            this.cmbDbSource.TabIndex = 0;
            // 
            // lblDbSource
            // 
            this.lblDbSource.AutoSize = true;
            this.lblDbSource.Location = new System.Drawing.Point(16, 27);
            this.lblDbSource.Name = "lblDbSource";
            this.lblDbSource.Size = new System.Drawing.Size(53, 13);
            this.lblDbSource.TabIndex = 0;
            this.lblDbSource.Text = "Database";
            // 
            // bwMoveData
            // 
            this.bwMoveData.WorkerReportsProgress = true;
            this.bwMoveData.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwMoveData_DoWork);
            this.bwMoveData.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwMoveData_ProgressChanged);
            this.bwMoveData.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwMoveData_RunWorkerCompleted);
            // 
            // rtbProgress
            // 
            this.rtbProgress.BackColor = System.Drawing.SystemColors.Window;
            this.rtbProgress.Location = new System.Drawing.Point(11, 185);
            this.rtbProgress.Multiline = true;
            this.rtbProgress.Name = "rtbProgress";
            this.rtbProgress.ReadOnly = true;
            this.rtbProgress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rtbProgress.Size = new System.Drawing.Size(637, 131);
            this.rtbProgress.TabIndex = 9;
            // 
            // FrmMoveData
            // 
            this.AcceptButton = this.btnExecute;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(673, 395);
            this.Controls.Add(this.rtbProgress);
            this.Controls.Add(this.gbButton);
            this.Controls.Add(this.pbMoveData);
            this.Controls.Add(this.gbDestination);
            this.Controls.Add(this.gbSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMoveData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Move Profile";
            this.Load += new System.EventHandler(this.FrmMoveData_Load);
            this.gbButton.ResumeLayout(false);
            this.gbDestination.ResumeLayout(false);
            this.gbDestination.PerformLayout();
            this.gbSource.ResumeLayout(false);
            this.gbSource.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbButton;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.ProgressBar pbMoveData;
        private System.Windows.Forms.GroupBox gbDestination;
        private System.Windows.Forms.ComboBox cmbDbDestination;
        private System.Windows.Forms.Label lblDbDestination;
        private System.Windows.Forms.GroupBox gbSource;
        private System.Windows.Forms.ComboBox cmbDbSource;
        private System.Windows.Forms.Label lblDbSource;
        private System.ComponentModel.BackgroundWorker bwMoveData;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TextBox rtbProgress;
    }
}