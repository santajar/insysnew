﻿namespace InsysTools
{
    partial class FrmOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOption));
            this.btnRD = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnMasking = new System.Windows.Forms.Button();
            this.btnUploadPTD = new System.Windows.Forms.Button();
            this.btnUploadEMS = new System.Windows.Forms.Button();
            this.btnUploadTIDRD = new System.Windows.Forms.Button();
            this.btnToggling = new System.Windows.Forms.Button();
            this.btnReplceTID = new System.Windows.Forms.Button();
            this.btnMoveData = new System.Windows.Forms.Button();
            this.btnQCEMS = new System.Windows.Forms.Button();
            this.btnUpdateParam = new System.Windows.Forms.Button();
            this.btnUpdateConn = new System.Windows.Forms.Button();
            this.btnRD.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRD
            // 
            this.btnRD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRD.Controls.Add(this.groupBox3);
            this.btnRD.Location = new System.Drawing.Point(12, 23);
            this.btnRD.Name = "btnRD";
            this.btnRD.Size = new System.Drawing.Size(417, 254);
            this.btnRD.TabIndex = 0;
            this.btnRD.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnMasking);
            this.groupBox3.Controls.Add(this.btnUploadPTD);
            this.groupBox3.Controls.Add(this.btnUploadEMS);
            this.groupBox3.Controls.Add(this.btnUploadTIDRD);
            this.groupBox3.Controls.Add(this.btnToggling);
            this.groupBox3.Controls.Add(this.btnReplceTID);
            this.groupBox3.Controls.Add(this.btnMoveData);
            this.groupBox3.Controls.Add(this.btnQCEMS);
            this.groupBox3.Controls.Add(this.btnUpdateParam);
            this.groupBox3.Controls.Add(this.btnUpdateConn);
            this.groupBox3.Location = new System.Drawing.Point(30, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(381, 222);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "INSYS TOOLS";
            // 
            // btnMasking
            // 
            this.btnMasking.Location = new System.Drawing.Point(262, 19);
            this.btnMasking.Name = "btnMasking";
            this.btnMasking.Size = new System.Drawing.Size(113, 43);
            this.btnMasking.TabIndex = 9;
            this.btnMasking.Text = "MASKING";
            this.btnMasking.UseVisualStyleBackColor = true;
            this.btnMasking.Click += new System.EventHandler(this.btnMasking_Click);
            // 
            // btnUploadPTD
            // 
            this.btnUploadPTD.Location = new System.Drawing.Point(262, 68);
            this.btnUploadPTD.Name = "btnUploadPTD";
            this.btnUploadPTD.Size = new System.Drawing.Size(113, 43);
            this.btnUploadPTD.TabIndex = 8;
            this.btnUploadPTD.Text = "UPLOAD PTD";
            this.btnUploadPTD.UseVisualStyleBackColor = true;
            this.btnUploadPTD.Visible = false;
            this.btnUploadPTD.Click += new System.EventHandler(this.btnUploadPTD_Click_1);
            // 
            // btnUploadEMS
            // 
            this.btnUploadEMS.Location = new System.Drawing.Point(134, 117);
            this.btnUploadEMS.Name = "btnUploadEMS";
            this.btnUploadEMS.Size = new System.Drawing.Size(122, 43);
            this.btnUploadEMS.TabIndex = 7;
            this.btnUploadEMS.Text = "UPLOAD EMS";
            this.btnUploadEMS.UseVisualStyleBackColor = true;
            this.btnUploadEMS.Click += new System.EventHandler(this.btnUploadEMS_Click);
            // 
            // btnUploadTIDRD
            // 
            this.btnUploadTIDRD.Location = new System.Drawing.Point(134, 68);
            this.btnUploadTIDRD.Name = "btnUploadTIDRD";
            this.btnUploadTIDRD.Size = new System.Drawing.Size(122, 43);
            this.btnUploadTIDRD.TabIndex = 6;
            this.btnUploadTIDRD.Text = "UPLOAD TID RD\r\n";
            this.btnUploadTIDRD.UseVisualStyleBackColor = true;
            this.btnUploadTIDRD.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnToggling
            // 
            this.btnToggling.Location = new System.Drawing.Point(6, 117);
            this.btnToggling.Name = "btnToggling";
            this.btnToggling.Size = new System.Drawing.Size(122, 43);
            this.btnToggling.TabIndex = 6;
            this.btnToggling.Text = "Open Toggling";
            this.btnToggling.UseVisualStyleBackColor = true;
            this.btnToggling.Click += new System.EventHandler(this.btnToggling_Click);
            // 
            // btnReplceTID
            // 
            this.btnReplceTID.Location = new System.Drawing.Point(6, 68);
            this.btnReplceTID.Name = "btnReplceTID";
            this.btnReplceTID.Size = new System.Drawing.Size(122, 43);
            this.btnReplceTID.TabIndex = 4;
            this.btnReplceTID.Text = "REPLACE TID";
            this.btnReplceTID.UseVisualStyleBackColor = true;
            this.btnReplceTID.Click += new System.EventHandler(this.btnReplceTID_Click);
            // 
            // btnMoveData
            // 
            this.btnMoveData.Location = new System.Drawing.Point(134, 19);
            this.btnMoveData.Name = "btnMoveData";
            this.btnMoveData.Size = new System.Drawing.Size(122, 43);
            this.btnMoveData.TabIndex = 0;
            this.btnMoveData.Text = "MOVE TID";
            this.btnMoveData.UseVisualStyleBackColor = true;
            this.btnMoveData.Click += new System.EventHandler(this.btnMoveData_Click);
            // 
            // btnQCEMS
            // 
            this.btnQCEMS.Location = new System.Drawing.Point(6, 166);
            this.btnQCEMS.Name = "btnQCEMS";
            this.btnQCEMS.Size = new System.Drawing.Size(122, 43);
            this.btnQCEMS.TabIndex = 1;
            this.btnQCEMS.Text = "QC EMS";
            this.btnQCEMS.UseVisualStyleBackColor = true;
            this.btnQCEMS.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnUpdateParam
            // 
            this.btnUpdateParam.Location = new System.Drawing.Point(134, 166);
            this.btnUpdateParam.Name = "btnUpdateParam";
            this.btnUpdateParam.Size = new System.Drawing.Size(122, 43);
            this.btnUpdateParam.TabIndex = 2;
            this.btnUpdateParam.Text = "UPDATE PARAMETER";
            this.btnUpdateParam.UseVisualStyleBackColor = true;
            this.btnUpdateParam.Click += new System.EventHandler(this.btnUpdateParam_Click);
            // 
            // btnUpdateConn
            // 
            this.btnUpdateConn.Location = new System.Drawing.Point(6, 19);
            this.btnUpdateConn.Name = "btnUpdateConn";
            this.btnUpdateConn.Size = new System.Drawing.Size(122, 43);
            this.btnUpdateConn.TabIndex = 1;
            this.btnUpdateConn.Text = "UPDATE CONNECTION";
            this.btnUpdateConn.UseVisualStyleBackColor = true;
            this.btnUpdateConn.Click += new System.EventHandler(this.btnUpdateConn_Click);
            // 
            // FrmOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 307);
            this.Controls.Add(this.btnRD);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOption";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InSys Tools";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmOption_FormClosed);
            this.Load += new System.EventHandler(this.FrmOption_Load);
            this.btnRD.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox btnRD;
        private System.Windows.Forms.Button btnUpdateConn;
        private System.Windows.Forms.Button btnMoveData;
        private System.Windows.Forms.Button btnUpdateParam;
        private System.Windows.Forms.Button btnQCEMS;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnReplceTID;
        private System.Windows.Forms.Button btnToggling;
        private System.Windows.Forms.Button btnUploadTIDRD;
        private System.Windows.Forms.Button btnUploadEMS;
        private System.Windows.Forms.Button btnUploadPTD;
        private System.Windows.Forms.Button btnMasking;
    }
}