using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using Threading = System.Threading;
using ExcelOffice = Microsoft.Office.Interop.Excel;
using InSysClass;
using System.Configuration;
using ipXML;
using Excel;

namespace InsysTools
{
    public partial class FrmUploadDataEms : Form
    {
        private readonly CultureInfo oCultInfo = Threading.Thread.CurrentThread.CurrentCulture;
        //Needed to store original culture, and then restore it later.

        //Reference tables. Needed to validate source Excel file when uploading to database.
        private DataTable oTableReferenceDE = new DataTable();  //Filled at form load. used at upload time.
        private DataTable oTableReferenceAA = new DataTable();  //Filled at form load. used at upload time.

        //Reference columns. Needed as looping counter when uploading source Excel file to database.
        private List<string> sColumnsDE = new List<string>();   //Filled at form load. used at upload time.
        private List<string> sColumnsAA = new List<string>();   //Filled at form load. used at upload time.

        private SqlConnection oSqlConn;
        static string sConnstring;

        private string sUserID;
        private SqlConnection oSqlConnAuditTrail;
        private string sConnstringAuditTrail;
        static string sAppDirectory = Directory.GetCurrentDirectory();

        //private Excel._Application oXLapp;   //Excel object to be used throughout process.
        //private Excel._Workbook oXLbook;     //Excel object to be used throughout process.
        //private Excel._Worksheet oXLsheet;   //Excel object to be used throughout process.
        //private Excel.Range oXLrange;       //Excel object to be used throughout process.

        private int iCells = 1; //Needed as a counter variable cross thread.
        private int iStartRowExcel;  //Retrieved at form load. used at upload time.

        public string sFile;

        DataTable dtTemplate = new DataTable();
        DataTable dtExcelUpload = new DataTable();

        enum UploadMode
        {
            Add=1,
            Update,
            Delete,
        }

        UploadMode modeUpload;

        /// <summary>
        /// Initialize the SQL Server connection
        /// </summary>
        /// <returns>SqlConnection : object sqlconnection</returns>
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnstring = oObjInit.sGetConnString();
                sConnstring = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnstring);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
                //Logs.doWriteErrorFile("[MAIN] InitConn : " + ex.Message);
            }
            //oSqlConn = oSqlTempConn;
            return oSqlTempConn;
        }
        public FrmUploadDataEms(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail, string _sUserID)
        {
            InitializeComponent();

            oSqlConn = _oSqlConn;
            sUserID = _sUserID;
            //sConnstring = oSqlConn.ConnectionString.ToString();
            //sConnstring = InitConnection();
            sUserID = _sUserID;
        }

        //private void doLogin()
        //{
        //    FrmLogin frmLogin = new FrmLogin();
        //    frmLogin.ShowDialog();
        //    if (!frmLogin.isCanLoginToSystem) 
        //        Application.ExitThread();
        //    else
        //    {
        //        UserData.sUserID = frmLogin.sGetUserID();
        //        UserData.sUserName = frmLogin.sGetUserName();
        //        UserData.sPassword = CommonClass.sGetEncrypt(frmLogin.sGetPassword());
        //        UserData.isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
        //        UserData.sGroupID = frmLogin.sGetGroupID();

        //        sConnstring = frmLogin.sConnString;
        //        oSqlConn = new SqlConnection(sConnstring);
        //        oSqlConn.Open();

        //        sConnstringAuditTrail = frmLogin.sConnStringAuditTrail;
        //        oSqlConnAuditTrail = new SqlConnection(sConnstringAuditTrail);
        //        oSqlConnAuditTrail.Open();

        //        //WriteLog(CommonMessage.sLoginSucccess);
        //    }
        //}

        /// <summary>
        /// Write status to Audit Trail Logs, set window's title bar and current user.
        /// </summary>
        /// <param name="sLog">string : Logs description to inserted to Audit Trail Logs</param>
        protected void WriteLog(string sLog)
        {
            CommonClass.InputLog(oSqlConn, "", string.IsNullOrEmpty(UserData.sUserID) ? "" : UserData.sUserID, "", sLog, "");
            //CommonClass.InputLog(oSqlConnAuditTrail, "", string.IsNullOrEmpty(UserData.sUserID) ? "" : UserData.sUserID, "", sLog, "");
        }

        /// <summary>
        /// Determines wheter have configuration or not.
        /// </summary>
        /// <returns>boolean : true if configuration found, else false</returns>
        protected bool isHaveConfiguration()
        {
            string sActiveDir = Application.StartupPath;
            string sFileName = sActiveDir + "\\Application.conf";

            if (File.Exists(sFileName)) return true;
            else return false;
        }

        ///// <summary>
        ///// Form to upload data to Excel
        ///// </summary>
        ///// <param name="_oSqlConn">SqlConnection : Connection string to database</param>
        //public FrmUploadDataEms(SqlConnection _oSqlConn, SqlConnection _oSqlConnAuditTrail)
        //{
        //    this.oSqlConn = _oSqlConn;
        //    oSqlConnAuditTrail = _oSqlConnAuditTrail;
        //    InitializeComponent();
        //}

        /// <summary>
        /// Initializes all the datas on this form load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void FrmUploadDataEms_Load(object sender, EventArgs e)
        {
            try
            {
                //if (isHaveConfiguration())

                //{
                //    //doLogin();
                //}
                //else Application.Exit();

                // fillcomboboxTemplate();

                // add tatang for version 5. avocado  
                string version = ConfigurationManager.AppSettings["Version"];
                if (rbYes.Checked)
                    dtTemplate = dtGetEmsXmlMap(version);
                else
                {
                    string sTemplate = ConfigurationManager.AppSettings["Template"];
                    if (!string.IsNullOrEmpty(sTemplate))
                        dtTemplate = CommonLib.ConvertCSVtoDataTable(sTemplate, ',');
                }

                InitData();
                CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", CommonMessage.sFormOpened + "Upload", "");
                //CommonClass.InputLog(oSqlConnAuditTrail, "", UserData.sUserID, "", CommonMessage.sFormOpened + "Upload", "");

            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void fillcomboboxTemplate()
        {
            string sTemplate = ConfigurationManager.AppSettings["Template"];
            if (!string.IsNullOrEmpty(sTemplate))
                dtTemplate = CommonLib.ConvertCSVtoDataTable(sTemplate, ',');
        }

        /// <summary>
        /// Releases all unnecessary datas on this form closing event.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void FrmUploadDataEms_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                DisposeData();
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Dispose();
            }
        }

        /// <summary>
        /// Enables Upload Button if the specified source file exists.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSourceFile_TextChanged(object sender, EventArgs e)
        {
            btnUpload.Enabled = File.Exists(txtSourceFile.Text);
        }

        /// <summary>
        /// Shows the full path of the source file on mouse enter.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void txtSourceFile_MouseEnter(object sender, EventArgs e)
        {
            tipUpload.Show(txtSourceFile.Text, txtSourceFile);
        }

        /// <summary>
        /// Hides the tooltip on mouse leave.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void txtSourceFile_MouseLeave(object sender, EventArgs e)
        {
            tipUpload.Hide(txtSourceFile);
        }

        /// <summary>
        /// Shows a dialog box to choose a source file.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            ofdUpload.InitialDirectory = Application.StartupPath;
            if (ofdUpload.ShowDialog() == DialogResult.OK)
            {
                this.Cursor = CommonClass.DisplayWaitCursor(true);
                txtSourceFile.Text = ofdUpload.FileName;

                //OleDbConnection oOleConn = new OleDbConnection(OleDBConnLib.sConnACE12(txtSourceFile.Text, ""));
                //oOleConn.Open();
                //new OleDbDataAdapter(new OleDbCommand("SELECT * FROM [Sheet1$]", oOleConn)).Fill(dtExcelUpload);
                //oOleConn.Close();

                FileStream stream = File.Open(txtSourceFile.Text, FileMode.Open, FileAccess.Read);
                //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                //IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                //...
                //3. DataSet - The result of each spreadsheet will be created in the result.Tables
                DataSet resultSet = excelReader.AsDataSet();
                //...
                //4. DataSet - Create column names from first row
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet result = excelReader.AsDataSet();

                dtExcelUpload = result.Tables[0];

                //XSSFWorkbook wb;
                //XSSFSheet sh;
                //String Sheet_name;

                //using (var fs = new FileStream(Path, FileMode.Open, FileAccess.Read))
                //{
                //    wb = new XSSFWorkbook(fs);

                //    Sheet_name = wb.GetSheetAt(0).SheetName;  //get first sheet name
                //}
                //DataTable DT = new DataTable();
                //DT.Rows.Clear();
                //DT.Columns.Clear();

                //// get sheet
                //sh = (XSSFSheet)wb.GetSheet(Sheet_name);

                //add value ditukar 190304 tatang for tdm
                dtExcelUpload.Columns["Force_SettlementDay"].ColumnName = "Force_SettlementDayChecklist";
                dtExcelUpload.Columns["Force_SettlementDayvalues"].ColumnName = "Force_SettlementDay";
                //add untuk QRBCA jingin QRIS di TDM , namun di EMS masih ingin QRBCA 5.01
                string sVersionConfig = ConfigurationManager.AppSettings["Version"];
                if (sVersionConfig == "5.01")
                {
                    dtExcelUpload.Columns["Kota"].ColumnName = "city";
                    dtExcelUpload.Columns["Kode Pos"].ColumnName = "postalcode";
                }
                dtExcelUpload.AcceptChanges();
                //add value ditukar 190304 tatang for tdm

                this.Cursor = CommonClass.DisplayWaitCursor(false);
            }
        }

        /// <summary>
        /// Disables all the button so they won't interrupt the uploading process, and starts the uploading task.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                SetDisplay(false);
                wrkUpload.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile("ERROR, Upload : " + ex.Message);
            }
        }

        public static string sPTerminalID;

        /// <summary>
        /// Starts a new thread to begin the task.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void wrkUpload_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {                
                SetCultureInfo("en-US");
                //oDataUpload.StartUpload();
                //wrkUpload.ReportProgress(100, oDataUpload.LogMessage);

                StartEmsUpload();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile("ERROR : " + ex.Message + ex.StackTrace);
                wrkUpload.ReportProgress(100, "ERROR: " + ex.Message + ex.StackTrace);
                CommonClass.UserAccessDelete(oSqlConn, sPTerminalID);
            }
        }
        protected DataTable dtGetEmsXmlMap(string sVersion)
        {
            DataTable dtTemp = new DataTable();
            string sQuery = string.Format("SELECT * FROM tbEmsMsXmlMap WITH(NOLOCK) WHERE EMSXMLVersion = '{0}' ORDER BY EMSMapId"
                , sVersion
                );
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
                (new SqlDataAdapter(oCmd)).Fill(dtTemp);
            return dtTemp;
        }

        private void StartEmsUpload()
        {                
            foreach (DataRow rowExcel in dtExcelUpload.Rows)
            {
                DataTable dtTemp = dtExcelUpload.Clone();
                dtTemp.TableName = "Terminal";
                dtTemp.ImportRow(rowExcel);
                string sTerminalID = dtTemp.Rows[0]["Terminal_INIT"].ToString();
                sPTerminalID = sTerminalID;
                CommonClass.UserAccessDelete(oSqlConn, sTerminalID);

                List<DataTable> listTableEms = new List<DataTable>();
                listTableEms.Add(dtTemp);
                //listTableEms.Add(dtAttributeDefault()); old tutup tatang

                // add tatang for version 5. avocado        
                string sSoftware_Version5 = dtTemp.Rows[0]["Software_Version"].ToString();
                string sVersionConfig = ConfigurationManager.AppSettings["Version"];
                if ((sSoftware_Version5 == "AVOCADO" || sSoftware_Version5 == "AVOCADO_MOVE")&& rbYes.Checked)
                        listTableEms.Add(dtAttributeAVOCADO(sVersionConfig));
                    else listTableEms.Add(dtAttributeDefault());

                EMS_XML xmlEms = new EMS_XML(listTableEms);
                EMS_Process processEms = new EMS_Process(xmlEms, oSqlConn, UserData.sUserID);
                processEms.emsAddUpdate(dtTemplate, true);
                string sStatus = string.IsNullOrEmpty(processEms.Error) ? "SUCCESS" + " "+sSoftware_Version5 : processEms.Error;
                wrkUpload.ReportProgress(0, string.Format("{0}, {1}", sTerminalID, sStatus));
                CommonClass.doWritelogFile("Logs : " + string.Format("{0}, {1}", sTerminalID, sStatus));
            }
        }

        private DataTable dtAttributeDefault()
        {
            string[] arr = new string[] { "2.00", "ADD_UPDATE", "UPLOAD", string.Format("UPLOAD{0:yyMMddhhmmsstt}", DateTime.Now) };

            DataTable dtTempAttribute = new DataTable();
            dtTempAttribute.TableName = "Attribute";
            dtTempAttribute.Columns.Add("Version");
            dtTempAttribute.Columns.Add("Type");
            dtTempAttribute.Columns.Add("Sender");
            dtTempAttribute.Columns.Add("Sender_ID");
            dtTempAttribute.Rows.Add(arr);
            return dtTempAttribute;
        }


        // add tatang for avocado version 5
        private DataTable dtAttributeAVOCADO(string _sSoftware_Version5)
        {
            string[] arr = new string[] { _sSoftware_Version5, "ADD_UPDATE", "UPLOAD", string.Format("UPLOAD{0:yyMMddhhmmsstt}", DateTime.Now) };

            DataTable dtTempAttribute = new DataTable();
            dtTempAttribute.TableName = "Attribute";
            dtTempAttribute.Columns.Add("Version");
            dtTempAttribute.Columns.Add("Type");
            dtTempAttribute.Columns.Add("Sender");
            dtTempAttribute.Columns.Add("Sender_ID");
            dtTempAttribute.Rows.Add(arr);
            return dtTempAttribute;
        }
        /// <summary>
        /// Reports progress to the current thread.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void wrkUpload_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                DisplayStatus(e.UserState.ToString());
                if (e.ProgressPercentage == 100)
                {
                    barUploadProgress.Style = ProgressBarStyle.Blocks;
                    this.Cursor = Cursors.Default;
                    DisplayStatus("FINISHED");
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Sets display back to original after the process completes.
        /// </summary>
        /// <param name="sender">Object identifier</param>
        /// <param name="e">Event data</param>
        private void wrkUpload_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                SetDisplay(true);
                iCells = 1;
                //if (rbAdd.Checked == true)
                //{

                //    CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", "ADD Terminal Finish", "ADD Terminal Finish");
                //}
                //if (rbUpdate.Checked == true)
                //    CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", "UPDATE Terminal Finish", "UPDATE Terminal Finish");
                //if (rbDelete.Checked == true)
                //    CommonClass.InputLog(oSqlConn, "", UserData.sUserID, "", "DELETE Terminal Finish", "DELETE Terminal Finish");
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rbAdd_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAdd.Checked)
            {
                modeUpload = UploadMode.Add;
                rbUpdate.Checked = false;
                rbDelete.Checked = false;
            }
        }

        private void rbUpdate_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdate.Checked)
            {
                modeUpload = UploadMode.Update;
                rbAdd.Checked = false;
                rbDelete.Checked = false;
            }
        }

        private void rbDelete_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDelete.Checked)
            {
                modeUpload = UploadMode.Delete;
                rbUpdate.Checked = false;
                rbAdd.Checked = false;
            }
        }

        private void cmbTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTemplate.SelectedIndex > -1)
            {
                sFile = cmbTemplate.SelectedValue.ToString();
                
            }
        }

        #region Function
        /// <summary>
        /// Displays current status on the text box.
        /// </summary>
        /// <param name="sStatus"></param>
        private void DisplayStatus(string sStatus)
        {
            string[] arrsStatus = sStatus.Split('\n');
            foreach (string sTemp in arrsStatus)
                txtStatus.AppendText(sTemp + Environment.NewLine);
            txtStatus.SelectionStart = txtStatus.TextLength;
            txtStatus.ScrollToCaret();
        }

        /// <summary>
        /// Releases all the datas, such as Microsoft Excel service; Reverts current thread's Culture Info; Drop temporary database tables;
        /// </summary>
        private void DisposeData()
        {
            //ExcelEndSession();
            ResetCultureInfo();
            //DropDatabaseTables();
        }

        /// <summary>
        /// Starts Microsofr Excel service.
        /// </summary>
        /// <param name="sFileName">Filename</param>
        //private void ExcelStartSession(string sFileName)
        //{
        //    wrkUpload.ReportProgress(0, 
        //        string.Format("{0:MMM dd, yyyy hh:mm:ss tt} Opening Excel Application", DateTime.Now));
        //    oXLapp = new Excel.Application();

        //    wrkUpload.ReportProgress(0, 
        //        string.Format("{0:MMM dd, yyyy hh:mm:ss tt} Opening Excel Workbook", DateTime.Now));
        //    oXLbook = oXLapp.Workbooks.Open(
        //        sFileName, 0, true, 5, "",
        //        "", true, Excel.XlPlatform.xlWindows, "\t", false,
        //        false, 0, true, 1, 0);

        //    wrkUpload.ReportProgress(0, 
        //        string.Format("{0:MMM dd, yyyy hh:mm:ss tt} Opening Excel Worksheet", DateTime.Now));
        //    oXLsheet = (Excel._Worksheet)oXLbook.Worksheets.get_Item(1);

        //    wrkUpload.ReportProgress(0, 
        //        string.Format("{0:MMM dd, yyyy hh:mm:ss tt} Retrieving Excel used cells", DateTime.Now));
        //    oXLrange = oXLsheet.UsedRange;
        //}

        /// <summary>
        /// Ends Microsoft Excel service.
        /// </summary>
        //private void ExcelEndSession()
        //{
        //    if (oXLapp != null && oXLbook != null && oXLrange != null && oXLsheet != null)
        //    {
        //        try
        //        {
        //            oXLbook.Close(false, null, null);
        //            oXLapp.Quit();

        //            DisposeExcelObj(oXLsheet);
        //            DisposeExcelObj(oXLbook);
        //            DisposeExcelObj(oXLapp);
        //        }
        //        catch
        //        {
        //            DisposeExcelObj(oXLsheet);
        //            DisposeExcelObj(oXLbook);
        //            DisposeExcelObj(oXLapp);
        //        }
        //    }
        //}

        /// <summary>
        /// Releases Microsoft Excel service objects.
        /// </summary>
        /// <param name="oExcelObj">Excel object</param>
        private void DisposeExcelObj(object oExcelObj)
        {
            try
            {
                Marshal.ReleaseComObject(oExcelObj);
                oExcelObj = null;
            }
            catch (Exception oException)
            {
                CommonClass.doWriteErrorFile(oException.ToString());
                oExcelObj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        /// <summary>
        /// Sets current thread's Culture Info to a desired one.
        /// </summary>
        /// <param name="sCultName">Culture name</param>
        /// <returns>The new Culture Info</returns>
        private CultureInfo SetCultureInfo(string sCultName)
        {
            return Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(sCultName);
        }

        /// <summary>
        /// Reverts current's thread Culture Info to the original Culture Info before it's changed.
        /// </summary>
        /// <returns>The original Culture Info</returns>
        private CultureInfo ResetCultureInfo()
        {
            return Threading.Thread.CurrentThread.CurrentCulture = oCultInfo;
        }

        /// <summary>
        /// Initializes datas, such as Culture Info, Database Map Configuration, and Local Variables.
        /// </summary>
        private void InitData()
        {
            SetCultureInfo("en-US");
            //InitMapConfig();
            InitLocals();
        }

        /// <summary>
        /// Retrieves starting row form database.
        /// </summary>
        private void InitStartRow()
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPUploadControlGet, oSqlConn);

            iStartRowExcel = Convert.ToInt16(oSqlComm.ExecuteScalar());
        }

        /// <summary>
        /// Clears all columns format.
        /// </summary>
        private void InitColumnList()
        {
            sColumnsDE.Clear();
            sColumnsAA.Clear();
        }

        /// <summary>
        /// Clears text box.
        /// </summary>
        private void InitTextBox()
        {
            txtStatus.Text = "";
        }

        /// <summary>
        /// Initializes local variables.
        /// </summary>
        private void InitLocals()
        {
            InitTextBox();
            InitStartRow();
            //InitColumnList();
        }

        /// <summary>
        /// Initializes database map configuration.
        /// </summary>
        private void InitMapConfig()
        {
            DropDatabaseTables();//reset any un-reset table(s) (if any)
            CreateDatabaseTables();
            FillMapTables();
        }

        /// <summary>
        /// Drops a table in database.
        /// </summary>
        /// <param name="sTableName">Table name</param>
        private void DropTable(string sTableName)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPDataUploadDropTable, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;

            oSqlComm.Parameters.Add("@sTableName", SqlDbType.VarChar).Value = sTableName;

            oSqlComm.ExecuteNonQuery();
        }

        /// <summary>
        /// Drops all unrequired database tables.
        /// </summary>
        private void DropDatabaseTables()
        {
            DropTable(sGetTableName("DE"));
            DropTable(sGetTableName("AA"));
        }

        /// <summary>
        /// Creates required database tables.
        /// </summary>
        private void CreateDatabaseTables()
        {
            CreateTable("DE");
            CreateTable("AA");
        }

        /// <summary>
        /// Generates table name for the current user.
        /// </summary>
        /// <param name="sTag">Tag (DE/AA)</param>
        /// <returns>Table name</returns>
        private string sGetTableName(string sTag)
        {
            MessageBox.Show(UserData.sUserID);
            return "tbUpload" + UserData.sUserID + sTag;
        }

        /// <summary>
        /// Retrieves column collection.
        /// </summary>
        /// <param name="sTag">Tag (DE/AA)</param>
        /// <returns>Column collection</returns>
        private string sGetColumnCollection(string sTag)
        {
            DataTable oTable = new DataTable();
            string sColumn = "";

            if (oSqlConn.State != ConnectionState.Open)
            {
                oSqlConn.Close();
                oSqlConn.Open();
            }

            new SqlDataAdapter(new SqlCommand(
                sTag == "DE" ? CommonSP.sSPUploadGetColumnsDE :
                sTag == "AA" ? CommonSP.sSPUploadGetColumnsAA :
                string.Empty, oSqlConn)).Fill(oTable);

            if (sTag == "AA")
            {
                sColumnsAA.Add("TerminalID");
                sColumnsAA.Add("AcquirerName");
                sColumn += "[" + sColumnsAA[0] + "] [varchar](max) NULL, ";
                sColumn += "[" + sColumnsAA[1] + "] [varchar](max) NULL, ";
            }

            foreach (DataRow oRow in oTable.Rows)
            {
                sColumn += "[" + oRow[0].ToString() + "] [varchar](max) NULL, ";
                switch (sTag)
                {
                    case "DE":
                        sColumnsDE.Add(oRow[0].ToString());
                        break;
                    case "AA":
                        sColumnsAA.Add(oRow[0].ToString());
                        break;
                    default:
                        return null;
                }
            }

            return sColumn.Remove(sColumn.Length - 2);
        }

        /// <summary>
        /// Creates a table in the database.
        /// </summary>
        /// <param name="sTag">Tag (DE/AA)</param>
        private void CreateTable(string sTag)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPDataUploadCreateTable, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;

            oSqlComm.Parameters.Add("@sTableName", SqlDbType.VarChar).Value = sGetTableName(sTag);
            oSqlComm.Parameters.Add("@sColumnCollection", SqlDbType.VarChar).Value = sGetColumnCollection(sTag);

            oSqlComm.ExecuteNonQuery();
        }

        /// <summary>
        /// Populate database table.
        /// </summary>
        /// <param name="sStoredProcedure">Stored procedure name</param>
        /// <param name="oTable">Table name</param>
        private void FillTable(string sStoredProcedure, DataTable oTable)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            new SqlDataAdapter(new SqlCommand(sStoredProcedure, oSqlConn)).Fill(oTable);
        }

        /// <summary>
        /// Populates map tables.
        /// </summary>
        private void FillMapTables()
        {
            FillTable(CommonSP.sSPUploadTagDEBrowse, oTableReferenceDE);
            FillTable(CommonSP.sSPUploadTagAABrowse, oTableReferenceAA);
        }

        /// <summary>
        /// Erases upload profile.
        /// </summary>
        /// <param name="sTableName">Table name</param>
        private void ClearUploadProfile(string sTableName)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(CommonSP.sSPDataUploadDelete, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;

            oSqlComm.Parameters.Add("@sTableName", SqlDbType.VarChar).Value = sTableName;

            oSqlComm.ExecuteNonQuery();
        }

        /// <summary>
        /// Erases all upload profiles.
        /// </summary>
        private void ClearUploadProfiles()
        {
            ClearUploadProfile(sGetTableName("DE"));
            ClearUploadProfile(sGetTableName("AA"));
        }

        /// <summary>
        /// Generates DE (Terminal) table.
        /// </summary>
        /// <returns>DE (Terminal) table</returns>
        //private DataTable GenerateTableDE()
        //{
        //    DataTable oTable = new DataTable();
        //    object[] oCells = new object[oTableReferenceDE.Rows.Count];
        //    object oCell;

        //    foreach (DataRow oRow in oTableReferenceDE.Rows)
        //        oTable.Columns.Add(oRow["ColumnName"].ToString());

        //    for (int iRow = iStartRowExcel; iRow <= oXLrange.Rows.Count; iRow++)
        //    {
        //        for (int iCol = 1; iCol <= oCells.Length; iCol++)
        //        {
        //            if ((oCell = (oXLrange.Cells[iRow, Convert.ToInt16(oTableReferenceDE.Rows[iCol - 1]["SourceColumn"].ToString())] as Excel.Range).Value2) == null)
        //            {   // (iCol == 1) because Excel is dirty
        //                if (iCol == 1) return oTable;
        //                if (Convert.ToBoolean(oTableReferenceDE.Rows[iCol - 1]["Mandatory"].ToString()))
        //                {
        //                    object oCol = "";
        //                    if ((oCol = (oXLrange.Cells[iStartRowExcel > 1 ? iStartRowExcel - 1 : 1, Convert.ToInt16(oTableReferenceDE.Rows[iCol - 1]["SourceColumn"].ToString())] as Excel.Range).Value2) == null) oCol = "";
        //                    string sReport = "Column [" + oCol.ToString() + "] Line [" + iRow.ToString() + "] is mandatory.";
        //                    wrkUpload.ReportProgress(100, "ERROR DE : " + sReport);
        //                    MessageBox.Show(sReport, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        //                    return null;
        //                }
        //                else oCell = "";
        //            }
        //            oCells[iCol - 1] = oCell.ToString().ToUpper();
        //            wrkUpload.ReportProgress(0,
        //                string.Format("{0:MMM dd, yyyy hh:mm:ss tt} Processed {1} cells", DateTime.Now, (iCells++).ToString()));
        //        }
        //        oTable.Rows.Add(oCells);
        //    }
        //    return oTable;
        //}

        /// <summary>
        /// Generates AA Acquirer table.
        /// </summary>
        /// <returns>AA Acquirer table</returns>
        //private DataTable GenerateTableAA()
        //{
        //    DataTable oTable = new DataTable();
        //    object[] oCollCells = new object[sColumnsAA.Count];
        //    object oCell;

        //    foreach (string sColumnName in sColumnsAA)
        //        oTable.Columns.Add(sColumnName);
        //    // loop for each excel row
        //    for (int iRowExcel = iStartRowExcel, iRowAAIndex = 0; iRowExcel <= oXLrange.Rows.Count; iRowExcel++, iRowAAIndex = 0)
        //    {   // (...............[iRow, 1] because Excel is dirty........)
        //        if ((oXLrange.Cells[iRowExcel, 1] as Excel.Range).Value2 == null)
        //            return oTable;

        //        // get init TerminalId
        //        oCollCells[0] = (oXLrange.Cells[iRowExcel, Convert.ToInt16(oTableReferenceDE.Rows[0]["SourceColumn"].ToString())] as Excel.Range).Value2.ToString().ToUpper();
        //        // loop for each excel column
        //        for (int iColExcel = sColumnsDE.Count + 1; iColExcel - sColumnsDE.Count - 1 < oTableReferenceAA.Rows.Count; )
        //        {
        //            oCollCells[1] = oTableReferenceAA.Rows[iRowAAIndex]["AcquirerName"].ToString().ToUpper();
        //            for (int iIndexColumnsAA = 2; iIndexColumnsAA < sColumnsAA.Count; iIndexColumnsAA++)
        //            {
        //                if (iRowAAIndex >= oTableReferenceAA.Rows.Count ||
        //                    oTable.Columns[iIndexColumnsAA].ColumnName != oTableReferenceAA.Rows[iRowAAIndex]["ColumnName"].ToString())
        //                {
        //                    oCollCells[iIndexColumnsAA] = "";
        //                }
        //                else //(sColumnsAA[iIndexColumnsAA] == oTableReferenceAA.Rows[iRowAAIndex]["ColumnName"].ToString())
        //                {
        //                    if ((oCell = (oXLrange.Cells[iRowExcel, Convert.ToInt16(oTableReferenceAA.Rows[iRowAAIndex]["SourceColumn"].ToString())] as Excel.Range).Value2) == null)
        //                    {
        //                        if (Convert.ToBoolean(oTableReferenceAA.Rows[iRowAAIndex]["Mandatory"].ToString()))
        //                        {
        //                            object oCol = "";
        //                            if ((oCol = (oXLrange.Cells[iStartRowExcel > 1 ? iStartRowExcel - 1 : 1, Convert.ToInt16(oTableReferenceAA.Rows[iRowAAIndex]["SourceColumn"].ToString())] as Excel.Range).Value2) == null) oCol = "";
        //                            string sReport = "Column [" + oCol.ToString() + "] Line [" + iRowExcel.ToString() + "] is mandatory.";
        //                            wrkUpload.ReportProgress(100, "ERROR AA: " + sReport);
        //                            MessageBox.Show(sReport, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        //                            return null;
        //                        }
        //                        else oCell = "";
        //                    }
        //                    oCollCells[iIndexColumnsAA] = oCell;
        //                    iRowAAIndex++;
        //                }
        //            }
        //            oTable.Rows.Add(oCollCells);
        //            if (iRowAAIndex == oTableReferenceAA.Rows.Count)
        //                break;
        //        }
        //    }
        //    return oTable;
        //}
        
        /// <summary>
        /// Generates required tables.
        /// </summary>
        /// <param name="oTableProfileDE">DE (Terminal) table</param>
        /// <param name="oTableProfileAA">AA (Acquirer) table</param>
        /// <returns></returns>
        //private bool GenerateTables(ref DataTable oTableProfileDE, ref DataTable oTableProfileAA)
        //{
        //    return
        //        (oTableProfileDE = GenerateTableDE()) != null &&
        //        (oTableProfileAA = GenerateTableAA()) != null;
        //}

        /// <summary>
        /// Generates value list to be inserted into the database table.
        /// </summary>
        /// <param name="sParameterList">Parameter list</param>
        /// <returns>Parameter list string</returns>
        private string sGetParameterList(List<string> sParameterList)
        {
            string sColumns = "";
            foreach (string sCol in sParameterList)
                sColumns += "@" + sCol.Replace(" ", "_") + ",";
            return sColumns.Remove(sColumns.Length - 1);
        }

        /// <summary>
        /// Generates column list to be modified in the database table.
        /// </summary>
        /// <param name="sColumnList">Column list</param>
        /// <returns>Column list string</returns>
        private string sGetColumnList(List<string> sColumnList)
        {
            string sColumns = "";
            foreach (string sCol in sColumnList)
                sColumns += "[" + sCol + "],";
            return sColumns.Remove(sColumns.Length - 1);
        }

        /// <summary>
        /// Generates the INSERT command.
        /// </summary>
        /// <param name="sTag">Tag (DE/AA)</param>
        /// <param name="sColumnList">Column list</param>
        /// <returns>Command string</returns>
        private string sGetInsertCommand(string sTag, List<string> sColumnList)
        {
            return "INSERT INTO [dbo].[" + sGetTableName(sTag) + "] (" + sGetColumnList(sColumnList) + ") VALUES (" + sGetParameterList(sColumnList) + ")";
        }

        /// <summary>
        /// Sends the profile table to the database.
        /// </summary>
        /// <param name="sTag">Tag (DE/AA)</param>
        /// <param name="sColumnList">Column list</param>
        /// <param name="oTable">The table</param>
        private void InsertProfile(string sTag, List<string> sColumnList, DataTable oTable)
        {
            SqlDataAdapter oAdapt = new SqlDataAdapter();
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            oAdapt.InsertCommand = new SqlCommand(sGetInsertCommand(sTag, sColumnList), oSqlConn);
            foreach (string sParam in sColumnList)
                oAdapt.InsertCommand.Parameters.Add("@" + sParam.Replace(" ", "_") + "", SqlDbType.VarChar).SourceColumn = sParam;
            oAdapt.Update(oTable);
        }

        /// <summary>
        /// Starts data upload process add/update/delete new terminal profile.
        /// </summary>
        private void doUploadProfiles(string sSPName)
        {
            if (oSqlConn.State != ConnectionState.Open) { oSqlConn.Close(); oSqlConn.Open(); }
            SqlCommand oSqlComm = new SqlCommand(sSPName, oSqlConn);
            oSqlComm.CommandType = CommandType.StoredProcedure;
            oSqlComm.CommandTimeout = 60000;

            oSqlComm.Parameters.Add("@TerminalTable", SqlDbType.VarChar).Value = sGetTableName("DE");
            oSqlComm.Parameters.Add("@AcquirerTable", SqlDbType.VarChar).Value = sGetTableName("AA");
            oSqlComm.Parameters.Add("@sUser", SqlDbType.VarChar, 30).Value = UserData.sUserID;

            SqlDataAdapter oLogAdater = new SqlDataAdapter(oSqlComm);
            DataTable dtLog = new DataTable();
            oLogAdater.Fill(dtLog);
            oLogAdater.Dispose();
            ShowUploadLog(dtLog);
        }

        /// <summary>
        /// All the Upload Data process.
        /// </summary>
        //private void UploadProfiles()
        //{
        //    ClearUploadProfiles();
            
        //    DataTable oTableProfileDE = new DataTable();
        //    DataTable oTableProfileAA = new DataTable();
        //    wrkUpload.ReportProgress(0,
        //        string.Format("{0:MMM dd, yyyy hh:mm:ss tt} Start reading Excel data", DateTime.Now));
        //    if (GenerateTables(ref oTableProfileDE, ref oTableProfileAA))
        //    {
        //        wrkUpload.ReportProgress(0, string.Format("{0:MMM dd, yyyy hh:mm:ss tt} Uploading data",
        //            DateTime.Now));
        //        InsertProfile("DE", sColumnsDE, oTableProfileDE);
        //        InsertProfile("AA", sColumnsAA, oTableProfileAA);
        //        switch (modeUpload)
        //        {
        //            case UploadMode.Add:
        //                doUploadProfiles(CommonSP.sSPDataUploadStartProcessAdd);
        //                break;
        //            case UploadMode.Update:
        //                doUploadProfiles(CommonSP.sSPDataUploadStartProcessUpdate);
        //                break;
        //            case UploadMode.Delete:
        //                doUploadProfiles(CommonSP.sSPDataUploadStartProcessDelete);
        //                break;
        //        }
                
        //        // prepare the new content
        //        if(modeUpload == UploadMode.Delete)
        //            foreach (DataRow rowDE in oTableProfileDE.Rows)
        //                SaveFullContentClass.ClearInitTemp(oSqlConn, rowDE["TerminalID"].ToString());
        //        else
        //        foreach (DataRow rowDE in oTableProfileDE.Rows)
        //            SaveFullContentClass.SaveFullContent(oSqlConn, rowDE["TerminalID"].ToString());

        //        wrkUpload.ReportProgress(100, string.Format("{0:MMM dd, yyyy hh:mm:ss tt} Done",
        //            DateTime.Now));
        //    }

        //    MessageBox.Show(iCells > 1 ? "Upload Process Done!" : "No profile was uploaded.",
        //        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //}

        /// <summary>
        /// Sets display.
        /// </summary>
        /// <param name="isEnabled">Is enabled</param>
        private void SetDisplay(bool isEnabled)
        {
            barUploadProgress.Style = isEnabled ? ProgressBarStyle.Blocks : ProgressBarStyle.Marquee;
            this.Cursor = isEnabled ? Cursors.Default : Cursors.AppStarting;

            //if (!isEnabled) txtStatus.Text = "";
            //txtSourceFile.ReadOnly = !isEnabled;

            gbUserInput.Enabled = isEnabled;
            gbButton.Enabled = isEnabled;
            
            //btnBrowse.Enabled = isEnabled;
            //btnUpload.Enabled = isEnabled;
            //btnCancel.Enabled = isEnabled;
            
            this.ControlBox = isEnabled;
        }

        /// <summary>
        /// Show Upload Log
        /// </summary>
        /// <param name="dtUploadLog"></param>
        private void ShowUploadLog(DataTable dtUploadLog)
        {
            foreach (DataRow drow in dtUploadLog.Rows)
            {
                string sLog = string.Format("{0:MMM dd, yyyy hh:mm:ss tt} {2}. Status : {3}",
                    drow["Time"],
                    drow["TerminalId"].ToString(),
                    drow["Description"].ToString(),
                    drow["Remarks"].ToString());
                wrkUpload.ReportProgress(100, sLog);
            }
        }
        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
        }

        private void rbNo_CheckedChanged(object sender, EventArgs e)
        {
            if (rbNo.Checked)
            {
                rbYes.Checked = false;
                string sTemplate = ConfigurationManager.AppSettings["Template"];
                if (!string.IsNullOrEmpty(sTemplate))
                    dtTemplate = CommonLib.ConvertCSVtoDataTable(sTemplate, ',');
            }
        }

        private void rbYes_CheckedChanged(object sender, EventArgs e)
        {
            if (rbYes.Checked)
            {
                string version = ConfigurationManager.AppSettings["Version"];
                dtTemplate = dtGetEmsXmlMap(version);
                rbNo.Checked = false;
            }
        }
    }
}