﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using InSysClass;
using System.Data.SqlClient;
using Ini;
using System.IO;

namespace InsysTools
{
    public partial class frmReplaceTID : Form
    {
        protected SqlConnection oConn;
        protected string sFileName = "";
        protected string sMessage = "";
        protected int iTotalTID = 0;
        protected int iCurrTotal = 0;
        string sUserID = "";
        /// <summary>
        /// Initialize Form Replace TID
        /// </summary>
        public frmReplaceTID(SqlConnection _oConn, string _sUserID)
        {
            oConn = _oConn;
            sUserID = _sUserID;
            InitializeComponent();
        }

        /// <summary>
        /// initialize data Form Replace TID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmReplaceTID_Load(object sender, EventArgs e)
        {
          // doInitializeConnection();
            doInitializeForm();
          // doLogin();
        }

        /// <summary>
        /// Initialize Connection Database 
        /// </summary>
        protected void doLogin()
        {
            if (isHaveConfiguration()) doStartLogin();
            else Application.ExitThread();
        }

        /// <summary>
        /// Determine User can Log in
        /// </summary>
        protected void doStartLogin()
        {
           //FrmLogin fLogin = new FrmLogin(oConn);
            //try
            //{
            //    //fLogin.ShowDialog();
            //}
            //finally
            //{
            //    UserData.sUserID = fLogin.sGetUserID();
            //    UserData.sUserName = fLogin.sGetUserName();
            //    UserData.sPassword = CommonClass.sGetEncrypt(fLogin.sGetPassword());
            //    UserData.isSuperUser = UserPrivilege.IsAllowed(PrivilegeCode.SU);
            //    UserData.sGroupID = fLogin.sGetGroupID();
            //    //iStatus = fLogin.iGetStatus();
            //}

            //if (fLogin.isCanLoginToSystem)
            //{
            //    this.Visible = true;
            //    //doSetupDisplay(iStatus);
            //}
            //else
            //    Application.ExitThread();

            //fLogin.Dispose();
        }

        /// <summary>
        /// Determines wheter have configuration or not.
        /// </summary>
        /// <returns>boolean : true if configuration found, else false</returns>
        protected bool isHaveConfiguration()
        {
            string sActiveDir = Application.StartupPath;
            string sFileName = sActiveDir + "\\Application.conf";

            if (File.Exists(sFileName)) return true;
            else return false;
        }

        /// <summary>
        /// Open Connection
        /// </summary>
        protected void doInitializeConnection()
        {
            oConn = new SqlConnection(new InitData(Application.StartupPath).sGetConnString());
            try
            {
                oConn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }
        }

        /// <summary>
        /// Set display form
        /// </summary>
        protected void doInitializeForm()
        {
            txtFileName.ReadOnly = txtFileName.Enabled = true;
            rtbMessage.Enabled = rtbMessage.ReadOnly = true;
            pbProgress.Style = ProgressBarStyle.Continuous;
            ofdReplaceFile.Filter = "Excel Format (*.xls,*.xlsx)|*.xls;*.xlsx";

           // ofdReplaceFile.Filter = "Excel 97-2003 (*.xls)|*.xls|Excel 2007  (*.xlsx)|*.xlsx";
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                doBrowseFile();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Get Url File 
        /// </summary>
        protected void doBrowseFile()
        {
            if (ofdReplaceFile.ShowDialog() != DialogResult.Cancel)
            {
                txtFileName.Text = ofdReplaceFile.FileName;
                string sFileNameTxt = ofdReplaceFile.FileName;
                //sFileName = Application.StartupPath + "\\" + ofdReplaceFile.SafeFileName;
                sFileName = ofdReplaceFile.FileName;
                //doCopyFile(ofdReplaceFile.FileName, sFileName);
            }
        }

        /// <summary>
        /// Copy File
        /// </summary>
        /// <param name="sSourceFile">string : URL source File</param>
        /// <param name="sDestFile">string : URL Destination File </param>
        protected void doCopyFile(string sSourceFile, string sDestFile)
        {
            if (File.Exists(sDestFile))
                File.Delete(sDestFile);
            File.Copy(sSourceFile, sDestFile);
        }

        private void btnStartProcess_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFileName.Text))
                MessageBox.Show("Please Choose File");
            else
            {
                try
                {
                    doEnableForm(false);
                    bgExcelWorker.RunWorkerAsync(sFileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        protected void doEnableForm(bool isEnable)
        {
            gbFile.Enabled = isEnable;
            gbButton.Enabled = isEnable;
        }

        protected DataTable dtReadExcel()
        {
            DataTable dtTable = new DataTable();

            try
            {
                NewExcelOleDb oExcel = new NewExcelOleDb(sFileName);
                doExcelAddColumn(oExcel);
                dtTable = oExcel.dtReadExcel("Sheet1");
                oExcel.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dtTable;
        }

        protected void doExcelAddColumn(NewExcelOleDb oleDB)
        {
            oleDB.AddColumnName("OldTID");
            oleDB.AddColumnName("NewTID");
            oleDB.AddColumnName("KodeDummy");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bgExcelWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bgExcelWorker.WorkerReportsProgress = true;
            doReplaceTID();
        }

        protected void doReplaceTID()
        {
            DataTable dtReplaceTID = dtReadExcel();
            iTotalTID = dtReplaceTID.Rows.Count;
            iCurrTotal = 0;
            if (iTotalTID > 0)
            {
                foreach (DataRow drRow in dtReplaceTID.Rows)
                {
                    sMessage = "";
                    doUpdateDbReplaceTID(drRow);
                    iCurrTotal++;
                    
                    //bgExcelWorker.ReportProgress((iCurrTotal * 100) / iTotalTID);

                    //Remove WriteLog to doUpdateDbReplaceTID so every TID only generate 1 row of log in AuditTrail
                    doWriteLog(drRow["OldTID"].ToString(), drRow["NewTID"].ToString(), drRow["KodeDummy"].ToString());
                }
            }
        }

        protected void doUpdateDbReplaceTID(DataRow drRow)
        {
            using (SqlCommand oComm = new SqlCommand(StoreProcedure.sReplaceTID, oConn))
            {
                oComm.CommandType = CommandType.StoredProcedure;

                oComm.Parameters.Add("@sOldTID", SqlDbType.Char).Value = drRow["OldTID"];
                oComm.Parameters.Add("@sNewTID", SqlDbType.Char).Value = drRow["NewTID"];
                oComm.Parameters.Add("@sMIDDummy", SqlDbType.VarChar).Value = drRow["KodeDummy"];

                int iRespon = oComm.ExecuteNonQuery();
                if (iRespon > 0)
                {
                    //doInsertReportLog(drRow["OldTID"].ToString(), drRow["NewTID"].ToString(), "Replace Succeed");
                    bgExcelWorker.ReportProgress(0, string.Format("Replace Old '{0}' with '{1}'. SUCCESS", drRow["OldTID"], drRow["NewTID"]));
                    CommonClass.InputLog(oConn, "", sUserID, "", "Replace TID " + drRow["OldTID"] + " To " + drRow["NewTID"] + " SUCESS", "Replace TID SUCESS");
                }
                else
                {
                    doWriteLog(drRow["OldTID"].ToString(), drRow["NewTID"].ToString(), drRow["KodeDummy"].ToString(), sMessage);
                    bgExcelWorker.ReportProgress(0, string.Format("Replace Old '{0}' with '{1}'. FAILED", drRow["OldTID"], drRow["NewTID"]));
                    CommonClass.InputLog(oConn, "", sUserID, "", "Replace TID " + drRow["OldTID"] + " To " + drRow["NewTID"]+ " FAILED", "Replace TID FAILED");
                }
                //using (SqlDataReader oRead = oComm.ExecuteReader())
                //{
                //    //while (oRead.Read())
                //    //{
                //    //    //rtbMessage.Text = oRead[0].ToString();
                //    //    sMessage = oRead[0].ToString();
                //    //    doWriteLog(drRow["OldTID"].ToString(), drRow["NewTID"].ToString(), drRow["KodeDummy"].ToString(), sMessage);
                //    //}

                //    if (oRead.Read())
                //    {
                //        sMessage = oRead[0].ToString();
                //        doWriteLog(drRow["OldTID"].ToString(), drRow["NewTID"].ToString(), drRow["KodeDummy"].ToString(), sMessage);
                //        doInsertReportLog(drRow["OldTID"].ToString(), drRow["NewTID"].ToString(), "Replace Failed");
                //    }
                //    else
                //    {
                //        doWriteLog(drRow["OldTID"].ToString(), drRow["NewTID"].ToString(), drRow["KodeDummy"].ToString());
                //        doInsertReportLog(drRow["OldTID"].ToString(), drRow["NewTID"].ToString(), "Replace Succeed");
                //    }

                //    oRead.Close();
                //    oRead.Dispose();
                //}
            }
        }

        private void bgExcelWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbProgress.Value = e.ProgressPercentage;

            //doWriteMessageToRTB("Processing " + iCurrTotal.ToString() + " of " + iTotalTID.ToString() + " TID(s).");
            doWriteMessageToRTB(e.UserState.ToString());
        }

        protected void doWriteMessageToRTB(string sCurrMessage)
        {
            rtbMessage.Text += sCurrMessage;
            rtbMessage.Text += System.Environment.NewLine;
        }

        private void bgExcelWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
           // File.Delete(sFileName);
            doWriteMessageToRTB("Replace TID Process Finished.");
            doEnableForm(true);
            btnStartProcess.Enabled = false;
        }

        /// <summary>
        /// Write Log
        /// </summary>
        /// <param name="sOldTID">string : Old Terminal ID</param>
        /// <param name="sNewTID">string : New Terminal ID</param>
        /// <param name="sMID">string : MID</param>
        protected void doWriteLog(string sOldTID, string sNewTID, string sMID)
        {
            CommonClass.InputLog(oConn, sOldTID, UserData.sUserID, "", "Replace TID", sGenerateLogDetail(sOldTID, sNewTID, sMID));
        }

        /// <summary>
        /// Write Log
        /// </summary>
        /// <param name="sOldTID">string : Old Terminal ID</param>
        /// <param name="sNewTID">string : New Terminal ID</param>
        /// <param name="sMID">string : MID</param>
        /// <param name="sMessage">string : Message</param>
        protected void doWriteLog(string sOldTID, string sNewTID, string sMID, string sMessage)
        {
            CommonClass.InputLog(oConn, sOldTID, UserData.sUserID, "", "Fail to Replace TID", sMessage);
        }

        /// <summary>
        /// Generate Detail Log
        /// </summary>
        /// <param name="sOldTID">string : Old Terminal ID</param>
        /// <param name="sNewTID">string : New Terminal ID</param>
        /// <param name="sMID">String : MID</param>
        /// <returns>string :Detail Log</returns>
        protected string sGenerateLogDetail(string sOldTID, string sNewTID, string sMID)
        {
            return "Replace Old TID " + sOldTID + " with New TID " + sNewTID + " using MID Dummy " + sMID;
        }

        /// <summary>
        /// Insert Report Log
        /// </summary>
        /// <param name="sOldTID">string : OLD Terminal ID</param>
        /// <param name="sNewTID">string : New Terminal ID</param>
        /// <param name="sStatus">string : Status</param>
        protected void doInsertReportLog(string sOldTID, string sNewTID, string sStatus)
        {
            using (SqlCommand oComm = new SqlCommand(StoreProcedure.sReportLogInsert, oConn))
            {
                oComm.CommandType = CommandType.StoredProcedure;
                oComm.Parameters.Add("@sOldTID", SqlDbType.VarChar).Value = sOldTID;
                oComm.Parameters.Add("@sNewTID", SqlDbType.VarChar).Value = sNewTID;
                oComm.Parameters.Add("@sStatus", SqlDbType.VarChar).Value = sStatus;
                oComm.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = UserData.sUserID;

                oComm.ExecuteNonQuery();
            }
        }

        private void btnViewLog_Click(object sender, EventArgs e)
        {
            this.Hide();

            frmViewLog fLog = new frmViewLog(oConn);

            try
            {
                fLog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fLog.Dispose();
                this.Show();
                this.Focus();
            }

        }
    }
}
