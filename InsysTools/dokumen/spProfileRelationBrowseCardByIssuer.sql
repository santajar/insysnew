IF OBJECT_ID ('dbo.spProfileRelationBrowseCardByIssuer') IS NOT NULL
	DROP PROCEDURE dbo.spProfileRelationBrowseCardByIssuer
GO

-- =============================================
-- Author:		Tatang Sumarta
-- Create date: Jul 15, 2019
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Get Data Card Relation by Issuer 
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationBrowseCardByIssuer]
	@sTerminalID VARCHAR(8),
	@sIssuer VARCHAR(8)
AS
SET NOCOUNT ON;

 IF OBJECT_ID('TEMPDB..#TempRelationAll') IS NOT NULL  
 DROP TABLE #TempRelationAll 
 
  SELECT 
	RelationTagID,
	TerminalID,
	RelationTag,
	RelationLengthOfTagLength,
	RelationTagLength,
	RelationTagValue,
ROW_NUMBER() OVER (ORDER BY TerminalID,RelationTagID ASC) AS RowID	
INTO #TempRelationAll
FROM tbProfileRelation WITH (NOLOCK)
WHERE TerminalID = @sTerminalID   
ORDER BY TerminalID,RelationTagID ASC 

 
 IF OBJECT_ID('TEMPDB..#TempRel') IS NOT NULL  
 DROP TABLE #TempRel 
 SELECT RowID,RelationTagID, RelationTagValue AS Issuer 
 INTO #TempRel 
 FROM #TempRelationAll WITH(NOLOCK)
 WHERE TerminalID = @sTerminalID   
 AND RelationTag IN ('AD002', 'AD02') 
 ORDER BY TerminalID,RelationTagID 
 
 --SELECT * FROM #TempRel
 
 select DISTINCT b.Issuer,a.RelationTagValue as Card 
 from #TempRelationAll a with(NOLOCK) 
 INNER JOIN (SELECT RowID,RelationTagID, Issuer FROM #TempRel ) b 
 ON a.RowID = b.RowID - 1 
 where a.TerminalID = @sTerminalID  
 AND a.RelationTag in ('AD001', 'AD02') 
 AND b.Issuer = @sIssuer AND a.RelationTagValue <> '' 