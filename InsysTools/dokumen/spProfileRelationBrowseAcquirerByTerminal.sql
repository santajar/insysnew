IF OBJECT_ID ('dbo.spProfileRelationBrowseAcquirerByTerminal') IS NOT NULL
	DROP PROCEDURE dbo.spProfileRelationBrowseAcquirerByTerminal
GO

-- =============================================
-- Author:		Tatang Sumarta
-- Create date: Jul 15, 2019
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Get Data Acquirer Relation by Terminal 
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationBrowseAcquirerByTerminal]
	@sTerminalID VARCHAR(8)
AS
SET NOCOUNT ON;

 IF OBJECT_ID('TEMPDB..#TempAcquirer') IS NOT NULL  
 DROP TABLE #TempAcquirer 
 
 SELECT DISTINCT AcquirerName 
 INTO #TempAcquirer
 FROM tbProfileAcquirer WITH (NOLOCK) WHERE TerminalID = @sTerminalID
 AND AcquirerTag IN ('AA001','AA01')
 	

 SELECT DISTINCT RelationTagValue AS Acquirer 
 FROM tbProfileRelation WITH(NOLOCK)
 WHERE TerminalID = @sTerminalID   
 AND RelationTag IN ('AD003', 'AD03') 
 AND RelationTagValue IN (SELECT AcquirerName FROM #TempAcquirer)
 
 