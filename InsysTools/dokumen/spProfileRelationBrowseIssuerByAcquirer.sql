IF OBJECT_ID ('dbo.spProfileRelationBrowseIssuerByAcquirer') IS NOT NULL
	DROP PROCEDURE dbo.spProfileRelationBrowseIssuerByAcquirer
GO

-- =============================================
-- Author:		Tatang Sumarta
-- Create date: Jul 15, 2019
-- Modify date: 
--				1. <MMM dd, yyyy>, <description>
-- Description:	
--				1. Get Data Issuer by Acquirer
-- =============================================
CREATE PROCEDURE [dbo].[spProfileRelationBrowseIssuerByAcquirer]
	@sTerminalID VARCHAR(8),
	@sAcquirer VARCHAR(8)
AS
SET NOCOUNT ON;

 IF OBJECT_ID('TEMPDB..#TempRelationAll') IS NOT NULL  
 DROP TABLE #TempRelationAll 
 
  SELECT 
	RelationTagID,
	TerminalID,
	RelationTag,
	RelationLengthOfTagLength,
	RelationTagLength,
	RelationTagValue,
ROW_NUMBER() OVER (ORDER BY TerminalID,RelationTagID ASC) AS RowID	
INTO #TempRelationAll
FROM tbProfileRelation WITH (NOLOCK)
WHERE TerminalID = @sTerminalID   
ORDER BY TerminalID,RelationTagID ASC 

 
 IF OBJECT_ID('TEMPDB..#TempAcquirerRel') IS NOT NULL  
 DROP TABLE #TempAcquirerRel 
 SELECT RowID,RelationTagID, RelationTagValue AS Acquirer 
 INTO #TempAcquirerRel 
 FROM #TempRelationAll WITH(NOLOCK)
 WHERE TerminalID = @sTerminalID   
 AND RelationTag IN ('AD003', 'AD03') 
 ORDER BY TerminalID,RelationTagID 
 
 --SELECT * FROM #TempAcquirerRel
 
 select DISTINCT b.Acquirer,a.RelationTagValue as Issuer 
 from #TempRelationAll a with(NOLOCK) 
 INNER JOIN (SELECT RowID,RelationTagID, Acquirer FROM #TempAcquirerRel ) b 
 ON a.RowID = b.RowID - 1 
 where a.TerminalID = @sTerminalID  
 AND a.RelationTag in ('AD002', 'AD02') 
 AND b.Acquirer = @sAcquirer AND a.RelationTagValue <> '' 