
IF OBJECT_ID('TEMPDB..#TempAcquirerRel') IS NOT NULL 
DROP TABLE #TempAcquirerRel 

SELECT RelationTagID, RelationTagValue AS Acquirer 
INTO #TempAcquirerRel 
FROM tbProfileRelation WITH(NOLOCK)
WHERE TerminalID = 'AVOAIR03'  AND RelationTag in ('AD003', 'AD03') 
ORDER BY TerminalID,RelationTagID 

select DISTINCT b.Acquirer,a.RelationTagValue as Issuer 
from tbProfileRelation a with(NOLOCK) 
INNER JOIN (SELECT RelationTagID, Acquirer FROM #TempAcquirerRel 
) b ON a.RelationTagId = b.RelationTagid - 1 
where a.TerminalID = 'AVOAIR03' AND a.RelationTag in ('AD002', 'AD02') 
AND b.Acquirer = 'BCA'
AND a.RelationTagValue <> '' 


IF OBJECT_ID('TEMPDB..#TempCardRel') IS NOT NULL 
DROP TABLE #TempCardRel 

SELECT RelationTagID, RelationTagValue AS Issuer 
INTO #TempCardRel 
FROM tbProfileRelation WITH(NOLOCK)
WHERE TerminalID = 'AVOAIR03'  AND RelationTag in ('AD002', 'AD02') 
ORDER BY TerminalID,RelationTagID 

select DISTINCT b.Issuer,a.RelationTagValue as Card 
from tbProfileRelation a with(NOLOCK) 
INNER JOIN (SELECT RelationTagID, Issuer FROM #TempCardRel 
) b ON a.RelationTagId = b.RelationTagid - 1 
where a.TerminalID = 'AVOAIR03' AND a.RelationTag in ('AD001', 'AD01') 
AND b.Issuer = 'MASTERCARD'
AND a.RelationTagValue <> '' 