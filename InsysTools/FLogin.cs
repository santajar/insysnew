﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using InSysClass;

namespace AppUpdateConnection
{
    
    public partial class FLogin : Form
    {
        static string sUserName;
        static string sPassword;
        static string sPassFromDB;
        static string sUserNameFromDB;

        string sConnString = ConfigurationManager.ConnectionStrings["Main"].ConnectionString;
        static string sAppDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
        protected SqlConnection oSqlConn;
       
        static bool bDebug = true;

        public FLogin()
        {
            InitializeComponent();
              
            oSqlConn = new SqlConnection(sConnString);
            oSqlConn.Open();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            oSqlConn = new SqlConnection(sConnString);
            sUserName = txtName.Text;
            sPassword = CommonClass.sGetEncrypt(txtPass.Text);
            
            DataTable dt = dtLoadUser();
            if (dt.Rows.Count > 0)
            {
                sPassFromDB = dt.Rows[0][2].ToString();
               
                sUserNameFromDB = dt.Rows[0][0].ToString();
            }
           
            if (sUserName == sUserNameFromDB && sPassword == sPassFromDB)
            {
               
                Fmain fmain = new Fmain(oSqlConn, sUserName);
                CommonClass.InputLog(oSqlConn, "", sUserName, "", "Login Success By AppUpdateProvider ", "Login AppUpdateProvider");
                this.Hide();
                fmain.ShowDialog();
            }
            else
            {
                MessageBox.Show("Wrong Passord");
            }
            
        }


        protected DataTable dtLoadUser()
        {
            DataTable dtTable = new DataTable();
            try
            {
                string sCmd = " select * from tbUserLogin where UserID =" + "'" +txtName.Text+ "'";
                using (SqlCommand oSqlCmd = new SqlCommand(sCmd, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter oAdapt = new SqlDataAdapter(oSqlCmd))
                        oAdapt.Fill(dtTable);
                }
            }
            catch (Exception ex)
            {
                dtTable = null;
            }
            return dtTable;
        }

      

        private void Flogin_Load(object sender, EventArgs e)
        {
        }

        private void Flogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CommonClass.InputLog(oSqlConn, "", sUserName, "", "User Logout " , "Logout Success");
            this.Close();
        }

    }
}
