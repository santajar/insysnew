﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using InSysClass;
using Ini;

namespace InSysConsoleAutoInitFtps
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;
        protected string sDirectory;

        public InitData(string _sDirectory)
        {
            sDirectory = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectory + "\\Application.config";
            string sEncryptFileName = sDirectory + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception)
                {
                    //Logs.doWriteErrorFile(sDirectory, ex.Message);
                }
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword);
        }

        public string sGetConnString(int iMaxPoolSize)
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword, iMaxPoolSize);
        }

    }

    class Program
    {
        protected static SqlConnection oSqlConn;
        protected static List<string> ltContentAui = new List<string>();
        static string sMessage = null;
        static string sConnString;
        static string sAppDirectory = Directory.GetCurrentDirectory();
        static CultureInfo ciUSFormat = new CultureInfo("en-US"); // English - United States
        static string sTemp = "";
        static string sStatus = "0";
        static string sStatusNotOpen = "0";
        public static DataTable dtISO8583 = new DataTable();

        protected class DataCompletion
        {
            public string sTerminalID = null; //CM001
            public string sSerialNumber = null; //CM002
            public string sFreeRAM = null; //CM003
            public string sTotalRAM = null; //CM004
            public string sInstallDate = null; //CM101 sLastInitTime
            public string sSV_Name = null; //CM102
            public string sPABX = null; //CM005
            public string sOSVers = null; //CM006
            public string sKernelEmvVers = null; //CM007
            public string sReaderSN = null; //CM008
            public string sPSAMSN = null; //CM009
            public string sMCSN = null; //CM010
            public string sMemUsg = null; //CM011
            public string sICCID = null; //CM012

            //string sLastInitTime = null;
            //string sPABX = null;
            //string sOSVers = null;
            //string sKernelEmvVers = null;
            //string sReaderSN = null;
            //string sPSAMSN = null;
            //string sMCSN = null;
            //string sMemUsg = null;
            //string sICCID = null;
        }
        static void Main(string[] args)
        {
            string path = ConfigurationManager.AppSettings["Path"].ToString();
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            string sFileNotOpen = "StatusNotOpen.txt";
            string sFilePathNotOpen = Application.StartupPath + "\\" + sFileNotOpen;
            try
            {
                if (sFilePathNotOpen != null)
                {
                    File.Delete(sFilePathNotOpen);
                    Write2File(sFilePathNotOpen, "0", true);
                }
                else Write2File(sFilePathNotOpen, "0", true);
            }
            catch (Exception ex)
            {
                sTemp = sGetMMMddyyyyhhmmsstt() + " " + string.Format("{0} File Status Not Open, {1}", UserData.sUserID, sMessage + " " + sFilePathNotOpen + " " + ex.Message); // Set the content of the file
                Console.WriteLine(sTemp);
                CommonClass.doWritelogFile(string.Format("{0} File Status Not Open, {1}", UserData.sUserID, sMessage + " " + sFilePathNotOpen + " " + ex.Message));
            }
            InitStatusNotOpen();
            while (sStatusNotOpen == "0")
            {
                try
                {
                    oSqlConn = InitConnection();
                    if (oSqlConn.State == ConnectionState.Open)
                    {
                        #region 
                        //=============================== Completion
                        string ExtentionCompletion = ConfigurationManager.AppSettings["ExtentionCompletion"].ToString();
                        string[] asFilename;
                        asFilename = null;
                        asFilename = Directory.GetFiles(path+"\\Completion\\", @"*."+ ExtentionCompletion, SearchOption.TopDirectoryOnly); 
                        
                        ltContentAui = new List<string>();

                        //string sCurrDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        //string Pathlength = sCurrDir + @"\\";
                        //if (!Directory.Exists(Pathlength))
                        //    Directory.CreateDirectory(Pathlength);
                        //string filePathLength = Pathlength + "Length.txt";
                        //string[] lengths = System.IO.File.ReadAllLines(filePathLength);
                        //List<string> listlengths = new List<string>();
                        //foreach (string len in lengths)
                        //{
                        //    listlengths.Add(len);
                        //}
  
                        bool isEmpty = asFilename.Any();
                        if (isEmpty)
                        {
                            File.Delete(sFileNotOpen);
                            Write2File(sFileNotOpen, "1", true);
                            foreach (string sFilename in asFilename)
                            {
                                if (!string.IsNullOrEmpty(sFilename))
                                {
                                    ltContentAui.Clear();
                                    string sTerminalInit = null;
                                    sTerminalInit = Path.GetFileNameWithoutExtension(sFilename);
                                    if (IsTerminalExist(sTerminalInit))
                                    {
                                        AddContentStatus(sFilename);
                                        foreach (string sContentAui in ltContentAui)
                                        {
                                            try
                                            {
                                                #region

                                                string fileName = null;
                                                fileName = System.IO.Path.GetFileName(sFilename);
                                                if (!string.IsNullOrEmpty(fileName))
                                                {
                                                    //string PathTerminalBackup = path + "\\Backup\\" + @"\\" + sTerminalInit + "\\";
                                                    //if (!Directory.Exists(PathTerminalBackup))
                                                    //    Directory.CreateDirectory(PathTerminalBackup);
                                                    //string pathDestination = PathTerminalBackup + fileName;
                                                    //if (!string.IsNullOrEmpty(pathDestination))
                                                    //{                                                             

                                                        ServiceInSysAutoInitFtps.ServiceInSysClient serviceInSys = new ServiceInSysAutoInitFtps.ServiceInSysClient();
                                                        string sSourceFilename = path + "\\COMPLETION\\" + sTerminalInit + "." + ExtentionCompletion;
                                                        // extract file
                                                        DataCompletion dataCompletion = new DataCompletion();
                                                        Do_ReadCompletionData(ref dataCompletion, sSourceFilename);

                                                        serviceInSys.UploadFileNotAllow(sTerminalInit);

                                                        string sRemoteFileCompletion = @"\COMPLETION\" + Path.GetFileName(sTerminalInit + "." + ExtentionCompletion);
                                                        serviceInSys.DeleteFtp(sRemoteFileCompletion);
                                                                
                                                        //CommonConsole.GetAppNameSN(sContentAui, ref sAppName, ref sSerialNum);
                                                        //CommonConsole.GetAutoInitMessage(sContentAui, ref sLastInitTime, ref sPABX, ref sAppName, ref sOSVers, ref sKernelEmvVers, ref sSerialNum, ref sReaderSN, ref sPSAMSN, ref sMCSN, ref sMemUsg, ref sICCID);
                                                              
                                                        //string sRemoteFileProfile = @"\PROFILE\" + Path.GetFileName(sFilename);
                                                        //serviceInSys.DeleteFtp(sRemoteFileProfile);

                                                        SqlCommand oCmd;
                                                        oCmd = new SqlCommand(string.Format("update tbProfileTerminalList SET AllowDownload = 0 where TerminalID='{0}'", sTerminalInit), oSqlConn);
                                                        if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                                                        oCmd.ExecuteNonQuery();

                                                        bool IsStartInit = true;
                                                        bool IsCompleteInit = true;
                                                        int Percentage = 100;
                                                        string ProcessCode = "";
                                                        SendLogInit(dataCompletion,IsStartInit, IsCompleteInit, Percentage, ProcessCode);
                                                        WriteLogInit(dataCompletion);

                                                        sMessage = string.Format("{0} Auto Initialize Ftps Complete", sTerminalInit);

                                                        // insert log to database
                                                        //WriteCompletionLog(dataCompletion, sFilename);
                                                        string sConsoleMessage = string.Format("Read COMPLETION '{0}' Success", sFilename);
                                                        sTemp = sGetMMMddyyyyhhmmsstt() + " " + sConsoleMessage;
                                                        Console.WriteLine(sTemp);
                                                    //}

                                                } 
                                                
                                                #endregion

                                                //GetAppNameSN
                                                //List<string> listcontentvaluetrim = new List<string>();
                                                //int istarttrim = 0, intindexcurrentstarttrim;

                                                //foreach (string indexcurrentstart in listlengths)
                                                //{
                                                //    intindexcurrentstarttrim = int.Parse(indexcurrentstart);
                                                //    listcontentvaluetrim.Add(sContentAui.Substring(istarttrim, intindexcurrentstarttrim).Trim());
                                                //    istarttrim = istarttrim + intindexcurrentstarttrim;
                                                //}

                                                //List<string> listcontentvalue = new List<string>();
                                                //int istart = 0, intindexcurrentstart;
                                                //string lengthvalue;
                                                //foreach (string indexcurrentstart in listlengths)
                                                //{
                                                //    intindexcurrentstart = int.Parse(indexcurrentstart);
                                                //    lengthvalue = String.Format("{0:00}", sContentAui.Substring(istart, intindexcurrentstart).Trim().Length);
                                                //    listcontentvalue.Add(lengthvalue + (sContentAui.Substring(istart, intindexcurrentstart)).Trim());
                                                //    istart = istart + intindexcurrentstart;
                                                //}
 
                                                // }

                                            }
                                            catch (SqlException ex)
                                            {
                                                //sMessage = string.Format("Update Allow Init Ftps Terminal : |" + sStatusInit + "|Failed|From File|" + sTerminalInit + " " + ex.Message);
                                                oSqlConn = InitConnection();
                                                File.Delete(sFileNotOpen);
                                                Write2File(sFileNotOpen, "0", true);
                                            }
                                            sTemp = sGetMMMddyyyyhhmmsstt() + " " + sMessage; // Set the content of the file

                                            Console.WriteLine(sTemp);
                                            CommonClass.doWritelogFile(string.Format("{0}", sMessage));

                                        }
                                        
                                    
                                    }// terminal not found
                                    else
                                    {
                                        sMessage = "Terminal " + sTerminalInit + " Not Found";
                                        sTemp = sGetMMMddyyyyhhmmsstt() + " " + sMessage;
                                        Console.WriteLine(sTemp);
                                        CommonClass.doWritelogFile(string.Format("{0}", sMessage));
                                    }

                                }// terminalid
                            } // file name

                        } //======= end completion                        
                        #endregion
                        Thread.Sleep(200);                       
                    }
                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn = InitConnection();
                        oSqlConn.Open();
                    }
                    File.Delete(sFileNotOpen);
                    Write2File(sFileNotOpen, "0", true);
                }
                catch (Exception ex)
                {
                    oSqlConn = InitConnection();
                    sTemp = sGetMMMddyyyyhhmmsstt() + " " + string.Format("{0} Auto Init Ftps Failed, {1}", UserData.sUserID, sMessage + " " + ex.Message); // Set the content of the file
                    Console.WriteLine(sTemp);
                    File.Delete(sFileNotOpen);
                    Write2File(sFileNotOpen, "0", true);                    
                }
                System.Threading.Thread.Sleep(200);
                InitStatusNotOpen();
            } // end while status not open
            
        }
        protected static void WriteCompletionLog(DataCompletion dataCompletion, string sFilename)
        {
            if (!string.IsNullOrEmpty(dataCompletion.sTerminalID) && !string.IsNullOrEmpty(dataCompletion.sSerialNumber))
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlCommand sqlcmd = new SqlCommand(CommonSP.sSPRD_AuditDownloadInsert, oSqlConn))
                {
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = dataCompletion.sTerminalID;
                    sqlcmd.Parameters.Add("@sSerialNumber", SqlDbType.VarChar).Value = dataCompletion.sSerialNumber;
                    sqlcmd.Parameters.Add("@sInstallDate", SqlDbType.VarChar).Value = dataCompletion.sInstallDate;
                    sqlcmd.Parameters.Add("@sSV_Name", SqlDbType.VarChar).Value = dataCompletion.sSV_Name;
                    sqlcmd.Parameters.Add("@sFreeRAM", SqlDbType.VarChar).Value = dataCompletion.sFreeRAM;
                    sqlcmd.Parameters.Add("@sTotalRAM", SqlDbType.VarChar).Value = dataCompletion.sTotalRAM;
                    sqlcmd.Parameters.Add("@sFilename", SqlDbType.VarChar).Value = sFilename;

                    sqlcmd.ExecuteNonQuery();
                }
            }
        }
        protected static void Do_ReadCompletionData(ref DataCompletion dataCompletion, string sFilename)
        {
            string sContent = null;
            using (FileStream fs = new FileStream(sFilename, FileMode.Open, FileAccess.Read))
            {
                if (fs.Length > 0)
                {
                    using (StreamReader sr = new StreamReader(fs))
                        sContent = sr.ReadToEnd();

                    int iIndex = 0;
                    string sTag = null;
                    string sLength = null;
                    string sValue = null;

                    while (iIndex < sContent.Length)
                    {
                        if ((sContent.ToCharArray())[iIndex] != '\0')
                        {
                            sTag = sContent.Substring(iIndex, 5);
                            iIndex += 5;

                            sLength = sContent.Substring(iIndex, 3);
                            iIndex += 3;

                            sValue = sContent.Substring(iIndex, int.Parse(sLength));
                            iIndex += int.Parse(sLength);

                            switch (sTag)
                            {
                                case "CM001": dataCompletion.sTerminalID = sValue; break;
                                case "CM002": dataCompletion.sSerialNumber = sValue; break;
                                case "CM003": dataCompletion.sFreeRAM = sValue; break;
                                case "CM004": dataCompletion.sTotalRAM = sValue; break;
                                case "CM101": dataCompletion.sInstallDate = sValue; break;
                                case "CM102": dataCompletion.sSV_Name = sValue; break;
                                case "CM005": dataCompletion.sPABX = sValue; break;
                                case "CM006": dataCompletion.sOSVers = sValue; break;
                                case "CM007": dataCompletion.sKernelEmvVers = sValue; break;
                                case "CM008": dataCompletion.sReaderSN = sValue; break;
                                case "CM009": dataCompletion.sPSAMSN = sValue; break;
                                case "CM010": dataCompletion.sMCSN = sValue; break;
                                case "CM011": dataCompletion.sMemUsg = sValue; break;
                                case "CM012": dataCompletion.sICCID = sValue; break;
                            }
                        }
                    }
                }
            }
        }
        protected bool IsApplicationAllowInit(string _sTerminalId, string _sAppName)
        {
            bool isAllow = false;
            string sQuery = string.Format("SELECT DBO.IsApplicationAllowInit('{0}','{1}')", _sTerminalId, _sAppName);
            using (SqlCommand oCmd = new SqlCommand(sQuery, oSqlConn))
            {
                DataTable dtTemp = new DataTable();
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                dtTemp.Load(oCmd.ExecuteReader());
                if (dtTemp.Rows.Count > 0)
                    isAllow = bool.Parse(dtTemp.Rows[0][0].ToString());
            }
            return isAllow;
        }

        private static void SendLogInit(DataCompletion dataCompletion, bool isStartInit, bool isComplete, int iPercentage, string sProcCode)
        {
            string sStatusInit = "Auto Initialize Ftps Complete";
            SendLogInitAudit(dataCompletion.sTerminalID, dataCompletion.sSV_Name, dataCompletion.sSerialNumber, sStatusInit);
            SendLogInitConn(dataCompletion.sTerminalID, 0, 100);
        }
        private static void WriteLogInit(DataCompletion dataCompletion)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitUpdate, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = dataCompletion.sTerminalID;
                oCmd.Parameters.Add("@sLastInitTime", SqlDbType.VarChar).Value = dataCompletion.sInstallDate;
                oCmd.Parameters.Add("@sPABX", SqlDbType.VarChar).Value = dataCompletion.sPABX;
                oCmd.Parameters.Add("@sSoftwareVers", SqlDbType.VarChar).Value = dataCompletion.sSV_Name;
                oCmd.Parameters.Add("@sOSVers", SqlDbType.VarChar).Value = dataCompletion.sOSVers;
                oCmd.Parameters.Add("@sKernelEmvVers", SqlDbType.VarChar).Value = dataCompletion.sKernelEmvVers;
                oCmd.Parameters.Add("@sEdcSN", SqlDbType.VarChar).Value = dataCompletion.sSerialNumber;
                oCmd.Parameters.Add("@sReaderSN", SqlDbType.VarChar).Value = dataCompletion.sReaderSN;
                oCmd.Parameters.Add("@sPSAMSN", SqlDbType.VarChar).Value = dataCompletion.sPSAMSN;
                oCmd.Parameters.Add("@sMCSN", SqlDbType.VarChar).Value = dataCompletion.sMCSN;
                oCmd.Parameters.Add("@sMemUsg", SqlDbType.VarChar).Value = dataCompletion.sMemUsg;
                oCmd.Parameters.Add("@sICCID", SqlDbType.VarChar).Value = dataCompletion.sICCID;
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }
        private static void SendLogInitAudit(string sTerminalId, string sAppName, string sSerialNum, string sStatus)
        {
            try
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPAuditInitInsert, oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 60;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                    oCmd.Parameters.Add("@sSoftware", SqlDbType.VarChar).Value = string.IsNullOrEmpty(sAppName) ? "" : sAppName;
                    oCmd.Parameters.Add("@sSerialNum", SqlDbType.VarChar).Value = string.IsNullOrEmpty(sSerialNum) ? "" : sSerialNum;
                    oCmd.Parameters.Add("@sStatus", SqlDbType.VarChar).Value = sStatus;
                    oCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWritelogFile("|ERROR|SendLogInitFtpsAudit|" + ex.StackTrace);
            }
        }

        private static void SendLogInitConn(string sTerminalId, int iStartInit, int iPercentage)
        {
            try
            {
                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPInitLogConnNewInitConsole, oSqlConn))
                {
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.CommandTimeout = 60;
                    oCmd.Parameters.Add("@sTerminalID", SqlDbType.VarChar).Value = sTerminalId;
                    oCmd.Parameters.Add("@iStartInit", SqlDbType.Int).Value = iStartInit;
                    oCmd.Parameters.Add("@iPercentage", SqlDbType.Int).Value = iPercentage;
                    oCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                CommonClass.doWritelogFile(string.Format("|ERROR|SendLogInitFtpsConn| {0} | TerminalID : {1} | Start Init : {2} | Percentage : {3}", ex.StackTrace, sTerminalId, iStartInit, iPercentage));
            }
        }

        static bool IsTerminalExist(string _sTerminalId)
        {
            bool bReturn = true;
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPTerminalListBrowse, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value =
                    string.Format("WHERE TerminalID='{0}'", _sTerminalId);

                if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
                using (SqlDataReader read = oCmd.ExecuteReader())
                    if (!read.HasRows)
                        bReturn = false;
            }
            return bReturn;
        }

        //static bool IsAllowLastDate(string _sTerminalId)
        //{
        //    bool bReturn = true;
        //    using (SqlCommand oCmd = new SqlCommand(, oSqlConn))
        //    {
        //        oCmd.CommandType = CommandType.StoredProcedure;
        //        oCmd.Parameters.Add("@sCondition", SqlDbType.VarChar).Value =
        //            string.Format("WHERE TerminalID='{0}'", _sTerminalId);

        //        if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
        //        using (SqlDataReader read = oCmd.ExecuteReader())
        //            if (!read.HasRows)
        //                bReturn = false;
        //    }
        //    return bReturn;
        //}

        static void ProcessExit(object sender, EventArgs e)
        {
            //this.Close();
        }
        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                sConnString = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();
            }
            catch (Exception)
            {
                oSqlTempConn = null;
            }

            return oSqlTempConn;
        }

        protected static void AddContentStatus(string _sFilename)
        {
            using (StreamReader sr = new StreamReader(_sFilename))
            {
                string sStatusInit;
                while ((sStatusInit = sr.ReadLine()) != null)
                    ltContentAui.Add(sStatusInit);
            }
        }
        protected static void Write2File(string sFileName, string sValue, bool bNewLine)
        {
            try
            {
                using (FileStream oFileStream = new FileStream(sFileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter oStreamWriter = new StreamWriter(oFileStream))
                    {
                        oStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        if (bNewLine) oStreamWriter.WriteLine(sValue);
                        else oStreamWriter.Write(sValue);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected static string sGetMMMddyyyyhhmmsstt()
        {
            return DateTime.Now.ToString("MMM dd, yyyy, hh:mm:ss.fff tt ", ciUSFormat);
        }

        protected static void InitStatusNotOpen()
        {
            string[] lineOfContents = File.ReadAllLines("StatusNotOpen.txt");
            foreach (var line in lineOfContents)
            {
                string[] tokens = line.Split(',');
                sStatus = (tokens[0]);
            }
        }
    }
}
