using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using InSysClass;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Web.Configuration;

namespace ipXML
{
    public class SqlEMS_XML
    {
        protected string sSenderId;

        protected List<string> ltStringColumnXML = new List<string>();
        protected List<string> ltStringColumnXMLTerminal = new List<string>();
        protected List<SqlEMS_XML.ValueXML> ltObjValueXML = new List<SqlEMS_XML.ValueXML>();

        public class ValueXML
        {
            List<string> sValueXML = new List<string>();

            public ValueXML(List<string> _sValueXML)
            {
                sValueXML = _sValueXML;
            }

            public List<string> Value
            {
                get { return sValueXML; }
            }
        }

        protected SqlConnection oSqlConn;
        protected EMS_XML oXMLClass;

        public SqlEMS_XML(string _sConnString, string _sSenderId, EMS_XML _oXMLClass) : this((new SqlConnection(_sConnString)), _sSenderId, _oXMLClass) { }

        public SqlEMS_XML(SqlConnection _oSqlConn, string _sSenderId, EMS_XML _oXMLClass)
        {
            oSqlConn = _oSqlConn;
            oSqlConn.Open();
            sSenderId = _sSenderId;
            oXMLClass = new EMS_XML();
            oXMLClass = _oXMLClass;
            InitiateColumns();
            InitiateValue();
        }

        public SqlEMS_XML(string _sConnString, EMS_XML _oXmlClass) : this((new SqlConnection(_sConnString)), _oXmlClass) { }

        public SqlEMS_XML(SqlConnection _oSqlConn, EMS_XML _oXMLClass)
        {
            oSqlConn = _oSqlConn;
            oSqlConn.Open();
            oXMLClass = new EMS_XML();
            oXMLClass = _oXMLClass;
            sSenderId = oXMLClass.Attribute.sSenderID;
            InitiateColumns();
            InitiateValue();
        }

        public List<string> ListColumnXML
        {
            get { return ltStringColumnXML; }
        }

        public List<SqlEMS_XML.ValueXML> ListValueXML
        {
            get { return ltObjValueXML; }
        }

        protected void InitiateColumns()
        {
            ltStringColumnXML = new List<string>();
            FillListColumn(oXMLClass.Attribute);
            
            if (oXMLClass.Attribute.sTypes.ToLower().Contains("add"))
            {
                if (oXMLClass.Data.ltTerminal.Count > 0 &&
                    oXMLClass.Data.ltTerminal[0].ColumnValueXML.Count > 0)
                    foreach (EMS_XML.DataClass.TerminalClass.ColumnValue oColValue in oXMLClass.Data.ltTerminal[0].ColumnValueXML)
                    {
                        if (oColValue != null && !string.IsNullOrEmpty(oColValue.sColName))
                        {
                            ltStringColumnXML.Add(oColValue.sColName);
                            ltStringColumnXMLTerminal.Add(oColValue.sColName);
                        }
                    }
                FillListColumn(oXMLClass.Data.Terminal.Result);
            }
            else if (oXMLClass.Attribute.sTypes.ToLower().Contains("delete"))
                ltStringColumnXML.Add("Terminal_INIT");
            else if (oXMLClass.Attribute.sTypes.ToLower().Contains("inq"))
            {
                FillListColumn(oXMLClass.Data.Filter);
            }
        }

        protected void FillListColumn(object oObj)
        {
            PropertyInfo[] pArrayInfo = oObj.GetType().GetProperties();
            foreach (PropertyInfo pInfo in pArrayInfo)
                if (!pInfo.PropertyType.Name.ToLower().Contains("class"))
                    ltStringColumnXML.Add(pInfo.Name);
        }

        protected void InitiateValue()
        {
            if (oXMLClass.Attribute.sTypes.ToLower().Contains("add") || oXMLClass.Attribute.sTypes.ToLower().Contains("delete"))
            {
                foreach (EMS_XML.DataClass.TerminalClass oTerminal in oXMLClass.Data.ltTerminal)
                {
                    EMS_XML oTempXML = new EMS_XML();

                    oTempXML.Attribute = oXMLClass.Attribute;
                    oTempXML.Data.Terminal = oTerminal;
                    oTempXML.Data.Terminal.Result = oTerminal.Result;
                    oTempXML.Data.Filter = new EMS_XML.DataClass.FilterClass();

                    FillValue(oTempXML);
                }
            }
            else if (oXMLClass.Attribute.sTypes.ToLower().Contains("inq"))
            {
                foreach (EMS_XML.DataClass.FilterClass oFilter in oXMLClass.Data.ltFilter)
                {
                    EMS_XML oTempXML = new EMS_XML();
                    oTempXML.Attribute = oXMLClass.Attribute;
                    oTempXML.Data.Terminal = new EMS_XML.DataClass.TerminalClass();
                    oTempXML.Data.Filter = oFilter;

                    FillValue(oTempXML);
                }
            }
        }

        protected void FillValue(EMS_XML oXML)
        {
            PropertyInfo pInfo;
            object oObj;
            string sValue = "";

            List<string> sValueXML = new List<string>();
            SqlEMS_XML.ValueXML oTempValueXML = new SqlEMS_XML.ValueXML(sValueXML);
            foreach (string sColumn in ltStringColumnXML)
            {
                pInfo = oXML.Attribute.GetType().GetProperty(sColumn);
                oObj = oXML.Attribute;

                if (pInfo == null)
                {
                    pInfo = oXML.Data.Terminal.Result.GetType().GetProperty(sColumn);
                    if (pInfo == null)
                    {
                        pInfo = oXML.Data.Filter.GetType().GetProperty(sColumn);
                        if (pInfo == null)
                        {
                            sValue = oXML.Data.Terminal.sGetColumnValue(sColumn);
                            oObj = null;
                        }
                        else
                            oObj = oXML.Data.Filter;
                    }
                    else
                        oObj = oXML.Data.Terminal.Result;
                }

                if (oObj == null)
                    sValueXML.Add(sValue);
                else
                    sValueXML.Add(pInfo.GetValue(oObj, null).ToString());

                oTempValueXML = new SqlEMS_XML.ValueXML(sValueXML);
            }
            ltObjValueXML.Add(oTempValueXML);
        }

        protected string sGenerateInsertRow(List<ValueXML> oValueXML)
        {
            string sInsertRow = "";
            foreach (ValueXML oValXml in oValueXML)
            {
                sInsertRow += sGenerateRow(oValXml.Value);

                if (oValueXML.FindIndex(delegate(ValueXML oValXmlTemp) { return oValXmlTemp.Equals(oValXml); }) < oValueXML.Count - 1)
                    sInsertRow += " UNION ALL ";
            }
            return sInsertRow;
        }

        protected string sGenerateColumn()
        {
            string sCreateColumn = "";

            for (int iIndex = 0; iIndex < ltStringColumnXML.Count; iIndex++)
            {
                sCreateColumn += string.Format(" [{0}] VARCHAR(100)", ltStringColumnXML[iIndex]);
                if (iIndex + 1 < ltStringColumnXML.Count)
                    sCreateColumn += ",";
            }

            return sCreateColumn;
        }

        public bool IsCreateTable(ref string sErrMessage, ref EMS_XML _oEms_Xml)
        {
            sErrMessage = "";//"Create Table Success";
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(EMSCommon.XML.sSPCreateTable, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTableName", SqlDbType.VarChar).Value = sSenderId;
                    oSqlCmd.Parameters.Add("@sColumnDesc", SqlDbType.VarChar).Value = sGenerateColumn();
                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn.Close();
                        oSqlConn.Open();
                    }

                    using (SqlDataReader oReader = oSqlCmd.ExecuteReader())
                    {
                        if (oReader.Read())
                        {
                            sErrMessage = oReader[0].ToString();
                            using (DataTable dtTemp = new DataTable())
                            {
                                dtTemp.Load(oReader);
                                _oEms_Xml = XmlReturnResult("", dtTemp, EMSCommonClass.XMLResponsType.RESULT);
                            }
                        }
                        oReader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                sErrMessage = ex.Message;
            }
            return string.IsNullOrEmpty(sErrMessage) ? true : false;
        }

        protected string sGenerateRow(List<string> sInsertValue)
        {
            string sCreateRow = " SELECT ";

            for (int iIndex = 0; iIndex < sInsertValue.Count; iIndex++)
            {
                sCreateRow += string.Format(" '{0}'",
                    !string.IsNullOrEmpty(sInsertValue[iIndex]) ? sInsertValue[iIndex].Replace("'", "''") : "");
                {
                    if (iIndex + 1 < sInsertValue.Count)
                        sCreateRow += ",";
                }
            }

            return sCreateRow;
        }

        public bool IsInsertRow(ref string sErrMsg, ref EMS_XML _oEms_Xml)
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(EMSCommon.XML.sSPInsertTable, oSqlConn))
                {
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTableName", SqlDbType.VarChar).Value = sSenderId;
                    oSqlCmd.Parameters.Add("@sRowValue", SqlDbType.VarChar).Value = sGenerateInsertRow(ltObjValueXML);

                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn.Close();
                        oSqlConn.Open();
                    }

                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        if (oRead.Read())
                        {
                            sErrMsg = oRead[0].ToString();
                            string sFileResult = string.Format(@"{0}\{1}.xml", EMSCommonClass.DirectoryResult, _oEms_Xml.Attribute.sSenderID);
                            using (DataTable dtTemp = new DataTable())
                            {
                                dtTemp.Load(oRead);
                                _oEms_Xml = XmlReturnResult(sFileResult, dtTemp, EMSCommonClass.XMLResponsType.RESULT);
                            }
                        }
                        oRead.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                sErrMsg = ex.Message;
            }
            return string.IsNullOrEmpty(sErrMsg) ? true : false;
        }

        public bool IsXMLAddUpdate(string sSenderId, ref string sErrMsg, ref EMS_XML oXml, string _sFileResult)
        {
            EMS_Process oProcess = new EMS_Process(oXml, oSqlConn);
            oXml = oProcess.emsAddUpdate();
            if (!string.IsNullOrEmpty(_sFileResult))
                oXml.CreateXmlFile(_sFileResult, true); //matikan dulu
            sErrMsg = oProcess.Error; 
            return string.IsNullOrEmpty(sErrMsg) ? true : false;
        }

        public bool IsXMLDelete(string sSenderId, ref string sErrMsg, ref EMS_XML oXml, string _sFileResult)
        {
            EMS_Process oProcess = new EMS_Process(oXml, oSqlConn);
            oXml = oProcess.emsDelete();
            if (!string.IsNullOrEmpty(_sFileResult)) oXml.CreateXmlFile(_sFileResult, true);
            sErrMsg = oProcess.Error;
            return string.IsNullOrEmpty(sErrMsg) ? true : false;
        }

        //add log -tatang
        protected static string sDirLog;
        public static void WriteLog(string sMessage)
        {
            string sFilename = string.Format(@"{0}\{1:yyyyMMdd}.log", sDirLog, DateTime.Now);
            StreamWriter sw = new StreamWriter(sFilename, true);
            sw.WriteLine(string.Format("[{0:yyyy MMM dd, hh:mm:ss tt}] : {1}", DateTime.Now, sMessage));
            sw.Close();
            sw.Dispose();
        }

        public bool IsXMLInquiry(string sSenderId, ref string sErrMsg, ref EMS_XML oXml, string _sFileResult)
        {
            EMS_Process oProcess = new EMS_Process(oXml, oSqlConn);
            oXml = oProcess.emsInquiry();
            if (!string.IsNullOrEmpty(_sFileResult)) oXml.CreateXmlFile(_sFileResult, true);
            sErrMsg = oProcess.Error;

            //add log -tatang  if one treath cek, dont for active
            //sDirLog = WebConfigurationManager.AppSettings["Path"].ToString() + "LOG";
            //sErrMsg = string.Format("CreateXmlFile : {0}\n{1}", _sFileResult, "Position "+ sErrMsg);
            //WriteLog(sErrMsg);

            return string.IsNullOrEmpty(sErrMsg) ? true : false;
        }

        public void XMLInquiryAllProfile(string sSenderId)
        {
            using (SqlCommand oSqlCmd = new SqlCommand(EMSCommon.XML.sSPXMLInquiryAllProfile, oSqlConn))
            {
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.CommandTimeout = 60000;
                oSqlCmd.Parameters.Add("@sTableName", SqlDbType.VarChar).Value = sSenderId;
                if (oSqlConn.State != ConnectionState.Open)
                {
                    oSqlConn.Close();
                    oSqlConn.Open();
                }
                int iResult = oSqlCmd.ExecuteNonQuery();
            }
        }

        protected bool IsXMLQuerySuccess(string sSenderId, ref string sErrMsg, ref EMS_XML _oXml,
            string _sFileResult, string sQuery, EMSCommonClass.XMLResponsType response)
        {
            try
            {
                using (SqlCommand oSqlCmd = new SqlCommand(sQuery, oSqlConn))
                {
                    oSqlCmd.CommandTimeout = 60000;
                    oSqlCmd.CommandType = CommandType.StoredProcedure;
                    oSqlCmd.Parameters.Add("@sTableName", SqlDbType.VarChar).Value = sSenderId;
                    if (oSqlConn.State != ConnectionState.Open)
                    {
                        oSqlConn.Close();
                        oSqlConn.Open();
                    }
                    using (SqlDataReader oRead = oSqlCmd.ExecuteReader())
                    {
                        DataTable dtTemp = new DataTable();
                        if (oRead.HasRows)
                        {
                            dtTemp.Load(oRead);
                            foreach (DataRow row in dtTemp.Rows)
                            {
                                string sTemp = null;
                                foreach (DataColumn col in dtTemp.Columns)
                                    sTemp += row[col] + ", ";
                            }

                            _oXml = XmlReturnResult(_sFileResult, dtTemp, response);
                        }
                        dtTemp.Dispose();
                        sErrMsg = "";
                    }
                }
            }
            catch (Exception ex)
            {
                sErrMsg = "[SqlEms_Xml.IsXMlQuerySuccess] TIDAK BERHASIL : " + ex.Message;
                EMSCommonClass.WriteLog(sErrMsg);
            }
            return string.IsNullOrEmpty(sErrMsg) ? true : false;
        }

        protected List<string> ltGetXMLColumnMap(string sVersion)
        {
            List<string> ltTemp = new List<string>();
            DataTable dtTemp = EMSCommonClass.dtGetEmsXmlMap(oSqlConn, sVersion);
            if (dtTemp != null)
            {
                ltTemp.Add("Software_Version");
                foreach (DataRow row in dtTemp.Rows)
                    ltTemp.Add(row["EMSXMLColumn"].ToString());
            }
            return ltTemp;
        }

        public bool IsXMLColumnMapValid(string sVersion, ref string sColumnError)
        {
            bool IsReturn = true;
            List<string> ltTemp = ltGetXMLColumnMap(sVersion);
            if (ltTemp.Count != ltStringColumnXMLTerminal.Count)
                IsReturn = false;
            else
            {
                for (int iIndex = 0; iIndex < ltStringColumnXMLTerminal.Count; iIndex++)
                {
                    if (ltTemp[iIndex] != ltStringColumnXMLTerminal[iIndex])
                    {
                        IsReturn = false;
                        sColumnError = ltStringColumnXMLTerminal[iIndex];
                        break;
                    }
                }
            }
            return IsReturn;
        }

        //protected void WriteResult(string _sFileResult, DataTable dtResult, EMSCommonClass.XMLResponsType response)
        //{
        //    EMS_XML oResultEms = new EMS_XML();
        //    oResultEms.Attribute = oXMLClass.Attribute;

        //    if (response == EMSCommonClass.XMLResponsType.Result)
        //    {
        //        foreach (DataRow row in dtResult.Rows)
        //        {
        //            oResultEms.Data.Terminal.AddColumnValue("Terminal_Init", oXMLClass.Data.Terminal.sGetColumnValue("Terminal_Init".ToUpper()));

        //            oResultEms.Data.Terminal.AddColumnValue("Process_Type", row["IsAdd"].ToString() == "1" ? "ADD" : "UPDATE");
        //            oResultEms.Data.Terminal.Result.sResultFieldName = "Terminal_Init";
        //            oResultEms.Data.Terminal.Result.sResultCode = row["ResultCode"].ToString();
        //            oResultEms.Data.Terminal.Result.sResultDesc = row["ResultDesc"].ToString();
        //            oResultEms.Data.Terminal.Result.sTimeProcess = DateTime.Now.ToString("ddMMyyyyhhmmss");

        //            oResultEms.Data.doAddTerminal(oResultEms.Data.Terminal);
        //        }
        //    }
        //    else
        //    {
        //        oResultEms.Data.Terminal.AddColumnValue("Software_Version", oXMLClass.Attribute.sVersions);

        //        oResultEms.FillTerminalClass(dtResult);
        //        DeleteResultTable(oResultEms.Attribute.sSenderID);
        //    }
        //    oResultEms.CreateXmlFile(_sFileResult);
        //}

        protected EMS_XML XmlReturnResult(string _sFileResult, DataTable dtResult, EMSCommonClass.XMLResponsType response)
        {
            EMS_XML oResultEms = new EMS_XML();
            oResultEms.Attribute = oXMLClass.Attribute;
            oResultEms.Attribute.sTypes = response.ToString();

            if (response == EMSCommonClass.XMLResponsType.RESULT)
            {
                foreach (DataRow row in dtResult.Rows)
                {
                    EMS_XML.DataClass.TerminalClass oTerm = new EMS_XML.DataClass.TerminalClass();
                    oTerm.AddColumnValue("Terminal_Init", row["TerminalId"].ToString());

                    if (row["IsAddUpdDel"].ToString() == "1")
                        oTerm.AddColumnValue("Process_Type", "ADD");
                    else if (row["IsAddUpdDel"].ToString() == "0")
                        oTerm.AddColumnValue("Process_Type", "UPDATE");
                    else if (row["IsAddUpdDel"].ToString() == "2")
                        oTerm.AddColumnValue("Process_Type", "DELETE");

                    if (row["ResultCode"].ToString() != "000")
                    {
                        oTerm.AddColumnValue("Response_Code", "001");

                        oTerm.Result.sResultFieldName = "Terminal_Init";
                        oTerm.Result.sResultCode = row["ResultCode"].ToString();
                        oTerm.Result.sResultDesc = row["ResultDesc"].ToString();
                        oTerm.Result.sTimeProcess = DateTime.Now.ToString("ddMMyyyyhhmmss");
                    }
                    else
                        oTerm.AddColumnValue("Response_Code", "000");
                    oResultEms.Data.doAddTerminal(oTerm);
                }
            }
            else
            {
                oResultEms.FillTerminalClass(dtResult);
                DeleteResultTable(oResultEms.Attribute.sSenderID);
            }
            if (!string.IsNullOrEmpty(_sFileResult)) oResultEms.CreateXmlFile(_sFileResult, true);
            return oResultEms;
        }

        protected void DeleteResultTable(string sTableName)
        {
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPDeleteTableResult, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTableName", SqlDbType.VarChar).Value = sTableName;
                if (oSqlConn.State != ConnectionState.Open)
                    oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }

        public void DeleteSenderTable()
        {
            using (SqlCommand oCmd = new SqlCommand(EMSCommon.XML.sSPDeleteTableSender, oSqlConn))
            {
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sSenderID", SqlDbType.VarChar).Value = sSenderId;
                if (oSqlConn.State != ConnectionState.Open)
                    oSqlConn.Open();
                oCmd.ExecuteNonQuery();
            }
        }
    }
}