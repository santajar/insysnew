using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.Reflection;
using System.IO;
using System.Threading;

namespace ipXML
{
    public class EMS_XML
    {
        protected AttributeClass oAttributeClass;
        protected DataClass oDataClass;

        public EMS_XML()
        {
            InitClasses();
        }

        public EMS_XML(EMS_XML oXMLInit)
            : this()
        {
            InitiateClass(oXMLInit);
        }

        public EMS_XML(List<DataTable> dtXML)
        {
            InitClasses();
            FillClasses(dtXML);
        }

        protected void InitClasses()
        {
            //EMSCommonClass.WriteLog("EMS_XML.InitClasses");

            oAttributeClass = new AttributeClass();
            oDataClass = new DataClass();
        }

        public void FillClasses(List<DataTable> dtXML)
        {
            //EMSCommonClass.WriteLog("EMS_XML.FillClasses");

            FillAttributeClass(dtFindTable("Attribute", dtXML));

            //EMSCommonClass.WriteLog("FillClasses : " + oAttributeClass.sTypes);

            if (oAttributeClass.sTypes == EMSCommonClass.XMLRequestType.INQ.ToString()
                || oAttributeClass.sTypes == EMSCommonClass.XMLRequestType.INQINIT.ToString())
            {
                //EMSCommonClass.WriteLog("FillClasses : Inq");

                FillFilterClass(dtFindTable("Filter", dtXML));
            }
            else if (oAttributeClass.sTypes == EMSCommonClass.XMLRequestType.DELETE.ToString())
            {
                FillTerminalClass(dtFindTable("Terminal", dtXML));
            }
            else if (oAttributeClass.sTypes.Replace('/', '_') == EMSCommonClass.XMLRequestType.ADD_UPDATE.ToString())
            {
                FillTerminalClass(dtFindTable("Terminal", dtXML));
            }
        }

        protected DataTable dtFindTable(string sTableName, List<DataTable> dtXMLTemp)
        {
            //EMSCommonClass.WriteLog("EMS_XML.dtFindTable");

            DataTable dtTemp = new DataTable();
            dtTemp = dtXMLTemp.Find(delegate(DataTable dt) { return dt.TableName == sTableName; });
            return dtTemp;
        }

        protected void FillAttributeClass(DataTable dtTemp)
        {
            //EMSCommonClass.WriteLog("EMS_XML.FillAttributeClass");

            //oAttributeClass.sSenderID = dtTemp.Rows[0]["Sender_Id"].ToString();
            //oAttributeClass.sVersions = dtTemp.Rows[0]["Version"].ToString();
            //oAttributeClass.sTypes = dtTemp.Rows[0]["Type"].ToString();
            //oAttributeClass.sSenders = dtTemp.Rows[0]["Sender"].ToString();

            //revision tatang
            oAttributeClass.sSenderID = dtTemp.Rows[0][3].ToString();
            oAttributeClass.sVersions = dtTemp.Rows[0][0].ToString();
            oAttributeClass.sTypes = dtTemp.Rows[0][1].ToString();
            oAttributeClass.sSenders = dtTemp.Rows[0][2].ToString();

            //

            //EMSCommonClass.WriteLog("FillAttributeClass : " + dtTemp.Rows[0]["Sender_Id"].ToString());
            //EMSCommonClass.WriteLog("FillAttributeClass : " + dtTemp.Rows[0]["Version"].ToString());
            //EMSCommonClass.WriteLog("FillAttributeClass : " + dtTemp.Rows[0]["Type"].ToString());
            //EMSCommonClass.WriteLog("FillAttributeClass : " + dtTemp.Rows[0]["Sender"].ToString());
        }

        public void FillTerminalClass(DataTable dtTemp)
        {
            //EMSCommonClass.WriteLog("EMS_XML.FillTerminalClass");

            foreach (DataRow row in dtTemp.Rows)
            {
                DataClass.TerminalClass oTerminal = new DataClass.TerminalClass();
                foreach (DataColumn col in dtTemp.Columns)
                {
                    if (col.ColumnMapping != MappingType.Hidden)
                        oTerminal.AddColumnValue(col.ColumnName, row[col].ToString());
                }
                oDataClass.doAddTerminal(oTerminal);
            }
        }

        protected void FillFilterClass(DataTable dtTemp)
        {
            //EMSCommonClass.WriteLog("EMS_XML.FillFilterClass Start");

            foreach (DataRow row in dtTemp.Rows)
            {
                DataClass.FilterClass oFilter = oDataClass.Filter;
                oFilter.sFilterFieldName = row["Field_Name"].ToString();
                oFilter.sValues = row["Values"].ToString();
                oDataClass.doAddFilter(oFilter);
                //EMSCommonClass.WriteLog("FillFilterClass : " + row["Field_Name"].ToString());
            }

            //EMSCommonClass.WriteLog("FillFilterClass : End");
        }

        protected void InitiateClass(EMS_XML oXMLInit)
        {
            //EMSCommonClass.WriteLog("EMS_XML.InitiateClass");

            oAttributeClass = oXMLInit.oAttributeClass;
            oDataClass = oXMLInit.oDataClass;
        }

        public AttributeClass Attribute
        {
            set { oAttributeClass = value; }
            get { return oAttributeClass; }
        }

        public DataClass Data
        {
            set { oDataClass = value; }
            get { return oDataClass; }
        }

        public class AttributeClass
        {
            protected string sVersion = "";
            protected string sType = "";
            protected string sSender = "";
            protected string sSender_ID = "";

            #region "Setter - Getter"
            public string sVersions
            {
                set { sVersion = value; }
                get { return sVersion; }
            }

            public string sTypes
            {
                set { sType = value; }
                get { return sType; }
            }

            public string sSenders
            {
                set { sSender = value; }
                get { return sSender; }
            }

            public string sSenderID
            {
                set { sSender_ID = value; }
                get { return sSender_ID; }
            }
            #endregion

            public AttributeClass()
            {

            }
        }

        public class DataClass
        {

            protected TerminalClass oTerminalClass;
            protected FilterClass oFilterClass;

            protected List<TerminalClass> oListTerminal;
            protected List<FilterClass> oListFilter;

            public TerminalClass Terminal
            {
                set { oTerminalClass = value; }
                get { return oTerminalClass; }
            }

            public FilterClass Filter
            {
                set { oFilterClass = value; }
                get { return oFilterClass; }
            }

            public List<TerminalClass> ltTerminal
            {
                get { return oListTerminal; }
            }

            public List<FilterClass> ltFilter
            {
                get { return oListFilter; }
            }

            public DataClass()
            {
                InitiateClass();
            }

            protected void InitiateClass()
            {
                oListTerminal = new List<TerminalClass>();
                oListFilter = new List<FilterClass>();

                oTerminalClass = new TerminalClass();
                oFilterClass = new FilterClass();
            }

            public void doAddTerminal(TerminalClass oTempTerminalClass)
            {
                oListTerminal.Add(oTempTerminalClass);
            }

            public void doAddFilter(FilterClass oTempFilterClass)
            {
                oListFilter.Add(oTempFilterClass);
            }

            public void doRemoveTerminal(TerminalClass oTempTerminalClass)
            {
                oListTerminal.Remove(oTempTerminalClass);
            }

            public void doRemoveFilter(FilterClass oTempFilterClass)
            {
                oListFilter.Remove(oTempFilterClass);
            }

            public bool isFindTerminal(TerminalClass oTempTermnalClass)
            {
                return oTempTermnalClass.sGetColumnValue("Terminal_Init") == oTerminalClass.sGetColumnValue("Terminal_Init");
            }

            public bool isFindResult(FilterClass oTempFilterClass)
            {
                return (oTempFilterClass.sFilterFieldName == oFilterClass.sFilterFieldName) &&
                        (oTempFilterClass.sValues == oFilterClass.sValues);
            }

            public class TerminalClass
            {
                public class ColumnValue
                {
                    protected string sColumnName;
                    protected string sColumnValue;

                    public ColumnValue()
                    {
                        sColName = "";
                        sColValue = "";
                    }

                    public string sColName
                    {
                        set { sColumnName = value; }
                        get { return sColumnName; }
                    }

                    public string sColValue
                    {
                        set { sColumnValue = value; }
                        get { return sColumnValue; }
                    }
                }

                public class ResultClass
                {
                    protected string sField_Name = "";
                    protected string sResult_Code = "";
                    protected string sResult_Description = "";
                    protected string sTime_Process = "";

                    #region "Setter - Getter"
                    public string sResultFieldName
                    {
                        set { sField_Name = value; }
                        get { return sField_Name; }
                    }

                    public string sResultCode
                    {
                        set { sResult_Code = value; }
                        get { return sResult_Code; }
                    }

                    public string sResultDesc
                    {
                        set { sResult_Description = value; }
                        get { return sResult_Description; }
                    }

                    public string sTimeProcess
                    {
                        set { sTime_Process = value; }
                        get { return sTime_Process; }
                    }
                    #endregion

                    public ResultClass()
                    {
                    }
                }

                protected List<ColumnValue> ltColumnValue = new List<ColumnValue>();

                public List<ColumnValue> ColumnValueXML
                {
                    get { return ltColumnValue; }
                }

                protected ResultClass oResultClass;

                public ResultClass Result
                {
                    set { oResultClass = value; }
                    get { return oResultClass; }
                }

                public TerminalClass()
                {
                    InitiateClass();
                }

                protected void InitiateClass()
                {
                    oResultClass = new ResultClass();
                }

                public void AddColumnValue(string _sColumnName, string _sColumnValue)
                {
                    ColumnValue oColValTemp = new ColumnValue();
                    oColValTemp.sColName = _sColumnName;
                    oColValTemp.sColValue = _sColumnValue;

                    ltColumnValue.Add(oColValTemp);
                }

                public void UpdateColumnValue(string _sColumnName, string _sColumnValue)
                {
                    ColumnValue oColValueOld = FindColumnValue(_sColumnName);
                    ltColumnValue.Remove(oColValueOld);

                    ColumnValue oColValTemp = new ColumnValue();
                    oColValTemp.sColName = _sColumnName;
                    oColValTemp.sColValue = _sColumnValue;

                    ltColumnValue.Add(oColValTemp);
                }

                public string sGetColumnValue(string sColumnName)
                {
                    ColumnValue oColValTemp = FindColumnValue(sColumnName.ToUpper());
                    return (oColValTemp != null) ? oColValTemp.sColValue : "";
                }

                protected ColumnValue FindColumnValue(string sColumnName)
                {
                    ColumnValue oColValTemp = new ColumnValue();
                    oColValTemp = ltColumnValue.Find(delegate(ColumnValue colval) { return colval.sColName.ToUpper() == sColumnName.ToUpper(); });
                    return oColValTemp;
                }
            }

            public class FilterClass
            {
                protected string sField_Name = "";
                protected string sValue = "";

                #region "Setter - Getter"
                public string sFilterFieldName
                {
                    set { sField_Name = value; }
                    get { return sField_Name; }
                }

                public string sValues
                {
                    set { sValue = value; }
                    get { return sValue; }
                }
                #endregion

                public FilterClass()
                {
                }
            }
        }

        #region"Create XML File"
        public void CreateXmlFile(string sFilename, bool IsResult)
        {
            string sResultFilename = IsResult ? sFilename.Replace(".xml", "_result.xml") : sFilename;
            StreamWriter sw = new StreamWriter(sResultFilename);
            sw.AutoFlush = true;
            sw.Write(sWriteXMLHeader);
            sw.Write(string.Format("{0}\n", sWriteXMLTag("TMScript", true, 0)));
            sw.Write(sWriteXMLAttribute());
            sw.Write(sWriteXMLData(IsResult));
            sw.Write(string.Format("{0}\n", sWriteXMLTag("TMScript", false, 0)));
            sw.Close();
            sw.Dispose();
        }

        protected string sWriteXMLHeader
        {
            get { return "<?xml version=\"1.0\"?>\n"; }
        }

        protected string sWriteXMLTag(string sTag, bool IsOpenTag, int iLevel)
        {
            return string.Format("{2}<{0}{1}>", IsOpenTag ? "" : "/", sTag,
                iLevel != 0 ? "".PadLeft(iLevel * 5, ' ') : "");
        }

        protected string sWriteXMLColumnValue(string sXMLColumn, string sXMLValue, int iLevel)
        {
            return string.Format("{0}{1}{2}\n",
                sWriteXMLTag(sXMLColumn, true, iLevel),
                sXMLValue,
                sWriteXMLTag(sXMLColumn, false, 0));
        }

        protected string sWriteXMLAttribute()
        {
            string sXmlTemp = "";
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Attribute", true, 1));
            sXmlTemp += sWriteXMLColumnValue("Version", oAttributeClass.sVersions, 2);
            sXmlTemp += sWriteXMLColumnValue("Type", oAttributeClass.sTypes, 2);
            sXmlTemp += sWriteXMLColumnValue("Sender", oAttributeClass.sSenders, 2);
            sXmlTemp += sWriteXMLColumnValue("Sender_ID", oAttributeClass.sSenderID, 2);
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Attribute", false, 1));
            return sXmlTemp;
        }

        protected string sWriteXMLData(bool _IsResult)
        {
            string sXmlTemp = "";
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Data", true, 1));
            sXmlTemp += sWriteXMLTerminals(_IsResult);
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Data", false, 1));
            return sXmlTemp;
        }

        protected string sWriteXMLTerminals(bool _IsResult)
        {
            string sXmlTemp = "";
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Terminals", true, 2));
            sXmlTemp += _IsResult ? sWriteXMLTerminalResult() : sWriteXMLTerminalSender();
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Terminals", false, 2));
            return sXmlTemp;
        }

        protected string sWriteXMLTerminalSender()
        {
            string sXmlTemp = "";
            foreach (EMS_XML.DataClass.TerminalClass oTerminal in oDataClass.ltTerminal)
            {
                sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Terminal", true, 3));
                foreach (DataClass.TerminalClass.ColumnValue oColValTemp in oDataClass.Terminal.ColumnValueXML)
                    sXmlTemp += sWriteXMLColumnValue(oColValTemp.sColName, oColValTemp.sColValue, 4);
                foreach (DataClass.TerminalClass.ColumnValue oColValTemp in oTerminal.ColumnValueXML)
                    sXmlTemp += sWriteXMLColumnValue(oColValTemp.sColName, oColValTemp.sColValue, 4);
                sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Terminal", false, 3));
            }
            return sXmlTemp;
        }

        protected string sWriteXMLTerminalResult()
        {
            string sXmlTemp = "";
            foreach (EMS_XML.DataClass.TerminalClass oTerminal in oDataClass.ltTerminal)
            {
                sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Terminal", true, 3));
                //if (oAttributeClass.sTypes == EMSCommonClass.XMLRequestType.INQ.ToString())
                if (oAttributeClass.sTypes.Contains(EMSCommonClass.XMLRequestType.INQ.ToString()))
                {
                    foreach (DataClass.TerminalClass.ColumnValue oColValTemp in oDataClass.Terminal.ColumnValueXML)
                        sXmlTemp += sWriteXMLColumnValue(oColValTemp.sColName, oColValTemp.sColValue, 4);
                    foreach (DataClass.TerminalClass.ColumnValue oColValTemp in oTerminal.ColumnValueXML)
                        sXmlTemp += sWriteXMLColumnValue(oColValTemp.sColName, oColValTemp.sColValue, 4);
                }
                else
                {
                    sXmlTemp += sWriteXMLColumnValue("Terminal_Init", oTerminal.sGetColumnValue("Terminal_Init"), 4);
                    sXmlTemp += sWriteXMLColumnValue("Process_Type", oTerminal.sGetColumnValue("Process_Type"), 4);
                    sXmlTemp += sWriteXMLColumnValue("Response_Code", oTerminal.sGetColumnValue("Response_Code"), 4);
                    sXmlTemp += sWriteXMLResults(oTerminal);
                }
                sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Terminal", false, 3));
            }
            return sXmlTemp;
        }

        protected string sWriteXMLResults(EMS_XML.DataClass.TerminalClass oTerminal)
        {
            string sXmlTemp = "";
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Results", true, 4));
            sXmlTemp += sWriteXMLResult(oTerminal);
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Results", false, 4));
            return sXmlTemp;
        }

        protected string sWriteXMLResult(EMS_XML.DataClass.TerminalClass oTerminal)
        {
            string sXmlTemp = "";
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Result", true, 5));
            sXmlTemp += sWriteXMLColumnValue("Field_Name", oTerminal.Result.sResultFieldName, 6);
            sXmlTemp += sWriteXMLColumnValue("Result_Code", oTerminal.Result.sResultCode, 6);
            sXmlTemp += sWriteXMLColumnValue("Result_Description", oTerminal.Result.sResultDesc, 6);
            sXmlTemp += sWriteXMLColumnValue("Time_Process", oTerminal.Result.sTimeProcess, 6);
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Result", false, 5));
            return sXmlTemp;
        }

        protected string sWriteXMLFilters()
        {
            string sXmlTemp = "";
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Filters", true, 2));
            sXmlTemp += sWriteXMLFilter();
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Filters", false, 2));
            return sXmlTemp;
        }

        protected string sWriteXMLFilter()
        {
            string sXmlTemp = "";
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Filter", true, 3));
            sXmlTemp += sWriteXMLColumnValue("Field_Name", oDataClass.Filter.sFilterFieldName, 4);
            sXmlTemp += sWriteXMLColumnValue("Values", oDataClass.Filter.sValues, 4);
            sXmlTemp += string.Format("{0}\n", sWriteXMLTag("Filter", false, 3));
            return sXmlTemp;
        }
        #endregion
    }
}