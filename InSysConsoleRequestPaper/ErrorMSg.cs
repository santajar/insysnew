using System;
using System.Collections.Generic;
using System.Text;

namespace InSysConsoleRequestPaper
{
    class ErrorMsg
    {
        public static string sNotAllow = "INIT NOT ALLOWED";
        public static string sInvalidTerminal = "INVALID TID";
        public static string sInvalidSN = "INVALID SN";
        public static string sInvalidAppVersion = "INVALID VERSI";
        public static string sInvalidMTI = "INVALID MTI";
        public static string sInvalidProcCode = "INVALID PROC. CODE";
        public static string sUnknown = "UNKNOWN";
    }
}
