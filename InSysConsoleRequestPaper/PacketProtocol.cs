﻿using InSysClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InSysConsoleRequestPaper
{
    public class PacketProtocol
    {
        byte[] arrbPacket;
        List<byte[]> ltarrbPacket = new List<byte[]>();
        int iTotalPacket;

        public PacketProtocol(byte[] _arrbPacket)
        {
            arrbPacket = new byte[_arrbPacket.Length];
            arrbPacket = _arrbPacket;

            ProcessPacket();
        }

        protected void ProcessPacket()
        {
            int iTotalLengthOfPacket = arrbPacket.Length;
            int iCurrentLengthOfPacket=0;

            string sTemp = CommonLib.sByteArrayToHexString(arrbPacket).Replace(" ", "");
            int iLengthOfPacket = CommonConsole.iISOLenReceive(sTemp.Substring(0, 4));
            iCurrentLengthOfPacket += 2;

            while (iCurrentLengthOfPacket < iTotalLengthOfPacket)
            {
                byte[] arrbTempPacket = new byte[iCurrentLengthOfPacket];
                Array.Copy(arrbPacket, iCurrentLengthOfPacket, arrbTempPacket, 0, iLengthOfPacket);
                ltarrbPacket.Add(arrbTempPacket);

                iCurrentLengthOfPacket += iLengthOfPacket;
                iLengthOfPacket = CommonConsole.iISOLenReceive(sTemp.Substring(iCurrentLengthOfPacket * 2, 4));
                iCurrentLengthOfPacket += 2;

                if (iCurrentLengthOfPacket == iTotalLengthOfPacket) break;
            }
        }

        public void GetPacketList(ref List<byte[]> _ltarrbPacket, ref int _iTotalPacket)
        {
            _ltarrbPacket = ltarrbPacket;
            _iTotalPacket = iTotalPacket;
        }
    }
}
