using System;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Security.AccessControl;
using InSysClass;
using System.Drawing;
using System.Windows.Forms;

namespace InSysConsoleAutoInitReport
{
    /// <summary>
    /// Type of objects used in application
    /// </summary>
    enum ObjectType
    {
        COMBOBOX = 1,
        NULL,
        RADIOBUTTON,
        DATETIMEPICKER,
        TEXTBOX
    }

    /// <summary>
    /// Type of error when showing error message
    /// </summary>
    enum ErrorType
    {
        ERROR = 1,
        WARNING
    }

    /// <summary>
    /// Type of Command of EMS
    /// </summary>
    enum EmsTypeCommand
    {
        A,  //AddUpdate,
        D,  //Delete
        I,  //Inquiry
    }
    /// <summary>
    /// Type of Form
    /// </summary>
    enum FormType
    {
        terminal = 1,
        acquirer = 2,
        issuer = 3,
        card = 4,
        addcardterminal = 20,
        relation = 5,
        tle = 6,
        ems = 7,
        loyaltypool = 8,
        loyaltyprod = 9,
        gprs = 10,
        pinpad = 11,
        currency = 12,
        remotedownload = 13,
        aid=14,
        capk=15,
        bankcode=16,
        productcode=17,
        keymanagement=18,
        EMVManagement = 19,
        CustomMenu = 20,
        promomanagement = 21,
       
    }
    /// <summary>
    /// Type of tag
    /// </summary>
    enum TagType
    {
        de,
        dc,
        aa,
        ae,
        ac,
        ad,
        ai,
        pk,
        tl,
        gp,
        rm,
        pm,
    }

    enum GroupRegionSnType
    {
        Group = 1,
        Region,
        Terminal,
    }

    enum ActionType
    {
        Create = 1,
        Read,
        Update = 2,
        Delete
    }

    enum ScheduleType
    {
        Daily = 1,
        Weekly,
        Monthly,
    }

    static class UserData
    {
        private static string _sUserID;        
        /// <summary>
        /// Get or Set UserID from login
        /// </summary>
        public static string sUserID
        {
            set { _sUserID = value; }
            get { return _sUserID; }
        }

        private static string _sUserName;
        /// <summary>
        /// Get or Set UserName from login
        /// </summary>
        public static string sUserName
        {
            set { _sUserName = value; }
            get { return _sUserName; }
        }

        private static string _sPassword;
        /// <summary>
        /// Get or Set Password from login
        /// </summary>
        public static string sPassword
        {
            set { _sPassword = value; }
            get { return _sPassword; }
        }

        private static bool _isSuperUser;
        /// <summary>
        /// Get or Set if User is a SuperUser
        /// </summary>
        public static bool isSuperUser
        {
            set { _isSuperUser = value; }
            get { return _isSuperUser; }
        }

        private static string _sTerminalIdActive;
        public static string sTerminalIdActive
        {
            get { return _sTerminalIdActive; }
            set { _sTerminalIdActive = value; }
        }

        private static string _sGroupID;
        public static string sGroupID
        {
            get { return _sGroupID; }
            set { _sGroupID = value; }
        }
    }

    class CommonClass
    {
        /// <summary>
        /// Add new Audit Trail Log to database.
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : connection to database</param>
        /// <param name="sTID">string : Terminal ID where the changes occur</param>
        /// <param name="sUsrID">string : User who made the modification</param>
        /// <param name="sDBName">string : Database Name which contains the Terminal ID</param>
        /// <param name="sActDesc">string : The changes that occur </param>
        /// <param name="sActDetl">string : Details of the modification </param>
        public static void InputLog(SqlConnection oSqlConn, string sTID, string sUsrID, string sDBName, string sActDesc, string sActDetl)
        {
            try
            {
                SqlParameter[] oSqlParam = new SqlParameter[5];
                oSqlParam[0] = new SqlParameter("@sTerminalID", System.Data.SqlDbType.VarChar, 8);
                oSqlParam[0].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[0].Value = sTID;
                oSqlParam[1] = new SqlParameter("@sUserId", System.Data.SqlDbType.VarChar, 10);
                oSqlParam[1].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[1].Value = sUsrID;
                oSqlParam[2] = new SqlParameter("@sDatabaseName", System.Data.SqlDbType.VarChar, 50);
                oSqlParam[2].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[2].Value = sDBName;
                oSqlParam[3] = new SqlParameter("@sActionDesc", System.Data.SqlDbType.VarChar, sActDesc.Length);
                oSqlParam[3].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[3].Value = sActDesc;
                oSqlParam[4] = new SqlParameter("@sActionDetail", System.Data.SqlDbType.VarChar, sActDetl.Length);
                oSqlParam[4].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[4].Value = (string.IsNullOrEmpty(sActDetl)) ? null : sActDetl;
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAuditTrailInsert, oSqlConn);
                if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
                oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                oSqlCmd.Parameters.AddRange(oSqlParam);
                oSqlCmd.ExecuteNonQuery();

                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }

        public static void doWriteErrorFile(string sError)
        {
            if (!isHaveLogDir()) Directory.CreateDirectory(sGetErrorDirectory());  // If folder not found then crete the error folder
            CommonLib.Log(sGetErrorDirectory(), sError); // Write error to file
        }

        /// <summary>
        /// Add new Audit Trail Log to database when Super User access SuperUser's menu
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : connection to database</param>
        /// <param name="sTID">string : Terminal ID where the changes occur</param>
        /// <param name="sUsrID">string : User who made the modification</param>
        /// <param name="sDBName">string : Database Name which contains the Terminal ID</param>
        /// <param name="sActDesc">string : The changes that occur </param>
        /// <param name="sActDetl">string : Details of the modification </param>
        public static void InputLogSU(SqlConnection oSqlConn, string sTID, string sUsrID, string sDBName, string sActDesc, string sActDetl)
        {
            try
            {
                SqlParameter[] oSqlParam = new SqlParameter[5];
                oSqlParam[0] = new SqlParameter("@sTerminalID", System.Data.SqlDbType.VarChar, 8);
                oSqlParam[0].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[0].Value = sTID;
                oSqlParam[1] = new SqlParameter("@sUserId", System.Data.SqlDbType.VarChar, 10);
                oSqlParam[1].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[1].Value = sUsrID;
                oSqlParam[2] = new SqlParameter("@sDatabaseName", System.Data.SqlDbType.VarChar, 50);
                oSqlParam[2].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[2].Value = sDBName;
                oSqlParam[3] = new SqlParameter("@sActionDesc", System.Data.SqlDbType.VarChar, sActDesc.Length);
                oSqlParam[3].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[3].Value = sActDesc;
                oSqlParam[4] = new SqlParameter("@sActionDetail", System.Data.SqlDbType.VarChar, sActDetl.Length);
                oSqlParam[4].Direction = System.Data.ParameterDirection.Input;
                oSqlParam[4].Value = (string.IsNullOrEmpty(sActDetl)) ? null : sActDetl;
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPAuditTrailSUInsert, oSqlConn);
                if (oSqlConn.State != System.Data.ConnectionState.Open) oSqlConn.Open();
                oSqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                oSqlCmd.Parameters.AddRange(oSqlParam);
                oSqlCmd.ExecuteNonQuery();

                oSqlCmd.Dispose();
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
            }
        }


        /// <summary>   
        /// Show message box with Error message and refouces to object that causes the error.
        /// </summary>
        /// <param name="sErrMsg">string : Error message to display in message box</param>
        /// <param name="sender">object : the object that caused the error</param>
        /// <param name="objType">ObjectType : Type of the objects</param>
        /// <param name="errType">ErrorType : Type of the error</param>


        /// <summary>
        /// Generate the connection string for Sql Connection
        /// </summary>
        /// <param name="sHost">string : server of the database</param>
        /// <param name="sDatabaseName">string : database that is used</param>
        /// <param name="sUserID">string :UserId to connect to database</param>
        /// <param name="sPassword">string : Password to connect to database</param>
        /// <returns>string : Connection String for Sql Connection</returns>
        public static string sConnString(string sHost, string sDatabaseName, string sUserID, string sPassword)
        {
            return "server=" + sHost + ";uid=" + sUserID + ";pwd=" + sPassword + ";database=" + sDatabaseName + "; Connection Lifetime=0;";
        }

        /// <summary>
        /// Cut message from EDC
        /// </summary>
        /// <param name="sValue">string : message from EDC</param>
        /// <returns>string : result of cutting</returns>
        private static string sCuttingMsgFromEDC(string sValue)
        {
            return sValue.Substring(2, sValue.Length - 6);
        }

        /// <summary>
        /// Convert Character string to a byte array value.
        /// </summary>
        /// <param name="sValue">string : Chararcter string will be converted</param>
        /// <returns>byte array : Result from the conversion</returns>
        private static byte[] CharStringtoByteArray(string sValue)
        {
            int iLenString = sValue.Length;
            byte[] arrbBuffer = new byte[iLenString];

            for (int iCount = 0; iCount < iLenString; iCount++)
            {
                arrbBuffer[iCount] = Convert.ToByte(Convert.ToInt32(Convert.ToChar(sValue.Substring(iCount, 1))));
            }
            return arrbBuffer;
        }

        public static byte[] arrbProcessingDataToSend(byte[] arrbValue)
        {
            string sResult = "";
            sResult = CommonLib.sByteArrayToHexString(arrbValue).Replace(" ", "");
            sResult = CommonClass.sCuttingMsgFromEDC(sResult);

            // length iso message 28byte
            // header specifierny 0028
            // seharusnya 001C
            sResult = sResult.Substring(4, sResult.Length - 4);
            string sLength = sLengthDataToHex(sResult.Length / 2);
            sResult = sLength + sResult;

            //proses perubahaan hexsa ke character
            byte[] arrbByte = CommonLib.HexStringToByteArray(sResult);
            //sResult = Common.ByteArrayToCharString(arrbByte);

            return arrbByte;
        }

        /// <summary>
        /// Convert length data from decimal to hex string.
        /// The format for the Hex value is 4 character. 
        /// If the length of conversion's result less than 4 then add character "0" if front of hex value.
        /// </summary>
        /// <param name="iLength">int : Lengt of the data</param>
        /// <returns>string : length of the data in Hex value</returns>
        private static string sLengthDataToHex(int iLength)
        {
            string sLen = CommonLib.sConvertDecToHex(iLength);
            switch (sLen.Length)
            {
                case 1: // if length = 1
                    sLen = "000" + sLen;
                    break;
                case 2:// if length = 2
                    sLen = "00" + sLen;
                    break;
                case 3:// if length = 3
                    sLen = "0" + sLen;
                    break;
                default: // if length = 4
                    break;
            };
            return sLen;
        }

        /// <summary>
        /// Create check sum from given value.
        /// Lenth of the check sum must be 2.
        /// </summary>
        /// <param name="sValue">string : value to be checked</param>
        /// <returns>string : check sum in Hex value</returns>
        private static string sCheckSum(string sValue)
        {
            Int32 iResult = 0;
            for (int iCount = 0; iCount < sValue.Length; iCount += 2)
                iResult = iResult ^ Convert.ToInt32(sValue.Substring(iCount, 2), 16);  // XOR

            string sResultBinary = Convert.ToString(iResult, 2);
            string sResultHex = Convert.ToString(Convert.ToInt32(sResultBinary, 2), 16);
            sResultHex = "00".Substring(0, 2 - sResultHex.Length) + sResultHex;
            return sResultHex;
        }

        private static string sAddingDataToEDCMsg(string sValue)
        {
            return "02" + sValue + "03" + sCheckSum(sValue + "03");
        }

        //Tes
        /// <summary>
        /// Converting Hex value to decimal
        /// </summary>
        /// <param name="sValue">string : Hex value for conversion</param>
        /// <returns>string : conversion's result in decimal value</returns>
        public static string sConvertHextoDec(string sValue)
        {
            string sResultValue = Convert.ToString(Convert.ToInt32(sValue.Substring(2, 2), 16));
            return "0000".Substring(0, 4 - sResultValue.Length) + sResultValue;
        }

        /// <summary>
        /// Converts the first 4 characters of given value into decimal.
        /// The given value is in Hex value.
        /// </summary>
        /// <param name="sValue">string : Hex value to be cut and converted</param>
        /// <returns>string : decimal value resulted from the conversion</returns>
        private static string sConvert4CharInFrontIntoDec(string sValue)
        {
            return sConvertHextoDec(sValue.Substring(0, 4)) + sValue.Substring(4, sValue.Length - 4);
        }

        public static byte[] sProcessingDataToWrite(byte[] arrbValue)
        {
            if (arrbValue != null)
            {
                string sResult = "";
                //byte[] arrbByte = CharStringtoByteArray(sValue);
                sResult = CommonLib.sByteArrayToHexString(arrbValue).Replace(" ", "");
                sResult = sConvert4CharInFrontIntoDec(sResult);
                sResult = sAddingDataToEDCMsg(sResult);
                //proses perubahaan hexsa ke character 
                byte[] arrbByte = CommonLib.HexStringToByteArray(sResult);

                return arrbByte;
            }
            else return null;
        }


        /// <summary>
        /// Get the directory which holds error folder
        /// </summary>
        /// <returns>string : The directory path</returns>
        protected static string sGetErrorDirectory()
        {
            return Application.StartupPath + "\\LOGS";
        }

        /// <summary>
        /// Determines if the Error Folder is already exists or not
        /// </summary>
        /// <returns>boolean : true if folder is exist, else return false</returns>
        protected static bool isHaveLogDir()
        {
            if (Directory.Exists(sGetErrorDirectory())) 
                return true;
            else 
                return false;
        }


        public static void doWritelogFile(string sError)
        {
            if (!isHaveLogDir()) Directory.CreateDirectory(sGetErrorDirectory());  // If folder not found then crete the error folder
            CommonLib.Log(sGetErrorDirectory(), sError); // Write error to file
        }

        /// <summary>
        /// Determine if the given value match desired format.
        /// </summary>
        /// <param name="sText">string : value to be validated</param>
        /// <param name="sFormat">string : desired format for the value</param>
        /// <returns>boolean : true if value is valid, else return false</returns>
        public static bool isFormatValid(string sText, string sFormat)
        {
            string sValidInput = "!@#$%&*()_+-={}[]:\";'<>,.?|/\\ ";

            switch (sFormat)
            {
                case "H": return Regex.IsMatch(sText, "^[a-fA-F0-9]+$"); // Hexadecimal
                case "A": return Regex.IsMatch(sText, "^[a-zA-Z0-9]+$"); // Aplhanumeric
                case "A1": return Regex.IsMatch(sText, "^[a-zA-Z0-9\\s]+$"); // Alphanumeric and white space
                case "A2": return Regex.IsMatch(sText, "^[a-zA-Z0-9]+$"); // Aplhanumeric in asterix display
                case "N": return Regex.IsMatch(sText, "^[0-9]+$"); // Numeric
                case "N1": return Regex.IsMatch(sText, "^((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])$"); // Ip address
                case "S": // Special characters
                    foreach (char c in sText.Trim().ToUpper().ToCharArray())
                        if (!sValidInput.Contains(c.ToString()) &&
                            (c < 'A' || c > 'Z') &&
                            (c < '0' || c > '9')) return false;
                    return true;
                case "T": return CommonLib.isValidDatetime(sText); // Datetime
            }
            return false;
        }

        /// <summary>
        /// Generate TLV value
        /// </summary>
        /// <param name="sTag">string : Tag title</param>
        /// <param name="sValue">string : Tag value</param>
        /// <param name="sLength">string : Length of tag value</param>
        /// <returns>string : TLV Value</returns>
        public static string sGenerateTLV(string sTag, string sValue, string sLength)
        {
                return sTag + ((string)sValue.Length.ToString()).PadLeft(int.Parse(sLength), '0') + sValue;
        }
        /// <summary>
        /// Determines if the template definition alread exists or not
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        /// <returns>boolean : true if template definition is found, else return false</returns>
        //public static bool IsTemplateDefinitionExists(SqlConnection oSqlConn)
        //{
        //    return
        //        (int)new SqlCommand(CommonSP.sSPUploadTagDE1Count, oSqlConn).ExecuteScalar() > 0 ||
        //        (int)new SqlCommand(CommonSP.sSPUploadTagAA1Count, oSqlConn).ExecuteScalar() > 0;
        //}
        public static bool IsTemplateDefinitionExists(SqlConnection oSqlConn, string sDbName)
        {

            SqlCommand cmdDECount = new SqlCommand(CommonSP.sSPUploadTagDECount, oSqlConn);

            cmdDECount.CommandType = CommandType.StoredProcedure;
            cmdDECount.Parameters.Add("@sCond", SqlDbType.VarChar).Value = " where Template = '" + sDbName.ToString() + "'";

            SqlCommand cmdAACount = new SqlCommand(CommonSP.sSPUploadTagAACount, oSqlConn);

            cmdAACount.CommandType = CommandType.StoredProcedure;
            cmdAACount.Parameters.Add("@sCond", SqlDbType.VarChar).Value = " where Template = '" + sDbName.ToString() + "'";
            
               
            return
                 (int)cmdDECount.ExecuteScalar() > 0 || (int)cmdAACount.ExecuteScalar() > 0;
        }
      

        /// <summary>
        /// Determines if the upload definition alread exists or not
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        /// <returns>boolean : true if upload definition is found, else return false</returns>
        public static bool IsUploadDefinitionExists(SqlConnection oSqlConn, string sTemplate)
        {
            
            SqlCommand cmdDESourceCount = new SqlCommand(CommonSP.sSPUploadTagDESourceCount, oSqlConn);
            cmdDESourceCount.CommandType = CommandType.StoredProcedure;
            cmdDESourceCount.Parameters.Add("@sTemplate", SqlDbType.VarChar, 25).Value = sTemplate;

            SqlCommand cmdAASourceCount = new SqlCommand(CommonSP.sSPUploadTagAASourceCount, oSqlConn);
            cmdAASourceCount.CommandType = CommandType.StoredProcedure;
            cmdAASourceCount.Parameters.Add("@sTemplate", SqlDbType.VarChar, 25).Value = sTemplate;

            return
                (int)cmdDESourceCount.ExecuteScalar() > 0 ||
                (int)cmdAASourceCount.ExecuteScalar() > 0;
            //return
            //    (int)new SqlCommand(CommonSP.sSPUploadTagDESourceCount, oSqlConn).ExecuteScalar() > 0 ||
            //    (int)new SqlCommand(CommonSP.sSPUploadTagAASourceCount, oSqlConn).ExecuteScalar() > 0;
        }

        /// <summary>
        /// Determines if Register old file is allowed or not.
        /// </summary>
        /// <param name="_sFilePath">string : path for the file</param>
        /// <param name="sTerminalId">string : Terminal ID wanted to be registered</param>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        /// <param name="iDbId">int : DatabaseID which contain sTerminalID</param>
        /// <returns>boolean : true if is allowed, else return false</returns>
        public static bool isCanRegisterOldFile(string _sFilePath, 
            string sTerminalId,
            SqlConnection oSqlConn,
            int iDbId)
        {
            string sFilePath = _sFilePath + sTerminalId + ".txt";
            string sFileValue = CommonLib.sReadTxtFile(sFilePath);
            if (sFileValue != null)
            {
                sFileValue = sFileValue.Replace("\"", "");
                int iIndexStart = sFileValue.IndexOf("DE0108") + "DE0108".Length;
                string sOldValue = sFileValue.Substring(iIndexStart, 8);
                StringBuilder sbFileValue = new StringBuilder(sFileValue);
                sFileValue = sbFileValue.Replace(sOldValue, sTerminalId, iIndexStart, sTerminalId.Length).ToString();
            }
            return (!string.IsNullOrEmpty(sFileValue) && isCanImportFileToDatabase(oSqlConn, iDbId, sTerminalId, sFileValue));
        }

        /// <summary>
        /// Determines if file can be imported to database or not.
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : Connection to Sql Database</param>
        /// <param name="iDbId">int : DatabaseID which contain sTerminalID</param>
        /// <param name="sTerminalId">string : Terminal ID wanted to be imported</param>
        /// <param name="sValue">string : Content of the file</param>
        /// <returns>boolean : true if file can be imported, else false</returns>
        protected static bool isCanImportFileToDatabase(SqlConnection oSqlConn,
            int iDbId,
            string sTerminalId, string sValue)
        {
            try
            {
                SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPTerminalImport, oSqlConn);
                oSqlCmd.CommandType = CommandType.StoredProcedure;
                oSqlCmd.Parameters.Add("@sDatabaseId", SqlDbType.VarChar).Value = iDbId.ToString();
                oSqlCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalId;
                oSqlCmd.Parameters.Add("@sContent", SqlDbType.VarChar).Value = sValue;

                oSqlCmd.ExecuteNonQuery();
                oSqlCmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                CommonClass.doWriteErrorFile(ex.Message);
                return false;
            }
        }


        public static bool isBulkImportProfileSuccess(string _sTerminalID, int iDatabaseId, string sTableType,
            DataTable _dtProfileTLV, DataSet _dsTables,
            SqlTransaction _oTrans, SqlConnection oSqlConn,
            ref string sError)
        {
            string sTagHeader = "DE";
            int iTableID = 2;
            string sDestTableName = "tbProfileTerminal";
            switch (sTableType.ToLower())
            {
                case "terminal":
                    sTagHeader = "DE";
                    iTableID = 2;
                    sDestTableName = "tbProfileTerminal";
                    break;
                case "acquirer":
                    sTagHeader = "AA";
                    iTableID = 3;
                    sDestTableName = "tbProfileAcquirer";
                    break;
                case "issuer":
                    sTagHeader = "AE";
                    iTableID = 4;
                    sDestTableName = "tbProfileIssuer";
                    break;
                case "relation":
                    sTagHeader = "AD";
                    iTableID = 5;
                    sDestTableName = "tbProfileRelation";
                    break;
                case "custommenu":
                    sTagHeader = "MN";
                    iTableID = 6;
                    sDestTableName = "tbProfileCustomMenu";
                    break;
                default : 
                    sTagHeader="DC";
                    iTableID=2;
                    sDestTableName="tbProfileTerminal";
                    break;
            }

            DataRow[] arrRowsProfileTLV = _dtProfileTLV.Select(string.Format("Tag LIKE '{0}%'", sTagHeader), "RowId");
            if (arrRowsProfileTLV.Length > 0)
            {
                DataTable dtFilteredProfileTLV = arrRowsProfileTLV.CopyToDataTable();
                if (dtFilteredProfileTLV.Rows.Count > 0)
                {
                    DataTable dtProfile = _dsTables.Tables[iTableID];
                    dtProfile.Rows.Clear();

                    foreach (DataRow rowFilteredProfile in dtFilteredProfileTLV.Rows)
                    {
                        DataRow rowProfile = dtProfile.NewRow();
                        rowProfile[0] = _sTerminalID;
                        rowProfile[1] = sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation"
                            && sTableType.ToLower() != "custommenu" ?
                            rowFilteredProfile["Name"] : rowFilteredProfile["Tag"];
                        rowProfile[2] = sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation"
                            && sTableType.ToLower() != "custommenu" ?
                            rowFilteredProfile["Tag"] : rowFilteredProfile["TagLength"].ToString().Length;
                        rowProfile[3] = sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation"
                            && sTableType.ToLower() != "custommenu" ?
                            rowFilteredProfile["TagLength"].ToString().Length : rowFilteredProfile["TagLength"];
                        rowProfile[4] = sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation"
                            && sTableType.ToLower() != "custommenu" ?
                            rowFilteredProfile["TagLength"] : rowFilteredProfile["TagValue"];
                        if (sTableType.ToLower() != "terminal"
                            && sTableType.ToLower() != "count"
                            && sTableType.ToLower() != "relation"
                            && sTableType.ToLower() != "custommenu")
                            rowProfile[5] = rowFilteredProfile["TagValue"];
                        dtProfile.Rows.Add(rowProfile);
                    }

                    try
                    {
                        SqlBulkCopy sqlBulkProfile = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.Default, _oTrans);
                        sqlBulkProfile.DestinationTableName = sDestTableName;
                        foreach (DataColumn col in dtProfile.Columns)
                        {
                            if (dtProfile.Columns.Contains(col.ColumnName))
                                sqlBulkProfile.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                        }
                        sqlBulkProfile.WriteToServer(dtProfile);

                        if (sTableType.ToLower() == "terminal")
                        {
                            SqlCommand oCmd = new SqlCommand("SELECT * FROM tbProfileTerminalList WHERE TerminalID = ''", oSqlConn, _oTrans);
                            DataTable dtTempColumnTerminalList = new DataTable();
                            new SqlDataAdapter(oCmd).Fill(dtTempColumnTerminalList);
                           
                            DataTable dtProfileTerminalList = _dsTables.Tables[1];
                            dtProfileTerminalList.Rows.Clear();
                            DataRow rowTerminalList = dtProfileTerminalList.NewRow();
                            rowTerminalList["TerminalID"] = _sTerminalID;
                            rowTerminalList["DatabaseID"] = iDatabaseId;
                            rowTerminalList["AllowDownload"] = 1;
                            rowTerminalList["StatusMaster"] = 0;
                            if (dtTempColumnTerminalList.Columns.Contains("RemoteDownload"))
                                rowTerminalList["RemoteDownload"] = 0;
                            if (dtTempColumnTerminalList.Columns.Contains("EMVInit"))
                                rowTerminalList["EMVInit"] = 0;
                            if (dtTempColumnTerminalList.Columns.Contains("LogoPrint"))
                                rowTerminalList["LogoPrint"] = 0;
                            if (dtTempColumnTerminalList.Columns.Contains("LogoIdle"))
                                rowTerminalList["LogoIdle"] = 0;
                            dtProfileTerminalList.Rows.Add(rowTerminalList);

                            SqlBulkCopy sqlBulkTerminalList = new SqlBulkCopy(oSqlConn, SqlBulkCopyOptions.Default, _oTrans);
                            sqlBulkTerminalList.DestinationTableName = "tbProfileTerminalList";
                            foreach (DataColumn col in dtProfileTerminalList.Columns)
                            {
                                if (dtProfileTerminalList.Columns.Contains(col.ColumnName))
                                    sqlBulkTerminalList.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                            }
                            sqlBulkTerminalList.WriteToServer(dtProfileTerminalList);
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {
                        sError = ex.Message;
                        return false;
                    }
                }
            }
            return true;
        }

    

        public static DataTable dtGetExcel(string sFilename)
        {
            string sConnString = string.Format("provider=Microsoft.Jet.OLEDB.4.0;data source={0};Extended Properties=Excel 8.0;", 
                sFilename);
            OleDbConnection oOleConn = new OleDbConnection(sConnString);
            OleDbCommand oOleCmd = new OleDbCommand("SELECT * FROM [Sheet1$]", oOleConn);

            oOleConn.Open();
            OleDbDataReader oRead = oOleCmd.ExecuteReader();
            DataTable dtTemp = new DataTable();
            dtTemp.Load(oRead);
            oRead.Close();
            oRead.Dispose();
            oOleCmd.Dispose();
            oOleConn.Close();

            return dtTemp;
        }

        public static void SyncToEms(SqlConnection oSqlConn, string sTerminalId, EmsTypeCommand typeCmd)
        {
            using (SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPXmlSyncEms, oSqlConn))
            {
                oSqlCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = sTerminalId;
                oSqlCmd.Parameters.Add("@sCommand", SqlDbType.VarChar).Value = typeCmd.ToString();
                if (oSqlConn.State != ConnectionState.Open)
                    oSqlConn.Open();
                oSqlCmd.ExecuteNonQuery();
            }
        }

        public static void AddFileSecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
        {
            // Create a new FileInfo object.
            FileInfo fInfo = new FileInfo(FileName);

            // Get a FileSecurity object that represents the 
            // current security settings.
            FileSecurity fSecurity = fInfo.GetAccessControl();

            // Add the FileSystemAccessRule to the security settings. 
            fSecurity.AddAccessRule(new FileSystemAccessRule(Account,
                                                            Rights,
                                                            ControlType));
            // Set the new access settings.
            fInfo.SetAccessControl(fSecurity);
        }
        public static string sGetEncrypt(string sClearString)
        {
            return DataEncryptionClass.sGetMD5Result(sClearString);
        }
        public static void UserAccessInsert(SqlConnection _oSqlConn, string _sTerminalId)
        {
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPUserAccessInsert, _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = UserData.sUserID;
                oCmd.ExecuteNonQuery();
            }
        }

        public static void UserAccessDelete(SqlConnection _oSqlConn, string _sTerminalId)
        {
            if(!string.IsNullOrEmpty(UserData.sUserID))
                using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPUserAccessDelete, _oSqlConn))
                {
                    if (_oSqlConn.State != ConnectionState.Open)
                        _oSqlConn.Open();
                    oCmd.CommandType = CommandType.StoredProcedure;
                    oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                    oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar).Value = UserData.sUserID;
                    oCmd.ExecuteNonQuery();
                }
        }

        public static bool IsAllowUserAccess(SqlConnection _oSqlConn, string _sTerminalId, ref string sUserIdOnAccess)
        {
            bool isAllow = true;
            using (SqlCommand oCmd = new SqlCommand(CommonSP.sSPUserAccessBrowse, _oSqlConn))
            {
                if (_oSqlConn.State != ConnectionState.Open)
                    _oSqlConn.Open();
                oCmd.CommandType = CommandType.StoredProcedure;
                oCmd.Parameters.Add("@sTerminalId", SqlDbType.VarChar).Value = _sTerminalId;
                oCmd.Parameters.Add("@sUserId", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                oCmd.Parameters.Add("@sOutput", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                oCmd.ExecuteNonQuery();
                
                sUserIdOnAccess = oCmd.Parameters["@sUserId"].Value.ToString();
                isAllow = oCmd.Parameters["@sOutput"].Value.ToString() == "1" ? false : true;
            }
            return isAllow;
        }



        public static DataTable dtGetItemListEnabled(SqlConnection _oSqlConn)
        {
            DataTable dtTemp = new DataTable();
            using (SqlCommand oComm = new SqlCommand(CommonSP.sSPItemEnableBrowse, _oSqlConn))
            {
                oComm.CommandType = CommandType.StoredProcedure;
                oComm.Parameters.Add("@sGroupID", SqlDbType.VarChar).Value = UserData.sGroupID;
                using (SqlDataAdapter oAdapt = new SqlDataAdapter(oComm))
                    oAdapt.Fill(dtTemp);
            }
            return dtTemp;
        }


        /// <summary>
        /// Create a Masked Text Box.
        /// </summary>
        /// <param name="oPointLocation">The desired location of the Masked Text Box</param>
        /// <param name="oSize">The desired size of the Masked Text Box</param>
        /// <returns>The Masked Text Box</returns>
       

    }

    class CommonMessage
    {
        public static string sUnregisteredExpiry = "Unregistered or Expired Application.";
        public static string sConfirmationTitle = "Confirmation";
        public static string sConfirmationText = "Do you want to save the changes?";
        public static string sConfirmationDelete = "Are You Sure Want to Delete?";

        #region "Menu"
        //Login
        public static string sErrLoginUserID = "User ID still empty. Please fill User ID.";
        public static string sErrLoginUserIDFormat = "User ID must only contains alphanumeric.";
        public static string sErrLoginPassword = "Password still empty. Please fill Password.";
        public static string sErrLoginUserIDPasswordNotFound = "User ID or Password not match. Please contact Administator.";
        public static string sErrLockedUserID = "User ID has been locked. Please contact Administator.";

        //Exit
        public static string sExitMessageText = "Are You Sure want to Exit?";
        public static string sExitMessageTitle = "Exit";

        //Logoff
        public static string sLogoutText = "Are You Sure want to Logout?";
        public static string sLogoutTitle = "Logout";

        //AllowInit
        public static string sInitDataText = "Use permission to Initialize Data?" + "\n" + "Status : ";
        public static string sInitDataTitle = "Initialize Data";

        //AutoInit
        public static string sErrorEmpty = "is still empty. Please fill";
        public static string sErrorFormat = "can only receive Numeric format. Please fill other value.";

        //User Management
        public static string sErrUserManUserName = "User Name still empty. Please fill User Name.";
        public static string sErrUserManUserNameFormat = "User Name must only contains alphanumeric.";
        public static string sErrUserManPassFormat = "Password must only contains alphanumeric.";
        public static string sErrUserManUidDouble = "User ID has already been used. Please change the User ID.";

        public static string sUserManDeleteTitle = "Delete User";
        public static string sUserMandDeleteUserText = "Are you sure want to delete user : '";

        //Change Password
        public static string sErrChangePassOldPassEmpty = "Old Password still Emtpy. Please Fill Your Old Password.";
        public static string sErrChangePassNewPassEmpty = "New Password still Emtpy. Please Fill Your New Password.";
        public static string sErrChangePassConfirmPassEmtyp = "Confirm Password still Emtpy. Please Fill Your Confirm Password.";
        public static string sErrChangePassMatchOldPass = "Old Password not match. Please Check Your Old Password";
        public static string sErrChangePassMatchNewPass = "New Password not match. Please Check Your New Password";
        public static string sChangePassConfirmText = "Are you sure want to change password?";
        public static string sChangePassSuccess = "Change Password Success.";

        //Primary DB
        public static string sErrPrimeDbDatabaseSource = "Server still empty. Please fill Server.";
        public static string sErrPrimeDbUserIDDB = "User ID still empty. Please fill User ID.";
        public static string sErrPrimeDbPasswordDB = "Password still empty. Please fill Password.";
        public static string sPrimeDbConnSuccess = "Connection to Database Success.";
        public static string sErrPrimeDbConnection = "Connection Failed. Please Contact Administrator.";

        //Audit Trail DB
        public static string sErrAuditTrailDBDatabaseSource = "Server Audit Trail still empty. Please fill Server Audit Trail.";
        public static string sErrAuditTrailDBUserIDDB = "User ID Audit Trail still empty. Please fill User ID Audit Trail.";
        public static string sErrAuditTrailDBPasswordDB = "Password Audit Trail still empty. Please fill Password Audit Trail.";
        public static string sAuditTrailDBConnSucces = "Connection to Audit Trail Database Success.";
        public static string sErrAuditTrailDBConnection = "Connection Failed. Please Contact Administrator.";

        //Database Template
        public static string sDatabaseTemplateTitle = "Choose Database as Template";
        public static string sDatabaseTemplateText = "Please select Database:";

        //"Upload"
        public static string sErrUploadDefinitionTitle = "Upload Definition";
        public static string sErrUploadTitle = "Upload.";
        public static string sErrUploadText = "Error Template Definition file or file is not exist.\nCreate Template Definition file?";

        public static string sErrUploadDataTitle = "Upload";
        public static string sErrUploadDataSubTitle = "Upload.";
        public static string sErrUploadDataText = "Error Map Definition file or file is not exist.\nCreate Map Definition file?";


        //"Export Profile to Text"
        public static string sErrExportProfileToText = "Database is Not selected or \"Profile/Master Profile\" is has not been selected";

        //"Debug"
        public static string sDebugTitle = "InSys";
        public static string sDebugMessage()
        {
            if (CommonVariable.IsDebugOn()) return "Disable DEBUG?";
            else return "Enable DEBUG?";
        }

        //"Testing"
        public static string sTerminal = "Terminal : ";
        public static string sInitPwd = "\nInit Pwd : ";
        public static string sF2Pwd = "\nF.2 Pwd : ";
        public static string sF99Pwd = "\nF.99 Pwd : ";

        //"Register"
        public static string sRegisterTitle = "Register Old File";
        public static string sRegisterText = "Please fill the Terminal ID";
        #endregion

        #region "Profile"
        //"Profile"
        public static string sCardDelConfirmText = "Are you sure want to delete card?";
        public static string sDelConfirmText = "Are you sure want to delete ";
        public static string sCopyNewCardName = "Are you sure want to copy ";//add code (ibnu) 20-10-2014
        public static string sCardNameText = "Please fill the new name Card Name";//add code (ibnu) 20-10-2014
        public static string sCopyNewCardNameOnTerminal = "Are you sure want to copy ";//add code (ibnu) 20-10-2014
        public static string sCardNameTextOnTerminal = "Please fill the new name Card Name";//add code (ibnu) 20-10-2014
        public static string sTleNameText = "Please fill the new  Name";//add code (ibnu) 20-10-2014
        public static string sAIDNameText = "Please fill the new name Tle Name";//add code (ibnu) 27-10-2014
        public static string sTleIdText = "Please fill the new TLEID";
        //"Copy Terminal"
        public static string sCopyTerminalTitle = "New Terminal ID";
        public static string sCopyTerminalText = "Please fill the new Terminal ID";

        //"SQL Error Message"
        public static string sSQLErrorPKConstraint = "Violation of PRIMARY KEY constraint";
        public static string sSQLErrorDuplicateKey = "Cannot insert duplicate key in object";

        //message copy AID
        public static string sCopyAID = "Are you sure want to copy ";//add code (ibnu) 22-10-2014
        public static string sCopyCardPromtText = "Please fill the new AID Name ";//add code (ibnu) 22-10-2014

        #endregion

        #region "Log"
        public static string sRegisterSuccess = "Register Success";
        public static string sAccessLog = "Access Log";
        public static string sDeleteLog = "Delete Log";
        public static string sExportDataToExcel = "Export Data to Excel File";
        public static string sImportDataFromExcel = "Import Data from Excel File";
        public static string sUpdateDBPrimaryConn = "Update Primary Database Connection";
        public static string sChangePass = "User Change Password";
        public static string sLoginSucccess = "Login Success";
        public static string sLogoutSuccess = "User Logout";
        public static string sLogOutSuccess_ConnStringChanged = sLogoutSuccess + " caused by changing Connection String.";
        public static string sAddUserMngmt = "Add User Management";
        public static string sDeleteUserMngmt = "Delete User Management";
        public static string sEditUserMngmt = "Edit User Management";
        public static string sUpdateUserMngmt = "Update User Management";
        public static string sTrackEDC = "View EDC Tracker"; //"Browse"
        public static string sDownload = "Download File"; //"Download"
        public static string sViewUnited = "View United"; //"Monitor"
        public static string sViewUnitedDetail = "View United Detail";
        public static string sUpdDBConn = "Update Database Connection";
        public static string sDebugAndTesting = "Debug and Testing";
        public static string sFrmDebugOld = "Debug";
        public static string sLogInsys = "Log Insys";
        public static string sSetModem = "Setting Modem";
        public static string sAccessInitTrail = "Access Init Trail";
        public static string sInitTrailImportDetail = "Init Trail Import Detail";
        public static string sInitTrailImportSummary = "Init Trail Import Summary";
        public static string sInitTrailExportDetail = "Init Trail Export Detail";
        public static string sInitTrailExportSummary = "Init Trail Export Summary";
        public static string sInitTrailDeleteDetail = "Init Trail Delete Detail";
        public static string sInitTrailDeleteSummary = "Init Trail Delete Summary";

        public static string sAccessInitSoftwareTrail = "Access Init Software Trail";
        public static string sInitSoftwareTrailImportDetail = "Init Software Trail Import Detail";
        public static string sInitSoftwareTrailExportDetail = "Init Software Trail Export Detail";
        public static string sInitSoftwareTrailDeleteDetail = "Init Software Trail Delete Detail";
        public static string sExportProfileToText = "Export profile to Textfile";
        public static string sFormOpened = "View Form ";
        #endregion

        public static string sInputPassword = "Input Password";
        public static string sFrmPromptString = "Prompt String";
        public static string sViewSN = "View Terminal Serial Number";

        public static string sAutoInitPathEmpty = "Auto Init Report Path is still empty. Please fill Auto Init Report Path.";
        public static string sInitCompressEmpty = "Init Compress Path is still empty. Please fill Init Compress Path";

        #region "AID"
        public static string sErrAIDInvalid = "Invalid AID or AID List allready exist";
        public static string sErrAIDEmptyField = "AID Field cannot empty";
        #endregion

        #region "CAPK"
        public static string sErrCAPKInvalid = "Invalid CAPK or CAPK Index allready exist";
        public static string sErrCAPKEmptyField = "CAPK Field cannot empty.";
        #endregion

        #region "Version"
        public static string sDbIDNotValid = "Database ID is not valid. Please input other value.";
        public static string sDbIdNewEmpty = "New Database ID is still empty. Please fill new Database ID.";
        public static string sDbIdExistingEmpty = "Existing Database ID is still empty. Please fill existing Database ID";
        public static string sDbSourceEqDbDest = "Database source is similar with Database Destination. Please choose other Database Destination.";
        public static string sDbNameEmpty = "Database Name is still empty. Please fill Database Name.";
        public static string sNotChecked = " is not selected.";
        public static string sIsEmpty = "is empty. Please fill";
        public static string sConfirmDeleteDbVersion = "Are you sure want to delete this version ?";
        public static string sConfirmDeleteDbVersionProfile = "Deleting this version will also delete all profiles in this Version.\nAre you sure want to delete this Version ?";
        public static string sConfirmDeleteDbVersionCardList = "Deleting this version will also delete all Card List in this Version.\nAre you sure want to delete this Version ?";
        #endregion

        #region "Application"
        public static string sFieldEmpty = " is still empty. Please fill ";
        public static string sAllowCompressEmpty = "Please select Allow Compress value.";
        public static string sFieldNotNumeric = " has invalid format (can only receive numbers).";
        #endregion

        //"Software Package"
        public static string sSoftwarePackageTitle = "Choose Software Package Name";
        public static string sSoftwarePackageText = "Please select Package name :";

    }

    class CommonVariable
    {
        public const string PathMyComputer = "::{20D04FE0-3AEA-1069-A2D8-08002B30309D}";
        public const string PathMyDocuments ="::{450D8FBA-AD25-11D0-98A8-0800361B1103}";
        public const string PathMyNetworkPlaces = "::{208D2C60-3AEA-1069-A2D7-08002B30309D}";
        public const string PathPrinters = "::{2227A280-3AEA-1069-A2DE-08002B30309D}";
        public const string PathRecycleBin = "::{645FF040-5081-101B-9F08-00AA002F954E}";

        protected static bool isDebug;
        protected static bool isInitData;

        /// <summary>
        /// Determines if Debug is on or off
        /// </summary>
        /// <returns>boolean : true if on, else false</returns>
        public static bool IsDebugOn()
        {
            return isDebug;
        }

        /// <summary>
        /// Set status of debug
        /// </summary>
        /// <param name="_isDebug">boolean : status of debug, true if on</param>
        public static void SetisDebugOn(bool _isDebug)
        {
            isDebug = _isDebug;
        }

        /// <summary>
        /// Determines if Init data is allowed or not.
        /// </summary>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        /// <returns>boolean : true if allowed, else false</returns>
        public static bool IsCanInitData(SqlConnection oSqlConn)
        {
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPControlFlagAllowInitBrowse, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            
            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            SqlDataReader oReader = oSqlCmd.ExecuteReader();
            if (oReader.Read())
                isInitData = oReader["Flag"].ToString() == "1" ? true : false;

            oReader.Close();
            oReader.Dispose();
            oSqlCmd.Dispose();

            return isInitData;
        }

        /// <summary>
        /// Set the status of Allow Init data
        /// </summary>
        /// <param name="_isInitData">boolean : true if InitAllowed, else false</param>
        /// <param name="oSqlConn">Sql Connection : Connecton to Sql Database</param>
        public static void SetIsCanInitData(bool _isInitData, SqlConnection oSqlConn)
        {
            isInitData = _isInitData;
            SqlCommand oSqlCmd = new SqlCommand(CommonSP.sSPControlFlagAllowInitUpdate, oSqlConn);
            oSqlCmd.CommandType = CommandType.StoredProcedure;
            oSqlCmd.Parameters.Add("@bAllowInit", SqlDbType.Bit).Value = isInitData;

            if (oSqlConn.State == ConnectionState.Closed) oSqlConn.Open();
            oSqlCmd.ExecuteNonQuery();
            oSqlCmd.Dispose();
        }
    }
}