﻿using Ini;
using InSysClass;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

namespace InSysConsoleAutoInitReport
{
    class InitData
    {
        protected string sDataSource;
        protected string sDatabase;
        protected string sUserID;
        protected string sPassword;
        protected string sDirectory;

        public InitData(string _sDirectory)
        {
            sDirectory = _sDirectory;
            doGetInitData();
        }

        protected void doGetInitData()
        {
            string sFileName = sDirectory + "\\Application.config";
            string sEncryptFileName = sDirectory + "\\Application.conf";

            if (File.Exists(sEncryptFileName))
            {
                EncryptionLib.DecryptFile(sEncryptFileName, sFileName);
                try
                {
                    IniFile objIniFile = new IniFile(sFileName);
                    sDataSource = objIniFile.IniReadValue("Database", "DataSource");
                    sDatabase = objIniFile.IniReadValue("Database", "Database");
                    sUserID = objIniFile.IniReadValue("Database", "UserID");
                    sPassword = objIniFile.IniReadValue("Database", "Password");
                }
                catch (Exception)
                {
                    //Logs.doWriteErrorFile(sDirectory, ex.Message);
                }
                File.Delete(sFileName);
            }
        }

        public string sGetDataSource()
        {
            return sDataSource;
        }

        public string sGetDatabase()
        {
            return sDatabase;
        }

        public string sGetUserID()
        {
            return sUserID;
        }

        public string sGetPassword()
        {
            return sPassword;
        }

        public string sGetConnString()
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword);
        }

        public string sGetConnString(int iMaxPoolSize)
        {
            return SQLConnLib.sConnStringAsyncMARSMaxPool(sDataSource, sDatabase, sUserID, sPassword, iMaxPoolSize);
        }

    }
    class Program
    {
        static SqlConnection oSqlConn;
        static string sConnString;
        static string sAppDirectory = Directory.GetCurrentDirectory();
        static CultureInfo ciUSFormat = new CultureInfo("en-US"); // English - United States

        static void Main(string[] args)
        {
            try
            {
                string path = ConfigurationManager.AppSettings["Path"].ToString();
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                string filename = "AutoInit-"+ sGetMMMddyyyyhhmmsstt()+".txt";
                string pathfilename = path +"\\"+ filename;

                Console.WriteLine("START: AutoInitReport");

                DataTable table  = new DataTable();
                oSqlConn = null;
                oSqlConn = InitConnection();
                
                SqlCommand sqlcommand = new SqlCommand("spAutoInitLogGenerateReportConsole", oSqlConn);
                Console.WriteLine("spAutoInitLogGenerateReportConsole");

                if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
                Console.WriteLine("After oSqlConn");
                sqlcommand.CommandType = CommandType.StoredProcedure;
                Console.WriteLine("Before ExecuteReader");
                SqlDataReader reader = sqlcommand.ExecuteReader();

                if (reader.HasRows)
                {
                    System.Threading.Thread.Sleep(2500);
                    table.Load(reader);
                }
                else
                {
                    Console.WriteLine("No rows found.");
                }
                reader.Close();

                sqlcommand.Dispose();                   

                var result = new StringBuilder();
                foreach (DataRow row in table.Rows)
                {
                    for (int i = 0; i < table.Columns.Count; i++)
                    {
                        result.Append(row[i].ToString());
                        result.Append(i == table.Columns.Count - 1 ? "\n" : ",");
                    }
                    result.AppendLine();
                }

                StreamWriter objWriter = new StreamWriter(pathfilename, false);
                Console.WriteLine("SUCCESS : AutoInitReport -> Location :" + pathfilename);
                objWriter.WriteLine(result.ToString());
                objWriter.Close();
                Console.WriteLine("FINISH : AutoInitReport ");
                Console.ReadKey();
            }
            catch ( Exception ex)
            {
                CommonClass.doWritelogFile("Error : "+ ex.Message+" "+Environment.StackTrace);
                Console.WriteLine("Error : " + ex.Message + " " + Environment.StackTrace);
                Console.ReadKey();
            }

        }

        protected static DataTable GetAutoInitLogGenerateReportConsole()
        {
            DataTable dtTemp = new DataTable();
            if (oSqlConn.State != ConnectionState.Open) oSqlConn.Open();
            using (SqlCommand sqlcommand = new SqlCommand("spAutoInitLogGenerateReportConsole", oSqlConn))
            {
                sqlcommand.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader reader = sqlcommand.ExecuteReader())
                {
                    if (reader.HasRows) dtTemp.Load(reader);
                }
            }
            return dtTemp;
        }

        protected static void Write2File(string sFileName, string sValue, bool bNewLine)
        {
            try
            {
                using (FileStream oFileStream = new FileStream(sFileName, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter oStreamWriter = new StreamWriter(oFileStream))
                    {
                        oStreamWriter.BaseStream.Seek(0, SeekOrigin.End);
                        if (bNewLine) oStreamWriter.WriteLine(sValue);
                        else oStreamWriter.Write(sValue);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }

        protected static string sGetMMMddyyyyhhmmsstt()
        {
            return DateTime.Now.ToString("yyMMddhhmmss", ciUSFormat);
        }

        static SqlConnection InitConnection()
        {
            SqlConnection oSqlTempConn = new SqlConnection();
            try
            {
                InitData oObjInit = new InitData(sAppDirectory);
                sConnString = oObjInit.sGetConnString();
                sConnString = oObjInit.sGetConnString(int.Parse(ConfigurationManager.AppSettings["Max Pool Size"].ToString()));
                oSqlTempConn = SQLConnLib.EstablishConnection(sConnString);
                oSqlTempConn.InitializeLifetimeService();

                Console.WriteLine("InitConnection");
            }
            catch (Exception)
            {
                oSqlTempConn = null;
            }

            return oSqlTempConn;
        }


    }
}
